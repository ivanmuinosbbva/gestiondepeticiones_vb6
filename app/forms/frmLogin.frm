VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmLogin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CGM"
   ClientHeight    =   3180
   ClientLeft      =   2895
   ClientTop       =   2535
   ClientWidth     =   4365
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLogin.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmLogin.frx":058A
   ScaleHeight     =   3180
   ScaleWidth      =   4365
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "C&ontinuar"
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2640
      Width           =   885
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Acept&ar"
      Default         =   -1  'True
      Height          =   375
      Left            =   2400
      TabIndex        =   2
      Top             =   2640
      Width           =   885
   End
   Begin VB.TextBox USER_PASSWORD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   2040
      PasswordChar    =   "�"
      TabIndex        =   1
      Top             =   1320
      Width           =   1485
   End
   Begin AT_MaskText.MaskText USER_ID 
      Height          =   300
      Left            =   2040
      TabIndex        =   0
      Top             =   840
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   529
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      UpperCase       =   -1  'True
   End
   Begin VB.Label lblWarningTitle 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Bloq May�s activado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   165
      Left            =   480
      TabIndex        =   9
      Top             =   1920
      Width           =   1530
   End
   Begin VB.Image imgWarning 
      Height          =   240
      Left            =   120
      Picture         =   "frmLogin.frx":39A4
      Top             =   1920
      Width           =   240
   End
   Begin VB.Label lblWarningMsg 
      BackStyle       =   0  'Transparent
      Caption         =   "Si tiene activada la tecla Bloq May�s es posible que escriba incorrectamente su contrase�a."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   600
      TabIndex        =   8
      Top             =   2160
      Width           =   3015
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblMensaje 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "          "
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   480
      TabIndex        =   7
      Top             =   2640
      Width           =   450
   End
   Begin VB.Label lblAplicacion 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ingreso al sistema"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblContrasenia 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Co&ntrase�a"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   720
      TabIndex        =   5
      Top             =   1320
      Width           =   840
   End
   Begin VB.Label lblIdentificacion 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "&Usuario"
      ForeColor       =   &H8000000E&
      Height          =   195
      Left            =   1080
      TabIndex        =   4
      Top             =   840
      Width           =   540
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 28.10.2015 - Nuevo: se controla que la pwd ingresada tenga al menos 8 caracteres.
' -001- b. FJS 28.10.2015 - Nuevo: se agrega Trim a los campos para quitar los espacios al principio y al final de la cadena.
' -002- a. FJS 29.12.2015 - Nuevo: se agrega esta condici�n para evitar que ejecute la rutina antes de estar conectado a la base de datos.
' -003- a. FJS 14.03.2016 - Nuevo: control de Caps Lock activado (para evitar errores al ingresar contrase�as).
'GMT01 - PET 11111 - SE FUERZAN LOS PARAMETROS POR EL ADMINISTRADOR

Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

Option Explicit

Dim Hab_Tips As Integer 'GMT01

Private Sub Form_Load()
    USER_ID = cstmGetUserName
    Call setHabilCtrl(cmdContinuar, DISABLE)
    Call ActivarWarning(False)
    Call Puntero(False)
End Sub

Private Sub cmdAceptar_Click()
    Call Puntero(True)

    If Len(Trim(USER_ID)) < 1 Then          ' upd -001- b.
        MsgBox "Debe ingresar un usuario v�lido para trabajar con la aplicaci�n.", vbExclamation + vbOKOnly, "Falta ingresar el usuario"
        Call Puntero(False)
        If USER_ID.Enabled Then
            USER_ID.SetFocus
        Else
            USER_ID.Enabled = True
        End If
        Exit Sub
    End If
    If Len(Trim(USER_PASSWORD)) < 1 Then    ' upd -001- b.
        MsgBox "Debe ingresar una contrase�a para entrar a la aplicaci�n.", vbExclamation + vbOKOnly, "Falta ingresar la contrase�a"
        Call Puntero(False)
        If USER_PASSWORD.Enabled Then
            USER_PASSWORD.SetFocus
        Else
            USER_PASSWORD.Enabled = True
        End If
        Exit Sub
    End If
    USER_ID.Enabled = False
    USER_PASSWORD.Enabled = False
    ''{ add -001- a.
    'If Len(Trim(USER_PASSWORD)) < 8 Then
    '    MsgBox "La longitud de la contrase�a para entrar a la" & vbCrLf & "aplicaci�n debe ser mayor o igual a 8 caracteres.", vbExclamation + vbOKOnly, "Falta ingresar la contrase�a"
    '    Call Puntero(False)
    '    USER_PASSWORD.SetFocus
    '    Exit Sub
    'End If
    ''}
    lblMensaje = ""
    Call HabilitarMenues(False)
    Call setHabilCtrl(cmdAceptar, DISABLE)
    Call Status("Intentando la conexi�n...")
    glLOGIN_SuperUser = False
    glAdoError = ""
    lblMensaje.ToolTipText = ""
    
    With mdiPrincipal.AdoConnection
        .CerrarConexion
        .SetODBCDriver glDRIVER_ODBC
        If InStr(1, "TEST|DESA|", glENTORNO, vbTextCompare) > 0 Then
            .UsuarioAplicacion = Desencriptar(glBASEUSR)
            .PasswordAplicacion = Desencriptar(glBASEPWD)
        Else
            .UsuarioAplicacion = USER_ID
            .PasswordAplicacion = USER_PASSWORD
        End If
        .BaseAplicacion = ""
        .ComponenteSeguridad = ""
        .Aplicacion = glAPLICACION_SYBASE
        .BaseAplicacion = glBASE_APLICACION
        .ServidorDSN = glSERVER_DSN             ' add *
        .Servidor = glSERVER_SEGURIDAD
        .servidorPuerto = glSERVER_PUERTO       ' add *
        .PasswordSeguridad = glPASSWORD_SEGURIDAD
        .BaseSeguridad = glBASE_SEGURIDAD
        .UsuarioSeguridad = glUSUARIO_SEGURIDAD
        .TimeOut = 120
        .Encriptado = IIf(CInt(glPASSWORD_ENCRIPTADO) > 0, True, False)
        .EsquemaSeguridad = IIf(glESQUEMA_SEGURIDAD = "SI", True, False)
        .AbrirConexion
    End With
    
    With mdiPrincipal
        glONLINE = False
        If .AdoConnection.IsOnLine Then
            'MsgBox "Conectado"
            If Not .AdoConnection.CrearDSN("Conexion Para Crystal", "CRREP", .AdoConnection.BaseAplicacion) Then
                MsgBox "Error DSN CRREPORT:" & Chr$(13) & glAdoError
            End If
            glLOGIN_ID_REAL = USER_ID
            glLOGIN_PASSWORD = USER_PASSWORD
            glLOGIN_Equipo = cstmGetComputerName
            Set aplCONN = .AdoConnection.ConexionBase          ' ??????
            
            'MsgBox .AdoConnection.ConexionBase.ConnectionString, vbInformation + vbOKOnly
            'MsgBox aplCONN.ConnectionString, vbInformation + vbOKOnly
            
            Call PerfilCrear
            .sbPrincipal.Panels(4) = ""             ' Perfil actuante del recurso actual
            If InicializarRecurso(USER_ID) Then
                lblMensaje = ""
                lblMensaje.visible = True
                .sbPrincipal.Panels(2) = .AdoConnection.UsuarioAplicacion       ' upd -006- b. Antes: USER_ID
                .sbPrincipal.Panels(3) = USER_ID
                .sbPrincipal.Panels(5) = glLOGIN_NAME_REEMPLAZO
                .sbPrincipal.Panels(6) = .AdoConnection.Servidor '& " (" & .AdoConnection.DRIVER & "/" & glENTORNO & ")"
                .sbPrincipal.Panels(6).ToolTipText = .sbPrincipal.Panels(6).text
                'Load frmPeticionesShellMsg2            ' Nueva bandeja de mensajes
                Call setHabilCtrl(cmdContinuar, NORMAL)
                cmdContinuar.SetFocus
                glONLINE = True
            Else
                lblMensaje.ForeColor = COL_ROJO
                lblMensaje = "Error en el ingreso."
                lblMensaje.visible = True
                USER_ID.Enabled = True
                USER_PASSWORD.Enabled = True
                USER_PASSWORD.SetFocus
                Call setHabilCtrl(cmdAceptar, NORMAL)
                Call setHabilCtrl(cmdContinuar, DISABLE)
            End If
        Else
            lblMensaje.ForeColor = COL_ROJO
            lblMensaje = "Error en el ingreso."
            lblMensaje.ToolTipText = "Haga doble click aqu� para ver una descripci�n del error."
            lblMensaje.visible = True
            USER_ID.Enabled = True
            USER_PASSWORD.Enabled = True
            Call setHabilCtrl(cmdAceptar, NORMAL)
        End If
        'USER_ID.Enabled = True
        Call Puntero(False)
        Call Status("Listo.")
    End With
End Sub

Private Sub cmdContinuar_Click()
    Me.Hide
    USER_ID.Enabled = True
    USER_PASSWORD.Enabled = True
    If glONLINE Then
        Call Puntero(True)
        Call HabilitarMenues(True)
        
        'GMT01 - INI
        ' Forzar parametros por marca de hablitacion
        Call recupHabilitaciones
        Select Case Hab_Tips
            Case 0
               SaveSetting "GesPet", "Preferencias\General", "IniciarTips", "N"
            Case 1
               SaveSetting "GesPet", "Preferencias\General", "IniciarTips", "S"
            Case 2
               Hab_Tips = 2
            Case 3
         'Esto se pone para enviar tips a un grupo especifico de usuarios en CGM, despues deberia parametrizarse (se hace prueba con el grupo de corporate function y homologacion
               If glLOGIN_Grupo = "11-21" Or glLOGIN_Grupo = "24-03" Then
                  SaveSetting "GesPet", "Preferencias\General", "IniciarTips", "S"
               Else
                  SaveSetting "GesPet", "Preferencias\General", "IniciarTips", "N"
               End If
            Case Else
               SaveSetting "GesPet", "Preferencias\General", "IniciarTips", "N"
        End Select
        'GMT01 - FIN
        
        Call Puntero(False)
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not glONLINE Then
        Call HabilitarMenues(False)
    End If
End Sub

Private Sub lblMensaje_DblClick()
    If glAdoError <> "" Then
        MsgBox (glAdoError)
    End If
End Sub

Public Function InvisibleLogin(sUserName As String, sUserPassword As String) As Boolean
    Call HabilitarMenues(False)
    Call Puntero(True)
    Call Status("Intentando la conexi�n...")
    glLOGIN_SuperUser = False
    glAdoError = ""
    With mdiPrincipal
        .AdoConnection.CerrarConexion
        .AdoConnection.SetODBCDriver glDRIVER_ODBC
        .AdoConnection.UsuarioAplicacion = sUserName
        .AdoConnection.PasswordAplicacion = sUserPassword
        .AdoConnection.BaseAplicacion = ""
        .AdoConnection.ComponenteSeguridad = ""
        .AdoConnection.Aplicacion = glAPLICACION_SYBASE
        .AdoConnection.BaseAplicacion = glBASE_APLICACION
        .AdoConnection.ServidorDSN = glSERVER_DSN
        .AdoConnection.Servidor = glSERVER_SEGURIDAD
        .AdoConnection.PasswordSeguridad = glPASSWORD_SEGURIDAD
        .AdoConnection.BaseSeguridad = glBASE_SEGURIDAD
        .AdoConnection.UsuarioSeguridad = glUSUARIO_SEGURIDAD
        .AdoConnection.TimeOut = 120
        .AdoConnection.Encriptado = IIf(CInt(glPASSWORD_ENCRIPTADO) > 0, True, False)
        .AdoConnection.EsquemaSeguridad = IIf(glESQUEMA_SEGURIDAD = "SI", True, False)
        .AdoConnection.AbrirConexion
        If .AdoConnection.IsOnLine Then
            If Not .AdoConnection.CrearDSN("Conexion Para Crystal", "CRREP", .AdoConnection.BaseAplicacion) Then
                MsgBox "Error DSN CRREPORT:" & Chr$(13) & glAdoError
            End If
            .sbPrincipal.Panels(2) = sUserName
            .sbPrincipal.Panels(6) = .AdoConnection.Servidor '& " (" & .AdoConnection.DRIVER & "/" & glENTORNO & ")"
            .sbPrincipal.Panels(6).ToolTipText = .sbPrincipal.Panels(6).text
            glLOGIN_ID_REAL = sUserName
            glLOGIN_PASSWORD = sUserPassword
            Set aplCONN = .AdoConnection.ConexionBase
            Call PerfilCrear
            .sbPrincipal.Panels(4) = ""
            If InicializarRecurso(sUserName) Then
               .sbPrincipal.Panels(3) = sUserName
               .sbPrincipal.Panels(5) = glLOGIN_NAME_REEMPLAZO
            End If
            glONLINE = True
            InvisibleLogin = True
            cmdContinuar_Click
        Else
            Call setHabilCtrl(cmdAceptar, NORMAL)
            glONLINE = False
            InvisibleLogin = False
        End If
    End With
    Call Puntero(False)
    Call Status("Listo.")
End Function

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Call Validar
    If KeyCode = 27 Then
        Me.Hide
    End If
    With mdiPrincipal
        'If InStr(1, "XA00309|A114901|", cstmGetUserName, vbTextCompare) > 0 Then
        'If InStr(1, "XA00309|A114901|XA01236|XA03047|XA03170|XA03365|A82033|", cstmGetUserName, vbTextCompare) > 0 Then
            If .AdoConnection.IsOnLine Then         ' add -002- a.
                If KeyCode = vbKeyF5 And Shift = 4 Then
                    If glENTORNO = "PROD" And UCase(USER_PASSWORD) = "JE09GE" Then
                        .sbPrincipal.Panels(2) = .AdoConnection.UsuarioAplicacion
                        .sbPrincipal.Panels(3) = USER_ID
                        InicializarRecurso (USER_ID)
                        .sbPrincipal.Panels(5) = glLOGIN_NAME_REEMPLAZO
                        .sbPrincipal.Panels(6) = .AdoConnection.Servidor '& " (" & .AdoConnection.DRIVER & "/" & glENTORNO & ")"
                        .sbPrincipal.Panels(6).ToolTipText = .sbPrincipal.Panels(6).text
                        glLOGIN_ID_REAL = USER_ID
                        glONLINE = True
                        cmdAceptar.Enabled = False
                        cmdContinuar.Enabled = True
                        cmdContinuar.SetFocus
                    End If
                End If
                'If .AdoConnection.IsOnLine Then
                If KeyCode = vbKeyF12 And Shift = 4 Then
                    'fraLogin.Enabled = False
                    Call Puntero(True)
                    Call Status("Cargando recursos... aguarde...")
                    Load frmRecursosConsulta
                    'frmRecursosConsulta.Show 1
                    Call Puntero(False)
                    Call Status("Listo.")
                    frmRecursosConsulta.Show 1
                    'fraLogin.Enabled = True
                    If Len(USER_ID) > 0 Then USER_PASSWORD.SetFocus
                End If
            End If                                      ' add -002- a.
        'End If
    End With
    Call Validar                                        ' add -003- a.
End Sub

'{ add -003- a.
Private Sub USER_PASSWORD_Change()
    Call Validar
End Sub

Private Sub USER_PASSWORD_GotFocus()
    Call Validar
End Sub

Private Sub Validar()
    Const VK_CAPITAL As Integer = &H14
    Select Case GetKeyState(VK_CAPITAL)
        Case 1, -127: Call ActivarWarning(True)
        Case Else
            Call ActivarWarning(False)
    End Select
End Sub

Public Sub ActivarWarning(activar As Boolean)
    imgWarning.visible = activar
    lblWarningTitle.visible = activar
    lblWarningMsg.visible = activar
End Sub
'}

'GMT01 - INI
Private Function recupHabilitaciones() As Boolean
    
    'se recuperan los valores para saber que flujo de trabajo esta habilitado
    recupHabilitaciones = False
    Hab_Tips = 0

    'indica si ver la solapa de vinculados
    If sp_GetVarios("HAB_TIPS") Then
        Hab_Tips = ClearNull(aplRST.Fields!var_numero)
    End If
    
    recupHabilitaciones = True
 End Function
'GMT01 - FIN

