VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesCarga 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Datos Petici�n"
   ClientHeight    =   8250
   ClientLeft      =   915
   ClientTop       =   2730
   ClientWidth     =   11880
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmNewPeticionesCarga.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   8250
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   0
      Left            =   25
      TabIndex        =   103
      Top             =   435
      Width           =   1138
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   1
      Left            =   1180
      TabIndex        =   104
      Top             =   435
      Width           =   1138
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   2
      Left            =   2325
      TabIndex        =   105
      Top             =   435
      Width           =   1138
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   3
      Left            =   3480
      TabIndex        =   102
      Top             =   435
      Width           =   1138
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   4
      Left            =   4600
      TabIndex        =   108
      Top             =   435
      Width           =   1138
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   5
      Left            =   5760
      TabIndex        =   126
      Top             =   435
      Width           =   1138
   End
   Begin VB.CommandButton orjBtn 
      Caption         =   "Datos"
      Height          =   350
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   59
      TabStop         =   0   'False
      Top             =   120
      Width           =   1170
   End
   Begin VB.CommandButton orjBtn 
      Caption         =   "Detalle"
      Height          =   350
      Index           =   1
      Left            =   1170
      MaskColor       =   &H00E0E0E0&
      Style           =   1  'Graphical
      TabIndex        =   60
      TabStop         =   0   'False
      Top             =   120
      Width           =   1170
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "Pet. Anexas"
      Height          =   350
      Index           =   2
      Left            =   2325
      Style           =   1  'Graphical
      TabIndex        =   67
      TabStop         =   0   'False
      Top             =   120
      Width           =   1170
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "Doc.Metod."
      Height          =   350
      Index           =   3
      Left            =   3480
      Style           =   1  'Graphical
      TabIndex        =   87
      TabStop         =   0   'False
      Top             =   120
      Width           =   1170
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "Adjuntos"
      Height          =   350
      Index           =   4
      Left            =   4600
      Style           =   1  'Graphical
      TabIndex        =   109
      TabStop         =   0   'False
      Top             =   120
      Width           =   1170
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "Conforme"
      Height          =   350
      Index           =   5
      Left            =   5760
      Style           =   1  'Graphical
      TabIndex        =   127
      TabStop         =   0   'False
      Top             =   120
      Width           =   1170
   End
   Begin VB.Frame fraButtPpal 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8190
      Left            =   10320
      TabIndex        =   31
      Top             =   -30
      Width           =   1515
      Begin VB.CommandButton cmdGrupos 
         Caption         =   "Grupos"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   86
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   2640
         Width           =   1410
      End
      Begin VB.CommandButton cmdRptHs 
         Caption         =   "    Horas Trabajadas"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   85
         TabStop         =   0   'False
         ToolTipText     =   "Invoca el reporte de Horas rabajadas"
         Top             =   4485
         Width           =   1410
      End
      Begin VB.CommandButton cmdAgrupar 
         Caption         =   "Agrupamientos"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   76
         TabStop         =   0   'False
         ToolTipText     =   "Visualiza los agrupamientos que referencian esta Petici�n"
         Top             =   3360
         Width           =   1410
      End
      Begin VB.CommandButton cmdSector 
         Caption         =   "Sectores"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   42
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   2190
         Width           =   1410
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   41
         TabStop         =   0   'False
         ToolTipText     =   "Permite Eliminar la Petici�n"
         Top             =   6120
         Width           =   1410
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   40
         TabStop         =   0   'False
         ToolTipText     =   "Permite completar los datos de la Petici�n"
         Top             =   5670
         Width           =   1410
      End
      Begin VB.CommandButton cmdAsigNro 
         Caption         =   "Asigna N�mero"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   39
         TabStop         =   0   'False
         ToolTipText     =   "asigna n�mero a la petici�n"
         Top             =   4920
         Visible         =   0   'False
         Width           =   1410
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Impresi�n Formulario"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   38
         TabStop         =   0   'False
         ToolTipText     =   "Imporime un formulario con los datos de la Petici�n"
         Top             =   540
         Width           =   1410
      End
      Begin VB.CommandButton cmdCambioEstado 
         Caption         =   "     Cambio          de Estado"
         Height          =   450
         Left            =   40
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Permite cambiar el estado de la Petici�n"
         Top             =   1440
         Width           =   1410
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "GRABAR"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   34
         TabStop         =   0   'False
         ToolTipText     =   "Confirma los cambios ralizados"
         Top             =   6570
         Width           =   1410
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "CANCELAR"
         Enabled         =   0   'False
         Height          =   450
         Left            =   30
         TabIndex        =   33
         TabStop         =   0   'False
         ToolTipText     =   "Terminar la operaci�n"
         Top             =   7680
         Width           =   1410
      End
      Begin VB.CommandButton cmdHistView 
         Caption         =   "Historial"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   32
         TabStop         =   0   'False
         ToolTipText     =   "Muestra el historial de la Petici�n"
         Top             =   4080
         Width           =   1410
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "Modo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   30
         TabIndex        =   35
         Top             =   120
         Width           =   1425
      End
      Begin VB.Label lblModo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   30
         TabIndex        =   36
         Top             =   330
         Width           =   1200
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   0
      Left            =   0
      TabIndex        =   27
      Top             =   360
      Width           =   10305
      Begin VB.ComboBox cmbPetImpact 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmNewPeticionesCarga.frx":000C
         Left            =   1795
         List            =   "frmNewPeticionesCarga.frx":0016
         Style           =   2  'Dropdown List
         TabIndex        =   148
         Top             =   1740
         Width           =   855
      End
      Begin VB.ComboBox cmbPetClass 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4920
         Style           =   2  'Dropdown List
         TabIndex        =   147
         ToolTipText     =   "Indica la clase de la petici�n actual"
         Top             =   1708
         Width           =   2175
      End
      Begin VB.ComboBox cboTipopet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   145
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1260
         Width           =   1500
      End
      Begin VB.ComboBox cboImportancia 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   144
         Top             =   2175
         Width           =   2355
      End
      Begin VB.ComboBox cboBpar 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   141
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   7200
         Width           =   6960
      End
      Begin VB.ComboBox cboOrientacion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   79
         Top             =   3525
         Width           =   2025
      End
      Begin VB.ComboBox cboCorpLocal 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   78
         Top             =   3075
         Width           =   2025
      End
      Begin VB.CommandButton cmdVerAnexora 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         Picture         =   "frmNewPeticionesCarga.frx":0022
         Style           =   1  'Graphical
         TabIndex        =   71
         TabStop         =   0   'False
         ToolTipText     =   "Abre la Petici�n que agrupa a la actual"
         Top             =   4440
         Width           =   510
      End
      Begin VB.ComboBox cboPrioridad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   3990
         Width           =   2025
      End
      Begin VB.ComboBox cboSector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   7
         ToolTipText     =   "Sector Solicitante"
         Top             =   4890
         Width           =   8940
      End
      Begin VB.ComboBox cboSupervisor 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   10
         ToolTipText     =   "Recurso que actu� como Supervisor"
         Top             =   6255
         Width           =   6960
      End
      Begin VB.ComboBox cboDirector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   11
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   6720
         Width           =   6960
      End
      Begin VB.ComboBox cboSolicitante 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   8
         ToolTipText     =   "Recurso que actu� como Solicitante"
         Top             =   5340
         Width           =   6960
      End
      Begin VB.ComboBox cboReferente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1150
         Style           =   2  'Dropdown List
         TabIndex        =   9
         ToolTipText     =   "Recurso que actu� como Referente"
         Top             =   5790
         Width           =   6960
      End
      Begin AT_MaskText.MaskText txtNrointerno 
         Height          =   315
         Left            =   8460
         TabIndex        =   0
         Top             =   360
         Visible         =   0   'False
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtFechaEstado 
         Height          =   315
         Left            =   4920
         TabIndex        =   14
         ToolTipText     =   "Fecha en la que ingres� al estado actual"
         Top             =   2190
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   1155
         TabIndex        =   1
         Top             =   360
         Width           =   5325
         _ExtentX        =   9393
         _ExtentY        =   556
         MaxLength       =   50
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtEstado 
         Height          =   315
         Left            =   4920
         TabIndex        =   12
         ToolTipText     =   "Estado en que se encuentra la Petici�n"
         Top             =   795
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin AT_MaskText.MaskText txtSituacion 
         Height          =   315
         Left            =   4920
         TabIndex        =   13
         ToolTipText     =   "Situaci�n particular"
         Top             =   1245
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin AT_MaskText.MaskText txtFiniplan 
         Height          =   315
         Left            =   4920
         TabIndex        =   15
         ToolTipText     =   "Fecha de Inicio Planificada"
         Top             =   4080
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFinireal 
         Height          =   315
         Left            =   4920
         TabIndex        =   18
         ToolTipText     =   "Fecha de Inicio Real"
         Top             =   4440
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFfinplan 
         Height          =   315
         Left            =   6885
         TabIndex        =   16
         ToolTipText     =   "Fecha de finalizaci�n Planificada"
         Top             =   4080
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFfinreal 
         Height          =   315
         Left            =   6885
         TabIndex        =   19
         ToolTipText     =   "Fecha de Finalizaci�n Real"
         Top             =   4440
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHspresup 
         Height          =   315
         Left            =   4920
         TabIndex        =   17
         ToolTipText     =   "Esfuerzo presupuestado"
         Top             =   3540
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtFechaComite 
         Height          =   315
         Left            =   4920
         TabIndex        =   6
         ToolTipText     =   "Fecha en que se remiti� al Business Partner"
         Top             =   3090
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaPedido 
         Height          =   315
         Left            =   4920
         TabIndex        =   4
         ToolTipText     =   "Fecha en que se inici� la Peticion "
         Top             =   2640
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Locked          =   -1  'True
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNroAsignado 
         Height          =   315
         Left            =   7380
         TabIndex        =   73
         ToolTipText     =   "N�mero asignado por el administrador"
         Top             =   360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtNroAnexada 
         Height          =   315
         Left            =   1155
         TabIndex        =   3
         ToolTipText     =   "Petici�n a la cual est� anexada"
         Top             =   4440
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         MaxLength       =   30
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         AutoSelect      =   0   'False
      End
      Begin AT_MaskText.MaskText txtFechaRequerida 
         Height          =   315
         Left            =   1155
         TabIndex        =   2
         Top             =   780
         Width           =   1530
         _ExtentX        =   2699
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUsualta 
         Height          =   315
         Left            =   6990
         TabIndex        =   74
         Top             =   2640
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         AutoSelect      =   0   'False
      End
      Begin AT_MaskText.MaskText txtusrImportancia 
         Height          =   315
         Left            =   1155
         TabIndex        =   80
         Top             =   2640
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin VB.Label lblPetEquipo 
         AutoSize        =   -1  'True
         Caption         =   "Equipamiento tecnol�gico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   90
         TabIndex        =   146
         Top             =   1650
         Width           =   1200
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblPetClass 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   143
         Top             =   1770
         Width           =   450
      End
      Begin VB.Label Label41 
         AutoSize        =   -1  'True
         Caption         =   "B. Partner"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   142
         Top             =   7245
         Width           =   840
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         Caption         =   "Orientaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   84
         Top             =   3585
         Width           =   975
      End
      Begin VB.Label Label33 
         AutoSize        =   -1  'True
         Caption         =   "Corp/Local"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   83
         Top             =   3135
         Width           =   915
      End
      Begin VB.Label Label32 
         AutoSize        =   -1  'True
         Caption         =   "Asignada x"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   82
         Top             =   2685
         Width           =   930
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   81
         Top             =   2235
         Width           =   840
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "Fin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6480
         TabIndex        =   77
         Top             =   4080
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "User"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6480
         TabIndex        =   75
         Top             =   2685
         Width           =   390
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "F. Requerida"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   72
         Top             =   855
         Width           =   1050
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Anexada a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   66
         Top             =   4500
         Width           =   945
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   65
         Top             =   4050
         Width           =   765
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "F.Alta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4380
         TabIndex        =   64
         Top             =   2685
         Width           =   480
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "F.Env. B.Part."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   63
         Top             =   3135
         Width           =   1080
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "Nro"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6720
         TabIndex        =   62
         Top             =   420
         Width           =   285
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   61
         Top             =   1305
         Width           =   360
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "F.Planif.Inicio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   58
         Top             =   4080
         Width           =   1110
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "Fin"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6480
         TabIndex        =   57
         Top             =   4500
         Width           =   240
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "F. Real  Inicio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   56
         Top             =   4500
         Width           =   1110
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Hs. Presup."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   55
         Top             =   3585
         Width           =   930
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   48
         Top             =   4950
         Width           =   555
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Solicitante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   47
         Top             =   5400
         Width           =   900
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Supervisor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   46
         Top             =   6315
         Width           =   915
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Referente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   45
         Top             =   5850
         Width           =   855
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Autorizante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   44
         Top             =   6765
         Width           =   1005
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   43
         Top             =   1305
         Width           =   780
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "F.Estado Act."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   30
         Top             =   2235
         Width           =   1080
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   29
         Top             =   405
         Width           =   480
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3780
         TabIndex        =   28
         Top             =   855
         Width           =   570
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   1
      Left            =   0
      TabIndex        =   26
      Top             =   360
      Width           =   10305
      Begin VB.TextBox memDescripcion 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1150
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   20
         Top             =   180
         Width           =   7000
      End
      Begin VB.TextBox memCaracteristicas 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1150
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   21
         Top             =   1380
         Width           =   7000
      End
      Begin VB.TextBox memRelaciones 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1150
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   24
         Top             =   4980
         Width           =   7000
      End
      Begin VB.TextBox memObservaciones 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1150
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   25
         Top             =   6180
         Width           =   7000
      End
      Begin VB.TextBox memMotivos 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1150
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   22
         Top             =   2580
         Width           =   7000
      End
      Begin VB.TextBox memBeneficios 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1150
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   23
         Top             =   3780
         Width           =   7000
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Descripci�n del Pedido"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   200
         TabIndex        =   54
         Top             =   180
         Width           =   2000
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Caracter�sticas Principales"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   200
         TabIndex        =   53
         Top             =   1380
         Width           =   2000
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         Caption         =   "Motivos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   200
         TabIndex        =   52
         Top             =   2580
         Width           =   2000
      End
      Begin VB.Label Label21 
         Alignment       =   1  'Right Justify
         Caption         =   "Beneficios Esperados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   200
         TabIndex        =   51
         Top             =   3780
         Width           =   2000
      End
      Begin VB.Label Label22 
         Alignment       =   1  'Right Justify
         Caption         =   "Relaci�n con otros requerimientos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   200
         TabIndex        =   50
         Top             =   4980
         Width           =   2000
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   195
         TabIndex        =   49
         Top             =   6180
         Width           =   1995
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   3
      Left            =   0
      TabIndex        =   88
      Top             =   360
      Width           =   10305
      Begin VB.Frame fraDocMet 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   60
         TabIndex        =   96
         Top             =   3330
         Width           =   8805
         Begin VB.TextBox docFile 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   107
            Top             =   315
            Width           =   6735
         End
         Begin VB.TextBox docPath 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   106
            Top             =   679
            Width           =   7095
         End
         Begin VB.CommandButton docSel 
            Caption         =   "..."
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   8115
            TabIndex        =   101
            TabStop         =   0   'False
            Top             =   315
            Width           =   330
         End
         Begin VB.TextBox docComent 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1200
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   100
            Top             =   1050
            Width           =   7095
         End
         Begin VB.Label Label42 
            AutoSize        =   -1  'True
            Caption         =   "Archivo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   152
            Top             =   360
            Width           =   645
         End
         Begin VB.Label lblDocMetod 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   99
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label Label39 
            AutoSize        =   -1  'True
            Caption         =   "Path"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   98
            Top             =   690
            Width           =   390
         End
         Begin VB.Label Label38 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   97
            Top             =   1050
            Width           =   1080
         End
      End
      Begin VB.CommandButton docCan 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   95
         TabStop         =   0   'False
         Top             =   5880
         Width           =   1170
      End
      Begin VB.CommandButton docCon 
         Caption         =   "Confirmar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   94
         TabStop         =   0   'False
         Top             =   5370
         Width           =   1170
      End
      Begin VB.CommandButton docMod 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   93
         TabStop         =   0   'False
         Top             =   4350
         Width           =   1170
      End
      Begin VB.CommandButton docDel 
         Caption         =   "Desvincular"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   92
         TabStop         =   0   'False
         Top             =   4860
         Width           =   1170
      End
      Begin VB.CommandButton docAdd 
         Caption         =   "Vincular"
         Height          =   500
         Left            =   8970
         TabIndex        =   91
         TabStop         =   0   'False
         Top             =   3840
         Width           =   1170
      End
      Begin VB.CommandButton docView 
         Caption         =   "Ver docum."
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   89
         TabStop         =   0   'False
         Top             =   1260
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdDocMetod 
         Height          =   3030
         Left            =   90
         TabIndex        =   90
         Top             =   300
         Width           =   8805
         _ExtentX        =   15531
         _ExtentY        =   5345
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   4
      Left            =   0
      TabIndex        =   110
      Top             =   360
      Width           =   10305
      Begin VB.Frame fraAdjPet 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   60
         TabIndex        =   111
         Top             =   3330
         Width           =   8775
         Begin VB.TextBox adjPath 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   136
            Top             =   2280
            Visible         =   0   'False
            Width           =   6975
         End
         Begin VB.ComboBox cboAdjClass 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmNewPeticionesCarga.frx":0554
            Left            =   1680
            List            =   "frmNewPeticionesCarga.frx":0556
            Style           =   2  'Dropdown List
            TabIndex        =   113
            Top             =   660
            Width           =   6975
         End
         Begin VB.TextBox adjFile 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   112
            Top             =   315
            Width           =   6550
         End
         Begin VB.CommandButton adjSel 
            Caption         =   "..."
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   8280
            TabIndex        =   115
            TabStop         =   0   'False
            Top             =   315
            Width           =   330
         End
         Begin VB.TextBox adjComent 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1200
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   114
            Top             =   1050
            Width           =   6975
         End
         Begin VB.Label Label43 
            AutoSize        =   -1  'True
            Caption         =   "Archivo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   153
            Top             =   360
            Width           =   645
         End
         Begin VB.Label lblAdjPet 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   116
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label Label37 
            AutoSize        =   -1  'True
            Caption         =   "Tipo documento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   117
            Top             =   690
            Width           =   1365
         End
         Begin VB.Label Label35 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   118
            Top             =   1050
            Width           =   1080
         End
      End
      Begin VB.CommandButton adjCan 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   119
         TabStop         =   0   'False
         Top             =   5880
         Width           =   1170
      End
      Begin VB.CommandButton adjCon 
         Caption         =   "Confirmar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   120
         TabStop         =   0   'False
         Top             =   5370
         Width           =   1170
      End
      Begin VB.CommandButton adjMod 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   121
         TabStop         =   0   'False
         Top             =   4350
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton adjDel 
         Caption         =   "Desvincular"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   122
         TabStop         =   0   'False
         Top             =   4860
         Width           =   1170
      End
      Begin VB.CommandButton adjAdd 
         Caption         =   "Adjuntar"
         Height          =   500
         Left            =   8970
         TabIndex        =   123
         TabStop         =   0   'False
         Top             =   3840
         Width           =   1170
      End
      Begin VB.CommandButton adjView 
         Caption         =   "Ver adjunto"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   124
         TabStop         =   0   'False
         Top             =   1260
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdAdjPet 
         Height          =   3030
         Left            =   90
         TabIndex        =   125
         Top             =   300
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   5345
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   2
      Left            =   0
      TabIndex        =   68
      Top             =   360
      Width           =   10305
      Begin VB.CommandButton cmdVerAnexa 
         Caption         =   "Ver Peticion"
         Height          =   500
         Left            =   8970
         TabIndex        =   70
         TabStop         =   0   'False
         Top             =   1230
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdAnexas 
         Height          =   4920
         Left            =   150
         TabIndex        =   69
         Top             =   480
         Width           =   8715
         _ExtentX        =   15372
         _ExtentY        =   8678
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   5
      Left            =   0
      TabIndex        =   128
      Top             =   360
      Width           =   10305
      Begin VB.CommandButton pcfMod 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   139
         TabStop         =   0   'False
         Top             =   4350
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.Frame fraConforme 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   90
         TabIndex        =   129
         Top             =   3330
         Width           =   8775
         Begin VB.ComboBox cboPcfTipo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmNewPeticionesCarga.frx":0558
            Left            =   1800
            List            =   "frmNewPeticionesCarga.frx":055A
            Style           =   2  'Dropdown List
            TabIndex        =   151
            Top             =   300
            Width           =   3885
         End
         Begin VB.TextBox pcfTexto 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1200
            Left            =   1800
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   150
            Top             =   1080
            Width           =   5385
         End
         Begin VB.ComboBox cboPcfModo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmNewPeticionesCarga.frx":055C
            Left            =   1800
            List            =   "frmNewPeticionesCarga.frx":055E
            Style           =   2  'Dropdown List
            TabIndex        =   149
            Top             =   690
            Width           =   3885
         End
         Begin VB.Label Label40 
            AutoSize        =   -1  'True
            Caption         =   "Req."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   140
            Top             =   720
            Width           =   375
         End
         Begin VB.Label Label36 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   138
            Top             =   1080
            Width           =   1080
         End
         Begin VB.Label Label28 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de conforme"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   137
            Top             =   330
            Width           =   1470
         End
         Begin VB.Label lblPetConf 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   130
            Top             =   30
            Visible         =   0   'False
            Width           =   105
         End
      End
      Begin VB.CommandButton pcfCan 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   131
         TabStop         =   0   'False
         Top             =   5880
         Width           =   1170
      End
      Begin VB.CommandButton pcfCon 
         Caption         =   "Confirmar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   132
         TabStop         =   0   'False
         Top             =   5370
         Width           =   1170
      End
      Begin VB.CommandButton pcfDel 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   8970
         TabIndex        =   133
         TabStop         =   0   'False
         Top             =   4860
         Width           =   1170
      End
      Begin VB.CommandButton pcfAdd 
         Caption         =   "Agregar"
         Height          =   500
         Left            =   8970
         TabIndex        =   134
         TabStop         =   0   'False
         Top             =   3840
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdPetConf 
         Height          =   3030
         Left            =   90
         TabIndex        =   135
         Top             =   300
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   5345
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
End
Attribute VB_Name = "frmPeticionesCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 25.06.2007 - Se agregan dos atributos (Clase de petici�n e indicador de impacto tecnol�gico del
'                             mismo). Los controles son: cmbPetClass y cmbPetImpact.
' -001- b. - FJS 26.06.2007 - Se inicializan en el evento Form_Load y se agregan las cargas y controles seg�n el perfil del usuario.
' -001- c. - FJS 26.06.2007 - Se carga el control con el dato correspondiente al registro solicitado
' -001- d. - FJS 27.06.2007 - Se agrega el control de valores v�lidos para el control.
' -001- e. - FJS 27.06.2007 - Se agregan los par�metros necesarios para contemplar los nuevos campos (Clase de petici�n e indicador si corresponde impacto tecnol�gico)
' -001- f. - FJS 27.06.2007 - Se elimina la posibilidad de realizar altas de peticiones de tipo "Proyecto". En caso de que a una petici�n Normal, Especial o Propia le
'                             corresponda dicho estado, solo el BP asignado a la petici�n podr� transformarla en Proyecto.
'                             Los resp. de Sector y/o Grupo de cualquiera de las gerencias de medios podran realizar altas de peticiones "Especiales". No obstante ello,
'                             las altas de peticiones "Especiales" que no sean realizadas por usuarios de organizaci�n solo podr�n ser de clase "Atenci�n al usuario".
'                             Solo podr�n realizar altas de peticiones Normales los solicitantes de �reas usuarias (no Medios) y los pertenecientes a la gerencia de Organizaci�n.
'                             Es decir que no podr�n hacer altas de peticiones Normales los usuarios de las dem�s gerencias de medios, por m�s que tengan asignado un perfil Solic.
' -001- g. - FJS 05.07.2007 - Adem�s, se habilitar� la posibilidad de que el BP modifique una petici�n de tipo Normal a Especial. De este modo el BP podr�, por ejemplo, transformar
'                             peticiones dadas de alta por Organizaci�n como Normales (con sector solicitante de Organizaci�n) en Especiales (con la verdadera �rea usuaria como
'                             sector solicitante).
' -001- h. - FJS 10.07.2007 - Se actualiza el manejo y control de archivos incrustados (adjuntos) y vinculados a una petici�n.
'                             Importante: se modifica la funci�n esBP para la validaci�n entre cadenas, agregandole la funci�n TRIM porque en ciertos casos esta carencia hace que
'                             la expresi�n se evalue incorrectamente, devolviendo False cuando es True.
'                             Se comenta el m�todo SetFocus del campo de comentarios, para focalizar el tipo de clasificaci�n del archivo que se adjunta.
' -001- i. - FJS 17.07.2007 - Se adaptan las validaciones de los conformes de la petici�n al esquema de los nuevos controles SOX para documentos adjuntos y conformes de usuario.
' -001- j. - FJS 18.07.2007 - Se inicializa el valor que tomar� por defecto el campo de la tabla Peticion para los nuevos controles SOX para documentos adjuntos. Seg�n la fecha de
'                             corte definida se guardar� con valor 1 (activado) o 0 (desactivado).
' -001- k. - FJS 18.07.2007 - Se agrega una nueva clase para una petici�n denominada SPUFI que tendr� el mismo tratamiento que la clase Mantenimiento correctivo (es para peticiones
'                             propias cursadas por DyD.
' -001- l. - FJS 18.07.2007 - Peticiones Especiales: la petici�n, en el alta queda en estado "Al BP". El sector/grupo queda en estado "A confirmar". Cuando el BP pase la petici�n a
'                             "Aprobada", el sector/grupo que estaba "A confirmar" quedar� "A planificar".
' -001- m. - FJS 19.07.2007 - Se guarda en el log del sistema el cambio en el att. impacto tecnol�gico y la clase de la petici�n (Ver documento anexo).
' -001- n. - FJS 20.07.2007 - Las peticiones propias de DyD no pueden ser de la clase Atenci�n a usuario. Deber�n ser de tipo Especial a partir de ahora (Anexo I, punto 8).
' -001- o. - FJS 24.07.2007 - Los OKs pueden ser dados solamente por: Solicitante o Referente de la petici�n, salvo para las peticiones Propias (es as� actualmente).
'                             Si el sector solicitante es de la direcci�n de Medios y la petici�n es propia, tambi�n puede dar el OK el responsable del Sector que tiene injerencia
'                             en el sector solicitante de la petici�n (esta consideraci�n es nueva, ya que actualmente no se requieren OKs para las peticiones propias, pero en el
'                             modelo de gesti�n es necesario que exista un Ok del supervisor en este tipo de peticiones.
' -002- a. - FJS 26.07.2007 - Por definici�n, y fuera del alcance del proyecto actual, se establece cambiar la restricci�n al tipo de documento que puede estipular un BP tanto en el
'                             alta como en la modificaci�n. Incluido en el documento Anexo.
' -002- b. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por
'                             parte del usuario).
' -002- c. - FJS 30.07.2007 - Se condiciona la carga del control ComboBox para la clase de la petici�n (cmbPetClass) seg�n los valores del ComboBox de tipo de la petici�n (cboTipoPet).
' -002- d. - FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -002- e. - FJS 03.08.2007 - Se agrega una confirmaci�n antes de ejecutar la asignaci�n de documentos y/o conformes a la base ya que no es posible volver atr�s las situaciones debido
'                             a tema de seguridad y perfiles.
' -002- f. - FJS 03.08.2007 - Se corrige un defecto inesperado: perdi� el control de la fecha requerida de la petici�n.
' -002- g. - FJS 03.08.2007 - Se valida que al momento de adjuntar el archivo adjunto, en el nombre exista el c�digo del documento (Ej.: C100)
'
' mov -001-
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Option Explicit

Const colNroAsignado = 0
Const colTitulo = 1
Const colNroInterno = 2

Dim bFlgObservaciones As Boolean, bFlgBeneficios As Boolean, bFlgCaracteristicas As Boolean
Dim bFlgDescripcion As Boolean, bFlgMotivos As Boolean, bFlgRelaciones As Boolean
Dim bFlgExtension As Boolean
Dim sBpar As String, sSoli As String, sRefe As String, sSupe As String, sDire As String
Dim xSec As String, xGer As String, xDir As String
Dim antBpar As String
Dim sEstado As String
Dim sSituacion As String
Dim sUsualta As String
Dim anxNroPeticion
Dim defTipoPet As String
Dim xPerfNivel As String, xPerfArea As String
Dim xPerfDir As String, xPerfGer As String, xPerfSec As String, xPerfGru As String
Dim flgIngerencia As Boolean
Dim flgEjecutor As Boolean
Dim flgBP As Boolean
Dim flgSolicitor As Boolean
Dim flgEnCarga As Boolean
Dim xUsrPerfilActual As String, xUsrLoginActual As String
Dim PerfilInternoActual As String
Dim txObservaciones As String
Dim txBeneficios As String
Dim txCaracteristicas As String
Dim txDescripcion As String
Dim txMotivos As String
Dim txRelaciones As String

Dim glPathDocMetod As String
Dim docOpcion As String
Dim docIndex As Integer
Dim glPathAdjPet As String
Dim adjOpcion As String
Dim adjIndex As Integer
Dim pcfOpcion As String
''Dim pcfIndex As Integer

Dim antPathAdjPet As String
Dim antPathDocMetod As String
Dim sPcfSecuencia As String
Dim prjNroInterno As String

Dim bControlesSOX As Byte       ' add -001- j.

Const orjDAT = 0
Const orjMEM = 1
Const orjANX = 2
Const orjDOC = 3
Const orjADJ = 4
Const orjCNF = 5

Private Sub Form_Load()
    fjsNewStatusMessage Trim(Me.Name), 7    ' add -002- b.
    flgEnCarga = True
   'Me.AutoRedraw = True
    Dim i As Integer
    cboPrioridad.Clear
    cboPrioridad.AddItem "1 : Regulatorio"
    cboPrioridad.AddItem "2 : Muy alta"
    cboPrioridad.AddItem "3 : Alta"
    cboPrioridad.AddItem "4 : Media"
    cboPrioridad.AddItem "5 : Baja"
    cboPrioridad.AddItem "6 : Muy baja"
    
    cboCorpLocal.Clear
    cboCorpLocal.AddItem "Local                                      ||L"
    cboCorpLocal.AddItem "Corporativa                                ||C"
    cboCorpLocal.ListIndex = 0
    
    '{ del -001- h.
    'cboAdjClass.AddItem "Requerimiento                                ||REQU"
    'cboAdjClass.AddItem "Doc. de alcance desarrollo                   ||ALCA"
    'cboAdjClass.AddItem "Evaluaci�n costo - beneficio                 ||COST"
    'cboAdjClass.AddItem "Plan de ejecuci�n                            ||PLEJ"
    'cboAdjClass.AddItem "Plan de implantaci�n Desarrollo              ||PLIM"
    'cboAdjClass.AddItem "Mail                                         ||MAIL"
    'cboAdjClass.AddItem "Otros                                        ||OTRO"
    '}
    '{ add -001- h.
    Select Case glModoPeticion
        Case "ALTA"
            With cboAdjClass
                .Clear
                .AddItem "C102 : Dise�o de soluci�n"
                .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
                .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
                .AddItem "C204 : Casos de prueba"
                .AddItem "CG04 : Documento de cambio de alcance"
                .AddItem "T700 : Plan de implantaci�n Desarrollo"       ' Este es el T700
                .ListIndex = -1
            End With
        Case Else
            If glPeticion_sox001 = 1 Then
                With cboAdjClass
                    .Clear
                    .AddItem "C102 : Dise�o de soluci�n"
                    .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
                    .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
                    .AddItem "C204 : Casos de prueba"
                    .AddItem "CG04 : Documento de cambio de alcance"
                    .AddItem "T700 : Plan de implantaci�n Desarrollo"       ' Este es el T700
                    .ListIndex = -1
                End With
            Else
                With cboAdjClass
                    .Clear
                    .AddItem "REQU : Requerimiento"
                    .AddItem "ALCA : Doc. de alcance desarrollo"
                    .AddItem "COST : Evaluaci�n costo - beneficio"
                    .AddItem "PLEJ : Plan de ejecuci�n"
                    .AddItem "PLIM : Plan de implantaci�n Desarrollo"
                    .AddItem "MAIL : Mail"
                    .AddItem "OTRO : Otros"
                    .ListIndex = -1
                End With
            End If
    End Select
    '}
        
    ' Estos son los controles para los conformes
    cboPcfTipo.Clear
    cboPcfTipo.AddItem "Ok Parcial Documento Alcance           ||ALCP"
    cboPcfTipo.AddItem "Ok Documento Alcance                   ||ALCA"
    cboPcfTipo.AddItem "Ok Parcial Prueba Usuario              ||TESP"
    cboPcfTipo.AddItem "Ok Prueba Usuario                      ||TEST"

    cboPcfModo.Clear
    cboPcfModo.AddItem "                                   ||REQ"
    cboPcfModo.AddItem "No Requerido                       ||EXC"

    defTipoPet = "PRO"
    cboTipopet.Clear
    
    'guardo este perfil por si lo cambio
    PerfilInternoActual = glUsrPerfilActual
    
    Call InitTipoPet
    
    '{ add -001- a. - Se inicializa el control cmbPetClass (indicador de la clase de la petici�n) con todos los valores definidos. Tambi�n se prefija la opci�n en "No" para el indicador de Impacto tecnol�gico.
    With cmbPetClass
        .Clear
        Select Case glModoPeticion
            Case "ALTA"
                Select Case PerfilInternoActual
                    Case "CGRU", "CSEC", "BPAR"
                        .AddItem "S/C (Sin clase)                                               ||SINC"
                        .AddItem "Correctivo                                                ||CORR"
                        .AddItem "Atenci�n a usuario                                                ||ATEN"
                        .AddItem "Optimizaci�n                                              ||OPTI"
                        .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                        .AddItem "Nuevo desarrollo                                              ||NUEV"
                        .AddItem "SPUFI                                              ||SPUF"    ' add -001- k.
                        .ListIndex = 0
                    Case Else
                        .AddItem "S/C (Sin clase)                                               ||SINC"
                        .ListIndex = 0
                End Select
            Case Else
                .AddItem "S/C (Sin clase)                                               ||SINC"
                .AddItem "Correctivo                                                ||CORR"
                .AddItem "Atenci�n a usuario                                                ||ATEN"
                .AddItem "Optimizaci�n                                              ||OPTI"
                .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                .AddItem "Nuevo desarrollo                                              ||NUEV"
                .AddItem "SPUFI                                              ||SPUF"    ' add -001- k.
                .ListIndex = 0
        End Select
    End With
    
    With cmbPetImpact
        .ListIndex = 0      ' Por defecto las peticiones no implican impacto tecnol�gico
    End With
    '}
    For i = 0 To orjBtn.Count - 1
        orjFiller(i).Width = orjBtn(i).Width - 48
        orjFiller(i).Left = orjBtn(i).Left + 16
        orjFiller(i).Top = orjBtn(i).Top + orjBtn(i).Height - 32
    Next
    
    'averiguar todo lo que se puede
    'acerca de su perfil
     xPerfNivel = getPerfNivel(PerfilInternoActual)
     xPerfArea = getPerfArea(PerfilInternoActual)

     xPerfDir = ""
     xPerfGer = ""
     xPerfSec = ""
     xPerfGru = ""
     Select Case xPerfNivel
         Case "DIRE"
             xPerfDir = xPerfArea
         Case "GERE"
             If sp_GetGerenciaXt(xPerfArea, Null) Then
                 If Not aplRST.EOF Then
                     xPerfDir = ClearNull(aplRST!cod_direccion)
                     xPerfGer = xPerfArea
                 End If
                 aplRST.Close
             End If
         Case "SECT"
             If sp_GetSectorXt(xPerfArea, Null, Null) Then
                 If Not aplRST.EOF Then
                     xPerfDir = ClearNull(aplRST!cod_direccion)
                     xPerfGer = ClearNull(aplRST!cod_gerencia)
                     xPerfSec = xPerfArea
                 End If
                 aplRST.Close
             End If
         Case "GRUP"
             If sp_GetGrupoXt(xPerfArea, Null) Then
                 If Not aplRST.EOF Then
                     xPerfDir = ClearNull(aplRST!cod_direccion)
                     xPerfGer = ClearNull(aplRST!cod_gerencia)
                     xPerfSec = ClearNull(aplRST!cod_sector)
                     xPerfGru = xPerfArea
                 End If
                 aplRST.Close
             End If
     End Select
   
    bFlgExtension = False
    
    Call InicializarPantalla(True)
    Call FormCenterModal(Me, mdiPrincipal)
End Sub

Private Sub cboImportancia_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    xUsrLoginActual = glLOGIN_ID_REEMPLAZO
    xUsrPerfilActual = PerfilInternoActual
End Sub

Private Sub cboSector_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    If PerfilInternoActual = "SOLI" Then Exit Sub
    If CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ" Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call puntero(True)
    Call CargaIntervinientes
    Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))
    Call puntero(False)
    Call LockProceso(False)
End Sub

Private Sub cmdSector_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    frmPeticionesSector.Show 1
    DoEvents
    Call LockProceso(False)
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla(False)
    End If
End Sub

Private Sub cmdGrupos_Click()
If flgEnCarga = True Then Exit Sub
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glSector = ""
        frmPeticionesGrupo.Show vbModal
        DoEvents
        Call LockProceso(False)
        If Me.Tag = "REFRESH" Then
            InicializarPantalla
        End If

End Sub

Private Sub cboSolicitante_Click()
''''    If flgEnCarga = True Then
''''        Exit Sub
''''    End If
''''    If Not LockProceso(True) Then
''''        Exit Sub
''''    End If
''''    Dim sDir, sGer, sSec As String
''''    If sp_GetRecurso(CodigoCombo(cboSolicitante), "R") Then
''''        sDir = ClearNull(aplRST!cod_direccion)
''''        sSec = ClearNull(aplRST!cod_sector)
''''        sGer = ClearNull(aplRST!cod_gerencia)
''''        aplRST.Close
''''        Call SetCombo(cboSector, sSec, True)
''''    End If
''''    Call LockProceso(False)
End Sub

Private Sub cboTipopet_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    If Not LockProceso(True) Then
        Exit Sub
    End If
    
    If PerfilInternoActual = "SADM" Then
        Call LockProceso(False)
        Exit Sub
    End If
    
    If CodigoCombo(cboTipopet, True) = "PRJ" Then
        If cboImportancia.ListIndex = -1 Then cboImportancia.ListIndex = cboImportancia.ListCount - 1
        If cboCorpLocal.ListIndex = -1 Then cboCorpLocal.ListIndex = 0
    End If
    
    '{ add -002- c.
    If glModoPeticion = "MODI" Then
        Call HabilitarBotones
        Call LockProceso(False)
        Exit Sub
    End If
    '}
    
    '{ del -002- c.
    'If glModoPeticion = "MODI" Then
    '    If CodigoCombo(cboTipopet, True) = "PRJ" Then
    '        'estamos en el caso en que se cambio de NOR o PRO a PRJ
    '        Call HabilitarBotones
    '        Call LockProceso(False)
    '        Exit Sub
    '    ElseIf CodigoCombo(cboTipopet, True) = "ESP" Then
    '        'estamos en el caso en que se cambio de PRO a ESP
    '        Call HabilitarBotones
    '        Call LockProceso(False)
    '        Exit Sub
    '    Else
    '        Call setHabilCtrl(cboCorpLocal, "DIS")
    '        Call setHabilCtrl(cboOrientacion, "DIS")
    '        Call setHabilCtrl(cboImportancia, "DIS")
    '        Call LockProceso(False)
    '        Exit Sub
    '    End If
    'End If
    '}
    
    Call puntero(True)
    
    Dim sDir, sGer, sSec As String
'''    If CodigoCombo(cboTipopet, True) = "ESP" Then
'''        sEstado = "CONFEC"
'''        Call setHabilCtrl(cboPrioridad, "DIS")
'''        cboPrioridad.ListIndex = -1
'''        Call setHabilCtrl(cboSolicitante, "NOR")
'''        Call InicializarCombos
'''    End If
    If CodigoCombo(cboTipopet, True) = "ESP" Then
        sEstado = "APROBA"
        txtFechaComite.Text = Format(Date, "dd/mm/yyyy")
        Call setHabilCtrl(cboPrioridad, "NOR")
        Call setHabilCtrl(cboSolicitante, "NOR")
        Call setHabilCtrl(cboBpar, "DIS")
        Call InicializarCombos
    End If
    If CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ" Then
        sEstado = "APROBA"
        txtFechaComite.Text = Format(Date, "dd/mm/yyyy")
        Call setHabilCtrl(cboPrioridad, "NOR")
        Call setHabilCtrl(cboSolicitante, "DIS")
        Call setHabilCtrl(cboBpar, "NOR")
        Call InicializarCombos
        cboBpar.ListIndex = -1
    End If
    If CodigoCombo(cboTipopet, True) = "PRJ" Then
        Call setHabilCtrl(cboCorpLocal, "NOR")
        Call setHabilCtrl(cboOrientacion, "NOR")
        cboPrioridad.ListIndex = 1
        If glModoPeticion = "ALTA" Then
            cboImportancia.ListIndex = cboImportancia.ListCount - 1
        End If
        If glModoPeticion = "ALTA" Then
            If PerfilInternoActual = "CGRU" Then
                Call setHabilCtrl(cboImportancia, "DIS")
            Else
                Call setHabilCtrl(cboImportancia, "NOR")
            End If
        End If
    Else
        Call setHabilCtrl(cboCorpLocal, "DIS")
        Call setHabilCtrl(cboOrientacion, "DIS")
        Call setHabilCtrl(cboImportancia, "DIS")
        cboImportancia.ListIndex = -1
        cboCorpLocal.ListIndex = 0
    End If
    Call LockProceso(False)
    defTipoPet = CodigoCombo(cboTipopet, True)
    DoEvents
    Call puntero(False)

End Sub

Private Sub cmdAgrupar_Click()
    glSelectAgrup = ""
    frmPeticionAgrup.Show vbModal
    DoEvents
End Sub

Private Sub cmdAsigNro_Click()
    'en una epoca el numero podia asignarse manualmente
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        frmAsignaNumero.Show 1
        DoEvents
        If Me.Tag = "REFRESH" Then
            cmdAsigNro.Enabled = False
            Call InicializarPantalla(False)
        End If
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdCambioEstado_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        frmPeticionesCambioEstado.Show 1
        DoEvents
        If Me.Tag = "REFRESH" Then
            Call InicializarPantalla(False)
        End If
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If MsgBox("Confirma la eliminaci�n", vbYesNo) = vbNo Then
        Call LockProceso(False)
        Exit Sub
    End If
    If Not sp_DeletePeticion(txtNrointerno.Text) Then
        Call LockProceso(False)
        Exit Sub
    End If
    Me.Hide
    touchForms
    Call LockProceso(False)
    Unload Me
End Sub

Private Sub cmdPrint_Click()
    Dim nCopias As Integer

    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion = "" Then
        MsgBox ("No selecciono ninguna Petici�n")
        Call LockProceso(False)
        Exit Sub
    End If
    
   rptFormulario.Show vbModal
    nCopias = rptFormulario.Copias
    Call puntero(True)
    DoEvents
    While nCopias > 0
        If rptFormulario.chkFormu Then
            Call PrintFormulario(glNumeroPeticion)
        End If
        If rptFormulario.chkHisto Then
            Call PrintHitorial(glNumeroPeticion)
        End If
        nCopias = nCopias - 1
    Wend
    Call puntero(False)
    
    Call LockProceso(False)
End Sub

Private Sub cmdVerAnexora_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If Val(getStrCodigo(txtNroAnexada.Text, True)) = 0 Then
        MsgBox ("Esta Petici�n no se encuentra anexada")
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    glNumeroPeticion = getStrCodigo(txtNroAnexada.Text, True)
    touchForms
    Call InicializarPantalla(True)
End Sub

Private Sub cmdVerAnexa_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If anxNroPeticion = "" Then
        MsgBox ("No selecciono ninguna Petici�n")
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    glNumeroPeticion = anxNroPeticion
    touchForms
    Call InicializarPantalla(True)
End Sub

Private Sub Form_Resize()
'    Me.WindowState = vbMaximized
End Sub

Private Sub Label37_Click()

End Sub

Private Sub orjBtn_Click(Index As Integer)
    Dim i As Integer
    For i = 0 To orjBtn.Count - 1
        orjFiller(i).Visible = False
        orjBtn(i).Font.Bold = False
        orjBtn(i).BackColor = RGB(208, 208, 208)
        orjFrame(i).Visible = False
    Next
    orjFrame(Index).Visible = True
    orjBtn(Index).Font.Bold = True
    orjBtn(Index).BackColor = orjFrame(Index).BackColor
    orjFiller(Index).Visible = True
    If Index = orjDOC Then
        Call CargarDocMetod
    End If
    If Index = orjADJ Then
        Call CargarAdjPet
    End If
    If Index = orjCNF Then
        Call CargarPetConf
    End If
End Sub

Private Sub cmdHistView_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        frmHistorial.Show 1
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glModoPeticion = "MODI"
    Call HabilitarBotones
    '{ add -002- c.
    'cboTipopet.SetFocus
    'cmbPetClass.SetFocus
    '}
End Sub

Private Sub cmdCancel_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glModoPeticion = "MODI" Then
        glModoPeticion = "VIEW"
        'CUIDADO
        Call InitTipoPet
        Call CargaPeticion(True)
        Call HabilitarBotones
    Else
        Unload Me
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdOK_Click()
    Dim sDir, sGer, sSec As String
    Dim xBpar, sSoli, sRefe, sSupe, sAuto As String
    Dim auxNro As String
    Dim xEstado As String
    Dim nNumeroAsignado As Integer
    Dim iUsrPerfilActual As String, iUsrLoginActual As String
    Dim bMsg As Boolean
    Dim sTXTalcance As String
    Dim sTipoPet As String
    '{ add -001- m. Variables para guardar la leyenda de los cambios realizados para el historial
    Dim sCambioDeClase As String
    Dim sCambioImpacto As String
    '}
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    
    bMsg = False
    
    If CamposObligatorios Then
        '{ add -001- j. Determina si por la fecha de alta de la petici�n se controla con la nueva metodolog�a o no.
        If SOX_Documentos(txtFechaPedido.DateValue) Then
            bControlesSOX = 1       ' Nuevos controles
        Else
            bControlesSOX = 0       ' Controles anteriores a SOX
        End If
        '}
        If Not valSectorHabil(CodigoCombo(cboSector, True)) Then
            MsgBox ("El sector solicitante est� marcado como inhabilitado"), vbOKOnly + vbInformation
            Call LockProceso(False)
            Exit Sub
        End If
        touchForms
        sSec = CodigoCombo(Me.cboSector, True)
        If sp_GetSectorXt(sSec, Null, Null) Then
            sDir = ClearNull(aplRST!cod_direccion)
            sGer = ClearNull(aplRST!cod_gerencia)
            aplRST.Close
        End If
        
        iUsrPerfilActual = xUsrPerfilActual
        iUsrLoginActual = xUsrLoginActual
        If iUsrPerfilActual = "SADM" Then
            iUsrPerfilActual = "ADMI"
        End If
        If ClearNull(CodigoCombo(cboImportancia, True)) = "" Then
            iUsrPerfilActual = ""
            iUsrLoginActual = ""
        End If
        
        sSoli = CodigoCombo(Me.cboSolicitante)
        sRefe = CodigoCombo(Me.cboReferente)
        sSupe = CodigoCombo(Me.cboSupervisor)
        sAuto = CodigoCombo(Me.cboDirector)
        sTipoPet = CodigoCombo(Me.cboTipopet, True)
        
        If glModoPeticion = "ALTA" Then
            If sTipoPet = "NOR" Or sTipoPet = "ESP" Then
                xBpar = getBpSector(CodigoCombo(cboSector, True))
            Else
                xBpar = CodigoCombo(cboBpar)
            End If
        Else
            xBpar = CodigoCombo(cboBpar)
        End If
        
        'ESTO ES POR LA SMODIFICACIONES DE PET QUE VINIERON SIN bp
        'If xBpar = "" Then
        '    xBpar = getBpSector(CodigoCombo(cboSector, True))
        'End If
        
        If xBpar = "" Then
            MsgBox ("No se ha especificado Business Partner"), vbOKOnly + vbInformation
            Call LockProceso(False)
            Exit Sub
        End If
        
        If glModoPeticion = "ALTA" Then
            If sTipoPet = "ESP" Then
                sRefe = sSoli
                sSupe = Null
                sAuto = sSoli
            End If
            'en el alta el nro proyecto es cero
            If sp_UpdatePeticion(Null, txtNroAsignado.Text, txtTitulo.Text, sTipoPet, CodigoCombo(Me.cboPrioridad), CodigoCombo(cboCorpLocal, True), CodigoCombo(cboOrientacion, True), CodigoCombo(cboImportancia, True), iUsrPerfilActual, iUsrLoginActual, getStrCodigo(txtNroAnexada.Text, True), 0, txtFechaPedido.DateValue, txtFechaRequerida.DateValue, txtFechaComite.DateValue, txtFiniplan.DateValue, txtFfinplan.DateValue, txtFinireal.DateValue, txtFfinreal.DateValue, txtHspresup, sDir, sGer, sSec, sUsualta, sSoli, sRefe, sSupe, sAuto, sEstado, txtFechaEstado.DateValue, sSituacion, xBpar, CodigoCombo(cmbPetClass, True), cmbPetImpact, bControlesSOX) Then    ' upd -001- e., j.
                'recupera el ultimo numero generado
                If Not aplRST.EOF Then
                    auxNro = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
                    aplRST.Close
                End If
            End If
        Else
            If sTipoPet = "ESP" Then
                sRefe = sSoli
                sSupe = Null
                sAuto = sSoli
            End If
            '{ add -001- m. Grabo en el log (historial) los cambios en los att. de impacto tecnol�gico y clase de la petici�n
            If UCase(glPETImpTech) <> UCase(Left(cmbPetImpact, 1)) Then
                If UCase(glPETImpTech) = "S" Then
                    sCambioImpacto = "Antes: Con impacto tecnol�gico / Despu�s: Sin impacto tecnol�gico"
                Else
                    sCambioImpacto = "Antes: Sin impacto tecnol�gico / Despu�s: Con impacto tecnol�gico"
                End If
                Call sp_AddHistorial(glNumeroPeticion, "PCHGIMP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<Impacto tecnol�gico>> " & Trim(sCambioImpacto))
                Call sp_DoMensaje("PET", "PCHGIMP", glNumeroPeticion, "", "NULL", "", "")
            End If
            If glPETClass <> CodigoCombo(cmbPetClass, True) Then
                sCambioDeClase = "Clase anterior: " & Trim(glPETClass) & " - Nueva clase: " & Trim(CodigoCombo(cmbPetClass, True))
                Call sp_AddHistorial(glNumeroPeticion, "PCHGCLS", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<Cambio de la clase>> " & Trim(sCambioDeClase))
                Call sp_DoMensaje("PET", "PCHGCLS", glNumeroPeticion, "", "NULL", "", "")
            End If
            '}
            If sp_UpdatePeticion(txtNrointerno.Text, txtNroAsignado.Text, txtTitulo.Text, sTipoPet, CodigoCombo(Me.cboPrioridad), CodigoCombo(cboCorpLocal, True), CodigoCombo(cboOrientacion, True), CodigoCombo(cboImportancia, True), iUsrPerfilActual, iUsrLoginActual, getStrCodigo(txtNroAnexada.Text, True), prjNroInterno, txtFechaPedido.DateValue, txtFechaRequerida.DateValue, txtFechaComite.DateValue, txtFiniplan.DateValue, txtFfinplan.DateValue, txtFinireal.DateValue, txtFfinreal.DateValue, txtHspresup, sDir, sGer, sSec, sUsualta, sSoli, sRefe, sSupe, sAuto, sEstado, txtFechaEstado.DateValue, sSituacion, xBpar, CodigoCombo(cmbPetClass, True), cmbPetImpact, bControlesSOX) Then    ' upd -001- e., j.
                If Not aplRST.EOF Then
                    auxNro = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
                    aplRST.Close
                End If
            End If
        End If
        
        sTXTalcance = ""
        If Not IsEmpty(auxNro) And auxNro <> "" Then
            glNumeroPeticion = auxNro
            If bFlgDescripcion Then
                Call sp_UpdateMemo(auxNro, "DESCRIPCIO", memDescripcion.Text)
                If bFlgExtension = True Then
                    'Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<DESCRIPCION>>" & vbCrLf & txDescripcion)
                    sTXTalcance = sTXTalcance & "<<DESCRIPCION ANT.>>" & vbCrLf & txDescripcion & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgCaracteristicas Then
                Call sp_UpdateMemo(auxNro, "CARACTERIS", memCaracteristicas.Text)
                If bFlgExtension = True Then
                    'Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<CARACTERISTICAS>>" & vbCrLf & txCaracteristicas)
                    sTXTalcance = sTXTalcance & "<<CARACTERISTICAS ANT.>>" & vbCrLf & txCaracteristicas & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgMotivos Then
                Call sp_UpdateMemo(auxNro, "MOTIVOS", memMotivos.Text)
                If bFlgExtension = True Then
                    'Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<MOTIVOS>>" & vbCrLf & txMotivos)
                    sTXTalcance = sTXTalcance & "<<MOTIVOS ANT.>>" & vbCrLf & txMotivos & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgBeneficios Then
                Call sp_UpdateMemo(auxNro, "BENEFICIOS", memBeneficios.Text)
                If bFlgExtension = True Then
                    'Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<BENEFICIOS>>" & vbCrLf & txBeneficios)
                    sTXTalcance = sTXTalcance & "<<BENEFICIOS ANT.>>" & vbCrLf & txBeneficios & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgRelaciones Then
                Call sp_UpdateMemo(auxNro, "RELACIONES", memRelaciones.Text)
                If bFlgExtension = True Then
                    'Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<RELACIONES>>" & vbCrLf & txRelaciones)
                    sTXTalcance = sTXTalcance & "<<RELACIONES ANT.>>" & vbCrLf & txRelaciones & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgObservaciones Then
                Call sp_UpdateMemo(auxNro, "OBSERVACIO", memObservaciones.Text)
                If bFlgExtension = True Then
                    'Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<OBSERVACIONES>>" & vbCrLf & txObservaciones)
                    sTXTalcance = sTXTalcance & "<<OBSERVACIONES ANT.>>" & vbCrLf & txObservaciones & vbCrLf
                    bMsg = True
                End If
            End If
        End If
        
        If glModoPeticion = "MODI" And antBpar <> "" And antBpar <> xBpar Then
            Call sp_AddHistorial(glNumeroPeticion, "PCHGBP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<Anterior:" & antBpar & " --> Vigente: " & xBpar & ">>")
        End If
        
        If bMsg = True Then
            Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sTXTalcance)
            Call sp_DoMensaje("PET", "PCHGTXT", glNumeroPeticion, "", "NULL", "", "")
        End If
        
        'CASO MUY ESPECIAL alta de peticiones PROPIAS
        If (sTipoPet = "PRO" Or sTipoPet = "ESP" Or sTipoPet = "PRJ") And glModoPeticion = "ALTA" Then
            'Asigna el numero automaticamente
            nNumeroAsignado = sp_ProximoNumero("ULPETASI")
            If nNumeroAsignado > 0 Then
                If Not sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, nNumeroAsignado) Then
                    MsgBox ("Error al asignar numero")
                End If
            Else
                MsgBox ("Error al asignar numero")
            End If
'''            If CodigoCombo(cboPrioridad) = "3" Then
'''                xEstado = "ESTIMA"
'''            Else
'''                xEstado = "PLANIF"
'''            End If
            If CodigoCombo(cboPrioridad) = "5" Or CodigoCombo(cboPrioridad) = "6" Then
                xEstado = "ESTIMA"
            Else
                xEstado = "PLANIF"
            End If
            '{ add -001- l.
            If sTipoPet = "ESP" Then
                If PerfilInternoActual = "CSEC" Or PerfilInternoActual = "CGRU" Then
                    xEstado = "COMITE"      'Estado: Al business partner
                End If
            End If
            '}
            Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", xEstado, Null, Null)
            If sTipoPet = "PRO" Or sTipoPet = "ESP" Then
                Call sp_AddHistorial(glNumeroPeticion, "PNEW003", xEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<Alta de Peticion>>")
            Else
                Call sp_AddHistorial(glNumeroPeticion, "PNEW004", xEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "<<Alta de Proyecto>>")
            End If
            
            'si es CSEC o CGRU genera un sector
            If PerfilInternoActual = "CSEC" Or PerfilInternoActual = "CGRU" Then
                '{ add -001- l.
                If sTipoPet = "ESP" Then
                    xEstado = "ACONFI"
                End If
                '}
                If sp_InsertPeticionSector(glNumeroPeticion, xPerfSec, Null, Null, Null, Null, 0, xEstado, Date, "", 0, "0") Then
                    'si es un CGRU a nivel de grupo ademas genera un grupo
                    If PerfilInternoActual = "CGRU" And xPerfGru <> "" Then
                        Call sp_InsertPeticionGrupo(glNumeroPeticion, xPerfGru, Null, Null, Null, Null, 0, xEstado, Date, "", 0, "0")
                    End If
                    If PerfilInternoActual = "CGRU" Then
                        If sTipoPet = "PRO" Then
                            Call sp_DoMensaje("SEC", "PNEW003", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                        ElseIf sTipoPet = "PRJ" Then
                            Call sp_DoMensaje("SEC", "PNEW004", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                        End If
                    End If
                    If sTipoPet = "PRO" Then
                        Call sp_DoMensaje("PET", "PNEW003", glNumeroPeticion, "", "NULL", "", "")
                    ElseIf sTipoPet = "PRJ" Then
                        Call sp_DoMensaje("PET", "PNEW004", glNumeroPeticion, "", "NULL", "", "")
                    End If
                    If sTipoPet = "ESP" Then
                        Call sp_DoMensaje("PET", "PNEW005", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                        Call sp_DoMensaje("PET", "PNEW005", glNumeroPeticion, "", "NULL", "", "")
                    End If
                End If
            End If
        End If
        
        Me.Tag = "REFRESH"
        Call touchForms
        If glModoPeticion = "ALTA" Then
            glModoPeticion = "VIEW"
            Call InicializarPantalla(False)
            'Me.Refresh
            DoEvents
        Else
            glModoPeticion = "VIEW"
            Call InicializarPantalla(False)
            'Call HabilitarBotones
            'Me.Refresh
            DoEvents
        End If
    End If
    Call LockProceso(False)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo x
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
x:
End Sub

Private Sub InitTipoPet()
    cboTipopet.Clear
    Select Case glModoPeticion
        Case "ALTA"
            Select Case PerfilInternoActual
                Case "ADMI"
                    cboTipopet.AddItem "Normal                                  ||NOR"
                    cboTipopet.AddItem "Propia                                  ||PRO"
                    cboTipopet.AddItem "Especial                                ||ESP"
                    'cboTipopet.AddItem "Proyecto                                ||PRJ" ' del -001- f.
                    cboTipopet.AddItem "Auditor�a Interna                       ||AUI"
                    cboTipopet.AddItem "Auditor�a Externa                       ||AUX"
                Case "SADM"
                    cboTipopet.AddItem "Normal                                  ||NOR"
                    cboTipopet.AddItem "Propia                                  ||PRO"
                    cboTipopet.AddItem "Especial                                ||ESP"
                    cboTipopet.AddItem "Proyecto                                ||PRJ"
                    cboTipopet.AddItem "Auditor�a Interna                       ||AUI"
                    cboTipopet.AddItem "Auditor�a Externa                       ||AUX"
                Case "SOLI"
                    'cboTipopet.AddItem "Normal                                  ||NOR"     ' del -001- f.
                    '{ add -001. f.
                    If glLOGIN_Direccion = "MEDIO" Then
                        If glLOGIN_Gerencia = "ORG." Then
                            cboTipopet.AddItem "Normal                                  ||NOR"
                        Else
                            cboTipopet.AddItem "Propia                                  ||PRO"
                        End If
                    Else
                        cboTipopet.AddItem "Normal                                  ||NOR"
                    End If
                    '}
                Case "CGRU", "CSEC"
                    '{ add -001. f.
                    If glLOGIN_Direccion <> "MEDIO" Then
                        cboTipopet.AddItem "Normal                                  ||NOR"
                    Else
                        cboTipopet.AddItem "Propia                                  ||PRO"
                        cboTipopet.AddItem "Especial                                ||ESP"
                    End If
                    '}
                    '{ del -001- f.
                    'cboTipopet.AddItem "Propia                                  ||PRO"
                    'cboTipopet.AddItem "Especial                                ||ESP"
                    '}
                    '{ del -001- f.
                    'If glLOGIN_Gerencia = "ORG." Then
                    '    cboTipopet.AddItem "Especial                                ||ESP"
                    'End If
                    '}
                    'cboTipopet.AddItem "Proyecto                                ||PRJ" ' del -001- f.
                    txtFechaComite.Text = Format(Date, "dd/mm/yyyy")
                '{ add -002- c.
                Case "BPAR"
                    cboTipopet.AddItem "Normal                                  ||NOR"
                    cboTipopet.AddItem "Proyecto                                ||PRJ"
                    cboTipopet.AddItem "Especial                                ||ESP"
                '}
            End Select
        Case Else
            cboTipopet.AddItem "Normal                                  ||NOR"
            cboTipopet.AddItem "Propia                                  ||PRO"
            cboTipopet.AddItem "Proyecto                                ||PRJ"
            cboTipopet.AddItem "Especial                                ||ESP"
            cboTipopet.AddItem "Auditor�a Interna                       ||AUI"
            cboTipopet.AddItem "Auditor�a Externa                       ||AUX"
    End Select
    cboTipopet.ListIndex = 0
End Sub

Public Sub InicializarPantalla(Optional bCompleto)
    If IsMissing(bCompleto) Then
        bCompleto = True
    End If
    Me.Tag = ""
    flgEnCarga = True
    Call LockProceso(True)
    sSoli = ""
    sRefe = ""
    sDire = ""
    sSupe = ""
    sUsualta = glLOGIN_ID_REEMPLAZO
    xUsrLoginActual = glLOGIN_ID_REEMPLAZO
    xUsrPerfilActual = PerfilInternoActual
    cmdModificar.Enabled = False
    cmdOk.Enabled = False
    cmdPrint.Enabled = False
    cmdHistView.Enabled = False
    cmdRptHs.Enabled = False
    cmdAgrupar.Enabled = False
    cmdAsigNro.Enabled = False
    cmdCambioEstado.Enabled = False
    cmdEliminar.Enabled = False
    cmdSector.Enabled = False
    cmdGrupos.Enabled = False
    
    '{ add -001- h.
    Select Case glModoPeticion
        Case "ALTA"
            With cboAdjClass
                .Clear
                .AddItem "C102 : Dise�o de soluci�n"
                .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
                .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
                .AddItem "C204 : Casos de prueba"
                .AddItem "CG04 : Documento de cambio de alcance"
                .AddItem "T700 : Plan de implantaci�n Desarrollo"       ' Este es el T700
                .ListIndex = -1
            End With
        Case Else
            If glPeticion_sox001 = 1 Then
                With cboAdjClass
                    .Clear
                    .AddItem "C102 : Dise�o de soluci�n"
                    .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
                    .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
                    .AddItem "C204 : Casos de prueba"
                    .AddItem "CG04 : Documento de cambio de alcance"
                    .AddItem "T700 : Plan de implantaci�n Desarrollo"       ' Este es el T700
                    .ListIndex = -1
                End With
            Else
                With cboAdjClass
                    .Clear
                    .AddItem "REQU : Requerimiento"
                    .AddItem "ALCA : Doc. de alcance desarrollo"
                    .AddItem "COST : Evaluaci�n costo - beneficio"
                    .AddItem "PLEJ : Plan de ejecuci�n"
                    .AddItem "PLIM : Plan de implantaci�n Desarrollo"
                    .AddItem "MAIL : Mail"
                    .AddItem "OTRO : Otros"
                    .ListIndex = -1
                End With
            End If
    End Select
    '}
    
    Select Case glModoPeticion
        Case "ALTA"
            orjBtn(orjANX).Visible = False
            orjBtn(orjDOC).Visible = False
            orjBtn(orjADJ).Visible = False
            orjBtn(orjCNF).Visible = False
            txtNrointerno.Text = ""
            txtFechaPedido.Text = Format$(Date, "ddmmyyyy")
            txtFechaPedido.ForceFormat
            cboPrioridad.ListIndex = -1
            cboTipopet.ListIndex = 0
            If PerfilInternoActual = "SADM" Then
                txtFechaComite.Text = Format(Date, "dd/mm/yyyy")
                txtFechaComite.ForceFormat
            End If
            If PerfilInternoActual <> "SADM" Then
                sSoli = glLOGIN_ID_REEMPLAZO
            End If
            Call InicializarCombos
        Case Else
            orjBtn(orjANX).Visible = True
            'aqui
            orjBtn(orjDOC).Visible = True
            orjBtn(orjADJ).Visible = True
            orjBtn(orjCNF).Visible = True
            Call CargaPeticion(bCompleto)
    End Select
    
    If bCompleto = True Then
        Call CargaAnexas
    End If
    
    Call HabilitarBotones

    bFlgObservaciones = False
    bFlgBeneficios = False
    bFlgCaracteristicas = False
    bFlgMotivos = False
    bFlgRelaciones = False
    bFlgDescripcion = False
    On Error Resume Next
    'Show
    Call orjBtn_Click(orjMEM)
    DoEvents
    Call orjBtn_Click(orjDAT)
    DoEvents
    txtTitulo.SetFocus
    DoEvents
    Call LockProceso(False)
    flgEnCarga = False
End Sub

Private Sub HabilitarBotones()

'{ add -002- c. - Variables locales para guardar los atributos
Dim PetClassActual As String
Dim PetTypeActual As String
'}

Call setHabilCtrl(cboTipopet, "DIS")
'{ add -001- a. - Se deshabilitan por defecto ambos controles (Clase de petici�n e Impacto tecnol�gico)
Call setHabilCtrl(cmbPetClass, "DIS")
Call setHabilCtrl(cmbPetImpact, "DIS")
'}
cmdModificar.Enabled = False
cmdOk.Enabled = False
cmdPrint.Enabled = False
cmdHistView.Enabled = False
cmdRptHs.Enabled = False
cmdAsigNro.Enabled = False
cmdCambioEstado.Enabled = False
cmdEliminar.Enabled = False
cmdSector.Enabled = False
cmdGrupos.Enabled = False
cmdCancel.Enabled = True
cmdAgrupar.Enabled = False
cboImportancia.Locked = False
cboImportancia.ToolTipText = ""

Call setHabilCtrl(txtTitulo, "DIS")
Call setHabilCtrl(txtFechaRequerida, "DIS")
Call setHabilCtrl(txtFechaPedido, "DIS")
Call setHabilCtrl(txtFechaComite, "DIS")
Call setHabilCtrl(txtFinireal, "DIS")
Call setHabilCtrl(txtFiniplan, "DIS")
Call setHabilCtrl(txtFfinreal, "DIS")
Call setHabilCtrl(txtFfinplan, "DIS")
Call setHabilCtrl(txtHspresup, "DIS")
Call setHabilCtrl(txtusrImportancia, "DIS")
Call setHabilCtrl(cboPrioridad, "DIS")
Call setHabilCtrl(cboCorpLocal, "DIS")
Call setHabilCtrl(cboOrientacion, "DIS")
Call setHabilCtrl(cboImportancia, "DIS")
Call setHabilCtrl(cboBpar, "DIS")
Call setHabilCtrl(cboSector, "DIS")
Call setHabilCtrl(cboSolicitante, "DIS")
Call setHabilCtrl(cboReferente, "DIS")
Call setHabilCtrl(cboSupervisor, "DIS")
Call setHabilCtrl(cboDirector, "DIS")
Call setHabilCtrl(memBeneficios, "DIS")
Call setHabilCtrl(memCaracteristicas, "DIS")
Call setHabilCtrl(memDescripcion, "DIS")
Call setHabilCtrl(memMotivos, "DIS")
Call setHabilCtrl(memObservaciones, "DIS")
Call setHabilCtrl(memRelaciones, "DIS")
Call setHabilCtrl(txtEstado, "DIS")
Call setHabilCtrl(txtSituacion, "DIS")
Call setHabilCtrl(txtFechaEstado, "DIS")
Call setHabilCtrl(txtNroAnexada, "DIS")
Call setHabilCtrl(txtUsualta, "DIS")
Call setHabilCtrl(txtNroAsignado, "DIS")

If Val(getStrCodigo(txtNroAnexada.Text, True)) = 0 Then
    cmdVerAnexora.Enabled = False
Else
    cmdVerAnexora.Enabled = True
End If

Select Case glModoPeticion
    Case "ALTA"
        orjBtn(orjANX).Visible = False
        orjBtn(orjDOC).Visible = False
        orjBtn(orjADJ).Visible = False
        orjBtn(orjCNF).Visible = False
        glNroAsignado = ""
        lblModo.Caption = "ALTA"
        cmdOk.Enabled = True
        cmdCancel.Caption = "CANCELA"
        Select Case PerfilInternoActual
            Case "ADMI"
                sEstado = "COMITE"
                sSituacion = ""
                cboTipopet.ListIndex = 0
                Call setHabilCtrl(txtTitulo, "NOR")
                Call setHabilCtrl(txtFechaRequerida, "NOR")
                Call setHabilCtrl(cboBpar, "NOR")
                Call setHabilCtrl(cboSector, "NOR")
                Call setHabilCtrl(memBeneficios, "NOR")
                Call setHabilCtrl(memCaracteristicas, "NOR")
                Call setHabilCtrl(memDescripcion, "NOR")
                Call setHabilCtrl(memMotivos, "NOR")
                Call setHabilCtrl(memObservaciones, "NOR")
                Call setHabilCtrl(memRelaciones, "NOR")
            Case "SOLI"
                sEstado = "CONFEC"
                sSituacion = ""
                '{ add -001- f.
                If glLOGIN_Direccion = "MEDIO" Then
                    If glLOGIN_Gerencia = "ORG." Then
                        cboTipopet.ListIndex = PosicionCombo(Me.cboTipopet, "NOR", True)
                    Else
                        cboTipopet.ListIndex = PosicionCombo(Me.cboTipopet, "PRO", True)
                    End If
                Else
                    cboTipopet.ListIndex = PosicionCombo(Me.cboTipopet, "NOR", True)
                End If
                Call setHabilCtrl(txtTitulo, "NOR")
                Call setHabilCtrl(txtFechaRequerida, "NOR")
                Call setHabilCtrl(cboSector, "NOR")
                Call setHabilCtrl(memBeneficios, "NOR")
                Call setHabilCtrl(memCaracteristicas, "NOR")
                Call setHabilCtrl(memDescripcion, "NOR")
                Call setHabilCtrl(memMotivos, "NOR")
                Call setHabilCtrl(memObservaciones, "NOR")
                Call setHabilCtrl(memRelaciones, "NOR")
                Call setHabilCtrl(cboPrioridad, "NOR")
                '}
                '{ del -001- f.
                'cboTipopet.ListIndex = PosicionCombo(Me.cboTipopet, "NOR", True)
                'Call setHabilCtrl(txtTitulo, "NOR")
                'Call setHabilCtrl(txtFechaRequerida, "NOR")
                'Call setHabilCtrl(cboSector, "NOR")
                'Call setHabilCtrl(memBeneficios, "NOR")
                'Call setHabilCtrl(memCaracteristicas, "NOR")
                'Call setHabilCtrl(memDescripcion, "NOR")
                'Call setHabilCtrl(memMotivos, "NOR")
                'Call setHabilCtrl(memObservaciones, "NOR")
                'Call setHabilCtrl(memRelaciones, "NOR")
                '}
            Case "CSEC", "CGRU"
                'por defecto para un alta asume PROPIA
                cboTipopet.ListIndex = PosicionCombo(Me.cboTipopet, "PRO", True)
                Call setHabilCtrl(cmbPetClass, "NOR")   ' add -001- b. - En el alta de una nueva petici�n, se habilita el control para los Resp. de Grupo y/o Sector
                sEstado = "APROBA"
                sSituacion = ""
                Call setHabilCtrl(cboTipopet, "NOR")
                Call setHabilCtrl(cboPrioridad, "NOR")
                Call setHabilCtrl(txtTitulo, "NOR")
                Call setHabilCtrl(txtFechaRequerida, "NOR")
                Call setHabilCtrl(cboBpar, "NOR")
                Call setHabilCtrl(cboSector, "NOR")
                'Call setHabilCtrl(cboSolicitante, "NOR")
                Call setHabilCtrl(memBeneficios, "NOR")
                Call setHabilCtrl(memCaracteristicas, "NOR")
                Call setHabilCtrl(memDescripcion, "NOR")
                Call setHabilCtrl(memMotivos, "NOR")
                Call setHabilCtrl(memObservaciones, "NOR")
                Call setHabilCtrl(memRelaciones, "NOR")
                Call setHabilCtrl(cboSector, "NOR")
                cboBpar.ListIndex = -1
            Case "SADM"
                sEstado = "CONFEC"
                sSituacion = ""
                Call setHabilCtrl(txtTitulo, "NOR")
                Call setHabilCtrl(txtFechaRequerida, "NOR")
                Call setHabilCtrl(txtFinireal, "NOR")
                Call setHabilCtrl(txtFiniplan, "NOR")
                Call setHabilCtrl(txtFfinreal, "NOR")
                Call setHabilCtrl(txtFfinplan, "NOR")
                Call setHabilCtrl(txtFechaComite, "NOR")
                Call setHabilCtrl(cboTipopet, "NOR")
                Call setHabilCtrl(cboBpar, "NOR")
                Call setHabilCtrl(cboSector, "NOR")
                Call setHabilCtrl(cboSolicitante, "NOR")
                Call setHabilCtrl(cboReferente, "NOR")
                Call setHabilCtrl(cboSupervisor, "NOR")
                Call setHabilCtrl(cboDirector, "NOR")
                Call setHabilCtrl(cboPrioridad, "NOR")
                Call setHabilCtrl(memBeneficios, "NOR")
                Call setHabilCtrl(memCaracteristicas, "NOR")
                Call setHabilCtrl(memDescripcion, "NOR")
                Call setHabilCtrl(memMotivos, "NOR")
                Call setHabilCtrl(memObservaciones, "NOR")
                Call setHabilCtrl(memRelaciones, "NOR")
                Call setHabilCtrl(cboCorpLocal, "NOR")
                Call setHabilCtrl(cboOrientacion, "NOR")
                Call setHabilCtrl(cboImportancia, "NOR")
        End Select
    Case "MODI"
        orjBtn(orjANX).Visible = False
        orjBtn(orjDOC).Visible = False
        orjBtn(orjADJ).Visible = False
        orjBtn(orjCNF).Visible = False
        Call orjBtn_Click(orjDAT)
        lblModo.Caption = "MODIFICA"
        cmdOk.Enabled = True
        cmdCancel.Caption = "CANCELA"
        Select Case PerfilInternoActual
            Case "SOLI"
                Call setHabilCtrl(memBeneficios, "NOR")
                Call setHabilCtrl(memCaracteristicas, "NOR")
                Call setHabilCtrl(memDescripcion, "NOR")
                Call setHabilCtrl(memMotivos, "NOR")
                Call setHabilCtrl(memObservaciones, "NOR")
                Call setHabilCtrl(memRelaciones, "NOR")
                Call setHabilCtrl(txtTitulo, "NOR")
                If bFlgExtension = False Then
                    Call setHabilCtrl(txtFechaRequerida, "NOR")
                    If sEstado = "CONFEC" Then
                        txtTitulo.SetFocus
                    Else
                        txtFechaRequerida.SetFocus
                    End If
                Else
                    Call orjBtn_Click(orjMEM)
                    DoEvents
                    memDescripcion.SetFocus
                End If
            Case "REFE"
                Call setHabilCtrl(txtTitulo, "NOR")
                Call setHabilCtrl(txtFechaRequerida, "NOR")
                Call setHabilCtrl(memBeneficios, "NOR")
                Call setHabilCtrl(memCaracteristicas, "NOR")
                Call setHabilCtrl(memDescripcion, "NOR")
                Call setHabilCtrl(memMotivos, "NOR")
                Call setHabilCtrl(memObservaciones, "NOR")
                Call setHabilCtrl(memRelaciones, "NOR")
            Case "CSEC"
                If flgIngerencia Then
                    If CodigoCombo(cboTipopet, True) = "NOR" Then
                        cboTipopet.Clear
                        cboTipopet.AddItem "Normal                                  ||NOR"
                        cboTipopet.AddItem "Proyecto                                ||PRJ"
                        cboTipopet.ListIndex = 0
                        cmdCancel.Enabled = True
                        cmdOk.Enabled = True
                        DoEvents
                        Call setHabilCtrl(cboTipopet, "NOR")
                    End If
                    If CodigoCombo(cboTipopet, True) = "PRO" Then
                        cboTipopet.Clear
                        cboTipopet.AddItem "Propia                                  ||PRO"
                        cboTipopet.AddItem "Proyecto                                ||PRJ"
                        cboTipopet.ListIndex = 0
                        cmdCancel.Enabled = True
                        cmdOk.Enabled = True
                        DoEvents
                        Call setHabilCtrl(cboTipopet, "NOR")
                    End If
                    '{ del -002- c.
                    ''{ add -001- b. - Se el perfil actual es es Resp. de Sector y esta modificando la petici�n de or�gen "Especial": el control es cargado con todas las opciones y habilitado para trabajar.
                    'If CodigoCombo(cboTipopet, True) = "ESP" Then
                    '    Call setHabilCtrl(cmbPetClass, "NOR")
                    'End If
                    ''}
                    '}
                    Call setHabilCtrl(cmbPetClass, "DIS")       ' add -002- c.
                    If (CodigoCombo(cboTipopet, True) = "PRJ" Or CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "ESP") Then
                        Call setHabilCtrl(txtTitulo, "NOR")
                        Call setHabilCtrl(cboPrioridad, "NOR")
                        Call setHabilCtrl(txtFechaRequerida, "NOR")
                        Call setHabilCtrl(memBeneficios, "NOR")
                        Call setHabilCtrl(memCaracteristicas, "NOR")
                        Call setHabilCtrl(memDescripcion, "NOR")
                        Call setHabilCtrl(memMotivos, "NOR")
                        Call setHabilCtrl(memObservaciones, "NOR")
                        Call setHabilCtrl(memRelaciones, "NOR")
                    End If
                    If CodigoCombo(cboTipopet, True) = "PRJ" Then
                        Call setHabilCtrl(cboCorpLocal, "NOR")
                        Call setHabilCtrl(cboOrientacion, "NOR")
                        If modifImportancia(xUsrLoginActual, xUsrPerfilActual, glLOGIN_ID_REEMPLAZO, PerfilInternoActual) Then
                            Call setHabilCtrl(cboImportancia, "NOR")
                            cboImportancia.Locked = False
                        Else
                            Call setHabilCtrl(cboImportancia, "NOR")
                            cboImportancia.Locked = True
                            cboImportancia.ToolTipText = "No puede modificarse, debido a que el perfil que la defini� previamente es superior o igual al actual."
                        End If
                    End If
                End If
        Case "CGRU"
            If flgIngerencia Then
                If (CodigoCombo(cboTipopet, True) = "PRJ" Or CodigoCombo(cboTipopet, True) = "PRO") Or CodigoCombo(cboTipopet, True) = "ESP" Then
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(cboPrioridad, "NOR")
                    Call setHabilCtrl(txtFechaRequerida, "NOR")
                    Call setHabilCtrl(memBeneficios, "NOR")
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    'Call setHabilCtrl(cmbPetClass, "NOR")       ' add -001- b.     ' del -002- c.
                    Call setHabilCtrl(cmbPetClass, "DIS")       ' add -002- c.
                End If
                If CodigoCombo(cboTipopet, True) = "PRJ" Then
                    Call setHabilCtrl(cboCorpLocal, "NOR")
                    Call setHabilCtrl(cboOrientacion, "NOR")
                End If
            End If
        Case "CDIR", "CGCI"
            If CodigoCombo(cboTipopet, True) = "PRJ" Then
                If modifImportancia(xUsrLoginActual, xUsrPerfilActual, glLOGIN_ID_REEMPLAZO, PerfilInternoActual) Then
                    Call setHabilCtrl(cboImportancia, "NOR")
                    cboImportancia.Locked = False
                Else
                    Call setHabilCtrl(cboImportancia, "NOR")
                    cboImportancia.Locked = True
                    cboImportancia.ToolTipText = "No puede modificarse, debido a que el perfil que la defini� previamente es superior o igual al actual."
                End If
            End If
        Case "ADMI"
            Call setHabilCtrl(txtFechaComite, "NOR")
            Call setHabilCtrl(cboPrioridad, "NOR")
        Case "BPAR"
            Call setHabilCtrl(txtFechaComite, "DIS")
            Call setHabilCtrl(cboPrioridad, "NOR")
            '{ add -002- a.
            Call setHabilCtrl(cboBpar, "NOR")
            Call setHabilCtrl(cmbPetImpact, "NOR")
            Call setHabilCtrl(cboOrientacion, "NOR")    ' Se agrega Orientaci�n
            '}
            '{ add -002- c.
            ' Inicializa las opciones de tipo
            If CodigoCombo(cboTipopet, True) <> "PRO" Then
                PetTypeActual = CodigoCombo(cboTipopet, True)
                With cboTipopet
                    .Clear
                    .AddItem "Normal                                  ||NOR"
                    .AddItem "Especial                                ||ESP"
                    .AddItem "Proyecto                                ||PRJ"
                End With
                cboTipopet.ListIndex = PosicionCombo(cboTipopet, ClearNull(PetTypeActual), True)
                ' Inicializa las opciones de clase
                PetClassActual = CodigoCombo(cmbPetClass, True)
                Select Case CodigoCombo(cboTipopet, True)
                    Case "NOR"
                        With cmbPetClass
                            .Clear
                            .AddItem "Atenci�n a usuario                                                ||ATEN"
                            .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                            .AddItem "Nuevo desarrollo                                              ||NUEV"
                        End With
                    Case "ESP"
                        With cmbPetClass
                            .Clear
                            .AddItem "Atenci�n a usuario                                                ||ATEN"
                            .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                            .AddItem "Nuevo desarrollo                                              ||NUEV"
                        End With
                    Case "PRJ"
                        With cmbPetClass
                            .Clear
                            .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                            .AddItem "Nuevo desarrollo                                              ||NUEV"
                        End With
                End Select
                cmbPetClass.ListIndex = PosicionCombo(cmbPetClass, ClearNull(PetClassActual), True)
                DoEvents
                Call setHabilCtrl(cboTipopet, "NOR")
                Call setHabilCtrl(cmbPetClass, "NOR")
            Else
                Call setHabilCtrl(cboTipopet, "DIS")
                Call setHabilCtrl(cmbPetClass, "DIS")
                Call setHabilCtrl(cmbPetImpact, "DIS")
            End If
            '}
            '{ mov -001- }
        Case "SADM"
            Call CargaCombosConTodo
            If xSec <> "" Then
                Call SetCombo(cboSector, xSec, True)
            End If
            If sSoli <> "" Then
                Call SetCombo(cboSolicitante, sSoli)
            End If
            If sRefe <> "" Then
                Call SetCombo(cboReferente, sRefe)
            End If
            If sSupe <> "" Then
                Call SetCombo(cboSupervisor, sSupe)
            End If
            If sDire <> "" Then
                Call SetCombo(cboDirector, sDire)
            End If
            Call setHabilCtrl(txtTitulo, "NOR")
            Call setHabilCtrl(txtFechaRequerida, "NOR")
            Call setHabilCtrl(txtFechaComite, "NOR")
            Call setHabilCtrl(txtFinireal, "NOR")
            Call setHabilCtrl(txtFiniplan, "NOR")
            Call setHabilCtrl(txtFfinreal, "NOR")
            Call setHabilCtrl(txtFfinplan, "NOR")
            Call setHabilCtrl(txtHspresup, "NOR")
            Call setHabilCtrl(cboTipopet, "NOR")
            Call setHabilCtrl(cboPrioridad, "NOR")
            Call setHabilCtrl(cboSector, "NOR")
            Call setHabilCtrl(cboBpar, "NOR")
            Call setHabilCtrl(cboSolicitante, "NOR")
            Call setHabilCtrl(cboReferente, "NOR")
            Call setHabilCtrl(cboSupervisor, "NOR")
            Call setHabilCtrl(cboDirector, "NOR")
            Call setHabilCtrl(memBeneficios, "NOR")
            Call setHabilCtrl(memCaracteristicas, "NOR")
            Call setHabilCtrl(memDescripcion, "NOR")
            Call setHabilCtrl(memMotivos, "NOR")
            Call setHabilCtrl(memObservaciones, "NOR")
            Call setHabilCtrl(memRelaciones, "NOR")
            Call setHabilCtrl(cboCorpLocal, "NOR")
            Call setHabilCtrl(cboOrientacion, "NOR")
            Call setHabilCtrl(cboImportancia, "NOR")
        End Select
    Case "VIEW"
        lblModo.Caption = "VISUALIZAR"
        cmdOk.Enabled = False
        cmdCancel.Caption = "CERRAR"
        cmdPrint.Enabled = True
        cmdHistView.Enabled = True
        cmdRptHs.Enabled = True
        cmdAgrupar.Enabled = True
        'aqui
        orjBtn(orjANX).Visible = True
        orjBtn(orjDOC).Visible = True
        orjBtn(orjADJ).Visible = True
        orjBtn(orjCNF).Visible = True
        Select Case PerfilInternoActual
            Case "SADM"
                    cmdCambioEstado.Enabled = True
                    cmdModificar.Enabled = True
                    cmdAsigNro.Visible = True
                    cmdAsigNro.Enabled = True
                    cmdEliminar.Enabled = True
            Case "CGRU"
                'esto est� sin tabla, horrible!!
                If flgIngerencia Then
                    If (CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                        cmdModificar.Enabled = True
                       ''cmdEliminar.Enabled = True
                    End If
                 End If
            Case "CSEC"
                'esto est� sin tabla, horrible!!
                If flgIngerencia Then
                    If (CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                        cmdModificar.Enabled = True
                        ''cmdEliminar.Enabled = True
                    End If
                    If (CodigoCombo(cboTipopet, True) = "NOR") Then
                        cmdModificar.Enabled = True
                    End If
                 End If
            Case "CDIR", "CGCI"
                If CodigoCombo(cboTipopet, True) = "PRJ" Then
                        cmdModificar.Enabled = True
                End If
            '{ add -001- g. Importante: hay que confirmar que entonces esta porci�n de c�digo no es necesaria, ya que la operatoria normal estar�a contemplada en el Case Else que sigue a continuaci�n.
            'Case "BPAR"
            '    If (CodigoCombo(cboTipopet, True) = "NOR") Then
            '        cmdModificar.Enabled = True
            '    End If
            '}
            Case Else
                If flgIngerencia Then
                    If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado) Then
                        If Not aplRST.EOF Then
                            cmdModificar.Enabled = True
                            aplRST.Close
                        End If
                    End If
                    If Val(glNroAsignado) = 0 And sp_GetAccionPerfil("PASINRO", PerfilInternoActual, sEstado) Then
                        If Not aplRST.EOF Then
                            cmdAsigNro.Enabled = True
                            aplRST.Close
                        End If
                    End If
                    If Val(glNroAsignado) = 0 And sp_GetAccionPerfil("PDEL000", PerfilInternoActual, sEstado) Then
                        If Not aplRST.EOF Then
                            cmdEliminar.Enabled = True
                            aplRST.Close
                        End If
                    End If
                    If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado) Then
                        If Not aplRST.EOF Then
                            cmdCambioEstado.Enabled = True
                            aplRST.Close
                        End If
                    End If
                    If PerfilInternoActual = "SOLI" And _
                        InStr("PLANIF|PLANOK|ESTIMA|ESTIOK|EJECUC", sEstado) > 0 And _
                        (CodigoCombo(cboTipopet, True) = "NOR" Or CodigoCombo(cboTipopet, True) = "ESP" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                        bFlgExtension = True
                        cmdModificar.Enabled = True
                    End If
                    If PerfilInternoActual = "SOLI" And _
                        InStr("CONFEC|DEVSOL", sEstado) > 0 And _
                        (CodigoCombo(cboTipopet, True) = "NOR" Or CodigoCombo(cboTipopet, True) = "ESP" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                        cmdGrupos.Enabled = True
                    End If
                    If PerfilInternoActual = "REFE" And _
                        InStr("REFERE|DEVREF", sEstado) > 0 And _
                        (CodigoCombo(cboTipopet, True) = "NOR" Or CodigoCombo(cboTipopet, True) = "ESP" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                        cmdGrupos.Enabled = True
                    End If
                End If
        End Select
        cmdSector.Enabled = True
End Select

Call LockProceso(False)

End Sub

Function CamposObligatorios() As Boolean
    ' Control de t�tulo de la petici�n
    If ClearNull(txtTitulo.Text) = "" Then
       Call orjBtn_Click(orjDAT)
       MsgBox "Falta Especificar T�tulo", vbOKOnly + vbInformation
       CamposObligatorios = False
       Exit Function
    End If
    
    ' Control entre fecha de requerido y fecha de pedido
    If (Not IsNull(txtFechaRequerida.DateValue)) And (Not IsNull(txtFechaPedido.DateValue)) Then
        If txtFechaRequerida.DateValue <= txtFechaPedido.DateValue Then
            Call orjBtn_Click(orjDAT)
            MsgBox "La fecha requerida debe ser mayor a la fecha de alta", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    '{ add -002- f.
    Else
        If IsNull(txtFechaRequerida.DateValue) Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Debe ingresar la fecha requerida para la petici�n.", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        Else
            If txtFechaRequerida.DateValue <= txtFechaPedido.DateValue Then
                Call orjBtn_Click(orjDAT)
                MsgBox "La fecha requerida debe ser mayor a la fecha de alta", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
        End If
    '}
    End If
    
    ' Control de tipo Propia o Especial
    If CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "ESP" Then
        If cboPrioridad.ListIndex < 0 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Falta Especificar PRIORIDAD", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    End If
    
    ' Control de tipo Proyecto
    If CodigoCombo(cboTipopet, True) = "PRJ" Then
        ' Control de Importancia (Visibilidad)
        If cboImportancia.ListIndex < 0 Then
           Call orjBtn_Click(orjDAT)
           MsgBox "Falta Especificar VISIBILIDAD", vbOKOnly + vbInformation
           CamposObligatorios = False
           Exit Function
        End If
        ' Control de Corporativo/Local
        If cboCorpLocal.ListIndex < 0 Then
           Call orjBtn_Click(orjDAT)
           MsgBox "Falta Especificar Corp/Local", vbOKOnly + vbInformation
           CamposObligatorios = False
           Exit Function
        End If
        ' Control de orientaci�n
        If cboOrientacion.ListIndex < 0 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Falta Especificar ORIENTACION", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
        ' Control de prioridad
        If cboPrioridad.ListIndex < 0 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Falta Especificar PRIORIDAD", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
        '{ add -001- d. (Proyecto) Error si trata de designar la case CORR, ATEN o SPUF
        If CodigoCombo(cmbPetClass, True) = "CORR" Or CodigoCombo(cmbPetClass, True) = "ATEN" Or CodigoCombo(cmbPetClass, True) = "SPUF" Or CodigoCombo(cmbPetClass, True) = "" Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Las peticiones de tipo Proyecto solo pueden ser de alguna de las siguientes clases:" & vbCrLf & vbCrLf & "- Mantenimiento evolutivo" & vbCrLf & "- Nuevo desarrollo", vbCritical + vbOKOnly, "Clase de petici�n"
            CamposObligatorios = False
            Exit Function
        End If
        '}
    End If
    '{ add -001- d. Control de clase de petici�n
    If CodigoCombo(cmbPetClass, True) = "SINC" And (Not PerfilInternoActual = "SOLI" And Not PerfilInternoActual = "REFE" And Not PerfilInternoActual = "AUTO" And Not PerfilInternoActual = "SUPE") Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Debe indicar la clase de la petici�n actual para grabar los datos.", vbCritical + vbOKOnly, "Clase de petici�n"
        CamposObligatorios = False
        cmbPetClass.SetFocus
        Exit Function
    End If
    
    '{ del -002- c.
    'If PerfilInternoActual = "BPAR" Then
    '    If InStr(1, CodigoCombo(cboTipopet, True), "NOR") Then
    '        If CodigoCombo(cmbPetClass, True) <> "CORR" And CodigoCombo(cmbPetClass, True) <> "OPTI" Then
    '            Call orjBtn_Click(orjDAT)
    '            MsgBox "Las peticiones de tipo Normal para el perfil actual solo pueden ser de alguna de las siguientes clases:" & vbCrLf & _
    '                   " - Correctivo" & vbCrLf & _
    '                   " - Optimizaci�n", vbCritical + vbOKOnly, "Clase de petici�n"
    '    CamposObligatorios = False
    '            Exit Function
    '        End If
    '    End If
    'End If
    '}
    
    ' Control de impacto tecnol�gico
    If cmbPetImpact.ListIndex = 1 And Not PerfilInternoActual = "SOLI" Then          ' Indicado que lleva impacto tecnol�gico
        If InStr(1, CodigoCombo(cboTipopet, True), "PRO") Then
            Select Case CodigoCombo(cmbPetClass, True)       ' Clase de petici�n
                Case "CORR"  ' Clase: Correctivo
                    Call orjBtn_Click(orjDAT)
                    MsgBox "La petici�n de la clase correctivo no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                    CamposObligatorios = False
                    Exit Function
                Case "ATEN"  'Clase: atenci�n a usuario
                    Call orjBtn_Click(orjDAT)
                    MsgBox "La petici�n de la clase atenci�n a usuario no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                    CamposObligatorios = False
                    Exit Function
                Case Else
                    Call orjBtn_Click(orjDAT)
                    MsgBox "La petici�n propia no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                    CamposObligatorios = False
                    Exit Function
            End Select
        End If
    End If
    '}
    '{ add -001- f.
    If Not PerfilInternoActual = "SOLI" Then
        Select Case glModoPeticion
            Case "ALTA", "MODI"     ' upd -001- h. Se agrega el modo MODI
                If InStr(1, CodigoCombo(cboTipopet, True), "ESP") Then
                    If glLOGIN_Gerencia <> "ORG." Then
                        If CodigoCombo(cmbPetClass, True) <> "ATEN" Then
                            Call orjBtn_Click(orjDAT)
                            MsgBox "Las peticiones de tipo Especial para el perfil actual solo pueden ser de clase Atenci�n a Usuario.", vbCritical + vbOKOnly, "Clase de petici�n"
                            CamposObligatorios = False
                            Exit Function
                        End If
                    End If
                End If
                If InStr(1, CodigoCombo(cboTipopet, True), "PRO") Then
                    ' If cmbPetClass.ListIndex <> 1 And cmbPetClass.ListIndex <> 2 And cmbPetClass.ListIndex <> 3 Then   ' del -001- n.
                    '{ add -001- n. Controla si no debe cambiar el tipo a Especial
                    If CodigoCombo(cmbPetClass, True) = "ATEN" Then
                        Call orjBtn_Click(orjDAT)
                        MsgBox "Para generar una petici�n de clase Atenci�n al usuario, debe especificar que el tipo sea Especial.", vbCritical + vbOKOnly, "Clase de petici�n"
                        CamposObligatorios = False
                        Exit Function
                    End If
                    '}
                    If CodigoCombo(cmbPetClass, True) <> "CORR" And CodigoCombo(cmbPetClass, True) <> "OPTI" And CodigoCombo(cmbPetClass, True) <> "SPUF" Then   ' add -001- n.
                        Call orjBtn_Click(orjDAT)
                        MsgBox "Las peticiones de tipo Propia para el perfil actual solo pueden ser una de las siguientes clases: " & vbCrLf & vbCrLf & _
                        "- Correctivo" & vbCrLf & _
                        "- SPUFI" & vbCrLf & _
                        "- Optimizaci�n", vbCritical + vbOKOnly, "Clase de petici�n"
                        CamposObligatorios = False
                        Exit Function
                    End If
                End If
                If InStr(1, CodigoCombo(cboTipopet, True), "PRJ") Then
                    If CodigoCombo(cmbPetClass, True) <> "OPTI" And CodigoCombo(cmbPetClass, True) <> "EVOL" And CodigoCombo(cmbPetClass, True) <> "NUEV" Then
                        Call orjBtn_Click(orjDAT)
                        MsgBox "Las peticiones de tipo Proyecto para el perfil actual solo pueden ser una de las siguientes clases: " & vbCrLf & vbCrLf & _
                        "- Correctivo" & vbCrLf & _
                        "- Atenci�n a Usuario", vbCritical + vbOKOnly, "Clase de petici�n"
                        CamposObligatorios = False
                        Exit Function
                    End If
                End If
        End Select
    End If
    '}
    ' Control de sector
    If cboSector.ListIndex < 0 Then
       Call orjBtn_Click(orjDAT)
       MsgBox "Falta Especificar Sector", vbOKOnly + vbInformation
       CamposObligatorios = False
       Exit Function
    End If
    ' Control de solicitante
    If cboSolicitante.ListIndex < 0 Then
       Call orjBtn_Click(orjDAT)
       MsgBox "Falta Especificar Solicitante", vbOKOnly + vbInformation
       CamposObligatorios = False
       Exit Function
    End If
    ' Control de la descripci�n
    If ClearNull(memDescripcion.Text) = "" Then
       Call orjBtn_Click(orjMEM)
       MsgBox "Falta Especificar Descripci�n", vbOKOnly + vbInformation
       CamposObligatorios = False
       Exit Function
    End If
    If CodigoCombo(cboTipopet, True) <> "PRO" Then
        If ClearNull(memCaracteristicas.Text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Falta Especificar Caracter�sticas", vbOKOnly + vbInformation
           CamposObligatorios = False
           Exit Function
        End If
        If ClearNull(memMotivos.Text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Falta Especificar Motivos", vbOKOnly + vbInformation
           CamposObligatorios = False
           Exit Function
        End If
        If ClearNull(memBeneficios.Text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Falta Especificar Beneficios Esperados", vbOKOnly + vbInformation
           CamposObligatorios = False
           Exit Function
        End If
    End If
    'If PerfilInternoActual = "ADMI" Or PerfilInternoActual = "SADM" Then
    '    If cboPrioridad.ListIndex < 0 Then
    '        MsgBox "Falta Especificar PRIORIDAD", vbOKOnly + vbInformation
    '        CamposObligatorios = False
    '        Exit Function
    '    End If
    'End If
    'OJO: esta porci�n de c�digo es utilizada para debug
    'If MsgBox("Confirma???", vbQuestion + vbYesNo, "Confirmar") = vbYes Then
        CamposObligatorios = True
    'Else
    '    CamposObligatorios = False
    'End If
End Function

Private Sub CargaPeticion(Optional bCompleto)
    If IsMissing(bCompleto) Then
        bCompleto = True
    End If
    'Dim sDir, sGer, sSec As String
    Dim xDescri As String
    Dim xAsig As String
    
    Dim xImportancia As String
    Dim xCorpLocal As String
    Dim xOrientacion As String
    
    Call puntero(True)
    flgIngerencia = False
    flgSolicitor = False
  
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            txtNrointerno.Text = IIf(Not IsNull(aplRST!pet_nrointerno), aplRST!pet_nrointerno, "")
            txtNroAsignado.Text = IIf(Not IsNull(aplRST!pet_nroasignado), aplRST!pet_nroasignado, "S/N")
            If Val(ClearNull(aplRST!pet_nroanexada)) > 0 Then
                xAsig = IIf(Val(ClearNull(aplRST!anx_nroasignado)) > 0, ClearNull(aplRST!anx_nroasignado), "S/N")
                txtNroAnexada.Text = xAsig & Space(10) & "||" & IIf(Not IsNull(aplRST!pet_nroanexada), aplRST!pet_nroanexada, "")
            Else
                txtNroAnexada.Text = ""
            End If
            glNroAsignado = txtNroAsignado.Text
            prjNroInterno = IIf(Not IsNull(aplRST!prj_nrointerno), aplRST!prj_nrointerno, "")
            txtTitulo.Text = ClearNull(aplRST!titulo)
            sEstado = ClearNull(aplRST!cod_estado)
            txtEstado.Text = ClearNull(aplRST!nom_estado)
            sSituacion = ClearNull(aplRST!cod_situacion)
            txtSituacion.Text = ClearNull(aplRST!nom_situacion)
            txtFechaPedido.Text = IIf(Not IsNull(aplRST!fe_pedido), Format(aplRST!fe_pedido, "dd/mm/yyyy"), "")
            txtFechaRequerida.Text = IIf(Not IsNull(aplRST!fe_requerida), Format(aplRST!fe_requerida, "dd/mm/yyyy"), "")
            txtFiniplan.Text = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
            txtFfinplan.Text = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
            txtFinireal.Text = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "dd/mm/yyyy"), "")
            txtFfinreal.Text = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "dd/mm/yyyy"), "")
            txtFechaEstado.Text = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "dd/mm/yyyy"), "")
            txtFechaComite.Text = IIf(Not IsNull(aplRST!fe_comite), Format(aplRST!fe_comite, "dd/mm/yyyy"), "")
            txtHspresup.Text = ClearNull(aplRST!horaspresup)
            cboPrioridad.ListIndex = PosicionCombo(Me.cboPrioridad, ClearNull(aplRST!prioridad))
            cboTipopet.ListIndex = PosicionCombo(cboTipopet, ClearNull(aplRST!cod_tipo_peticion), True)
            '{ add -001- b. - Se carga el control con el dato correspondiente al registro solicitado
            cmbPetClass.ListIndex = PosicionCombo(cmbPetClass, ClearNull(aplRST!cod_clase), True)
            If aplRST!pet_imptech = "S" Then
                cmbPetImpact.ListIndex = 1
            Else
                cmbPetImpact.ListIndex = 0
            End If
            '}
            '{ add -001- i. - Guardo en la variable global glPETFechaEnvioAlBP la fecha de pasaje a comit� (BP) de la petici�n, en la variable global glPETImpTech indicador de impacto tecnol�gico de la petici�n y en glPETClass la clase de la petici�n
            glPETFechaEnvioAlBP = IIf(IsNull(aplRST!fe_comite), Format(0, "dd/mm/yyyy"), ClearNull(aplRST!fe_comite))
            glPETImpTech = ClearNull(aplRST!pet_imptech)
            glPETClass = ClearNull(aplRST!cod_clase)
            glPETType = ClearNull(aplRST!cod_tipo_peticion)
            '}
            xCorpLocal = ClearNull(aplRST!corp_local)
            xImportancia = ClearNull(aplRST!importancia_cod)
            xOrientacion = ClearNull(aplRST!cod_orientacion)
            xUsrLoginActual = ClearNull(aplRST!importancia_usr)
            xUsrPerfilActual = ClearNull(aplRST!importancia_prf)
            txtusrImportancia.Text = ClearNull(aplRST!importancia_usrname)
            txtUsualta.ToolTipText = ClearNull(aplRST!usualta_usrname)
            
            If Val(txtNroAnexada.Text) = 0 Then
                cmdVerAnexora.Enabled = False
            Else
                cmdVerAnexora.Enabled = True
            End If

            xDir = ClearNull(aplRST!cod_direccion)
            xSec = ClearNull(aplRST!cod_sector)
            xGer = ClearNull(aplRST!cod_gerencia)
            sSoli = ClearNull(aplRST!cod_solicitante)
            sRefe = ClearNull(aplRST!cod_referente)
            sSupe = ClearNull(aplRST!cod_supervisor)
            sDire = ClearNull(aplRST!cod_director)
            sUsualta = ClearNull(aplRST!cod_usualta)
            txtUsualta.Text = ClearNull(aplRST!cod_usualta)
            sBpar = ClearNull(aplRST!cod_bpar)
            aplRST.Close

            antBpar = sBpar
            
            If bCompleto = True Then
                Call Status("Cargando Descripcion")
                memDescripcion.Text = sp_GetMemo(glNumeroPeticion, "DESCRIPCIO")
                Call Status("Cargando Caracteristicas")
                memCaracteristicas.Text = sp_GetMemo(glNumeroPeticion, "CARACTERIS")
                Call Status("Cargando Motivos")
                memMotivos.Text = sp_GetMemo(glNumeroPeticion, "MOTIVOS")
                Call Status("Cargando Beneficios")
                memBeneficios.Text = sp_GetMemo(glNumeroPeticion, "BENEFICIOS")
                Call Status("Cargando Relaciones")
                memRelaciones.Text = sp_GetMemo(glNumeroPeticion, "RELACIONES")
                Call Status("Cargando Observaciones")
                memObservaciones.Text = sp_GetMemo(glNumeroPeticion, "OBSERVACIO")
                txObservaciones = memObservaciones.Text
                txBeneficios = memBeneficios.Text
                txCaracteristicas = memCaracteristicas.Text
                txDescripcion = memDescripcion.Text
                txMotivos = memMotivos.Text
                txRelaciones = memRelaciones.Text
            End If
        Else
            aplRST.Close
        End If
        
        ''INGERENCIA
        flgEjecutor = InEjecutor()
        flgBP = esBP()
        Select Case PerfilInternoActual
        Case "BPAR"
            If flgBP Then
                flgIngerencia = True
            End If
        Case "ADMI", "SADM"
            flgIngerencia = True
        Case "SOLI", "REFE", "AUTO", "SUPE"
            If xPerfNivel = "BBVA" Or _
                xPerfNivel = "DIRE" And xDir = xPerfDir Or _
                xPerfNivel = "GERE" And xGer = xPerfGer Or _
                xPerfNivel = "SECT" And xSec = xPerfSec Then
                flgIngerencia = True
                flgSolicitor = True
            End If
        'Se hacen preguntas especiales para los que estan en la rama ejecutora
        Case "CSEC"
            If InSector() Then
                flgIngerencia = True
            End If
        Case "CGRU"
            If InGrupo() Then
                flgIngerencia = True
            End If
        Case "CDIR", "CGCI"
            'esto esta harcodeado, por pedido de Milstein
            'estos dos perfiles pueden Modificar, (solo la VISIBILIDAD)
            flgIngerencia = True
        End Select
        
        Call InicializarCombos
        If xSec <> "" Then
            Call SetCombo(cboSector, xSec, True)
        End If
        If sSoli <> "" Then
            Call SetCombo(cboSolicitante, sSoli)
        End If
        If sRefe <> "" Then
            Call SetCombo(cboReferente, sRefe)
        End If
        If sSupe <> "" Then
            Call SetCombo(cboSupervisor, sSupe)
        End If
        If sDire <> "" Then
            Call SetCombo(cboDirector, sDire)
        End If
    
        Call SetCombo(cboBpar, sBpar)
    
        cboCorpLocal.ListIndex = PosicionCombo(cboCorpLocal, xCorpLocal, True)
        cboImportancia.ListIndex = PosicionCombo(cboImportancia, xImportancia, True)
        cboOrientacion.ListIndex = PosicionCombo(cboOrientacion, xOrientacion, True)
    End If
    Call puntero(False)
End Sub

Private Sub InicializarCombos()
    'Debug.Print "InicializarCombos"
    Dim xDescri As String
    
    '{ add -002- c. Carga din�mica de la clase de la petici�n seg�n el tipo de petici�n
    Select Case glLOGIN_Gerencia
        Case "ORG."
            ' Organizaci�n
            Select Case glModoPeticion
                Case "ALTA"
                    Select Case PerfilInternoActual
                        Case "CGRU", "CSEC"
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "PRO"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Correctivo                                                ||CORR"
                                        .AddItem "Optimizaci�n                                              ||OPTI"
                                        .ListIndex = 0
                                    End With
                                Case "ESP"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Atenci�n a usuario                                                ||ATEN"
                                        .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                                        .AddItem "Nuevo desarrollo                                              ||NUEV"
                                        .ListIndex = 0
                                    End With
                            End Select
                        Case Else
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "NOR"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "S/C (Sin clase)                                               ||SINC"
                                        .ListIndex = 0
                                    End With
                            End Select
                    End Select
                Case "MODI"
                    Select Case PerfilInternoActual
                        Case "CGRU", "CSEC"
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "PRO"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Correctivo                                                ||CORR"
                                        .AddItem "Optimizaci�n                                              ||OPTI"
                                        .ListIndex = 0
                                    End With
                                Case "ESP"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Atenci�n a usuario                                                ||ATEN"
                                        .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                                        .AddItem "Nuevo desarrollo                                              ||NUEV"
                                        .ListIndex = 0
                                    End With
                            End Select
                        Case "BPAR"
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "NOR"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Atenci�n a usuario                                                ||ATEN"
                                        .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                                        .AddItem "Nuevo desarrollo                                              ||NUEV"
                                        .ListIndex = 0
                                    End With
                                Case "ESP"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Atenci�n a usuario                                                ||ATEN"
                                        .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                                        .AddItem "Nuevo desarrollo                                              ||NUEV"
                                        .ListIndex = 0
                                    End With
                                Case "PRJ"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Mantenimiento evolutivo                                               ||EVOL"
                                        .AddItem "Nuevo desarrollo                                              ||NUEV"
                                        .ListIndex = 0
                                    End With
                            End Select
                        Case Else
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "NOR"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "S/C (Sin clase)                                               ||SINC"
                                        .ListIndex = 0
                                    End With
                            End Select
                    End Select
            End Select
        Case "DESA"
        ' Dise�o y Desarrollo
            Select Case glModoPeticion
                Case "ALTA"
                    Select Case PerfilInternoActual
                        Case "CGRU", "CSEC"
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "PRO"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Correctivo                                                ||CORR"
                                        .AddItem "Optimizaci�n                                              ||OPTI"
                                        .AddItem "SPUFI                                              ||SPUF"
                                        .ListIndex = 0
                                    End With
                                Case "ESP"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Atenci�n a usuario                                                ||ATEN"
                                        .ListIndex = 0
                                    End With
                            End Select
                    End Select
                Case "MODI"
                    Select Case PerfilInternoActual
                        Case "CGRU", "CSEC"
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "PRO"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Correctivo                                                ||CORR"
                                        .AddItem "Optimizaci�n                                              ||OPTI"
                                        .AddItem "SPUFI                                              ||SPUF"
                                        .ListIndex = 0
                                    End With
                                Case "ESP"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "Atenci�n a usuario                                                ||ATEN"
                                        .ListIndex = 0
                                    End With
                            End Select
                    End Select
            End Select
        Case Else
        ' Usuarios
            Select Case glModoPeticion
                Case "ALTA"
                    Select Case PerfilInternoActual
                        Case "SOLI"
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "NOR"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "S/C (Sin clase)                                               ||SINC"
                                        .ListIndex = 0
                                    End With
                            End Select
                    End Select
                Case "MODI"
                    Select Case PerfilInternoActual
                        Case "SOLI"
                            Select Case CodigoCombo(cboTipopet, True)
                                Case "NOR"
                                    With cmbPetClass
                                        .Clear
                                        .AddItem "S/C (Sin clase)                                               ||SINC"
                                        .ListIndex = 0
                                    End With
                            End Select
                    End Select
            End Select
    End Select
    '}
    
    cboImportancia.Clear
    If sp_GetImportancia(Null, Null) Then
        Do While Not aplRST.EOF
            cboImportancia.AddItem Trim(aplRST!nom_importancia) & Space(40) & "||" & Trim(aplRST!cod_importancia)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboOrientacion.Clear
    If sp_GetOrientacion(Null, Null) Then
        Do While Not aplRST.EOF
            cboOrientacion.AddItem Trim(aplRST!nom_orientacion) & Space(40) & "||" & Trim(aplRST!cod_orientacion)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboBpar.Clear
    If sp_GetRecursoPerfil(Null, "BPAR") Then
        Do While Not aplRST.EOF
            cboBpar.AddItem Trim(aplRST!cod_recurso) & " :  " & Trim(aplRST!nom_recurso)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    Select Case glModoPeticion
        Case "MODI"
            Select Case PerfilInternoActual
                Case "SADM"
                    'carga todo para que pueda modificar cuelquier cosa
                    Call CargaCombosConTodo
                Case "CSEC", "CGRU"
                    If CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ" Then
                        'primero cargo lo que figura en la peticion
                        Call CargaCombosConValor
                        'cargo lo que depende del perfil
                        Call CargaSectoresPosibles
                    Else
                        'solo cargo lo que figura en la peticion
                        Call CargaCombosConValor
                    End If
                Case Else
                    'solo cargo lo que figura en la peticion
                    Call CargaCombosConValor
                End Select
        Case "ALTA"
            Select Case PerfilInternoActual
                Case "SADM"
                    'carga todo para que pueda modificar cuelquoier cosa
                    Call CargaCombosConTodo
                Case "SOLI"
                    'cargo lo que depende del perfil
                    Call CargaSectoresPosibles
                    If sp_GetRecurso(sSoli, "R") Then
                        If Not aplRST.EOF Then
                            cboSolicitante.AddItem aplRST(0) & ": " & aplRST(1)
                        End If
                        aplRST.Close
                    End If
                    cboSolicitante.ListIndex = 0
                Case "CSEC", "CGRU"
                    If CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ" Then
                        'cargo lo que depende del perfil
                        Call CargaSectoresPosibles
                        cboSolicitante.Clear
                        cboReferente.Clear
                        cboDirector.Clear
                        cboSupervisor.Clear
                        If sp_GetRecurso(sSoli, "R") Then
                            If Not aplRST.EOF Then
                                cboSolicitante.AddItem aplRST(0) & ": " & aplRST(1)
                                cboReferente.AddItem aplRST(0) & ": " & Trim(aplRST(1))
                                cboDirector.AddItem aplRST(0) & ": " & Trim(aplRST(1))
                            End If
                            aplRST.Close
                        End If
                        cboSolicitante.ListIndex = 0
                        cboReferente.ListIndex = 0
                        cboDirector.ListIndex = 0
                    Else
                        'todo
                        Call CargaCombosConTodo
                    End If
            End Select
            Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))
        Case "VIEW"
            'solo cargo lo que figura en la peticion
            Call CargaCombosConValor
            Call SetCombo(cboBpar, CodigoCombo(cboSector, True))
    End Select
    Call Status("Listo")
End Sub

Sub CargaSectoresPosibles()
    'Debug.Print "CargaSectoresPosibles"
    Dim xDescri As String
    Call Status("Cargando Sectores")
    cboSector.Clear
    If sp_GetSectorXt(xPerfSec, xPerfGer, xPerfDir) Then
        Do While Not aplRST.EOF
            cboSector.AddItem Trim(aplRST!nom_direccion) & ": " & Trim(aplRST!nom_gerencia) & ": " & Trim(aplRST!nom_sector) & Space(80) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    If cboSector.ListCount = 1 Then
        cboSector.ListIndex = 0
    Else
        Call SetCombo(cboSector, xPerfSec, True)
    End If
End Sub

Sub CargaIntervinientes()
    'Debug.Print "CargaIntervinientes"
    Dim sSec, sGer, sDir As String
    cboSolicitante.Clear
    cboReferente.Clear
    cboDirector.Clear
    cboSupervisor.Clear
    Call Status("Cargando Solicitantes")
        
    If PerfilInternoActual <> "SADM" Then
        sSec = CodigoCombo(Me.cboSector, True)
        If sp_GetSectorXt(sSec, Null, Null) Then
            sDir = ClearNull(aplRST!cod_direccion)
            sGer = ClearNull(aplRST!cod_gerencia)
            aplRST.Close
        End If
        If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "SOLI") Then
            Do While Not aplRST.EOF
                cboSolicitante.AddItem aplRST!cod_recurso & ": " & aplRST!nom_recurso
                aplRST.MoveNext
            Loop
            aplRST.Close
        End If
        If cboSolicitante.ListCount > 0 Then
            cboSolicitante.ListIndex = 0
        End If
    Else
        If sp_GetRecurso(Null, "R") Then
            Do While Not aplRST.EOF
                cboSolicitante.AddItem aplRST(0) & ": " & aplRST(1)
                cboReferente.AddItem aplRST(0) & ": " & Trim(aplRST(1))
                cboDirector.AddItem aplRST(0) & ": " & Trim(aplRST(1))
                cboSupervisor.AddItem aplRST(0) & ": " & Trim(aplRST(1))
                aplRST.MoveNext
            Loop
            aplRST.Close
        End If
        cboSolicitante.ListIndex = -1
        cboReferente.ListIndex = -1
        cboDirector.ListIndex = -1
        cboSupervisor.ListIndex = -1
    End If

    If sSoli <> "" Then
        Call SetCombo(cboSolicitante, sSoli)
    End If
    If sRefe <> "" Then
        Call SetCombo(cboReferente, sRefe)
    End If
    If sSupe <> "" Then
        Call SetCombo(cboSupervisor, sSupe)
    End If
    If sDire <> "" Then
        Call SetCombo(cboDirector, sDire)
    End If

    Call Status("")

    '''    Call Status("Cargando Referentes")
    '''    If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "REFE") Then
    '''        Do While Not aplRST.EOF
    '''            cboReferente.AddItem aplRST!cod_recurso & ": " & aplRST!nom_recurso
    '''            aplRST.MoveNext
    '''        Loop
    '''        aplRST.Close
    '''    End If
    '''    Call Status("Cargando Autorizante")
    '''
    '''    If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "DIRE") Then
    '''        Do While Not aplRST.EOF
    '''            cboDirector.AddItem aplRST!cod_recurso & ": " & aplRST!nom_recurso
    '''            aplRST.MoveNext
    '''        Loop
    '''        aplRST.Close
    '''    End If
    '''    Call Status("Cargando Supervisor")
    '''    If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "SUPE") Then
    '''        Do While Not aplRST.EOF
    '''            cboSupervisor.AddItem aplRST!cod_recurso & ": " & aplRST!nom_recurso
    '''            aplRST.MoveNext
    '''        Loop
    '''        aplRST.Close
    '''    End If
End Sub
Sub CargaCombosConTodo()
'    Debug.Print "CargaCombosConTodo"
    Dim xDescri As String
    Call Status("Cargando Sectores")
    cboSector.Clear
    If sp_GetSectorXt(Null, Null, Null) Then
        Do While Not aplRST.EOF
            cboSector.AddItem Trim(aplRST!nom_direccion) & ": " & Trim(aplRST!nom_gerencia) & ": " & Trim(aplRST!nom_sector) & Space(80) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
'''    cboSector.ListIndex = 0
    If xSec <> "" Then
        Call SetCombo(cboSector, xSec, True)
    End If
    Call CargaIntervinientes
End Sub

Sub CargaCombosConValor()
'    Debug.Print "CargaCombosConValor"
    Dim xDescri As String
    
    cboSector.Clear
            
    If sSoli <> "" Then
        Call Status("Cargando Solicitante")
        cboSolicitante.Clear
        If sp_GetRecurso(sSoli, "R") Then
            xDescri = sSoli & " : " & ClearNull(aplRST(1))
            aplRST.Close
        Else
            xDescri = sSoli & " : " & "Error Solicitante"
        End If
        cboSolicitante.AddItem xDescri
        cboSolicitante.ListIndex = 0
    End If
    If sRefe <> "" Then
        Call Status("Cargando Referente")
        cboReferente.Clear
        If sp_GetRecurso(sRefe, "R") Then
            xDescri = sRefe & " : " & ClearNull(aplRST(1))
            aplRST.Close
        Else
            xDescri = sRefe & " : " & "Error Referente"
        End If
        cboReferente.AddItem xDescri
        cboReferente.ListIndex = 0
    End If
    If sSupe <> "" Then
        Call Status("Cargando Supervisor")
        cboSupervisor.Clear
        If sp_GetRecurso(sSupe, "R") Then
            xDescri = sSupe & " : " & ClearNull(aplRST(1))
            aplRST.Close
        Else
            xDescri = sSupe & " : " & "Error Supervisor"
        End If
        cboSupervisor.AddItem xDescri
        cboSupervisor.ListIndex = 0
    End If
    If sDire <> "" Then
        Call Status("Cargando Director")
        cboDirector.Clear
        If sp_GetRecurso(sDire, "R") Then
            xDescri = sDire & " : " & ClearNull(aplRST(1))
            aplRST.Close
        Else
            xDescri = sDire & " : " & "Error Director"
        End If
        cboDirector.AddItem xDescri
        cboDirector.ListIndex = 0
    End If
    cboSector.Clear
    If sp_GetSectorXt(xSec, xGer, xDir) Then
        If Not aplRST.EOF Then
            cboSector.AddItem Trim(aplRST!nom_direccion) & ": " & Trim(aplRST!nom_gerencia) & ": " & Trim(aplRST!nom_sector) & Space(80) & "||" & aplRST(0)
            cboSector.ListIndex = 0
        End If
        aplRST.Close
    End If
End Sub

Private Sub CargaAnexas()
    anxNroPeticion = ""
    cmdVerAnexa.Enabled = False
    With grdAnexas
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colNroInterno) = "NroInt."
        .TextMatrix(0, colNroAsignado) = "Petici�n"
        .TextMatrix(0, colTitulo) = "T�tulo"
        .colWidth(colNroAsignado) = 800
        .colWidth(colTitulo) = 6000
        .colWidth(colNroInterno) = 0
        .ColAlignment(colTitulo) = 0
    End With
    'truchada solo para mostrar
    Call Status("Cargando Anexas")
    If glNumeroPeticion <> "" Then
        If Not sp_GetPeticionAnexadas(glNumeroPeticion) Then
            aplRST.Close
            Call Status("")
            Exit Sub
        End If
    Else
          Exit Sub
    End If
    Do While Not aplRST.EOF
       ' If ClearNull(aplRST!cod_situacion) = "OPINOK" Then
       ' MsgBox ("hola")
       ' End If
        With grdAnexas
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!titulo)
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    '{ add -002- d.
    Dim i As Long
    
    With grdAnexas
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
    End With
    '}
    If grdAnexas.Rows > 1 Then
        cmdVerAnexa.Enabled = True
    End If
    Call Status("")
End Sub
Private Sub anxSeleccion()
    With grdAnexas
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                anxNroPeticion = .TextMatrix(.RowSel, colNroInterno)
            End If
        End If
    End With
End Sub
Private Sub grdAnexas_Click()
    Call anxSeleccion
End Sub

'{ add -002- d.
Private Sub grdAnexas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdAnexas_Click
    End If
End Sub
'}

Private Sub memBeneficios_Change()
    bFlgBeneficios = True
End Sub
Private Sub memCaracteristicas_Change()
    bFlgCaracteristicas = True
End Sub
Private Sub memDescripcion_Change()
    bFlgDescripcion = True
End Sub
Private Sub memMotivos_Change()
bFlgMotivos = True
End Sub
Private Sub memObservaciones_Change()
    bFlgObservaciones = True
End Sub
Private Sub memRelaciones_Change()
    bFlgRelaciones = True
End Sub
Private Function modifImportancia(oUsualta, oldPerf, nUsualta, newPerf) As Boolean
    modifImportancia = False
    If oldPerf = "" Then
        modifImportancia = True
    End If
    If newPerf = "ADMI" Or newPerf = "SADM" Then
        modifImportancia = True
    End If
    If oUsualta = nUsualta Then
        modifImportancia = True
    End If
    Select Case oldPerf
    Case "ADMI"
        modifImportancia = True
    Case "CGRU"
        modifImportancia = True
    Case "CSEC"
        If InStr("CGCI|CDIR", newPerf) > 0 Then
            modifImportancia = True
        End If
    Case "CGCI"
        If InStr("CDIR", newPerf) > 0 Then
            modifImportancia = True
        End If
    Case "CDIR"
        If newPerf = "CDIR" Then
            modifImportancia = True
        End If
    End Select
End Function

Private Function InGrupo() As Boolean
    InGrupo = False
    If Not sp_GetPeticionGrupo(glNumeroPeticion, Null, Null) Then Exit Function
    Do While Not aplRST.EOF
        If xPerfNivel = "BBVA" Or _
            xPerfNivel = "DIRE" And ClearNull(aplRST!cod_direccion) = xPerfDir Or _
            xPerfNivel = "GERE" And ClearNull(aplRST!cod_gerencia) = xPerfGer Or _
            xPerfNivel = "SECT" And ClearNull(aplRST!cod_sector) = xPerfSec Or _
            xPerfNivel = "GRUP" And ClearNull(aplRST!cod_grupo) = xPerfGru Then
            InGrupo = True
        End If
        aplRST.MoveNext
    Loop
    aplRST.Close
End Function
Private Function InSector() As Boolean
    InSector = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If xPerfNivel = "BBVA" Or _
            xPerfNivel = "DIRE" And ClearNull(aplRST!cod_direccion) = xPerfDir Or _
            xPerfNivel = "GERE" And ClearNull(aplRST!cod_gerencia) = xPerfGer Or _
            xPerfNivel = "SECT" And ClearNull(aplRST!cod_sector) = xPerfSec Then
            InSector = True
        End If
        aplRST.MoveNext
    Loop
    aplRST.Close
End Function
Private Function InEjecutor() As Boolean
    InEjecutor = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If ClearNull(aplRST!cod_sector) = glLOGIN_Sector Then
            InEjecutor = True
        End If
        aplRST.MoveNext
    Loop
    aplRST.Close
End Function

Private Function esBP() As Boolean
    esBP = False
    If PerfilInternoActual = "BPAR" Then
        If sBpar = Trim(glLOGIN_ID_REEMPLAZO) Then      ' add -001- h. - Se agrega la funci�n Trim a glLOGIN_ID_REEMPLAZO
            esBP = True
        Else
            'MsgBox ("CUIDADO")
            PerfilInternoActual = "VIEW"
        End If
    End If
End Function

Private Sub cmdRptHs_Click()
    Dim txtAux As String
    Dim sTitulo As String
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call puntero(True)
    
    'se fuerza por parnalla
    mdiPrincipal.CrystalReport1.Destination = crptToWindow
    
    'mdiPrincipal.CrystalReport1.Connect = "DSN=" & mdiPrincipal.AdoConnection.Servidor & ";SRVR=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
    'mdiPrincipal.CrystalReport1.UserName = mdiPrincipal.AdoConnection.UsuarioAplicacion
    'mdiPrincipal.CrystalReport1.Password = mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad
    'xRet = mdiPrincipal.CrystalReport1.LogOnServer("PdSODBC.DLL", "CRREP", "atr", "E037799", "123456pwddif")
    mdiPrincipal.CrystalReport1.ReportFileName = App.Path & "\" & "hstrapeta.rpt"
    mdiPrincipal.CrystalReport1.Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
    
    'DSN = name;UID = userID;PWD = password;DSQ = database qualifier

    mdiPrincipal.CrystalReport1.SelectionFormula = ""
    mdiPrincipal.CrystalReport1.RetrieveStoredProcParams
    
    'se fuerza la fecha
    mdiPrincipal.CrystalReport1.StoredProcParam(0) = "19000101"
    mdiPrincipal.CrystalReport1.StoredProcParam(1) = "20100101"
    
    mdiPrincipal.CrystalReport1.StoredProcParam(2) = "PET"
    mdiPrincipal.CrystalReport1.StoredProcParam(3) = "" & glNumeroPeticion
    sTitulo = "Petici�n: " & ClearNull(txtNroAsignado.Text)
    'se fuerza el nivel: recurso
    mdiPrincipal.CrystalReport1.StoredProcParam(4) = 1



    mdiPrincipal.CrystalReport1.Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
    mdiPrincipal.CrystalReport1.Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
    mdiPrincipal.CrystalReport1.Formulas(2) = "@0_TITULO='" & sTitulo & "'"
'    mdiPrincipal.CrystalReport1.Connect = "DSN=" & mdiPrincipal.SybaseLogIn.DsnAplicacion & ";UID=" & mdiPrincipal.SybaseLogIn.UserAplicacion & ";PWD=" & mdiPrincipal.SybaseLogIn.PasswordAplicacion & mdiPrincipal.SybaseLogIn.ComponenteSI & ";SRVR=" & mdiPrincipal.SybaseLogIn.ServerSI & ";DB=" & mdiPrincipal.SybaseLogIn.BaseSI
    mdiPrincipal.CrystalReport1.WindowLeft = 1
    mdiPrincipal.CrystalReport1.WindowTop = 1
    mdiPrincipal.CrystalReport1.WindowState = crptMaximized
   
    mdiPrincipal.CrystalReport1.Action = 1
    mdiPrincipal.CrystalReport1.Formulas(2) = ""
    
    Call puntero(False)
    Call LockProceso(False)
   
   'Call cmdCancelar_Click
End Sub

' --------------------------------------------------------------
' --------------------------------------------------------------
' --------------------------------------------------------------
Private Sub CargarDocMetod()
    docView.Enabled = False
    docDel.Enabled = False
    docMod.Enabled = False
    docAdd.Enabled = False

    With grdDocMetod
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, 0) = ""
        .TextMatrix(0, 1) = "Archivo"
        .TextMatrix(0, 2) = "Path"
        .TextMatrix(0, 3) = "Usuario"
        .TextMatrix(0, 4) = "Fecha"
        .TextMatrix(0, 5) = "Comentario"
        .colWidth(0) = 0
        .colWidth(1) = 3500
        .colWidth(2) = 0
        .colWidth(3) = 2000
        .colWidth(4) = 980
        .colWidth(5) = 5000
        .ColAlignment(1) = 0
        .ColAlignment(5) = 0
    End With

    Call Status("Cargando documentos vinculados...")

    If glNumeroPeticion <> "" Then
        If Not sp_GetAdjunto(glNumeroPeticion, Null) Then
            aplRST.Close
            GoTo finAdj
            Exit Sub
        End If
    Else
          Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdDocMetod
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST!adj_file)
            .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!adj_path)
            .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!nom_audit)
            .TextMatrix(.Rows - 1, 4) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST!adj_texto)
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    '{ add -002- d.
    Dim i As Long
    
    With grdDocMetod
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
    End With
    '}
finAdj:
    Call Status("")
    Call docSeleccion

End Sub
Private Sub docSeleccion()
    Call setHabilCtrl(docFile, "DIS")
    Call setHabilCtrl(docPath, "DIS")
    Call setHabilCtrl(docComent, "DIS")

    With grdDocMetod
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If flgEjecutor Then
           docAdd.Enabled = True
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 1) <> "" Then
                glPathDocMetod = .TextMatrix(.RowSel, 2) & .TextMatrix(.RowSel, 1)
                docFile = .TextMatrix(.RowSel, 1)
                docPath = .TextMatrix(.RowSel, 2)
                docComent = .TextMatrix(.RowSel, 5)

                docView.Enabled = True
                If flgEjecutor Then
                   docMod.Enabled = True
                   docDel.Enabled = True
                End If

            End If
        End If
    End With
End Sub
Private Sub grdDocMetod_Click()
    Call docSeleccion
End Sub

'{ add -002- d.
Private Sub grdDocMetod_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDocMetod_Click
    End If
End Sub
'}

Private Sub grdDocMetod_DblClick()
    Call docSeleccion
    DoEvents
    If ClearNull(glPathDocMetod) = "" Then
        Exit Sub
    End If
    AbrirAdjunto glPathDocMetod
End Sub

Private Sub AbrirAdjunto(xFilename)
    Dim lRetCode As Long
    lRetCode = ShellExecute(Me.HWnd, "open", xFilename, "", "C:\", 1)
    If lRetCode <= 31 Then MsgBox "Error al Abrir el Documento"
End Sub
Private Sub docView_Click()
    If ClearNull(glPathDocMetod) = "" Then
        Exit Sub
    End If
    AbrirAdjunto glPathDocMetod
End Sub
Private Sub docSel_Click()
    Dim sPath As String
    selFile.prpPattern = "*.xls;*.doc;*.pps;*.mpp;*.ppt;*.rtf;*.pdf;*.zip;*.msg"
   ' selFile.prpDrive = glWRKDIR
   ' selFile.prpPath = glWRKDIR
    sPath = ""
    If sp_GetVarios("PTHADJ_1") Then
        sPath = ClearNull(aplRST!var_texto)
    End If
    If sp_GetVarios("PTHADJ_2") Then
        sPath = sPath & ClearNull(aplRST!var_texto)
    End If
    If sp_GetVarios("PTHADJ_3") Then
        sPath = sPath & ClearNull(aplRST!var_texto)
    End If
    selFile.prpNetwork = True
    'selFile.prpDrive = ""
    If ClearNull(antPathDocMetod) = "" Then
        antPathDocMetod = sPath
    End If
    If InStr(antPathDocMetod, sPath) = 0 Then
        antPathDocMetod = antPathDocMetod
    End If
    selFile.prpPath = antPathDocMetod
    selFile.Show vbModal
    If selFile.prpPathFile = "" Then
        Exit Sub
    End If
    If InStr(selFile.prpPath, sPath) = 0 Then
        MsgBox ("No puede adjuntar elementos que no esten en el path permitido.")
        Exit Sub
    End If
    If Len(selFile.prpFile) > 50 Then
        MsgBox ("El nombre del archivo no puede exceder los 50 caracteres.")
        Exit Sub
    End If
    If Len(selFile.prpPath) > 250 Then
        MsgBox ("El path no puede exceder los 50 caracteres.")
        Exit Sub
    End If
    If Dir(selFile.prpPathFile) = "" Then
        MsgBox ("Archivo inexistente")
        Exit Sub
    End If
    antPathDocMetod = selFile.prpPath
    docFile.Text = selFile.prpFile
    docPath.Text = selFile.prpPath
    docComent.SetFocus
End Sub
Private Sub docCan_Click()
    docFile = "": docPath = "": docComent = ""
    Call docHabBtn(0, "X")
End Sub

Private Sub docCon_Click()
    If ClearNull(docFile.Text) = "" Or ClearNull(docPath.Text) = "" Then
        docFile = "": docPath = "": docComent = ""
        MsgBox ("No se ha seleccionado archivo.")
        Exit Sub
    End If
    Select Case docOpcion
        Case "A"
            If sp_InsertAdjunto(glNumeroPeticion, docFile.Text, docPath.Text, docComent.Text) Then
                Call docHabBtn(0, "X")
            End If
        Case "M"
            If sp_UpdateAdjunto(glNumeroPeticion, docFile.Text, docComent.Text) Then
                With grdDocMetod
                    If .RowSel > 0 Then
                       .TextMatrix(.RowSel, 5) = docComent.Text
                    End If
                End With
                Call docHabBtn(0, "X", False)
            End If
        Case "E"
            If sp_DeleteAdjunto(glNumeroPeticion, docFile.Text) Then
            docFile = "": docPath = "": docComent = ""
                Call docHabBtn(0, "X")
            End If
        Case Else
            docFile = "": docPath = "": docComent = ""
            Call docHabBtn(0, "X")
    End Select
End Sub

Private Sub docDel_Click()
    Call docHabBtn(1, "E")
End Sub

Private Sub docMod_Click()
    Call docHabBtn(1, "M")
End Sub

Private Sub docAdd_Click()
    Call docHabBtn(1, "A")
    Call docSel_Click
End Sub

Sub docHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjADJ).Enabled = True
            orjBtn(orjCNF).Enabled = True
            lblDocMetod = "": lblDocMetod.Visible = False
            grdDocMetod.Enabled = True
            grdDocMetod.SetFocus
            docView.Enabled = False
            docAdd.Enabled = False
            If flgEjecutor Then
               docAdd.Enabled = True
            End If
            docMod.Enabled = False
            docDel.Enabled = False
            docCon.Enabled = False
            docCan.Enabled = False
            docSel.Enabled = False
            'sOpcionSeleccionada = ""
            docComent.Locked = True

            If IsMissing(vCargaGrid) Then
                Call CargarDocMetod
            Else
                Call docSeleccion
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjADJ).Enabled = False
            orjBtn(orjCNF).Enabled = False
            fraButtPpal.Enabled = False

            grdDocMetod.Enabled = False
            docAdd.Enabled = False
            docMod.Enabled = False
            docDel.Enabled = False
            docCon.Enabled = True
            docCan.Enabled = True
            Select Case sOpcionSeleccionada
                Case "A"
                    docSel.Enabled = True
                    lblDocMetod = " AGREGAR ": lblDocMetod.Visible = True
                    docFile = "": docPath = "": docComent = ""
                    docComent.Locked = False
                    fraDocMet.Enabled = True
                    Call setHabilCtrl(docComent, "NOR")
                Case "M"
                    lblDocMetod = " MODIFICAR ": lblDocMetod.Visible = True
                    fraDocMet.Enabled = True
                    docComent.Locked = False
                    Call setHabilCtrl(docComent, "NOR")
                    docComent.SetFocus
                Case "E"
                    lblDocMetod = " DESVINCULAR ": lblDocMetod.Visible = True
                    fraDocMet.Enabled = False
            End Select
    End Select
    docOpcion = sOpcionSeleccionada
End Sub

Private Sub adjCan_Click()
    adjFile = "": adjPath = "": adjComent = ""
    Call adjHabBtn(0, "X")
End Sub

Private Sub adjAdd_Click()
    Call adjHabBtn(1, "A")
    Call adjSel_Click
End Sub

Private Sub adjMod_Click()
    Call adjHabBtn(1, "M")
End Sub

Private Sub adjDel_Click()  ' Desadjuntar un archivo adjunto
    Call adjHabBtn(1, "E")
End Sub

Private Sub adjCon_Click()
    If ClearNull(adjFile.Text) = "" Then
        adjFile = "": adjPath = "": adjComent = ""
        MsgBox ("No se ha seleccionado archivo.")
        Exit Sub
    End If
    If cboAdjClass.ListIndex < 0 Then
        MsgBox "Falta Especificar Tipo de Archivo", vbOKOnly + vbInformation
        Exit Sub
    End If
    
    '{ add -002- g. - Verifica que exista el c�digo de documento seleccionado en la cadena del nombre del archivo
    If InStr(1, Trim(adjFile.Text), CodigoCombo(cboAdjClass, False), vbTextCompare) = 0 Then
        MsgBox "El documento a adjuntar debe contener la clave " & Trim(UCase(CodigoCombo(cboAdjClass, False))) & " dentro del nombre del archivo.", vbOKOnly + vbInformation
        Exit Sub
    End If
    '}
    
    If PerfilInternoActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
        If CodigoCombo(cboAdjClass, False) = "ALCA" Then     ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
            MsgBox "Un 'Doc. de alcance desarrollo'," & Chr(13) & "solo puede adjuntarse" & Chr(13) & "por usuarios de DyD", vbOKOnly + vbInformation
            Exit Sub
        End If
        If CodigoCombo(cboAdjClass, False) = "T700" Then     ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
            MsgBox "Un 'Plan de Implantaci�n Desarrollo'," & Chr(13) & "solo puede adjuntarse" & Chr(13) & "por usuarios de DyD", vbOKOnly + vbInformation
            Exit Sub
        End If
    End If
    
    Select Case adjOpcion
        Case "A"
            If MsgBox(constConfirmMsgAttachDocumentos, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then     ' add -002- e.
                If sp_InsertPeticionAdjunto(glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjPath.Text, adjFile.Text, adjComent.Text) Then  ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
                    Call sp_AddHistorial(glNumeroPeticion, "ADJADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboAdjClass) & "-->" & adjFile.Text)
                    If CodigoCombo(cboAdjClass, False) = "ALCA" Then        ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
                        Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
                    End If
                    Call adjHabBtn(0, "X")
                    adjPath.Text = ""
                End If
            End If      ' add -002- e.
        Case "E"
            If MsgBox(constConfirmMsgDeAttachDocumentos, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then       ' add -002- e.
                If sp_DeletePeticionAdjunto(glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjFile.Text) Then    ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
                    Call sp_AddHistorial(glNumeroPeticion, "ADJDEL", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboAdjClass) & "-->" & adjFile.Text)
                    If CodigoCombo(cboAdjClass, False) = "ALCA" Or CodigoCombo(cboAdjClass, False) = "C100" Or CodigoCombo(cboAdjClass, False) = "P950" Then         ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
                        'verifica si no existen mas documentos de alcance
                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA") Then
                            Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
                        Else
                            aplRST.Close
                        End If
                        '{ add -001- i.
                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100") Then
                            Call sp_DeletePeticionConf(glNumeroPeticion, "C100", "REQ", "1")
                        Else
                            aplRST.Close
                        End If
                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950") Then
                            Call sp_DeletePeticionConf(glNumeroPeticion, "P950", "REQ", "1")
                        Else
                            aplRST.Close
                        End If
                        '}
                    End If
                    adjFile.Text = "": adjPath.Text = "": adjComent.Text = ""
                    Call adjHabBtn(0, "X")
                End If
            End If      ' add -002- e.
        Case Else
            adjFile.Text = "": adjPath.Text = "": adjComent.Text = ""
            Call adjHabBtn(0, "X")
    End Select
End Sub

Private Sub adjSel_Click()
    Dim sPath As String
    selFile.prpPattern = "*.xls;*.doc;*.rtf;*.pps;*.mpp;*.ppt;*.tif;*.bmp;*.jpg;*.htm;*.html;*.txt;*.pdf;*.zip;*.msg"
    sPath = ""
    selFile.prpNetwork = False
    If ClearNull(antPathAdjPet) = "" Then
        antPathAdjPet = sPath
    End If
    selFile.prpPath = antPathAdjPet
    selFile.Show vbModal
    If selFile.prpPathFile = "" Then
        Exit Sub
    End If
    If Len(selFile.prpFile) > 100 Then
        MsgBox ("El nombre del archivo no puede exceder los 100 caracteres.")
        Exit Sub
    End If
    If Len(selFile.prpPath) > 250 Then
        MsgBox ("El path no puede exceder los 250 caracteres.")
        Exit Sub
    End If
    If Dir(selFile.prpPathFile) = "" Then
        MsgBox ("Archivo inexistente")
        Exit Sub
    End If
    If SizeFile(selFile.prpPathFile) > 2000000 Then
        MsgBox ("El tama�o del archivo excede lo aceptable por la aplicaci�n")
        Exit Sub
    End If

    antPathAdjPet = selFile.prpPath
    adjFile.Text = selFile.prpFile
    adjPath.Text = selFile.prpPath
    'adjComent.SetFocus     ' del -001- h.
    cboAdjClass.SetFocus    ' add -001- h.
End Sub

Private Sub CargarAdjPet()
    adjView.Enabled = False
    adjDel.Enabled = False
    adjMod.Enabled = False
    adjAdd.Enabled = False

    With grdAdjPet
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, 0) = ""
        .TextMatrix(0, 1) = "Tipo"
        .TextMatrix(0, 2) = "Archivo"
        .TextMatrix(0, 3) = "Usuario"
        .TextMatrix(0, 4) = "Fecha"
        .TextMatrix(0, 5) = "Comentario"
        .colWidth(0) = 0
        .colWidth(1) = 900
        .colWidth(2) = 3500
        .colWidth(3) = 2000
        .colWidth(4) = 980
        .colWidth(5) = 5000
        .ColAlignment(2) = 0
        .ColAlignment(5) = 0
    End With

    Call Status("Cargando documentos adjuntos...")

    If glNumeroPeticion <> "" Then
        If Not sp_GetAdjuntosPet(glNumeroPeticion, Null) Then
            aplRST.Close
            GoTo finAdjp
            Exit Sub
        End If
    Else
          Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdAdjPet
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST!audit_user)
            .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST!adj_tipo)
            .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!adj_file)
            .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!nom_audit)
            .TextMatrix(.Rows - 1, 4) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST!adj_texto)
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    '{ add -002- d.
    Dim i As Long
    
    With grdAdjPet
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
    End With
    '}
finAdjp:
    Call Status("")
    Call adjSeleccion

End Sub

Private Sub adjSeleccion()
    Call setHabilCtrl(adjFile, "DIS")
    Call setHabilCtrl(adjPath, "DIS")
    Call setHabilCtrl(adjComent, "DIS")
    Call setHabilCtrl(cboAdjClass, "DIS")

    adjAdd.Enabled = False
    adjMod.Enabled = False
    adjDel.Enabled = False
    If PerfilInternoActual = "SADM" Or flgEjecutor Or flgSolicitor Then
       adjAdd.Enabled = True
    End If
    With grdAdjPet
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 1) <> "" Then
                glPathAdjPet = .TextMatrix(.RowSel, 2)
                '{ add -001- i.
                If glPeticion_sox001 = 1 Then
                    Call SetCombo(cboAdjClass, .TextMatrix(.RowSel, 1), False)
                Else
                '}
                    Call SetCombo(cboAdjClass, .TextMatrix(.RowSel, 1), True)
                End If      ' add -001- i.
                adjFile = .TextMatrix(.RowSel, 2)
                adjComent = .TextMatrix(.RowSel, 5)

                adjView.Enabled = True
                If PerfilInternoActual = "SADM" Or .TextMatrix(.RowSel, 0) = Trim(glLOGIN_ID_REEMPLAZO) Then    ' add -001- o. - Se agreg� la funci�n TRIM para evitar que no se habiliten las opciones de edici�n sobre los documentos
                   adjMod.Enabled = True
                   adjDel.Enabled = True
                End If
            End If
        End If
    End With
End Sub

Private Sub adjView_Click()
    DoEvents
    If ClearNull(glPathAdjPet) = "" Then
        Exit Sub
    End If
    auxMensaje.prpTexto = "ATENCION" & Chr(13) & Chr(13) & Chr(13) & "Si realiza modificaciones al documento" & Chr(13) & "no ser�n reflejadas en el adjunto"
    auxMensaje.prpSegundos = 3000
    auxMensaje.Disparar
    PeticionAdjunto2File glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjFile.Text, glWRKDIR       ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
    AbrirAdjunto glWRKDIR & adjFile.Text
End Sub

Private Sub grdAdjPet_Click()
    Call adjSeleccion
End Sub

'{ add -002- d.
Private Sub grdAdjPet_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdAdjPet_Click
    End If
End Sub
'}

Private Sub grdAdjPet_DblClick()
    Call adjSeleccion
    DoEvents
    If ClearNull(glPathAdjPet) = "" Then
        Exit Sub
    End If
    auxMensaje.prpTexto = "ATENCION" & Chr(13) & Chr(13) & Chr(13) & "Si realiza modificaciones al documento" & Chr(13) & "no ser�n reflejadas en el adjunto"
    auxMensaje.prpSegundos = 3000
    auxMensaje.Disparar
    PeticionAdjunto2File glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjFile.Text, glWRKDIR       ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
    AbrirAdjunto glWRKDIR & adjFile.Text
End Sub

Sub adjHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjDOC).Enabled = True
            orjBtn(orjCNF).Enabled = True
            lblAdjPet = "": lblAdjPet.Visible = False
            grdAdjPet.Enabled = True
            grdAdjPet.SetFocus
            adjView.Enabled = False
            adjAdd.Enabled = False
            'ZZZZ
            'los de Organizacion tambien pueden adjuntar
            If PerfilInternoActual = "SADM" Or flgEjecutor Or flgSolicitor Or glLOGIN_Gerencia = "ORG." Then
               adjAdd.Enabled = True
            End If
            adjMod.Enabled = False
            adjDel.Enabled = False
            adjCon.Enabled = False
            adjCan.Enabled = False
            adjSel.Enabled = False
            'sOpcionSeleccionada = ""
            adjComent.Locked = True

            If IsMissing(vCargaGrid) Then
                Call CargarAdjPet
            Else
                Call adjSeleccion
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjDOC).Enabled = False
            orjBtn(orjCNF).Enabled = False
            fraButtPpal.Enabled = False

            grdAdjPet.Enabled = False
            adjAdd.Enabled = False
            adjMod.Enabled = False
            adjDel.Enabled = False
            adjCon.Enabled = True
            adjCan.Enabled = True
            adjView.Enabled = False
            Select Case sOpcionSeleccionada
                Case "A"
                    adjSel.Enabled = True
                    lblAdjPet = " AGREGAR ": lblAdjPet.Visible = True
                    adjFile = "": adjPath = "": adjComent = ""
                    adjComent.Locked = False
                    fraAdjPet.Enabled = True
                    Call setHabilCtrl(adjComent, "NOR")
                    Call setHabilCtrl(cboAdjClass, "NOR")
                    cboAdjClass.ListIndex = -1
                Case "M"
                    lblAdjPet = " MODIFICAR ": lblAdjPet.Visible = True
                    fraAdjPet.Enabled = True
                    adjComent.Locked = False
                    Call setHabilCtrl(adjComent, "NOR")
                    Call setHabilCtrl(cboAdjClass, "DIS")
                    adjComent.SetFocus
                Case "E"
                    lblAdjPet = " DESVINCULAR ": lblAdjPet.Visible = True
                    fraAdjPet.Enabled = False
            End Select
    End Select
    adjOpcion = sOpcionSeleccionada
End Sub

' --------------------------------------------------------------
' --------------------------------------------------------------
' --------------------------------------------------------------
Private Sub CargarPetConf()
    pcfDel.Enabled = False
''    pcfMod.Enabled = False
    pcfAdd.Enabled = False

    With grdPetConf
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, 0) = "Tipo"
        .TextMatrix(0, 1) = "Requerido"
        .TextMatrix(0, 2) = "Usuario"
        .TextMatrix(0, 3) = "Fecha"
        .TextMatrix(0, 4) = "Comentario"
        .colWidth(0) = 800
        .colWidth(1) = 800
        .colWidth(2) = 3000
        .colWidth(3) = 980
        .colWidth(4) = 5000
        .colWidth(5) = 0
        .colWidth(6) = 0
        .ColAlignment(0) = 0
        .ColAlignment(1) = 0
        .ColAlignment(4) = 0
        .ColAlignment(6) = 0
    End With

    Call Status("Cargando PetConf")

    If glNumeroPeticion <> "" Then
        If Not sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
            aplRST.Close
            GoTo finPcf
            Exit Sub
        End If
    Else
          Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdPetConf
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST!ok_tipo)
            .TextMatrix(.Rows - 1, 1) = IIf(ClearNull(aplRST!ok_modo) = "EXC", "No Req.", "")
            .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!nom_audit)
            .TextMatrix(.Rows - 1, 3) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST!ok_texto)
            .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST!ok_modo)
            .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST!ok_secuencia)
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    '{ add -002- a.
    Dim i As Long
    
    With grdPetConf
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
    End With
    '}
finPcf:
    Call Status("")
    Call pcfSeleccion
End Sub

Private Sub pcfSeleccion()
    Call setHabilCtrl(cboPcfTipo, "DIS")
    Call setHabilCtrl(pcfTexto, "DIS")
    Call setHabilCtrl(cboPcfModo, "DIS")

    If flgSolicitor Or PerfilInternoActual = "SADM" Or glLOGIN_Gerencia = "ORG." Then
        pcfAdd.Enabled = True
    '{ add -001- Si es responsable de Sector, de la direcci�n de Medios y la petici�n es propia, entonces debo adjuntar un Conforme
    ElseIf PerfilInternoActual = "CSEC" And glLOGIN_Direccion = "MEDIO" And glPETType = "PRO" Then
        pcfAdd.Enabled = True
    '}
    End If
    
    With grdPetConf
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 0) <> "" Then
                Call SetCombo(cboPcfTipo, .TextMatrix(.RowSel, 0), True)
                Call SetCombo(cboPcfModo, .TextMatrix(.RowSel, 5), True)
                pcfTexto = .TextMatrix(.RowSel, 4)
''                If flgSolicitor Or PerfilInternoActual = "SADM" Then
''                   pcfMod.Enabled = True
''                End If
                sPcfSecuencia = .TextMatrix(.RowSel, 6)
                If PerfilInternoActual = "SADM" Or PerfilInternoActual = "ADMI" Then
                   pcfDel.Enabled = True
                End If
            End If
        End If
    End With
End Sub
Private Sub grdPetConf_Click()
    Call pcfSeleccion
End Sub
Private Sub grdPetConf_DblClick()
    Call pcfSeleccion
    DoEvents
End Sub

'{ add -002- d.
Private Sub grdPetConf_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdPetConf_Click
    End If
End Sub
'}

Private Sub pcfCan_Click()
    pcfTexto = ""
    Call pcfHabBtn(0, "X")
End Sub

Private Sub pcfCon_Click()
    Dim sPcfTipo As String
    Dim sPcfModo As String
    
    If cboPcfTipo.ListIndex < 0 Then
        MsgBox "Falta Especificar Tipo de Conforme", vbOKOnly + vbInformation
        Exit Sub
    End If
    If cboPcfModo.ListIndex < 0 Then
        MsgBox "Falta Especificar Exenci�n", vbOKOnly + vbInformation
        Exit Sub
    End If
    
    sPcfTipo = CodigoCombo(cboPcfTipo, True)
    sPcfModo = CodigoCombo(cboPcfModo, True)
    
    ' Ok completo al documento de alcance
    If sPcfTipo = "ALCA" And sPcfModo = "REQ" Then
        If Not SOX_Documentos(glPETFechaEnvioAlBP) Then     ' add -001- i.
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA") Then
                MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                Exit Sub
            Else
                aplRST.Close
            End If
        '{ add -001- i.
        Else
            If glPETType = "PRJ" Or glPETImpTech = "S" Then      ' Proyectos o tiene impacto tecnol�gico
                If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100") Then
                    MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                    Exit Sub
                Else
                    aplRST.Close
                End If
            Else
                If glPETClass = "EVOL" Or glPETClass = "OPTI" Or glPETClass = "NUEV" Then
                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950") Then
                        MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                        Exit Sub
                    Else
                        aplRST.Close
                    End If
                End If
            End If
        End If
        '}
    End If
    ' OK parcial al documento de alcance
    If sPcfTipo = "ALCP" Then
        If Not SOX_Documentos(glPETFechaEnvioAlBP) Then     ' add -001- i.
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA") Then
                MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                Exit Sub
            Else
                aplRST.Close
            End If
        '{ add -001- i.
        Else
            If glPETType = "PRJ" Or glPETImpTech = "S" Then          ' Proyectos con impacto tecnol�gico
                If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100") Then
                    MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                    Exit Sub
                Else
                    aplRST.Close
                End If
            Else
                If glPETClass = "EVOL" Or glPETClass = "OPTI" Or glPETClass = "NUEV" Then
                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950") Then
                        MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                        Exit Sub
                    Else
                        aplRST.Close
                    End If
                End If
            End If
        End If
        '}
    End If
    
    'If (Not flgSolicitor) And PerfilInternoActual <> "SADM" And glLOGIN_Gerencia <> "ORG." Then        ' del -001- o.
    If Not (PerfilInternoActual = "CSEC" And glLOGIN_Direccion = "MEDIO" And glPETType = "PRO") Then    ' add -001- o.
        If (Not flgSolicitor) And PerfilInternoActual <> "SADM" And glLOGIN_Gerencia <> "ORG." Then
            If sPcfTipo = "TESP" Then
                MsgBox "Solo pueden dar ok Parciales de Prueba de Usuario" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
                Exit Sub
            End If
            If sPcfTipo = "TEST" Then
                MsgBox "Solo pueden dar ok de Prueba de Usuario" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
                Exit Sub
            End If
            If sPcfTipo = "ALCA" Then
                MsgBox "Solo pueden dar ok a Documentos de Alcance" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
                Exit Sub
            End If
            If sPcfTipo = "ALCP" Then
                MsgBox "Solo pueden dar ok Parciales a Documentos de Alcance" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
                Exit Sub
            End If
        End If
    End If  ' add -001- o.
    
    If sPcfTipo = "TEST" And PerfilInternoActual <> "SADM" And InStr("ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA", sEstado) = 0 Then
        MsgBox "El estado de la Petici�n" & Chr(13) & "no permite declarar un OK a Prueba de Usuario.", vbOKOnly + vbInformation
        Exit Sub
    End If
    
    If sPcfTipo = "TESP" And PerfilInternoActual <> "SADM" And InStr("ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA", sEstado) = 0 Then
        MsgBox "El estado de la Petici�n" & Chr(13) & "no permite declarar un OK a Prueba Parcial.", vbOKOnly + vbInformation
        Exit Sub
    End If
    
    Select Case pcfOpcion
        Case "A"
            If MsgBox(constConfirmMsgAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then      ' add -002- e.
                If sPcfTipo = "TESP" Then
                    sPcfModo = "EXC"
                    sPcfSecuencia = "0"
                ElseIf sPcfTipo = "ALCP" Then
                    sPcfModo = "EXC"
                    sPcfSecuencia = "0"
                Else
                    sPcfSecuencia = "1"
                End If
                If sp_InsertPeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, pcfTexto.Text, sPcfSecuencia) Then
                    If sPcfTipo = "ALCA" Then
                        Call sp_AddHistorial(glNumeroPeticion, "OKALCA", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                    End If
                    If sPcfTipo = "ALCP" Then
                        Call sp_AddHistorial(glNumeroPeticion, "OKALCA", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                    End If
                    If sPcfTipo = "TEST" Then
                        Call sp_AddHistorial(glNumeroPeticion, "OKTEST", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                    End If
                    If sPcfTipo = "TESP" Then
                        Call sp_AddHistorial(glNumeroPeticion, "OKTEST", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                    End If
                    Call pcfHabBtn(0, "X")
                End If
            End If      ' add -002- e.
        Case "M"
'''            If sp_UpdatePeticionConf(glNumeroPeticion, sPcfTipo, CodigoCombo(cboPcfModo, True), pcfTexto.Text) Then
'''                With grdPetConf
'''                    If .RowSel > 0 Then
'''                       .TextMatrix(.RowSel, 5) = pcfTexto.Text
'''                    End If
'''                End With
'''                Call pcfHabBtn(0, "X", False)
'''            End If
        Case "E"
            If MsgBox(constConfirmMsgDeAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then        ' add -002- e.
                If sp_DeletePeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, sPcfSecuencia) Then
                    If sPcfTipo = "ALCA" Then
                        Call sp_AddHistorial(glNumeroPeticion, "OKALCA", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "Eliminaci�n " & TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                    End If
                    If sPcfTipo = "TEST" Then
                        Call sp_AddHistorial(glNumeroPeticion, "OKTEST", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "Eliminaci�n " & TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                    End If
                    pcfTexto = ""
                    Call pcfHabBtn(0, "X")
                End If
            End If      ' add -002- e.
        Case Else
            pcfTexto = ""
            Call pcfHabBtn(0, "X")
    End Select
End Sub

Private Sub pcfAdd_Click()
    Call pcfHabBtn(1, "A")
End Sub

Private Sub pcfMod_Click()
    Call pcfHabBtn(1, "M")
End Sub

Private Sub pcfDel_Click()
    Call pcfHabBtn(1, "E")
End Sub

Sub pcfHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    Call setHabilCtrl(cboPcfModo, "DIS")
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjDOC).Enabled = True
            orjBtn(orjADJ).Enabled = True
            lblPetConf = "": lblPetConf.Visible = False
            grdPetConf.Enabled = True
            grdPetConf.SetFocus
            pcfAdd.Enabled = False
            ''If flgEjecutor Then
               pcfAdd.Enabled = False
            ''End If
''            pcfMod.Enabled = False
            pcfDel.Enabled = False
            pcfCon.Enabled = False
            pcfCan.Enabled = False
            'sOpcionSeleccionada = ""
            pcfTexto.Locked = True

            If IsMissing(vCargaGrid) Then
                Call CargarPetConf
            Else
                Call pcfSeleccion
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjDOC).Enabled = False
            orjBtn(orjADJ).Enabled = False
            fraButtPpal.Enabled = False

            grdPetConf.Enabled = False
            pcfAdd.Enabled = False
''            pcfMod.Enabled = False
            pcfDel.Enabled = False
            pcfCon.Enabled = True
            pcfCan.Enabled = True
            Select Case sOpcionSeleccionada
                Case "A"
                    lblPetConf = " AGREGAR ": lblPetConf.Visible = True
                    pcfTexto = ""
                    pcfTexto.Locked = False
                    fraConforme.Enabled = True
                    Call setHabilCtrl(pcfTexto, "NOR")
                    Call setHabilCtrl(cboPcfTipo, "NOR")
                    Call SetCombo(cboPcfModo, "REQ", True)
                    If PerfilInternoActual = "SADM" Then
                        Call setHabilCtrl(cboPcfModo, "NOR")
                    End If
                Case "M"
                    lblPetConf = " MODIFICAR ": lblPetConf.Visible = True
                    fraConforme.Enabled = True
                    pcfTexto.Locked = False
                    Call setHabilCtrl(pcfTexto, "NOR")
                    Call setHabilCtrl(cboPcfTipo, "DIS")
                    If PerfilInternoActual = "SADM" Then
                        Call setHabilCtrl(cboPcfModo, "NOR")
                    End If
                    pcfTexto.SetFocus
                Case "E"
                    lblPetConf = " DESVINCULAR ": lblPetConf.Visible = True
                    fraConforme.Enabled = False
            End Select
    End Select
    pcfOpcion = sOpcionSeleccionada
End Sub

'{ mov -001-
            '{ del -002- a.
            '''********** cuidado
            'If CodigoCombo(cboTipopet, True) = "NOR" Then
            '    cboTipopet.Clear
            '    cboTipopet.AddItem "Normal                                  ||NOR"
            '    cboTipopet.AddItem "Proyecto                                ||PRJ"
            '    cboTipopet.ListIndex = 0
            '    cmdCancel.Enabled = True
            '    cmdOk.Enabled = True
            '    DoEvents
            '    Call setHabilCtrl(cboTipopet, "NOR")
            '    Call setHabilCtrl(cboBpar, "NOR")
            '    '{ add -001- b.
            '    Call setHabilCtrl(cmbPetClass, "NOR")
            '    Call setHabilCtrl(cmbPetImpact, "NOR")
            '    '}
            'End If
            'If CodigoCombo(cboTipopet, True) = "PRO" Then
            '    cboTipopet.Clear
            '    cboTipopet.AddItem "Propia                                  ||PRO"
            '    cboTipopet.AddItem "Proyecto                                ||PRJ"
            '    cboTipopet.AddItem "Especial                                ||ESP"
            '    cboTipopet.ListIndex = 0
            '    cmdCancel.Enabled = True
            '    cmdOk.Enabled = True
            '    DoEvents
            '    Call setHabilCtrl(cboTipopet, "NOR")
            '    Call setHabilCtrl(cboBpar, "NOR")
            '    '{ add -001- b.
            '    Call setHabilCtrl(cmbPetClass, "NOR")
            '    Call setHabilCtrl(cmbPetImpact, "NOR")
            '    '}
            'End If
            'If CodigoCombo(cboTipopet, True) = "PRJ" Then
            '    Call setHabilCtrl(cboImportancia, "NOR")
            '    cboImportancia.Locked = False
            '    Call setHabilCtrl(cboOrientacion, "NOR")
            '    '{ add -001- b.
            '    Call setHabilCtrl(cmbPetClass, "NOR")
            '    Call setHabilCtrl(cmbPetImpact, "NOR")
            '    '}
            'End If
            'If CodigoCombo(cboTipopet, True) = "PRJ" Then
            '    cboTipopet.Clear
            '    cboTipopet.AddItem "Proyecto                                ||PRJ"
            '    cboTipopet.AddItem "Especial                                ||ESP"
            '    cboTipopet.ListIndex = 0
            '    cmdCancel.Enabled = True
            '    cmdOk.Enabled = True
            '    DoEvents
            '    Call setHabilCtrl(cboTipopet, "NOR")
            '    Call setHabilCtrl(cboBpar, "NOR")
            'End If
            'If CodigoCombo(cboTipopet, True) = "AUI" Then
            '    Call setHabilCtrl(cboBpar, "NOR")
            'End If
            'If CodigoCombo(cboTipopet, True) = "ESP" Then
            '    cboTipopet.Clear
            '    cboTipopet.AddItem "Especial                                ||ESP"
            '    cboTipopet.AddItem "Proyecto                                ||PRJ"
            '    cboTipopet.ListIndex = 0
            '    cmdCancel.Enabled = True
            '    cmdOk.Enabled = True
            '    DoEvents
            '    Call CargaCombosConTodo
            '    Call setHabilCtrl(cboTipopet, "NOR")
            '    '{ add -001- b.
            '    Call setHabilCtrl(cmbPetClass, "NOR")
            '    Call setHabilCtrl(cmbPetImpact, "NOR")
            '    '}
            '    Call setHabilCtrl(cboSector, "NOR")
            '    Call setHabilCtrl(cboBpar, "NOR")
            '    Call setHabilCtrl(cboSolicitante, "NOR")
            '    Call setHabilCtrl(memBeneficios, "NOR")
            '    Call setHabilCtrl(memCaracteristicas, "NOR")
            '    Call setHabilCtrl(memDescripcion, "NOR")
            '    Call setHabilCtrl(memMotivos, "NOR")
            '    Call setHabilCtrl(memObservaciones, "NOR")
            '    Call setHabilCtrl(memRelaciones, "NOR")
            'End If
            '''********** cuidado
            '}
'}
