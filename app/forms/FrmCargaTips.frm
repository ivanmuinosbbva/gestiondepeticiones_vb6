VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form FrmCargaTips 
   Caption         =   "Administración de Tips"
   ClientHeight    =   6375
   ClientLeft      =   5895
   ClientTop       =   3615
   ClientWidth     =   9300
   LinkTopic       =   "Form2"
   ScaleHeight     =   6375
   ScaleWidth      =   9300
   Begin VB.TextBox TxtRng 
      Height          =   285
      Left            =   7320
      TabIndex        =   14
      Text            =   "TxtRng"
      Top             =   5880
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox TxtVersion 
      Height          =   285
      Left            =   6720
      TabIndex        =   13
      Text            =   "version"
      Top             =   5880
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton Cerrar 
      Caption         =   "Cerrar"
      Height          =   495
      Left            =   8280
      TabIndex        =   9
      Top             =   5760
      Width           =   855
   End
   Begin VB.Frame Frame2 
      Caption         =   "Zona de carga"
      Height          =   2775
      Left            =   120
      TabIndex        =   4
      Top             =   3480
      Width           =   8055
      Begin VB.TextBox TxtTipTipo 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   5400
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   315
         Width           =   975
      End
      Begin VB.TextBox TextNroTip 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   315
         Width           =   375
      End
      Begin VB.CheckBox CheckVisualizar 
         Caption         =   "Visualizar Tip"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6360
         TabIndex        =   8
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox txtMensaje 
         Height          =   1935
         Left            =   120
         MaxLength       =   500
         MultiLine       =   -1  'True
         TabIndex        =   6
         Top             =   720
         Width           =   6015
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo de Tip:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4200
         TabIndex        =   11
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Mensaje del Tip Nro:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Lista de Tips cargados"
      Height          =   3255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   9135
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdCargaTips 
         Height          =   2895
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   8895
         _ExtentX        =   15690
         _ExtentY        =   5106
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         _NumberOfBands  =   1
         _Band(0).Cols   =   7
      End
   End
   Begin VB.CommandButton Modificar 
      Caption         =   "Modificar"
      Height          =   495
      Left            =   8280
      TabIndex        =   2
      Top             =   5040
      Width           =   855
   End
   Begin VB.CommandButton Eliminar 
      Caption         =   "Eliminar"
      Height          =   495
      Left            =   8280
      TabIndex        =   1
      Top             =   4320
      Width           =   855
   End
   Begin VB.CommandButton Agregar 
      Caption         =   "Agregar"
      Height          =   495
      Left            =   8280
      TabIndex        =   0
      Top             =   3600
      Width           =   855
   End
End
Attribute VB_Name = "FrmCargaTips"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Const colNroTip = 0
Const colTipoTip = 1
Const colHabil = 2
Const colMensaje = 3
Const colVersion = 4
Const colRng = 5
Const colFECHA = 6

Dim inCarga As Boolean

Private Sub Agregar_Click()
Dim ma_hab As String
    
    If CamposObligatorios Then
        If CheckVisualizar.value = 1 Then
           ma_hab = "S"
        Else
           ma_hab = "N"
        End If
        
        Call sp_InsertTips("Siempre", "TIP", Null, 1, txtMensaje.text, ma_hab, Format(date, "YYYY-MM-DD hh:mm:ss"))
        Call InicializarPantalla
    End If
End Sub

Private Sub Cerrar_Click()
    Call LockProceso(False)
    Unload Me
End Sub

Private Sub Modificar_Click()
    Dim ma_hab As String
    
    If CheckVisualizar.value = 1 Then
       ma_hab = "S"
    Else
       ma_hab = "N"
    End If
    
    'Si se selecciono un registro
    If TextNroTip.text <> " " And TextNroTip.text > "0" Then
       If CamposObligatorios Then
          Call sp_UpdateTips(TxtVersion.text, TxtTipTipo.text, TextNroTip.text, TxtRng.text, txtMensaje.text, ma_hab, Format(date, "YYYY-MM-DD hh:mm:ss"))
          Call InicializarPantalla
       End If
    Else
       MsgBox ("Por favor, primero seleccione el registro a eliminar")
    End If
End Sub
Private Sub Eliminar_Click()
    'Si se selecciono un registro
    If TextNroTip.text <> " " And TextNroTip.text > "0" Then
       Call sp_DeleteTips(Null, TxtTipTipo.text, TextNroTip.text, Null)
       Call InicializarPantalla
    Else
       MsgBox ("Por favor, primero seleccione el registro a eliminar")
    End If
End Sub

Private Sub Form_Load()
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    Show
    Call LimpiarForm(Me)
    
    grdCargaTips.Enabled = True
    Agregar.Enabled = True
    Modificar.Enabled = True
    Eliminar.Enabled = True
    Cerrar.Enabled = True
    
    Call CargarGrid
    Call LockProceso(False)
    
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    
    If Trim(txtMensaje.text) = "" Then
        MsgBox ("Por favor ingrese el mensaje del TIP")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Sub CargarGrid()
Dim I As Integer
    Call Puntero(True)
    inCarga = True
    
    With grdCargaTips
        .visible = False
        DoEvents
        .Clear
        .Rows = 1
        .FocusRect = flexFocusNone
        
        .TextMatrix(0, colNroTip) = "NRO.TIP": .ColWidth(colNroTip) = 800
        .TextMatrix(0, colTipoTip) = "TIPO TIP": .ColWidth(colTipoTip) = 850
        .TextMatrix(0, colHabil) = "HABILITADO": .ColWidth(colHabil) = 1150
        .TextMatrix(0, colMensaje) = "MENSAJE": .ColWidth(colMensaje) = 20000
        .TextMatrix(0, colVersion) = "VERSION": .ColWidth(colVersion) = 0
        .TextMatrix(0, colRng) = "Rng": .ColWidth(colRng) = 0
        .TextMatrix(0, colFECHA) = "Fecha": .ColWidth(colFECHA) = 0
    
         If sp_GetTips(Null, Null, Null, Null, Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colNroTip) = ClearNull(aplRST.Fields!tips_nro)
                .TextMatrix(.Rows - 1, colTipoTip) = ClearNull(aplRST.Fields!tips_tipo)
                .TextMatrix(.Rows - 1, colHabil) = ClearNull(aplRST.Fields!tips_hab)
                .TextMatrix(.Rows - 1, colMensaje) = ClearNull(aplRST.Fields!tips_txt)
                .TextMatrix(.Rows - 1, colVersion) = ClearNull(aplRST.Fields!tips_version)
                .TextMatrix(.Rows - 1, colRng) = ClearNull(aplRST.Fields!tips_rng)
                .TextMatrix(.Rows - 1, colFECHA) = ClearNull(aplRST.Fields!tips_date)
                aplRST.MoveNext
                DoEvents
            Loop
         End If
         .visible = True
     End With
     
     With grdCargaTips
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .row = 0
        For I = 0 To .cols - 1
            .Col = I
            .CellFontBold = True
        Next I
        .FocusRect = flexFocusNone
    End With
finx:
    DoEvents
    Call Puntero(False)
    Call MostrarSeleccion
    inCarga = False
End Sub

Sub MostrarSeleccion(Optional flgSort)
     Dim nRow As Integer
     Dim ma_habil As String
    
    inCarga = True
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    With grdCargaTips
        nRow = .RowSel
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, 0) <> "" Then
                TextNroTip.text = ClearNull(.TextMatrix(.RowSel, colNroTip))
                TxtTipTipo.text = ClearNull(.TextMatrix(.RowSel, colTipoTip))
                txtMensaje.text = ClearNull(.TextMatrix(.RowSel, colMensaje))
                TxtVersion.text = ClearNull(.TextMatrix(.RowSel, colVersion))
                TxtRng.text = ClearNull(.TextMatrix(.RowSel, colRng))
                ma_habil = ClearNull(.TextMatrix(.RowSel, colHabil))
                
                If ma_habil = "S" Then
                   CheckVisualizar.value = 1
                Else
                   CheckVisualizar.value = 0
                End If
            End If
        End If
    End With
    inCarga = False
End Sub
Private Sub grdCargaTips_Click()
    Call MostrarSeleccion
End Sub
Private Sub grdCarga()
    Call MostrarSeleccion
End Sub

