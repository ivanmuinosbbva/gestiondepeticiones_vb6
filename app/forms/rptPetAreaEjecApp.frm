VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptPetAreaEjecApp 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5355
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   8700
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5355
   ScaleWidth      =   8700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraOpciones 
      Height          =   945
      Left            =   1965
      TabIndex        =   25
      Top             =   4400
      Width           =   6735
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   5640
         TabIndex        =   27
         Top             =   360
         Width           =   975
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   4560
         TabIndex        =   26
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame frmCrtPrn 
      Height          =   945
      Left            =   0
      TabIndex        =   22
      Top             =   4400
      Width           =   1815
      Begin VB.OptionButton CrtPrn 
         Caption         =   "Pantalla"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   24
         Top             =   240
         Value           =   -1  'True
         Width           =   1395
      End
      Begin VB.OptionButton CrtPrn 
         Caption         =   "Impresora"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   23
         Top             =   540
         Width           =   1395
      End
   End
   Begin VB.Frame fraRpt 
      Height          =   3585
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   8700
      Begin VB.ComboBox cboEstructura 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "rptPetAreaEjecApp.frx":0000
         Left            =   1140
         List            =   "rptPetAreaEjecApp.frx":0002
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   420
         Width           =   6885
      End
      Begin VB.Frame fraNivel 
         Caption         =   " Nivel de agrupamiento "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2085
         Left            =   5760
         TabIndex        =   5
         Top             =   1020
         Width           =   2265
         Begin VB.OptionButton OptNivel 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   120
            TabIndex        =   11
            Top             =   1230
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   3
            Left            =   120
            TabIndex        =   10
            Top             =   960
            Width           =   1245
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   4
            Left            =   120
            TabIndex        =   9
            Top             =   705
            Width           =   1125
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   8
            Top             =   420
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Aplicativo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   1
            Left            =   120
            TabIndex        =   7
            Top             =   1485
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sin agrupar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   120
            TabIndex        =   6
            Top             =   1740
            Width           =   1365
         End
      End
      Begin VB.ComboBox cboAplicativo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1140
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   840
         Width           =   3885
      End
      Begin VB.ComboBox cmbPetClass 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "rptPetAreaEjecApp.frx":0004
         Left            =   1140
         List            =   "rptPetAreaEjecApp.frx":000E
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   2600
         Width           =   3885
      End
      Begin VB.ComboBox cmbDetalle 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "rptPetAreaEjecApp.frx":0048
         Left            =   1140
         List            =   "rptPetAreaEjecApp.frx":0052
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   2125
         Width           =   3885
      End
      Begin VB.ComboBox cmbHorasDetalle 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "rptPetAreaEjecApp.frx":009E
         Left            =   1140
         List            =   "rptPetAreaEjecApp.frx":00A8
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   3045
         Width           =   3885
      End
      Begin AT_MaskText.MaskText txtDESDE 
         Height          =   315
         Left            =   1140
         TabIndex        =   13
         Top             =   1260
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTA 
         Height          =   315
         Left            =   1140
         TabIndex        =   14
         Top             =   1680
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   21
         Top             =   450
         Width           =   360
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   20
         Top             =   1327
         Width           =   480
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   19
         Top             =   1747
         Width           =   450
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Aplicativo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   18
         Top             =   915
         Width           =   765
      End
      Begin VB.Label lblPetClass 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   17
         Top             =   2675
         Width           =   435
      End
      Begin VB.Label lblNivDet 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   16
         Top             =   2200
         Width           =   390
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Incluir"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   15
         Top             =   3120
         Width           =   495
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "3. HORAS TRABAJADAS POR �REA EJECUTORA Y APLICATIVO"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   28
      Top             =   120
      Width           =   5760
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptPetAreaEjecApp.frx":00E2
      Top             =   0
      Width           =   8700
   End
End
Attribute VB_Name = "rptPetAreaEjecApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 06.07.2007 - Se agregan las opciones de resumir el informe por peticiones y filtrar la clase de las peticiones a informar.
' -001- b. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 25.09.2007 - Se agrega la clase SPUFIs al control Combobox.
' -003- a. FJS 15.05.2008 - Se cambia la asignaci�n de la propiedad 'ReportFileName' del control CrystalReport utilizando la funci�n de formato de ruta corto (8.3) porque por ejemplo, cuando el string tiene 127 caracteres da un error 20504 en runtime al momento de mostrar el reporte (da error de archivo de reporte no encontrado).
' -004- a. FJS 16.10.2008 - Se inhabilita la opci�n de seleccionar por clase cuando se pide con detalle.
' -005- a. FJS 15.04.2009 - Se cambian los s�mbolos para optimizar la visualizaci�n de los datos del combo.
' -006- a. FJS 03.09.2009 - Se restringe la carga del combobox solo para �reas habilitadas, recursos habiltados, etc.
' -006- b. FJS 07.09.2009 - Se corrige el �ltimo par�metro (esta mal pasado, los recursos activos son "A", no "S").
' -007- a. FJS 07.09.2009 - Se agrega una nueva opci�n para seleccionar si se desea reportear las horas cargadas a peticiones o a tareas o ambas.

Option Explicit

Dim flgEnCarga As Boolean
Dim xNivel As Long

Private Sub Form_Load()
    flgEnCarga = False
    Call InicializarCombos
    txtDESDE.Text = Format(DateAdd("m", -1, date), "dd/mm/yyyy")
    txtHASTA.Text = Format(date, "dd/mm/yyyy")
    CrtPrn(1).Value = True
    CrtPrn(0).Value = True
    OptNivel(4).Value = True
End Sub

Sub InicializarCombos()
    flgEnCarga = True
    Call puntero(True)
    'Call Status("Cargando direcciones...")     ' del -005- a.
    Call Status("Inicializando...")             ' add -005- a.
    If sp_GetDireccion("", "S") Then            ' upd -006- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboEstructura.AddItem Trim(aplRST!nom_direccion) & Space(150) & "||DIRE|" & Trim(aplRST!cod_direccion)
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    'Call Status("Cargando gerencias...")       ' del -005- a.
    If sp_GetGerenciaXt("", "", "S") Then       ' upd -006- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboEstructura.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & Space(150) & "||GERE|" & Trim(aplRST!cod_gerencia)   ' upd -005- a. Se cambia ">> " por "� "
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    'Call Status("Cargando sector...")              ' del -005- a.
    If sp_GetSectorXt(Null, Null, Null, "S") Then   ' upd -006- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboEstructura.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & Space(150) & "||SECT|" & Trim(aplRST!cod_sector)    ' upd -005- a. Se cambia ">> " por "� "
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    'Call Status("Cargando grupo...")           ' del -005- a.
    If sp_GetGrupoXt("", "", "S") Then          ' upd -006- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboEstructura.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & Space(150) & "||GRUP|" & Trim(aplRST!cod_grupo) ' upd -005- a. Se cambia ">> " por "� "
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboEstructura.AddItem "Todo el BBVA" & Space(150) & "||NULL|NULL", 0
    cboEstructura.ListIndex = 0
    
    Call Initrecurso
    
    '{ add -001- a.
    With cmbPetClass
        .Clear
        .AddItem "Todas" & Space(100) & "||NULL"
        .AddItem "Correctivo" & Space(100) & "||CORR"
        .AddItem "Atenci�n a usuario" & Space(100) & "||ATEN"
        .AddItem "Optimizaci�n" & Space(100) & "||OPTI"
        .AddItem "Mantenimiento evolutivo" & Space(100) & "||EVOL"
        .AddItem "Nuevo desarrollo" & Space(100) & "||NUEV"
        .AddItem "SPUFI" & Space(100) & "||SPUF"            ' add -002- a.
        .AddItem "* Sin clase" & Space(100) & "||SINC"      ' add -xxx- a.
        .ListIndex = 0
    End With
    cmbDetalle.ListIndex = 0
    '}
    '{ add -007- a.
    With cmbHorasDetalle
        .Clear
        .AddItem "Tareas y peticiones" & Space(100) & "||ALL"
        .AddItem "Solo tareas" & Space(100) & "||TAR"
        .AddItem "Solo peticiones" & Space(100) & "||PET"
        .ListIndex = 0
    End With
    '}
    
    flgEnCarga = False
    Call Status("Listo.")
    Call puntero(False)
End Sub

Private Sub cboEstructura_Click()
Dim vRetorno() As String
Dim xNivel As String
Dim flgRecurso As Boolean

If flgEnCarga = True Then
    Exit Sub
End If
    
If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
    MsgBox ("Error")
    Exit Sub
End If
Call puntero(True)

flgEnCarga = True
flgRecurso = True

xNivel = vRetorno(1)
Select Case xNivel
    Case "NULL"
           OptNivel(0).Enabled = True
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = True
           OptNivel(4).Enabled = True
           OptNivel(5).Enabled = True
           OptNivel(4).Value = True
    Case "DIRE"
           OptNivel(0).Enabled = True
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = True
           OptNivel(4).Enabled = True
           OptNivel(5).Enabled = True
           OptNivel(4).Value = True
    Case "GERE"
           OptNivel(0).Enabled = True
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = True
           OptNivel(4).Enabled = True
           OptNivel(5).Enabled = False
           OptNivel(4).Value = True
    Case "SECT"
           OptNivel(0).Enabled = True
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = True
           OptNivel(4).Enabled = False
           OptNivel(5).Enabled = False
           OptNivel(3).Value = True
    Case "GRUP"
           OptNivel(0).Enabled = True
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = False
           OptNivel(4).Enabled = False
           OptNivel(5).Enabled = False
           OptNivel(2).Value = True
End Select
Call Initrecurso
Call puntero(False)
flgEnCarga = False

End Sub

Private Sub Initrecurso()
    Dim vRetorno() As String
    Dim xNivel As String
    Dim flgRecurso As Boolean

    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
        MsgBox ("Error.")
        Exit Sub
    End If
    Call puntero(True)

    flgRecurso = True

    xNivel = vRetorno(1)
    cboAplicativo.Clear
    
    Select Case xNivel
        Case "NULL"
            If Not sp_GetRecurso(Null, "R", "A") Then           ' upd -006- a.  ' upd -006- b.
                flgRecurso = False
            End If
        Case "DIRE"
            If Not sp_GetRecurso(vRetorno(2), "DI", "A") Then   ' upd -006- a.  ' upd -006- b.
                flgRecurso = False
            End If
        Case "GERE"
            If Not sp_GetRecurso(vRetorno(2), "GE", "A") Then   ' upd -006- a.  ' upd -006- b.
                flgRecurso = False
            End If
        Case "SECT"
            If Not sp_GetRecurso(vRetorno(2), "GR", "A") Then   ' upd -006- a.  ' upd -006- b.
                flgRecurso = False
            End If
        Case "GRUP"
            If Not sp_GetRecurso(vRetorno(2), "SU", "A") Then   ' upd -006- a.  ' upd -006- b.
                flgRecurso = False
            End If
    End Select
    If flgRecurso = True Then
        Do While Not aplRST.EOF
            cboAplicativo.AddItem StrConv(Trim(aplRST!nom_recurso), vbProperCase) & Space(150) & "||" & Trim(aplRST!cod_recurso)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboAplicativo.AddItem "Sin filtrar por recurso" & Space(150) & "||NULL", 0
    cboAplicativo.ListIndex = 0

    Call puntero(False)
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdOk_Click()
    Dim txtAux As String
    Dim sTitulo As String
    Dim xRet As Integer
    Dim vRetorno() As String
    Dim cPathFileNameReport As String   ' add -003- a.
    
    If Not CamposObligatorios Then
        Exit Sub
    End If
    
    '{ add -001- a.
    Select Case Left(cmbDetalle.Text, 1)
        Case "C"
    '}
            If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
                MsgBox ("Error")
            End If
            'mdiPrincipal.CrystalReport1.ReportFileName = App.Path & "\" & "hstraejec.rpt"  ' del -003- a.
            '{ add -003- a.
            If glENTORNO = "TEST" Or glENTORNO = "DESA" Then
                If glBASE = "CONS" Then
                    cPathFileNameReport = App.Path & "\reportes\consolidar\" & "hstraejec.rpt"
                Else
                    cPathFileNameReport = App.Path & "\reportes\banco\" & "hstraejec.rpt"
                    'cPathFileNameReport = App.Path & "\reportes\banco\" & "hstraejecapp.rpt"
                End If
            Else
                cPathFileNameReport = App.Path & "\hstraejec.rpt"
            End If
            mdiPrincipal.CrystalReport1.ReportFileName = GetShortName(cPathFileNameReport)
            '}
            mdiPrincipal.CrystalReport1.Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
            mdiPrincipal.CrystalReport1.SelectionFormula = ""
            mdiPrincipal.CrystalReport1.RetrieveStoredProcParams
            mdiPrincipal.CrystalReport1.StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
            mdiPrincipal.CrystalReport1.StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
            mdiPrincipal.CrystalReport1.StoredProcParam(2) = vRetorno(1)
            mdiPrincipal.CrystalReport1.StoredProcParam(3) = vRetorno(2)
            mdiPrincipal.CrystalReport1.StoredProcParam(4) = CodigoCombo(cboAplicativo, True)
            mdiPrincipal.CrystalReport1.StoredProcParam(5) = xNivel
            mdiPrincipal.CrystalReport1.StoredProcParam(6) = CodigoCombo(cmbHorasDetalle, True)       ' add -007- a.
            mdiPrincipal.CrystalReport1.Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
            mdiPrincipal.CrystalReport1.Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
            mdiPrincipal.CrystalReport1.Formulas(2) = "@0_TITULO='" & "Area Ejecutora: " & ClearNull(TextoCombo(cboEstructura)) & "'"
            If CodigoCombo(cboAplicativo, True) = "NULL" Then
                mdiPrincipal.CrystalReport1.Formulas(3) = "@1_TITULO=''"
            Else
                mdiPrincipal.CrystalReport1.Formulas(3) = "@1_TITULO='" & "Para el recurso: " & ClearNull(ReplaceApostrofo(TextoCombo(cboAplicativo))) & "'"   ' add -006- a. Se agrega la funci�n ReplaceApostrofo()
            End If
            mdiPrincipal.CrystalReport1.Formulas(4) = "@2_TITULO='" & "Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy")) & "'"
            '    mdiPrincipal.CrystalReport1.Connect = "DSN=" & mdiPrincipal.SybaseLogIn.DsnAplicacion & ";UID=" & mdiPrincipal.SybaseLogIn.UserAplicacion & ";PWD=" & mdiPrincipal.SybaseLogIn.PasswordAplicacion & mdiPrincipal.SybaseLogIn.ComponenteSI & ";SRVR=" & mdiPrincipal.SybaseLogIn.ServerSI & ";DB=" & mdiPrincipal.SybaseLogIn.BaseSI
            '"SRVR=PRUEBA_S11;DB=atr;UID=Odeatr;PWD=PassGes"
            '{call sp_rptHsTrabAreaSoli(?,?,?,?,?,?,?)}
            ' Esto es para debuggear en DESARROLLO
            'Debug.Print "sp_rptHsTrabAreaEjec " & _
            '            "'" & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, Date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")) & "', " _
            '            ; "'" & IIf(IsNull(txtHASTA.DateValue), Format(Date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")) & "', " & _
            '            "'" & vRetorno(1) & "', " & _
            '            "'" & vRetorno(2) & "', " & _
            '            "'" & CodigoCombo(cboAplicativo, True) & "', " & _
            '            "'" & xNivel & "'"
            mdiPrincipal.CrystalReport1.WindowLeft = 1
            mdiPrincipal.CrystalReport1.WindowTop = 1
            mdiPrincipal.CrystalReport1.WindowState = crptMaximized
           
            mdiPrincipal.CrystalReport1.Action = 1
            mdiPrincipal.CrystalReport1.Formulas(2) = ""
            mdiPrincipal.CrystalReport1.Formulas(3) = ""
            mdiPrincipal.CrystalReport1.Formulas(4) = ""
        '{ add -001- a.
        Case "S"
            If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
                MsgBox ("Error")
            End If
            With mdiPrincipal.CrystalReport1
                '{ add -003- a.
                If glENTORNO = "TEST" Or glENTORNO = "DESA" Then
                    If glBASE = "CONS" Then
                        cPathFileNameReport = App.Path & "\reportes\consolidar\" & "hstraejecclass.rpt"
                    Else
                        cPathFileNameReport = App.Path & "\reportes\banco\" & "hstraejecclass.rpt"
                    End If
                Else
                    cPathFileNameReport = App.Path & "\hstraejecclass.rpt"
                End If
                .ReportFileName = GetShortName(cPathFileNameReport)
                '}
                .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
                .SelectionFormula = ""
                .RetrieveStoredProcParams
                .StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
                .StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
                .StoredProcParam(2) = vRetorno(1)
                .StoredProcParam(3) = vRetorno(2)
                .StoredProcParam(4) = CodigoCombo(cboAplicativo, True)
                .StoredProcParam(5) = xNivel
                .StoredProcParam(6) = CodigoCombo(cmbPetClass, True)     ' add -001- a.
                .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
                .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
                .Formulas(2) = "@0_TITULO='" & "Area Ejecutora: " & ClearNull(TextoCombo(cboEstructura)) & "'"
                If CodigoCombo(cboAplicativo, True) = "NULL" Then
                    .Formulas(3) = "@1_TITULO=''"
                Else
                    .Formulas(3) = "@1_TITULO='" & "Para el recurso: " & ClearNull(TextoCombo(cboAplicativo)) & "'"
                End If
                .Formulas(4) = "@2_TITULO='" & "Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy")) & "'"
                ' Esto es para debuggear en DESARROLLO
                'Debug.Print "sp_rptHsTrabAreaEjec2 " & _
                '            "'" & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, Date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")) & "', " _
                '            ; "'" & IIf(IsNull(txtHASTA.DateValue), Format(Date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")) & "', " & _
                '            "'" & vRetorno(1) & "', " & _
                '            "'" & vRetorno(2) & "', " & _
                '            "'" & CodigoCombo(cboAplicativo, True) & "', " & _
                '            "'" & xNivel & "', " & _
                '            "'" & CodigoCombo(cmbPetClass, True) & "'"
                .WindowLeft = 1
                .WindowTop = 1
                .WindowState = crptMaximized
                .Action = 1
                .Formulas(2) = ""
                .Formulas(3) = ""
                .Formulas(4) = ""
            End With
        '}
    End Select
End Sub

Private Sub CrtPrn_Click(Index As Integer)
    Select Case Index
        Case 0
            mdiPrincipal.CrystalReport1.Destination = crptToWindow
        Case 1
            mdiPrincipal.CrystalReport1.Destination = crptToPrinter
    End Select
End Sub

Private Sub OptNivel_Click(Index As Integer)
    xNivel = Index
    Select Case xNivel
        Case 0, 1
            'cboAplicativo.Enabled = True              ' del -004- a.
            Call setHabilCtrl(cboAplicativo, "NOR")    ' add -004- a.
        Case Else
            'cboAplicativo.Enabled = False             ' del -004- a.
            Call setHabilCtrl(cboAplicativo, "DIS")    ' add -004- a.
            If cboAplicativo.ListCount > 0 Then
                cboAplicativo.ListIndex = 0
            End If
    End Select
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If txtDESDE = "" Then
        MsgBox ("Falta Fecha inicio informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA = "" Then
        MsgBox ("Falta Fecha fin informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox ("Fecha Hasta menor que Fecha Desde")
        CamposObligatorios = False
        Exit Function
    End If
End Function

'{ add -002- b.
'Private Sub Form_Activate()
'    StatusBarObjectIdentity Me
'End Sub
'}

'{ add -004- a.
Private Sub cmbDetalle_Click()
    If Left(cmbDetalle.Text, 1) = "C" Then
        cmbPetClass.ListIndex = 0
        Call setHabilCtrl(cmbPetClass, "DIS")
    Else
        Call setHabilCtrl(cmbPetClass, "NOR")
    End If
End Sub
'}


