VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form auxFeriado 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mantenimiento del calendario laboral"
   ClientHeight    =   7275
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10620
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7275
   ScaleWidth      =   10620
   ShowInTaskbar   =   0   'False
   Begin TabDlg.SSTab SSTab1 
      Height          =   7155
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8955
      _ExtentX        =   15796
      _ExtentY        =   12621
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "auxFeriado.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "pnlBotones"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "auxFeriado.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "auxFeriado.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4095
         Left            =   360
         TabIndex        =   5
         Top             =   600
         Width           =   5085
         Begin VB.VScrollBar scrollMes 
            Height          =   435
            Left            =   2820
            Max             =   11
            TabIndex        =   57
            Top             =   180
            Value           =   11
            Width           =   255
         End
         Begin VB.VScrollBar scrollAnio 
            Height          =   435
            Left            =   4260
            Max             =   79
            TabIndex        =   56
            Top             =   180
            Value           =   79
            Width           =   255
         End
         Begin VB.Frame Frame2 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3375
            Left            =   60
            TabIndex        =   6
            Top             =   600
            Width           =   4965
            Begin VB.Label lblSabado 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "SA"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   3930
               TabIndex        =   55
               Top             =   300
               Width           =   495
            End
            Begin VB.Label lblViernes 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "VI"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   3360
               TabIndex        =   54
               Top             =   300
               Width           =   495
            End
            Begin VB.Label lblJueves 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "JU"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2790
               TabIndex        =   53
               Top             =   300
               Width           =   495
            End
            Begin VB.Label lblMiercoles 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "MI"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2220
               TabIndex        =   52
               Top             =   300
               Width           =   495
            End
            Begin VB.Label lblMartes 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "MA"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   1650
               TabIndex        =   51
               Top             =   300
               Width           =   495
            End
            Begin VB.Label lblLunes 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "LU"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   1080
               TabIndex        =   50
               Top             =   300
               Width           =   495
            End
            Begin VB.Label lblDomingo 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "DO"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   510
               TabIndex        =   49
               Top             =   300
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   41
               Left            =   3930
               TabIndex        =   48
               Tag             =   "CLEAR"
               Top             =   2760
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   40
               Left            =   3360
               TabIndex        =   47
               Tag             =   "CLEAR"
               Top             =   2760
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   39
               Left            =   2790
               TabIndex        =   46
               Tag             =   "CLEAR"
               Top             =   2760
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   38
               Left            =   2220
               TabIndex        =   45
               Tag             =   "CLEAR"
               Top             =   2760
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   37
               Left            =   1650
               TabIndex        =   44
               Tag             =   "CLEAR"
               Top             =   2760
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   36
               Left            =   1080
               TabIndex        =   43
               Tag             =   "CLEAR"
               Top             =   2760
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   35
               Left            =   510
               TabIndex        =   42
               Tag             =   "CLEAR"
               Top             =   2760
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   34
               Left            =   3930
               TabIndex        =   41
               Tag             =   "CLEAR"
               Top             =   2340
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   33
               Left            =   3360
               TabIndex        =   40
               Tag             =   "CLEAR"
               Top             =   2340
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   32
               Left            =   2790
               TabIndex        =   39
               Tag             =   "CLEAR"
               Top             =   2340
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   31
               Left            =   2220
               TabIndex        =   38
               Tag             =   "CLEAR"
               Top             =   2340
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   30
               Left            =   1650
               TabIndex        =   37
               Tag             =   "CLEAR"
               Top             =   2340
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   29
               Left            =   1080
               TabIndex        =   36
               Tag             =   "CLEAR"
               Top             =   2340
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   28
               Left            =   510
               TabIndex        =   35
               Tag             =   "CLEAR"
               Top             =   2340
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   27
               Left            =   3930
               TabIndex        =   34
               Tag             =   "CLEAR"
               Top             =   1920
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   26
               Left            =   3360
               TabIndex        =   33
               Tag             =   "CLEAR"
               Top             =   1920
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   25
               Left            =   2790
               TabIndex        =   32
               Tag             =   "CLEAR"
               Top             =   1920
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   24
               Left            =   2220
               TabIndex        =   31
               Tag             =   "CLEAR"
               Top             =   1920
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   23
               Left            =   1650
               TabIndex        =   30
               Tag             =   "CLEAR"
               Top             =   1920
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   22
               Left            =   1080
               TabIndex        =   29
               Tag             =   "CLEAR"
               Top             =   1920
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   21
               Left            =   510
               TabIndex        =   28
               Tag             =   "CLEAR"
               Top             =   1920
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   20
               Left            =   3930
               TabIndex        =   27
               Tag             =   "CLEAR"
               Top             =   1500
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   19
               Left            =   3360
               TabIndex        =   26
               Tag             =   "CLEAR"
               Top             =   1500
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   18
               Left            =   2790
               TabIndex        =   25
               Tag             =   "CLEAR"
               Top             =   1500
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   17
               Left            =   2220
               TabIndex        =   24
               Tag             =   "CLEAR"
               Top             =   1500
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   16
               Left            =   1650
               TabIndex        =   23
               Tag             =   "CLEAR"
               Top             =   1500
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   15
               Left            =   1080
               TabIndex        =   22
               Tag             =   "CLEAR"
               Top             =   1500
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   14
               Left            =   510
               TabIndex        =   21
               Tag             =   "CLEAR"
               Top             =   1500
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   13
               Left            =   3930
               TabIndex        =   20
               Tag             =   "CLEAR"
               Top             =   1080
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   12
               Left            =   3360
               TabIndex        =   19
               Tag             =   "CLEAR"
               Top             =   1080
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   11
               Left            =   2790
               TabIndex        =   18
               Tag             =   "CLEAR"
               Top             =   1080
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   10
               Left            =   2220
               TabIndex        =   17
               Tag             =   "CLEAR"
               Top             =   1080
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   9
               Left            =   1650
               TabIndex        =   16
               Tag             =   "CLEAR"
               Top             =   1080
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   8
               Left            =   1080
               TabIndex        =   15
               Tag             =   "CLEAR"
               Top             =   1080
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   7
               Left            =   510
               TabIndex        =   14
               Tag             =   "CLEAR"
               Top             =   1080
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   6
               Left            =   3930
               TabIndex        =   13
               Tag             =   "CLEAR"
               Top             =   660
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   5
               Left            =   3360
               TabIndex        =   12
               Tag             =   "CLEAR"
               Top             =   660
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   4
               Left            =   2790
               TabIndex        =   11
               Tag             =   "CLEAR"
               Top             =   660
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   3
               Left            =   2220
               TabIndex        =   10
               Tag             =   "CLEAR"
               Top             =   660
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   2
               Left            =   1650
               TabIndex        =   9
               Tag             =   "CLEAR"
               Top             =   660
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   1
               Left            =   1080
               TabIndex        =   8
               Tag             =   "CLEAR"
               Top             =   660
               Width           =   495
            End
            Begin VB.Label lblDia 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   0
               Left            =   510
               TabIndex        =   7
               Tag             =   "CLEAR"
               Top             =   660
               Width           =   495
            End
            Begin VB.Shape Shape1 
               BackStyle       =   1  'Opaque
               BorderColor     =   &H00C0C0C0&
               Height          =   3015
               Left            =   120
               Top             =   240
               Width           =   4695
            End
         End
         Begin VB.Label lblAnio 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3390
            TabIndex        =   59
            Top             =   210
            Width           =   795
         End
         Begin VB.Label lblMes 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   570
            TabIndex        =   58
            Top             =   210
            Width           =   2205
         End
      End
      Begin VB.Frame pnlBotones 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4095
         Left            =   5550
         TabIndex        =   1
         Top             =   600
         Width           =   1305
         Begin VB.CommandButton cmdMarcar 
            Caption         =   "Marcar s�bados y domingos"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   675
            Left            =   60
            TabIndex        =   4
            TabStop         =   0   'False
            ToolTipText     =   "Marca autom�ticamente los s�bados y domingos del mes seleccionado"
            Top             =   120
            Width           =   1155
         End
         Begin VB.CommandButton cmdCerrar 
            Caption         =   "Cerrar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   60
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   3420
            Width           =   1155
         End
         Begin VB.CommandButton cmdModificar 
            Caption         =   "Aplicar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   60
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   2850
            Width           =   1155
         End
      End
   End
End
Attribute VB_Name = "auxFeriado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 03.09.2009 - Se cambia el formato del calendario.
' -003- a. FJS 26.10.2010 - Se agrega un bot�n para marcar los fines de semana de manera autom�tica en la vista del calendario actual.

Option Explicit

Dim nMes As Integer
Dim nAnio As Integer
Dim nMaxDia As Integer
'{ add -003- a.
Dim Fe_Ini As Date
Dim Fe_fin As Date
'}

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    Show
    Call InicializarPantalla
End Sub

Sub InicializarPantalla()
    Dim nIndice As Integer
    Me.Tag = ""
    Call LimpiarForm(Me)
    nMes = Month(date)
    nAnio = Year(date)
    
    scrollAnio.Value = 2079 - nAnio
    scrollMes.Value = 12 - nMes
    
'    lblAnio = nAnio
'    Call ArmarCalendario
End Sub

Function ActualizarFeriados() As Boolean
    Dim nIndice As Integer
    If sp_DeleteFeriado(CStr(nAnio) & Format(nMes, "00")) Then
        For nIndice = 0 To 41
            If lblDia(nIndice).ForeColor = COL_ROJO And Trim(lblDia(nIndice).Caption) <> "" Then
                sp_InsertFeriado (CDate(Format(Format(nMes, "00") & "/" & Format(lblDia(nIndice).Caption, "00") & "/" & nAnio, "mm/dd/yyyy")))
            End If
        Next nIndice
    Else
        ActualizarFeriados = False
    End If
End Function

Private Sub cmdModificar_Click()
    Call ActualizarFeriados
End Sub

Sub ArmarCalendario()
    Dim nInicio As Integer
    Dim nIndice As Integer
    Dim nPosicion As Integer
    
    ' Esta subrutina inicializa los controles del calendario
    For nIndice = 0 To 41
        lblDia(nIndice) = ""
        lblDia(nIndice).ToolTipText = ""
        '{ add -002- a.
        lblDia(nIndice).BackColor = vbWhite
        lblDia(nIndice).Font.name = "Verdana"
        lblDia(nIndice).Font.Size = 8
        '}
    Next nIndice
    ' Aqu� establece el l�mite superior del calendario seg�n la fecha seleccionada
    Select Case nMes
        Case 1, 3, 5, 7, 8, 10, 12
            nMaxDia = 31
        Case 4, 6, 9, 11
            nMaxDia = 30
        Case 2
            If IsDate("29/02/" & nAnio) Then
                nMaxDia = 29
            Else
                nMaxDia = 28
            End If
    End Select
    nInicio = Weekday("1/" & nMes & "/" & nAnio) - 1
    '{ add -003- a.
    Fe_Ini = CDate("1/" & nMes & "/" & nAnio)
    Fe_fin = CDate(Format(Format(nMes, "00") & "/" & Format(nMaxDia, "00") & "/" & nAnio, "mm/dd/yyyy"))
    '}
    For nIndice = 1 To nMaxDia
        nPosicion = nIndice + nInicio - 1
        lblDia(nPosicion).ForeColor = COL_NEGRO
        lblDia(nPosicion).ToolTipText = "Click Marca/Desmarca Feriado"
        lblDia(nPosicion) = nIndice
    Next nIndice
    If BuscarFeriados(CDate(Format(Format(nMes, "00") & "/01/" & nAnio, "mm/dd/yyyy")), CDate(Format(Format(nMes, "00") & "/" & Format(nMaxDia, "00") & "/" & nAnio, "mm/dd/yyyy"))) Then
        Call MarcarFeriados
    End If
End Sub

Sub MarcarFeriados()
    Dim nIndice As Integer
    Do While Not aplRST.EOF
        For nIndice = 0 To 41
            If Val(lblDia(nIndice).Caption) = Val(Left(aplRST(0), 2)) Then
                lblDia(nIndice).ForeColor = COL_ROJO
                DoEvents
                Exit For
            End If
        Next nIndice
        aplRST.MoveNext
    Loop
End Sub

Private Sub lblDia_Click(Index As Integer)
    If Trim(lblDia(Index).Caption) = "" Then Exit Sub
    If lblDia(Index).ForeColor = COL_NEGRO Then
        lblDia(Index).ForeColor = COL_ROJO
    Else
        lblDia(Index).ForeColor = COL_NEGRO
    End If
End Sub

Private Sub scrollAnio_Change()
    nAnio = 2079 - scrollAnio.Value
    lblAnio = nAnio
    DoEvents
    Call ArmarCalendario
    DoEvents
End Sub

Private Sub scrollMes_Change()
    DoEvents
    nMes = 12 - scrollMes.Value
    Select Case nMes
        Case 1: lblMes = "ENERO"
        Case 2: lblMes = "FEBRERO"
        Case 3: lblMes = "MARZO"
        Case 4: lblMes = "ABRIL"
        Case 5: lblMes = "MAYO"
        Case 6: lblMes = "JUNIO"
        Case 7: lblMes = "JULIO"
        Case 8: lblMes = "AGOSTO"
        Case 9: lblMes = "SETIEMBRE"
        Case 10: lblMes = "OCTUBRE"
        Case 11: lblMes = "NOVIEMBRE"
        Case 12: lblMes = "DICIEMBRE"
    End Select
    Call ArmarCalendario
    DoEvents
End Sub

Function BuscarFeriados(sFechaDesde As Date, sFechaHasta As Date) As Boolean
    If sp_GetFeriado(sFechaDesde, sFechaHasta) Then
        BuscarFeriados = True
    Else
        BuscarFeriados = False
    End If
End Function

'{ add -003- a.
Private Sub cmdMarcar_Click()
    Dim nIndice As Integer
    Dim fe_aux As Date
    
    fe_aux = Fe_Ini
    Do While fe_aux <= Fe_fin
        If Weekday(fe_aux, vbSunday) = 1 Or Weekday(fe_aux, vbSunday) = 7 Then
            For nIndice = 0 To 41
                If Val(lblDia(nIndice).Caption) = Val(Day(fe_aux)) Then
                    lblDia(nIndice).ForeColor = COL_ROJO
                    Exit For
                End If
            Next nIndice
        End If
        fe_aux = DateAdd("d", 1, fe_aux)
        DoEvents
    Loop
End Sub
'}

Private Sub cmdCerrar_Click()
    Unload Me
End Sub
