VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptPetFin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consulta de peticiones finalizadas"
   ClientHeight    =   4170
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9915
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4170
   ScaleWidth      =   9915
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame pnlBotones 
      Height          =   4095
      Left            =   8400
      TabIndex        =   1
      Top             =   0
      Width           =   1395
      Begin VB.CommandButton cmdGenerar 
         Caption         =   "GENER&AR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Genera un archivo Excel con los datos utilizados en la Minuta"
         Top             =   240
         Width           =   1275
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "C&ERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   3540
         Width           =   1275
      End
   End
   Begin VB.Frame frmGral 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4080
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8205
      Begin VB.CommandButton cmdGuardar 
         Height          =   315
         Left            =   7680
         Picture         =   "rptPetFin.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   24
         ToolTipText     =   "Seleccionar un archivo de destino para el proceso de exportaci�n"
         Top             =   3240
         Width           =   350
      End
      Begin VB.ComboBox cmbPetImpact 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   2880
         Width           =   4455
      End
      Begin VB.ComboBox cmbPetClass 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   2520
         Width           =   4455
      End
      Begin VB.ComboBox cmbPetType 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   2160
         Width           =   4455
      End
      Begin VB.Frame frmEjec 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   215
         Left            =   1680
         TabIndex        =   6
         Top             =   660
         Width           =   3120
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   1080
            TabIndex        =   8
            Top             =   0
            Width           =   885
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   0
            TabIndex        =   7
            Top             =   0
            Width           =   975
         End
      End
      Begin VB.ComboBox cboGerenciaSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   960
         Width           =   6135
      End
      Begin VB.ComboBox cboSectorSec 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   960
         Width           =   6135
      End
      Begin AT_MaskText.MaskText txtDESDEfinreal 
         Height          =   315
         Left            =   1680
         TabIndex        =   9
         Top             =   1365
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAfinreal 
         Height          =   315
         Left            =   1680
         TabIndex        =   10
         Top             =   1725
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFile 
         Height          =   315
         Left            =   1680
         TabIndex        =   12
         Top             =   3240
         Width           =   5985
         _ExtentX        =   10557
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Impacto tecnol�gico"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   0
         TabIndex        =   20
         Top             =   2955
         Width           =   1545
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Clase de petici�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   19
         Top             =   2595
         Width           =   1305
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tipo de petici�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   345
         TabIndex        =   18
         Top             =   2235
         Width           =   1200
      End
      Begin VB.Label Lbl7 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1155
         TabIndex        =   17
         Top             =   660
         Width           =   390
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1185
         TabIndex        =   16
         Top             =   1035
         Width           =   360
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1125
         TabIndex        =   15
         Top             =   1792
         Width           =   450
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "Finalizado Desde"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   315
         TabIndex        =   14
         Top             =   1432
         Width           =   1275
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Salida"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1110
         TabIndex        =   13
         Top             =   3307
         Width           =   465
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "�rea ejecutora"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   1125
      End
   End
End
Attribute VB_Name = "rptPetFin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -000- a. FJS 02.07.2007 - Nuevo objeto: Generaci�n de consulta de peticiones finalizadas
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto
'                           form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 14.12.2015 - Correcci�n: se modifica la rutina para evitar el uso de doble recordset concurrente porque algunas versiones
'                           de ADO no soportan el mismo. En este caso, se quita la parte de lectura de documentos adjuntos y se coloca
'                           fuera del loop principal.

Option Explicit

Dim flgEnCarga As Boolean
Dim secNivel As String
Dim secArea As String
Dim secDire As String
Dim secGere As String
Dim secSect As String
Dim secGrup As String

Private auxAgrup As String
Private xSelectAgrup As String

Const XLleft = -4131
Const XLcenter = -4108
Const XLright = -4152

Const colNroInterno = 1
Const colNroAsignado = 2
Const colTipoPet = 3
Const colTitulo = 4
Const colDetalle = 5
Const colSector = 6
Const colEjecuc = 7
Const colAgrup = 8
Const colFFin = 9
Const colFInf = 10
Const colHs = 11
Const colPetClass = 12
Const colPetImpTech = 13
Const colPetDocsAdj = 14

Private Sub Form_Load()
    Call InicializarCombos
    xSelectAgrup = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "AGR_SISTEMA", "0", glArchivoINI)

    'inicializo desde el lunes hasta el domingo proximo pasado
    txtDESDEfinreal.Text = Format(date - Weekday(date, vbSunday) - 5, "dd/mm/yyyy")
    txtHASTAfinreal.Text = Format(date - Weekday(date, vbSunday) + 1, "dd/mm/yyyy")
End Sub

Private Sub InicializarCombos()
    Dim i As Integer
    Dim auxCodPet, auxCodSec As String
    Dim xDireSec, xGereSec, xSectSec As String
    
    flgEnCarga = True
    
    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        xGereSec = ClearNull(aplRST!cod_gerencia)
        xSectSec = ClearNull(aplRST!cod_sector)
        xDireSec = ClearNull(aplRST!cod_direccion)
    End If
    
    If sp_GetGerenciaXt("", "") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboGerenciaSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(120) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|NULL|NULL"
            End If
            aplRST.MoveNext
        Loop
    End If
    
    If sp_GetSectorXt(Null, Null, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboSectorSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(120) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|NULL"
            End If
            aplRST.MoveNext
        Loop
    End If
    cboGerenciaSec.ListIndex = PosicionCombo(Me.cboGerenciaSec, xDireSec & xGereSec, True)
    cboSectorSec.ListIndex = PosicionCombo(Me.cboSectorSec, xDireSec & xGereSec & xSectSec, True)
    txtFile.Text = GetSetting("GesPet", "Export01", "FileMinuta", glWRKDIR & "Implementaciones_" & glBASE & ".xls")
    
    With cmbPetType
'        .AddItem "Todos" & ESPACIOS & ESPACIOS & "||NOR||ESP||PRO||PRJ||AUI||AUX"
'        .AddItem "Normal, especial y proyecto" & ESPACIOS & ESPACIOS & "||NOR||ESP||PRJ"
'        .AddItem "Propia" & ESPACIOS & ESPACIOS & "||PRO"
'        .AddItem "Proyecto" & ESPACIOS & ESPACIOS & "||PRJ"
'        .AddItem "Normal y especial" & ESPACIOS & ESPACIOS & "||NOR||ESP"
'        .AddItem "Normal, especial, proyecto, auditoria interna y externa" & ESPACIOS & ESPACIOS & "||NOR||ESP||PRJ||AUI||AUX"
'        .AddItem "Auditoria interna y externa" & ESPACIOS & ESPACIOS & "||AUI||AUX"
        
        .AddItem "Todos" & ESPACIOS & ESPACIOS & "||NOR/ESP/PRO/PRJ/AUI/AUX"
        .AddItem "Normal, especial y proyecto" & ESPACIOS & ESPACIOS & "||NOR/ESP/PRJ"
        .AddItem "Propia" & ESPACIOS & ESPACIOS & "||PRO"
        .AddItem "Proyecto" & ESPACIOS & ESPACIOS & "||PRJ"
        .AddItem "Normal y especial" & ESPACIOS & ESPACIOS & "||NOR/ESP"
        .AddItem "Normal, especial, proyecto, auditoria interna y externa" & ESPACIOS & ESPACIOS & "||NOR/ESP/PRJ/AUI/AUX"
        .AddItem "Auditoria interna y externa" & ESPACIOS & ESPACIOS & "||AUI/AUX"
        
        .ListIndex = 0
    End With
    
    With cmbPetClass
'        .AddItem "Todas" & ESPACIOS & ESPACIOS & "||CORR||ATEN||OPTI||EVOL||NUEV"
'        .AddItem "Todas menos correctivo/atenci�n a usuario" & ESPACIOS & ESPACIOS & "||OPTI||EVOL||NUEV"
'        .AddItem "Solo correctivo/atenci�n a usuario" & ESPACIOS & ESPACIOS & "||CORR||ATEN"
'        .AddItem "Solo correctivo" & ESPACIOS & ESPACIOS & "||CORR"
'        .AddItem "Solo atenci�n a usuario" & ESPACIOS & ESPACIOS & "||ATEN"
'        .AddItem "Optimizaci�n" & ESPACIOS & ESPACIOS & "||OPTI"
'        .AddItem "Mantenimiento evolutivo" & ESPACIOS & ESPACIOS & "||EVOL"
'        .AddItem "Nuevo desarrollo" & ESPACIOS & ESPACIOS & "||NUEV"
        
        .AddItem "Todas" & ESPACIOS & ESPACIOS & "||CORR/ATEN/OPTI/EVOL/NUEV"
        .AddItem "Todas menos correctivo/atenci�n a usuario" & ESPACIOS & ESPACIOS & "||OPTI/EVOL/NUEV"
        .AddItem "Solo correctivo/atenci�n a usuario" & ESPACIOS & ESPACIOS & "||CORR/ATEN"
        .AddItem "Solo correctivo" & ESPACIOS & ESPACIOS & "||CORR"
        .AddItem "Solo atenci�n a usuario" & ESPACIOS & ESPACIOS & "||ATEN"
        .AddItem "Optimizaci�n" & ESPACIOS & ESPACIOS & "||OPTI"
        .AddItem "Mantenimiento evolutivo" & ESPACIOS & ESPACIOS & "||EVOL"
        .AddItem "Nuevo desarrollo" & ESPACIOS & ESPACIOS & "||NUEV"
        
        
        .ListIndex = 0
    End With
    
    With cmbPetImpact
        '.AddItem "Todos" & ESPACIOS & ESPACIOS & "||S||N"
        .AddItem "Todos" & ESPACIOS & ESPACIOS & "||S/N"
        .AddItem "No" & ESPACIOS & ESPACIOS & "||N"
        .AddItem "Si" & ESPACIOS & ESPACIOS & "||S"
        .ListIndex = 0
    End With
    
    Call Status("Listo.")
    flgEnCarga = False
End Sub

Private Sub cmdGenerar_Click()
    If ClearNull(txtFile.Text) <> "" Then
        If MsgBox("�Confirma el inicio de proceso?", vbQuestion + vbYesNo) = vbYes Then
            Call Procesar
        End If
    Else
        txtFile.SetFocus
        MsgBox "No se defini� el archivo de salida.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub Procesar()
    If flgEnCarga = True Then Exit Sub

    Const MENSAJE_SINDATOS = "La consulta no arroja resultados."

    Dim cntRows As Integer, i As Integer, j As Integer
    Dim cntLast As Integer
    Dim cntField As Integer
    Dim cntTotal As Long            ' add -002- a. El total de registros a recorrer.
    Dim sTexto As String
    Dim sTtaux As String
    Dim sPetic As String
    Dim sAuxPetic As String
    Dim flgGcia As Boolean
    Dim auxHoras As Long
    Dim sPetDocsAdj As String
    Dim posIni As Long
    
    secNivel = verNivelSec()
    secArea = verAreaSec()
    cntField = 0
    cntRows = 0
    Dim ExcelApp, ExcelWrk, xlSheet As Object

    If Dir(txtFile.Text) <> "" Then
        If MsgBox("El archivo ya existe." & Chr(13) & "�Sobreescribe?", vbYesNo) = vbNo Then
            GoTo finx
        End If
        On Error Resume Next
        Kill txtFile
    End If
    If Dir(txtFile.Text) <> "" Then
        MsgBox "No puede sobreescribir el archivo, verifique que no est� en uso", vbExclamation + vbOKOnly
        GoTo finx
    End If
    Call Puntero(True)
    Call Status("Realizando consulta...")
    
    If OptNivelSec(1).Value = True Then     'Nivel: Sector
        If Not sp_GetPeticionesFinalizadas(0, secGere, secSect, "TERMIN", txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, CodigoCombo(cmbPetType, True), CodigoCombo(cmbPetClass, True), CodigoCombo(cmbPetImpact, True)) Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
        If aplRST.EOF Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
    Else                                    'Nivel: Gerencia
        If Not sp_GetPeticionesFinalizadas(0, secGere, "NULL", "ANEXAD|ANULAD|TERMIN|CANCEL|RECHAZ|RECHTE", txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, CodigoCombo(cmbPetType, True), CodigoCombo(cmbPetClass, True), CodigoCombo(cmbPetImpact, True)) Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
        If aplRST.EOF Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
    End If
    
    'cntTotal = aplRST.RecordCount
    Call Status("Generando planilla...")
    
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.Visible = False
    On Error Resume Next
    Set ExcelWrk = ExcelApp.Workbooks.Add
    If ExcelWrk Is Nothing Then
        MsgBox "Error al abrir planilla", vbCritical + vbOKOnly
        GoTo finx
    End If
    
    ExcelWrk.SaveAs FileName:=txtFile.Text
    Set xlSheet = ExcelWrk.Sheets(1)
    Call Puntero(True)
    
    xlSheet.Cells.VerticalAlignment = -4160
    xlSheet.Cells.Font.name = "Tahoma"
    xlSheet.Cells.Font.Size = 7
    
    'encabezados
    With xlSheet
        .Columns(colNroInterno).ColumnWidth = 3
        .Cells(1, colNroAsignado) = "Nro": .Columns(colNroAsignado).HorizontalAlignment = XLright: .Columns(colNroAsignado).ColumnWidth = 4
        .Cells(1, colTipoPet) = "Tipo": .Columns(colTipoPet).HorizontalAlignment = XLcenter: .Columns(colTipoPet).ColumnWidth = 3.5
        .Cells(1, colTitulo) = "T�tulo": .Columns(colTitulo).HorizontalAlignment = XLleft:  .Columns(colTitulo).ColumnWidth = 23
        .Cells(1, colDetalle) = "Descripci�n": xlSheet.Columns(colDetalle).HorizontalAlignment = XLleft: xlSheet.Columns(colDetalle).ColumnWidth = 40
        .Cells(1, colSector) = "Sector Solicitante": .Columns(colSector).ColumnWidth = 0
        .Cells(1, colEjecuc) = "Sector Ejecuci�n": .Columns(colEjecuc).ColumnWidth = 25
        If OptNivelSec(1).Value = True Then
            .Cells(1, colFFin) = "Fecha Fin Sector": .Columns(colFFin).HorizontalAlignment = XLcenter: .Columns(colFFin).ColumnWidth = 7.5: .Columns(colFFin).NumberFormat = "dd/mm/yy"
        Else
            .Cells(1, colFFin) = "Fecha Fin Gerencia": .Columns(colFFin).HorizontalAlignment = XLcenter: .Columns(colFFin).ColumnWidth = 7.5: .Columns(colFFin).NumberFormat = "dd/mm/yy"
        End If
        .Cells(1, colFInf) = "Fecha Informado": .Columns(colFInf).HorizontalAlignment = XLcenter: .Columns(colFInf).ColumnWidth = 7.5: .Columns(colFInf).NumberFormat = "dd/mm/yy"
        .Cells(1, colAgrup) = "Sistema": xlSheet.Columns(colAgrup).HorizontalAlignment = XLleft: xlSheet.Columns(colAgrup).HorizontalAlignment = XLleft: xlSheet.Columns(colAgrup).ColumnWidth = 25
        .Cells(1, colPetClass) = "Clase": xlSheet.Columns(colPetClass).HorizontalAlignment = XLleft: xlSheet.Columns(colPetClass).HorizontalAlignment = XLleft: xlSheet.Columns(colPetClass).ColumnWidth = 25
        .Cells(1, colPetImpTech) = "Impacto tecnol�gico": xlSheet.Columns(colPetImpTech).HorizontalAlignment = XLleft: xlSheet.Columns(colPetImpTech).HorizontalAlignment = XLleft: xlSheet.Columns(colPetImpTech).ColumnWidth = 25
        .Cells(1, colPetDocsAdj) = "Documentos adjuntos": xlSheet.Columns(colPetDocsAdj).HorizontalAlignment = XLleft: xlSheet.Columns(colPetDocsAdj).HorizontalAlignment = XLleft: xlSheet.Columns(colPetDocsAdj).ColumnWidth = 25
    End With
    xlSheet.Rows(1).RowHeight = 30
    xlSheet.Rows(1).Font.Bold = True
    xlSheet.Rows(1).HorizontalAlignment = XLleft
    ExcelApp.ActiveWindow.Zoom = 75
    
    i = 1
    sAuxPetic = ""
    
    Do While Not aplRST.EOF
        '{ del -002- a.
        'If sp_GetAdjuntosPet2(aplRST!pet_nrointerno, "", Null, Null) Then
        '    With adoRSAux1
        '        Do While Not .EOF
        '            If Len(sPetDocsAdj) > 0 Then sPetDocsAdj = sPetDocsAdj & ", "
        '            sPetDocsAdj = sPetDocsAdj & Trim(.Fields!adj_file)
        '            .MoveNext
        '            DoEvents
        '        Loop
        '    End With
        'End If
        '}
        If OptNivelSec(1).Value = True Then
            i = i + 1
            'Call Status("" & i & " de " & cntTotal)
            Call Status("" & i)
            xlSheet.Cells(i, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            xlSheet.Cells(i, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            xlSheet.Cells(i, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
            xlSheet.Cells(i, colTitulo) = ClearNull(aplRST!Titulo)
            xlSheet.Cells(i, colEjecuc) = ClearNull(aplRST!sec_nom_sector)
            xlSheet.Cells(i, colFFin) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
            xlSheet.Cells(i, colFInf) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
            xlSheet.Cells(i, colHs) = ClearNull(aplRST!horaspresup)
            xlSheet.Cells(i, colPetClass) = ClearNull(aplRST!cod_clase)
            xlSheet.Cells(i, colPetImpTech) = ClearNull(aplRST!pet_imptech)
            'xlSheet.Cells(i, colPetDocsAdj) = ClearNull(sPetDocsAdj)       ' del -002- a.
        Else
            If sAuxPetic <> ClearNull(aplRST!pet_nrointerno) Then
                i = i + 1
                'Call Status("" & i & " de " & cntTotal)
                Call Status("" & i)
                xlSheet.Cells(i, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
                xlSheet.Cells(i, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
                xlSheet.Cells(i, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
                xlSheet.Cells(i, colTitulo) = ClearNull(aplRST!Titulo)
                xlSheet.Cells(i, colEjecuc) = ""
                xlSheet.Cells(i, colFFin) = ""
                xlSheet.Cells(i, colFInf) = ""
                If InStr("|TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then
                    xlSheet.Cells(i, colHs) = ClearNull(aplRST!horaspresup)
                    xlSheet.Cells(i, colPetClass) = ClearNull(aplRST!cod_clase)
                    xlSheet.Cells(i, colPetImpTech) = ClearNull(aplRST!pet_imptech)
                    'xlSheet.Cells(i, colPetDocsAdj) = ClearNull(sPetDocsAdj)       ' del -002- a.
                End If
            Else
                If InStr("|TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then
                    xlSheet.Cells(i, colHs) = xlSheet.Cells(i, colHs) + ClearNull(aplRST!horaspresup)
                    xlSheet.Cells(i, colPetClass) = ClearNull(aplRST!cod_clase)
                    xlSheet.Cells(i, colPetImpTech) = ClearNull(aplRST!pet_imptech)
                    'xlSheet.Cells(i, colPetDocsAdj) = ClearNull(sPetDocsAdj)       ' del -002- a.
                End If
            End If
            sAuxPetic = ClearNull(aplRST!pet_nrointerno)
        End If
        aplRST.MoveNext
        'sPetDocsAdj = ""           ' del -002- a.
    Loop
    '{ del -002- a.
    'aplRST.Close
    'adoRSAux1.Close
    '}
    cntRows = i
    
    If OptNivelSec(0).Value = True Then
        For i = cntRows To 2 Step -1
            sAuxPetic = getNuevoEstadoGerencia(xlSheet.Cells(i, colNroInterno), secGere)
            If sAuxPetic <> "TERMIN" Then
                xlSheet.Rows(i).Delete
                cntRows = cntRows - 1
            End If
        Next
    End If
    
    If OptNivelSec(0).Value = True Then
        For i = 2 To cntRows
            If sp_GetPeticionSectorXt(xlSheet.Cells(i, colNroInterno), Null) Then
                Do While Not aplRST.EOF
                    If ClearNull(aplRST!cod_gerencia) = secGere Then
                         If ClearNull(aplRST!cod_estado) = "TERMIN" Then
                            xlSheet.Cells(i, colEjecuc) = xlSheet.Cells(i, colEjecuc) & ClearNull(aplRST!nom_sector) & Chr(10)
                            sTtaux = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "yyyy-mm-dd"), "")
                            If sTtaux > Format(xlSheet.Cells(i, colFFin), "yyyy-mm-dd") Then
                                xlSheet.Cells(i, colFFin) = sTtaux
                            End If
                         End If
                         If InStr("ANEXAD|ANULAD|TERMIN|CANCEL|RECHAZ|RECHTE", ClearNull(aplRST!cod_estado)) > 0 Then
                            sTtaux = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
                            If sTtaux > Format(xlSheet.Cells(i, colFInf), "yyyy-mm-dd") Then
                                xlSheet.Cells(i, colFInf) = sTtaux
                            End If
                         End If
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next
    End If
    
    For i = 2 To cntRows
        sPetic = xlSheet.Cells(i, colNroInterno)
        sTexto = ""
        sTtaux = sp_GetMemo(sPetic, "DESCRIPCIO")
        sTtaux = ReplaceString(sTtaux, 13, "")
        sTtaux = cutString(sTtaux, 10)
        If Len(sTtaux) > 2 Then
            sTexto = sTexto & sTtaux & Chr(10)
        End If
        sTexto = ReplaceString(sTexto, 13, "")
        sTexto = cutString(sTexto, 10)
        xlSheet.Cells(i, colDetalle) = sTexto
    Next
    
    '{ add -002- a.
    For i = 2 To cntRows
        sPetDocsAdj = ""
        If sp_GetAdjuntosPet(xlSheet.Cells(i, colNroInterno), "", Null, Null) Then
            Do While Not aplRST.EOF
                If Len(sPetDocsAdj) > 0 Then sPetDocsAdj = sPetDocsAdj & ", "
                sPetDocsAdj = sPetDocsAdj & ClearNull(aplRST.Fields!adj_file)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        xlSheet.Cells(i, colPetDocsAdj) = ClearNull(sPetDocsAdj)
    Next i
    '}
    
    If Val(xSelectAgrup) > 0 Then
        For i = 2 To cntRows
            sPetic = xlSheet.Cells(i, colNroInterno)
            sTexto = ""
            If sp_GetAgrupPeticHijo(xSelectAgrup, sPetic) Then
                Do While Not aplRST.EOF
                    sTexto = sTexto & ClearNull(aplRST!agr_titulo) & Chr(10)
                    aplRST.MoveNext
                    DoEvents
                Loop
                sTexto = cutString(sTexto, 10)
                xlSheet.Cells(i, colAgrup) = sTexto
            End If
        Next
    End If
   
    'Call Status("Columna Horas")
    xlSheet.Columns(colHs).Delete
    'Call Status("Columna Int")
    xlSheet.Columns(colNroInterno).Delete
    'Call Status("HorizontalAlignment")
    With xlSheet.Rows(1)
        .HorizontalAlignment = 4131
        .RowHeight = 30
    End With
    
    With xlSheet.Range(xlSheet.Cells(1, 1), xlSheet.Cells(cntRows, colPetDocsAdj - 1))
        .Borders(5).LineStyle = -4142
        .Borders(6).LineStyle = -4142
        .Borders(7).LineStyle = 1: .Borders(7).Weight = 1
        .Borders(8).LineStyle = 1: .Borders(8).Weight = 1
        .Borders(9).LineStyle = 1: .Borders(9).Weight = 1
        .Borders(10).LineStyle = 1: .Borders(10).Weight = 1
        .Borders(11).LineStyle = 1: .Borders(11).Weight = 1
        .Borders(12).LineStyle = 1: .Borders(12).Weight = 1
    End With
    
    xlSheet.Cells.WrapText = True
    xlSheet.Cells.Select
    xlSheet.Cells.EntireRow.AutoFit
    
    On Error Resume Next
    
    With xlSheet.PageSetup
        .PrintTitleRows = ""
        .PrintTitleColumns = ""
        .Orientation = 2
        .Zoom = 100
    End With
    
errOpen:
    ExcelWrk.Save
    ExcelWrk.Close
    ExcelApp.Application.Quit
    Call SaveSetting("GesPet", "Export01", "PetFin", ClearNull(txtFile.Text))
    Call Puntero(False)
    Call Status("Listo.")
    If cntRows < 2 Then
       Call Puntero(False)
       MsgBox "La consulta no arroja resultados que cumplan con el criterio", vbInformation + vbOKOnly
    Else
       'MsgBox (txtFile.Text & "  Finalizado")
       MsgBox "Se ha generado exitosamente el archivo: " & _
            Trim(txtFile.Text) & vbCrLf & "Proceso de exportaci�n finalizado.", vbInformation + vbOKOnly, "Exportaci�n finalizada"
    End If
    Unload Me
    Exit Sub
finx:
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub cmdCerrar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    LockProceso (False)
    Unload Me
End Sub

Private Sub OptNivelSec_Click(Index As Integer)
    cboGerenciaSec.Visible = False
    cboSectorSec.Visible = False
    cboGerenciaSec.Enabled = False
    cboSectorSec.Enabled = False
    Select Case Index
        Case 0
            cboGerenciaSec.Visible = True
            cboGerenciaSec.Enabled = True
        Case 1
            cboSectorSec.Visible = True
            cboSectorSec.Enabled = True
    End Select
End Sub

Function verAreaSec() As Variant
    Dim vRetorno() As String
    Dim auxAgrup As String
       
    verAreaSec = "NULL"
    secDire = "NULL"
    secGere = "NULL"
    secSect = "NULL"
    secGrup = "NULL"
       
    If OptNivelSec(0).Value = True Then
         If cboGerenciaSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Gerencia")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboGerenciaSec, True)
         auxAgrup = DatosCombo(Me.cboGerenciaSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(1).Value = True Then
         If cboSectorSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Sector")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboSectorSec, True)
         auxAgrup = DatosCombo(Me.cboSectorSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
End Function

Function verNivelSec() As Variant
    verNivelSec = "SECT"
End Function

Private Sub cmdGuardar_Click()
    Dim tmpFile As String
    
    tmpFile = Dialogo_GuardarExportacion("GUARDAR")
    txtFile = IIf(tmpFile = "", txtFile, tmpFile)
End Sub
