VERSION 5.00
Begin VB.Form auxSetPetHoras 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Preferencias para carga de horas"
   ClientHeight    =   2865
   ClientLeft      =   1140
   ClientTop       =   330
   ClientWidth     =   4905
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2865
   ScaleWidth      =   4905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraNivel 
      Caption         =   "Opciones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   945
      Left            =   60
      TabIndex        =   3
      Top             =   0
      Width           =   4815
      Begin VB.OptionButton OptNivel 
         Caption         =   "Visualizar solamente las peticiones asignadas al recurso"
         Height          =   255
         Index           =   1
         Left            =   90
         TabIndex        =   5
         Top             =   540
         Width           =   4635
      End
      Begin VB.OptionButton OptNivel 
         Caption         =   "Visualizar las peticiones del grupo/sector"
         Height          =   225
         Index           =   0
         Left            =   90
         TabIndex        =   4
         Top             =   300
         Width           =   4575
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   60
      TabIndex        =   0
      Top             =   2220
      Width           =   4815
      Begin VB.CommandButton cmdBD 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   3840
         TabIndex        =   2
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   2880
         TabIndex        =   1
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Horas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   60
      TabIndex        =   6
      Top             =   960
      Width           =   4815
      Begin VB.OptionButton optHoras 
         Caption         =   "Traerme las horas cargadas del �ltimo mes"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   8
         Top             =   540
         Width           =   3915
      End
      Begin VB.OptionButton optHoras 
         Caption         =   "Traerme las horas cargadas de la �ltima semana"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   300
         Width           =   3915
      End
   End
End
Attribute VB_Name = "auxSetPetHoras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Private Sub Form_Load()
    If GetSetting("GesPet", "CargaHoras", "Visualizar") = "USR" Then
        OptNivel(1).Value = True
    Else
        OptNivel(0).Value = True
    End If
    If GetSetting("GesPet", "CargaHoras", "ListaInicio") = "SEM" Then
        optHoras(0).Value = True
    Else
        optHoras(1).Value = True
    End If
End Sub

Private Sub cmdOK_Click()
    If OptNivel(1).Value Then
        SaveSetting "GesPet", "CargaHoras", "Visualizar", "USR"
    Else
        SaveSetting "GesPet", "CargaHoras", "Visualizar", "GRP"
    End If
    If optHoras(0).Value Then
        SaveSetting "GesPet", "CargaHoras", "ListaInicio", "SEM"
    Else
        SaveSetting "GesPet", "CargaHoras", "ListaInicio", "MES"
    End If
    Unload Me
End Sub

Private Sub cmdBD_Click()
    Unload Me
End Sub

'{ add -002- b.
'Private Sub Form_Activate()
'    StatusBarObjectIdentity Me
'End Sub
'}
