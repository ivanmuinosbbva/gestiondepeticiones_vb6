VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form auxHorasRecCategoria 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Peticiones asignadas directamente al recurso"
   ClientHeight    =   6405
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8760
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   8760
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4710
      Left            =   60
      TabIndex        =   4
      Top             =   1140
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   8308
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   60
      TabIndex        =   5
      Top             =   5760
      Width           =   8655
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   7680
         TabIndex        =   6
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      Begin VB.Label lblDescripcion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   450
         Left            =   120
         TabIndex        =   3
         Top             =   540
         Width           =   8385
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblRecurso 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   4545
      End
      Begin VB.Label lblFecha 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   4545
      End
   End
End
Attribute VB_Name = "auxHorasRecCategoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colPET_NROINTERNO = 0
Private Const colPET_NROASIGNADO = 1
Private Const colPET_CATEGORIA = 2
Private Const colPET_CLASE = 3
Private Const colPET_TITULO = 4
Private Const colHORAS_GRUPO = 5
Private Const colHORAS_RECURSO = 6
Private Const colHORAS_PRCJ = 7
Private Const colTOTCOLS = 8

Public pubRecurso As String
Public pet_nrointerno As Long

Dim sRecursoNombre As String
Dim bLoading As Boolean

Private Sub Form_Load()
    bLoading = True
    pet_nrointerno = 0
    lblFecha.Caption = "Per�odo de consulta entre el: " & Format(glFechaDesde, "dd/mm/yyyy") & " y " & Format(glFechaHasta, "dd/mm/yyyy")
    lblDescripcion.Caption = "Se listan las peticiones en estado activo donde el recurso se encuentra vinculado directamente." & vbCrLf & "Porcentaje de horas cargadas por el recurso versus horas presupuestadas por el grupo en la petici�n."
    pubRecurso = CodigoCombo(frmCargaHoras.cboRecurso, False)
    sRecursoNombre = sp_GetRecursoNombre(pubRecurso)
    Me.Caption = "Peticiones asignadas directamente al recurso " & Trim(sRecursoNombre)
    Call IniciarScroll(grdDatos)
End Sub

Sub CargarGrid()
    Call Puntero(True)
    
    With grdDatos
        .visible = False
        .Clear
        .Rows = 1
        .cols = colTOTCOLS
        .RowHeight(0) = 600
        .RowHeightMin = 255
        .WordWrap = True
        .TextMatrix(0, colPET_NROINTERNO) = "pet_nrointerno": .ColWidth(colPET_NROINTERNO) = 0: .ColAlignment(colPET_NROINTERNO) = flexAlignLeftCenter
        .TextMatrix(0, colPET_NROASIGNADO) = "Petic.": .ColWidth(colPET_NROASIGNADO) = 700: .ColAlignment(colPET_NROASIGNADO) = flexAlignRightCenter
        .TextMatrix(0, colPET_CATEGORIA) = "Categ.": .ColWidth(colPET_CATEGORIA) = 700: .ColAlignment(colPET_CATEGORIA) = flexAlignLeftCenter
        .TextMatrix(0, colPET_CLASE) = "Clase": .ColWidth(colPET_CLASE) = 700: .ColAlignment(colPET_CLASE) = flexAlignLeftCenter
        .TextMatrix(0, colPET_TITULO) = "T�tulo": .ColWidth(colPET_TITULO) = 3600: .ColAlignment(colPET_TITULO) = flexAlignLeftCenter
        .TextMatrix(0, colHORAS_GRUPO) = "Hs. presup. x Grupo": .ColWidth(colHORAS_GRUPO) = 950: .ColAlignment(colHORAS_GRUPO) = flexAlignRightCenter
        .TextMatrix(0, colHORAS_RECURSO) = "Hs. cargad. x Recurso": .ColWidth(colHORAS_RECURSO) = 950: .ColAlignment(colHORAS_RECURSO) = flexAlignRightCenter
        .TextMatrix(0, colHORAS_PRCJ) = "% carga Recurso": .ColWidth(colHORAS_PRCJ) = 950: .ColAlignment(colHORAS_PRCJ) = flexAlignRightCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    
    If Not sp_GetHorasTrabajadasXtC("0", pubRecurso, glFechaDesde, glFechaHasta) Then
        Call Puntero(False)
        Call Status("Listo.")
        Exit Sub
    End If

    With grdDatos
        Do While Not aplRST.EOF
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colPET_NROINTERNO) = ClearNull(aplRST.Fields!pet_nrointerno)
            .TextMatrix(.Rows - 1, colPET_NROASIGNADO) = ClearNull(aplRST.Fields!pet_nroasignado)
            .TextMatrix(.Rows - 1, colPET_CLASE) = ClearNull(aplRST.Fields!cod_clase)
            .TextMatrix(.Rows - 1, colPET_CATEGORIA) = ClearNull(aplRST.Fields!categoria)
            .TextMatrix(.Rows - 1, colPET_TITULO) = ClearNull(aplRST.Fields!Titulo)
            .TextMatrix(.Rows - 1, colHORAS_GRUPO) = IIf(aplRST.Fields!horas_presup_grupo = 0, " - ", Format(ClearNull(aplRST.Fields!horas_presup_grupo), "###,##0.0"))
            .TextMatrix(.Rows - 1, colHORAS_RECURSO) = IIf(aplRST.Fields!horas_cargadas_recurso = 0, " - ", Format(ClearNull(aplRST.Fields!horas_cargadas_recurso), "###,##0.0"))
            .TextMatrix(.Rows - 1, colHORAS_PRCJ) = IIf(aplRST.Fields!horas_presup_grupo = 0, " - ", Format(ClearNull(aplRST.Fields!porcj_carga), "###,##0.0") & "%")   ' Para evitar mostrar datos cuando no hay horas presupuestadas.
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
            aplRST.MoveNext
            DoEvents
        Loop
        .RowSel = 1: .ColSel = .cols - 1
        .visible = True
    End With
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub cmdOK_Click()
    'Unload Me
    Me.Hide
End Sub

Private Sub grdDatos_DblClick()
    Call SeleccionarPeticion
End Sub

Private Sub SeleccionarPeticion()
    With grdDatos
        If .Rows > 0 Then
            pet_nrointerno = .TextMatrix(.RowSel, colPET_NROINTERNO) ' = ClearNull(aplRST.Fields!pet_nrointerno)
            'Unload Me
            cmdOK_Click
        End If
    End With
End Sub
