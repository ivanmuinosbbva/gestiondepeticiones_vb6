VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   8730
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14235
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmBaseABM.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8730
   ScaleWidth      =   14235
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtSolicitudJustificacion 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1305
      Left            =   12720
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   62
      Top             =   1500
      Width           =   1275
   End
   Begin MSComctlLib.ImageList imgPeticiones 
      Left            =   13620
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   37
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":0B3A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":10D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":166E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":1C08
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":21A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":273C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":2CD6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":3270
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":380A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":3DA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":433E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":48D8
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":4E72
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":540C
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":59A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":5F40
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":64DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":6A74
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":700E
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":75A8
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":7942
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":7EDC
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":8476
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":8A10
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":8FAA
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":9544
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":9ADE
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":A078
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":A612
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":ABAC
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":AF46
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":B2E0
            Key             =   ""
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":B87A
            Key             =   ""
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":BE14
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":C3AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":C948
            Key             =   ""
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmBaseABM.frx":CEE2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   600
      Left            =   0
      TabIndex        =   61
      Top             =   0
      Width           =   14235
      _ExtentX        =   25109
      _ExtentY        =   1058
      ButtonWidth     =   2381
      ButtonHeight    =   1005
      Appearance      =   1
      Style           =   1
      ImageList       =   "imgPeticiones"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Datos generales"
            ImageIndex      =   36
            Style           =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Detalles"
            ImageIndex      =   37
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "ER && CA"
            ImageIndex      =   24
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Vinculados"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Adjuntos"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Conformes"
            ImageIndex      =   17
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Homologación"
            ImageIndex      =   34
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Controles"
            ImageIndex      =   35
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   8055
      Left            =   60
      TabIndex        =   11
      Top             =   600
      Width           =   12255
      _ExtentX        =   21616
      _ExtentY        =   14208
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "frmBaseABM.frx":D47C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Line5"
      Tab(0).Control(1)=   "UpDown1"
      Tab(0).Control(2)=   "Text2"
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "frmBaseABM.frx":D498
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "cboAccion"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Command1"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Command2"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Command3"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Command4"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "cmdAplicativos"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "cmdAnular"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cmdCambioEstado"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "cmdCambioEstadoPeriodo"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).ControlCount=   9
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "frmBaseABM.frx":D4B4
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lblTotHoras"
      Tab(2).Control(1)=   "Label10"
      Tab(2).Control(2)=   "txtPrjId"
      Tab(2).Control(3)=   "txtPrjSubId"
      Tab(2).Control(4)=   "txtPrjSubsId"
      Tab(2).Control(5)=   "fraConformeHomo"
      Tab(2).Control(6)=   "Frame1"
      Tab(2).Control(7)=   "Frame2"
      Tab(2).Control(8)=   "Frame3"
      Tab(2).Control(9)=   "Frame4"
      Tab(2).Control(10)=   "cmdHomoCancelar"
      Tab(2).Control(11)=   "cmdHomoGuardar"
      Tab(2).Control(12)=   "cmdHomoEliminar"
      Tab(2).Control(13)=   "cmdHomoEditar"
      Tab(2).Control(14)=   "cmdHomoNuevo"
      Tab(2).Control(15)=   "cmdHomoActualizar"
      Tab(2).Control(16)=   "cmdHomoConfirmar"
      Tab(2).Control(17)=   "cmdItemRespoEditar"
      Tab(2).Control(18)=   "cmdItemRespoGuardar"
      Tab(2).Control(19)=   "cmdItemRespoCancelar"
      Tab(2).Control(20)=   "cmdItemRespoEliminar"
      Tab(2).Control(21)=   "cmdKPICancelar"
      Tab(2).Control(22)=   "cmdKPIGuardar"
      Tab(2).Control(23)=   "cmdKPIEditar"
      Tab(2).Control(24)=   "cmdKPIQuitar"
      Tab(2).Control(25)=   "cmdKPINuevo"
      Tab(2).Control(26)=   "cmdAyudaClase"
      Tab(2).Control(26).Enabled=   0   'False
      Tab(2).ControlCount=   27
      Begin VB.CommandButton cmdCambioEstadoPeriodo 
         Caption         =   "Cambio Est. Período"
         Enabled         =   0   'False
         Height          =   495
         Left            =   4980
         TabIndex        =   84
         Top             =   1800
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdCambioEstado 
         Caption         =   "Cierre Planif."
         Enabled         =   0   'False
         Height          =   495
         Left            =   4980
         TabIndex        =   83
         Top             =   2280
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdAnular 
         Caption         =   "Anular"
         Enabled         =   0   'False
         Height          =   495
         Left            =   4980
         Picture         =   "frmBaseABM.frx":D4D0
         Style           =   1  'Graphical
         TabIndex        =   82
         Top             =   2760
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdAplicativos 
         Caption         =   "Aplicativos"
         Enabled         =   0   'False
         Height          =   435
         Left            =   1740
         TabIndex        =   81
         TabStop         =   0   'False
         ToolTipText     =   "Ver la ventana de carga, modificación y eliminación de horas por aplicativo"
         Top             =   1680
         Visible         =   0   'False
         Width           =   1110
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Agregar"
         Height          =   435
         Left            =   300
         TabIndex        =   80
         TabStop         =   0   'False
         ToolTipText     =   "Agregar un nuevo registro de horas trabajadas"
         Top             =   1680
         Width           =   1110
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Modificar"
         Height          =   435
         Left            =   300
         TabIndex        =   79
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el registro seleccionado"
         Top             =   2130
         Width           =   1110
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Eliminar"
         Height          =   435
         Left            =   300
         TabIndex        =   78
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el registro seleccionado"
         Top             =   2580
         Width           =   1110
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Confirmar"
         Height          =   435
         Left            =   300
         TabIndex        =   77
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operación"
         Top             =   3030
         Width           =   1110
      End
      Begin VB.ComboBox cboAccion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmBaseABM.frx":E00A
         Left            =   360
         List            =   "frmBaseABM.frx":E00C
         Style           =   2  'Dropdown List
         TabIndex        =   76
         Top             =   780
         Width           =   5535
      End
      Begin VB.CommandButton cmdAyudaClase 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -65280
         Picture         =   "frmBaseABM.frx":E00E
         Style           =   1  'Graphical
         TabIndex        =   75
         TabStop         =   0   'False
         Top             =   420
         Width           =   300
      End
      Begin VB.CommandButton cmdKPINuevo 
         Height          =   300
         Left            =   -66240
         Picture         =   "frmBaseABM.frx":E598
         Style           =   1  'Graphical
         TabIndex        =   67
         ToolTipText     =   "Agregar un nuevo KPI"
         Top             =   120
         Width           =   375
      End
      Begin VB.CommandButton cmdKPIQuitar 
         Height          =   300
         Left            =   -66240
         Picture         =   "frmBaseABM.frx":EB22
         Style           =   1  'Graphical
         TabIndex        =   66
         ToolTipText     =   "Eliminar el KPI actual"
         Top             =   750
         Width           =   375
      End
      Begin VB.CommandButton cmdKPIEditar 
         Height          =   300
         Left            =   -66240
         Picture         =   "frmBaseABM.frx":F0AC
         Style           =   1  'Graphical
         TabIndex        =   65
         ToolTipText     =   "Editar KPI seleccionado"
         Top             =   435
         Width           =   375
      End
      Begin VB.CommandButton cmdKPIGuardar 
         Height          =   300
         Left            =   -66240
         Picture         =   "frmBaseABM.frx":F636
         Style           =   1  'Graphical
         TabIndex        =   64
         ToolTipText     =   "Guardar / Confirmar"
         Top             =   1320
         Width           =   375
      End
      Begin VB.CommandButton cmdKPICancelar 
         Height          =   300
         Left            =   -66240
         Picture         =   "frmBaseABM.frx":FBC0
         Style           =   1  'Graphical
         TabIndex        =   63
         ToolTipText     =   "Deshacer / Cancelar"
         Top             =   1635
         Width           =   375
      End
      Begin VB.CommandButton cmdItemRespoEliminar 
         Height          =   375
         Left            =   -64680
         Picture         =   "frmBaseABM.frx":1014A
         Style           =   1  'Graphical
         TabIndex        =   60
         ToolTipText     =   "Quitar los responsables seleccionados"
         Top             =   420
         Width           =   375
      End
      Begin VB.CommandButton cmdItemRespoCancelar 
         Height          =   375
         Left            =   -63360
         Picture         =   "frmBaseABM.frx":104D4
         Style           =   1  'Graphical
         TabIndex        =   59
         ToolTipText     =   "Cancelar la operación actual"
         Top             =   420
         Width           =   375
      End
      Begin VB.CommandButton cmdItemRespoGuardar 
         Height          =   375
         Left            =   -63720
         Picture         =   "frmBaseABM.frx":10A5E
         Style           =   1  'Graphical
         TabIndex        =   58
         ToolTipText     =   "Confirmar los cambios"
         Top             =   420
         Width           =   375
      End
      Begin VB.CommandButton cmdItemRespoEditar 
         Height          =   375
         Left            =   -64080
         Picture         =   "frmBaseABM.frx":10FE8
         Style           =   1  'Graphical
         TabIndex        =   57
         ToolTipText     =   "Editar el item seleccionado"
         Top             =   420
         Width           =   375
      End
      Begin VB.CommandButton cmdHomoConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -71700
         Style           =   1  'Graphical
         TabIndex        =   56
         ToolTipText     =   "Envia por correo electrónico a los recursos seleccionados los datos relevantes del informe actual."
         Top             =   3240
         Width           =   855
      End
      Begin VB.CommandButton cmdHomoActualizar 
         Caption         =   "Actualizar"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -71760
         Picture         =   "frmBaseABM.frx":11572
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   2580
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton cmdHomoNuevo 
         Caption         =   "Nuevo"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -70800
         Picture         =   "frmBaseABM.frx":11AFC
         Style           =   1  'Graphical
         TabIndex        =   54
         ToolTipText     =   "Generar un nuevo informe de homologación"
         Top             =   2580
         Width           =   855
      End
      Begin VB.CommandButton cmdHomoEditar 
         Caption         =   "&Editar"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -69960
         Picture         =   "frmBaseABM.frx":12086
         Style           =   1  'Graphical
         TabIndex        =   53
         ToolTipText     =   "Modifica el informe actual"
         Top             =   2580
         Width           =   855
      End
      Begin VB.CommandButton cmdHomoEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -69120
         Picture         =   "frmBaseABM.frx":12610
         Style           =   1  'Graphical
         TabIndex        =   52
         ToolTipText     =   "Elimina definitivamente el informe actual"
         Top             =   2580
         Width           =   855
      End
      Begin VB.CommandButton cmdHomoGuardar 
         Caption         =   "Guardar"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -68280
         Picture         =   "frmBaseABM.frx":12B9A
         Style           =   1  'Graphical
         TabIndex        =   51
         ToolTipText     =   "Guarda los datos actuales y/o confirma una acción"
         Top             =   2580
         Width           =   855
      End
      Begin VB.CommandButton cmdHomoCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   -67440
         Picture         =   "frmBaseABM.frx":13124
         Style           =   1  'Graphical
         TabIndex        =   50
         ToolTipText     =   "Cancela la operación actual"
         Top             =   2580
         Width           =   855
      End
      Begin VB.Frame Frame4 
         Caption         =   "Anexas"
         Height          =   3195
         Left            =   -71760
         TabIndex        =   49
         Top             =   3840
         Width           =   3735
         Begin VB.CommandButton cmdVerAnexa 
            Height          =   495
            Left            =   2460
            Picture         =   "frmBaseABM.frx":136AE
            Style           =   1  'Graphical
            TabIndex        =   71
            TabStop         =   0   'False
            ToolTipText     =   "Ver petición"
            Top             =   720
            Visible         =   0   'False
            Width           =   1170
         End
         Begin MSFlexGridLib.MSFlexGrid grdAnexas 
            Height          =   1740
            Left            =   180
            TabIndex        =   70
            Top             =   600
            Width           =   1995
            _ExtentX        =   3519
            _ExtentY        =   3069
            _Version        =   393216
            Cols            =   3
            FixedCols       =   0
            ForeColorSel    =   16777215
            GridColor       =   14737632
            AllowBigSelection=   0   'False
            ScrollTrack     =   -1  'True
            FocusRect       =   0
            GridLinesFixed  =   0
            SelectionMode   =   1
            AllowUserResizing=   1
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Adjuntos"
         Height          =   4695
         Left            =   -64860
         TabIndex        =   34
         Top             =   2340
         Width           =   1935
         Begin VB.CommandButton adjCan 
            Caption         =   "Cancelar"
            Enabled         =   0   'False
            Height          =   450
            Left            =   300
            TabIndex        =   40
            TabStop         =   0   'False
            Top             =   4080
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton adjCon 
            Caption         =   "Confirmar"
            Enabled         =   0   'False
            Height          =   450
            Left            =   300
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   3600
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton adjMod 
            Caption         =   "Modificar"
            Enabled         =   0   'False
            Height          =   450
            Left            =   300
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   2640
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton adjDel 
            Caption         =   "Desvincular"
            Enabled         =   0   'False
            Height          =   450
            Left            =   300
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   3120
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton adjAdd 
            Caption         =   "Adjuntar"
            Height          =   450
            Left            =   300
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   2160
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton adjView 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   450
            Left            =   420
            Picture         =   "frmBaseABM.frx":13C38
            Style           =   1  'Graphical
            TabIndex        =   35
            TabStop         =   0   'False
            ToolTipText     =   "Ver adjunto"
            Top             =   420
            Visible         =   0   'False
            Width           =   1170
         End
         Begin MSFlexGridLib.MSFlexGrid grdAdjPet 
            Height          =   1170
            Left            =   240
            TabIndex        =   41
            Top             =   1020
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   2064
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            ForeColorSel    =   16777215
            GridColor       =   14737632
            AllowBigSelection=   0   'False
            ScrollTrack     =   -1  'True
            FocusRect       =   0
            GridLinesFixed  =   0
            SelectionMode   =   1
            AllowUserResizing=   1
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Vinculados"
         Height          =   3735
         Left            =   -67920
         TabIndex        =   33
         Top             =   3300
         Width           =   2955
         Begin VB.CommandButton docView 
            Enabled         =   0   'False
            Height          =   500
            Left            =   240
            Picture         =   "frmBaseABM.frx":141C2
            Style           =   1  'Graphical
            TabIndex        =   48
            TabStop         =   0   'False
            ToolTipText     =   "Ver docum."
            Top             =   240
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton docAdd 
            Caption         =   "Vincular"
            Height          =   500
            Left            =   1560
            TabIndex        =   47
            TabStop         =   0   'False
            Top             =   600
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton docDel 
            Caption         =   "Desvincular"
            Enabled         =   0   'False
            Height          =   500
            Left            =   1560
            TabIndex        =   46
            TabStop         =   0   'False
            Top             =   1620
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton docMod 
            Caption         =   "Modificar"
            Enabled         =   0   'False
            Height          =   500
            Left            =   1560
            TabIndex        =   45
            TabStop         =   0   'False
            Top             =   1110
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton docCon 
            Caption         =   "Confirmar"
            Enabled         =   0   'False
            Height          =   500
            Left            =   1560
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   2130
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton docCan 
            Caption         =   "Cancelar"
            Enabled         =   0   'False
            Height          =   500
            Left            =   1560
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   2640
            Visible         =   0   'False
            Width           =   1170
         End
         Begin MSFlexGridLib.MSFlexGrid grdDocMetod 
            Height          =   2730
            Left            =   120
            TabIndex        =   42
            Top             =   780
            Visible         =   0   'False
            Width           =   1425
            _ExtentX        =   2514
            _ExtentY        =   4815
            _Version        =   393216
            Cols            =   6
            FixedCols       =   0
            ForeColorSel    =   16777215
            GridColor       =   14737632
            AllowBigSelection=   0   'False
            ScrollTrack     =   -1  'True
            FocusRect       =   0
            GridLinesFixed  =   0
            SelectionMode   =   1
            AllowUserResizing=   1
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "conformes"
         Height          =   4815
         Left            =   -74820
         TabIndex        =   25
         Top             =   2220
         Width           =   2955
         Begin VB.CommandButton pcfAdd 
            Caption         =   "Agregar"
            Height          =   500
            Left            =   360
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   3060
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton pcfDel 
            Caption         =   "Eliminar"
            Enabled         =   0   'False
            Height          =   500
            Left            =   420
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   3660
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton pcfCon 
            Caption         =   "Confirmar"
            Enabled         =   0   'False
            Height          =   500
            Left            =   1620
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   3660
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton pcfCan 
            Caption         =   "Cancelar"
            Enabled         =   0   'False
            Height          =   500
            Left            =   420
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   4200
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton pcfMod 
            Caption         =   "Modificar"
            Enabled         =   0   'False
            Height          =   500
            Left            =   1560
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   3060
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.CommandButton cmdDeshabilitarManual 
            Caption         =   "Deshabilitar manual"
            Height          =   500
            Left            =   360
            TabIndex        =   26
            TabStop         =   0   'False
            ToolTipText     =   "Deshabilitar manualmente la validez de la petición para pasajes a Producción"
            Top             =   240
            Visible         =   0   'False
            Width           =   1170
         End
         Begin MSFlexGridLib.MSFlexGrid grdPetConf 
            Height          =   2370
            Left            =   360
            TabIndex        =   32
            Top             =   600
            Visible         =   0   'False
            Width           =   1935
            _ExtentX        =   3413
            _ExtentY        =   4180
            _Version        =   393216
            Cols            =   7
            FixedCols       =   0
            ForeColorSel    =   16777215
            GridColor       =   14737632
            AllowBigSelection=   0   'False
            ScrollTrack     =   -1  'True
            FocusRect       =   0
            GridLinesFixed  =   0
            SelectionMode   =   1
            AllowUserResizing=   1
         End
      End
      Begin VB.Frame fraConformeHomo 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1740
         Left            =   -74700
         TabIndex        =   14
         Top             =   720
         Visible         =   0   'False
         Width           =   8115
         Begin VB.ComboBox cboPcfTipoHomo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmBaseABM.frx":1474C
            Left            =   1800
            List            =   "frmBaseABM.frx":1474E
            Style           =   2  'Dropdown List
            TabIndex        =   20
            Top             =   480
            Width           =   3465
         End
         Begin VB.TextBox pcfTextoHomo 
            Height          =   675
            Left            =   1800
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   19
            Top             =   840
            Width           =   4905
         End
         Begin VB.CommandButton cmdHomoAdd 
            Caption         =   "Agregar"
            Height          =   375
            Left            =   7020
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   240
            Width           =   930
         End
         Begin VB.CommandButton cmdHomoDel 
            Caption         =   "Quitar"
            Enabled         =   0   'False
            Height          =   375
            Left            =   7020
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   600
            Width           =   930
         End
         Begin VB.CommandButton cmdHomoConf 
            Caption         =   "Confirmar"
            Enabled         =   0   'False
            Height          =   375
            Left            =   7020
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   960
            Width           =   930
         End
         Begin VB.CommandButton cmdHomoCancel 
            Caption         =   "Cancelar"
            Enabled         =   0   'False
            Height          =   375
            Left            =   7020
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   1320
            Width           =   930
         End
         Begin VB.Label lblpcfTextoHomo 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            Height          =   195
            Left            =   120
            TabIndex        =   24
            Top             =   840
            Width           =   1065
         End
         Begin VB.Label lblcboPcfTipoHomo 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de conforme"
            Height          =   195
            Left            =   120
            TabIndex        =   23
            Top             =   480
            Width           =   1245
         End
         Begin VB.Label lblPetConfHomo 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   22
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblHomologacion 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   " Homologación "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   -1  'True
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   195
            Left            =   5640
            TabIndex        =   21
            Top             =   0
            Width           =   1290
         End
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   -66120
         Locked          =   -1  'True
         TabIndex        =   12
         Text            =   "1 de 1"
         Top             =   720
         Width           =   615
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   300
         Left            =   -65520
         TabIndex        =   13
         Top             =   720
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   315
         Left            =   -63360
         TabIndex        =   72
         Top             =   1320
         Visible         =   0   'False
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   315
         Left            =   -63720
         TabIndex        =   73
         Top             =   1320
         Visible         =   0   'False
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   315
         Left            =   -64080
         TabIndex        =   74
         Top             =   1320
         Visible         =   0   'False
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   11
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   11
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Suma de horas visualizadas:"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -70860
         TabIndex        =   69
         Top             =   7380
         Width           =   2025
      End
      Begin VB.Label lblTotHoras 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "HHH:MM"
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -68670
         TabIndex        =   68
         Top             =   7380
         Width           =   795
         WordWrap        =   -1  'True
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00808080&
         X1              =   -74520
         X2              =   -65280
         Y1              =   5640
         Y2              =   5640
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   7455
      Left            =   8760
      TabIndex        =   2
      Top             =   1500
      Width           =   1455
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Gerencia"
         Top             =   4680
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Height          =   500
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la Gerencia selecionada"
         Top             =   5190
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Gerencia seleccionada"
         Top             =   5700
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   6720
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operación"
         Top             =   6210
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      Height          =   2775
      Left            =   120
      TabIndex        =   1
      Top             =   4680
      Width           =   8535
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1200
         TabIndex        =   10
         Text            =   "Text1"
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label lblDatos 
         Caption         =   "Label1"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   9
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   465
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4575
      Left            =   120
      TabIndex        =   0
      Top             =   1620
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   8070
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

