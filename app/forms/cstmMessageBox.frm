VERSION 5.00
Begin VB.Form cstmMessageBox 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Cambio de estado de la petici�n: Ejecuci�n"
   ClientHeight    =   3180
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5700
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   5700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Default         =   -1  'True
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   2700
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Continuar"
      Height          =   375
      Left            =   3720
      TabIndex        =   0
      Top             =   2700
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.Image Image1 
      Height          =   240
      Left            =   5280
      Picture         =   "cstmMessageBox.frx":0000
      Top             =   180
      Width           =   240
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "�Desea continuar?"
      Height          =   195
      Left            =   180
      TabIndex        =   5
      Top             =   1920
      Width           =   5355
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Una vez confirmado dicho cambio, ya no podr�n modificarse estos datos de la petici�n. "
      Height          =   390
      Left            =   180
      TabIndex        =   4
      Top             =   1320
      Width           =   5355
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblMensaje 
      AutoSize        =   -1  'True
      Caption         =   $"cstmMessageBox.frx":058A
      Height          =   585
      Left            =   180
      TabIndex        =   3
      Top             =   540
      Width           =   5355
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      Caption         =   "ADVERTENCIA"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   180
      TabIndex        =   1
      Top             =   180
      Width           =   1155
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00808080&
      Height          =   3075
      Left            =   60
      Top             =   60
      Width           =   5595
   End
End
Attribute VB_Name = "cstmMessageBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public msgIcon As Image
Public msgTitle As String
Public msgText As String
Public ModoBotones As Botones
Public bContinuar As Boolean

Public Enum Botones
    OkOnly = 1
    OkCancel = 2
    SiNo = 3
End Enum

Private Sub Form_Load()
    ' Titulo de la ventana
    With lblTitulo
        .Font.name = "Tahoma"
        .Font.Size = 8
        .FontBold = True
    End With
    bContinuar = False
    cmdAceptar.visible = True
    cmdCancelar.visible = True
    cmdCancelar.Default = True
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdAceptar_Click()
    bContinuar = True
    Unload Me
End Sub
