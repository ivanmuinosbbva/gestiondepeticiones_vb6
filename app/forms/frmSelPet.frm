VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmSelPet 
   Caption         =   "Seleccionar una petici�n"
   ClientHeight    =   7140
   ClientLeft      =   1155
   ClientTop       =   345
   ClientWidth     =   10815
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "frmSelPet.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   10815
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraFiltros 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1485
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   10725
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3150
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   270
         Width           =   5415
      End
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "Filtrar"
         Height          =   315
         Left            =   1320
         TabIndex        =   2
         Top             =   300
         Width           =   1065
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   315
         Left            =   1320
         TabIndex        =   1
         Top             =   990
         Width           =   1065
      End
      Begin AT_MaskText.MaskText txtNroBuscar 
         Height          =   315
         Left            =   3150
         TabIndex        =   3
         Top             =   960
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   3150
         TabIndex        =   4
         Top             =   630
         Width           =   5385
         _ExtentX        =   9499
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Nro.:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2670
         TabIndex        =   10
         Top             =   1020
         Width           =   375
      End
      Begin VB.Label Label2 
         Caption         =   "Filtrar por Estado/T�tulo"
         ForeColor       =   &H00FF0000&
         Height          =   480
         Left            =   120
         TabIndex        =   9
         Top             =   270
         Width           =   1050
      End
      Begin VB.Label Label3 
         Caption         =   "Buscar por N�mero"
         ForeColor       =   &H00FF0000&
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   960
         Width           =   900
      End
      Begin VB.Label lblAgrup 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "T�tulo:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2595
         TabIndex        =   7
         Top             =   690
         Width           =   450
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2490
         TabIndex        =   6
         Top             =   345
         Width           =   555
      End
   End
   Begin VB.Frame fraBotonera 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5685
      Left            =   9750
      TabIndex        =   11
      Top             =   1440
      Width           =   1035
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   60
         TabIndex        =   13
         Top             =   5220
         Width           =   885
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   4800
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5595
      Left            =   60
      TabIndex        =   14
      Top             =   1500
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   9869
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelPet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -004- a. FJS 03.06.2009 - Se adecua para igualar al programa llamador (rptPetiTare.frm)
' -005- a. FJS 12.04.2010 - Se agregan los estados terminales ANEXAD y ANULAD.

Option Explicit

Dim flgNumero As Boolean
Dim flgTitulo As Boolean
Dim auxEstado As String

Const colNroAsignado = 0
Const colTipo = 1
Const colClase = 2
Const colTitulo = 3
Const colESTADO = 4
Const colSituacion = 5
Const colPrioridad = 6
Const colEsfuerzo = 7
Const colFinicio = 8
Const colFtermin = 9
Const colNroInterno = 10
Const colEstCod = 11
Const colSitCod = 12
Const colCodigoRecurso = 13
Const colNota = 14
Const colDerivo = 15

Private Sub Form_Load()
    flgNumero = False
    flgTitulo = False
    glAuxRetorno = ""
    cboEstado.AddItem "Peticiones en estado terminal" & ESPACIOS & ESPACIOS & "||" & "RECHAZ|RECHTE|CANCEL|TERMIN|ANEXAD|ANULAD|"     ' upd -005- a.
    cboEstado.AddItem "Peticiones en revisi�n" & ESPACIOS & ESPACIOS & "||" & "REVISA|"
    'cboEstado.AddItem "Peticiones en estado activo" & ESPACIOS & ESPACIOS & "||" & "CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|SUSPEN|REVISA|", 0
    cboEstado.AddItem "Peticiones en estado activo" & ESPACIOS & ESPACIOS & "||" & "CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|SUSPEN|", 0
    cboEstado.ListIndex = 0
    Call Status("Listo.")
    Call CargarGrid
    Call IniciarScroll(grdDatos)      ' add -003- a.
End Sub

Private Sub cmdBuscar_Click()
    If Val(txtNroBuscar) > 0 Then
        flgNumero = True
        flgTitulo = False
        txtTitulo = ""
        Call CargarGrid
    End If
End Sub

Private Sub cmdFiltro_Click()
    flgNumero = False
    flgTitulo = True
    txtNroBuscar = ""
    Call CargarGrid
End Sub

Private Sub cmdCerrar_Click()
    glAuxRetorno = ""
    'Unload Me
    Me.Hide
End Sub

Private Sub CargarGrid()
    Dim i As Long
    cmdAceptar.Enabled = False
    Call Puntero(True)
    Call Status("Cargando peticiones...")
    With grdDatos
        .visible = False
        .Clear
        '.HighLight = flexHighlightNever
        '.RowHeightMin = 255
        .cols = 13
        .Rows = 1
        .TextMatrix(0, colNroAsignado) = "Petici�n": .ColWidth(colNroAsignado) = 800
        .TextMatrix(0, colTipo) = "Tipo": .ColWidth(colTipo) = 500
        .TextMatrix(0, colClase) = "Clase": .ColWidth(colClase) = 600
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 4000: .ColAlignment(colTitulo) = flexAlignLeftCenter
        .TextMatrix(0, colESTADO) = "Estado": .ColWidth(colESTADO) = 2000
        .TextMatrix(0, colSituacion) = "Situaci�n": .ColWidth(colSituacion) = 1000
        .TextMatrix(0, colPrioridad) = "Pri.": .ColWidth(colPrioridad) = 600
        .TextMatrix(0, colEsfuerzo) = "Horas": .ColWidth(colEsfuerzo) = 600
        .TextMatrix(0, colFinicio) = "F.Ini.Planif": .ColWidth(colFinicio) = 1100
        .TextMatrix(0, colFtermin) = "F.Fin Planif": .ColWidth(colFtermin) = 1100
        .TextMatrix(0, colNroInterno) = "NroInt.": .ColWidth(colNroInterno) = 800
        .ColWidth(colEstCod) = 0
        .ColWidth(colSitCod) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If flgNumero = True Then
        If Not sp_GetUnaPeticionAsig(txtNroBuscar) Then
           aplRST.Close
           GoTo finx
        End If
    ElseIf flgTitulo = True Then
        If Not sp_GetUnaPeticionTitulo(txtTitulo, DatosCombo(cboEstado)) Then
           aplRST.Close
           GoTo finx
        End If
    Else
        If Not sp_GetUnaPeticionTitulo(txtTitulo, DatosCombo(cboEstado)) Then
           aplRST.Close
           GoTo finx
        End If
    End If
    Do While Not aplRST.EOF
        With grdDatos
            i = i + 1
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, colTipo) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, colClase) = ClearNull(aplRST!cod_clase)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
            .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colEstCod) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colSitCod) = ClearNull(aplRST!cod_situacion)
            .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
            If i Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
        End With
        DoEvents
        aplRST.MoveNext
    Loop
'    With grdDatos
'        .Col = colNroAsignado
'        .Sort = flexSortStringNoCaseDescending
'    End With
finx:
    grdDatos.visible = True
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
            .RowSel = 1
            .Col = .MouseCol
            .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                glAuxRetorno = padRight(ClearNull(.TextMatrix(.RowSel, colNroAsignado)), 6) & " : " & .TextMatrix(.RowSel, colTitulo) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & .TextMatrix(.RowSel, colNroInterno) & "|S"  ' del -004- a.
                'glAuxRetorno = padRight(ClearNull(.TextMatrix(.RowSel, colNroAsignado)), 9) & " : " & .TextMatrix(.RowSel, colTitulo) & Space(150) & " || " & .TextMatrix(.RowSel, colNroInterno) & " |S"  ' add -004- a.
                cmdAceptar.Enabled = True
            End If
        End If
        .Refresh
    End With
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            If Me.Height < 7680 Then Me.Height = 7680
            If Me.Width < 9045 Then Me.Width = 9045
        End If
        ' Ancho
        fraFiltros.Width = Me.ScaleWidth - 100
        grdDatos.Width = Me.ScaleWidth - fraBotonera.Width - 150
        fraBotonera.Left = grdDatos.Width + 100
        ' Alto
        grdDatos.Height = Me.ScaleHeight - fraFiltros.Height - 50
        fraBotonera.Height = Me.ScaleHeight - fraFiltros.Height ' - 150
        cmdCerrar.Top = fraBotonera.Height - cmdCerrar.Height - 100
    End If
    Debug.Print "Height: " & Me.Height & " / Width: " & Me.Width
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub txtNroBuscar_KeyUp(KeyCode As Integer, Shift As Integer)
    cmdAceptar.Enabled = False
End Sub

Private Sub txtTitulo_KeyUp(KeyCode As Integer, Shift As Integer)
    cmdAceptar.Enabled = False
End Sub

Private Sub cmdAceptar_Click()
    'Unload Me
    Me.Hide
End Sub

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
