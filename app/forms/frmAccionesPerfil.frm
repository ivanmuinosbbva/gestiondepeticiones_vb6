VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAccionesPerfil 
   Caption         =   "ACCIONES POR PERFIL"
   ClientHeight    =   7980
   ClientLeft      =   1755
   ClientTop       =   1545
   ClientWidth     =   11745
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7980
   ScaleWidth      =   11745
   WindowState     =   2  'Maximized
   Begin VB.CheckBox chkSoloHabilitados 
      Caption         =   "Listar s�lo permisos otorgados al perfil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7200
      TabIndex        =   10
      Top             =   120
      Width           =   3135
   End
   Begin VB.ComboBox cboFiltroPerfil 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   120
      Width           =   2655
   End
   Begin TabDlg.SSTab sstPermisos 
      Height          =   7815
      Left            =   2880
      TabIndex        =   6
      Top             =   120
      Width           =   7485
      _ExtentX        =   13203
      _ExtentY        =   13785
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmAccionesPerfil.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "grdDatosDetalle"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "grdDatos"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Especificaciones"
      TabPicture(1)   =   "frmAccionesPerfil.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "trwEstados"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cboPerfilEstado"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      Begin VB.ComboBox cboPerfilEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -74880
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   480
         Width           =   2655
      End
      Begin MSComctlLib.TreeView trwEstados 
         Height          =   6855
         Left            =   -74880
         TabIndex        =   11
         Top             =   840
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   12091
         _Version        =   393217
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "imgPermisos"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdDatos 
         Height          =   7380
         Left            =   0
         TabIndex        =   7
         Top             =   360
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   13018
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdDatosDetalle 
         Height          =   1845
         Left            =   0
         TabIndex        =   8
         Top             =   5880
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   3254
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList imgPermisos 
      Left            =   240
      Top             =   7200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":0038
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":05D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":0B6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":1106
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":16A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":1C3A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":21D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAccionesPerfil.frx":276E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trwPermisos 
      Height          =   7455
      Left            =   120
      TabIndex        =   5
      Top             =   480
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   13150
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   6
      ImageList       =   "imgPermisos"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame pnlBotones 
      Height          =   7935
      Left            =   10440
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   6780
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   7290
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Sector seleccionado"
         Top             =   6270
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector"
         Top             =   5250
         Width           =   1170
      End
   End
End
Attribute VB_Name = "frmAccionesPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. - FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.

Option Explicit

Private Const colACCION_CODIGO = 0
Private Const colACCION_NOMBRE = 1
Private Const colACCION_PERMISO = 2
Private Const colACCION_OTROS = 3
Private Const colACCION_PORESTADO = 4
Private Const colACCION_MULTIPLE = 5

Private Const colDESDE_ESTADO = 0
'Private Const colHACIA_ESTADO = 1

Private Const FONT_NAME = "Tahoma"
Private Const FONT_SIZE = 8

Private Const GRID_HEIGHT_LONG = 7380       '6420
Private Const GRID_HEIGHT_SHORT = 5460      '4410
Private Const PERMISO_SI = "�"
Private Const PERMISO_NO = "-"
Private Const SEPARADOR = " � "

Dim iOpcionSeleccionada As Integer
Dim bEnCarga As Boolean

Private Sub Form_Load()
    bEnCarga = True
    Call InicializarCombos
    Call InicializarPantalla
    Call IniciarScroll(grdDatos)
    Call IniciarScroll(grdDatosDetalle)
    Call CargarArbol
    'Call CargarArbolEstados
    bEnCarga = False
End Sub

Private Sub CargarArbolEstados()
    Call CargarEstadosNodos(Null, "CONFEC")
'    Call CargarEstadosNodos("REFERE")
'    Call CargarEstadosNodos("AUTORI")
'    Call CargarEstadosNodos("SUPERV")
End Sub

Private Sub CargarEstadosNodos(raiz, estado)
    Dim nodo As MSComctlLib.Node
    Dim perfil As String
    Dim perfilNom As String
    Dim estadoNom As String
    Dim estadosSiguientes() As String
    
    Dim nivel1_Estado As String
    Dim nivel2_Perfil As String
    Dim nivel3_NuevoEstado As String
    
    Dim i As Integer
    Dim j As Integer
    
    With trwEstados
        If estado = "CONFEC" Then .Nodes.Clear
        If sp_GetEstado(estado) Then
            estadoNom = ClearNull(aplRST.Fields!nom_estado)
        End If
        
'        If IsMissing(raiz) Then
'            Set nodo = .Nodes.Add(, , estado, estadoNom, 3): nodo.Expanded = True: nodo.Bold = True
'        Else
'            Set nodo = .Nodes.Add(raiz, tvwChild, estado, estadoNom, 3): nodo.Expanded = True: nodo.Bold = True
'        End If
        Set nodo = .Nodes.Add(, , estado, estadoNom, 3): nodo.Expanded = True: nodo.Bold = True
        
        For i = 0 To cboPerfilEstado.ListCount - 1
            perfil = CodigoCombo2(cboPerfilEstado.List(i), True)
            perfilNom = Mid(cboPerfilEstado.List(i), 1, 30)
            
            If sp_GetAccionPerfil("PCHGEST", perfil, estado, Null, Null) Then
                nivel1_Estado = estado
                nivel2_Perfil = nivel1_Estado & perfil
                'Set nodo = .Nodes.Add(estado, tvwChild, estado & perfil, perfilNom, 2): nodo.Expanded = True: nodo.Bold = True
                Set nodo = .Nodes.Add(nivel1_Estado, tvwChild, nivel2_Perfil, perfilNom, 2): nodo.Expanded = True: nodo.Bold = True
                Set nodo = .Nodes.Add(nivel2_Perfil, tvwChild, "A" & nivel2_Perfil, "cambia de estado", 4): nodo.Expanded = True: nodo.ForeColor = RGB(192, 192, 192)
                Do While Not aplRST.EOF
                    nivel3_NuevoEstado = nivel2_Perfil & ClearNull(aplRST.Fields!cod_estnew)
                    Set nodo = .Nodes.Add("A" & nivel2_Perfil, tvwChild, "B" & nivel3_NuevoEstado, ClearNull(aplRST.Fields!nom_estnew), 3): nodo.Expanded = True
                    
                    ' llamada recursiva
                    'Call CargarEstadosNodos(nivel3_NuevoEstado, ClearNull(aplRST.Fields!cod_estnew))
'                    ' Guardo los estados siguientes para volver a empezar
'                    ReDim Preserve estadosSiguientes(j)
'                    estadosSiguientes(j) = ClearNull(aplRST.Fields!nom_estnew)
                    aplRST.MoveNext
                    DoEvents
'                    j = j + 1
                Loop
            End If
        Next i
    End With
End Sub

Private Sub CargarArbol()
    Dim nodo As MSComctlLib.Node
    
    With trwPermisos
        .Nodes.Clear
        Set nodo = .Nodes.Add(, , "RAIZ", "Todos los permisos", 5): nodo.Expanded = True: nodo.Bold = True
        If sp_GetAccionesGrupo(Null, Null, "S") Then
            Do While Not aplRST.EOF
                Set nodo = .Nodes.Add("RAIZ", tvwChild, "A" & ClearNull(aplRST.Fields!accionGrupoId), ClearNull(aplRST.Fields!accionGrupoNom), 7): nodo.Expanded = True
                aplRST.MoveNext
                DoEvents
            Loop
            Call CargarGrid
        End If
    End With
End Sub

Private Sub CargarGrid()
    With grdDatos
        .visible = False
        .Clear
        .Font.name = FONT_NAME
        .Font.Size = FONT_SIZE
        .cols = 6
        .HighLight = flexHighlightNever
        .FocusRect = flexFocusNone
        .RowHeightMin = 300
        .Rows = 1
        .TextMatrix(0, colACCION_CODIGO) = "cod_accion": .ColWidth(colACCION_CODIGO) = 0
        .TextMatrix(0, colACCION_NOMBRE) = "Detalle de permisos para " & TextoCombo(cboFiltroPerfil, CodigoCombo(cboFiltroPerfil, True), True): .ColWidth(colACCION_NOMBRE) = 4300
        .TextMatrix(0, colACCION_PERMISO) = "Habilitado": .ColWidth(colACCION_PERMISO) = 1100: .ColAlignment(colACCION_PERMISO) = flexAlignCenterCenter
        .TextMatrix(0, colACCION_OTROS) = "cantidad":: .ColWidth(colACCION_OTROS) = 0
        .TextMatrix(0, colACCION_PORESTADO) = "Por estado":: .ColWidth(colACCION_PORESTADO) = 1100: .ColAlignment(colACCION_PORESTADO) = flexAlignCenterCenter
        .TextMatrix(0, colACCION_MULTIPLE) = "M�ltiples":: .ColWidth(colACCION_MULTIPLE) = 1100: .ColAlignment(colACCION_MULTIPLE) = flexAlignCenterCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetAccionPerfilConfig(IIf(iOpcionSeleccionada = 0, Null, iOpcionSeleccionada), CodigoCombo(cboFiltroPerfil, True)) Then GoTo Fin:
    Do While Not aplRST.EOF
        With grdDatos
            If (chkSoloHabilitados.Value = 0 Or (chkSoloHabilitados.Value = 1 And ClearNull(aplRST.Fields!permiso) = "S")) Then
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colACCION_CODIGO) = ClearNull(aplRST.Fields!cod_accion)
                .TextMatrix(.Rows - 1, colACCION_NOMBRE) = ClearNull(aplRST.Fields!leyenda)
                .TextMatrix(.Rows - 1, colACCION_PERMISO) = IIf(ClearNull(aplRST.Fields!permiso) = "S", PERMISO_SI, PERMISO_NO)
                .TextMatrix(.Rows - 1, colACCION_OTROS) = ClearNull(aplRST.Fields!cantidad)
                .TextMatrix(.Rows - 1, colACCION_PORESTADO) = IIf(ClearNull(aplRST.Fields!porEstado) = "S", PERMISO_SI, PERMISO_NO)
                .TextMatrix(.Rows - 1, colACCION_MULTIPLE) = IIf(ClearNull(aplRST.Fields!multiplesEstados) = "S", PERMISO_SI, PERMISO_NO)
                If ClearNull(aplRST.Fields!permiso) = "N" Then
                    .RowSel = .Rows - 1
                    Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorDarkGrey)
                End If
            End If
        End With
        aplRST.MoveNext
        DoEvents
    Loop
Fin:
    grdDatos.visible = True
End Sub

Private Sub CargarGridDetalle()
    With grdDatosDetalle
        .visible = False
        .Clear
        .Font.name = FONT_NAME
        .Font.Size = FONT_SIZE
        
        .HighLight = flexHighlightNever
        .FocusRect = flexFocusNone
        .RowHeightMin = 300
        .Rows = 1
        .cols = 1
        If grdDatos.TextMatrix(grdDatos.RowSel, colACCION_MULTIPLE) = PERMISO_SI Then
            .TextMatrix(0, colDESDE_ESTADO) = "Cambio de estado (Origen - Destino)"
        Else
            .TextMatrix(0, colDESDE_ESTADO) = "S�lo cuando se encuentre en alguno de estos estados:"
        End If
        .ColWidth(colDESDE_ESTADO) = 8000
        .ColAlignment(colDESDE_ESTADO) = flexAlignCenterCenter
        Call CambiarEfectoLinea(grdDatosDetalle, prmGridEffectFontBold)
    End With
    If Not sp_GetAccionPerfil(grdDatos.TextMatrix(grdDatos.RowSel, colACCION_CODIGO), CodigoCombo(cboFiltroPerfil, True), Null, Null, Null) Then GoTo Fin:
    Do While Not aplRST.EOF
        With grdDatosDetalle
            .Rows = .Rows + 1
            If grdDatos.TextMatrix(grdDatos.RowSel, colACCION_MULTIPLE) = PERMISO_SI Then
                .TextMatrix(.Rows - 1, colDESDE_ESTADO) = ClearNull(aplRST.Fields!nom_estado) & SEPARADOR & ClearNull(aplRST.Fields!nom_estnew)
            Else
                .TextMatrix(.Rows - 1, colDESDE_ESTADO) = ClearNull(aplRST.Fields!nom_estado)
            End If
        End With
        aplRST.MoveNext
        DoEvents
    Loop
Fin:
    grdDatosDetalle.visible = True
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colACCION_CODIGO) <> "" Then
                If .TextMatrix(.RowSel, colACCION_PORESTADO) = "�" Then
                    grdDatos.Height = GRID_HEIGHT_SHORT
                    Call CargarGridDetalle
                Else
                    grdDatosDetalle.visible = False
                    grdDatos.Height = GRID_HEIGHT_LONG
                End If
            End If
        End If
    End With
End Sub

Private Sub InicializarCombos()
    With cboFiltroPerfil
        If sp_GetPerfil(Null, "S", "S") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_perfil) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_perfil)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = 0
    End With
    
'    With cboPerfilEstado
'        If sp_GetPerfil(Null, "S", "S") Then
'            Do While Not aplRST.EOF
'                .AddItem ClearNull(aplRST.Fields!nom_perfil) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_perfil)
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        .ListIndex = 0
'    End With
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    'sOpcionSeleccionada = ""
    cboPerfilEstado.visible = False
    trwEstados.visible = False
    Call HabilitarBotones(0)
End Sub

Private Sub cmdAgregar_Click()
    'sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    'sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    'sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
'    Select Case sOpcionSeleccionada
'        Case "A"
'            If CamposObligatorios Then
'                If sp_UpdateAccionesPerfil(CodigoCombo(Me.cboAccion), CodigoCombo(Me.cboPerfil), CodigoCombo(Me.cboEstado), CodigoCombo(Me.cboEstnew)) Then
'                    Call HabilitarBotones(0)
'                End If
'            End If
'        Case "E"
'            If sp_DeleteAccionesPerfil(CodigoCombo(Me.cboAccion), CodigoCombo(Me.cboPerfil), CodigoCombo(Me.cboEstado), CodigoCombo(Me.cboEstnew)) Then
'                Call HabilitarBotones(0)
'            End If
'    End Select
End Sub

Private Sub cmdCerrar_Click()
'    Select Case sOpcionSeleccionada
'        Case "A", "E"
'            sOpcionSeleccionada = ""
'            Call HabilitarBotones(0, False)
'        Case Else
'            Unload Me
'    End Select
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            'lblSeleccion = "": lblSeleccion.Visible = False
            'fraDatos.Enabled = False
            grdDatos.Enabled = True
            'grdDatos.SetFocus
            cmdAgregar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            'sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
'            Select Case sOpcionSeleccionada
'                Case "A"
'                    'lblSeleccion = " AGREGAR ": lblSeleccion.Visible = True
'                    'cboEstnew.ListIndex = -1
'                    'cboEstado.ListIndex = -1
'                    'cboAccion.ListIndex = -1
'                    'cboPerfil.ListIndex = -1
'                    'fraDatos.Enabled = True
'                Case "E"
'                    'lblSeleccion = " ELIMINAR ": lblSeleccion.Visible = True
'                    'fraDatos.Enabled = False
'            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
End Function

Private Sub grdDatos_Click()
    If Not bEnCarga Then Call MostrarSeleccion
End Sub

Private Sub cboFiltroPerfil_Click()
    If Not bEnCarga Then CargarGrid
End Sub

Private Sub cboFiltroAccion_Click()
    If Not bEnCarga Then CargarGrid
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Private Sub trwPermisos_NodeClick(ByVal Node As MSComctlLib.Node)
    If Node.Key = "RAIZ" Then
        iOpcionSeleccionada = 0
    Else
        iOpcionSeleccionada = CInt(Mid(Node.Key, 2))
    End If
    Call CargarGrid
End Sub

Private Sub chkSoloHabilitados_Click()
    Call CargarGrid
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatosDetalle)
    Call DetenerScroll(grdDatos)
End Sub
