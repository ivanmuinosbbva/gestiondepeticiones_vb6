VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRecurso 
   Caption         =   "RECURSOS"
   ClientHeight    =   7965
   ClientLeft      =   1755
   ClientTop       =   1545
   ClientWidth     =   11850
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRecurso.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   11850
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3945
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   10515
      _ExtentX        =   18547
      _ExtentY        =   6959
      _Version        =   393216
      Cols            =   16
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraFiltros 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   0
      TabIndex        =   38
      Top             =   3960
      Width           =   10515
      Begin VB.ComboBox cmbVerRecursosEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   135
         Width           =   5175
      End
      Begin VB.Label lblCantidadRecursos 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   10410
         TabIndex        =   41
         Top             =   210
         Width           =   45
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Ver :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   39
         Top             =   195
         Width           =   375
      End
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7930
      Left            =   10560
      TabIndex        =   7
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdHoras 
         Caption         =   "Transferir horas"
         Height          =   500
         Left            =   60
         TabIndex        =   53
         Top             =   1200
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CheckBox cmdBuscarPorNombre 
         Caption         =   "B�squ&eda"
         Height          =   500
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   1920
         Width           =   1170
      End
      Begin VB.CommandButton cmdDelegados 
         Caption         =   "Ver delegados"
         Enabled         =   0   'False
         Height          =   500
         Left            =   60
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Modifica los perfiles del Recurso"
         Top             =   660
         Width           =   1170
      End
      Begin VB.CommandButton cmdQuitarDelegar 
         Caption         =   "Quitar delegaci�n"
         Height          =   500
         Left            =   60
         TabIndex        =   36
         ToolTipText     =   "Elimina la delegaci�n existente"
         Top             =   6240
         Width           =   1170
      End
      Begin VB.CommandButton cmdPerfiles 
         Caption         =   "Perfiles"
         Height          =   500
         Left            =   60
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Modifica los perfiles del Recurso"
         Top             =   150
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   7350
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Recurso seleccionado"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el Recurso seleccionado"
         Top             =   4080
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Recurso"
         Top             =   3570
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Left            =   0
      TabIndex        =   3
      Top             =   4440
      Width           =   10515
      Begin VB.Frame fraReferencias 
         Caption         =   " Referencias "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   8520
         TabIndex        =   33
         Top             =   2400
         Width           =   1935
         Begin VB.Shape shpRecursoSuspendido 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   1  'Opaque
            Height          =   255
            Left            =   1440
            Top             =   555
            Width           =   375
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Recurso suspendido"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   165
            Left            =   120
            TabIndex        =   35
            Top             =   600
            Width           =   1200
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Recurso con baja"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   165
            Left            =   120
            TabIndex        =   34
            Top             =   285
            Width           =   1050
         End
         Begin VB.Shape shpRecursoEnBaja 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   1  'Opaque
            Height          =   255
            Left            =   1440
            Top             =   240
            Width           =   375
         End
      End
      Begin VB.ComboBox cboEuser 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6300
         Style           =   2  'Dropdown List
         TabIndex        =   32
         ToolTipText     =   "Especifica si el recurso desea recibir las notificaciones a trav�s de Mail"
         Top             =   3030
         Width           =   2055
      End
      Begin VB.CheckBox chkAcargo 
         Caption         =   "A cargo del �rea"
         Height          =   255
         Left            =   7680
         TabIndex        =   28
         ToolTipText     =   "El recurso esta habilitado para imputarse horas trabajadas sin tener la tarea/proyecto asignado"
         Top             =   1688
         Width           =   1545
      End
      Begin VB.TextBox txtObservaciones 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   900
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   27
         Top             =   2370
         Width           =   7455
      End
      Begin VB.ComboBox cboDelegCod 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   2010
         Width           =   2925
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmRecurso.frx":000C
         Left            =   8430
         List            =   "frmRecurso.frx":000E
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   990
         Width           =   1900
      End
      Begin VB.ComboBox cboVinculo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8430
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   660
         Width           =   1900
      End
      Begin VB.ComboBox cboDireccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   660
         Width           =   6525
      End
      Begin VB.ComboBox cboGerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   990
         Width           =   6525
      End
      Begin VB.ComboBox cboSector 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1320
         Width           =   6525
      End
      Begin VB.ComboBox cboGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   1650
         Width           =   6525
      End
      Begin AT_MaskText.MaskText txtCodRecurso 
         Height          =   315
         Left            =   900
         TabIndex        =   1
         Top             =   330
         Width           =   1000
         _ExtentX        =   1773
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNomRecurso 
         Height          =   315
         Left            =   2820
         TabIndex        =   2
         Top             =   330
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   556
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
      End
      Begin AT_MaskText.MaskText txtHorasDiarias 
         Height          =   315
         Left            =   9930
         TabIndex        =   21
         Top             =   1350
         Width           =   405
         _ExtentX        =   714
         _ExtentY        =   556
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtFechaDesde 
         Height          =   315
         Left            =   4680
         TabIndex        =   23
         Top             =   2010
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaHasta 
         Height          =   315
         Left            =   6840
         TabIndex        =   24
         Top             =   2010
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtEmail 
         Height          =   330
         Left            =   900
         TabIndex        =   30
         Top             =   3030
         Width           =   5385
         _ExtentX        =   9499
         _ExtentY        =   582
         MaxLength       =   60
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   60
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   195
         Left            =   60
         TabIndex        =   52
         Top             =   390
         Width           =   495
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         Height          =   195
         Left            =   60
         TabIndex        =   51
         Top             =   735
         Width           =   645
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         Height          =   195
         Left            =   60
         TabIndex        =   50
         Top             =   1725
         Width           =   435
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         Height          =   195
         Left            =   60
         TabIndex        =   49
         Top             =   1395
         Width           =   465
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         Height          =   195
         Left            =   60
         TabIndex        =   48
         Top             =   1065
         Width           =   630
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Delega a"
         Height          =   195
         Left            =   60
         TabIndex        =   47
         Top             =   2085
         Width           =   630
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Observ."
         Height          =   195
         Left            =   60
         TabIndex        =   46
         Top             =   2400
         Width           =   585
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Email"
         Height          =   195
         Left            =   120
         TabIndex        =   31
         Top             =   3105
         Width           =   360
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Left            =   4080
         TabIndex        =   26
         Top             =   2070
         Width           =   450
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   6240
         TabIndex        =   25
         Top             =   2070
         Width           =   420
      End
      Begin VB.Label Label12 
         Caption         =   "Horas Diarias"
         Height          =   165
         Left            =   8700
         TabIndex        =   22
         Top             =   1425
         Width           =   1155
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         Height          =   195
         Left            =   7680
         TabIndex        =   19
         Top             =   1065
         Width           =   495
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "V�nculo"
         Height          =   195
         Left            =   7680
         TabIndex        =   17
         Top             =   750
         Width           =   495
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Left            =   2040
         TabIndex        =   5
         Top             =   390
         Width           =   555
      End
      Begin VB.Label lblSeleccion 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   10320
         TabIndex        =   4
         Top             =   120
         Visible         =   0   'False
         Width           =   45
      End
   End
   Begin VB.Frame fraSeek 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   0
      TabIndex        =   42
      Top             =   3360
      Width           =   10515
      Begin AT_MaskText.MaskText txtNombreRecursoSeek 
         Height          =   315
         Left            =   2640
         TabIndex        =   43
         Top             =   180
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   556
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
      End
      Begin VB.Label lblNombreRecursoSeek 
         AutoSize        =   -1  'True
         Caption         =   "Ingrese el nombre a buscar :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   44
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "frmRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 01.10.2007 - Se corrige en la carga de los combobox la falta del m�todo Clear (se estan multiplicando los valores repetidamente con cada edici�n de un registro).
' -004- a. FJS 13.07.2008 - Se inhabilita el evento cuando se hace la carga la primera vez en la grilla (performance).
' -005- a. FJS 09.12.2008 - Se agrega un bot�n para facilitar el remover la delegaci�n de recursos.
' -006- a. FJS 11.12.2008 - Se agrega un bot�n para sacar un reporte que liste aquellos recursos delegados.
' -007- a. FJS 12.12.2008 - Se agrega un filtro para ver el estado de los recursos.
' -008- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -009- a. FJS 11.03.2009 - Se agrega un control para evitar que se parametrice la recepci�n de correo electr�nico si no tiene definida una direcci�n "tentativamente" v�lida.
' -009- b. FJS 11.03.2009 - Se muestra completa la columna de direcci�n de correo electr�nico del recurso.
' -009- c. FJS 11.03.2009 - Se agrega la validaci�n del estado (para que no quede con estado indeterminado).
' -009- d. FJS 11.03.2009 - Se muestra la cantidad de recursos (registros) seleccionados en pantalla.
' -009- e. FJS 25.03.2009 - Se agrega un control para que no se carguen m�s horas diarias que las normales.
' -010- a. FJS 01.04.2009 - Nuevas mejoras a la est�tica de la pantalla.
' -011- a. FJS 03.09.2009 - Se modifican las llamadas a los SP que cargan las �reas para evitar mostrar y/o utilizar aquellas que ya se encuentran inhabilitadas.
' -012- a. FJS 13.10.2009 - Se agrega una advertencia para tener en cuenta las implicancias de un cambio de �rea para un recurso.
' -013- a. FJS 20.08.2010 - Se inhabilita la eliminaci�n f�sica de un recurso de la base (problemas de inconsistencia).
' -014- a. FJS 07.10.2010 - Nueva funcionalidad para b�squeda en la grilla.

Option Explicit

Private Const colCODRECURSO = 0
Private Const colNOMRECURSO = 1
Private Const colCODDIRECCION = 2
Private Const colCODGERENCIA = 3
Private Const colCODSECTOR = 4
Private Const colCODGRUPO = 5
Private Const colVINCULO = 7
Private Const colACARGO = 6
Private Const colESTADO = 8
Private Const colHORAS = 9
Private Const colDELEGACOD = 10
Private Const colDELEGADES = 11
Private Const colDELEGAHAS = 12
Private Const colOBSERV = 13
Private Const colEmail = 14
Private Const colEuser = 15

Dim vDireccion() As String
Dim vGerencia() As String
Dim vSector() As String
Dim vGrupo() As String

Dim sDireccionActual As String
Dim sGerenciaActual As String
Dim SSectorActual As String
Dim SGrupoActual As String

Dim sOpcionSeleccionada As String
Dim inCarga As Boolean
Dim bCambioEstructura As Boolean

'Private Sub Command1_Click()
'    Load frmRecursoEstructura
'    frmRecursoEstructura.Show
'End Sub

Private Sub Form_Load()
    LockProceso (True)
    shpRecursoEnBaja.BackColor = RGB(250, 177, 163)
    shpRecursoEnBaja.BorderColor = vbRed
    shpRecursoSuspendido.BackColor = RGB(254, 233, 218)
    shpRecursoSuspendido.BorderColor = RGB(251, 158, 102)
    'Show
    DoEvents
    bCambioEstructura = False
    inCarga = True
    Call InicializarCombosCompletos
    Call InicializarCombos
    Call InicializarPantalla
    LockProceso (False)
    Call IniciarScroll(grdDatos)
    inCarga = False
    Show
End Sub

Private Sub Form_Resize()
    Me.WindowState = vbMaximized
End Sub

Private Sub InicializarCombosCompletos()
    Dim i As Long
    
    If sp_GetDireccion("") Then
        If Not aplRST.EOF Then Erase vDireccion
        Do While Not aplRST.EOF
            ReDim Preserve vDireccion(i)
            vDireccion(i) = aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    i = 0
    
    If sp_GetGerencia("", "") Then
        If Not aplRST.EOF Then Erase vGerencia
        Do While Not aplRST.EOF
            ReDim Preserve vGerencia(i)
            vGerencia(i) = aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    i = 0
    
    If sp_GetSector("", "") Then
        If Not aplRST.EOF Then Erase vSector
        Do While Not aplRST.EOF
            ReDim Preserve vSector(i)
            vSector(i) = aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    i = 0
    
    If sp_GetGrupo("", "") Then
        If Not aplRST.EOF Then Erase vGrupo
        Do While Not aplRST.EOF
            ReDim Preserve vGrupo(i)
            vGrupo(i) = aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    If sp_GetRecurso("", "R", "A") Then
        If Not aplRST.EOF Then cboDelegCod.Clear
        Do While Not aplRST.EOF
            cboDelegCod.AddItem ClearNull(aplRST(1)) & Space(40) & "||" & ClearNull(aplRST(0))
            aplRST.MoveNext
        Loop
    End If
    
    With cmbVerRecursosEstado
        .Clear
        .AddItem "Ver todos los recursos" & Space(100) & "||NULL"
        .AddItem "Solo activos" & Space(100) & "||A"
        .AddItem "Solo suspendidos" & Space(100) & "||S"
        .AddItem "Solo dados de baja" & Space(100) & "||B"
        inCarga = True
        .ListIndex = 1
        inCarga = False
    End With
    
    With cboEstado
        .Clear
        .AddItem "A : ACTIVO"
        .AddItem "S : SUSPENDIDO"
        .AddItem "B : BAJA"
    End With
    With cboVinculo
        .Clear
        .AddItem "E : EFECTIVO"
        .AddItem "C : CONTRATADO"
        '.AddItem "F : FSW/OTROS"
    End With
    With cboEuser
        .Clear
        .AddItem "S : Recibe email"
        .AddItem "N : No recibe"
    End With
End Sub

Private Sub InicializarCombos()
    Dim i As Long
    
    Call Puntero(True)
    
    With cboDireccion
        .Clear
        For i = 0 To UBound(vDireccion)
            .AddItem vDireccion(i)
        Next i
    End With
    
    With cboGerencia
        .Clear
        For i = 0 To UBound(vGerencia)
            .AddItem vGerencia(i)
        Next i
    End With

    With cboSector
        .Clear
        For i = 0 To UBound(vSector)
            .AddItem vSector(i)
        Next i
    End With

    With cboGrupo
        .Clear
        For i = 0 To UBound(vGrupo)
            .AddItem vGrupo(i)
        Next i
    End With
    Call Puntero(False)
End Sub

Private Sub InicializarCombosEdicion()
    Dim sCodigo As String
        
    cboDireccion.visible = False
    cboGerencia.visible = False
    cboSector.visible = False
    cboGrupo.visible = False
    
    Call Puntero(True)
    With grdDatos
        If sp_GetDireccion("", "S") Then
            If Not aplRST.EOF Then cboDireccion.Clear
            Do While Not aplRST.EOF
                cboDireccion.AddItem aplRST(0) & " : " & Trim(aplRST(1))
                aplRST.MoveNext
            Loop
            If sOpcionSeleccionada = "M" Then
                cboDireccion.ListIndex = PosicionCombo(cboDireccion, .TextMatrix(.RowSel, colCODDIRECCION), False)
                sCodigo = .TextMatrix(.RowSel, colCODDIRECCION)
            ElseIf sOpcionSeleccionada = "A" Then
                cboDireccion.ListIndex = 0
                sCodigo = CodigoCombo(cboDireccion)
            End If
        End If
        'If sp_GetGerencia("", "", "S") Then
        If sp_GetGerencia("", sCodigo, "S") Then
            If Not aplRST.EOF Then cboGerencia.Clear
            Do While Not aplRST.EOF
                cboGerencia.AddItem aplRST(0) & " : " & Trim(aplRST(1))
                aplRST.MoveNext
            Loop
            cboGerencia.ListIndex = PosicionCombo(cboGerencia, .TextMatrix(.RowSel, colCODGERENCIA), False)
            If cboGerencia.ListCount < 1 Then Call setHabilCtrl(cboGerencia, DISABLE)
            sCodigo = CodigoCombo(cboGerencia)
        End If
        'If sp_GetSector("", "", "S") Then
        If sp_GetSector("", sCodigo, "S") Then
            If Not aplRST.EOF Then cboSector.Clear
            Do While Not aplRST.EOF
                cboSector.AddItem aplRST(0) & " : " & Trim(aplRST(1))
                aplRST.MoveNext
            Loop
            cboSector.ListIndex = PosicionCombo(cboSector, .TextMatrix(.RowSel, colCODSECTOR), False)
            If cboSector.ListCount < 1 Then Call setHabilCtrl(cboSector, DISABLE)
            sCodigo = CodigoCombo(cboSector)
        End If
        'If sp_GetGrupo("", "", "S") Then
        If sp_GetGrupo("", sCodigo, "S") Then
            If Not aplRST.EOF Then cboGrupo.Clear
            Do While Not aplRST.EOF
                cboGrupo.AddItem aplRST(0) & " : " & Trim(aplRST(1))
                aplRST.MoveNext
            Loop
            cboGrupo.ListIndex = PosicionCombo(cboGrupo, .TextMatrix(.RowSel, colCODGRUPO), False)
            If cboGrupo.ListCount < 1 Then Call setHabilCtrl(cboGrupo, DISABLE)
        End If
    End With
    
    cboDireccion.visible = True
    cboGerencia.visible = True
    cboSector.visible = True
    cboGrupo.visible = True
    Call Puntero(False)
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Call HabilitarBotones(0)
End Sub

Private Sub cmbVerRecursosEstado_Click()
    If Not inCarga Then
        If cmbVerRecursosEstado.ListIndex = -1 Then
            cmbVerRecursosEstado.ListIndex = 0
        Else
            CargarGrid
        End If
    End If
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
           'cmdPerfiles.Enabled = False 'GMT01
            cmdPerfiles.Enabled = True  'GMT01
            
            cmdBuscarPorNombre.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdQuitarDelegar.Enabled = False     ' add -005- a.
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            
          '   If InPerfil("ASEG") Then 'GMT01
                Call setHabilCtrl(cmdPerfiles, NORMAL)
          '  End If 'GMT01
            Call InicializarCombos
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
            Call setHabilCtrl(cmbVerRecursosEstado, "NOR")  ' add -009- e.
        Case 1
            grdDatos.Enabled = False
            cmdPerfiles.Enabled = False
            cmdBuscarPorNombre.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            If cboDelegCod.ListIndex > -1 Then cmdQuitarDelegar.Enabled = True
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    txtCodRecurso = ""
                    txtNomRecurso = ""
                    txtObservaciones = ""
                    cboDireccion.ListIndex = -1
                    cboGerencia.ListIndex = -1
                    cboSector.ListIndex = -1
                    cboGrupo.ListIndex = -1
                    cboVinculo.ListIndex = SetCombo(cboVinculo, "E", False)
                    cboEstado.ListIndex = SetCombo(cboEstado, "A", False)
                    cboEuser.ListIndex = SetCombo(cboEuser, "N", False)
                    txtEmail.text = ""
                    cboDelegCod.ListIndex = -1
                    txtFechaDesde.text = ""
                    txtFechaHasta.text = ""
                    txtHorasDiarias.text = "8"
                    chkAcargo.value = 0
                    txtCodRecurso.Enabled = True
                    Call setHabilCtrl(cmbVerRecursosEstado, "DIS")  ' add -009- e.
                    Call InicializarCombosEdicion
                    fraDatos.Enabled = True
                    txtCodRecurso.SetFocus
                Case "M"
                    bCambioEstructura = False
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    txtCodRecurso.Enabled = False
                    Call setHabilCtrl(cmbVerRecursosEstado, "DIS")  ' add -009- e.
                    Call InicializarCombosEdicion
                    fraDatos.Enabled = True
                    txtNomRecurso.SetFocus
                Case "E"
                    Call setHabilCtrl(cmbVerRecursosEstado, "DIS")  ' add -009- e.
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Private Sub cboDelegCod_Click()
    If cboDelegCod.ListIndex >= 0 Then
        If cboDelegCod.Enabled Then cmdQuitarDelegar.Enabled = True     ' add -003- a.
        If IsNull(txtFechaDesde.DateValue) Then
            txtFechaDesde.text = Format(date, "dd/mm/yyyy")
        End If
    Else
        txtFechaDesde.text = ""
        txtFechaHasta.text = ""
        cmdQuitarDelegar.Enabled = False    ' add -003- a.
    End If
End Sub

Private Sub cboDireccion_Click()
    Dim xAux As String
    If inCarga Then
        Exit Sub
    End If
    If cboDireccion.ListIndex >= 0 Then
        xAux = CodigoCombo(Me.cboDireccion)
        cboSector.Clear
        cboGrupo.Clear
        cargaGerencias (xAux)
    End If
End Sub

Private Sub cboGerencia_Click()
    Dim xAux As String
    If inCarga Then
        Exit Sub
    End If
    If cboGerencia.ListIndex >= 0 Then
        xAux = CodigoCombo(Me.cboGerencia)
        cboGrupo.Clear
        cargaSector (xAux)
    End If
End Sub

Private Sub cboSector_Click()
Dim xAux As String
    If inCarga Then
        Exit Sub
    End If
    If cboSector.ListIndex >= 0 Then
        xAux = CodigoCombo(Me.cboSector)
        cargaGrupo (xAux)
    End If
End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdPerfiles_Click()
    If Not LockProceso(True) Then Exit Sub
    frmRecursoPerfil.Show 1
    DoEvents
    LockProceso (False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()
    Dim bTienePerfil As Boolean
    Dim bContinuar As Boolean
    
    If Not LockProceso(True) Then Exit Sub
    Dim sAux As String
    Dim oAux As String
    
    sAux = txtCodRecurso.text
    oAux = txtObservaciones.text
    OutTab oAux
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertRecurso(txtCodRecurso, txtNomRecurso, CodigoCombo(Me.cboVinculo), CodigoCombo(Me.cboDireccion), CodigoCombo(Me.cboGerencia), CodigoCombo(Me.cboSector), CodigoCombo(Me.cboGrupo), IIf(chkAcargo.value = 1, "S", ""), CodigoCombo(Me.cboEstado), txtHorasDiarias.text, ClearNull(oAux), CodigoCombo(cboDelegCod, True), txtFechaDesde.DateValue, txtFechaHasta.DateValue, txtEmail, CodigoCombo(Me.cboEuser)) Then
                    Call InicializarCombos
                    Call HabilitarBotones(0)
                    Call GridSeek(Me, grdDatos, colCODRECURSO, sAux, True)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                bTienePerfil = False
                bContinuar = True
                If sp_GetRecursoPerfil(txtCodRecurso, Null) Then
                    'bTienePerfil = True
                    'bContinuar = False
                    Do While Not aplRST.EOF
                        If (ClearNull(aplRST.Fields!cod_nivel) = "GRUP" And ClearNull(aplRST.Fields!cod_area) <> CodigoCombo(cboGrupo)) Or _
                            (ClearNull(aplRST.Fields!cod_nivel) = "SECT" And ClearNull(aplRST.Fields!cod_area) <> CodigoCombo(cboSector)) Or _
                            (ClearNull(aplRST.Fields!cod_nivel) = "GERE" And ClearNull(aplRST.Fields!cod_area) <> CodigoCombo(cboGerencia)) Or _
                            (ClearNull(aplRST.Fields!cod_nivel) = "DIRE" And ClearNull(aplRST.Fields!cod_area) <> CodigoCombo(cboDireccion)) Then
                            bTienePerfil = True
                            bContinuar = False
                            Exit Do
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    
                End If
                If bTienePerfil Then
                    bContinuar = True
                    MsgBox "El recurso tiene perfiles definidos. Se actualizar�n" & vbCrLf & "los mismos. Revise luego los cambios realizados.", vbInformation + vbOKOnly
'                    If MsgBox("El recurso tiene perfiles definidos. Se actualizar�n" & vbCrLf & _
'                              "los mismos. Revise luego los cambios realizados.", vbInformation + vbOKCancel) = vbOK Then
'                        bContinuar = True
'                    End If
                End If
                If bContinuar Then
                    If sp_UpdateRecurso(txtCodRecurso, txtNomRecurso, CodigoCombo(Me.cboVinculo), CodigoCombo(Me.cboDireccion), CodigoCombo(Me.cboGerencia), CodigoCombo(Me.cboSector), CodigoCombo(Me.cboGrupo), IIf(chkAcargo.value = 1, "S", ""), CodigoCombo(Me.cboEstado), txtHorasDiarias.text, ClearNull(oAux), CodigoCombo(cboDelegCod, True), txtFechaDesde.DateValue, txtFechaHasta.DateValue, txtEmail, CodigoCombo(Me.cboEuser)) Then
                        With grdDatos
                            If .RowSel > 0 Then
                               .TextMatrix(.RowSel, colNOMRECURSO) = txtNomRecurso
                               .TextMatrix(.RowSel, colCODDIRECCION) = CodigoCombo(Me.cboDireccion)
                               .TextMatrix(.RowSel, colCODGERENCIA) = CodigoCombo(Me.cboGerencia)
                               .TextMatrix(.RowSel, colCODSECTOR) = CodigoCombo(Me.cboSector)
                               .TextMatrix(.RowSel, colCODGRUPO) = CodigoCombo(Me.cboGrupo)
                               .TextMatrix(.RowSel, colVINCULO) = CodigoCombo(Me.cboVinculo)
                               .TextMatrix(.RowSel, colESTADO) = CodigoCombo(Me.cboEstado)
                               .TextMatrix(.RowSel, colACARGO) = IIf(chkAcargo.value = 1, "S", "")
                               .TextMatrix(.RowSel, colHORAS) = txtHorasDiarias
                               .TextMatrix(.RowSel, colOBSERV) = ClearNull(txtObservaciones.text)
                               .TextMatrix(.RowSel, colDELEGACOD) = CodigoCombo(cboDelegCod, True)
                               .TextMatrix(.RowSel, colDELEGADES) = IIf(Not IsNull(txtFechaDesde), Format(txtFechaDesde, "dd/mm/yyyy"), "")
                               .TextMatrix(.RowSel, colDELEGAHAS) = IIf(Not IsNull(txtFechaHasta), Format(txtFechaHasta, "dd/mm/yyyy"), "")
                               .TextMatrix(.RowSel, colEmail) = ClearNull(txtEmail.text)
                               .TextMatrix(.RowSel, colEuser) = CodigoCombo(Me.cboEuser)
                               '{ add -004- a.
                               Select Case CodigoCombo(Me.cboEstado)
                                   Case "B": Call PintarLinea(grdDatos, prmGridFillRowColorRed)
                                   Case "S": Call PintarLinea(grdDatos, prmGridFillRowColorLightOrange)
                                   'Case Else
                                   '    Call PintarLinea(grdDatos)
                               End Select
                               '}
                            End If
                        End With
                        'Call InicializarCombos
                        'Call HabilitarBotones(0, False)
                    End If
                    ' Llamar al sp que realiza el registro del cambio de estructura para el recurso
                    If bCambioEstructura Then Call sp_UpdateRecursoEstructura(txtCodRecurso, cboDireccion.Tag, cboGerencia.Tag, cboSector.Tag, cboGrupo.Tag)
                    Call InicializarCombos
                    Call HabilitarBotones(0, False)
                End If
                
            End If
        Case "E"
            If sp_DeleteRecurso(txtCodRecurso) Then
                Call HabilitarBotones(0)
            End If
    End Select
    LockProceso (False)
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call Puntero(True)
    Call Status("Cancelando...")
    Select Case sOpcionSeleccionada
        Case "A", "M"
            'Call InicializarCombos
            sOpcionSeleccionada = ""
            Call InicializarCombos
            Call HabilitarBotones(0, False)
            Call Status("Listo.")
        Case "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
            Call Status("Listo.")
        Case Else
            Call Puntero(False)
            Call Status("Listo.")
            Unload Me
    End Select
    LockProceso (False)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    ' Control: C�digo de recurso
    If Trim(txtCodRecurso.text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodRecurso.text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    ' Control: Nombre y apellido del recurso
    If Trim(txtNomRecurso.text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtNomRecurso.text, ":") > 0 Then
        MsgBox ("La Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    ' Control: Direcci�n
    If cboDireccion.ListIndex < 0 Then
        MsgBox ("Debe asignarle una Direccion")
        CamposObligatorios = False
        Exit Function
    End If
    If Not ValidaSectores Then
        CamposObligatorios = False
        Exit Function
    End If
    If Not ValidaDelegado Then
        CamposObligatorios = False
        Exit Function
    End If
    '{ add -009- a.
    'If CodigoCombo(cboVinculo, False) <> "F" Then
    If Not ValidaCorreo Then
        CamposObligatorios = False
        Exit Function
    End If
    'End If
    '}
    '{ add -009- c.
    If CodigoCombo(cboEstado, False) = "" Then
        MsgBox "Debe establecer el estado del recurso", vbExclamation + vbOKOnly, "Estado del recurso"
        CamposObligatorios = False
        Exit Function
    End If
    '}
    '{ add -009- e.
    If Val(txtHorasDiarias.text) > 24 Then
        MsgBox "No puede establecer m�s de 24 horas diarias para el recurso.", vbExclamation + vbOKOnly, "Horas nominales del recurso"
        CamposObligatorios = False
        Exit Function
    End If
    
    If CodigoCombo(cboVinculo, False) = "" Then
        MsgBox "Debe establecer el vinculo del recurso", vbExclamation + vbOKOnly, "V�nculo del recurso"
        CamposObligatorios = False
        Exit Function
    End If
    '}
    
    ' Valida el cambio de estructura
    If cboDireccion.Tag <> CodigoCombo(cboDireccion) Or _
        cboGerencia.Tag <> CodigoCombo(cboGerencia) Or _
        cboSector.Tag <> CodigoCombo(cboSector) Or _
        cboGrupo.Tag <> CodigoCombo(cboGrupo) Then
            'MsgBox "Cambio en la estructura!", vbInformation
            bCambioEstructura = True
    End If
End Function

Private Sub cargaGerencias(sDire As String)
    cboGerencia.Enabled = False
    cboGerencia.Clear
    If sp_GetGerencia("", sDire, "S") Then      ' upd -011- a. Se agrega el par�metro opcional "S" para eviat cargar los inhabilitados
        Do While Not aplRST.EOF
            cboGerencia.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
    If ClearNull(cboGerencia.Tag) <> "" Then
        cboGerencia.ListIndex = PosicionCombo(cboGerencia, ClearNull(cboGerencia.Tag))
    End If
    If cboGerencia.ListCount < 1 Then
        Call setHabilCtrl(cboGerencia, DISABLE)
    Else
        Call setHabilCtrl(cboGerencia, NORMAL)
    End If
    cboGerencia.Enabled = True
End Sub

Private Sub cargaSector(sGERE As String)
    cboSector.Clear
    If sp_GetSector("", sGERE, "S") Then        ' upd -011- a. Se agrega el par�metro opcional "S" para eviat cargar los inhabilitados
        Do While Not aplRST.EOF
            cboSector.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
    If ClearNull(cboSector.Tag) <> "" Then
        cboSector.ListIndex = PosicionCombo(cboSector, ClearNull(cboSector.Tag))
    End If
    If cboSector.ListCount < 1 Then
        Call setHabilCtrl(cboSector, DISABLE)
    Else
        Call setHabilCtrl(cboSector, NORMAL)
    End If
End Sub

Private Sub cargaGrupo(sSECT As String)
    cboGrupo.Clear
    If sp_GetGrupo("", sSECT, "S") Then         ' upd -011- a. Se agrega el par�metro opcional "S" para eviat cargar los inhabilitados
        Do While Not aplRST.EOF
            cboGrupo.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
    If ClearNull(cboGrupo.Tag) <> "" Then
        cboGrupo.ListIndex = PosicionCombo(cboGrupo, ClearNull(cboGrupo.Tag))
    End If
    If cboGrupo.ListCount < 1 Then
        Call setHabilCtrl(cboGrupo, DISABLE)
    Else
        Call setHabilCtrl(cboGrupo, NORMAL)
    End If
End Sub

Private Sub CargarGrid()
    Call Puntero(True)
    
    inCarga = True
    With grdDatos
        fraSeek.visible = False ' add -014- a.
        .visible = False
        DoEvents
        .Clear
        .Rows = 1
        .FocusRect = flexFocusNone
        .TextMatrix(0, colCODRECURSO) = "Codigo": .ColWidth(colCODRECURSO) = 930
        .TextMatrix(0, colNOMRECURSO) = "Nombre": .ColWidth(colNOMRECURSO) = 2500
        .TextMatrix(0, colCODDIRECCION) = "Direcc": .ColWidth(colCODDIRECCION) = 900
        .TextMatrix(0, colCODGERENCIA) = "Geren": .ColWidth(colCODGERENCIA) = 900
        .TextMatrix(0, colCODSECTOR) = "Sector": .ColWidth(colCODSECTOR) = 900
        .TextMatrix(0, colCODGRUPO) = "Grup": .ColWidth(colCODGRUPO) = 900
        .TextMatrix(0, colACARGO) = "a Cargo": .ColWidth(colACARGO) = 500
        .TextMatrix(0, colVINCULO) = "Vinc": .ColWidth(colVINCULO) = 500
        .TextMatrix(0, colESTADO) = "Est": .ColWidth(colESTADO) = 500
        .TextMatrix(0, colHORAS) = "Horas": .ColWidth(colHORAS) = 500
        .TextMatrix(0, colOBSERV) = "Observ.": .ColWidth(colOBSERV) = 0
        .TextMatrix(0, colDELEGACOD) = "Delega": .ColWidth(colDELEGACOD) = 900
        .TextMatrix(0, colDELEGADES) = "Desde": .ColWidth(colDELEGADES) = 1200
        .TextMatrix(0, colDELEGAHAS) = "Hasta": .ColWidth(colDELEGAHAS) = 1200
        .TextMatrix(0, colEmail) = "Direcci�n de correo electr�nico": .ColWidth(colEmail) = 4000   ' add -009- b.
        .TextMatrix(0, colEuser) = "Hab. @": .ColWidth(colEuser) = 300
        CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    End With
    'If Not sp_GetRecurso("", "R", cFiltroRecurso) Then GoTo finx            ' upd -007- a.     ' del -009- d.
    '{ add -009- d.
    If Not sp_GetRecurso("", "R", CodigoCombo(cmbVerRecursosEstado, True)) Then
        lblCantidadRecursos = "No hay recursos para mostrar."
        GoTo finx
    End If

    If aplRST.RecordCount = 1 Then
        lblCantidadRecursos = "Un recurso mostrado."
    Else
        lblCantidadRecursos = aplRST.RecordCount & " recurso(s) mostrado(s)."
    End If
    '}
    'Call ObtenerTiempo(True)
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODRECURSO) = ClearNull(aplRST.Fields.Item("cod_recurso"))
            .TextMatrix(.Rows - 1, colNOMRECURSO) = ClearNull(aplRST.Fields.Item("nom_recurso"))
            .TextMatrix(.Rows - 1, colCODDIRECCION) = ClearNull(aplRST.Fields.Item("cod_direccion"))
            .TextMatrix(.Rows - 1, colCODGERENCIA) = ClearNull(aplRST.Fields.Item("cod_gerencia"))
            .TextMatrix(.Rows - 1, colCODSECTOR) = ClearNull(aplRST.Fields.Item("cod_sector"))
            .TextMatrix(.Rows - 1, colCODGRUPO) = ClearNull(aplRST.Fields.Item("cod_grupo"))
            .TextMatrix(.Rows - 1, colVINCULO) = ClearNull(aplRST.Fields.Item("vinculo"))
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST.Fields.Item("estado_recurso"))
            Select Case aplRST.Fields.Item("estado_recurso")
                Case "B": PintarLinea grdDatos, prmGridFillRowColorRed
                Case "S": PintarLinea grdDatos, prmGridFillRowColorLightOrange
            End Select
            .TextMatrix(.Rows - 1, colACARGO) = IIf(ClearNull(aplRST.Fields.Item("flg_cargoarea")) = "", "", ClearNull(aplRST.Fields.Item("flg_cargoarea")))
            .TextMatrix(.Rows - 1, colHORAS) = ClearNull(aplRST.Fields.Item("horasdiarias"))
            .TextMatrix(.Rows - 1, colOBSERV) = ClearNull(aplRST.Fields.Item("observaciones"))
            .TextMatrix(.Rows - 1, colEmail) = ClearNull(aplRST.Fields.Item("email"))
            .TextMatrix(.Rows - 1, colEuser) = ClearNull(aplRST.Fields.Item("euser"))
            .TextMatrix(.Rows - 1, colDELEGACOD) = ClearNull(aplRST.Fields.Item("dlg_recurso"))
            .TextMatrix(.Rows - 1, colDELEGADES) = IIf(Not IsNull(aplRST!dlg_desde), Format(aplRST!dlg_desde, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colDELEGAHAS) = IIf(Not IsNull(aplRST!dlg_hasta), Format(aplRST!dlg_hasta, "dd/mm/yyyy"), "")
            'If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
        End With
        aplRST.MoveNext
    Loop
    'Call ObtenerTiempo(False)
finx:
    With grdDatos
        .visible = True
        fraSeek.visible = True
    End With
    DoEvents
    Call Puntero(False)
    Call MostrarSeleccion
    inCarga = False
End Sub

Public Function MostrarSeleccion(Optional flgSort)
    Dim nRow As Integer
    
    inCarga = True
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    With grdDatos
        nRow = .RowSel
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, 0) <> "" Then
                txtCodRecurso = ClearNull(.TextMatrix(.RowSel, colCODRECURSO))
                txtNomRecurso = ClearNull(.TextMatrix(.RowSel, colNOMRECURSO))
                cboDireccion.ListIndex = PosicionCombo(Me.cboDireccion, .TextMatrix(.RowSel, colCODDIRECCION))
                cboGerencia.ListIndex = PosicionCombo(Me.cboGerencia, .TextMatrix(.RowSel, colCODGERENCIA))
                cboSector.ListIndex = PosicionCombo(Me.cboSector, .TextMatrix(.RowSel, colCODSECTOR))
                cboGrupo.ListIndex = PosicionCombo(Me.cboGrupo, .TextMatrix(.RowSel, colCODGRUPO))
                cboVinculo.ListIndex = PosicionCombo(Me.cboVinculo, .TextMatrix(.RowSel, colVINCULO))
                cboEstado.ListIndex = PosicionCombo(Me.cboEstado, .TextMatrix(.RowSel, colESTADO))
                cboDelegCod.ListIndex = PosicionCombo(cboDelegCod, .TextMatrix(.RowSel, colDELEGACOD), True)
                txtHorasDiarias = ClearNull(.TextMatrix(.RowSel, colHORAS))
                txtObservaciones = ClearNull(.TextMatrix(.RowSel, colOBSERV))
                txtEmail = ClearNull(.TextMatrix(.RowSel, colEmail))
                cboEuser.ListIndex = PosicionCombo(Me.cboEuser, .TextMatrix(.RowSel, colEuser))
                chkAcargo.value = IIf(.TextMatrix(.RowSel, colACARGO) = "S", 1, 0)
                txtFechaDesde.text = ClearNull(.TextMatrix(.RowSel, colDELEGADES))
                txtFechaHasta.text = ClearNull(.TextMatrix(.RowSel, colDELEGAHAS))
                ' Para validar el cambio de sector
                cboDireccion.Tag = .TextMatrix(.RowSel, colCODDIRECCION)
                cboGerencia.Tag = .TextMatrix(.RowSel, colCODGERENCIA)
                cboSector.Tag = .TextMatrix(.RowSel, colCODSECTOR)
                cboGrupo.Tag = .TextMatrix(.RowSel, colCODGRUPO)
                
'                sDireccionActual = ""
'                sGerenciaActual = ""
'                SSectorActual = ""
'                SGrupoActual = ""
            End If
        End If
    End With
    inCarga = False
End Function

Private Sub grdDatos_Click()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
            If .Rows > 0 Then
                .row = 1: .Col = 0: .RowSel = 1: .ColSel = 15
            End If
        End If
    End With
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Private Sub grdDatos_RowColChange()
    'If Not bCargandoFiltro Then
    If Not inCarga Then
        Call MostrarSeleccion
    End If
End Sub

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton Then
         Call MenuGridFind(grdDatos)
    End If
End Sub

Private Function ValidaCorreo() As Boolean
    If Len(cboEuser) = 0 Then
        MsgBox "Debe especificar las opciones de correo para el recurso" & vbCrLf & "(esta opci�n se encuentra junto a la direcci�n de email)", vbExclamation + vbOKOnly, "Opciones de correo"
        ValidaCorreo = False
        Exit Function
    End If
    If Len(txtEmail) = 0 And CodigoCombo(cboEuser, False) = "S" Then
        MsgBox "No se ha establecido una direcci�n de correo electr�nico" & vbCrLf & "para que el recurso reciba notificaciones de correo. Revise.", vbExclamation + vbOKOnly, "Direcci�n de correo electr�nico"
        ValidaCorreo = False
        Exit Function
    End If
    If CodigoCombo(cboEuser, False) = "S" And Not Validar_Email(txtEmail) Then
        If MsgBox("La direcci�n de correo electr�nico definida parece no ser una direcci�n v�lida." & vbCrLf & "�Desea continuar de todos modos?", vbExclamation + vbYesNo, "Direcci�n de correo electr�nico") <> vbYes Then
            ValidaCorreo = False
            Exit Function
        End If
    End If
    ValidaCorreo = True
End Function

Private Function ValidaDelegado() As Boolean
    Dim xVal As Integer
    ValidaDelegado = False
    xVal = 0
    If cboDelegCod.ListIndex > -1 Then
        xVal = xVal + 1
    End If
    If Not IsNull(txtFechaDesde.DateValue) Then
        xVal = xVal + 1
    End If
    If Not IsNull(txtFechaHasta.DateValue) Then
        xVal = xVal + 1
    End If
    If ClearNull(txtCodRecurso) = CodigoCombo(cboDelegCod, True) Then       ' Se controla que no se delegue un recurso para �l mismo
        MsgBox ("El recurso no puede delegarse en �l mismo")
        ValidaDelegado = False
        Exit Function
    End If
    If xVal = 0 Then
        ValidaDelegado = True
        Exit Function
    End If
    If xVal <> 3 Then
        MsgBox ("Faltan datos de delegaci�n")
        ValidaDelegado = False
        Exit Function
    End If
    If txtFechaHasta.DateValue < txtFechaDesde.DateValue Then
        MsgBox ("Error fechas delegaci�n")
        ValidaDelegado = False
        Exit Function
    End If
    ValidaDelegado = True
End Function

Function ValidaSectores() As Boolean
    Dim xDir, xGer, xSec, xSub As String
    Dim sDir, sGer, sSec As String

    ValidaSectores = False

    xDir = ClearNull(CodigoCombo(Me.cboDireccion))
    xGer = ClearNull(CodigoCombo(Me.cboGerencia))
    xSec = ClearNull(CodigoCombo(Me.cboSector))
    xSub = ClearNull(CodigoCombo(Me.cboGrupo))

    If xSub <> "" Then
        If sp_GetGrupo(xSub, "") Then
            sSec = ClearNull(aplRST!cod_sector)
            aplRST.Close
            If xSec <> "" And xSec <> sSec Then
                MsgBox ("El Grupo no pertenece al Sector")
                Exit Function
            End If
        Else
            MsgBox ("Error en Grupo")
            Exit Function
        End If
    End If

    If xSec <> "" Then
        If sp_GetSector(xSec, "") Then
            sGer = ClearNull(aplRST!cod_gerencia)
            aplRST.Close
            If xGer <> "" And xGer <> sGer Then
                MsgBox ("El Sector no pertenece a la Gerencia")
                Exit Function
            End If
        Else
            MsgBox ("Error en Sector")
            Exit Function
        End If
    End If

    If xGer <> "" Then
        If sp_GetGerencia(xGer, "") Then
            sDir = ClearNull(aplRST!cod_direccion)
            aplRST.Close
            If xDir <> "" And xDir <> sDir Then
                MsgBox ("La Gerencia no pertenece a la Direcci�n")
                Exit Function
            End If
        Else
            MsgBox ("Error en Gerencia")
            Exit Function
        End If
    End If
    ValidaSectores = True
End Function

Private Sub txtFechaDesde_LostFocus()
    If IsNull(txtFechaDesde.DateValue) Then
        txtFechaHasta.text = ""
    End If
End Sub

Private Sub cmdQuitarDelegar_Click()
    cboDelegCod.ListIndex = -1
    txtFechaDesde.text = ""
    txtFechaHasta.text = ""
    txtObservaciones.SetFocus
End Sub

''{ add -006- a.
'Private Sub cmdDelegados_Click()
'    Dim cPathFileNameReport As String
'
'    With mdiPrincipal.CrystalReport1
'        cPathFileNameReport = App.Path & "\" & "recudelega.rpt"
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .RetrieveStoredProcParams
'        .Connect = "DS N=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='Recursos que han delegado sus funciones'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'    End With
'End Sub
''}

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

'{ add -014- a.
Private Sub cmdBuscarPorNombre_Click()
    If cmdBuscarPorNombre.value = 1 Then
        txtNombreRecursoSeek.text = ""
        grdDatos.Height = 3345
        txtNombreRecursoSeek.SetFocus
    Else
        grdDatos.Height = 3945
        grdDatos.SetFocus
    End If
End Sub

Private Sub txtNombreRecursoSeek_Change()
    Call GridSeek2(Me, grdDatos, 1, txtNombreRecursoSeek, True)
End Sub
'}

Private Sub cboVinculo_Click()
'    If CodigoCombo(cboVinculo, False) = "F" Then
'        chkAcargo.Value = 0
'        cboDelegCod.ListIndex = -1
'        txtFechaDesde = ""
'        txtFechaHasta = ""
'        txtEmail = ""
'        cboEuser.ListIndex = -1
'        txtHorasDiarias = "0"
'        Call setHabilCtrl(chkAcargo, "DIS")
'        Call setHabilCtrl(cboDelegCod, "DIS")
'        Call setHabilCtrl(txtFechaDesde, "DIS")
'        Call setHabilCtrl(txtFechaHasta, "DIS")
'        Call setHabilCtrl(txtEmail, "DIS")
'        Call setHabilCtrl(cboEuser, "DIS")
'        Call setHabilCtrl(txtHorasDiarias, "DIS")
'    Else
        Call setHabilCtrl(chkAcargo, "NOR")
        Call setHabilCtrl(cboDelegCod, "NOR")
        Call setHabilCtrl(txtFechaDesde, "NOR")
        Call setHabilCtrl(txtFechaHasta, "NOR")
        Call setHabilCtrl(txtEmail, "NOR")
        Call setHabilCtrl(cboEuser, "NOR")
        Call setHabilCtrl(txtHorasDiarias, "NOR"): txtHorasDiarias = "8"
'    End If
End Sub

Private Sub cmdHoras_Click()
    ' Rutina para transferir horas desde un legajo a otro (se supone que es para las internalizaciones de recursos)
    ' Uno se posiciona sobre el recurso destino e ingresa el dato del legajo anterior, y una fecha desde y hasta para
    ' traerse las horas al nuevo legajo.
    Dim sFechaDesde, sFechaHasta
    Dim sLegajoAnterior As String
    Dim Horas() As New HorasTrabajadas
    Dim pet_nrointerno() As Long
    Dim i As Long, J As Long
    
    With grdDatos
        If PedirFechaDesdeHasta() Then
            sFechaDesde = glFechaDesde
            sFechaHasta = glFechaHasta
            sLegajoAnterior = InputBox("Ingrese el legajo anterior", "Legajo anterior")
            If Trim(sLegajoAnterior) <> "" Then
                Call Puntero(True)
                Call Status("1. Actualizando las horas trabajadas...")
                ' Cargo los registros con el legajo viejo
                'If sp_GetHorasTrabajadas(.TextMatrix(.RowSel, colCODRECURSO), sFechaDesde, sFechaHasta) Then
                If sp_GetHorasTrabajadas(sLegajoAnterior, sFechaDesde, sFechaHasta) Then
                    Do While Not aplRST.EOF
                        ReDim Preserve Horas(i)
                        Horas(i).Legajo = ClearNull(aplRST.Fields!cod_recurso)
                        Horas(i).Tarea = ClearNull(aplRST.Fields!cod_tarea)
                        Horas(i).PeticionNroInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                        Horas(i).FechaDesde = ClearNull(aplRST.Fields!fe_desde)
                        Horas(i).FechaHasta = ClearNull(aplRST.Fields!fe_hasta)
                        Horas(i).Horas = ClearNull(aplRST.Fields!Horas)
                        Horas(i).SinAsignar = IIf(ClearNull(aplRST.Fields!trabsinasignar) = "S", True, False)
                        Horas(i).Observaciones = ClearNull(aplRST.Fields!Observaciones)
                        i = i + 1
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    ' Cargo los nuevos registros con el legajo nuevo
                    For i = 0 To UBound(Horas)
                        Call sp_InsertHorasTrabajadas(.TextMatrix(.RowSel, colCODRECURSO), Horas(i).Tarea, Horas(i).PeticionNroInterno, Horas(i).FechaDesde, Horas(i).FechaHasta, Horas(i).Horas, IIf(Horas(i).SinAsignar, "S", "N"), Horas(i).Observaciones, 1)
                    Next i
                    ' Por �ltimo elimino los registros del legajo viejo reemplazados
                    For i = 0 To UBound(Horas)
                        Call sp_DeleteHorasTrabajadas(Horas(i).Legajo, Horas(i).Tarea, Horas(i).PeticionNroInterno, Horas(i).FechaDesde, Horas(i).FechaHasta)
                    Next i
                End If
            End If
            
            Call Status("2. Actualizando las peticiones asignadas...")
            J = 0
            If sp_GetPeticionRecurso(0, .TextMatrix(.RowSel, colCODGRUPO)) Then
                Do While Not aplRST.EOF
                    If ClearNull(aplRST.Fields!cod_recurso) = sLegajoAnterior Then
                        ReDim Preserve pet_nrointerno(J)
                        pet_nrointerno(J) = aplRST.Fields!pet_nrointerno
                        J = J + 1
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
                If J > 0 Then
                    For J = 0 To UBound(pet_nrointerno)
                        Call sp_InsertPeticionRecurso(pet_nrointerno(J), ClearNull(.TextMatrix(.RowSel, colCODRECURSO)), ClearNull(.TextMatrix(.RowSel, colCODGRUPO)), ClearNull(.TextMatrix(.RowSel, colCODSECTOR)), ClearNull(.TextMatrix(.RowSel, colCODGERENCIA)), ClearNull(.TextMatrix(.RowSel, colCODDIRECCION)))
                    Next J
                End If
            End If
            
            Call Puntero(False)
            If i > 0 Then MsgBox "Proceso finalizado." & vbCrLf & _
                                "Se realizaron " & i & " actualizaciones de horas." & vbCrLf & _
                                "Se realizaron " & J & " actualizaciones de peticiones.", vbInformation, vbOKOnly
            Erase Horas
            Erase pet_nrointerno
        End If
    End With
End Sub
