VERSION 5.00
Begin VB.Form auxSelPerfil 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3420
   ClientLeft      =   3345
   ClientTop       =   3375
   ClientWidth     =   7980
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3420
   ScaleWidth      =   7980
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Left            =   120
      TabIndex        =   3
      Top             =   540
      Width           =   7665
      Begin VB.Frame fraNivel 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   1245
         Begin VB.OptionButton OptNivel 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   4
            Left            =   50
            TabIndex        =   14
            Top             =   1410
            Width           =   1000
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   3
            Left            =   50
            TabIndex        =   13
            Top             =   1170
            Width           =   1000
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   50
            TabIndex        =   12
            Top             =   930
            Width           =   1000
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   50
            TabIndex        =   11
            Top             =   690
            Width           =   1125
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "BBVA"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   50
            TabIndex        =   10
            Top             =   480
            Width           =   1000
         End
         Begin VB.Label LblDatosRec 
            AutoSize        =   -1  'True
            Caption         =   "Nivel"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   15
            Top             =   120
            Width           =   375
         End
      End
      Begin VB.ComboBox cboSector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2205
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1410
         Width           =   5385
      End
      Begin VB.ComboBox cboGerencia 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2205
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1050
         Width           =   5385
      End
      Begin VB.ComboBox cboDireccion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2205
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   690
         Width           =   5385
      End
      Begin VB.ComboBox cboGrupo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2205
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1770
         Width           =   5385
      End
      Begin VB.ComboBox cboPerfil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2205
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   270
         Width           =   3375
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1635
         TabIndex        =   20
         Top             =   1485
         Width           =   480
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1470
         TabIndex        =   19
         Top             =   1125
         Width           =   645
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1440
         TabIndex        =   18
         Top             =   765
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1680
         TabIndex        =   17
         Top             =   1845
         Width           =   435
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Perfil"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1695
         TabIndex        =   16
         Top             =   345
         Width           =   420
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   2400
      TabIndex        =   2
      Top             =   2940
      Width           =   1365
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3960
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2940
      Width           =   1365
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Seleccione con cual perfil realizar� la consulta"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   180
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3435
   End
End
Attribute VB_Name = "auxSelPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Public bAceptar As Boolean
Public pubPerfil As String
Public pubNivel As String
Public pubArea As String
Private flgEnCarga As Boolean

Private Sub cboPerfil_Click()
    If flgEnCarga Then Exit Sub
    
    flgEnCarga = True
    
    pubPerfil = CodigoCombo(cboPerfil)
    
    Select Case pubPerfil
        Case "ADMI", "SADM", "ASEG"
            OptNivel(0).value = True
            Me.fraNivel.Enabled = False
        Case "AUTO", "CDIR"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = False
            OptNivel(4).Enabled = False
            OptNivel(1).value = True
            Me.fraNivel.Enabled = True
        Case "SOLI", "CGRU"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = True
            OptNivel(1).value = False
            OptNivel(2).value = False
            OptNivel(3).value = False
            OptNivel(4).value = False
            OptNivel(4).value = True
            Me.fraNivel.Enabled = True
        Case "REFE", "CSEC"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = True
            OptNivel(1).value = False
            OptNivel(2).value = False
            OptNivel(3).value = False
            OptNivel(4).value = False
            OptNivel(3).value = True
            Me.fraNivel.Enabled = True
        Case "SUPE", "CGCI"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = False
            OptNivel(4).Enabled = False
            OptNivel(1).value = False
            OptNivel(2).value = False
            OptNivel(3).value = False
            OptNivel(4).value = False
            OptNivel(2).value = True
            Me.fraNivel.Enabled = True
    End Select
    flgEnCarga = False
End Sub

Private Sub OptNivel_Click(Index As Integer)
    cboDireccion.visible = False
    cboGerencia.visible = False
    cboSector.visible = False
    cboGrupo.visible = False
    cboDireccion.Enabled = False
    cboGerencia.Enabled = False
    cboSector.Enabled = False
    cboGrupo.Enabled = False
    
    Select Case Index
        Case 1
            cboDireccion.visible = True
            cboDireccion.Enabled = True
        Case 2
            cboGerencia.visible = True
            cboGerencia.Enabled = True
        Case 3
            cboSector.visible = True
            cboSector.Enabled = True
        Case 4
            cboGrupo.visible = True
            cboGrupo.Enabled = True
    End Select
End Sub

Function verArea() As String
    verArea = ""
    If OptNivel(0).value = True Then
         verArea = "BBVA"
    End If
    If OptNivel(1).value = True Then
         verArea = CodigoCombo(Me.cboDireccion, True)
    End If
    If OptNivel(2).value = True Then
         verArea = CodigoCombo(Me.cboGerencia, True)
    End If
    If OptNivel(3).value = True Then
         verArea = CodigoCombo(Me.cboSector, True)
    End If
    If OptNivel(4).value = True Then
         verArea = CodigoCombo(Me.cboGrupo, True)
    End If
End Function

Private Sub cmdAceptar_Click()
    pubPerfil = CodigoCombo(Me.cboPerfil)
    pubNivel = verNivel()
    pubArea = verArea()
    If pubPerfil = "" Then
        MsgBox ("Debe seleccionar Perfil")
        Exit Sub
    End If
    If pubNivel = "" Then
        MsgBox ("Debe seleccionar Nivel")
        Exit Sub
    End If
    If pubArea = "" Then
        MsgBox ("Debe seleccionar Area")
        Exit Sub
    End If
    bAceptar = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    pubPerfil = ""
    pubNivel = ""
    pubArea = ""
    bAceptar = False
    Me.Hide
End Sub

Private Sub Form_Load()
    bAceptar = False
End Sub

Sub InicializarCombos()
    Dim i As Integer
    Dim xDire, xGere, xSect, xGrup As String
    
    flgEnCarga = True
    
    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        xDire = ClearNull(aplRST!cod_direccion)
        xGere = ClearNull(aplRST!cod_gerencia)
        xSect = ClearNull(aplRST!cod_sector)
        xGrup = ClearNull(aplRST!cod_grupo)
        aplRST.Close
    End If
    
    Call Status("Cargando Perfiles")
    If sp_GetPerfil(Null, Null) Then
        Do While Not aplRST.EOF
            cboPerfil.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If

    Call SetCombo(cboPerfil, pubPerfil)
    
    Call Status("Cargando Direcciones")
    If sp_GetDireccion("") Then
        Do While Not aplRST.EOF
            cboDireccion.AddItem Trim(aplRST!nom_direccion) & Space(100) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando Gerencias")
    If sp_GetGerenciaXt("", "") Then
        Do While Not aplRST.EOF
            cboGerencia.AddItem Trim(aplRST!nom_direccion) & ">> " & Trim(aplRST!nom_gerencia) & Space(80) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    If sp_GetSectorXt(Null, Null, Null) Then
        Do While Not aplRST.EOF
            cboSector.AddItem Trim(aplRST!nom_direccion) & ">> " & Trim(aplRST!nom_gerencia) & ">> " & Trim(aplRST!nom_sector) & Space(80) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando Grupo")
    If sp_GetGrupoXt("", "") Then
        Do While Not aplRST.EOF
            cboGrupo.AddItem Trim(aplRST!nom_direccion) & ">> " & Trim(aplRST!nom_gerencia) & ">> " & Trim(aplRST!nom_sector) & ">> " & Trim(aplRST!nom_grupo) & Space(80) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboDireccion.ListIndex = PosicionCombo(Me.cboDireccion, xDire, True)
    cboGerencia.ListIndex = PosicionCombo(Me.cboGerencia, xGere, True)
    cboSector.ListIndex = PosicionCombo(Me.cboSector, xSect, True)
    cboGrupo.ListIndex = PosicionCombo(Me.cboGrupo, xGrup, True)
    Call Status("Listo")
    flgEnCarga = False
    i = cboPerfil.ListIndex
    cboPerfil.ListIndex = -1
    cboPerfil.ListIndex = i
End Sub

Function verNivel() As String
    verNivel = ""
    If OptNivel(0).value = True Then
         verNivel = "BBVA"
    End If
    If OptNivel(1).value = True Then
         verNivel = "DIRE"
    End If
    If OptNivel(2).value = True Then
         verNivel = "GERE"
    End If
    If OptNivel(3).value = True Then
         verNivel = "SECT"
    End If
    If OptNivel(4).value = True Then
         verNivel = "GRUP"
    End If
End Function
