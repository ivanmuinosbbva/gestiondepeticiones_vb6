VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionAgrup 
   Caption         =   "Agrupamientos que integra esta petici�n"
   ClientHeight    =   4725
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8355
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   4725
   ScaleWidth      =   8355
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Left            =   7290
      TabIndex        =   2
      Top             =   0
      Width           =   1035
      Begin VB.CommandButton cmdQuitarAgr 
         Caption         =   "Quitar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Quita esta Petici�n del agrupamiento seleccionado"
         Top             =   540
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "C&errar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   4200
         Width           =   885
      End
      Begin VB.CommandButton cmdAgregarAgr 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Incorpora esta Petici�n a otro agrupamiento"
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   30
      TabIndex        =   1
      Top             =   0
      Width           =   7245
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4170
      Left            =   0
      TabIndex        =   0
      Top             =   540
      Width           =   7305
      _ExtentX        =   12885
      _ExtentY        =   7355
      _Version        =   393216
      Cols            =   13
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmPeticionAgrup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Dim flgEnCarga As Boolean

Const colTitulo = 0
Const colVisibilidad = 1
Const colActualiza = 2
Const colNomUsualta = 3
Const colESTADO = 4
Const colNroInterno = 5
Const colDireccion = 6
Const colGerencia = 7
Const colSector = 8
Const colGrupo = 9
Const colCodUsualta = 10
Const colCodVisibilidad = 11
Const colCodActualiza = 12

Private Sub Form_Load()
    flgEnCarga = True
    Call InicializarPantalla
    Call IniciarScroll(grdDatos)      ' add -003- a.
    flgEnCarga = False
End Sub

Sub InicializarPantalla()
    Call CargarGrid
End Sub

Private Sub cmdQuitarAgr_Click()
    If (Val(ClearNull(glSelectAgrup)) > 0) And (glNumeroPeticion <> "") Then
        If MsgBox("Desea que esta Petici�n deje de integrar el Agrupamiento seleccionado?", vbYesNo) = vbNo Then
            Exit Sub
        End If
        Call sp_DelAgrupPetic(glSelectAgrup, glNumeroPeticion)
        Call CargarGrid
    End If
End Sub

Private Sub cmdAgregarAgr_Click()
    glModoAgrup = "ALTAX"
    glSelectAgrup = ""
''    glNivelAgrup = ""
''    glAreaAgrup = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "MODI"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
   
    DoEvents
    If ClearNull(glSelectAgrup) = "" Then
        Exit Sub
    End If
    
    If (Val(ClearNull(glSelectAgrup)) > 0) And (glNumeroPeticion <> "") Then
            'si ya existe agrup-petic no hace nada, ni siquiera avisa
            If sp_GetAgrupPetic(glSelectAgrup, glNumeroPeticion) Then
                aplRST.Close
                Exit Sub
            End If
            Call Puntero(True)
            If sp_GetAgrupPeticDup(glSelectAgrup, glNumeroPeticion) Then
                Call Puntero(False)
                If MsgBox("Esta Peticion ya est� declarada en el agrupamiento: " & ClearNull(aplRST!agr_titulo) & Chr(13) & "Desea igualmente que integre '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbNo Then
                   aplRST.Close
                   Exit Sub
                End If
                aplRST.Close
            Else
                Call Puntero(False)
                If MsgBox("Desea que esta Petici�n integre el Agrupamiento '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbNo Then
                    Exit Sub
                End If
            End If
        Call sp_InsAgrupPetic(glSelectAgrup, glNumeroPeticion)
        Call CargarGrid
    End If
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call LockProceso(False)
    Unload Me
End Sub

Sub CargarGrid()
    DoEvents
    With grdDatos
        DoEvents
        .Clear
        '.HighLight = flexHighlightNever
        .FocusRect = flexFocusNone
        .Rows = 1
        .Font.name = "Tahoma"
        .Font.Size = 8
        .RowHeightMin = 295
        .TextMatrix(0, colTitulo) = "Agrupamiento": .ColAlignment(colTitulo) = flexAlignLeftCenter: .ColWidth(colTitulo) = 4800
        .TextMatrix(0, colVisibilidad) = "Visibilidad": .ColWidth(colVisibilidad) = 1000: .ColWidth(colCodVisibilidad) = 0
        .TextMatrix(0, colActualiza) = "Incorp.Petic.": .ColWidth(colActualiza) = 1200: .ColWidth(colCodActualiza) = 0
        .TextMatrix(0, colNomUsualta) = "Usuario Alta Agrupamiento": .ColWidth(colNomUsualta) = 2500
        .TextMatrix(0, colESTADO) = "Hab.": .ColAlignment(colESTADO) = flexAlignCenterCenter: .ColWidth(colESTADO) = 1000
        .TextMatrix(0, colNroInterno) = "Nro.Int.": .ColWidth(colNroInterno) = 800
        .ColWidth(colDireccion) = 0
        .ColWidth(colGerencia) = 0
        .ColWidth(colSector) = 0
        .ColWidth(colGrupo) = 0
        .ColWidth(colCodUsualta) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    Call Puntero(True)
    Call Status("Cargando Agrupamientos")
    If Not sp_GetAgrupPetic(0, glNumeroPeticion) Then
       GoTo finx
    End If
    
    Do While Not aplRST.EOF
        If ClearNull(aplRST!agr_vigente) = "S" Then
        If chkNvAgrup(aplRST!agr_visibilidad, aplRST!cod_direccion, aplRST!cod_gerencia, aplRST!cod_sector, aplRST!cod_grupo, aplRST!cod_usualta, glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REEMPLAZO) Then
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!agr_titulo)
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!agr_nrointerno)
            .TextMatrix(.Rows - 1, colNomUsualta) = ClearNull(aplRST!nom_usualta)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!agr_vigente)
            .TextMatrix(.Rows - 1, colVisibilidad) = getDescNvAgrup(ClearNull(aplRST!agr_visibilidad))
            .TextMatrix(.Rows - 1, colActualiza) = getDescNvAgrup(ClearNull(aplRST!agr_actualiza))
            .TextMatrix(.Rows - 1, colDireccion) = ClearNull(aplRST!cod_direccion)
            .TextMatrix(.Rows - 1, colGerencia) = ClearNull(aplRST!cod_gerencia)
            .TextMatrix(.Rows - 1, colSector) = ClearNull(aplRST!cod_sector)
            .TextMatrix(.Rows - 1, colGrupo) = ClearNull(aplRST!cod_grupo)
            .TextMatrix(.Rows - 1, colCodUsualta) = ClearNull(aplRST!cod_usualta)
            .TextMatrix(.Rows - 1, colCodVisibilidad) = ClearNull(aplRST!agr_visibilidad)
            .TextMatrix(.Rows - 1, colCodActualiza) = ClearNull(aplRST!agr_actualiza)
            'Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
        End With
        End If
        End If
        aplRST.MoveNext
    Loop
finx:
    Call Puntero(False)
    Call Status("Listo")
    Call LockProceso(False)

    If grdDatos.Rows > 1 Then
        grdDatos.TopRow = 1
        grdDatos.row = 1
        grdDatos.rowSel = 1
        grdDatos.col = 0
        grdDatos.ColSel = grdDatos.cols - 1
        Call MostrarSeleccion
    End If
End Sub

Public Sub MostrarSeleccion()
    cmdQuitarAgr.Enabled = False
    With grdDatos
        If .rowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.rowSel, colNroInterno) <> "" Then
                glSelectAgrup = .TextMatrix(.rowSel, colNroInterno)
                If chkNvAgrup(.TextMatrix(.rowSel, colCodActualiza), .TextMatrix(.rowSel, colDireccion), .TextMatrix(.rowSel, colGerencia), .TextMatrix(.rowSel, colSector), .TextMatrix(.rowSel, colGrupo), .TextMatrix(.rowSel, colCodUsualta), glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REEMPLAZO) Then
                    cmdQuitarAgr.Enabled = True
                End If
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    'Debug.Print "grdDatos_Click"
    Call MostrarSeleccion
    DoEvents
End Sub

Private Sub grdDatos_RowColChange()
    If flgEnCarga Then Exit Sub
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
