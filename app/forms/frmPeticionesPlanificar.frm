VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmPeticionesPlanificar 
   Caption         =   "Planificaci�n"
   ClientHeight    =   8415
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15360
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesPlanificar.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8415
   ScaleWidth      =   15360
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab sstPlanificacion 
      Height          =   6495
      Left            =   60
      TabIndex        =   37
      Top             =   1860
      Width           =   13995
      _ExtentX        =   24686
      _ExtentY        =   11456
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "Planificaci�n && priorizaci�n"
      TabPicture(0)   =   "frmPeticionesPlanificar.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "grdDatos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Criterios de inclusi�n autom�tica"
      TabPicture(1)   =   "frmPeticionesPlanificar.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraCtriterioPlanificacion(1)"
      Tab(1).Control(1)=   "fraCtriterioPlanificacion(0)"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Datos a visualizar"
      TabPicture(2)   =   "frmPeticionesPlanificar.frx":05C2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraDatosPresentacion"
      Tab(2).ControlCount=   1
      Begin VB.Frame fraDatosPresentacion 
         Caption         =   "Informaci�n a mostrar por pantalla"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5655
         Left            =   -74820
         TabIndex        =   51
         Top             =   600
         Width           =   6555
         Begin VB.CheckBox chkInfoAIncluir 
            Caption         =   "Incluir los datos del proyecto IDM vinculado a la petici�n"
            Height          =   255
            Index           =   1
            Left            =   360
            TabIndex        =   53
            Top             =   840
            Width           =   4455
         End
         Begin VB.CheckBox chkInfoAIncluir 
            Caption         =   "Incluir los datos de la gerencia y sector solicitante de la petici�n"
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   52
            Top             =   540
            Width           =   5115
         End
      End
      Begin VB.Frame fraCtriterioPlanificacion 
         Caption         =   "Proceso autom�tico de planificaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4575
         Index           =   1
         Left            =   -74820
         TabIndex        =   44
         Top             =   1980
         Width           =   13575
         Begin VB.Label lblCriterios 
            AutoSize        =   -1  'True
            Caption         =   "lblCriterios"
            Height          =   195
            Index           =   1
            Left            =   180
            TabIndex        =   50
            Top             =   420
            Width           =   750
         End
         Begin VB.Label lblCriterios 
            AutoSize        =   -1  'True
            Caption         =   "lblCriterios"
            Height          =   195
            Index           =   2
            Left            =   180
            TabIndex        =   49
            Top             =   660
            Width           =   750
         End
         Begin VB.Label lblCriterios 
            AutoSize        =   -1  'True
            Caption         =   "lblCriterios"
            Height          =   195
            Index           =   3
            Left            =   180
            TabIndex        =   48
            Top             =   900
            Width           =   750
         End
         Begin VB.Label lblCriterios 
            AutoSize        =   -1  'True
            Caption         =   "lblCriterios"
            Height          =   195
            Index           =   4
            Left            =   180
            TabIndex        =   47
            Top             =   1140
            Width           =   750
         End
         Begin VB.Label lblCriterios 
            AutoSize        =   -1  'True
            Caption         =   "lblCriterios"
            Height          =   195
            Index           =   5
            Left            =   180
            TabIndex        =   46
            Top             =   1380
            Width           =   750
         End
         Begin VB.Label lblCriterios 
            AutoSize        =   -1  'True
            Caption         =   "lblCriterios"
            Height          =   195
            Index           =   6
            Left            =   180
            TabIndex        =   45
            Top             =   1620
            Width           =   750
         End
      End
      Begin VB.Frame fraCtriterioPlanificacion 
         Caption         =   "Proceso autom�tico de planificaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Index           =   0
         Left            =   -74820
         TabIndex        =   39
         Top             =   780
         Width           =   13575
         Begin VB.Label lblProceso 
            AutoSize        =   -1  'True
            Caption         =   "lblProcesos"
            Height          =   555
            Index           =   0
            Left            =   180
            TabIndex        =   40
            Top             =   300
            Width           =   13275
            WordWrap        =   -1  'True
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grdDatos 
         Height          =   6075
         Left            =   60
         TabIndex        =   38
         Top             =   360
         Width           =   13875
         _ExtentX        =   24474
         _ExtentY        =   10716
         _Version        =   393216
         GridColor       =   14737632
         GridColorFixed  =   14737632
         AllowBigSelection=   0   'False
         GridLinesFixed  =   1
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraPeriodo 
      Height          =   1815
      Left            =   10080
      TabIndex        =   20
      Top             =   0
      Width           =   3975
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   31
         Top             =   720
         Width           =   510
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   30
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Fecha desde"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   5
         Left            =   840
         TabIndex        =   29
         Top             =   720
         Width           =   915
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Fecha hasta"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   6
         Left            =   840
         TabIndex        =   28
         Top             =   960
         Width           =   885
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   7
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   555
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   8
         Left            =   840
         TabIndex        =   26
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Vigente:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   25
         Top             =   480
         Width           =   600
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Vigencia"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   9
         Left            =   840
         TabIndex        =   24
         Top             =   480
         Width           =   585
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Realizado por:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   13
         Left            =   120
         TabIndex        =   23
         Top             =   1200
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Responsable"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   14
         Left            =   1320
         TabIndex        =   22
         Top             =   1200
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   " Datos del per�odo "
         ForeColor       =   &H00D28D55&
         Height          =   195
         Left            =   2490
         TabIndex        =   21
         Top             =   0
         Width           =   1350
      End
   End
   Begin VB.Frame fraBotonera 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8355
      Left            =   14100
      TabIndex        =   1
      Top             =   0
      Width           =   1260
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "C&errar"
         Height          =   495
         Left            =   80
         TabIndex        =   11
         Top             =   7800
         Width           =   1095
      End
      Begin VB.CommandButton cmdPriorizarTodo 
         Caption         =   "Prioriz&ar todo"
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":05DE
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Prioriza todas las peticiones o hasta el l�mite indicado debajo"
         Top             =   720
         Width           =   1095
      End
      Begin VB.CommandButton cmdPriorizarNada 
         Caption         =   "B&orrar todo"
         Height          =   555
         Left            =   80
         Style           =   1  'Graphical
         TabIndex        =   41
         ToolTipText     =   "Prioriza todas las peticiones o hasta el l�mite indicado debajo"
         Top             =   180
         Width           =   1095
      End
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Actualizar"
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":0B68
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   3600
         Width           =   1095
      End
      Begin VB.CommandButton cmdDescartar 
         Caption         =   "Descartar"
         Enabled         =   0   'False
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":10F2
         Style           =   1  'Graphical
         TabIndex        =   18
         Tag             =   "Guard&ar *"
         Top             =   6900
         Width           =   1095
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "Guardar"
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":167C
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   6360
         Width           =   1095
      End
      Begin VB.CommandButton cmdQuitar 
         Caption         =   "Quitar"
         Enabled         =   0   'False
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":1C06
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   4980
         Width           =   1095
      End
      Begin VB.CommandButton cmdEditar 
         Caption         =   "Editar"
         Enabled         =   0   'False
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":2190
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Editar la planificaci�n actual"
         Top             =   5820
         Width           =   1095
      End
      Begin AT_MaskText.MaskText txtPriorizacionLimite 
         Height          =   285
         Left            =   180
         TabIndex        =   36
         Top             =   1320
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   503
         MaxLength       =   12
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   12
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin MSComCtl2.UpDown udPriorizacionLimite 
         Height          =   285
         Left            =   900
         TabIndex        =   34
         Top             =   1320
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   503
         _Version        =   393216
         Value           =   1
         AutoBuddy       =   -1  'True
         BuddyControl    =   "txtPriorizacionLimite"
         BuddyDispid     =   196638
         OrigLeft        =   840
         OrigTop         =   720
         OrigRight       =   1095
         OrigBottom      =   975
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "E&xportar"
         Enabled         =   0   'False
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":271A
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   1920
         Width           =   1095
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   555
         Left            =   80
         Picture         =   "frmPeticionesPlanificar.frx":2CA4
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   "Agregar manualmente peticiones para planificar en el per�odo actual"
         Top             =   4440
         Width           =   1095
      End
   End
   Begin VB.Frame fraEncabezadoPrincipal 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   9975
      Begin VB.ComboBox cboFiltroPriorizadas 
         Height          =   315
         Left            =   7440
         Style           =   2  'Dropdown List
         TabIndex        =   43
         Top             =   600
         Width           =   2355
      End
      Begin VB.ComboBox cboFiltroGrupoEjecutor 
         Height          =   315
         Left            =   7440
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   960
         Width           =   2355
      End
      Begin VB.ComboBox cboFiltroEstadosPeticion 
         Height          =   315
         Left            =   7440
         Style           =   2  'Dropdown List
         TabIndex        =   33
         Top             =   1320
         Width           =   2355
      End
      Begin VB.ComboBox cboFiltroSituacion 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   1320
         Width           =   4815
      End
      Begin VB.ComboBox cboFiltroRefSistemas 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   960
         Width           =   4815
      End
      Begin VB.ComboBox cboFiltroPeriodo 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   240
         Width           =   4815
      End
      Begin VB.ComboBox cboFiltroRefBPE 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   600
         Width           =   4815
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Priorizadas:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   15
         Left            =   6660
         TabIndex        =   42
         Top             =   660
         Width           =   840
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Ver:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   17
         Left            =   120
         TabIndex        =   16
         Top             =   1380
         Width           =   300
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Grupo ejecutor:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   12
         Left            =   6660
         TabIndex        =   10
         Top             =   1020
         Width           =   1140
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Ref. de sistemas:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   8
         Top             =   1020
         Width           =   1260
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Ref. de RGyP:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   7
         Top             =   660
         Width           =   1035
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Per�odo:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   4
         Top             =   300
         Width           =   600
      End
      Begin VB.Label lblEncabezadoPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Estado de peticiones:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   0
         Left            =   6660
         TabIndex        =   3
         Top             =   1380
         Width           =   1545
      End
   End
End
Attribute VB_Name = "frmPeticionesPlanificar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const PERIODO_CERRADO = "PLAN40"

' COLUMNAS PARA PRIORIZAR (PETICIONES)
Private Const col_PET_FilaNro = 0
Private Const col_PET_MultiSel = 1
Private Const col_PET_NroAsignado = 2
Private Const col_PET_Periodo = 3
Private Const col_PET_FechaAlta = 4
Private Const col_PET_Tipo = 5
Private Const col_PET_Clase = 6
Private Const col_PET_Regulatorio = 7
Private Const col_PET_Titulo = 8
Private Const col_PET_SectorSolicitante = 9
Private Const col_PET_Puntaje = 10
Private Const col_PET_Prioridad = 11
Private Const col_PET_ProjId = 12
Private Const col_PET_ProjSubId = 13
Private Const col_PET_ProjSubSId = 14
Private Const col_PET_ProjNom = 15
Private Const col_PET_estado = 16
Private Const col_PET_nom_bpe = 17
Private Const col_PET_nom_bpar = 18
Private Const col_PET_Analisis = 19
Private Const col_PET_Compromiso = 20
Private Const col_PET_HorasPresup = 21
Private Const col_PET_PlanIni = 22
Private Const col_PET_PlanFin = 23
Private Const col_PET_RealIni = 24
Private Const col_PET_RealFin = 25
Private Const col_PET_Replanificaciones = 26
Private Const col_PET_Comprometido = 27
Private Const col_PET_cod_bpar = 28
Private Const col_PET_codigoperiodo = 29
Private Const col_PET_pet_nrointerno = 30
Private Const col_PET_per_nrointerno = 31
Private Const col_PET_cod_estado = 32
Private Const col_PET_situacion = 33
Private Const col_PET_PrioridadAnterior = 34
Private Const col_PET_Cumplido = 35

Private Const col_PET_NUMERO_COLUMNAS = 36

Dim bLoad As Boolean                                    ' Variable global que indica que se est� cargando con datos la grilla
Dim bSorting As Boolean                                 ' Variable global que indica que se est� ordenando la grilla
Dim cValor As String                                    ' Variable auxiliar para guardar valor de tipo String
Dim cModo As String                                     ' Variable global que indica el modo de la pantalla (edici�n, eliminaci�n, etc.)
Dim cModoTrabajo As String                              ' Variable auxiliar para saber con que modo de grilla trabajamos
Dim bCambiosPendientes As Boolean                       ' Variable general para indicar si hay cambios sin guardar
Dim cPeticionesRepetidas As String                      ' Variable auxiliar para guardar un string con los n�meros de petici�n para una funci�n
Dim cUltimoValor As String                              ' Para el CTRL + Z
Dim lLastFocusRow As Long                               ' Guarda el valor de la ultima fila seleccionada para el autofocus
Dim EstadoPeriodoActual As String                       ' Estado del per�odo
Dim objRST As ADODB.Recordset                           ' Recordset copia para poder realizar los filtros y dem�s
Dim lUltimaColumnaOrdenada As Long                      ' Variable auxiliar para el ordenamiento de la grilla
Dim bNewCell As Boolean

Private Sub chkInfoAIncluir_Click(Index As Integer)
    Call CargarGrilla
    sstPlanificacion.Tab = 0
End Sub

Private Sub Form_Load()
    cModo = "VISTA"
    cModoTrabajo = "GBPE"
    Call Inicializar
    Call CargarGrilla
    
    grdDatos.HighLight = flexHighlightNever
    Call IniciarScroll(grdDatos)
    Call Puntero(False)
End Sub

Private Sub Inicializar()
    Dim iIndex As Integer
    
    iIndex = -1
    bLoad = True
    Call Puntero(True)
    
    lblProceso(0) = "El proceso autom�tico de incorporaci�n de peticiones al periodo de planificaci�n se ejecuta a principio de mes. "
    lblProceso(0) = lblProceso(0) & "Antes de incorporar las peticiones nuevas, se quitan aquellas que no han sido ni analizadas ni comprometidas por los referentes de sistema, y se pasan al nuevo periodo. "
    lblProceso(0) = lblProceso(0) & "Cuando pasan al nuevo periodo, si hubieran tenido una priorizaci�n, esta es inicializada, por lo que vuelve a competir con el resto de las peticiones."
    
    fraCtriterioPlanificacion(1).Caption = "Los criterios para incluir autom�ticamente peticiones a los periodos de planificaci�n"
    
    lblCriterios(1) = "� Clasificadas como Nuevo desarrollo o Mantenimiento evolutivo."
    lblCriterios(2) = "� No son regulatorios."
    lblCriterios(3) = "� Tienen referente de Sistema designado."
    lblCriterios(4) = "� Tienen cargada palancas de valoraci�n o la petici�n est� asociada a un proyecto."
    lblCriterios(5) = "� La petici�n no est� vinculada a la SDA."
    lblCriterios(6) = "� La petici�n no est� ya vinculada a un periodo anterior."
    
    cmdPriorizarTodo.ToolTipText = "Establecer la prioridad de todas las peticiones en pantalla"
    cmdPriorizarNada.ToolTipText = "Quitar toda la prioridad de las peticiones listadas en pantalla."
    cmdRefresh.ToolTipText = "Actualizar los datos de la grilla"
    cmdGuardar.ToolTipText = "Guardar los cambios realizados"
    cmdEditar.ToolTipText = "Editar la planificaci�n actual"
    cmdAgregar.ToolTipText = "Agregar manualmente peticiones para planificar en el per�odo actual"
    cmdQuitar.ToolTipText = "Quitar manualmente peticiones de la planificaci�n actual"
    cmdDescartar.ToolTipText = "Descarta los cambios realizados"
    cmdExportar.ToolTipText = "Exportar la grilla a una planilla Microsoft Excel"

    
    Call setHabilCtrl(cboFiltroPeriodo, NORMAL)
    Call setHabilCtrl(cmdAgregar, NORMAL)
    Call setHabilCtrl(cmdEditar, NORMAL)
    Call setHabilCtrl(cmdPriorizarTodo, DISABLE)
    Call setHabilCtrl(cmdPriorizarNada, DISABLE)
    Call setHabilCtrl(cmdDescartar, "DIS")
    Call setHabilCtrl(cmdQuitar, NORMAL)
    
    Dim vSectoresSistemas() As String           ' Auxiliar para cargar todos los sectores activos de la gerencia de sistemas.
    Dim vSectoresSistemasNombre() As String
    Dim i As Long
    
    i = 0
    If sp_GetSector(Null, "DESA", "S") Then
        Do While Not aplRST.EOF
            ReDim Preserve vSectoresSistemas(i)
            ReDim Preserve vSectoresSistemasNombre(i)
            vSectoresSistemas(i) = ClearNull(aplRST.Fields!cod_sector)
            vSectoresSistemasNombre(i) = ClearNull(aplRST.Fields!nom_sector)
            aplRST.MoveNext
            i = i + 1
            DoEvents
        Loop
    End If

    With cboFiltroEstadosPeticion
        .Clear
        .AddItem "* Todas las peticiones" & ESPACIOS & ESPACIOS & "||NULL"
        .AddItem "Solo peticiones en estado activo" & ESPACIOS & ESPACIOS & "||ACTI"
        .ListIndex = 1
    End With
    
    With cboFiltroPriorizadas
        .Clear
        .AddItem "* Todas las peticiones" & ESPACIOS & ESPACIOS & "||NULL"
        .AddItem "Solo peticiones priorizadas" & ESPACIOS & ESPACIOS & "||S"
        .ListIndex = IIf(glUsrPerfilActual = "GBPE", 0, 1)
        If glUsrPerfilActual <> "GBPE" Then
            Call setHabilCtrl(cboFiltroPriorizadas, DISABLE)
        End If
    End With
    
    With cboFiltroGrupoEjecutor
        .Clear
        .AddItem "* Todos los grupos" & ESPACIOS & ESPACIOS & "||NULL"
        For i = 0 To UBound(vSectoresSistemas)
            .AddItem "--- Sector: " & vSectoresSistemasNombre(i) & " ---" & ESPACIOS & ESPACIOS & "||SECT"
            If sp_GetGrupoXt(Null, vSectoresSistemas(i), "S") Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!cod_grupo) & ": " & ClearNull(aplRST.Fields!nom_grupo) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_grupo)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next i
        .ListIndex = 0
    End With

    With cboFiltroPeriodo
        .Clear
        .AddItem "* Todos" & ESPACIOS & ESPACIOS & "||NULL"
        If sp_GetPeriodo(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!per_abrev) & ": " & ClearNull(aplRST.Fields!per_nombre) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST.Fields!per_nrointerno)
                If ClearNull(aplRST.Fields!vigente) = "S" Then iIndex = cboFiltroPeriodo.ListCount - 1
                aplRST.MoveNext
                DoEvents
            Loop
            If iIndex = -1 Then
                .ListIndex = .ListCount - 1
            Else
                .ListIndex = iIndex
            End If
        End If
    End With
    Call ActualizarDatosPeriodo

    ' Cargo los Referentes de RGyP
    If sp_GetRecursoPerfil(Null, "GBPE") Then
        With cboFiltroRefBPE
            .Clear
            .AddItem "* Todos los Referentes de RGyP" & ESPACIOS & ESPACIOS & "||NULL"
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_recurso) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
            If .ListCount > 0 Then .ListIndex = 0
        End With
    End If
    
    ' Cargo los Referentes de sistema
    If sp_GetRecursoPerfil(Null, "BPAR") Then
        With cboFiltroRefSistemas
            .Clear
            .AddItem "* Todos los Referentes de sistema" & ESPACIOS & ESPACIOS & "||NULL"
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_recurso) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
            If .ListCount > 0 Then .ListIndex = 0
        End With
    End If
    
    With cboFiltroSituacion
        .Clear
        .AddItem "* Todas" & ESPACIOS & ESPACIOS & "||NULL"
        .AddItem "Pendientes" & ESPACIOS & ESPACIOS & "||PEND"
        .AddItem "Analizadas" & ESPACIOS & ESPACIOS & "||ANAL"
        .AddItem "Comprometidas" & ESPACIOS & ESPACIOS & "||COMP"
        .ListIndex = 0
    End With
    bLoad = False
    
    chkInfoAIncluir(0).value = 0       ' Por defecto no se muestra
    chkInfoAIncluir(1).value = 0       ' Por defecto no se muestra
End Sub

Private Sub ActualizarDatosPeriodo()
    If CodigoCombo(cboFiltroPeriodo, True) = "NULL" Then
        lblEncabezadoPrincipal(5) = "-"
        lblEncabezadoPrincipal(6) = "-"
        lblEncabezadoPrincipal(8) = "-"
        lblEncabezadoPrincipal(9) = "-"
        lblEncabezadoPrincipal(14) = "-"
        EstadoPeriodoActual = "-"
    Else
        If sp_GetPeriodo(CodigoCombo(cboFiltroPeriodo, True), Null) Then
            lblEncabezadoPrincipal(5) = ClearNull(aplRST.Fields!fe_desde)
            lblEncabezadoPrincipal(6) = ClearNull(aplRST.Fields!fe_hasta)
            lblEncabezadoPrincipal(8) = ClearNull(aplRST.Fields!nom_estado)
            lblEncabezadoPrincipal(9) = IIf(ClearNull(aplRST.Fields!vigente) = "S", "Si", "No")
            lblEncabezadoPrincipal(14) = IIf(ClearNull(aplRST.Fields!nom_ultusrid) = "", "-", ClearNull(aplRST.Fields!nom_ultusrid))
            EstadoPeriodoActual = ClearNull(aplRST.Fields!per_estado)
            If EstadoPeriodoActual = PERIODO_CERRADO Then
                lblEncabezadoPrincipal(8).ForeColor = vbBlack
                lblEncabezadoPrincipal(8).FontBold = True
            Else
                lblEncabezadoPrincipal(8).ForeColor = vbBlack
                lblEncabezadoPrincipal(8).FontBold = False
            End If
        End If
    End If
    Call HabilitarBotones
End Sub

Private Sub Form_Resize()
    Call FormLayoutResize
End Sub

Private Sub FormLayoutResize()
    Debug.Print "Height: " & Me.Height & " / Width: " & Me.Width
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            Me.Top = 0
            Me.Left = 0
            'If Me.Height < 8565 Then Me.Height = 8565
            If Me.Height < 8595 Then Me.Height = 8595
            If Me.Width < 19080 Then Me.Width = 19080
            'If Me.Width < 14550 Then Me.Width = 14550
        End If
        ' **************************************************************
        ' PLANO HORIZONTAL
        ' **************************************************************
        'If (fraBotonera.Width - 200) < Me.ScaleWidth Then grdDatos.Width = Me.ScaleWidth - fraBotonera.Width - 200
        If (fraBotonera.Width - 200) < Me.ScaleWidth Then sstPlanificacion.Width = Me.ScaleWidth - fraBotonera.Width - 200
        
        'fraBotonera.Left = grdDatos.Width + 150
        fraBotonera.Left = sstPlanificacion.Width + 150
        grdDatos.Width = sstPlanificacion.Width - 100
        'fraEncabezadoPrincipal.Width = grdDatos.Width - fraPeriodo.Width - 100
        fraEncabezadoPrincipal.Width = sstPlanificacion.Width - fraPeriodo.Width - 50
        fraPeriodo.Left = fraEncabezadoPrincipal.Width + 100          'fraEncabezadoPrincipal.Width - fraPeriodo.Width - 100
        
        ' Filtros
        cboFiltroEstadosPeticion.Left = lblEncabezadoPrincipal(0).Left + lblEncabezadoPrincipal(0).Width + 100
        cboFiltroGrupoEjecutor.Left = cboFiltroEstadosPeticion.Left
        cboFiltroGrupoEjecutor.Width = 5000
        cboFiltroPriorizadas.Left = cboFiltroGrupoEjecutor.Left
        cboFiltroPriorizadas.Width = 5000
        cboFiltroEstadosPeticion.Width = 4000
        
        fraCtriterioPlanificacion(0).Width = sstPlanificacion.Width - 500
        fraCtriterioPlanificacion(1).Width = fraCtriterioPlanificacion(0).Width
        
        ' **************************************************************
        ' PLANO VERTICAL
        ' **************************************************************
        'grdDatos.Height = Me.ScaleHeight - fraEncabezadoPrincipal.Height - 350
        'If (fraEncabezadoPrincipal.Height - 50) < Me.ScaleHeight Then grdDatos.Height = Me.ScaleHeight - fraEncabezadoPrincipal.Height - 50
        If (fraEncabezadoPrincipal.Height - 50) < Me.ScaleHeight Then sstPlanificacion.Height = Me.ScaleHeight - fraEncabezadoPrincipal.Height - 100
        grdDatos.Height = sstPlanificacion.Height - 450
        'fraBotonera.Height = Me.ScaleHeight - 350
        If (Me.ScaleHeight - 50) > 0 Then fraBotonera.Height = Me.ScaleHeight - 50
        'fraBotonera.Height = Me.ScaleHeight - 50
        cmdCerrar.Top = fraBotonera.Height - cmdCerrar.Height - 100
        
        fraCtriterioPlanificacion(1).Height = sstPlanificacion.Height - fraCtriterioPlanificacion(0).Height - 1000
    End If
    'Debug.Print "Height: " & Me.Height & " / Width: " & Me.Width
End Sub

Private Sub cboFiltroPeriodo_Click()
    If Not bLoad Then
        If CodigoCombo(cboFiltroPeriodo, True) <> "NULL" Then
            Call ActualizarDatosPeriodo
        Else
            lblEncabezadoPrincipal(5) = "-"
            lblEncabezadoPrincipal(6) = "-"
            lblEncabezadoPrincipal(8) = "-"
            lblEncabezadoPrincipal(9) = "-"
            lblEncabezadoPrincipal(14) = "-"
            EstadoPeriodoActual = "-"
        End If
        Call CargarGrilla
        If Me.visible Then grdDatos.SetFocus
    End If
End Sub

Private Sub cmdQuitar_Click()
    Dim sMensajeHistorial As String
    
    With grdDatos
        If MsgBox("�Confirma desvincular la petici�n n� " & .TextMatrix(.rowSel, col_PET_NroAsignado) & " de la planificaci�n?", vbQuestion + vbYesNo, "Desvincular petici�n") = vbYes Then
            sMensajeHistorial = "La petici�n " & .TextMatrix(.rowSel, col_PET_NroAsignado) & " ha sido quitada manualmente de la planificaci�n " & .TextMatrix(.rowSel, col_PET_Periodo) & "."
            Call sp_DeletePeticionPlancab(.TextMatrix(.rowSel, col_PET_codigoperiodo), .TextMatrix(.rowSel, col_PET_pet_nrointerno))
            Call sp_AddHistorial(.TextMatrix(.rowSel, col_PET_pet_nrointerno), "PDELPRI0", .TextMatrix(.rowSel, col_PET_cod_estado), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sMensajeHistorial)
            cmdRefresh_Click
        End If
    End With
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .visible = False
        .Clear
        .cols = col_PET_NUMERO_COLUMNAS
        .FixedCols = 1
        .RowHeight(0) = 300
        .RowHeightMin = 260
        .Rows = 1
        .MergeCells = flexMergeNever
        .TextMatrix(0, col_PET_FilaNro) = "N�": .ColWidth(col_PET_FilaNro) = 400: .ColAlignment(col_PET_FilaNro) = flexAlignRightCenter
        .TextMatrix(0, col_PET_MultiSel) = "": .ColWidth(col_PET_MultiSel) = 250: .ColAlignment(col_PET_MultiSel) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_NroAsignado) = "Pet. N�": .ColWidth(col_PET_NroAsignado) = 700: .ColAlignment(col_PET_NroAsignado) = flexAlignRightCenter
        .TextMatrix(0, col_PET_Periodo) = "Per�odo": .ColWidth(col_PET_Periodo) = 800: .ColAlignment(col_PET_Periodo) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_FechaAlta) = "F.Alta Pet.": .ColWidth(col_PET_FechaAlta) = 1000: .ColAlignment(col_PET_FechaAlta) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_Tipo) = "Tipo":  .ColWidth(col_PET_Tipo) = 600: .ColAlignment(col_PET_Tipo) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_Clase) = "Clase":  .ColWidth(col_PET_Clase) = 600: .ColAlignment(col_PET_Clase) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_Regulatorio) = "Reg.":  .ColWidth(col_PET_Regulatorio) = 500: .ColAlignment(col_PET_Regulatorio) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_Titulo) = "T�tulo de la petici�n":  .ColWidth(col_PET_Titulo) = 3500: .ColAlignment(col_PET_Titulo) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_SectorSolicitante) = "Gerencia y Sector solicitante": .ColWidth(col_PET_SectorSolicitante) = IIf(chkInfoAIncluir(0).value = 1, 4500, 0): .ColAlignment(col_PET_SectorSolicitante) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_Puntaje) = "Ptos.":  .ColWidth(col_PET_Puntaje) = 700: .ColAlignment(col_PET_Puntaje) = flexAlignRightCenter
        .TextMatrix(0, col_PET_Prioridad) = "Prior.":  .ColWidth(col_PET_Prioridad) = 700: .ColAlignment(col_PET_Prioridad) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_SectorSolicitante) = "Gerencia y Sector solicitante": .ColWidth(col_PET_SectorSolicitante) = IIf(chkInfoAIncluir(0).value = 1, 4500, 0): .ColAlignment(col_PET_SectorSolicitante) = flexAlignLeftCenter
        ' Proyecto IDM
        .TextMatrix(0, col_PET_ProjId) = "ProjId.":  .ColWidth(col_PET_ProjId) = IIf(chkInfoAIncluir(1).value = 1, 600, 0): .ColAlignment(col_PET_ProjId) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_ProjSubId) = "ProjSubId.":  .ColWidth(col_PET_ProjSubId) = IIf(chkInfoAIncluir(1).value = 1, 600, 0): .ColAlignment(col_PET_ProjSubId) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_ProjSubSId) = "ProjSubSId.":  .ColWidth(col_PET_ProjSubSId) = IIf(chkInfoAIncluir(1).value = 1, 600, 0): .ColAlignment(col_PET_ProjSubSId) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_ProjNom) = "Nombre del proyecto IDM":  .ColWidth(col_PET_ProjNom) = IIf(chkInfoAIncluir(1).value = 1, 4500, 0): .ColAlignment(col_PET_ProjNom) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_estado) = "Estado Pet.":  .ColWidth(col_PET_estado) = 2000: .ColAlignment(col_PET_estado) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_nom_bpe) = "Ref. de RGyP": .ColWidth(col_PET_nom_bpe) = 2000: .ColAlignment(col_PET_nom_bpe) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_nom_bpar) = "Ref. de sistemas": .ColWidth(col_PET_nom_bpar) = 2000: .ColAlignment(col_PET_nom_bpar) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_Analisis) = "AN.":  .ColWidth(col_PET_Analisis) = 400: .ColAlignment(col_PET_Analisis) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_Compromiso) = "PL.":  .ColWidth(col_PET_Compromiso) = 400: .ColAlignment(col_PET_Compromiso) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_HorasPresup) = "Hs.":  .ColWidth(col_PET_HorasPresup) = 1000: .ColAlignment(col_PET_HorasPresup) = flexAlignRightCenter
        .TextMatrix(0, col_PET_PlanIni) = "Plan. Inicio":  .ColWidth(col_PET_PlanIni) = 1000: .ColAlignment(col_PET_PlanIni) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_PlanFin) = "Plan. Fin":  .ColWidth(col_PET_PlanFin) = 1000: .ColAlignment(col_PET_PlanFin) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_RealIni) = "Real Inicio":  .ColWidth(col_PET_RealIni) = 1000: .ColAlignment(col_PET_RealIni) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_RealFin) = "Real Fin":  .ColWidth(col_PET_RealFin) = 1000: .ColAlignment(col_PET_RealFin) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_Replanificaciones) = "Replanif.":  .ColWidth(col_PET_Replanificaciones) = 1000: .ColAlignment(col_PET_Replanificaciones) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_Comprometido) = "Comprometido":  .ColWidth(col_PET_Comprometido) = 1400: .ColAlignment(col_PET_Comprometido) = flexAlignCenterCenter
        .TextMatrix(0, col_PET_Cumplido) = "Cumplido en":  .ColWidth(col_PET_Cumplido) = 1400: .ColAlignment(col_PET_Cumplido) = flexAlignCenterCenter
        ' Columnas que no se ven
        .TextMatrix(0, col_PET_cod_bpar) = "cod_bpar": .ColWidth(col_PET_cod_bpar) = 0
        .TextMatrix(0, col_PET_codigoperiodo) = "per_nrointerno": .ColWidth(col_PET_codigoperiodo) = 0: .ColAlignment(col_PET_codigoperiodo) = flexAlignLeftCenter
        .TextMatrix(0, col_PET_pet_nrointerno) = "pet_nrointerno":  .ColWidth(col_PET_pet_nrointerno) = 0
        .TextMatrix(0, col_PET_per_nrointerno) = "per_nrointerno":  .ColWidth(col_PET_per_nrointerno) = 0
        .TextMatrix(0, col_PET_cod_estado) = "cod_estado":  .ColWidth(col_PET_cod_estado) = 0
        .TextMatrix(0, col_PET_situacion) = "situacion": .ColWidth(col_PET_situacion) = 0
        .TextMatrix(0, col_PET_PrioridadAnterior) = "prioridad_anterior": .ColWidth(col_PET_PrioridadAnterior) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub CargarGrilla()
    Dim lPendientes, lPriorizadas, lIncluidas As Long
    Dim Registros As Long
    Dim lPeriodoSeleccionado As Long
    Dim lCol_Ori, lRow_Ori As Long
    
    lPendientes = 0
    lPriorizadas = 0
    bLoad = True
    lPeriodoSeleccionado = IIf(CodigoCombo(cboFiltroPeriodo, True) = "NULL", 0, CodigoCombo(cboFiltroPeriodo, True))

    Call Puntero(True)
    Call InicializarGrilla
    With grdDatos
        If sp_GetPetPlanificar(IIf(lPeriodoSeleccionado = 0, Null, lPeriodoSeleccionado), CodigoCombo(cboFiltroRefBPE, True), CodigoCombo(cboFiltroRefSistemas, True), CodigoCombo(cboFiltroSituacion, True), CodigoCombo(cboFiltroEstadosPeticion, True), CodigoCombo(cboFiltroGrupoEjecutor, True), CodigoCombo(cboFiltroPriorizadas, True)) Then
            Registros = aplRST.RecordCount
            Call Status(Registros & IIf(Registros > 1, " peticiones.", " petici�n."))
            Do While Not aplRST.EOF
CheckPoint:
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, col_PET_FilaNro) = .Rows - 1
                .TextMatrix(.Rows - 1, col_PET_MultiSel) = ClearNull(aplRST.Fields!origen)
                .TextMatrix(.Rows - 1, col_PET_Periodo) = ClearNull(aplRST.Fields!per_abrev)
                .TextMatrix(.Rows - 1, col_PET_FechaAlta) = ClearNull(aplRST.Fields!fe_pedido)
                .TextMatrix(.Rows - 1, col_PET_per_nrointerno) = ClearNull(aplRST.Fields!per_nrointerno)
                .TextMatrix(.Rows - 1, col_PET_NroAsignado) = ClearNull(aplRST.Fields!pet_nroasignado)
                .TextMatrix(.Rows - 1, col_PET_Tipo) = ClearNull(aplRST.Fields!cod_tipo_peticion)
                .TextMatrix(.Rows - 1, col_PET_Clase) = ClearNull(aplRST.Fields!cod_clase)
                .TextMatrix(.Rows - 1, col_PET_Regulatorio) = ClearNull(aplRST.Fields!pet_regulatorio)
                .TextMatrix(.Rows - 1, col_PET_Titulo) = ClearNull(aplRST.Fields!Titulo)
                .TextMatrix(.Rows - 1, col_PET_SectorSolicitante) = ClearNull(aplRST.Fields!nom_sector)
                .TextMatrix(.Rows - 1, col_PET_Puntaje) = IIf(aplRST.Fields!puntuacion = 9999999, "-", Format(ClearNull(aplRST.Fields!puntuacion), "###,##0.00"))
                .TextMatrix(.Rows - 1, col_PET_Prioridad) = ClearNull(aplRST.Fields!prioridad)
                .TextMatrix(.Rows - 1, col_PET_estado) = ClearNull(aplRST.Fields!nom_estado)
                .TextMatrix(.Rows - 1, col_PET_Analisis) = ClearNull(aplRST.Fields!analisis)
                .TextMatrix(.Rows - 1, col_PET_Compromiso) = ClearNull(aplRST.Fields!planificado)
                .TextMatrix(.Rows - 1, col_PET_HorasPresup) = ClearNull(aplRST.Fields!horaspresup)
                .TextMatrix(.Rows - 1, col_PET_PlanIni) = IIf(ClearNull(aplRST.Fields!fe_ini_plan) = "", "-", ClearNull(aplRST.Fields!fe_ini_plan))
                .TextMatrix(.Rows - 1, col_PET_PlanFin) = IIf(ClearNull(aplRST.Fields!fe_fin_plan) = "", "-", ClearNull(aplRST.Fields!fe_fin_plan))
                .TextMatrix(.Rows - 1, col_PET_RealIni) = IIf(ClearNull(aplRST.Fields!fe_ini_real) = "", "-", ClearNull(aplRST.Fields!fe_ini_real))
                .TextMatrix(.Rows - 1, col_PET_RealFin) = IIf(ClearNull(aplRST.Fields!fe_fin_real) = "", "-", ClearNull(aplRST.Fields!fe_fin_real))
                If IsNull(aplRST.Fields!cant_planif) Then
                    .TextMatrix(.Rows - 1, col_PET_Replanificaciones) = "-"
                Else
                    .TextMatrix(.Rows - 1, col_PET_Replanificaciones) = IIf(Val(aplRST.Fields!cant_planif) > 0, aplRST.Fields!cant_planif, "-")
                End If
                .TextMatrix(.Rows - 1, col_PET_Comprometido) = ClearNull(aplRST.Fields!compromiso)
                .TextMatrix(.Rows - 1, col_PET_Cumplido) = ClearNull(aplRST.Fields!cumplido)
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                    Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorDarkGrey)
                End If
                ' Columnas invisibles
                .TextMatrix(.Rows - 1, col_PET_codigoperiodo) = IIf(ClearNull(aplRST.Fields!per_nrointerno) = 0, "-", ClearNull(aplRST.Fields!per_nrointerno))  ' add -007- a.
                .TextMatrix(.Rows - 1, col_PET_pet_nrointerno) = ClearNull(aplRST.Fields!pet_nrointerno)
                .TextMatrix(.Rows - 1, col_PET_cod_estado) = ClearNull(aplRST.Fields!cod_estado)
                .TextMatrix(.Rows - 1, col_PET_cod_bpar) = ClearNull(aplRST.Fields!cod_bpar)
                .TextMatrix(.Rows - 1, col_PET_nom_bpe) = ClearNull(aplRST.Fields!nom_BPE)
                .TextMatrix(.Rows - 1, col_PET_nom_bpar) = ClearNull(aplRST.Fields!nom_bpar)
                .TextMatrix(.Rows - 1, col_PET_PrioridadAnterior) = ClearNull(aplRST.Fields!prioridad)
                
                .TextMatrix(.Rows - 1, col_PET_ProjId) = ClearNull(aplRST.Fields!pet_projid)
                .TextMatrix(.Rows - 1, col_PET_ProjSubId) = ClearNull(aplRST.Fields!pet_projsubid)
                .TextMatrix(.Rows - 1, col_PET_ProjSubSId) = ClearNull(aplRST.Fields!pet_projsubsid)
                .TextMatrix(.Rows - 1, col_PET_ProjNom) = ClearNull(aplRST.Fields!projnom)
                If .Rows Mod 2 > 0 Then Call PintarLineaNoFix(grdDatos, prmGridFillRowColorLightGrey2)                       ' add -019- b.
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        udPriorizacionLimite.Max = .Rows - 1
        If Not .visible Then .visible = True
    End With
    bLoad = False
    Call Puntero(False)
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    Dim Valor As Long
    Dim bValid As Boolean
        
    'Debug.Print Now & "|" & KeyAscii
    
    If KeyAscii = 13 Then
        If grdDatos.ColSel = col_PET_Prioridad Then
            grdDatos.row = grdDatos.rowSel + 1
        End If
    End If
        
    bValid = False
    If Not IsNumeric(Chr(KeyAscii)) Then Exit Sub
End Sub

Private Sub grdDatos_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim bValid As Boolean
    
    If cModo <> "EDITAR" Then Exit Sub
    bValid = False
    With grdDatos
        Debug.Print "KeyCode " & KeyCode
        'Label1.Caption = .row = .rowSel And .col = .ColSel
        Select Case .ColSel
            Case col_PET_Prioridad
                Select Case KeyCode
                    Case 46: .TextMatrix(.row, .col) = "": bValid = True    ' Del: borra la prioridad
                    Case 48, 96: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "0", "0")
                    Case 49, 97: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "1", "1")
                    Case 50, 98: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "2", "2")
                    Case 51, 99: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "3", "3")
                    Case 52, 100: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "4", "4")
                    Case 53, 101: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "5", "5")
                    Case 54, 102: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "6", "6")
                    Case 55, 103: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "7", "7")
                    Case 56, 104: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "8", "8")
                    Case 57, 105: .TextMatrix(.row, .col) = IIf(Not bNewCell, .TextMatrix(.row, .col) & "9", "9")
                Case Else
                    If IsNumeric(Chr(KeyCode)) Then
                        .TextMatrix(.row, col_PET_Prioridad) = .TextMatrix(.row, col_PET_Prioridad) & Chr(KeyCode)
                    End If
                End Select
        End Select
    End With
    bNewCell = False
End Sub

Private Sub grdDatos_LeaveCell()
    ' aqui deberia validarse
    bNewCell = True
    With grdDatos
        If .ColSel = col_PET_Prioridad Then
            .TextMatrix(.row, .col) = Format(.TextMatrix(.row, .col), "#####0")
        End If
    End With
End Sub

Private Sub grdDatos_EnterCell()
    If Not bLoad And Not bSorting And Not bFormateando Then
        cUltimoValor = grdDatos.TextMatrix(grdDatos.row, grdDatos.col)
    End If
End Sub

Private Sub grdDatos_LostFocus()
    bNewCell = True
End Sub

Private Sub grdDatos_RowColChange()
    If Not bLoad And Not bSorting And Not bFormateando Then
        cValor = ""
    End If
End Sub

Private Sub cboFiltroRefBPE_Click()
    If Not bLoad Then Call CargarGrilla
End Sub

Private Sub cboFiltroRefSistemas_Click()
    If Not bLoad Then Call CargarGrilla
End Sub

Private Sub cmdGuardar_Click()
    Const ERROR_PRIORIZACION_REPETIDA As Long = 0
    Const ERROR_PRIORIZACION_INCOMPLETA As Long = 1
    Const ERROR_PRIORIZACION_SALTOS As Long = 2
    
    Dim i As Long, J As Long, k As Long
    Dim sMensajeHistorial As String
    Dim bContinuar As Boolean
    Dim vPriorizacion() As Long
    'Dim vPriorizacionOK() As Long               ' Guarda todos los elementos de priorizaci�n actuales
    Dim prioridadAsignada As Long
    Dim prioridadRepetida As Long
    Dim bErrores(2) As Boolean
    Dim NroHistorial As Long
    
    bContinuar = True
    bErrores(ERROR_PRIORIZACION_REPETIDA) = False
    bErrores(ERROR_PRIORIZACION_INCOMPLETA) = False
    bErrores(ERROR_PRIORIZACION_SALTOS) = False
    Erase vPriorizacion
    
    ' Controles
    With grdDatos
        Select Case cModoTrabajo
            Case "GBPE", "SADM"
                '1. Control de completitud de priorizaci�n
                For i = 1 To .Rows - 1
                    If IsNumeric(.TextMatrix(i, col_PET_Prioridad)) Or ClearNull(.TextMatrix(i, col_PET_Prioridad)) = "" Then
                        ReDim Preserve vPriorizacion(i - 1)
                        vPriorizacion(i - 1) = IIf(IsNumeric(.TextMatrix(i, col_PET_Prioridad)), .TextMatrix(i, col_PET_Prioridad), 0)
                    End If
                Next i
                
                ' 2. Control de no repetici�n de orden de priorizaci�n
                For i = 1 To .Rows - 1
                    If IsNumeric(.TextMatrix(i, col_PET_Prioridad)) Or ClearNull(.TextMatrix(i, col_PET_Prioridad)) = "" Then
                        prioridadAsignada = IIf(IsNumeric(.TextMatrix(i, col_PET_Prioridad)), .TextMatrix(i, col_PET_Prioridad), 0)
                        prioridadRepetida = 0
                        For J = 0 To UBound(vPriorizacion)
                            If vPriorizacion(J) <> 0 And vPriorizacion(J) = prioridadAsignada Then
                                prioridadRepetida = prioridadRepetida + 1
                                If prioridadRepetida > 1 Then
                                    bErrores(ERROR_PRIORIZACION_REPETIDA) = True      ' Se repite un orden de priorizaci�n
                                    Exit For
                                End If
                            End If
                        Next J
                    End If
                Next i
                
                ' 3. Control de que no queden "huecos" en la numeraci�n
                Erase vPriorizacion
                J = 0
                For i = 1 To .Rows - 1
                    If IsNumeric(.TextMatrix(i, col_PET_Prioridad)) Then
                        ReDim Preserve vPriorizacion(J)
                        vPriorizacion(J) = .TextMatrix(i, col_PET_Prioridad)
                        J = J + 1
                    End If
                Next i
                
                Call ordenar(vPriorizacion)
                
                k = 0
                For i = 0 To J - 1
                    k = k + 1
                    If vPriorizacion(i) <> k Then
                        bErrores(ERROR_PRIORIZACION_SALTOS) = True
                        Exit For
                    End If
                Next i
            Case Else
                bContinuar = False
        End Select
    End With
    
    If bErrores(ERROR_PRIORIZACION_REPETIDA) Or bErrores(ERROR_PRIORIZACION_INCOMPLETA) Or bErrores(ERROR_PRIORIZACION_SALTOS) Then
        If bErrores(ERROR_PRIORIZACION_REPETIDA) Then
            MsgBox "Se repiti� un n�mero de priorizaci�n. Corrija para continuar.", vbExclamation + vbOKOnly
        End If
        If bErrores(ERROR_PRIORIZACION_SALTOS) Then
            MsgBox "La asignaci�n de prioridades tiene saltos (falta el " & k & "). Corrija para continuar.", vbExclamation + vbOKOnly
        End If
        bContinuar = False
    End If
    
    If bContinuar Then
        If MsgBox("�Est� seguro de guardar los cambios realizados?", vbOKCancel + vbQuestion, "Guardar los cambios") = vbOK Then
            Call Puntero(True)
            With grdDatos
                For i = 1 To .Rows - 1          ' Debe guardar el dato de priorizaci�n (dato de la petici�n)
                    If IsNumeric(.TextMatrix(i, col_PET_Prioridad)) Or ClearNull(.TextMatrix(i, col_PET_Prioridad)) = "" Then
                        If .TextMatrix(i, col_PET_Prioridad) <> .TextMatrix(i, col_PET_PrioridadAnterior) Then
                            sMensajeHistorial = "Per�odo " & .TextMatrix(i, col_PET_codigoperiodo) & ": " & .TextMatrix(i, col_PET_Periodo) & " | Cambio en la priorizaci�n: " & IIf(.TextMatrix(i, col_PET_Prioridad) = "", "ninguna", .TextMatrix(i, col_PET_Prioridad)) & " (antes: " & IIf(.TextMatrix(i, col_PET_PrioridadAnterior) = "", "ninguna", .TextMatrix(i, col_PET_PrioridadAnterior)) & ")."
                            Call sp_UpdatePeticionPlancab(.TextMatrix(i, col_PET_per_nrointerno), .TextMatrix(i, col_PET_pet_nrointerno), .TextMatrix(i, col_PET_Prioridad), glLOGIN_ID_REEMPLAZO)
                            Call sp_AddHistorial(.TextMatrix(i, col_PET_pet_nrointerno), "PCHGPRI2", .TextMatrix(i, col_PET_cod_estado), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sMensajeHistorial)
                        Else
                            If ClearNull(.TextMatrix(i, col_PET_Prioridad)) <> "" And _
                                ClearNull(.TextMatrix(i, col_PET_Prioridad)) <> ClearNull(.TextMatrix(i, col_PET_PrioridadAnterior)) Then
                                    sMensajeHistorial = "Per�odo " & .TextMatrix(i, col_PET_codigoperiodo) & ": " & .TextMatrix(i, col_PET_Periodo) & " | Asignaci�n inicial de priorizaci�n: " & IIf(.TextMatrix(i, col_PET_Prioridad) = "", "ninguna", .TextMatrix(i, col_PET_Prioridad)) & " (antes: " & IIf(.TextMatrix(i, col_PET_PrioridadAnterior) = "", "ninguna", .TextMatrix(i, col_PET_PrioridadAnterior)) & ")."
                                    Call sp_UpdatePeticionPlancab(.TextMatrix(i, col_PET_per_nrointerno), .TextMatrix(i, col_PET_pet_nrointerno), .TextMatrix(i, col_PET_Prioridad), glLOGIN_ID_REEMPLAZO)
                                    Call sp_AddHistorial(.TextMatrix(i, col_PET_pet_nrointerno), "PCHGPRI1", .TextMatrix(i, col_PET_cod_estado), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sMensajeHistorial)
                            End If
                        End If
                        If ClearNull(.TextMatrix(i, col_PET_cod_estado)) = "REFBPE" And IsNumeric(.TextMatrix(i, col_PET_Prioridad)) Then
                            ' ********************************************************************************
                            ' Cambio de estado "Al ref. de RGyP" a "Al ref. de sistemas"
                            ' ********************************************************************************
                            Call sp_DeleteMensaje(.TextMatrix(i, col_PET_pet_nrointerno), 0)
                            Call sp_UpdatePetField(.TextMatrix(i, col_PET_pet_nrointerno), "ESTADO", "COMITE", Null, Null)
                            Call sp_UpdatePetField(.TextMatrix(i, col_PET_pet_nrointerno), "SITUAC", "", Null, Null)
                            Call sp_UpdatePetField(.TextMatrix(i, col_PET_pet_nrointerno), "NROANEX", "", Null, 0)
                            'se eliminan los sec/gru opinion/evaluacion
                            Call sp_DeletePeticionSectorEstado(.TextMatrix(i, col_PET_pet_nrointerno), "OPINIO|OPINOK|EVALUA|EVALOK")
                            ' TODO: revisar en el sp_AddHistorial que no genere la l�nea en HistorialMemo si no se ingresa una observaci�n (evitar grabar "<< >>" sin sentido).
                            NroHistorial = sp_AddHistorial(.TextMatrix(i, col_PET_pet_nrointerno), "PCHGEST", "COMITE", Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Se pasa al referente de sistema por asignaci�n de prioridad. �")
                            Call sp_UpdatePetField(.TextMatrix(i, col_PET_pet_nrointerno), "FECHACOM", Null, Format(Now, "dd/mm/yyyy"), Null)
                        End If
                    End If
                Next i
            End With
            Call Puntero(False)
            cModo = "VISTA"
            If bContinuar Then Call cmdRefresh_Click
            Call HabilitarBotones
            MsgBox "Proceso finalizado.", vbInformation
        End If
    End If
End Sub

Private Sub ordenar(vector() As Long)
    Dim i As Long, J As Long, aux As Long
    Dim Limite As Long
    
    Limite = UBound(vector)
    
    For i = 1 To Limite
        For J = 0 To Limite - 1
            If vector(J) > vector(J + 1) Then
                aux = vector(J)
                vector(J) = vector(J + 1)
                vector(J + 1) = aux
            End If
        Next J
    Next i
End Sub

Private Sub listar(vector() As Long)
    Dim i As Long
    Dim Resultado As String
    
    For i = 0 To UBound(vector)
        If i = 0 Then
            Resultado = vector(i)
        Else
            Resultado = Resultado & "." & vector(i)
        End If
    Next i
    
    Debug.Print "Orden: " & Resultado
End Sub

Private Sub grdDatos_DblClick()
    With grdDatos
        If cModo = "EDITAR" Then
            MsgBox "Mientras est� editando la planificaci�n no puede acceder a la petici�n", vbExclamation + vbOKOnly
            Exit Sub
        End If
        If .rowSel > 0 Then
            Call Puntero(True)
            glNumeroPeticion = .TextMatrix(.rowSel, col_PET_pet_nrointerno)
            If glNumeroPeticion <> "" Then
                If Not LockProceso(True) Then Exit Sub
                glModoPeticion = "VIEW"
                On Error Resume Next
                frmPeticionesCarga.Show vbModal
                DoEvents
                grdDatos.SetFocus
            End If
        End If
    End With
End Sub

Private Sub cmdPriorizarTodo_Click()
    Dim i As Long
    Dim Limite As Long
    
    With grdDatos
        Limite = .Rows - 1
        If IsNumeric(txtPriorizacionLimite) Then
            If CLng(txtPriorizacionLimite) > 0 Then
                If CLng(txtPriorizacionLimite) < .Rows - 1 Then
                    Limite = CLng(txtPriorizacionLimite)
                Else
                    txtPriorizacionLimite = Limite
                End If
            End If
        End If
    
        .Enabled = False
        For i = 1 To Limite
            .TextMatrix(i, col_PET_Prioridad) = i
        Next i
        .Enabled = True
    End With
End Sub

Private Sub cmdPriorizarNada_Click()
    Dim i As Long
    Dim Limite As Long
    
    With grdDatos
        Limite = .Rows - 1
        If IsNumeric(txtPriorizacionLimite) Then
            If CLng(txtPriorizacionLimite) > 0 Then
                If CLng(txtPriorizacionLimite) < .Rows - 1 Then
                    Limite = CLng(txtPriorizacionLimite)
                Else
                    txtPriorizacionLimite = Limite
                End If
            End If
        End If
    
        .Enabled = False
        For i = 1 To Limite
            .TextMatrix(i, col_PET_Prioridad) = ""
        Next i
        .Enabled = True
    End With
End Sub

Private Sub cmdRefresh_Click()
    Call CargarGrilla
End Sub

Private Sub cmdCerrar_Click()
    Me.Hide
End Sub

Private Sub grdDatos_Click()
    bSorting = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada, , , True)
    bSorting = False
End Sub

Private Sub grdDatos_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        MenuGridFind grdDatos
    End If
End Sub

Private Sub cmdAgregar_Click()
    If EstadoPeriodoActual = PERIODO_CERRADO Then
        MsgBox "No puede incorporar peticiones a per�odos cerrados.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    Load frmPeticionesPlanificarAgregar
    With frmPeticionesPlanificarAgregar
        .Show 1
        If .bHayCambios Then
            cmdRefresh_Click
        End If
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub cboFiltroEstadosPeticion_Click()
    If Not bLoad Then Call CargarGrilla
End Sub

Private Sub cboFiltroSituacion_Click()
    If Not bLoad Then Call CargarGrilla
End Sub

Private Sub cboFiltroPriorizadas_Click()
    If Not bLoad Then Call CargarGrilla
End Sub

Private Sub cboFiltroGrupoEjecutor_Click()
    If CodigoCombo(cboFiltroGrupoEjecutor, True) = "SECT" Then Exit Sub
    If Not bLoad Then Call CargarGrilla
End Sub

Private Sub HabilitarBotones()
    Call setHabilCtrl(cmdRefresh, NORMAL)
    Call setHabilCtrl(cmdExportar, NORMAL)
    
    Select Case glUsrPerfilActual
        Case "GBPE"
            Call setHabilCtrl(cmdPriorizarTodo, DISABLE)
            Call setHabilCtrl(cmdPriorizarNada, DISABLE)
            Call setHabilCtrl(txtPriorizacionLimite, DISABLE): udPriorizacionLimite.Enabled = False
            Call setHabilCtrl(cmdAgregar, DISABLE)
            Call setHabilCtrl(cmdEditar, DISABLE)
            Call setHabilCtrl(cmdGuardar, DISABLE)
            Call setHabilCtrl(cmdQuitar, DISABLE)
            Call setHabilCtrl(cmdDescartar, DISABLE)
            If EstadoPeriodoActual = PERIODO_CERRADO Then
                Call setHabilCtrl(cmdPriorizarTodo, DISABLE)
                Call setHabilCtrl(cmdPriorizarNada, DISABLE)
                Call setHabilCtrl(txtPriorizacionLimite, DISABLE): udPriorizacionLimite.Enabled = False
                Call setHabilCtrl(cmdAgregar, DISABLE)
                Call setHabilCtrl(cmdEditar, DISABLE)
                Call setHabilCtrl(cmdGuardar, DISABLE)
                Call setHabilCtrl(cmdQuitar, DISABLE)
                Call setHabilCtrl(cmdDescartar, DISABLE)
            Else
                If InPerfil("CSEC") Then
                    Select Case cModo
                        Case "EDITAR"
                            Call setHabilCtrl(cmdPriorizarTodo, NORMAL)
                            Call setHabilCtrl(cmdPriorizarNada, NORMAL)
                            Call setHabilCtrl(txtPriorizacionLimite, NORMAL): udPriorizacionLimite.Enabled = True
                            Call setHabilCtrl(cmdRefresh, DISABLE)
                            Call setHabilCtrl(cmdExportar, DISABLE)
                            Call setHabilCtrl(cmdAgregar, DISABLE)
                            Call setHabilCtrl(cmdEditar, DISABLE)
                            Call setHabilCtrl(cmdGuardar, NORMAL)
                            Call setHabilCtrl(cmdQuitar, DISABLE)
                            Call setHabilCtrl(cmdDescartar, NORMAL)
                            Call setHabilCtrl(cmdCerrar, DISABLE)
                            grdDatos.SetFocus
                        Case "CANCEL", "VISTA"
                            Call setHabilCtrl(cmdPriorizarTodo, DISABLE)
                            Call setHabilCtrl(cmdPriorizarNada, DISABLE)
                            Call setHabilCtrl(txtPriorizacionLimite, DISABLE): udPriorizacionLimite.Enabled = False
                            Call setHabilCtrl(cmdRefresh, NORMAL)
                            Call setHabilCtrl(cmdExportar, NORMAL)
                            Call setHabilCtrl(cmdAgregar, NORMAL)
                            Call setHabilCtrl(cmdEditar, NORMAL)
                            Call setHabilCtrl(cmdGuardar, DISABLE)
                            Call setHabilCtrl(cmdQuitar, NORMAL)
                            Call setHabilCtrl(cmdDescartar, DISABLE)
                            Call setHabilCtrl(cmdCerrar, NORMAL)
                    End Select
                End If
            End If
        Case Else
            Call setHabilCtrl(cmdPriorizarTodo, DISABLE)
            Call setHabilCtrl(cmdPriorizarNada, DISABLE)
            Call setHabilCtrl(txtPriorizacionLimite, DISABLE): udPriorizacionLimite.Enabled = False
            Call setHabilCtrl(cmdGuardar, DISABLE)
            Call setHabilCtrl(cmdAgregar, DISABLE)
            Call setHabilCtrl(cmdEditar, DISABLE)
            Call setHabilCtrl(cmdQuitar, DISABLE)
            Call setHabilCtrl(cmdDescartar, DISABLE)
    End Select
End Sub

Private Sub txtPriorizacionLimite_Change()
    If Not IsNumeric(txtPriorizacionLimite.text) Then
        txtPriorizacionLimite.text = ""
    End If
End Sub

Private Sub cmdEditar_Click()
    If Not LockProceso(True) Then Exit Sub
    cModo = "EDITAR"
    Call HabilitarBotones
    Call LockProceso(False)
End Sub

Private Sub cmdDescartar_Click()
    Dim i As Long
    
    If MsgBox("�Confirma descartar los cambios realizados?", vbQuestion + vbYesNo) = vbNo Then
        Exit Sub
    End If
    
    If Not LockProceso(True) Then Exit Sub
    With grdDatos
        If cModo = "EDITAR" Then
            For i = 1 To .Rows - 1
                If ClearNull(.TextMatrix(i, col_PET_Prioridad)) <> "" Then
                    If ClearNull(.TextMatrix(i, col_PET_Prioridad)) <> ClearNull(.TextMatrix(i, col_PET_PrioridadAnterior)) Then
                        .TextMatrix(i, col_PET_Prioridad) = ClearNull(.TextMatrix(i, col_PET_PrioridadAnterior))
                    End If
                End If
            Next i
        End If
    End With
    cModo = "CANCEL"
    Call HabilitarBotones
    Call LockProceso(False)
End Sub

Private Sub cmdExportar_Click()
    Dim lPeriodoSeleccionado As Long
    Dim sNombreArchivo As String
    lPeriodoSeleccionado = IIf(CodigoCombo(cboFiltroPeriodo, True) = "NULL", 0, CodigoCombo(cboFiltroPeriodo, True))
    
    If sp_GetPetPlanificar(IIf(lPeriodoSeleccionado = 0, Null, lPeriodoSeleccionado), CodigoCombo(cboFiltroRefBPE, True), CodigoCombo(cboFiltroRefSistemas, True), CodigoCombo(cboFiltroSituacion, True), CodigoCombo(cboFiltroEstadosPeticion, True), CodigoCombo(cboFiltroGrupoEjecutor, True), CodigoCombo(cboFiltroPriorizadas, True)) Then
        If aplRST.RecordCount > 0 Then
            sNombreArchivo = Dialogo_GuardarExportacion("GUARDAR")
            If ClearNull(sNombreArchivo) <> "" Then
                If MsgBox("�Confirma la exportaci�n?", vbQuestion + vbOKCancel, "Exportaci�n") = vbOK Then
                    Call Status("Preparando la exportaci�n...")
                    Call Puntero(True)
                    Call ExportarAExcel(aplRST, sNombreArchivo)
                    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
                    Call Status("Listo.")
                    Call Puntero(False)
                End If
            End If
        End If
    End If
End Sub

Private Function ExportarAExcel(rst, sOutputPathXLS As String) As Boolean
    Dim Excel       As Object
    Dim Libro       As Object
    Dim Hoja        As Object
    Dim arrData     As Variant
    Dim iRec        As Long
    Dim iCol        As Integer
    Dim iRow        As Integer
    
    Set Excel = CreateObject("Excel.Application")
    Set Libro = Excel.Workbooks.Add
      
    ' -- Hacer referencia a la hoja
    Set Hoja = Libro.WorkSheets(1)
      
    Excel.visible = False: Excel.UserControl = True
    iCol = rst.Fields.Count
    For iCol = 1 To rst.Fields.Count
        Hoja.Cells(1, iCol).value = rst.Fields(iCol - 1).name
    Next
      
    If Val(Mid(Excel.Version, 1, InStr(1, Excel.Version, ".") - 1)) > 8 Then
        Hoja.Cells(2, 1).CopyFromRecordset (rst)
    Else
        arrData = rst.GetRows
        iRec = UBound(arrData, 2) + 1
        For iCol = 0 To rst.Fields.Count - 1
            For iRow = 0 To iRec - 1
                If IsDate(arrData(iCol, iRow)) Then
                    arrData(iCol, iRow) = Format(arrData(iCol, iRow))
                ElseIf IsArray(arrData(iCol, iRow)) Then
                    arrData(iCol, iRow) = "Array Field"
                End If
            Next iRow
        Next iCol
        ' -- Traspasa los datos a la hoja de Excel
        Hoja.Cells(2, 1).Resize(iRec, rst.Fields.Count).value = GetData(arrData)
    End If
  
    Excel.Selection.CurrentRegion.Columns.AutoFit
    Excel.Selection.CurrentRegion.Rows.AutoFit
    rst.Close
    Set rst = Nothing
    
    ' -- Guardar el libro
    Libro.SaveAs sOutputPathXLS
    Libro.Close
    ' -- Elimina las referencias Xls
    Set Hoja = Nothing
    Set Libro = Nothing
    Excel.Quit
    Set Excel = Nothing
      
    ExportarAExcel = True
    Me.Enabled = True
    Exit Function
errSub:
    MsgBox Err.DESCRIPTION, vbCritical, "Error"
    ExportarAExcel = False
    Me.Enabled = True
End Function

Private Function GetData(vValue As Variant) As Variant
    Dim x As Long, y As Long, xMax As Long, yMax As Long, T As Variant
      
    xMax = UBound(vValue, 2): yMax = UBound(vValue, 1)
      
    ReDim T(xMax, yMax)
    For x = 0 To xMax
        For y = 0 To yMax
            T(x, y) = vValue(y, x)
        Next y
    Next x
    GetData = T
End Function
