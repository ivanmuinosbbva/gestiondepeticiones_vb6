VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPeticionChangemanOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n"
   ClientHeight    =   4920
   ClientLeft      =   2565
   ClientTop       =   1500
   ClientWidth     =   6150
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionChangemanOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4920
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.Slider sldDepurar 
      Height          =   495
      Left            =   240
      TabIndex        =   11
      Top             =   1320
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   873
      _Version        =   393216
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3780
      Index           =   3
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample4 
         Caption         =   "Sample 4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Left            =   2100
         TabIndex        =   7
         Top             =   840
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3780
      Index           =   2
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample3 
         Caption         =   "Sample 3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Left            =   1545
         TabIndex        =   6
         Top             =   675
         Width           =   2055
      End
   End
   Begin VB.PictureBox picOptions 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3780
      Index           =   1
      Left            =   -20000
      ScaleHeight     =   3780
      ScaleWidth      =   5685
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   480
      Width           =   5685
      Begin VB.Frame fraSample2 
         Caption         =   "Sample 2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1785
         Left            =   645
         TabIndex        =   5
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Aplicar"
      Height          =   375
      Left            =   4920
      TabIndex        =   1
      Top             =   4455
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cerrar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3720
      TabIndex        =   0
      Top             =   4455
      Width           =   1095
   End
   Begin VB.Label lblDescripcion 
      AutoSize        =   -1  'True
      Caption         =   $"frmPeticionChangemanOptions.frx":000C
      Height          =   540
      Left            =   240
      TabIndex        =   10
      Top             =   600
      Width           =   5340
      WordWrap        =   -1  'True
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   6000
      Y1              =   4320
      Y2              =   4320
   End
   Begin VB.Label lblMensaje 
      AutoSize        =   -1  'True
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   180
      Left            =   240
      TabIndex        =   9
      Top             =   1920
      Width           =   495
   End
   Begin VB.Label lblOptions 
      AutoSize        =   -1  'True
      Caption         =   "Cantidad de d�as antes de realizar la depuraci�n"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   8
      Top             =   240
      Width           =   4740
   End
End
Attribute VB_Name = "frmPeticionChangemanOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    'center the form
    Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
    Inicializar
End Sub

Private Sub Inicializar()
    If sp_GetVarios("CGMPRM1") Then
        If Not aplRST.EOF Then
            ' Esto quiere decir que no existe la definici�n de par�metros en la tabla Varios
            If CLng(aplRST.Fields!var_numero) = 0 Then
                aplRST.Close: Set aplRST = Nothing
                If Not sp_UpdateVarios("CGMPRM1", 15, "", date) Then
                    MsgBox "No se ha podido inicializar el par�metro de cantidad de d�as para la autodepuraci�n."
                Else
                    Inicializar
                End If
            Else
                ' Carga el valor del par�metro en el control Slider
                lblMensaje = "La bandeja diaria se autodepura cada " & CLng(aplRST.Fields!var_numero) & " d�a(s)."
                sldDepurar.Value = CLng(aplRST.Fields!var_numero)
            End If
        End If
    End If
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub sldDepurar_Scroll()
    lblMensaje = "La bandeja diaria se autodepura cada " & sldDepurar.Value & " d�a(s)."
    sldDepurar.SelectRange = True
End Sub

Private Sub cmdApply_Click()
    If MsgBox("�Confirma la modificaci�n del par�metro de autodepuraci�n?", vbQuestion + vbYesNo, "Modificaci�n") = vbYes Then
        If Not sp_UpdateVarios("CGMPRM1", CLng(sldDepurar.Value), "", date) Then
            MsgBox "No se ha podido inicializar el par�metro de cantidad de d�as para la autodepuraci�n."
        Else
            Inicializar
        End If
    End If
End Sub
