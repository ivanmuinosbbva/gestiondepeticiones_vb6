VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAplicativosShell 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Trabajar con Aplicativos"
   ClientHeight    =   7305
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10905
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAplicativosShell.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7305
   ScaleWidth      =   10905
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraOpciones 
      Caption         =   " Opciones de filtro "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   9195
      Begin VB.TextBox txtCodigo 
         Height          =   285
         Left            =   2400
         TabIndex        =   7
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   2400
         TabIndex        =   6
         Top             =   720
         Width           =   5295
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "Para editar un aplicativo en particular, presione Enter sobre el aplicativo selecionado o haga doble click"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   150
         Index           =   1
         Left            =   120
         TabIndex        =   10
         Top             =   1080
         Width           =   6255
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "C�digo de aplicativo:"
         ForeColor       =   &H00000000&
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   1590
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "Nombre o descripci�n:"
         Height          =   180
         Index           =   3
         Left            =   240
         TabIndex        =   8
         Top             =   720
         Width           =   1695
      End
   End
   Begin VB.Frame fraBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7095
      Left            =   9360
      TabIndex        =   0
      Top             =   120
      Width           =   1455
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Default         =   -1  'True
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   6600
         Width           =   1215
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5655
      Left            =   120
      TabIndex        =   11
      Top             =   1560
      Width           =   9195
      _ExtentX        =   16219
      _ExtentY        =   9975
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   8421504
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAplicativosShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim aplRSTAux As ADODB.Recordset

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    InicializarGrilla
    CargarGrilla
    modUser32.IniciarScroll grdDatos
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .visible = False
        .Clear
        .cols = 14
        .Rows = 1
        .TextMatrix(0, 0) = "Codigo"
        .TextMatrix(0, 1) = "Nombre o descripci�n de aplicativo"
        .TextMatrix(0, 2) = "F. Alta"
        .TextMatrix(0, 3) = "Ult.Mod."
        .TextMatrix(0, 4) = "Ambiente"
        .TextMatrix(0, 5) = "Hab."
        .TextMatrix(0, 6) = "Direcci�n"
        .TextMatrix(0, 7) = "Responsable"
        .TextMatrix(0, 8) = "Gerencia"
        .TextMatrix(0, 9) = "Responsable"
        .TextMatrix(0, 10) = "Sector"
        .TextMatrix(0, 11) = "Responsable"
        .TextMatrix(0, 12) = "Grupo"
        .TextMatrix(0, 13) = "Responsable"
        .ColWidth(0) = 2000: .ColAlignment(0) = flexAlignLeftCenter
        .ColWidth(1) = 4000
        .ColWidth(2) = 1200
        .ColWidth(3) = 1200
        .ColWidth(4) = 1200
        .ColWidth(5) = 500
        .ColWidth(6) = 3000
        .ColWidth(7) = 3000
        .ColWidth(8) = 3000
        .ColWidth(9) = 3000
        .ColWidth(10) = 3000
        .ColWidth(11) = 3000
        .ColWidth(12) = 3000
        .ColWidth(13) = 3000
        .BackColorBkg = Me.BackColor
        .Font.name = "Verdana"
        .Font.Size = 7
    End With
    
    Dim i As Long
    With grdDatos
        .FocusRect = flexFocusNone
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Row = 0
        For i = 0 To .cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .Col = 0
        .ColSel = .cols - 1
    End With
End Sub

Private Sub CargarGrilla()
    With grdDatos
        InicializarGrilla
        If sp_GetAplicativo(Null, Null, Null) Then
            Set aplRSTAux = aplRST      ' Asigno el recordset general al auxiliar
            Do While Not aplRSTAux.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, 0) = Trim(aplRSTAux.Fields.Item("app_id"))
                .TextMatrix(.Rows - 1, 1) = Trim(aplRSTAux.Fields.Item("app_nombre"))
                .TextMatrix(.Rows - 1, 2) = Format(aplRSTAux.Fields.Item("app_fe_alta"), "dd.mm.yyyy")
                .TextMatrix(.Rows - 1, 3) = Format(aplRSTAux.Fields.Item("app_fe_lupd"), "dd.mm.yyyy")
                Select Case aplRSTAux.Fields.Item("app_amb")
                    Case "D"
                        .TextMatrix(.Rows - 1, 4) = "Distribu�do"
                    Case "H"
                        .TextMatrix(.Rows - 1, 4) = "No distribu�do"
                    Case "B"
                        .TextMatrix(.Rows - 1, 4) = "Ambos"
                End Select
                .TextMatrix(.Rows - 1, 5) = IIf(aplRSTAux.Fields.Item("app_hab") = "S", "Si", "No")
                .TextMatrix(.Rows - 1, 6) = ClearNull(aplRSTAux.Fields.Item("nom_direccion"))
                '.TextMatrix(.Rows - 1, 7) = IIf(IsNull(aplRSTAux!usr_direccion), "", Trim(Trim(aplRSTAux.Fields.Item("usr_direccion"))))
                .TextMatrix(.Rows - 1, 8) = ClearNull(aplRSTAux.Fields.Item("nom_gerencia"))
                '.TextMatrix(.Rows - 1, 9) = IIf(IsNull(aplRSTAux.Fields!usr_gerencia), "", Trim(Trim(aplRSTAux.Fields.Item("usr_gerencia"))))
                .TextMatrix(.Rows - 1, 10) = IIf(IsNull(aplRSTAux.Fields.Item("nom_sector")), "***", Trim(aplRSTAux.Fields.Item("nom_sector")))
                '.TextMatrix(.Rows - 1, 11) = IIf(IsNull(aplRSTAux.Fields.Item("usr_sector")), "***", Trim(Trim(aplRSTAux.Fields.Item("usr_sector"))))
                .TextMatrix(.Rows - 1, 12) = IIf(IsNull(aplRSTAux.Fields.Item("nom_grupo")), "***", Trim(aplRSTAux.Fields.Item("nom_grupo")))
                '.TextMatrix(.Rows - 1, 13) = IIf(IsNull(aplRSTAux.Fields.Item("usr_grupo")), "***", Trim(Trim(aplRSTAux.Fields.Item("usr_grupo"))))
                aplRSTAux.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
End Sub

Private Sub CargarGrilla_Filtrada(rsAux As ADODB.Recordset)
    With grdDatos
        InicializarGrilla
        Do While Not rsAux.EOF
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 0) = Trim(rsAux.Fields.Item("app_id"))
            .TextMatrix(.Rows - 1, 1) = Trim(rsAux.Fields.Item("app_nombre"))
            .TextMatrix(.Rows - 1, 2) = Format(rsAux.Fields.Item("app_fe_alta"), "dd.mm.yyyy")
            .TextMatrix(.Rows - 1, 3) = Format(rsAux.Fields.Item("app_fe_lupd"), "dd.mm.yyyy")
            Select Case rsAux.Fields.Item("app_amb")
                Case "D"
                    .TextMatrix(.Rows - 1, 4) = "Distribu�do"
                Case "H"
                    .TextMatrix(.Rows - 1, 4) = "No distribu�do"
                Case "B"
                    .TextMatrix(.Rows - 1, 4) = "Ambos"
            End Select
            .TextMatrix(.Rows - 1, 5) = IIf(rsAux.Fields.Item("app_hab") = "S", "Si", "No")
            .TextMatrix(.Rows - 1, 6) = Trim(rsAux.Fields.Item("nom_direccion"))
            .TextMatrix(.Rows - 1, 7) = IIf(IsNull(rsAux!usr_direccion), "", Trim(Trim(rsAux.Fields.Item("usr_direccion"))))
            .TextMatrix(.Rows - 1, 8) = Trim(rsAux.Fields.Item("nom_gerencia"))
            .TextMatrix(.Rows - 1, 9) = IIf(IsNull(rsAux.Fields!usr_gerencia), "", Trim(Trim(rsAux.Fields.Item("usr_gerencia"))))
            .TextMatrix(.Rows - 1, 10) = IIf(IsNull(rsAux.Fields.Item("nom_sector")), "***", Trim(rsAux.Fields.Item("nom_sector")))
            .TextMatrix(.Rows - 1, 11) = IIf(IsNull(rsAux.Fields.Item("usr_sector")), "***", Trim(Trim(rsAux.Fields.Item("usr_sector"))))
            .TextMatrix(.Rows - 1, 12) = IIf(IsNull(rsAux.Fields.Item("nom_grupo")), "***", Trim(rsAux.Fields.Item("nom_grupo")))
            .TextMatrix(.Rows - 1, 13) = IIf(IsNull(rsAux.Fields.Item("usr_grupo")), "***", Trim(Trim(rsAux.Fields.Item("usr_grupo"))))
            rsAux.MoveNext
            DoEvents
        Loop
        .visible = True
    End With
End Sub

Private Sub Filtrar()
    Dim cFilters As String
   
    ' Inicializa los filtros para abarcar todos los items
    aplRSTAux.Filter = adFilterNone
    
    ' C�digo
    If Len(Trim(txtCodigo)) > 0 Then
        cFilters = "app_id LIKE '*" & Trim(txtCodigo) & "*'"
    End If
    ' Nombre
    If Len(Trim(txtNombre)) > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = Trim(cFilters) & " AND app_nombre LIKE '*" & Trim(txtNombre) & "*'"
        Else
            cFilters = "app_nombre LIKE '*" & Trim(txtNombre) & "*'"
        End If
    End If
    ' Si existe alg�n criterio, realiza el filtro...
    If Len(cFilters) > 0 Then
        aplRSTAux.Filter = cFilters
        CargarGrilla_Filtrada aplRSTAux
    Else
        CargarGrilla_Filtrada aplRSTAux
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub txtCodigo_Change()
    If Len(Trim(txtCodigo)) > 0 Then
        If Mid(txtCodigo, Len(txtCodigo), 1) = "'" Then
            txtCodigo = Mid(txtCodigo, 1, Len(txtCodigo) - 1)
        End If
    End If
    Filtrar
End Sub

Private Sub txtNombre_Change()
    If Len(Trim(txtNombre)) > 0 Then
        If Mid(txtNombre, Len(txtNombre), 1) = "'" Then
            txtNombre = Mid(txtNombre, 1, Len(txtNombre) - 1)
        End If
    End If
    Filtrar
End Sub

Private Sub cmdAgregar_Click()
    frmAplicativosCarga.pModo = "ALTA"
    frmAplicativosCarga.Show 1
End Sub

Private Sub cmdModificar_Click()
    frmAplicativosCarga.pModo = "MODI"
    frmAplicativosCarga.Show 1
End Sub
