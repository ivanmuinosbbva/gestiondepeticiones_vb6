VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptPetiTare 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5205
   ClientLeft      =   795
   ClientTop       =   2025
   ClientWidth     =   8715
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5205
   ScaleWidth      =   8715
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3765
      Left            =   0
      TabIndex        =   1
      Top             =   840
      Width           =   8715
      Begin VB.ComboBox cboTarea 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   840
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   720
         Width           =   7425
      End
      Begin VB.ComboBox cboPeticion 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   840
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   360
         Width           =   7425
      End
      Begin VB.CommandButton cmdMasPeticiones 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8280
         Picture         =   "rptPetiTare.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar otra Peticion que no figura en la lista"
         Top             =   360
         Width           =   315
      End
      Begin VB.Frame fraNivel 
         Caption         =   " Nivel de agrupamiento "
         Height          =   1965
         Left            =   5760
         TabIndex        =   9
         Top             =   1320
         Width           =   2505
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sin agrupar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   240
            TabIndex        =   15
            Top             =   1620
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Recurso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   1
            Left            =   240
            TabIndex        =   14
            Top             =   1365
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   13
            Top             =   300
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   4
            Left            =   240
            TabIndex        =   12
            Top             =   585
            Width           =   1125
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   3
            Left            =   240
            TabIndex        =   11
            Top             =   840
            Width           =   1245
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   240
            TabIndex        =   10
            Top             =   1110
            Width           =   1365
         End
      End
      Begin AT_MaskText.MaskText txtDESDE 
         Height          =   315
         Left            =   840
         TabIndex        =   5
         Top             =   1080
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTA 
         Height          =   315
         Left            =   840
         TabIndex        =   6
         Top             =   1440
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtCodtarea 
         Height          =   315
         Left            =   900
         TabIndex        =   19
         Top             =   2760
         Visible         =   0   'False
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPetnroasig 
         Height          =   315
         Left            =   900
         TabIndex        =   20
         Top             =   2400
         Visible         =   0   'False
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   428
         Width           =   555
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Tarea"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   788
         Width           =   420
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   1500
         Width           =   420
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   1140
         Width           =   450
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   2
      Top             =   4550
      Width           =   8715
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6780
         TabIndex        =   4
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7740
         TabIndex        =   3
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "1. HORAS TRABAJADAS POR POR PETICI�N / TAREA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4080
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptPetiTare.frx":058A
      Top             =   0
      Width           =   8700
   End
End
Attribute VB_Name = "rptPetiTare"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 15.05.2008 - Se cambia la asignaci�n de la propiedad 'ReportFileName' del control CrystalReport utilizando la funci�n de formato de ruta corto (8.3) porque por ejemplo, cuando el string tiene 127 caracteres da un error 20504 en runtime al momento de mostrar el reporte (da error de archivo de reporte no encontrado).
' -003- a. FJS 15.04.2009 - Se cambian los s�mbolos para optimizar la visualizaci�n de los datos del combo.
' -004- a. FJS 03.06.2009 - Se agrega una funci�n que discrimina y reemplaza los aprostrofos (generan en CR el error 20515).

Option Explicit

Dim flgEnCarga As Boolean
Dim xNivel As Long

Private Sub Form_Load()
    flgEnCarga = False
    Call InicializarCombos
    txtDESDE.text = Format(DateAdd("m", -1, date), "dd/mm/yyyy")
    txtHASTA.text = Format(date, "dd/mm/yyyy")
    'CrtPrn(1).Value = True
    'CrtPrn(0).Value = True
    OptNivel(4).value = True
    Call LockProceso(False)
End Sub

Private Sub cboTarea_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    flgEnCarga = True
    txtCodtarea = ""
    txtCodtarea = CodigoCombo(cboTarea)
    cboTarea.ToolTipText = "Descripci�n: " & TextoCombo(cboTarea, CodigoCombo(cboTarea, False), False)  ' add -xxx-
    'cboTarea.ToolTipText = TextoCombo(cboTarea, CodigoCombo(cboTarea, False), False)
    txtPetnroasig = ""
    cboPeticion.ListIndex = -1
    flgEnCarga = False
End Sub

Private Sub cboPeticion_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    flgEnCarga = True
    txtPetnroasig = ""
    txtPetnroasig = CodigoCombo(cboPeticion)
    cboPeticion.ToolTipText = "Descripci�n: " & TextoCombo(cboPeticion, CodigoCombo(cboPeticion, True), True)     ' add -xxx-
    txtCodtarea = ""
    cboTarea.ListIndex = -1
    flgEnCarga = False
    ' DESARROLLO
    Debug.Print CodigoCombo(cboPeticion, True)
End Sub

Private Sub cmdMasPeticiones_Click()
    Dim vRetorno() As String
    Dim nPos As Integer
    flgEnCarga = True
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glAuxRetorno = ""
    glAuxEstado = "ANULA|CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|RECHAZ|RECHTE|CANCEL|SUSPEN|REVISA|TERMIN"
    frmSelPet.Show 1
    DoEvents
    If glAuxRetorno <> "" Then
        If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
            txtCodtarea = ""
            cboTarea.ListIndex = -1
            nPos = PosicionCombo(cboPeticion, vRetorno(3), True)
            If nPos = -1 Then
                cboPeticion.AddItem glAuxRetorno
                nPos = PosicionCombo(cboPeticion, vRetorno(3), True)
                If nPos = -1 Then
                    MsgBox ("Error al incorporar petici�n")
                    cboPeticion.ListIndex = 0
                Else
                    cboPeticion.ListIndex = nPos
                End If
            Else
                cboPeticion.ListIndex = nPos
            End If
            txtPetnroasig = ""
            txtPetnroasig = CodigoCombo(cboPeticion)
        End If
        cboPeticion.Refresh
        cboPeticion.SetFocus
    End If
    flgEnCarga = False
    Call LockProceso(False)
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdOK_Click()
    If Not IsDate(txtDESDE.DateValue) Then
        MsgBox "La fecha desde no es v�lida", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    If Not IsDate(txtHASTA.DateValue) Then
        MsgBox "La fecha hasta no es v�lida", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox "La fecha hasta no puede ser menor a la fecha desde", vbExclamation + vbOKOnly
        Exit Sub
    End If

    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        'Call ViejoReporte
    End If
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim vRetorno() As String
    Dim sReportName As String
    Dim txtAux As String
    
    If cboPeticion.ListIndex = -1 And cboTarea.ListIndex = -1 Then
        MsgBox "Debe seleccionar una petici�n o una tarea", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    
    Call Puntero(True)
    Call Status("Cargando el informe...")
    sReportName = "\hstrapeta6.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Horas trabajadas por petici�n / tarea"
        txtAux = CodigoCombo(Me.cboPeticion)
        If cboTarea.ListIndex > -1 Then
            .ReportComments = "Tarea: " & CodigoCombo(cboTarea) & " : " & ClearNull(TextoCombo(cboTarea)) & "    Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy"))
        Else
            .ReportComments = "Petici�n: " & IIf(Val(ClearNull(txtAux)) > 0, ClearNull(txtAux), "S/N") & " : " & ClearNull(TextoCombo(cboPeticion)) & "    Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy"))
        End If
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fdesde": crParamDef.AddCurrentValue (IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")))
            Case "@fhasta": crParamDef.AddCurrentValue (IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")))
            Case "@tipo"
                If cboTarea.ListIndex > -1 Then
                    crParamDef.AddCurrentValue ("TAR")
                Else
                    crParamDef.AddCurrentValue ("PET")
                End If
            Case "@codigo"
                If cboTarea.ListIndex > -1 Then
                    crParamDef.AddCurrentValue (CodigoCombo(cboTarea))
                Else
                    crParamDef.AddCurrentValue ("" & CodigoCombo(Me.cboPeticion, True))
                End If
            Case "@detalle": crParamDef.AddCurrentValue (CStr(xNivel))
        End Select
    Next
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Status("Listo.")
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub OptNivel_Click(Index As Integer)
    xNivel = Index
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If cboTarea.ListIndex < 0 And cboPeticion.ListIndex < 0 Then
        MsgBox ("Falta seleccionar Petici�n o Tarea")
        CamposObligatorios = False
        Exit Function
    End If
    If cboTarea.ListIndex >= 0 And cboPeticion.ListIndex >= 0 Then
        MsgBox ("No puede asignar simult�neamente Petici�n y Tarea")
        CamposObligatorios = False
        Exit Function
    End If
    If txtDESDE = "" Then
        MsgBox ("Falta Fecha inicio informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA = "" Then
        MsgBox ("Falta Fecha fin informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox ("Fecha Hasta menor que Fecha Desde")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Sub InicializarCombos()
    Dim vExtendido() As String
    Dim sValExtendido As String
    
    flgEnCarga = True
    Call Puntero(True)
    Call Status("Inicializando...")         ' add -003- a.
    cboPeticion.Clear
    If ClearNull(glLOGIN_Grupo) <> "" Then
        If sp_GetPeticionGrupoXt(0, Null, ClearNull(glLOGIN_Grupo), "EVALUA|ESTIMA|PLANIF|PLANOK|EJECUC") Then
            Do While Not aplRST.EOF
                'cboPeticion.AddItem padRight(ClearNull(aplRST!pet_nroasignado), 9) & " : " & Trim(aplRST!Titulo) & Space(150) & " || " & Trim(aplRST!pet_nrointerno) & " |N"
                cboPeticion.AddItem padRight(ClearNull(aplRST!pet_nroasignado), 6) & " : " & Trim(aplRST!Titulo) & ESPACIOS & ESPACIOS & " || " & Trim(aplRST!pet_nrointerno) & " |N"
                aplRST.MoveNext
            Loop
        End If
    ElseIf ClearNull(glLOGIN_Sector) <> "" Then
        If sp_GetPeticionSectorXt(0, ClearNull(glLOGIN_Sector), "EVALUA|ESTIMA|PLANIF|PLANOK|EJECUC") Then
            Do While Not aplRST.EOF
                'cboPeticion.AddItem padRight(ClearNull(aplRST!pet_nroasignado), 9) & " : " & Trim(aplRST!Titulo) & Space(150) & " || " & Trim(aplRST!pet_nrointerno) & " |N"
                cboPeticion.AddItem padRight(ClearNull(aplRST!pet_nroasignado), 6) & " : " & Trim(aplRST!Titulo) & ESPACIOS & ESPACIOS & " || " & Trim(aplRST!pet_nrointerno) & " |N"
                aplRST.MoveNext
            Loop
        End If
    End If
    
    cboPeticion.ListIndex = -1
    cboTarea.Clear
    If sp_GetTarea(Null) Then
        Do While Not aplRST.EOF
            'cboTarea.AddItem padLeft(aplRST!cod_tarea, 9) & " : " & Trim(aplRST!nom_tarea) & Space(150) & " ||N"
            cboTarea.AddItem padLeft(aplRST!cod_tarea, 9) & " : " & Trim(aplRST!nom_tarea) & ESPACIOS & ESPACIOS & " ||N"
            aplRST.MoveNext
        Loop
    End If
    cboTarea.ListIndex = -1
    Call Status("Listo.")
    flgEnCarga = False
    Call Puntero(False)
End Sub

'Private Sub ViejoReporte()
'    Dim txtAux As String
'    Dim sTitulo As String
'    Dim xRet As Integer
'    Dim vRetorno() As String
'    If Not CamposObligatorios Then
'        Exit Sub
'    End If
'
'    'Err.Raise 3709
'    'Err.Raise 51
'
'    'mdiPrincipal.CrystalReport1.Connect = "DSN=" & mdiPrincipal.AdoConnection.Servidor & ";SRVR=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'    'mdiPrincipal.CrystalReport1.UserName = mdiPrincipal.AdoConnection.UsuarioAplicacion
'    'mdiPrincipal.CrystalReport1.Password = mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad
'    'xRet = mdiPrincipal.CrystalReport1.LogOnServer("PdSODBC.DLL", "CRREP", "atr", "E037799", "123456pwddif")
'    'mdiPrincipal.CrystalReport1.ReportFileName = App.Path & "\" & "hstrapeta.rpt"  ' del -002- a.
'    '{ add -002- a.
'    Dim cPathFileNameReport As String
'    cPathFileNameReport = App.Path & "\" & "hstrapeta.rpt"
'    mdiPrincipal.CrystalReport1.ReportFileName = GetShortName(cPathFileNameReport)
'    '}
'    mdiPrincipal.CrystalReport1.Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'
'    'DSN = name;UID = userID;PWD = password;DSQ = database qualifier
'
'    mdiPrincipal.CrystalReport1.SelectionFormula = ""
'    mdiPrincipal.CrystalReport1.RetrieveStoredProcParams
'
'    mdiPrincipal.CrystalReport1.StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
'    mdiPrincipal.CrystalReport1.StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
'    txtAux = CodigoCombo(Me.cboPeticion)
'    If cboTarea.ListIndex > -1 Then
'        mdiPrincipal.CrystalReport1.StoredProcParam(2) = "TAR"
'        mdiPrincipal.CrystalReport1.StoredProcParam(3) = CodigoCombo(cboTarea)
'        sTitulo = "Tarea: " & CodigoCombo(cboTarea) & " : " & ClearNull(TextoCombo(cboTarea)) & "    Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy"))
'    Else
'        mdiPrincipal.CrystalReport1.StoredProcParam(2) = "PET"
'        mdiPrincipal.CrystalReport1.StoredProcParam(3) = "" & CodigoCombo(Me.cboPeticion, True)
'        sTitulo = "Petici�n: " & IIf(Val(ClearNull(txtAux)) > 0, ClearNull(txtAux), "S/N") & " : " & ClearNull(TextoCombo(cboPeticion)) & "    Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy"))
'    End If
'    mdiPrincipal.CrystalReport1.StoredProcParam(4) = xNivel
'    ' Para DESARROLLO
'    'Debug.Print "sp_rptHsTrabPetiTare " & "'" & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, Date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")) & "', " & _
'    '                                     "'" & IIf(IsNull(txtHASTA.DateValue), Format(Date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")) & "', " & _
'    '                                     "'" & IIf(cboTarea.ListIndex > -1, "TAR", "PET") & "', " & _
'    '                                     "'" & IIf(cboTarea.ListIndex > -1, CodigoCombo(cboTarea), CodigoCombo(Me.cboPeticion, True)) & "', " & _
'    '                                     "'" & xNivel & "'"
'    'Debug.Print cboPeticion
'    mdiPrincipal.CrystalReport1.Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'    mdiPrincipal.CrystalReport1.Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'    mdiPrincipal.CrystalReport1.Formulas(2) = "@0_TITULO='" & ReplaceApostrofo(sTitulo) & "'"   ' upd -004- a.
''    mdiPrincipal.CrystalReport1.Connect = "DSN=" & mdiPrincipal.SybaseLogIn.DsnAplicacion & ";UID=" & mdiPrincipal.SybaseLogIn.UserAplicacion & ";PWD=" & mdiPrincipal.SybaseLogIn.PasswordAplicacion & mdiPrincipal.SybaseLogIn.ComponenteSI & ";SRVR=" & mdiPrincipal.SybaseLogIn.ServerSI & ";DB=" & mdiPrincipal.SybaseLogIn.BaseSI
'    mdiPrincipal.CrystalReport1.WindowLeft = 1
'    mdiPrincipal.CrystalReport1.WindowTop = 1
'    mdiPrincipal.CrystalReport1.WindowState = crptMaximized
'    mdiPrincipal.CrystalReport1.Action = 1
'    mdiPrincipal.CrystalReport1.Formulas(2) = ""
'   'Call cmdCancelar_Click
'End Sub


