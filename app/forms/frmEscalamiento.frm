VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmEscalamiento 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "MANTENIIMENTO DE ESCALAMIENTOS"
   ClientHeight    =   6195
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6195
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame pnlBotones 
      Height          =   6195
      Left            =   8220
      TabIndex        =   6
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   5610
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Sector seleccionado"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector"
         Top             =   3570
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2340
      Left            =   0
      TabIndex        =   1
      Top             =   3840
      Width           =   8205
      Begin VB.ComboBox cboAccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1740
         Width           =   4425
      End
      Begin VB.ComboBox cboAlcance 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   300
         Width           =   4425
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   660
         Width           =   4425
      End
      Begin AT_MaskText.MaskText txtPzoDesde 
         Height          =   315
         Left            =   1440
         TabIndex        =   14
         Top             =   1020
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   556
         MaxLength       =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   4
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtPzoHasta 
         Height          =   315
         Left            =   1440
         TabIndex        =   16
         Top             =   1380
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   556
         MaxLength       =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   4
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label Label2 
         Caption         =   "Plazo Hasta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   17
         Top             =   1395
         Width           =   1080
      End
      Begin VB.Label Label5 
         Caption         =   "Plazo Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   15
         Top             =   1035
         Width           =   1080
      End
      Begin VB.Label Label3 
         Caption         =   "Alcance"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   1305
      End
      Begin VB.Label Label4 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   1305
      End
      Begin VB.Label Label1 
         Caption         =   "Acci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   1800
         Width           =   1305
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   30
         Visible         =   0   'False
         Width           =   105
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3810
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   6720
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmEscalamiento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. - FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. - FJS 05.10.2007 - Se agregan dos nuevos c�digos al combobox que determina la acci�n a realizar.

Option Explicit

Const colCODALCANCE = 0
Const colCODESTADO = 1
Const colPZODESDE = 2
Const colPZOHASTA = 3
Const colCODACCION = 4
Const colFECACTIVO = 5
Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarCombos
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Me.Top = 0
    Me.Left = 0
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    Call LimpiarForm(Me)
    Call HabilitarBotones(0)
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsEscalamiento(CodigoCombo(Me.cboAlcance), CodigoCombo(Me.cboEstado), txtPzoDesde.Text, txtPzoHasta.Text, CodigoCombo(Me.cboAccion), Null) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "E"
            If sp_DelEscalamiento(CodigoCombo(Me.cboAlcance), CodigoCombo(Me.cboEstado), txtPzoDesde.Text, txtPzoHasta.Text, CodigoCombo(Me.cboAccion)) Then
                  Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.Visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            'grdDatos.SetFocus
            cmdAgregar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.Visible = True
                    cboAlcance.ListIndex = -1
                    cboEstado.ListIndex = -1
                    cboAccion.ListIndex = -1
                    fraDatos.Enabled = True
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If cboAccion.ListIndex < 0 Then
        MsgBox ("Debe seleccionar una Accion")
        CamposObligatorios = False
        Exit Function
    End If
    If cboEstado.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un Estado")
        CamposObligatorios = False
        Exit Function
    End If
    If cboAlcance.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un alcance")
        CamposObligatorios = False
        Exit Function
    End If
    If Val(txtPzoDesde.Text) = 0 Then
        MsgBox ("Debe especificar Plazo Desde")
        CamposObligatorios = False
        Exit Function
    End If
    If Val(txtPzoHasta.Text) = 0 Then
        MsgBox ("Debe especificar Plazo Hasta")
        CamposObligatorios = False
        Exit Function
    End If
    If Val(txtPzoHasta.Text) < Val(txtPzoDesde.Text) Then
        MsgBox ("Error Desde-Hasta")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Sub InicializarCombos()
    If sp_GetEstado(Null) Then
        Do While Not aplRST.EOF
            cboEstado.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboAlcance.AddItem "PET" & " : Petici�n"
    cboAlcance.AddItem "SEC" & " : Sector"
    cboAlcance.AddItem "GRU" & " : Grupo"
    
    cboAccion.AddItem "ULTMPET" & " : Ultim�tum Petici�n"
    cboAccion.AddItem "ESC1PET" & " : Escalamiento Petici�n 1ra Instancia"
    cboAccion.AddItem "ESC2PET" & " : Escalamiento Petici�n 2da Instancia"
    cboAccion.AddItem "ULTMSEC" & " : Ultim�tum Sector"
    cboAccion.AddItem "ESC1SEC" & " : Escalamiento Sector 1ra Instancia"
    cboAccion.AddItem "ESC2SEC" & " : Escalamiento Sector 2da Instancia"
    cboAccion.AddItem "ULTMGRU" & " : Ultim�tum Grupo"
    cboAccion.AddItem "ESC1GRU" & " : Escalamiento Grupo 1ra Instancia"
    '{ add -003- a.
    cboAccion.AddItem "REVIGRU" & " : A revisar Grupo"
    cboAccion.AddItem "REVISEC" & " : A revisar Sector"
    cboAccion.AddItem "CANCGRU" & " : Se cancela el grupo"
    cboAccion.AddItem "CANCSEC" & " : Se cancela el sector"
    '}
End Sub

Sub CargarGrid()
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colCODALCANCE) = "ALCANCE"
        .TextMatrix(0, colCODESTADO) = "ESTADO"
        .TextMatrix(0, colPZODESDE) = "PZO.DESDE"
        .TextMatrix(0, colPZOHASTA) = "PZO.HASTA"
        .TextMatrix(0, colCODACCION) = "ACCI�N"
        .TextMatrix(0, colFECACTIVO) = "F.ACTIVO"
        .ColWidth(colCODALCANCE) = 1000
        .ColWidth(colCODESTADO) = 3000
        .ColWidth(colPZODESDE) = 1000
        .ColWidth(colPZOHASTA) = 1000
        .ColWidth(colCODACCION) = 1000
        .ColWidth(colFECACTIVO) = 2000
    End With
    ' Devuelvo todos las definiciones de escalamientos disponibles
    If Not sp_GetEscalamiento(Null, Null) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODALCANCE) = ClearNull(aplRST.Fields.Item("evt_alcance"))
            .TextMatrix(.Rows - 1, colCODESTADO) = ClearNull(aplRST.Fields.Item("cod_estado"))
            .TextMatrix(.Rows - 1, colPZODESDE) = ClearNull(aplRST.Fields.Item("pzo_desde"))
            .TextMatrix(.Rows - 1, colPZOHASTA) = ClearNull(aplRST.Fields.Item("pzo_hasta"))
            .TextMatrix(.Rows - 1, colCODACCION) = ClearNull(aplRST.Fields.Item("cod_accion"))
            .TextMatrix(.Rows - 1, colFECACTIVO) = IIf(Not IsNull(aplRST!fec_activo), Format(aplRST!fec_activo, "dd/mm/yyyy"), "")
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    '{ add -002- a.
    Dim i As Long
    
    With grdDatos
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .FocusRect = flexFocusNone
    End With
    '}
    Call MostrarSeleccion
End Sub

Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 0) <> "" Then
                cboAlcance.ListIndex = PosicionCombo(Me.cboAlcance, .TextMatrix(.RowSel, colCODALCANCE))
                cboEstado.ListIndex = PosicionCombo(Me.cboEstado, .TextMatrix(.RowSel, colCODESTADO))
                txtPzoDesde = ClearNull(.TextMatrix(.RowSel, colPZODESDE))
                txtPzoHasta = ClearNull(.TextMatrix(.RowSel, colPZOHASTA))
                cboAccion.ListIndex = PosicionCombo(Me.cboAccion, .TextMatrix(.RowSel, colCODACCION))
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
'    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}
