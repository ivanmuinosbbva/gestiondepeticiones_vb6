VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmSelPetGrp 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccionar petici�n"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8910
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   8910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   1485
      Left            =   60
      TabIndex        =   3
      Top             =   0
      Width           =   8805
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         TabIndex        =   6
         Top             =   990
         Width           =   1065
      End
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "Filtrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         TabIndex        =   5
         Top             =   300
         Width           =   1065
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3150
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   270
         Width           =   5415
      End
      Begin AT_MaskText.MaskText txtNroBuscar 
         Height          =   315
         Left            =   3150
         TabIndex        =   7
         Top             =   960
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   3150
         TabIndex        =   8
         Top             =   630
         Width           =   5385
         _ExtentX        =   9499
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2490
         TabIndex        =   13
         Top             =   345
         Width           =   555
      End
      Begin VB.Label lblAgrup 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "T�tulo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2595
         TabIndex        =   12
         Top             =   690
         Width           =   450
      End
      Begin VB.Label Label3 
         Caption         =   "Buscar por N�mero"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   960
         Width           =   900
      End
      Begin VB.Label Label2 
         Caption         =   "Filtrar por Estado/T�tulo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   480
         Left            =   120
         TabIndex        =   10
         Top             =   270
         Width           =   1050
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Nro.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2670
         TabIndex        =   9
         Top             =   1020
         Width           =   375
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4965
      Left            =   7830
      TabIndex        =   0
      Top             =   1440
      Width           =   1035
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   4095
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   1
         Top             =   4485
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4935
      Left            =   60
      TabIndex        =   14
      Top             =   1500
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   8705
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelPetGrp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim flgNumero As Boolean
Dim flgTitulo As Boolean
Dim auxEstado As String
Dim l_pet_nrointerno As Long

Const colNroAsignado = 0
Const colTitulo = 1
Const colESTADO = 2
Const colSituacion = 3
Const colPrioridad = 4
Const colEsfuerzo = 5
Const colFinicio = 6
Const colFtermin = 7
Const colNroInterno = 8
Const colEstCod = 9
Const colSitCod = 10
Const colCodigoRecurso = 11
Const colNota = 12
Const colDerivo = 13

Private Sub Form_Load()
    flgNumero = False
    flgTitulo = False
    glAuxRetorno = ""

    cboEstado.AddItem "Peticiones en estado Terminal" & Space(90) & "||" & "RECHAZ|RECHTE|CANCEL|TERMIN|"
    cboEstado.AddItem "Peticiones en estado Activo" & Space(90) & "||" & "CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|SUSPEN|REVISA|", 0
    cboEstado.ListIndex = 0
    
    'If glflgProyecto = True Then
    '    txtNroBuscar.Enabled = False
    '    cmdBuscar.Enabled = False
    'End If
    
    If sp_GetGrupo(glLOGIN_Grupo, glLOGIN_Sector) Then
        Me.Caption = "Seleccionar petici�n de " & ClearNull(aplRST.Fields!nom_grupo)
    End If

    Call Status("Listo.")
   
   Call CargarGrid
   modUser32.IniciarScroll grdDatos
End Sub

Private Sub cmdBuscar_Click()
    If Val(txtNroBuscar) > 0 Then
        flgNumero = True
        flgTitulo = False
        txtTitulo = ""
        CargarGrid
    End If
End Sub

Private Sub cmdFiltro_Click()
    flgNumero = False
    flgTitulo = True
    txtNroBuscar = ""
    CargarGrid
End Sub

Private Sub cmdOK_Click()
    glAuxRetorno = ""
    Unload Me
End Sub

Sub CargarGrid()
    cmdAceptar.Enabled = False
    Call Puntero(True)
    Call Status("Cargando peticiones...")
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colNroAsignado) = "Petici�n"
        .TextMatrix(0, colTitulo) = "T�tulo"
        .TextMatrix(0, colESTADO) = "Estado"
        .TextMatrix(0, colSituacion) = "Situaci�n"
        .TextMatrix(0, colPrioridad) = "Pri."
        .TextMatrix(0, colEsfuerzo) = "Horas"
        .TextMatrix(0, colFinicio) = "F.Ini.Planif"
        .TextMatrix(0, colFtermin) = "F.Fin Planif"
        .TextMatrix(0, colNroInterno) = "NroInt."
        .ColWidth(colNroAsignado) = 700
        .ColWidth(colTitulo) = 4000
        .ColWidth(colESTADO) = 1800
        .ColWidth(colSituacion) = 1000
        .ColWidth(colPrioridad) = 600
        .ColWidth(colEsfuerzo) = 600
        .ColWidth(colFinicio) = 1100
        .ColWidth(colFtermin) = 1100
        .ColWidth(colNroInterno) = 800
        .ColWidth(colEstCod) = 0
        .ColWidth(colSitCod) = 0
    End With
    If flgNumero = True Then
        If Not sp_GetUnaPeticionAsig(txtNroBuscar) Then
            l_pet_nrointerno = ClearNull(aplRST.Fields!pet_nrointerno)
            If Not sp_GetUnaPetTituloGrp(l_pet_nrointerno, txtTitulo, DatosCombo(cboEstado), glLOGIN_Sector, glLOGIN_Grupo) Then
               aplRST.Close
               GoTo finx
            End If
            aplRST.Close
            GoTo finx
        End If
    ElseIf flgTitulo = True Then
        If Not sp_GetUnaPetTituloGrp(Null, txtTitulo, DatosCombo(cboEstado), glLOGIN_Sector, glLOGIN_Grupo) Then
           aplRST.Close
           GoTo finx
        End If
    Else
        If Not sp_GetUnaPetTituloGrp(Null, txtTitulo, DatosCombo(cboEstado), glLOGIN_Sector, glLOGIN_Grupo) Then
           aplRST.Close
           GoTo finx
        End If
    End If
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
            .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colEstCod) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colSitCod) = ClearNull(aplRST!cod_situacion)
            .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close

    Dim i As Long
    
    With grdDatos
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        For i = 0 To .cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .FocusRect = flexFocusNone
    End With
    With grdDatos
        .Col = colNroAsignado
        .Sort = flexSortStringNoCaseDescending
    End With
finx:
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
                .Col = .MouseCol
                .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                'glAuxRetorno = padRight(ClearNull(.TextMatrix(.RowSel, colNroAsignado)), 6) & " : " & .TextMatrix(.RowSel, colTitulo) & Space(40) & "||" & .TextMatrix(.RowSel, colNroInterno) & "|S"  ' del -004- a.
                glAuxRetorno = padRight(ClearNull(.TextMatrix(.RowSel, colNroAsignado)), 9) & " : " & .TextMatrix(.RowSel, colTitulo) & Space(150) & " || " & .TextMatrix(.RowSel, colNroInterno) & " |S"  ' add -004- a.
                cmdAceptar.Enabled = True
            End If
        End If
        .Refresh
    End With
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub txtNroBuscar_KeyUp(KeyCode As Integer, Shift As Integer)
    cmdAceptar.Enabled = False
End Sub

Private Sub txtTitulo_KeyUp(KeyCode As Integer, Shift As Integer)
    cmdAceptar.Enabled = False
End Sub

Private Sub cmdAceptar_Click()
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub

