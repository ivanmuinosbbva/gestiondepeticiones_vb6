VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDrivers 
   Caption         =   "Administrador de drivers"
   ClientHeight    =   6945
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9465
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDrivers.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6945
   ScaleWidth      =   9465
   Begin VB.Frame fraDatos 
      Height          =   3435
      Left            =   60
      TabIndex        =   6
      Top             =   3480
      Width           =   8175
      Begin VB.ComboBox cboIndicador 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   2460
         Width           =   2655
      End
      Begin VB.ComboBox cboResponsable 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   2100
         Width           =   2655
      End
      Begin VB.TextBox txtValor 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         TabIndex        =   17
         Top             =   1740
         Width           =   735
      End
      Begin VB.TextBox txtAyuda 
         Height          =   675
         Left            =   1260
         MaxLength       =   255
         MultiLine       =   -1  'True
         TabIndex        =   14
         Top             =   1020
         Width           =   5535
      End
      Begin VB.TextBox txtNombre 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         MaxLength       =   60
         TabIndex        =   9
         Top             =   660
         Width           =   5535
      End
      Begin VB.ComboBox cboHabilitado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   2820
         Width           =   1215
      End
      Begin VB.TextBox txtCodigo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         TabIndex        =   7
         Top             =   300
         Width           =   1095
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Indicador"
         Height          =   195
         Index           =   6
         Left            =   240
         TabIndex        =   21
         Top             =   2535
         Width           =   675
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Responsable"
         Height          =   195
         Index           =   5
         Left            =   240
         TabIndex        =   18
         Top             =   2175
         Width           =   915
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Valor"
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   16
         Top             =   1800
         Width           =   360
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Ayuda"
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   15
         Top             =   1080
         Width           =   465
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   495
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   11
         Top             =   720
         Width           =   555
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Habilitado"
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   10
         Top             =   2895
         Width           =   705
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   6915
      Left            =   8280
      TabIndex        =   0
      Top             =   0
      Width           =   1155
      Begin VB.CommandButton cmdValores 
         Caption         =   "Opciones"
         Height          =   435
         Left            =   120
         TabIndex        =   22
         Top             =   180
         Width           =   945
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   435
         Left            =   120
         TabIndex        =   5
         Top             =   4680
         Width           =   945
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   435
         Left            =   120
         TabIndex        =   4
         Top             =   5100
         Width           =   945
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   435
         Left            =   120
         TabIndex        =   3
         Top             =   5520
         Width           =   945
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   435
         Left            =   120
         TabIndex        =   2
         Top             =   5940
         Width           =   945
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   435
         Left            =   120
         TabIndex        =   1
         Top             =   6360
         Width           =   945
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3420
      Left            =   60
      TabIndex        =   13
      Top             =   60
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   6033
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDrivers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const col_Codigo = 0
Private Const COL_NOMBRE = 1
Private Const COL_VALOR = 2
Private Const COL_RESPONSABLE = 3
Private Const COL_HABILITADO = 4
Private Const COL_INDICADOR = 5
Private Const COL_INDICADORNOMBRE = 6
Private Const COL_AYUDA = 7
Private Const COL_TOTALCOLS = 8
Private Const SCREEN_MAXHEIGHT = 7455
Private Const SCREEN_MAXWIDTH = 9585

Public lDriverSeleccionado As Long
Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarControles
    Call IniciarScroll(grdDatos)
    Call InicializarPantalla
End Sub

Private Sub InicializarControles()
    ' Inicializa el tama�o del form
    Me.Top = 0
    Me.Left = 0
    Me.Height = SCREEN_MAXHEIGHT
    Me.Width = SCREEN_MAXWIDTH
    
    With cboResponsable
        .Clear
        .AddItem "Usuario" & ESPACIOS & "||SOLI"
        .AddItem "RGYP" & ESPACIOS & "||BPE"
    End With
    
    With cboIndicador
        .Clear
        If sp_GetIndicador(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!indicadorId) & ": " & ClearNull(aplRST.Fields!indicadorNom)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    
    With cboHabilitado
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
    End With
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    'Show
    'Call LimpiarForm(Me)
    Call CargarGrilla
    Call HabilitarBotones(0)
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        Me.Height = SCREEN_MAXHEIGHT
        Me.Width = SCREEN_MAXWIDTH
    End If
End Sub

Private Sub Form_DblClick()
    Me.Top = 0
    Me.Left = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub CargarGrilla()
    With grdDatos
        .Clear
        .Rows = 1
        .cols = COL_TOTALCOLS
        .RowHeightMin = 280
        .TextMatrix(0, col_Codigo) = "C�digo": .ColWidth(col_Codigo) = 1000: .ColAlignment(col_Codigo) = flexAlignLeftCenter
        .TextMatrix(0, COL_NOMBRE) = "Nombre": .ColWidth(COL_NOMBRE) = 4000: .ColAlignment(COL_NOMBRE) = flexAlignLeftCenter
        .TextMatrix(0, COL_VALOR) = "Valor": .ColWidth(COL_VALOR) = 1000: .ColAlignment(COL_VALOR) = flexAlignCenterCenter
        .TextMatrix(0, COL_RESPONSABLE) = "Responsable": .ColWidth(COL_RESPONSABLE) = 1000: .ColAlignment(COL_RESPONSABLE) = flexAlignLeftCenter
        .TextMatrix(0, COL_HABILITADO) = "Habilitado": .ColWidth(COL_HABILITADO) = 1000: .ColAlignment(COL_HABILITADO) = flexAlignLeftCenter
        .TextMatrix(0, COL_INDICADOR) = "": .ColWidth(COL_INDICADOR) = 0: .ColAlignment(COL_INDICADOR) = flexAlignLeftCenter
        .TextMatrix(0, COL_INDICADORNOMBRE) = "Indicador": .ColWidth(COL_INDICADORNOMBRE) = 2000: .ColAlignment(COL_INDICADORNOMBRE) = flexAlignLeftCenter
        .TextMatrix(0, COL_AYUDA) = "Ayuda": .ColWidth(COL_AYUDA) = 0: .ColAlignment(COL_AYUDA) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
        If sp_GetDrivers(Null, "S", Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, col_Codigo) = ClearNull(aplRST.Fields!driverId)
                .TextMatrix(.Rows - 1, COL_NOMBRE) = ClearNull(aplRST.Fields!driverNom)
                .TextMatrix(.Rows - 1, COL_VALOR) = Format(ClearNull(aplRST.Fields!driverValor), "###,###,##0.00")
                .TextMatrix(.Rows - 1, COL_RESPONSABLE) = ClearNull(aplRST.Fields!driverResp)
                .TextMatrix(.Rows - 1, COL_HABILITADO) = ClearNull(aplRST.Fields!driverHab)
                .TextMatrix(.Rows - 1, COL_INDICADOR) = ClearNull(aplRST.Fields!indicadorId)
                .TextMatrix(.Rows - 1, COL_INDICADORNOMBRE) = ClearNull(aplRST.Fields!indicadorNombre)
                .TextMatrix(.Rows - 1, COL_AYUDA) = ClearNull(aplRST.Fields!driverAyuda)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .RowSel > 0 Then
            lDriverSeleccionado = ClearNull(.TextMatrix(.RowSel, col_Codigo))
            If .TextMatrix(.RowSel, col_Codigo) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, col_Codigo))
                txtNombre = ClearNull(.TextMatrix(.RowSel, COL_NOMBRE))
                txtAyuda = ClearNull(.TextMatrix(.RowSel, COL_AYUDA))
                txtValor = ClearNull(.TextMatrix(.RowSel, COL_VALOR))
                Call SetCombo(cboResponsable, ClearNull(.TextMatrix(.RowSel, COL_RESPONSABLE)), True)
                Call SetCombo(cboIndicador, ClearNull(.TextMatrix(.RowSel, COL_INDICADOR)))
                Call SetCombo(cboHabilitado, ClearNull(.TextMatrix(.RowSel, COL_HABILITADO)), True)
            End If
        End If
    End With
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True

    If ClearNull(txtCodigo.text) = "" Then
        MsgBox "Debe completar el c�digo.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.text, ":") > 0 Then
        MsgBox "El c�digo no pude contener el caracter ':'.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If ClearNull(txtNombre.text) = "" Then
        MsgBox "Debe completar el nombre.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtNombre.text, ":") > 0 Then
        MsgBox "El nombre no pude contener el caracter ':'.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If cboHabilitado.ListIndex < 0 Then
        MsgBox "Debe seleccionar si est� habilitado o no.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            'lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    fraDatos.Caption = " Agregar "
                    txtCodigo = "": txtNombre = ""
                    cboHabilitado.ListIndex = 0
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    fraDatos.Caption = " Modificar "
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtNombre.SetFocus
                Case "E"
                    fraDatos.Caption = " Eliminar "
                    fraDatos.Enabled = False
            End Select
    End Select
End Sub

Private Sub cmdConfirmar_Click()
    Call GuardarDatos
End Sub

Private Sub GuardarDatos()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                Call sp_InsertIndicador(txtCodigo, txtNombre, CodigoCombo(cboHabilitado, True))
                Call HabilitarBotones(0)
            End If
        Case "M"
            If CamposObligatorios Then
                Call sp_UpdateIndicador(txtCodigo, txtNombre, CodigoCombo(cboHabilitado, True))
                With grdDatos
                    If .RowSel > 0 Then
                       .TextMatrix(.RowSel, col_Codigo) = ClearNull(txtCodigo)
                       .TextMatrix(.RowSel, COL_NOMBRE) = ClearNull(txtNombre)
                       .TextMatrix(.RowSel, COL_HABILITADO) = CodigoCombo(cboHabilitado, True)
                    End If
                End With
                Call HabilitarBotones(0, False)
            End If
        Case "E"
            If sp_DeleteIndicador(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdValores_Click()
    With grdDatos
        If .RowSel > 0 Then
            frmDriversValores.Show 1
        End If
    End With
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub txtValor_Validate(Cancel As Boolean)
    If Len(txtValor) > 0 Then
        Call ValidarNumero(txtValor)
    End If
End Sub

Private Function ValidarNumero(ByRef oControl As TextBox) As Boolean
    ValidarNumero = False
    If Len(oControl) > 0 Then
        If IsNumeric(oControl) Then
            If CDbl(oControl) > 0 Then
                oControl = Replace(oControl, ".", ",", 1, , vbTextCompare)
                oControl = Format(oControl, "########0.00")
                ValidarNumero = True
            End If
        End If
    End If
End Function

