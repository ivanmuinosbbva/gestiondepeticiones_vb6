VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesShellView01 
   Caption         =   "Consulta General de Peticiones"
   ClientHeight    =   7965
   ClientLeft      =   1755
   ClientTop       =   1545
   ClientWidth     =   14130
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesShellView01.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   12930
   ScaleWidth      =   23760
   Begin VB.Frame frmGral 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7900
      Left            =   60
      TabIndex        =   0
      Top             =   30
      Width           =   12765
      Begin VB.ComboBox cboCategoria 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   11115
         Style           =   2  'Dropdown List
         TabIndex        =   114
         Top             =   1920
         Width           =   1500
      End
      Begin VB.ComboBox cboBPE 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   108
         ToolTipText     =   "Referente de Reingenier�a y Gesti�n de la Demanda y Procesos"
         Top             =   4560
         Width           =   5355
      End
      Begin VB.ComboBox cboRO 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5880
         Style           =   2  'Dropdown List
         TabIndex        =   105
         Top             =   1560
         Width           =   3045
      End
      Begin VB.ComboBox cboEmp 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   11115
         Style           =   2  'Dropdown List
         TabIndex        =   103
         Top             =   840
         Width           =   1500
      End
      Begin VB.CommandButton cmdPrjFilter 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8220
         Picture         =   "frmPeticionesShellView01.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto IDM"
         Top             =   1920
         Width           =   345
      End
      Begin VB.CommandButton cmdAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11880
         Picture         =   "frmPeticionesShellView01.frx":0B14
         Style           =   1  'Graphical
         TabIndex        =   65
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar agrupamiento para filtrar las peticiones"
         Top             =   510
         Width           =   345
      End
      Begin VB.ComboBox cboEstadoPet 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   93
         Top             =   2280
         Width           =   6420
      End
      Begin VB.ComboBox cboImportancia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   88
         Top             =   1560
         Width           =   2775
      End
      Begin VB.Frame fraApertura 
         Caption         =   " Apertura "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1215
         Left            =   10620
         TabIndex        =   84
         Top             =   6420
         Width           =   1935
         Begin VB.OptionButton optApertura 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   87
            Top             =   840
            Width           =   1215
         End
         Begin VB.OptionButton optApertura 
            Caption         =   "Sector"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   86
            Top             =   600
            Width           =   1215
         End
         Begin VB.OptionButton optApertura 
            Caption         =   "Sin apertura"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   85
            Top             =   360
            Width           =   1215
         End
      End
      Begin VB.ComboBox cmbRegulatorio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesShellView01.frx":109E
         Left            =   11115
         List            =   "frmPeticionesShellView01.frx":10AE
         Style           =   2  'Dropdown List
         TabIndex        =   83
         Top             =   1560
         Width           =   1500
      End
      Begin VB.ComboBox cboPeticionImpacto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesShellView01.frx":10D2
         Left            =   11115
         List            =   "frmPeticionesShellView01.frx":10E2
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1200
         Width           =   1500
      End
      Begin VB.ComboBox cboPeticionClase 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5880
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1200
         Width           =   3045
      End
      Begin VB.CommandButton cmdClrAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12240
         Picture         =   "frmPeticionesShellView01.frx":1116
         Style           =   1  'Graphical
         TabIndex        =   66
         TabStop         =   0   'False
         ToolTipText     =   "Limpia agrupamiento seleccionado"
         Top             =   510
         Width           =   345
      End
      Begin VB.Frame frmEjec 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   290
         Left            =   1440
         TabIndex        =   64
         Top             =   5520
         Width           =   4815
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Grupo"
            Height          =   195
            Index           =   4
            Left            =   3840
            TabIndex        =   22
            Top             =   30
            Width           =   885
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Sector"
            Height          =   195
            Index           =   3
            Left            =   3000
            TabIndex        =   21
            Top             =   30
            Width           =   885
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Gerencia"
            Height          =   195
            Index           =   2
            Left            =   1920
            TabIndex        =   20
            Top             =   30
            Width           =   975
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Direcci�n"
            Height          =   195
            Index           =   1
            Left            =   840
            TabIndex        =   19
            Top             =   30
            Width           =   1125
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "BBVA"
            Height          =   195
            Index           =   0
            Left            =   0
            TabIndex        =   18
            Top             =   30
            Width           =   765
         End
      End
      Begin VB.ComboBox cboEstadoSec 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   6180
         Width           =   6975
      End
      Begin VB.ComboBox cboSituacionSec 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   6540
         Width           =   6975
      End
      Begin VB.ComboBox cboSituacionPet 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   2640
         Width           =   6420
      End
      Begin VB.ComboBox cboTipopet 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   5
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1200
         Width           =   2775
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   1440
         TabIndex        =   4
         Top             =   855
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   556
         MaxLength       =   120
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   120
      End
      Begin AT_MaskText.MaskText txtNroDesde 
         Height          =   315
         Left            =   1440
         TabIndex        =   2
         Top             =   510
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtNroHasta 
         Height          =   315
         Left            =   3240
         TabIndex        =   3
         Top             =   510
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtDESDEiniplan 
         Height          =   315
         Left            =   1440
         TabIndex        =   26
         Top             =   6900
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEfinplan 
         Height          =   315
         Left            =   1440
         TabIndex        =   30
         Top             =   7260
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAiniplan 
         Height          =   315
         Left            =   3120
         TabIndex        =   27
         Top             =   6900
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAfinplan 
         Height          =   315
         Left            =   3120
         TabIndex        =   31
         Top             =   7260
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEestado 
         Height          =   315
         Left            =   9015
         TabIndex        =   8
         Top             =   2280
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAestado 
         Height          =   315
         Left            =   11115
         TabIndex        =   9
         Top             =   2280
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEinireal 
         Height          =   315
         Left            =   5640
         TabIndex        =   28
         Top             =   6900
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAinireal 
         Height          =   315
         Left            =   7320
         TabIndex        =   29
         Top             =   6900
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEfinreal 
         Height          =   315
         Left            =   5640
         TabIndex        =   32
         Top             =   7260
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAfinreal 
         Height          =   315
         Left            =   7320
         TabIndex        =   33
         Top             =   7260
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAgrup 
         Height          =   315
         Left            =   7860
         TabIndex        =   78
         Top             =   510
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.Frame frmSolic 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   215
         Left            =   1440
         TabIndex        =   63
         Top             =   3555
         Width           =   4590
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Sector"
            Height          =   195
            Index           =   3
            Left            =   3000
            TabIndex        =   15
            Top             =   0
            Width           =   885
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Gerencia"
            Height          =   195
            Index           =   2
            Left            =   1920
            TabIndex        =   14
            Top             =   0
            Width           =   1000
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Direcci�n"
            Height          =   195
            Index           =   1
            Left            =   840
            TabIndex        =   13
            Top             =   0
            Width           =   1035
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "BBVA"
            Height          =   195
            Index           =   0
            Left            =   0
            TabIndex        =   12
            Top             =   0
            Width           =   765
         End
      End
      Begin AT_MaskText.MaskText txtDESDEalta 
         Height          =   315
         Left            =   8760
         TabIndex        =   95
         Top             =   4200
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAalta 
         Height          =   315
         Left            =   11055
         TabIndex        =   96
         Top             =   4200
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.CommandButton cmdEraseProyectos 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8580
         Picture         =   "frmPeticionesShellView01.frx":16A0
         Style           =   1  'Graphical
         TabIndex        =   99
         TabStop         =   0   'False
         ToolTipText     =   "Quitar el filtro actual de proyecto IDM"
         Top             =   1920
         Width           =   345
      End
      Begin VB.ComboBox cboBpar 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   17
         ToolTipText     =   "Referente de Sistema"
         Top             =   4200
         Width           =   5355
      End
      Begin AT_MaskText.MaskText txtBPEDesde 
         Height          =   315
         Left            =   8760
         TabIndex        =   109
         Top             =   4560
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtBPEHasta 
         Height          =   315
         Left            =   11055
         TabIndex        =   110
         Top             =   4560
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjNom 
         Height          =   315
         Left            =   1440
         TabIndex        =   91
         Top             =   1920
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   556
         MaxLength       =   50
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.ComboBox cboBBVAPet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   45
         Top             =   3840
         Width           =   11115
      End
      Begin VB.ComboBox cboGrupoSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   5820
         Width           =   11115
      End
      Begin VB.ComboBox cboDireccionSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   60
         Top             =   5820
         Width           =   11115
      End
      Begin VB.ComboBox cboGerenciaSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   59
         Top             =   5820
         Width           =   11115
      End
      Begin VB.ComboBox cboSectorSec 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   58
         Top             =   5820
         Width           =   11115
      End
      Begin VB.ComboBox cboBBVASec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   57
         Top             =   5820
         Width           =   11115
      End
      Begin VB.ComboBox cboDireccionPet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   3840
         Width           =   11115
      End
      Begin VB.ComboBox cboGerenciaPet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   47
         Top             =   3840
         Width           =   11115
      End
      Begin VB.ComboBox cboSectorPet 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   46
         Top             =   3840
         Width           =   11115
      End
      Begin AT_MaskText.MaskText txtDesdeAltaPeticion 
         Height          =   315
         Left            =   9015
         TabIndex        =   116
         ToolTipText     =   "Fecha desde de alta de la petici�n"
         Top             =   2640
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHastaAltaPeticion 
         Height          =   315
         Left            =   11115
         TabIndex        =   117
         ToolTipText     =   "Fecha hasta para el alta de petici�n"
         Top             =   2640
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label32 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   10605
         TabIndex        =   118
         Top             =   2700
         Width           =   420
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "Alta Desde"
         Height          =   195
         Left            =   8160
         TabIndex        =   115
         Top             =   2700
         Width           =   780
      End
      Begin VB.Image imgAlertaFiltroEstados 
         Height          =   240
         Left            =   1140
         Picture         =   "frmPeticionesShellView01.frx":1C2A
         ToolTipText     =   "Se recomienda filtrar por alguna de las opciones disponibles para evitar demoras en la consulta"
         Top             =   2325
         Width           =   240
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Categor�a"
         Height          =   195
         Left            =   10200
         TabIndex        =   113
         Top             =   1980
         Width           =   705
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Index           =   1
         Left            =   8160
         TabIndex        =   112
         Top             =   4620
         Width           =   450
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Index           =   1
         Left            =   10500
         TabIndex        =   111
         Top             =   4620
         Width           =   420
      End
      Begin VB.Label Label41 
         AutoSize        =   -1  'True
         Caption         =   "Ref. de RGP"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   107
         Top             =   4620
         Width           =   885
      End
      Begin VB.Label Label42 
         AutoSize        =   -1  'True
         Caption         =   "Riesgo operacional"
         Height          =   195
         Left            =   4440
         TabIndex        =   104
         Top             =   1635
         Width           =   1350
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "Empresa"
         Height          =   195
         Left            =   10410
         TabIndex        =   102
         Top             =   915
         Width           =   615
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Index           =   0
         Left            =   10500
         TabIndex        =   98
         Top             =   4260
         Width           =   420
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Index           =   0
         Left            =   8160
         TabIndex        =   97
         Top             =   4260
         Width           =   450
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Estado Pet."
         Height          =   195
         Left            =   120
         TabIndex        =   94
         Top             =   2348
         Width           =   840
      End
      Begin VB.Label Label47 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3840
         TabIndex        =   92
         Top             =   1980
         Width           =   435
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Proyecto IDM"
         Height          =   195
         Left            =   120
         TabIndex        =   90
         Top             =   1980
         Width           =   975
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad"
         Height          =   195
         Left            =   120
         TabIndex        =   89
         Top             =   1635
         Width           =   675
      End
      Begin VB.Label Label28 
         AutoSize        =   -1  'True
         Caption         =   "Regulatorio"
         Height          =   195
         Left            =   10200
         TabIndex        =   82
         Top             =   1635
         Width           =   825
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "N�mero desde"
         Height          =   195
         Left            =   120
         TabIndex        =   81
         Top             =   570
         Width           =   1035
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   2700
         TabIndex        =   80
         Top             =   570
         Width           =   420
      End
      Begin VB.Label lblAgrup 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Agrupamientos "
         Height          =   195
         Left            =   6720
         TabIndex        =   79
         Top             =   570
         Width           =   1125
      End
      Begin VB.Label lblPetImpTech 
         AutoSize        =   -1  'True
         Caption         =   "Impacto tecnol�gico"
         Height          =   195
         Left            =   9585
         TabIndex        =   77
         Top             =   1275
         Width           =   1440
      End
      Begin VB.Label lblPetClass 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         Height          =   195
         Left            =   5400
         TabIndex        =   76
         Top             =   1275
         Width           =   390
      End
      Begin VB.Label Label41 
         AutoSize        =   -1  'True
         Caption         =   "Ref. de Sistema"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   74
         Top             =   4275
         Width           =   1140
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin.Real Desde"
         Height          =   195
         Left            =   4320
         TabIndex        =   72
         Top             =   7290
         Width           =   1230
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini.Real Desde"
         Height          =   195
         Left            =   4320
         TabIndex        =   71
         Top             =   6945
         Width           =   1200
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   6840
         TabIndex        =   70
         Top             =   7290
         Width           =   420
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   6840
         TabIndex        =   69
         Top             =   6945
         Width           =   420
      End
      Begin VB.Line Line3 
         BorderWidth     =   2
         X1              =   2040
         X2              =   12540
         Y1              =   5310
         Y2              =   5310
      End
      Begin VB.Line Line2 
         BorderWidth     =   2
         X1              =   2100
         X2              =   12600
         Y1              =   3390
         Y2              =   3390
      End
      Begin VB.Line Line1 
         BorderWidth     =   2
         X1              =   1500
         X2              =   12600
         Y1              =   360
         Y2              =   360
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "Estado Desde"
         Height          =   195
         Left            =   7950
         TabIndex        =   62
         Top             =   2340
         Width           =   990
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   10605
         TabIndex        =   61
         Top             =   2340
         Width           =   420
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini.Plan.Desde"
         Height          =   195
         Left            =   120
         TabIndex        =   56
         Top             =   6945
         Width           =   1200
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin Plan.Desde"
         Height          =   195
         Left            =   120
         TabIndex        =   55
         Top             =   7290
         Width           =   1215
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   2640
         TabIndex        =   54
         Top             =   6945
         Width           =   420
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   2640
         TabIndex        =   53
         Top             =   7290
         Width           =   420
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         Height          =   195
         Left            =   120
         TabIndex        =   52
         Top             =   5880
         Width           =   345
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Estado Sec/Gru"
         Height          =   195
         Left            =   120
         TabIndex        =   51
         Top             =   6240
         Width           =   1110
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Situac. Sec/Gru"
         Height          =   195
         Left            =   120
         TabIndex        =   50
         Top             =   6600
         Width           =   1110
      End
      Begin VB.Label Lbl7 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         Height          =   195
         Left            =   120
         TabIndex        =   49
         Top             =   5535
         Width           =   345
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Filtro �rea ejecutora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   48
         Top             =   5190
         Width           =   1740
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         Height          =   195
         Left            =   120
         TabIndex        =   44
         Top             =   3915
         Width           =   345
      End
      Begin VB.Label LblDatosRec 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         Height          =   195
         Left            =   120
         TabIndex        =   43
         Top             =   3555
         Width           =   345
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Filtro �rea solicitante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   42
         Top             =   3240
         Width           =   1800
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n Pet."
         Height          =   195
         Left            =   120
         TabIndex        =   41
         Top             =   2708
         Width           =   990
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         Height          =   195
         Left            =   120
         TabIndex        =   40
         Top             =   1275
         Width           =   300
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo (o parte)"
         Height          =   195
         Left            =   120
         TabIndex        =   39
         Top             =   915
         Width           =   1080
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Filtro petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   38
         Top             =   210
         Width           =   1155
      End
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7900
      Left            =   12840
      TabIndex        =   35
      Top             =   30
      Width           =   1275
      Begin VB.CommandButton Command1 
         Height          =   495
         Left            =   60
         Picture         =   "frmPeticionesShellView01.frx":21B4
         Style           =   1  'Graphical
         TabIndex        =   106
         Top             =   6780
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdChgBP 
         Caption         =   "Reasig. Referentes"
         Height          =   500
         Left            =   60
         TabIndex        =   75
         TabStop         =   0   'False
         ToolTipText     =   "Reasigna el Ref. de Sistemas o RGP a las peticiones seleccionadas"
         Top             =   2910
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdPrintPet 
         Caption         =   "Imprimir formulario"
         Height          =   500
         Left            =   60
         TabIndex        =   73
         TabStop         =   0   'False
         ToolTipText     =   "Imporime un formulario por cada Peticion seleccionada."
         Top             =   3930
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar"
         Height          =   615
         Left            =   60
         Picture         =   "frmPeticionesShellView01.frx":273E
         Style           =   1  'Graphical
         TabIndex        =   68
         TabStop         =   0   'False
         ToolTipText     =   "Genera un archivo Excel con la salida de la consulta"
         Top             =   180
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgrupar 
         Caption         =   "Agrupar"
         Height          =   500
         Left            =   60
         TabIndex        =   67
         TabStop         =   0   'False
         ToolTipText     =   "Adjunta las peticiones seleccionadas en un agrupamiento"
         Top             =   3420
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   60
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   7320
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Consultar"
         Height          =   500
         Left            =   60
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Realizar la consulta"
         Top             =   5040
         Width           =   1170
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "Acceder"
         Default         =   -1  'True
         Height          =   500
         Left            =   60
         TabIndex        =   34
         TabStop         =   0   'False
         ToolTipText     =   "Ver la Petici�n"
         Top             =   5040
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   7065
      Left            =   60
      TabIndex        =   1
      Top             =   840
      Width           =   10395
      _ExtentX        =   18336
      _ExtentY        =   12462
      _Version        =   393216
      Cols            =   35
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      HighLight       =   2
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame frmFiltro 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   60
      TabIndex        =   100
      Top             =   30
      Width           =   10485
      Begin VB.Label lblOpcionesApertura 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label47"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   9780
         TabIndex        =   101
         Top             =   480
         Width           =   555
      End
   End
End
Attribute VB_Name = "frmPeticionesShellView01"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 04.07.2007 - Se inicializan los nuevos controles combobox para soportar en la consulta la inclusi�n de filtros (impacto tecnol�gico, clase de la petici�n y documentos adjuntos)
' -001- b. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 18.09.2007 - Se agrega una columna m�s a la grilla para indicar si la petici�n pertenece al esquema nuevo o el anterior (tener en cuenta la propiedad Cols del control MSFlexGrid).
'                           Se agrega adem�s en el combo de clases el de SIN clase (util para realizar consultas).
' -003- b. FJS 18.09.2007 - Se cambia el orden de las columnas para tener la clase e indicador de IT luego del identificador de tipo de la petici�n.
' -004- a. FJS 08.10.2007 - Se agrega un manejador de error porque cuando se ejecuta la carga de la grilla de un conjunto de resultados muy grande, genera un error de runtime. Se agrega c�digo para manejar lo
'                           m�s elegantemente este error.
' -004- b. FJS 08.10.2007 - Se maneja de mejor manera la automatizaci�n de los controles de la parte de especificaci�n de par�metros de consultas.
' -005- a. FJS 21.11.2007 - Se agrega, para el perfil de Superadministrador la posibilidad de realizar una exportaci�n a Excel desde la pantalla inicial de consultas, sin necesidad de pasar por el resultado
'                           de la consulta (que trae aparejado el problema de capacidad (Overflow)).
' -006- a. FJS 18.03.2008 - Se agrega una mejora para el sector de Susana Dopazo.
' -007- a. FJS 21.07.2008 - Se contempla el argumento de devolver peticiones con documentos adjuntos o no.
' -008- a. FJS 10.09.2008 - Se agrega la columna para indicar si es regulatorio.
' -009- a. FJS 17.12.2008 - Se agregan opciones de filtro para marcar registros.
' -010- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -011- a. FJS 13.02.2009 - Se agrega un contador para indicar la cantidad de peticiones.
' -012- a. FJS 13.02.2009 - Nueva consulta para devolver los registros hist�ricos.
' -012- b. FJS 18.03.2009 - Manejo de peticiones hist�ricas.
' -013- a. FJS 17.03.2009 - Se agrega un nuevo filtro para especificar un documento en particular.
' -014- a. FJS 27.03.2009 - Se modifican estos controles porque no son los apropiadoa. Se cambian de CheckBox a OptionButtons.
' -014- b. FJS 27.03.2009 - Se agrega una leyenda para indicar el grado de apertura de la consulta.
' -015- a. FJS 31.03.2009 - Se agregan nuevas opciones de filtro para devolver peticiones.
' -015- b. FJS 03.06.2009 - Se quita la parte de proyectos (no proyectos IDM).
' -015- c. FJS 01.07.2009 - Se agrega el c�digo de legajo.
' -016- a. FJS 13.04.2010 - Se agrega un item para contemplar los estados terminales (Barbarito).
' -017- a. FJS 07.09.2010 - Planificaci�n DYD.
' -018- a. FJS 03.02.2011 - Mejora: en vez de cargar todos los combos de una, solo se cargan los necesarios, y el resto por demanda.
' -019- a. FJS 12.03.2012 - Fusi�n Banco/Consolidar.
' -020- a. FJS 11.05.2012 - Reasignaci�n masiva de BP: se agrega registro en el historial y que contemple la actualizaci�n de los datos de planificaci�n.
' -021- a. FJS 12.07.2012 - Nuevo: Se agrega el campo para indentificar peticiones vinculadas a la mitigaci�n de factores de Riesgo Operacional.
' -022- a. FJS 12.01.2016 - Modificaci�n: se lee el dato de empresa de una tabla (deja de estar hardcodeado).
' -023- a. FJS 16.02.2016 - Nuevo: se agrega funcionalidad para la nueva gerencia BPE.
' -024- a. FJS 29.06.2016 - Nuevo: nuevo campo calculado para determinar la puntuaci�n de una petici�n (basado en f�rmula de valorizaci�n).
' -025- a. FJS 29.06.2016 - Nuevo: se agrega un nuevo campo calculado para determinar la categor�a a la cual pertenece la petici�n.
' -026- a. FJS 30.06.2016 - Nuevo: filtro por categor�a.
' -027- a. FJS 20.07.2016 - Nuevo: se agregan los datos nombre del ref. de sistema + c�digo y nombre de ref. de BPE.
' -028- a. FJS 23.08.2016 - Nuevo: se quita la restricci�n de tama�o de la aplicaci�n (se permite la maximizaci�n de la pantalla).
' -028- b. FJS 23.08.2016 - Nuevo: se habilita el coloreo de filas en la grilla para mejorar la visibilidad de los datos.
' -029- a. FJS 26.08.2016 - Nuevo: para optimizar los tiempos de carga, se oculta el formulario, no se descarga de memoria.

Option Explicit

Private Destinatarios() As udtDestinatarios

Private CRC32 As New clsCRC32

Dim WithEvents logCONN As ADODB.Connection
Attribute logCONN.VB_VarHelpID = -1
Dim flgConnectOK As Boolean

Public secDire As String
Public secGere As String
Public secSect As String
Public secGrup As String

Public txtPrjId As Long
Public txtPrjSubId As Long
Public txtPrjSubsId As Long

'Private Const DISTANCIA As Integer = 250
Private Const colMultiSelect = 0
Private Const colEmpresa = 1        ' add -019- a.
Private Const colNroAsignado = 2
Private Const colTipoPet = 3
Private Const colPetClass = 4
Private Const colPetCategoria = 5        ' add -024- a.
Private Const colPetImpTech = 6
Private Const colRegulatorio = 7
Private Const colTitulo = 8
Private Const colPrioridad = 9
Private Const colPetPuntuacion = 10       ' add -024- a.
Private Const colESTADO = 11
Private Const colSituacion = 12
Private Const colSecSol = 13
Private Const colEsfuerzo = 14
Private Const colFinicio = 15
Private Const colFtermin = 16
Private Const colImportancia = 17
Private Const colCorpLocal = 18
Private Const colOrientacion = 19
Private Const colSecNomArea = 20
Private Const colEstHijo = 21
Private Const colSitHijo = 22
Private Const colPrioEjecuc = 23
Private Const colSecEsfuerzo = 24
Private Const colSecFinicio = 25
Private Const colSecFtermin = 26
Private Const colSecCodDir = 27
Private Const colSecCodGer = 28
Private Const colSecCodSec = 29
Private Const colSecCodSub = 30
Private Const colNroInterno = 31
Private Const colSRTPETIPLAN = 32
Private Const colSRTPETFPLAN = 33
Private Const colSRTSECIPLAN = 34
Private Const colSRTSECFPLAN = 35
Private Const colEstadoFec = 36
'Private Const colPetSOx = 35
Private Const colPetHist = 37
Private Const colCODESTADO = 38
Private Const colPetRO = 39            ' add -021- a.
Private Const colPetBP = 40
Private Const colPetBPNom = 41
Private Const colPetRGP = 42
Private Const colPetRGPNom = 43
Private Const colTOTACOLS = 44          ' add -025- a.

Private Const NIVEL_BBVA = 0
Private Const NIVEL_DIRECCION = 1
Private Const NIVEL_GERENCIA = 2
Private Const NIVEL_SECTOR = 3
Private Const NIVEL_GRUPO = 4
Private Const APERTURA_PETICION = 0
Private Const APERTURA_SECTORES = 1
Private Const APERTURA_GRUPOS = 2

Dim flgQuery As String              ' add -015- a.
Dim flgEnCarga As Boolean
Dim petTipo As String
Dim petPrioridad As String
Dim petNivel As String
Dim petArea As String
Dim petAreaTxt As String
Dim petEstado As String
Dim petSituacion As String
Dim petTitulo As String
Dim petImportancia As String
Dim secNivel As String
Dim secArea As String
Dim secAreaTxt As String
Dim secEstado As String
Dim secSituacion As String
Dim auxFiltro As String
Dim flgNoHijos As Boolean
Dim xDirePet, xGerePet, xSectPet, xGrupPet As String
Dim xDireSec, xGereSec, xSectSec, xGrupSec As String
Dim modo As String

Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long

Private Sub Form_Load()
    Call Inicializar
End Sub

Private Sub Form_Activate()
    Me.WindowState = vbMaximized
End Sub

'Private Sub Form_GotFocus()
'    If flgQuery = "QRY2" Then
'        fraOtrosFiltros.visible = False
'        frmGral.visible = True
'        cmdMasFiltros.Caption = "M�s filtros..."
'        flgQuery = "QRY1"
'    End If
'End Sub

Private Sub Inicializar()
    Call Puntero(True)
    Call Status("Inicializando...")
    Me.Tag = Me.Caption                     ' add -004- b.
    lblAgrup.visible = True
    txtAgrup.visible = True
    cmdAgrup.visible = True
    cmdClrAgrup.visible = True
    flgQuery = "QRY1"                       ' add -015- a.
    Call initQuery
    Call InicializarCombos
    OptNivelPet(0).value = True
    OptNivelSec(0).value = True
    'Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)            ' add -010- a.
    Call Status("Listo.")
    Call Puntero(False)
End Sub

Private Sub initQuery()
    modo = "Filtro"
    cmdConfirmar.visible = True
    cmdVisualizar.visible = False
    ' 1ero
    cmdChgBP.visible = False
    Call setHabilCtrl(cmdChgBP, "DIS")
    
    cmdAgrupar.visible = False
    cmdPrintPet.visible = False
    cmdExportar.visible = False
    grdDatos.visible = False
    frmFiltro.visible = False
    
    cmdVisualizar.Enabled = True
    cmdAgrupar.Enabled = True
    cmdPrintPet.Enabled = True

    'cmdConfirmar.Enabled = True
    Call setHabilCtrl(cmdConfirmar, "NOR")
    frmGral.visible = True
'    If flgQuery = "QRY1" Then
'        frmGral.visible = True
'        fraOtrosFiltros.visible = False
'        cmdMasFiltros.visible = True
'    Else
'        frmGral.visible = False
'        fraOtrosFiltros.visible = True
'        cmdMasFiltros.visible = True
'    End If
    'If glUsrPerfilActual = "SADM" Then
    If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
        'cmdChgBP.Enabled = True
        If secNivel = "NULL" Then Call setHabilCtrl(cmdChgBP, "NOR")
        'cmdChgBP.Visible = True
        Call setHabilCtrl(cmdChgBP, "NOR")
        cmdExportar.Enabled = True
        cmdExportar.visible = True
    End If
    If glUsrPerfilActual = "GBPE" Then
        Call setHabilCtrl(cmdChgBP, "NOR")
    End If
    Me.Caption = Me.Tag
End Sub

Private Sub InicializarCombos()
    Dim i As Integer
    Dim auxCodPet, auxCodSec As String
    
    flgEnCarga = True
    lblOpcionesApertura.Caption = ""

    With cboEmp
        .Clear
        If sp_GetEmpresa(Null, Null) Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!empid) & ": " & ClearNull(aplRST.Fields!empabrev) & ESPACIOS & "||" & ClearNull(aplRST.Fields!empid)
                aplRST.MoveNext
                DoEvents
            Loop
            .AddItem "Todas" & ESPACIOS & "||NULL", 0
            .ListIndex = 0
        End If
    End With
    
    With cboRO
        .Clear
        .AddItem "Todas" & ESPACIOS & "||NULL"
        .AddItem "Alta" & ESPACIOS & "||A"
        .AddItem "Media" & ESPACIOS & "||M"
        .AddItem "Baja" & ESPACIOS & "||B"
        .ListIndex = 0
    End With
    
    ' Estos datos sirven para poder posicionar los valores de los combos de nivel y �rea, seg�n donde pertenezca el usuario actuante
    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        xDirePet = ClearNull(aplRST!cod_direccion)
        xGerePet = ClearNull(aplRST!cod_gerencia)
        xSectPet = ClearNull(aplRST!cod_sector)
        xGrupPet = ClearNull(aplRST!cod_grupo)
        xDireSec = ClearNull(aplRST!cod_direccion)
        xGereSec = ClearNull(aplRST!cod_gerencia)
        xSectSec = ClearNull(aplRST!cod_sector)
        xGrupSec = ClearNull(aplRST!cod_grupo)
    End If
    
    With cboTipopet
        .Clear
        .AddItem "Todos" & ESPACIOS & "||NULL"
        .AddItem "NOR: Normal" & ESPACIOS & "||NOR"
        .AddItem "ESP: Especial" & ESPACIOS & "||ESP"
        .AddItem "PRO: Propia" & ESPACIOS & "||PRO"
        .AddItem "PRJ: Proyecto" & ESPACIOS & "||PRJ"
        .AddItem "AUI: Auditor�a Interna" & ESPACIOS & "||AUI"
        .AddItem "AUX: Auditor�a Externa" & ESPACIOS & "||AUX"
        .ListIndex = 0     'Por defecto son todos
    End With
    
    With cboPeticionClase
        .Clear
        .AddItem "Todas" & ESPACIOS & "||NULL"
        .AddItem "--" & ESPACIOS & "||SINC"
        .AddItem "NUEV: Nuevo desarrollo" & ESPACIOS & "||NUEV"
        .AddItem "EVOL: Manten. evolutivo" & ESPACIOS & "||EVOL"
        .AddItem "OPTI: Optimizaci�n" & ESPACIOS & "||OPTI"
        .AddItem "CORR: Correctivo" & ESPACIOS & "||CORR"
        .AddItem "ATEN: Atenci�n a usuario" & ESPACIOS & "||ATEN"
        .AddItem "SPUF: Spufi" & ESPACIOS & "||SPUF"
        .ListIndex = 0
    End With
     
    With cboPeticionImpacto
        .Clear
        .AddItem "Todos" & ESPACIOS & "||NULL"
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 0
    End With

    With cmbRegulatorio
        .Clear
        .AddItem "Todos" & ESPACIOS & "||NULL"
        .AddItem "--" & ESPACIOS & "||-"
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 0
    End With
   
    With cboImportancia
        If sp_GetImportancia(Null, Null) Then
            .Clear
            .AddItem "Todas" & ESPACIOS & "||NULL", 0
            Do While Not aplRST.EOF
                cboImportancia.AddItem Trim(aplRST!nom_importancia) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_importancia)
                aplRST.MoveNext
            Loop
        End If
        .ListIndex = 0
    End With
    
    With cboBpar
        .Clear
        If sp_GetRecursoPerfil(Null, "BPAR") Then
            Do While Not aplRST.EOF
                cboBpar.AddItem Trim(aplRST!nom_recurso) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_recurso)
                aplRST.MoveNext
            Loop
        End If
        .AddItem "Sin filtrar por Ref. de Sistema" & ESPACIOS & ESPACIOS & "||" & "NULL", 0
        .ListIndex = 0
    End With
    
    With cboBPE
        .Clear
        If sp_GetRecursoPerfil(Null, "GBPE") Then
            Do While Not aplRST.EOF
                .AddItem Trim(aplRST!nom_recurso) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_recurso)
                aplRST.MoveNext
            Loop
        End If
        .AddItem "Sin filtrar por Ref. de RGyP" & ESPACIOS & ESPACIOS & "||" & "NULL", 0
        .ListIndex = 0
    End With
    
    With cboEstadoPet
        .Clear
        If sp_GetEstado(Null) Then
            Do While Not aplRST.EOF
                If InStr("PLANRK|EJECRK|ESTIRK", ClearNull(aplRST!cod_estado)) = 0 Then
                    If Val(ClearNull(aplRST!flg_petici)) > 0 Then       ' Para estados de petici�n
                        .AddItem Trim(aplRST!nom_estado) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & aplRST!cod_estado
                        If Val(ClearNull(aplRST!flg_petici)) > 1 Then
                            auxCodPet = auxCodPet & aplRST!cod_estado & "|"
                        End If
                    End If
                    If Val(ClearNull(aplRST!flg_secgru)) > 0 Then       ' Para estados de sectores y grupos
                        cboEstadoSec.AddItem Trim(aplRST!nom_estado) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & aplRST!cod_estado
                        If Val(ClearNull(aplRST!flg_secgru)) > 1 Then
                            auxCodSec = auxCodSec & aplRST!cod_estado & "|"
                        End If
                    End If
                End If
                aplRST.MoveNext
            Loop
        End If
        .AddItem "Peticiones en estado terminal" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", 0    ' add -016- a.
        .AddItem "Peticiones en estado activo" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & auxCodPet, 0
        .AddItem "Sin filtrar por estado" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & "NULL", 0
        .ListIndex = 0
    End With
    
    With cboEstadoSec
        .Clear
        .AddItem "Sector/Grupo en estado terminal" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", 0    ' add -016- a.
        .AddItem "Sector/Grupo en estado activo" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & auxCodSec, 0
        .AddItem "Sector/Grupo en estado pendiente" & ESPACIOS & ESPACIOS & ESPACIOS & "||OPINIO|EVALUA|PLANIF|ESTIMA|", 0
        .AddItem "Sin filtrar por estado" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & "NULL", 0
        .ListIndex = 0
    End With

    With cboSituacionPet
        .Clear
        If sp_GetSituacion(Null) Then
            Do While Not aplRST.EOF
                .AddItem Trim(aplRST!nom_situacion) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & aplRST!cod_situacion
                aplRST.MoveNext
            Loop
        End If
        .AddItem "Sin filtrar por situaci�n" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & "NULL", 0
        .ListIndex = 0
    End With
    
    With cboSituacionSec
        .Clear
        If sp_GetSituacion(Null) Then
            Do While Not aplRST.EOF
                .AddItem Trim(aplRST!nom_situacion) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & aplRST!cod_situacion
                aplRST.MoveNext
            Loop
        End If
        .AddItem "Sin filtrar por situaci�n" & ESPACIOS & ESPACIOS & ESPACIOS & "||" & "NULL", 0
        .ListIndex = 0
    End With
    
    With cboBBVASec
        .Clear
        .AddItem "Todas las �res ejecutoras" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL", 0
        .ListIndex = 0
    End With
    
    With cboBBVAPet
        .Clear
        .AddItem "Todas las �res solicitantes" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL", 0
        .ListIndex = 0
    End With
    
    With cboCategoria
        .Clear
        .AddItem "Todas" & ESPACIOS & "||NULL"
        .AddItem "---" & ESPACIOS & "||---"
        .AddItem "1. REG" & ESPACIOS & "||REG"
        .AddItem "2. SDA" & ESPACIOS & "||SDA"
        .AddItem "3. PES" & ESPACIOS & "||PES"
        .AddItem "4. ENG" & ESPACIOS & "||ENG"
        .AddItem "5. PRI" & ESPACIOS & "||PRI"
        .AddItem "6. CSI" & ESPACIOS & "||CSI"
        .AddItem "7. SPR" & ESPACIOS & "||SPR"
        .ListIndex = 0
    End With

    Call cmdEraseProyectos_Click
    flgEnCarga = False
End Sub

Private Sub cmdEraseProyectos_Click()
    txtPrjId = 0
    txtPrjSubId = 0
    txtPrjSubsId = 0
    txtPrjNom = ""
End Sub

Private Sub cmdPrjFilter_Click()
    Load frmProyectosIDMSeleccion
    frmProyectosIDMSeleccion.Show 1
    With frmProyectosIDMSeleccion
        txtPrjId = .lProjId
        txtPrjSubId = .lProjSubId
        txtPrjSubsId = .lProjSubSId
        txtPrjNom.text = Trim(.cProjNom)
    End With
End Sub

Private Sub cmdChgBP_Click()
    Call CambioDeBPMasivo
End Sub

Private Sub CambioDeBPMasivo()
    Dim lPeriodo_Actual As Long
    Dim lPeriodo_Siguiente As Long
    Dim sPeriodo_Actual As String
    Dim sPeriodo_Siguiente As String
    Dim sLeyenda As String
    Dim flgMulti As Boolean
    Dim i As Integer
    Dim xBP As String
    Dim xBPNom As String
    Dim xPerfil As String
    
    If flgEnCarga Then Exit Sub
    
    flgEnCarga = True
    
    auxSelBpar.Show vbModal
    flgEnCarga = False
    
    If auxSelBpar.CodRecurso = "" Then
        Exit Sub
    End If
    
    DoEvents
    
    xBP = auxSelBpar.CodRecurso
    xBPNom = auxSelBpar.NomRecurso
    xPerfil = auxSelBpar.CodPerfil
    
    Dim nRwD As Integer, nRwH As Integer
    flgMulti = False
        
    With grdDatos
        If .Rows < 2 Then
            Exit Sub
        End If
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                If (Not flgMulti) Then
                    If MsgBox("Desea reasignar esta/s petici�n/es a '" & xBPNom & "'?", vbQuestion + vbYesNo) = vbNo Then
                        GoTo finx
                    End If
                End If
                Call Puntero(True)
                flgMulti = True
                Call Status("Orden:" & i)
                Select Case xPerfil
                    Case "BPAR"
                        Call sp_UpdatePetField(.TextMatrix(i, colNroInterno), "BPARTNE", xBP, Null, Null)
                        sLeyenda = "� Cambio de Referente de Sistemas: " & sp_GetRecursoNombre(.TextMatrix(i, colPetBP)) & " (" & .TextMatrix(i, colPetBP) & ") por " & xBPNom & " (" & xBP & "). �"
                        lPeriodo_Actual = 0: lPeriodo_Siguiente = 0
                        If sp_GetPeriodo(Null, Null) Then         ' Obtengo el per�odo de planificaci�n actual y el pr�ximo (en caso que se estuviera planificando para el pr�ximo per�odo)
                            Do While Not aplRST.EOF
                                If ClearNull(aplRST.Fields!vigente) = "S" Then
                                    lPeriodo_Actual = ClearNull(aplRST.Fields!per_nrointerno)
                                    sPeriodo_Actual = ClearNull(aplRST.Fields!per_abrev)
                                    aplRST.MoveNext
                                    If Not aplRST.EOF Then
                                        lPeriodo_Siguiente = ClearNull(aplRST.Fields!per_nrointerno)
                                        sPeriodo_Siguiente = ClearNull(aplRST.Fields!per_abrev)
                                    End If
                                    Exit Do
                                End If
                                aplRST.MoveNext
                                DoEvents
                            Loop
                            If sp_GetPeticionPlancab(lPeriodo_Actual, .TextMatrix(i, colNroInterno)) Then
                                If lPeriodo_Actual <> 0 Then
                                    Call sp_UpdatePeticionPlancabField(lPeriodo_Actual, .TextMatrix(i, colNroInterno), "COD_BPAR", xBP, Null, Null)
                                    sLeyenda = sLeyenda & " Por cambio de BP se actualiz� la petici�n planificada en " & sPeriodo_Actual
                                End If
                            End If
                            If sp_GetPeticionPlancab(lPeriodo_Siguiente, .TextMatrix(i, colNroInterno)) Then
                                If lPeriodo_Siguiente <> 0 Then
                                    Call sp_UpdatePeticionPlancabField(lPeriodo_Siguiente, .TextMatrix(i, colNroInterno), "COD_BPAR", xBP, Null, Null)
                                    sLeyenda = sLeyenda & " Por cambio de BP se actualiz� la petici�n planificada en " & sPeriodo_Siguiente
                                End If
                            End If
                        End If
                        Call sp_AddHistorial(.TextMatrix(i, colNroInterno), "PCHGBP", .TextMatrix(i, colCODESTADO), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sLeyenda)
                    Case "GBPE"
                        Call sp_UpdatePetField(.TextMatrix(i, colNroInterno), "REFBPE", xBP, Null, Null)
                        sLeyenda = "� Cambio de Referente de RGP: " & sp_GetRecursoNombre(.TextMatrix(i, colPetRGP)) & " (" & .TextMatrix(i, colPetRGP) & ") por " & xBPNom & " (" & xBP & "). �"
                        Call sp_AddHistorial(.TextMatrix(i, colNroInterno), "PCHGBPE", .TextMatrix(i, colCODESTADO), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sLeyenda)
                End Select
                .TextMatrix(i, colMultiSelect) = ""
           End If
        Next i
    End With
    MsgBox "Proceso de reasignaci�n finalizado.", vbInformation + vbOKOnly, "Reasignar referentes"
finx:
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub cmdAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "VIEW"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    txtAgrup.text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdAgrupar_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim i As Integer
    glSelectAgrup = ""
    glModoAgrup = "ALTAX"
    glNumeroAgrup = ""
    glModoSelectAgrup = "MODI"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    DoEvents
    
    If ClearNull(glSelectAgrup) = "" Then
        Exit Sub
    End If
    
    Dim nRwD As Integer, nRwH As Integer
    flgMulti = False
        
    With grdDatos
        If .Rows < 2 Then
            Exit Sub
        End If
        Call Puntero(True)
        Call Status("")
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                If (Not flgMulti) Then
                    If MsgBox("Desea que esta/s Peticion/es integren el Agrupamiento '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbNo Then
                        Exit For
                    End If
                End If
                Call Status("Orden:" & i)
                flgMulti = True
                'si ya existe agrup-petic no hace nada, ni siquiera avisa
                If sp_GetAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                    aplRST.Close
                Else
                    Call Puntero(True)
                    If sp_GetAgrupPeticDup(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                        Call Puntero(False)
                        If MsgBox("La Peticion:" & .TextMatrix(i, colTitulo) & Chr(13) & "ya est� declarada en el agrupamiento: " & ClearNull(aplRST!agr_titulo) & Chr(13) & "Desea igualmente que integre '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbYes Then
                           aplRST.Close
                           Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                        Else
                           aplRST.Close
                        End If
                    Else
                        Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                    End If
                    Call Puntero(False)
                End If
                .TextMatrix(i, colMultiSelect) = ""
           End If
        Next
    End With
    Call Puntero(False)
    Call Status("")
End Sub

Private Sub cmdClrAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = ""
    txtAgrup.text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdPrintPet_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim i As Integer
    Dim nCopias As Integer
    
    If grdDatos.Rows < 2 Then
        Exit Sub
    End If
    
    rptFormulario.Show vbModal
    If Not rptFormulario.m_seleccion Then
        Exit Sub
    End If
    If (Not rptFormulario.chkFormu) And (Not rptFormulario.chkHisto) Then
        Exit Sub
    End If
    nCopias = rptFormulario.Copias
    
    Call Puntero(True)
    flgMulti = False
    DoEvents
    With grdDatos
        While nCopias > 0
            i = 1
            For i = 1 To .Rows - 1
               If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                    flgMulti = True
                    Call Status("Orden:" & i)
                    If rptFormulario.chkFormu Then
                        Call PrintFormulario(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If rptFormulario.chkHisto Then
                        Call PrintHitorial(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If nCopias = 1 Then
                        .TextMatrix(i, colMultiSelect) = ""
                    End If
               End If
            Next
            nCopias = nCopias - 1
        Wend
    End With
    Call Puntero(False)
    If flgMulti Then
        MsgBox "Impresi�n finalizada.", vbInformation + vbOKOnly
    Else
        MsgBox "Nada seleccionado para imprimir.", vbExclamation + vbOKOnly
    End If
    Call Status("Listo.")
End Sub

Private Sub cmdExportar_Click()
    If flgEnCarga = True Then Exit Sub
    glEsMinuta = False
    frmPeticionesShellView01Expo.Show vbModal
End Sub

Private Sub cmdSetExport_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    On Error Resume Next
    frmPeticionesShellView01Expo.Show vbModal
    Call LockProceso(False)
End Sub

Private Sub cmdMinuta_Click()
    If flgEnCarga = True Then Exit Sub
    glEsMinuta = True
    frmPeticionesShellView01Expo.Show vbModal
End Sub

Private Sub cmdVisualizar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
'        If grdDatos.TextMatrix(grdDatos.RowSel, colPetHist) <> "H" Then    ' add -012- b.
            glModoPeticion = "VIEW"
            On Error Resume Next
            frmPeticionesCarga.Show vbModal
            'Call LockProceso(False)
'        '{ add -012- b.
'        Else
'            MsgBox "La petici�n se encuentra en los archivos hist�ricos." & vbCrLf & "No puede trabajar desde aqu� con esta petici�n.", vbInformation + vbOKOnly, "Petici�n en hist�ricos"
'        End If
'        '}
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdCerrar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If grdDatos.visible Then
        Call initQuery
    Else
        Call LockProceso(False)
        Call Status("Listo.")
        'Unload Me          ' del -029- a.
        Me.Hide             ' add -029- a.
    End If
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    petTipo = CodigoCombo(cboTipopet, True)
    'petPrioridad = CodigoCombo(cboPrioridad, True)
    petPrioridad = "NULL"
    petNivel = verNivelPet()
    petArea = verAreaPet()
    petEstado = DatosCombo(cboEstadoPet)
    petImportancia = CodigoCombo(cboImportancia, True)
    petSituacion = CodigoCombo(cboSituacionPet, True)
    secNivel = verNivelSec()
    secArea = verAreaSec()
    If secArea = "ERROR" Then
        LockProceso (False)
        Exit Sub
    End If
    secEstado = DatosCombo(cboEstadoSec)
    secSituacion = CodigoCombo(cboSituacionSec, True)
    auxFiltro = ""
    auxFiltro = auxFiltro & "Tipo Petici�n: " & TextoCombo(cboTipopet) & Chr$(13)
    'auxFiltro = auxFiltro & "Prioridad: " & TextoCombo(cboPrioridad) & Chr$(13)
    auxFiltro = auxFiltro & "Area Solicitante: " & petAreaTxt & Chr$(13)
    auxFiltro = auxFiltro & "Estado Petici�n: " & TextoCombo(cboEstadoPet) & Chr$(13)
    auxFiltro = auxFiltro & "Situaci�n Petici�n: " & TextoCombo(cboSituacionPet) & Chr$(13)
    Call initView
    Call CargarGrid
    'Call CargarGridClip
    LockProceso (False)
End Sub

Private Sub verAreaFiltro()
    Debug.Print "verAreaFiltro()"
    If flgEnCarga Then Exit Sub
    If ClearNull(txtDESDEiniplan.text) <> "" Or _
        ClearNull(txtDESDEfinplan.text) <> "" Or _
        ClearNull(txtHASTAiniplan.text) <> "" Or _
        ClearNull(txtDESDEinireal.text) <> "" Or _
        ClearNull(txtDESDEfinreal.text) <> "" Or _
        ClearNull(txtHASTAinireal.text) <> "" Or _
        ClearNull(txtHASTAfinreal.text) <> "" Or _
        cboEstadoSec.ListIndex > 0 Or _
        cboSituacionSec.ListIndex > 0 Or _
        OptNivelSec(0).value = False Then
    End If
End Sub

Private Sub cboEstadoSec_Click()
    'verAreaFiltro
End Sub

Private Sub cboSituacionSec_Click()
    'verAreaFiltro
End Sub

Private Sub txtDESDEiniplan_LostFocus()
    'verAreaFiltro
End Sub

Private Sub txtDESDEfinplan_LostFocus()
    'verAreaFiltro
End Sub

Private Sub txtHASTAiniplan_LostFocus()
    'verAreaFiltro
End Sub

Private Sub txtHASTAfinplan_LostFocus()
    'verAreaFiltro
End Sub

Private Sub txtDESDEinireal_LostFocus()
    'verAreaFiltro
End Sub

Private Sub txtDESDEfinreal_LostFocus()
    'verAreaFiltro
End Sub

Private Sub txtHASTAinireal_LostFocus()
    'verAreaFiltro
End Sub

Private Sub txtHASTAfinreal_LostFocus()
    'verAreaFiltro
End Sub

Private Sub chkAreSec_Click()
    'verAreaFiltro
End Sub

Private Sub chkAreGru_Click()
    'verAreaFiltro
End Sub

'{ add -028- a.
Private Sub Form_Resize()
    'Debug.Print "Me.WindowState: " & Me.WindowState & " / Me.Width: " & Me.Width & " - Me.Height: " & Me.Height
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState = 0 Then
            Me.Top = 0
            Me.Left = 0
            'Me.Height = 8475
            'Me.Width = 12000
            Me.Height = 8475
            Me.Width = 14300
        End If
        ' Ancho
        If Me.visible Then
            'If pnlBotones.Width - 150 > Me.ScaleWidth Then
            If Me.ScaleWidth > pnlBotones.Width - 150 Then frmFiltro.Width = Me.ScaleWidth - pnlBotones.Width - 150
            If Me.ScaleWidth > pnlBotones.Width - 150 Then grdDatos.Width = Me.ScaleWidth - pnlBotones.Width - 150
            pnlBotones.Left = frmFiltro.Width + 100
            lblOpcionesApertura.Left = frmFiltro.Width - lblOpcionesApertura.Width - 200
            ' Alto
            If Me.ScaleHeight > 0 Then grdDatos.Height = Me.ScaleHeight - frmFiltro.Height - 100
            If Me.ScaleHeight > 0 Then pnlBotones.Height = Me.ScaleHeight - 50
            cmdCerrar.Top = pnlBotones.Height - cmdCerrar.Height - 100
            Command1.Top = cmdCerrar.Top - Command1.Height - 20
        End If
    End If
End Sub
'}

Private Sub initView()
    modo = "Vista"
    cmdConfirmar.Enabled = False
    cmdConfirmar.visible = False
    cmdVisualizar.visible = True
    cmdVisualizar.Enabled = False
    cmdChgBP.visible = True
    cmdAgrupar.visible = True
    cmdPrintPet.visible = True
    cmdPrintPet.Enabled = False
    'cmdChgBP.Enabled = False           ' Esto deshabilitaba la opci�n
    cmdAgrupar.Enabled = False
    frmGral.visible = False
    
    cmdVisualizar.Enabled = True
    cmdAgrupar.Enabled = True
    cmdPrintPet.Enabled = True
'    '{ add -015- a.
'    fraOtrosFiltros.visible = False
'    cmdMasFiltros.visible = False
'    '}
    grdDatos.visible = True
    frmFiltro.visible = True
End Sub

Public Function verNivelPet() As Variant
    verNivelPet = "NULL"
    If OptNivelPet(0).value = True Then
         verNivelPet = "NULL"
    End If
    If OptNivelPet(1).value = True Then
         verNivelPet = "DIRE"
    End If
    If OptNivelPet(2).value = True Then
         verNivelPet = "GERE"
    End If
    If OptNivelPet(3).value = True Then
         verNivelPet = "SECT"
    End If
End Function

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    If grdDatos.row <= grdDatos.rowSel Then
        nRwD = grdDatos.row
        nRwH = grdDatos.rowSel
    Else
        nRwH = grdDatos.row
        nRwD = grdDatos.rowSel
    End If
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, colMultiSelect) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, colMultiSelect) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        If Button = vbLeftButton Then
        With grdDatos
        If .rowSel > 0 Then
             If ClearNull(.TextMatrix(.rowSel, colMultiSelect)) = "" Then
                .TextMatrix(.rowSel, colMultiSelect) = "�"
             Else
                .TextMatrix(.rowSel, colMultiSelect) = ""
            End If
        End If
        End With
        End If
    End If
    If Shift = 0 Then
        If Button = vbLeftButton Then
            With grdDatos
                For i = 1 To .Rows - 1
                   .TextMatrix(i, colMultiSelect) = ""
                Next
                If .rowSel > 0 Then
                    .TextMatrix(.rowSel, colMultiSelect) = "�"
                End If
            End With
        End If
    End If
End Sub

Private Sub grdDatos_RowColChange()
    If Not flgEnCarga Then Call MostrarSeleccion
End Sub

Private Sub OptNivelPet_Click(Index As Integer)
    cboBBVAPet.visible = False
    cboDireccionPet.visible = False
    cboGerenciaPet.visible = False
    cboSectorPet.visible = False
    'cboBBVAPet.Enabled = False
    'cboDireccionPet.Enabled = False
    'cboGerenciaPet.Enabled = False
    'cboSectorPet.Enabled = False
    Select Case Index
        Case NIVEL_BBVA  ' Todo BBVA
            cboBBVAPet.visible = True
        Case NIVEL_DIRECCION  ' Direcci�n
            '{ add -018- a.
            cboDireccionPet.Clear
            'cboDireccionSec.Clear
            If sp_GetDireccion(Null, "S") Then
                Do While Not aplRST.EOF
                    cboDireccionPet.AddItem Trim(aplRST!nom_direccion) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion)
'                    If ClearNull(aplRST!es_ejecutor) = "S" Then
'                        cboDireccionSec.AddItem Trim(aplRST!nom_direccion) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_direccion) & "|NULL|NULL|NULL"
'                    End If
                    aplRST.MoveNext
                Loop
            End If
            cboDireccionPet.ListIndex = PosicionCombo(Me.cboDireccionPet, xDirePet, True)
            '}
            cboDireccionPet.visible = True
            cboDireccionPet.Enabled = True
        Case NIVEL_GERENCIA  ' Gerencia
            '{ add -018- a.
            If sp_GetGerenciaXt(Null, Null, "S") Then
                cboGerenciaPet.Clear
                'cboGerenciaSec.Clear
                Do While Not aplRST.EOF
                    cboGerenciaPet.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & aplRST(0)
'                    If ClearNull(aplRST!es_ejecutor) = "S" Then
'                        cboGerenciaSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|NULL|NULL"
'                    End If
                    aplRST.MoveNext
                Loop
            End If
            cboGerenciaPet.ListIndex = PosicionCombo(Me.cboGerenciaPet, xGerePet, True)
            '}
            cboGerenciaPet.visible = True
            cboGerenciaPet.Enabled = True
        Case NIVEL_SECTOR  ' Sector
            '{ add -018- a.
            If sp_GetSectorXt(Null, Null, Null, "S") Then
                cboSectorPet.Clear
                'cboSectorSec.Clear
                Do While Not aplRST.EOF
                    cboSectorPet.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & aplRST(0)
'                    If ClearNull(aplRST!es_ejecutor) = "S" Then
'                        cboSectorSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|NULL"
'                    End If
                    aplRST.MoveNext
                Loop
            End If
            cboSectorPet.ListIndex = PosicionCombo(Me.cboSectorPet, xSectPet, True)
            '}
            cboSectorPet.visible = True
            cboSectorPet.Enabled = True
    End Select
End Sub

Public Function verAreaPet() As Variant
    verAreaPet = "NULL"
    petAreaTxt = ""
    If OptNivelPet(0).value = True Then
         verAreaPet = "NULL"
         petAreaTxt = TextoCombo(Me.cboBBVAPet)
    End If
    If OptNivelPet(1).value = True Then
         verAreaPet = CodigoCombo(Me.cboDireccionPet, True)
         petAreaTxt = TextoCombo(Me.cboDireccionPet)
    End If
    If OptNivelPet(2).value = True Then
         verAreaPet = CodigoCombo(Me.cboGerenciaPet, True)
         petAreaTxt = TextoCombo(Me.cboGerenciaPet)
    End If
    If OptNivelPet(3).value = True Then
         verAreaPet = CodigoCombo(Me.cboSectorPet, True)
         petAreaTxt = TextoCombo(Me.cboSectorPet)
    End If
End Function

Private Sub OptNivelSec_Click(Index As Integer)
    Dim sDireccion As String
    Dim sGerencia As String
            
    cboBBVASec.visible = False
    cboDireccionSec.visible = False
    cboGerenciaSec.visible = False
    cboSectorSec.visible = False
    cboGrupoSec.visible = False
    'cboBBVASec.Enabled = False
    'cboDireccionSec.Enabled = False
    'cboGerenciaSec.Enabled = False
    'cboSectorSec.Enabled = False
    'cboGrupoSec.Enabled = False
    Select Case Index
        Case NIVEL_BBVA      ' Todo el BBVA
            cboBBVASec.visible = True
            'verAreaFiltro
            Call HabilitarApertura(Index)
            'optApertura(APERTURA_PETICION).Value = True: optApertura(APERTURA_PETICION).Enabled = True
            'optApertura(APERTURA_SECTORES).Value = False: optApertura(APERTURA_SECTORES).Enabled = True
            'optApertura(APERTURA_GRUPOS).Value = False: optApertura(APERTURA_GRUPOS).Enabled = True
        Case NIVEL_DIRECCION      ' Nivel: Direcci�n
            '{ add -018- a.
            If sp_GetDireccion(Null, "S") Then
                cboDireccionSec.Clear
                Do While Not aplRST.EOF
                    If ClearNull(aplRST!es_ejecutor) = "S" Then
                        cboDireccionSec.AddItem Trim(aplRST!nom_direccion) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_direccion) & "|NULL|NULL|NULL"
                    End If
                    aplRST.MoveNext
                Loop
            End If
            cboDireccionSec.ListIndex = PosicionCombo(Me.cboDireccionSec, xDireSec, True)
            '}
            cboDireccionSec.visible = True
            cboDireccionSec.Enabled = True
            Call HabilitarApertura(Index)
            'optApertura(0).Value = False: optApertura(0).Enabled = False
            'optApertura(1).Value = False: optApertura(1).Enabled = True
            'optApertura(2).Value = True: optApertura(2).Enabled = True
        Case NIVEL_GERENCIA      ' Nivel: Gerencia
            '{ add -018- a.
            If sp_GetGerenciaXt(Null, Null, "S") Then
                cboGerenciaSec.Clear
                Do While Not aplRST.EOF
                    If ClearNull(aplRST!es_ejecutor) = "S" Then
                        cboGerenciaSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|NULL|NULL"
                    End If
                    aplRST.MoveNext
                Loop
            End If
            cboGerenciaSec.ListIndex = PosicionCombo(Me.cboGerenciaSec, xDireSec & xGereSec, True)
            '}
            cboGerenciaSec.visible = True
            cboGerenciaSec.Enabled = True
            Call HabilitarApertura(Index)
'            optApertura(0).Value = False: optApertura(0).Enabled = False
'            optApertura(1).Value = False: optApertura(1).Enabled = True
'            optApertura(2).Value = True: optApertura(2).Enabled = True
        Case NIVEL_SECTOR      ' Nivel: Sector
            '{ add -018- a.
            If sp_GetSectorXt(Null, Null, Null, "S") Then
                cboSectorSec.Clear
                Do While Not aplRST.EOF
                    If ClearNull(aplRST!es_ejecutor) = "S" Then
                        cboSectorSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|NULL"
                    End If
                    aplRST.MoveNext
                Loop
            End If
            cboSectorSec.ListIndex = PosicionCombo(Me.cboSectorSec, xDireSec & xGereSec & xSectSec, True)
            '}
            cboSectorSec.visible = True
            cboSectorSec.Enabled = True
            Call HabilitarApertura(Index)
'            optApertura(0).Value = False: optApertura(0).Enabled = False
'            optApertura(1).Value = False: optApertura(1).Enabled = True
'            optApertura(2).Value = True: optApertura(2).Enabled = True
        Case NIVEL_GRUPO      ' Nivel: Grupo
            '{ add -018- a.
            If sp_GetGrupoXt("", "", "S") Then
                cboGrupoSec.Clear
                Do While Not aplRST.EOF
                    If ClearNull(aplRST!es_ejecutor) = "S" Then
                        'cboGrupoSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(DISTANCIA) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & ClearNull(aplRST!cod_grupo) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_grupo)
                        sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                        sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                        cboGrupoSec.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & ClearNull(aplRST!cod_grupo) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_grupo)
                    End If
                    aplRST.MoveNext
                Loop
            End If
            cboGrupoSec.ListIndex = PosicionCombo(Me.cboGrupoSec, xDireSec & xGereSec & xSectSec & xGrupSec, True)
            '}
            cboGrupoSec.visible = True
            cboGrupoSec.Enabled = True
            Call HabilitarApertura(Index)
'            optApertura(0).Value = False: optApertura(0).Enabled = False
'            optApertura(1).Value = False: optApertura(1).Enabled = False
'            optApertura(2).Value = True: optApertura(2).Enabled = True
    End Select
End Sub

Private Sub HabilitarApertura(indice As Integer)
    Select Case indice
        Case NIVEL_BBVA
            optApertura(APERTURA_PETICION).value = True: optApertura(APERTURA_PETICION).Enabled = True
            optApertura(APERTURA_SECTORES).value = False: optApertura(APERTURA_SECTORES).Enabled = True
            optApertura(APERTURA_GRUPOS).value = False: optApertura(APERTURA_GRUPOS).Enabled = True
        Case NIVEL_DIRECCION
            optApertura(0).value = False: optApertura(0).Enabled = False
            optApertura(1).value = False: optApertura(1).Enabled = True
            optApertura(2).value = True: optApertura(2).Enabled = True
        Case NIVEL_GERENCIA
            optApertura(0).value = False: optApertura(0).Enabled = False
            optApertura(1).value = False: optApertura(1).Enabled = True
            optApertura(2).value = True: optApertura(2).Enabled = True
        Case NIVEL_SECTOR
            optApertura(0).value = False: optApertura(0).Enabled = False
            optApertura(1).value = False: optApertura(1).Enabled = True
            optApertura(2).value = True: optApertura(2).Enabled = True
        Case NIVEL_GRUPO
            optApertura(0).value = False: optApertura(0).Enabled = False
            optApertura(1).value = False: optApertura(1).Enabled = False
            optApertura(2).value = True: optApertura(2).Enabled = True
    End Select
End Sub

Public Function verAreaSec() As Variant
    Dim vRetorno() As String
    Dim auxAgrup As String
       
    verAreaSec = "NULL"
    secDire = "NULL"
    secGere = "NULL"
    secSect = "NULL"
    secGrup = "NULL"
    secAreaTxt = ""
       
    If OptNivelSec(0).value = True Then
         secAreaTxt = TextoCombo(Me.cboBBVASec)
    End If
    If OptNivelSec(1).value = True Then
         If cboDireccionSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Direcci�n")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboDireccionSec, True)
         auxAgrup = DatosCombo(Me.cboDireccionSec)
         secAreaTxt = TextoCombo(Me.cboDireccionSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(2).value = True Then
         If cboGerenciaSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Gerencia")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboGerenciaSec, True)
         auxAgrup = DatosCombo(Me.cboGerenciaSec)
         secAreaTxt = TextoCombo(Me.cboGerenciaSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(3).value = True Then
         If cboSectorSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Sector")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboSectorSec, True)
         auxAgrup = DatosCombo(Me.cboSectorSec)
         secAreaTxt = TextoCombo(Me.cboSectorSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(4).value = True Then
         If cboGrupoSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Grupo")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboGrupoSec, True)
         secAreaTxt = TextoCombo(Me.cboGrupoSec)
         auxAgrup = DatosCombo(Me.cboGrupoSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
End Function

Public Function verNivelSec() As Variant
    If optApertura(0).value Then
        verNivelSec = "NULL"
    ElseIf optApertura(1).value Then
        verNivelSec = "SECT"
    ElseIf optApertura(2).value Then
        verNivelSec = "GRUP"
    End If
    ' AGREGADO 13.11.2014
    If verNivelSec = "NULL" Then
        If InStr(1, "SADM|ADMI|GBPE|", glUsrPerfilActual, vbTextCompare) > 0 Then
            Call setHabilCtrl(cmdChgBP, "NOR")
        End If
    Else
        Call setHabilCtrl(cmdChgBP, "DIS")
    End If
End Function

Private Sub InicializarGrilla()
    If secNivel = "NULL" And secArea = "NULL" And secEstado = "NULL" And secSituacion = "NULL" And _
        IsNull(txtDESDEiniplan.DateValue) And IsNull(txtHASTAiniplan.DateValue) And IsNull(txtDESDEfinplan.DateValue) And IsNull(txtHASTAfinplan.DateValue) And _
        IsNull(txtDESDEinireal.DateValue) And IsNull(txtHASTAinireal.DateValue) And IsNull(txtDESDEfinreal.DateValue) And IsNull(txtHASTAfinreal.DateValue) Then
        flgNoHijos = True
    Else
        flgNoHijos = False
    End If
    With grdDatos
        .visible = False
        .Clear
        '.cols = 40                                  ' add -012- b.  ' upd -019- a. Antes 36 ' upd -020- a. Antes -1 ' 39
        .cols = colTOTACOLS                       ' add -025- a.
        .Rows = 1
        .HighLight = flexHighlightAlways
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .TextMatrix(0, colEmpresa) = "Emp.": .ColWidth(colEmpresa) = 500: .ColAlignment(colEmpresa) = flexAlignRightCenter  ' add -019- a.
        .TextMatrix(0, colNroAsignado) = "Pet. N�": .ColWidth(colNroAsignado) = 700: .ColAlignment(colNroAsignado) = flexAlignRightCenter
        .TextMatrix(0, colPrioridad) = "Pri.": .ColWidth(colPrioridad) = 400: .ColAlignment(colPrioridad) = 3
        .TextMatrix(0, colTipoPet) = "Tipo": .ColWidth(colTipoPet) = 500: .ColAlignment(colTipoPet) = flexAlignLeftCenter
        .TextMatrix(0, colPetClass) = "Clase": .ColWidth(colPetClass) = 600: .ColAlignment(colPetClass) = flexAlignLeftCenter
        .TextMatrix(0, colRegulatorio) = "Reg.": .ColWidth(colRegulatorio) = 500: .ColAlignment(colRegulatorio) = flexAlignCenterCenter
        .TextMatrix(0, colPetImpTech) = "I.T.": .ColWidth(colPetImpTech) = 500: .ColAlignment(colPetImpTech) = flexAlignCenterCenter
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 4500: .ColAlignment(colTitulo) = 0
        .TextMatrix(0, colESTADO) = "Estado Pet": .ColWidth(colESTADO) = 2000
        .TextMatrix(0, colEstadoFec) = "F.Estado": .ColWidth(colEstadoFec) = 1200
        .TextMatrix(0, colSituacion) = "Situaci�n Pet.": .ColWidth(colSituacion) = 1600
        .TextMatrix(0, colCODESTADO) = "cod_estado": .ColWidth(colCODESTADO) = 0    ' add -020- a.
        .TextMatrix(0, colSecSol) = "Sect. Solicitante": .ColWidth(colSecSol) = 3000
        .TextMatrix(0, colEsfuerzo) = "Hs.Pet"
        .TextMatrix(0, colFinicio) = "F.Ini.Planif.Pet."
        .TextMatrix(0, colFtermin) = "F.Fin Planif.Pet."
        .TextMatrix(0, colImportancia) = "Visibilidad": .ColWidth(colImportancia) = 2000
        .TextMatrix(0, colOrientacion) = "Orientaci�n": .ColWidth(colOrientacion) = 3000: .ColAlignment(colOrientacion) = flexAlignLeftCenter
        .TextMatrix(0, colCorpLocal) = "Corp/Local": .ColWidth(colCorpLocal) = 1000: .ColAlignment(colCorpLocal) = flexAlignCenterCenter
        .TextMatrix(0, colEstHijo) = "Est. Sec/Grp"
        .TextMatrix(0, colSitHijo) = "Sit. Sec/Grp"
        .TextMatrix(0, colPrioEjecuc) = "Pri.": .ColWidth(colPrioEjecuc) = 0
        .TextMatrix(0, colNroInterno) = "NroInt.": .ColWidth(colNroInterno) = 0
        .TextMatrix(0, colSecCodDir) = "Cod.Dir"
        .TextMatrix(0, colSecCodGer) = "Cod.Ger"
        .TextMatrix(0, colSecCodSec) = "Cod.Sec"
        .TextMatrix(0, colSecCodSub) = "Cod.Sub"
        .TextMatrix(0, colSecNomArea) = ""
        '.TextMatrix(0, colPetSOx) = "MN.": .ColWidth(colPetSOx) = 0 ': .ColAlignment(colPetSOx) = flexAlignCenterCenter
        .TextMatrix(0, colPetHist) = "His.": .ColWidth(colPetHist) = 0
        .TextMatrix(0, colPetRO) = "Vinc.RO": .ColWidth(colPetRO) = 700: .ColAlignment(colPetRO) = flexAlignLeftCenter ' add -021- a.
        .TextMatrix(0, colPetBP) = "Ref. de Sistema": .ColWidth(colPetBP) = 1400: .ColAlignment(colPetBP) = flexAlignLeftCenter
        .ColWidth(colMultiSelect) = 200
        .ColWidth(colSRTPETIPLAN) = 0
        .ColWidth(colSRTPETFPLAN) = 0
        .ColWidth(colSRTSECIPLAN) = 0
        .ColWidth(colSRTSECFPLAN) = 0
        .TextMatrix(0, colPetCategoria) = "Categ.": .ColWidth(colPetCategoria) = 650: .ColAlignment(colPetCategoria) = flexAlignLeftCenter         ' add -024- a.
        .TextMatrix(0, colPetPuntuacion) = "Ptos.": .ColWidth(colPetPuntuacion) = 700: .ColAlignment(colPetPuntuacion) = flexAlignRightCenter     ' add -025- a.
        '{ add -025- a.
        .TextMatrix(0, colPetBPNom) = "Nombre de Ref. de sistemas": .ColWidth(colPetBPNom) = 2500: .ColAlignment(colPetBPNom) = flexAlignLeftCenter
        .TextMatrix(0, colPetRGP) = "Ref. de RGP": .ColWidth(colPetRGP) = 1000: .ColAlignment(colPetRGP) = flexAlignLeftCenter
        .TextMatrix(0, colPetRGPNom) = "Nombre de Ref. de RGP": .ColWidth(colPetRGPNom) = 2500: .ColAlignment(colPetRGPNom) = flexAlignLeftCenter
        '}
        If flgNoHijos Then
            .ColWidth(colEsfuerzo) = 800
            .ColWidth(colFinicio) = 1000
            .ColWidth(colFtermin) = 1000
            .ColWidth(colEstHijo) = 0
            .ColWidth(colSitHijo) = 0
            .ColWidth(colSecCodDir) = 0: .TextMatrix(0, colSecCodDir) = ""
            .ColWidth(colSecCodGer) = 0: .TextMatrix(0, colSecCodGer) = ""
            .ColWidth(colSecCodSec) = 0: .TextMatrix(0, colSecCodSec) = ""
            .ColWidth(colSecCodSub) = 0: .TextMatrix(0, colSecCodSub) = ""
            .ColWidth(colSecNomArea) = 0: .TextMatrix(0, colSecNomArea) = ""
            .ColWidth(colSecEsfuerzo) = 0: .TextMatrix(0, colSecEsfuerzo) = ""
            .ColWidth(colSecFinicio) = 0: .TextMatrix(0, colSecFinicio) = ""
            .ColWidth(colSecFtermin) = 0: .TextMatrix(0, colSecFtermin) = ""
        Else
            .ColWidth(colEsfuerzo) = 0
            .ColWidth(colFinicio) = 0
            .ColWidth(colFtermin) = 0
            .ColWidth(colSecEsfuerzo) = 600
            .ColWidth(colSecFinicio) = 1000
            .ColWidth(colSecFtermin) = 1000
            .ColWidth(colEstHijo) = 2500
            .ColWidth(colSitHijo) = 1800
            .ColWidth(colSecCodDir) = 0
            .ColWidth(colSecCodGer) = 0
            .ColWidth(colSecCodSec) = 0
            .ColWidth(colSecCodSub) = 0
            .ColWidth(colSecNomArea) = 4000
            If secNivel = "GRUP" Then
                .TextMatrix(0, colSecNomArea) = "Grupo Ejec."
                .TextMatrix(0, colEstHijo) = "Est.Grupo"
                .TextMatrix(0, colSitHijo) = "Sit.Grupo"
                .TextMatrix(0, colSecEsfuerzo) = "Hs.Grupo"
                .TextMatrix(0, colSecFinicio) = "F.Ini.Planif.Gru."
                .TextMatrix(0, colSecFtermin) = "F.Fin Planif.Gru."
            Else
                .TextMatrix(0, colSecNomArea) = "Sector Ejec."
                .TextMatrix(0, colEstHijo) = "Est.Sector"
                .TextMatrix(0, colSitHijo) = "Sit.Sector"
                .TextMatrix(0, colSecEsfuerzo) = "Hs.Sect."
                .TextMatrix(0, colSecFinicio) = "F.Ini.Planif.Sec."
                .TextMatrix(0, colSecFtermin) = "F.Fin Planif.Sec"
            End If
            If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                .ColWidth(colPrioEjecuc) = 350
            End If
        End If
    End With
    Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
End Sub

Private Sub CargarGrid()
    On Error GoTo ErrHandler                ' add -004- a.
    
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
    Dim xAux As String
    Dim vRetorno() As String
    Dim auxAgrup As String
    Dim lPeticiones_Listadas As Long        ' add -011- a.
    Dim cod_recurso(1 To 5) As String       ' add -015- a.
    '{ add -004- a.
    Dim lRecCount As Long
    Dim bGridOverflow As Boolean
    Dim bRegenerate As Boolean
    Dim i As Long, J As Long
    
    bRegenerate = False
    bGridOverflow = False
    '}
    flgEnCarga = True
    
    auxAgrup = ""
    If ClearNull(txtAgrup.text) <> "" Then
        If ParseString(vRetorno, txtAgrup.text, "|") > 0 Then
            auxAgrup = vRetorno(2)
        End If
    End If
    
    glNumeroPeticion = ""
    glCodigoRecurso = ""
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    
    Call Puntero(True)
    Call InicializarGrilla
    Call Status("Cargando peticiones...")
    If ClearNull(txtTitulo.text) = "" Then
        xAux = "NULL"
    Else
        xAux = ClearNull(txtTitulo.text)
    End If
    cmdVisualizar.visible = False
    ' NOTA: aqu� vendr�a el c�digo para la parte hist�rica (1)
'    cod_recurso(1) = ""     ' Usuario
'    cod_recurso(2) = ""     ' Solicitante
'    cod_recurso(3) = ""     ' Referente
'    cod_recurso(4) = ""     ' Supervisor
'    cod_recurso(5) = ""     ' Director/Autorizante
'    If cboUsuario.ListIndex > 0 Then cod_recurso(1) = CodigoCombo(cboUsuario, True)
'    If cboSolicitante.ListIndex > 0 Then cod_recurso(2) = CodigoCombo(cboSolicitante, True)
'    If cboReferente.ListIndex > 0 Then cod_recurso(3) = CodigoCombo(cboReferente, True)
'    If cboSupervisor.ListIndex > 0 Then cod_recurso(4) = CodigoCombo(cboSupervisor, True)
'    If cboDirector.ListIndex > 0 Then cod_recurso(5) = CodigoCombo(cboDirector, True)

    'If Not sp_rptPeticion00(txtNroDesde.Text, txtNroHasta.Text, petTipo, petPrioridad, txtDESDEalta.DateValue, txtHASTAalta.DateValue, petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
    '                        CodigoCombo(Me.cboBpar, True), CodigoCombo(cboPeticionClase, True), UCase(Left(cboPeticionImpacto.Text, 1)), UCase(Left(cmbPetDocu.Text, 1)), CodigoCombo(cmbRegulatorio, True), IIf(UCase(Left(cmbPetDocu, 1)) = "C", UCase(Left(cmbDocumentoAdjunto, 4)), Null), CodigoCombo(cmbTextos, True), Trim(txtMEM_Texto), CodigoCombo(cmbOpcionesTexto, True), cod_recurso(), txtFechaPedidoDesde, txtFechaPedidoHasta, txtPrjId, txtPrjSubId, txtPrjSubsId) Then   ' upd -008- a. Se agrega la variable-filtro 'cFiltroRegulatorio'    ' upd -013- a.      ' upd -015- a.
    '   GoTo finx
    'End If
'If Not sp_rptPeticion00(txtNroDesde.Text, _
'                        txtNroHasta.Text, _
'                        petTipo, _
'                        petPrioridad, _
'                        txtDESDEalta.DateValue, _
'                        txtHASTAalta.DateValue, _
'                        petNivel, petArea, petEstado, _
'                        txtDESDEestado.DateValue, _
'                        txtHASTAestado.DateValue, _
'                        petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, _
'                        txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, _
'                        txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, _
'                        txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
'                        CodigoCombo(Me.cboBpar, True), CodigoCombo(cboPeticionClase, True), CodigoCombo(cboPeticionImpacto, True), _
'                        CodigoCombo(cmbPetDocu, True), CodigoCombo(cmbRegulatorio, True), _
'                        IIf(CodigoCombo(cmbPetDocu, True) = "C", CodigoCombo(cmbDocumentoAdjunto, True), Null), _
'                        CodigoCombo(cmbTextos, True), _
'                        ClearNull(txtMEM_Texto), _
'                        CodigoCombo(cmbOpcionesTexto, True), _
'                        cod_recurso(), _
'                        txtFechaPedidoDesde, _
'                        txtFechaPedidoHasta, _
'                        txtPrjId, txtPrjSubId, txtPrjSubsId, IIf(CodigoCombo(cboEmp, True) = "NULL", Null, CodigoCombo(cboEmp, True)), CodigoCombo(cboRO, True), CodigoCombo(cboBPE, True), txtBPEDesde, txtBPEHasta, CodigoCombo(cboCategoria, True)) Then ' upd -019- a.   ' upd -021- a.  ' upd -023- a. ' upd -026- a.
    
    If Not sp_rptPeticion00(txtNroDesde.text, txtNroHasta.text, _
                        petTipo, _
                        petPrioridad, _
                        txtDESDEalta.DateValue, txtHASTAalta.DateValue, _
                        petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, _
                        petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, _
                        txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, _
                        txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, _
                        txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
                        CodigoCombo(Me.cboBpar, True), CodigoCombo(cboPeticionClase, True), CodigoCombo(cboPeticionImpacto, True), CodigoCombo(cmbRegulatorio, True), _
                        txtPrjId, txtPrjSubId, txtPrjSubsId, IIf(CodigoCombo(cboEmp, True) = "NULL", Null, CodigoCombo(cboEmp, True)), CodigoCombo(cboRO, True), CodigoCombo(cboBPE, True), txtBPEDesde, txtBPEHasta, CodigoCombo(cboCategoria, True), txtDesdeAltaPeticion.DateValue, txtHastaAltaPeticion.DateValue) Then
        GoTo finx
    End If
    lPeticiones_Listadas = lPeticiones_Listadas + aplRST.RecordCount   ' add -011- a.
' NUEVO GRID
'    With grdDatos
'        .Rows = aplRST.RecordCount + 1      ' Inicializa el total de filas
'        .Cols = aplRST.Fields.Count         ' Inicializa el total de columnas
'        For i = 1 To grdDatos.Rows
'            If i Mod 2 > 0 Then
'                .RowSel = i
'                .Row = .RowSel
'                For j = 0 To .Cols - 1
'                    .Col = j
'                    .CellBackColor = &HF4F4F4
'                Next j
'            End If
'        Next i
'    End With
'    'Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
'    i = 0
'    Call ObtenerTiempo(True)
'    Do While Not aplRST.EOF
'        With grdDatos
'            i = i + 1
'            If bGridOverflow Then
'                Exit Do
'            End If
'            .TextMatrix(i, colEmpresa) = ClearNull(aplRST!pet_emp)  ' add -019- a.
'            .TextMatrix(i, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
'            .TextMatrix(i, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
'            .TextMatrix(i, colTitulo) = ClearNull(aplRST!Titulo)
'            .TextMatrix(i, colCODESTADO) = ClearNull(aplRST!cod_estado)
'            .TextMatrix(i, colESTADO) = ClearNull(aplRST!nom_estado)
'            .TextMatrix(i, colSituacion) = ClearNull(aplRST!nom_situacion)
'            .TextMatrix(i, colEstHijo) = ClearNull(aplRST!sec_nom_estado)
'            .TextMatrix(i, colSitHijo) = ClearNull(aplRST!sec_nom_situacion)
'            .TextMatrix(i, colEsfuerzo) = ClearNull(aplRST!horaspresup)
'            .TextMatrix(i, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
'            .TextMatrix(i, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
'            .TextMatrix(i, colEstadoFec) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
'            .TextMatrix(i, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
'            .TextMatrix(i, colPrioridad) = ClearNull(aplRST!prioridad)
'            .TextMatrix(i, colImportancia) = ClearNull(aplRST!importancia_nom)
'            .TextMatrix(i, colCorpLocal) = ClearNull(aplRST!corp_local)
'            .TextMatrix(i, colOrientacion) = ClearNull(aplRST!nom_orientacion)
'            .TextMatrix(i, colSecSol) = ClearNull(aplRST!sol_sector)
'            .TextMatrix(i, colPetRO) = ClearNull(aplRST!pet_riesgo)        ' add -021- a.
'            If flgNoHijos = True Then
'                .TextMatrix(i, colSecCodDir) = ""
'                .TextMatrix(i, colSecCodGer) = ""
'                .TextMatrix(i, colSecCodSec) = ""
'                .TextMatrix(i, colSecCodSub) = ""
'                .TextMatrix(i, colSecNomArea) = ""
'                .TextMatrix(i, colSecEsfuerzo) = ""
'                .TextMatrix(i, colSecFinicio) = ""
'                .TextMatrix(i, colSecFtermin) = ""
'            Else
'                .TextMatrix(i, colSecCodDir) = ClearNull(aplRST!sec_cod_direccion)
'                .TextMatrix(i, colSecCodGer) = ClearNull(aplRST!sec_cod_gerencia)
'                .TextMatrix(i, colSecCodSec) = ClearNull(aplRST!sec_cod_sector)
'                .TextMatrix(i, colSecCodSub) = ClearNull(aplRST!sec_cod_grupo)
'                .TextMatrix(i, colSecEsfuerzo) = ClearNull(aplRST!sec_horaspresup)
'                .TextMatrix(i, colSecFinicio) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'                .TextMatrix(i, colSecFtermin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'                If secNivel = "GRUP" Then
'                   .TextMatrix(i, colSecNomArea) = ClearNull(aplRST!sec_nom_sector) & " � " & ClearNull(aplRST!sec_nom_grupo)
'                Else
'                   .TextMatrix(i, colSecNomArea) = ClearNull(aplRST!sec_nom_sector)
'                End If
'                If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
'                   .TextMatrix(i, colPrioEjecuc) = ClearNull(aplRST!prio_ejecuc)
'                End If
'            End If
'            .TextMatrix(i, colSRTPETIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_ini_plan")), Format(aplRST.Fields.item("fe_ini_plan"), "yyyymmdd"), "")
'            .TextMatrix(i, colSRTPETFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_fin_plan")), Format(aplRST.Fields.item("fe_fin_plan"), "yyyymmdd"), "")
'            .TextMatrix(i, colSRTSECIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_ini_plan")), Format(aplRST.Fields.item("sec_fe_ini_plan"), "yyyymmdd"), "")
'            .TextMatrix(i, colSRTSECFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_fin_plan")), Format(aplRST.Fields.item("sec_fe_fin_plan"), "yyyymmdd"), "")
'            .TextMatrix(i, colPetClass) = Trim(aplRST!cod_clase)
'            .TextMatrix(i, colPetImpTech) = Trim(aplRST!pet_imptech)
'            .TextMatrix(i, colPetSOx) = IIf(aplRST!pet_sox001 = 1, 1, 0)    ' add -003- a.
'            .TextMatrix(i, colRegulatorio) = IIf(IsNull(aplRST!pet_regulatorio), "-", ClearNull(aplRST!pet_regulatorio))    ' add -008- a.
'            .TextMatrix(i, colPetHist) = "L"                                ' add -012- a.
'            'If i Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
'        End With
'        aplRST.MoveNext
'        DoEvents
'    Loop
'    Call ObtenerTiempo(False)
    
' ORIGINAL
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            If bGridOverflow Then
                Exit Do
            End If
            .TextMatrix(.Rows - 1, colEmpresa) = ClearNull(aplRST!pet_emp)  ' add -019- a.
            .TextMatrix(.Rows - 1, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
            .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, colCODESTADO) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!sec_nom_estado)
            .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!sec_nom_situacion)
            .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
            .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colEstadoFec) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
            .TextMatrix(.Rows - 1, colImportancia) = ClearNull(aplRST!importancia_nom)
            .TextMatrix(.Rows - 1, colCorpLocal) = ClearNull(aplRST!corp_local)
            .TextMatrix(.Rows - 1, colOrientacion) = ClearNull(aplRST!nom_orientacion)
            .TextMatrix(.Rows - 1, colSecSol) = ClearNull(aplRST!sol_sector)
            .TextMatrix(.Rows - 1, colPetRO) = ClearNull(aplRST!pet_riesgo)        ' add -021- a.
            If flgNoHijos = True Then
                .TextMatrix(.Rows - 1, colSecCodDir) = ""
                .TextMatrix(.Rows - 1, colSecCodGer) = ""
                .TextMatrix(.Rows - 1, colSecCodSec) = ""
                .TextMatrix(.Rows - 1, colSecCodSub) = ""
                .TextMatrix(.Rows - 1, colSecNomArea) = ""
                .TextMatrix(.Rows - 1, colSecEsfuerzo) = ""
                .TextMatrix(.Rows - 1, colSecFinicio) = ""
                .TextMatrix(.Rows - 1, colSecFtermin) = ""
            Else
                .TextMatrix(.Rows - 1, colSecCodDir) = ClearNull(aplRST!sec_cod_direccion)
                .TextMatrix(.Rows - 1, colSecCodGer) = ClearNull(aplRST!sec_cod_gerencia)
                .TextMatrix(.Rows - 1, colSecCodSec) = ClearNull(aplRST!sec_cod_sector)
                .TextMatrix(.Rows - 1, colSecCodSub) = ClearNull(aplRST!sec_cod_grupo)
                .TextMatrix(.Rows - 1, colSecEsfuerzo) = ClearNull(aplRST!sec_horaspresup)
                .TextMatrix(.Rows - 1, colSecFinicio) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colSecFtermin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
                If secNivel = "GRUP" Then
                   .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector) & " � " & ClearNull(aplRST!sec_nom_grupo)
                Else
                   .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector)
                End If
                If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                   .TextMatrix(.Rows - 1, colPrioEjecuc) = ClearNull(aplRST!prio_ejecuc)
                End If
            End If
            .TextMatrix(.Rows - 1, colSRTPETIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("fe_ini_plan")), Format(aplRST.Fields.Item("fe_ini_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colSRTPETFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("fe_fin_plan")), Format(aplRST.Fields.Item("fe_fin_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colSRTSECIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("sec_fe_ini_plan")), Format(aplRST.Fields.Item("sec_fe_ini_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colSRTSECFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("sec_fe_fin_plan")), Format(aplRST.Fields.Item("sec_fe_fin_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colPetClass) = IIf(ClearNull(aplRST!cod_clase) = "SINC", "-", ClearNull(aplRST!cod_clase))
            .TextMatrix(.Rows - 1, colPetImpTech) = ClearNull(aplRST!pet_imptech)
            '.TextMatrix(.Rows - 1, colPetSOx) = IIf(aplRST!pet_sox001 = 1, 1, 0)    ' add -003- a.
            .TextMatrix(.Rows - 1, colRegulatorio) = IIf(IsNull(aplRST!pet_regulatorio), "-", ClearNull(aplRST!pet_regulatorio))    ' add -008- a.
            .TextMatrix(.Rows - 1, colPetHist) = "L"                                ' add -012- a.
            .TextMatrix(.Rows - 1, colPetBP) = ClearNull(aplRST!cod_bpar)
            .TextMatrix(.Rows - 1, colPetCategoria) = ClearNull(aplRST!categoria)                               ' add -024- a.
            .TextMatrix(.Rows - 1, colPetPuntuacion) = Format(ClearNull(aplRST!puntuacion), "###0.000")         ' add -025- a.
            '{ add -027- a.
            .TextMatrix(.Rows - 1, colPetBPNom) = ClearNull(aplRST!nom_bpar)
            .TextMatrix(.Rows - 1, colPetRGP) = ClearNull(aplRST!cod_bpe)
            .TextMatrix(.Rows - 1, colPetRGPNom) = ClearNull(aplRST!nom_BPE)
            '}
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)                       ' add -028- b.
        End With
        aplRST.MoveNext
        DoEvents
    Loop
finx:
    grdDatos.visible = True    ' del -012- a.
    Call Puntero(False)
    '{ add -011- a.
    'Call Status("Listo." & IIf(lPeticiones_Listadas > 0, " " & lPeticiones_Listadas & " peticiones listadas.", ""))
    If Not bGridOverflow Then
        Me.Caption = Me.Tag & " (" & Trim(CStr(Format(lPeticiones_Listadas, "###,###,###"))) & " filas)"
        Call Status("Listo." & IIf(lPeticiones_Listadas > 0, " " & lPeticiones_Listadas & " peticiones listadas.", ""))
    End If
    LockProceso (False)
    cmdExportar.visible = False
    'cmdChgBP.Enabled = False
    'cmdAgrupar.Enabled = False
    'cmdPrintPet.Enabled = False
    cmdVisualizar.visible = True
    With grdDatos
        If grdDatos.Rows > 1 Then
            cmdExportar.visible = True
            If grdDatos.Rows = 2 Then
                glNumeroPeticion = .TextMatrix(1, colNroInterno)
                .TextMatrix(1, colMultiSelect) = "�"
            End If
            'glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
            'Call grdDatos_Click
        End If
    End With
    LockProceso (False)
    flgEnCarga = False
    '{ add -004- a.
    If bGridOverflow And bRegenerate Then
        cmdCerrar_Click
    End If
    '}
    Exit Sub
'{ add -004- a.
ErrHandler:
    Dim sErrMenssage As String
    
    Select Case Err.Number
        Case 30006      ' No queda memoria para seguir agregando filas al MSFlexGrid
            bGridOverflow = True
            sErrMenssage = "La consulta que intenta generar es demasiado grande (excede la cantidad de " & Trim(Format(lRecCount, "###,###,###")) & " filas)." & vbCrLf
            sErrMenssage = sErrMenssage & "�Desea volver a generar la consulta agregando restricciones para devolver una menor cantidad de filas?"
            If MsgBox(sErrMenssage, vbInformation + vbYesNo, "Consulta demasiado grande") = vbYes Then
                bRegenerate = True
                Resume Next
            Else
                bRegenerate = False
                MsgBox "IMPORTANTE" & vbCrLf & "La informaci�n mostrada en pantalla ser� parcial." & vbCrLf & "No se ha podido cargar todo el conjunto de resultados esperado.", vbExclamation + vbOKOnly, "Resultado parcial"
                Me.Caption = Me.Tag & " (" & Trim(CStr(Format(grdDatos.Rows - 1, "###,###,###"))) & " filas)"
                Call Status("Listo." & IIf(grdDatos.Rows - 1 > 1, " " & grdDatos.Rows - 1 & " peticiones listadas.", ""))
                Resume Next
            End If
        Case 3265       ' No se encontr� el elemento en la colecci�n que corresponde con el nombre o el ordinal pedido.
            Resume Next
        Case Else
            Call Puntero(False)
            MsgBox Err.DESCRIPTION & " (" & Err.Number & ")", vbExclamation + vbOKOnly
    End Select
'}
End Sub

''{ add -002- a.
'Private Sub grdDatos_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then
'        grdDatos_Click
'    End If
'End Sub
''}

Private Sub grdDatos_Click()
    bSorting = True
    With grdDatos
        glNumeroPeticion = .TextMatrix(.rowSel, colNroInterno)
    End With
    flgEnCarga = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    flgEnCarga = False
    bSorting = False
End Sub

'Private Sub grdDatos_Click()
'    With grdDatos
'        glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
'        'Debug.Print glNumeroPeticion & " (" & Now & ")"
'        'Text1 = glNumeroPeticion
'        If .MouseRow = 0 And .Rows > 1 Then
'           .RowSel = 1
'           .Col = .MouseCol
'           .Sort = flexSortStringNoCaseAscending
'        End If
'    End With
'    'Call MostrarSeleccion
'End Sub

Private Sub grdDatos_DblClick()
    'DoEvents
    If Not LockProceso(True) Then
        Exit Sub
    End If
    'Call MostrarSeleccion
    If glNumeroPeticion <> "" Then
        If grdDatos.TextMatrix(grdDatos.rowSel, colPetHist) <> "H" Then    ' add -012- b.
            'If Not LockProceso(True) Then
            '    Exit Sub
            'End If
            glModoPeticion = "VIEW"
            'On Error Resume Next
            frmPeticionesCarga.Show vbModal
            'Call LockProceso(False)
        '{ add -012- b.
        Else
            MsgBox "La petici�n se encuentra en los archivos hist�ricos." & vbCrLf & "No puede trabajar desde aqu� con esta petici�n.", vbInformation + vbOKOnly, "Petici�n en hist�ricos"
        End If
        '}
    End If
    Call LockProceso(False)
End Sub

Private Sub MostrarSeleccion(Optional flgSort)
    If Not flgEnCarga Then
        If IsMissing(flgSort) Then
            flgSort = True
        End If
'        cmdVisualizar.Enabled = False
'
'        With grdDatos
'            If .RowSel > 0 Then
'                If .TextMatrix(.RowSel, colNroInterno) <> "" Then
'                    'glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
'                    cmdVisualizar.Enabled = True
'                    If InPerfil("ADMI") Then
'                        cmdChgBP.Enabled = True
'                    End If
'                    cmdAgrupar.Enabled = True
'                    cmdPrintPet.Enabled = True
'                End If
'            End If
'        End With
'        Call LockProceso(False)
    End If
End Sub

'{ add -006- a.
Private Sub txtNroDesde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 22 Then 'paste(ctrl+ v)
        KeyAscii = 0
        txtNroDesde.text = Clipboard.GetText(vbCFText)
    End If
    If KeyAscii = 13 And Len(txtNroDesde) > 0 Then
        txtNroHasta.SetFocus
    End If
End Sub

Private Sub txtNroHasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(txtNroHasta) > 0 Then
        cmdConfirmar.SetFocus
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then
        cmdCerrar_Click
        flgEnCarga = Not flgEnCarga
    End If
End Sub
'}

Private Sub txtNroDesde_LostFocus()
    If Val(txtNroDesde) > 100000 Then
        txtNroDesde = ""
        Call Status("N�mero inv�lido")
        Exit Sub
    End If
    txtNroHasta.text = txtNroDesde.text
End Sub

Private Sub txtNroHasta_LostFocus()
    If Val(txtNroHasta) > 100000 Then
        txtNroHasta = ""
        Call Status("N�mero inv�lido")
        Exit Sub
    End If
End Sub

'Private Sub CrtPrn_Click(Index As Integer)
'    Select Case Index
'        Case 0: mdiPrincipal.CrystalReport1.Destination = crptToWindow
'        Case 1: mdiPrincipal.CrystalReport1.Destination = crptToPrinter
'    End Select
'End Sub

'{ add -014- b.
Private Sub optApertura_Click(Index As Integer)
    Select Case Index
        Case 0: lblOpcionesApertura.Caption = "Nivel de apertura: Peticiones"
        Case 1: lblOpcionesApertura.Caption = "Nivel de apertura: Sectores"
        Case 2: lblOpcionesApertura.Caption = "Nivel de apertura: Grupos"
    End Select
    verNivelSec
End Sub
'}

''{ add -015- a.
'Private Sub cmdMasFiltros_Click()
'    If frmGral.visible Then
'        fraOtrosFiltros.visible = True
'        frmGral.visible = False
'        cmdMasFiltros.Caption = "Menos filtros..."
'        flgQuery = "QRY2"
'    Else
'        fraOtrosFiltros.visible = False
'        frmGral.visible = True
'        cmdMasFiltros.Caption = "M�s filtros..."
'        flgQuery = "QRY1"
'    End If
'End Sub
''}

''{ add -015- a.
'Private Sub cmbTextos_Click()
'    If cmbTextos.ListIndex = 0 Then
'        If cmbOpcionesTexto.ListCount > 0 Then
'            cmbOpcionesTexto.ListIndex = 0
'            txtMEM_Texto = ""
'            Call setHabilCtrl(cmbOpcionesTexto, "DIS")
'            Call setHabilCtrl(txtMEM_Texto, "DIS")
'        End If
'    Else
'        Call setHabilCtrl(cmbOpcionesTexto, "NOR")
'        Call setHabilCtrl(txtMEM_Texto, "NOR")
'    End If
'End Sub
''}

'{ add -010- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}

' ******************************************************* FIN CODIGO ************************************************************

'    '****************************************************************************************************************
'    Parte hist�rica (1)
'    '****************************************************************************************************************
'    If chkHistorico.Value = 1 Then          ' add -012- b.
'        If Not sp_rptPeticion00h(txtNroDesde.Text, txtNroHasta.Text, petTipo, petPrioridad, txtDESDEalta.DateValue, txtHASTAalta.DateValue, petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
'                                CodigoCombo(Me.cboBpar, True), CodigoCombo(cboPeticionClase, True), UCase(Left(cboPeticionImpacto.Text, 1)), UCase(Left(cmbPetDocu.Text, 1)), CodigoCombo(cmbRegulatorio, True), IIf(UCase(Left(cmbPetDocu, 1)) = "C", UCase(Left(cmbDocumentoAdjunto, 4)), Null), txtPrjId, txtPrjSubId, txtPrjSubsId) Then   ' upd -013- a.    ' upd -015- a. Se agregan los tres campos de proyecto IDM.
'        End If
'        lPeticiones_Listadas = lPeticiones_Listadas + aplRST.RecordCount   ' add -011- a.
'        Do While Not aplRST.EOF
'            With grdDatos
'                .Rows = .Rows + 1
'                .TextMatrix(.Rows - 1, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
'                .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
'                .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!titulo)
'                .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
'                .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
'                .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!sec_nom_estado)
'                .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!sec_nom_situacion)
'                .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
'                .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
'                .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
'                .TextMatrix(.Rows - 1, colEstadoFec) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
'                .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
'                .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
'                .TextMatrix(.Rows - 1, colImportancia) = ClearNull(aplRST!importancia_nom)
'                .TextMatrix(.Rows - 1, colCorpLocal) = ClearNull(aplRST!corp_local)
'                .TextMatrix(.Rows - 1, colOrientacion) = ClearNull(aplRST!nom_orientacion)
'                .TextMatrix(.Rows - 1, colSecSol) = ClearNull(aplRST!sol_sector)
'                If flgNoHijos = True Then
'                    .TextMatrix(.Rows - 1, colSecCodDir) = ""
'                    .TextMatrix(.Rows - 1, colSecCodGer) = ""
'                    .TextMatrix(.Rows - 1, colSecCodSec) = ""
'                    .TextMatrix(.Rows - 1, colSecCodSub) = ""
'                    .TextMatrix(.Rows - 1, colSecNomArea) = ""
'                    .TextMatrix(.Rows - 1, colSecEsfuerzo) = ""
'                    .TextMatrix(.Rows - 1, colSecFinicio) = ""
'                    .TextMatrix(.Rows - 1, colSecFtermin) = ""
'                Else
'                    .TextMatrix(.Rows - 1, colSecCodDir) = ClearNull(aplRST!sec_cod_direccion)
'                    .TextMatrix(.Rows - 1, colSecCodGer) = ClearNull(aplRST!sec_cod_gerencia)
'                    .TextMatrix(.Rows - 1, colSecCodSec) = ClearNull(aplRST!sec_cod_sector)
'                    .TextMatrix(.Rows - 1, colSecCodSub) = ClearNull(aplRST!sec_cod_grupo)
'                    .TextMatrix(.Rows - 1, colSecEsfuerzo) = ClearNull(aplRST!sec_horaspresup)
'                    .TextMatrix(.Rows - 1, colSecFinicio) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'                    .TextMatrix(.Rows - 1, colSecFtermin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'                    If secNivel = "GRUP" Then
'                       .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector) & " � " & ClearNull(aplRST!sec_nom_grupo)
'                    Else
'                       .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector)
'                    End If
'                    If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
'                       .TextMatrix(.Rows - 1, colPrioEjecuc) = ClearNull(aplRST!prio_ejecuc)
'                    End If
'                End If
'                .TextMatrix(.Rows - 1, colSRTPETIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_ini_plan")), Format(aplRST.Fields.item("fe_ini_plan"), "yyyymmdd"), "")
'                .TextMatrix(.Rows - 1, colSRTPETFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_fin_plan")), Format(aplRST.Fields.item("fe_fin_plan"), "yyyymmdd"), "")
'                .TextMatrix(.Rows - 1, colSRTSECIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_ini_plan")), Format(aplRST.Fields.item("sec_fe_ini_plan"), "yyyymmdd"), "")
'                .TextMatrix(.Rows - 1, colSRTSECFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_fin_plan")), Format(aplRST.Fields.item("sec_fe_fin_plan"), "yyyymmdd"), "")
'                .TextMatrix(.Rows - 1, colPetClass) = Trim(aplRST!cod_clase)
'                .TextMatrix(.Rows - 1, colPetImpTech) = Trim(aplRST!pet_imptech)
'                .TextMatrix(.Rows - 1, colPetSOx) = IIf(aplRST!pet_sox001 = 1, 1, 0)
'                .TextMatrix(.Rows - 1, colRegulatorio) = IIf(IsNull(aplRST!pet_regulatorio), "-", ClearNull(aplRST!pet_regulatorio))
'                .TextMatrix(.Rows - 1, colPetHist) = "H"    ' add -012- a.
'                CambiarForeColorLinea grdDatos, prmGridFillRowColorDarkGreen2
'            End With
'            aplRST.MoveNext
'        Loop
'    End If
'    '}

'' Proceso que baja documentaci�n a disco de peticiones asociadas a proyectos idm
'Private Sub Command1_Click()
'    Dim pProjId(6) As Long
'    Dim pProjSubId(6) As Long
'    Dim pProjSubsId(6) As Long
'
'    Dim AdjTipo As String
'    Dim AdjNombre As String
'
'    Dim pet_nrointerno() As Long
'    Dim pet_nroasignado() As Long
'    Dim pet_proyecto() As String
'    Dim cProyStr As String
'
'    Dim i As Integer, j As Integer
'    Dim wrkDir As String
'
'    wrkDir = glWRKDIR & "Icaro\"
'
'    Call Status("Procesando...")
'    Call Puntero(True)
'
'    pProjId(0) = 7
'    pProjSubId(0) = 3
'    pProjSubsId(0) = 0
'
'    pProjId(1) = 7
'    pProjSubId(1) = 3
'    pProjSubsId(1) = 1
'
'    pProjId(2) = 7
'    pProjSubId(2) = 3
'    pProjSubsId(2) = 2
'
'    pProjId(3) = 7
'    pProjSubId(3) = 3
'    pProjSubsId(3) = 3
'
'    pProjId(4) = 7
'    pProjSubId(4) = 3
'    pProjSubsId(4) = 5
'
'    pProjId(5) = 7
'    pProjSubId(5) = 3
'    pProjSubsId(5) = 6
'
'    ' Guardo en un vector las peticiones de cada proyecto
'    For i = 0 To UBound(pProjId) - 1
'        If sp_GetProyIDMPet(pProjId(i), pProjSubId(i), pProjSubsId(i)) Then
'            Debug.Print pProjId(i) & "." & pProjSubId(i) & "." & pProjSubsId(i) & ": " & aplRST.RecordCount & " peticiones."
'            ' Creo un directorio para el proyecto
'            cProyStr = pProjId(i) & "." & pProjSubId(i) & "." & pProjSubsId(i)
'            If Not EstadoDeArchivo(wrkDir & cProyStr) Then MkDir (wrkDir & cProyStr)
'
'            Do While Not aplRST.EOF
'                ReDim Preserve pet_nrointerno(j)
'                ReDim Preserve pet_nroasignado(j)
'                ReDim Preserve pet_proyecto(j)
'                pet_nrointerno(j) = aplRST.Fields!pet_nrointerno
'                pet_nroasignado(j) = aplRST.Fields!pet_nroasignado
'                pet_proyecto(j) = CStr(pProjId(i) & "." & pProjSubId(i) & "." & pProjSubsId(i))
'                aplRST.MoveNext
'                DoEvents
'                j = j + 1
'            Loop
'        End If
'    Next i
'
'    ' Iteramos el vector de peticiones bajando a disco los documentos
'    For i = 0 To j - 1
'        If sp_GetAdjuntosPet(pet_nrointerno(i), "P950", Null, Null) Then
'            Do While Not aplRST.EOF
'                AdjTipo = ClearNull(aplRST.Fields!adj_tipo)
'                AdjNombre = ClearNull(aplRST.Fields!adj_file)
'                If Not EstadoDeArchivo(wrkDir & pet_proyecto(i) & "\" & pet_nroasignado(i)) Then MkDir (wrkDir & pet_proyecto(i) & "\" & pet_nroasignado(i) & "\")
'                Call PeticionAdjunto2File(pet_nrointerno(i), AdjTipo, AdjNombre, wrkDir & pet_proyecto(i) & "\" & pet_nroasignado(i) & "\")
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        Else
'            If sp_GetAdjuntosPet(pet_nrointerno(i), "C100", Null, Null) Then
'                Do While Not aplRST.EOF
'                    AdjTipo = ClearNull(aplRST.Fields!adj_tipo)
'                    AdjNombre = ClearNull(aplRST.Fields!adj_file)
'                    If Not EstadoDeArchivo(wrkDir & pet_proyecto(i) & "\" & pet_nroasignado(i)) Then MkDir (wrkDir & pet_proyecto(i) & "\" & pet_nroasignado(i) & "\")
'                    Call PeticionAdjunto2File(pet_nrointerno(i), AdjTipo, AdjNombre, wrkDir & pet_proyecto(i) & "\" & pet_nroasignado(i) & "\")
'                    aplRST.MoveNext
'                    DoEvents
'                Loop
'            End If
'        End If
'        DoEvents
'    Next i
'    Call Status("Listo.")
'    Call Puntero(False)
'    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
'End Sub

Private Sub CancelarPeticiones()
    'Const PROCESO_SISTEMA = "JP00"
    Const PROCESO_SISTEMA = "OdeGesPet"
    Const ESTADO_CANCELADO = "CANCEL"
    Const ESTADO_FINALIZADO = "CANCEL"
    
    ' Seg�n el estado de la petici�n:
    ' - Si el estado es terminar, no se hace nada
    ' - Si el estado es activo, entonces la petici�n se cancela (CANCELADO).
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sSolicitante As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long
    Dim sGrupoHomologador As String

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno

    flgPruebaDeResultado = False             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Debug.Print "INICIANDO EL PROCESO: " & Now
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            'vPeticionNroInterno = CargarArchivo(sFile)              ' Nros. internos
            'TextoHistorialPeticion = "Esta finalizaci�n/cancelaci�n es producto de un proceso autom�tico ya que no se hizo oportunamente por el ref. de sistemas."
            'TextoHistorialGrupo = "Esta finalizaci�n/cancelaci�n es producto de un proceso autom�tico ya que no se hizo oportunamente por el ref. de sistemas."
            'TextoHistorialPeticion = "Se cancela la petici�n por obsoleta. Proceso de depuraci�n de ambiente desarrollo."
            'TextoHistorialGrupo = "Se cancela la petici�n por obsoleta. Proceso de depuraci�n de ambiente desarrollo."
            TextoHistorialPeticion = "Se cancela la petici�n por obsoleta."
            TextoHistorialGrupo = "Se cancela la petici�n por obsoleta."
            flgBorrarPlanificacion = True

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
            'For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)
                'ReDim Preserve vPeticionNroAsignado(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                'If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    'vPeticionNroAsignado(i) = ClearNull(aplRST.Fields!pet_nroasignado)
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            flgContinuar = True
            
            ' Proceso principal de cancelaci�n de peticiones por inactividad
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    J = 0
                    ' Obtengo los grupos activos de la petici�n (incluso el grupo homologador)
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                flgTieneGrupo = True
                                ReDim Preserve vPeticionSectorId(J)
                                ReDim Preserve vPeticionGrupoId(J)
                                vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                J = J + 1
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If

                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                    End If

                    If flgTieneGrupo Then
                        ' Por cada grupo activo realizo la cancelaci�n del mismo y registro los cambios en el historial
                        For J = 0 To UBound(vPeticionGrupoId)
                            If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                                EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                            End If
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "ESTADO", IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), Null, Null)
                            Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                            If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                            Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                                Call chkSectorOpiEva(vPeticionNroInterno(i))
                            End If
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                            NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                            If NuevoEstadoPeticion <> EstadoPeticion Then
                                If NuevoEstadoPeticion <> "CANCEL" Then NuevoEstadoPeticion = "CANCEL"
                                Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                            End If
                        Next J
                    Else
                        'Call sp_UpdatePetSecField(vPeticionNroInterno(i), "148", "ESTADO", "CANCEL", Null, Null)
                        ' La petici�n no tiene grupos activos
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                            Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), Null, Null)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "NROANEX", "", Null, 0)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Now, Null)
                            Call sp_ChgEstadoSector(vPeticionNroInterno(i), Null, IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO))
                            Call sp_ChgEstadoGrupo(vPeticionNroInterno(i), Null, Null, IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO))
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                        Else
                            Debug.Print "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal."
                        End If
                    End If
                    Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                    Call sp_DeletePeticionChangeMan(vPeticionNroInterno(i))               ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                    If flgBorrarPlanificacion Then Call sp_DeletePeticionPlancab(per_nrointerno, vPeticionNroInterno(i))
                    DoEvents
                Next i
            End If

            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
        Debug.Print "PROCESO FINALIZADO: " & Now
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub FinalizarPeticiones()
    'Const PROCESO_SISTEMA = "JP00"
    Const PROCESO_SISTEMA = "OdeGesPet"
    Const ESTADO_CANCELADO = "CANCEL"
    'Const ESTADO_FINALIZADO = "TERMIN"
    Const ESTADO_FINALIZADO = "CANCEL"
    
    ' Seg�n el estado de la petici�n:
    ' - Si el estado es Suspendida, la petici�n se Cancela.
    ' - Si el estado es otro (activo), entonces la petici�n se Finaliza.
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sSolicitante As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long
    Dim sGrupoHomologador As String

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno

    flgPruebaDeResultado = False             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Debug.Print "INICIANDO EL PROCESO: " & Now
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            'vPeticionNroInterno = CargarArchivo(sFile)              ' Nros. internos
            'TextoHistorialPeticion = "Esta finalizaci�n/cancelaci�n es producto de un proceso autom�tico ya que no se hizo oportunamente por el ref. de sistemas."
            'TextoHistorialGrupo = "Esta finalizaci�n/cancelaci�n es producto de un proceso autom�tico ya que no se hizo oportunamente por el ref. de sistemas."
            TextoHistorialPeticion = "Se cancela la petici�n por obsoleta. Proceso de depuraci�n de ambiente desarrollo."
            TextoHistorialGrupo = "Se cancela la petici�n por obsoleta. Proceso de depuraci�n de ambiente desarrollo."
            flgBorrarPlanificacion = True

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
            'For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)
                'ReDim Preserve vPeticionNroAsignado(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                'If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    'vPeticionNroAsignado(i) = ClearNull(aplRST.Fields!pet_nroasignado)
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            flgContinuar = True
            
            ' Proceso principal de cancelaci�n de peticiones por inactividad
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    J = 0
                    ' Obtengo los grupos activos de la petici�n (incluso el grupo homologador)
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                flgTieneGrupo = True
                                ReDim Preserve vPeticionSectorId(J)
                                ReDim Preserve vPeticionGrupoId(J)
                                vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                J = J + 1
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If

                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                        'sSolicitante = ClearNull(aplRST.Fields!cod_solicitante)
                    End If

                    If flgTieneGrupo Then
                        ' Por cada grupo activo realizo la cancelaci�n del mismo y registro los cambios en el historial
                        For J = 0 To UBound(vPeticionGrupoId)
                            If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                                EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                            End If
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "ESTADO", IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), Null, Null)
                            Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                            If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                            Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                                Call chkSectorOpiEva(vPeticionNroInterno(i))
                            End If
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                            NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                            If NuevoEstadoPeticion <> EstadoPeticion Then
                                'If NuevoEstadoPeticion <> "CANCEL" Then NuevoEstadoPeticion = "CANCEL"
                                If (NuevoEstadoPeticion <> "CANCEL" And EstadoPeticion = "SUSPEN") Then NuevoEstadoPeticion = "CANCEL"
                                Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                            End If
                        Next J
                    Else
                        ' La petici�n no tiene grupos activos
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                            Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), Null, Null)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "NROANEX", "", Null, 0)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Now, Null)
                            Call sp_ChgEstadoSector(vPeticionNroInterno(i), Null, IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO))
                            Call sp_ChgEstadoGrupo(vPeticionNroInterno(i), Null, Null, IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO))
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", IIf(EstadoPeticion = "SUSPEN", ESTADO_CANCELADO, ESTADO_FINALIZADO), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                        Else
                            Debug.Print "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal."
                        End If
                    End If
                    Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                    Call sp_DeletePeticionChangeMan(vPeticionNroInterno(i))               ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                    If flgBorrarPlanificacion Then Call sp_DeletePeticionPlancab(per_nrointerno, vPeticionNroInterno(i))
                    DoEvents
                Next i
            End If

            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
        Debug.Print "PROCESO FINALIZADO: " & Now
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub AsignarPeticionA(sLegajo, sNombre, PeticionNroAsignado)
    Dim i As Long, J As Long
    Dim flgNoExiste As Boolean

    flgNoExiste = True
    For i = 0 To UBound(Destinatarios)
        If Destinatarios(i).Legajo = sLegajo Then
            Destinatarios(i).peticiones.Add (PeticionNroAsignado)
            flgNoExiste = False
            Exit For
        End If
    Next i

    If flgNoExiste Then
        J = UBound(Destinatarios) + 1
        ReDim Preserve Destinatarios(J)
        Destinatarios(J).Legajo = sLegajo
        Destinatarios(J).Nombre = sNombre
        Destinatarios(J).peticiones.Add (PeticionNroAsignado)
    End If
End Sub

Private Sub FinalizarGrupoHomologador()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vEstadoPeticion() As String
    Dim sFile As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim TotalPeticiones As Long
    Dim sGrupoHomologador As String
    Dim sSectorHomologador As String
    Dim flgContinuar As Boolean
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno
    Erase vEstadoPeticion

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
        sSectorHomologador = ClearNull(aplRST.Fields!cod_sector)
    End If

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            'vPeticionNroAsignado = CargarArchivo(sFile)
            vPeticionNroInterno = CargarArchivo(sFile)
            TextoHistorialGrupo = "El grupo fue Cancelado (Petici�n cancelada por proceso de depuraci�n)."

            'For i = 0 To UBound(vPeticionNroAsignado)
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1)
                'ReDim Preserve vPeticionNroInterno(i)
                ReDim Preserve vPeticionNroAsignado(i)
                ReDim Preserve vEstadoPeticion(i)
                If sp_GetUnaPeticionAsig(vPeticionNroInterno(i)) Then
                    vPeticionNroAsignado(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                    vEstadoPeticion(i) = ClearNull(aplRST.Fields!cod_estado)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            flgContinuar = True

            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), sSectorHomologador, sGrupoHomologador) Then
                        Call Status("Cancelando grupo ... (" & i + 1 & " de " & TotalPeticiones & ")")
                        Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", sGrupoHomologador)
                        Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", sSectorHomologador)
                        Call sp_UpdatePetSubField(vPeticionNroInterno(i), sGrupoHomologador, "ESTADO", "CANCEL", Null, Null)
                        Call getIntegracionGrupo(vPeticionNroInterno(i), sSectorHomologador, Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                        If getNuevoEstadoSector(vPeticionNroInterno(i), sSectorHomologador) = "CANCEL" Then
                            Call sp_UpdatePetSecField(vPeticionNroInterno(i), sSectorHomologador, "ESTADO", "CANCEL", Null, Null)
                        End If
                    End If
                    DoEvents
                Next i
            End If
            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Erase vEstadoPeticion
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        End If
    End If
End Sub

Private Sub CargarProyecto()
    Dim ExcelApp, ExcelWrk, xlSheet As Object
    Dim xRow, iRow As Long

    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.visible = False
    On Error Resume Next
    Set ExcelWrk = ExcelApp.Workbooks.Open("C:\trabajo\cgm\Peticion con proyectos en PROD.xls")    ' upd -007- a.
    If ExcelWrk Is Nothing Then
        MsgBox ("Error al abrir planilla")
        Exit Sub
    End If
    Set xlSheet = ExcelWrk.Sheets("Hoja1")
    If xlSheet Is Nothing Then
        ExcelWrk.Close
        ExcelApp.Application.Quit
        MsgBox ("Error, hoja Hoja1 inexistente.")
        Exit Sub
    End If
    xRow = 1
   
    On Error GoTo 0

    xRow = xRow + 1
    iRow = 0
    
    Call Puntero(True)
    While ClearNull(xlSheet.Cells(xRow, 1)) <> ""       ' Nro. interno de petici�n
        iRow = iRow + 1
        Call Status("Proc: " & iRow)
        Call sp_UpdatePetField(CLng(ClearNull(xlSheet.Cells(xRow, 1))), "pet_projid", Null, Null, CLng(ClearNull(xlSheet.Cells(xRow, 4))))
        Call sp_UpdatePetField(CLng(ClearNull(xlSheet.Cells(xRow, 1))), "pet_projsubid", Null, Null, CLng(ClearNull(xlSheet.Cells(xRow, 5))))
        Call sp_UpdatePetField(CLng(ClearNull(xlSheet.Cells(xRow, 1))), "pet_projsubsid", Null, Null, CLng(ClearNull(xlSheet.Cells(xRow, 6))))
        DoEvents
        xRow = xRow + 1
    Wend
    
    Call Puntero(False)
    Call Status("Listo.")
    ExcelWrk.Save
    ExcelWrk.Close
    ExcelApp.Application.Quit
    MsgBox "Proceso finalizado."
End Sub

Private Sub BorrarProyecto()
    ' Este proceso actualiza los datos de peticion blanqueando el codigo de proyecto idm
    Dim MiCadena, MiN�mero
    Open App.Path & "\lista de proyectos.txt" For Input As #1                   ' Abre el archivo para recibir los datos.
    Do While Not EOF(1)                             ' Repite el bucle hasta el final del archivo.
       Input #1, MiCadena ', MiN�mero                 ' Lee el car�cter en dos variables.
       'Debug.Print MiCadena ', MiN�mero               ' Imprime datos en la ventana Inmediato.
       If sp_GetUnaPeticion(CLng(MiCadena)) Then
            Debug.Print "Nro asignado: " & aplRST!pet_nroasignado & " (" & Trim(MiCadena) & ")"
            Call sp_UpdatePeticion(aplRST!pet_nrointerno, aplRST!pet_nroasignado, aplRST!Titulo, aplRST!cod_tipo_peticion, aplRST!prioridad, aplRST!corp_local, aplRST!cod_orientacion, aplRST!importancia_cod, _
                            aplRST!importancia_prf, aplRST!importancia_usr, aplRST!pet_nroanexada, aplRST!prj_nrointerno, aplRST!fe_pedido, aplRST!fe_requerida, aplRST!fe_comite, aplRST!fe_ini_plan, aplRST!fe_fin_plan, _
                            aplRST!fe_ini_real, aplRST!fe_fin_real, aplRST!horaspresup, aplRST!cod_direccion, aplRST!cod_gerencia, aplRST!cod_sector, aplRST!cod_usualta, aplRST!cod_solicitante, aplRST!cod_referente, _
                            aplRST!cod_supervisor, aplRST!cod_director, aplRST!cod_estado, aplRST!fe_estado, aplRST!cod_situacion, aplRST!cod_bpar, aplRST!cod_clase, aplRST!pet_imptech, aplRST!pet_sox001, aplRST!pet_regulatorio, _
                            0, 0, 0, aplRST!pet_emp, aplRST!pet_ro, aplRST!cod_bpe, aplRST!fecha_BPE, aplRST!pet_driver, aplRST!cargaBeneficios)
       End If
       DoEvents
       MiN�mero = MiN�mero + 1
       Call Status(CStr(MiN�mero))
    Loop
    Close #1   ' Cierra el archivo.
    MsgBox "Proceso finalizado"
End Sub

Private Sub Depuracion_Solicitante()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String
    Dim vAdjuntoTipo() As String
    Dim vAdjuntoFile() As String

    Dim sFile As String

    Dim sGerencia As String
    Dim sSector As String
    Dim sSolicitante As String
    Dim sTipo As String
    Dim sClase As String
    Dim sRegulatorio As String
    Dim sImpacto As String
    Dim sGestiona As String
    Dim sReferenteBPE As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim NroHistorial As Long
    Dim TotalPeticiones As Long
        
    Dim flgTieneSolicitante As Boolean
    Dim flgTieneSector As Boolean
    Dim flgTieneGrupo As Boolean
    Dim flgAbortar As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sBeneficios As String
    Dim sObservaciones As String
    Dim sMensajeErrores As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno
    Erase vPeticionSectorId
    Erase vPeticionGrupoId
    Erase vAdjuntoTipo
    Erase vAdjuntoFile

    flgPruebaDeResultado = False                ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.
    
    sMensajeErrores = ""                        ' Mensaje para el log de errores

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            'vPeticionNroAsignado = CargarArchivo(sFile)
            vPeticionNroInterno = CargarArchivo(sFile)
            TextoHistorialPeticion = "Petici�n devuelta al Solicitante mediante proceso autom�tico."
            flgContinuar = False
            
            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroInterno)
                flgContinuar = True
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroAsignado(i)

                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    vPeticionNroAsignado(i) = ClearNull(aplRST.Fields!pet_nroasignado)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            'flgContinuar = True
            
            ' Proceso principal de devoluci�n de peticiones al Solicitante
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    sMensajeErrores = "Pet. " & vPeticionNroAsignado(i) & ":"
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    Erase vAdjuntoTipo
                    Erase vAdjuntoFile
                    J = 0
                    
                    ' Obtengo datos generales de la petici�n
                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                        sGerencia = ClearNull(aplRST.Fields!cod_gerencia)               ' Gerencia solicitante
                        sSector = ClearNull(aplRST.Fields!cod_sector)                   ' Sector solicitante
                        sSolicitante = ClearNull(aplRST.Fields!cod_solicitante)
                        sTipo = ClearNull(aplRST.Fields!cod_tipo_peticion)
                        sClase = ClearNull(aplRST.Fields!cod_clase)
                        sRegulatorio = ClearNull(aplRST.Fields!pet_regulatorio)
                        sImpacto = ClearNull(aplRST.Fields!pet_imptech)
                    End If
                    
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                        flgAbortar = False
                        
                        ' Ya est� en estado "En confecci�n" o "Devuelto al solicitante"
                        If InStr(1, "CONFEC|DEVSOL|", EstadoPeticion, vbTextCompare) > 0 Then
                            sMensajeErrores = sMensajeErrores & vbCrLf & vbTab & "- Ya en estado <En confecci�n> o <Devuelto al solicitante>."
                            flgAbortar = True
                        End If
                        
                        ' Verifico el solicitante
                        flgTieneSolicitante = False
                        If sp_GetRecursoPerfil(sSolicitante, "SOLI") Then
                            flgTieneSolicitante = True
                        End If
                        
                        Select Case sTipo
                            Case "ESP"
                                If Not flgTieneSolicitante Then
                                    sMensajeErrores = sMensajeErrores & vbCrLf & vbTab & "- No existe Solicitante para asignar (" & sTipo & ")."
                                    flgAbortar = True
                                End If
                            Case Else       ' NOR, PRJ
                                If Not flgTieneSolicitante Then
                                    sMensajeErrores = sMensajeErrores & vbCrLf & vbTab & "- No existe Solicitante para asignar (" & sTipo & ")."
                                    flgAbortar = True
                                End If
                        End Select
                        
                        If flgAbortar Then GoTo saltear:
                    
                        ' Obtengo TODOS los grupos de la petici�n
                        If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                            Do While Not aplRST.EOF
                                flgTieneGrupo = True
                                ReDim Preserve vPeticionSectorId(J)
                                ReDim Preserve vPeticionGrupoId(J)
                                vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                J = J + 1
                                aplRST.MoveNext
                                DoEvents
                            Loop
                        End If
    
                        If flgTieneGrupo Then
                            ' Borro cada grupo sin realizar el registro en el historial
                            For J = 0 To UBound(vPeticionGrupoId)
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), vPeticionGrupoId(J))
                                Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Next J
                        End If
                            
                            ' Obtengo TODOS los sectores de la petici�n
                            Erase vPeticionSectorId
                            flgTieneSector = False
                            If sp_GetPeticionSector(vPeticionNroInterno(i), Null) Then
                                J = 0
                                Do While Not aplRST.EOF
                                    flgTieneSector = True
                                    ReDim Preserve vPeticionSectorId(J)
                                    vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                    J = J + 1
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                            
                            ' Borro cada sector sin realizar el registro en el historial
                            If flgTieneSector Then
                                For J = 0 To UBound(vPeticionSectorId)
                                    Call sp_DeletePeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                                    Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                                Next J
                            End If
                        'End If
                        
                        ' Inicializo la clase, regulatorio e impacto tecnol�gico
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "TIPO", "NOR", Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "CLASE", "SINC", Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "IMPACTO", "-", Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "REGULATORIO", "-", Null, Null)
                        ' TODO: falta Visibilidad, RO, Proyecto
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "IMPOCOD", Null, Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "RIESGO", Null, Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "pet_projid", Null, Null, 0)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "pet_projsubid", Null, Null, 0)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "pet_projsubsid", Null, Null, 0)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "BENEFICIOS", "S", Null, 0)
                        
                        ' Inicializo el atributo Orientaci�n
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "ORIENTACION", "", Null, Null)
                        
                        ' Asignamos el referente de BPE
                        If sp_GetSector(sSector, Null, Null) Then
                            sGestiona = ClearNull(aplRST.Fields!cod_perfil)
                            sReferenteBPE = ClearNull(aplRST.Fields!cod_bpar)
                        End If
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "REFBPE", sReferenteBPE, Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "GESTION", IIf(sGestiona = "GBPE", "BPE", "DESA"), Null, Null)
                        
                        ' Borramos el referente, supervisor y autorizante
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "CHKREFE", "", Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "CHKSUPE", "", Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "CHKAUTO", "", Null, Null)
                        
                        ' Borramos el referente de sistema
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "BPARTNE", "", Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHACOM", Null, Null, Null)      ' TODO: probar esto
                        
                        ' Borramos todas las fechas de planificaci�n y/o ejecuci�n que tuviera
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAIRL", Null, Null, Null)      ' TODO: probar esto
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Null, Null)      ' TODO: probar esto
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAIPN", Null, Null, Null)      ' TODO: probar esto
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFPN", Null, Null, Null)      ' TODO: probar esto
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "HORASPN", Null, Null, 0)
                        
                        ' Borro todos los informes de homologaci�n que pudiera tener
                        Call sp_DeletePeticionInformes(vPeticionNroInterno(i), Null, Null)
                        
                        ' Borro todos los conformes de la petici�n
                        Call sp_DeletePeticionConf(vPeticionNroInterno(i), Null, Null, Null)
                        
                        ' Reemplazo el tipo de todos los documentos adjuntos forz�ndolo a "OTRO"
                        If sp_GetAdjuntosPet(vPeticionNroInterno(i), Null, Null, Null) Then
                            J = 0
                            Do While Not aplRST.EOF
                                ReDim Preserve vAdjuntoTipo(J)
                                ReDim Preserve vAdjuntoFile(J)
                                vAdjuntoTipo(J) = ClearNull(aplRST.Fields!adj_tipo)
                                vAdjuntoFile(J) = ClearNull(aplRST.Fields!adj_file)
                                aplRST.MoveNext
                                J = J + 1
                                DoEvents
                            Loop
                            For J = 0 To UBound(vAdjuntoFile)
                                Call sp_UpdatePeticionAdjunto(vPeticionNroInterno(i), vAdjuntoTipo(J), vAdjuntoFile(J), "Cambio de tipo autom�tico (petici�n devuelta al Solicitante mediante proceso autom�tico.)", "OTRO", vAdjuntoFile(J))
                            Next J
                        End If
                        
                        ' Modifico los textos de la petici�n (paso lo que hay en "Beneficios esperados" a "Observaciones"
                        sBeneficios = sp_GetMemo(vPeticionNroInterno(i), "BENEFICIOS")
                        sObservaciones = sp_GetMemo(vPeticionNroInterno(i), "OBSERVACIO")
                        If ClearNull(sBeneficios) <> "" Then
                            sObservaciones = IIf(ClearNull(sObservaciones) = "", "", sObservaciones & vbCrLf & vbCrLf) & "BENEFICIOS ESPERADOS" & vbCrLf & vbCrLf & ClearNull(sBeneficios)
                            Call sp_UpdateMemo(vPeticionNroInterno(i), "OBSERVACIO", sObservaciones)
                        End If
                        
                        ' Actualizo el estado de la petici�n a "Devuelto al Solicitante"
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", "DEVSOL", Null, Null)
                        Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAESTADO", Null, Now, Null)
                        NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", "DEVSOL", "", "", "", "", PROCESO_SISTEMA, TextoHistorialPeticion, PROCESO_SISTEMA)
                        Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)                        'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                        ' Deshabilita para pasajes a producci�n y quita de la planificaci�n
                        Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                        Call sp_DeletePeticionChangeMan(vPeticionNroInterno(i))                 ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                        Call sp_DeletePeticionPlancab(Null, vPeticionNroInterno(i))
                        sMensajeErrores = sMensajeErrores & vbCrLf & vbTab & "- Proceso OK."
                        Call sp_InsAgrupPetic(1575, vPeticionNroInterno(i))                     ' Agrego la petici�n al agrupamiento de peticiones a devolver
                    Else
                        sMensajeErrores = sMensajeErrores & vbCrLf & vbTab & "- En estado terminal."
                        flgAbortar = True
                    End If
                    DoEvents
saltear:
                Call doLog(sMensajeErrores)
                Next i
            End If
            ' Inicializaci�n para la pr�xima petici�n
            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Erase vPeticionSectorId
            Erase vPeticionGrupoId
            Erase vAdjuntoTipo
            Erase vAdjuntoFile
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub Depuracion_Cancelacion()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sSolicitante As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long
    Dim sGrupoHomologador As String

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno

    flgPruebaDeResultado = False             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            'vPeticionNroAsignado = CargarArchivo(sFile)
            vPeticionNroInterno = CargarArchivo(sFile)
            TextoHistorialPeticion = "Petici�n cancelada por proceso de depuraci�n."
            TextoHistorialGrupo = "El grupo fue Cancelado (Petici�n cancelada por proceso de depuraci�n)."

            ' Se elimina de la planificaci�n
            flgBorrarPlanificacion = False
            If sp_GetPeriodo(Null, Null) Then                                             ' Obtengo los datos del per�odo vigente
                Do While Not aplRST.EOF
                    If ClearNull(aplRST.Fields!vigente) = "S" Then
                        per_nrointerno = ClearNull(aplRST.Fields!per_nrointerno)
                        per_estado = ClearNull(aplRST.Fields!per_estado)
                        Exit Do
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            If InStr(1, "PLAN10|PLAN20|", per_estado, vbTextCompare) > 0 Then       ' Si a�n el per�odo no alcanz� la 3ra. o 4ta. instancia, se elimina de la planificaci�n
                flgBorrarPlanificacion = True
            End If

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroAsignado(i)

                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    vPeticionNroAsignado(i) = ClearNull(aplRST.Fields!pet_nroasignado)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            ' Arma los mensajes de correo para los solicitantes
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Asignando peticiones a solicitantes... " & i)
                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    sSolicitante = ClearNull(aplRST.Fields!cod_solicitante)             ' Obtengo el solicitante de la petici�n
                    If flgPruebaDeResultado Then sMensaje2 = sMensaje2 & sSolicitante & vbTab & vPeticionNroAsignado(i) & vbCrLf
                    If i = 0 Then
                        ReDim Preserve Destinatarios(i)
                        Destinatarios(i).Legajo = sSolicitante
                        Destinatarios(i).peticiones.Add (vPeticionNroAsignado(i))
                    Else
                        Call AsignarPeticionA(sSolicitante, "", vPeticionNroAsignado(i))
                    End If
                End If
                DoEvents
            Next i

            ' Carga los datos de mail y habilitaci�n por solicitante / Arma un log
            For i = 0 To UBound(Destinatarios)
                If sp_GetRecurso(Destinatarios(i).Legajo, "R") Then
                    Destinatarios(i).Habilitado = IIf(ClearNull(aplRST.Fields!euser) = "S", True, False)
                    Destinatarios(i).email = ClearNull(aplRST.Fields!email)
                Else
                    Debug.Print "El solicitante " & Destinatarios(i).Legajo & " no est� cargado en la base de Recursos!!!"
                End If
                If flgPruebaDeResultado Then
                    'If Destinatarios(i).Solicitante = "A39150" Then Stop
                    sMensaje1 = sMensaje1 & ClearNull(Destinatarios(i).Legajo) & " (" & ClearNull(Destinatarios(i).email) & ", " & IIf(Destinatarios(i).Habilitado, "SI", "NO") & ") tiene " & Destinatarios(i).peticiones.Count & " peticiones." & vbCrLf
                    For J = 1 To Destinatarios(i).peticiones.Count
                        sMensaje1 = sMensaje1 & vbTab & Destinatarios(i).peticiones(J) & vbCrLf
                    Next J
                End If
            Next i
            If flgPruebaDeResultado Then
                sMensaje1 = "Total peticiones a cancelar: " & TotalPeticiones & vbCrLf & vbCrLf & sMensaje1
                Call doFile("detalle_mails.log", sMensaje1)
                Call doFile("detalle_plano.log", sMensaje2)
            End If

            flgContinuar = True
            
            ' Proceso principal de cancelaci�n de peticiones por inactividad
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    J = 0
                    ' Obtengo los grupos activos de la petici�n
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            If ClearNull(aplRST.Fields!cod_grupo) <> sGrupoHomologador Then
                                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                    flgTieneGrupo = True
                                    ReDim Preserve vPeticionSectorId(J)
                                    ReDim Preserve vPeticionGrupoId(J)
                                    vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                    vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                    J = J + 1
                                End If
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If

                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                        sSolicitante = ClearNull(aplRST.Fields!cod_solicitante)
                    End If

                    If flgTieneGrupo Then
                        ' Por cada grupo activo realizo la cancelaci�n del mismo y registro los cambios en el historial
                        For J = 0 To UBound(vPeticionGrupoId)
                            If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                                EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                            End If
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "ESTADO", "CANCEL", Null, Null)
                            Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                            If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                            Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                                Call chkSectorOpiEva(vPeticionNroInterno(i))
                            End If
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                            NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), "CANCEL", glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                            If NuevoEstadoPeticion <> EstadoPeticion Then
                                If NuevoEstadoPeticion <> "CANCEL" Then NuevoEstadoPeticion = "CANCEL"
                                Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, PROCESO_SISTEMA, TextoHistorialPeticion, PROCESO_SISTEMA)
                            End If
                        Next J
                    Else
                        ' La petici�n no tiene grupos activos
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                            Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", "CANCEL", Null, Null)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "NROANEX", "", Null, 0)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Now, Null)
                            Call sp_ChgEstadoSector(vPeticionNroInterno(i), Null, "CANCEL")
                            Call sp_ChgEstadoGrupo(vPeticionNroInterno(i), Null, Null, "CANCEL")
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", "CANCEL", Null, Null, Null, Null, PROCESO_SISTEMA, TextoHistorialPeticion, PROCESO_SISTEMA)
                            Call sp_InsAgrupPetic(1574, vPeticionNroInterno(i))                     ' Agrego la petici�n al agrupamiento de peticiones a cancelar
                            Call doFile("detalle_plano.log", "Se cancel� correctamente la petici�n " & vPeticionNroAsignado(i) & ".")
                        Else
                            'Debug.Print "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal."
                            Call doFile("detalle_plano.log", "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal.")
                        End If
                    End If
                    Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                    Call sp_DeletePeticionChangeMan(vPeticionNroInterno(i))               ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                    If flgBorrarPlanificacion Then Call sp_DeletePeticionPlancab(per_nrointerno, vPeticionNroInterno(i))
                    DoEvents
                Next i
            End If

            ' Enviar email al solicitante
            If flgContinuar Then
                For i = 0 To UBound(Destinatarios)
                    'If Validar_Email(Destinatarios(i).email) Then
                        'Call EnviarCorreo_CancelaPeticion(Destinatarios(i).email, Destinatarios(i).peticiones)
                        Call EnviarCorreo_CancelaPeticion("fernandojavier.spitz@bbva.com", Destinatarios(i).peticiones)
                    'End If
                    DoEvents
                Next i
            End If

            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub BajarAdjuntos()
    Dim ubicacion As String
    Dim checkSUM As String
    Dim fso As FileSystemObject
    Dim sCRC32 As String
    Dim Nombre As String
    Dim mensaje As String
    Dim Tipo As String
    Dim FileName As String
    Dim txtCommand As String
    Dim actualizados As Long
    Dim pet_nrointerno As Long
    Dim total As Long
    Dim i As Long, J As Long
    Dim sFile As String, sSkipFile As String
    Dim bSkip As Boolean
    
    Dim vPeticionNroInterno() As Long
    Dim vPeticionNroInternoAvoid() As Long
    
    Set fso = New FileSystemObject
    
    ubicacion = "\\bbvdfs\DFS\APPS\GesPet"
    
    Erase vPeticionNroInterno

    'sSkipFile = AbrirArchivo            ' A skipear
    sFile = AbrirArchivo                ' A procesar
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            
            'vPeticionNroInternoAvoid = CargarArchivo(sSkipFile)
            vPeticionNroInterno = CargarArchivo(sFile)
            
            total = UBound(vPeticionNroInterno)
            bSkip = False
            For i = 0 To UBound(vPeticionNroInterno)
'                bSkip = False
'                For j = 0 To UBound(vPeticionNroInternoAvoid)
'                    If vPeticionNroInternoAvoid(j) = vPeticionNroInterno(i) Then
'                        bSkip = True
'                        Exit For
'                    End If
'                Next j
                
                If Not bSkip Then
                    Call Status("1. Procesando... " & i + 1 & " de " & total)
                    'pet_nrointerno = vPeticionNroInterno(i)
                    If sp_GetAdjuntosPet(vPeticionNroInterno(i), Null, Null, Null) Then
                        Do While Not aplRST.EOF
                            Nombre = ClearNull(aplRST.Fields!adj_file)
                            Tipo = ClearNull(aplRST.Fields!adj_tipo)
                            FileName = ClearNull(aplRST.Fields!adj_file)
                            If PeticionAdjunto2FileNew(vPeticionNroInterno(i), Tipo, Nombre, glWRKDIR) Then
                                txtCommand = ""
                                txtCommand = txtCommand & "UPDATE PeticionAdjunto "
                                txtCommand = txtCommand & "SET adj_objeto = ''"
                                txtCommand = txtCommand & "WHERE "
                                txtCommand = txtCommand & "pet_nrointerno=" & vPeticionNroInterno(i) & " AND "
                                txtCommand = txtCommand & "adj_tipo='" & Tipo & "' AND "
                                txtCommand = txtCommand & "adj_file='" & FileName & "'"
                                
        '                        txtCommand = txtCommand & "UPDATE PeticionAdjunto "
        '                        txtCommand = txtCommand & "SET adj_objeto = NULL,"
        '                        txtCommand = txtCommand & "adj_url = '" & ubicacion & "',"
        '                        txtCommand = txtCommand & "adj_urlname = '" & Nombre & "',"
        '                        txtCommand = txtCommand & "adj_filesystem = 'S',"
        '                        txtCommand = txtCommand & "adj_CRC32 = " & CRC32.CalcCRC32(glWRKDIR & Nombre)
        '                        txtCommand = txtCommand & "WHERE "
        '                        txtCommand = txtCommand & "pet_nrointerno=" & vPeticionNroInterno(i) & " AND "
        '                        txtCommand = txtCommand & "adj_tipo='" & Tipo & "' AND "
        '                        txtCommand = txtCommand & "adj_file='" & FileName & "'"
                                aplCONN.Execute txtCommand, actualizados
                                If actualizados = 1 Then
                                    mensaje = ubicacion & "\" & Nombre
                                    fso.CopyFile glWRKDIR & "\" & Nombre, ubicacion & "\" & Nombre
                                    Call doFile("MigracionAdjuntos_2010.txt", Nombre)
                                    fso.DeleteFile glWRKDIR & "\" & Nombre
                                Else
                                    Stop
                                End If
                            Else
                                Call doFile("MigracionAdjuntos_2010_Revisar.txt", Nombre)
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If
                End If
            Next i
        End If
    End If
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub VerificacionLogins()
    On Error GoTo Errores
    Dim i As Long
    Dim J As Long

    'If sp_GetRecurso(Null, "R", "A") Then
    If sp_GetRecurso("A126354", "R", "A") Then          ' A126024 Abertura
        Call Puntero(True)
        Do While Not aplRST.EOF
            flgConnectOK = False
            Call Status("Procesando... " & i + 1)
            
            If InStr(1, "A127163|A126024|", ClearNull(aplRST.Fields!cod_recurso), vbTextCompare) = 0 Then
                Set logCONN = New ADODB.Connection
                logCONN.ConnectionString = ("DRIVER=SYBASE ASE ODBC DRIVER;SRVR=DSPROD02;DB=GesPet;UID=" & ClearNull(aplRST.Fields!cod_recurso) & ";PWD=5905243178")
                logCONN.Open
                ' El error se da cuando el usuario logra conectarse con la media clave
                If flgConnectOK Then
                    Call doLog("FIX - " & ClearNull(aplRST.Fields!cod_recurso) & " (" & ClearNull(aplRST.Fields!nom_recurso) & ") corregido.")
                    Call mdiPrincipal.AdoConnection.InicializarPassword2(logCONN, "5905243178", "Pomelo01")
                    Call mdiPrincipal.AdoConnection.InicializarPassword2(logCONN, "Pomelo01", "5905243178")
                End If
                
                If logCONN.State = adStateOpen Then logCONN.Close
                Set logCONN = Nothing
            Else
                Call doLog("VOID- " & ClearNull(aplRST.Fields!cod_recurso) & " (" & ClearNull(aplRST.Fields!nom_recurso) & ") salteado!.")
            End If
            aplRST.MoveNext
            i = i + 1
            For J = 1 To 500000
            Next J
            DoEvents
        Loop
    End If
    MsgBox "Proceso finalizado.", vbInformation
    Call Puntero(False)
    Call Status("Listo.")
    Exit Sub
Errores:
    If Err.Number <> 0 Then
        If InStr(1, Err.DESCRIPTION, "[SYBASE][ODBC Sybase driver][Sybase]Login failed.", vbTextCompare) > 0 Then
            Resume Next
        Else
            'Stop
            Debug.Print "ERR - " & ClearNull(aplRST.Fields!nom_recurso) & " (" & ClearNull(aplRST.Fields!cod_recurso) & ") " & Err.DESCRIPTION
            Resume Next
        End If
    End If
End Sub

Private Sub logCONN_ConnectComplete(ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pConnection As ADODB.Connection)
    Dim strConnInfo As String
    
    'adStatus:
    ' 1:adStatusOK (OK)                             ' Indicates that the operation that has caused this event to occur completed successfully.
    ' 2:adStatusErrorsOccurred (ERRORSOCCURRED)     ' Indicates that the operation that has caused this event to occur did not complete successfully.
    
    If adStatus = 1 Then
        flgConnectOK = True
    End If
End Sub

Private Sub TranspasoDeRecursos()
    Dim vPeticionNroInterno() As Long
    Dim vPeticionNroAsignado() As Long
    Dim sFile As String
    Dim i As Long, J As Long, k As Long
    Dim sRecurso As String
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            
            vPeticionNroAsignado = CargarArchivo(sFile)
            
            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                'TotalPeticiones = TotalPeticiones + 1
            Next i
            
            k = 0
            J = UBound(vPeticionNroInterno)
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("1. Procesando... " & i + 1 & " de " & J + 1)
                Debug.Print vPeticionNroAsignado(i)
                If sp_GetPeticionRecurso(vPeticionNroInterno(i), "22-11") Then
                    sRecurso = ClearNull(aplRST.Fields!cod_recurso)
                    Debug.Print vPeticionNroAsignado(i) & " - " & sRecurso
                    Call sp_DeletePeticionRecurso(vPeticionNroInterno(i), sRecurso)
                    Call sp_InsertPeticionRecurso(vPeticionNroInterno(i), sRecurso, "22-04", "251", "DESA", "MEDIO")
                End If
            Next i
        End If
    End If
    Erase vPeticionNroInterno
    Erase vPeticionNroAsignado
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

Private Sub QuitarBeneficios()
    'Dim vPeticionNroInterno() As Long
    Dim PeticionNroInterno As Long
    Dim vIndicador() As Long
    Dim vSubIndicador() As Long
    Dim sFile As String
    Dim i As Long, J As Long, k As Long
    
    PeticionNroInterno = 60305
    
    If sp_GetPeticionBeneficios(PeticionNroInterno, Null, Null) Then
        Call Puntero(True)
        Call Status("Procesando... ")
        Do While Not aplRST.EOF
            ReDim Preserve vIndicador(i)
            ReDim Preserve vSubIndicador(i)
            vIndicador(i) = aplRST.Fields!indicadorId
            vSubIndicador(i) = aplRST.Fields!driverId
            i = i + 1
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    ' Proceso las bajas
    For i = 0 To UBound(vSubIndicador)
        Call sp_DeletePeticionBeneficios(PeticionNroInterno, vIndicador(i), vSubIndicador(i))
    Next i
    
    Erase vIndicador
    Erase vSubIndicador
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

Private Sub LimpiarHistorial()
    Dim vPeticionNroInterno() As Long
    Dim vHistorialNroInterno() As Long
    Dim vAdministrador() As String
    Dim sFile As String
    Dim i As Long, J As Long, k As Long
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroInterno = CargarArchivo(sFile)
            k = 0
            J = UBound(vPeticionNroInterno)
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("1. Procesando... " & i + 1 & " de " & J + 1)
                If sp_GetHistorial(vPeticionNroInterno(i), Null) Then
                    Do While Not aplRST.EOF
                        If InStr(1, "XA00309|A125958|", ClearNull(aplRST.Fields!audit_user), vbTextCompare) > 0 Then
                            'If ClearNull(aplRST.Fields!replc_user) = "" Then Stop
                            ReDim Preserve vHistorialNroInterno(k)
                            ReDim Preserve vAdministrador(k)
                            vHistorialNroInterno(k) = aplRST.Fields!hst_nrointerno
                            If ClearNull(aplRST.Fields!replc_user) = "" Then
                                doLog (vPeticionNroInterno(i))
                                vAdministrador(k) = "JP00"
                            Else
                                vAdministrador(k) = ClearNull(aplRST.Fields!replc_user)
                            End If
                            k = k + 1
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            Next i
            ' Recorro todos los numeros de historial y actualizo el usuario
            J = UBound(vHistorialNroInterno)
            For i = 0 To UBound(vHistorialNroInterno)
                Call Status("2. Procesando... " & i + 1 & " de " & J + 1)
                Call sp_UpdHistorialField(vHistorialNroInterno(i), "audit_user", vAdministrador(i), Null, Null)
            Next i
        End If
    End If
    Erase vPeticionNroInterno
    Erase vHistorialNroInterno
    Erase vAdministrador
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

Private Sub EliminarPeticionesEnviadas_terminal()
    Dim vPeticionNroInterno() As Long
    Dim sFile As String
    Dim i As Long, J As Long
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroInterno = CargarArchivo(sFile)
            J = UBound(vPeticionNroInterno)
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1 & " de " & J + 1)
                Call sp_DeletePeticionEnviadas(vPeticionNroInterno(i))
            Next i
        End If
    End If
    Erase vPeticionNroInterno
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

Private Sub BorrarMensajesViejos()
    Dim vPeticionNroInterno() As Long
    Dim sFile As String
    Dim i As Long, J As Long
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroInterno = CargarArchivo(sFile)
            J = UBound(vPeticionNroInterno)
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1 & " de " & J + 1)
                Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)
            Next i
        End If
    End If
    Erase vPeticionNroInterno
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

'Private Sub RecalcularValoracion()
'
'End Sub

Private Sub CompletarCheckNoCargaBeneficios()
    Dim PetNroInterno() As Long
    Dim petNroInterno2() As Long
    Dim i As Long, J As Long
    
    If sp_GetAgrupPetic(1573, 0) Then
        Do While Not aplRST.EOF
            ReDim Preserve PetNroInterno(i)
            PetNroInterno(i) = aplRST.Fields!pet_nrointerno
            i = i + 1
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    Erase petNroInterno2
    J = 0
    For i = 0 To UBound(PetNroInterno)
        If sp_GetUnaPeticion(PetNroInterno(i)) Then
            If ClearNull(aplRST.Fields!cod_estado) = "DEVSOL" Then
                If ClearNull(aplRST.Fields!cargaBeneficios) <> "N" Then
                    ReDim Preserve petNroInterno2(J)
                    petNroInterno2(J) = aplRST.Fields!pet_nrointerno
                    J = J + 1
                End If
            End If
            'aplRST.MoveNext
        End If
    Next i
    Erase PetNroInterno
    For i = 0 To UBound(petNroInterno2)
        Call sp_UpdatePetField(petNroInterno2(i), "BENEFICIOS", "N", Null, Null)
    Next i
    
    Erase PetNroInterno
    Erase petNroInterno2
    
    MsgBox "Proceso finalizado."
End Sub

Private Sub CompletarPalancas()
    Const PROCESO_SISTEMA = "JP00"
    Const COEFICIENTE = 20
    Const VALOR_14_NIGUNO = 60
    Const VALOR_15_NIGUNO = 69
    
    Dim sFile As String
    Dim sumaImpacto As Double, sumaFacilidad As Double
    Dim vPeticionNroInterno() As Long
    Dim flgPalanca14 As Boolean, flgPalanca15 As Boolean
    Dim flgRecalcular As Boolean
    Dim i As Long, J As Long
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroInterno = CargarArchivo(sFile)
            
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1)
                flgPalanca14 = False
                flgPalanca15 = False
                flgRecalcular = True
                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    If ClearNull(aplRST.Fields!valor_impacto) = "" And ClearNull(aplRST.Fields!valor_facilidad) = "" Then
                        flgRecalcular = False
                    End If
                End If
'                If sp_GetPeticionBeneficios(vPeticionNroInterno(i)) Then
'                    Do While Not aplRST.EOF
'                        If ClearNull(aplRST.Fields!driverId) = "14" Then
'                            flgPalanca14 = True
'                        End If
'                        If ClearNull(aplRST.Fields!driverId) = "15" Then
'                            flgPalanca15 = True
'                        End If
'                        aplRST.MoveNext
'                        DoEvents
'                    Loop
'                End If
'                If Not flgPalanca14 Then Call sp_UpdatePeticionBeneficios(vPeticionNroInterno(i), 1, 14, VALOR_14_NIGUNO)
'                If Not flgPalanca15 Then Call sp_UpdatePeticionBeneficios(vPeticionNroInterno(i), 1, 15, VALOR_15_NIGUNO)
                Call sp_UpdatePeticionBeneficios(vPeticionNroInterno(i), 1, 14, VALOR_14_NIGUNO)
                Call sp_UpdatePeticionBeneficios(vPeticionNroInterno(i), 1, 15, VALOR_15_NIGUNO)
                sumaImpacto = 0
                sumaFacilidad = 0
                If flgRecalcular Then
                    If sp_GetPeticionBeneficios(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            If Not IsNull(aplRST.Fields!Valor) Then
                                Select Case ClearNull(aplRST.Fields!indicadorId)
                                    Case "1"
                                        sumaImpacto = sumaImpacto + (IIf(IsNull(aplRST.Fields!valor_referencia), 0, aplRST.Fields!valor_referencia) * aplRST.Fields!Porcentaje)
                                    Case "2"
                                        sumaFacilidad = sumaFacilidad + (IIf(IsNull(aplRST.Fields!valor_referencia), 0, aplRST.Fields!valor_referencia) * aplRST.Fields!Porcentaje)
                                End Select
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If
                    Call sp_UpdatePetField(glNumeroPeticion, "VALOR_IMPACTO", Null, Null, Fix(sumaImpacto * COEFICIENTE))
                    Call sp_UpdatePetField(glNumeroPeticion, "VALOR_FACILIDAD", Null, Null, Fix(sumaFacilidad * COEFICIENTE))
                End If
            Next i
        End If
    End If
    Erase vPeticionNroInterno
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

' Proceso para depurar peticiones al 16/06/2016 (Pato Ahkrass)
Private Sub FinalizarPeticionesPato_Criterio1()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sReferenteDeSistema As String
    Dim sNombreReferente As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long                 ' Nro. total de peticiones a procesar desde el archivo
    Dim ParcialPeticiones As Long               ' Cantidad parcial para cada referente de sistemas
    Dim sGrupoHomologador As String

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno
    Erase vPeticionSectorId
    Erase vPeticionGrupoId
    Erase Destinatarios
    
    ' * Criterio 1:
    '
    ' Peticiones en estado "En ejecuci�n" que han sido dadas de alta hace m�s de 3 a�os y
    ' no tienen horas cargadas en los �ltimos 6 meses por ning�n grupo vinculado.
    '
    ' * Acci�n a realizar:
    '
    ' Se cancela la petici�n (se cancela cada grupo ejecutor activo)

    flgPruebaDeResultado = True             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo("Proceso 1")
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            TextoHistorialPeticion = "Petici�n �En ejecuci�n� dada de alta hace > 3 a�os sin horas cargadas por ning�n grupo vinculado y sin ning�n conformes de pruebas de usuario (PROC1)."
            TextoHistorialGrupo = "Petici�n �En ejecuci�n� dada de alta hace > 3 a�os sin horas cargadas por ning�n grupo vinculado y sin ning�n conformes de pruebas de usuario (PROC1)."

            ' Se elimina de la planificaci�n
            flgBorrarPlanificacion = False
            per_estado = ""
            If sp_GetPeriodo(Null, Null) Then                                             ' Obtengo los datos del per�odo vigente
                Do While Not aplRST.EOF
                    If ClearNull(aplRST.Fields!vigente) = "S" Then
                        per_nrointerno = ClearNull(aplRST.Fields!per_nrointerno)
                        per_estado = ClearNull(aplRST.Fields!per_estado)
                        Exit Do
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            If ClearNull(per_estado) <> "" Then
                If InStr(1, "PLAN10|PLAN20|", per_estado, vbTextCompare) > 0 Then       ' Si a�n el per�odo no alcanz� la 3ra. o 4ta. instancia, se elimina de la planificaci�n
                    flgBorrarPlanificacion = True
                End If
            End If

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            ' Arma los mensajes de correo para los solicitantes
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Asignando peticiones a solicitantes... " & i)
                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    sReferenteDeSistema = ClearNull(aplRST.Fields!cod_bpar)             ' Referente de sistema
                End If
                If sp_GetRecurso(sReferenteDeSistema, "R") Then
                    sNombreReferente = ClearNull(aplRST.Fields!nom_recurso)
                End If
                If i = 0 Then
                    ReDim Preserve Destinatarios(i)
                    Destinatarios(i).Legajo = sReferenteDeSistema
                    Destinatarios(i).Nombre = sNombreReferente
                    Destinatarios(i).peticiones.Add (vPeticionNroAsignado(i))
                Else
                    Call AsignarPeticionA(sReferenteDeSistema, sNombreReferente, vPeticionNroAsignado(i))
                End If
                DoEvents
            Next i

            ' Arma un log por cada referente de sistema
            If flgPruebaDeResultado Then
                For i = 0 To UBound(Destinatarios)
                    ParcialPeticiones = 0
                    If flgPruebaDeResultado Then
                        sMensaje1 = "Referente de sistemas: " & Destinatarios(i).Nombre & " (" & Destinatarios(i).Legajo & ")" & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Criterio:" & vbCrLf & "*********" & vbCrLf
                        sMensaje1 = sMensaje1 & "Peticiones en estado �En ejecuci�n� que han sido dadas de alta hace m�s de 3 a�os y" & vbCrLf
                        sMensaje1 = sMensaje1 & "no tienen horas cargadas en los �ltimos 6 meses por ning�n grupo vinculado." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Acci�n:" & vbCrLf & "*******" & vbCrLf
                        sMensaje1 = sMensaje1 & "Se cancela la petici�n (se cancelan todos los ejecutores activos)." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Detalle de peticiones procesadas:" & vbCrLf & "*********************************" & vbCrLf & vbCrLf
                        For J = 1 To Destinatarios(i).peticiones.Count
                            If sp_GetUnaPeticionAsig(Destinatarios(i).peticiones(J)) Then
                                If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                    ParcialPeticiones = ParcialPeticiones + 1
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!pet_nroasignado) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_tipo_peticion) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_clase) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!fe_pedido) & vbTab
                                    sMensaje1 = sMensaje1 & "Alta hace " & Fix(DateDiff("yyyy", aplRST.Fields!fe_pedido, Now)) & " a�os apr�x." & vbTab & vbTab
                                    sMensaje1 = sMensaje1 & Mid(ClearNull(aplRST.Fields!Titulo), 1, 50) & vbCrLf
                                End If
                            End If
                        Next J
                        sMensaje1 = sMensaje1 & vbCrLf & ParcialPeticiones & " registro(s)."
                    End If
                     If ExisteArchivo("C:\temp\" & Destinatarios(i).Legajo & "_PROC1.log") Then Kill "C:\temp\" & Destinatarios(i).Legajo & "_PROC1.log"
                    Call doFile2("C:\temp\" & Destinatarios(i).Legajo & "_PROC1.log", sMensaje1)
                Next i
            End If

            flgContinuar = True
            
            ' ************************************************************************************************************************************************
            ' Proceso principal
            ' ************************************************************************************************************************************************
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    J = 0
                    ' Obtengo los grupos activos de la petici�n
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            'If ClearNull(aplRST.Fields!cod_grupo) <> sGrupoHomologador Then
                                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                    flgTieneGrupo = True
                                    ReDim Preserve vPeticionSectorId(J)
                                    ReDim Preserve vPeticionGrupoId(J)
                                    vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                    vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                    J = J + 1
                                End If
                            'End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If

                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                        sReferenteDeSistema = ClearNull(aplRST.Fields!cod_solicitante)
                    End If

                    If flgTieneGrupo Then
                        ' Por cada grupo activo realizo la cancelaci�n del mismo y registro los cambios en el historial
                        For J = 0 To UBound(vPeticionGrupoId)
                            If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                                EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                            End If
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "ESTADO", "CANCEL", Null, Null)
                            Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                            If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                            Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                                Call chkSectorOpiEva(vPeticionNroInterno(i))
                            End If
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                            NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), "CANCEL", glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                            If NuevoEstadoPeticion <> EstadoPeticion Then
                                If NuevoEstadoPeticion <> "CANCEL" Then NuevoEstadoPeticion = "CANCEL"
                                Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                            End If
                        Next J
                    Else
                        ' La petici�n no tiene grupos activos
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                            Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", "CANCEL", Null, Null)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "NROANEX", "", Null, 0)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Now, Null)
                            Call sp_ChgEstadoSector(vPeticionNroInterno(i), Null, "CANCEL")
                            Call sp_ChgEstadoGrupo(vPeticionNroInterno(i), Null, Null, "CANCEL")
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", "CANCEL", Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                        Else
                            Debug.Print "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal."
                        End If
                    End If
                    Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                    Call sp_DeletePeticionChangeMan(vPeticionNroInterno(i))               ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                    If flgBorrarPlanificacion Then Call sp_DeletePeticionPlancab(per_nrointerno, vPeticionNroInterno(i))
                    DoEvents
                Next i
            End If
            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Erase vPeticionSectorId
            Erase vPeticionGrupoId
            Erase Destinatarios
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub FinalizarPeticionesPato_Criterio2()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sReferenteDeSistema As String
    Dim sNombreReferente As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long                 ' Nro. total de peticiones a procesar desde el archivo
    Dim ParcialPeticiones As Long               ' Cantidad parcial para cada referente de sistemas
    Dim sGrupoHomologador As String

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno
    Erase vPeticionSectorId
    Erase vPeticionGrupoId
    Erase Destinatarios
    
    ' * Criterio 2:
    '
    ' Peticiones en estado "En ejecuci�n" que tiene conforme FINAL de homologaci�n
    ' para pasar a producci�n hace > de 4 meses.
    '
    ' * Acci�n a realizar:
    '
    ' Se finalizan los grupos con estado activo de manera autom�tica.

    flgPruebaDeResultado = True             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo("Proceso 2")
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            TextoHistorialPeticion = "Petici�n �En ejecuci�n� dada de alta hace > 3 a�os sin horas cargadas por ning�n grupo vinculado y sin ning�n conformes de pruebas de usuario (PROC2)."
            TextoHistorialGrupo = "Petici�n �En ejecuci�n� dada de alta hace > 3 a�os sin horas cargadas por ning�n grupo vinculado y sin ning�n conformes de pruebas de usuario (PROC2)."

            ' Se elimina de la planificaci�n
            If Not flgPruebaDeResultado Then
                per_estado = ""
                flgBorrarPlanificacion = False
                If sp_GetPeriodo(Null, Null) Then                                             ' Obtengo los datos del per�odo vigente
                    Do While Not aplRST.EOF
                        If ClearNull(aplRST.Fields!vigente) = "S" Then
                            per_nrointerno = ClearNull(aplRST.Fields!per_nrointerno)
                            per_estado = ClearNull(aplRST.Fields!per_estado)
                            Exit Do
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
                If ClearNull(per_estado) <> "" Then
                    If InStr(1, "PLAN10|PLAN20|", per_estado, vbTextCompare) > 0 Then       ' Si a�n el per�odo no alcanz� la 3ra. o 4ta. instancia, se elimina de la planificaci�n
                        flgBorrarPlanificacion = True
                    End If
                End If
            End If

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            ' Arma los mensajes de correo para los solicitantes
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Asignando peticiones a solicitantes... " & i)
                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    sReferenteDeSistema = ClearNull(aplRST.Fields!cod_bpar)             ' Referente de sistema
                End If
                If sp_GetRecurso(sReferenteDeSistema, "R") Then
                    sNombreReferente = ClearNull(aplRST.Fields!nom_recurso)
                End If
                If i = 0 Then
                    ReDim Preserve Destinatarios(i)
                    Destinatarios(i).Legajo = sReferenteDeSistema
                    Destinatarios(i).Nombre = sNombreReferente
                    Destinatarios(i).peticiones.Add (vPeticionNroAsignado(i))
                Else
                    Call AsignarPeticionA(sReferenteDeSistema, sNombreReferente, vPeticionNroAsignado(i))
                End If
                DoEvents
            Next i
            
            Dim sTitulo As String
            Dim fechaSOB As Date
            Dim pet_nrointerno As Long

            ' Arma un log por cada referente de sistema
            If flgPruebaDeResultado Then
                For i = 0 To UBound(Destinatarios)
                    ParcialPeticiones = 0
                    If flgPruebaDeResultado Then
                        sMensaje1 = "Referente de sistemas: " & Destinatarios(i).Nombre & " (" & Destinatarios(i).Legajo & ")" & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Criterio:" & vbCrLf & "*********" & vbCrLf
                        sMensaje1 = sMensaje1 & "Peticiones en estado �En ejecuci�n� que tienen conforme final" & vbCrLf
                        sMensaje1 = sMensaje1 & "de homologaci�n para pasar a producci�n hace m�s de 4 meses." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Acci�n:" & vbCrLf & "*******" & vbCrLf
                        sMensaje1 = sMensaje1 & "Se finaliza la petici�n (se finalizan los grupos ejecutores en estado activo)." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Detalle de peticiones procesadas:" & vbCrLf & "*********************************" & vbCrLf & vbCrLf
                        For J = 1 To Destinatarios(i).peticiones.Count
                            If sp_GetUnaPeticionAsig(Destinatarios(i).peticiones(J)) Then
                                If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                    ParcialPeticiones = ParcialPeticiones + 1
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!pet_nroasignado) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_tipo_peticion) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_clase) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!fe_pedido) & vbTab
                                    sTitulo = Mid(ClearNull(aplRST.Fields!Titulo), 1, 50)
                                    pet_nrointerno = aplRST.Fields!pet_nrointerno
                                    If sp_GetPeticionConf(pet_nrointerno, "HSOB.F", Null) Then
                                        fechaSOB = CDate(aplRST.Fields!audit_date)
                                    End If
                                    If Fix(DateDiff("m", fechaSOB, Now)) >= 12 Then
                                        sMensaje1 = sMensaje1 & "SOB hace " & padRight(Fix(DateDiff("yyyy", fechaSOB, Now)), 3) & " a�os  apr�x."
                                    Else
                                        sMensaje1 = sMensaje1 & "SOB hace " & padRight(Fix(DateDiff("m", fechaSOB, Now)), 3) & " meses apr�x."
                                    End If
                                    sMensaje1 = sMensaje1 & " (" & ClearNull(Format(fechaSOB, "dd/MM/yyyy")) & ")" & vbTab
                                    sMensaje1 = sMensaje1 & sTitulo & vbCrLf
                                End If
                            End If
                        Next J
                        sMensaje1 = sMensaje1 & vbCrLf & ParcialPeticiones & " registro(s)."
                    End If
                     If ExisteArchivo("C:\temp\" & Destinatarios(i).Legajo & "_PROC2.log") Then Kill "C:\temp\" & Destinatarios(i).Legajo & "_PROC2.log"
                    Call doFile2("C:\temp\" & Destinatarios(i).Legajo & "_PROC2.log", sMensaje1)
                Next i
            End If

            flgContinuar = True
            
            ' ************************************************************************************************************************************************
            ' Proceso principal
            ' ************************************************************************************************************************************************
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    J = 0
                    ' Obtengo los grupos activos de la petici�n
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                flgTieneGrupo = True
                                ReDim Preserve vPeticionSectorId(J)
                                ReDim Preserve vPeticionGrupoId(J)
                                vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                J = J + 1
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If

                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                        sReferenteDeSistema = ClearNull(aplRST.Fields!cod_solicitante)
                    End If

                    If flgTieneGrupo Then
                        ' Por cada grupo activo realizo la finalizaci�n del mismo y registro los cambios en el historial
                        For J = 0 To UBound(vPeticionGrupoId)
                            If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                                EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                            End If
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "ESTADO", "TERMIN", Null, Null)
                            Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                            If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                            Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                                Call chkSectorOpiEva(vPeticionNroInterno(i))
                            End If
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                            NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), "TERMIN", glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                            If NuevoEstadoPeticion <> EstadoPeticion Then
                                If NuevoEstadoPeticion <> "TERMIN" Then NuevoEstadoPeticion = "TERMIN"
                                Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                            End If
                        Next J
                    Else
                        ' La petici�n no tiene grupos activos
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                            Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", "TERMIN", Null, Null)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "NROANEX", "", Null, 0)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Now, Null)
                            Call sp_ChgEstadoSector(vPeticionNroInterno(i), Null, "TERMIN")
                            Call sp_ChgEstadoGrupo(vPeticionNroInterno(i), Null, Null, "TERMIN")
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", "TERMIN", Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                        Else
                            Debug.Print "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal."
                        End If
                    End If
                    Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                    Call sp_DeletePeticionChangeMan(vPeticionNroInterno(i))               ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                    If flgBorrarPlanificacion Then Call sp_DeletePeticionPlancab(per_nrointerno, vPeticionNroInterno(i))
                    DoEvents
                Next i
            End If
            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Erase vPeticionSectorId
            Erase vPeticionGrupoId
            Erase Destinatarios
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub FinalizarPeticionesPato_Criterio3()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sReferenteDeSistema As String
    Dim sNombreReferente As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long                 ' Nro. total de peticiones a procesar desde el archivo
    Dim ParcialPeticiones As Long               ' Cantidad parcial para cada referente de sistemas
    Dim sGrupoHomologador As String

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno
    Erase vPeticionSectorId
    Erase vPeticionGrupoId
    Erase Destinatarios
    
    ' * Criterio 3:
    '
    ' Peticiones en estado "En ejecuci�n" que hace > de 4 meses
    ' no se le cargan horas (ning�n grupo ejecutor).
    '
    ' * Acci�n a realizar:
    '
    ' Se suspenden los grupos con estado activo de manera autom�tica.
    
    Dim fe_PlanIni As Date
    Dim fe_PlanFin As Date
    Dim fe_RealIni As Date
    Dim fe_RealFin As Date
    Dim fe_PasProd As Date
    Dim fe_Suspen As Date
    Dim motivoSuspen  As Long
    Dim horaspresup  As Long
    Dim cod_estado As String
    Dim fe_estado As Date
    Dim cod_situacion As String
    Dim hst_nrointerno_sol As Long
    Dim hst_nrointerno_rsp As Long

    flgPruebaDeResultado = True             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo("Proceso 3")
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            TextoHistorialPeticion = "Peticiones en estado �En ejecuci�n� que desde hace por lo menos 4 meses no se le cargan horas. Se suspenden los grupos ejecutores (PROC3)."
            TextoHistorialGrupo = "Peticiones en estado �En ejecuci�n� que desde hace por lo menos 4 meses no se le cargan horas. Se suspenden los grupos ejecutores (PROC3)."

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            ' Arma los mensajes de correo para los solicitantes
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Asignando peticiones a solicitantes... " & i)
                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    sReferenteDeSistema = ClearNull(aplRST.Fields!cod_bpar)             ' Referente de sistema
                End If
                If sp_GetRecurso(sReferenteDeSistema, "R") Then
                    sNombreReferente = ClearNull(aplRST.Fields!nom_recurso)
                End If
                If i = 0 Then
                    ReDim Preserve Destinatarios(i)
                    Destinatarios(i).Legajo = sReferenteDeSistema
                    Destinatarios(i).Nombre = sNombreReferente
                    Destinatarios(i).peticiones.Add (vPeticionNroAsignado(i))
                Else
                    Call AsignarPeticionA(sReferenteDeSistema, sNombreReferente, vPeticionNroAsignado(i))
                End If
                DoEvents
            Next i
            
            Dim sTitulo As String
            Dim fechaSOB As Date
            Dim pet_nrointerno As Long

            ' Arma un log por cada referente de sistema
            If flgPruebaDeResultado Then
                For i = 0 To UBound(Destinatarios)
                    ParcialPeticiones = 0
                    If flgPruebaDeResultado Then
                        sMensaje1 = "Referente de sistemas: " & Destinatarios(i).Nombre & " (" & Destinatarios(i).Legajo & ")" & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Criterio:" & vbCrLf & "*********" & vbCrLf
                        sMensaje1 = sMensaje1 & "Peticiones en estado �En ejecuci�n� que desde hace" & vbCrLf
                        sMensaje1 = sMensaje1 & "por lo menos 4 meses no se le cargan horas." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Acci�n:" & vbCrLf & "*******" & vbCrLf
                        sMensaje1 = sMensaje1 & "Se suspende la petici�n (se suspenden los grupos ejecutores)." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Detalle de peticiones procesadas:" & vbCrLf & "*********************************" & vbCrLf & vbCrLf
                        For J = 1 To Destinatarios(i).peticiones.Count
                            If sp_GetUnaPeticionAsig(Destinatarios(i).peticiones(J)) Then
                                If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                    ParcialPeticiones = ParcialPeticiones + 1
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!pet_nroasignado) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_tipo_peticion) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_clase) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!fe_pedido) & vbTab
                                    sTitulo = Mid(ClearNull(aplRST.Fields!Titulo), 1, 50)
                                    sMensaje1 = sMensaje1 & sTitulo & vbCrLf
                                End If
                            End If
                        Next J
                        sMensaje1 = sMensaje1 & vbCrLf & ParcialPeticiones & " registro(s)."
                    End If
                     If ExisteArchivo("C:\temp\" & Destinatarios(i).Legajo & "_PROC3.log") Then Kill "C:\temp\" & Destinatarios(i).Legajo & "_PROC3.log"
                    Call doFile2("C:\temp\" & Destinatarios(i).Legajo & "_PROC3.log", sMensaje1)
                Next i
            End If

            flgContinuar = True
            
            ' ************************************************************************************************************************************************
            ' Proceso principal
            ' ************************************************************************************************************************************************
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    J = 0
                    ' Obtengo los grupos activos de la petici�n
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|SUSPEN|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                If ClearNull(aplRST.Fields!cod_grupo) <> sGrupoHomologador Then
                                    flgTieneGrupo = True
                                    ReDim Preserve vPeticionSectorId(J)
                                    ReDim Preserve vPeticionGrupoId(J)
                                    vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                    vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                    J = J + 1
                                End If
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If

                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                        sReferenteDeSistema = ClearNull(aplRST.Fields!cod_solicitante)
                    End If

                    If flgTieneGrupo Then
                        ' Por cada grupo activo realizo la suspensi�n del mismo y registro los cambios en el historial
                        For J = 0 To UBound(vPeticionGrupoId)
                            If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                                EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                            End If
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                            
                            If sp_GetPeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), vPeticionGrupoId(J)) Then
                                If Not IsNull(aplRST.Fields!fe_ini_plan) Then
                                    fe_PlanIni = aplRST.Fields!fe_ini_plan
                                End If
                                If Not IsNull(aplRST.Fields!fe_fin_plan) Then
                                    fe_PlanFin = aplRST.Fields!fe_fin_plan
                                End If
                                If Not IsNull(aplRST.Fields!fe_ini_real) Then
                                    fe_RealIni = aplRST.Fields!fe_ini_real
                                End If
                                If Not IsNull(aplRST.Fields!fe_fin_real) Then
                                    fe_RealFin = aplRST.Fields!fe_fin_real
                                End If
                                If Not IsNull(aplRST.Fields!fe_produccion) Then
                                    fe_PasProd = aplRST.Fields!fe_produccion
                                End If
                                If Not IsNull(aplRST.Fields!fe_suspension) Then
                                    fe_Suspen = aplRST.Fields!fe_suspension
                                    fe_Suspen = DateAdd("d", 7, fe_Suspen)
                                Else
                                    fe_Suspen = DateAdd("d", 7, Now)
                                End If
                                'motivoSuspen = IIf(IsNull(aplRST.Fields!cod_motivo), Null, aplRST.Fields!cod_motivo)
                                If IsNull(aplRST.Fields!cod_motivo) Then
                                    motivoSuspen = 0
                                Else
                                    motivoSuspen = aplRST.Fields!cod_motivo
                                End If
                                horaspresup = aplRST.Fields!horaspresup
                                cod_estado = "SUSPEN"
                                fe_estado = Now
                                cod_situacion = IIf(IsNull(aplRST.Fields!cod_situacion), Null, aplRST.Fields!cod_situacion)
                                hst_nrointerno_sol = aplRST.Fields!hst_nrointerno_sol
                                hst_nrointerno_rsp = aplRST.Fields!hst_nrointerno_rsp
                            End If
                            Call sp_UpdatePeticionGrupo(vPeticionNroInterno(i), vPeticionGrupoId(J), fe_PlanIni, fe_PlanFin, fe_RealIni, fe_RealFin, fe_PasProd, fe_Suspen, 50, horaspresup, cod_estado, fe_estado, cod_situacion, hst_nrointerno_sol, hst_nrointerno_rsp)
                            Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                            If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                            Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                                Call chkSectorOpiEva(vPeticionNroInterno(i))
                            End If
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                            NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), "SUSPEN", glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                            If NuevoEstadoPeticion <> EstadoPeticion Then
                                If NuevoEstadoPeticion <> "SUSPEN" Then NuevoEstadoPeticion = "SUSPEN"
                                Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                            End If
                        Next J
                    Else
                        ' La petici�n no tiene grupos activos
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|SUSPEN|", EstadoPeticion, vbTextCompare) = 0 Then
                            Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", "SUSPEN", Null, Null)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "NROANEX", "", Null, 0)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Now, Null)
                            Call sp_ChgEstadoSector(vPeticionNroInterno(i), Null, "SUSPEN")
                            Call sp_ChgEstadoGrupo(vPeticionNroInterno(i), Null, Null, "SUSPEN")
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", "SUSPEN", Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                        Else
                            Debug.Print "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal o suspendida."
                        End If
                    End If
                    Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                    DoEvents
                Next i
            End If
            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Erase vPeticionSectorId
            Erase vPeticionGrupoId
            Erase Destinatarios
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub FinalizarPeticionesPato_Criterio4()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sReferenteDeSistema As String
    Dim sNombreReferente As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long                 ' Nro. total de peticiones a procesar desde el archivo
    Dim ParcialPeticiones As Long               ' Cantidad parcial para cada referente de sistemas
    Dim sGrupoHomologador As String

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno
    Erase vPeticionSectorId
    Erase vPeticionGrupoId
    Erase Destinatarios
    
    ' * Criterio 4:
    '
    ' Peticiones en estado "Suspendida" hace > de un a�o.
    '
    ' * Acci�n a realizar:
    '
    ' Se cancela la petici�n (se cancelan los grupos con estado activo de manera autom�tica).

    flgPruebaDeResultado = True             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.

    If sp_GetGrupoHomologacion Then
        sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo("Proceso 4")
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            TextoHistorialPeticion = "Petici�n en estado �Suspendida� desde hace m�s de un a�o. Se cancela (PROC4)."
            TextoHistorialGrupo = "Petici�n en estado �Suspendida� desde hace m�s de un a�o. Se cancela (PROC4)."

            ' Se elimina de la planificaci�n
            If Not flgPruebaDeResultado Then
                per_estado = ""
                flgBorrarPlanificacion = False
                If sp_GetPeriodo(Null, Null) Then                                             ' Obtengo los datos del per�odo vigente
                    Do While Not aplRST.EOF
                        If ClearNull(aplRST.Fields!vigente) = "S" Then
                            per_nrointerno = ClearNull(aplRST.Fields!per_nrointerno)
                            per_estado = ClearNull(aplRST.Fields!per_estado)
                            Exit Do
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
                If ClearNull(per_estado) <> "" Then
                    If InStr(1, "PLAN10|PLAN20|", per_estado, vbTextCompare) > 0 Then       ' Si a�n el per�odo no alcanz� la 3ra. o 4ta. instancia, se elimina de la planificaci�n
                        flgBorrarPlanificacion = True
                    End If
                End If
            End If

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i

            ' Arma los mensajes de correo para los solicitantes
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Asignando peticiones a solicitantes... " & i)
                If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                    sReferenteDeSistema = ClearNull(aplRST.Fields!cod_bpar)             ' Referente de sistema
                End If
                If sp_GetRecurso(sReferenteDeSistema, "R") Then
                    sNombreReferente = ClearNull(aplRST.Fields!nom_recurso)
                End If
                If i = 0 Then
                    ReDim Preserve Destinatarios(i)
                    Destinatarios(i).Legajo = sReferenteDeSistema
                    Destinatarios(i).Nombre = sNombreReferente
                    Destinatarios(i).peticiones.Add (vPeticionNroAsignado(i))
                Else
                    Call AsignarPeticionA(sReferenteDeSistema, sNombreReferente, vPeticionNroAsignado(i))
                End If
                DoEvents
            Next i
            
            Dim sTitulo As String
            Dim fechaEstado As Date

            ' Arma un log por cada referente de sistema
            If flgPruebaDeResultado Then
                For i = 0 To UBound(Destinatarios)
                    ParcialPeticiones = 0
                    If flgPruebaDeResultado Then
                        sMensaje1 = "Referente de sistemas: " & Destinatarios(i).Nombre & " (" & Destinatarios(i).Legajo & ")" & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Criterio:" & vbCrLf & "*********" & vbCrLf
                        sMensaje1 = sMensaje1 & "Peticiones en estado �Suspendida� hace > de un a�o." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Acci�n:" & vbCrLf & "*******" & vbCrLf
                        sMensaje1 = sMensaje1 & "Se cancela la petici�n (se cancelan los grupos ejecutores en estado activo)." & vbCrLf & vbCrLf
                        sMensaje1 = sMensaje1 & "Detalle de peticiones procesadas:" & vbCrLf & "*********************************" & vbCrLf & vbCrLf
                        For J = 1 To Destinatarios(i).peticiones.Count
                            If sp_GetUnaPeticionAsig(Destinatarios(i).peticiones(J)) Then
                                If ClearNull(aplRST.Fields!cod_estado) = "SUSPEN" Then
                                    ParcialPeticiones = ParcialPeticiones + 1
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!pet_nroasignado) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_tipo_peticion) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!cod_clase) & vbTab
                                    sMensaje1 = sMensaje1 & ClearNull(aplRST.Fields!fe_pedido) & vbTab
                                    sTitulo = Mid(ClearNull(aplRST.Fields!Titulo), 1, 50)
                                    fechaEstado = aplRST.Fields!fe_estado
                                    If Fix(DateDiff("m", fechaEstado, Now)) >= 12 Then
                                        sMensaje1 = sMensaje1 & "Suspendida desde hace " & padRight(Fix(DateDiff("yyyy", fechaEstado, Now)), 2) & " a�os  apr�x."
                                    Else
                                        sMensaje1 = sMensaje1 & "Suspendida desde hace " & padRight(Fix(DateDiff("m", fechaEstado, Now)), 2) & " meses apr�x."
                                    End If
                                    sMensaje1 = sMensaje1 & " (" & ClearNull(Format(fechaEstado, "dd/MM/yyyy")) & ")" & vbTab
                                    sMensaje1 = sMensaje1 & sTitulo & vbCrLf
                                End If
                            End If
                        Next J
                        sMensaje1 = sMensaje1 & vbCrLf & ParcialPeticiones & " registro(s)."
                    End If
                     If ExisteArchivo("C:\temp\" & Destinatarios(i).Legajo & "_PROC4.log") Then Kill "C:\temp\" & Destinatarios(i).Legajo & "_PROC4.log"
                    Call doFile2("C:\temp\" & Destinatarios(i).Legajo & "_PROC4.log", sMensaje1)
                Next i
            End If

            flgContinuar = True
            
            ' ************************************************************************************************************************************************
            ' Proceso principal
            ' ************************************************************************************************************************************************
            If flgContinuar Then
                For i = 0 To UBound(vPeticionNroInterno)
                    Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                    flgTieneGrupo = False
                    Erase vPeticionSectorId
                    Erase vPeticionGrupoId
                    J = 0
                    ' Obtengo los grupos activos de la petici�n
                    If sp_GetPeticionGrupo(vPeticionNroInterno(i), Null, Null) Then
                        Do While Not aplRST.EOF
                            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                flgTieneGrupo = True
                                ReDim Preserve vPeticionSectorId(J)
                                ReDim Preserve vPeticionGrupoId(J)
                                vPeticionSectorId(J) = ClearNull(aplRST.Fields!cod_sector)
                                vPeticionGrupoId(J) = ClearNull(aplRST.Fields!cod_grupo)
                                J = J + 1
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If

                    If sp_GetUnaPeticion(vPeticionNroInterno(i)) Then
                        EstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
                        sReferenteDeSistema = ClearNull(aplRST.Fields!cod_solicitante)
                    End If

                    If flgTieneGrupo Then
                        ' Por cada grupo activo realizo la finalizaci�n del mismo y registro los cambios en el historial
                        For J = 0 To UBound(vPeticionGrupoId)
                            If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                                EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                            End If
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                            Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "ESTADO", "TERMIN", Null, Null)
                            Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                            If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                            Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                            If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                                Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                                Call chkSectorOpiEva(vPeticionNroInterno(i))
                            End If
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                            NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), "TERMIN", glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                            Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                            If NuevoEstadoPeticion <> EstadoPeticion Then
                                If NuevoEstadoPeticion <> "TERMIN" Then NuevoEstadoPeticion = "TERMIN"
                                Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                            End If
                        Next J
                    Else
                        ' La petici�n no tiene grupos activos
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                            Call sp_DeleteMensaje(vPeticionNroInterno(i), 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONES
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", "TERMIN", Null, Null)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "NROANEX", "", Null, 0)
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "FECHAFRL", Null, Now, Null)
                            Call sp_ChgEstadoSector(vPeticionNroInterno(i), Null, "TERMIN")
                            Call sp_ChgEstadoGrupo(vPeticionNroInterno(i), Null, Null, "TERMIN")
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", "TERMIN", Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                        Else
                            Debug.Print "La petici�n " & vPeticionNroAsignado(i) & " ya se encuentra en estado terminal."
                        End If
                    End If
                    Call sp_DeletePeticionEnviadas2(vPeticionNroInterno(i))
                    Call sp_DeletePeticionChangeMan(vPeticionNroInterno(i))               ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                    If flgBorrarPlanificacion Then Call sp_DeletePeticionPlancab(per_nrointerno, vPeticionNroInterno(i))
                    DoEvents
                Next i
            End If
            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Erase vPeticionSectorId
            Erase vPeticionGrupoId
            Erase Destinatarios
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub ImportarPeticionesProduccion()
    ' Peticion                  OK
    ' PeticionSector
    ' PeticionGrupo
    ' PeticionGrupoRecurso
    ' Historial
    ' PeticionConf
    ' PeticionAdjunto
    ' PeticionMemo
    ' PeticionInfohc
    ' PeticionInfohd
    ' PeticionInfohr
    ' PeticionInfoht
    ' PeticionKPI
    ' PeticionBeneficios
    
    Dim connAlt As ADODB.Connection
    Set connAlt = New ADODB.Connection
    
    connAlt.ConnectionString = "DRIVER=Sybase ASE ODBC Driver;SRVR=DSPROD02;DB=GesPet;UID=A125958;PWD=eisenach5905243178;DLDBL=4096"
    connAlt.CursorLocation = adUseClient
    connAlt.Open
    
    ' Peticion
    If FuncionesImportacion2.sp_GetUnaPeticion2(53130, connAlt) Then
        Do While Not aplRST1.EOF
            Call sp_UpdatePeticion(aplRST1.Fields!pet_nrointerno, aplRST1.Fields!pet_nroasignado, aplRST1.Fields!Titulo, aplRST1.Fields!cod_tipo_peticion, aplRST1.Fields!prioridad, aplRST1.Fields!corp_local, _
                aplRST1.Fields!cod_orientacion, aplRST1.Fields!importancia_cod, aplRST1.Fields!importancia_prf, aplRST1.Fields!importancia_usr, aplRST1.Fields!pet_nroanexada, aplRST1.Fields!prj_nrointerno, _
                aplRST1.Fields!fe_pedido, aplRST1.Fields!fe_requerida, aplRST1.Fields!fe_comite, aplRST1.Fields!fe_ini_plan, aplRST1.Fields!fe_fin_plan, aplRST1.Fields!fe_ini_real, aplRST1.Fields!fe_fin_real, _
                aplRST1.Fields!horaspresup, aplRST1.Fields!cod_direccion, aplRST1.Fields!cod_gerencia, aplRST1.Fields!cod_sector, aplRST1.Fields!cod_usualta, _
                aplRST1.Fields!cod_solicitante, aplRST1.Fields!cod_referente, aplRST1.Fields!cod_supervisor, aplRST1.Fields!cod_director, aplRST1.Fields!cod_estado, _
                aplRST1.Fields!fe_estado, aplRST1.Fields!cod_situacion, aplRST1.Fields!cod_bpar, aplRST1.Fields!cod_clase, aplRST1.Fields!pet_imptech, _
                aplRST1.Fields!pet_sox001, aplRST1.Fields!pet_regulatorio, aplRST1.Fields!pet_projid, aplRST1.Fields!pet_projsubid, aplRST1.Fields!pet_projsubsid, _
                aplRST1.Fields!pet_emp, aplRST1.Fields!pet_ro, aplRST1.Fields!cod_bpe, aplRST1.Fields!fecha_BPE, aplRST1.Fields!pet_driver, aplRST1.Fields!cargaBeneficios)
            aplRST1.MoveNext
            DoEvents
        Loop
    End If
    
    ' PeticionSector
    If FuncionesImportacion2.sp_GetPeticionSector2(53130, Null, connAlt) Then
        Do While Not aplRST1.EOF
            Call sp_UpdatePeticionSector(aplRST1.Fields!pet_nrointerno, aplRST1.Fields!cod_sector, aplRST1.Fields!fe_ini_plan, aplRST1.Fields!fe_fin_plan, aplRST1.Fields!fe_ini_real, _
                aplRST1.Fields!fe_fin_real, aplRST1.Fields!horaspresup, aplRST1.Fields!cod_estado, aplRST1.Fields!fe_estado, aplRST1.Fields!cod_situacion, _
                aplRST1.Fields!hst_nrointerno_sol, aplRST1.Fields!hst_nrointerno_rsp)
            aplRST1.MoveNext
            DoEvents
        Loop
    End If
    
    ' El anterior proceso no funciona porque no existen los sectores tra�dos de producci�n en desarrollo.
    
    
    ' PeticionGrupo
    
    connAlt.Close
    Set connAlt = Nothing
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

Private Sub FinalizarPeticionesPato_Criterio2Correccion()
    Const PROCESO_SISTEMA = "JP00"
    
    Dim vPeticionNroAsignado() As Long
    Dim vPeticionNroInterno() As Long
    Dim vPeticionSectorId() As String
    Dim vPeticionGrupoId() As String

    Dim sFile As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim EstadoPeticion As String
    Dim TextoHistorialPeticion As String
    Dim TextoHistorialGrupo As String
    Dim NroHistorial As Long
    Dim sReferenteDeSistema As String
    Dim sNombreReferente As String
    Dim flgTieneGrupo As Boolean
    Dim TotalPeticiones As Long                 ' Nro. total de peticiones a procesar desde el archivo
    Dim ParcialPeticiones As Long               ' Cantidad parcial para cada referente de sistemas

    Dim per_nrointerno As Long
    Dim per_estado As String
    Dim flgBorrarPlanificacion As Boolean
    Dim flgContinuar As Boolean
    Dim flgPruebaDeResultado As Boolean
    Dim sMensaje1 As String
    Dim sMensaje2 As String
    Dim i As Long, J As Long

    Erase vPeticionNroAsignado
    Erase vPeticionNroInterno
    Erase vPeticionSectorId
    Erase vPeticionGrupoId
    Erase Destinatarios
    
    ' * Criterio 2:
    '
    ' Peticiones en estado "En ejecuci�n" que tiene conforme FINAL de homologaci�n
    ' para pasar a producci�n hace > de 4 meses.
    '
    ' * Acci�n a realizar:
    '
    ' Se finalizan los grupos con estado activo de manera autom�tica.

    flgPruebaDeResultado = True             ' Este flag activa la ejecuci�n como prueba de resultado: presenta los datos reales a procesar en un log.
    
    sMensaje1 = ""
    sMensaje2 = ""

    sFile = AbrirArchivo("Proceso 2")
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            TextoHistorialPeticion = "Petici�n �En ejecuci�n� dada de alta hace > 3 a�os sin horas cargadas por ning�n grupo vinculado y sin ning�n conformes de pruebas de usuario (PROC2)."
            TextoHistorialGrupo = "Petici�n �En ejecuci�n� dada de alta hace > 3 a�os sin horas cargadas por ning�n grupo vinculado y sin ning�n conformes de pruebas de usuario (PROC2)."

            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)

                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i
           
            ' ************************************************************************************************************************************************
            ' Proceso principal
            ' ************************************************************************************************************************************************
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando petici�n " & vPeticionNroAsignado(i) & " ... (" & i + 1 & " de " & TotalPeticiones & ")")
                For J = 0 To UBound(vPeticionGrupoId)
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoPeticion, vbTextCompare) = 0 Then
                        If sp_GetPeticionSector(vPeticionNroInterno(i), vPeticionSectorId(J)) Then
                            EstadoSector = ClearNull(aplRST.Fields!cod_estado)
                        End If
                        Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CGRU", "GRUP", vPeticionGrupoId(J))           ' Borra todos los mensajes para el grupo actual
                        Call sp_DelMensajePerfil(vPeticionNroInterno(i), "CSEC", "SECT", vPeticionSectorId(J))          ' Elimina todos los mensajes para ese sector
                        Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "ESTADO", "TERMIN", Null, Null)
                        Call getIntegracionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                        NuevoEstadoSector = getNuevoEstadoSector(vPeticionNroInterno(i), vPeticionSectorId(J))
                        If NuevoEstadoSector <> EstadoSector Then Call sp_UpdatePetSecField(vPeticionNroInterno(i), vPeticionSectorId(J), "ESTADO", NuevoEstadoSector, Null, Null)
                        Call getIntegracionSector(vPeticionNroInterno(i), Null, "ESTIOK|PLANOK|EJECUC|TERMIN|")
                        If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                            Call sp_DeletePeticionGrupo(vPeticionNroInterno(i), vPeticionSectorId(J), Null)
                            Call chkSectorOpiEva(vPeticionNroInterno(i))
                        End If
                        NuevoEstadoPeticion = getNuevoEstadoPeticion(vPeticionNroInterno(i))
                        NroHistorial = sp_AddHistorial2(vPeticionNroInterno(i), "GCHGEST", NuevoEstadoPeticion, vPeticionSectorId(J), NuevoEstadoSector, vPeticionGrupoId(J), "TERMIN", glLOGIN_ID_REEMPLAZO, TextoHistorialGrupo, PROCESO_SISTEMA)
                        Call sp_UpdatePetSubField(vPeticionNroInterno(i), vPeticionGrupoId(J), "HSTSOL", Null, Null, NroHistorial)
                        If NuevoEstadoPeticion <> EstadoPeticion Then
                            If NuevoEstadoPeticion <> "TERMIN" Then NuevoEstadoPeticion = "TERMIN"
                            Call sp_UpdatePetField(vPeticionNroInterno(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                            Call sp_AddHistorial2(vPeticionNroInterno(i), "PCHGEST", NuevoEstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoHistorialPeticion, PROCESO_SISTEMA)
                        End If
                    End If
                Next J
            Next i
                
            Erase vPeticionNroAsignado
            Erase vPeticionNroInterno
            Erase vPeticionSectorId
            Erase vPeticionGrupoId
            Erase Destinatarios
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
        Else
            MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
        End If
    Else
        MsgBox "Proceso cancelado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub CargarGridClip()
    On Error GoTo ErrHandler                ' add -004- a.
    
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
    Dim xAux As String
    Dim vRetorno() As String
    Dim auxAgrup As String
    Dim lPeticiones_Listadas As Long        ' add -011- a.
    Dim cod_recurso(1 To 5) As String       ' add -015- a.
    Dim lRecCount As Long
    Dim bGridOverflow As Boolean
    Dim bRegenerate As Boolean
    Dim i As Long, J As Long
    
    bRegenerate = False
    bGridOverflow = False
    flgEnCarga = True
    
    auxAgrup = ""
    If ClearNull(txtAgrup.text) <> "" Then
        If ParseString(vRetorno, txtAgrup.text, "|") > 0 Then
            auxAgrup = vRetorno(2)
        End If
    End If
    
    glNumeroPeticion = ""
    glCodigoRecurso = ""
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    
    Call Puntero(True)
    Call InicializarGrilla
    Call Status("Cargando peticiones...")
    If ClearNull(txtTitulo.text) = "" Then
        xAux = "NULL"
    Else
        xAux = ClearNull(txtTitulo.text)
    End If
    cmdVisualizar.visible = False
    
    If Not sp_rptPeticion00(txtNroDesde.text, txtNroHasta.text, _
                        petTipo, _
                        petPrioridad, _
                        txtDESDEalta.DateValue, txtHASTAalta.DateValue, _
                        petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, _
                        petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, _
                        txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, _
                        txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, _
                        txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
                        CodigoCombo(Me.cboBpar, True), CodigoCombo(cboPeticionClase, True), CodigoCombo(cboPeticionImpacto, True), CodigoCombo(cmbRegulatorio, True), _
                        txtPrjId, txtPrjSubId, txtPrjSubsId, IIf(CodigoCombo(cboEmp, True) = "NULL", Null, CodigoCombo(cboEmp, True)), CodigoCombo(cboRO, True), CodigoCombo(cboBPE, True), txtBPEDesde, txtBPEHasta, CodigoCombo(cboCategoria, True), txtDesdeAltaPeticion.DateValue, txtHastaAltaPeticion.DateValue) Then
        GoTo finx
    End If
    lPeticiones_Listadas = lPeticiones_Listadas + aplRST.RecordCount   ' add -011- a.
    
    
    With grdDatos
        .Redraw = False
        .Rows = aplRST.RecordCount + 1
        
        ' Selecciona
        .row = 1
        .col = 0
          
        .rowSel = .Rows - 1
        .ColSel = .cols - 1
        
        .Clip = aplRST.GetString(adClipString, -1, Chr(9), Chr(13), vbNullString)
        .row = 1
      
        ' habilita nuevamente el Redraw en el control
        .Redraw = True
    End With
    
    
    
    
'    Do While Not aplRST.EOF
'        With grdDatos
'            .Rows = .Rows + 1
'            If bGridOverflow Then
'                Exit Do
'            End If
'            .TextMatrix(.Rows - 1, colEmpresa) = ClearNull(aplRST!pet_emp)  ' add -019- a.
'            .TextMatrix(.Rows - 1, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
'            .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
'            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
'            .TextMatrix(.Rows - 1, colCODESTADO) = ClearNull(aplRST!cod_estado)
'            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
'            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
'            .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!sec_nom_estado)
'            .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!sec_nom_situacion)
'            .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
'            .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
'            .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
'            .TextMatrix(.Rows - 1, colEstadoFec) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
'            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
'            .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
'            .TextMatrix(.Rows - 1, colImportancia) = ClearNull(aplRST!importancia_nom)
'            .TextMatrix(.Rows - 1, colCorpLocal) = ClearNull(aplRST!corp_local)
'            .TextMatrix(.Rows - 1, colOrientacion) = ClearNull(aplRST!nom_orientacion)
'            .TextMatrix(.Rows - 1, colSecSol) = ClearNull(aplRST!sol_sector)
'            .TextMatrix(.Rows - 1, colPetRO) = ClearNull(aplRST!pet_riesgo)        ' add -021- a.
'            If flgNoHijos = True Then
'                .TextMatrix(.Rows - 1, colSecCodDir) = ""
'                .TextMatrix(.Rows - 1, colSecCodGer) = ""
'                .TextMatrix(.Rows - 1, colSecCodSec) = ""
'                .TextMatrix(.Rows - 1, colSecCodSub) = ""
'                .TextMatrix(.Rows - 1, colSecNomArea) = ""
'                .TextMatrix(.Rows - 1, colSecEsfuerzo) = ""
'                .TextMatrix(.Rows - 1, colSecFinicio) = ""
'                .TextMatrix(.Rows - 1, colSecFtermin) = ""
'            Else
'                .TextMatrix(.Rows - 1, colSecCodDir) = ClearNull(aplRST!sec_cod_direccion)
'                .TextMatrix(.Rows - 1, colSecCodGer) = ClearNull(aplRST!sec_cod_gerencia)
'                .TextMatrix(.Rows - 1, colSecCodSec) = ClearNull(aplRST!sec_cod_sector)
'                .TextMatrix(.Rows - 1, colSecCodSub) = ClearNull(aplRST!sec_cod_grupo)
'                .TextMatrix(.Rows - 1, colSecEsfuerzo) = ClearNull(aplRST!sec_horaspresup)
'                .TextMatrix(.Rows - 1, colSecFinicio) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'                .TextMatrix(.Rows - 1, colSecFtermin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'                If secNivel = "GRUP" Then
'                   .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector) & " � " & ClearNull(aplRST!sec_nom_grupo)
'                Else
'                   .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector)
'                End If
'                If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
'                   .TextMatrix(.Rows - 1, colPrioEjecuc) = ClearNull(aplRST!prio_ejecuc)
'                End If
'            End If
'            .TextMatrix(.Rows - 1, colSRTPETIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("fe_ini_plan")), Format(aplRST.Fields.Item("fe_ini_plan"), "yyyymmdd"), "")
'            .TextMatrix(.Rows - 1, colSRTPETFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("fe_fin_plan")), Format(aplRST.Fields.Item("fe_fin_plan"), "yyyymmdd"), "")
'            .TextMatrix(.Rows - 1, colSRTSECIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("sec_fe_ini_plan")), Format(aplRST.Fields.Item("sec_fe_ini_plan"), "yyyymmdd"), "")
'            .TextMatrix(.Rows - 1, colSRTSECFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.Item("sec_fe_fin_plan")), Format(aplRST.Fields.Item("sec_fe_fin_plan"), "yyyymmdd"), "")
'            .TextMatrix(.Rows - 1, colPetClass) = IIf(ClearNull(aplRST!cod_clase) = "SINC", "-", ClearNull(aplRST!cod_clase))
'            .TextMatrix(.Rows - 1, colPetImpTech) = ClearNull(aplRST!pet_imptech)
'            '.TextMatrix(.Rows - 1, colPetSOx) = IIf(aplRST!pet_sox001 = 1, 1, 0)    ' add -003- a.
'            .TextMatrix(.Rows - 1, colRegulatorio) = IIf(IsNull(aplRST!pet_regulatorio), "-", ClearNull(aplRST!pet_regulatorio))    ' add -008- a.
'            .TextMatrix(.Rows - 1, colPetHist) = "L"                                ' add -012- a.
'            .TextMatrix(.Rows - 1, colPetBP) = ClearNull(aplRST!cod_bpar)
'            .TextMatrix(.Rows - 1, colPetCategoria) = ClearNull(aplRST!categoria)                               ' add -024- a.
'            .TextMatrix(.Rows - 1, colPetPuntuacion) = Format(ClearNull(aplRST!puntuacion), "###0.000")         ' add -025- a.
'            '{ add -027- a.
'            .TextMatrix(.Rows - 1, colPetBPNom) = ClearNull(aplRST!nom_bpar)
'            .TextMatrix(.Rows - 1, colPetRGP) = ClearNull(aplRST!cod_BPE)
'            .TextMatrix(.Rows - 1, colPetRGPNom) = ClearNull(aplRST!nom_BPE)
'            '}
'            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)                       ' add -028- b.
'        End With
'        aplRST.MoveNext
'        DoEvents
'    Loop
finx:
    grdDatos.visible = True    ' del -012- a.
    Call Puntero(False)
    '{ add -011- a.
    Call Status("Listo." & IIf(lPeticiones_Listadas > 0, " " & lPeticiones_Listadas & " peticiones listadas.", ""))
    LockProceso (False)
    cmdExportar.visible = False
    'cmdChgBP.Enabled = False
    'cmdAgrupar.Enabled = False
    'cmdPrintPet.Enabled = False
    cmdVisualizar.visible = True
    If grdDatos.Rows > 1 Then cmdExportar.visible = True
    LockProceso (False)
    flgEnCarga = False
    '{ add -004- a.
    If bGridOverflow And bRegenerate Then
        cmdCerrar_Click
    End If
    '}
    Exit Sub
ErrHandler:
    Dim sErrMenssage As String
    
    Select Case Err.Number
        Case 30006      ' No queda memoria para seguir agregando filas al MSFlexGrid
            bGridOverflow = True
            sErrMenssage = "La consulta que intenta generar es demasiado grande (excede la cantidad de " & Trim(Format(lRecCount, "###,###,###")) & " filas)." & vbCrLf
            sErrMenssage = sErrMenssage & "�Desea volver a generar la consulta agregando restricciones para devolver una menor cantidad de filas?"
            If MsgBox(sErrMenssage, vbInformation + vbYesNo, "Consulta demasiado grande") = vbYes Then
                bRegenerate = True
                Resume Next
            Else
                bRegenerate = False
                MsgBox "IMPORTANTE" & vbCrLf & "La informaci�n mostrada en pantalla ser� parcial. No se ha podido cargar todo el conjunto de resultados esperado.", vbExclamation + vbOKOnly, "Resultado parcial"
                Me.Caption = Me.Tag & " (" & Trim(CStr(Format(lRecCount, "###,###,###"))) & " filas)"
                Resume Next
            End If
        Case 3265       ' No se encontr� el elemento en la colecci�n que corresponde con el nombre o el ordinal pedido.
            Resume Next
        Case Else
            Call Puntero(False)
            MsgBox Err.DESCRIPTION & " (" & Err.Number & ")", vbExclamation + vbOKOnly
    End Select
End Sub

Private Sub cboEstadoPet_Click()
    If cboEstadoPet.ListIndex = 0 Then
        imgAlertaFiltroEstados.visible = True
    Else
        imgAlertaFiltroEstados.visible = False
    End If
End Sub

Private Sub Command1_Click()
    'Call QuitarHistorialRepetido
    'Call RecalcularPuntaje
    'Call FinalizarGrupoHomologador
    'Call FinalizarPeticiones                    ' Ultima corrida el 24.02.17 (FJS)
    Call CancelarPeticiones
    'Call CargarProyecto
    'Call Depuracion_Solicitante
    'Call Depuracion_Cancelacion
    'Call CompletarPalancas
    'Call RecalcularValoracion
    'Call CompletarCheckNoCargaBeneficios
    'Call BorrarMensajesViejos
    'Call EliminarPeticionesEnviadas_terminal
    'Call LimpiarHistorial
    'Call QuitarBeneficios
    'Call TranspasoDeRecursos
    
    'Call ImportarPeticionesProduccion
    
'    Call FinalizarPeticionesPato_Criterio1                   ' NUEVO
'    Call FinalizarPeticionesPato_Criterio2                   ' NUEVO
'    Call FinalizarPeticionesPato_Criterio3                   ' NUEVO
'    Call FinalizarPeticionesPato_Criterio4
'    Call VerificacionLogins
'    Call BajarAdjuntos
    
    'MsgBox "Nada implementado!"
End Sub

Private Sub RecalcularPuntaje()
    Dim sFile As String
    Dim vPeticionNroInterno() As Long
    Dim i As Long
    
    ' TODO: lo que hay que hacer es corregir en la tabla PeticionBeneficios, los valores que se guardaron en el campo valor.
    ' revisar porque por alg�n motivo se estan duplicando con cada corrida...
    
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroInterno = CargarArchivo(sFile)
            
            For i = 0 To UBound(vPeticionNroInterno)
                Call Status("Procesando... " & i + 1)
                Call sp_UpdatePetField2(vPeticionNroInterno(i), "PUNTAJE", Null, Null, Null)
                DoEvents
            Next i
        End If
    End If
    Erase vPeticionNroInterno
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

Private Sub QuitarHistorialRepetido()
    Dim sFile As String
    Dim vPeticionNroInterno() As Long
    Dim vHistorialNroInterno() As Long
    Dim vHistorialNroInternoABorrar() As Long
    Dim i As Long
    Dim J As Long
    Dim k As Long
    
    ' TODO: lo que hay que hacer es corregir en la tabla PeticionBeneficios, los valores que se guardaron en el campo valor.
    ' revisar porque por alg�n motivo se estan duplicando con cada corrida...
    
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Puntero(True)
            Call Status("Procesando... ")
            vPeticionNroInterno = CargarArchivo(sFile)
            
            For i = 0 To UBound(vPeticionNroInterno)        ' Por petici�n
                Call Status("Procesando... " & i + 1 & " (" & vPeticionNroInterno(i) & ")")
                Erase vHistorialNroInterno
                J = 0
                If sp_GetHistorial(vPeticionNroInterno(i), Null) Then
                    Do While Not aplRST.EOF
                        If ClearNull(aplRST.Fields!cod_evento) = "PCHGPRI1" Then
                            ReDim Preserve vHistorialNroInterno(J)
                            vHistorialNroInterno(J) = aplRST.Fields!hst_nrointerno
                            J = J + 1
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
                
                If UBound(vHistorialNroInterno) > 0 Then
                    Dim posicion As Integer
                    Dim Texto As String
                    Dim number1 As Long
                    Dim number2 As Long
                    
                    J = 0
                    
                    Erase vHistorialNroInternoABorrar
                    For k = 0 To UBound(vHistorialNroInterno)
                        'Debug.Print vHistorialNroInterno(k)
                        Call sp_GetHistorialMemo(vHistorialNroInterno(k))
                        If aplRST.State = adStateOpen Then
                            If aplRST.RecordCount > 0 Then aplRST.MoveFirst
                            Do While Not aplRST.EOF
                                'Debug.Print ClearNull(aplRST.Fields!mem_texto)
                                If InStr(1, ClearNull(aplRST.Fields!mem_texto), "Asignaci�n inicial de priorizaci�n", vbTextCompare) > 0 Then
                                    posicion = InStr(1, ClearNull(aplRST.Fields!mem_texto), "Asignaci�n inicial de priorizaci�n", vbTextCompare) + 34 + 1
                                    Texto = Mid(ClearNull(aplRST.Fields!mem_texto), posicion)
                                    If IsNumeric(Mid(Texto, 1, InStr(1, Texto, "(", vbTextCompare) - 1)) Then
                                        number1 = Mid(Texto, 1, InStr(1, Texto, "(", vbTextCompare) - 1)
                                        number2 = Replace(Mid(Texto, InStr(1, Texto, "(", vbTextCompare) + 6 + 1), ").", "", 1)
                                        If number1 = number2 Then
                                            ReDim Preserve vHistorialNroInternoABorrar(J)
                                            vHistorialNroInternoABorrar(J) = aplRST.Fields!hst_nrointerno
                                            J = J + 1
                                        End If
                                    End If
                                End If
                                aplRST.MoveNext
                                DoEvents
                            Loop
                        End If
                    Next k
                    
                    If J > 0 Then
                        For k = 0 To UBound(vHistorialNroInternoABorrar)
                            Call sp_DeleteHistorialPet(vPeticionNroInterno(i), vHistorialNroInternoABorrar(k))
                        Next k
                    Else
                        Debug.Print "No se proceso " & vPeticionNroInterno(i)
                    End If
                    DoEvents
                End If
            Next i
        End If
    End If
    
    Erase vPeticionNroInterno
    Erase vHistorialNroInternoABorrar
    Erase vHistorialNroInterno
    
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub

