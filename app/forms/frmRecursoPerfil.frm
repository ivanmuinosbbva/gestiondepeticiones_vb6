VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRecursoPerfil 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "PERFILES DEL RECURSO"
   ClientHeight    =   5625
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   8910
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   8910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame pnlBotones 
      Height          =   5625
      Left            =   7560
      TabIndex        =   20
      Top             =   -30
      Width           =   1275
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Asigna un nuevo perfil al Recurso"
         Top             =   3030
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el nivel del perfil seleccionado"
         Top             =   3540
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Perfil del Recurso"
         Top             =   4050
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   5070
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   4560
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3345
      Left            =   0
      TabIndex        =   1
      Top             =   2250
      Width           =   7545
      Begin VB.ComboBox cboPerfil 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmRecursoPerfil.frx":0000
         Left            =   945
         List            =   "frmRecursoPerfil.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   270
         Width           =   4695
      End
      Begin VB.ComboBox cboGrupo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   945
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   2880
         Width           =   6465
      End
      Begin VB.ComboBox cboDireccion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   945
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1800
         Width           =   6465
      End
      Begin VB.ComboBox cboGerencia 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   945
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   2160
         Width           =   6465
      End
      Begin VB.ComboBox cboSector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   945
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   2520
         Width           =   6465
      End
      Begin VB.Frame fraNivel 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1600
         Left            =   5880
         TabIndex        =   3
         Top             =   120
         Width           =   1545
         Begin VB.OptionButton OptNivel 
            Caption         =   "BBVA"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   150
            TabIndex        =   8
            Top             =   360
            Width           =   1000
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   150
            TabIndex        =   7
            Top             =   570
            Width           =   1000
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   150
            TabIndex        =   6
            Top             =   810
            Width           =   1000
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   3
            Left            =   150
            TabIndex        =   5
            Top             =   1050
            Width           =   1000
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   4
            Left            =   150
            TabIndex        =   4
            Top             =   1290
            Width           =   1000
         End
         Begin VB.Label LblDatosRec 
            AutoSize        =   -1  'True
            Caption         =   " Nivel del perfil "
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   90
            TabIndex        =   9
            Top             =   30
            Width           =   1275
         End
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Perfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   135
         TabIndex        =   19
         Top             =   300
         Width           =   435
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   135
         TabIndex        =   17
         Top             =   2948
         Width           =   510
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   1868
         Width           =   780
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   2228
         Width           =   750
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   135
         TabIndex        =   12
         Top             =   2588
         Width           =   555
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   30
         Visible         =   0   'False
         Width           =   105
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   2220
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   7545
      _ExtentX        =   13309
      _ExtentY        =   3916
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   1
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmRecursoPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 28.04.2008 - Se agrega el control de selecci�n de perfil v�lido porque ocurre que si entramos a modificar directamente cuando entramos a la pantalla, se guarda un registro con los datos del recurso pero con perfil "en blanco".
' -004- a. FJS 13.08.2008 - Se agrega un nuevo perfil para Catalogadores.
' -005- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -006- a. FJS 16.04.2009 - Se agrega el perfil de analista ejecutor.
' -007- a. FJS 19.10.2009 - Se corrige para mostrar correctamente todos los datos.
' -007- b. FJS 26.02.2010 - Bug: estaba trayendo todos los perfiles en vez del modificado.
' -008- a. FJS 19.07.2010 - Planificaci�n DYD: Se agrega el perfil para Oficina de Proyecto.
' -009- GMT01  30.05.2019 - Se habilitan los botones solo para el perfil de seguridad informatica

Option Explicit

Const colCODPERFIL = 0
Const colNomPerfil = 1
Const colCODNIVEL = 2
Const colNOMNIVEL = 3
Const colCODAREA = 4
Const colNOMAREA = 5
Const colFLGAREA = 6
Dim sOpcionSeleccionada As String
Dim xDire, xGere, xSect, xGrup As String

Private Sub Form_Load()
    Call InicializarCombos
    Call InicializarPantalla
    Call IniciarScroll(grdDatos)      ' add -005- a.
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Me.Caption = "Perfiles del Recurso: " & frmRecurso.txtNomRecurso.text
    Call LimpiarForm(Me)
    Call HabilitarBotones(0)
End Sub

Private Sub cboPerfil_Click()
    Dim aux As String
    aux = CodigoCombo(cboPerfil)
    Select Case aux
        Case "CDIR"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = False
            OptNivel(4).Enabled = False
            OptNivel(1).value = True
            Me.fraNivel.Enabled = True
        Case "CGRU", "CHRS", "ANAL", "CHSF"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = True
            OptNivel(1).value = False
            OptNivel(2).value = False
            OptNivel(3).value = False
            OptNivel(4).value = False
            OptNivel(4).value = True
            Me.fraNivel.Enabled = True
        Case "CSEC"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = True
            OptNivel(1).value = False
            OptNivel(2).value = False
            OptNivel(3).value = False
            OptNivel(4).value = False
            OptNivel(3).value = True
            Me.fraNivel.Enabled = True
        Case "CGCI"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = False
            OptNivel(4).Enabled = False
            OptNivel(1).value = False
            OptNivel(2).value = False
            OptNivel(3).value = False
            OptNivel(4).value = False
            OptNivel(2).value = True
            Me.fraNivel.Enabled = True
        Case "SOLI", "REFE", "SUPE", "AUTO"
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = False
            OptNivel(1).value = False
            OptNivel(2).value = False
            OptNivel(3).value = False
            OptNivel(4).value = False
            OptNivel(3).value = True
            Me.fraNivel.Enabled = True
        Case Else
            OptNivel(0).value = True
            OptNivel(0).Enabled = False
            OptNivel(1).Enabled = False
            OptNivel(2).Enabled = False
            OptNivel(3).Enabled = False
            OptNivel(4).Enabled = False
            Me.fraNivel.Enabled = False
    End Select
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_UpdateRecursoPerfil(frmRecurso.txtCodRecurso.text, CodigoCombo(Me.cboPerfil), verNivel(), verArea()) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdateRecursoPerfil(frmRecurso.txtCodRecurso.text, CodigoCombo(Me.cboPerfil), verNivel(), verArea()) Then
                    With grdDatos
                        If .RowSel > 0 Then
                            If sp_GetRecursoPerfil(frmRecurso.txtCodRecurso.text, CodigoCombo(Me.cboPerfil)) Then   ' upd -007- b.
                                .TextMatrix(.RowSel, colCODNIVEL) = ClearNull(aplRST!cod_nivel)
                                .TextMatrix(.RowSel, colCODAREA) = ClearNull(aplRST!cod_area)
                                .TextMatrix(.RowSel, colNOMNIVEL) = getDescNivel(.TextMatrix(.RowSel, colCODNIVEL))
                                .TextMatrix(.RowSel, colNOMAREA) = ClearNull(aplRST!nom_area)
                            End If
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeleteRecursoPerfil(frmRecurso.txtCodRecurso.text, CodigoCombo(Me.cboPerfil)) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            'GMT01 - INI
            'cmdAgregar.Enabled = True
            'cmdModificar.Enabled = True
            'cmdEliminar.Enabled = True
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            
            If InPerfil("ASEG") Then
               cmdAgregar.Enabled = True
               cmdModificar.Enabled = True
               cmdEliminar.Enabled = True
            End If
            'GMT01 - FIN
            
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            cboPerfil.ListIndex = cboPerfil.ListIndex
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    cboDireccion.ListIndex = PosicionCombo(Me.cboDireccion, xDire, True)
                    cboGerencia.ListIndex = PosicionCombo(Me.cboGerencia, xGere, True)
                    cboSector.ListIndex = PosicionCombo(Me.cboSector, xSect, True)
                    cboGrupo.ListIndex = PosicionCombo(Me.cboGrupo, xGrup, True)
                    cboPerfil.ListIndex = 0
                    cboPerfil.Enabled = True
                    'cboDireccion.Enabled = True
                    'cboGerencia.Enabled = True
                    'cboSector.Enabled = True
                    'cboGrupo.Enabled = True
                    fraDatos.Enabled = True
                    'OptNivel(4) = True
                    cboPerfil.Enabled = True
                    cboPerfil.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = True
                    'cboDireccion.Enabled = True
                    'cboGerencia.Enabled = True
                    'cboSector.Enabled = True
                    'cboGrupo.Enabled = True
                    cboPerfil.ListIndex = cboPerfil.ListIndex
                    cboPerfil.Enabled = False
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    
    '{ add -003- a.
    If cboPerfil.ListIndex = -1 Then
        MsgBox "Debe seleccionar un perfil v�lido para el recurso.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    '}
    
    If verNivel() = "" Then
        MsgBox "Debe completar nivel", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If verArea() = "" Then
        MsgBox "Debe completar �rea", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If OptNivel(1).value = True Then
        If Not valDireccionHabil(CodigoCombo(cboDireccion, True)) Then
            MsgBox "La direcci�n est� marcada como inhabilitada", vbExclamation + vbOKOnly
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If OptNivel(2).value = True Then
        If Not valGerenciaHabil(CodigoCombo(cboGerencia, True)) Then
            MsgBox "La gerencia est� marcada como inhabilitada", vbExclamation + vbOKOnly
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If OptNivel(3).value = True Then
        If Not valSectorHabil(CodigoCombo(cboSector, True)) Then
            MsgBox "El sector est� marcado como inhabilitado", vbExclamation + vbOKOnly
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If OptNivel(4).value = True Then
        If Not valGrupoHabil(CodigoCombo(cboGrupo, True)) Then
            MsgBox "El grupo est� marcado como inhabilitado", vbExclamation + vbOKOnly
            CamposObligatorios = False
            Exit Function
        End If
    End If
End Function

Private Sub CargarGrid()
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colCODPERFIL) = "Perfil"
        .TextMatrix(0, colNomPerfil) = ""
        .TextMatrix(0, colCODNIVEL) = "Nivel"
        .TextMatrix(0, colNOMNIVEL) = ""
        .TextMatrix(0, colCODAREA) = "Area"
        .TextMatrix(0, colFLGAREA) = "Habil"
        .ColWidth(colCODPERFIL) = 800: .ColAlignment(colCODPERFIL) = 2
        .ColWidth(colNomPerfil) = 2000: .ColAlignment(colNomPerfil) = 2
        .ColWidth(colCODNIVEL) = 800: .ColAlignment(colCODNIVEL) = 2
        .ColWidth(colNOMNIVEL) = 1000: .ColAlignment(colNOMNIVEL) = 2
        .ColWidth(colCODAREA) = 800: .ColAlignment(colCODAREA) = 2
        .ColWidth(colNOMAREA) = 5000: .ColAlignment(colNOMAREA) = 2
        .ColWidth(colFLGAREA) = 600: .ColAlignment(colFLGAREA) = 2
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetRecursoPerfil(frmRecurso.txtCodRecurso.text, Null) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODPERFIL) = ClearNull(aplRST!cod_perfil)
            .TextMatrix(.Rows - 1, colNomPerfil) = ClearNull(aplRST!nom_perfil)
            .TextMatrix(.Rows - 1, colCODNIVEL) = ClearNull(aplRST!cod_nivel)
            .TextMatrix(.Rows - 1, colNOMNIVEL) = getDescNivel(.TextMatrix(.Rows - 1, colCODNIVEL))
            .TextMatrix(.Rows - 1, colCODAREA) = ClearNull(aplRST!cod_area)
            .TextMatrix(.Rows - 1, colNOMAREA) = ClearNull(aplRST!nom_area)
            .TextMatrix(.Rows - 1, colFLGAREA) = ClearNull(aplRST!flg_area)
        End With
        aplRST.MoveNext
    Loop
'    'aplRST.Close
'    '{ add -002- a.
'    Dim i As Long
'
'    With grdDatos
'        .BackColorFixed = Me.BackColor
'        .BackColorSel = RGB(58, 110, 165)
'        .BackColorBkg = Me.BackColor
'        .Font.name = "Tahoma"
'        .Font.Size = 8
'        .Row = 0
'        For i = 0 To .Cols - 1
'            .Col = i
'            .CellFontBold = True
'        Next i
'        .FocusRect = flexFocusNone
'    End With
'    '}
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Private Sub MostrarSeleccion()
    Dim sAux As String
    
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colCODPERFIL) <> "" Then
                .ColSel = 5
                ' ABM Recursos
                sAux = .TextMatrix(.RowSel, colCODNIVEL)
                cboPerfil.ListIndex = PosicionCombo(Me.cboPerfil, .TextMatrix(.RowSel, colCODPERFIL))
                Select Case sAux
                Case "BBVA"
                   OptNivel(0) = True
                Case "DIRE"
                   OptNivel(1) = True
                   cboDireccion.ListIndex = PosicionCombo(Me.cboDireccion, .TextMatrix(.RowSel, colCODAREA), True)
                Case "GERE"
                   OptNivel(2) = True
                   cboGerencia.ListIndex = PosicionCombo(Me.cboGerencia, .TextMatrix(.RowSel, colCODAREA), True)
                Case "SECT"
                   OptNivel(3) = True
                   cboSector.ListIndex = PosicionCombo(Me.cboSector, .TextMatrix(.RowSel, colCODAREA), True)
                Case "GRUP"
                   OptNivel(4) = True
                   cboGrupo.ListIndex = PosicionCombo(Me.cboGrupo, .TextMatrix(.RowSel, colCODAREA), True)
                End Select
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Sub InicializarCombos()
    Dim i As Integer
    
    If sp_GetRecurso(frmRecurso.txtCodRecurso.text, "R") Then
        xDire = ClearNull(aplRST!cod_direccion)
        xGere = ClearNull(aplRST!cod_gerencia)
        xSect = ClearNull(aplRST!cod_sector)
        xGrup = ClearNull(aplRST!cod_grupo)
        aplRST.Close
    End If

    Call Status("Cargando perfiles...")
    If sp_GetPerfil(Null, Null) Then
        Do While Not aplRST.EOF
            cboPerfil.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando direcciones...")
    If sp_GetDireccion("") Then
        Do While Not aplRST.EOF
            cboDireccion.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando gerencias...")
    If sp_GetGerenciaXt("", "") Then
        Do While Not aplRST.EOF
            cboGerencia.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & Space(200) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando sectores...")
    If sp_GetSectorXt(Null, Null, Null) Then
        Do While Not aplRST.EOF
            cboSector.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & Space(200) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando grupos...")
    If sp_GetGrupoXt("", "") Then
        Do While Not aplRST.EOF
            cboGrupo.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & Space(200) & "||" & aplRST(0)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboDireccion.ListIndex = PosicionCombo(Me.cboDireccion, xDire, True)
    cboGerencia.ListIndex = PosicionCombo(Me.cboGerencia, xGere, True)
    cboSector.ListIndex = PosicionCombo(Me.cboSector, xSect, True)
    cboGrupo.ListIndex = PosicionCombo(Me.cboGrupo, xGrup, True)
    Call Status("Listo.")
End Sub


Private Sub OptNivel_Click(Index As Integer)
    cboDireccion.visible = False
    cboGerencia.visible = False
    cboSector.visible = False
    cboGrupo.visible = False
    cboDireccion.Enabled = False
    cboGerencia.Enabled = False
    cboSector.Enabled = False
    cboGrupo.Enabled = False
    
    Select Case Index
        Case 1
            cboDireccion.visible = True
            cboDireccion.Enabled = True
            If cboDireccion.ListIndex = -1 Then
                cboDireccion.ListIndex = 0
            End If
        Case 2
            cboGerencia.visible = True
            cboGerencia.Enabled = True
            If cboGerencia.ListIndex = -1 Then
                cboGerencia.ListIndex = 0
            End If
        Case 3
            cboSector.visible = True
            cboSector.Enabled = True
            If cboSector.ListIndex = -1 Then
                cboSector.ListIndex = 0
            End If
        Case 4
            cboGrupo.visible = True
            cboGrupo.Enabled = True
            If cboGrupo.ListIndex = -1 Then
                cboGrupo.ListIndex = 0
            End If
    End Select
End Sub


Function verNivel() As String
    verNivel = ""
    If OptNivel(0).value = True Then
         verNivel = "BBVA"
    End If
    If OptNivel(1).value = True Then
         verNivel = "DIRE"
    End If
    If OptNivel(2).value = True Then
         verNivel = "GERE"
    End If
    If OptNivel(3).value = True Then
         verNivel = "SECT"
    End If
    If OptNivel(4).value = True Then
         verNivel = "GRUP"
    End If
End Function

Function verArea() As String
    verArea = ""
    If OptNivel(0).value = True Then
         verArea = "BBVA"
    End If
    If OptNivel(1).value = True Then
         verArea = CodigoCombo(Me.cboDireccion, True)
    End If
    If OptNivel(2).value = True Then
         verArea = CodigoCombo(Me.cboGerencia, True)
    End If
    If OptNivel(3).value = True Then
         verArea = CodigoCombo(Me.cboSector, True)
    End If
    If OptNivel(4).value = True Then
         verArea = CodigoCombo(Me.cboGrupo, True)
    End If
End Function

'{ add -005- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
