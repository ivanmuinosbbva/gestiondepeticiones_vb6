VERSION 5.00
Begin VB.Form frmPeticionesCargaInforme 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nuevo informe de homologación"
   ClientHeight    =   2640
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6975
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesCargaInforme.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2640
   ScaleWidth      =   6975
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraInforme 
      Caption         =   " Datos para el nuevo informe "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   6855
      Begin VB.TextBox txtClasePet 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   8
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox txtTipoPet 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   7
         Top             =   360
         Width           =   1095
      End
      Begin VB.ComboBox cboTipoProceso 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1440
         Width           =   2055
      End
      Begin VB.ComboBox cboInfoPlantilla 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1080
         Width           =   4575
      End
      Begin VB.Label lblInforme 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   6
         Top             =   765
         Width           =   390
      End
      Begin VB.Label lblInforme 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   5
         Top             =   405
         Width           =   300
      End
      Begin VB.Label lblInforme 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de proceso:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   1500
         Width           =   1200
      End
      Begin VB.Label lblInforme 
         AutoSize        =   -1  'True
         Caption         =   "Plantilla:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   1140
         Width           =   600
      End
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   60
      TabIndex        =   9
      Top             =   1980
      Width           =   6855
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4920
         TabIndex        =   11
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5880
         TabIndex        =   10
         Top             =   180
         Width           =   885
      End
   End
End
Attribute VB_Name = "frmPeticionesCargaInforme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public PlantillaId As Long
'Public sPetTipo As String
'Public sPetClase As String

Private Sub Form_Load()
    Call InicializarCombos
End Sub

Private Sub InicializarCombos()
    Call setHabilCtrl(txtTipoPet, "DIS")
    Call setHabilCtrl(txtClasePet, "DIS")
    'txtTipoPet = sPetTipo
    'txtClasePet = sPetClase
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        txtTipoPet = ClearNull(aplRST.Fields!cod_tipo_peticion)
        txtClasePet = ClearNull(aplRST.Fields!cod_clase)
    End If
    
    With cboTipoProceso
        .Clear
        .AddItem "Completo" & ESPACIOS & "||C"
        .AddItem "Reducido" & ESPACIOS & "||R"
    End With
    Call setHabilCtrl(cboTipoProceso, "DIS")

    With cboInfoPlantilla
        .Clear
        If sp_GetInfoproc(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!infoprot_id) & ": " & ClearNull(aplRST.Fields!infoprot_nom) & " - Versión: " & ClearNull(aplRST.Fields!infoprot_vermaj) & "." & ClearNull(aplRST.Fields!infoprot_vermin) & ESPACIOS & "||" & ClearNull(aplRST.Fields!infoprot_tipo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

Private Sub cboInfoPlantilla_Click()
    cboTipoProceso.ListIndex = PosicionCombo(cboTipoProceso, CodigoCombo(cboInfoPlantilla, True), True)
End Sub

Private Sub cmdAceptar_Click()
    Call Aceptar
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    PlantillaId = 0
    Unload Me
End Sub

Private Sub Aceptar()
    If cboInfoPlantilla.ListIndex > -1 Then
        PlantillaId = CodigoCombo(cboInfoPlantilla)
    Else
        MsgBox "Debe seleccionar una plantilla.", vbExclamation + vbOKOnly
    End If
End Sub
