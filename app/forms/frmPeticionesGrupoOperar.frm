VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPeticionesGrupoOperar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cambio Estado Grupo Interviniente"
   ClientHeight    =   6690
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   8310
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6690
   ScaleWidth      =   8310
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraEstadoPlanificacion 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   60
      TabIndex        =   42
      Top             =   1560
      Width           =   8175
      Begin VB.ComboBox cboAccion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesGrupoOperar.frx":0000
         Left            =   1860
         List            =   "frmPeticionesGrupoOperar.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   46
         Top             =   240
         Width           =   5625
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Posibles estados:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   45
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Colocar en estado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   44
         Top             =   255
         Width           =   1560
      End
      Begin VB.Label lblAccionesPosteriores 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   555
         Left            =   1860
         TabIndex        =   43
         Top             =   600
         Width           =   6195
         WordWrap        =   -1  'True
      End
   End
   Begin MSComctlLib.ImageList imgAccion 
      Left            =   240
      Top             =   5940
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesGrupoOperar.frx":0004
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesGrupoOperar.frx":059E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesGrupoOperar.frx":0B38
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraSector 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3270
      Left            =   60
      TabIndex        =   11
      Top             =   2805
      Width           =   8190
      Begin VB.ComboBox cboMotivo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesGrupoOperar.frx":10D2
         Left            =   1365
         List            =   "frmPeticionesGrupoOperar.frx":10D4
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1145
         Width           =   6705
      End
      Begin VB.TextBox obsSolic 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   1365
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   1500
         Width           =   6705
      End
      Begin VB.TextBox obsResp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   1365
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         Top             =   2295
         Width           =   6705
      End
      Begin AT_MaskText.MaskText txtHoras 
         Height          =   315
         Left            =   1365
         TabIndex        =   0
         Top             =   150
         Width           =   885
         _ExtentX        =   1561
         _ExtentY        =   556
         MaxLength       =   5
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   5
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtFechaInicio 
         Height          =   315
         Left            =   1365
         TabIndex        =   1
         Top             =   478
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaIniReal 
         Height          =   315
         Left            =   3720
         TabIndex        =   3
         Top             =   480
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaFinReal 
         Height          =   315
         Left            =   3720
         TabIndex        =   4
         Top             =   810
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaPasajeProduccion 
         Height          =   315
         Left            =   6600
         TabIndex        =   5
         Top             =   480
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaTermin 
         Height          =   315
         Left            =   1365
         TabIndex        =   2
         Top             =   806
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaFinSuspension 
         Height          =   315
         Left            =   6600
         TabIndex        =   6
         Top             =   810
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblFechaFinSuspension 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin. Suspen."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   5280
         TabIndex        =   41
         Top             =   870
         Width           =   1125
      End
      Begin VB.Label lblMotivo 
         AutoSize        =   -1  'True
         Caption         =   "Motivo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   40
         Top             =   1215
         Width           =   585
      End
      Begin VB.Label lblFechaPasajeProduccion 
         AutoSize        =   -1  'True
         Caption         =   "F.Pasaje Prod."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   5280
         TabIndex        =   37
         Top             =   555
         Width           =   1185
      End
      Begin VB.Label lblResp 
         Caption         =   "Observaci�n estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   120
         TabIndex        =   36
         Top             =   1620
         Width           =   1230
      End
      Begin VB.Label lblSolic 
         Caption         =   "Observaci�n nuevo estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         TabIndex        =   35
         Top             =   2400
         Width           =   1290
      End
      Begin VB.Label lblAccion 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   8010
         TabIndex        =   17
         Top             =   180
         Width           =   45
      End
      Begin VB.Label lblHoras 
         AutoSize        =   -1  'True
         Caption         =   "Hs. Presup."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   210
         Width           =   930
      End
      Begin VB.Label lblFechaInicio 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini Planif"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   555
         Width           =   870
      End
      Begin VB.Label lblFechaTermin 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin Planif"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   870
         Width           =   885
      End
      Begin VB.Label lblFechaFinReal 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin Real"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2880
         TabIndex        =   13
         Top             =   870
         Width           =   795
      End
      Begin VB.Label lblFechaIniReal 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini Real"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   2880
         TabIndex        =   12
         Top             =   555
         Width           =   780
      End
   End
   Begin VB.Frame pnlBtnControl 
      Height          =   645
      Left            =   60
      TabIndex        =   38
      Top             =   6000
      Width           =   8190
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7230
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6300
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   795
      Left            =   60
      TabIndex        =   18
      Top             =   0
      Width           =   8190
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   24
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6705
         TabIndex        =   23
         Top             =   480
         Width           =   765
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1305
         TabIndex        =   22
         Top             =   480
         Width           =   3180
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1305
         TabIndex        =   21
         Top             =   240
         Width           =   6705
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   7620
         TabIndex        =   20
         Top             =   480
         Width           =   480
      End
      Begin VB.Label Label6 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   19
         Top             =   480
         Width           =   1200
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   25
      Top             =   720
      Width           =   8190
      Begin VB.Label lblSector 
         Caption         =   "sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   29
         Top             =   180
         Width           =   2745
      End
      Begin VB.Label Label9 
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   28
         Top             =   180
         Width           =   1125
      End
      Begin VB.Label lblEstadoSector 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5565
         TabIndex        =   27
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label4 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   26
         Top             =   180
         Width           =   1200
      End
   End
   Begin VB.Frame Frame3 
      Enabled         =   0   'False
      Height          =   480
      Left            =   60
      TabIndex        =   30
      Top             =   1155
      Width           =   8190
      Begin VB.Label lblGrupo 
         Caption         =   "grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   31
         Top             =   180
         Width           =   2745
      End
      Begin VB.Label Label8 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   34
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoGrupo 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5580
         TabIndex        =   33
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label5 
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   32
         Top             =   180
         Width           =   1125
      End
   End
End
Attribute VB_Name = "frmPeticionesGrupoOperar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'GMT01 - 05/06/2019 - PET 70487 - Se agrega validacion de data
'GMT02 - 23/10/2019 - PET 70881 - Se asigna categoria PRI, para las peticiones normales

Option Explicit

Private Const VALIDACION_ARQUITECTURA = 1
Private Const VALIDACION_SEGURIDADINF = 2
Private Const VALIDACION_DATA = 3  'GMT01

Private Const ESTADO_AVANZA = 2
Private Const ESTADO_RETROCEDE = 3
Private Const ESTADO_TERMINAL = 1

Private Const ACCION_PERFIL_ESTADO_NORMAL = 1
Private Const ACCION_PERFIL_ESTADO_ESPECIAL = 2         ' Acciones por perfil/estado especiales para Seg. Inf. (ER & CA y Tecnolog�a)

Dim sOpcionSeleccionada As String
Dim EstadoSolicitud As String
Dim EstadoSolicitudPorDefecto As String
Dim sDireccion As String, sGerencia As String, sSector As String, sGrupo As String
Dim sCoorSect As String
Dim solGerencia As String
Dim EstadoPeticion As String
Dim NuevoEstadoPeticion As String
Dim EstadoSector As String
Dim NuevoEstadoSector As String
Dim NuevoEstadoGrupo As String
Dim sCategoria As String 'GMT02
Dim Hab_RGyP As Boolean  'GMT02
Dim sTipoPet As String
Dim sClasePet As String
Dim sImpacto As String
Dim sFechaComite
Dim dFechaPedido As Date
Dim xPerfNivel As String
Dim xPerfArea As String
Dim hst_nrointerno_sol
Dim sec_nrointerno_sol
Dim flgAccion As Boolean
Dim lPetNroAsignado As Long
Dim nCodigoMotivo As Integer
Dim bOrigenIncidencia As Boolean

'Dim bSoyGrupoHomologador As Boolean
'Dim cGrupoHomologador As String
'Dim cGrupoHomologadorSector As String
'Dim cGrupoHomologadorEstado As String
'Dim cSectorHomologadorEstado As String
'Dim cSectorHomologadorNuevoEstado As String

Dim sHomoGrupoEstado As String
Dim sHomoSectorEstado As String
Dim sHomoGrupoEstadoNuevo As String
Dim sHomoSectorEstadoNuevo As String

Dim cMensaje As String
Dim cEstadosActivos As String
Dim dFechaControl As Date
Dim cMensajeControl As String
Dim cRegulatorio As String

Dim lEstadoPlanificacion_Peticion As Long

Dim bEsGrupoHomologable As Boolean
Dim bConformeTEST As Boolean
Dim bGrupoEstuvoEnEjecucion As Boolean
Dim bGrupoVieneDeTerminal As Boolean
Dim bExisteHomologacion As Boolean                      ' Indica si existe vinculado a la petici�n el grupo homologador
Dim bPeticionEstuvoEnEjecucion As Boolean               ' Se refiere a la petici�n
Dim dValidacionTecnicaInicio As Date
Dim bRequiereValidacionTecnica As Boolean
Dim bSeCompletoValidacionArquitecura As Boolean         ' Indica si el referente de sistemas respondi� todas las preguntas de validaci�n t�cnica de arquitectura
Dim bSeCompletoValidacionSegInf As Boolean              ' Indica si el referente de sistemas respondi� todas las preguntas de validaci�n t�cnica de seguridad
Dim bSeCompletoValidacionData As Boolean        'GMT01->  Indica si el referente de sistemas respondi� todas las preguntas de validaci�n t�cnica de data
Dim bRequiereConformeArquitectura As Boolean            ' Indica si por las respuestas dadas a la validaci�n, se requiere un conforme expl�cito de arquitectura
Dim bRequiereConformeSeguridad As Boolean               ' Indica si por las respuestas dadas a la validaci�n, se requiere un conforme expl�cito de seguridad
Dim bRequiereConformeData As Boolean            'GMT01->  Indica si por las respuestas dadas a la validaci�n, se requiere un conforme expl�cito de data

'Dim bGrupoHomologadorActivo As Boolean

Dim bHomologacionEnEstadoActivo As Boolean                      ' Si existe homologaci�n para la petici�n actual, indica si est� en estado activo

Dim bEsGrupoTecnologia As Boolean
Dim bEsGrupoSeguridadInformatica As Boolean
Dim bEsPlanificada As Boolean

' Estado del grupo y sector de homologaci�n para esta petici�n
'Dim sHomoGrupoEstado As String
'Dim sHomoSectorEstado As String
'Dim sHomoGrupoEstadoNuevo As String
'Dim sHomoSectorEstadoNuevo As String

'Dim GrupoHomologador_EstadoNew As String
Dim bReplanificacion As Boolean                 ' add -028- b.
Dim fechaPeticionPlanificaInicio As Date
Dim fechaPeticionPlanificaFinal As Date

Private Sub Form_Load()
    Call Puntero(True)
    Call InicializarPantalla
    Call InicializarCombos
    Call Puntero(False)
End Sub

Sub InicializarPantalla()
    Dim auxNro As String
    Me.Tag = ""
    
    Call setHabilCtrl(txtFechaTermin, "DIS")
    Call setHabilCtrl(txtFechaInicio, "DIS")
    Call setHabilCtrl(txtFechaIniReal, "DIS")
    Call setHabilCtrl(txtFechaFinReal, "DIS")
    Call setHabilCtrl(txtHoras, "DIS")
    Call setHabilCtrl(obsSolic, "DIS")
    Call setHabilCtrl(obsResp, "DIS")
    Call setHabilCtrl(txtFechaPasajeProduccion, "DIS")
    Call setHabilCtrl(cboMotivo, "DIS")
    Call setHabilCtrl(txtFechaFinSuspension, "DIS")     ' add -007- a.
   
    xPerfNivel = getPerfNivel(glUsrPerfilActual)
    xPerfArea = getPerfArea(glUsrPerfilActual)
    
    ' Validaci�n t�cnica
    bRequiereValidacionTecnica = False
    If sp_GetVarios("CTRLPET1") Then
        dValidacionTecnicaInicio = CDate(aplRST.Fields!var_fecha)
    End If
    
    'esto es porque puede entrarse desde Grupos genericos sin pasar por sector
    If sp_GetPeticionGrupo(glNumeroPeticion, Null, glGrupo) Then sSector = ClearNull(aplRST!cod_sector)
    
    sTipoPet = ""
    
    With cboMotivo
        If sp_GetMotivos(Null, "S") Then       ' Solicita todos los motivos existentes
            .Clear
            Do While Not aplRST.EOF
                .AddItem Trim(aplRST.Fields!nom_motivo) & ESPACIOS & ESPACIOS & "||" & CStr(aplRST.Fields!cod_motivo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With

    glForzarEvaluar = False
    bRequiereValidacionTecnica = False
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            sImpacto = ClearNull(aplRST!pet_imptech)
            sClasePet = ClearNull(aplRST!cod_clase)
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNro = "S/N"
            Else
                auxNro = ClearNull(aplRST!pet_nroasignado)
                lPetNroAsignado = CLng(auxNro)
            End If
            sFechaComite = aplRST!fe_comite
            dFechaPedido = CDate(aplRST.Fields!fe_pedido)
            lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
            lblPrioridad = ClearNull(aplRST!prioridad)
            EstadoPeticion = ClearNull(aplRST!cod_estado)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
            sTipoPet = ClearNull(aplRST!cod_tipo_peticion)
            sCategoria = ClearNull(aplRST!categoria) 'GMT02
            solGerencia = ClearNull(aplRST!cod_gerencia)
            cRegulatorio = ClearNull(aplRST!pet_regulatorio)
            bEsPlanificada = IIf(ClearNull(aplRST.Fields!pet_prioridad) <> "0", True, False)
            If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then
                If dFechaPedido >= dValidacionTecnicaInicio Then
                    bRequiereValidacionTecnica = True
                End If
            End If
            bOrigenIncidencia = IIf(ClearNull(aplRST.Fields!sgi_incidencia) <> "", True, False)
        End If
    End If
    
    ' Validacion t�cnica
    bRequiereConformeArquitectura = False
    bRequiereConformeSeguridad = False
    bRequiereConformeData = False  'GMT01
    
    bSeCompletoValidacionArquitecura = False
    bSeCompletoValidacionSegInf = False
    bSeCompletoValidacionData = False  'GMT01
    
    If bRequiereValidacionTecnica Then
        If sp_GetPeticionValidacion(glNumeroPeticion, VALIDACION_ARQUITECTURA, Null) Then
            bSeCompletoValidacionArquitecura = True
            Do While Not aplRST.EOF
                If Not (ClearNull(aplRST.Fields!valitemvalor) = "S" Or ClearNull(aplRST.Fields!valitemvalor) = "N") Then
                    bSeCompletoValidacionArquitecura = False
                End If
                If ClearNull(aplRST.Fields!valitemvalor) = "S" Then bRequiereConformeArquitectura = True
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        
        If sp_GetPeticionValidacion(glNumeroPeticion, VALIDACION_SEGURIDADINF, Null) Then
            bSeCompletoValidacionSegInf = True
            Do While Not aplRST.EOF
                If Not (ClearNull(aplRST.Fields!valitemvalor) = "S" Or ClearNull(aplRST.Fields!valitemvalor) = "N") Then
                    bSeCompletoValidacionSegInf = False
                End If
                If ClearNull(aplRST.Fields!valitemvalor) = "S" Then bRequiereConformeSeguridad = True
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        
        'GMT01 - INI
        If sp_GetPeticionValidacion(glNumeroPeticion, VALIDACION_DATA, Null) Then
            bSeCompletoValidacionData = True
            Do While Not aplRST.EOF
                If Not (ClearNull(aplRST.Fields!valitemvalor) = "S" Or ClearNull(aplRST.Fields!valitemvalor) = "N") Then
                    bSeCompletoValidacionData = False
                End If
                If ClearNull(aplRST.Fields!valitemvalor) = "S" Then bRequiereConformeData = True
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        'GMT01 - FIN
    End If
    
    If sp_GetPeticionSector(glNumeroPeticion, sSector) Then
       If Not aplRST.EOF Then
          sec_nrointerno_sol = ClearNull(aplRST!hst_nrointerno_sol)
          lblSector = ClearNull(aplRST!nom_sector)
          lblEstadoSector = ClearNull(aplRST!nom_estado)
          EstadoSector = ClearNull(aplRST!cod_estado)
        End If
    End If
    fIniPlan = Null
    fFinPlan = Null
    fIniReal = Null
    fFinReal = Null
    fPasajeProduccion = Null
    fFechaFinSuspension = Null
    hsPresup = 0
    If sp_GetPeticionGrupo(glNumeroPeticion, sSector, glGrupo) Then
        fIniPlan = aplRST!fe_ini_plan
        fFinPlan = aplRST!fe_fin_plan
        fIniReal = aplRST!fe_ini_real
        fFinReal = aplRST!fe_fin_real
        fPasajeProduccion = aplRST!fe_produccion
        fFechaFinSuspension = aplRST!fe_suspension
        hsPresup = Val(ClearNull(aplRST!horaspresup))
        txtFechaTermin.text = IIf(Not IsNull(fFinPlan), Format(fFinPlan, "dd/mm/yyyy"), "")
        txtFechaInicio.text = IIf(Not IsNull(fIniPlan), Format(fIniPlan, "dd/mm/yyyy"), "")
        txtHoras.text = hsPresup
        txtFechaFinReal.text = IIf(Not IsNull(fFinReal), Format(fFinReal, "dd/mm/yyyy"), "")
        txtFechaIniReal.text = IIf(Not IsNull(fIniReal), Format(fIniReal, "dd/mm/yyyy"), "")
        txtFechaPasajeProduccion.text = IIf(Not IsNull(fPasajeProduccion), Format(fPasajeProduccion, "dd/mm/yyyy"), "")
        txtFechaFinSuspension.text = IIf(Not IsNull(fFechaFinSuspension), Format(fFechaFinSuspension, "dd/mm/yyyy"), "")
        cboMotivo.ListIndex = PosicionCombo(cboMotivo, ClearNull(aplRST!cod_motivo), True): cboMotivo.Tag = ClearNull(aplRST!cod_motivo)
        hst_nrointerno_sol = ClearNull(aplRST!hst_nrointerno_sol)
        EstadoSolicitud = ClearNull(aplRST!cod_estado)
        lblGrupo = ClearNull(aplRST!nom_grupo)
        lblEstadoGrupo = ClearNull(aplRST!nom_estado)
        sGerencia = ClearNull(aplRST!cod_gerencia)
        sDireccion = ClearNull(aplRST!cod_direccion)
        sSector = ClearNull(aplRST!cod_sector)
        sGrupo = ClearNull(aplRST!cod_grupo)
        bEsGrupoHomologable = IIf(ClearNull(aplRST!homo) = "S", True, False)
        bGrupoVieneDeTerminal = IIf(InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", EstadoSolicitud, vbTextCompare) > 0, True, False)
        EstadoSolicitudPorDefecto = ClearNull(aplRST.Fields!cod_estado_default)
    End If
    
    bEsGrupoTecnologia = False
    bEsGrupoSeguridadInformatica = False
    If glTecnoGrupo = glGrupo Then bEsGrupoTecnologia = True
    If glSegInfGrupo = glGrupo Then bEsGrupoSeguridadInformatica = True
    
    ' Determino si la petici�n ya tiene un conforme final a las pruebas del usuario
    bConformeTEST = False
    If sp_GetPeticionConf(glNumeroPeticion, "TEST", Null) Then
        bConformeTEST = True
    End If
    
    ' Determino si el grupo estuvo al menos una vez en estado "En ejecuci�n"
    bGrupoEstuvoEnEjecucion = False
    If sp_GetHistorial(glNumeroPeticion, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!cod_sector) = sSector And ClearNull(aplRST.Fields!cod_grupo) = glGrupo Then
                If ClearNull(aplRST.Fields!gru_estado) = "EJECUC" Then
                    bGrupoEstuvoEnEjecucion = True
                    Exit Do
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    bExisteHomologacion = False
    bHomologacionEnEstadoActivo = False
    sHomoGrupoEstado = ""
    sHomoSectorEstado = ""
    If sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then       ' Determino si existe asignado a la petici�n el grupo Homologaci�n y si existe, cargo las variables de estado para el grupo y sector
        bExisteHomologacion = True
        sHomoGrupoEstado = ClearNull(aplRST.Fields!cod_estado)
        sHomoSectorEstado = ClearNull(aplRST.Fields!sector_cod_estado)
        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sHomoGrupoEstado, vbTextCompare) = 0 Then
            bHomologacionEnEstadoActivo = True
        End If
    End If
    If Val(hst_nrointerno_sol) > 0 Then
        obsSolic.text = sp_GetHistorialMemo(hst_nrointerno_sol)
    Else
        obsSolic.text = ""
    End If
    obsResp.text = ""
End Sub

Sub InicializarCombos()
    Dim flgExtendido As Boolean
    Dim xAccion As String
    Dim filtroAccion As Integer
    
    If bEsGrupoTecnologia Or bEsGrupoSeguridadInformatica Then
        filtroAccion = ACCION_PERFIL_ESTADO_ESPECIAL
    Else
        filtroAccion = ACCION_PERFIL_ESTADO_NORMAL
    End If
    
    flgExtendido = False
    If xPerfNivel = "BBVA" Or _
        xPerfNivel = "DIRE" And sDireccion = xPerfArea Or _
        xPerfNivel = "GERE" And sGerencia = xPerfArea Or _
        xPerfNivel = "SECT" And sSector = xPerfArea Or _
        xPerfNivel = "GRUP" And sGrupo = xPerfArea Then
        'If sp_GetAccionPerfil("GCHGEST", glUsrPerfilActual, EstadoSolicitud, Null, Null) Then
        If sp_GetAccionPerfilEstados(filtroAccion, "GCHGEST", glUsrPerfilActual, EstadoSolicitud, Null, Null) Then
            Do While Not aplRST.EOF
                cboAccion.AddItem aplRST!nom_estnew & ESPACIOS & ESPACIOS & "||" & aplRST!cod_estnew
                If InStr(1, "OPINOK|EVALOK|", ClearNull(aplRST!cod_estnew)) = 0 Then     ' Si el grupo NO puede pasar a los estados "Con opini�n" o "Con evaluaci�n", entonces dejo apagado el flag
                    flgExtendido = True
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End If
    
    If flgExtendido = False Then
        txtFechaTermin.visible = False
        txtFechaInicio.visible = False
        txtFechaIniReal.visible = False
        txtFechaFinReal.visible = False
        txtHoras.visible = False
        lblFechaTermin.visible = False
        lblFechaInicio.visible = False
        lblFechaIniReal.visible = False
        lblFechaFinReal.visible = False
        lblHoras.visible = False
        txtFechaPasajeProduccion.visible = False
        txtFechaFinSuspension.visible = False
        lblFechaPasajeProduccion.visible = False
        lblFechaFinSuspension.visible = False
    End If
    
    If cboAccion.ListCount > 0 Then
        Call setHabilCtrl(cboAccion, "OBL")
        If cboAccion.ListCount = 1 Then
            cboAccion.ListIndex = 0
            Call setHabilCtrl(cboAccion, "DIS")
        Else
            cboAccion.ListIndex = PosicionCombo(cboAccion, EstadoSolicitudPorDefecto, True)
        End If
    Else
        Call setHabilCtrl(cboAccion, "DIS")
    End If
End Sub

Private Sub cboAccion_Click()
    Dim filtroAccion As Integer
    
    If bEsGrupoTecnologia Or bEsGrupoSeguridadInformatica Then
        filtroAccion = ACCION_PERFIL_ESTADO_ESPECIAL
    Else
        filtroAccion = ACCION_PERFIL_ESTADO_NORMAL
    End If
    
    If cboAccion.ListIndex < 0 Then
        MsgBox "Debe seleccionar una acci�n!", vbExclamation + vbOKOnly
        Exit Sub
    End If
    txtFechaTermin.text = IIf(Not IsNull(fFinPlan), Format(fFinPlan, "dd/mm/yyyy"), "")
    txtFechaInicio.text = IIf(Not IsNull(fIniPlan), Format(fIniPlan, "dd/mm/yyyy"), "")
    txtHoras.text = hsPresup
    txtFechaFinReal.text = IIf(Not IsNull(fFinReal), Format(fFinReal, "dd/mm/yyyy"), "")
    txtFechaIniReal.text = IIf(Not IsNull(fIniReal), Format(fIniReal, "dd/mm/yyyy"), "")
    txtFechaPasajeProduccion.text = IIf(Not IsNull(fPasajeProduccion), Format(fPasajeProduccion, "dd/mm/yyyy"), "")
    sOpcionSeleccionada = CodigoCombo(Me.cboAccion, True)
    lblAccionesPosteriores.Caption = ""
    If sp_GetAccionPerfilEstados(filtroAccion, "GCHGEST", glUsrPerfilActual, sOpcionSeleccionada, Null, Null) Then
        Do While Not aplRST.EOF
            lblAccionesPosteriores.Caption = lblAccionesPosteriores.Caption & IIf(lblAccionesPosteriores.Caption = "", ClearNull(aplRST.Fields!nom_estnew), ", " & ClearNull(aplRST.Fields!nom_estnew))
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    Call HabilitarControles
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub cmdConfirmar_Click()
    Dim sMensajeError As String
    Dim bError As Boolean
    
    bError = False
    
    If cboAccion.ListIndex < 0 Then
        MsgBox "Debe seleccionar una acci�n de la lista para continuar.", vbExclamation + vbOKOnly, "Acci�n a realizar"
        Call setHabilCtrl(cmdConfirmar, NORMAL)
        Call setHabilCtrl(cmdCerrar, NORMAL)
        Exit Sub
    End If
    
    If sClasePet = "SINC" And InStr(1, "TERMIN|EJECUC|EJECRK|", sOpcionSeleccionada, vbTextCompare) > 0 Then
        MsgBox "Antes de cambiar al estado seleccionado, la petici�n debe ser clasificada.", vbExclamation + vbOKOnly, "Clasificaci�n de la Petici�n"
        Call setHabilCtrl(cmdConfirmar, NORMAL)
        Call setHabilCtrl(cmdCerrar, NORMAL)
        Exit Sub
    End If
    
    ' Controles de validaci�n t�cnica
    If sOpcionSeleccionada = "EJECUC" And bRequiereValidacionTecnica Then
        If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then
            ' Primero valida la completitud de la validaci�n t�cnica
            'If Not (bSeCompletoValidacionArquitecura And bSeCompletoValidacionSegInf) Then  '-> GMT01
             If Not (bSeCompletoValidacionArquitecura And bSeCompletoValidacionSegInf And bSeCompletoValidacionData) Then  '-> GMT01
                MsgBox "Antes de cambiar al estado Ejecuci�n, el referente" & vbCrLf & "de sistema debe completar la validaci�n t�cnica.", vbExclamation + vbOKOnly, "Validaci�n t�cnica"
                Call setHabilCtrl(cmdConfirmar, NORMAL)
                Call setHabilCtrl(cmdCerrar, NORMAL)
                Exit Sub
            End If
        End If
    End If
    
    Call CambioDeEstado
    
    'GMT02- INI
    Call recupHabilitaciones
    
    If Hab_RGyP = False Then
        If (sOpcionSeleccionada = "EJECUC" Or sOpcionSeleccionada = "PLANOK" Or sOpcionSeleccionada = "PLANIF" Or sOpcionSeleccionada = "ESTIMA") Then
           If (sTipoPet = "NOR" Or sTipoPet = "PRO") And cRegulatorio <> "S" And sCategoria <> "SDA" Then
              Call sp_UpdPeticionPri(glNumeroPeticion, 1)
           End If
        End If
    End If
    'GMT02 - FIN
    
End Sub

Private Function PeticionEstuvoEnEjecucion() As Boolean
    If sp_GetHistorial(glNumeroPeticion, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!pet_estado) = "EJECUC" Then
                bPeticionEstuvoEnEjecucion = True
                Exit Do
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
End Function

Private Sub CambioDeEstado()
    Dim sMsgPeticion As String
    Dim xSituacion As String
    Dim bContinue As Boolean
    Dim cHistorial As String
    Dim NroHistorial As Long
    Dim bControlHomologacion As Byte
        
    Call setHabilCtrl(cmdConfirmar, DISABLE)
    Call setHabilCtrl(cmdCerrar, DISABLE)
    
    bContinue = False
    bControlHomologacion = 0

    If sGerencia = "DESA" Then
        If Not glEsHomologador Then
            If CamposObligatoriosEstandard(bControlHomologacion) And _
               CamposObligatoriosSOx And _
               CamposObligatoriosFechas And _
               CamposObligatoriosHoras And _
               CamposObligatoriosHomologacion Then
                    bContinue = True
            End If
        Else
            If CamposObligatoriosFechas Then
                bContinue = True
            End If
        End If
    Else
        If CamposObligatoriosEstandard(bControlHomologacion) And _
           CamposObligatoriosFechas Then
                bContinue = True
        End If
    End If
    
    ' Si la petici�n NUNCA estuvo en ejecuci�n o si ning�n grupo se encuentra
    ' a�n en Ejecuci�n, entonces solicito confirmaci�n.
    If bContinue Then
        Call PeticionEstuvoEnEjecucion
        If sOpcionSeleccionada = "EJECUC" Then
            If Not glEsHomologador And Not bEsGrupoTecnologia And Not bEsGrupoSeguridadInformatica Then
                If Not EstadoPeticion = "TERMIN" Then
                    If Not bPeticionEstuvoEnEjecucion Then
                        cstmMessageBox.Show 1
                        bContinue = cstmMessageBox.bContinuar
                    End If
                Else
                    bContinue = True
                End If
            Else
                bContinue = True
            End If
        End If
    End If
    
    If bContinue Then
        Call Puntero(True)
        Call Status("Procesando...")
        Select Case bControlHomologacion
            Case 5  ' Controles v�lidos sin observaciones
                cHistorial = "� Fecha de pasaje a Producci�n indicado el " & Format(date, "dd/mm/yyyy") & " : Prevista para el " & txtFechaPasajeProduccion.text & "  �"
                NroHistorial = sp_AddHistorial(glNumeroPeticion, "PCHGPRD", EstadoPeticion, sSector, EstadoSector, glGrupo, EstadoSolicitud, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, Trim(cHistorial))
            Case 6  ' Advertencia 1
                cMensajeControl = "La fecha ingresada de pasaje previsto a Producci�n se encuentra inclu�da dentro de las pr�ximas 48 horas." & vbCrLf & _
                                  "De continuar, ser� asignado de manera autom�tica a la petici�n un No Conforme de Homologaci�n (HOMA)." & vbCrLf & vbCrLf & _
                                  "�Desea continuar o prefiere cancelar para modificar?"
                If MsgBox(cMensajeControl, vbQuestion + vbOKCancel, "Controles de Homologaci�n autom�ticos") = vbOK Then
                    cHistorial = "� Fecha de pasaje a Producci�n indicado el " & Format(date, "dd/mm/yyyy") & " : Prevista para el " & txtFechaPasajeProduccion.text & "  � " & _
                                 "Advertencia: La fecha ingresada se encuentra inclu�da dentro de las pr�ximas 48 horas."
                    NroHistorial = sp_AddHistorial(glNumeroPeticion, "PCHGPRD", EstadoPeticion, sSector, EstadoSector, glGrupo, EstadoSolicitud, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, Trim(cHistorial))
                    ' Se agrega de manera autom�tica un conforme de homologaci�n de tipo OMA
                    If Not sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Then
                        If sp_InsertPeticionConf(glNumeroPeticion, "HOMA.P", "N/A", "� No Conforme de Homologaci�n c/ observaciones mayores �", "1", glLOGIN_ID_REEMPLAZO) Then
                            NroHistorial = sp_AddHistorial(glNumeroPeticion, "A.HOMA.P", EstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "HOMA.P : No conf. parcial Homologaci�n - C/ observ. mayores" & "  --->  " & "Generado autom�ticamente por el sistema.")
                        End If
                    End If
                Else
                    Call Status("Listo.")
                    Call Puntero(False)
                    Call setHabilCtrl(cmdConfirmar, NORMAL)
                    Call setHabilCtrl(cmdCerrar, NORMAL)
                    Exit Sub
                End If
            Case 7  ' Advertencia 2
                cHistorial = "� Fecha de pasaje a Producci�n indicado el " & Format(date, "dd/mm/yyyy") & " : Prevista para el " & txtFechaPasajeProduccion.text & "  � " & _
                             "Advertencia: La fecha ingresada se encuentra inclu�da dentro de las pr�ximas 120 horas."
                NroHistorial = sp_AddHistorial(glNumeroPeticion, "PCHGPRD", EstadoPeticion, sSector, EstadoSector, glGrupo, EstadoSolicitud, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, Trim(cHistorial))
                MsgBox "Advertencia: La fecha de pasaje a Producci�n se encuentra inclu�da dentro de las pr�ximas 120 horas.", vbExclamation + vbOKOnly, "Advertencia"
            Case 8  ' Advertencia 1 + 2
                cMensajeControl = "La fecha ingresada de pasaje previsto a Producci�n se encuentra inclu�da dentro de las pr�ximas 48 horas." & vbCrLf & _
                                  "De continuar, ser� asignado de manera autom�tica a la petici�n un No Conforme de Homologaci�n (OMA)." & vbCrLf & vbCrLf & _
                                  "�Desea continuar o prefiere cancelar para modificar?"
                If MsgBox(cMensajeControl, vbQuestion + vbOKCancel, "Controles de Homologaci�n autom�ticos") = vbOK Then
                    cHistorial = "� Fecha de pasaje a Producci�n indicado el " & Format(date, "dd/mm/yyyy") & " : Prevista para el " & txtFechaPasajeProduccion.text & "  � " & _
                                 "Advertencia: La fecha ingresada se encuentra inclu�da dentro de las pr�ximas 48 horas."
                    If Not sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Then
                        ' Se agrega de manera autom�tica un conforme de homologaci�n de tipo HOMA
                        If sp_InsertPeticionConf(glNumeroPeticion, "HOMA.P", "N/A", "� No Conforme de Homologaci�n c/ observaciones mayores �", "1", glLOGIN_ID_REEMPLAZO) Then
                            NroHistorial = sp_AddHistorial(glNumeroPeticion, "A.HOMA.P", EstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "HOMA.P : No conf. parcial Homologaci�n - C/ observ. mayores" & "  --->  " & "Generado autom�ticamente por el sistema.")
                        End If
                    End If
                Else
                    Call Status("Listo.")
                    Call Puntero(False)
                    Call setHabilCtrl(cmdConfirmar, NORMAL)
                    Call setHabilCtrl(cmdCerrar, NORMAL)
                    Exit Sub
                End If
        End Select
        
        bReplanificacion = False
        xSituacion = ""
        Select Case sOpcionSeleccionada
            Case "OPINOK": obsResp.text = "� OPINION � " & vbCrLf & ClearNull(obsResp.text)
            Case "EVALOK": obsResp.text = "� EVALUACION � " & vbCrLf & ClearNull(obsResp.text)
            Case "ESTIOK"
                obsResp.text = "� ESTIMACION Horas Planif: " & txtHoras & IIf(ClearNull(obsResp.text) = "", " hs. �", " hs. � " & ClearNull(obsResp.text))
                sMsgPeticion = "El grupo pasa a ESTIMADO"
            Case "PLANOK": obsResp.text = "� PLANIFICACION Inicio Planif: " & txtFechaInicio.text & " | Finalizaci�n Planif: " & txtFechaTermin.text & " | Horas Planif: " & txtHoras.text & " hs. � " & vbCrLf & ClearNull(obsResp.text)
            Case "PLANIF": obsResp.text = "� A PLANIFICAR � " & vbCrLf & obsResp.text
            Case "ESTIMA": obsResp.text = "� A ESTIMAR ESFUERZO � " & vbCrLf & obsResp.text
            Case "ESTIRK"
                obsResp.text = "� RE-ESTIMACION Horas Planif: " & txtHoras & IIf(ClearNull(obsResp.text) = "", " hs. �", " hs. � " & vbCrLf & ClearNull(obsResp.text))
                sOpcionSeleccionada = "ESTIOK"
            Case "PLANRK"
                obsResp.text = "� RE-PLANIFICACION Inicio Planif: " & txtFechaInicio.text & " | Finalizaci�n Planif: " & txtFechaTermin.text & " | Horas Planif: " & txtHoras.text & " hs. � " & vbCrLf & ClearNull(obsResp.text)
                sOpcionSeleccionada = "PLANOK"
            Case "EJECRK"
                obsResp.text = "� RE-PLANIFICACION Inicio Planif: " & txtFechaInicio.text & " | Finalizaci�n Planif: " & txtFechaTermin.text & " | Fch. Pasaje a Prod.: " & txtFechaPasajeProduccion.text & " | Horas Planif: " & txtHoras.text & " hs. � " & vbCrLf & ClearNull(obsResp.text)
                sMsgPeticion = "El grupo fue re-planificado en ejecuci�n"
                sOpcionSeleccionada = "EJECUC"
                bReplanificacion = True
            Case "EJECUC"
                obsResp.text = "� EJECUCION Inicio Planif: " & txtFechaInicio.text & " | Finalizaci�n Planif: " & txtFechaTermin.text & " | Fch. Pasaje a Prod.: " & txtFechaPasajeProduccion.text & " | Horas Planif: " & txtHoras.text & " hs. � " & vbCrLf & ClearNull(obsResp.text)
                sMsgPeticion = "El grupo fue puesto en EJECUCION"
            Case "RECHTE"
                obsResp.text = "� RECHAZO T�CNICO � " & vbCrLf & obsResp.text
                sMsgPeticion = "El grupo fue Rechazado T�cnicamente"
            Case "CANCEL"
                obsResp.text = "� CANCELADO � " & vbCrLf & obsResp.text
                sMsgPeticion = "El grupo fue Cancelado"
            Case "SUSPEN"
                obsResp.text = "� SUSPENSION � " & vbCrLf & obsResp.text
                sMsgPeticion = "El grupo fue Suspendido"
                If cboMotivo.ListIndex > 0 Then     ' Se agrega al texto de la observaci�n el motivo
                    obsResp.text = Trim(obsResp.text) & vbCrLf & " (Motivo: " & TextoCombo(cboMotivo, CodigoCombo(cboMotivo, True), True) & " / Fecha fin de suspensi�n estimada: " & Format(txtFechaFinSuspension.DateValue, "dd.mm.yyyy") & ")" ' upd -028- b.
                End If
            Case "REVISA": obsResp.text = "� A REVISAR � " & obsResp.text
            Case "TERMIN"
                obsResp.text = "� FINALIZACION Inicio Planif: " & txtFechaInicio.text & " | Finalizaci�n Planif: " & txtFechaTermin.text & " | Fch. Pasaje a Prod.: " & txtFechaPasajeProduccion.text & " | Horas Planif: " & txtHoras.text & " hs. � " & vbCrLf & ClearNull(obsResp.text)
                sMsgPeticion = "El grupo pasa a FINALIZADO."
        End Select
        
        NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", EstadoPeticion, sSector, Null, glGrupo, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, obsResp.text)
        ' Aqu� inicializo los valores de nuevo estado con el estado actual (Petici�n, Sector y Grupo)
        NuevoEstadoPeticion = EstadoPeticion
        NuevoEstadoSector = EstadoSector
        NuevoEstadoGrupo = sOpcionSeleccionada
        
        Select Case sOpcionSeleccionada
            Case "PLANIF", "ESTIMA", "CANCEL", "RECHTE", "TERMIN", "SUSPEN", "PLANOK", "ESTIOK", "EJECUC", "REVISA", "OPINOK", "EVALOK"     ' upd -035- a. Se agreg� OPINOK y EVALOK
                If Not glEsHomologador Then                                                                 ' Si no soy el grupo homologador, elimino los mensajes. Para homologaci�n, los mensajes se dejan y que sean borrados por el propio grupo de manera manual
                    Call sp_DelMensajePerfil(glNumeroPeticion, "CGRU", "GRUP", glGrupo)                     ' Borra todos los mensajes para el grupo actual
                End If
                Call sp_UpdatePeticionGrupo(glNumeroPeticion, glGrupo, txtFechaInicio.DateValue, txtFechaTermin.DateValue, txtFechaIniReal.DateValue, txtFechaFinReal.DateValue, IIf(txtFechaPasajeProduccion = "", Null, txtFechaPasajeProduccion.DateValue), IIf(txtFechaFinSuspension.DateValue = "", Null, txtFechaFinSuspension.DateValue), IIf(Len(CodigoCombo(cboMotivo, True)) = 0, 0, CodigoCombo(cboMotivo, True)), CInt(Val(txtHoras.text)), sOpcionSeleccionada, date, "", NroHistorial, 0) ' add -007- a. Se agrega par�metro txtFechaFinSuspension y el c�digo del motivo de suspensi�n
                Call getIntegracionGrupo(glNumeroPeticion, sSector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")    ' Integra las fechas de los grupos involucrados en el sector
                NuevoEstadoSector = getNuevoEstadoSector(glNumeroPeticion, sSector)                         ' Aqu� evalua los estados de los grupos para determinar el nuevo estado del sector al que pertenece este grupo (lo anterior vale sin tomar en cuenta el estado del grupo Homologaci�n)       ' add -012- a.
                If NuevoEstadoSector <> "TERMIN" Then                                                       ' Si el nuevo estado no es el de "Finalizado" entonces todavia no hay fecha de fin real, por lo que inicializa este dato (esto sin tener en cuenta al grupo homologador. Si fuera �ste, entonces no tiene en cuenta su estado
                    If glGrupo <> glHomologacionGrupo Then fFinReal = Null
                End If
                If NuevoEstadoSector <> EstadoSector Then
                    Call sp_DelMensajePerfil(glNumeroPeticion, "CSEC", "SECT", sSector)                     ' Elimina todos los mensajes para ese sector
                End If
                If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL|OPINOK|EVALOK|", NuevoEstadoSector) > 0 Then
                    Call sp_UpdatePeticionSector(glNumeroPeticion, sSector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, date, xSituacion, 0, 0)
                Else
                    Call sp_UpdatePeticionSector(glNumeroPeticion, sSector, Null, Null, Null, Null, 0, NuevoEstadoSector, date, xSituacion, 0, 0)   ' upd -006- a.
                End If
                Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")            ' Para la integraci�n de las fechas, aqu� ya estoy descartando de la evaluaci�n al grupo Homologaci�n
                NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)                              ' Para el nuevo estado de la petici�n, aqu� ya estoy descartando al grupo Homologaci�n
                If InStr(1, "OPINOK|EVALOK|", NuevoEstadoSector, vbTextCompare) > 0 Then
                    Call sp_DeletePeticionGrupo(glNumeroPeticion, sSector, Null)
                    Call chkSectorOpiEva(glNumeroPeticion)                                                  ' Se actualiza la situaci�n y el estado de la petici�n
                    If NuevoEstadoPeticion <> glEstadoPeticion Then NuevoEstadoPeticion = glEstadoPeticion
                    'glEstadoPeticion = "COMITE"            ' TODO: REVISAR ESTO EN DETALLE!!! 08.03.2016
                Else
                    Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
                    Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)                      ' Ver por qu� Cancela!!! 03.04.2012!!! Listo: La volvemos NULLEABLE! 08.03.2016
                End If
                If NuevoEstadoPeticion <> "TERMIN" Then
                    If glGrupo <> glHomologacionGrupo Then
                        fFinReal = Null
                    End If
                End If
                If InStr(1, "TERMIN|ANEXAD|ANULAD|CANCEL|RECHTE|RECHAZ", NuevoEstadoPeticion, vbTextCompare) > 0 Then   ' El nuevo estado alcanzado por la petici�n es alguno de los estados terminales
                    Call sp_DeletePeticionChangeMan(glNumeroPeticion)       ' Elimino la petici�n de la tabla de PeticionesChangeMan (deja de ser v�lida para pasajes a Producci�n)
                    Call sp_DeletePeticionEnviadas(glNumeroPeticion)
                    Call sp_DeleteMensaje(glNumeroPeticion, 0)              ' Borro todos los mensajes que pudiera tener sobre esta petici�n (31.03.2016)
                End If
                If InStr(1, "APROBA|ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoPeticion) > 0 Then   ' Actualiza las horas, fechas globales a nivel de la petici�n para integrarlas si corresponde
                    Call sp_UpdatePetFechas(glNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
                Else
                    Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
                End If
                ' MENSAJES FORZADOS:
                ' Cuando se pas� a "Rechazo t�cnico" fuerza un mensaje al Responsable de Sector.
                ' Se agrega ahora, siempre y cuando no sea el grupo homologador quien lo hace. Esto implicaria que el
                ' responsable de sector de este grupo (en este caso Dopazo) reciba avisos que no son necesarios.
                If sOpcionSeleccionada = "RECHTE" Then
                    If glGrupo <> glHomologacionGrupo Then    ' Antes de enviar, evaluo si quien realizo este cambio de estado del grupo no es el grupo homologador... si es, no envio los mensajes al supervisor.
                        Call sp_DoMensaje("SEC", "GMOD000", glNumeroPeticion, sSector, "NULL", "", "")
                    End If
                End If
                If sOpcionSeleccionada = "REVISA" Then      ' Cuando se pas� a "A Revisar" fuerza un mensaje al Responsable de Ejecuci�n
                    Call sp_DoMensaje("GRU", "REVIGRU", glNumeroPeticion, glGrupo, "REVISA", "", "")
                End If
                ' TODO: agregar envio de mensaje a GBPE
                
                ' ************************************************************************************************************************
                '{ add -009- a. Si es un grupo homologable, genero los mensajes del cambio de
                '               fecha prevista para pasaje a Producci�n
                ' ************************************************************************************************************************
                If sOpcionSeleccionada = "EJECUC" Or sOpcionSeleccionada = "EJECRK" Then    ' Si el grupo pasa a "En ejecuci�n" o "Replanificado en Ejecuci�n"
                    If bEsGrupoHomologable Then                                             ' Si es un grupo homologable
                        If IsNull(fPasajeProduccion) Then                                   ' Si la fecha prevista de pasaje a producci�n cambi�, aviso al grupo Homologador
                            cMensaje = Trim(lblSector) & "  � " & Trim(lblGrupo) & vbCrLf & "La fecha prevista de pasaje a producci�n se carga por primera vez para este grupo: " & Format(txtFechaPasajeProduccion.DateValue, "dd.mm.yyyy")
                            Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 112, sOpcionSeleccionada, cMensaje)
                        Else                                                                ' Si se modifica esta fecha...
                            If fPasajeProduccion <> txtFechaPasajeProduccion.DateValue Then
                                cMensaje = Trim(lblSector) & "  � " & Trim(lblGrupo) & vbCrLf & "Se modific� la fecha prevista de pasaje a producci�n: antes era " & Format(fPasajeProduccion, "dd.mm.yyyy") & " y ahora es " & Format(txtFechaPasajeProduccion.DateValue, "dd.mm.yyyy")
                                Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 113, sOpcionSeleccionada, cMensaje)
                            End If
                        End If
                    End If
                End If
                ' ************************************************************************************************************************
                ' Paso final: evaluar como debe responder el grupo homologador
                ' ************************************************************************************************************************
                Call EvaluarHomologacion
        End Select
        Call sp_UpdHistorial(NroHistorial, "GCHGEST", NuevoEstadoPeticion, sSector, NuevoEstadoSector, glGrupo, NuevoEstadoGrupo, glLOGIN_ID_REEMPLAZO)     ' Ahora por si hubo cambios, vuelve a actualizar el historial para el evento de cambio de estado de grupo (por eso usa NroHistorial, para pisar el anterior)
        ' 14.10.2016
        ' Esto es nuevo: si una petici�n no tiene ning�n grupo en ejecuci�n, pero tiene todo bien (OK, etc.) y luego se pasa a
        ' "En ejecuci�n" (cualquier grupo), entonces se valida y corresponde ponerla como v�lida para pasaje a producci�n.
        If InStr(1, "EJECUC|EJECRK|", NuevoEstadoPeticion, vbTextCompare) > 0 Then
            Call ControlesPasajeProduccion(sClasePet, NuevoEstadoPeticion, bPeticionEstuvoEnEjecucion)
        End If
        If NuevoEstadoPeticion = "EJECUC" Then
            ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
            FuncionesHOST.sp_InsertPeticionEnviadas2 glNumeroPeticion, glNroAsignado, sClasePet, "", ""
        Else
            FuncionesHOST.sp_DeletePeticionEnviadas2 glNumeroPeticion
        End If
        Call Puntero(False)
        Call Status("Listo.")
        Call touchForms
        Unload Me
    Else
        Call setHabilCtrl(cmdConfirmar, NORMAL)
        Call setHabilCtrl(cmdCerrar, NORMAL)
    End If
End Sub

Private Sub EvaluarHomologacion()
    If bGrupoVieneDeTerminal Then
        sHomoGrupoEstadoNuevo = ""
        If bEsGrupoHomologable Then
            Select Case sOpcionSeleccionada
                Case "ESTIMA", "PLANIF"
                    sHomoGrupoEstadoNuevo = IIf(bConformeTEST, "EJECUC", "ESTIMA")
                Case "EJECUC"
                    If Not bReplanificacion Then
                        sHomoGrupoEstadoNuevo = IIf(bConformeTEST, "EJECUC", "PLANIF")
                    End If
                Case "SUSPEN"
                    sHomoGrupoEstadoNuevo = "ESTIMA"
                    If bGrupoEstuvoEnEjecucion Then
                        sHomoGrupoEstadoNuevo = IIf(bConformeTEST, "EJECUC", "PLANIF")
                    End If
                Case Else
                    sHomoGrupoEstadoNuevo = "ESTIMA"
            End Select
            If bExisteHomologacion Then                                 ' Si ya existe, actualizo los datos (solo si corresponde)
                If sHomoGrupoEstadoNuevo <> sHomoGrupoEstado Then
                    If bHomologacionEnEstadoActivo Then
                        Call ActualizarHomologacion
                    Else
                        If sHomoGrupoEstado <> "RECHTE" Then            ' Si no est� activo, lo reactivo (siempre que no est� en rechazo t�cnico)
                            Call ActualizarHomologacion
                        End If
                    End If
                End If
            Else
                Call AgregarHomologacion                                ' Si no, agrego al grupo homologador en estado activo
            End If
        End If
    Else
        ' Solo actualizo entonces aquellos estados que corresponde:
        ' a. Cuando DYD pasa a "A planificar", Homologaci�n pasa a "A estimar esfuerzo"
        ' b. Cuando DYD pasa a "Ejecuci�n", Homologaci�n pasa a "A planificar"
        ' IMPORTANTE: hay que tener en cuenta, que si Homologaci�n se encuentra en alg�n estado terminal por
        ' decisi�n propia, no hay que cambiarlo (por ej.: se encuentra en rechazo t�cnico porque por la clase no
        ' correponde continuar el seguimiento).
        If Not glEsHomologador Then
            If bEsGrupoHomologable Then
                If bHomologacionEnEstadoActivo Then
                    sHomoGrupoEstadoNuevo = sHomoGrupoEstado    ' Por defecto su estado actual
                    If InStr(1, "PLANIF|EJECUC|TERMIN|", sOpcionSeleccionada, vbTextCompare) > 0 Then
                        Select Case sOpcionSeleccionada
                            Case "PLANIF"
                                sHomoGrupoEstadoNuevo = "ESTIMA"
                                ' Si homologaci�n ya esta en A planificar o En ejecuci�n, no hay que cambiarlo a Estimar esfuerzo, porque es retroceder en los estados
                                If InStr(1, "PLANIF|EJECUC|ESTIOK|PLANOK|CANCEL|SUSPEN|", sHomoGrupoEstado, vbTextCompare) > 0 Then
                                    sHomoGrupoEstadoNuevo = ""
                                End If
                            Case "EJECUC"
                                If InStr(1, "ESTIMA|ESTIOK|CANCEL|", sHomoGrupoEstado, vbTextCompare) > 0 Then
                                    sHomoGrupoEstadoNuevo = "PLANIF"
                                End If
                        End Select
                        If sHomoGrupoEstadoNuevo <> "" Then
                            If sHomoGrupoEstado <> sHomoGrupoEstadoNuevo Then
                                Call ActualizarHomologacion
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
    ' MENSAJES PARA EL GRUPO HOMOLOGADOR: Por cada grupo que cambia de estado, envio un mensaje al grupo Homologador (exceptuando de esto al mism�simo grupo Homologador)
    If EstadoSolicitud <> sOpcionSeleccionada Then
        If bEsGrupoHomologable Then
            If bExisteHomologacion Then
                cMensaje = Trim(lblSector) & " � " & Trim(lblGrupo) & vbCrLf & "El grupo ha cambiado de estado: de '" & Trim(lblEstadoGrupo) & "' ha pasado a '" & Trim(Left(cboAccion, Len(cboAccion) - 8)) & "'"
                Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 115, sOpcionSeleccionada, cMensaje)
            End If
        End If
    End If
End Sub


' NUEVO
'Private Sub ControlesPasajeProduccion()
'    'Dim sConformes As String
'    '{ new
'    Dim bPeticionEstuvoEnEjecucion As Boolean
'
'    Dim docAlcance As Boolean               ' P950/C100
'    Dim okAlcance As Boolean                ' ALCA/ALCP
'    Dim cambioAlcance As Boolean            ' CG04
'    Dim okCambioAlcance As Boolean          '
'    Dim okMailAlcance As Boolean            ' EML1
'    Dim okMailUsuario As Boolean            ' EML2
'    Dim C204 As Boolean                     ' Pruebas unitarias/sistema
'    Dim T710 As Boolean                     ' Aplicaci�n de principios de desarrollo
'    Dim okUsuario As Boolean                ' TEST/TESP
'    Dim okSupervisor As Boolean             ' OKPP/OKPF
'    Dim OMA As Boolean
'    Dim OME As Boolean
'    Dim SOB As Boolean
'
'    Dim i As Integer                        ' Auxiliar para iterar sobre los datos de adjuntos y conformes
'    '}
'
'    Dim bOK_SI As Boolean
'    Dim bOKLider As Boolean
'    Dim bOKLider_Homologacion As Boolean
'    Dim bOKPasajeProduccion As Boolean
'
'    bOK_SI = False
'    bOKLider = False
'    bOKLider_Homologacion = False
'    bOKPasajeProduccion = False
'    'sConformes = ""
'
'    ' TODO: 14.10.16: Este flag habr�a que obtenerlo una sola vez al principio.
'    If sp_GetVarios("CTRLPRD1") Then
'        If Not aplRST.EOF Then
'            If ClearNull(aplRST.Fields!var_numero) = 1 Then
'                bOKLider_Homologacion = True
'            End If
'        End If
'    End If
'
''NO HARIA FALTA
''    ' 1. Cargo todos los conformes que tiene la petici�n
''    If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
''        Do While Not aplRST.EOF
''            sConformes = sConformes & ClearNull(aplRST.Fields!ok_tipo) & "|"
''            aplRST.MoveNext
''            DoEvents
''        Loop
''    End If
'
'    bOKPasajeProduccion = False
'
'    If EstadoSolicitud <> "EJECUC" Then
'        If sp_GetHistorial(glNumeroPeticion, Null) Then
'            Do While Not aplRST.EOF
'                If ClearNull(aplRST.Fields!pet_estado) = "EJECUC" Then
'                    bPeticionEstuvoEnEjecucion = True
'                    Exit Sub
'                End If
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'    Else
'        bPeticionEstuvoEnEjecucion = True
'    End If
'
'
'    If sp_GetAdjuntosPet(glNumeroPeticion, Null, Null, Null) Then
'        Do While Not aplRST.EOF
'            Select Case Mid(ClearNull(aplRST.Fields!adj_tipo), 1, 4)
'                Case "C100", "P950": docAlcance = True
'                Case "C204": C204 = True
'                Case "CG04": cambioAlcance = True
'                Case "EML1": okMailAlcance = True
'                Case "EML2": okMailUsuario = True
'                Case "T710": T710 = True
'            End Select
'            aplRST.MoveNext
'            DoEvents
'        Loop
'    End If
'
'    If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
'        Do While Not aplRST.EOF
'            Select Case Mid(ClearNull(aplRST.Fields!adj_tipo), 1, 4)
'                Case "ALCA", "ALCP": okAlcance = True
'                Case "TESP", "TEST"
'                    If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
'                        bOKLider = True
'                    Else
'                        okUsuario = True
'                    End If
'                Case "OKPF", "OKPP": okSupervisor = True
'                Case "HOMA": OMA = True
'                Case "HOME": OME = True
'                Case "HSOB": SOB = True
'            End Select
'            aplRST.MoveNext
'            DoEvents
'        Loop
'    End If
'
'
'    ' NUEVA VALIDACION
'    Select Case sClasePet
'        Case "NUEV", "EVOL"
'            ' Tiene documento de alcance y ok al alcance
'            If docAlcance And okAlcance Then
'                ' Tiene cambio de alcance y ok al cambio de alcance
'                If cambioAlcance Then
'                    If okCambioAlcance Then
'
'                    End If
'                End If
'                If bPeticionEstuvoEnEjecucion Then
'                    If C204 Then
'                        If T710 Then
'                            If SOB Then
'                                If Not OMA Or (OMA And okSupervisor) Then
'                                    bOKPasajeProduccion = True
'                                End If
'                            End If
'                        End If
'                    End If
'                End If
'            End If
'        Case "OPTI"
'            If docAlcance Then          ' Opcional
'                If okAlcance Then
'
'                End If
'                If cambioAlcance And okCambioAlcance Then
'
'                End If
'            End If
'            If bPeticionEstuvoEnEjecucion Then
'                If C204 Then
'                    If okUsuario Or (bOKLider And okMailUsuario) Then
'                        If Not OMA Or (OMA And okSupervisor) Then
'                            bOKPasajeProduccion = True
'                        End If
'                    End If
'                End If
'            End If
'        Case "ATEN"
'            If bPeticionEstuvoEnEjecucion Then
'                If okUsuario Or (bOKLider And okMailUsuario) Then
'                    If Not OMA Or (OMA And okSupervisor) Then
'                        bOKPasajeProduccion = True
'                    End If
'                End If
'            End If
'        Case Else
'            ' Correctivos, spufis
'            If bPeticionEstuvoEnEjecucion Then
'                If okUsuario Or (bOKLider And okMailUsuario) Then
'                    If Not OMA Or (OMA And okSupervisor) Then
'                        bOKPasajeProduccion = True
'                    End If
'                End If
'            End If
'    End Select
'    If bOKPasajeProduccion Then
'        Call HabilitarControles_PasajeProduccion(glNumeroPeticion, glNroAsignado, sClasePet, "", "", glLOGIN_ID_REAL, "02")
'    Else
'        Call sp_DeletePeticionChangeMan(glNumeroPeticion)
'        Call sp_DeletePeticionEnviadas(glNumeroPeticion)
'    End If
'
''    ' ORIGINAL
''    Select Case sClasePet
''        Case "NUEV", "EVOL", "OPTI"
''            If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
''                If InStr(1, sConformes, "OK", vbTextCompare) > 0 Then
''                    If bOK_SI Then
''                        bOKPasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
''                    Else
''                        bOKPasajeProduccion = True
''                    End If
''                End If
''            Else
''                If InStr(1, sConformes, "TES", vbTextCompare) > 0 Then
''                    If bOK_SI Then
''                        bOKPasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
''                    Else
''                        bOKPasajeProduccion = True
''                    End If
''                End If
''            End If
''        Case Else
''            ' Ac� ya deber�a tener cargado el flag (cuando ingres� a la petici�n por ejemplo).
''            If bOKLider_Homologacion Then
''                bOKLider = False
''                If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
''                    Do While Not aplRST.EOF
''                        If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
''                            bOKLider = True
''                            Exit Do
''                        End If
''                        aplRST.MoveNext
''                        DoEvents
''                    Loop
''                End If
''                If bOKLider Then
''                    If InStr(1, sConformes, "HOME", vbTextCompare) > 0 Or _
''                        InStr(1, sConformes, "HSOB", vbTextCompare) > 0 Then
''                        If bOK_SI Then
''                            bOKPasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
''                        Else
''                            bOKPasajeProduccion = True
''                        End If
''                    End If
''                End If
''            End If
''            If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
''                If InStr(1, sConformes, "OK", vbTextCompare) > 0 Then
''                    If bOK_SI Then
''                        bOKPasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
''                    Else
''                        bOKPasajeProduccion = True
''                    End If
''                End If
''            Else
''                If bOK_SI Then
''                    bOKPasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
''                Else
''                    bOKPasajeProduccion = True
''                End If
''            End If
''    End Select
''
''    If bOKPasajeProduccion Then
''        Call HabilitarControles_PasajeProduccion(glNumeroPeticion, glNroAsignado, sClasePet, "", "", glLOGIN_ID_REAL, "02")
''    Else
''        Call sp_DeletePeticionChangeMan(glNumeroPeticion)
''        Call sp_DeletePeticionEnviadas(glNumeroPeticion)
''    End If
'End Sub

Private Sub HabilitarControles()
    cmdConfirmar.Enabled = True
    cmdCerrar.Enabled = True
    fraSector.Enabled = True
    lblAccion = ""
        
    Call setHabilCtrl(txtFechaTermin, "DIS")
    Call setHabilCtrl(txtFechaInicio, "DIS")
    Call setHabilCtrl(txtFechaIniReal, "DIS")
    Call setHabilCtrl(txtFechaFinReal, "DIS")
    Call setHabilCtrl(txtHoras, "DIS")
    Call setHabilCtrl(obsSolic, "DIS")
    Call setHabilCtrl(obsResp, "DIS")
    Call setHabilCtrl(txtFechaPasajeProduccion, "DIS")
    Call setHabilCtrl(txtFechaFinSuspension, "DIS")
    Call setHabilCtrl(cboMotivo, "DIS")
    
    ' Observaciones para el cambio de estado
    Select Case sOpcionSeleccionada
        Case "RECHTE", "OPINOK", "EVALOK", "CANCEL", "REVISA", "PLANRK", "ESTIRK", "EJECRK", "SUSPEN"
            Call setHabilCtrl(obsResp, "OBL")
        Case Else
            Call setHabilCtrl(obsResp, "NOR")
    End Select
    
    ' Si el estado seleccionado no es "Suspender temporalmente" entonces desmarco las opciones si corresponde
    If sOpcionSeleccionada <> "SUSPEN" Then
        txtFechaFinSuspension.text = ""
        cboMotivo.ListIndex = -1
    End If
            
    Select Case sOpcionSeleccionada
        Case "OPINOK"
            lblAccion = "OPINAR"
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
        Case "EVALOK"
            lblAccion = "EVALUAR"
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
        Case "ESTIOK"
            lblAccion = "ESTIMAR"
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
            If glEsHomologador Then
                If Val(txtHoras) = 0 Then txtHoras.text = 1
            End If
        Case "ESTIRK"
            lblAccion = "ESTIMAR"
            Call setHabilCtrl(txtHoras, "OBL")
        Case "ESTIMA"
            lblAccion = "ESTIMAR"
            txtHoras.text = IIf(IsNull(hsPresup), "", hsPresup): Call setHabilCtrl(txtHoras, "OBL")
            txtFechaTermin.text = ""
            txtFechaInicio.text = ""
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
        Case "PLANIF"
            lblAccion = "PLANIFICAR"
            Call setHabilCtrl(txtHoras, "OBL")
            txtHoras.text = IIf(IsNull(hsPresup), "", hsPresup):
            txtFechaInicio.text = IIf(IsNull(fIniPlan), "", fIniPlan): Call setHabilCtrl(txtFechaInicio, "OBL")
            txtFechaTermin.text = IIf(IsNull(fFinPlan), "", fFinPlan): Call setHabilCtrl(txtFechaTermin, "OBL")
            txtFechaIniReal.text = IIf(IsNull(fIniReal), "", fIniReal)
            txtFechaFinReal.text = IIf(IsNull(fFinReal), "", fFinReal)
        Case "PLANOK"
            lblAccion = "PLANIFICAR"
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
            If glEsHomologador Then
                If Val(txtHoras) = 0 Then txtHoras.text = 1
            End If
        Case "PLANRK"
            lblAccion = "PLANIFICAR"
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
        Case "EJECRK"
            lblAccion = "PLANIFICAR"
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtFechaIniReal, "OBL")
            If Not glEsHomologador Then
                If InStr(1, "NUEV|EVOL|OPTI|", sClasePet, vbTextCompare) > 0 Then
                    Call setHabilCtrl(txtFechaPasajeProduccion, IIf(bEsGrupoHomologable, "OBL", "NOR"))
                End If
            End If
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
        Case "RECHTE"
            lblAccion = "RECHAZAR"
            If bEsGrupoSeguridadInformatica Or bEsGrupoTecnologia Then
                Call setHabilCtrl(obsResp, NORMAL)
            End If
        Case "EJECUC"
            lblAccion = "EJECUTAR"
            Call setHabilCtrl(txtFechaIniReal, "OBL")
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
            If txtFechaIniReal.text = "" And txtFechaInicio.text <> "" Then
                txtFechaIniReal.text = txtFechaInicio.text
            End If
            If Not glEsHomologador And Not bEsGrupoTecnologia And Not bEsGrupoSeguridadInformatica Then
                If InStr(1, "NUEV|EVOL|OPTI|", sClasePet, vbTextCompare) > 0 Then
                    Call setHabilCtrl(txtFechaPasajeProduccion, IIf(bEsGrupoHomologable, "OBL", "NOR"))
                Else
                    fPasajeProduccion = Null
                    txtFechaPasajeProduccion.text = ""
                    Call setHabilCtrl(txtFechaPasajeProduccion, "DIS")
                End If
            End If
        Case "CANCEL"
            lblAccion = "Cancelar"
            If bEsGrupoSeguridadInformatica Or bEsGrupoTecnologia Then
                Call setHabilCtrl(obsResp, NORMAL)
            Else
                Call setHabilCtrl(obsResp, "OBL")
            End If
        Case "REVISA"
            lblAccion = "REVISAR"
        Case "SUSPEN"
            lblAccion = "SUSPENDER"
            Select Case sGerencia
                Case "DESA"
                    If EstadoSolicitud = "SUSPEN" Then
                        Call setHabilCtrl(txtFechaFinSuspension, IIf(bEsGrupoHomologable, "OBL", "NOR"))
                        Call setHabilCtrl(cboMotivo, IIf(bEsGrupoHomologable, "OBL", "NOR"))
                        txtFechaFinSuspension.text = IIf(Not IsNull(fFechaFinSuspension), Format(fFechaFinSuspension, "dd/mm/yyyy"), "")
                        cboMotivo.ListIndex = PosicionCombo(cboMotivo, ClearNull(cboMotivo.Tag), True)
                    Else
                        Call setHabilCtrl(txtFechaFinSuspension, IIf(bEsGrupoHomologable, "OBL", "NOR"))
                        Call setHabilCtrl(cboMotivo, IIf(bEsGrupoHomologable, "OBL", "NOR"))
                        cboMotivo.ListIndex = -1
                    End If
            End Select
        Case "TERMIN"
            lblAccion = "FINALIZAR"
            Call setHabilCtrl(txtHoras, "OBL")
            Call setHabilCtrl(txtFechaIniReal, "OBL")
            Call setHabilCtrl(txtFechaFinReal, "OBL")
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            If glEsHomologador Or bEsGrupoTecnologia Or bEsGrupoSeguridadInformatica Then
                ' Fecha de inicio real
                If IsNull(txtFechaIniReal.DateValue) Then
                    txtFechaIniReal.DateValue = Now: txtFechaIniReal.text = txtFechaIniReal.DateValue
                Else
                    If Not IsDate(CDate(txtFechaIniReal.DateValue)) Then
                        txtFechaIniReal.DateValue = Now: txtFechaIniReal.text = txtFechaIniReal.DateValue
                    End If
                End If
                ' Fecha fin real
                If IsNull(txtFechaFinReal.DateValue) Then
                    txtFechaFinReal.DateValue = Now: txtFechaFinReal.text = txtFechaFinReal.DateValue
                Else
                    If Not IsDate(CDate(txtFechaFinReal.DateValue)) Then
                        txtFechaFinReal.DateValue = Now: txtFechaFinReal.text = txtFechaFinReal.DateValue
                    End If
                End If
            End If
            
            If Not glEsHomologador And Not bEsGrupoTecnologia And Not bEsGrupoSeguridadInformatica Then
                If InStr(1, "NUEV|EVOL|OPTI|", sClasePet, vbTextCompare) > 0 Then
                    Call setHabilCtrl(txtFechaPasajeProduccion, IIf(bEsGrupoHomologable, "OBL", "NOR"))
                End If
                Call setHabilCtrl(txtHoras, "OBL")
                If txtFechaIniReal.text = "" And txtFechaInicio.text <> "" Then
                    txtFechaIniReal.text = txtFechaInicio.text
                End If
            End If
        Case Else
            lblAccion = sOpcionSeleccionada
    End Select
End Sub

Private Function CamposObligatorios() As Boolean
    ' 16.06.2017 - Estos controles eran los que se realizaban antes de SOx. Ya no se necesita.
    
    CamposObligatorios = False
    ' mov -001-
    'nuevo requerimiento Susana Dopazo SOX
    If (sTipoPet = "PRJ") And (sOpcionSeleccionada = "PLANOK" Or sOpcionSeleccionada = "TERMIN" Or sOpcionSeleccionada = "EJECUC") Then
        If sGerencia = "DESA" Then
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA", Null, Null) Then
                MsgBox "El grupo no puede pasar PLANIFICADO, EJECUCION o FINALIZADO," & Chr(13) & "Falta el documento de alcance.", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        End If
    End If
    
    '{ add -001- f.
    If sOpcionSeleccionada = "TERMIN" And sGerencia = "DESA" Then   ' upd -001- f. - Solo exigible a DyD
        If Not sp_GetAdjuntosPet(glNumeroPeticion, "PLIM", Null, Null) Then
            MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el documento Plan de implantaci�n (PLIM).", vbOKOnly + vbInformation
            CamposObligatorios = False
            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
            Exit Function
        End If
    End If
    '}
    If sOpcionSeleccionada = "TERMIN" Then
        If sGerencia = "DESA" Then
            If Not sp_VerPeticionHoras(glNumeroPeticion) Then
                MsgBox "El grupo no puede pasar a FINALIZADO," & Chr(13) & "No posee horas trabajadas informadas", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        End If
    End If
    
    ''' esto va, de acuerdo a mail/telefonico/patrignani-efron
    If sOpcionSeleccionada = "TERMIN" And sFechaComite >= CDate("2006/09/30") Then
        If InStr(1, "NOR|ESP|PRJ|", sTipoPet, vbTextCompare) > 0 Then
            If sGerencia = "DESA" Then
                If Not sp_GetPeticionConf(glNumeroPeticion, "TEST", 1) Then
                    MsgBox "El grupo no puede cambiar el estado a FINALIZADO," & Chr(13) & "Falta el Ok de usuario.", vbOKOnly + vbInformation
                    CamposObligatorios = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
            End If
        End If
        'requerimiento Susana Dopazo 06/03/2007 porque Organizacion genera propias para DESA
        If sTipoPet = "PRO" Then
            If solGerencia <> "DESA" And sGerencia = "DESA" Then
                If Not sp_GetPeticionConf(glNumeroPeticion, "TEST", 1) Then
                    MsgBox "El grupo no puede cambiar el estado a FINALIZADO," & Chr(13) & "Falta el Ok de usuario.", vbOKOnly + vbInformation
                    CamposObligatorios = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
            End If
        End If
    End If
    
    If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) = 0 Then
        Select Case sOpcionSeleccionada
            Case "RECHTE", "OPINOK", "EVALOK", "CANCEL", "SUSPEN", "REVISA", "PLANRK", "ESTIRK", "EJECRK"
                If ClearNull(Me.obsResp.text) = "" Then
                    MsgBox "Se deben ingresar obervaciones", vbOKOnly + vbInformation
                    CamposObligatorios = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
        End Select
    End If

    Select Case sOpcionSeleccionada
        Case "ESTIOK", "ESTIRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        Case "PLANOK", "PLANRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaInicio.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaTermin.text = "" Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        Case "EJECUC", "EJECRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaIniReal.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Real", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaInicio.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaTermin.text = "" Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        Case "TERMIN"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaFinReal.text = "" Then
                MsgBox "Se debe informar Fecha de Fin Real", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaIniReal.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Real", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaInicio.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If txtFechaTermin.text = "" Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
    End Select
    If (Not IsNull(txtFechaInicio.DateValue)) And (Not IsNull(txtFechaTermin.DateValue)) Then
        If txtFechaTermin.DateValue < txtFechaInicio.DateValue Then
            MsgBox "La Fecha Inicio Plan. no debe ser mayor a la Fecha Fin Plan.", vbOKOnly + vbInformation
            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If (Not IsNull(txtFechaIniReal.DateValue)) And (Not IsNull(txtFechaFinReal.DateValue)) Then
        If txtFechaFinReal.DateValue < txtFechaIniReal.DateValue Then
            MsgBox "La Fecha Inicio Real no debe ser mayor a la Fecha Fin Real", vbOKOnly + vbInformation
            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If txtFechaIniReal.DateValue > date Then
        MsgBox "La Fecha Inicio Real no debe ser mayor a la Fecha del D�a.", vbOKOnly + vbInformation
        CamposObligatorios = False
        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
        Exit Function
    End If
    If txtFechaFinReal.DateValue > date Then
        MsgBox "La Fecha Fin Real no debe ser mayor a la Fecha del D�a.", vbOKOnly + vbInformation
        CamposObligatorios = False
        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
        Exit Function
    End If
    CamposObligatorios = True
End Function

Private Function CamposObligatoriosEstandard(ByRef bControlHomologacion As Byte) As Boolean
    CamposObligatoriosEstandard = False
        
    If Val(txtHoras.text) > 32767 Then
        MsgBox "El valor m�ximo que puede usarse para las horas presupuestadas es 32767.", vbOKOnly + vbExclamation
        CamposObligatoriosEstandard = False
        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
        Exit Function
    End If
    
    ' --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ' Control fecha prevista de pasaje a producci�n
    ' --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    If Not glEsHomologador Then                                                                                 ' Si no soy el grupo Homologador
        If bEsGrupoHomologable And InStr(1, "EJECUC|EJECRK|", sOpcionSeleccionada, vbTextCompare) > 0 Then
            If InStr(1, "EVOL|OPTI|NUEV|", sClasePet, vbTextCompare) > 0 Then
                If Not IsDate(txtFechaPasajeProduccion.DateValue) Then                                          ' Controles l�gicos de fecha prevista pasaje a Producci�n
                    Select Case sOpcionSeleccionada
                        Case "EJECUC": MsgBox "Debe ingresar la fecha prevista de pasaje a Producci�n" & vbCrLf & "para pasar al grupo al estado en ejecuci�n.", vbOKOnly + vbExclamation, "Fecha prevista de pasaje a Producci�n"
                        Case "EJECRK": MsgBox "Debe ingresar la fecha prevista de pasaje a Producci�n" & vbCrLf & "para pasar al grupo a replanificado en ejecuci�n.", vbOKOnly + vbExclamation, "Fecha prevista de pasaje a Producci�n"
                    End Select
                    CamposObligatoriosEstandard = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                Else
                    If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) = 0 Then
                        If Not txtFechaPasajeProduccion.DateValue >= date Then
                            MsgBox "La fecha prevista de pasaje a Producci�n" & vbCrLf & "debe ser mayor o igual a la fecha de hoy", vbOKOnly + vbExclamation, "Fecha prevista de pasaje a Producci�n"
                            CamposObligatoriosEstandard = False
                            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                            Exit Function
                        Else
                            ' Ahora se solicita este dato solo si el grupo es Homologable:
                            If sp_GetGrupoHomologable(glGrupo) Then
                                ' Si la petici�n no ten�a el dato de Fecha prevista de pasaje a Producci�n
                                ' o tiene un dato nuevo, entonces se debe comprobar lo siguiente:
                                If IsNull(fPasajeProduccion) Or fPasajeProduccion <> txtFechaPasajeProduccion.DateValue Then
                                    ' 1. Si la fecha ingresada corresponde a un plazo menor a 48 hs. -dos d�as h�biles-
                                    '    desde el momento en que se registra, emitir un mensaje de ADVERTENCIA e ingresar
                                    '    autom�ticamente un conforme parcial de tipo HOMA
                                    If DiasOcurridos(date, txtFechaPasajeProduccion.DateValue) <= 2 Then
                                        bControlHomologacion = 1
                                        ' Agregar mensaje de advertencia y conforme parcial tipo HOMA
                                    End If
                                    ' 2. Si la fecha ingresada corresponde a un plazo menor a 120 hs. -cinco d�as h�biles-
                                    '    desde el momento en que se registra, emitir un mensaje de ADVERTENCIA e ingresar
                                    '    autom�ticamente una observaci�n aclaratoria al evento de HISTORIAL.
                                    If DiasOcurridos(date, txtFechaPasajeProduccion.DateValue) <= 5 Then
                                        bControlHomologacion = bControlHomologacion + 2
                                        ' Agregar mensaje de advertencia e ingresar una observaci�n en el Historial
                                    End If
                                    bControlHomologacion = bControlHomologacion + 5
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                ' Controles l�gicos de fecha prevista pasaje a Producci�n cuando no es obligatoria:
                ' Si no se ingreso ninguna fecha, ningun dato, entonces lo dejo pasar sin problema. Puedo guardar el valor en nulo.
                ' Pero si agrego alg�n dato, valido que la fecha sea v�lida y mayor o igual a la fecha del d�a.
                If Len(txtFechaPasajeProduccion.text) > 0 Then
                    If IsDate(txtFechaPasajeProduccion.DateValue) Then
                        If Not txtFechaPasajeProduccion.DateValue >= date Then
                            MsgBox "La fecha prevista de pasaje a Producci�n" & vbCrLf & "debe ser mayor o igual a la fecha de hoy", vbOKOnly + vbExclamation, "Fecha prevista de pasaje a Producci�n"
                            CamposObligatoriosEstandard = False
                            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                            Exit Function
                        End If
                    Else
                        MsgBox "La fecha ingresada debe ser una fecha v�lida.", vbOKOnly + vbExclamation, "Fecha prevista de pasaje a Producci�n"
                        CamposObligatoriosEstandard = False
                        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                        Exit Function
                    End If
                End If
            End If
        Else
            If bEsGrupoHomologable And InStr(1, "EJECUC|EJECRK|TERMIN|", sOpcionSeleccionada, vbTextCompare) > 0 Then                ' add -038- a.
                If InStr(1, "NUEV|EVOL|OPTI|", sClasePet, vbTextCompare) > 0 Then                   ' Si la clase lo requiere, debe ingresar una fecha v�lida
                    If IsDate(txtFechaPasajeProduccion.DateValue) Then                              ' Si existe una fecha prevista de pasaje a Producci�n cargada
                        If Not IsNull(fPasajeProduccion) Then                                       ' Si esa fecha ya existia en el grupo
                            If fPasajeProduccion <> txtFechaPasajeProduccion.DateValue Then         ' Comparo si no la han cambiado... si la cambiaron, entonces provoco un error
                                MsgBox "No puede modificar la fecha prevista de pasaje a Producci�n.", vbOKOnly + vbExclamation, "Fecha prevista de pasaje a Producci�n"
                                txtFechaPasajeProduccion = fPasajeProduccion
                                CamposObligatoriosEstandard = False
                                Exit Function
                            End If
                        End If
                    Else
                        MsgBox "Debe ingresar una fecha v�lida para pasaje a Producci�n.", vbOKOnly + vbInformation, "Fecha prevista de pasaje a Producci�n"
                        CamposObligatoriosEstandard = False
                        Exit Function
                    End If
                End If
            End If
        End If
        If InStr(1, "EJECUC|EJECRK|TERMIN|", sOpcionSeleccionada, vbTextCompare) > 0 Then           ' Si la opci�n seleccionada es "En ejecuci�n" o "Replanificado en ejecuci�n" o "Finalizado"
            If cRegulatorio = "-" Then
                Select Case sOpcionSeleccionada
                    Case "EJECUC", "EJECRK": MsgBox "El grupo no puede pasar al estado EJECUCION o REPLANIFICADO EN EJECUC." & vbCrLf & "Debe encontrarse definido el atributo Regulatorio por el Responsable de Sector o el Business Partner.", vbOKOnly + vbInformation, "Regulatorio"
                    Case "TERMIN": MsgBox "El grupo no puede pasar al estado FINALIZADO." & vbCrLf & "Debe encontrarse definido el atributo Regulatorio por el Responsable de Sector o el Business Partner.", vbOKOnly + vbInformation, "Regulatorio"
                End Select
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                CamposObligatoriosEstandard = False
                Exit Function
            End If
        End If
    End If
    ' --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ' Control de la fecha de fin de suspensi�n
    ' --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    If sOpcionSeleccionada = "SUSPEN" And bEsGrupoHomologable Then
        If txtFechaFinSuspension.DateValue = "" Then                            ' Primero controlo la fecha de fin de suspensi�n
            MsgBox "Se debe informar la fecha estimada de fin de la suspensi�n", vbOKOnly + vbInformation
            CamposObligatoriosEstandard = False
            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
            txtFechaFinSuspension.SetFocus
            Exit Function
        Else
            If Not IsDate(txtFechaFinSuspension.DateValue) Then
                MsgBox "Se debe informar una fecha estimada de fin de la suspensi�n v�lida", vbOKOnly + vbInformation
                CamposObligatoriosEstandard = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                txtFechaFinSuspension.SetFocus
                Exit Function
            End If
            If CDate(txtFechaFinSuspension.DateValue) < date Then
                MsgBox "Se debe informar una fecha estimada de fin de la suspensi�n mayor o igual al d�a de hoy", vbOKOnly + vbInformation
                CamposObligatoriosEstandard = False
                txtFechaFinSuspension.text = CStr(date)
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                txtFechaFinSuspension.SetFocus
                Exit Function
            End If
        End If
        ' Segundo, el motivo de suspensi�n
        If cboMotivo.ListIndex = -1 Then
            MsgBox "Se debe seleccionar un motivo de suspensi�n de la lista", vbOKOnly + vbInformation
            CamposObligatoriosEstandard = False
            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
            txtFechaFinSuspension.SetFocus
            Exit Function
        End If
    End If
    ' Controles para observaciones en el cambio de estado
    If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) = 0 Then
        Select Case sOpcionSeleccionada
            Case "RECHTE", "OPINOK", "EVALOK", "CANCEL", "REVISA", "PLANRK", "ESTIRK", "EJECRK"
                If ClearNull(Me.obsResp.text) = "" Then
                    If Not (bEsGrupoSeguridadInformatica Or bEsGrupoTecnologia) Then
                        MsgBox "Se deben ingresar observaciones", vbOKOnly + vbInformation
                        CamposObligatoriosEstandard = False
                        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                        Exit Function
                    End If
                End If
            Case "SUSPEN"
                If ClearNull(Me.obsResp.text) = "" Then
                    MsgBox "Se deben ingresar observaciones", vbOKOnly + vbInformation
                    CamposObligatoriosEstandard = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
        End Select
    End If
    CamposObligatoriosEstandard = True
End Function

Private Function CamposObligatoriosSOx() As Boolean
    Dim ExisteDocumentoAlcance As Boolean
    Dim locGrupo As String
    Dim locSector As String
    Dim bExcepcion As Boolean
    Dim bExcepcionFechaControl As Boolean
    
    ExisteDocumentoAlcance = False
    CamposObligatoriosSOx = False
    If sOpcionSeleccionada = "TERMIN" Then
        ' Documento de alcance
        If sTipoPet = "PRJ" Or UCase(sImpacto) = "S" Then                       ' Documento de alcance: C100
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                MsgBox "El grupo no puede pasar al estado FINALIZADO." & Chr(13) & "Falta el documento de alcance de tipo C100.", vbOKOnly + vbInformation
                CamposObligatoriosSOx = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            ExisteDocumentoAlcance = True
        Else
            If InStr(1, "EVOL|OPTI|NUEV|", sClasePet, vbTextCompare) > 0 Then      ' Documento de alcance: P950
                If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
                    MsgBox "El grupo no puede pasar al estado FINALIZADO." & Chr(13) & "Falta el documento de alcance de tipo P950.", vbOKOnly + vbInformation
                    CamposObligatoriosSOx = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
                ExisteDocumentoAlcance = True
            End If
        End If
        ' Conforme al documento de alcance
        If ExisteDocumentoAlcance Then
            If Not sp_GetPeticionConf(glNumeroPeticion, "ALCA", 1) Then
                MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el Conforme final al documento de alcance (ALCA).", vbOKOnly + vbInformation
                CamposObligatoriosSOx = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        End If
        ' Documento de casos de prueba: C204
        If Not InStr(1, "SPUF|CORR|ATEN|", sClasePet, vbTextCompare) > 0 Then
            If bEsGrupoHomologable Then
                If Not sp_GetAdjuntosPet(glNumeroPeticion, "C204", Null, Null) Then
                    MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el documento de casos de prueba (C204) para este grupo.", vbOKOnly + vbInformation
                    CamposObligatoriosSOx = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
            End If
        End If
        ' *** Control T710 ***
        If bEsGrupoHomologable Then
            If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then
                If Not sp_GetAdjuntosPet(glNumeroPeticion, "T710", sSector, glGrupo) Then
                    MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el documento Principios de Desarrollo Aplicativo (T710) para este grupo.", vbOKOnly + vbInformation   ' add -015- a.
                    CamposObligatoriosSOx = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
            End If
        End If
        ' - CONFORMES DE USUARIO -
        
        If bOrigenIncidencia Then
            ' Conforme final de datos del usuario: DATF
            If Not sp_GetPeticionConf(glNumeroPeticion, "DATF", 1) Then
                MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & _
                        "Falta el conforme final de datos de usuario (DATF).", vbOKOnly + vbInformation
                CamposObligatoriosSOx = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        Else
            ' Conforme final de pruebas del usuario: TEST
            If Not sp_GetPeticionConf(glNumeroPeticion, "TEST", 1) Then
                MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & _
                        "Falta el conforme final de pruebas del usuario (TEST).", vbOKOnly + vbInformation
                CamposObligatoriosSOx = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        End If
    End If
    
    ' Si es una petici�n con origen en incidencia (SGI) entonces la finalizaci�n o
    ' cancelaci�n del grupo queda supeditada a que el grupo homologador est� finalizado.
    If bOrigenIncidencia Then
        If InStr(1, "TERMIN|CANCEL|RECHAZ|", sOpcionSeleccionada, vbTextCompare) > 0 Then
            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sHomoGrupoEstado, vbTextCompare) = 0 Then
                MsgBox "El grupo no puede cambiar al estado " & UCase(TextoCombo(cboAccion, CodigoCombo(cboAccion, True), True)) & Chr(13) & _
                        "hasta que homologaci�n no finalice su participaci�n.", vbOKOnly + vbInformation
                CamposObligatoriosSOx = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        End If
    End If
    
    CamposObligatoriosSOx = True
End Function

Private Function CamposObligatoriosFechas() As Boolean
    CamposObligatoriosFechas = False
    ' Controles sobre cantidad de horas informadas y fechas de inicio y finalizaci�n
    Select Case sOpcionSeleccionada
        Case "ESTIOK", "ESTIRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas presupuestadas.", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        Case "PLANOK", "PLANRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas presupuestadas.", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                Call setHabilCtrl(cmdConfirmar, NORMAL)
                Call setHabilCtrl(cmdCerrar, NORMAL)
                txtHoras.SetFocus
                Exit Function
            End If
            If Not IsDate(txtFechaInicio.DateValue) Then
                MsgBox "Se debe informar la fecha de Inicio planificada", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                Call setHabilCtrl(cmdConfirmar, NORMAL)
                Call setHabilCtrl(cmdCerrar, NORMAL)
                txtFechaInicio.SetFocus
                Exit Function
            End If
            If Not IsDate(txtFechaTermin.DateValue) Then
                MsgBox "Se debe informar la fecha de Fin planificada", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                Call setHabilCtrl(cmdConfirmar, NORMAL)
                Call setHabilCtrl(cmdCerrar, NORMAL)
                txtFechaTermin.SetFocus
                Exit Function
            End If
        Case "EJECUC", "EJECRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas presupuestadas.", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If Not IsDate(txtFechaIniReal.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio Real", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If Not IsDate(txtFechaInicio.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If Not IsDate(txtFechaTermin.DateValue) Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
        Case "TERMIN"
            If Not glEsHomologador Then
                If Val(txtHoras.text) = 0 Then
                    MsgBox "Se deben informar las horas presupuestadas.", vbOKOnly + vbInformation
                    CamposObligatoriosFechas = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
            End If
            If Not IsDate(txtFechaFinReal.DateValue) Then
                MsgBox "Se debe informar Fecha de Fin Real", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If Not IsDate(txtFechaIniReal.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio Real", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If Not IsDate(txtFechaInicio.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
            If Not IsDate(txtFechaTermin.DateValue) Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatoriosFechas = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
            End If
    End Select
    
    ' Control de fechas de inicio y de finalizaci�n
    If (Not IsNull(txtFechaInicio.DateValue)) And (Not IsNull(txtFechaTermin.DateValue)) Then
        If txtFechaTermin.DateValue < txtFechaInicio.DateValue Then
            MsgBox "La Fecha Inicio Plan. no debe ser mayor a la Fecha Fin Plan.", vbOKOnly + vbInformation
            CamposObligatoriosFechas = False
            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
            Exit Function
        End If
    End If
    ' Control de fechas de inicio real y de finalizaci�n real
    If (Not IsNull(txtFechaIniReal.DateValue)) And (Not IsNull(txtFechaFinReal.DateValue)) Then
        If txtFechaFinReal.DateValue < txtFechaIniReal.DateValue Then
            MsgBox "La Fecha Inicio Real no debe ser mayor a la Fecha Fin Real", vbOKOnly + vbInformation
            CamposObligatoriosFechas = False
            If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
            If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
            Exit Function
        End If
    End If
    ' Control de fecha de inicio real mayor al today
    If txtFechaIniReal.DateValue > date Then
        MsgBox "La Fecha Inicio Real no debe ser mayor a la fecha del d�a.", vbOKOnly + vbInformation
        CamposObligatoriosFechas = False
        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
        If txtFechaIniReal.visible Then txtFechaIniReal.SetFocus
        Exit Function
    End If
    ' Control de fecha de finalizaci�n real mayor al today
    If txtFechaFinReal.DateValue > date Then
        MsgBox "La Fecha Fin Real no debe ser mayor a la fecha del d�a.", vbOKOnly + vbInformation
        CamposObligatoriosFechas = False
        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
        Exit Function
    End If
    CamposObligatoriosFechas = True
End Function

Private Function CamposObligatoriosHoras() As Boolean
    CamposObligatoriosHoras = False
    If sOpcionSeleccionada = "TERMIN" Then
        If Not bOrigenIncidencia Then
            If bEsGrupoHomologable Then
                If Not sp_VerPeticionHoras(glNumeroPeticion) Then
                    MsgBox "El grupo no puede pasar a FINALIZADO." & Chr(13) & "No posee horas trabajadas informadas", vbOKOnly + vbInformation
                    CamposObligatoriosHoras = False
                    If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                    If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                    Exit Function
                End If
            End If
        End If
    End If
    CamposObligatoriosHoras = True
End Function

Private Function CamposObligatoriosHomologacion() As Boolean
    Dim bOKLider As Boolean
    
    If sOpcionSeleccionada = "TERMIN" Then
        If InStr(1, "OPTI|NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then
            If sp_GetGrupoHomologable(glGrupo) Then
                If Not sp_GetPeticionConf(glNumeroPeticion, "HSOB.F", Null) And _
                   Not sp_GetPeticionConf(glNumeroPeticion, "HOME.F", Null) Then
                        MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta alguno de los Conformes finales de Homologaci�n.", vbOKOnly + vbInformation
                        CamposObligatoriosHomologacion = False
                        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                        Exit Function
                End If
            End If
        End If
    End If
    If sOpcionSeleccionada = "TERMIN" Then
        If bEsGrupoHomologable Then
            If Not bOrigenIncidencia Then
                If sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Or _
                   sp_GetPeticionConf(glNumeroPeticion, "HOMA.F", Null) Then
                        MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & "La petici�n tiene un NO conforme de homologaci�n.", vbOKOnly + vbInformation
                        CamposObligatoriosHomologacion = False
                        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                        Exit Function
                End If
            End If
        End If
    End If

    bOKLider = False
    If bEsGrupoHomologable And sOpcionSeleccionada = "TERMIN" Then
        If sp_GetPeticionConf(glNumeroPeticion, "TEST", Null) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                    bOKLider = True     ' Tiene un conforme otorgado por un l�der de DYD
                    Exit Do
                End If
                aplRST.MoveNext
                DoEvents
            Loop
            ' Entonces, busco que tenga el conforme de homologaci�n para continuar. Caso contrario,
            ' no se permite finalizar el grupo hasta que este el conforme de homologaci�n.
            If bOKLider Then
                If Not sp_GetPeticionConf(glNumeroPeticion, "HSOB.%", Null) And _
                   Not sp_GetPeticionConf(glNumeroPeticion, "HOME.%", Null) Then
                        MsgBox "El grupo no puede cambiar al estado FINALIZADO." & Chr(13) & _
                               "Falta alguno de los conforme de homologaci�n.", vbOKOnly + vbInformation
                        CamposObligatoriosHomologacion = False
                        If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                        If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                        Exit Function
                End If
            End If
        End If
    End If
    CamposObligatoriosHomologacion = True
End Function

Private Sub txtFechaIniReal_LostFocus()
    If txtFechaInicio.Enabled = True And txtFechaInicio.text = "" And txtFechaIniReal.text <> "" Then
        txtFechaInicio.text = txtFechaIniReal.text
    End If
End Sub

Private Sub txtFechaFinReal_LostFocus()
    If txtFechaTermin.Enabled = True And txtFechaTermin.text = "" And txtFechaFinReal.text <> "" Then
        txtFechaTermin.text = txtFechaFinReal.text
    End If
End Sub

Private Sub txtFechaInicio_LostFocus()
    If txtFechaIniReal.Enabled = True And txtFechaIniReal.text = "" And txtFechaInicio.text <> "" Then
        txtFechaIniReal.text = txtFechaInicio.text
    End If
End Sub

Private Sub ActualizarHomologacion()
    Dim NuevoEstado_Leyenda As String
    Dim NroHistorial As Long
    Dim cMensaje1 As String
    Dim cMensaje2 As String
    
    If sp_GetEstado(sHomoGrupoEstadoNuevo) Then        ' Obtengo el nombre del estado
        NuevoEstado_Leyenda = ClearNull(aplRST.Fields!nom_estado)
    End If
    
    NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", NuevoEstadoPeticion, glHomologacionSector, sHomoSectorEstado, glHomologacionGrupo, sHomoGrupoEstadoNuevo, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & UCase(NuevoEstado_Leyenda) & " � Cambio autom�tico de estado del Grupo Homologador.")
    Call sp_UpdatePeticionGrupo(glNumeroPeticion, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, sHomoGrupoEstadoNuevo, date, "", NroHistorial, 0)
    Call sp_DoMensaje("GRU", "AGCHGEST", glNumeroPeticion, glHomologacionGrupo, sHomoGrupoEstadoNuevo, "Mensaje1", "Mensaje2")
    
    
    sHomoSectorEstadoNuevo = getNuevoEstadoSector(glNumeroPeticion, glHomologacionSector)
    If sHomoSectorEstado <> sHomoSectorEstadoNuevo Then
        Call sp_UpdatePeticionSector(glNumeroPeticion, glHomologacionSector, Null, Null, Null, Null, 0, sHomoSectorEstadoNuevo, date, "", NroHistorial, 0)
    End If
End Sub

Private Sub AgregarHomologacion()
    Dim NroHistorial As Long
    Dim cMensaje1 As String
    Dim cMensaje2 As String
    
    Call sp_InsertPeticionGrupo(glNumeroPeticion, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, sHomoGrupoEstadoNuevo, date, "", 0, 0)                     ' Es agregado el grupo Homologador en estado "A estimar Esfuerzo"
    Call sp_DoMensaje("GRU", "PAUTHOM", glNumeroPeticion, glHomologacionGrupo, sHomoGrupoEstadoNuevo, "", "")
    Call sp_DoMensaje("GRU", "PAUTHOM", glNumeroPeticion, glHomologacionGrupo, sHomoGrupoEstadoNuevo, "", "")
    NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", NuevoEstadoPeticion, glHomologacionSector, sHomoGrupoEstadoNuevo, glHomologacionGrupo, sHomoGrupoEstadoNuevo, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & "Asignaci�n autom�tica del grupo Homologador �")  ' Detalla el historial
    Call sp_UpdatePeticionGrupo(glNumeroPeticion, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, sHomoGrupoEstadoNuevo, date, "", NroHistorial, 0)          ' Esta llamada es solo para darle el numero de historial
End Sub
'GMT02 - INI
Private Function recupHabilitaciones() As Boolean
    
    'se recuperan los valores para saber que flujo de trabajo esta habilitado
    recupHabilitaciones = False
    Hab_RGyP = True

    'indica si ver la solapa de vinculados
    If sp_GetVarios("HAB_RGyP") Then
        Hab_RGyP = ClearNull(aplRST.Fields!var_numero)
    End If
    
    recupHabilitaciones = True
 End Function
'GMT02 - FIN


