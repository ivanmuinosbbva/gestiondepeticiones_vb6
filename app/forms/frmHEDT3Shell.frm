VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmHEDT3Shell 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Definir los campos que componen el COPY"
   ClientHeight    =   6660
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10125
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHEDT3Shell.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6660
   ScaleWidth      =   10125
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraBotonera 
      Height          =   6615
      Left            =   8760
      TabIndex        =   17
      Top             =   0
      Width           =   1335
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   500
         Left            =   60
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n actual"
         Top             =   5490
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   60
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   6000
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   500
         Left            =   60
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el campo seleccionado"
         Top             =   4980
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   500
         Left            =   60
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el campo seleccionado"
         Top             =   4470
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   500
         Left            =   60
         TabIndex        =   26
         TabStop         =   0   'False
         ToolTipText     =   "Agregar un nuevo campo"
         Top             =   3960
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      Caption         =   " Modo "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   120
      TabIndex        =   13
      Top             =   3240
      Width           =   8535
      Begin VB.ListBox lstFormato 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   4680
         TabIndex        =   9
         Top             =   1080
         Width           =   3735
      End
      Begin VB.ComboBox cmbSubTipo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmHEDT3Shell.frx":014A
         Left            =   840
         List            =   "frmHEDT3Shell.frx":014C
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1440
         Width           =   3615
      End
      Begin VB.ComboBox cmbTipo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmHEDT3Shell.frx":014E
         Left            =   840
         List            =   "frmHEDT3Shell.frx":0150
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1080
         Width           =   3615
      End
      Begin VB.ComboBox cmbTipoDato 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmHEDT3Shell.frx":0152
         Left            =   4680
         List            =   "frmHEDT3Shell.frx":0154
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   720
         Width           =   2055
      End
      Begin AT_MaskText.MaskText txtIdCampo 
         Height          =   300
         Left            =   840
         TabIndex        =   0
         Top             =   360
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   529
         MaxLength       =   18
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   18
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtRutina 
         Height          =   300
         Left            =   840
         TabIndex        =   8
         Top             =   1800
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   300
         Left            =   4680
         TabIndex        =   1
         Top             =   360
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   529
         MaxLength       =   20
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   20
      End
      Begin AT_MaskText.MaskText txtPosDesde 
         Height          =   300
         Left            =   840
         TabIndex        =   2
         Top             =   720
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   529
         MaxLength       =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   4
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtLongitud 
         Height          =   300
         Left            =   2880
         TabIndex        =   3
         Top             =   720
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   529
         MaxLength       =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   4
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtDecimales 
         Height          =   300
         Left            =   7680
         TabIndex        =   5
         Top             =   720
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   529
         MaxLength       =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   4
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Subtipo"
         Height          =   180
         Index           =   10
         Left            =   120
         TabIndex        =   32
         Top             =   1515
         Width           =   570
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         Height          =   180
         Index           =   9
         Left            =   120
         TabIndex        =   31
         Top             =   1155
         Width           =   330
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Decimales"
         Height          =   180
         Index           =   7
         Left            =   6840
         TabIndex        =   25
         Top             =   780
         Width           =   795
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Tipo dato"
         Height          =   180
         Index           =   6
         Left            =   3840
         TabIndex        =   24
         Top             =   780
         Width           =   705
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Longitud"
         Height          =   180
         Index           =   2
         Left            =   2040
         TabIndex        =   23
         Top             =   780
         Width           =   645
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Pos. Dde"
         Height          =   180
         Index           =   1
         Left            =   120
         TabIndex        =   22
         Top             =   780
         Width           =   675
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         Height          =   180
         Index           =   8
         Left            =   3720
         TabIndex        =   16
         Top             =   420
         Width           =   900
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Rutina"
         Height          =   180
         Index           =   3
         Left            =   120
         TabIndex        =   15
         Top             =   1860
         Width           =   480
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Campo"
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   420
         Width           =   525
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3135
      Left            =   120
      TabIndex        =   18
      Top             =   120
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   5530
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraAudit 
      Height          =   1095
      Left            =   120
      TabIndex        =   19
      Top             =   5520
      Width           =   3375
      Begin AT_MaskText.MaskText txtUltMod 
         Height          =   300
         Left            =   1800
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   660
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUsuario 
         Height          =   300
         Left            =   1800
         TabIndex        =   10
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de �ltima modificaci�n"
         Height          =   420
         Index           =   4
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   1695
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Usuario que realiz� �ltima modificaci�n"
         Height          =   360
         Index           =   5
         Left            =   120
         TabIndex        =   20
         Top             =   180
         Width           =   1560
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraOtro 
      Height          =   1095
      Left            =   3600
      TabIndex        =   12
      Top             =   5520
      Width           =   5055
   End
End
Attribute VB_Name = "frmHEDT3Shell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.10.2009 - Se agrega el manejo de formulario para perfiles de Administradores de Seg. Inform�tica
' -002- a. FJS 12.11.2009 - Se cambia la obtenci�n de la descripci�n de una rutina desde una tabla ahora.
' -002- b. FJS 12.11.2009 - Se agregan campos �tiles a la grilla.
' -003- a. FJS 12.01.2011 - Nuevo: nuevas funcionalidades para Seguridad Inform�tica.

Option Explicit

Public sDSNId As String, sCpyId As String

Private Const CPO_ID = 0
Private Const CPO_DSC = 1
Private Const cpo_nrobyte = 2
Private Const cpo_canbyte = 3
Private Const cpo_decimals = 4
Private Const cpo_type = 5
Private Const cpo_nomrut = 6
Private Const CPO_FEULT = 7
Private Const CPO_USERID = 8
Private Const CPO_TIPOCAMPO = 9
Private Const CPO_ESTADO = 10

Dim sOpcionSeleccionada As String
Dim inCarga As Boolean
Dim bSegInf As Boolean      ' add -001- a.

Private Sub Form_Load()
    '{ add -001- a. Si el usuario tiene perfil de Adm. de Seg. Inform�tica, no puede realizar ninguna de las otras
    '               tareas respecto del cat�logo. Solo puede visar elementos.
    If InPerfil("ASEG") Then
        bSegInf = True
        HabilitarBotones 2, "NO"
    Else
        bSegInf = False
        HabilitarBotones 0, "NO"
    End If
    '}
    Call Inicializar
    Call Cargar_Grilla
    'HabilitarBotones 0, "NO"       ' del -001- a.
    Call Status("Listo.")
End Sub

Private Sub Inicializar()
    Me.Top = 720
    Me.Left = 720
    Call LockProceso(True)
    DoEvents
    
    cmbTipo.Font.name = "Verdana"
    cmbSubTipo.Font.name = "Verdana"
    cmbTipo.Font.Size = 8
    cmbSubTipo.Font.Size = 8
    
    Call setHabilCtrl(txtUsuario, "DIS")
    Call setHabilCtrl(txtUltMod, "DIS")
    'Call IniciarScroll(grdDatos)
    
    Call CargarCombosConTodo
    Call LockProceso(False)
End Sub

Private Sub CargarCombos()
    With cmbTipoDato
        .Clear
        .AddItem "ALFABETIC" & Space(100) & "||A"
        .AddItem "ALFANUMERIC" & Space(100) & "||X"
        .AddItem "DISPLAY" & Space(100) & "||9"
        .AddItem "DECIMAL" & Space(100) & "||D"
        .AddItem "SMALLINT" & Space(100) & "||S"
        .AddItem "INTEGER" & Space(100) & "||I"
        .AddItem "DOUBLE" & Space(100) & "||O"
        .AddItem "FLOAT" & Space(100) & "||F"
        .AddItem "COMP" & Space(100) & "||C"
        .AddItem "COMP-2" & Space(100) & "||2"
        .AddItem "COMP-3" & Space(100) & "||3"
        .AddItem "BINNARY" & Space(100) & "||B"
        .ListIndex = 0
    End With
    
    ' Cargo los tipos definidos
    If sp_GetHEDT011(Null, Null) Then
        With cmbTipo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!tipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!tipo_id)
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
    ' Cargo los subtipos definidos
    If sp_GetHEDT012(CodigoCombo(cmbTipo, True), Null, Null, Null) Then
        With cmbSubTipo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!subtipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!subtipo_id)
                aplRST.MoveNext
                DoEvents
            Loop
            .AddItem "Otro..." & Space(100) & "||OTRO"
        End With
    End If
End Sub

Private Sub CargarCombosEdicion()
    Dim cTipo As String, cSubTipo As String
    
    inCarga = True
    ' Cargo los tipos definidos
    cTipo = CodigoCombo(cmbTipo, True)
    If sp_GetHEDT011(Null, Null) Then
        With cmbTipo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!tipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!tipo_id)
                aplRST.MoveNext
                DoEvents
            Loop
        End With
        cmbTipo.ListIndex = SetCombo(cmbTipo, cTipo, True)
    End If
    ' Cargo los subtipos definidos
    cSubTipo = CodigoCombo(cmbSubTipo, True)
    If sp_GetHEDT012(CodigoCombo(cmbTipo, True), Null, Null, Null) Then
        With cmbSubTipo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!subtipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!tipo_id) & ClearNull(aplRST.Fields!subtipo_id)
                aplRST.MoveNext
                DoEvents
            Loop
            .AddItem "Otro..." & Space(100) & "||OTRO"
        End With
        cmbSubTipo.ListIndex = SetCombo(cmbSubTipo, cSubTipo, True)
    End If
    inCarga = False
End Sub

Private Sub CargarCombosConTodo()
    With cmbTipoDato
        .Clear
        .AddItem "ALFABETIC" & Space(100) & "||A"
        .AddItem "ALFANUMERIC" & Space(100) & "||X"
        .AddItem "DISPLAY" & Space(100) & "||9"
        .AddItem "DECIMAL" & Space(100) & "||D"
        .AddItem "SMALLINT" & Space(100) & "||S"
        .AddItem "INTEGER" & Space(100) & "||I"
        .AddItem "DOUBLE" & Space(100) & "||O"
        .AddItem "FLOAT" & Space(100) & "||F"
        .AddItem "COMP" & Space(100) & "||C"
        .AddItem "COMP-2" & Space(100) & "||2"
        .AddItem "COMP-3" & Space(100) & "||3"
        .AddItem "BINNARY" & Space(100) & "||B"
        .ListIndex = 0
    End With
    ' Cargo los tipos definidos
    If sp_GetHEDT011(Null, Null) Then
        With cmbTipo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!tipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!tipo_id)
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
    ' Cargo los subtipos definidos
    If sp_GetHEDT012(Null, Null, Null, Null) Then
        With cmbSubTipo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!subtipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!tipo_id) & ClearNull(aplRST.Fields!subtipo_id) ' del -002- a.
                '.AddItem ClearNull(aplRST.Fields!subtipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!subtipo_id) ' add -002- a.
                aplRST.MoveNext
                DoEvents
            Loop
            .AddItem "Otro..." & Space(100) & "||OTRO"
        End With
    End If
End Sub

Private Sub Inicializar_Grilla()
    With grdDatos
        .Visible = False
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Clear
        .cols = 11      ' upd -002- b. Antes 10.
        .Rows = 1
        .TextMatrix(0, CPO_ID) = "Campo": .ColWidth(CPO_ID) = 1000: .ColAlignment(CPO_ID) = 0
        .TextMatrix(0, CPO_DSC) = "Descripci�n": .ColWidth(CPO_DSC) = 1600
        .TextMatrix(0, cpo_nrobyte) = "Pos.": .ColWidth(cpo_nrobyte) = 600: .ColAlignment(cpo_nrobyte) = 3
        .TextMatrix(0, cpo_canbyte) = "Long.": .ColWidth(cpo_canbyte) = 600: .ColAlignment(cpo_canbyte) = 3
        .TextMatrix(0, cpo_decimals) = "Dec.": .ColWidth(cpo_decimals) = 600: .ColAlignment(cpo_decimals) = 3
        .TextMatrix(0, cpo_type) = "Tipo de dato": .ColWidth(cpo_type) = 1800
        .TextMatrix(0, cpo_nomrut) = "Rutina": .ColWidth(cpo_nomrut) = 1400
        .TextMatrix(0, CPO_FEULT) = "Ult. mod.": .ColWidth(CPO_FEULT) = 1800
        .TextMatrix(0, CPO_USERID) = "Usuario": .ColWidth(CPO_USERID) = 1000
        .TextMatrix(0, CPO_TIPOCAMPO) = "": .ColWidth(CPO_TIPOCAMPO) = 0
        .TextMatrix(0, CPO_ESTADO) = "": .ColWidth(CPO_ESTADO) = 0
        .BackColorBkg = Me.BackColor
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub Cargar_Grilla()
    With grdDatos
        Call Inicializar_Grilla
        If sp_GetHEDT003(sDSNId, sCpyId, Null) Then  ' upd -003- a.
            Call Puntero(True)
                Do While Not aplRST.EOF
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, CPO_ID) = ClearNull(aplRST.Fields!CPO_ID)
                    .TextMatrix(.Rows - 1, CPO_DSC) = ClearNull(aplRST.Fields!CPO_DSC)
                    .TextMatrix(.Rows - 1, cpo_nrobyte) = ClearNull(aplRST.Fields!cpo_nrobyte)
                    .TextMatrix(.Rows - 1, cpo_canbyte) = ClearNull(aplRST.Fields!cpo_canbyte)
                    .TextMatrix(.Rows - 1, cpo_decimals) = IIf(Val(ClearNull(aplRST.Fields!cpo_decimals)) = 0 Or ClearNull(aplRST.Fields!cpo_decimals) = "", "0", ClearNull(aplRST.Fields!cpo_decimals))
                    .TextMatrix(.Rows - 1, cpo_type) = TextoCombo(cmbTipoDato, ClearNull(aplRST.Fields!cpo_type), True)
                    .TextMatrix(.Rows - 1, cpo_nomrut) = ClearNull(aplRST.Fields!cpo_nomrut)
                    .TextMatrix(.Rows - 1, CPO_FEULT) = ClearNull(aplRST.Fields!CPO_FEULT)
                    .TextMatrix(.Rows - 1, CPO_USERID) = ClearNull(aplRST.Fields!CPO_USERID)
                    .TextMatrix(.Rows - 1, CPO_TIPOCAMPO) = ClearNull(aplRST.Fields!cpo_type)
                    .TextMatrix(.Rows - 1, CPO_ESTADO) = ClearNull(aplRST.Fields!Estado)    ' add -002- b.
                    aplRST.MoveNext
                    DoEvents
                Loop
            Call MostrarSeleccion
        End If
        .Visible = True
        Call Puntero(False)
    End With
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Call HabilitarBotones(0)
End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertHEDT003(sDSNId, sCpyId, txtIdCampo, txtPosDesde, txtLongitud, CodigoCombo(cmbTipoDato, True), IIf(txtDecimales.Text = "", 0, txtDecimales), txtDescripcion, txtRutina) Then  ' upd -003- a.
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdateHEDT003(sDSNId, sCpyId, txtIdCampo, txtPosDesde, txtLongitud, CodigoCombo(cmbTipoDato, True), IIf(txtDecimales.Text = "", 0, txtDecimales), txtDescripcion, txtRutina) Then  ' upd -003- a.
                    With grdDatos
                        If .RowSel > 0 Then
                            If sp_GetHEDT003(sDSNId, sCpyId, txtIdCampo) Then    ' upd -003- a.
                                .TextMatrix(.RowSel, 0) = ClearNull(aplRST.Fields!CPO_ID)
                                .TextMatrix(.RowSel, 1) = ClearNull(aplRST.Fields!CPO_DSC)
                                .TextMatrix(.RowSel, 2) = ClearNull(aplRST.Fields!cpo_nrobyte)
                                .TextMatrix(.RowSel, 3) = ClearNull(aplRST.Fields!cpo_canbyte)
                                .TextMatrix(.RowSel, 4) = IIf(Val(ClearNull(aplRST.Fields!cpo_decimals)) = 0 Or ClearNull(aplRST.Fields!cpo_decimals) = "", "0", ClearNull(aplRST.Fields!cpo_decimals))
                                .TextMatrix(.RowSel, 5) = TextoCombo(cmbTipoDato, ClearNull(aplRST.Fields!cpo_type), True)
                                .TextMatrix(.RowSel, 6) = ClearNull(aplRST.Fields!cpo_nomrut)
                                .TextMatrix(.RowSel, 7) = ClearNull(aplRST.Fields!CPO_FEULT)
                                .TextMatrix(.RowSel, 8) = ClearNull(aplRST.Fields!CPO_USERID)
                                .TextMatrix(.RowSel, 9) = ClearNull(aplRST.Fields!cpo_type)
                                .TextMatrix(.RowSel, 10) = ClearNull(aplRST.Fields!Estado)       ' add -002- b.
                            End If
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If MsgBox("�Esta seguro de eliminar definitivamente el CAMPO de la base?", vbQuestion + vbYesNo, "Eliminar CAMPO") = vbYes Then
                If sp_DeleteHEDT003(sDSNId, sCpyId, txtIdCampo) Then ' upd -003- a.
                    Call HabilitarBotones(0)
                    Call Cargar_Grilla
                End If
            End If
    End Select
    LockProceso (False)
End Sub

Private Function CamposObligatorios() As Boolean
    If Len(txtIdCampo) > 18 Then
        MsgBox "El nombre del campo no debe ser mayor a 18 posiciones.", vbExclamation + vbOKOnly, "Nombre de campo"
        CamposObligatorios = False
        Exit Function
    End If
    If txtIdCampo.Text = "" Then
        MsgBox "El nombre del campo no puede estar vacio.", vbExclamation + vbOKOnly, "Nombre de campo"
        CamposObligatorios = False
        Exit Function
    End If
    If txtDescripcion.Text = "" Then
        MsgBox "La descripci�n del campo no puede estar vacio.", vbExclamation + vbOKOnly, "Descripci�n de campo"
        CamposObligatorios = False
        Exit Function
    End If
    If Val(txtLongitud) = 0 Then
        MsgBox "La longitud desde debe ser un n�mero mayor a cero.", vbExclamation + vbOKOnly, "Longitud del campo"
        CamposObligatorios = False
        Exit Function
    End If
    If Val(txtPosDesde) = 0 Then
        MsgBox "La posici�n desde debe ser un n�mero mayor a cero.", vbExclamation + vbOKOnly, "Posici�n desde del campo"
        CamposObligatorios = False
        Exit Function
    End If
    If cmbTipoDato.ListIndex = -1 Then
        MsgBox "Debe especificar el tipo de dato.", vbExclamation + vbOKOnly, "Tipo de dato del campo"
        CamposObligatorios = False
        Exit Function
    End If
    If cmbTipo.ListIndex = -1 Then
        MsgBox "Debe especificar el tipo de rutina.", vbExclamation + vbOKOnly, "Tipo de rutina del campo"
        CamposObligatorios = False
        Exit Function
    End If
    If cmbSubTipo.ListIndex = -1 Then
        MsgBox "Debe especificar el subtipo de rutina.", vbExclamation + vbOKOnly, "Subtipo de rutina del campo"
        CamposObligatorios = False
        Exit Function
    End If
    If Len(txtRutina) > 8 Then
        MsgBox "El nombre de la rutina no debe ser mayor a 8 posiciones.", vbExclamation + vbOKOnly, "Nombre de rutina"
        CamposObligatorios = False
        Exit Function
    End If
    If txtRutina.Text = "" Then
        MsgBox "El nombre de la rutina no puede estar vacio.", vbExclamation + vbOKOnly, "Nombre de rutina"
        CamposObligatorios = False
        Exit Function
    End If
    CamposObligatorios = True
End Function

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            ' Controles de datos
            CargarCombosConTodo
            Call setHabilCtrl(txtIdCampo, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtPosDesde, "DIS")
            Call setHabilCtrl(txtLongitud, "DIS")
            Call setHabilCtrl(cmbTipoDato, "DIS")
            Call setHabilCtrl(txtDecimales, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtUltMod, "DIS")
            Call setHabilCtrl(txtUsuario, "DIS")
            Call setHabilCtrl(cmbTipo, "DIS")
            Call setHabilCtrl(cmbSubTipo, "DIS")
            Call setHabilCtrl(lstFormato, "DIS")
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call Cargar_Grilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    Call CargarCombos
                    ' Controles de datos
                    Call setHabilCtrl(txtIdCampo, "NOR")
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(txtPosDesde, "NOR")
                    Call setHabilCtrl(txtLongitud, "NOR")
                    Call setHabilCtrl(cmbTipoDato, "NOR")
                    Call setHabilCtrl(txtDecimales, "NOR")
                    'Call setHabilCtrl(txtRutina, "NOR")
                    Call setHabilCtrl(cmbTipo, "NOR")
                    Call setHabilCtrl(cmbSubTipo, "NOR")
                    Call setHabilCtrl(lstFormato, "NOR")
                    fraDatos.Caption = " AGREGAR "
                    txtIdCampo = ""
                    txtDescripcion = ""
                    txtPosDesde = ""
                    txtLongitud = ""
                    cmbTipoDato.ListIndex = -1
                    txtDecimales = ""
                    txtRutina = ""
                    cmbTipo.ListIndex = -1
                    cmbSubTipo.ListIndex = -1
                    lstFormato.Clear
                    txtUsuario = ""
                    txtUltMod = ""
                    fraDatos.Enabled = True
                    txtIdCampo.SetFocus
                Case "M"
                    Call CargarCombosEdicion
                    ' Controles de datos
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(txtPosDesde, "NOR")
                    Call setHabilCtrl(txtLongitud, "NOR")
                    Call setHabilCtrl(cmbTipoDato, "NOR")
                    Call setHabilCtrl(txtDecimales, "NOR")
                    'Call setHabilCtrl(txtRutina, "NOR")
                    Call setHabilCtrl(cmbTipo, "NOR")
                    Call setHabilCtrl(cmbSubTipo, "NOR")
                    Call setHabilCtrl(lstFormato, "NOR")
                    fraDatos.Caption = " MODIFICAR "
                    txtIdCampo.Enabled = False
                    fraDatos.Enabled = True
                    txtDescripcion.SetFocus
                Case "E"
                    fraDatos.Caption = " ELIMINAR "
                    fraDatos.Enabled = False
            End Select
        '{ add -001- a.
        Case 2
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            ' Controles de datos
            Call CargarCombosConTodo
            Call setHabilCtrl(txtIdCampo, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtPosDesde, "DIS")
            Call setHabilCtrl(txtLongitud, "DIS")
            Call setHabilCtrl(cmbTipoDato, "DIS")
            Call setHabilCtrl(txtDecimales, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtUltMod, "DIS")
            Call setHabilCtrl(txtUsuario, "DIS")
            Call setHabilCtrl(cmbTipo, "DIS")
            Call setHabilCtrl(cmbSubTipo, "DIS")
            Call setHabilCtrl(lstFormato, "DIS")
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call Cargar_Grilla
            Else
                Call MostrarSeleccion
            End If
        '}
    End Select
    Call LockProceso(False)
End Sub

Private Function MostrarSeleccion()
    inCarga = True
    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, 0) <> "" Then
                txtIdCampo = ClearNull(.TextMatrix(.RowSel, 0))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, 1))
                txtPosDesde = ClearNull(.TextMatrix(.RowSel, 2))
                txtLongitud = ClearNull(.TextMatrix(.RowSel, 3))
                cmbTipoDato.ListIndex = SetCombo(cmbTipoDato, ClearNull(.TextMatrix(.RowSel, 9)), True)
                txtDecimales = ClearNull(.TextMatrix(.RowSel, 4))
                txtRutina = ClearNull(.TextMatrix(.RowSel, 6))
                txtUltMod.DateValue = ClearNull(.TextMatrix(.RowSel, 7)): txtUltMod = txtUltMod.DateValue
                txtUsuario = ClearNull(.TextMatrix(.RowSel, 8))
                If sp_GetHEDT012(Null, Null, Null, ClearNull(.TextMatrix(.RowSel, 6))) Then
                    cmbTipo.ListIndex = SetCombo(cmbTipo, aplRST.Fields!tipo_id, True)
                    cmbSubTipo.ListIndex = SetCombo(cmbSubTipo, aplRST.Fields!tipo_id & aplRST.Fields!subtipo_id, True)    ' del -002- a.
                    Call cmbSubTipo_Click
                Else
                    'cmbTipo.ListIndex = PosicionCombo(cmbTipo, ClearNull(.TextMatrix(.RowSel, CPO_TIPOCAMPO)), True)
                    'cmbSubTipo.ListIndex = cmbSubTipo.ListCount - 1
                    cmbTipo.ListIndex = -1
                    cmbSubTipo.ListIndex = -1
                    lstFormato.Clear
                End If
                If .TextMatrix(.RowSel, 10) = "A" Then
                    Call setHabilCtrl(cmdAgregar, "NOR")
                    Call setHabilCtrl(cmdModificar, "NOR")
                    Call setHabilCtrl(cmdEliminar, "NOR")
                Else
                    Call setHabilCtrl(cmdAgregar, "DIS")
                    Call setHabilCtrl(cmdModificar, "DIS")
                    Call setHabilCtrl(cmdEliminar, "DIS")
                End If
            End If
        End If
    End With
    inCarga = False
End Function

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A", "M"
            'If MsgBox("�Confirma la cancelaci�n?", vbQuestion + vbYesNo, "Cancelar operaci�n") = vbYes Then
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
            'End If
        Case "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
    Call LockProceso(False)
End Sub

Private Sub grdDatos_RowColChange()
    If Not inCarga Then Call MostrarSeleccion
End Sub

Private Sub cmbTipo_Click()
    If Not inCarga Then
        If sp_GetHEDT012(CodigoCombo(cmbTipo, True), Null, Null, Null) Then
            With cmbSubTipo
                .Clear
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!subtipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!tipo_id) & ClearNull(aplRST.Fields!subtipo_id) ' del -002- a.
                    '.AddItem ClearNull(aplRST.Fields!subtipo_nom) & Space(100) & "||" & ClearNull(aplRST.Fields!subtipo_id)     ' add -002- a.
                    aplRST.MoveNext
                    DoEvents
                Loop
                .AddItem "Otro..." & Space(100) & "||OTRO"
                'If .ListCount > -1 Then .ListIndex = 0
            End With
        End If
    End If
End Sub

Private Sub cmbSubTipo_Click()
    Call setHabilCtrl(txtRutina, "DIS")
    With lstFormato
        .Clear
        '{ add -002- a.
        If sp_GetHEDT013(CodigoCombo(cmbTipo, True), Right(CodigoCombo(cmbSubTipo, True), 1), Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!item_nom)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        '}
    End With
    If ClearNull(sOpcionSeleccionada) <> "" Then
        If InStr(1, "A|M|", sOpcionSeleccionada, vbTextCompare) > 0 Then
            If CodigoCombo(cmbSubTipo, True) <> "OTRO" Then
                'If sOpcionSeleccionada = "A" Or sOpcionSeleccionada = "M" Then
                If sp_GetHEDT012(CodigoCombo(cmbTipo, True), Right(CodigoCombo(cmbSubTipo, True), 1), Null, Null) Then
                    txtRutina = ClearNull(aplRST.Fields!rutina)
                End If
                'End If
            '{ add -002- a.
            Else
                txtRutina = ""
                Call setHabilCtrl(txtRutina, "NOR")
            '}
            End If
        End If
    End If
End Sub
