VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptPetAreaSoli 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4770
   ClientLeft      =   795
   ClientTop       =   2025
   ClientWidth     =   9795
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4770
   ScaleWidth      =   9795
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      Height          =   3285
      Left            =   0
      TabIndex        =   1
      Top             =   840
      Width           =   9780
      Begin VB.ComboBox cboAreaEjec 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   720
         Width           =   8160
      End
      Begin VB.Frame fraNivel 
         Caption         =   " Nivel de agrupamiento "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1665
         Left            =   6855
         TabIndex        =   11
         Top             =   1320
         Width           =   2505
         Begin VB.OptionButton OptNivel 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   240
            TabIndex        =   14
            Top             =   360
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   240
            TabIndex        =   13
            Top             =   645
            Width           =   1125
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   1
            Left            =   240
            TabIndex        =   12
            Top             =   900
            Width           =   1245
         End
      End
      Begin VB.ComboBox cboAreaSoli 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   360
         Width           =   8160
      End
      Begin AT_MaskText.MaskText txtDESDE 
         Height          =   315
         Left            =   1440
         TabIndex        =   7
         Top             =   1080
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTA 
         Height          =   315
         Left            =   1440
         TabIndex        =   8
         Top             =   1440
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Area ejecutora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   16
         Top             =   780
         Width           =   1080
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   10
         Top             =   1500
         Width           =   420
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   9
         Top             =   1140
         Width           =   450
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Area solicitante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   5
         Top             =   420
         Width           =   1110
      End
   End
   Begin VB.Frame fraOpciones 
      Height          =   645
      Left            =   0
      TabIndex        =   2
      Top             =   4100
      Width           =   9780
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7800
         TabIndex        =   4
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8760
         TabIndex        =   3
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "2. HORAS TRABAJADAS POR �REA SOLICITANTE"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4575
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptPetAreaSoli.frx":0000
      Stretch         =   -1  'True
      Top             =   0
      Width           =   9780
   End
End
Attribute VB_Name = "rptPetAreaSoli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 15.05.2008 - Se cambia la asignaci�n de la propiedad 'ReportFileName' del control CrystalReport utilizando la funci�n de formato de ruta corto (8.3) porque por ejemplo, cuando el string tiene 127 caracteres da un error 20504 en runtime al momento de mostrar el reporte (da error de archivo de reporte no encontrado).
' -003- a. FJS 15.04.2009 - Se cambian los simbolos para la carga de los combos para facilitar la correcta visualizaci�n de datos en pantalla.
' -004- a. FJS 03.09.2009 - Se restringe la carga del combobox solo para �reas habilitadas.

Option Explicit

Dim flgEnCarga As Boolean
Dim xNivel As Long

Private Sub cboAreaEjec_Click()
    cboAreaEjec.ToolTipText = TextoCombo(cboAreaEjec)
End Sub

Private Sub Form_Load()
    flgEnCarga = False
    Call InicializarCombos
    txtDESDE.Text = Format(DateAdd("m", -1, date), "dd/mm/yyyy")
    txtHASTA.Text = Format(date, "dd/mm/yyyy")
    OptNivel(3).Value = True
End Sub

Sub InicializarCombos()
    Dim sDireccion As String
    Dim sGerencia As String
    
    flgEnCarga = True
    Call Status("Inicializando...")
    Call Puntero(True)
    
    If sp_GetDireccion("", "S") Then        ' upd -004- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            'cboAreaSoli.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||DIRE|" & Trim(aplRST!cod_direccion)
            cboAreaSoli.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboAreaEjec.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||DIRE|" & Trim(aplRST!cod_direccion)
                cboAreaEjec.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    'Call Status("Cargando Gerencias")      ' del -003- a.
    If sp_GetGerenciaXt("", "", "S") Then   ' upd -004- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            'cboAreaSoli.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & Space(200) & "||GERE|" & Trim(aplRST!cod_gerencia)     ' upd -003- a. Se cambia ">> " por "� ".
            cboAreaSoli.AddItem sDireccion & "� " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||GERE|" & Trim(aplRST!cod_gerencia)
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboAreaEjec.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & Space(200) & "||GERE|" & Trim(aplRST!cod_gerencia) ' upd -003- a. Se cambia ">> " por "� ".
                cboAreaEjec.AddItem sDireccion & "� " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||GERE|" & Trim(aplRST!cod_gerencia)
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    'Call Status("Cargando Sector")                 ' del -003- a.
    If sp_GetSectorXt(Null, Null, Null, "S") Then   ' upd -004- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            'cboAreaSoli.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & Space(200) & "||SECT|" & Trim(aplRST!cod_sector)     ' upd -003- a. Se cambia ">> " por "� ".
            cboAreaSoli.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & ESPACIOS & "||SECT|" & Trim(aplRST!cod_sector)
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboAreaEjec.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & Space(200) & "||SECT|" & Trim(aplRST!cod_sector) ' upd -003- a. Se cambia ">> " por "� ".
                cboAreaEjec.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & ESPACIOS & "||SECT|" & Trim(aplRST!cod_sector)
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    'Call Status("Cargando Grupo")          ' del -003- a.
    If sp_GetGrupoXt("", "", "S") Then      ' upd -004- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboAreaEjec.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & Space(200) & "||GRUP|" & Trim(aplRST!cod_grupo)  ' upd -003- a. Se cambia ">> " por "� ".
                cboAreaEjec.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & ESPACIOS & "||GRUP|" & Trim(aplRST!cod_grupo)
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
   
    cboAreaSoli.AddItem "Todo el BBVA" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL|NULL", 0
    cboAreaEjec.AddItem "Todo el BBVA" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL|NULL", 0
    
    cboAreaSoli.ListIndex = 0
    cboAreaEjec.ListIndex = 0
    
    flgEnCarga = False
    Call Status("Listo.")
    Call Puntero(False)
End Sub

Private Sub cboAreaSoli_Click()
    Dim vRetorno() As String
    Dim xNivel As String
    If flgEnCarga = True Then
        Exit Sub
    End If
    flgEnCarga = True
        
    If ParseString(vRetorno, DatosCombo(cboAreaSoli), "|") = 0 Then
        MsgBox ("Error")
    End If
    cboAreaSoli.ToolTipText = TextoCombo(cboAreaSoli)
    
    xNivel = vRetorno(1)
    Select Case xNivel
    Case "NULL"
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = True
           OptNivel(3).Value = True
    Case "DIRE"
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = True
           OptNivel(3).Value = True
    Case "GERE"
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = True
           OptNivel(3).Enabled = False
           OptNivel(2).Value = True
    Case "SECT"
           OptNivel(1).Enabled = True
           OptNivel(2).Enabled = False
           OptNivel(3).Enabled = False
           OptNivel(1).Value = True
    End Select
    flgEnCarga = False
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdOK_Click()
    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        'Call ViejoReporte
    End If
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    Dim sTitulo(3) As String
    Dim vRetSoli() As String
    Dim vRetEjec() As String
    
    sReportName = "\hstrasolic6.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    
    sTitulo(0) = "�rea Solicitante: " & ClearNull(TextoCombo(cboAreaSoli))
    sTitulo(1) = "�rea ejecutora: " & ClearNull(TextoCombo(cboAreaEjec))
    sTitulo(2) = "Entre: " & txtDESDE & " y " & txtHASTA
    
    If ParseString(vRetSoli, DatosCombo(cboAreaSoli), "|") = 0 Then MsgBox ("Error")
    If ParseString(vRetEjec, DatosCombo(cboAreaEjec), "|") = 0 Then MsgBox ("Error")
    
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crPortrait
        .ReportTitle = "Horas trabajadas por �rea solicitante"
        .ReportComments = sTitulo(0) & vbCrLf & sTitulo(1) & vbCrLf & sTitulo(2)
    End With
        
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
 
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fdesde": crParamDef.AddCurrentValue (IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")))
            Case "@fhasta": crParamDef.AddCurrentValue (IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")))
            Case "@nivSoli": crParamDef.AddCurrentValue (vRetSoli(1))
            Case "@areSoli": crParamDef.AddCurrentValue (vRetSoli(2))
            Case "@nivEjec": crParamDef.AddCurrentValue (vRetEjec(1))
            Case "@areEjec": crParamDef.AddCurrentValue (vRetEjec(2))
            Case "@detalle": crParamDef.AddCurrentValue (CStr(xNivel))
        End Select
    Next
    ' F�rmulas del reporte
    With crReport
        .FormulaFields.GetItemByName("@@0_TITULO").Text = Chr(34) & sTitulo(0) & Chr(34)
        .FormulaFields.GetItemByName("@@1_TITULO").Text = Chr(34) & sTitulo(1) & Chr(34)
        .FormulaFields.GetItemByName("@@2_TITULO").Text = Chr(34) & sTitulo(2) & Chr(34)
    End With
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

'Private Sub ViejoReporte()
'    Dim cPathFileNameReport As String
'    Dim txtAux As String
'    Dim sTitulo As String
'    Dim xRet As Integer
'    Dim vRetSoli() As String
'    Dim vRetEjec() As String
'
'    With mdiPrincipal.CrystalReport1
'        If Not CamposObligatorios Then
'            Exit Sub
'        End If
'        If ParseString(vRetSoli, DatosCombo(cboAreaSoli), "|") = 0 Then MsgBox ("Error")
'        If ParseString(vRetEjec, DatosCombo(cboAreaEjec), "|") = 0 Then MsgBox ("Error")
'        '.ReportFileName = App.Path & "\" & "hstrasolic.rpt" ' del -002- a.
'        '{ add -002- a.
'        cPathFileNameReport = App.Path & "\" & "hstrasolic.rpt"
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        '}
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .RetrieveStoredProcParams
'        .StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
'        .StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
'        .StoredProcParam(2) = vRetSoli(1)
'        .StoredProcParam(3) = vRetSoli(2)
'        .StoredProcParam(4) = vRetEjec(1)
'        .StoredProcParam(5) = vRetEjec(2)
'        .StoredProcParam(6) = xNivel
'        .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'        .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'        .Formulas(2) = "@0_TITULO='" & "Area Solicitante: " & ClearNull(TextoCombo(cboAreaSoli)) & "'"
'        .Formulas(3) = "@1_TITULO='" & "Area Ejecutora: " & ClearNull(TextoCombo(cboAreaEjec)) & "'"
'        .Formulas(4) = "@2_TITULO='" & "Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy")) & "'"
'    '    .Connect = "DSN=" & mdiPrincipal.SybaseLogIn.DsnAplicacion & ";UID=" & mdiPrincipal.SybaseLogIn.UserAplicacion & ";PWD=" & mdiPrincipal.SybaseLogIn.PasswordAplicacion & mdiPrincipal.SybaseLogIn.ComponenteSI & ";SRVR=" & mdiPrincipal.SybaseLogIn.ServerSI & ";DB=" & mdiPrincipal.SybaseLogIn.BaseSI
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        .Formulas(2) = ""
'        .Formulas(3) = ""
'        .Formulas(4) = ""
'    End With
'   'Call cmdCancelar_Click
'End Sub

Private Sub OptNivel_Click(Index As Integer)
    xNivel = Index
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If txtDESDE = "" Then
        MsgBox ("Falta Fecha inicio informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA = "" Then
        MsgBox ("Falta Fecha fin informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox ("Fecha Hasta menor que Fecha Desde")
        CamposObligatorios = False
        Exit Function
    End If
End Function
