VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAgrupShell 
   Caption         =   "Mantenimiento de agrupamientos"
   ClientHeight    =   7200
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11310
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAgrupShell.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7200
   ScaleWidth      =   11310
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1155
      Left            =   75
      TabIndex        =   1
      Top             =   0
      Width           =   10155
      Begin VB.ComboBox cboNivel 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   13
         ToolTipText     =   "Visibilidad"
         Top             =   390
         Width           =   1155
      End
      Begin VB.CheckBox chkPropias 
         Caption         =   "S�lo propios"
         Height          =   195
         Left            =   3690
         TabIndex        =   9
         ToolTipText     =   "Muestra s�lo los agrupamientos dados de alta por el usuario"
         Top             =   818
         Value           =   1  'Checked
         Width           =   4995
      End
      Begin VB.ComboBox cboVigencia 
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   750
         Width           =   2295
      End
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "Consultar"
         Height          =   315
         Left            =   8940
         TabIndex        =   2
         Top             =   780
         Width           =   1065
      End
      Begin VB.ComboBox cboDire 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   390
         Width           =   8745
      End
      Begin VB.ComboBox cboSect 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   390
         Width           =   8745
      End
      Begin VB.ComboBox cboGrup 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   390
         Width           =   8745
      End
      Begin VB.ComboBox cboGere 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   390
         Width           =   8745
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   75
         TabIndex        =   16
         Top             =   150
         Width           =   675
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "�rea"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   1320
         TabIndex        =   15
         Top             =   150
         Width           =   345
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Vigencia"
         ForeColor       =   &H00FF0000&
         Height          =   210
         Left            =   450
         TabIndex        =   8
         Top             =   810
         Width           =   720
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5955
      Left            =   60
      TabIndex        =   20
      Top             =   1200
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   10504
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Agrupamientos"
      TabPicture(0)   =   "frmAgrupShell.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lswDatos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Referencia"
      TabPicture(1)   =   "frmAgrupShell.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "imgReferencias(0)"
      Tab(1).Control(1)=   "lblReferencias(0)"
      Tab(1).Control(2)=   "imgReferencias(1)"
      Tab(1).Control(3)=   "lblReferencias(1)"
      Tab(1).Control(4)=   "imgReferencias(2)"
      Tab(1).Control(5)=   "lblReferencias(2)"
      Tab(1).Control(6)=   "imgReferencias(4)"
      Tab(1).Control(7)=   "lblReferencias(4)"
      Tab(1).ControlCount=   8
      Begin MSComctlLib.ListView lswDatos 
         Height          =   5175
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   9315
         _ExtentX        =   16431
         _ExtentY        =   9128
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "imgAgrupamientos"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   0
         Left            =   -74760
         Picture         =   "frmAgrupShell.frx":05C2
         Top             =   540
         Width           =   240
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento cerrado (no admite el agregado de peticiones)"
         Height          =   195
         Index           =   0
         Left            =   -74340
         TabIndex        =   25
         Top             =   570
         Width           =   4365
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   1
         Left            =   -74760
         Picture         =   "frmAgrupShell.frx":0B4C
         Top             =   870
         Width           =   240
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento restringido (solo para uso del usuario que lo di� de alta)"
         Height          =   195
         Index           =   1
         Left            =   -74340
         TabIndex        =   24
         Top             =   870
         Width           =   5040
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   2
         Left            =   -74760
         Picture         =   "frmAgrupShell.frx":10D6
         Top             =   1440
         Width           =   240
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento (sin hijos)"
         Height          =   195
         Index           =   2
         Left            =   -74340
         TabIndex        =   23
         Top             =   1470
         Width           =   1740
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   4
         Left            =   -74760
         Picture         =   "frmAgrupShell.frx":1660
         Top             =   1110
         Width           =   240
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento (con hijos)"
         Height          =   195
         Index           =   4
         Left            =   -74340
         TabIndex        =   22
         Top             =   1170
         Width           =   1800
      End
   End
   Begin MSComctlLib.ImageList imgAgrupamientos 
      Left            =   9540
      Top             =   1380
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   18
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":1BEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":2184
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":271E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":2CB8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":3252
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":37EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":3D86
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":4320
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":48BA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":4E54
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":53EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":5988
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":5F22
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":64BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":6A56
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":6FF0
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":758A
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupShell.frx":7B24
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7185
      Left            =   10260
      TabIndex        =   3
      Top             =   0
      Width           =   1005
      Begin VB.CommandButton cmdImportar 
         Caption         =   "Importar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   60
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Accede al agrupamiento seleccionado"
         Top             =   540
         Width           =   885
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Listado"
         Height          =   375
         Left            =   60
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo agrupamiento"
         Top             =   2400
         Width           =   885
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Nuevo"
         Height          =   375
         Left            =   60
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo agrupamiento"
         Top             =   1560
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Salir de esta pantalla"
         Top             =   6720
         Width           =   885
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "Acceder"
         Height          =   375
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Accede al agrupamiento seleccionado"
         Top             =   1200
         Width           =   885
      End
      Begin VB.CommandButton cmdMigrar 
         Caption         =   "Migrar"
         Height          =   375
         Left            =   60
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Accede al agrupamiento seleccionado"
         Top             =   160
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3210
      Left            =   180
      TabIndex        =   0
      Top             =   1260
      Width           =   3405
      _ExtentX        =   6006
      _ExtentY        =   5662
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAgrupShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Const colTitulo = 0
Const colCodigoRecurso = 1
Const colVisibilidad = 2
Const colAdmpet = 3
Const colActualiza = 4
Const colESTADO = 5
Const colNroInterno = 6
Const colNroPadre = 7

Const IMAGEN_AGRUP_PADRE_ABIERTO = 3    '10
Const IMAGEN_AGRUP_PADRE_CERRADO = 11
Const IMAGEN_AGRUP_NORMAL = 8
Const IMAGEN_AGRUP_PRIVADO = 5  '7

Dim sOpcionSeleccionada As String
Dim flgNumero As Boolean
Dim flgCarga As Boolean
Dim sArea As String

Dim xPerfil As String
Dim xNivel As String
Dim xArea As String
Dim xDir As String, xGer As String, xSec As String, xGru As String
Dim xxx As String

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    Me.Height = 7710
    Me.Width = 11430
    flgNumero = False
    xDir = glLOGIN_Direccion
    xGer = glLOGIN_Gerencia
    xSec = glLOGIN_Sector
    xGru = glLOGIN_Grupo
    flgCarga = True
    If glUsrPerfilActual = "SADM" Then
        xGer = ""
        xSec = ""
        xGru = ""
    End If
    
    Call InicializarCombos
    Call InicializarPantalla
    'Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)      ' add -003- a.
    flgCarga = False
End Sub

Private Sub cboNivel_Click()
    Dim xNv As String
    If Not LockProceso(True) Then
        Exit Sub
    End If
    xNv = CodigoCombo(cboNivel, True)
    Call setHabilCtrl(cboDire, "DIS")
    Call setHabilCtrl(cboGere, "DIS")
    Call setHabilCtrl(cboSect, "DIS")
    Call setHabilCtrl(cboGrup, "DIS")
    cboDire.ListIndex = -1
    cboGere.ListIndex = -1
    cboSect.ListIndex = -1
    cboGrup.ListIndex = -1
    Select Case xNv
        Case "USR"
        Case "DIR"
            cboDire.visible = True
            cboGere.visible = False
            cboSect.visible = False
            cboGrup.visible = False
            Call setHabilCtrl(cboDire, "NOR")
            Call SetCombo(cboDire, xDir, True)
            If cboDire.ListCount > 0 Then
                If cboDire.ListIndex = -1 Then
                    cboDire.ListIndex = 0
                End If
            End If
        Case "GER"
            cboDire.visible = False
            cboGere.visible = True
            cboSect.visible = False
            cboGrup.visible = False
            Call setHabilCtrl(cboGere, "NOR")
            Call SetCombo(cboGere, xDir & xGer, True)
            If cboGere.ListCount > 0 Then
                If cboGere.ListIndex = -1 Then
                    cboGere.ListIndex = 0
                End If
            End If
        Case "SEC"
            cboDire.visible = False
            cboGere.visible = False
            cboSect.visible = True
            cboGrup.visible = False
            Call setHabilCtrl(cboSect, "NOR")
            Call SetCombo(cboSect, xDir & xGer & xSec, True)
            If cboSect.ListCount > 0 Then
                If cboSect.ListIndex = -1 Then
                    cboSect.ListIndex = 0
                End If
            End If
        Case "GRU"
            cboDire.visible = False
            cboGere.visible = False
            cboSect.visible = False
            cboGrup.visible = True
            Call setHabilCtrl(cboGrup, "NOR")
            Call SetCombo(cboGrup, xDir & xGer & xSec & xGru, True)
            If cboGrup.ListCount > 0 Then
                If cboGrup.ListIndex = -1 Then
                    cboGrup.ListIndex = 0
                End If
            End If
        Case "PUB"
    End Select
    If Not flgCarga Then Call CargarGrid
    Call LockProceso(False)
End Sub

Private Sub cmdFiltro_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    flgNumero = False
    glNumeroAgrup = ""
    CargarGrid
End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glNumeroAgrup = ""
    glCodigoRecurso = glLOGIN_ID_REEMPLAZO
    glModoAgrup = "ALTA"
    frmAgrupCarga.Show vbModal
    DoEvents
    Call LockProceso(False)
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla
    End If
End Sub

Private Sub cmdVisualizar_Click()
    If glNumeroAgrup <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoAgrup = "VIEW"
        On Error Resume Next
        frmAgrupCarga.Show vbModal
        DoEvents
        On Error GoTo 0
        Call LockProceso(False)
        If Me.Tag = "REFRESH" Then
            If frmAgrupCarga.bEdicion Then
                Call InicializarPantalla
            End If
        End If
    End If
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    'Unload Me              ' del
    Me.Hide
    Call LockProceso(False)
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        Me.Height = 7710
        Me.Width = 11430
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    glNumeroAgrup = ""
    modUser32.DetenerScroll grdDatos      ' add -003- a.
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    Call HabilitarBotones(0)
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            cmdAgregar.Enabled = True
            grdDatos.Enabled = True
            sOpcionSeleccionada = ""
            'If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
            If InPerfil("SADM") Or InPerfil("ADMI") Then
                cmdMigrar.Enabled = True
            Else
                cmdMigrar.Enabled = False
            End If
            cmdCerrar.visible = True
            cmdVisualizar.Enabled = False
       
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion(False)
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Caption = "Cancelar"
            cmdAgregar.visible = True
            cmdCerrar.visible = False
            cmdVisualizar.Enabled = False
    End Select
    Call LockProceso(False)
End Sub

Sub InicializarCombos()
    Dim i As Integer
    Dim xNivel As String, xAreaCod As String, xAreaCodX As String, xAreaNom As String
    Dim xDire As String, xGere As String, xSect As String, xGrup As String
    
    With cboNivel
        .Clear
        .AddItem getDescNvAgrup("USR") & ESPACIOS & "||USR"
        .AddItem getDescNvAgrup("DIR") & ESPACIOS & "||DIR"
        .AddItem getDescNvAgrup("GER") & ESPACIOS & "||GER"
        .AddItem getDescNvAgrup("SEC") & ESPACIOS & "||SEC"
        .AddItem getDescNvAgrup("GRU") & ESPACIOS & "||GRU"
        .AddItem getDescNvAgrup("PUB") & ESPACIOS & "||PUB"
        .ListIndex = 0
    End With
    
    cboVigencia.Clear
    cboVigencia.AddItem "Vigentes" & ESPACIOS & "||S"
    cboVigencia.AddItem "No vigentes" & ESPACIOS & "||N"
    'cboVigencia.AddItem "<todas>" & Space(40) & "||T"
    cboVigencia.AddItem "* Todas" & ESPACIOS & "||T"
    cboVigencia.ListIndex = 0
    Dim iNx As Integer
 
    chkPropias.Caption = "S�lo dados de alta por " & glLOGIN_ID_REEMPLAZO

    Call Status("Cargando �reas...")
    If sp_rptEstructura(IIf(xDir <> "", xDir, "NULL"), IIf(xGer <> "", xGer, "NULL"), IIf(xSec <> "", xSec, "NULL"), IIf(xGru <> "", xGru, "NULL"), "2") Then
        Do While Not aplRST.EOF
            iNx = 0
            xAreaCod = ""
            xAreaCodX = ""
            xAreaNom = ""
            If ClearNull(aplRST!cod_direccion) <> "" Then
                xAreaCodX = ClearNull(aplRST!cod_direccion)
                xAreaCod = ClearNull(aplRST!cod_direccion)
                xAreaNom = xAreaNom & ClearNull(aplRST!nom_direccion)
                iNx = iNx + 1
            End If
            If ClearNull(aplRST!cod_gerencia) <> "" Then
                xAreaCodX = xAreaCodX & ClearNull(aplRST!cod_gerencia)
                xAreaCod = ClearNull(aplRST!cod_gerencia)
                xAreaNom = xAreaNom & " � " & ClearNull(aplRST!nom_gerencia)
                iNx = iNx + 1
            End If
            If ClearNull(aplRST!cod_sector) <> "" Then
                xAreaCod = ClearNull(aplRST!cod_sector)
                xAreaCodX = xAreaCodX & ClearNull(aplRST!cod_sector)
                xAreaNom = xAreaNom & " � " & ClearNull(aplRST!nom_sector)
                iNx = iNx + 1
            End If
            If ClearNull(aplRST!cod_grupo) <> "" Then
                xAreaCod = ClearNull(aplRST!cod_grupo)
                xAreaCodX = xAreaCodX & ClearNull(aplRST!cod_grupo)
                xAreaNom = xAreaNom & " � " & ClearNull(aplRST!nom_grupo)
                iNx = iNx + 1
            End If
            
            Select Case iNx
                Case 1
                    cboDire.AddItem xAreaNom & Space(300) & "||" & xAreaCodX & "|DIR|!" & xAreaCod & "!"
                    xDire = xDire & "!" & xAreaCod
                Case 2
                    cboGere.AddItem xAreaNom & Space(300) & "||" & xAreaCodX & "|GER|!" & xAreaCod & "!"
                    xGere = xGere & "!" & xAreaCod
                Case 3
                    cboSect.AddItem xAreaNom & Space(300) & "||" & xAreaCodX & "|SEC|!" & xAreaCod & "!"
                    xSect = xSect & "!" & xAreaCod
                Case 4
                    cboGrup.AddItem xAreaNom & Space(300) & "||" & xAreaCodX & "|GRU|!" & xAreaCod & "!"
                    xGrup = xGrup & "!" & xAreaCod
            End Select
            aplRST.MoveNext
        Loop
        aplRST.Close
        If cboDire.ListCount > 1 And Len(xDire) < 250 Then
            cboDire.AddItem "<Todas>" & Space(300) & "||ALL" & "|DIR|" & xDire & "!", 0
        End If
        If cboGere.ListCount > 1 And Len(xGere) < 250 Then
            cboGere.AddItem "<Todas>" & Space(300) & "||ALL" & "|GER|" & xGere & "!", 0
        End If
        If cboSect.ListCount > 1 And Len(xSect) < 250 Then
            cboSect.AddItem "<Todos>" & Space(300) & "||ALL" & "|SEC|" & xSect & "!", 0
        End If
        If cboGrup.ListCount > 1 And Len(xGrup) < 250 Then
            cboGrup.AddItem "<Todos>" & Space(300) & "||ALL" & "|GRU|" & xGrup & "!", 0
        End If
    End If
    Call Status("Listo.")
End Sub

Sub CargarGrid()
    Dim xNivel As String, xArea As String
    Dim vRetorno() As String
    'glNumeroAgrup = ""
    'glCodigoRecurso = ""

    Dim xNv As String
    Dim itmX As ListItem

    xNv = CodigoCombo(cboNivel, True)
    Select Case xNv
        Case "USR"
            xArea = glLOGIN_ID_REEMPLAZO
            xNivel = "USR"
        Case "DIR"
            If ParseString(vRetorno, DatosCombo(cboDire), "|") > 0 Then
                xNivel = vRetorno(2)
                xArea = vRetorno(3)
            End If
        Case "GER"
            If ParseString(vRetorno, DatosCombo(cboGere), "|") > 0 Then
                xNivel = vRetorno(2)
                xArea = vRetorno(3)
            End If
        Case "SEC"
            If ParseString(vRetorno, DatosCombo(cboSect), "|") > 0 Then
                xNivel = vRetorno(2)
                xArea = vRetorno(3)
            End If
        Case "GRU"
            If ParseString(vRetorno, DatosCombo(cboGrup), "|") > 0 Then
                xNivel = vRetorno(2)
                xArea = vRetorno(3)
            End If
        Case "PUB"
            xArea = ""
            xNivel = "PUB"
    End Select
    xArea = ReplaceString(xArea, Asc("!"), "|")
        
    DoEvents
    cmdVisualizar.Enabled = False
    cmdFiltro.Enabled = False
    With grdDatos
        .Clear
        DoEvents
        .HighLight = flexHighlightNever
        .RowHeightMin = 265
        .Rows = 1
        '.TextMatrix(0, colCodigoRecurso) = "Usuario Alta"
        .TextMatrix(0, colCodigoRecurso) = "Propietario": .ColWidth(colCodigoRecurso) = 2000
        .TextMatrix(0, colTitulo) = "Nombre": .ColWidth(colTitulo) = 4800: .ColAlignment(colTitulo) = flexAlignLeftCenter
        .TextMatrix(0, colESTADO) = "Vig.": .ColWidth(colESTADO) = 450: .ColAlignment(colESTADO) = flexAlignCenterCenter
        .TextMatrix(0, colVisibilidad) = "Niv.Visib.": .ColWidth(colVisibilidad) = 1000
        .TextMatrix(0, colAdmpet) = "Admite.Pet.": .ColWidth(colAdmpet) = 800: .ColAlignment(colAdmpet) = flexAlignCenterCenter
        .TextMatrix(0, colActualiza) = "Niv.Inc.Petic.": .ColWidth(colActualiza) = 1000
        .TextMatrix(0, colNroInterno) = "Nro.Int.": .ColWidth(colNroInterno) = 800
        .ColWidth(colNroPadre) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    
    With lswDatos
        ' Agrega objetos ColumnHeaders
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "T�tulo", 4000
        .ColumnHeaders.Add , , "Propietario", 2000
        .ColumnHeaders.Add , , "Vigente", 800, lvwColumnCenter
        .ColumnHeaders.Add , , "Visibilidad", 1200, lvwColumnCenter
        .ColumnHeaders.Add , , "Admite", 800, lvwColumnCenter
        .ColumnHeaders.Add , , "Nro. interno", 1000
        .ColumnHeaders.Add , , "Padre", 1000
        .ColumnHeaders.Add , , "Actualiza", 1200
        ' Asigna la propiedad View el valor Report.
        .View = lvwReport
        .ListItems.Clear
    End With
    
    Call Puntero(True)
    Call Status("Cargando agrupamientos...")
    If glUsrPerfilActual = "SADM" Then
        If Not sp_GetAgrupNivelRecur("SADM", xNivel, xArea, CodigoCombo(cboVigencia, True)) Then
           GoTo finx
        End If
    Else
        If Not sp_GetAgrupNivelRecur("VIEW", xNivel, xArea, CodigoCombo(cboVigencia, True)) Then
           GoTo finx
        End If
    End If
    grdDatos.visible = False
    Do While Not aplRST.EOF
        If chkPropias.Value = 0 Or (chkPropias.Value = 1 And ClearNull(aplRST!cod_usualta) = glLOGIN_ID_REEMPLAZO) Then
            With grdDatos
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!agr_nrointerno)
                .TextMatrix(.Rows - 1, colNroPadre) = ClearNull(aplRST!agr_nropadre)
                .TextMatrix(.Rows - 1, colTitulo) = Space(aplRST!nivel * 3) & ClearNull(aplRST!agr_titulo)
                .TextMatrix(.Rows - 1, colCodigoRecurso) = ClearNull(aplRST!nom_usualta)
                .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!agr_vigente)
                .TextMatrix(.Rows - 1, colVisibilidad) = getDescNvAgrup(ClearNull(aplRST!agr_visibilidad))
                .TextMatrix(.Rows - 1, colAdmpet) = ClearNull(aplRST!agr_admpet)
                .TextMatrix(.Rows - 1, colActualiza) = getDescNvAgrup(ClearNull(aplRST!agr_actualiza))
            End With
            
            With lswDatos
                ' Privados
                If ClearNull(aplRST!agr_visibilidad) = "USR" Then
                    Set itmX = .ListItems.Add(, , Space(aplRST!nivel * 3) & ClearNull(aplRST!agr_titulo), , IMAGEN_AGRUP_PRIVADO)
                Else
                    If CStr(aplRST!agr_admpet) = "S" Then
                        'Set itmX = .ListItems.Add(, , Space(aplRST!nivel * 3) & ClearNull(aplRST!agr_titulo), , IMAGEN_AGRUP_NORMAL)
                        If ClearNull(aplRST!agr_nropadre) = "0" Then
                            Set itmX = .ListItems.Add(, , Space(aplRST!nivel * 3) & ClearNull(aplRST!agr_titulo), , IMAGEN_AGRUP_PADRE_ABIERTO)
                            itmX.Bold = True
                        Else
                            Set itmX = .ListItems.Add(, , Space(aplRST!nivel * 3) & ClearNull(aplRST!agr_titulo), , IMAGEN_AGRUP_NORMAL)
                        End If
                    Else
                        Set itmX = .ListItems.Add(, , Space(aplRST!nivel * 3) & ClearNull(aplRST!agr_titulo), , IMAGEN_AGRUP_PADRE_CERRADO)
                        itmX.Bold = True
                    End If
                End If
                itmX.SubItems(1) = IIf(IsNull(aplRST!nom_usualta), "-", ClearNull(aplRST!nom_usualta))
                itmX.SubItems(2) = CStr(aplRST!agr_vigente)
                itmX.SubItems(3) = getDescNvAgrup(ClearNull(aplRST!agr_visibilidad))
                itmX.SubItems(4) = CStr(aplRST!agr_admpet)
                itmX.SubItems(5) = CStr(aplRST!agr_nrointerno)
                itmX.SubItems(6) = CStr(aplRST!agr_nropadre)
                itmX.SubItems(7) = getDescNvAgrup(ClearNull(aplRST!agr_actualiza))
            End With
        End If
        aplRST.MoveNext
    Loop
finx:
    cmdFiltro.Enabled = True
    If glNumeroAgrup <> "" Then
        If grdDatos.Rows > 1 Then
            Call GridSeek(Me, grdDatos, colNroInterno, glNumeroAgrup, True)
            Call MostrarSeleccion(False)
        End If
    Else
        If grdDatos.Rows > 1 Then
            Call MostrarSeleccion(False)
        End If
    End If
    Call Puntero(False)
    Call Status("Listo.")
    Call LockProceso(False)
End Sub

Public Sub MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    cmdVisualizar.Enabled = False
    sArea = ""
    With grdDatos
        If .RowSel > 0 Then
'            .Row = .RowSel
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                glNumeroAgrup = .TextMatrix(.RowSel, colNroInterno)
'                sArea = .TextMatrix(.RowSel, colCodHijo)
                cmdVisualizar.Enabled = True
            End If
        End If
    End With
End Sub

Private Sub MostrarSeleccionItem(ByVal Item As MSComctlLib.ListItem)
    glNumeroAgrup = Item.ListSubItems(5)       ' .TextMatrix(.RowSel, colNroInterno)
    Call setHabilCtrl(cmdVisualizar, NORMAL)
End Sub

Private Sub grdDatos_Click()
    'Debug.Print "grdDatos_Click"
    Call MostrarSeleccion
    DoEvents
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Private Sub grdDatos_DblClick()
    'Debug.Print "grdDatos_DblClick"
    Call MostrarSeleccion
    DoEvents
    If glNumeroAgrup <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoAgrup = "VIEW"
        On Error Resume Next
        frmAgrupCarga.Show vbModal
        DoEvents
        If Me.Tag = "REFRESH" Then
             InicializarPantalla
        End If
    End If
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
         Call MenuGridFind(grdDatos)
    End If
End Sub

Private Sub cmdMigrar_Click()
    Dim sLegajo         As String       ' Destinatario
    Dim agr_nrointerno  As Long
    Dim v_agr_nrointerno() As Long
    Dim agr_nropadre    As Long
    Dim agr_titulo      As String
    Dim agr_vigente     As String
    Dim cod_direccion   As String
    Dim cod_gerencia    As String
    Dim cod_sector      As String
    Dim cod_grupo       As String
    Dim agr_visibilidad As String
    Dim agr_actualiza   As String
    Dim agr_admpet      As String
    Dim i As Long
    
    With grdDatos
        sLegajo = InputBox("Ingrese el legajo del usuario destinatario", "Destinatario", "Legajo")
        If ClearNull(sLegajo) <> "" Then
            If sp_GetRecurso(sLegajo, "R") Then
                cod_direccion = ClearNull(aplRST.Fields!cod_direccion)
                cod_gerencia = ClearNull(aplRST.Fields!cod_gerencia)
                cod_sector = ClearNull(aplRST.Fields!cod_sector)
                cod_grupo = ClearNull(aplRST.Fields!cod_grupo)
                If MsgBox("Confirma la migraci�n del agrupamiento al usuario " & ClearNull(aplRST.Fields!cod_recurso) & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")?", vbQuestion + vbOKCancel, "Inicio de migraci�n") = vbOK Then
                    Call Puntero(True)
                    Call Status("Procesando...")
                    If sp_GetAgrup(.TextMatrix(.RowSel, colNroInterno)) Then
                        agr_nrointerno = ClearNull(aplRST.Fields!agr_nrointerno)
                        agr_nropadre = ClearNull(aplRST.Fields!agr_nropadre)
                        agr_titulo = ClearNull(aplRST.Fields!agr_titulo)
                        agr_vigente = ClearNull(aplRST.Fields!agr_vigente)
                        agr_visibilidad = ClearNull(aplRST.Fields!agr_visibilidad)
                        agr_actualiza = ClearNull(aplRST.Fields!agr_actualiza)
                        agr_admpet = ClearNull(aplRST.Fields!agr_admpet)
                        Call sp_UpdAgrup(agr_nrointerno, agr_nropadre, agr_titulo, agr_vigente, cod_direccion, cod_gerencia, cod_sector, cod_grupo, sLegajo, agr_visibilidad, agr_actualiza, agr_admpet)
                        If sp_GetAgrupAgrup(agr_nrointerno) Then
                            i = 0
                            Do While Not aplRST.EOF
                                ReDim Preserve v_agr_nrointerno(i)
                                v_agr_nrointerno(i) = aplRST.Fields!agr_nrointerno
                                aplRST.MoveNext
                                i = i + 1
                                DoEvents
                            Loop
                        End If
                        If i > 0 Then
                            For i = 0 To UBound(v_agr_nrointerno)
                                If sp_GetAgrup(v_agr_nrointerno(i)) Then
                                    agr_nropadre = ClearNull(aplRST.Fields!agr_nropadre)
                                    agr_titulo = ClearNull(aplRST.Fields!agr_titulo)
                                    agr_vigente = ClearNull(aplRST.Fields!agr_vigente)
                                    agr_visibilidad = ClearNull(aplRST.Fields!agr_visibilidad)
                                    agr_actualiza = ClearNull(aplRST.Fields!agr_actualiza)
                                    agr_admpet = ClearNull(aplRST.Fields!agr_admpet)
                                    Call sp_UpdAgrup(v_agr_nrointerno(i), agr_nropadre, agr_titulo, agr_vigente, cod_direccion, cod_gerencia, cod_sector, cod_grupo, sLegajo, agr_visibilidad, agr_actualiza, agr_admpet)
                                End If
                            Next i
                        End If
                    End If
                    MsgBox "Proceso realizado."
                End If
            End If
            Call Status("Listo.")
            Call Puntero(False)
        End If
    End With
End Sub

Private Sub cmdImprimir_Click()
    ' Listado
End Sub

Private Sub lswDatos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Call MostrarSeleccionItem(Item)
End Sub

Private Sub cmdImportar_Click()
    ' Importar listado de peticiones
End Sub

