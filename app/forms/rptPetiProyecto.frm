VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptPetiProyecto 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5490
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   8715
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5490
   ScaleWidth      =   8715
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraOpciones 
      Height          =   945
      Left            =   0
      TabIndex        =   21
      Top             =   4550
      Width           =   8715
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   7560
         TabIndex        =   15
         Top             =   360
         Width           =   975
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6480
         TabIndex        =   14
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame fraRpt 
      Height          =   3765
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   8715
      Begin VB.CommandButton cmdEraseProyectos 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         Picture         =   "rptPetiProyecto.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Quitar el filtro actual de proyecto IDM"
         Top             =   240
         Width           =   390
      End
      Begin VB.Frame fraNivel 
         Caption         =   " Nivel de agrupamiento "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2325
         Left            =   5580
         TabIndex        =   7
         Top             =   1200
         Width           =   2505
         Begin VB.OptionButton OptNivel 
            Caption         =   "Petici�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   24
            Top             =   1620
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   240
            TabIndex        =   11
            Top             =   1110
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   3
            Left            =   240
            TabIndex        =   10
            Top             =   840
            Width           =   1245
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   4
            Left            =   240
            TabIndex        =   9
            Top             =   585
            Width           =   1125
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   8
            Top             =   300
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Recurso"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   1
            Left            =   240
            TabIndex        =   12
            Top             =   1365
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sin agrupar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   240
            TabIndex        =   13
            Top             =   1900
            Width           =   1365
         End
      End
      Begin AT_MaskText.MaskText txtDESDE 
         Height          =   315
         Left            =   960
         TabIndex        =   5
         Top             =   960
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTA 
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Top             =   1320
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtCodtarea 
         Height          =   315
         Left            =   4620
         TabIndex        =   16
         Top             =   1800
         Visible         =   0   'False
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPetnroasig 
         Height          =   315
         Left            =   4620
         TabIndex        =   17
         Top             =   1440
         Visible         =   0   'False
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   315
         Left            =   1920
         TabIndex        =   3
         Top             =   240
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   315
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   315
         Left            =   960
         TabIndex        =   1
         Top             =   240
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   556
         MaxLength       =   11
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   11
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjNom 
         Height          =   315
         Left            =   960
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   600
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   556
         MaxLength       =   100
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.CommandButton cmdMasProyectos 
         Height          =   315
         Left            =   2640
         Picture         =   "rptPetiProyecto.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto de la lista"
         Top             =   240
         Width           =   390
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   20
         Top             =   1027
         Width           =   480
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   19
         Top             =   1387
         Width           =   450
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Proyecto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   18
         Top             =   307
         Width           =   675
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "10. HORAS TRABAJADAS POR POR PROYECTO / PETICI�N"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   120
      TabIndex        =   22
      Top             =   120
      Width           =   4500
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptPetiProyecto.frx":0B14
      Top             =   0
      Width           =   8700
   End
End
Attribute VB_Name = "rptPetiProyecto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim flgEnCarga As Boolean
Dim xNivel As Long

Private Sub cmdEraseProyectos_Click()
    txtPrjId = ""
    txtPrjSubId = ""
    txtPrjSubsId = ""
    txtPrjNom = ""
End Sub

Private Sub cmdMasProyectos_Click()
    Load frmProyectosIDMSeleccion
    frmProyectosIDMSeleccion.Show 1
    With frmProyectosIDMSeleccion
        txtPrjId.Text = .lProjId
        txtPrjSubId.Text = .lProjSubId
        txtPrjSubsId.Text = .lProjSubSId
        txtPrjNom.Text = Trim(.cProjNom)
    End With
End Sub

Private Sub Form_Load()
    flgEnCarga = False
    Call setHabilCtrl(txtPrjNom, "DIS")
    
    'Call InicializarCombos
    txtDESDE.Text = Format(DateAdd("m", -1, date), "dd/mm/yyyy")
    txtHASTA.Text = Format(date, "dd/mm/yyyy")
    'CrtPrn(1).Value = True
    'CrtPrn(0).Value = True
    OptNivel(4).Value = True
    Call LockProceso(False)
End Sub

'Private Sub cboTarea_Click()
'    If flgEnCarga = True Then
'        Exit Sub
'    End If
'    flgEnCarga = True
'    txtCodtarea = ""
'    txtCodtarea = CodigoCombo(cboTarea)
'    cboTarea.ToolTipText = "Descripci�n: " & TextoCombo(cboTarea, CodigoCombo(cboTarea, False), False)  ' add -xxx-
'    'cboTarea.ToolTipText = TextoCombo(cboTarea, CodigoCombo(cboTarea, False), False)
'    txtPetnroasig = ""
'    cboPeticion.ListIndex = -1
'    flgEnCarga = False
'End Sub

'Private Sub cboPeticion_Click()
'    If flgEnCarga = True Then
'        Exit Sub
'    End If
'    flgEnCarga = True
'    txtPetnroasig = ""
'    txtPetnroasig = CodigoCombo(cboPeticion)
'    cboPeticion.ToolTipText = "Descripci�n: " & TextoCombo(cboPeticion, CodigoCombo(cboPeticion, True), True)     ' add -xxx-
'    txtCodtarea = ""
'    cboTarea.ListIndex = -1
'    flgEnCarga = False
'    ' DESARROLLO
'    Debug.Print CodigoCombo(cboPeticion, True)
'End Sub

'Private Sub cmdMasPeticiones_Click()
'    Dim vRetorno() As String
'    Dim nPos As Integer
'    flgEnCarga = True
'    If Not LockProceso(True) Then
'        Exit Sub
'    End If
'    glAuxRetorno = ""
'    glAuxEstado = "ANULA|CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|RECHAZ|RECHTE|CANCEL|SUSPEN|REVISA|TERMIN"
'    frmSelPet.Show 1
'    DoEvents
'    If glAuxRetorno <> "" Then
'        If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
'            txtCodtarea = ""
'            cboTarea.ListIndex = -1
'            nPos = PosicionCombo(cboPeticion, vRetorno(3), True)
'            If nPos = -1 Then
'                cboPeticion.AddItem glAuxRetorno
'                nPos = PosicionCombo(cboPeticion, vRetorno(3), True)
'                If nPos = -1 Then
'                    MsgBox ("Error al incorporar petici�n")
'                    cboPeticion.ListIndex = 0
'                Else
'                    cboPeticion.ListIndex = nPos
'                End If
'            Else
'                cboPeticion.ListIndex = nPos
'            End If
'            txtPetnroasig = ""
'            txtPetnroasig = CodigoCombo(cboPeticion)
'        End If
'        cboPeticion.Refresh
'        cboPeticion.SetFocus
'    End If
'    flgEnCarga = False
'    Call LockProceso(False)
'End Sub

Private Sub OptNivel_Click(Index As Integer)
    xNivel = Index
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
'    If cboTarea.ListIndex < 0 And cboPeticion.ListIndex < 0 Then
'        MsgBox ("Falta seleccionar Petici�n o Tarea")
'        CamposObligatorios = False
'        Exit Function
'    End If
'    If cboTarea.ListIndex >= 0 And cboPeticion.ListIndex >= 0 Then
'        MsgBox ("No puede asignar simult�neamente Petici�n y Tarea")
'        CamposObligatorios = False
'        Exit Function
'    End If
    If txtDESDE = "" Then
        MsgBox ("Falta Fecha inicio informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA = "" Then
        MsgBox ("Falta Fecha fin informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox ("Fecha Hasta menor que Fecha Desde")
        CamposObligatorios = False
        Exit Function
    End If
End Function

'Sub InicializarCombos()
'    Dim vExtendido() As String
'    Dim sValExtendido As String
'
'    flgEnCarga = True
'    Call Puntero(True)
'
'    'Call Status("Cargando Peticiones")     ' del -003- a.
'    Call Status("Inicializando...")         ' add -003- a.
'    cboPeticion.Clear
'    If ClearNull(glLOGIN_Grupo) <> "" Then
'        If sp_GetPeticionGrupoXt(0, Null, ClearNull(glLOGIN_Grupo), "EVALUA|ESTIMA|PLANIF|PLANOK|EJECUC") Then
'            Do While Not aplRST.EOF
'                cboPeticion.AddItem padRight(ClearNull(aplRST!pet_nroasignado), 9) & " : " & Trim(aplRST!Titulo) & Space(150) & " || " & Trim(aplRST!pet_nrointerno) & " |N"
'                aplRST.MoveNext
'            Loop
'            aplRST.Close
'        End If
'    ElseIf ClearNull(glLOGIN_Sector) <> "" Then
'        If sp_GetPeticionSectorXt(0, ClearNull(glLOGIN_Sector), "EVALUA|ESTIMA|PLANIF|PLANOK|EJECUC") Then
'            Do While Not aplRST.EOF
'                cboPeticion.AddItem padRight(ClearNull(aplRST!pet_nroasignado), 9) & " : " & Trim(aplRST!Titulo) & Space(150) & " || " & Trim(aplRST!pet_nrointerno) & " |N"
'                aplRST.MoveNext
'            Loop
'            aplRST.Close
'        End If
'    End If
'
'    cboPeticion.ListIndex = -1
'
'    'Call Status("Cargando Tareas")     ' del -003- a.
'    cboTarea.Clear
'    If sp_GetTarea(Null) Then
'        Do While Not aplRST.EOF
'            cboTarea.AddItem padLeft(aplRST!cod_tarea, 9) & " : " & Trim(aplRST!nom_tarea) & Space(150) & " ||N"
'            aplRST.MoveNext
'        Loop
'        aplRST.Close
'    End If
'    cboTarea.ListIndex = -1
'    Call Status("Listo.")
'    flgEnCarga = False
'    Call Puntero(False)
'End Sub

'Private Sub ViejoReporte()
'    Dim txtAux As String
'    Dim sTitulo As String
'    Dim xRet As Integer
'    Dim vRetorno() As String
'    Dim cPathFileNameReport As String
'
'    If Not CamposObligatorios Then
'        Exit Sub
'    End If
'    cPathFileNameReport = App.Path & "\" & IIf(xNivel = 6, "hstrapetproyp.rpt", "hstrapetproy.rpt")
'
'    With mdiPrincipal.CrystalReport1
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .RetrieveStoredProcParams
'
'        .StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
'        .StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
'        .StoredProcParam(2) = Val(txtPrjId)
'        .StoredProcParam(3) = Val(txtPrjSubId)
'        .StoredProcParam(4) = Val(txtPrjSubsId)
'        .StoredProcParam(5) = xNivel
'        sTitulo = "Peticiones del proyecto: " & ClearNull(txtPrjId) & "." & ClearNull(txtPrjSubId) & "." & ClearNull(txtPrjSubsId) & "    Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy"))
'        .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'        .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'        .Formulas(2) = "@0_TITULO='" & ReplaceApostrofo(sTitulo) & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'    End With
'End Sub

Private Sub cmdOK_Click()
    If ControlPedido Then
        If glCrystalNewVersion Then
            Call NuevoReporte
        Else
            'Call ViejoReporte
        End If
    End If
End Sub

Private Function ControlPedido() As Boolean
    ControlPedido = True
    If ClearNull(txtPrjId) = "" Then
        MsgBox "Debe ingresar o seleccionar un proyecto existente.", vbExclamation + vbOKOnly
        ControlPedido = False
        txtPrjId.SetFocus
    End If
End Function

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim vRetorno() As String
    Dim sReportName As String
    Dim txtAux As String
    Dim txtDetalle As String

    Call Puntero(True)
    sReportName = IIf(xNivel = 6, "hstrapetproyp6.rpt", "hstrapetproy6.rpt")
    txtAux = IIf(txtPrjId = "", 0, txtPrjId) & "." & IIf(txtPrjSubId = "", 0, txtPrjSubId) & "." & IIf(txtPrjSubsId = "", 0, txtPrjSubsId)
    
    Select Case xNivel
        Case 0: txtDetalle = "Sin agrupamiento"
        Case 1: txtDetalle = "Agrupamiento por recurso"
        Case 2: txtDetalle = "Agrupamiento por grupo"
        Case 3: txtDetalle = "Agrupamiento por sector"
        Case 4: txtDetalle = "Agrupamiento por gerencia"
        Case 5: txtDetalle = "Agrupamiento por direcci�n"
        Case 6: txtDetalle = "Agrupamiento por recurso/petici�n"
    End Select
    
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Horas trabajadas por proyecto / petici�n"
        .ReportComments = "Proyecto: " & txtAux & vbTab & vbTab & " Entre: " & ClearNull(txtDESDE.DateValue) & " y " & ClearNull(txtHASTA.DateValue) & vbTab & vbTab & txtDetalle
    End With

    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
    
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fdesde": crParamDef.AddCurrentValue (IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")))
            Case "@fhasta": crParamDef.AddCurrentValue (IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")))
            Case "@projid": crParamDef.AddCurrentValue CLng(IIf(txtPrjId = "", 0, txtPrjId))
            Case "@projsubid": crParamDef.AddCurrentValue CLng(IIf(txtPrjSubId = "", 0, txtPrjSubId))
            Case "@projsubsid": crParamDef.AddCurrentValue CLng(IIf(txtPrjSubsId = "", 0, txtPrjSubsId))
            Case "@detalle": crParamDef.AddCurrentValue CStr(xNivel)
        End Select
    Next
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1

    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub
