VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDriversValores 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Opciones para drivers"
   ClientHeight    =   5940
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6960
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDriversValores.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5940
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraBotonera 
      Height          =   2415
      Left            =   5700
      TabIndex        =   12
      Top             =   3480
      Width           =   1215
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   435
         Left            =   120
         TabIndex        =   17
         Top             =   1860
         Width           =   945
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   435
         Left            =   120
         TabIndex        =   16
         Top             =   1440
         Width           =   945
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   435
         Left            =   120
         TabIndex        =   15
         Top             =   1020
         Width           =   945
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   435
         Left            =   120
         TabIndex        =   14
         Top             =   600
         Width           =   945
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   435
         Left            =   120
         TabIndex        =   13
         Top             =   180
         Width           =   945
      End
   End
   Begin VB.Frame fraDatos 
      Height          =   2415
      Left            =   60
      TabIndex        =   1
      Top             =   3480
      Width           =   5595
      Begin VB.TextBox txtOrden 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         TabIndex        =   10
         Top             =   1380
         Width           =   675
      End
      Begin VB.TextBox txtCodigo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         TabIndex        =   5
         Top             =   300
         Width           =   915
      End
      Begin VB.ComboBox cboHabilitado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1740
         Width           =   1215
      End
      Begin VB.TextBox txtNombre 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         MaxLength       =   60
         TabIndex        =   3
         Top             =   660
         Width           =   3915
      End
      Begin VB.TextBox txtValor 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0,00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1260
         TabIndex        =   2
         Top             =   1020
         Width           =   675
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Orden"
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   11
         Top             =   1440
         Width           =   450
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Habilitado"
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   9
         Top             =   1815
         Width           =   705
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   8
         Top             =   720
         Width           =   555
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   495
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Valor"
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   6
         Top             =   1080
         Width           =   360
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3420
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   6033
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Consolas"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDriversValores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const col_Codigo = 0
Private Const COL_NOMBRE = 1
Private Const COL_VALOR = 2
Private Const COL_ORDEN = 3
Private Const COL_HABILITADO = 4
Private Const COL_DRIVER = 5
Private Const COL_TOTALCOLS = 6
Private Const SCREEN_MAXHEIGHT = 6420
Private Const SCREEN_MAXWIDTH = 7050

Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarControles
    Call IniciarScroll(grdDatos)
    Call InicializarPantalla
End Sub

Private Sub InicializarControles()
    ' Inicializa el tama�o del form
    'Me.Top = 0
    'Me.Left = 0
    Me.Height = SCREEN_MAXHEIGHT
    Me.Width = SCREEN_MAXWIDTH
    
    With cboHabilitado
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
    End With
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Call CargarGrilla
    Call HabilitarBotones(0)
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        Me.Height = SCREEN_MAXHEIGHT
        Me.Width = SCREEN_MAXWIDTH
    End If
End Sub

Private Sub Form_DblClick()
    'Me.Top = 0
    'Me.Left = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub CargarGrilla()
    With grdDatos
        .Clear
        .Rows = 1
        .cols = COL_TOTALCOLS
        .RowHeightMin = 280
        .TextMatrix(0, col_Codigo) = "C�digo": .ColWidth(col_Codigo) = 700: .ColAlignment(col_Codigo) = flexAlignLeftCenter
        .TextMatrix(0, COL_NOMBRE) = "Nombre": .ColWidth(COL_NOMBRE) = 3600: .ColAlignment(COL_NOMBRE) = flexAlignLeftCenter
        .TextMatrix(0, COL_VALOR) = "Valor": .ColWidth(COL_VALOR) = 1000: .ColAlignment(COL_VALOR) = flexAlignCenterCenter
        .TextMatrix(0, COL_ORDEN) = "Orden": .ColWidth(COL_ORDEN) = 800: .ColAlignment(COL_ORDEN) = flexAlignCenterCenter
        .TextMatrix(0, COL_HABILITADO) = "Hab.": .ColWidth(COL_HABILITADO) = 800: .ColAlignment(COL_HABILITADO) = flexAlignLeftCenter
        .TextMatrix(0, COL_DRIVER) = "": .ColWidth(COL_DRIVER) = 0: .ColAlignment(COL_DRIVER) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
        If sp_GetValoracion(frmDrivers.lDriverSeleccionado, Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, col_Codigo) = ClearNull(aplRST.Fields!valorId)
                .TextMatrix(.Rows - 1, COL_NOMBRE) = ClearNull(aplRST.Fields!valorTexto)
                .TextMatrix(.Rows - 1, COL_VALOR) = Format(ClearNull(aplRST.Fields!valorNum), "###,###,##0.00")
                .TextMatrix(.Rows - 1, COL_ORDEN) = ClearNull(aplRST.Fields!valorOrden)
                .TextMatrix(.Rows - 1, COL_HABILITADO) = ClearNull(aplRST.Fields!valorHab)
                .TextMatrix(.Rows - 1, COL_DRIVER) = ClearNull(aplRST.Fields!valorDriver)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            'lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    fraDatos.Caption = " Agregar "
                    txtCodigo = "": txtNombre = ""
                    txtValor = "": txtOrden = ""
                    cboHabilitado.ListIndex = 0
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    fraDatos.Caption = " Modificar "
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtNombre.SetFocus
                Case "E"
                    fraDatos.Caption = " Eliminar "
                    fraDatos.Enabled = False
            End Select
    End Select
End Sub

Private Sub cmdConfirmar_Click()
    Call GuardarDatos
End Sub

Private Sub GuardarDatos()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                'Call sp_InsertIndicador(txtCodigo, txtNombre, CodigoCombo(cboHabilitado, True))
                Call sp_InsertValoracion(txtCodigo, txtNombre, txtValor, txtOrden, CodigoCombo(cboHabilitado, True), frmDrivers.lDriverSeleccionado)
                Call HabilitarBotones(0)
            End If
        Case "M"
            If CamposObligatorios Then
                'Call sp_UpdateIndicador(txtCodigo, txtNombre, CodigoCombo(cboHabilitado, True))
                Call sp_UpdateValoracion(txtCodigo, txtNombre, txtValor, txtOrden, CodigoCombo(cboHabilitado, True), frmDrivers.lDriverSeleccionado)
                With grdDatos
                    If .RowSel > 0 Then
                       .TextMatrix(.RowSel, col_Codigo) = ClearNull(txtCodigo)
                       .TextMatrix(.RowSel, COL_NOMBRE) = ClearNull(txtNombre)
                       .TextMatrix(.RowSel, COL_VALOR) = Format(ClearNull(txtValor), "###,###,##0.00")
                       .TextMatrix(.RowSel, COL_ORDEN) = ClearNull(txtOrden)
                       .TextMatrix(.RowSel, COL_HABILITADO) = CodigoCombo(cboHabilitado, True)
                    End If
                End With
                Call HabilitarBotones(0, False)
            End If
        Case "E"
            'If sp_DeleteIndicador(txtCodigo) Then
            If sp_DeleteValoracion(ClearNull(txtCodigo)) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, col_Codigo) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, col_Codigo))
                txtNombre = ClearNull(.TextMatrix(.RowSel, COL_NOMBRE))
                txtValor = ClearNull(.TextMatrix(.RowSel, COL_VALOR))
                txtOrden = ClearNull(.TextMatrix(.RowSel, COL_ORDEN))
                Call SetCombo(cboHabilitado, ClearNull(.TextMatrix(.RowSel, COL_HABILITADO)), True)
            End If
        End If
    End With
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True

    If ClearNull(txtCodigo.text) = "" Then
        MsgBox "Debe completar el c�digo.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.text, ":") > 0 Then
        MsgBox "El c�digo no pude contener el caracter ':'.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If ClearNull(txtNombre.text) = "" Then
        MsgBox "Debe completar el nombre.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtNombre.text, ":") > 0 Then
        MsgBox "El nombre no pude contener el caracter ':'.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If cboHabilitado.ListIndex < 0 Then
        MsgBox "Debe seleccionar si est� habilitado o no.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
'    If Not ValidarNumero(txtValor) Then
'        MsgBox "Debe seleccionar si est� habilitado o no.", vbExclamation + vbOKOnly
'        CamposObligatorios = False
'        Exit Function
'    End If
End Function

Private Sub txtValor_Validate(Cancel As Boolean)
    If Len(txtValor) > 0 Then
        Call ValidarNumero(txtValor)
    End If
End Sub

Private Function ValidarNumero(ByRef oControl As TextBox) As Boolean
    ValidarNumero = False
    If Len(oControl) > 0 Then
        If IsNumeric(oControl) Then
            If CDbl(oControl) > 0 Then
                oControl = Replace(oControl, ".", ",", 1, , vbTextCompare)
                oControl = Format(oControl, "########0.00")
                ValidarNumero = True
            End If
        End If
    End If
End Function
