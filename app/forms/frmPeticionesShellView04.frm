VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPeticionesShellView04 
   Caption         =   "Form2"
   ClientHeight    =   8745
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11910
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesShellView04.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8745
   ScaleWidth      =   11910
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   11040
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesShellView04.frx":058A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   600
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   1058
      ButtonWidth     =   1535
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Consultar"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Otro"
            ImageIndex      =   1
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7815
      Left            =   60
      TabIndex        =   0
      Top             =   660
      Width           =   11685
      _ExtentX        =   20611
      _ExtentY        =   13785
      _Version        =   393216
      TabOrientation  =   3
      Style           =   1
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmPeticionesShellView04.frx":0B24
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblFiltro(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblFiltro(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblFiltro(2)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblFiltro(3)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblFiltro(4)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblFiltro(5)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblFiltro(6)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblFiltro(7)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblFiltro(8)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblFiltro(9)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Line1"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblFiltro(10)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblFiltro(11)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblFiltro(12)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblFiltro(13)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblFiltro(14)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblFiltro(15)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "lblFiltro(16)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Line2"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "lblFiltro(17)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "lblFiltro(18)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "lblFiltro(19)"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "lblFiltro(20)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "lblFiltro(21)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "lblFiltro(22)"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "lblFiltro(23)"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "lblFiltro(24)"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "lblFiltro(25)"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "Line3"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "lblFiltro(26)"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "lblFiltro(27)"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "lblFiltro(28)"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "lblFiltro(29)"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "lblFiltro(30)"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "lblFiltro(31)"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "lblFiltro(32)"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "lblFiltro(33)"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "lblFiltro(34)"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "lblFiltro(35)"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "lblFiltro(36)"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "lblFiltro(37)"
      Tab(0).Control(40).Enabled=   0   'False
      Tab(0).Control(41)=   "DTPicker14"
      Tab(0).Control(41).Enabled=   0   'False
      Tab(0).Control(42)=   "DTPicker12"
      Tab(0).Control(42).Enabled=   0   'False
      Tab(0).Control(43)=   "DTPicker10"
      Tab(0).Control(43).Enabled=   0   'False
      Tab(0).Control(44)=   "DTPicker8"
      Tab(0).Control(44).Enabled=   0   'False
      Tab(0).Control(45)=   "DTPicker6"
      Tab(0).Control(45).Enabled=   0   'False
      Tab(0).Control(46)=   "DTPicker4"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).Control(47)=   "DTPicker2"
      Tab(0).Control(47).Enabled=   0   'False
      Tab(0).Control(48)=   "MaskText4"
      Tab(0).Control(48).Enabled=   0   'False
      Tab(0).Control(49)=   "MaskText3"
      Tab(0).Control(49).Enabled=   0   'False
      Tab(0).Control(50)=   "MaskText2"
      Tab(0).Control(50).Enabled=   0   'False
      Tab(0).Control(51)=   "MaskText1"
      Tab(0).Control(51).Enabled=   0   'False
      Tab(0).Control(52)=   "cmdAgrup"
      Tab(0).Control(52).Enabled=   0   'False
      Tab(0).Control(53)=   "cmdClrAgrup"
      Tab(0).Control(53).Enabled=   0   'False
      Tab(0).Control(54)=   "cboTipopet"
      Tab(0).Control(54).Enabled=   0   'False
      Tab(0).Control(55)=   "Combo1"
      Tab(0).Control(55).Enabled=   0   'False
      Tab(0).Control(56)=   "Combo2"
      Tab(0).Control(56).Enabled=   0   'False
      Tab(0).Control(57)=   "Combo3"
      Tab(0).Control(57).Enabled=   0   'False
      Tab(0).Control(58)=   "Combo4"
      Tab(0).Control(58).Enabled=   0   'False
      Tab(0).Control(59)=   "Combo5"
      Tab(0).Control(59).Enabled=   0   'False
      Tab(0).Control(60)=   "Combo6"
      Tab(0).Control(60).Enabled=   0   'False
      Tab(0).Control(61)=   "DTPicker1"
      Tab(0).Control(61).Enabled=   0   'False
      Tab(0).Control(62)=   "Combo7"
      Tab(0).Control(62).Enabled=   0   'False
      Tab(0).Control(63)=   "Combo8"
      Tab(0).Control(63).Enabled=   0   'False
      Tab(0).Control(64)=   "frmSolic"
      Tab(0).Control(64).Enabled=   0   'False
      Tab(0).Control(65)=   "Combo9"
      Tab(0).Control(65).Enabled=   0   'False
      Tab(0).Control(66)=   "Combo10"
      Tab(0).Control(66).Enabled=   0   'False
      Tab(0).Control(67)=   "Combo11"
      Tab(0).Control(67).Enabled=   0   'False
      Tab(0).Control(68)=   "DTPicker3"
      Tab(0).Control(68).Enabled=   0   'False
      Tab(0).Control(69)=   "DTPicker5"
      Tab(0).Control(69).Enabled=   0   'False
      Tab(0).Control(70)=   "Frame1"
      Tab(0).Control(70).Enabled=   0   'False
      Tab(0).Control(71)=   "Combo12"
      Tab(0).Control(71).Enabled=   0   'False
      Tab(0).Control(72)=   "Combo13"
      Tab(0).Control(72).Enabled=   0   'False
      Tab(0).Control(73)=   "Combo14"
      Tab(0).Control(73).Enabled=   0   'False
      Tab(0).Control(74)=   "DTPicker7"
      Tab(0).Control(74).Enabled=   0   'False
      Tab(0).Control(75)=   "DTPicker9"
      Tab(0).Control(75).Enabled=   0   'False
      Tab(0).Control(76)=   "DTPicker11"
      Tab(0).Control(76).Enabled=   0   'False
      Tab(0).Control(77)=   "DTPicker13"
      Tab(0).Control(77).Enabled=   0   'False
      Tab(0).Control(78)=   "fraApertura"
      Tab(0).Control(78).Enabled=   0   'False
      Tab(0).ControlCount=   79
      TabCaption(1)   =   "Espec�ficos"
      TabPicture(1)   =   "frmPeticionesShellView04.frx":0B40
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Resultado"
      TabPicture(2)   =   "frmPeticionesShellView04.frx":0B5C
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "Exportar"
      TabPicture(3)   =   "frmPeticionesShellView04.frx":0B78
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      Begin VB.Frame fraApertura 
         Caption         =   " Apertura "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   9720
         TabIndex        =   85
         Top             =   6210
         Width           =   1455
         Begin VB.OptionButton optApertura 
            Caption         =   "Sin apertura"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   88
            Top             =   360
            Width           =   1215
         End
         Begin VB.OptionButton optApertura 
            Caption         =   "Sector"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   87
            Top             =   600
            Width           =   1215
         End
         Begin VB.OptionButton optApertura 
            Caption         =   "Grupo"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   86
            Top             =   840
            Width           =   1215
         End
      End
      Begin MSComCtl2.DTPicker DTPicker13 
         Height          =   315
         Left            =   5520
         TabIndex        =   83
         Top             =   7050
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker11 
         Height          =   315
         Left            =   5520
         TabIndex        =   79
         Top             =   6690
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker9 
         Height          =   315
         Left            =   1440
         TabIndex        =   75
         Top             =   7050
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker7 
         Height          =   315
         Left            =   1440
         TabIndex        =   71
         Top             =   6690
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin VB.ComboBox Combo14 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   68
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   6330
         Width           =   4575
      End
      Begin VB.ComboBox Combo13 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   67
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   5970
         Width           =   4575
      End
      Begin VB.ComboBox Combo12 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   64
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   5610
         Width           =   8535
      End
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   215
         Left            =   1440
         TabIndex        =   57
         Top             =   5370
         Width           =   4590
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Sector"
            Height          =   195
            Index           =   7
            Left            =   3000
            TabIndex        =   61
            Top             =   0
            Width           =   885
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Gerencia"
            Height          =   195
            Index           =   6
            Left            =   1920
            TabIndex        =   60
            Top             =   0
            Width           =   1000
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Direcci�n"
            Height          =   195
            Index           =   5
            Left            =   840
            TabIndex        =   59
            Top             =   0
            Width           =   1035
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "BBVA"
            Height          =   195
            Index           =   4
            Left            =   0
            TabIndex        =   58
            Top             =   0
            Width           =   765
         End
      End
      Begin MSComCtl2.DTPicker DTPicker5 
         Height          =   315
         Left            =   6360
         TabIndex        =   54
         Top             =   4410
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker3 
         Height          =   315
         Left            =   6360
         TabIndex        =   50
         Top             =   4050
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin VB.ComboBox Combo11 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   47
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   4410
         Width           =   4095
      End
      Begin VB.ComboBox Combo10 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   46
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   4050
         Width           =   4095
      End
      Begin VB.ComboBox Combo9 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   43
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   3690
         Width           =   8535
      End
      Begin VB.Frame frmSolic 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   215
         Left            =   1440
         TabIndex        =   36
         Top             =   3450
         Width           =   4590
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "BBVA"
            Height          =   195
            Index           =   0
            Left            =   0
            TabIndex        =   40
            Top             =   0
            Width           =   765
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Direcci�n"
            Height          =   195
            Index           =   1
            Left            =   840
            TabIndex        =   39
            Top             =   0
            Width           =   1035
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Gerencia"
            Height          =   195
            Index           =   2
            Left            =   1920
            TabIndex        =   38
            Top             =   0
            Width           =   1000
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Sector"
            Height          =   195
            Index           =   3
            Left            =   3000
            TabIndex        =   37
            Top             =   0
            Width           =   885
         End
      End
      Begin VB.ComboBox Combo8 
         Height          =   315
         Left            =   6960
         Style           =   2  'Dropdown List
         TabIndex        =   34
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1890
         Width           =   975
      End
      Begin VB.ComboBox Combo7 
         Height          =   315
         Left            =   3720
         Style           =   2  'Dropdown List
         TabIndex        =   32
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1890
         Width           =   1335
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   5760
         TabIndex        =   29
         Top             =   2250
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin VB.ComboBox Combo6 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   26
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   2610
         Width           =   3615
      End
      Begin VB.ComboBox Combo5 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   25
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   2250
         Width           =   3615
      End
      Begin VB.ComboBox Combo4 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   21
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1890
         Width           =   1335
      End
      Begin VB.ComboBox Combo3 
         Height          =   315
         Left            =   9120
         Style           =   2  'Dropdown List
         TabIndex        =   19
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1530
         Width           =   975
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   6960
         Style           =   2  'Dropdown List
         TabIndex        =   17
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1530
         Width           =   975
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   3720
         Style           =   2  'Dropdown List
         TabIndex        =   16
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1530
         Width           =   1335
      End
      Begin VB.ComboBox cboTipopet 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   13
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1530
         Width           =   1335
      End
      Begin VB.CommandButton cmdClrAgrup 
         Height          =   315
         Left            =   10320
         Picture         =   "frmPeticionesShellView04.frx":0B94
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Limpia agrupamiento seleccionado"
         Top             =   810
         Width           =   345
      End
      Begin VB.CommandButton cmdAgrup 
         Height          =   315
         Left            =   9960
         Picture         =   "frmPeticionesShellView04.frx":111E
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar agrupamiento para filtrar las peticiones"
         Top             =   810
         Width           =   345
      End
      Begin AT_MaskText.MaskText MaskText1 
         Height          =   300
         Left            =   1440
         TabIndex        =   3
         Top             =   810
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText MaskText2 
         Height          =   300
         Left            =   3240
         TabIndex        =   5
         Top             =   810
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText MaskText3 
         Height          =   300
         Left            =   5640
         TabIndex        =   7
         Top             =   810
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText MaskText4 
         Height          =   300
         Left            =   1440
         TabIndex        =   11
         Top             =   1170
         Width           =   9255
         _ExtentX        =   16325
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   315
         Left            =   7920
         TabIndex        =   30
         Top             =   2250
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker4 
         Height          =   315
         Left            =   8640
         TabIndex        =   51
         Top             =   4050
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker6 
         Height          =   315
         Left            =   8640
         TabIndex        =   55
         Top             =   4410
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker8 
         Height          =   315
         Left            =   3480
         TabIndex        =   72
         Top             =   6690
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker10 
         Height          =   315
         Left            =   3480
         TabIndex        =   76
         Top             =   7050
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker12 
         Height          =   315
         Left            =   7560
         TabIndex        =   80
         Top             =   6690
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin MSComCtl2.DTPicker DTPicker14 
         Height          =   315
         Left            =   7560
         TabIndex        =   84
         Top             =   7050
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   42459
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   37
         Left            =   6960
         TabIndex        =   82
         Top             =   7050
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   36
         Left            =   4920
         TabIndex        =   81
         Top             =   7050
         Width           =   510
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   35
         Left            =   6960
         TabIndex        =   78
         Top             =   6690
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   34
         Left            =   4920
         TabIndex        =   77
         Top             =   6690
         Width           =   510
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   33
         Left            =   2880
         TabIndex        =   74
         Top             =   7050
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   32
         Left            =   240
         TabIndex        =   73
         Top             =   7050
         Width           =   510
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   31
         Left            =   2880
         TabIndex        =   70
         Top             =   6690
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   30
         Left            =   240
         TabIndex        =   69
         Top             =   6690
         Width           =   510
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n:"
         Height          =   195
         Index           =   29
         Left            =   240
         TabIndex        =   66
         Top             =   6330
         Width           =   705
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         Height          =   195
         Index           =   28
         Left            =   240
         TabIndex        =   65
         Top             =   5970
         Width           =   555
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "�rea:"
         Height          =   195
         Index           =   27
         Left            =   240
         TabIndex        =   63
         Top             =   5670
         Width           =   405
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Nivel:"
         Height          =   195
         Index           =   26
         Left            =   240
         TabIndex        =   62
         Top             =   5370
         Width           =   405
      End
      Begin VB.Line Line3 
         BorderWidth     =   2
         X1              =   2520
         X2              =   10920
         Y1              =   5130
         Y2              =   5130
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Filtro por �rea ejecutora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   240
         TabIndex        =   56
         Top             =   5010
         Width           =   2070
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   24
         Left            =   8040
         TabIndex        =   53
         Top             =   4410
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   23
         Left            =   5760
         TabIndex        =   52
         Top             =   4410
         Width           =   510
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   22
         Left            =   8040
         TabIndex        =   49
         Top             =   4050
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   21
         Left            =   5760
         TabIndex        =   48
         Top             =   4050
         Width           =   510
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Ref. RGyP:"
         Height          =   195
         Index           =   20
         Left            =   240
         TabIndex        =   45
         Top             =   4410
         Width           =   810
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Ref. sistemas:"
         Height          =   195
         Index           =   19
         Left            =   240
         TabIndex        =   44
         Top             =   4050
         Width           =   1035
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "�rea:"
         Height          =   195
         Index           =   18
         Left            =   240
         TabIndex        =   42
         Top             =   3750
         Width           =   405
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Nivel:"
         Height          =   195
         Index           =   17
         Left            =   240
         TabIndex        =   41
         Top             =   3450
         Width           =   405
      End
      Begin VB.Line Line2 
         BorderWidth     =   2
         X1              =   2520
         X2              =   10920
         Y1              =   3210
         Y2              =   3210
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Filtro por �rea solicitante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   240
         TabIndex        =   35
         Top             =   3090
         Width           =   2130
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Riesgo operacional:"
         Height          =   195
         Index           =   15
         Left            =   5280
         TabIndex        =   33
         Top             =   1890
         Width           =   1410
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Empresa:"
         Height          =   195
         Index           =   14
         Left            =   3000
         TabIndex        =   31
         Top             =   1890
         Width           =   675
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   13
         Left            =   7320
         TabIndex        =   28
         Top             =   2250
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   12
         Left            =   5160
         TabIndex        =   27
         Top             =   2250
         Width           =   510
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n:"
         Height          =   195
         Index           =   11
         Left            =   240
         TabIndex        =   24
         Top             =   2610
         Width           =   705
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         Height          =   195
         Index           =   10
         Left            =   240
         TabIndex        =   23
         Top             =   2250
         Width           =   555
      End
      Begin VB.Line Line1 
         BorderWidth     =   2
         X1              =   2880
         X2              =   10920
         Y1              =   570
         Y2              =   570
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Filtro por datos de la petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   240
         TabIndex        =   22
         Top             =   450
         Width           =   2460
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad:"
         Height          =   195
         Index           =   8
         Left            =   240
         TabIndex        =   20
         Top             =   1890
         Width           =   735
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Regulatorio:"
         Height          =   195
         Index           =   7
         Left            =   8160
         TabIndex        =   18
         Top             =   1530
         Width           =   885
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Impacto tecnol�gico:"
         Height          =   195
         Index           =   6
         Left            =   5280
         TabIndex        =   15
         Top             =   1530
         Width           =   1500
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Clase:"
         Height          =   195
         Index           =   5
         Left            =   3000
         TabIndex        =   14
         Top             =   1530
         Width           =   450
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Tipo:"
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   12
         Top             =   1530
         Width           =   360
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo (o parte):"
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   10
         Top             =   1170
         Width           =   1140
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento:"
         Height          =   195
         Index           =   2
         Left            =   4440
         TabIndex        =   6
         Top             =   810
         Width           =   1065
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   1
         Left            =   2640
         TabIndex        =   4
         Top             =   810
         Width           =   480
      End
      Begin VB.Label lblFiltro 
         AutoSize        =   -1  'True
         Caption         =   "Nro. desde:"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   810
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmPeticionesShellView04"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

