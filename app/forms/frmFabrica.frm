VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFabrica 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "F�bricas de software"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11040
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   11040
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3060
      Left            =   120
      TabIndex        =   6
      Top             =   3480
      Width           =   9525
      Begin VB.ComboBox cboTipoFabrica 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmFabrica.frx":0000
         Left            =   3960
         List            =   "frmFabrica.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   2400
         Width           =   3705
      End
      Begin VB.ComboBox cboGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   1680
         Width           =   5265
      End
      Begin VB.ComboBox cboSector 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1320
         Width           =   5265
      End
      Begin VB.ComboBox cboGerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   960
         Width           =   5265
      End
      Begin VB.ComboBox cboDireccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   600
         Width           =   5265
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmFabrica.frx":0004
         Left            =   1200
         List            =   "frmFabrica.frx":0006
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   2400
         Width           =   1900
      End
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   300
         Left            =   1200
         TabIndex        =   7
         Top             =   240
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   300
         Left            =   1200
         TabIndex        =   8
         Top             =   2040
         Width           =   6465
         _ExtentX        =   11404
         _ExtentY        =   529
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3360
         TabIndex        =   23
         Top             =   2475
         Width           =   300
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   675
         Width           =   645
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   1755
         Width           =   435
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   1395
         Width           =   465
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   1035
         Width           =   630
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   2475
         Width           =   495
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   11
         Top             =   30
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n por defecto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   120
         TabIndex        =   10
         Top             =   1980
         Width           =   975
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   300
         Width           =   495
      End
   End
   Begin VB.Frame pnlBotones 
      Height          =   6545
      Left            =   9720
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Gerencia"
         Top             =   3930
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la Gerencia selecionada"
         Top             =   4440
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Gerencia seleccionada"
         Top             =   4950
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   5970
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5460
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3450
      Left            =   120
      TabIndex        =   12
      Top             =   30
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   6085
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFabrica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 24.06.2015 - Nuevo: se mejora el mantenimiento de las f�bricas de software.

Option Explicit

Private Const colCODFAB = 0
Private Const colNOMFAB = 1
Private Const colCODDIRECCION = 2
Private Const colNOMDIRECCION = 3
Private Const colCODGERENCIA = 4
Private Const colNOMGERENCIA = 5
Private Const colCODSECTOR = 6
Private Const colNOMSECTOR = 7
Private Const colCODGRUPO = 8
Private Const colNOMGRUPO = 9
Private Const colESTADO = 10
Private Const colTIPOFABCOD = 11
Private Const colTIPOFABNOM = 12
Private Const colNOMRESPONSABLE = 13
Private Const colNOMFABDEFAULT = 14
Private Const colTOTALCOLS = 15

Dim vDireccion() As String
Dim vGerencia() As String
Dim vSector() As String
Dim vGrupo() As String

Dim inCarga As Boolean
Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call IniciarScroll(grdDatos)
    Call InicializarCombos
    Call InicializarPantalla
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    Me.Top = 0
    Me.Left = 0
    sOpcionSeleccionada = ""
    Call HabilitarBotones(0)
    Call CargarGrid
End Sub

Private Sub InicializarCombos()
    Dim i As Long
    
    '{ add -001- a.
    With cboTipoFabrica
        .Clear
        If sp_GetTipoFabrica(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!TipoFabNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!TipoFabId)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    '}
    If sp_GetDireccion("") Then
        cboDireccion.Clear
        If Not aplRST.EOF Then Erase vDireccion
        Do While Not aplRST.EOF
            ReDim Preserve vDireccion(i)
            vDireccion(i) = aplRST(0) & " : " & Trim(aplRST(1))
            cboDireccion.AddItem vDireccion(i)
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    i = 0
    If sp_GetGerencia("", "") Then
        cboGerencia.Clear
        If Not aplRST.EOF Then Erase vGerencia
        Do While Not aplRST.EOF
            ReDim Preserve vGerencia(i)
            vGerencia(i) = aplRST(0) & " : " & Trim(aplRST(1))
            cboGerencia.AddItem vGerencia(i)
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    i = 0
    If sp_GetSector("", "") Then
        cboSector.Clear
        If Not aplRST.EOF Then Erase vSector
        Do While Not aplRST.EOF
            ReDim Preserve vSector(i)
            vSector(i) = aplRST(0) & " : " & Trim(aplRST(1))
            cboSector.AddItem vSector(i)
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    i = 0
    If sp_GetGrupo("", "") Then
        cboGrupo.Clear
        If Not aplRST.EOF Then Erase vGrupo
        Do While Not aplRST.EOF
            ReDim Preserve vGrupo(i)
            vGrupo(i) = aplRST(0) & " : " & Trim(aplRST(1))
            cboGrupo.AddItem vGrupo(i)
            aplRST.MoveNext
            i = i + 1
        Loop
    End If
    With cboEstado
        .Clear
        .AddItem "A : ACTIVO"
        .AddItem "S : SUSPENDIDO"
        .AddItem "B : BAJA"
    End With
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            Call setHabilCtrl(cmdAgregar, "NOR")
            Call setHabilCtrl(cmdModificar, "NOR")
            Call setHabilCtrl(cmdConfirmar, "DIS")
            Call setHabilCtrl(cmdEliminar, "DIS")
            Call setHabilCtrl(cmdCerrar, "NOR")
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            sOpcionSeleccionada = ""
            Call setHabilCtrl(txtCodigo, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(cboDireccion, "DIS")
            Call setHabilCtrl(cboGerencia, "DIS")
            Call setHabilCtrl(cboSector, "DIS")
            Call setHabilCtrl(cboGrupo, "DIS")
            Call setHabilCtrl(cboEstado, "DIS")
            Call setHabilCtrl(cboTipoFabrica, "DIS")            ' add -001- a.
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            Call setHabilCtrl(cmdAgregar, "DIS")
            Call setHabilCtrl(cmdModificar, "DIS")
            Call setHabilCtrl(cmdConfirmar, "NOR")
            Call setHabilCtrl(cmdCerrar, "NOR")
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    Call setHabilCtrl(txtCodigo, "NOR")
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(cboDireccion, "NOR")
                    Call setHabilCtrl(cboGerencia, "NOR")
                    Call setHabilCtrl(cboSector, "NOR")
                    Call setHabilCtrl(cboGrupo, "NOR")
                    'Call setHabilCtrl(cboEstado, "NOR")
                    Call setHabilCtrl(cboTipoFabrica, "NOR")            ' add -001- a.
                    txtCodigo = ""
                    txtDescripcion = ""
                    cboDireccion.ListIndex = -1
                    cboGerencia.ListIndex = -1
                    cboSector.ListIndex = -1
                    cboGrupo.ListIndex = -1
                    cboTipoFabrica.ListIndex = -1                       ' add -001- a.
                    cboEstado.ListIndex = SetCombo(cboEstado, "A", False)
                    txtCodigo.Enabled = True
                    fraDatos.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    'txtCodigo.Enabled = False
                    Call setHabilCtrl(txtCodigo, "DIS")
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(cboDireccion, "NOR")
                    Call setHabilCtrl(cboGerencia, "NOR")
                    Call setHabilCtrl(cboSector, "NOR")
                    Call setHabilCtrl(cboGrupo, "NOR")
                    Call setHabilCtrl(cboEstado, "NOR")
                    Call setHabilCtrl(cboTipoFabrica, "NOR")            ' add -001- a.
                    fraDatos.Enabled = True
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Public Function MostrarSeleccion(Optional flgSort)
    Dim nRow As Integer
    
    inCarga = True
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    With grdDatos
        nRow = .RowSel
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, 0) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODFAB))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNOMFABDEFAULT))
                cboDireccion.ListIndex = PosicionCombo(Me.cboDireccion, .TextMatrix(.RowSel, colCODDIRECCION), False)
                cboGerencia.ListIndex = PosicionCombo(Me.cboGerencia, .TextMatrix(.RowSel, colCODGERENCIA), False)
                cboSector.ListIndex = PosicionCombo(Me.cboSector, .TextMatrix(.RowSel, colCODSECTOR), False)
                cboGrupo.ListIndex = PosicionCombo(Me.cboGrupo, .TextMatrix(.RowSel, colCODGRUPO), False)
                cboEstado.ListIndex = PosicionCombo(Me.cboEstado, .TextMatrix(.RowSel, colESTADO))
                cboTipoFabrica.ListIndex = PosicionCombo(Me.cboTipoFabrica, .TextMatrix(.RowSel, colTIPOFABCOD), True)     ' add -001- a.
            End If
        End If
    End With
    inCarga = False
End Function

Private Sub CargarGrid()
    Call Puntero(True)
    
    inCarga = True
    With grdDatos
        .visible = False
        DoEvents
        .Clear
        .cols = colTOTALCOLS               ' upd -001- a. Antes 7.
        .Rows = 1
        .FocusRect = flexFocusNone
        .TextMatrix(0, colCODFAB) = "Codigo": .ColWidth(colCODFAB) = 1200
        .TextMatrix(0, colNOMFAB) = "Denominaci�n autom�tica": .ColWidth(colNOMFAB) = 4000
        .TextMatrix(0, colNOMFABDEFAULT) = "Descripci�n por defecto": .ColWidth(colNOMFABDEFAULT) = 4000
        .TextMatrix(0, colCODDIRECCION) = "Direcc.": .ColWidth(colCODDIRECCION) = 0
        .TextMatrix(0, colCODGERENCIA) = "Geren.": .ColWidth(colCODGERENCIA) = 0
        .TextMatrix(0, colCODSECTOR) = "Sector": .ColWidth(colCODSECTOR) = 0
        .TextMatrix(0, colCODGRUPO) = "Grup.": .ColWidth(colCODGRUPO) = 0
        .TextMatrix(0, colNOMDIRECCION) = "Direcci�n": .ColWidth(colNOMDIRECCION) = 2000
        .TextMatrix(0, colNOMGERENCIA) = "Gerencia": .ColWidth(colNOMGERENCIA) = 2000
        .TextMatrix(0, colNOMSECTOR) = "Sector": .ColWidth(colNOMSECTOR) = 2000
        .TextMatrix(0, colNOMGRUPO) = "Grupo": .ColWidth(colNOMGRUPO) = 2000
        .TextMatrix(0, colESTADO) = "Est.": .ColWidth(colESTADO) = 500
        .TextMatrix(0, colTIPOFABCOD) = "Tipo": .ColWidth(colTIPOFABCOD) = 0
        .TextMatrix(0, colTIPOFABNOM) = "Tipo": .ColWidth(colTIPOFABNOM) = 1500
        .TextMatrix(0, colNOMRESPONSABLE) = "Responsable": .ColWidth(colNOMRESPONSABLE) = 2000
        'Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
        If sp_GetFabrica(Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colCODFAB) = ClearNull(aplRST.Fields!cod_fab)
                .TextMatrix(.Rows - 1, colNOMFAB) = ClearNull(aplRST.Fields!nom_fab)
                .TextMatrix(.Rows - 1, colNOMFABDEFAULT) = ClearNull(aplRST.Fields!nom_default)
                .TextMatrix(.Rows - 1, colCODDIRECCION) = ClearNull(aplRST.Fields!cod_direccion)
                .TextMatrix(.Rows - 1, colCODGERENCIA) = ClearNull(aplRST.Fields!cod_gerencia)
                .TextMatrix(.Rows - 1, colCODSECTOR) = ClearNull(aplRST.Fields!cod_sector)
                .TextMatrix(.Rows - 1, colCODGRUPO) = ClearNull(aplRST.Fields!cod_grupo)
                .TextMatrix(.Rows - 1, colNOMDIRECCION) = ClearNull(aplRST.Fields!nom_direccion)
                .TextMatrix(.Rows - 1, colNOMGERENCIA) = ClearNull(aplRST.Fields!nom_gerencia)
                .TextMatrix(.Rows - 1, colNOMSECTOR) = ClearNull(aplRST.Fields!nom_sector)
                .TextMatrix(.Rows - 1, colNOMGRUPO) = ClearNull(aplRST.Fields!nom_grupo)
                .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST.Fields!estado_fab)
                .TextMatrix(.Rows - 1, colTIPOFABCOD) = ClearNull(aplRST.Fields!tipo_fab)
                .TextMatrix(.Rows - 1, colTIPOFABNOM) = ClearNull(aplRST.Fields!TipoFabNom)
                .TextMatrix(.Rows - 1, colNOMRESPONSABLE) = ClearNull(aplRST.Fields!nom_responsable)
'                Select Case ClearNull(aplRST.Fields!estado_fab)
'                    Case "B": Call PintarLinea(grdDatos, prmGridFillRowColorRed)
'                    Case "S": Call PintarLinea(grdDatos, prmGridFillRowColorLightOrange)
'                End Select
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
finx:
    DoEvents
    Call Puntero(False)
    Call MostrarSeleccion
    inCarga = False
End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                Call sp_InsertFabrica(txtCodigo, txtDescripcion, CodigoCombo(cboGerencia, False), CodigoCombo(cboDireccion, False), CodigoCombo(cboSector, False), CodigoCombo(cboGrupo, False), CodigoCombo(cboEstado, False), CodigoCombo(cboTipoFabrica, True))
                Call InicializarCombos
                Call HabilitarBotones(0)
            End If
        Case "M"
            If CamposObligatorios Then
                Call sp_UpdateFabrica(txtCodigo, txtDescripcion, CodigoCombo(cboGerencia, False), CodigoCombo(cboDireccion, False), CodigoCombo(cboSector, False), CodigoCombo(cboGrupo, False), CodigoCombo(cboEstado, False), CodigoCombo(cboTipoFabrica, True))
                Call InicializarCombos
                Call HabilitarBotones(0)
            End If
        Case "E"
            If sp_DeleteFabrica(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
            'If sp_DeleteRecurso(txtCodigo) Then
            '    Call HabilitarBotones(0)
            'End If
    End Select
    Call LockProceso(False)
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    ' Control: C�digo de recurso
    If Trim(txtCodigo.text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If sOpcionSeleccionada = "A" Then
        If sp_GetFabrica(ClearNull(txtCodigo.text)) Then
            MsgBox "El c�digo " & ClearNull(txtCodigo.text) & " ya existe en la tabla de F�bricas.", vbCritical + vbOKOnly
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If InStr(txtCodigo.text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    '{ add -001- a.
    If cboTipoFabrica.ListIndex < 0 Then
        MsgBox ("Debe asignar un tipo de f�brica")
        CamposObligatorios = False
        Exit Function
    End If
    '}
'    ' Control: Nombre y apellido del recurso
'    If Trim(txtDescripcion.Text) = "" Then
'        MsgBox ("Debe completar Descripci�n")
'        CamposObligatorios = False
'        Exit Function
'    End If
    If InStr(txtDescripcion.text, ":") > 0 Then
        MsgBox ("La Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    ' Control: Direcci�n
    If cboDireccion.ListIndex < 0 Then
        MsgBox ("Debe asignarle una Direccion")
        CamposObligatorios = False
        Exit Function
    End If
    If CodigoCombo(cboEstado, False) = "" Then
        MsgBox "Debe establecer el estado del recurso", vbExclamation + vbOKOnly, "Estado del recurso"
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub cboDireccion_Click()
    Dim xAux As String
    If inCarga Then
        Exit Sub
    End If
    If cboDireccion.ListIndex >= 0 Then
        xAux = CodigoCombo(Me.cboDireccion)
        cboSector.Clear
        cboGrupo.Clear
        cargaGerencias (xAux)
    End If
End Sub

Private Sub cboGerencia_Click()
    Dim xAux As String
    If inCarga Then
        Exit Sub
    End If
    If cboGerencia.ListIndex >= 0 Then
        xAux = CodigoCombo(Me.cboGerencia)
        cboGrupo.Clear
        cargaSector (xAux)
    End If
End Sub

Private Sub cboSector_Click()
Dim xAux As String
    If inCarga Then
        Exit Sub
    End If
    If cboSector.ListIndex >= 0 Then
        xAux = CodigoCombo(Me.cboSector)
        cargaGrupo (xAux)
    End If
End Sub

Private Sub cargaGerencias(sDire As String)
    cboGerencia.Clear
    If sp_GetGerencia("", sDire, "S") Then
        Do While Not aplRST.EOF
            cboGerencia.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
End Sub

Private Sub cargaSector(sGERE As String)
    cboSector.Clear
    If sp_GetSector("", sGERE, "S") Then
        Do While Not aplRST.EOF
            cboSector.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
End Sub

Private Sub cargaGrupo(sSECT As String)
    cboGrupo.Clear
    If sp_GetGrupo("", sSECT, "S") Then
        Do While Not aplRST.EOF
            cboGrupo.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
End Sub

Private Sub grdDatos_RowColChange()
    If Not inCarga Then
        Call MostrarSeleccion
    End If
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
