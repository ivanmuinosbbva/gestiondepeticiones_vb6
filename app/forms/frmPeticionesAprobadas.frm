VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPeticionesAprobadas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de peticiones que han sido conformadas"
   ClientHeight    =   5700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11115
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesAprobadas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5700
   ScaleWidth      =   11115
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraGeneral 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5415
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11055
      Begin MSComctlLib.ListView lswPeticiones 
         Height          =   4335
         Left            =   120
         TabIndex        =   13
         Top             =   960
         Width           =   10815
         _ExtentX        =   19076
         _ExtentY        =   7646
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.CommandButton cmdTranferir 
         Caption         =   "Transferir"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   310
         Left            =   9600
         TabIndex        =   12
         Top             =   600
         Width           =   1335
      End
      Begin VB.ComboBox cboPeticionEstados 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6000
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   240
         Width           =   2775
      End
      Begin VB.CommandButton cmdAplicarFiltros 
         Caption         =   "Aplicar filtros"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   310
         Left            =   9600
         TabIndex        =   9
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox txtNroHst 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   4
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox txtNroDsd 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin AT_MaskText.MaskText txtFechaDesde 
         Height          =   285
         Left            =   3600
         TabIndex        =   7
         Top             =   240
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   503
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaHasta 
         Height          =   285
         Left            =   3600
         TabIndex        =   8
         Top             =   600
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   503
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   4
         Left            =   5280
         TabIndex        =   10
         Top             =   240
         Width           =   585
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Fecha hasta:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   3
         Left            =   2400
         TabIndex        =   6
         Top             =   600
         Width           =   975
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Fecha desde:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   2
         Left            =   2400
         TabIndex        =   5
         Top             =   240
         Width           =   1005
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "N� hasta:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   720
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "N� desde:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   750
      End
   End
End
Attribute VB_Name = "frmPeticionesAprobadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim oItem As ListItem
Private Const colPET_ASIGNADO As String = "N� Petici�n"
Private Const colTIPO As String = "Tipo"
Private Const colCLASE As String = "Clase"
Private Const colESTADO As String = "Estado"
Private Const colCONFORME As String = "Conforme"
Private Const colFECHA As String = "Dado el"
Private Const colUSUARIO As String = "Usuario"
Private Const colNOMBRE_APELLIDO As String = "Nombre y apellido"
Private Const colSECUENCIA As String = "Secuencia"
Private Const colPET_INTERNO As String = "N� interno"

Private Sub Form_Load()
    Inicializar
End Sub

Private Sub Inicializar()
    FormatearControl
    CargarFiltros
    CargarPeticionesInicial
End Sub

Private Sub FormatearControl()
    With lswPeticiones
        .ListItems.Clear      ' Limpia el ListView
        .ColumnHeaders.Add , , colPET_ASIGNADO, 1000, lvwColumnLeft
        .ColumnHeaders.Add , , colTIPO, 600, lvwColumnLeft
        .ColumnHeaders.Add , , colCLASE, 600, lvwColumnLeft
        .ColumnHeaders.Add , , colESTADO, 1200, lvwColumnLeft
        .ColumnHeaders.Add , , colCONFORME, 800, lvwColumnCenter
        .ColumnHeaders.Add , , colFECHA, 2000, lvwColumnCenter
        .ColumnHeaders.Add , , colUSUARIO, 1000, lvwColumnLeft
        .ColumnHeaders.Add , , colNOMBRE_APELLIDO, 2000, lvwColumnLeft
        .ColumnHeaders.Add , , colSECUENCIA, 800, lvwColumnCenter
        .ColumnHeaders.Add , , colPET_INTERNO, 800, lvwColumnRight
        .Sorted = True
    End With
End Sub

Private Sub CargarFiltros()
    ' Estado de las peticiones
    With cboPeticionEstados
        .Clear
        .AddItem "* Todos"
        If Not sp_GetEstado(Null) Then Exit Sub
        Do While Not aplRST.EOF
            .AddItem Trim(aplRST.Fields!nom_estado) & Space(100) & "||" & Trim(aplRST.Fields!cod_estado)
            aplRST.MoveNext
            DoEvents
        Loop
        aplRST.Close
        .ListIndex = 0      ' Selecciona el primer elemento del combo
    End With
End Sub

Private Sub CargarPeticionesInicial()
    Dim i As Long
    With lswPeticiones
        .visible = False
        .ListItems.Clear      ' Limpia el ListView
        If Not sp_GetPeticionAprobadas(Null, Null, Null, Null, Null) Then Exit Sub
        Do While Not aplRST.EOF
            i = i + 1
            Set oItem = .ListItems.Add(, , Format(aplRST.Fields!pet_nroasignado, "###,###,##0"))
            oItem.SubItems(1) = Trim(aplRST.Fields!cod_tipo_peticion)
            oItem.SubItems(2) = Trim(aplRST.Fields!cod_clase)
            oItem.SubItems(3) = Trim(aplRST.Fields!nom_estado)
            oItem.SubItems(4) = Trim(aplRST.Fields!ok_tipo)
            oItem.SubItems(5) = Trim(Format(aplRST.Fields!audit_date, "dd/mm/yyyy hh:mm:ss"))
            oItem.SubItems(6) = Trim(aplRST.Fields!audit_user)
            If IsNull(aplRST.Fields!nom_recurso) Then
                oItem.SubItems(7) = "*** N/A"
            Else
                oItem.SubItems(7) = Trim(aplRST.Fields!nom_recurso)
            End If
            oItem.SubItems(8) = Trim(aplRST.Fields!ok_secuencia)
            oItem.SubItems(9) = Format(aplRST.Fields!pet_nrointerno, "###,###,##0")
            aplRST.MoveNext
            DoEvents
        Loop
        aplRST.Close
        .visible = True
    End With
'    If i = 1 Then
'        sbPeticiones.SimpleText = Format(i, "###,###,##0") & " registro."
'    Else
'        sbPeticiones.SimpleText = Format(i, "###,###,##0") & " registros."
'    End If
End Sub

Private Sub CargarPeticiones(vNroDsd As Variant, _
                             vNroHst As Variant, _
                             vFechaDesde As Variant, _
                             vFechaHasta As Variant, _
                             vEstado As Variant)
    Dim i As Long
    With lswPeticiones
        .visible = False
        .ListItems.Clear      ' Limpia el ListView
        If vNroDsd = "" Then vNroDsd = Null
        If vNroHst = "" Then vNroHst = Null
        If vEstado = "" Or vEstado = "Todos" Then vEstado = Null
        If vFechaDesde = "" Then
            vFechaDesde = Null
        Else
            vFechaDesde = Format(vFechaDesde, "yyyymmdd")
        End If
        If vFechaHasta = "" Then
            vFechaHasta = Null
        Else
            vFechaHasta = Format(vFechaHasta, "yyyymmdd")
        End If
        If Not sp_GetPeticionAprobadas(vNroDsd, vNroHst, vFechaDesde, vFechaHasta, vEstado) Then Exit Sub
        Do While Not aplRST.EOF
            i = i + 1
            Set oItem = .ListItems.Add(, , Format(aplRST.Fields!pet_nroasignado, "###,###,##0"))
            oItem.SubItems(1) = Trim(aplRST.Fields!cod_tipo_peticion)
            oItem.SubItems(2) = Trim(aplRST.Fields!cod_clase)
            oItem.SubItems(3) = Trim(aplRST.Fields!nom_estado)
            oItem.SubItems(4) = Trim(aplRST.Fields!ok_tipo)
            oItem.SubItems(5) = Trim(Format(aplRST.Fields!audit_date, "dd/mm/yyyy"))
            oItem.SubItems(6) = Trim(aplRST.Fields!audit_user)
            oItem.SubItems(7) = Trim(aplRST.Fields!nom_recurso)
            oItem.SubItems(8) = Trim(aplRST.Fields!ok_secuencia)
            oItem.SubItems(9) = Format(aplRST.Fields!pet_nrointerno, "###,###,##0")
            aplRST.MoveNext
            DoEvents
        Loop
        aplRST.Close
        .visible = True
    End With
'    If i = 1 Then
'        sbPeticiones.SimpleText = Format(i, "###,###,##0") & " registro."
'    Else
'        sbPeticiones.SimpleText = Format(i, "###,###,##0") & " registros."
'    End If
End Sub

Private Sub cmdAplicarFiltros_Click()
    CargarPeticiones txtNroDsd, txtNroHst, txtFechaDesde, txtFechaHasta, CodigoCombo(cboPeticionEstados, True)
End Sub

Private Sub cmdTranferir_Click()
    Dim i As Long
    
    If MsgBox("�Confirma la selecci�n de peticiones?", vbQuestion + vbYesNo, "Selecci�n de peticiones") = vbYes Then
        With lswPeticiones
            For i = 1 To .ListItems.Count
                If .ListItems(i).Selected Then
                    ' Obtengo el sector y grupo de la petici�n
                    Dim sSector, sGrupo As String
                    sSector = ""
                    sGrupo = ""
                    If sp_GetPeticionGrupo(.ListItems(i).SubItems(9), sSector, sGrupo) Then
                        sSector = ClearNull(aplRST.Fields(2))
                        sGrupo = ClearNull(aplRST.Fields(1))
                        ' Aqui agregar SP que los ingrese a PeticionEnviadas
                        Call Habilitar_PasajeProduccion(.ListItems(i).SubItems(9), .ListItems(i).Text, .ListItems(i).SubItems(2), sSector, sGrupo, .ListItems(i).SubItems(6), "M")
                    End If
                End If
            Next i
        End With
        Call Status("Listo.")
        Unload Me
    End If
End Sub

'Private Sub lswPeticiones_ColumnClick(ByVal ColumnHeader As ComctlLib.ColumnHeader)
'    With lswPeticiones
'        If .SortOrder = lvwAscending Then
'            .SortOrder = lvwDescending
'            .SortKey = ColumnHeader.Index - 1
'        Else
'            .SortOrder = lvwAscending
'            .SortKey = ColumnHeader.Index - 1
'       End If
'    End With
'End Sub
