VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form auxRecActuantes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Posibles recursos actuantes"
   ClientHeight    =   5985
   ClientLeft      =   1140
   ClientTop       =   330
   ClientWidth     =   5025
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   5025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   60
      TabIndex        =   3
      Top             =   0
      Width           =   4935
      Begin VB.Label lblPerfil 
         Caption         =   "En caso de confirmar el cambio de estado, los siguientes recursos podr�n actuar sobre la Petici�n."
         ForeColor       =   &H00800000&
         Height          =   810
         Left            =   60
         TabIndex        =   4
         Top             =   180
         Width           =   4785
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   60
      TabIndex        =   0
      Top             =   5220
      Width           =   4935
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   3960
         TabIndex        =   1
         Top             =   300
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4110
      Left            =   60
      TabIndex        =   2
      Top             =   1080
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   7250
      _Version        =   393216
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "auxRecActuantes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 26.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Const colNomPerfil = 0
Const colNOMRECURSO = 1
Public pubNroPeticion As Long
Public pubCodEstado As String

Private Sub Form_Load()
    lblPerfil.Caption = "En caso de confirmar el cambio de estado, los siguientes recursos podr�n actuar sobre la Petici�n." & Chr$(13) & "Ud. no debe seleccionar un recurso de esta lista, la cual es s�lo informativa."
    Call IniciarScroll(grdDatos)       ' add -003- a.
End Sub

Sub CargarGrid()
    Call Puntero(True)
    Call Status("Cargando Recursos")
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colNomPerfil) = "Perfil"
        .TextMatrix(0, colNOMRECURSO) = "Recurso"
        .ColWidth(colNomPerfil) = 2100: .ColAlignment(colNomPerfil) = 0
        .ColWidth(colNOMRECURSO) = 2600: .ColAlignment(colNOMRECURSO) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetRecursoActuantePeticion(pubNroPeticion, pubCodEstado) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNomPerfil) = IIf(Not IsNull(aplRST.Fields.Item("nom_perfil")), aplRST.Fields.Item("nom_perfil"), "")
            .TextMatrix(.Rows - 1, colNOMRECURSO) = IIf(Not IsNull(aplRST.Fields.Item("nom_recurso")), aplRST.Fields.Item("nom_recurso"), "")
        End With
        aplRST.MoveNext
    Loop
    Call Puntero(False)
    Call Status("Listo")
End Sub

Public Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
