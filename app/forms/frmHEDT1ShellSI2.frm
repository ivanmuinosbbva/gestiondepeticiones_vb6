VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmHEDT1ShellSI2 
   Caption         =   "Form1"
   ClientHeight    =   7500
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11100
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7500
   ScaleWidth      =   11100
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraFiltros 
      Caption         =   "Filtros"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   9375
   End
   Begin VB.Frame fraBotonera 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7335
      Left            =   9600
      TabIndex        =   1
      Top             =   120
      Width           =   1455
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "&Agregar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   10
         ToolTipText     =   "Agregar un nuevo archivo"
         Top             =   5280
         Width           =   1290
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Mo&dificar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   9
         ToolTipText     =   "Modificar el archivo seleccionado"
         Top             =   5760
         Width           =   1290
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirm&ar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   8
         ToolTipText     =   "Confirma la operaci�n actual"
         Top             =   6240
         Width           =   1290
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "&Cerrar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   7
         ToolTipText     =   "Salir de esta pantalla"
         Top             =   6720
         Width           =   1290
      End
      Begin VB.CommandButton cmdCOPYS 
         Caption         =   "COPYs"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   6
         ToolTipText     =   "Accede a la pantalla para definir los Copys del DSN seleccionado"
         Top             =   160
         Width           =   1290
      End
      Begin VB.CheckBox chkSoloPendientes 
         Caption         =   "Solo pendientes y rechazados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   120
         TabIndex        =   5
         ToolTipText     =   "Con esta opci�n seleccionada, solo se ver�n en la grilla los archivos pendientes de visado"
         Top             =   2160
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.CheckBox chkBuscar 
         Caption         =   "B&uscar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Despliega un buscador secuencial de nombres de DSN que filtra la grilla a medida que escribe"
         Top             =   640
         Width           =   1290
      End
      Begin VB.CommandButton cmdCopiar 
         Caption         =   "Copiar nombres"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   3
         ToolTipText     =   "Puede copiar en el Portapapeles los nombres de los archivos seleccionados para pegar donde necesite"
         Top             =   1130
         Width           =   1290
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Informe"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   2
         Top             =   1610
         Width           =   1290
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5535
      Left            =   120
      TabIndex        =   0
      Top             =   1920
      Width           =   9435
      _ExtentX        =   16642
      _ExtentY        =   9763
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmHEDT1ShellSI2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private lUltimaColumnaOrdenada As Integer

Private Sub Form_Load()
    Inicializar_Columnas
End Sub

Private Sub Inicializar_Columnas()
    With grdDatos
        .Visible = False
        .Font.name = "Tahoma"
        .Font.Size = 8
'        .Cols = col_TOTALCOLS
'        .TextMatrix(0, colMultiSelect) = "": .ColWidth(colMultiSelect) = 150
'        .TextMatrix(0, col_dsn_id) = "C�digo": .ColWidth(col_dsn_id) = 1000
'        .TextMatrix(0, col_dsn_nom) = "Nombre del archivo": .ColWidth(col_dsn_nom) = 4000
'        .TextMatrix(0, col_dsn_enmas) = "Datos S.": .ColWidth(col_dsn_enmas) = 800
'        .TextMatrix(0, col_dsn_nomrut) = "Rutina": .ColWidth(col_dsn_nomrut) = 1000
'        .TextMatrix(0, col_dsn_feult) = "Ult. modificaci�n": .ColWidth(col_dsn_feult) = 1700
'        .TextMatrix(0, col_nom_usuario) = "Propietario": .ColWidth(col_nom_usuario) = 1500
'        .TextMatrix(0, col_dsn_vb) = "Visado": .ColWidth(col_dsn_vb) = 1000
'        .TextMatrix(0, col_nom_vb_usuario) = "Por": .ColWidth(col_nom_vb_usuario) = 1200
'        .TextMatrix(0, col_dsn_vb_fe) = "Fch. Visado": .ColWidth(col_dsn_vb_fe) = 1700
'        .TextMatrix(0, col_app_id) = "Aplicativo": .ColWidth(col_app_id) = 1000
'        .TextMatrix(0, col_app_name) = "Nombre del aplicativo": .ColWidth(col_app_name) = 2000
'        .TextMatrix(0, col_dsn_desc) = "Descripci�n del archivo": .ColWidth(col_dsn_desc) = 4000
'        .TextMatrix(0, col_nom_estado) = "Estado": .ColWidth(col_nom_estado) = 1600
'        .TextMatrix(0, col_Estado) = "": .ColWidth(col_Estado) = 0
'        .TextMatrix(0, col_valido) = "Validez": .ColWidth(col_valido) = 800
'        .TextMatrix(0, col_dsn_vb_id) = "dsn_vb": .ColWidth(col_dsn_vb_id) = 0
'        .TextMatrix(0, col_dsn_txt) = "Motivo de rechazo": .ColWidth(col_dsn_txt) = 2000
'        .TextMatrix(0, col_dsn_nomprod) = "Nombre productivo": .ColWidth(col_dsn_nomprod) = 3000
'        .TextMatrix(0, col_dsn_userid) = "dsn_userid": .ColWidth(col_dsn_userid) = 0
    End With
End Sub

Private Sub grdDatos_Click()
'    bSorting = True
'    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
'    bSorting = False
End Sub
