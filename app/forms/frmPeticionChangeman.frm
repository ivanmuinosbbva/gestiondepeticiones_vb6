VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionChangeman 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Peticiones enviadas al archivo de exportaci�n a Changeman"
   ClientHeight    =   7485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10560
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmPeticionChangeman.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7485
   ScaleWidth      =   10560
   Begin VB.Frame fraComandos2 
      Height          =   3375
      Left            =   8400
      TabIndex        =   5
      Top             =   3840
      Width           =   2055
      Begin VB.CommandButton cmdEliminar_Seleccion 
         Caption         =   "Eliminar selecci�n"
         Height          =   375
         Left            =   120
         TabIndex        =   16
         ToolTipText     =   "Elimina el registro f�sico de la bandeja diaria (PeticionChangeMan) y del hist�rico (PeticionEnviadas)"
         Top             =   960
         Width           =   1815
      End
      Begin VB.CommandButton cmdConfig 
         Caption         =   "Configuraci�n..."
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   2880
         Width           =   1815
      End
      Begin VB.CommandButton cmdVaciar 
         Caption         =   "Vaciar bandeja"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   600
         Width           =   1815
      End
      Begin VB.CommandButton cmdForzar 
         Caption         =   "Forzar hist�rico"
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame fraComandos 
      Height          =   3615
      Left            =   8400
      TabIndex        =   1
      Top             =   120
      Width           =   2055
      Begin VB.CommandButton cmdFormato 
         Caption         =   "..."
         Height          =   255
         Left            =   1560
         TabIndex        =   11
         Top             =   3240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar peticiones"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   1815
      End
      Begin VB.CommandButton cmdIncluir 
         Caption         =   "Incluir peticiones ..."
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Eliminar peticiones le permite quitar peticiones de manera permanente del hist�rico (son eliminadas tambi�n de la bandeja diaria)"
         ForeColor       =   &H00C1841C&
         Height          =   1305
         Left            =   120
         TabIndex        =   9
         Top             =   2040
         Width           =   1815
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Incluir peticiones le permite seleccionar peticiones de manera manual y arbitraria."
         ForeColor       =   &H00C1841C&
         Height          =   780
         Left            =   120
         TabIndex        =   8
         Top             =   1080
         Width           =   1770
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraPeticiones 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C1841C&
      Height          =   7095
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   8295
      Begin VB.ComboBox cmbVer 
         Height          =   300
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   280
         Width           =   2415
      End
      Begin MSFlexGridLib.MSFlexGrid grdDatosHoy 
         Height          =   6210
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   8055
         _ExtentX        =   14208
         _ExtentY        =   10954
         _Version        =   393216
         FixedCols       =   0
         BackColorSel    =   12682268
         ForeColorSel    =   16777215
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblOrdenDsc 
         AutoSize        =   -1  'True
         Caption         =   "� : Descendente"
         Height          =   180
         Left            =   6960
         TabIndex        =   15
         Top             =   440
         Width           =   1230
      End
      Begin VB.Label lblOrdenAsc 
         AutoSize        =   -1  'True
         Caption         =   "� : Ascendente"
         Height          =   180
         Left            =   6960
         TabIndex        =   14
         Top             =   240
         Width           =   1140
      End
      Begin VB.Label lblFraTitulo 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   8130
         TabIndex        =   12
         Top             =   0
         Width           =   45
      End
   End
End
Attribute VB_Name = "frmPeticionChangeman"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -002- a. FJS 22.02.2010 - Se agregan efectos para mejorar la legibilidad de la pantalla.
' -003- a. FJS 24.02.2010 - Se modific� el SP para soportar el pedido de las vigentes para pasajes y las hist�ricas que vencieron.
' -004- a. FJS 07.04.2010 - Se agrega la posibilidad de poder acceder a la petici�n haciendo dobre click sobre la grilla.

Option Explicit

Dim lCantidadDiasAlVencimiento As Long
Dim bLoading As Boolean
Dim lUltimaColumnaOrdenada As Integer

Private Sub Form_Load()
    Inicializar
End Sub

Private Sub Inicializar()
    bLoading = True
    Me.Top = 0
    Me.Left = 0
    '{ add -002- a.
    Call Status("Inicializando...")
    Screen.MousePointer = vbHourglass
    '}
    With cmbVer
        .Clear
        .AddItem "Vigentes a fecha"
        .AddItem "Hist�rico"
        .ListIndex = 0
    End With
    bLoading = True
    
    ' Agrega los tooltips a los botones
    cmdEliminar.ToolTipText = "Elimina una petici�n del hist�rico de enviadas"
    cmdForzar.ToolTipText = "Fuerza la bajada desde el hist�rico a la bandeja diaria de peticiones"
    cmdVaciar.ToolTipText = "Vacia la bandeja eliminando todas las peticiones del d�a"
    
    Inicializar_Parametros
    CargarGrid_Hoy
    modUser32.IniciarScroll grdDatosHoy     ' add -001- a.
    '{ add -002- a.
    Screen.MousePointer = vbNormal
    bLoading = False
    Call Status("Listo.")
    '}
End Sub

Private Sub Inicializar_Parametros()
    If sp_GetVarios("CGMPRM1") Then
        lCantidadDiasAlVencimiento = ClearNull(aplRST.Fields!var_numero)
    End If
End Sub

'{ add -003- a.
Private Sub Inicializar_Grilla()
    ' Define y formatea las columnas
    With grdDatosHoy
        .visible = False
        .Clear
        .cols = 11
        .Rows = 1
        .TextMatrix(0, 0) = "N� Petici�n"
        .TextMatrix(0, 1) = "T�tulo"
        .TextMatrix(0, 2) = "Tipo"
        .TextMatrix(0, 3) = "Clase"
        .TextMatrix(0, 4) = "Estado"
        .TextMatrix(0, 5) = "Fecha Est."
        .TextMatrix(0, 6) = "Sector"
        .TextMatrix(0, 7) = "Grupo"
        .TextMatrix(0, 8) = "N� Interno"
        .TextMatrix(0, 9) = "Estado"
        .TextMatrix(0, 10) = "Caduca en"
                
        .ColWidth(0) = 1300: .ColAlignment(0) = 6
        .ColWidth(1) = 4000: .ColAlignment(1) = 0
        .ColWidth(2) = 1000: .ColAlignment(2) = 0
        .ColWidth(3) = 1000: .ColAlignment(3) = 0
        .ColWidth(4) = 2000: .ColAlignment(4) = 0
        .ColWidth(5) = 1800: .ColAlignment(5) = 0
        .ColWidth(6) = 1200: .ColAlignment(6) = 0
        .ColWidth(7) = 1200: .ColAlignment(7) = 0
        .ColWidth(8) = 1200: .ColAlignment(8) = 6
        .ColWidth(9) = 1200: .ColAlignment(9) = 6
        .ColWidth(10) = 1200: .ColAlignment(10) = 6
    End With
End Sub
'}

Private Sub CargarGrid_Historico()
    Dim x As Long
    
    fraPeticiones.Caption = " Peticiones vencidas en el hist�rico "
    Inicializar_Grilla
    'If Not sp_GetPeticionEnviadas2(Null, Null, Null) Then Exit Sub         ' del -003- a.
    If Not sp_GetPeticionChangeMan2("2", Null, Null, Null) Then Exit Sub    ' add -003-
    '{ add -003- a.
    Do While Not aplRST.EOF
        With grdDatosHoy
            x = x + 1
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!cod_clase)
            .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, 5) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "dd/mm/yyyy hh:mm"), "")
            .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST!pet_sector)
            .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST!pet_grupo)
            .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, 9) = ClearNull(aplRST!cod_estado)
        End With
        aplRST.MoveNext
    Loop
    '}
    
    '{ del -003- a.
    'Do While Not aplRST.EOF
    '    x = x + 1
    '    With grdDatosHoy
    '        .Rows = .Rows + 1
    '        .TextMatrix(.Rows - 1, 0) = Format(ClearNull(aplRST!pet_nroasignado), "###,###,##0")
    '        .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST!titulo)
    '        .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!cod_tipo_peticion)
    '        .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!cod_clase)
    '        .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST!nom_estado)
    '        .TextMatrix(.Rows - 1, 5) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "dd/mm/yyyy hh:mm:ss"), "")
    '        .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST!pet_sector)
    '        .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST!pet_grupo)
    '        .TextMatrix(.Rows - 1, 8) = IIf(Not IsNull(aplRST!pet_date), Format(aplRST!pet_date, "dd/mm/yyyy hh:mm:ss"), "")
    '        .TextMatrix(.Rows - 1, 9) = ClearNull(aplRST!pet_usrid)
    '        .TextMatrix(.Rows - 1, 10) = ClearNull(aplRST!nom_recurso)
    '        .TextMatrix(.Rows - 1, 11) = Format(ClearNull(aplRST!pet_nrointerno), "###,###,##0")
    '        .TextMatrix(.Rows - 1, 12) = Format(ClearNull(aplRST!pet_record))
    '    End With
    '    aplRST.MoveNext
    'Loop
    '}
    aplRST.Close
    lblFraTitulo.Caption = " " & Format(x, "###,###,##0") & " "
    
    Dim i As Long
    With grdDatosHoy
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .FocusRect = flexFocusNone
        .row = 0
        For i = 0 To .cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        If .Rows > 1 Then
            .row = 1
            .ColSel = 0
            .RowSel = 1
        End If
        Call Status("Listo.")
        .visible = True
    End With
End Sub

Private Sub CargarGrid_Hoy()
    Dim x As Long
    
    fraPeticiones.Caption = " Peticiones v�lidas a fecha de hoy para pasajes a Producci�n "
    Inicializar_Grilla
    
    If Not sp_GetPeticionChangeMan2("1", Null, Null, Null) Then Exit Sub    ' upd -003- a.
    
    Do While Not aplRST.EOF
        With grdDatosHoy
            x = x + 1
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!cod_clase)
            .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, 5) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "dd/mm/yyyy hh:mm"), "")
            .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST!pet_sector)
            .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST!pet_grupo)
            .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, 9) = ClearNull(aplRST!cod_estado)
            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST!cod_estado), vbTextCompare) > 0 Then
                If DateDiff("d", CDate(aplRST.Fields!fe_estado), date, vbSunday) > 0 Then
                    .TextMatrix(.Rows - 1, 10) = Format(lCantidadDiasAlVencimiento - DateDiff("d", CDate(aplRST.Fields!fe_estado), date, vbSunday), "###,###,##0")
                Else
                    .TextMatrix(.Rows - 1, 10) = Format(lCantidadDiasAlVencimiento - DateDiff("d", CDate(aplRST.Fields!fe_estado), date, vbSunday), "###,###,##0")
                End If
                'PintarLinea grdDatosHoy, prmGridFillRowColorRose
            Else
                .TextMatrix(.Rows - 1, 10) = "Activa"
            End If
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    lblFraTitulo.Caption = " " & Format(x, "###,###,##0") & " "
    
    Dim i As Long
    With grdDatosHoy
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .FocusRect = flexFocusNone
        .row = 0
        For i = 0 To .cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        If .Rows > 1 Then
            .row = 1
            .ColSel = 0
            .RowSel = 1
        End If
        Call Status("Listo.")
        .visible = True
    End With
End Sub

Private Sub cmdEliminar_Click()
    Dim Desde_Pos As Long
    Dim Hasta_Pos As Long
    Dim Filas As Long
    Dim i As Long
    
    If MsgBox("�Confirma la eliminaci�n permanente de las peticiones del hist�rico de aprobaciones?", vbQuestion + vbYesNo) = vbYes Then
        With grdDatosHoy
            .Enabled = False
            ' Establezco los l�mites de la selecci�n
            If .row >= .RowSel Then
                Desde_Pos = .RowSel
                Hasta_Pos = .row
            End If
            If .RowSel >= .row Then
                Desde_Pos = .row
                Hasta_Pos = .RowSel
            End If
            Filas = (Hasta_Pos - Desde_Pos) + 1
            .row = Desde_Pos
            For i = 1 To Filas
                If Not sp_DeletePeticionEnviadas(CLng(.TextMatrix(.row, 11))) Then
                    MsgBox "No puede eliminarse la petici�n n� " & Format(.TextMatrix(.row, 11), "###,###,##0"), vbCritical + vbOKOnly, "Problemas al eliminar hist�rico"
                End If
                If Not .Rows - .row = 1 Then
                    .row = .row + 1
                End If
            Next i
            MsgBox "Eliminaci�n finalizada con �xito.", vbInformation + vbOKOnly, "Eliminaci�n"
            .Enabled = True
        End With
        CargarGrid_Historico
        CargarGrid_Hoy
    End If
End Sub

Private Sub cmdVaciar_Click()
    If MsgBox("�Confirma la eliminaci�n permanente de las peticiones de la bandeja diaria?", vbQuestion + vbYesNo) = vbYes Then
        If Not sp_DeletePeticionChangeMan(Null) Then
            MsgBox "No puede vaciarse la bandeja diaria", vbCritical + vbOKOnly, "Problemas al vaciar bandeja diaria"
        End If
        CargarGrid_Hoy
    End If
End Sub

Private Sub cmdForzar_Click()
    If MsgBox("�Confirma el vuelco completo de las peticiones del hist�rico?", vbQuestion + vbYesNo, "Forzar hist�rico") = vbYes Then
        If Not sp_InsertPetChangeMan2(Null) Then
            MsgBox "No puede forzarse el vuelco hist�rico a la bandeja diaria", vbCritical + vbOKOnly, "Problemas al forzar el hist�rico"
        End If
        CargarGrid_Hoy
    End If
End Sub

Private Sub cmdIncluir_Click()
    Load frmPeticionesAprobadas
    frmPeticionesAprobadas.Show 1
    CargarGrid_Historico
    CargarGrid_Hoy
End Sub

Private Sub cmdConfig_Click()
    Load frmPeticionChangemanOptions
    frmPeticionChangemanOptions.Show 1
End Sub

Private Sub cmdFormato_Click()
    Dim i As Long
    Dim NumeroPeticion, NroAsignado, PETClass, Usuario, sSector, sGrupo
    
    With grdDatosHoy
        .Enabled = False
        For i = 1 To .Rows
            Call Status("Actualizando peticiones... " & i & " de " & .Rows)
            If sp_GetUnaPeticion(CLng(grdDatosHoy.TextMatrix(grdDatosHoy.row, 11))) Then
                NumeroPeticion = aplRST!pet_nrointerno
                NroAsignado = aplRST!pet_nroasignado
                PETClass = aplRST!cod_clase
                Usuario = "VUELCO"
            End If
            
            sSector = ""
            sGrupo = ""
            If Not sp_GetPeticionGrupo(NumeroPeticion, sSector, sGrupo) Then
                If Not aplRST.EOF Then
                    sSector = ClearNull(aplRST.Fields(2))
                    sGrupo = ClearNull(aplRST.Fields(1))
                Else
                    sSector = "SECTOR"
                    sGrupo = "GRUPO"
                End If
            Else
                sSector = "SECTOR"
                sGrupo = "GRUPO"
            End If
            If Not sp_DeletePeticionEnviadas(NumeroPeticion) Then
                MsgBox "No puede eliminarse la petici�n n� " & Format(NroAsignado, "###,###,##0"), vbCritical + vbOKOnly, "Problemas al eliminar hist�rico"
            End If
            'FuncionesHOST.GesPetExportToHOST NumeroPeticion, NroAsignado, PETClass, sSector, sGrupo, Usuario, "02"
            Call Habilitar_PasajeProduccion(NumeroPeticion, NroAsignado, PETClass, sSector, sGrupo, Usuario, "02")
            If Not .Rows - .row = 1 Then
                .row = .row + 1
            End If
        Next i
        .Enabled = True
    End With
    Call Status("Listo.")
    MsgBox "Proceso de formateo finalizado.", vbOKOnly + vbInformation, "Formateo autom�tico"
    CargarGrid_Historico
End Sub

Private Sub grdDatosHoy_Click()
    With grdDatosHoy
        If .MouseRow = 0 And .Rows > 1 Then
            .visible = False
            .Col = .MouseCol
            If IsNumeric(.TextMatrix(1, .Col)) Then
                If Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortNumericDescending
                ElseIf Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortNumericAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortNumericDescending
                End If
            Else
                If Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortStringNoCaseDescending
                ElseIf Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortStringNoCaseAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortStringNoCaseDescending
                End If
            End If
            lUltimaColumnaOrdenada = .Col
            .visible = True
        Else
            If Not bLoading Then
                'Call MostrarSeleccion
            End If
        End If
    End With
End Sub

''{ add -004- a.
'Private Sub grdDatosHoy_DblClick()
'    With grdDatosHoy
'        DoEvents
'        glNumeroPeticion = .TextMatrix(.RowSel, 8)
'        If glNumeroPeticion <> "" Then
'            If Not LockProceso(True) Then
'                Exit Sub
'            End If
'            glModoPeticion = "VIEW"
'            'frmPeticionesCarga.Show vbModal
'            DoEvents
'            .SetFocus
'        End If
'    End With
'    DoEvents
'End Sub
''}

'{ add -001- a.
Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatosHoy
End Sub
'}

'{ add -003- a.
Private Sub cmbVer_Click()
    bLoading = True
    Screen.MousePointer = vbHourglass
    If Left(cmbVer.List(cmbVer.ListIndex), 1) = "H" Then
        CargarGrid_Historico
    Else
        CargarGrid_Hoy
    End If
    Screen.MousePointer = vbNormal
    bLoading = False
End Sub

'Private Sub grdDatosHoy_Click()
'    If Not bLoading Then
'        MostrarSeleccion
'    End If
'End Sub

'Private Sub grdDatosHoy_RowColChange()
'Private Sub grdDatosHoy_SelChange()
'    If Not bLoading Then
'        MostrarSeleccion
'    End If
'End Sub
'
'Sub MostrarSeleccion()
'    Dim nPos As Integer
'    With grdDatosHoy
'        sbPeticiones.SimpleText = sp_GetMemo(.TextMatrix(.RowSel, 8), "PETVALIDA")
'    End With
'    DoEvents
'End Sub
'}

Private Sub cmdEliminar_Seleccion_Click()
    If MsgBox("Confirma la eliminaci�n de la petici�n seleccionada de la bandeja diaria y el hist�rico?", vbQuestion + vbYesNo, "Eliminar validez de petici�n para Pasajes a Producci�n") = vbYes Then
        Call Puntero(True)
        Call Status("Eliminando... aguarde...")
        With grdDatosHoy
            If Not sp_DeletePeticionEnviadas(.TextMatrix(.RowSel, 8)) Then
                MsgBox "Error al intentar borrar."
            Else
                CargarGrid_Hoy
            End If
        End With
        Call Status("Listo.")
        Call Puntero(False)
    End If
End Sub

Private Sub pepe()
'    Dim cAux As String
    Dim cMensaje As String
'    Dim pet_nroasignado() As Long
'    Dim pet_nrointerno() As Long
'    Dim nFile As Integer
    Dim i As Long, x As Long

    Screen.MousePointer = vbHourglass

    With grdDatosHoy
        For i = 1 To .Rows - 1
            Call Status("Actualizando " & i + 1 & " de " & .Rows)
            If InStr(1, "REVISA", .TextMatrix(i, 9), vbTextCompare) > 0 Then
                If Not sp_UpdateEstadoPeticion(.TextMatrix(i, 8)) Then
                    MsgBox "Error al procesar la " & .TextMatrix(i, 0)
                Else
                    cMensaje = cMensaje & .TextMatrix(i, 0) & vbCrLf
                End If
            End If
        Next i
    End With
    Call Status("Listo.")
    
'    ' Abro un archivo para leer todas sus lineas
'    nFile = FreeFile
'    Open App.Path & "\salida.txt" For Input As #nFile
'        Do While Not EOF(nFile)
'            Input #nFile, cAux
'            If IsNumeric(cAux) Then
'                ReDim Preserve pet_nroasignado(i)
'                pet_nroasignado(i) = CLng(cAux)
'                i = i + 1
'            End If
'            DoEvents
'        Loop
'    Close #nFile
'
'    ' Obtengo los n�meros internos
'    For i = 0 To UBound(pet_nroasignado)
'        If sp_GetUnaPeticionAsig(pet_nroasignado(i)) Then
'            ReDim Preserve pet_nrointerno(x)
'            pet_nrointerno(x) = ClearNull(aplRST.Fields!pet_nrointerno)
'            x = x + 1
'        End If
'    Next i
'
'    For x = 0 To UBound(pet_nrointerno)
'        Call Status("Actualizando " & x + 1 & " de " & UBound(pet_nrointerno))
'
'        If Not sp_GetPeticionChangeMan(pet_nrointerno(x), Null, Null) Then
'            cMensaje = cMensaje & pet_nrointerno(x) & vbCrLf
'        End If
'        'If Not sp_DeletePeticionChangeMan(pet_nrointerno(x)) Then
'        '    MsgBox "Error al intentar actualizar " & pet_nroasignado(x)
'        'End If
'        'If sp_GetPeticionGrupoXt(pet_nrointerno(x), Null, Null, Null) Then
'        '    Do While Not aplRST.EOF
'        '        Call Status("Analizando " & x + 1 & " de " & UBound(pet_nrointerno))
'        '        If ClearNull(aplRST.Fields!cod_gerencia) = "DESA" Then
'        '            cMensaje = cMensaje & pet_nrointerno(x) & vbCrLf
'        '        End If
'        '        aplRST.MoveNext
'        '        DoEvents
'        '    Loop
'        'End If
'    Next x
'    MsgBox cMensaje
    Screen.MousePointer = vbNormal
    'MsgBox "Proceso finalizado"
    MsgBox cMensaje
End Sub

'Private Sub grdDatos_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'    With grdDatos
'        Text1.Text = "MouseRow: " & .MouseRow & " / MouseCol: " & .MouseCol
'    End With
'End Sub


