VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmVarios 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tabla: Varios"
   ClientHeight    =   7140
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8790
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   8790
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraDatos 
      Height          =   1455
      Left            =   120
      TabIndex        =   10
      Top             =   5640
      Width           =   7215
      Begin VB.TextBox txtNumero 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3720
         TabIndex        =   1
         Top             =   240
         Width           =   1215
      End
      Begin VB.TextBox txtTexto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         MaxLength       =   30
         TabIndex        =   2
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox txtCodigo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         MaxLength       =   8
         TabIndex        =   0
         Top             =   240
         Width           =   1455
      End
      Begin AT_MaskText.MaskText txtFecha 
         Height          =   315
         Left            =   720
         TabIndex        =   3
         Top             =   960
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblModo 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "lblModo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00D28D55&
         Height          =   195
         Left            =   6360
         TabIndex        =   16
         Top             =   0
         Width           =   750
      End
      Begin VB.Label lblVarios 
         AutoSize        =   -1  'True
         Caption         =   "Fecha"
         Height          =   180
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   960
         Width           =   450
      End
      Begin VB.Label lblVarios 
         AutoSize        =   -1  'True
         Caption         =   "N�mero"
         Height          =   180
         Index           =   2
         Left            =   2880
         TabIndex        =   13
         Top             =   240
         Width           =   585
      End
      Begin VB.Label lblVarios 
         AutoSize        =   -1  'True
         Caption         =   "Texto"
         Height          =   180
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   435
      End
      Begin VB.Label lblVarios 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   525
      End
   End
   Begin VB.Frame pnlBotones 
      Height          =   7075
      Left            =   7440
      TabIndex        =   4
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5940
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   6450
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la tarea seleccionada"
         Top             =   5430
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Height          =   500
         Left            =   60
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la tarea selecionada"
         Top             =   4920
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva tarea"
         Top             =   4410
         Width           =   1170
      End
      Begin VB.Label lblAdvertencia 
         Alignment       =   2  'Center
         Caption         =   "Antes de realizar una  modificaci�n a los valores de esta tabla, consulte con D&&D."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   2655
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   975
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5565
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   9816
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmVarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'GMT01 - 24/10/2019 - PET 70881 SE PONE LOGICO PARA HABILITAR LO QUE CORRESPONDA
Option Explicit

Private Const colVARIOS_CODIGO As Integer = 0
Private Const colVARIOS_NUMERO As Integer = 1
Private Const colVARIOS_TEXTO As Integer = 2
Private Const colVARIOS_FECHA As Integer = 3
Private sOpcionSeleccionada As String

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    lblAdvertencia = "Advertencia" & vbCrLf & vbCrLf & "Antes de realizar una  modificaci�n a los valores de esta tabla, consulte con D&&D."
    Call CargarGrilla
    Call HabilitarBotones(0)
    Call IniciarScroll(grdDatos)
End Sub

Private Sub Inicializar()
    With grdDatos
        .Clear
        .cols = 4
        .Rows = 1
        .TextMatrix(0, colVARIOS_CODIGO) = "C�digo"
        .TextMatrix(0, colVARIOS_NUMERO) = "N�mero"
        .TextMatrix(0, colVARIOS_TEXTO) = "Descripci�n"
        .TextMatrix(0, colVARIOS_FECHA) = "Fecha"
        .ColWidth(colVARIOS_CODIGO) = 1200: .ColAlignment(colVARIOS_CODIGO) = 0
        .ColWidth(colVARIOS_NUMERO) = 1200: .ColAlignment(colVARIOS_NUMERO) = 0
        .ColWidth(colVARIOS_TEXTO) = 2500: .ColAlignment(colVARIOS_TEXTO) = 0
        .ColWidth(colVARIOS_FECHA) = 1800: .ColAlignment(colVARIOS_FECHA) = 0
    End With
End Sub

Private Sub CargarGrilla()
    Dim I As Long
    
    Inicializar
    If Not sp_GetVarios(Null) Then Exit Sub
    Call Status(aplRST.RecordCount & " registro(s).")
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colVARIOS_CODIGO) = IIf(Not IsNull(aplRST.Fields.Item("var_codigo")), aplRST.Fields.Item("var_codigo"), "")
            .TextMatrix(.Rows - 1, colVARIOS_NUMERO) = IIf(Not IsNull(aplRST.Fields.Item("var_numero")), aplRST.Fields.Item("var_numero"), "")
            .TextMatrix(.Rows - 1, colVARIOS_TEXTO) = IIf(Not IsNull(aplRST.Fields.Item("var_texto")), aplRST.Fields.Item("var_texto"), "")
            .TextMatrix(.Rows - 1, colVARIOS_FECHA) = IIf(Not IsNull(aplRST.Fields.Item("var_fecha")), aplRST.Fields.Item("var_fecha"), "")
        End With
        aplRST.MoveNext
    Loop
    With grdDatos
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .row = 0
        For I = 0 To .cols - 1
            .Col = I
            .CellFontBold = True
        Next I
        .FocusRect = flexFocusNone
    End With
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, colVARIOS_CODIGO) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colVARIOS_CODIGO))
                txtNumero = ClearNull(.TextMatrix(.RowSel, colVARIOS_NUMERO))
                txtTexto = ClearNull(.TextMatrix(.RowSel, colVARIOS_TEXTO))
                txtFecha = ClearNull(.TextMatrix(.RowSel, colVARIOS_FECHA))
            End If
        End If
    End With
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblModo = "": lblModo.visible = False
            Call setHabilCtrl(txtCodigo, "DIS")
            Call setHabilCtrl(txtNumero, "DIS")
            Call setHabilCtrl(txtTexto, "DIS")
            Call setHabilCtrl(txtFecha, "DIS")
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    Call setHabilCtrl(txtCodigo, "NOR")
                    Call setHabilCtrl(txtNumero, "NOR")
                    Call setHabilCtrl(txtTexto, "NOR")
                    Call setHabilCtrl(txtFecha, "NOR")
                    lblModo = " AGREGAR ": lblModo.visible = True
                    txtCodigo = ""
                    txtTexto = ""
                    txtNumero = ""
                    txtFecha = ""
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    Call setHabilCtrl(txtCodigo, "DIS")
                    Call setHabilCtrl(txtNumero, "NOR")
                    Call setHabilCtrl(txtTexto, "NOR")
                    Call setHabilCtrl(txtFecha, "NOR")
                    lblModo = " MODIFICAR ": lblModo.visible = True
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtTexto.SetFocus
                Case "E"
                    lblModo = " ELIMINAR ": lblModo.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    'Call LockProceso(False)
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "B"
    Call HabilitarBotones(1)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1)
End Sub

Private Sub cmdConfirmar_Click()
    If CamposObligatorios Then
        Select Case sOpcionSeleccionada
            Case "A"
                Call sp_UpdateVarios(txtCodigo, txtNumero, txtTexto, IIf(IsDate(txtFecha.DateValue), txtFecha, Null))
                Call HabilitarBotones(0)
            Case "M"
                If logicaPorCodigo(txtCodigo, txtNumero) Then 'GMT01
                  Call sp_UpdateVarios(txtCodigo, txtNumero, txtTexto, IIf(IsDate(txtFecha.DateValue), txtFecha, Null))
                  With grdDatos
                      .TextMatrix(.RowSel, colVARIOS_CODIGO) = ClearNull(txtCodigo)
                      .TextMatrix(.RowSel, colVARIOS_NUMERO) = ClearNull(txtNumero)
                      .TextMatrix(.RowSel, colVARIOS_TEXTO) = ClearNull(txtTexto)
                      .TextMatrix(.RowSel, colVARIOS_FECHA) = ClearNull(txtFecha)
                  End With

                  Call HabilitarBotones(0, Null)
                End If 'GMT01
            Case "B"
                Call sp_DeleteVarios(txtCodigo)
                Call HabilitarBotones(0)
        End Select
    End If
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If txtCodigo = "" Then
        MsgBox "Debe completar el campo C�digo", vbExclamation + vbOKOnly, "Error en c�digo"
        CamposObligatorios = False
        Exit Function
    End If
    If txtNumero <> "" Then
        If Not IsNumeric(txtNumero) Then
            MsgBox "Debe completar el campo n�mero", vbExclamation + vbOKOnly, "Error en n�mero"
            CamposObligatorios = False
            Exit Function
        End If
    End If
End Function

Private Sub cmdCerrar_Click()
    If cmdCerrar.Caption = "CERRAR" Then
        Call Status("Listo.")
        Unload Me
    Else
        Call HabilitarBotones(0, Null)
        Exit Sub
    End If
    Call DetenerScroll(grdDatos)
End Sub
'GMT01 - INI
Private Function logicaPorCodigo(Codigo, txtNumero) As Boolean
    Dim habilitar As String
   
    logicaPorCodigo = True
      

    Select Case Codigo
       Case "HAB_RGyP"
            Select Case txtNumero
            Case 0
               habilitar = "N"
            Case 1
               habilitar = "S"
            Case Else
                 MsgBox "Numeros posibles 0 o 1", vbExclamation + vbOKOnly, "Numero invalido"
                 logicaPorCodigo = False
                 Exit Function
            End Select
       Case "HAB_KPI"
            Select Case txtNumero
            Case 0
               habilitar = "N"
            Case 1
               habilitar = "S"
            Case Else
                 MsgBox "Numeros posibles 0 o 1", vbExclamation + vbOKOnly, "Numero invalido"
                 logicaPorCodigo = False
                 Exit Function
            End Select
       Case "HAB_BEN"
            Select Case txtNumero
            Case 0
               habilitar = "N"
            Case 1
               habilitar = "S"
            Case Else
                 MsgBox "Numeros posibles 0 o 1", vbExclamation + vbOKOnly, "Numero invalido"
                 logicaPorCodigo = False
                 Exit Function
            End Select
       Case "HAB_VINC"
            Select Case txtNumero
            Case 0
               habilitar = "N"
            Case 1
               habilitar = "S"
            Case Else
                 MsgBox "Numeros posibles 0 o 1", vbExclamation + vbOKOnly, "Numero invalido"
                 logicaPorCodigo = False
                 Exit Function
            End Select
       Case "HAB_TIPS"
            Select Case txtNumero
            Case 0
               habilitar = "N"
            Case 1
               habilitar = "S"
            Case 2
               habilitar = "N"
            Case 3
               habilitar = "S"
            Case Else
                 MsgBox "Numeros posibles 0,1,2,3 (0=Se apaga tips/1=Se prende tips/2=Se prende tips manuales/3=Se prende tips a determinados grupos)", vbExclamation + vbOKOnly, "Numero invalido"
                 logicaPorCodigo = False
                 Exit Function
            End Select
    End Select
   
    If Codigo = "HAB_RGyP" Then
       Call sp_HabilitaRGyP(habilitar)
    End If
    
    
End Function
'GMT01 - FIN
