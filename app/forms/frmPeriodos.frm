VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeriodos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Per�odos de planificaci�n"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9645
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6360
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2340
      Left            =   120
      TabIndex        =   13
      Top             =   3990
      Width           =   8205
      Begin VB.ComboBox cboPerEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1680
         Width           =   3585
      End
      Begin VB.ComboBox cboPerTipo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   960
         Width           =   3585
      End
      Begin AT_MaskText.MaskText txtPerNroInterno 
         Height          =   315
         Left            =   1320
         TabIndex        =   0
         Top             =   240
         Width           =   1125
         _ExtentX        =   1984
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPerNombre 
         Height          =   315
         Left            =   1320
         TabIndex        =   1
         Top             =   600
         Width           =   5805
         _ExtentX        =   10239
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText txtPerAbrev 
         Height          =   315
         Left            =   6360
         TabIndex        =   6
         Top             =   1680
         Width           =   1605
         _ExtentX        =   2831
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPerFchDesde 
         Height          =   315
         Left            =   1320
         TabIndex        =   3
         Top             =   1320
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPerFchHasta 
         Height          =   315
         Left            =   3330
         TabIndex        =   4
         Top             =   1320
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Abreviatura"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   3
         Left            =   5280
         TabIndex        =   21
         Top             =   1747
         Width           =   900
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   2
         Left            =   120
         TabIndex        =   20
         Top             =   1755
         Width           =   510
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   18
         Top             =   30
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   17
         Top             =   667
         Width           =   840
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   16
         Top             =   307
         Width           =   495
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   15
         Top             =   1035
         Width           =   315
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Desde/Hasta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   1380
         Width           =   975
      End
   End
   Begin VB.Frame pnlBotones 
      Height          =   6195
      Left            =   8340
      TabIndex        =   7
      Top             =   120
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   5610
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Direcci�n seleccionada"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la direcci�n selecionada"
         Top             =   4080
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Direcci�n"
         Top             =   3570
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3810
      Left            =   120
      TabIndex        =   19
      Top             =   150
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   6720
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPeriodos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const col_PER_NROINTERNO = 0
Const col_PER_NOMBRE = 1
Const col_PER_TIPO = 2
Const col_NOM_TIPO = 3
Const col_FE_DESDE = 4
Const col_FE_HASTA = 5
Const col_PER_ESTADO = 6
Const col_NOM_ESTADO = 7
Const col_PER_ULTFCHEST = 8
Const col_PER_ULTUSRID = 9
Const col_PER_ABREV = 10

Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    modUser32.IniciarScroll grdDatos
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    With cboPerTipo
        .Clear
        .AddItem "Semanal" & ESPACIOS & "||Z"
        .AddItem "Quincenal" & ESPACIOS & "||Q"
        .AddItem "Mensual" & ESPACIOS & "||M"
        .AddItem "Bimestral" & ESPACIOS & "||B"
        .AddItem "Trimestral" & ESPACIOS & "||T"
        .AddItem "Cuatrimestral" & ESPACIOS & "||C"
        .AddItem "Semestral" & ESPACIOS & "||S"
        .AddItem "Anual" & ESPACIOS & "||A"
        .AddItem "Personalizado" & ESPACIOS & "||X"
        .ListIndex = -1
    End With
    
    With cboPerEstado
        .Clear
        .AddItem "1ra. instancia: Prior./Ord." & ESPACIOS & "||PLAN10"
        .AddItem "2da. instancia: Gerencia BP" & ESPACIOS & "||PLAN20"
        .AddItem "3ra. instancia: Planificaci�n inicial DYD" & ESPACIOS & "||PLAN30"
        .AddItem "4ra. instancia: Seguimiento" & ESPACIOS & "||PLAN36"
        .AddItem "Cerrado" & ESPACIOS & "||PLAN40"
        .ListIndex = -1
    End With
    Show
    Call HabilitarBotones(0)
End Sub

Private Sub CargarGrid()
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .cols = 11
        .Rows = 1
        .TextMatrix(0, col_PER_NROINTERNO) = "C�digo"
        .TextMatrix(0, col_PER_NOMBRE) = "Nombre"
        .TextMatrix(0, col_PER_TIPO) = "cod_tipo"
        .TextMatrix(0, col_NOM_TIPO) = "Tipo"
        .TextMatrix(0, col_FE_DESDE) = "Desde"
        .TextMatrix(0, col_FE_HASTA) = "Hasta"
        .TextMatrix(0, col_PER_ESTADO) = "cod_estado"
        .TextMatrix(0, col_NOM_ESTADO) = "Estado"
        .TextMatrix(0, col_PER_ULTFCHEST) = "Ult. Modif."
        .TextMatrix(0, col_PER_ULTUSRID) = "Ult. Usr."
        .TextMatrix(0, col_PER_ABREV) = "Abrev."
        
        .ColWidth(col_PER_NROINTERNO) = 800: .ColAlignment(col_PER_NROINTERNO) = 0
        .ColWidth(col_PER_NOMBRE) = 3500: .ColAlignment(col_PER_NOMBRE) = 0
        .ColWidth(col_PER_TIPO) = 0: .ColAlignment(col_PER_TIPO) = 0
        .ColWidth(col_NOM_TIPO) = 1600: .ColAlignment(col_NOM_TIPO) = 0
        .ColWidth(col_FE_DESDE) = 1000: .ColAlignment(col_FE_DESDE) = 0
        .ColWidth(col_FE_HASTA) = 1000: .ColAlignment(col_FE_HASTA) = 0
        .ColWidth(col_PER_ESTADO) = 0: .ColAlignment(col_PER_ESTADO) = 0
        .ColWidth(col_NOM_ESTADO) = 2400: .ColAlignment(col_NOM_ESTADO) = 0
        .ColWidth(col_PER_ULTFCHEST) = 1800: .ColAlignment(col_PER_ULTFCHEST) = 0
        .ColWidth(col_PER_ULTUSRID) = 1200: .ColAlignment(col_PER_ULTUSRID) = 0
        .ColWidth(col_PER_ABREV) = 1200: .ColAlignment(col_PER_ABREV) = 0
        FuncionesGrid.CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    End With
    If Not sp_GetPeriodo(Null, Null) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, col_PER_NROINTERNO) = ClearNull(aplRST.Fields!per_nrointerno)
            .TextMatrix(.Rows - 1, col_PER_NOMBRE) = ClearNull(aplRST.Fields!per_nombre)
            .TextMatrix(.Rows - 1, col_PER_TIPO) = ClearNull(aplRST.Fields!per_tipo)
            .TextMatrix(.Rows - 1, col_NOM_TIPO) = ClearNull(aplRST.Fields!nom_tipo)
            .TextMatrix(.Rows - 1, col_FE_DESDE) = ClearNull(aplRST.Fields!fe_desde)
            .TextMatrix(.Rows - 1, col_FE_HASTA) = ClearNull(aplRST.Fields!fe_hasta)
            .TextMatrix(.Rows - 1, col_PER_ESTADO) = ClearNull(aplRST.Fields!per_estado)
            .TextMatrix(.Rows - 1, col_NOM_ESTADO) = ClearNull(aplRST.Fields!nom_estado)
            .TextMatrix(.Rows - 1, col_PER_ULTFCHEST) = ClearNull(aplRST.Fields!per_ultfchest)
            .TextMatrix(.Rows - 1, col_PER_ULTUSRID) = ClearNull(aplRST.Fields!per_ultusrid)
            .TextMatrix(.Rows - 1, col_PER_ABREV) = ClearNull(aplRST.Fields!per_abrev)
        End With
        aplRST.MoveNext
    Loop
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, col_PER_NROINTERNO) <> "" Then
                txtPerNroInterno = ClearNull(.TextMatrix(.RowSel, col_PER_NROINTERNO))
                txtPerNombre = ClearNull(.TextMatrix(.RowSel, col_PER_NOMBRE))
                cboPerTipo.ListIndex = PosicionCombo(cboPerTipo, ClearNull(.TextMatrix(.RowSel, col_PER_TIPO)), True)
                txtPerFchDesde = ClearNull(.TextMatrix(.RowSel, col_FE_DESDE))
                txtPerFchHasta = ClearNull(.TextMatrix(.RowSel, col_FE_HASTA))
                cboPerEstado.ListIndex = PosicionCombo(cboPerEstado, ClearNull(.TextMatrix(.RowSel, col_PER_ESTADO)), True)
                txtPerAbrev = ClearNull(.TextMatrix(.RowSel, col_PER_ABREV))
            End If
        End If
    End With
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertPeriodo(txtPerNroInterno, txtPerNombre, CodigoCombo(cboPerTipo, True), txtPerFchDesde, txtPerFchHasta, CodigoCombo(cboPerEstado, True), txtPerAbrev) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdatePeriodo(txtPerNroInterno, txtPerNombre, CodigoCombo(cboPerTipo, True), txtPerFchDesde, txtPerFchHasta, CodigoCombo(cboPerEstado, True), txtPerAbrev) Then
                    With grdDatos
                        If .RowSel > 0 Then
                            .TextMatrix(.RowSel, col_PER_NOMBRE) = txtPerNombre
                            .TextMatrix(.RowSel, col_PER_TIPO) = CodigoCombo(cboPerTipo, True)
                            .TextMatrix(.RowSel, col_NOM_TIPO) = TextoCombo(cboPerTipo, CodigoCombo(cboPerTipo, True), True)
                            .TextMatrix(.RowSel, col_FE_DESDE) = txtPerFchDesde
                            .TextMatrix(.RowSel, col_FE_HASTA) = txtPerFchHasta
                            .TextMatrix(.RowSel, col_PER_ESTADO) = CodigoCombo(cboPerEstado, True)
                            .TextMatrix(.RowSel, col_NOM_ESTADO) = TextoCombo(cboPerEstado, CodigoCombo(cboPerEstado, True), True)
                            .TextMatrix(.RowSel, col_PER_ABREV) = txtPerAbrev
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeletePeriodo(txtPerNroInterno) Then
                grdDatos.RemoveItem grdDatos.RowSel
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    txtPerNroInterno = "": txtPerNombre = ""
                    cboPerTipo.ListIndex = 5
                    cboPerEstado.ListIndex = 0
                    txtPerFchDesde = ""
                    txtPerFchHasta = ""
                    txtPerAbrev = ""
                    fraDatos.Enabled = True
                    txtPerNroInterno.Enabled = True
                    txtPerNroInterno.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = True
                    txtPerNroInterno.Enabled = False
                    txtPerNombre.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True

    If Trim(txtPerNroInterno.text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If Not IsNumeric(txtPerNroInterno) Then
        MsgBox ("El c�digo debe ser un n�mero entero positivo")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtPerNombre.text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtPerNombre.text, ":") > 0 Then
        MsgBox ("La Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If cboPerTipo.ListIndex = -1 Then
        MsgBox ("Debe seleccionar un Tipo")
        CamposObligatorios = False
        Exit Function
    End If
    ' Control de las fechas
    If Trim(txtPerFchDesde.text) = "" Then
        MsgBox ("Debe ingresar la fecha de inicio del per�odo")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtPerFchHasta.text) = "" Then
        MsgBox ("Debe ingresar la fecha de fin del per�odo")
        CamposObligatorios = False
        Exit Function
    End If
    If txtPerFchDesde.DateValue > txtPerFchHasta.DateValue Then
        MsgBox ("La fecha de inicio de per�odo debe ser menor a la fecha de fin")
        CamposObligatorios = False
        Exit Function
    End If
    If cboPerEstado.ListIndex = -1 Then
        MsgBox ("Debe seleccionar un Estado")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtPerAbrev.text) = "" Then
        MsgBox ("Debe completar la abreviatura")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub
