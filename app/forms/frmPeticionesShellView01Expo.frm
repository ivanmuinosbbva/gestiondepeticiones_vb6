VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPeticionesShellView01Expo 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Campos a incluir en la exportaci�n"
   ClientHeight    =   8370
   ClientLeft      =   3780
   ClientTop       =   5295
   ClientWidth     =   11655
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesShellView01Expo.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.StatusBar sbExportar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   105
      Top             =   8100
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   476
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Frame frmPet 
      Caption         =   " Petici�n "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6555
      Left            =   30
      TabIndex        =   29
      Top             =   60
      Width           =   6945
      Begin VB.CheckBox CHKFechaPedido 
         Caption         =   "Fecha de alta petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   106
         ToolTipText     =   "T�tulo"
         Top             =   1440
         Width           =   1920
      End
      Begin VB.CheckBox chkHorasReales 
         Caption         =   "Total de horas cargadas a la petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   72
         Top             =   4320
         Width           =   3060
      End
      Begin VB.CheckBox chkPetCategoria 
         Caption         =   "Categor�a"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   102
         Top             =   1200
         Width           =   1140
      End
      Begin VB.CheckBox chkPetPuntaje 
         Caption         =   "Puntaje"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   101
         Top             =   960
         Width           =   1140
      End
      Begin VB.CheckBox chkPetCargaBeneficios 
         Caption         =   "Indica carga beneficios"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   100
         Top             =   1440
         Width           =   1980
      End
      Begin VB.CheckBox chkPetKPIs 
         Caption         =   "Detalle de KPIs"
         Enabled         =   0   'False
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   99
         Top             =   1920
         Width           =   1500
      End
      Begin VB.CheckBox chkPetGestion 
         Caption         =   "Origen"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   98
         Top             =   720
         Width           =   1140
      End
      Begin VB.CheckBox chkPetPriorizacion 
         Caption         =   "Detalle de indicadores"
         Enabled         =   0   'False
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   97
         Top             =   1680
         Width           =   1905
      End
      Begin VB.CheckBox chkPetValoracion 
         Caption         =   "Valoraci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   96
         Tag             =   "Impacto y visibilidad"
         Top             =   480
         Width           =   1800
      End
      Begin VB.CheckBox chkRefBPEFecha 
         Caption         =   "F. env�o al Ref. RGyP"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   95
         Top             =   5280
         Width           =   2340
      End
      Begin VB.CheckBox chkPetClass 
         Caption         =   "Clase"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   94
         Top             =   720
         Width           =   1905
      End
      Begin VB.CheckBox CHKbpar 
         Caption         =   "Ref. de Sistema"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   93
         Top             =   4560
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_ing_comi 
         Caption         =   "F. env�o al Ref. de Sistemas"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   92
         Top             =   4800
         Width           =   2340
      End
      Begin VB.CheckBox chkRefBPE 
         Caption         =   "Ref. de RGyP"
         Height          =   210
         Left            =   240
         TabIndex        =   91
         Top             =   5040
         Width           =   1620
      End
      Begin VB.CheckBox chkPetRO 
         Caption         =   "Vinc. a RO"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   4860
         TabIndex        =   88
         Top             =   240
         Width           =   1140
      End
      Begin VB.CheckBox chkEmpresa 
         Caption         =   "Empresa"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   87
         Top             =   4080
         Width           =   1020
      End
      Begin VB.CheckBox chkPetProyectoIDM 
         Caption         =   "Proyecto IDM"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   83
         Top             =   3840
         Width           =   1380
      End
      Begin VB.CheckBox chkPetConfHomo 
         Caption         =   "Conformes de homologaci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   82
         Top             =   5760
         Width           =   2340
      End
      Begin VB.CheckBox chkPetRegulatorio 
         Caption         =   "Regulatorio"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   80
         Top             =   1200
         Width           =   1500
      End
      Begin VB.CommandButton cmdStdMark 
         Caption         =   "Marcar todos"
         Height          =   375
         Left            =   5625
         TabIndex        =   69
         Top             =   6030
         Width           =   1200
      End
      Begin VB.CheckBox CHKrepla 
         Caption         =   "Cant. replanificaciones"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   63
         Top             =   2880
         Width           =   1980
      End
      Begin VB.CheckBox CHKfe_fin_orig 
         Caption         =   "F.Fin Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   62
         Top             =   2400
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_ini_orig 
         Caption         =   "F.Ini Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   61
         Top             =   2160
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_ini_real 
         Caption         =   "F.Ini Real. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   60
         Top             =   1680
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_fin_real 
         Caption         =   "F.Fin Real. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   59
         Top             =   1920
         Width           =   2100
      End
      Begin VB.CheckBox CHKnom_orientacion 
         Caption         =   "Orientaci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   58
         Top             =   3360
         Width           =   1410
      End
      Begin VB.CheckBox CHKfe_fin_plan 
         Caption         =   "F.Fin Planif. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   57
         Top             =   1440
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_ini_plan 
         Caption         =   "F.Ini Planif. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   56
         Top             =   1200
         Width           =   2100
      End
      Begin VB.CheckBox CHKhoraspresup 
         Caption         =   "Horas Presup. Pet."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   55
         Top             =   2640
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_situacion 
         Caption         =   "Situaci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   54
         Top             =   4080
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_fe_estado 
         Caption         =   "Fec. Estado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   53
         Top             =   3840
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_estado 
         Caption         =   "Estado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   52
         Top             =   3600
         Width           =   2100
      End
      Begin VB.CheckBox CHKprioridad 
         Caption         =   "Prioridad"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   51
         ToolTipText     =   "Categor�a"
         Top             =   3360
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_nroasignado 
         Caption         =   "Nro. asignado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   50
         ToolTipText     =   "N�mero de Petici�n"
         Top             =   240
         Value           =   1  'Checked
         Width           =   1380
      End
      Begin VB.CheckBox CHKcod_tipo_peticion 
         Caption         =   "Tipo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   49
         ToolTipText     =   "Tipo"
         Top             =   480
         Width           =   2100
      End
      Begin VB.CheckBox CHKtitulo 
         Caption         =   "T�tulo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   48
         ToolTipText     =   "T�tulo"
         Top             =   1680
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_sector 
         Caption         =   "Sector solicitante"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   47
         Top             =   4320
         Width           =   2100
      End
      Begin VB.CheckBox CHKimportancia_nom 
         Caption         =   "Visibilidad"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   46
         Top             =   3120
         Width           =   2100
      End
      Begin VB.CheckBox CHKcorp_local 
         Caption         =   "C/L"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   45
         ToolTipText     =   "Corporativa / Local"
         Top             =   3120
         Width           =   660
      End
      Begin VB.CheckBox CHKdetalle 
         Caption         =   "Descripci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   44
         ToolTipText     =   "T�tulo"
         Top             =   1920
         Width           =   2100
      End
      Begin VB.CheckBox CHKcaracte 
         Caption         =   "Caracter�sticas"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   43
         ToolTipText     =   "T�tulo"
         Top             =   2160
         Width           =   2100
      End
      Begin VB.CheckBox CHKmotivos 
         Caption         =   "Motivos"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   42
         ToolTipText     =   "T�tulo"
         Top             =   2400
         Width           =   2100
      End
      Begin VB.CheckBox CHKrelacio 
         Caption         =   "Relac c/Otros Req."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   41
         ToolTipText     =   "T�tulo"
         Top             =   2640
         Width           =   2190
      End
      Begin VB.CheckBox CHKobserva 
         Caption         =   "Observaciones"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   40
         ToolTipText     =   "T�tulo"
         Top             =   2880
         Width           =   2100
      End
      Begin VB.CheckBox CHKsoli 
         Caption         =   "Solicitante"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   39
         Top             =   240
         Width           =   2100
      End
      Begin VB.CheckBox CHKrefe 
         Caption         =   "Referente"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   38
         Top             =   480
         Width           =   2100
      End
      Begin VB.CheckBox CHKsupe 
         Caption         =   "Supervisor"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   37
         Top             =   720
         Width           =   2100
      End
      Begin VB.CheckBox CHKauto 
         Caption         =   "Autorizante"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   36
         Top             =   960
         Width           =   2100
      End
      Begin VB.CheckBox CHKhistorial 
         Caption         =   "Historial"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   35
         Top             =   3600
         Width           =   1100
      End
      Begin VB.CheckBox CHKpcf 
         Caption         =   "Conformes"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   34
         Top             =   5520
         Width           =   1155
      End
      Begin VB.CheckBox CHKseguimN 
         Caption         =   "Nro Seguimiento Asociado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   33
         Top             =   5700
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CheckBox CHKseguimT 
         Caption         =   "Tit. Seguimiento Asociado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   32
         Top             =   5940
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CheckBox chkPetDocu 
         Caption         =   "Documentos adjuntos"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   31
         Top             =   6000
         Width           =   1875
      End
      Begin VB.CheckBox chkPetImpTech 
         Caption         =   "Impacto tecnol�gico"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   30
         Top             =   960
         Width           =   2145
      End
      Begin VB.Image imgWarning 
         Height          =   240
         Left            =   120
         Picture         =   "frmPeticionesShellView01Expo.frx":058A
         Top             =   6240
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label lblAdvertencia 
         AutoSize        =   -1  'True
         Caption         =   "Esta opci�n puede implicar demoras importantes para finalizar"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   420
         TabIndex        =   79
         Top             =   6270
         Visible         =   0   'False
         Width           =   4425
      End
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8025
      Left            =   10590
      TabIndex        =   0
      Top             =   60
      Width           =   1035
      Begin VB.CommandButton cmdScreen 
         Caption         =   "Generar y Visualizar"
         Height          =   435
         Left            =   60
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Efectua la exportaci�n incluyendo los campos seleccionados y abre la planilla resultante luego de generar"
         Top             =   600
         Width           =   885
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Generar"
         Height          =   435
         Left            =   60
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Efectua la exportaci�n incluyendo los campos seleccionados"
         Top             =   150
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "Cerrar"
         Height          =   435
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Sale de la pantalla"
         Top             =   7500
         Width           =   885
      End
   End
   Begin VB.Frame frmAgrup 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1480
      Left            =   30
      TabIndex        =   64
      Top             =   6600
      Width           =   10515
      Begin MSComctlLib.ProgressBar pbExpoParcial 
         Height          =   255
         Left            =   6540
         TabIndex        =   103
         Top             =   1140
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   450
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Scrolling       =   1
      End
      Begin VB.CheckBox CHKgantt 
         Caption         =   "Diagrama de Gantt"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   6600
         TabIndex        =   85
         Top             =   285
         Width           =   2025
      End
      Begin VB.ComboBox cboGantt 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmPeticionesShellView01Expo.frx":0B14
         Left            =   8775
         List            =   "frmPeticionesShellView01Expo.frx":0B16
         Style           =   2  'Dropdown List
         TabIndex        =   84
         Top             =   240
         Width           =   1530
      End
      Begin VB.CommandButton cmdOpen 
         Height          =   315
         Left            =   5520
         Picture         =   "frmPeticionesShellView01Expo.frx":0B18
         Style           =   1  'Graphical
         TabIndex        =   81
         TabStop         =   0   'False
         Top             =   1080
         Width           =   375
      End
      Begin VB.CommandButton cmdClrAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5880
         Picture         =   "frmPeticionesShellView01Expo.frx":10A2
         Style           =   1  'Graphical
         TabIndex        =   66
         TabStop         =   0   'False
         ToolTipText     =   "Limpia agrupamiento"
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton cmdAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5520
         Picture         =   "frmPeticionesShellView01Expo.frx":162C
         Style           =   1  'Graphical
         TabIndex        =   65
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar agrupamiento para incluir informaci�n de pertenencia"
         Top             =   480
         Width           =   375
      End
      Begin MSComctlLib.ProgressBar pbExpoTotal 
         Height          =   255
         Left            =   6540
         TabIndex        =   104
         Top             =   840
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   450
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Max             =   1
         Scrolling       =   1
      End
      Begin VB.Label txtAgrup 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   120
         TabIndex        =   90
         Top             =   480
         Width           =   5415
      End
      Begin VB.Label txtFile 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   120
         TabIndex        =   89
         Top             =   1080
         Width           =   5415
      End
      Begin VB.Label lblGantt 
         AutoSize        =   -1  'True
         Caption         =   "Fechas planificadas"
         Height          =   195
         Left            =   6870
         TabIndex        =   86
         Top             =   525
         Width           =   1395
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Incluir informaci�n de pertenencia por agrupamiento"
         Height          =   195
         Left            =   120
         TabIndex        =   68
         Top             =   240
         Width           =   3750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Ubicaci�n y nombre del archivo de salida"
         Height          =   195
         Left            =   120
         TabIndex        =   67
         Top             =   840
         Width           =   2895
      End
   End
   Begin VB.Frame frmGru 
      Caption         =   " Grupo "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6555
      Left            =   7020
      TabIndex        =   27
      Top             =   60
      Visible         =   0   'False
      Width           =   3525
      Begin VB.CheckBox chkObservEstado 
         Caption         =   "Observaciones del estado actual"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   78
         Top             =   4440
         Width           =   2715
      End
      Begin VB.CheckBox chkMotivoSuspension 
         Caption         =   "Motivo de suspensi�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   77
         Top             =   4200
         Width           =   1905
      End
      Begin VB.CheckBox chkFechaSuspension 
         Caption         =   "F. Fin. Suspensi�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   76
         Top             =   3960
         Width           =   1665
      End
      Begin VB.CheckBox chkFechaProduccion 
         Caption         =   "F. Prev. pasaje a Producci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   75
         Top             =   3720
         Width           =   2385
      End
      Begin VB.CheckBox chkHorasRealesGrupo 
         Caption         =   "Total de horas cargadas por grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   74
         Top             =   3480
         Width           =   3180
      End
      Begin VB.CommandButton cmdGrpMark 
         Caption         =   "Marcar todos"
         Height          =   375
         Left            =   2190
         TabIndex        =   70
         Top             =   6045
         Width           =   1200
      End
      Begin VB.CheckBox CHKgru_fe_fin_orig 
         Caption         =   "F.Fin Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   11
         Top             =   2760
         Width           =   2100
      End
      Begin VB.CheckBox CHKgru_fe_ini_orig 
         Caption         =   "F.Ini Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   10
         Top             =   2520
         Width           =   2100
      End
      Begin VB.CheckBox CHKgru_fe_ini_real 
         Caption         =   "F.Ini Real. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   8
         Top             =   2040
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_fe_fin_real 
         Caption         =   "F.Fin real. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   9
         Top             =   2280
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_recursos 
         Caption         =   "Recursos asignados"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   12
         Top             =   3000
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_info_adic 
         Caption         =   "Info Adic. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   15
         Top             =   3240
         Width           =   1545
      End
      Begin VB.CheckBox CHKgru_fe_fin_plan 
         Caption         =   "F.Fin Planif. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   7
         Top             =   1800
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_fe_ini_plan 
         Caption         =   "F.Ini Planif. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   6
         Top             =   1560
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_horaspresup 
         Caption         =   "Horas presupuestadas por grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   5
         Top             =   1320
         Width           =   2745
      End
      Begin VB.CheckBox CHKgru_situacion 
         Caption         =   "Situaci�n del grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_estado 
         Caption         =   "Estado del grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   3
         Top             =   840
         Width           =   2500
      End
      Begin VB.CheckBox CHKprio_ejecuc 
         Caption         =   "Prioridad Ejecuci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_sector 
         Caption         =   "Grupo de ejecuci�n"
         Enabled         =   0   'False
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Value           =   1  'Checked
         Width           =   2500
      End
   End
   Begin VB.Frame frmSec 
      Caption         =   " Sector "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6555
      Left            =   7020
      TabIndex        =   26
      Top             =   60
      Visible         =   0   'False
      Width           =   3525
      Begin VB.CheckBox chkHorasRealesSector 
         Caption         =   "Total de horas cargadas por sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   73
         Top             =   2760
         Width           =   3180
      End
      Begin VB.CommandButton cmdSecMark 
         Caption         =   "Marcar todos"
         Height          =   375
         Left            =   2190
         TabIndex        =   71
         Top             =   6030
         Width           =   1200
      End
      Begin VB.CheckBox CHKsec_fe_fin_orig 
         Caption         =   "F.Fin Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   25
         Top             =   2520
         Width           =   2100
      End
      Begin VB.CheckBox CHKsec_fe_ini_orig 
         Caption         =   "F.Ini Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   24
         Top             =   2280
         Width           =   2100
      End
      Begin VB.CheckBox CHKsec_fe_ini_real 
         Caption         =   "F.Ini Real. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   22
         Top             =   1800
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_fe_fin_real 
         Caption         =   "F.Fin Real. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   23
         Top             =   2040
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_fe_fin_plan 
         Caption         =   "F.Fin Planif. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   21
         Top             =   1560
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_fe_ini_plan 
         Caption         =   "F.Ini Planif. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   20
         Top             =   1320
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_horaspresup 
         Caption         =   "Horas presupuestadas por sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   19
         Top             =   1080
         Width           =   3045
      End
      Begin VB.CheckBox CHKsec_situacion 
         Caption         =   "Situaci�n Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   18
         Top             =   840
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_estado 
         Caption         =   "Estado Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   17
         Top             =   600
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_sector 
         Caption         =   "Sector Ejecuci�n"
         Enabled         =   0   'False
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Value           =   1  'Checked
         Width           =   2500
      End
   End
End
Attribute VB_Name = "frmPeticionesShellView01Expo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const xlJustify = -4130
Private Const xlLeft = -4131
Private Const xlRight = -4152
Private Const xlCenter = -4108
Private Const xlGeneral = 1
    
Private Const colNroInterno = 1
Private Const colFecPlanIni = 2
Private Const colFecPlanFin = 3
Private Const colEstadoPet = 4
    
Private peticionTipo As String
Private petPrioridad As String
Private petNivel As String
Private petArea As String
Private petAreaTxt As String
Private petEstado As String
Private petSituacion As String
Private petTitu As String
Private petImportancia As String
Private peticionClase As String
Private peticionImpacto As String
Private peticionRegulatorio As String
Private peticionReferenteDeSistema As String
Private peticionEmpresa As String
Private peticionRiesgoOperacional As String
Private peticionReferenteBPE As String
Private secNivel As String
Private secAreaTxt As String
Private secEstado As String
Private secSituacion As String
Private auxFiltro As String
Private flgHijos As Boolean
Private auxAgrup As String
Private secDire As String
Private secGere As String
Private secSect As String
Private secGrup As String

Dim flgEnCarga As Boolean
Dim bScreen As Boolean
Dim iContador As Long
Dim cntRows As Long
Dim cntLast As Long
Dim grantBeg As Long
Dim grantEnd As Long
Dim J As Long
Dim auxDate As Date
Dim bCancelar As Boolean
Dim bEnProceso As Boolean

Dim ENCABEZADO_PRINCIPAL As Integer
Dim ENCABEZADO_SECUNDARIO As Integer
Dim ENCABEZADO_SECUNDARIO_DETALLE As Integer

Private Sub Form_Load()
    Debug.Print "Exportar_Form_Load()"
    bCancelar = False
    Call InicializarPantalla
    Call Inicializar
End Sub

Private Sub Inicializar()
    Dim vestados() As String
    Dim vRetorno() As String
    
    With frmPeticionesShellView01
        peticionTipo = CodigoCombo(.cboTipopet, True)
        peticionClase = CodigoCombo(.cboPeticionClase, True)
        peticionImpacto = CodigoCombo(.cboPeticionImpacto, True)
        peticionRegulatorio = CodigoCombo(.cmbRegulatorio, True)
        peticionReferenteDeSistema = CodigoCombo(.cboBpar, True)
        peticionEmpresa = CodigoCombo(.cboEmp, True)
        peticionRiesgoOperacional = CodigoCombo(.cboRO, True)
        peticionReferenteBPE = CodigoCombo(.cboBPE, True)
        petPrioridad = "NULL"
        petNivel = .verNivelPet()
        petArea = .verAreaPet()
        petEstado = DatosCombo(.cboEstadoPet)
        petImportancia = CodigoCombo(.cboImportancia, True)
        petSituacion = CodigoCombo(.cboSituacionPet, True)
        secNivel = .verNivelSec()
        Call .verAreaSec
        secDire = .secDire
        secGere = .secGere
        secSect = .secSect
        secGrup = .secGrup
        secEstado = DatosCombo(.cboEstadoSec)
        secSituacion = CodigoCombo(.cboSituacionSec, True)
        If ClearNull(.txtTitulo.text) = "" Then
            petTitu = "NULL"
        Else
            petTitu = ClearNull(.txtTitulo.text)
        End If
        If ClearNull(.txtAgrup.text) <> "" Then
            If ParseString(vRetorno, .txtAgrup.text, "|") > 0 Then
                auxAgrup = vRetorno(2)
            End If
        Else
            auxAgrup = ""
        End If
        If secNivel = "NULL" And secDire = "NULL" And secGere = "NULL" And secSect = "NULL" And secGrup = "NULL" And secEstado = "NULL" And secSituacion = "NULL" And _
            IsNull(.txtDESDEiniplan.DateValue) And IsNull(.txtHASTAiniplan.DateValue) And IsNull(.txtDESDEfinplan.DateValue) And IsNull(.txtHASTAfinplan.DateValue) And _
            IsNull(.txtDESDEinireal.DateValue) And IsNull(.txtHASTAinireal.DateValue) And IsNull(.txtDESDEfinreal.DateValue) And IsNull(.txtHASTAfinreal.DateValue) Then
            flgHijos = False
        Else
            flgHijos = True
        End If
        If flgHijos Then
            frmPet.Width = 6945
            frmPet.ZOrder (0)
            If secNivel = "GRUP" Then
                frmGru.visible = True
                frmGru.Enabled = True
                frmGru.ZOrder (0)
            Else
                frmSec.visible = True
                frmSec.Enabled = True
                frmSec.ZOrder (0)
            End If
            If secGrup = "NULL" Then
                CHKgru_recursos.value = 0
                CHKgru_recursos.Enabled = False
            End If
        Else
            frmPet.Width = 10520
            frmPet.ZOrder (0)
        End If
        cmdStdMark.Left = frmPet.Width - cmdStdMark.Width - 100
    End With
    
    If secNivel = "NULL" Then
        cboGantt.AddItem "Petici�n" & ESPACIOS & "||PETI"
    End If
    If secNivel = "SECT" Then
        cboGantt.AddItem "Sector" & ESPACIOS & "||SECT"
        cboGantt.AddItem "Petici�n" & ESPACIOS & "||PETI"
    End If
    If secNivel = "GRUP" Then
        cboGantt.AddItem "Grupo" & ESPACIOS & "||GRUP"
        cboGantt.AddItem "Petici�n" & ESPACIOS & "||PETI"
    End If
    If CHKgantt.value = 1 Then
        cboGantt.ListIndex = 0
        cboGantt.Enabled = True
    Else
        cboGantt.ListIndex = -1
        cboGantt.Enabled = False
    End If
    CHKpet_nroasignado.value = 1
    CHKpet_nroasignado.Enabled = False
    If frmPeticionesShellView01.cboEmp.ListIndex > 0 Then
        chkPetPriorizacion.Enabled = True
        chkPetKPIs.Enabled = True
    Else
        chkPetKPIs.value = 0
        chkPetPriorizacion.value = 0
        chkPetPriorizacion.Enabled = False
        chkPetKPIs.Enabled = False
    End If
End Sub

Private Sub CHKhistorial_Click()
    Call GenerarAdventencia(CHKhistorial)
End Sub

Private Sub GenerarAdventencia(ByRef Obj As CheckBox)
    If Obj.value = 1 Then
        imgWarning.visible = True
        lblAdvertencia.visible = True
        Obj.ForeColor = vbRed
    Else
        imgWarning.visible = False
        lblAdvertencia.visible = False
        Obj.ForeColor = vbBlack
    End If
End Sub

Private Sub cmdGrpMark_Click()
    If cmdGrpMark.Caption = "Marcar todos" Then
        cmdGrpMark.Caption = "Desmarcar todos"
        If CHKprio_ejecuc.Enabled Then CHKprio_ejecuc.value = 1
        CHKgru_estado.value = 1
        CHKgru_situacion.value = 1
        CHKgru_horaspresup.value = 1
        CHKgru_fe_ini_plan.value = 1
        CHKgru_fe_fin_plan.value = 1
        CHKgru_fe_ini_real.value = 1
        CHKgru_fe_fin_real.value = 1
        CHKgru_fe_ini_orig.value = 1
        CHKgru_fe_fin_orig.value = 1
        If CHKgru_info_adic.Enabled Then CHKgru_info_adic.value = 1
        CHKgru_recursos.value = 1
        chkHorasRealesGrupo.value = 1
        chkFechaProduccion.value = 1
        chkFechaSuspension.value = 1
        chkMotivoSuspension.value = 1
        chkObservEstado.value = 1
    Else
        cmdGrpMark.Caption = "Marcar todos"
        CHKprio_ejecuc.value = 0
        CHKgru_estado.value = 0
        CHKgru_situacion.value = 0
        CHKgru_horaspresup.value = 0
        CHKgru_fe_ini_plan.value = 0
        CHKgru_fe_fin_plan.value = 0
        CHKgru_fe_ini_real.value = 0
        CHKgru_fe_fin_real.value = 0
        CHKgru_fe_ini_orig.value = 0
        CHKgru_fe_fin_orig.value = 0
        CHKgru_info_adic.value = 0
        CHKgru_recursos.value = 0
        chkHorasRealesGrupo.value = 0
        chkFechaProduccion.value = 0
        chkFechaSuspension.value = 0
        chkMotivoSuspension.value = 0
        chkObservEstado.value = 0
    End If
End Sub

Private Sub cmdSecMark_Click()
    If cmdSecMark.Caption = "Marcar todos" Then
        cmdSecMark.Caption = "Desmarcar todos"
        CHKsec_estado.value = 1
        CHKsec_situacion.value = 1
        CHKsec_horaspresup.value = 1
        CHKsec_fe_ini_plan.value = 1
        CHKsec_fe_fin_plan.value = 1
        CHKsec_fe_ini_real.value = 1
        CHKsec_fe_fin_real.value = 1
        CHKsec_fe_ini_orig.value = 1
        CHKsec_fe_fin_orig.value = 1
        chkHorasRealesSector.value = 1
    Else
        cmdSecMark.Caption = "Marcar todos"
        CHKsec_estado.value = 0
        CHKsec_situacion.value = 0
        CHKsec_horaspresup.value = 0
        CHKsec_fe_ini_plan.value = 0
        CHKsec_fe_fin_plan.value = 0
        CHKsec_fe_ini_real.value = 0
        CHKsec_fe_fin_real.value = 0
        CHKsec_fe_ini_orig.value = 0
        CHKsec_fe_fin_orig.value = 0
        chkHorasRealesSector.value = 0
    End If
End Sub

Private Sub cmdStdMark_Click()
    If cmdStdMark.Caption = "Marcar todos" Then
        cmdStdMark.Caption = "Desmarcar todos"
        CHKcod_tipo_peticion.value = 1
        CHKtitulo.value = 1
        CHKFechaPedido.value = 1
        CHKdetalle.value = 1
        CHKcaracte.value = 1
        CHKmotivos.value = 1
        CHKrelacio.value = 1
        CHKobserva.value = 1
        CHKprioridad.value = 1
        CHKfe_ing_comi.value = 1
        CHKpet_estado.value = 1
        CHKpet_fe_estado.value = 1
        CHKpet_situacion.value = 1
        CHKpet_sector.value = 1
        CHKbpar.value = 1
        CHKsoli.value = 1
        CHKrefe.value = 1
        CHKsupe.value = 1
        CHKauto.value = 1
        CHKhoraspresup.value = 1
        CHKfe_ini_plan.value = 1
        CHKfe_fin_plan.value = 1
        CHKfe_ini_real.value = 1
        CHKfe_fin_real.value = 1
        CHKfe_ini_orig.value = 1
        CHKfe_fin_orig.value = 1
        CHKrepla.value = 1
        CHKimportancia_nom.value = 1
        CHKcorp_local.value = 1
        CHKnom_orientacion.value = 1
        If InPerfil("SADM") Or InPerfil("ADMI") Or InPerfil("BPAR") Or SoyElAnalista() Then
            CHKhistorial.value = 1
        Else
            CHKhistorial.value = 0
        End If
        chkPetClass.value = 1
        chkPetImpTech.value = 1
        chkPetDocu.value = 1
        CHKpcf.value = 1
        chkHorasReales.value = 1
        chkPetRegulatorio.value = 1
        chkPetConfHomo.value = 1
        chkPetProyectoIDM.value = 1
        chkEmpresa.value = 1
        chkPetRO = 1
        chkRefBPE.value = 1
        chkRefBPEFecha.value = 1
        chkPetValoracion.value = 1
        chkPetGestion.value = 1
        chkPetCargaBeneficios.value = 1
        If chkPetPriorizacion.Enabled Then chkPetPriorizacion.value = 1
        If chkPetKPIs.Enabled Then chkPetKPIs.value = 1
        chkPetPuntaje.value = 1
        chkPetCategoria.value = 1
    Else
        cmdStdMark.Caption = "Marcar todos"
        CHKcod_tipo_peticion.value = 0
        CHKtitulo.value = 0
        CHKFechaPedido.value = 0
        CHKdetalle.value = 0
        CHKcaracte.value = 0
        CHKmotivos.value = 0
        CHKrelacio.value = 0
        CHKobserva.value = 0
        CHKprioridad.value = 0
        CHKfe_ing_comi.value = 0
        CHKpet_estado.value = 0
        CHKpet_fe_estado.value = 0
        CHKpet_situacion.value = 0
        CHKpet_sector.value = 0
        CHKbpar.value = 0
        CHKsoli.value = 0
        CHKrefe.value = 0
        CHKsupe.value = 0
        CHKauto.value = 0
        CHKhoraspresup.value = 0
        CHKfe_ini_plan.value = 0
        CHKfe_fin_plan.value = 0
        CHKfe_ini_real.value = 0
        CHKfe_fin_real.value = 0
        CHKfe_ini_orig.value = 0
        CHKfe_fin_orig.value = 0
        CHKrepla.value = 0
        CHKimportancia_nom.value = 0
        CHKcorp_local.value = 0
        CHKnom_orientacion.value = 0
        CHKhistorial.value = 0
        chkPetClass.value = 0
        chkPetImpTech.value = 0
        chkPetDocu.value = 0
        CHKpcf.value = 0
        chkHorasReales.value = 0
        chkPetRegulatorio.value = 0
        chkPetConfHomo.value = 0
        chkPetProyectoIDM.value = 0
        chkEmpresa.value = 0
        chkPetRO.value = 0
        chkRefBPE.value = 0
        chkRefBPEFecha.value = 0
        chkPetValoracion.value = 0
        chkPetGestion.value = 0
        chkPetCargaBeneficios.value = 0
        chkPetPriorizacion.value = 0
        chkPetKPIs.value = 0
        chkPetPuntaje.value = 0
        chkPetCategoria.value = 0
    End If
End Sub

Sub InicializarPantalla()
    cmdOpen.ToolTipText = "Especificar nombre y ubicaci�n del archivo de salida."
    txtFile = GetSetting("GesPet", "Export01", "FileExport", glWRKDIR & "EXPORTACION.XLS")
    CHKpet_nroasignado.value = IIf(GetSetting("GesPet", "Export01", "CHKpet_nroasignado") = "SI", 1, 0)
    CHKcod_tipo_peticion.value = IIf(GetSetting("GesPet", "Export01", "CHKcod_tipo_peticion") = "SI", 1, 0)
    CHKtitulo.value = IIf(GetSetting("GesPet", "Export01", "CHKtitulo") = "SI", 1, 0)
    CHKFechaPedido.value = IIf(GetSetting("GesPet", "Export01", "CHKFechaPedido") = "SI", 1, 0)
    CHKdetalle.value = IIf(GetSetting("GesPet", "Export01", "CHKdetalle") = "SI", 1, 0)
    CHKcaracte.value = IIf(GetSetting("GesPet", "Export01", "CHKcaracte") = "SI", 1, 0)
    CHKmotivos.value = IIf(GetSetting("GesPet", "Export01", "CHKmotivos") = "SI", 1, 0)
    CHKrelacio.value = IIf(GetSetting("GesPet", "Export01", "CHKrelacio") = "SI", 1, 0)
    CHKobserva.value = IIf(GetSetting("GesPet", "Export01", "CHKobserva") = "SI", 1, 0)
    CHKprioridad.value = IIf(GetSetting("GesPet", "Export01", "CHKprioridad") = "SI", 1, 0)
    CHKfe_ing_comi.value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ing_comi") = "SI", 1, 0)
    CHKpet_estado.value = IIf(GetSetting("GesPet", "Export01", "CHKpet_estado") = "SI", 1, 0)
    CHKpet_fe_estado.value = IIf(GetSetting("GesPet", "Export01", "CHKpet_fe_estado") = "SI", 1, 0)
    CHKpet_situacion.value = IIf(GetSetting("GesPet", "Export01", "CHKpet_situacion") = "SI", 1, 0)
    CHKpet_sector.value = IIf(GetSetting("GesPet", "Export01", "CHKpet_sector") = "SI", 1, 0)
    CHKbpar.value = IIf(GetSetting("GesPet", "Export01", "CHKbpar") = "SI", 1, 0)
    CHKsoli.value = IIf(GetSetting("GesPet", "Export01", "CHKsoli") = "SI", 1, 0)
    CHKrefe.value = IIf(GetSetting("GesPet", "Export01", "CHKrefe") = "SI", 1, 0)
    CHKsupe.value = IIf(GetSetting("GesPet", "Export01", "CHKsupe") = "SI", 1, 0)
    CHKauto.value = IIf(GetSetting("GesPet", "Export01", "CHKauto") = "SI", 1, 0)
    CHKhoraspresup.value = IIf(GetSetting("GesPet", "Export01", "CHKhoraspresup") = "SI", 1, 0)
    CHKfe_ini_plan.value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ini_plan") = "SI", 1, 0)
    CHKfe_fin_plan.value = IIf(GetSetting("GesPet", "Export01", "CHKfe_fin_plan") = "SI", 1, 0)
    CHKfe_ini_real.value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ini_real") = "SI", 1, 0)
    CHKfe_fin_real.value = IIf(GetSetting("GesPet", "Export01", "CHKfe_fin_real") = "SI", 1, 0)
    CHKfe_ini_orig.value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ini_orig") = "SI", 1, 0)
    CHKfe_fin_orig.value = IIf(GetSetting("GesPet", "Export01", "CHKfe_fin_orig") = "SI", 1, 0)
    CHKrepla.value = IIf(GetSetting("GesPet", "Export01", "CHKrepla") = "SI", 1, 0)
    CHKimportancia_nom.value = IIf(GetSetting("GesPet", "Export01", "CHKimportancia_nom") = "SI", 1, 0)
    CHKcorp_local.value = IIf(GetSetting("GesPet", "Export01", "CHKcorp_local") = "SI", 1, 0)
    CHKnom_orientacion.value = IIf(GetSetting("GesPet", "Export01", "CHKnom_orientacion") = "SI", 1, 0)
    CHKsec_sector.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_sector") = "SI", 1, 0)
    CHKsec_estado.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_estado") = "SI", 1, 0)
    CHKsec_situacion.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_situacion") = "SI", 1, 0)
    CHKsec_horaspresup.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_horaspresup") = "SI", 1, 0)
    CHKsec_fe_ini_plan.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_ini_plan") = "SI", 1, 0)
    CHKsec_fe_fin_plan.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_fin_plan") = "SI", 1, 0)
    CHKsec_fe_ini_real.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_ini_real") = "SI", 1, 0)
    CHKsec_fe_fin_real.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_fin_real") = "SI", 1, 0)
    CHKsec_fe_ini_orig.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_ini_orig") = "SI", 1, 0)
    CHKsec_fe_fin_orig.value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_fin_orig") = "SI", 1, 0)
    CHKgru_sector.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_sector") = "SI", 1, 0)
    CHKprio_ejecuc.value = IIf(GetSetting("GesPet", "Export01", "CHKprio_ejecuc") = "SI", 1, 0)
    CHKgru_estado.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_estado") = "SI", 1, 0)
    CHKgru_situacion.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_situacion") = "SI", 1, 0)
    CHKgru_horaspresup.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_horaspresup") = "SI", 1, 0)
    CHKgru_fe_ini_plan.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_ini_plan") = "SI", 1, 0)
    CHKgru_fe_fin_plan.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_fin_plan") = "SI", 1, 0)
    CHKgru_fe_ini_real.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_ini_real") = "SI", 1, 0)
    CHKgru_fe_fin_real.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_fin_real") = "SI", 1, 0)
    CHKgru_fe_ini_orig.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_ini_orig") = "SI", 1, 0)
    CHKgru_fe_fin_orig.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_fin_orig") = "SI", 1, 0)
    CHKgru_info_adic.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_info_adic") = "SI", 1, 0)
    CHKgru_recursos.value = IIf(GetSetting("GesPet", "Export01", "CHKgru_recursos") = "SI", 1, 0)
    CHKgantt.value = IIf(GetSetting("GesPet", "Export01", "CHKgantt") = "SI", 1, 0)
    glSelectAgrupStr = GetSetting("GesPet", "Export01", "glSelectAgrupStr", "")
    glSelectAgrup = GetSetting("GesPet", "Export01", "glSelectAgrup", "")
    txtAgrup = ClearNull(Left(glSelectAgrupStr, 50))
    CHKhistorial.value = 0
    If InPerfil("SADM") Or InPerfil("ADMI") Or InPerfil("BPAR") Or InPerfil("GBPE") Or SoyElAnalista() Then
        CHKhistorial.visible = True
    Else
        CHKhistorial.visible = False
    End If
    If InPerfil("CGRU") Or InPerfil("CSEC") Then
        CHKgru_info_adic.Enabled = True
        CHKprio_ejecuc.Enabled = True
    Else
        CHKgru_info_adic.value = 0
        CHKgru_info_adic.Enabled = False
        CHKprio_ejecuc.value = 0
        CHKprio_ejecuc.Enabled = False
    End If
    chkPetClass.value = IIf(GetSetting("GesPet", "Export01", "chkPetClass") = "SI", 1, 0)
    chkPetImpTech.value = IIf(GetSetting("GesPet", "Export01", "chkPetImpTech") = "SI", 1, 0)
    chkPetDocu.value = IIf(GetSetting("GesPet", "Export01", "chkPetDocu") = "SI", 1, 0)
    chkHorasReales.value = IIf(GetSetting("GesPet", "Export01", "chkHorasReales") = "SI", 1, 0)
    CHKpcf.value = IIf(GetSetting("GesPet", "Export01", "CHKpcf") = "SI", 1, 0)
    chkFechaProduccion.value = IIf(GetSetting("GesPet", "Export01", "chkFechaProduccion") = "SI", 1, 0)
    chkFechaSuspension.value = IIf(GetSetting("GesPet", "Export01", "chkFechaSuspension") = "SI", 1, 0)
    chkMotivoSuspension.value = IIf(GetSetting("GesPet", "Export01", "chkMotivoSuspension") = "SI", 1, 0)
    chkObservEstado.value = IIf(GetSetting("GesPet", "Export01", "chkObservEstado") = "SI", 1, 0)
    chkPetRegulatorio.value = IIf(GetSetting("GesPet", "Export01", "chkPetRegulatorio") = "SI", 1, 0)
    chkPetConfHomo.value = IIf(GetSetting("GesPet", "Export01", "chkPetConfHomo") = "SI", 1, 0)
    chkPetProyectoIDM.value = IIf(GetSetting("GesPet", "Export01", "chkPetProyectoIDM") = "SI", 1, 0)
    chkRefBPE.value = IIf(GetSetting("GesPet", "Export01", "chkRefBPE") = "SI", 1, 0)
    chkRefBPEFecha.value = IIf(GetSetting("GesPet", "Export01", "chkRefBPEFecha") = "SI", 1, 0)
    chkPetValoracion.value = IIf(GetSetting("GesPet", "Export01", "chkPetValoracion") = "SI", 1, 0)
    chkPetRO.value = IIf(GetSetting("GesPet", "Export01", "chkPetRO") = "SI", 1, 0)
    chkEmpresa.value = IIf(GetSetting("GesPet", "Export01", "chkEmpresa") = "SI", 1, 0)
    chkPetGestion.value = IIf(GetSetting("GesPet", "Export01", "chkPetGestion") = "SI", 1, 0)
    chkPetCargaBeneficios.value = IIf(GetSetting("GesPet", "Export01", "chkPetCargaBeneficios") = "SI", 1, 0)
    chkPetPuntaje.value = IIf(GetSetting("GesPet", "Export01", "chkPetPuntaje") = "SI", 1, 0)
    chkPetCategoria.value = IIf(GetSetting("GesPet", "Export01", "chkPetCategoria") = "SI", 1, 0)
End Sub

Private Sub CHKgantt_Click()
    If CHKgantt.value = 1 Then
        cboGantt.Enabled = True
        If cboGantt.ListCount > 0 Then
            cboGantt.ListIndex = 0
        End If
    Else
        cboGantt.ListIndex = -1
        cboGantt.Enabled = False
    End If
End Sub

Private Sub cmdAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "EXPO"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    txtAgrup = ClearNull(Left(glSelectAgrupStr, 50))
    DoEvents
End Sub

Private Sub cmdClrAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = ""
    txtAgrup = glSelectAgrupStr
    DoEvents
End Sub

Private Sub HabiltarPaneles(modo As Boolean)
    frmPet.Enabled = modo
    frmGru.Enabled = modo
    frmSec.Enabled = modo
    frmAgrup.Enabled = modo
    Call setHabilCtrl(cmdConfirmar, IIf(modo, NORMAL, DISABLE))
    Call setHabilCtrl(cmdScreen, IIf(modo, NORMAL, DISABLE))
    cmdCerrar.Caption = IIf(modo, "Cerrar", "Cancelar")
    cmdCerrar.ToolTipText = IIf(modo, "Cierra la ventana actual", "Cancela el proceso de exportaci�n en ejecuci�n")
End Sub

Private Sub cmdConfirmar_Click()
    bScreen = False
    Call Exportar
End Sub

Private Sub cmdScreen_Click()
    bScreen = True
    Call Exportar
End Sub

Private Sub Exportar()
    If Len(Trim(txtFile)) > 0 Then
        If InicializarArchivo Then
            If MsgBox("�Confirma la generaci�n de la exportaci�n?", vbQuestion + vbOKCancel, "Exportaci�n de la consulta") = vbOK Then
                bEnProceso = True
                Call HabiltarPaneles(False)
                Call Generar
                If bCancelar Then
                    Unload Me
                    Exit Sub
                End If
                Call HabiltarPaneles(True)
                bEnProceso = False
            End If
        End If
    Else
        MsgBox "Debe especificar el nombre del archivo de salida.", vbExclamation + vbOKOnly, "Nombre de archivo de salida"
    End If
End Sub

Private Function InicializarArchivo() As Boolean
    InicializarArchivo = True
    If Dir(txtFile) <> "" Then
        If MsgBox("El archivo ya existe." & Chr(13) & "�Sobreescribir el archivo?", vbExclamation + vbYesNo) = vbNo Then
            InicializarArchivo = False
            Exit Function
        End If
        On Error Resume Next
        Kill txtFile
    End If
    If Dir(txtFile) <> "" Then
        MsgBox "No puede sobreescribir el archivo, verifique que no est� en uso.", vbExclamation
        InicializarArchivo = False
    End If
    ' Comprobamos la ruta especificada
    If Dir(ObtenerDirectorio(txtFile), vbDirectory) = "" Then
        MsgBox "El nombre del archivo o la ubicaci�n no es correcta. Revise.", vbExclamation
        InicializarArchivo = False
    End If
End Function

Private Sub PlanillaEncabezados(ByRef cntField As Long)
    ' Calcula la cantidad de columnas a generar para la planilla MS Excel
    pbExpoTotal.Max = pbExpoTotal.Max + 1
    Debug.Print "1. General"
    If CHKpet_nroasignado.value = 1 Then cntField = cntField + 1: CHKpet_nroasignado.Tag = cntField
    If chkEmpresa.value = 1 Then cntField = cntField + 1: chkEmpresa.Tag = cntField
    If chkPetRO.value = 1 Then cntField = cntField + 1: chkPetRO.Tag = cntField
    If CHKcod_tipo_peticion.value = 1 Then cntField = cntField + 1: CHKcod_tipo_peticion.Tag = cntField
    If chkPetClass.value = 1 Then cntField = cntField + 1: chkPetClass.Tag = cntField
    If chkPetRegulatorio.value = 1 Then cntField = cntField + 1: chkPetRegulatorio.Tag = cntField
    If chkPetImpTech.value = 1 Then cntField = cntField + 1: chkPetImpTech.Tag = cntField
    If CHKtitulo.value = 1 Then cntField = cntField + 1: CHKtitulo.Tag = cntField
    If CHKFechaPedido.value = 1 Then cntField = cntField + 1: CHKFechaPedido.Tag = cntField
    If CHKdetalle.value = 1 Then cntField = cntField + 1: CHKdetalle.Tag = cntField
    If CHKcaracte.value = 1 Then cntField = cntField + 1: CHKcaracte.Tag = cntField
    If CHKmotivos.value = 1 Then cntField = cntField + 1: CHKmotivos.Tag = cntField
    If CHKrelacio.value = 1 Then cntField = cntField + 1: CHKrelacio.Tag = cntField
    If CHKobserva.value = 1 Then cntField = cntField + 1: CHKobserva.Tag = cntField
    If CHKdetalle.value = 1 Or CHKcaracte.value = 1 Or CHKmotivos.value = 1 Or CHKrelacio.value = 1 Or CHKobserva.value = 1 Then
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "2. Detalles"
    End If
    If CHKprioridad.value = 1 Then cntField = cntField + 1: CHKprioridad.Tag = cntField
    If CHKfe_ing_comi.value = 1 Then cntField = cntField + 1: CHKfe_ing_comi.Tag = cntField
    If CHKpet_estado.value = 1 Then cntField = cntField + 1: CHKpet_estado.Tag = cntField
    If CHKpet_fe_estado.value = 1 Then cntField = cntField + 1: CHKpet_fe_estado.Tag = cntField
    If CHKpet_situacion.value = 1 Then cntField = cntField + 1: CHKpet_situacion.Tag = cntField
    If CHKpet_sector.value = 1 Then cntField = cntField + 1: CHKpet_sector.Tag = cntField
    If CHKbpar.value = 1 Then cntField = cntField + 1: CHKbpar.Tag = cntField
    If CHKsoli.value = 1 Then cntField = cntField + 1: CHKsoli.Tag = cntField
    If CHKrefe.value = 1 Then cntField = cntField + 1: CHKrefe.Tag = cntField
    If CHKsupe.value = 1 Then cntField = cntField + 1: CHKsupe.Tag = cntField
    If CHKauto.value = 1 Then cntField = cntField + 1: CHKauto.Tag = cntField
    If CHKsoli.value = 1 Or CHKrefe.value = 1 Or CHKsupe.value Or CHKauto.value Then
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "3. Solicitante"
    End If
    If CHKhoraspresup.value = 1 Then cntField = cntField + 1: CHKhoraspresup.Tag = cntField
    If chkHorasReales.value = 1 Then cntField = cntField + 1: chkHorasReales.Tag = cntField
    If CHKfe_ini_plan.value = 1 Then cntField = cntField + 1: CHKfe_ini_plan.Tag = cntField
    If CHKfe_fin_plan.value = 1 Then cntField = cntField + 1: CHKfe_fin_plan.Tag = cntField
    If CHKfe_ini_real.value = 1 Then cntField = cntField + 1: CHKfe_ini_real.Tag = cntField
    If CHKfe_fin_real.value = 1 Then cntField = cntField + 1: CHKfe_fin_real.Tag = cntField
    If CHKfe_ini_orig.value = 1 Then cntField = cntField + 1: CHKfe_ini_orig.Tag = cntField
    If CHKfe_fin_orig.value = 1 Then cntField = cntField + 1: CHKfe_fin_orig.Tag = cntField
    If CHKrepla.value = 1 Then cntField = cntField + 1: CHKrepla.Tag = cntField
    If CHKcorp_local.value = 1 Then cntField = cntField + 1: CHKcorp_local.Tag = cntField
    If CHKimportancia_nom.value = 1 Then cntField = cntField + 1: CHKimportancia_nom.Tag = cntField
    If CHKnom_orientacion.value = 1 Then cntField = cntField + 1: CHKnom_orientacion.Tag = cntField
    If CHKhistorial.value = 1 Then
        cntField = cntField + 1
        CHKhistorial.Tag = cntField
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "4. Historial"
    End If
    If CHKpcf.value = 1 Then
        cntField = cntField + 2
        CHKpcf.Tag = cntField - 1
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "5. Conformes"
    End If
    If chkPetDocu.value = 1 Then
        cntField = cntField + 1
        chkPetDocu.Tag = cntField
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "5. Documentos adjuntos"
    End If
    If chkPetConfHomo.value = 1 Then
        cntField = cntField + 3
        chkPetConfHomo.Tag = cntField - 2
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "6. Conformes de homologaci�n"
    End If
    If chkPetProyectoIDM.value = 1 Then
        cntField = cntField + 2
        chkPetProyectoIDM.Tag = cntField - 1
    End If
    If chkPetGestion.value = 1 Then cntField = cntField + 1: chkPetGestion.Tag = cntField
    If chkRefBPE.value = 1 Then cntField = cntField + 1: chkRefBPE.Tag = cntField
    If chkRefBPEFecha.value = 1 Then cntField = cntField + 1: chkRefBPEFecha.Tag = cntField
    If chkPetValoracion.value = 1 Then cntField = cntField + 2: chkPetValoracion.Tag = cntField - 1
    If chkPetCargaBeneficios.value = 1 Then cntField = cntField + 1: chkPetCargaBeneficios.Tag = cntField
    If chkPetPuntaje.value = 1 Then cntField = cntField + 1: chkPetPuntaje.Tag = cntField
    If chkPetCategoria.value = 1 Then cntField = cntField + 1: chkPetCategoria.Tag = cntField
    If chkPetPriorizacion.value = 1 Then
        If sp_GetDrivers(Null, Null, CodigoCombo(frmPeticionesShellView01.cboEmp, True)) Then
            cntField = cntField + 1                                 ' Esto es para marcar la columna de inicio (se usa en otras partes de la rutina)
            chkPetPriorizacion.Tag = cntField
            cntField = cntField + aplRST.RecordCount - 1
        End If
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "8. Indicadores"
    End If
    
    If chkPetKPIs.value = 1 Then
        If sp_GetIndicadorKPI(Null, CodigoCombo(frmPeticionesShellView01.cboEmp, True)) Then
            cntField = cntField + 1                                 ' Esto es para marcar la columna de inicio (se usa en otras partes de la rutina)
            chkPetKPIs.Tag = cntField
            cntField = cntField + (aplRST.RecordCount * 4) - 1      ' Se multiplica x 4 para por cada KPI detallar sus cuatro atributos
        End If
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "9. KPIs"
    End If
    If flgHijos Then
        If secNivel = "GRUP" Then           ' Abierto por grupos
            If CHKgru_sector.value = 1 Then cntField = cntField + 1: CHKgru_sector.Tag = cntField
            If CHKprio_ejecuc.value = 1 Then cntField = cntField + 1: CHKprio_ejecuc.Tag = cntField
            If CHKgru_estado.value = 1 Then cntField = cntField + 1: CHKgru_estado.Tag = cntField
            If CHKgru_situacion.value = 1 Then cntField = cntField + 1: CHKgru_situacion.Tag = cntField
            If CHKgru_horaspresup.value = 1 Then cntField = cntField + 1: CHKgru_horaspresup.Tag = cntField
            If chkHorasRealesGrupo.value = 1 Then cntField = cntField + 1: chkHorasRealesGrupo.Tag = cntField
            If CHKgru_fe_ini_plan.value = 1 Then cntField = cntField + 1: CHKgru_fe_ini_plan.Tag = cntField
            If CHKgru_fe_fin_plan.value = 1 Then cntField = cntField + 1: CHKgru_fe_fin_plan.Tag = cntField
            If CHKgru_fe_ini_real.value = 1 Then cntField = cntField + 1: CHKgru_fe_ini_real.Tag = cntField
            If CHKgru_fe_fin_real.value = 1 Then cntField = cntField + 1: CHKgru_fe_fin_real.Tag = cntField
            If CHKgru_fe_ini_orig.value = 1 Then cntField = cntField + 1: CHKgru_fe_ini_orig.Tag = cntField
            If CHKgru_fe_fin_orig.value = 1 Then cntField = cntField + 1: CHKgru_fe_fin_orig.Tag = cntField
            If CHKgru_info_adic.value = 1 Then cntField = cntField + 1: CHKgru_info_adic.Tag = cntField
            If chkFechaProduccion.value = 1 Then cntField = cntField + 1: chkFechaProduccion.Tag = cntField
            If chkFechaSuspension.value = 1 Then cntField = cntField + 1: chkFechaSuspension.Tag = cntField
            If chkMotivoSuspension.value = 1 Then cntField = cntField + 1: chkMotivoSuspension.Tag = cntField
            If chkObservEstado.value = 1 Then cntField = cntField + 1: chkObservEstado.Tag = cntField
            If CHKgru_recursos.value = 1 Then
                pbExpoTotal.Max = pbExpoTotal.Max + 1
                Debug.Print "10. Recursos (Grupo)"
            End If
        Else                            ' Abierto por sectores
            If CHKsec_sector.value = 1 Then cntField = cntField + 1: CHKsec_sector.Tag = cntField
            If CHKsec_estado.value = 1 Then cntField = cntField + 1: CHKsec_estado.Tag = cntField
            If CHKsec_situacion.value = 1 Then cntField = cntField + 1: CHKsec_situacion.Tag = cntField
            If CHKsec_horaspresup.value = 1 Then cntField = cntField + 1: CHKsec_horaspresup.Tag = cntField
            If chkHorasRealesSector.value = 1 Then cntField = cntField + 1: chkHorasRealesSector.Tag = cntField
            If CHKsec_fe_ini_plan.value = 1 Then cntField = cntField + 1: CHKsec_fe_ini_plan.Tag = cntField
            If CHKsec_fe_fin_plan.value = 1 Then cntField = cntField + 1: CHKsec_fe_fin_plan.Tag = cntField
            If CHKsec_fe_ini_real.value = 1 Then cntField = cntField + 1: CHKsec_fe_ini_real.Tag = cntField
            If CHKsec_fe_fin_real.value = 1 Then cntField = cntField + 1: CHKsec_fe_fin_real.Tag = cntField
            If CHKsec_fe_ini_orig.value = 1 Then cntField = cntField + 1: CHKsec_fe_ini_orig.Tag = cntField
            If CHKsec_fe_fin_orig.value = 1 Then cntField = cntField + 1: CHKsec_fe_fin_orig.Tag = cntField
        End If
    End If
    If Val(glSelectAgrup) > 0 Then
        cntField = cntField + 1
        txtAgrup.Tag = cntField
        pbExpoTotal.Max = pbExpoTotal.Max + 1
        Debug.Print "11. Agrupamientos"
    End If
End Sub

Private Sub PlanillaInicializaPrimeraFila(ByRef ExcelApp As Object, ByRef ExcelWrk As Object, ByRef xlSheet As Object)
    If chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1 Then
        ENCABEZADO_PRINCIPAL = 3
        ENCABEZADO_SECUNDARIO = 1
        ENCABEZADO_SECUNDARIO_DETALLE = 2
        With xlSheet.Rows(ENCABEZADO_SECUNDARIO)
           .WrapText = True
           .Orientation = 0
           .IndentLevel = 0
           .ShrinkToFit = False
           .MergeCells = False
           .RowHeight = 30
        End With
        With xlSheet.Rows(ENCABEZADO_PRINCIPAL)
           .WrapText = True
           .Orientation = 0
           .IndentLevel = 0
           .ShrinkToFit = False
           .MergeCells = False
           .RowHeight = 30
        End With
        With xlSheet.Rows(ENCABEZADO_SECUNDARIO_DETALLE)
           .WrapText = True
           .Orientation = 0
           .IndentLevel = 0
           .ShrinkToFit = False
           .MergeCells = False
           .RowHeight = 60
        End With
        xlSheet.Rows(ENCABEZADO_PRINCIPAL).Font.Bold = True
        xlSheet.Rows(ENCABEZADO_SECUNDARIO).Font.Bold = True
        xlSheet.Rows(ENCABEZADO_SECUNDARIO_DETALLE).Font.Bold = False
        ' Esto es para inmovilizar los paneles
        xlSheet.Range("B4").Activate
        ExcelApp.ActiveWindow.freezepanes = True
    Else
        ENCABEZADO_PRINCIPAL = 1
        With xlSheet.Rows(ENCABEZADO_PRINCIPAL)
           .WrapText = True
           .Orientation = 0
           .IndentLevel = 0
           .ShrinkToFit = False
           .MergeCells = False
           .RowHeight = 30
        End With
        xlSheet.Rows(ENCABEZADO_PRINCIPAL).Font.Bold = True
        ' Esto es para inmovilizar los paneles
        xlSheet.Range("B2").Activate
        ExcelApp.ActiveWindow.freezepanes = True
    End If
End Sub

Private Sub PlanillaFormatos(ByRef ExcelApp As Object, ByRef ExcelWrk As Object, ByRef xlSheet As Object, ByRef CANTIDAD_COLS_KPIS As Integer, ByRef inicio As Long, ByRef cntField As Long, ByRef cntLast As Long)
    On Error GoTo Errores
    Dim sExplicacion As String
    Dim CANTIDAD_COLS_DRIVERS As Integer
    
    With xlSheet
        .Columns(colNroInterno).ColumnWidth = 3
        If chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1 Then
             If chkPetPriorizacion.value = 1 Then
                 .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetPriorizacion.Tag)) = chkPetPriorizacion.Caption
             End If
             If chkPetKPIs.value = 1 Then
                 sExplicacion = "Los datos mostrados en cada celda son una concatenaci�n separada por pipes (|) que representan los cuatro siguientes valores:" & Chr(10) & Chr(10) & _
                                 "El primero es el valor esperado." & Chr(10) & _
                                 "El segundo es valor de partida." & Chr(10) & _
                                 "El tercero es tiempo esperado." & Chr(10) & _
                                 "El cuarto es la unidad de medida del tiempo esperado." & Chr(10) & Chr(10) & _
                                 "Por ejemplo, si tuvieramos una celda en la columna Comisiones, con el siguiente dato: [500000|0|60|MES] estar�a indicando lo siguiente:" & Chr(10) & Chr(10) & _
                                 "El KPI define un valor esperado de $ 500.000,00.- mensuales, partiendo de $ 0,00.-, a cumplir en 60 meses."
                 .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)) = chkPetKPIs.Caption
                 .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)).AddComment
                 .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)).Comment.text text:=sExplicacion
                 .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)).Comment.visible = False
                 .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)).Comment.Shape.Height = 250
                 .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)).Comment.Shape.Width = 300
                 With .Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)).Comment.Shape.TextFrame.Characters.Font
                     .name = "Times New Roman"
                     .Size = 12
                     .Bold = False
                     .ColorIndex = 0
                 End With
             End If
        End If
        If chkEmpresa.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkEmpresa.Tag)) = chkEmpresa.Caption: .Columns(Val(chkEmpresa.Tag)).HorizontalAlignment = xlRight: .Columns(Val(chkEmpresa.Tag)).ColumnWidth = 5
        If chkPetRO.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetRO.Tag)) = chkPetRO.Caption: .Columns(Val(chkPetRO.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(chkPetRO.Tag)).ColumnWidth = 6
        If CHKpet_nroasignado.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKpet_nroasignado.Tag)) = CHKpet_nroasignado.Caption: .Columns(Val(CHKpet_nroasignado.Tag)).HorizontalAlignment = xlRight:    .Columns(Val(CHKpet_nroasignado.Tag)).ColumnWidth = 9
        If CHKcod_tipo_peticion.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKcod_tipo_peticion.Tag)) = CHKcod_tipo_peticion.Caption: .Columns(Val(CHKcod_tipo_peticion.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKcod_tipo_peticion.Tag)).ColumnWidth = 5
        If chkPetClass.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetClass.Tag)) = chkPetClass.Caption: .Columns(Val(chkPetClass.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(chkPetClass.Tag)).ColumnWidth = 10
        If chkPetImpTech.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetImpTech.Tag)) = chkPetImpTech.Caption: .Columns(Val(chkPetImpTech.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(chkPetImpTech.Tag)).ColumnWidth = 10
        If CHKtitulo.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKtitulo.Tag)) = CHKtitulo.Caption: .Columns(Val(CHKtitulo.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(CHKtitulo.Tag)).ColumnWidth = 40
        If CHKFechaPedido.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKFechaPedido.Tag)) = CHKFechaPedido.Caption: .Columns(Val(CHKFechaPedido.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(CHKFechaPedido.Tag)).ColumnWidth = 15
        If CHKdetalle.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKdetalle.Tag)) = CHKdetalle.Caption
            With xlSheet.Columns(Val(CHKdetalle.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 40
            End With
        End If
        If CHKcaracte.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKcaracte.Tag)) = CHKcaracte.Caption
            With xlSheet.Columns(Val(CHKcaracte.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 40
            End With
        End If
        If CHKmotivos.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKmotivos.Tag)) = CHKmotivos.Caption
            With xlSheet.Columns(Val(CHKmotivos.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 40
            End With
        End If
        If CHKrelacio.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKrelacio.Tag)) = CHKrelacio.Caption
            With xlSheet.Columns(Val(CHKrelacio.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 40
            End With
        End If
        If CHKobserva.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKobserva.Tag)) = CHKobserva.Caption
            With xlSheet.Columns(Val(CHKobserva.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 40
            End With
        End If
        If CHKhistorial.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKhistorial.Tag)) = CHKhistorial.Caption
            With xlSheet.Columns(Val(CHKhistorial.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 50
            End With
        End If
        If chkPetDocu.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetDocu.Tag)) = chkPetDocu.Caption
            With xlSheet.Columns(Val(chkPetDocu.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 60
            End With
        End If
        If CHKpcf.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKpcf.Tag)) = CHKpcf.Caption
            .Cells(ENCABEZADO_PRINCIPAL, Val(CHKpcf.Tag) + 1) = "Ult.Fch. de conforme"
            With xlSheet.Columns(Val(CHKpcf.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 35
            End With
        End If
        If CHKprioridad.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKprioridad.Tag)) = CHKprioridad.Caption: .Columns(Val(CHKprioridad.Tag)).HorizontalAlignment = xlCenter
        If CHKfe_ing_comi.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKfe_ing_comi.Tag)) = CHKfe_ing_comi.Caption: .Columns(Val(CHKfe_ing_comi.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKfe_ing_comi.Tag)).ColumnWidth = 15
        If CHKpet_estado.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKpet_estado.Tag)) = CHKpet_estado.Caption: .Columns(Val(CHKpet_estado.Tag)).ColumnWidth = 23
        If CHKpet_fe_estado.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKpet_fe_estado.Tag)) = CHKpet_fe_estado.Caption: .Columns(Val(CHKpet_fe_estado.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKpet_fe_estado.Tag)).ColumnWidth = 15
        If CHKpet_situacion.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKpet_situacion.Tag)) = CHKpet_situacion.Caption
        If CHKpet_sector.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKpet_sector.Tag)) = CHKpet_sector.Caption: .Columns(Val(CHKpet_sector.Tag)).ColumnWidth = 60
        If CHKbpar.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKbpar.Tag)) = CHKbpar.Caption: .Columns(Val(CHKbpar.Tag)).ColumnWidth = 30
        If chkRefBPE.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkRefBPE.Tag)) = chkRefBPE.Caption: .Columns(Val(chkRefBPE.Tag)).ColumnWidth = 20
        If chkRefBPEFecha.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkRefBPEFecha.Tag)) = chkRefBPEFecha.Caption: .Columns(Val(chkRefBPEFecha.Tag)).ColumnWidth = 10
        If chkPetGestion.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetGestion.Tag)) = chkPetGestion.Caption: .Columns(Val(chkPetGestion.Tag)).ColumnWidth = 10
        If chkPetCargaBeneficios.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetCargaBeneficios.Tag)) = chkPetCargaBeneficios.Caption: .Columns(Val(chkPetCargaBeneficios.Tag)).ColumnWidth = 10
        If chkPetValoracion.value = 1 Then
             .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetValoracion.Tag)) = "Impacto": .Columns(Val(chkPetValoracion.Tag)).ColumnWidth = 10
             .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetValoracion.Tag) + 1) = "Facilidad": .Columns(Val(chkPetValoracion.Tag) + 1).ColumnWidth = 10
        End If
         If chkPetPuntaje.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetPuntaje.Tag)) = chkPetPuntaje.Caption: .Columns(Val(chkPetPuntaje.Tag)).ColumnWidth = 10
         If chkPetCategoria.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetCategoria.Tag)) = chkPetCategoria.Caption: .Columns(Val(chkPetCategoria.Tag)).ColumnWidth = 10
         If chkPetPriorizacion.value = 1 Then
             inicio = Val(chkPetPriorizacion.Tag)
             If sp_GetDrivers(Null, Null, Null, CodigoCombo(frmPeticionesShellView01.cboEmp, True)) Then
                 CANTIDAD_COLS_DRIVERS = aplRST.RecordCount
                 Do While Not aplRST.EOF
                     .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio) = ClearNull(aplRST.Fields!driverId) & ". " & ClearNull(aplRST.Fields!driverNom): .Columns(inicio).ColumnWidth = 11
                     ' Combino el sub-encabezado de drivers
                     ExcelApp.Range(.Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio), .Cells(ENCABEZADO_PRINCIPAL, inicio)).Select
                     ExcelApp.Selection.MergeCells = True
                     aplRST.MoveNext
                     inicio = inicio + 1
                     DoEvents
                 Loop
             End If
             With ExcelApp
                 ' Combino el encabezado de drivers
                 .DisplayAlerts = False             ' Ac� se generaba la advertencia...
                 .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetPriorizacion.Tag)), xlSheet.Cells(1, inicio - 1)).Select
                 .Selection.MergeCells = True
                 ' Pinto los drivers
                 .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetPriorizacion.Tag)), xlSheet.Cells(2, Val(chkPetPriorizacion.Tag) + (CANTIDAD_COLS_DRIVERS - 1))).Select
                 .Selection.Interior.Color = RGB(204, 229, 255)
                 .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetPriorizacion.Tag)), xlSheet.Cells(ENCABEZADO_SECUNDARIO_DETALLE, (Val(chkPetPriorizacion.Tag) + (CANTIDAD_COLS_DRIVERS - 1)))).Select
                 .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous: .Selection.Borders(xlEdgeTop).Weight = 3
                 .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous: .Selection.Borders(xlEdgeLeft).Weight = 3
                 .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous: .Selection.Borders(xlEdgeRight).Weight = 3
                 .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous: .Selection.Borders(xlEdgeBottom).Weight = 3
             End With
             With xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetPriorizacion.Tag))
                .HorizontalAlignment = xlCenter                 ' Revisar por qu� da error 1004: Propiedad HorizontalAlignment
                .VerticalAlignment = xlCenter
                .WrapText = True
                .Orientation = 0
             End With
         End If
         If chkPetKPIs.value = 1 Then
             inicio = Val(chkPetKPIs.Tag)
             If sp_GetIndicadorKPI(Null, Null) Then
                 CANTIDAD_COLS_KPIS = aplRST.RecordCount
                 Do While Not aplRST.EOF
                     sExplicacion = "Unidad de Medida: " & ClearNull(aplRST.Fields!unimedDesc) & " (" & ClearNull(aplRST.Fields!unimedSmb) & ")"
                     .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio) = ClearNull(aplRST.Fields!kpiid) & ". " & ClearNull(aplRST.Fields!kpinom): .Columns(inicio).ColumnWidth = 11
                     .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio).AddComment
                     .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio).Comment.text text:=sExplicacion
                     .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio).Comment.visible = False
                     .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio).Comment.Shape.Height = 60
                     .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio).Comment.Shape.Width = 80
                     With .Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio).Comment.Shape.TextFrame.Characters.Font
                         .name = "Tahoma"
                         .Size = 10
                         .Bold = False
                         .ColorIndex = 0
                     End With
                     ' Combino el encabezado
                     With ExcelApp
                         .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio), xlSheet.Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio + 3)).Select
                         .Selection.MergeCells = True
                         .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO_DETALLE, inicio), xlSheet.Cells(ENCABEZADO_PRINCIPAL, inicio + 3)).Select
                         .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous: .Selection.Borders(xlEdgeTop).Weight = 3
                         .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous: .Selection.Borders(xlEdgeLeft).Weight = 3
                         .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous: .Selection.Borders(xlEdgeRight).Weight = 3
                         .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous: .Selection.Borders(xlEdgeBottom).Weight = 3
                         .Selection.HorizontalAlignment = xlCenter                 ' Revisar por qu� da error 1004: Propiedad HorizontalAlignment
                         .Selection.VerticalAlignment = xlCenter
                         .Selection.WrapText = True
                         .Selection.Orientation = 0
                         .Selection.Font.Bold = True
                     End With
                     
                     ' Antes de pasar al siguiente, desgloso cada uno de ellos
                     .Cells(ENCABEZADO_PRINCIPAL, inicio) = "a. Esperado"
                     .Cells(ENCABEZADO_PRINCIPAL, inicio + 1) = "b. Partida"
                     .Cells(ENCABEZADO_PRINCIPAL, inicio + 2) = "c. Tiempo esperado"
                     .Cells(ENCABEZADO_PRINCIPAL, inicio + 3) = "d. Unidad de tiempo"
                     ExcelApp.Range(xlSheet.Cells(ENCABEZADO_PRINCIPAL, inicio), xlSheet.Cells(ENCABEZADO_PRINCIPAL, inicio + 3)).Select
                     ExcelApp.Selection.Font.Bold = False
                     aplRST.MoveNext
                     inicio = inicio + 4     ' Antes + 1
                     DoEvents
                 Loop
             End If
             With ExcelApp
                 .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)), xlSheet.Cells(ENCABEZADO_SECUNDARIO, inicio - 1)).Select
                 .Selection.MergeCells = True
                 ' Pinto los KPIs
                 .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)), xlSheet.Cells(ENCABEZADO_PRINCIPAL, Val(chkPetKPIs.Tag) + ((CANTIDAD_COLS_KPIS - 1) * 4))).Select
                 .Selection.Interior.Color = RGB(255, 229, 204)
                 .Range(xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag)), xlSheet.Cells(ENCABEZADO_SECUNDARIO_DETALLE, Val(chkPetPriorizacion.Tag) + ((CANTIDAD_COLS_KPIS - 1) * 4))).Select
                 .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous: .Selection.Borders(xlEdgeTop).Weight = 3
                 .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous: .Selection.Borders(xlEdgeLeft).Weight = 3
                 .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous: .Selection.Borders(xlEdgeRight).Weight = 3
                 .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous: .Selection.Borders(xlEdgeBottom).Weight = 3
             End With
             
             With xlSheet.Cells(ENCABEZADO_SECUNDARIO, Val(chkPetKPIs.Tag))
                .HorizontalAlignment = xlCenter
                .VerticalAlignment = xlCenter
                .WrapText = True
                .Orientation = 0
             End With
         End If
        If CHKsoli.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsoli.Tag)) = CHKsoli.Caption: .Columns(Val(CHKsoli.Tag)).ColumnWidth = 30
        If CHKrefe.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKrefe.Tag)) = CHKrefe.Caption: .Columns(Val(CHKrefe.Tag)).ColumnWidth = 30
        If CHKsupe.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsupe.Tag)) = CHKsupe.Caption: .Columns(Val(CHKsupe.Tag)).ColumnWidth = 30
        If CHKauto.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKauto.Tag)) = CHKauto.Caption: .Columns(Val(CHKauto.Tag)).ColumnWidth = 30
        If CHKhoraspresup.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKhoraspresup.Tag)) = CHKhoraspresup.Caption
        If chkHorasReales.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(chkHorasReales.Tag)) = "Hs. cargadas Pet.": xlSheet.Columns(Val(chkHorasReales.Tag)).NumberFormat = "###,###,##0.00"
        End If
        If CHKfe_ini_plan.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKfe_ini_plan.Tag)) = CHKfe_ini_plan.Caption: .Columns(Val(CHKfe_ini_plan.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKfe_ini_plan.Tag)).ColumnWidth = 15
        If CHKfe_fin_plan.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKfe_fin_plan.Tag)) = CHKfe_fin_plan.Caption: .Columns(Val(CHKfe_fin_plan.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKfe_fin_plan.Tag)).ColumnWidth = 15
        If CHKfe_ini_real.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKfe_ini_real.Tag)) = CHKfe_ini_real.Caption: .Columns(Val(CHKfe_ini_real.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKfe_ini_real.Tag)).ColumnWidth = 15
        If CHKfe_fin_real.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKfe_fin_real.Tag)) = CHKfe_fin_real.Caption: .Columns(Val(CHKfe_fin_real.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKfe_fin_real.Tag)).ColumnWidth = 15
        If CHKfe_ini_orig.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKfe_ini_orig.Tag)) = CHKfe_ini_orig.Caption: .Columns(Val(CHKfe_ini_orig.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKfe_ini_orig.Tag)).ColumnWidth = 15
        If CHKfe_fin_orig.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKfe_fin_orig.Tag)) = CHKfe_fin_orig.Caption: .Columns(Val(CHKfe_fin_orig.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKfe_fin_orig.Tag)).ColumnWidth = 15
        If CHKrepla.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKrepla.Tag)) = CHKrepla.Caption: .Columns(Val(CHKrepla.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKrepla.Tag)).ColumnWidth = 15
        If CHKimportancia_nom.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKimportancia_nom.Tag)) = CHKimportancia_nom.Caption: .Columns(Val(CHKimportancia_nom.Tag)).ColumnWidth = 15
        If CHKcorp_local.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKcorp_local.Tag)) = CHKcorp_local.Caption: .Columns(Val(CHKcorp_local.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKcorp_local.Tag)).ColumnWidth = 4
        If CHKnom_orientacion.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKnom_orientacion.Tag)) = CHKnom_orientacion.Caption: .Columns(Val(CHKnom_orientacion.Tag)).ColumnWidth = 30
        If flgHijos Then
            If secNivel = "GRUP" Then
                If CHKgru_sector.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_sector.Tag)) = CHKgru_sector.Caption: .Columns(Val(CHKgru_sector.Tag)).ColumnWidth = 40
                If CHKprio_ejecuc.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKprio_ejecuc.Tag)) = CHKprio_ejecuc.Caption: .Columns(Val(CHKprio_ejecuc.Tag)).ColumnWidth = 10
                If CHKgru_estado.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_estado.Tag)) = CHKgru_estado.Caption: .Columns(Val(CHKgru_estado.Tag)).ColumnWidth = 23
                If CHKgru_situacion.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_situacion.Tag)) = CHKgru_situacion.Caption
                If CHKgru_horaspresup.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_horaspresup.Tag)) = CHKgru_horaspresup.Caption
                If chkHorasRealesGrupo.value = 1 Then
                    .Cells(ENCABEZADO_PRINCIPAL, Val(chkHorasRealesGrupo.Tag)) = "Hs. cargadas por grupo": xlSheet.Columns(Val(chkHorasRealesGrupo.Tag)).NumberFormat = "###,###,##0.00"
                End If
                If CHKgru_fe_ini_plan.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_fe_ini_plan.Tag)) = CHKgru_fe_ini_plan.Caption: .Columns(Val(CHKgru_fe_ini_plan.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKgru_fe_ini_plan.Tag)).ColumnWidth = 15
                If CHKgru_fe_fin_plan.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_fe_fin_plan.Tag)) = CHKgru_fe_fin_plan.Caption: .Columns(Val(CHKgru_fe_fin_plan.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKgru_fe_fin_plan.Tag)).ColumnWidth = 15
                If CHKgru_fe_ini_real.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_fe_ini_real.Tag)) = CHKgru_fe_ini_real.Caption: .Columns(Val(CHKgru_fe_ini_real.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKgru_fe_ini_real.Tag)).ColumnWidth = 15
                If CHKgru_fe_fin_real.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_fe_fin_real.Tag)) = CHKgru_fe_fin_real.Caption: .Columns(Val(CHKgru_fe_fin_real.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKgru_fe_fin_real.Tag)).ColumnWidth = 15
                If CHKgru_fe_ini_orig.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_fe_ini_orig.Tag)) = CHKgru_fe_ini_orig.Caption: .Columns(Val(CHKgru_fe_ini_orig.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKgru_fe_ini_orig.Tag)).ColumnWidth = 15
                If CHKgru_fe_fin_orig.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_fe_fin_orig.Tag)) = CHKgru_fe_fin_orig.Caption: .Columns(Val(CHKgru_fe_fin_orig.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKgru_fe_fin_orig.Tag)).ColumnWidth = 15
                If (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                    If CHKprio_ejecuc.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKprio_ejecuc.Tag)) = CHKprio_ejecuc.Caption: .Columns(Val(CHKprio_ejecuc.Tag)).HorizontalAlignment = xlCenter
                    If CHKgru_info_adic.value = 1 Then
                        .Cells(ENCABEZADO_PRINCIPAL, Val(CHKgru_info_adic.Tag)) = CHKgru_info_adic.Caption
                        .Columns(Val(CHKgru_info_adic.Tag)).ColumnWidth = 50
                        With xlSheet.Columns(Val(CHKgru_info_adic.Tag))
                            .HorizontalAlignment = xlLeft
                            .WrapText = True
                            .Orientation = 0
                            .IndentLevel = 0
                            .ShrinkToFit = False
                            .MergeCells = False
                        End With
                    End If
                End If
                If chkFechaProduccion.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkFechaProduccion.Tag)) = chkFechaProduccion.Caption: .Columns(Val(chkFechaProduccion.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(chkFechaProduccion.Tag)).ColumnWidth = 15
                If chkFechaSuspension.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkFechaSuspension.Tag)) = chkFechaSuspension.Caption: .Columns(Val(chkFechaSuspension.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(chkFechaSuspension.Tag)).ColumnWidth = 15
                If chkMotivoSuspension.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkMotivoSuspension.Tag)) = chkMotivoSuspension.Caption: .Columns(Val(chkMotivoSuspension.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(chkMotivoSuspension.Tag)).ColumnWidth = 30
                If chkObservEstado.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkObservEstado.Tag)) = chkObservEstado.Caption: .Columns(Val(chkObservEstado.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(chkObservEstado.Tag)).ColumnWidth = 40
            Else
                If CHKsec_sector.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_sector.Tag)) = CHKsec_sector.Caption: .Columns(Val(CHKsec_sector.Tag)).ColumnWidth = 25
                If CHKsec_estado.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_estado.Tag)) = CHKsec_estado.Caption: .Columns(Val(CHKsec_estado.Tag)).ColumnWidth = 23
                If CHKsec_situacion.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_situacion.Tag)) = CHKsec_situacion.Caption
                If CHKsec_horaspresup.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_horaspresup.Tag)) = CHKsec_horaspresup.Caption
                If chkHorasRealesSector.value = 1 Then
                    .Cells(ENCABEZADO_PRINCIPAL, Val(chkHorasRealesSector.Tag)) = "Hs. cargadas por sector": xlSheet.Columns(Val(chkHorasRealesSector.Tag)).NumberFormat = "###,###,##0.00"
                End If
                If CHKsec_fe_ini_plan.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_fe_ini_plan.Tag)) = CHKsec_fe_ini_plan.Caption: .Columns(Val(CHKsec_fe_ini_plan.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKsec_fe_ini_plan.Tag)).ColumnWidth = 15
                If CHKsec_fe_fin_plan.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_fe_fin_plan.Tag)) = CHKsec_fe_fin_plan.Caption: .Columns(Val(CHKsec_fe_fin_plan.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKsec_fe_fin_plan.Tag)).ColumnWidth = 15
                If CHKsec_fe_ini_real.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_fe_ini_real.Tag)) = CHKsec_fe_ini_real.Caption: .Columns(Val(CHKsec_fe_ini_real.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKsec_fe_ini_real.Tag)).ColumnWidth = 15
                If CHKsec_fe_fin_real.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_fe_fin_real.Tag)) = CHKsec_fe_fin_real.Caption: .Columns(Val(CHKsec_fe_fin_real.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKsec_fe_fin_real.Tag)).ColumnWidth = 15
                If CHKsec_fe_ini_orig.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_fe_ini_orig.Tag)) = CHKsec_fe_ini_orig.Caption: .Columns(Val(CHKsec_fe_ini_orig.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKsec_fe_ini_orig.Tag)).ColumnWidth = 15
                If CHKsec_fe_fin_orig.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(CHKsec_fe_fin_orig.Tag)) = CHKsec_fe_fin_orig.Caption: .Columns(Val(CHKsec_fe_fin_orig.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(CHKsec_fe_fin_orig.Tag)).ColumnWidth = 15
            End If
        End If
        If chkPetRegulatorio.value = 1 Then .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetRegulatorio.Tag)) = chkPetRegulatorio.Caption: .Columns(Val(chkPetRegulatorio.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(chkPetRegulatorio.Tag)).ColumnWidth = 7      ' add -008- a.
        If chkPetConfHomo.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetConfHomo.Tag)) = "Conf.HSOB": .Columns(Val(chkPetConfHomo.Tag)).HorizontalAlignment = xlCenter: .Columns(Val(chkPetConfHomo.Tag)).ColumnWidth = 7
            .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetConfHomo.Tag + 1)) = "Conf.HOME": .Columns(Val(chkPetConfHomo.Tag) + 1).HorizontalAlignment = xlCenter: .Columns(Val(chkPetConfHomo.Tag) + 1).ColumnWidth = 7
            .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetConfHomo.Tag + 2)) = "Conf.HOMA": .Columns(Val(chkPetConfHomo.Tag) + 2).HorizontalAlignment = xlCenter: .Columns(Val(chkPetConfHomo.Tag) + 2).ColumnWidth = 7
        End If
        If chkPetProyectoIDM.value = 1 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetProyectoIDM.Tag)) = "Proyecto": .Columns(Val(chkPetProyectoIDM.Tag)).HorizontalAlignment = xlLeft: .Columns(Val(chkPetProyectoIDM.Tag)).ColumnWidth = 10
            .Cells(ENCABEZADO_PRINCIPAL, Val(chkPetProyectoIDM.Tag) + 1) = "Nombre del proyecto": .Columns(Val(chkPetProyectoIDM.Tag) + 1).HorizontalAlignment = xlLeft: .Columns(Val(chkPetProyectoIDM.Tag) + 1).ColumnWidth = 15
        End If
        ' Esto es para la parte de Grupos...
        ' O agrupamientos???? 26.08.2016
        If Val(glSelectAgrup) > 0 Then
            .Cells(ENCABEZADO_PRINCIPAL, Val(txtAgrup.Tag)) = txtAgrup
            With xlSheet.Columns(Val(txtAgrup.Tag))
                .HorizontalAlignment = xlLeft
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 40
            End With
        End If

        ' Opci�n de recursos asignados
        If secNivel = "GRUP" And CHKgru_recursos.value = 1 Then
           If sp_GetRecurso(secGrup, "SU", "A") Then
               Do While Not aplRST.EOF
                   For J = cntField + 1 To cntLast
                       If xlSheet.Cells(ENCABEZADO_PRINCIPAL, J) = ClearNull(aplRST(1)) Then
                           xlSheet.Columns(J).HorizontalAlignment = xlCenter
                           Exit For
                       End If
                   Next
                   If J > cntLast Then
                       xlSheet.Cells(ENCABEZADO_PRINCIPAL, cntLast) = ClearNull(aplRST(1))
                       xlSheet.Columns(cntLast).HorizontalAlignment = xlCenter
                       xlSheet.Columns(cntLast).ColumnWidth = 2
                       xlSheet.Columns(cntLast).EntireColumn.AutoFit
                       cntLast = cntLast + 1
                   End If
                   aplRST.MoveNext
                   DoEvents
               Loop
           End If
        End If
    End With
Exit Sub
Errores:
    If Err.Number = 1004 Then
        Debug.Print Err.DESCRIPTION & " (" & Now & ") " & Err.Source
        Resume Next
    ElseIf (Err.Number = 380 And Err.Source = "ProgCtrl") Then              ' Excede el valor MAX del progress bar...
        Debug.Print Err.DESCRIPTION & " (" & Now & ") " & Err.Source
        Resume Next
    Else
        MsgBox "No se puede generar la exportaci�n." & vbCrLf & "Descripci�n: " & Err.DESCRIPTION, vbCritical + vbOKOnly, "Error en " & Err.Source
        Set xlSheet = Nothing
        'Resume
        Set ExcelWrk = Nothing
        If Not IsNull(ExcelApp) Then
            ExcelApp.DisplayAlerts = False
            ExcelApp.Quit
            Set ExcelApp = Nothing
        End If
        Call Puntero(False)
        'Call Status("Listo.")
        Unload Me
        'Resume
    End If
End Sub

Private Sub Generar()
    On Error GoTo Errores
    If flgEnCarga Then Exit Sub
    Dim ExcelApp As Object, ExcelWrk As Object, xlSheet As Object
    Dim cntField As Long
    Dim sTexto As String
    Dim sTtaux As String
    Dim sTttmp As String
    Dim sPetic As String
    Dim auxPrjFilter As String
    Dim auxPlanIni As Date
    Dim auxPlanFin As Date
    Dim hAux As Long
    Dim aHisto()
    Dim strMes As String
    Dim flg2003 As Boolean
    Dim sFileName As String
    Dim lSumaHoras As Double
    Dim inicio As Long
    Dim CANTIDAD_COLS_KPIS As Integer
    Dim cntTotalRows As Long
    
    cntField = 0
    cntRows = 0
    cntTotalRows = 0                    ' Total real de registros del recordset principal
    strMes = "EneFebMarAbrMayJunJulAgoSetOctNovDic"
    flg2003 = False
    sFileName = txtFile
    pbExpoTotal.value = 0
    pbExpoTotal.Max = 1
    cntField = 4
    
    ' Se inicializan los encabezados
    Call PlanillaEncabezados(cntField)
    If bCancelar Then GoTo finCancelacion
    
    cntLast = cntField + 1
    
    Call StatusLocal(sbExportar, "Preparando la exportaci�n...")
    Call Puntero(True)
    
    If CrearObjetoExcel(ExcelApp, ExcelWrk, xlSheet, sFileName) Then           ' Crea la planilla para la exportaci�n
        ExcelApp.ActiveWindow.Zoom = 75
        xlSheet.Cells.VerticalAlignment = -4160
        If bCancelar Then GoTo finCancelacion
        Call PlanillaInicializaPrimeraFila(ExcelApp, ExcelWrk, xlSheet)         ' Determina el n�mero de fila donde comenzar la exportaci�n
        If bCancelar Then GoTo finCancelacion
        Call PlanillaFormatos(ExcelApp, ExcelWrk, xlSheet, CANTIDAD_COLS_KPIS, inicio, cntField, cntLast)
        If bCancelar Then GoTo finCancelacion
        iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 3, 1)
        pbExpoTotal.Max = pbExpoTotal.Max - 1
        With frmPeticionesShellView01
           If chkHorasReales.value = 0 Then
               If Not sp_rptPeticion01(0, .txtNroDesde.text, .txtNroHasta.text, peticionTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, peticionReferenteDeSistema, _
                                       peticionClase, peticionImpacto, peticionRegulatorio, peticionEmpresa, peticionRiesgoOperacional, .txtPrjId, .txtPrjSubId, .txtPrjSubsId, peticionReferenteBPE, .txtBPEDesde, .txtBPEHasta, CodigoCombo(.cboCategoria, True), .txtDesdeAltaPeticion.DateValue, .txtHastaAltaPeticion.DateValue) Then
                  GoTo finx
               End If
           Else
               If Not sp_rptPeticion03(0, .txtNroDesde.text, .txtNroHasta.text, peticionTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, peticionReferenteDeSistema, _
                                       peticionClase, peticionImpacto, peticionRegulatorio, peticionEmpresa, peticionRiesgoOperacional, .txtPrjId, .txtPrjSubId, .txtPrjSubsId, peticionReferenteBPE, .txtBPEDesde, .txtBPEHasta, CodigoCombo(.cboCategoria, True), .txtDesdeAltaPeticion.DateValue, .txtHastaAltaPeticion.DateValue) Then
                  GoTo finx
               End If
           End If
           cntTotalRows = aplRST.RecordCount
           aplRST.CacheSize = 50
        End With

        If bCancelar Then GoTo finCancelacion
        pbExpoParcial.value = 0
        pbExpoParcial.Max = cntTotalRows
        
        Dim cantidadLoop As Long
        Dim contador As Long
        cantidadLoop = aplRST.RecordCount
         
        For contador = 1 To cantidadLoop
           iContador = iContador + 1
           pbExpoParcial.value = contador
           lSumaHoras = 0
           Call StatusLocal(sbExportar, "Peticiones exportadas: " & contador)
           xlSheet.Cells(iContador, colNroInterno) = aplRST!pet_nrointerno
           If chkEmpresa.value = 1 Then xlSheet.Cells(iContador, Val(chkEmpresa.Tag)) = aplRST!pet_emp
           If chkPetRO.value = 1 Then xlSheet.Cells(iContador, Val(chkPetRO.Tag)) = aplRST!pet_riesgo
           If CHKpet_nroasignado.value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_nroasignado.Tag)) = aplRST!pet_nroasignado
           If CHKcod_tipo_peticion.value = 1 Then xlSheet.Cells(iContador, Val(CHKcod_tipo_peticion.Tag)) = aplRST!cod_tipo_peticion
           If chkPetClass.value = 1 Then xlSheet.Cells(iContador, Val(chkPetClass.Tag)) = aplRST!cod_clase
           If chkPetImpTech.value = 1 Then xlSheet.Cells(iContador, Val(chkPetImpTech.Tag)) = aplRST!pet_imptech
           If CHKtitulo.value = 1 Then xlSheet.Cells(iContador, Val(CHKtitulo.Tag)) = ClearNull(aplRST!Titulo)
           If CHKFechaPedido.value = 1 Then xlSheet.Cells(iContador, Val(CHKFechaPedido.Tag)) = ClearNull(aplRST!fe_pedido)
           If CHKprioridad.value = 1 Then xlSheet.Cells(iContador, Val(CHKprioridad.Tag)) = aplRST!prioridad
           If CHKfe_ing_comi.value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ing_comi.Tag)) = IIf(Not IsNull(aplRST!fe_comite), Format(aplRST!fe_comite, "yyyy-mm-dd"), "")
           If CHKpet_estado.value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_estado.Tag)) = ClearNull(aplRST!nom_estado)
           If CHKpet_fe_estado.value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_fe_estado.Tag)) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
           If CHKpet_situacion.value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_situacion.Tag)) = ClearNull(aplRST!nom_situacion) & " "
           If CHKpet_sector.value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_sector.Tag)) = ClearNull(aplRST!sol_direccion) & " � " & ClearNull(aplRST!sol_gerencia) & " � " & ClearNull(aplRST!sol_sector)
           If CHKbpar.value = 1 Then xlSheet.Cells(iContador, Val(CHKbpar.Tag)) = ClearNull(aplRST!nom_bpar)
           If chkPetGestion.value = 1 Then xlSheet.Cells(iContador, Val(chkPetGestion.Tag)) = aplRST.Fields!pet_driver
           If chkRefBPE.value = 1 Then xlSheet.Cells(iContador, Val(chkRefBPE.Tag)) = ClearNull(aplRST.Fields!nomBPE)
           If chkRefBPEFecha.value = 1 Then xlSheet.Cells(iContador, Val(chkRefBPEFecha.Tag)) = IIf(Not IsNull(aplRST!fecha_BPE), Format(aplRST!fecha_BPE, "yyyy-mm-dd"), "")
           If chkPetCargaBeneficios.value = 1 Then xlSheet.Cells(iContador, Val(chkPetCargaBeneficios.Tag)) = aplRST.Fields!cargaBeneficios
           If chkPetValoracion.value = 1 Then
               xlSheet.Cells(iContador, Val(chkPetValoracion.Tag)) = CDbl(aplRST.Fields!valor_impacto)
               xlSheet.Cells(iContador, Val(chkPetValoracion.Tag) + 1) = CDbl(aplRST.Fields!valor_facilidad)
           End If
           If CHKhoraspresup.value = 1 Then xlSheet.Cells(iContador, Val(CHKhoraspresup.Tag)) = aplRST!horaspresup
           ' Horas Reales (Cabecera de Peticiones)
           If chkHorasReales.value = 1 Then
               If Not IsNull(aplRST!horasCargadas) Then
                   xlSheet.Cells(iContador, Val(chkHorasReales.Tag)) = CDbl(ClearNull(aplRST!horasCargadas))
               Else
                   xlSheet.Cells(iContador, Val(chkHorasReales.Tag)) = "=" & CDbl(0)
               End If
           End If
            If flgHijos Then
                Select Case secNivel
                    Case "GRUP"
                        If chkHorasRealesGrupo.value = 1 Then
                            If Not IsNull(aplRST.Fields!horas_grupo) Then
                                xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag)) = CDbl(aplRST.Fields!horas_grupo)
                            Else
                                xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag)) = "=" & CDbl(0)
                            End If
                        End If
                    Case "SECT"
                        If chkHorasRealesSector.value = 1 Then
                            If Not IsNull(aplRST.Fields!horas_sector) Then
                                xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag)) = CDbl(aplRST.Fields!horas_sector)
                            Else
                                xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag)) = "=" & CDbl(0)
                            End If
                        End If
                End Select
            End If
           
           If CHKfe_ini_plan.value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
           If CHKfe_fin_plan.value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
           ''se guarda para grantt
           If CodigoCombo(cboGantt, True) = "PETI" Then
               xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
               xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
               xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!cod_estado)
           Else
               xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
               xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
               xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!sec_cod_estado)
           End If
           If CHKfe_ini_real.value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_real.Tag)) = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "yyyy-mm-dd"), "")
           If CHKfe_fin_real.value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_real.Tag)) = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "yyyy-mm-dd"), "")
           If CHKfe_ini_orig.value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!fe_ini_orig), Format(aplRST!fe_ini_orig, "yyyy-mm-dd"), "")
           If CHKfe_fin_orig.value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!fe_fin_orig), Format(aplRST!fe_fin_orig, "yyyy-mm-dd"), "")
           If CHKrepla.value = 1 Then xlSheet.Cells(iContador, Val(CHKrepla.Tag)) = IIf(Val(ClearNull(aplRST!cant_planif)) < 2, "", Val(ClearNull(aplRST!cant_planif)) - 1)
           If CHKimportancia_nom.value = 1 Then xlSheet.Cells(iContador, Val(CHKimportancia_nom.Tag)) = ClearNull(aplRST!importancia_nom)
           If CHKcorp_local.value = 1 Then xlSheet.Cells(iContador, Val(CHKcorp_local.Tag)) = ClearNull(aplRST!corp_local)
           If CHKnom_orientacion.value = 1 Then xlSheet.Cells(iContador, Val(CHKnom_orientacion.Tag)) = ClearNull(aplRST!nom_orientacion)
           If flgHijos Then
               If secNivel = "GRUP" Then
                   If CHKgru_sector.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_sector.Tag)) = ClearNull(aplRST!sec_nom_sector) & " � " & ClearNull(aplRST!sec_nom_grupo)
                   If (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                       If CHKprio_ejecuc.value = 1 Then xlSheet.Cells(iContador, Val(CHKprio_ejecuc.Tag)) = ClearNull(aplRST!prio_ejecuc) & " "
                       If CHKgru_info_adic.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_info_adic.Tag)) = ClearNull(aplRST!info_adicio) & " "
                   End If
                   If CHKgru_estado.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_estado.Tag)) = ClearNull(aplRST!sec_nom_estado)
                   If CHKgru_situacion.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
                   If CHKgru_horaspresup.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
                   If CHKgru_fe_ini_plan.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
                   If CHKgru_fe_fin_plan.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
                   If CHKgru_fe_ini_real.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
                   If CHKgru_fe_fin_real.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
                   If CHKgru_fe_ini_orig.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
                   If CHKgru_fe_fin_orig.value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
                   If chkFechaProduccion.value = 1 Then xlSheet.Cells(iContador, Val(chkFechaProduccion.Tag)) = IIf(Not IsNull(aplRST!fe_produccion), Format(aplRST!fe_produccion, "yyyy-mm-dd"), "")
                   If chkFechaSuspension.value = 1 Then xlSheet.Cells(iContador, Val(chkFechaSuspension.Tag)) = IIf(Not IsNull(aplRST!fe_suspension), Format(aplRST!fe_suspension, "yyyy-mm-dd"), "")
                   If chkMotivoSuspension.value = 1 Then xlSheet.Cells(iContador, Val(chkMotivoSuspension.Tag)) = ClearNull(aplRST!nom_motivo) & " "
                   If chkObservEstado.value = 1 Then xlSheet.Cells(iContador, Val(chkObservEstado.Tag)) = ClearNull(aplRST!ObsEstado) & " "
               Else
                   If CHKsec_sector.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_sector.Tag)) = ClearNull(aplRST!sec_nom_sector)
                   If CHKsec_estado.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_estado.Tag)) = ClearNull(aplRST!sec_nom_estado) & " "
                   If CHKsec_situacion.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
                   If CHKsec_horaspresup.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
                   If CHKsec_fe_ini_plan.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
                   If CHKsec_fe_fin_plan.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
                   If CHKsec_fe_ini_real.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
                   If CHKsec_fe_fin_real.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
                   If CHKsec_fe_ini_orig.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
                   If CHKsec_fe_fin_orig.value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
               End If
           End If
           If chkPetRegulatorio.value = 1 Then xlSheet.Cells(iContador, Val(chkPetRegulatorio.Tag)) = aplRST!pet_regulatorio
           If chkPetProyectoIDM.value = 1 Then
                If ClearNull(aplRST!pet_projid & "." & aplRST!pet_projsubid & "." & aplRST!pet_projsubsid) <> ".." And ClearNull(aplRST!pet_projid & "." & aplRST!pet_projsubid & "." & aplRST!pet_projsubsid) <> "0.0.0" Then
                    xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag)) = aplRST!pet_projid & "." & aplRST!pet_projsubid & "." & aplRST!pet_projsubsid
                    xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag) + 1) = aplRST!projnom
                End If
           End If
           If chkPetPuntaje.value = 1 Then
               ' Ac� hay error de NULL
               xlSheet.Cells(iContador, Val(chkPetPuntaje.Tag)) = CDbl(aplRST!puntuacion)
           End If
           If chkPetCategoria.value = 1 Then xlSheet.Cells(iContador, Val(chkPetCategoria.Tag)) = aplRST!categoria
           aplRST.MoveNext
           If bCancelar Then GoTo finCancelacion
           DoEvents
       Next contador
       cntRows = cntRows + iContador
       
       pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "1. HECHO General"

       ' Aqu� agrega la parte de Solicitante, Referente, Autorizante y Supervisor
       If CHKsoli.value = 1 Or CHKrefe.value = 1 Or CHKsupe.value = 1 Or CHKauto.value = 1 Then
           pbExpoParcial.value = 0
           For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
               If bCancelar Then GoTo finCancelacion
               pbExpoParcial.value = pbExpoParcial.value + 1
               Call StatusLocal(sbExportar, "Solicitantes: " & pbExpoParcial.value)
               sPetic = xlSheet.Cells(iContador, colNroInterno)
               Call sp_GetUnaPeticionXt(sPetic)
               If Not aplRST.EOF Then
                   If CHKsoli.value = 1 Then xlSheet.Cells(iContador, Val(CHKsoli.Tag)) = ClearNull(aplRST!nom_solicitante) & " "
                   If CHKrefe.value = 1 Then xlSheet.Cells(iContador, Val(CHKrefe.Tag)) = ClearNull(aplRST!nom_referente) & " "
                   If CHKsupe.value = 1 Then xlSheet.Cells(iContador, Val(CHKsupe.Tag)) = ClearNull(aplRST!nom_supervisor) & " "
                   If CHKauto.value = 1 Then xlSheet.Cells(iContador, Val(CHKauto.Tag)) = ClearNull(aplRST!nom_director) & " "
               End If
               DoEvents
           Next
           pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "2. HECHO Solicitante"
       End If
       
       ' Detalles y dem�s textos...
       If CHKdetalle.value = 1 Or CHKcaracte.value = 1 Or CHKmotivos.value = 1 Or CHKrelacio.value = 1 Or CHKobserva.value = 1 Then
            pbExpoParcial.value = 0
            For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
                If bCancelar Then GoTo finCancelacion
                Call StatusLocal(sbExportar, "Detalles: " & pbExpoParcial.value)
                sPetic = xlSheet.Cells(iContador, colNroInterno)
                pbExpoParcial.value = pbExpoParcial.value + 1
                If IsNumeric(sPetic) Then
                     If CHKdetalle.value = 1 Then
                         sTtaux = sp_GetMemo(sPetic, "DESCRIPCIO")
                         sTtaux = ReplaceString(sTtaux, 13, "")
                         sTtaux = cutString(sTtaux, 10)
                         If Len(sTtaux) > 2 Then
                             sTexto = ReplaceString(sTtaux, 13, "")
                             sTexto = cutString(sTexto, 10)
                             xlSheet.Cells(iContador, Val(CHKdetalle.Tag)) = sTexto
                         Else
                             xlSheet.Cells(iContador, Val(CHKdetalle.Tag)) = " "
                         End If
                     End If
                     If CHKcaracte.value = 1 Then
                         sTtaux = sp_GetMemo(sPetic, "CARACTERIS")
                         sTtaux = ReplaceString(sTtaux, 13, "")
                         sTtaux = cutString(sTtaux, 10)
                         If Len(sTtaux) > 2 Then
                             sTexto = ReplaceString(sTtaux, 13, "")
                             sTexto = cutString(sTexto, 10)
                             xlSheet.Cells(iContador, Val(CHKcaracte.Tag)) = sTexto
                         Else
                             xlSheet.Cells(iContador, Val(CHKcaracte.Tag)) = " "
                         End If
                     End If
                     If CHKmotivos.value = 1 Then
                         sTtaux = sp_GetMemo(sPetic, "MOTIVOS")
                         sTtaux = ReplaceString(sTtaux, 13, "")
                         sTtaux = cutString(sTtaux, 10)
                         If Len(sTtaux) > 2 Then
                             sTexto = ReplaceString(sTtaux, 13, "")
                             sTexto = cutString(sTexto, 10)
                             xlSheet.Cells(iContador, Val(CHKmotivos.Tag)) = sTexto
                         Else
                             xlSheet.Cells(iContador, Val(CHKmotivos.Tag)) = " "
                         End If
                     End If
                     If CHKrelacio.value = 1 Then
                         sTtaux = sp_GetMemo(sPetic, "RELACIONES")
                         sTtaux = ReplaceString(sTtaux, 13, "")
                         sTtaux = cutString(sTtaux, 10)
                         If Len(sTtaux) > 2 Then
                             sTexto = ReplaceString(sTtaux, 13, "")
                             sTexto = cutString(sTexto, 10)
                             xlSheet.Cells(iContador, Val(CHKrelacio.Tag)) = sTexto
                         Else
                             xlSheet.Cells(iContador, Val(CHKrelacio.Tag)) = " "
                         End If
                     End If
                     If CHKobserva.value = 1 Then
                         sTtaux = sp_GetMemo(sPetic, "OBSERVACIO")
                         sTtaux = ReplaceString(sTtaux, 13, "")
                         sTtaux = cutString(sTtaux, 10)
                         If Len(sTtaux) > 2 Then
                             sTexto = ReplaceString(sTtaux, 13, "")
                             sTexto = cutString(sTexto, 10)
                             xlSheet.Cells(iContador, Val(CHKobserva.Tag)) = sTexto
                         Else
                             xlSheet.Cells(iContador, Val(CHKobserva.Tag)) = " "
                         End If
                     End If
                End If
                DoEvents
            Next
            pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "3. HECHO Detalles"
        End If
       
       ' Agrupamientos...
       If Val(glSelectAgrup) > 0 Then
           pbExpoParcial.value = 0
           For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
               If bCancelar Then GoTo finCancelacion
               pbExpoParcial.value = pbExpoParcial.value + 1
               Call StatusLocal(sbExportar, "Agrupamiento: " & pbExpoParcial.value)
               sPetic = xlSheet.Cells(iContador, colNroInterno)
               sTexto = ""
               If sp_GetAgrupPeticHijo(glSelectAgrup, sPetic) Then
                   Do While Not aplRST.EOF
                       sTexto = sTexto & ClearNull(aplRST!agr_titulo) & Chr(10)
                       aplRST.MoveNext
                       DoEvents
                   Loop
                   sTexto = cutString(sTexto, 10)
                   xlSheet.Cells(iContador, Val(txtAgrup.Tag)) = sTexto
               End If
           Next
           pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "4. HECHO Agrupamientos"
       End If

        Dim cNombreEvento As String
        
        If CHKhistorial.value = 1 Then
            pbExpoParcial.value = 0
            For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
                If bCancelar Then GoTo finCancelacion
                pbExpoParcial.value = pbExpoParcial.value + 1
                ReDim aHisto(1 To 1)
                hAux = 0
                sTexto = " "
                Call StatusLocal(sbExportar, "Historial: " & pbExpoParcial.value)
                Call sp_GetHistorialXt(xlSheet.Cells(iContador, colNroInterno), Null)
                ' Guarda en un vector todos los n�meros internos del historial para esa petici�n
                Do While Not aplRST.EOF
                    hAux = hAux + 1
                    ReDim Preserve aHisto(1 To hAux)
                    aHisto(hAux) = aplRST!hst_nrointerno
                    aplRST.MoveNext
                    DoEvents
                Loop
                ' Por cada n�mero interno busca el texto correspondiente
                For J = 1 To hAux
                    If sp_GetHistorialXt(xlSheet.Cells(iContador, colNroInterno), aHisto(J)) Then
                        sTttmp = Format(aplRST!hst_fecha, "dd/mm/yyyy hh:mm") & ", " & ClearNull(aplRST!nom_audit) & Chr(10)
                        cNombreEvento = ClearNull(aplRST.Fields!nom_accion)
                        sTtaux = sp_GetHistorialMemo(aHisto(J))
                        sTtaux = ReplaceString(sTtaux, 13, "")      ' Esto reemplaza los retornos de carro
                        ' Aqu� evito cargar en el texto del historial un registro que no tiene la descripci�n detallada del evento.
                        ' En su lugar, guardo el nombre del evento
                        If InStr(1, sTtaux, "--->", vbTextCompare) > 0 And Len(sTtaux) = 4 Then
                            sTtaux = cNombreEvento
                        End If
                        ' Aqu� evito cargar en el texto del historial un registro que no tiene datos
                        If Not (InStr(1, sTtaux, "<<>>", vbTextCompare) > 0) Or (InStr(1, sTtaux, "��", vbTextCompare) > 0) Then
                            sTexto = Trim(sTexto) & sTttmp & sTtaux & Chr(10) & Chr(10)
                        End If
                        DoEvents
                    End If
                    DoEvents
                Next
                xlSheet.Cells(iContador, Val(CHKhistorial.Tag)) = sTexto
            Next
            pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "5. HECHO Historial"
        End If
        
        ' Documentos adjuntos
        If chkPetDocu.value = 1 Then
            pbExpoParcial.value = 0
            For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
                If bCancelar Then GoTo finCancelacion
                pbExpoParcial.value = pbExpoParcial.value + 1
                sTexto = ""
                Call StatusLocal(sbExportar, "Adjuntos: " & pbExpoParcial.value)
                If sp_GetAdjuntosPet(xlSheet.Cells(iContador, colNroInterno), Null, Null, Null) Then
                    Do While Not aplRST.EOF
                        sTexto = sTexto & ClearNull(aplRST.Fields!adj_file) & Chr(10)
                        aplRST.MoveNext
                    Loop
                    xlSheet.Cells(iContador, Val(chkPetDocu.Tag)) = sTexto
                End If
                DoEvents
            Next
            pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "5. HECHO Adjuntos"
        End If
       
        ' Conformes de usuario
        If CHKpcf.value = 1 Then
            pbExpoParcial.value = 0
            Dim UltFecha As Variant
            UltFecha = Null
            pbExpoParcial.value = 0
            For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
                If bCancelar Then GoTo finCancelacion
                pbExpoParcial.value = pbExpoParcial.value + 1
                sTexto = " "
                Call StatusLocal(sbExportar, "Conformes: " & pbExpoParcial.value)
                Call sp_GetPeticionConf(xlSheet.Cells(iContador, colNroInterno), Null, Null)
                Do While Not aplRST.EOF
                    If IsNull(UltFecha) Then
                        UltFecha = aplRST.Fields!audit_date
                    Else
                        If aplRST.Fields!audit_date > UltFecha Then
                            UltFecha = aplRST.Fields!audit_date
                        End If
                    End If
                    If Len(Trim(sTexto)) = 0 Then
                        sTexto = ClearNull(aplRST!ok_tipo) & " - " & ClearNull(aplRST!nom_audit)
                    Else
                        sTexto = ClearNull(sTexto) & Chr(10) & ClearNull(aplRST!ok_tipo) & " - " & ClearNull(aplRST!nom_audit)
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
                xlSheet.Cells(iContador, Val(CHKpcf.Tag)) = sTexto
                xlSheet.Cells(iContador, Val(CHKpcf.Tag) + 1) = IIf(IsNull(UltFecha), "", "" & Format(UltFecha, "yyyy-mm-dd"))
                UltFecha = Null
                DoEvents
            Next
            pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "6. HECHO Conformes"
        End If
        
        If chkPetConfHomo.value = 1 Then
            pbExpoParcial.value = 0
            For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
                If bCancelar Then GoTo finCancelacion
                pbExpoParcial.value = pbExpoParcial.value + 1
                Call StatusLocal(sbExportar, "Conformes homologaci�n: " & pbExpoParcial.value)
                Call sp_GetPeticionConfHomo(xlSheet.Cells(iContador, colNroInterno))
                Do While Not aplRST.EOF
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag)) = IIf(Not IsNull(aplRST!HSOB) And aplRST!HSOB > 0, "X", " ") & " "
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag + 1)) = IIf(Not IsNull(aplRST!HOME) And aplRST!HOME > 0, "X", " ") & " "
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag + 2)) = IIf(Not IsNull(aplRST!HOMA) And aplRST!HOMA > 0, "X", " ") & " "
                    aplRST.MoveNext
                Loop
                DoEvents
            Next
            pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "7. HECHO Conformes homologaci�n"
        End If
        
        If chkPetPriorizacion.value = 1 Then
            pbExpoParcial.value = 0
            For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
                If bCancelar Then GoTo finCancelacion
                pbExpoParcial.value = pbExpoParcial.value + 1
                Call StatusLocal(sbExportar, "Priorizacion: " & pbExpoParcial.value)
                If sp_GetPeticionBeneficios(xlSheet.Cells(iContador, colNroInterno), "S", CodigoCombo(frmPeticionesShellView01.cboEmp, True)) Then
                    inicio = Val(chkPetPriorizacion.Tag)
                    Do While Not aplRST.EOF
                        If IsNull(aplRST!valor_referencia) Then
                            xlSheet.Cells(iContador, inicio) = CDbl(0)
                        Else
                            xlSheet.Cells(iContador, inicio) = CDbl(aplRST!valor_referencia)
                        End If
                        inicio = inicio + 1
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
                DoEvents
            Next iContador
            pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "9. HECHO Indicadores"
        End If
        
        Dim columnaKPI As Integer
        If chkPetKPIs.value = 1 Then
            pbExpoParcial.value = 0
            For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
                If bCancelar Then GoTo finCancelacion
                pbExpoParcial.value = pbExpoParcial.value + 1
                Call StatusLocal(sbExportar, "KPIs: " & pbExpoParcial.value)
                If sp_GetPeticionKPI(xlSheet.Cells(iContador, colNroInterno), Null) Then
                    inicio = Val(chkPetKPIs.Tag)
                    Do While Not aplRST.EOF
                        For columnaKPI = inicio To (inicio + CANTIDAD_COLS_KPIS) Step 1
                            If xlSheet.Cells(ENCABEZADO_SECUNDARIO_DETALLE, columnaKPI) = ClearNull(aplRST!pet_kpiid) & ". " & ClearNull(aplRST!kpinom) Then
                                If ClearNull(xlSheet.Cells(iContador, columnaKPI)) = "" Then
                                    xlSheet.Cells(iContador, columnaKPI) = Val(ClearNull(aplRST!valorEsperado))
                                    xlSheet.Cells(iContador, columnaKPI + 1) = Val(ClearNull(aplRST!valorPartida))
                                    xlSheet.Cells(iContador, columnaKPI + 2) = Val(ClearNull(aplRST!tiempoEsperado))
                                    xlSheet.Cells(iContador, columnaKPI + 3) = ClearNull(aplRST!unimedTiempoDesc)
                                Else
                                    xlSheet.Cells(iContador, columnaKPI) = ClearNull(xlSheet.Cells(iContador, columnaKPI)) & vbCrLf & _
                                        Val(ClearNull(aplRST!valorEsperado)) & "|" & Val(ClearNull(aplRST!valorPartida)) & "|" & Val(ClearNull(aplRST!tiempoEsperado)) & "|" & ClearNull(aplRST!unimedTiempoDesc)
                                End If
                                Exit For
                            End If
                        Next columnaKPI
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
                DoEvents
            Next iContador
            pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "10. HECHO KPIs"
        End If
       
       ' Recursos asignados (en grupos)
       If secNivel = "GRUP" And secGrup <> "NULL" And CHKgru_recursos.value = 1 Then
           pbExpoParcial.value = 0
           For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 2) To cntRows
               If bCancelar Then GoTo finCancelacion
               pbExpoParcial.value = pbExpoParcial.value + 1
               Call StatusLocal(sbExportar, "Recursos:" & pbExpoParcial.value)
                If sp_GetPeticionRecurso(xlSheet.Cells(iContador, colNroInterno), secGrup) Then
                    Do While Not aplRST.EOF
                        For J = cntField + 1 To cntLast
                            If xlSheet.Cells(1, J) = ClearNull(aplRST.Fields.Item("nom_recurso")) Then
                                xlSheet.Cells(iContador, J) = "X"
                                xlSheet.Columns(J).HorizontalAlignment = xlCenter
                                Exit For
                            End If
                        Next
                        If J > cntLast Then
                            xlSheet.Cells(1, cntLast) = ClearNull(aplRST.Fields.Item("nom_recurso"))
                            xlSheet.Cells(iContador, cntLast) = "X"
                            xlSheet.Columns(cntLast).HorizontalAlignment = xlCenter
                            xlSheet.Columns(cntLast).ColumnWidth = 2
                            xlSheet.Columns(cntLast).EntireColumn.AutoFit
                            cntLast = cntLast + 1
                        End If
                        aplRST.MoveNext
                    Loop
                End If
                DoEvents
           Next
           pbExpoTotal.value = pbExpoTotal.value + 1: Debug.Print "11. HECHO Recursos (Grupos)"
       End If
       'Call StatusLocal(sbExportar, "Finalizando la exportaci�n... aguarde...")
        If CHKgantt.value = 1 Then
            Call Hacer_Gannt(xlSheet)      'comenzamos con el GANTT
        End If
        Call StatusLocal(sbExportar, "Finalizando la exportaci�n... aguarde...")
       xlSheet.Columns(colEstadoPet).Delete
       xlSheet.Columns(colFecPlanFin).Delete
       xlSheet.Columns(colFecPlanIni).Delete
       xlSheet.Columns(colNroInterno).Delete
       Call GuardarPantalla
       GoTo Continuar
    End If
Continuar:
    Call CerrarObjectoExcel(ExcelApp, ExcelWrk, xlSheet, sFileName)
    If bScreen Then
        Call EXEC_ShellExecute(Me.hWnd, sFileName)
    End If
    MsgBox "Se ha generado exitosamente el archivo: " & Trim(sFileName) & vbCrLf & "Proceso de exportaci�n finalizado.", vbInformation + vbOKOnly, "Exportaci�n finalizada"
    Call Puntero(False)
    bCancelar = True
    Exit Sub
finx:
    Call CerrarObjectoExcel(ExcelApp, ExcelWrk, xlSheet, sFileName)
    Call Puntero(False)
    bCancelar = True
    Exit Sub
finCancelacion:
    Call Puntero(False)
    'Call CerrarObjectoExcel(ExcelApp, ExcelWrk, xlSheet, sFileName)
    ExcelWrk.Close True, sFileName
    ExcelApp.Application.Quit
    Set xlSheet = Nothing
    Set ExcelWrk = Nothing
    Set ExcelApp = Nothing
    Call StatusLocal(sbExportar, "Proceso de exportaci�n cancelado por el usuario.")
    MsgBox "Proceso cancelado por el usuario.", vbExclamation + vbOKOnly
    Exit Sub
Errores:
    If Err.Number = 94 Then
        'Debug.Print Err.DESCRIPTION & " (" & Now & ") " & Err.Source        ' Uso no v�lido de NULL
        Resume Next
    End If
    If Err.Number = 1004 Then
        Debug.Print Err.DESCRIPTION & " (" & Now & ") " & Err.Source
        Resume Next
    ElseIf (Err.Number = 380 And Err.Source = "ProgCtrl") Then              ' Excede el valor MAX del progress bar...
        Debug.Print Err.DESCRIPTION & " (" & Now & ") " & Err.Source
        Resume Next
    Else
        MsgBox "No se puede generar la exportaci�n." & vbCrLf & "Descripci�n: " & Err.DESCRIPTION, vbCritical + vbOKOnly, "Error en " & Err.Source
        Set xlSheet = Nothing
        'Resume
        Set ExcelWrk = Nothing
        If Not IsNull(ExcelApp) Then
            ExcelApp.DisplayAlerts = False
            ExcelApp.Quit
            Set ExcelApp = Nothing
        End If
        bCancelar = True
        Call Puntero(False)
        'Call Status("Listo.")
'        Unload Me
        'Resume
    End If
End Sub

Private Sub Hacer_Gannt(xlSheet As Object)
    Dim auxPlanIni As Date
    Dim auxPlanFin As Date
    Dim strMes As String
    Dim flg2003 As Boolean
    Dim col As Long
    
    strMes = "EneFebMarAbrMayJunJulAgoSetOctNovDic"
    flg2003 = False
    
    Call StatusLocal(sbExportar, "Diagramando Gantt...")
    auxPlanFin = #1/1/1980#
    For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 3) To cntRows
        If iContador = 2 Or auxPlanIni = "00:00:00" Then auxPlanIni = xlSheet.Cells(iContador, colFecPlanIni)
        'averiguamos primero y �ltimo
        If xlSheet.Cells(iContador, colFecPlanIni) <> "" Then
            If xlSheet.Cells(iContador, colFecPlanIni) < auxPlanIni Then
                auxPlanIni = xlSheet.Cells(iContador, colFecPlanIni)
            End If
        End If
        If xlSheet.Cells(iContador, colFecPlanFin) <> "" Then
            If xlSheet.Cells(iContador, colFecPlanFin) > auxPlanFin Then
                auxPlanFin = xlSheet.Cells(iContador, colFecPlanFin)
            End If
        End If
    Next
    auxPlanIni = auxPlanIni - Day(auxPlanIni) + 1
    'OJO CON ESTO
    If auxPlanIni < #1/1/2004# Then
        flg2003 = True
        cntLast = cntLast + 1
        auxPlanIni = #1/1/2004#
    End If
    auxPlanFin = DateAdd("m", 1, auxPlanFin)
    auxPlanFin = auxPlanFin - Day(auxPlanFin)
    grantBeg = 0
    grantEnd = calColFecha(auxPlanIni, auxPlanFin)
    auxDate = auxPlanIni
    
    If grantEnd + cntLast > 250 Then
        MsgBox "El rango de fechas seleccionado es muy grande." & Chr$(13) & "Debe seleccione otro", vbExclamation + vbOKOnly
    End If
    
    'agrupa de a 4 columnitas
    For J = grantBeg To grantEnd Step 4
        Call StatusLocal(sbExportar, "Diagramando Gantt: " & J)
        col = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 3, 1)
        xlSheet.Range(xlSheet.Cells(col, J + cntLast), xlSheet.Cells(col, J + cntLast - 1 + 4)).MergeCells = True
        xlSheet.Cells(col, J + cntLast) = "'" & Mid(strMes, (Month(auxDate) - 1) * 3 + 1, 3) & Chr$(10) & Right("" & Year(auxDate), 2)
        auxDate = DateAdd("m", 1, auxDate)
        xlSheet.Range(xlSheet.Cells(col, J + cntLast - 1 + 4), xlSheet.Cells(cntRows, J + cntLast - 1)).Borders(10).LineStyle = 1
    Next
    
    xlSheet.Range(xlSheet.Cells(col, grantBeg + cntLast), xlSheet.Cells(1, grantEnd + cntLast)).ColumnWidth = (7 / 10)
    xlSheet.Range(xlSheet.Cells(col, grantBeg + cntLast), xlSheet.Cells(1, grantEnd + cntLast)).HorizontalAlignment = xlCenter
    xlSheet.Range(xlSheet.Cells(col, grantBeg + cntLast), xlSheet.Cells(1, grantEnd + cntLast)).Font.Bold = True    ' add new
    xlSheet.Range(xlSheet.Cells(col, cntLast), xlSheet.Cells(cntRows, grantEnd + cntLast)).Borders(9).LineStyle = 1
    xlSheet.Range(xlSheet.Cells(col, cntLast), xlSheet.Cells(cntRows, grantEnd + cntLast)).Borders(8).LineStyle = 1
    xlSheet.Range(xlSheet.Cells(col, cntLast), xlSheet.Cells(cntRows, grantEnd + cntLast)).Borders(12).LineStyle = 1
    xlSheet.Range(xlSheet.Cells(col, cntLast - 1), xlSheet.Cells(cntRows, cntLast - 1)).Borders(10).LineStyle = 1
        
    'ojo con esto
    If flg2003 Then
        cntLast = cntLast - 1
        auxPlanIni = #12/31/2003#
        xlSheet.Range(xlSheet.Cells(col, grantBeg + cntLast), xlSheet.Cells(1, grantBeg + cntLast)).ColumnWidth = (7 / 10) * 14
        xlSheet.Range(xlSheet.Cells(col, grantBeg + cntLast), xlSheet.Cells(1, grantBeg + cntLast)).HorizontalAlignment = xlCenter
        xlSheet.Range(xlSheet.Cells(col, cntLast), xlSheet.Cells(1, grantBeg + cntLast)).Font.Bold = False
        xlSheet.Cells(col, cntLast) = "'" & "A�o 2003" & Chr$(10) & "y anterior"
        xlSheet.Range(xlSheet.Cells(col, cntLast), xlSheet.Cells(cntRows, grantBeg + cntLast)).Borders(12).LineStyle = 1
        xlSheet.Range(xlSheet.Cells(col, cntLast - 1), xlSheet.Cells(cntRows, cntLast - 1)).Borders(10).LineStyle = 1
    End If

    For iContador = IIf(chkPetPriorizacion.value = 1 Or chkPetKPIs.value = 1, 4, 3) To cntRows
        Call StatusLocal(sbExportar, "Diagrama: " & iContador)
        If xlSheet.Cells(iContador, colFecPlanIni) <> "" Then
            If xlSheet.Cells(iContador, colFecPlanFin) = "" Then
                xlSheet.Cells(iContador, colFecPlanFin) = xlSheet.Cells(iContador, colFecPlanIni)
            End If
            grantBeg = calColFecha(auxPlanIni, xlSheet.Cells(iContador, colFecPlanIni))
            grantEnd = calColFecha(auxPlanIni, xlSheet.Cells(iContador, colFecPlanFin))
            
            If xlSheet.Cells(iContador, colEstadoPet) = "EJECUC" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 46 'rojo es 3
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "PLANOK" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 43 'verde
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "SUSPEN" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 15 'gris
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "REVISA" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 15 'gris
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "TERMIN" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 37  'azul
            End If
        End If
    Next
End Sub

Private Sub cmdCerrar_Click()
    If cmdCerrar.Caption = "Cancelar" Then
        Call Puntero(True)
        bCancelar = True
    Else
        Call Puntero(False)
        Unload Me
    End If
    If flgEnCarga Then Exit Sub
End Sub

Sub GuardarPantalla()
    SaveSetting "GesPet", "Export01", "CHKpet_nroasignado", IIf(CHKpet_nroasignado.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKcod_tipo_peticion", IIf(CHKcod_tipo_peticion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKtitulo", IIf(CHKtitulo.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKFechaPedido", IIf(CHKFechaPedido.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKdetalle", IIf(CHKdetalle.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKcaracte", IIf(CHKcaracte.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKmotivos", IIf(CHKmotivos.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKrelacio", IIf(CHKrelacio.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKobserva", IIf(CHKobserva.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKprioridad", IIf(CHKprioridad.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKfe_ing_comi", IIf(CHKfe_ing_comi.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKpet_estado", IIf(CHKpet_estado.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKpet_fe_estado", IIf(CHKpet_fe_estado.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKpet_situacion", IIf(CHKpet_situacion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKpet_sector", IIf(CHKpet_sector.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKbpar", IIf(CHKbpar.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsoli", IIf(CHKsoli.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKrefe", IIf(CHKrefe.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsupe", IIf(CHKsupe.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKauto", IIf(CHKauto.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKhoraspresup", IIf(CHKhoraspresup.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKfe_ini_plan", IIf(CHKfe_ini_plan.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKfe_fin_plan", IIf(CHKfe_fin_plan.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKfe_ini_real", IIf(CHKfe_ini_real.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKfe_fin_real", IIf(CHKfe_fin_real.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKfe_ini_orig", IIf(CHKfe_ini_orig.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKfe_fin_orig", IIf(CHKfe_fin_orig.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKrepla", IIf(CHKrepla.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKimportancia_nom", IIf(CHKimportancia_nom.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKcorp_local", IIf(CHKcorp_local.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKnom_orientacion", IIf(CHKnom_orientacion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_sector", IIf(CHKsec_sector.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_estado", IIf(CHKsec_estado.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_situacion", IIf(CHKsec_situacion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_horaspresup", IIf(CHKsec_horaspresup.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_plan", IIf(CHKsec_fe_ini_plan.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_plan", IIf(CHKsec_fe_fin_plan.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_real", IIf(CHKsec_fe_ini_real.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_real", IIf(CHKsec_fe_fin_real.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_orig", IIf(CHKsec_fe_ini_orig.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_orig", IIf(CHKsec_fe_fin_orig.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_sector", IIf(CHKgru_sector.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKprio_ejecuc", IIf(CHKprio_ejecuc.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_estado", IIf(CHKgru_estado.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_situacion", IIf(CHKgru_situacion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_horaspresup", IIf(CHKgru_horaspresup.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_plan", IIf(CHKgru_fe_ini_plan.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_plan", IIf(CHKgru_fe_fin_plan.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_real", IIf(CHKgru_fe_ini_real.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_real", IIf(CHKgru_fe_fin_real.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_orig", IIf(CHKgru_fe_ini_orig.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_orig", IIf(CHKgru_fe_fin_orig.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_info_adic", IIf(CHKgru_info_adic.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgru_recursos", IIf(CHKgru_recursos.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKgantt", IIf(CHKgantt.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "FileExport", ClearNull(txtFile)
    SaveSetting "GesPet", "Export01", "glSelectAgrupStr", ClearNull(glSelectAgrupStr)
    SaveSetting "GesPet", "Export01", "glSelectAgrup", ClearNull(glSelectAgrup)
    SaveSetting "GesPet", "Export01", "chkPetClass", IIf(chkPetClass.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetImpTech", IIf(chkPetImpTech.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetDocu", IIf(chkPetDocu.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkHorasReales", IIf(chkHorasReales.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkHorasRealesSector", IIf(chkHorasRealesSector.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkHorasRealesGrupo", IIf(chkHorasRealesGrupo.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "CHKpcf", IIf(CHKpcf.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkFechaProduccion", IIf(chkFechaProduccion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkFechaSuspension", IIf(chkFechaSuspension.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkMotivoSuspension", IIf(chkMotivoSuspension.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkObservEstado", IIf(chkObservEstado.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetRegulatorio", IIf(chkPetRegulatorio.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetConfHomo", IIf(chkPetConfHomo.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetProyectoIDM", IIf(chkPetProyectoIDM.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkRefBPE", IIf(chkRefBPE.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkRefBPEFecha", IIf(chkRefBPEFecha.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetValoracion", IIf(chkPetValoracion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetRO", IIf(chkPetRO.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkEmpresa", IIf(chkEmpresa.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetGestion", IIf(chkPetGestion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetCargaBeneficios", IIf(chkPetCargaBeneficios.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetPriorizacion", IIf(chkPetPriorizacion.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetKPIs", IIf(chkPetKPIs.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetPuntaje", IIf(chkPetPuntaje.value = 1, "SI", "NO")
    SaveSetting "GesPet", "Export01", "chkPetCategoria", IIf(chkPetCategoria.value = 1, "SI", "NO")
End Sub

Private Sub cmdOpen_Click()
    Dim sNewName As String
    
    sNewName = Dialogo_GuardarExportacion("GUARDAR")
    If Len(Trim(sNewName)) > 0 Then
        txtFile = sNewName
    End If
End Sub

'Private Sub Hacer_Historico()
'    Dim lSumaHoras As Long
'
'    With frmPeticionesShellView01
'        If chkHorasReales.Value = 1 Then
'            If Not sp_rptPeticion03(1, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), _
'                                    CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), Null) Then   ' upd -001- b. - Se agregan los par�metros necesarios para la consulta (se agregaron al final de la declaraci�n del procedimiento los controles para clase de petici�n e indicador de impacto tecnol�gico).
'               'GoTo finx
'            End If
'        Else
'            If Not sp_rptPeticion01(1, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), _
'                                    CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), Null) Then   ' upd -001- b. - Se agregan los par�metros necesarios para la consulta (se agregaron al final de la declaraci�n del procedimiento los controles para clase de petici�n e indicador de impacto tecnol�gico).
'               'GoTo finx
'            End If
'        End If
'    End With
'
'    Do While Not aplRST.EOF
'        iContador = iContador + 1
'        lSumaHoras = 0
'        Call status("Cantidad de peticiones exportadas: " & iContador)
'        xlSheet.Cells(iContador, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
'        If CHKpet_nroasignado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_nroasignado.Tag)) = ClearNull(aplRST!pet_nroasignado)
'        If CHKcod_tipo_peticion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKcod_tipo_peticion.Tag)) = ClearNull(aplRST!cod_tipo_peticion)
'        If chkPetClass.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetClass.Tag)) = ClearNull(aplRST!cod_clase)
'        If chkPetImpTech.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetImpTech.Tag)) = ClearNull(aplRST!pet_imptech)
'        If CHKtitulo.Value = 1 Then xlSheet.Cells(iContador, Val(CHKtitulo.Tag)) = ClearNull(aplRST!titulo)
'        If CHKprioridad.Value = 1 Then xlSheet.Cells(iContador, Val(CHKprioridad.Tag)) = ClearNull(aplRST!prioridad) & " "
'        If CHKfe_ing_comi.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ing_comi.Tag)) = IIf(Not IsNull(aplRST!fe_comite), Format(aplRST!fe_comite, "yyyy-mm-dd"), "")
'        If CHKpet_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_estado.Tag)) = ClearNull(aplRST!nom_estado)
'        If CHKpet_fe_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_fe_estado.Tag)) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
'        If CHKpet_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_situacion.Tag)) = ClearNull(aplRST!nom_situacion) & " "
'        If CHKpet_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_sector.Tag)) = ClearNull(aplRST!sol_direccion) & " >> " & ClearNull(aplRST!sol_gerencia) & " >> " & ClearNull(aplRST!sol_sector)
'        If CHKbpar.Value = 1 Then xlSheet.Cells(iContador, Val(CHKbpar.Tag)) = ClearNull(aplRST!nom_bpar)
'        If CHKhoraspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKhoraspresup.Tag)) = ClearNull(aplRST!horaspresup)
'        ' Horas Reales (Cabecera de Peticiones)
'        If chkHorasReales.Value = 1 Then
'            If Not IsNull(aplRST!Horas_Responsables) Then
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag)) = CDbl(ClearNull(aplRST!Horas_Responsables))
'            Else
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag)) = "=" & CDbl(0)
'            End If
'            If Not IsNull(aplRST!Horas_Recursos) Then
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag) + 1) = CDbl(ClearNull(aplRST!Horas_Recursos))
'            Else
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag) + 1) = "=" & CDbl(0)
'            End If
'            If chkHorasRealesOtros.Value = 1 Then
'                If Not IsNull(aplRST!Horas_Responsables_NoAsignadas) Then
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag)) = CDbl(ClearNull(aplRST!Horas_Responsables_NoAsignadas))
'                Else
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag)) = "=" & CDbl(0)
'                End If
'                If Not IsNull(aplRST!Horas_Recursos_NoAsignadas) Then
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag) + 1) = CDbl(ClearNull(aplRST!Horas_Recursos_NoAsignadas))
'                Else
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag) + 1) = "=" & CDbl(0)
'                End If
'            End If
'        End If
'        If chkHorasReales.Value = 1 Then
'            If flgHijos Then
'                If secNivel = "GRUP" Then
'                    If chkHorasRealesGrupo.Value = 1 Then
'                        ' *** Abierto por Grupos ***
'                        ' Primero las horas de responsables (para asignados)
'                        Set rsAux = sp_GetHorasReales("GRU", "RSP", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), ClearNull(aplRST!sec_cod_grupo))
'                        If Not rsAux.EOF Then
'                            Do While Not rsAux.EOF
'                                lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                                DoEvents
'                                rsAux.MoveNext
'                            Loop
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag)) = ClearNull(lSumaHoras)
'                        Else
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag)) = 0
'                        End If
'                        rsAux.Close: lSumaHoras = 0
'                        ' Luego las horas de recursos (asignados)
'                        Set rsAux = sp_GetHorasReales("GRU", "REC", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), ClearNull(aplRST!sec_cod_grupo))
'                        If Not rsAux.EOF Then
'                            Do While Not rsAux.EOF
'                                lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                                DoEvents
'                                rsAux.MoveNext
'                            Loop
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag) + 1) = ClearNull(lSumaHoras)
'                        Else
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag) + 1) = 0
'                        End If
'                        rsAux.Close: lSumaHoras = 0
'                    End If
'                ElseIf secNivel = "SECT" Then
'                    ' *** Abierto por Sectores ***
'                    ' Primero las horas de responsables (asignadas)
'                    Set rsAux = sp_GetHorasReales("SEC", "RSP", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), Null)
'                    If Not rsAux.EOF Then
'                        Do While Not rsAux.EOF
'                            lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                            DoEvents
'                            rsAux.MoveNext
'                        Loop
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag)) = ClearNull(lSumaHoras)
'                    Else
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag)) = 0
'                    End If
'                    rsAux.Close: lSumaHoras = 0
'                    ' Luego las horas de recursos
'                    Set rsAux = sp_GetHorasReales("SEC", "REC", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), Null)
'                    If Not rsAux.EOF Then
'                        Do While Not rsAux.EOF
'                            lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                            DoEvents
'                            rsAux.MoveNext
'                        Loop
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag) + 1) = ClearNull(lSumaHoras)
'                    Else
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag) + 1) = 0
'                    End If
'                    rsAux.Close: lSumaHoras = 0
'                End If
'            End If
'        End If
'        If CHKfe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
'        If CHKfe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
'        ' Se guarda para grantt
'        If CodigoCombo(cboGantt, True) = "PETI" Then
'            xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!cod_estado)
'        Else
'            xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!sec_cod_estado)
'        End If
'        If CHKfe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_real.Tag)) = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "yyyy-mm-dd"), "")
'        If CHKfe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_real.Tag)) = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "yyyy-mm-dd"), "")
'        If CHKfe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!fe_ini_orig), Format(aplRST!fe_ini_orig, "yyyy-mm-dd"), "")
'        If CHKfe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!fe_fin_orig), Format(aplRST!fe_fin_orig, "yyyy-mm-dd"), "")
'        If CHKrepla.Value = 1 Then xlSheet.Cells(iContador, Val(CHKrepla.Tag)) = IIf(Val(ClearNull(aplRST!cant_planif)) < 2, "", Val(ClearNull(aplRST!cant_planif)) - 1)
'        If CHKimportancia_nom.Value = 1 Then xlSheet.Cells(iContador, Val(CHKimportancia_nom.Tag)) = ClearNull(aplRST!importancia_nom)
'        If CHKcorp_local.Value = 1 Then xlSheet.Cells(iContador, Val(CHKcorp_local.Tag)) = ClearNull(aplRST!corp_local)
'        If CHKnom_orientacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKnom_orientacion.Tag)) = ClearNull(aplRST!nom_orientacion)
'        If CHKseguimN.Value = 1 Then xlSheet.Cells(iContador, Val(CHKseguimN.Tag)) = ClearNull(aplRST!prj_nrointerno)
'        If CHKseguimT.Value = 1 Then xlSheet.Cells(iContador, Val(CHKseguimT.Tag)) = ClearNull(aplRST!prj_titulo)
'        If flgHijos Then
'            If secNivel = "GRUP" Then
'                If CHKgru_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_sector.Tag)) = ClearNull(aplRST!sec_nom_sector) & ">> " & ClearNull(aplRST!sec_nom_grupo)
'                If (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
'                    If CHKprio_ejecuc.Value = 1 Then xlSheet.Cells(iContador, Val(CHKprio_ejecuc.Tag)) = ClearNull(aplRST!prio_ejecuc) & " "
'                    If CHKgru_info_adic.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_info_adic.Tag)) = ClearNull(aplRST!info_adicio) & " "
'                End If
'                If CHKgru_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_estado.Tag)) = ClearNull(aplRST!sec_nom_estado)
'                If CHKgru_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
'                If CHKgru_horaspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
'                If CHKgru_fe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'                If CHKgru_fe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'                If CHKgru_fe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
'                If CHKgru_fe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
'                If CHKgru_fe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
'                If CHKgru_fe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
'                If chkFechaProduccion.Value = 1 Then xlSheet.Cells(iContador, Val(chkFechaProduccion.Tag)) = IIf(Not IsNull(aplRST!fe_produccion), Format(aplRST!fe_produccion, "yyyy-mm-dd"), "")
'                If chkFechaSuspension.Value = 1 Then xlSheet.Cells(iContador, Val(chkFechaSuspension.Tag)) = IIf(Not IsNull(aplRST!fe_suspension), Format(aplRST!fe_suspension, "yyyy-mm-dd"), "")
'                If chkMotivoSuspension.Value = 1 Then xlSheet.Cells(iContador, Val(chkMotivoSuspension.Tag)) = ClearNull(aplRST!nom_motivo) & " "
'                If chkObservEstado.Value = 1 Then xlSheet.Cells(iContador, Val(chkObservEstado.Tag)) = ClearNull(aplRST!ObsEstado) & " "
'            Else
'                If CHKsec_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_sector.Tag)) = ClearNull(aplRST!sec_nom_sector)
'                If CHKsec_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_estado.Tag)) = ClearNull(aplRST!sec_nom_estado) & " "
'                If CHKsec_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
'                If CHKsec_horaspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
'                If CHKsec_fe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'                If CHKsec_fe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'                If CHKsec_fe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
'                If CHKsec_fe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
'                If CHKsec_fe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
'                If CHKsec_fe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
'            End If
'        End If
'        If chkPetDocu.Value = 1 Then
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag)) = IIf(ClearNull(aplRST!pet_mod1None) = 0, "No hay adjuntos", "Tiene adjuntos") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 1)) = IIf(Not IsNull(aplRST!pet_mod1C100) And aplRST!pet_mod1C100 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 2)) = IIf(Not IsNull(aplRST!pet_mod1C102) And aplRST!pet_mod1C102 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 3)) = IIf(Not IsNull(aplRST!pet_mod1P950) And aplRST!pet_mod1P950 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 4)) = IIf(Not IsNull(aplRST!pet_mod1C204) And aplRST!pet_mod1C204 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 5)) = IIf(Not IsNull(aplRST!pet_mod1CG04) And aplRST!pet_mod1CG04 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 6)) = IIf(Not IsNull(aplRST!pet_mod1T710) And aplRST!pet_mod1T710 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 7)) = IIf(Not IsNull(aplRST!pet_mod1EML1) And aplRST!pet_mod1EML1 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 8)) = IIf(Not IsNull(aplRST!pet_mod1EML2) And aplRST!pet_mod1EML2 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 9)) = IIf(Not IsNull(aplRST!pet_mod1IDHX) And aplRST!pet_mod1IDHX > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10)) = IIf(Not IsNull(aplRST!pet_mod1OTRO) And aplRST!pet_mod1OTRO > 0, "X", " ") & " "
'            ' Para las peticiones del viejo modelo de control, se agregan a OTROs
'            If aplRST!pet_mod1OTRO = 0 Then
'                If Not Trim(xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10))) = "X" Then
'                    xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10)) = IIf(Not IsNull(aplRST!pet_mod0TODO) And aplRST!pet_mod0TODO > 0, "X", " ") & " "
'                End If
'            End If
'        End If
'        '}
'        If chkPetRegulatorio.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetRegulatorio.Tag)) = ClearNull(aplRST!pet_regulatorio)    ' add -009- a.
'        If chkPetProyectoIDM.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag)) = ClearNull(aplRST!pet_projid) & "." & ClearNull(aplRST!pet_projsubid) & "." & ClearNull(aplRST!pet_projsubsid)
'        If chkPetProyectoIDM.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag + 1)) = ClearNull(aplRST!ProjNom)
'        aplRST.MoveNext
'        DoEvents
'    Loop
'End Sub
''}

'Private Sub Command1_Click()
'       Dim aButtons(2) As String
'       aButtons(0) = "Go"
'       aButtons(1) = "Come"
'       aButtons(2) = "???"
'
'       Caption = aButtons(MsgBoxEx("Text" & vbCrLf & "More Text" & vbCrLf & "Even More Text", vbCustom, "Title", , , aButtons, aButtons(1), 0, 0, 200, 300, vbWhite, vbBlue))
'End Sub

