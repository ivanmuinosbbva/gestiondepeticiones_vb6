VERSION 5.00
Begin VB.Form frmHelp 
   Caption         =   "Ayuda"
   ClientHeight    =   6045
   ClientLeft      =   3135
   ClientTop       =   3465
   ClientWidth     =   4770
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHelp.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6045
   ScaleWidth      =   4770
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtHelp 
      BackColor       =   &H80000018&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5955
      Left            =   60
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   60
      Width           =   4635
   End
End
Attribute VB_Name = "frmHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit
Option Compare Text

Private Sub Form_Load()
    Dim sHelp As String
    
    sHelp = sHelp & "Agrupamientos de Peticiones" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Definici�n" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Se denomina 'agrupamiento' a un conjunto de peticiones establecido por un usuario con el objeto de poder visualizar f�cilmente -en forma conjunta- las peticiones que lo conforman." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "El usuario define para el agrupamiento un nombre que represente aquella caracter�stica que tienen en com�n las peticiones que incorporar� y seguidamente procede a incorporar en �l las peticiones que considera necesario." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Por ejemplo, un usuario puede crear un agrupamiento denominado 'Seguimiento Semanal'. Luego, el usuario incorpora en �l todas aquellas peticiones cuyo estado desea monitorear una vez a la semana. Una vez hecho esto, el usuario podr� en cualquier momento visualizar r�pidamente el conjunto de las peticiones que �l mismo incorpor� al agrupamiento." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Otros ejemplos de posibles agrupamientos de peticiones:" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "- 'Seguimiento Intensivo Sector XXX'" & vbCrLf
    sHelp = sHelp & "- 'Peque�o', 'Mediano', 'Grande'" & vbCrLf
    sHelp = sHelp & "- 'Comercial', 'Financiero', 'Regulatorio', etc." & vbCrLf
    sHelp = sHelp & "- 'Sergio', 'Karina', 'Silvia', etc." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Una petici�n puede estar incluida simult�neamente en m�s de un agrupamiento." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "El creador de un agrupamiento es el due�o del mismo y s�lo �l puede modificar los atributos que lo definen." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Organizaci�n jer�rquica" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Los agrupamientos pueden ser organizados en forma jer�rquica, de modo que un agrupamiento puede tener uno o m�s agrupamientos 'hijos' (sub-agrupamientos). Luego, toda vez que se realice una consulta filtrando por un agrupamiento que tiene hijos, el usuario visualizar� en forma conjunta todas las peticiones incluidas tanto en el agrupamiento padre como en sus agrupamientos hijos." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "No existen restricciones a la cantidad de niveles jer�rquicos en que el usuario puede organizar sus agrupamientos." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Uso de agrupamientos en la aplicaci�n" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Un agrupamiento puede ser utilizado por el usuario como filtro adicional en las consultas de peticiones. Asimismo, desde una petici�n cualquiera es posible consultar los agrupamientos que �sta integra." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Desde el ABM de agrupamientos es posible:" & vbCrLf
    sHelp = sHelp & "- Crear, modificar o eliminar un agrupamiento." & vbCrLf
    sHelp = sHelp & "- Declarar sub-agrupamientos (hijos de un agrupamiento)." & vbCrLf
    sHelp = sHelp & "- Incorporar peticiones a un agrupamiento." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Desde la Consulta General de Peticiones es posible:" & vbCrLf
    sHelp = sHelp & "- Realizar consultas combinando el filtro por agrupamiento con los dem�s filtros disponibles." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Desde la consulta a la que se accede 'por perfil' es posible:" & vbCrLf
    sHelp = sHelp & "- Realizar consultas combinando el filtro por agrupamiento con el filtro por estado." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Desde la petici�n es posible:" & vbCrLf
    sHelp = sHelp & "- Visualizar los agrupamientos a los que pertenece la petici�n." & vbCrLf
    sHelp = sHelp & "- Incorporar la petici�n a un agrupamiento." & vbCrLf
    sHelp = sHelp & "- Quitar la petici�n de un agrupamiento." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Nivel de visibilidad" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Todos los usuarios de la aplicaci�n se encuentran ubicados en alg�n punto del organigrama, sea a nivel Direcci�n, Gerencia, Sector o Grupo." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "El creador de un agrupamiento define el 'nivel de visibilidad' que tendr� el mismo, que puede ser:" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "- Privado: lo ve �nicamente el usuario que cre� el agrupamiento." & vbCrLf
    sHelp = sHelp & "- Grupo: lo ven todos los usuarios pertenecientes al Grupo de quien cre� el agrupamiento." & vbCrLf
    sHelp = sHelp & "- Sector: lo ven todos los usuarios pertenecientes al Sector de quien cre� el agrupamiento." & vbCrLf
    sHelp = sHelp & "- Gerencia: lo ven todos los usuarios pertenecientes a la Gerencia de quien cre� el agrupamiento." & vbCrLf
    sHelp = sHelp & "- Direcci�n: lo ven todos los usuarios pertenecientes a la Direcci�n de quien cre� el agrupamiento." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Adicionalmente, los agrupamientos con visibilidad a nivel Grupo, Sector, Gerencia o Direcci�n tambi�n pueden ser vistos por los usuarios que est�n ubicados en el organigrama en un nivel superior al nivel de visibilidad informado. " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Por ejemplo, un agrupamiento creado con Nivel de Visibilidad 'Sector' (lo ven todos los usuarios abarcados por el Sector de quien lo cre�) ser� visto tambi�n por el Gerente y el Director correspondientes al Sector. En cambio, los agrupamientos Privados s�lo pueden ser vistos por el usuario que los cre�." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "El Administrador puede definir tambi�n agrupamientos 'P�blicos', los cuales pueden ser visualizados por todos los usuarios de la aplicaci�n." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Nivel para incorporaci�n de peticiones" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Adem�s del citado nivel de visibilidad, el creador de un agrupamiento debe establecer el nivel a partir del cual se admitir� incorporar peticiones al mismo. Por ejemplo, un usuario podr�a crear un agrupamiento con nivel de visibilidad 'Sector' y nivel de incorporaci�n de peticiones 'Privado'. En este caso, todos los usuarios del sector podr�n ver el agrupamiento, pero s�lo el creador podr� incorporar peticiones al mismo." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "En cambio, si el usuario estableciera para el agrupamiento que el nivel de incorporaci�n de peticiones es 'Sector', todos los usuarios que integran el sector podr�n incorporar peticiones a ese agrupamiento. Adicionalmente, podr�n hacerlo tambi�n el Gerente y el Director correspondientes a ese Sector." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Para un agrupamiento que tiene hijos el due�o puede, si lo considera conveniente, inhibir la posibilidad de que le sean incorporadas peticiones. De ese modo, s�lo los hijos admitir�n peticiones, mientras que el padre servir� como un organizador e integrador de sus agrupamientos hijos." & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Ejemplo:" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "Padre    -->   Seguimiento           (no admite peticiones)" & vbCrLf
    sHelp = sHelp & "Hijo     -->   Seguimiento Diario    (admite peticiones)" & vbCrLf
    sHelp = sHelp & "Hijo     -->   Seguimiento Semanal   (admite peticiones)" & vbCrLf
    sHelp = sHelp & " " & vbCrLf
    sHelp = sHelp & "En este caso, las peticiones ser�n incorporadas s�lo en los agrupamientos hijos, pero tanto el padre como los hijos podr�n ser utilizados como filtro en las consultas." & vbCrLf
    
    txtHelp.text = sHelp
    'txtHelp.Locked = False
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> 1 Then
        txtHelp.Height = Me.ScaleHeight - 100
        txtHelp.Width = Me.ScaleWidth - 100
    End If
End Sub
