VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmAcercaNovedades 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Novedades de la versi�n"
   ClientHeight    =   7665
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12360
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAcercaNovedades.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7665
   ScaleWidth      =   12360
   ShowInTaskbar   =   0   'False
   Begin RichTextLib.RichTextBox rtfNovedades 
      Height          =   6975
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   12255
      _ExtentX        =   21616
      _ExtentY        =   12303
      _Version        =   393217
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"frmAcercaNovedades.frx":058A
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   11400
      TabIndex        =   0
      Top             =   7200
      Width           =   885
   End
End
Attribute VB_Name = "frmAcercaNovedades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Call Inicializar
    Call CargarNovedades
End Sub

Private Sub Inicializar()
    Me.Top = 0
    Me.Left = 0
    With rtfNovedades
        .BackColor = Me.BackColor
    End With
End Sub

Private Sub CargarNovedades()
    On Error GoTo Errores
    With rtfNovedades
        .LoadFile App.Path & "\versiones.rtf", 0
    End With
Exit Sub
Errores:
    Select Case Err.Number
        Case 321, 75    ' Formato de archivo no v�lido / Error de acceso a la ruta o al archivo
            MsgBox "No puede cargarse el archivo de novedades.", vbExclamation + vbOKOnly
            cmdOK_Click
        Case Else
            MsgBox "No puede cargarse el archivo de novedades.", vbExclamation + vbOKOnly
    End Select
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub
