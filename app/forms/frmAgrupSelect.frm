VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAgrupSelect 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccione un agrupamiento"
   ClientHeight    =   7395
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8355
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7395
   ScaleWidth      =   8355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab sstAgrupamientos 
      Height          =   6015
      Left            =   60
      TabIndex        =   14
      Top             =   1080
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   10610
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Agrupamientos"
      TabPicture(0)   =   "frmAgrupSelect.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "tvDatos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Referencias"
      TabPicture(1)   =   "frmAgrupSelect.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "imgReferencias(0)"
      Tab(1).Control(1)=   "lblReferencias(0)"
      Tab(1).Control(2)=   "imgReferencias(1)"
      Tab(1).Control(3)=   "lblReferencias(1)"
      Tab(1).Control(4)=   "imgReferencias(2)"
      Tab(1).Control(5)=   "lblReferencias(2)"
      Tab(1).Control(6)=   "imgReferencias(4)"
      Tab(1).Control(7)=   "lblReferencias(4)"
      Tab(1).ControlCount=   8
      Begin MSComctlLib.TreeView tvDatos 
         Height          =   5595
         Left            =   60
         TabIndex        =   15
         Top             =   360
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   9869
         _Version        =   393217
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento (con hijos)"
         Height          =   195
         Index           =   4
         Left            =   -74340
         TabIndex        =   19
         Top             =   1200
         Width           =   1800
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   4
         Left            =   -74760
         Picture         =   "frmAgrupSelect.frx":0038
         Top             =   1140
         Width           =   240
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento (sin hijos)"
         Height          =   195
         Index           =   2
         Left            =   -74340
         TabIndex        =   18
         Top             =   1500
         Width           =   1740
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   2
         Left            =   -74760
         Picture         =   "frmAgrupSelect.frx":05C2
         Top             =   1470
         Width           =   240
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento restringido (solo para uso del usuario que lo di� de alta)"
         Height          =   195
         Index           =   1
         Left            =   -74340
         TabIndex        =   17
         Top             =   900
         Width           =   5040
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   1
         Left            =   -74760
         Picture         =   "frmAgrupSelect.frx":0B4C
         Top             =   900
         Width           =   240
      End
      Begin VB.Label lblReferencias 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento cerrado (no admite el agregado de peticiones)"
         Height          =   195
         Index           =   0
         Left            =   -74340
         TabIndex        =   16
         Top             =   600
         Width           =   4365
      End
      Begin VB.Image imgReferencias 
         Height          =   240
         Index           =   0
         Left            =   -74760
         Picture         =   "frmAgrupSelect.frx":10D6
         Top             =   570
         Width           =   240
      End
   End
   Begin MSComctlLib.ImageList imgList 
      Left            =   7080
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   17
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":1660
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":1BFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":2194
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":272E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":2CC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":3262
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":37FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":3D96
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":4330
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":48CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":4E64
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":53FE
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":5998
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":5F32
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":64CC
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":6A66
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmAgrupSelect.frx":7000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar sbMain 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   7110
      Width           =   8355
      _ExtentX        =   14737
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraFiltros 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   8295
      Begin VB.CommandButton cmdFiltro 
         Enabled         =   0   'False
         Height          =   315
         Left            =   7860
         Picture         =   "frmAgrupSelect.frx":759A
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Actualizar listado de agrupamientos"
         Top             =   210
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.ComboBox cboNivel 
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   7
         ToolTipText     =   "Visibilidad"
         Top             =   210
         Width           =   1395
      End
      Begin VB.ComboBox cboGrup 
         Enabled         =   0   'False
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   600
         Width           =   7575
      End
      Begin VB.ComboBox cboDire 
         Enabled         =   0   'False
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   600
         Width           =   7575
      End
      Begin VB.ComboBox cboSect 
         Enabled         =   0   'False
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   600
         Width           =   7575
      End
      Begin VB.ComboBox cboGere 
         Enabled         =   0   'False
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   600
         Width           =   7575
      End
      Begin VB.Label lblArea 
         AutoSize        =   -1  'True
         Caption         =   "�rea"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   660
         Width           =   345
      End
      Begin VB.Label lblNivel 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   135
         TabIndex        =   1
         Top             =   270
         Width           =   345
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5835
      Left            =   7290
      TabIndex        =   3
      Top             =   1280
      Width           =   1035
      Begin VB.Timer tmrFrame 
         Left            =   120
         Top             =   720
      End
      Begin VB.CommandButton cmdAlta 
         Caption         =   "Nuevo"
         Enabled         =   0   'False
         Height          =   375
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Permite la creaci�n de un nuevo Agrupamiento"
         Top             =   180
         Visible         =   0   'False
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   5370
         Width           =   885
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "Aceptar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   4980
         Width           =   885
      End
   End
End
Attribute VB_Name = "frmAgrupSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 30.03.2009 - Se mejora la dinamica y el frontend del formulario y manejo de agrupamientos.
' -003- a. FJS 05.05.2016 - Nuevo: se inhabilita el timer.

Option Explicit

Public xNivelModi As String, xAreaModi As String, xNivelVisi As String

Dim xPerfil As String
Dim xNivelView As String, xAreaView As String
Dim xDir As String, xGer As String, xSec As String, xGru As String

Const colNroPadre = 0
Const colTitulo = 1
Const colNOMBRERECURSO = 2
Const colVisibilidad = 3
Const colAdmpet = 4
Const colActualiza = 5
Const colESTADO = 6
Const colCodigoRecurso = 7
Const colNroInterno = 8
Const colEsSelec = 9

Private Sub Form_Load()
    'tmrFrame.Enabled = False           ' del -003- a.
    Call LockProceso(True)
    xDir = glLOGIN_Direccion
    xGer = glLOGIN_Gerencia
    xSec = glLOGIN_Sector
    xGru = glLOGIN_Grupo
    If glUsrPerfilActual = "SADM" Then
        glModoSelectAgrup = "SADM"
        xGer = ""
        xSec = ""
        xGru = ""
    End If
    If glModoSelectAgrup = "EXPO" Then
        Me.Caption = "Seleccione un Agrupamiento para incluir en la exportaci�n"
        cboNivel.ToolTipText = "Criterio de visibilidad"
        'lblNivel.Caption = "Nivel Visibilidad"
        cboNivel.Enabled = True
        cmdFiltro.visible = True
    End If
    If glModoSelectAgrup = "VIEW" Then
        Me.Caption = "Seleccione un Agrupamiento para consulta de Peticiones"
        cboNivel.ToolTipText = "Criterio de visibilidad"
        'lblNivel.Caption = "Nivel Visibilidad"
        cboNivel.Enabled = True
        cmdFiltro.visible = True
    End If
    If glModoSelectAgrup = "MODI" Then
        Me.Caption = "Seleccione un Agrupamiento para incorporar Peticiones"
        cboNivel.ToolTipText = "Nivel a partir del cual pueden incorporarse Peticiones al Agrupamiento"
        'lblNivel.Caption = "Nivel Visibilidad"
        cboNivel.Enabled = True
        cmdFiltro.visible = True
    End If
    If glModoSelectAgrup = "OWNR" Then
        Me.Caption = "Seleccione un agrupamiento para incorporar a otro agrupamiento"
        lblNivel.visible = False
        lblArea.visible = False
        cboNivel.ToolTipText = ""
        cboNivel.visible = False
        cmdFiltro.visible = False
        cboDire.visible = False
        cboGere.visible = False
        cboSect.visible = False
        cboGrup.visible = False
    End If
    If glModoSelectAgrup = "SADM" Then
        Me.Caption = "Seleccione un Agrupamiento"
        cboNivel.ToolTipText = "Criterio de visibilidad"
        'lblNivel.Caption = "Nivel Visibilidad"
        cboNivel.Enabled = True
        cmdFiltro.visible = True
    End If
    Call InicializarCombos
    Call HabilitarBotones
    Call MostrarSeleccion(1)
    Call LockProceso(False)
    '{ del -003- a.
    'tmrFrame.interval = 500
    'tmrFrame.Enabled = True
    '}
''''    Call InicializarPantalla
''''    Call LockProceso(False)
End Sub

Private Sub cboNivel_Click()
    Dim xNv As String
    If Not LockProceso(True) Then
        Exit Sub
    End If
   
    xNv = CodigoCombo(cboNivel, True)
    Call setHabilCtrl(cboDire, "DIS")
    Call setHabilCtrl(cboGere, "DIS")
    Call setHabilCtrl(cboSect, "DIS")
    Call setHabilCtrl(cboGrup, "DIS")
    cboDire.ListIndex = -1
    cboGere.ListIndex = -1
    cboSect.ListIndex = -1
    cboGrup.ListIndex = -1
    Select Case xNv
        Case "USR"
        Case "DIR"
            cboDire.visible = True
            cboGere.visible = False
            cboSect.visible = False
            cboGrup.visible = False
            Call setHabilCtrl(cboDire, "NOR")
            Call SetCombo(cboDire, xDir, True)
            If cboDire.ListIndex = -1 Then
                cboDire.ListIndex = 0
            End If
        Case "GER"
            cboDire.visible = False
            cboGere.visible = True
            cboSect.visible = False
            cboGrup.visible = False
            Call setHabilCtrl(cboGere, "NOR")
            Call SetCombo(cboGere, xDir & xGer, True)
            If cboGere.ListIndex = -1 Then
                cboGere.ListIndex = 0
            End If
        Case "SEC"
            cboDire.visible = False
            cboGere.visible = False
            cboSect.visible = True
            cboGrup.visible = False
            Call setHabilCtrl(cboSect, "NOR")
            Call SetCombo(cboSect, xDir & xGer & xSec, True)
            If cboSect.ListIndex = -1 Then
                cboSect.ListIndex = 0
            End If
        Case "GRU"
            cboDire.visible = False
            cboGere.visible = False
            cboSect.visible = False
            cboGrup.visible = True
            Call setHabilCtrl(cboGrup, "NOR")
            Call SetCombo(cboGrup, xDir & xGer & xSec & xGru, True)
            If cboGrup.ListIndex = -1 Then
                cboGrup.ListIndex = 0
            End If
        Case "PUB"
    End Select
    Call CargarGrid
    Call LockProceso(False)
End Sub

Private Sub cmdAlta_Click()
    glNumeroAgrup = ""
    glCodigoRecurso = glLOGIN_ID_REEMPLAZO
    glModoAgrup = "ALTAX"
    frmAgrupCarga.Show vbModal
    glNumeroAgrup = ""
    DoEvents
    If Me.Tag = "REFRESH" Then
        Call CargarGrid
    End If
End Sub

Private Sub cmdFiltro_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glSelectAgrup = ""
    Call CargarGrid
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    Call LockProceso(False)
    Unload Me
End Sub

Private Sub cmdVisualizar_Click()
    If glSelectAgrup = "" Then
        Exit Sub
    End If
    If Not LockProceso(True) Then
        Exit Sub
    End If
    DoEvents
    Call LockProceso(False)
    Unload Me
End Sub

Sub HabilitarBotones()
    If glModoAgrup = "ALTAX" Then
        cmdAlta.visible = True
    Else
        cmdAlta.visible = False
    End If
    cmdFiltro.Enabled = True
    cmdCerrar.visible = True
    cmdVisualizar.visible = True
    cmdCerrar.Enabled = True
End Sub

Sub InicializarCombos()
    Dim I As Integer
    Dim xAreaCod As String, xAreaCodX As String, xAreaNom As String
    Dim xDire As String, xGere As String, xSect As String, xGrup As String
    
    cboNivel.Clear
    cboNivel.AddItem "Privado" & Space(30) & "||USR"
 
    If glModoSelectAgrup = "OWNR" Then
        cboNivel.ListIndex = 0
        Call CargarGrid                 ' add
        Exit Sub
    End If
    
    Dim iNx As Integer
    
    cboNivel.AddItem getDescNvAgrup("PUB") & Space(30) & "||PUB"
    cboNivel.AddItem getDescNvAgrup("DIR") & Space(30) & "||DIR"
    cboNivel.AddItem getDescNvAgrup("GER") & Space(30) & "||GER"
    cboNivel.AddItem getDescNvAgrup("SEC") & Space(30) & "||SEC"
    cboNivel.AddItem getDescNvAgrup("GRU") & Space(30) & "||GRU"
    'cboNivel.ListIndex = 1     ' del -002- a.
    cboNivel.ListIndex = 0     ' add -002- a.
    
    'Call Status("Cargando areas...")
    sbMain.SimpleText = "Cargando areas..."
    If sp_rptEstructura(IIf(xDir <> "", xDir, "NULL"), IIf(xGer <> "", xGer, "NULL"), IIf(xSec <> "", xSec, "NULL"), IIf(xGru <> "", xGru, "NULL"), "2") Then
        Do While Not aplRST.EOF
            iNx = 0
            xAreaCod = ""
            xAreaCodX = ""
            xAreaNom = ""
            If ClearNull(aplRST!cod_direccion) <> "" Then
                xAreaCodX = ClearNull(aplRST!cod_direccion)
                xAreaCod = ClearNull(aplRST!cod_direccion)
                'xAreaNom = xAreaNom & ClearNull(aplRST!nom_direccion)
                xAreaNom = xAreaNom & IIf(ClearNull(aplRST!abrev_direccion) <> "", ClearNull(aplRST!abrev_direccion), ClearNull(aplRST!nom_direccion))
                iNx = iNx + 1
            End If
            If ClearNull(aplRST!cod_gerencia) <> "" Then
                xAreaCodX = xAreaCodX & ClearNull(aplRST!cod_gerencia)
                xAreaCod = ClearNull(aplRST!cod_gerencia)
                'xAreaNom = xAreaNom & " � " & ClearNull(aplRST!nom_gerencia)
                xAreaNom = xAreaNom & " � " & IIf(ClearNull(aplRST!abrev_gerencia) <> "", ClearNull(aplRST!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                iNx = iNx + 1
            End If
            If ClearNull(aplRST!cod_sector) <> "" Then
                xAreaCod = ClearNull(aplRST!cod_sector)
                xAreaCodX = xAreaCodX & ClearNull(aplRST!cod_sector)
                xAreaNom = xAreaNom & " � " & ClearNull(aplRST!nom_sector)
                iNx = iNx + 1
            End If
            If ClearNull(aplRST!cod_grupo) <> "" Then
                xAreaCod = ClearNull(aplRST!cod_grupo)
                xAreaCodX = xAreaCodX & ClearNull(aplRST!cod_grupo)
                xAreaNom = xAreaNom & " � " & ClearNull(aplRST!nom_grupo)
                iNx = iNx + 1
            End If
            
            Select Case iNx
                Case 1
                    cboDire.AddItem xAreaNom & ESPACIOS & ESPACIOS & "||" & xAreaCodX & "|DIR|!" & xAreaCod & "!"
                    xDire = xDire & "!" & xAreaCod
                Case 2
                    cboGere.AddItem xAreaNom & ESPACIOS & ESPACIOS & "||" & xAreaCodX & "|GER|!" & xAreaCod & "!"
                    xGere = xGere & "!" & xAreaCod
                Case 3
                    cboSect.AddItem xAreaNom & ESPACIOS & ESPACIOS & "||" & xAreaCodX & "|SEC|!" & xAreaCod & "!"
                    xSect = xSect & "!" & xAreaCod
                Case 4
                    cboGrup.AddItem xAreaNom & ESPACIOS & ESPACIOS & "||" & xAreaCodX & "|GRU|!" & xAreaCod & "!"
                    xGrup = xGrup & "!" & xAreaCod
            End Select
            aplRST.MoveNext
        Loop
        'aplRST.Close
        If cboDire.ListCount > 1 And Len(xDire) < 250 Then
            cboDire.AddItem "<Todas>" & Space(140) & "||ALL" & "|DIR|" & xDire & "!", 0
        End If
        If cboDire.ListCount = 0 Then
            cboDire.AddItem "<No posee direcciones para visualizar>" & Space(130) & "||ALL" & "|DIR|NONE!", 0
        End If
        If cboGere.ListCount > 1 And Len(xGere) < 250 Then
            cboGere.AddItem "<Todas>" & Space(140) & "||ALL" & "|GER|" & xGere & "!", 0
        End If
        If cboGere.ListCount = 0 Then
            cboGere.AddItem "<No posee gerencias para visualizar>" & Space(130) & "||ALL" & "|GER|NONE!", 0
        End If
        If cboSect.ListCount > 1 And Len(xSect) < 250 Then
            cboSect.AddItem "<Todos>" & Space(140) & "||ALL" & "|SEC|" & xSect & "!", 0
        End If
        If cboSect.ListCount = 0 Then
            cboSect.AddItem "<No posee sectores para visualizar>" & Space(130) & "||SEC" & "|DIR|NONE!", 0
        End If
        If cboGrup.ListCount > 1 And Len(xGrup) < 250 Then
            cboGrup.AddItem "<Todos>" & Space(130) & "||ALL" & "|GRU|" & xGrup & "!", 0
        End If
        If cboGrup.ListCount = 0 Then
            cboGrup.AddItem "<No posee grupos para visualizar>" & Space(130) & "||ALL" & "|GRU|NONE!", 0
        End If
    End If
    Call setHabilCtrl(cboDire, "DIS")
    Call setHabilCtrl(cboGere, "DIS")
    Call setHabilCtrl(cboSect, "DIS")
    Call setHabilCtrl(cboGrup, "DIS")
    sbMain.SimpleText = "Listo."
    'Call Status("Listo.")
End Sub

Sub CargarGrid()
    Const vNroIn = 1
    Const vNroPa = 2
    Const vVisib = 3
    Const vActua = 4
    Const vDirec = 5
    Const vGeren = 6
    Const vSecto = 7
    Const vGrupo = 8
    Const vUsual = 9
    Const vAdmPe = 10
    Const vTitul = 11
    Const vVigen = 12
    Const vNivel = 13
    
    Dim vRetorno() As String
    Dim xModo As String
    Dim xNv As String
    Dim bPermite As Boolean
    Dim bEsSelec As Boolean
    Dim I As Integer
    Dim nodX As MSComctlLib.node
    
    cmdCerrar.Enabled = False
    cmdVisualizar.Enabled = False
    cmdFiltro.Enabled = False
    
    tvDatos.visible = False
    tvDatos.Nodes.Clear
    tvDatos.ImageList = imgList
    'tvDatos.Visible = True         ' del -002- a.
    
    Dim xModoSelectAgrup
    
    xModoSelectAgrup = glModoSelectAgrup
    If glModoSelectAgrup = "EXPO" Then
        xModoSelectAgrup = "VIEW"
    End If
    If glUsrPerfilActual = "SADM" Then
       xModoSelectAgrup = "SADM"
    End If
    
    xNv = CodigoCombo(cboNivel, True)
    Select Case xNv
        Case "USR"
            xAreaView = glLOGIN_ID_REEMPLAZO
            xNivelView = "USR"
        Case "DIR"
            Call ParseString(vRetorno, DatosCombo(cboDire), "|")
            xNivelView = vRetorno(2)
            xAreaView = vRetorno(3)
        Case "GER"
            Call ParseString(vRetorno, DatosCombo(cboGere), "|")
            xNivelView = vRetorno(2)
            xAreaView = vRetorno(3)
        Case "SEC"
            Call ParseString(vRetorno, DatosCombo(cboSect), "|")
            xNivelView = vRetorno(2)
            xAreaView = vRetorno(3)
        Case "GRU"
            Call ParseString(vRetorno, DatosCombo(cboGrup), "|")
            xNivelView = vRetorno(2)
            xAreaView = vRetorno(3)
        Case "PUB"
            xAreaView = ""
            xNivelView = "PUB"
    End Select
    xAreaView = ReplaceString(xAreaView, Asc("!"), "|")
    
    DoEvents
    Call Puntero(True)
    'Call Status("Cargando agrupamientos...")
    sbMain.SimpleText = "Cargando agrupamientos..."
    
    'Set nodX = tvDatos.Nodes.Add(, , "X0", "/")    ' del -002- a.
    'Set nodX = tvDatos.Nodes.Add(, , "X0", "� AGRUPAMIENTOS �", 2)
    tvDatos.Nodes.Clear
    'Set nodX = tvDatos.Nodes.Add(, , "X0", "� AGRUPAMIENTOS �", 3)
    Set nodX = tvDatos.Nodes.Add(, , "X0", "Agrupamientos", 15)
    nodX.Tag = "N"
    
    If xNivelView = "PUB" Then
        On Error Resume Next
        If UBound(vAgrPublico, 1) <= 1 Or Err > 0 Then
            ReDim vAgrPublico(1 To 13, 1 To 1)
            If Not sp_GetAgrupNivelRecur(xModoSelectAgrup, xNivelView, xAreaView, "S") Then
               GoTo finx
            End If
            Do While Not aplRST.EOF
                I = I + 1
                ReDim Preserve vAgrPublico(1 To 13, 1 To I)
                vAgrPublico(vNroIn, I) = Val(ClearNull(aplRST!agr_nrointerno))
                vAgrPublico(vNroPa, I) = Val(ClearNull(aplRST!agr_nropadre))
                vAgrPublico(vVisib, I) = ClearNull(aplRST!agr_visibilidad)
                vAgrPublico(vActua, I) = ClearNull(aplRST!agr_actualiza)
                vAgrPublico(vDirec, I) = ClearNull(aplRST!cod_direccion)
                vAgrPublico(vGeren, I) = ClearNull(aplRST!cod_gerencia)
                vAgrPublico(vSecto, I) = ClearNull(aplRST!cod_sector)
                vAgrPublico(vGrupo, I) = ClearNull(aplRST!cod_grupo)
                vAgrPublico(vUsual, I) = ClearNull(aplRST!cod_usualta)
                vAgrPublico(vAdmPe, I) = ClearNull(aplRST!agr_admpet)
                vAgrPublico(vVigen, I) = ClearNull(aplRST!agr_vigente)
                vAgrPublico(vTitul, I) = ClearNull(aplRST!agr_titulo)
                vAgrPublico(vNivel, I) = ClearNull(aplRST!nivel)
                aplRST.MoveNext
            Loop
        End If
    Else
        If Not sp_GetAgrupNivelRecur(xModoSelectAgrup, xNivelView, xAreaView, "S") Then
           GoTo finx
        End If
    End If
    On Error GoTo 0
    
    'tvDatos.Visible = False    ' del -002- a.
    '{ add -002- a.
    If aplRST.State <> 0 Then
        If Not aplRST.RecordCount = 0 Then
            sbMain.SimpleText = aplRST.RecordCount & " agrupamientos listados."
        Else
            sbMain.SimpleText = "No hay agrupamientos listados."
        End If
    Else
        sbMain.SimpleText = "No hay agrupamientos listados."
    End If
    '}
    
    If xNivelView = "PUB" Then
        'los agrupamientos publicos los carga del vector
        'chanchada para que sea mas rapido el agrupamiento -sistema-
        For I = 1 To UBound(vAgrPublico, 2)
            bPermite = True
            If I Mod 10 = 0 Then
                Call Status(ClearNull(vAgrPublico(vNroIn, I)))
            End If
            If (vAgrPublico(vVigen, I) <> "S") Then
                bPermite = False
            ElseIf (glModoSelectAgrup = "OWNR" And (vAgrPublico(vNroPa, I) <> 0 Or Val(ClearNull(glNumeroAgrup)) = vAgrPublico(vNroIn, I) Or vAgrPublico(vVisib, I) <> xNivelVisi Or vAgrPublico(vActua, I) <> xNivelModi)) Then
                bPermite = False
            'ElseIf (glModoSelectAgrup = "SADM" And (vAgrPublico(vNroPa, I) <> 0 Or Val(ClearNull(glNumeroAgrup)) = vAgrPublico(vNroIn, I))) Then
            '    bPermite = False
            ElseIf glModoSelectAgrup = "EXPO" And vAgrPublico(vNroPa, I) <> 0 Then
                bPermite = False
            End If
            
            If bPermite Then
                bEsSelec = False
                Select Case xModoSelectAgrup
                    Case "OWNR"
                        bEsSelec = True
                    Case "MODI"
                        If vAgrPublico(vAdmPe, I) = "S" Then
                            If chkNvAgrup(vAgrPublico(vActua, I), vAgrPublico(vDirec, I), vAgrPublico(vGeren, I), vAgrPublico(vSecto, I), vAgrPublico(vGrupo, I), vAgrPublico(vUsual, I), glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REEMPLAZO) Then
                                 bEsSelec = True
                            Else
                                bEsSelec = False
                            End If
                            If glUsrPerfilActual = "SADM" Then
                                 bEsSelec = True
                            End If
                        Else
                            bEsSelec = False
                        End If
                    Case "SADM"
                        bEsSelec = True
                    Case "VIEW"
                        bEsSelec = True
                End Select
            End If
            On Error Resume Next
            If bPermite Then
                Set nodX = tvDatos.Nodes.Add("X" & ClearNull(vAgrPublico(vNroPa, I)), tvwChild, "X" & ClearNull(vAgrPublico(vNroIn, I)), vAgrPublico(vTitul, I))
                If bEsSelec Then
                    nodX.Tag = "S"
                Else
                    nodX.Tag = "N"
                End If
                If (vAgrPublico(vNivel, I) = 0 Or vAgrPublico(vNivel, I) = 1) Then
                    nodX.Expanded = True
                End If
            End If
            On Error GoTo 0
        Next
    Else
        Do While Not aplRST.EOF
            'si estoy seleccionando para agregar en otro  agrupamiento
            'exijo que agr_nropadre = 0 y que no se agregue a s� mismo
    '''           ((glModoSelectAgrup = "MODI" And ClearNull(aplRST!agr_admpet) = "S")
            bPermite = True
            'Call Status(ClearNull(aplRST!agr_nrointerno))  ' del -pepe-
            If (ClearNull(aplRST!agr_vigente) <> "S") Then
                bPermite = False
            ElseIf (glModoSelectAgrup = "OWNR" And (Val(ClearNull(aplRST!agr_nropadre)) <> 0 Or Val(ClearNull(glNumeroAgrup)) = Val(ClearNull(aplRST!agr_nrointerno)) Or ClearNull(aplRST!agr_visibilidad) <> xNivelVisi Or ClearNull(aplRST!agr_actualiza) <> xNivelModi)) Then
                bPermite = False
            ElseIf (glModoSelectAgrup = "SADM" And (Val(ClearNull(aplRST!agr_nropadre)) <> 0 Or Val(ClearNull(glNumeroAgrup)) = Val(ClearNull(aplRST!agr_nrointerno)) Or ClearNull(aplRST!agr_visibilidad) <> xNivelVisi Or ClearNull(aplRST!agr_actualiza) <> xNivelModi)) Then
                bPermite = False
            ElseIf glModoSelectAgrup = "EXPO" And Val(ClearNull(aplRST!agr_nropadre)) <> 0 Then
                bPermite = False
            End If
            
            If bPermite Then
                bEsSelec = False
                Select Case xModoSelectAgrup
                Case "SADM"
                    bEsSelec = True
                Case "OWNR"
                    bEsSelec = True
                Case "MODI"
                    If ClearNull(aplRST!agr_admpet) = "S" Then
                        If chkNvAgrup(ClearNull(aplRST!agr_actualiza), ClearNull(aplRST!cod_direccion), ClearNull(aplRST!cod_gerencia), ClearNull(aplRST!cod_sector), ClearNull(aplRST!cod_grupo), ClearNull(aplRST!cod_usualta), glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REEMPLAZO) Then
                             bEsSelec = True
                        Else
                            bEsSelec = False
                        End If
                        If glUsrPerfilActual = "SADM" Then
                             bEsSelec = True
                        End If
                    Else
                        bEsSelec = False
                    End If
                Case "SADM"
                       bEsSelec = True
                Case "VIEW"
                    bEsSelec = True
                End Select
            End If
            On Error Resume Next
            If bPermite Then
                Set nodX = tvDatos.Nodes.Add("X" & ClearNull(Val(ClearNull(aplRST!agr_nropadre))), tvwChild, "X" & ClearNull(aplRST!agr_nrointerno), ClearNull(aplRST!agr_titulo))
                If bEsSelec Then
                    nodX.Tag = "S"
                Else
                    nodX.Tag = "N"
                End If
                If (aplRST!nivel = 0 Or aplRST!nivel = 1) Then
                    nodX.Expanded = True
                End If
            End If
            On Error GoTo 0
            aplRST.MoveNext
        Loop
        'aplRST.Close
    End If
        
    With tvDatos
        For I = 2 To tvDatos.Nodes.Count
            If .Nodes.Item(I).Children > 0 Then
                If .Nodes.Item(I).Tag = "S" Then
                   .Nodes.Item(I).Image = 12     '4
                   '.Nodes.item(i).ExpandedImage = 5
                Else
                   .Nodes.Item(I).Image = 1     '1
                   '.Nodes.item(i).ExpandedImage = 2
                End If
            Else
                If .Nodes.Item(I).Tag = "S" Then
                   '.Nodes.item(i).Image = 7
                   .Nodes.Item(I).Image = 10
                Else
                   '.Nodes.item(i).Image = 6
                   .Nodes.Item(I).Image = 16
                End If
            End If
        Next
        .Nodes.Item(1).Expanded = True
    End With
finx:
    xNivelView = ""
    xAreaView = ""
    xNivelModi = ""
    xAreaModi = ""
    xNivelVisi = ""
    Call HabilitarBotones
    tvDatos.visible = True
    If tvDatos.visible Then tvDatos.SetFocus
    Call Puntero(False)
    sbMain.SimpleText = "Listo."
    Call LockProceso(False)
End Sub

Public Sub MostrarSeleccion(ptrItem As Integer)
    If tvDatos.Nodes.Count > 0 Then
        cmdVisualizar.Enabled = False
        glSelectAgrup = ""
        glSelectAgrupStr = ""
        If tvDatos.Nodes(ptrItem).Tag = "S" Then
           glSelectAgrup = Mid(tvDatos.Nodes(ptrItem).Key, 2)
           glSelectAgrupStr = tvDatos.Nodes(ptrItem).text & Space(100) & "|" & Mid(tvDatos.Nodes(ptrItem).Key, 2)
          cmdVisualizar.Enabled = True
        End If
    End If
End Sub

'{ del -003- a.
'Private Sub tmrFrame_Timer()
'    tmrFrame.Enabled = False
'    tmrFrame.interval = 0
'    Call CargarGrid
'    Call HabilitarBotones
'    Call MostrarSeleccion(1)
'    Call LockProceso(False)
'End Sub
'}

Private Sub tvDatos_Click()
    If tvDatos.Nodes.Count > 0 Then
        Call MostrarSeleccion(tvDatos.SelectedItem.Index)
    End If
End Sub

Private Sub tvDatos_DblClick()
    If tvDatos.Nodes.Count > 0 Then
        Call MostrarSeleccion(tvDatos.SelectedItem.Index)
        If tvDatos.Nodes.Item(tvDatos.SelectedItem.Index).Children = 0 Then
            cmdVisualizar_Click
        End If
    End If
End Sub

