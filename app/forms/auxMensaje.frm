VERSION 5.00
Begin VB.Form auxMensaje 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   2790
   ClientLeft      =   2895
   ClientTop       =   2235
   ClientWidth     =   3810
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2790
   ScaleWidth      =   3810
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Left            =   120
      Top             =   120
   End
   Begin VB.Label lblMensaje 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Height          =   2295
      Left            =   90
      TabIndex        =   0
      Top             =   300
      Width           =   3615
   End
End
Attribute VB_Name = "auxMensaje"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Dim m_Texto As String
Dim m_Segundos As Integer

Private Sub Form_Load()
    On Error Resume Next
    Me.lblMensaje.Caption = m_Texto
    Me.Timer1.Interval = m_Segundos
End Sub

Public Sub Disparar()
    Me.lblMensaje.Caption = m_Texto
    Me.Timer1.Interval = m_Segundos
    Me.Timer1.Enabled = True
    Me.Show vbModal
End Sub

Private Sub Timer1_Timer()
    Me.Timer1.Enabled = False
    Unload Me
End Sub

Public Property Let prpTexto(ByVal NewVal As String)
    m_Texto = NewVal
End Property

Public Property Let prpSegundos(ByVal NewVal As Integer)
    m_Segundos = NewVal
End Property

