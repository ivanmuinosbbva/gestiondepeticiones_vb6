VERSION 5.00
Begin VB.Form frmHistorialAdd 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Historial"
   ClientHeight    =   5850
   ClientLeft      =   1140
   ClientTop       =   330
   ClientWidth     =   8790
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   8790
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   30
      TabIndex        =   6
      Top             =   -60
      Width           =   8760
      Begin VB.Label Label6 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   12
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         Height          =   195
         Left            =   7620
         TabIndex        =   11
         Top             =   480
         Width           =   480
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         Height          =   195
         Left            =   1305
         TabIndex        =   10
         Top             =   240
         Width           =   7305
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         Height          =   195
         Left            =   1300
         TabIndex        =   9
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6705
         TabIndex        =   8
         Top             =   480
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   7
         Top             =   240
         Width           =   1200
      End
   End
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4995
      Left            =   30
      TabIndex        =   2
      Top             =   840
      Width           =   7680
      Begin VB.TextBox txtMensaje 
         Height          =   3690
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   3
         Top             =   540
         Width           =   7455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones"
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1065
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4995
      Left            =   7740
      TabIndex        =   0
      Top             =   840
      Width           =   1035
      Begin VB.CommandButton cmdAddHist 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   4170
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   60
         TabIndex        =   1
         Top             =   4560
         Width           =   885
      End
   End
End
Attribute VB_Name = "frmHistorialAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Const colFECHA = 0
Const colEvento = 1
Const colEstDest = 2
Const colUser = 3
Const colObser = 4
Const colFechaSort = 6
Const colNroInterno = 7
Dim EstadoPeticion As String

Private Sub Form_Load()
    Dim auxNro As String
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNro = "S/N"
            Else
                auxNro = ClearNull(aplRST!pet_nroasignado)
            End If
            lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
            lblPrioridad = ClearNull(aplRST!prioridad)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
            EstadoPeticion = ClearNull(aplRST!cod_estado)
        End If
        aplRST.Close
    End If
    Call FormCenter(Me, mdiPrincipal, False)
End Sub

Private Sub cmdAddHist_Click()
    'Call sp_AddHistorial(glNumeroPeticion, "HSTADD", EstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Texto adicional � " & Trim(txtMensaje.text))
    If Len(Trim(txtMensaje.text)) > 0 Then
        Call sp_AddHistorial(glNumeroPeticion, "HSTADD", EstadoPeticion, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, Trim(txtMensaje.text))
        Unload Me
    Else
        MsgBox "Debe ingresar un texto.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub
