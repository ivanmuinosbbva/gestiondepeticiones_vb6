VERSION 5.00
Begin VB.Form frmInitForm 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CGM - Gesti�n de Peticiones"
   ClientHeight    =   2490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInitForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2490
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3600
      TabIndex        =   6
      Top             =   2040
      Width           =   975
   End
   Begin VB.Frame fraOpciones 
      Caption         =   "Opciones de conexi�n"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4455
      Begin VB.ComboBox cmbRed 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   600
         Width           =   2295
      End
      Begin VB.ComboBox cmbAmbiente 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   960
         Width           =   2295
      End
      Begin VB.Label lblInitForm 
         AutoSize        =   -1  'True
         Caption         =   "Red"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   285
      End
      Begin VB.Label lblInitForm 
         AutoSize        =   -1  'True
         Caption         =   "Ambiente"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   1
         Left            =   240
         TabIndex        =   4
         Top             =   960
         Width           =   720
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   0
      Top             =   2040
      Width           =   975
   End
End
Attribute VB_Name = "frmInitForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    With cmbRed
        .Clear
        .AddItem "Banco" & Space(100) & "||BF"
        .AddItem "Consolidar" & Space(100) & "||CONS"
        .ListIndex = 0
    End With
    
    With cmbAmbiente
        .Clear
        .AddItem "Desarrollo" & Space(100) & "||DESA"
        .AddItem "Producci�n" & Space(100) & "||PROD"
        .ListIndex = 0
    End With
End Sub

Private Sub cmdAceptar_Click()
    glENTORNO = CodigoCombo(cmbAmbiente, True)
    glBASE = CodigoCombo(cmbRed, True)
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    'Descarga
    Unload Me
End Sub
