VERSION 5.00
Begin VB.Form auxMensajeCorreo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Datos generados para conformar el correo electrónico"
   ClientHeight    =   7875
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10620
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7875
   ScaleWidth      =   10620
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraSecundario 
      Height          =   735
      Left            =   120
      TabIndex        =   8
      Top             =   7080
      Width           =   10455
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "C&errar"
         Height          =   375
         Left            =   9120
         TabIndex        =   9
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame fraPrincipal 
      Height          =   7095
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   10455
      Begin VB.Label lblMensaje 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4095
         Left            =   1080
         TabIndex        =   13
         Top             =   2520
         Width           =   9015
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblAsunto 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         Top             =   1845
         Width           =   9015
      End
      Begin VB.Label lblCC 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   1365
         Width           =   9015
      End
      Begin VB.Label lblPara 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   1080
         TabIndex        =   10
         Top             =   885
         Width           =   9015
      End
      Begin VB.Shape shpPara 
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00808080&
         Height          =   345
         Left            =   960
         Top             =   840
         Width           =   9255
      End
      Begin VB.Label lblCorreo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Para:"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   915
         Width           =   390
      End
      Begin VB.Label lblCorreo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CC:"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   1395
         Width           =   270
      End
      Begin VB.Label lblCorreo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Asunto:"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   5
         Top             =   1875
         Width           =   570
      End
      Begin VB.Shape Shape1 
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00808080&
         Height          =   345
         Left            =   960
         Top             =   1320
         Width           =   9255
      End
      Begin VB.Shape Shape2 
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00808080&
         Height          =   345
         Left            =   960
         Top             =   1800
         Width           =   9255
      End
      Begin VB.Shape Shape3 
         BackStyle       =   1  'Opaque
         BorderColor     =   &H00808080&
         Height          =   4335
         Left            =   960
         Top             =   2400
         Width           =   9255
      End
      Begin VB.Label lblCorreo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Click sobre los cuadros de texto para Copiar"
         ForeColor       =   &H00404040&
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   4
         Top             =   6840
         Width           =   6855
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblCorreo 
         BackStyle       =   0  'Transparent
         Caption         =   "Debe copiar el contenido de este formulario para generar el email de envio desde su proveedor de correo electrónico."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   315
         Index           =   4
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   10215
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblCorreo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Importante: si se trata de una EMErgencia, debe cambiar en el asunto EXC por EME."
         ForeColor       =   &H00808080&
         Height          =   195
         Index           =   5
         Left            =   960
         TabIndex        =   2
         Top             =   2160
         Width           =   6060
      End
      Begin VB.Label lblCopiado 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "lblCopiado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   4680
         TabIndex        =   1
         Top             =   600
         Visible         =   0   'False
         Width           =   885
      End
   End
End
Attribute VB_Name = "auxMensajeCorreo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public cPara As String
Public cCC As String
Public cAsunto As String
Public cMensaje As String

Private Sub Form_Load()
    lblPara.ToolTipText = "Click para copiar"
    lblCC.ToolTipText = "Click para copiar"
    lblAsunto.ToolTipText = "Click para copiar"
    lblMensaje.ToolTipText = "Click para copiar"
    lblCopiado = ""
    lblCopiado.Visible = True
    lblPara = cPara
    lblCC = cCC
    lblAsunto = cAsunto
    lblMensaje = cMensaje
End Sub

Private Sub lblPara_Click()
    Call Copiar_al_Clipboard(lblPara)
End Sub

Private Sub lblCC_Click()
    Call Copiar_al_Clipboard(lblCC)
End Sub

Private Sub lblAsunto_Click()
    Call Copiar_al_Clipboard(lblAsunto)
End Sub

Private Sub lblMensaje_Click()
    Call Copiar_al_Clipboard(lblMensaje)
End Sub

Private Sub lblPara_DblClick()
    Call lblPara_Click
End Sub

Private Sub lblCC_DblClick()
    Call lblCC_Click
End Sub

Private Sub lblAsunto_DblClick()
    Call lblAsunto_Click
End Sub

Private Sub lblMensaje_DblClick()
    Call lblMensaje_Click
End Sub

Private Sub Copiar_al_Clipboard(Texto As String)
    Call Puntero(True)
    With Clipboard
        .Clear
        .SetText Texto, vbCFText
        lblCopiado.Caption = "Copiado!"
        KillSomeTime 1
        lblCopiado.Caption = ""
    End With
    Call Puntero(False)
End Sub

Private Sub KillSomeTime(segs As Integer)
    Dim PauseTime, Start
    PauseTime = segs    ' Set duration in seconds
    Start = Timer       ' Set start time.
    Do While Timer < Start + PauseTime
        DoEvents        ' Yield to other processes.
    Loop
End Sub

Private Sub cmdCerrar_Click()
    If MsgBox("Recuerde que el email no es enviado automáticamente por el sistema, " & vbCrLf & _
              "sino que debe ser conformado y enviado manualmente por el usuario.", vbQuestion + vbOKOnly) = vbOK Then
        Unload Me
    End If
End Sub
