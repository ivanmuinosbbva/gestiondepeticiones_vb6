VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionesGrupoRecurso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Recursos Intervinientes en Petici�n/Grupo"
   ClientHeight    =   5340
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   8265
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5340
   ScaleWidth      =   8265
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   795
      Left            =   60
      TabIndex        =   1
      Top             =   -60
      Width           =   8190
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   7
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6705
         TabIndex        =   6
         Top             =   480
         Width           =   765
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1300
         TabIndex        =   5
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1305
         TabIndex        =   4
         Top             =   240
         Width           =   6705
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   7620
         TabIndex        =   3
         Top             =   480
         Width           =   480
      End
      Begin VB.Label Label6 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   2
         Top             =   480
         Width           =   1200
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   8
      Top             =   660
      Width           =   8190
      Begin VB.Label lblEstadoSector 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5565
         TabIndex        =   10
         Top             =   180
         Width           =   2475
      End
      Begin VB.Label lblSector 
         Caption         =   "sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   12
         Top             =   180
         Width           =   2745
      End
      Begin VB.Label Label9 
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   11
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label Label4 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   9
         Top             =   180
         Width           =   1200
      End
   End
   Begin VB.Frame Frame3 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   13
      Top             =   1100
      Width           =   8190
      Begin VB.Label lblGrupo 
         Caption         =   "grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   14
         Top             =   180
         Width           =   2745
      End
      Begin VB.Label Label8 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   17
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoGrupo 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5580
         TabIndex        =   16
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label5 
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   15
         Top             =   180
         Width           =   1200
      End
   End
   Begin VB.ComboBox cboRecurso 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1050
      Style           =   2  'Dropdown List
      TabIndex        =   24
      Top             =   4440
      Width           =   5295
   End
   Begin VB.CommandButton cmdMasRecursos 
      Height          =   315
      Left            =   6390
      Picture         =   "frmPeticionesGrupoRecurso.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   23
      TabStop         =   0   'False
      ToolTipText     =   "Seleccionar otro Recurso que no figure en la lista"
      Top             =   4448
      Width           =   315
   End
   Begin VB.Frame pnlBtnControl 
      Height          =   3795
      Left            =   7215
      TabIndex        =   0
      Top             =   1530
      Width           =   1035
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   90
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Asignar un recurso"
         Top             =   150
         Width           =   885
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   90
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Desasignar el recurso seleccionado"
         Top             =   540
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   90
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   3360
         Width           =   885
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   90
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   930
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdRecurso 
      Height          =   2670
      Left            =   30
      TabIndex        =   18
      Top             =   1620
      Width           =   7140
      _ExtentX        =   12594
      _ExtentY        =   4710
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Recurso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   25
      Top             =   4500
      Width           =   690
   End
End
Attribute VB_Name = "frmPeticionesGrupoRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 03.03.2008 - Incidencia: para evitar que se grabe un registro con recurso en blanco.
' -004- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -005- a. FJS 28.01.2009 - Se agregan algunas mejoras a la usabilidad de la pantalla para mejorar la experiencia del usuario.

Option Explicit

Dim sOpcionSeleccionada As String
Dim EstadoSolicitud As String
Dim sGrupo As String
Dim sSector As String
Dim sGerencia As String
Dim sDireccion As String
Dim sCoorSect As String
Dim EstadoPeticion As String
Dim NuevoEstadoPeticion As String
Dim EstadoSector As String
Dim NuevoEstadoSector As String
Dim NuevoEstadoGrupo As String
Dim xPerfNivel, xPerfArea As String
Dim hst_nrointerno_sol
Dim sec_nrointerno_sol
Dim flgAccion As Boolean
Dim cModo As String     ' add -003- a.

Private Const colCODPETICION = 0
Private Const colCODRECURSO = 1
Private Const colNOMRECURSO = 2
Private Const colHORAS = 3

Private Sub Form_Load()
    Call InicializarPantalla
    Call CargarGrid
    Call HabilitarBotones(0)
    Call IniciarScroll(grdRecurso)      ' add -004- a.
End Sub

'{ add -005- a.
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then       ' Tecla: ESCAPE
        cmdCerrar_Click
    End If
End Sub
'}

Private Sub cmdAgregar_Click()
    cModo = "A"     ' add -003- a.
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
    Call LockProceso(False)
End Sub

Private Sub cmdCerrar_Click()
    If sOpcionSeleccionada = "" Then
        Unload Me
    Else
        sOpcionSeleccionada = ""
        cmdCerrar.Caption = "Cerrar"
        Call HabilitarBotones(0, False)
    End If
End Sub

Private Sub cmdEliminar_Click()
    cModo = "E"     ' add -003- a.
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Dim NroHistorial As Long
    Dim xAccion As String
    Dim sMsgPeticion As String
    Dim xSituacion As String
    

If CamposObligatorios Then
    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = False
    Call Puntero(True)
    Call Status("Procesando...")
    Select Case sOpcionSeleccionada
       Case "A"
            Call sp_InsertPeticionRecurso(glNumeroPeticion, CodigoCombo(cboRecurso, True), sGrupo, sSector, sGerencia, sDireccion)
            sOpcionSeleccionada = ""
        Case "E"
            Call sp_DeletePeticionRecurso(glNumeroPeticion, CodigoCombo(cboRecurso, True))
            sOpcionSeleccionada = ""
    End Select
    Call HabilitarBotones(0)
    Call Puntero(False)
    Call Status("Listo")
End If
End Sub

Private Sub cmdMasRecursos_Click()
    auxSelRecurso.Show vbModal
    If auxSelRecurso.CodRecurso <> "" Then
        cboRecurso.AddItem auxSelRecurso.NomRecurso & ESPACIOS & ESPACIOS & "||" & auxSelRecurso.CodRecurso, 0
        cboRecurso.ListIndex = 0
    End If
End Sub

Sub InicializarPantalla()
   Dim auxNro As String
    Me.Tag = ""
''   xPerfNivel = getPerfNivel(glUsrPerfilActual)
''   xPerfArea = getPerfArea(glUsrPerfilActual)
    
    If sp_GetRecurso(glGrupo, "SU", "A") Then
        Do While Not aplRST.EOF
            cboRecurso.AddItem ClearNull(aplRST(1)) & Space(40) & "||" & ClearNull(aplRST(0))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    cmdConfirmar.Enabled = False
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNro = "S/N"
            Else
                auxNro = ClearNull(aplRST!pet_nroasignado)
            End If
            lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
            lblPrioridad = ClearNull(aplRST!prioridad)
            EstadoPeticion = ClearNull(aplRST!cod_estado)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
        End If
        aplRST.Close
    End If
    If sp_GetPeticionSector(glNumeroPeticion, glSector) Then
       If Not aplRST.EOF Then
          lblSector = ClearNull(aplRST!nom_sector)
          lblEstadoSector = ClearNull(aplRST!nom_estado)
        End If
        aplRST.Close
    End If
    If sp_GetPeticionGrupoXt(glNumeroPeticion, glSector, glGrupo) Then
       If Not aplRST.EOF Then
            lblGrupo = ClearNull(aplRST!nom_grupo)
            lblEstadoGrupo = ClearNull(aplRST!nom_estado)
            sGrupo = ClearNull(aplRST!cod_grupo)
            sSector = ClearNull(aplRST!cod_sector)
            sGerencia = ClearNull(aplRST!cod_gerencia)
            sDireccion = ClearNull(aplRST!cod_direccion)
        End If
        'aplRST.Close
    End If
End Sub

Private Sub CargarGrid()
    Dim nPos As Integer
    
    cboRecurso.ListIndex = -1
    cmdEliminar.Enabled = False
    
    With grdRecurso
        .Clear
        .HighLight = flexHighlightNever
        .RowHeightMin = 265
        .cols = 4
        .Rows = 1
        .ColWidth(colCODPETICION) = 0
        .TextMatrix(0, colCODRECURSO) = "Cod.": .ColWidth(colCODRECURSO) = 1200
        .TextMatrix(0, colNOMRECURSO) = "Recurso": .ColWidth(colNOMRECURSO) = 4500
        .TextMatrix(0, colHORAS) = "% Horas cargadas": .ColWidth(colHORAS) = 1000: .ColAlignment(colHORAS) = flexAlignRightCenter
        Call CambiarEfectoLinea(grdRecurso, prmGridEffectFontBold)
    End With
    If Not sp_GetPeticionRecursoHoras(glNumeroPeticion, glGrupo) Then Exit Sub
    cboRecurso.visible = False
    Do While Not aplRST.EOF
        With grdRecurso
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODPETICION) = ClearNull(aplRST.Fields.Item("pet_nrointerno"))
            .TextMatrix(.Rows - 1, colCODRECURSO) = ClearNull(aplRST.Fields.Item("cod_recurso"))
            .TextMatrix(.Rows - 1, colNOMRECURSO) = ClearNull(aplRST.Fields.Item("nom_recurso"))
            .TextMatrix(.Rows - 1, colHORAS) = Format(IIf(ClearNull(aplRST.Fields.Item("horas")) = "", 0, aplRST.Fields.Item("horas")), "Percent")
            If ClearNull(.TextMatrix(.Rows - 1, colCODRECURSO)) <> "" Then
                nPos = PosicionCombo(cboRecurso, ClearNull(.TextMatrix(.Rows - 1, colCODRECURSO)), True)
                If nPos = -1 Then
                    cboRecurso.AddItem .TextMatrix(.Rows - 1, colNOMRECURSO) & Space(40) & " || " & .TextMatrix(.Rows - 1, colCODRECURSO)
                End If
            End If
        End With
        aplRST.MoveNext
    Loop
    cboRecurso.visible = True
    Call MostrarSeleccion
    DoEvents
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    cmdAgregar.Enabled = False
    cmdEliminar.Enabled = False
    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = True
    cboRecurso.Enabled = False
    cmdMasRecursos.Enabled = False
    Select Case nOpcion
        Case 0
            cmdCerrar.Caption = "Cerrar"
            cboRecurso.ListIndex = -1
            grdRecurso.Enabled = True
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
            cmdAgregar.Enabled = True
        Case 1
            cmdAgregar.Enabled = False
            grdRecurso.Enabled = False
            Select Case sOpcionSeleccionada
                Case "A"
                    cboRecurso.Enabled = True
                    cmdMasRecursos.Enabled = True
                    cmdConfirmar.Enabled = True
                    cmdConfirmar.Enabled = True
                    cmdCerrar.Caption = "Cancelar"
''                    lblAccion = "Nuevo Grupo"
                Case "E"
                    cmdConfirmar.Enabled = True
                    cmdCerrar.Caption = "Cancelar"
''                    lblAccion = "ELIMINAR GRUPO"
            End Select
    End Select
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = False
    If cModo = "A" Then    ' add -003- a.
        If Len(Trim(cboRecurso.Text)) > 0 Then
            CamposObligatorios = True
        Else
            MsgBox "Debe seleccionar un recurso v�lido para continuar.", vbCritical + vbOKOnly, "Recurso inv�lido"
        End If
    '{ add -003- a.
    Else
        CamposObligatorios = True
    End If
    '}
End Function

Private Sub MostrarSeleccion()
    Dim sEstado As String
    cmdEliminar.Enabled = False
    grdRecurso.Enabled = False
    With grdRecurso
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            '{ add -003- a.
            cboRecurso.ListIndex = PosicionCombo(Me.cboRecurso, .TextMatrix(.RowSel, colCODRECURSO), True)
            cmdEliminar.Enabled = True
            '}
            '{ del -003- a.
            'If .TextMatrix(.RowSel, colCODRECURSO) <> "" Then  ' del -003- a.
            '   cboRecurso.ListIndex = PosicionCombo(Me.cboRecurso, .TextMatrix(.RowSel, colCODRECURSO), True)
            '    cmdEliminar.Enabled = True
            'End If
            '}
        End If
        .Enabled = True
    End With
    grdRecurso.Enabled = True
    Call Status("Listo.")
End Sub

Private Sub grdRecurso_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdRecurso_RowColChange()
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdRecurso_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdRecurso_Click
    End If
End Sub
'}

'{ add -004- a.
Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdRecurso
End Sub
'}
