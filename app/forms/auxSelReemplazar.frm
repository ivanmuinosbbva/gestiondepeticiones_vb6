VERSION 5.00
Begin VB.Form auxSelReemplazar 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   2370
   ClientLeft      =   3345
   ClientTop       =   3375
   ClientWidth     =   6960
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   120
      TabIndex        =   3
      Top             =   540
      Width           =   6675
      Begin VB.ComboBox cboRecurso 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1140
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   240
         Width           =   5235
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Recurso"
         Height          =   195
         Left            =   300
         TabIndex        =   5
         Top             =   315
         Width           =   585
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   435
      Left            =   2040
      TabIndex        =   2
      Top             =   1860
      Width           =   1365
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   435
      Left            =   3510
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1860
      Width           =   1365
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Seleccione con cual recurso desea ingresar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   3645
   End
End
Attribute VB_Name = "auxSelReemplazar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.12.2008 - Se formatea para una mejor visualizaci�n.

Option Explicit

Private Sub Form_Load()
    Dim flgRecursos As Boolean
    cboRecurso.ListIndex = -1
    
    flgRecursos = False
    flgRecursos = sp_GetRecursoPosible(glLOGIN_ID_REAL)
    
    If flgRecursos Then
        Do While Not aplRST.EOF
            'cboRecurso.AddItem Trim(aplRST(0)) & " : " & Trim(aplRST(1))   ' del -002- a.
            cboRecurso.AddItem aplRST(0) & " : " & Trim(aplRST(1))   ' add -002- a.
            aplRST.MoveNext
            cboRecurso.Enabled = True
        Loop
        cboRecurso.ListIndex = 0
    End If
End Sub

Private Sub cmdAceptar_Click()
    ' sLogin: el legajo del recurso a quien reemplaza el usuario actuante
   Dim sLogin As String
   If cboRecurso.ListIndex > -1 Then
      sLogin = CodigoCombo(cboRecurso)
      If InicializarRecurso(sLogin) = True Then
         mdiPrincipal.sbPrincipal.Panels(3) = glLOGIN_ID_REEMPLAZO
         mdiPrincipal.sbPrincipal.Panels(5) = glLOGIN_NAME_REEMPLAZO
         Call HabilitarMenues(True)
         Unload Me
      Else
         MsgBox "El Usuario indicado NO est� definido como Recurso en el aplicativo.", vbCritical, "Reemplazar a"
      End If
   Else
      MsgBox ("Debe seleccionar un RECURSO")
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
