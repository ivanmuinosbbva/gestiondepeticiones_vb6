VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form auxCambioPass 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CGM - Seguridad"
   ClientHeight    =   2775
   ClientLeft      =   3345
   ClientTop       =   3660
   ClientWidth     =   4395
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "auxCambioPass.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2775
   ScaleWidth      =   4395
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2745
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4365
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   3210
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   2280
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   2160
         TabIndex        =   8
         Top             =   2280
         Width           =   1005
      End
      Begin AT_MaskText.MaskText PASS_ACTUAL 
         Height          =   300
         Left            =   2280
         TabIndex        =   4
         Top             =   607
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PasswordChar    =   "�"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PasswordChar    =   "�"
         MaxLength       =   10
      End
      Begin AT_MaskText.MaskText PASS_NUEVO 
         Height          =   300
         Left            =   2280
         TabIndex        =   5
         Top             =   997
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PasswordChar    =   "�"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PasswordChar    =   "�"
         MaxLength       =   10
      End
      Begin AT_MaskText.MaskText PASS_VERIFICACION 
         Height          =   300
         Left            =   2280
         TabIndex        =   6
         Top             =   1387
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PasswordChar    =   "�"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PasswordChar    =   "�"
         MaxLength       =   10
      End
      Begin VB.Label Label1 
         Caption         =   "La contrase�a debe tener un m�nimo de 8 y puede tener un m�ximo de 10 caracteres alfanum�ricos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   1800
         Width           =   3975
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "Cambio de contrase�a"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1905
      End
      Begin VB.Label lblActual 
         AutoSize        =   -1  'True
         Caption         =   "Contrase�a actual"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   660
         Width           =   1320
      End
      Begin VB.Label lblNueva 
         AutoSize        =   -1  'True
         Caption         =   "Nueva contrase�a"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   1050
         Width           =   1320
      End
      Begin VB.Label lblVerificacion 
         AutoSize        =   -1  'True
         Caption         =   "Confirmaci�n de contrase�a"
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   1440
         Width           =   2010
      End
   End
End
Attribute VB_Name = "auxCambioPass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 26.11.2008 - Se contempla que si el usuario cancela la modificaci�n de la contrase�a, no se realice el logout autom�tico.
' -003- a. FJS 15.01.2009 - Se agrega una variable p�blica del formulario para indicarle si es obligatorio el cambio de contrase�a.
' -003- b. FJS 15.01.2009 - Se agrega control de hist�rico de contrase�as.
' -004- a. FJS 28.10.2015 - Nuevo: se agrega Trim a los campos para quitar los espacios al principio y al final de la cadena.

Option Explicit

Public bolPasswordChanged As Boolean        ' add -002- a.
Public bolCambioObligatorio As Boolean

Private Sub Form_Load()
    bolPasswordChanged = False              ' add -002- a.
End Sub

Private Sub cmdAceptar_Click()
    '{ add -003- b.
    Dim bHistorico As Boolean
    Dim iVeces As Integer
    Dim bContinuarCambios As Boolean
    Dim i As Long
    '}
    If ClearNull(PASS_ACTUAL) = "" Then
        MsgBox "No ha ingresado la contrase�a actual.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    If ClearNull(PASS_NUEVO) = "" Then
        MsgBox "No ha ingresado la nueva contrase�a.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    If ClearNull(PASS_VERIFICACION) = "" Then
        MsgBox "No ha ingresado la confirmaci�n de la nueva contrase�a.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    If Len(ClearNull(PASS_NUEVO)) < 8 Then
        MsgBox "La nueva contrase�a debe tener al menos 8 caracteres.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    If ClearNull(glLOGIN_PASSWORD) <> ClearNull(PASS_ACTUAL) Then
        MsgBox "La contrase�a actual ingresada no coincide con original.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    If PASS_NUEVO <> PASS_VERIFICACION Then
        MsgBox "La nueva contrase�a no coincide con la verificaci�n de la misma.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    '{ add -003- b.
    bContinuarCambios = True
    bHistorico = False
    If sp_GetVarios("LOGHIS") Then
        If aplRST.Fields!var_numero = 1 Then
            bHistorico = True
            iVeces = CInt(aplRST.Fields!var_texto)
            aplRST.Close
        End If
    End If
    If bHistorico Then
        Call Status("Verificando hist�rico de seguridad...")
        If sp_GetRecursoSeg(glLOGIN_ID_REAL) Then
            If Not aplRST.EOF Then
                If aplRST.RecordCount <= iVeces Then
                    Do While Not aplRST.EOF
                        If PASS_NUEVO = FuncionesEncriptacion.Desencriptar(aplRST.Fields!recurso_data) Then
                            If iVeces > 1 Then
                                MsgBox "La nueva contrase�a debe ser distinta a las " & iVeces & " anteriores contrase�as establecidas.", vbExclamation + vbOKOnly, "Contrase�a repetida"
                            Else
                                MsgBox "La nueva contrase�a debe ser distinta a la contrase�a anterior establecida.", vbExclamation + vbOKOnly, "Contrase�a repetida"
                            End If
                            bContinuarCambios = False
                            Exit Do
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                Else
                    For i = 1 To iVeces
                        If PASS_NUEVO = FuncionesEncriptacion.Desencriptar(aplRST.Fields!recurso_data) Then
                            If iVeces > 1 Then
                                MsgBox "La nueva contrase�a debe ser distinta a las " & iVeces & " anteriores contrase�as establecidas.", vbExclamation + vbOKOnly, "Contrase�a repetida"
                            Else
                                MsgBox "La nueva contrase�a debe ser distinta a la contrase�a anterior establecida.", vbExclamation + vbOKOnly, "Contrase�a repetida"
                            End If
                            bContinuarCambios = False
                            Exit For
                        End If
                    Next i
                End If
            End If
        End If
    End If
    If bContinuarCambios Then
    '}
        Call Status("Procesando...")
        If mdiPrincipal.AdoConnection.CambiarPassword(PASS_NUEVO) Then
        'If sp_password(PASS_NUEVO & mdiPrincipal.AdoConnection.ComponenteSeguridad, PASS_ACTUAL & mdiPrincipal.AdoConnection.ComponenteSeguridad, USER_ID) Then
            bolPasswordChanged = True
            MsgBox "Se ha cambiado la contrase�a correctamente." & vbCrLf & _
                   "Reingrese al sistema con su nueva contrase�a." & vbCrLf & vbCrLf & _
                   "Ahora el aplicativo se cerrar�.", vbInformation + vbOKOnly
            'glLOGIN_PASSWORD = ClearNull(PASS_NUEVO)
            Call Status("Listo.")
            mdiPrincipal.AdoConnection.CerrarConexion
            Unload Me
            End
        Else
            MsgBox "ERROR EN EL PROCESO:" & Chr$(13) & glAdoError, vbCritical + vbOKOnly
            'MsgBox ("ERROR EN EL PROCESO.")
        End If
    End If      ' add -003- b.
    Call Status("Listo.")
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
