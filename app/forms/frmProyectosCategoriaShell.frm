VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmProyectosCategoriaShell 
   Caption         =   "Categor�as"
   ClientHeight    =   6585
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9660
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmProyectosCategoriaShell.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   6585
   ScaleWidth      =   9660
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   120
      TabIndex        =   6
      Top             =   5040
      Width           =   8205
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   315
         Left            =   1350
         TabIndex        =   7
         Top             =   330
         Width           =   1000
         _ExtentX        =   1773
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Validation      =   0   'False
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   315
         Left            =   1350
         TabIndex        =   8
         Top             =   675
         Width           =   6465
         _ExtentX        =   11404
         _ExtentY        =   556
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Validation      =   0   'False
         AutoSelect      =   0   'False
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   11
         Top             =   30
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Label Label2 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   10
         Top             =   690
         Width           =   1100
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   9
         Top             =   345
         Width           =   1100
      End
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6545
      Left            =   8340
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Gerencia"
         Top             =   3930
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la Gerencia selecionada"
         Top             =   4440
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Gerencia seleccionada"
         Top             =   4950
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   5970
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5460
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5010
      Left            =   120
      TabIndex        =   12
      Top             =   30
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   8837
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmProyectosCategoriaShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colCODCATEGORIA = 0
Private Const colNOMCATEGORIA = 1

Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarPantalla
    modUser32.IniciarScroll grdDatos
End Sub

Sub InicializarPantalla()
    Me.Top = 0
    Me.Left = 0
    Me.Height = 7095
    Me.Width = 9780
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    Call HabilitarBotones(0)
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.Visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.Visible = True
                    txtCodigo = "": txtDescripcion = ""
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(txtCodigo.Text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.Text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtDescripcion.Text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtDescripcion.Text, ":") > 0 Then
        MsgBox ("a Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub CargarGrid()
    With grdDatos
        .Clear
        '.HighLight = flexHighlightNever
        .HighLight = flexHighlightWithFocus
        .FocusRect = flexFocusNone
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionByRow
        .Rows = 1
        .Cols = 2
        .TextMatrix(0, colCODCATEGORIA) = "CODIGO"
        .TextMatrix(0, colNOMCATEGORIA) = "DESCRIPCION"
        .ColWidth(colCODCATEGORIA) = 1000: .ColAlignment(colCODCATEGORIA) = 0
        .ColWidth(colNOMCATEGORIA) = 7000: .ColAlignment(colNOMCATEGORIA) = 0
    End With
    CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    
    If Not sp_GetProyIDMCategoria(Null, Null) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODCATEGORIA) = ClearNull(aplRST.Fields!ProjCatId)
            .TextMatrix(.Rows - 1, colNOMCATEGORIA) = ClearNull(aplRST.Fields!ProjCatNom)
        End With
        aplRST.MoveNext
    Loop
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colCODCATEGORIA) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODCATEGORIA))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNOMCATEGORIA))
            End If
        End If
    End With
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertProyectoCategoria(txtCodigo, txtDescripcion) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdateProyectoCategoria(txtCodigo, txtDescripcion) Then
                    With grdDatos
                        If .RowSel > 0 Then
                           .TextMatrix(.RowSel, colNOMCATEGORIA) = txtDescripcion
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeleteProyectoCategoria(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub
