VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmHistorial 
   Caption         =   "Historial - Detalle de eventos de la petici�n"
   ClientHeight    =   6840
   ClientLeft      =   1155
   ClientTop       =   345
   ClientWidth     =   11130
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "frmHistorial.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   6840
   ScaleWidth      =   11130
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4590
      Left            =   60
      TabIndex        =   5
      Top             =   840
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   8096
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin MSComctlLib.ImageList imgHistorial 
      Left            =   3840
      Top             =   4680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmHistorial.frx":058A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trwHistorial 
      Height          =   4575
      Left            =   60
      TabIndex        =   15
      Top             =   840
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   8070
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "imgHistorial"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraTitulo 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   60
      TabIndex        =   7
      Top             =   0
      Width           =   11025
      Begin VB.CheckBox chkEventosDeSistema 
         Alignment       =   1  'Right Justify
         Caption         =   "Diferenciar eventos de sistema"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   8400
         TabIndex        =   16
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label Label6 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   13
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         Height          =   195
         Left            =   10380
         TabIndex        =   12
         Top             =   480
         Width           =   480
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         Height          =   195
         Left            =   1305
         TabIndex        =   11
         Top             =   240
         Width           =   7005
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         Height          =   195
         Left            =   1300
         TabIndex        =   10
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label lblTituloOtro 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   9465
         TabIndex        =   9
         Top             =   480
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   240
         Width           =   1200
      End
   End
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1395
      Left            =   60
      TabIndex        =   2
      Top             =   5400
      Width           =   9945
      Begin VB.TextBox txtMensaje 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   870
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   3
         Top             =   420
         Width           =   9555
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Observaciones"
         ForeColor       =   &H00C2752E&
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   180
         Width           =   1065
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1395
      Left            =   10080
      TabIndex        =   0
      Top             =   5400
      Width           =   1005
      Begin VB.CommandButton cmdAddHist 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   60
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Cancel          =   -1  'True
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   60
         TabIndex        =   1
         Top             =   960
         Width           =   885
      End
      Begin VB.CommandButton cmdDelHist 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   60
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   555
         Visible         =   0   'False
         Width           =   885
      End
   End
End
Attribute VB_Name = "frmHistorial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 19.07.2007 - Se agranda el ancho de la columna para mejorar la visualizaci�n.
' -001- b. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 17.03.2008 - Se agrega la opci�n de eliminar una l�nea del historial puntual.
' -004- a. FJS 28.05.2008 - Se agrega la hora a la fecha del evento.
' -005- a. FJS 19.11.2008 - Se habilitan las preferencias del usuario: al cambiar el ancho de las columnas, el sistema guarda en el RW los anchos utilizados por el usuario para reutilizarlos al reiniciar la pantalla.
' -005- b. FJS 19.11.2008 - Se corrige la visualizaci�n del usuario cuando es un proceso autom�tico de CGM.
' -006- a. FJS 05.12.2008 - Se agrega un indicador de no texto en la carga de la grilla al momento de mostrar el historial.
' -006- d. FJS 05.12.2008 - Se permite el redimensionamiento del historial para mejor legilibilidad.
' -007- a. FJS 18.12.2008 - Una vez cargado el historial, se muestra el puntero del mouse normal y se inicializa la leyenda.
' -008- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -009- a. FJS 13.02.2009 - Se agrega el coloreado din�mico de los eventos del historial grabados manualmente.
' -010- a. FJS 02.03.2010 - Se modifica la carga del texto por cada evento del historial (mucho tiempo de carga).

Option Explicit

Private Const colFECHA = 0
Private Const colEvento = 1
Private Const colESTADO = 2
Private Const colUsrAudit = 3
Private Const colObser = 4
Private Const colSector = 5
Private Const colSectorEst = 6
Private Const colGrupo = 7
Private Const colGrupoEst = 8
Private Const colUsrRplc = 9
Private Const colUSRREPLCPERFIL = 10
Private Const colFechaSort = 11
Private Const colNroInterno = 12
Private Const colTOTCOLS = 13

Dim rowSel As Long

Dim lUltimoRegistroCargado As Long

Private Sub Form_Load()
    chkEventosDeSistema.visible = False
    '{ add -006- d. Posicionamiento y tama�o
    Me.Top = GetSetting("GesPet", "Preferencias\Historial\Posicion", "Top", 3495)
    Me.Left = GetSetting("GesPet", "Preferencias\Historial\Posicion", "Left", 4005)
    Me.Height = GetSetting("GesPet", "Preferencias\Historial\Tamanio", "Height", 7950)
    Me.Width = GetSetting("GesPet", "Preferencias\Historial\Tamanio", "Width", 11175)
    '}
    Dim auxNro As String
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
            auxNro = "S/N"
        Else
            auxNro = ClearNull(aplRST!pet_nroasignado)
        End If
        lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
        lblPrioridad = ClearNull(aplRST!prioridad)
        lblEstado.Caption = ClearNull(aplRST!nom_estado)
    End If
    lUltimoRegistroCargado = 0
    Call CargarGrid(DateAdd("d", -900, date), date)
    If SoyElAnalista And InStr(1, "DESA|TEST|", glENTORNO, vbTextCompare) > 0 Then
        cmdDelHist.visible = True
        cmdDelHist.Enabled = True
    End If
    fraTitulo.Enabled = True
    rowSel = -1
    Call Puntero(False)
    Call IniciarScroll(grdDatos)
    'Call CargarArbol
End Sub

Private Sub CargarArbol()
    Dim nodo As MSComctlLib.node
    'Dim proyecto As String
    'Dim nodoN1 As String
    'Dim nodoN2 As String
    'Dim icono As Byte
    
    With trwHistorial
        .Nodes.Clear
        .Font.name = "Consolas"
        .Font.Size = 8
        If sp_GetUnaPeticion(glNumeroPeticion) Then
            Set nodo = .Nodes.Add(, , "RAIZ", "Petici�n Nro. " & ClearNull(aplRST.Fields!pet_nroasignado), 1)
            nodo.Bold = True
            
            Set nodo = .Nodes.Add("RAIZ", tvwChild, "GENERAL", "Datos", 1)
            nodo.Expanded = True
            nodo.Bold = True
            Set nodo = .Nodes.Add("GENERAL", tvwChild, "Titulo", "T�tulo: " & ClearNull(aplRST.Fields!Titulo), 1)
            Set nodo = .Nodes.Add("GENERAL", tvwChild, "Tipo", "Tipo: " & ClearNull(aplRST.Fields!cod_tipo_peticion), 1)
            Set nodo = .Nodes.Add("GENERAL", tvwChild, "Clase", "Clase: " & ClearNull(aplRST.Fields!cod_clase), 1)
            Set nodo = .Nodes.Add("GENERAL", tvwChild, "Estado", "Estado: " & ClearNull(aplRST.Fields!nom_estado), 1)
            Set nodo = .Nodes.Add("GENERAL", tvwChild, "FechaEstado", "Fecha estado: " & ClearNull(aplRST.Fields!fe_estado), 1)
            Set nodo = .Nodes.Add("GENERAL", tvwChild, "BPAR", "Referente de Sistema: " & ClearNull(aplRST.Fields!cod_bpar), 1)
            '.Nodes(1).
            Set nodo = .Nodes.Add("RAIZ", tvwChild, "TEXTOS", "Detalles", 1): nodo.Bold = True
            
            Set nodo = .Nodes.Add("RAIZ", tvwChild, "ADJUNTOS", "Adjuntos", 1): nodo.Bold = True
            Set nodo = .Nodes.Add("RAIZ", tvwChild, "CONFORMES", "Conformes", 1): nodo.Bold = True
            
            Set nodo = .Nodes.Add("RAIZ", tvwChild, "SECTORES", "Sectores", 1): nodo.Bold = True
            Set nodo = .Nodes.Add("RAIZ", tvwChild, "GRUPOS", "Grupos", 1): nodo.Bold = True
            Set nodo = .Nodes.Add("RAIZ", tvwChild, "RECURSOS", "Recursos", 1): nodo.Bold = True
            
            'nodo.Selected
        End If
        
        If sp_GetPeticionSectorXt(glNumeroPeticion, Null) Then
            Do While Not aplRST.EOF
                Set nodo = .Nodes.Add("SECTORES", tvwChild, "SECT" & ClearNull(aplRST.Fields!cod_sector), ClearNull(aplRST.Fields!nom_sector), 1)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        
        If sp_GetPeticionGrupoXt(glNumeroPeticion, Null, Null) Then
            Do While Not aplRST.EOF
                Set nodo = .Nodes.Add("GRUPOS", tvwChild, "GRUP" & ClearNull(aplRST.Fields!cod_grupo), ClearNull(aplRST.Fields!nom_grupo), 1)
                Set nodo = .Nodes.Add("GRUP" & ClearNull(aplRST.Fields!cod_grupo), tvwChild, "ESTADO" & ClearNull(aplRST.Fields!cod_grupo), ClearNull(aplRST.Fields!nom_estado))
                Set nodo = .Nodes.Add("GRUP" & ClearNull(aplRST.Fields!cod_grupo), tvwChild, "FECHA" & ClearNull(aplRST.Fields!cod_grupo), ClearNull(aplRST.Fields!fe_estado))
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        
        'If Not flgCargados Then
            'Call sp_GetProyectoIDM2(Null, Null, Null, Null, IIf(chkAgrupamiento.Value = 1, "P", "P"), Null)
'        If Not sp_GetHistorialXt(glNumeroPeticion, Null) Then
'            grdDatos.Visible = True
'            Exit Sub
'        End If
        '    flgCargados = True
        'End If
'        Do While Not aplRST.EOF
'            proyecto = ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & "." & ClearNull(aplRST.Fields!ProjSubSId) & " - " & ClearNull(aplRST.Fields!projnom)
'            Select Case ClearNull(aplRST.Fields!nivel)
'                Case "3"
'                    icono = IMG_SUBSUBP
'                    Set nodo = .Nodes.Add(IIf(nodoN2 = "", "RAIZ", nodoN2), tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
'                Case "2"
'                    icono = IMG_SUBPROY
'                    nodoN2 = "A" & ClearNull(aplRST.Fields!orden)
'                    Set nodo = .Nodes.Add(IIf(nodoN1 = "", "RAIZ", nodoN1), tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
'                Case "1"
'                    icono = IMG_PROYECTO
'                    nodoN1 = "A" & ClearNull(aplRST.Fields!orden)
'                    nodoN2 = ""
'                    Set nodo = .Nodes.Add("RAIZ", tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
'            End Select
'            aplRST.MoveNext
'            DoEvents
'        Loop
        'sbProyectos.SimpleText = aplRST.RecordCount & " proyecto(s)."
        .Nodes(1).Expanded = True
    End With
End Sub

'{ add -006- d.
Private Sub Form_Resize()
    'Debug.Print "Height: " & Me.Height
    'Debug.Print "Width:  " & Me.Width
    If Me.WindowState <> vbMinimized Then           ' Horizontal
        If Me.WindowState <> vbMaximized Then
            'Me.Top = 0
            'Me.Left = 0
            If Me.Height < 7050 Then Me.Height = 7050
            If Me.Width < 11565 Then Me.Width = 11565
        End If
        If Me.ScaleWidth > 1 Then
            If Me.ScaleWidth > 4080 Then
                grdDatos.Width = Me.ScaleWidth - 80 ' - trwHistorial.Width - 80 (ESTO ACTIVAR PARA EL ARBOL!)
                fraTitulo.Width = Me.ScaleWidth - 80
                fraRpt.Width = Me.ScaleWidth - 1150
                txtMensaje.Width = fraRpt.Width - 200
                fraOpciones.Left = fraRpt.Width + 100
                lblTituloOtro.Left = fraTitulo.Width - lblPrioridad.Width - 1200
                lblPrioridad.Left = lblTituloOtro.Left + lblTituloOtro.Width + 200
                chkEventosDeSistema.Left = fraTitulo.Width - chkEventosDeSistema.Width - 100
            End If
        End If
        ' Vertical
        If Me.ScaleHeight > 4515 Then
            grdDatos.Height = Me.ScaleHeight - fraTitulo.Height - fraRpt.Height - 100 ' 2900
            fraRpt.Top = fraTitulo.Height + grdDatos.Height + 60
            fraOpciones.Top = fraTitulo.Height + grdDatos.Height + 60
        End If
    End If
End Sub
'}

Private Sub cmdAddHist_Click()
    frmHistorialAdd.Show 1
    Call CargarGrid(DateAdd("d", -900, date), date)
End Sub

Private Sub cmdOK_Click()
    Call GuardarPreferencias
    Unload Me
End Sub

'{ add -003- a.
Private Sub cmdDelHist_Click()
    Dim nRow As Integer
    Dim nRwD As Integer
    Dim nRwH As Integer
        
    If grdDatos.Rows > 1 Then
        If grdDatos.row <= grdDatos.rowSel Then
            nRwD = grdDatos.row
            nRwH = grdDatos.rowSel
        Else
            nRwH = grdDatos.row
            nRwD = grdDatos.rowSel
        End If
         
        If nRwD <> nRwH Then
            ' Eliminaci�n m�ltiple
            If MsgBox("�Confirma la eliminaci�n definitiva de estos" & vbCrLf & CInt(nRwH - nRwD) + 1 & " registros seleccionados del historial?", vbQuestion + vbYesNo) = vbYes Then
                Do While nRwD <= nRwH
                    sp_DeleteHistorialPet glNumeroPeticion, grdDatos.TextMatrix(nRwD, colNroInterno)
                    nRwD = nRwD + 1
                    DoEvents
                Loop
                Call CargarGrid(DateAdd("d", -900, date), date)
            End If
            Exit Sub
        Else
            ' Eliminaci�n simple
            If MsgBox("�Confirma la eliminaci�n de este registro del historial?", vbQuestion + vbYesNo) = vbYes Then
                sp_DeleteHistorialPet glNumeroPeticion, grdDatos.TextMatrix(nRwD, colNroInterno)
                Call CargarGrid(DateAdd("d", -900, date), date)
            End If
            Exit Sub
        End If
    Else
        MsgBox "No existen datos para eliminar.", vbExclamation + vbOKOnly
    End If
End Sub
'}

Private Sub CargarGrid(fDes As Date, fHas As Date)
    Const USUARIO_CONTROLM_PRODUCCION = "JP00"
    Const USUARIO_SGI = "GesIncCone"
    Const USUARIO_CONTROLM_DESARROLLO = "OdeGesPet"
    
    
    Dim cObservaciones As String
    Dim lPos1 As Long
    Dim lPos2 As Long
    Dim bSystemProcess As Boolean
    Dim evento As String
    
    With grdDatos
        .visible = False
        .Clear
        .cols = colTOTCOLS
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusNone
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .RowHeightMin = 270     ' 280
        .Rows = 1
        .TextMatrix(0, colFECHA) = "Fecha y hora": .ColWidth(colFECHA) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth01", 1500): .ColAlignment(colFECHA) = flexAlignLeftCenter
        .TextMatrix(0, colEvento) = "Evento": .ColWidth(colEvento) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth02", 2400): .ColAlignment(colEvento) = flexAlignLeftCenter
        .TextMatrix(0, colESTADO) = "Estado Petici�n": .ColWidth(colESTADO) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth03", 1500): .ColAlignment(colESTADO) = flexAlignLeftCenter
        .TextMatrix(0, colUsrAudit) = "Usuario": .ColWidth(colUsrAudit) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth05", 2000): .ColAlignment(colUsrAudit) = flexAlignLeftCenter
        .TextMatrix(0, colUsrRplc) = "En nombre de": .ColWidth(colUsrRplc) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth04", 2000): .ColAlignment(colUsrRplc) = flexAlignLeftCenter
        .TextMatrix(0, colUSRREPLCPERFIL) = "Con perfil de": .ColWidth(colUSRREPLCPERFIL) = 2000: .ColAlignment(colUSRREPLCPERFIL) = flexAlignLeftCenter
        .TextMatrix(0, colObser) = "Observaciones": .ColWidth(colObser) = 0: .ColAlignment(colObser) = flexAlignLeftCenter
        .TextMatrix(0, colSector) = "Sector": .ColWidth(colSector) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth07", 2000): .ColAlignment(colSector) = flexAlignLeftCenter
        .TextMatrix(0, colSectorEst) = "Estado Sector": .ColWidth(colSectorEst) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth08", 1300): .ColAlignment(colSectorEst) = flexAlignLeftCenter
        .TextMatrix(0, colGrupo) = "Grupo": .ColWidth(colGrupo) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth09", 2000): .ColAlignment(colGrupo) = flexAlignLeftCenter
        .TextMatrix(0, colGrupoEst) = "Estado Grupo": .ColWidth(colGrupoEst) = GetSetting("GesPet", "Preferencias\Historial", "ColWidth10", 1300): .ColAlignment(colGrupoEst) = flexAlignLeftCenter
        .TextMatrix(0, colNroInterno) = "N� interno": .ColWidth(colNroInterno) = 0: .ColWidth(colFechaSort) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetHistorialXt(glNumeroPeticion, Null) Then
        grdDatos.visible = True
        Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdDatos
            evento = ClearNull(aplRST!cod_evento)
            bSystemProcess = False
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colFECHA) = IIf(Not IsNull(aplRST!hst_fecha), Format(aplRST!hst_fecha, "dd/mm/yyyy   hh:mm"), "")    ' upd -004- a. Se cambia el formato para mostrar la hora del evento
            .TextMatrix(.Rows - 1, colEvento) = ClearNull(aplRST!nom_accion)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!pet_nom_estado)
            'If UCase(ClearNull(aplRST!audit_user)) = "JP00" Then
            If InStr(1, USUARIO_SGI & "|" & USUARIO_CONTROLM_DESARROLLO & "|" & USUARIO_CONTROLM_PRODUCCION & "|", UCase(ClearNull(aplRST!audit_user)), vbTextCompare) > 0 Then
                .TextMatrix(.Rows - 1, colUsrAudit) = "- Proceso de sistema -"
                .TextMatrix(.Rows - 1, colUsrRplc) = ""
                bSystemProcess = True
            Else
                .TextMatrix(.Rows - 1, colUsrAudit) = ClearNull(aplRST!audit_user) & ": " & ClearNull(aplRST!nom_audit)
                .TextMatrix(.Rows - 1, colUsrRplc) = ClearNull(aplRST!replc_user) & ": " & ClearNull(aplRST!nom_replc)
            End If
            'If bSystemProcess Then Call PintarLinea(grdDatos, prmGridFillRowColorLightOrange)
            'If ClearNull(aplRST!cod_evento) = "HSTADD" Then Call PintarLinea(grdDatos, prmGridFillRowColorLightBlue2)
            .TextMatrix(.Rows - 1, colObser) = ClearNull(aplRST!mem_texto)
            .TextMatrix(.Rows - 1, colSector) = IIf(ClearNull(aplRST!nom_sector) = "", "-", ClearNull(aplRST!nom_sector))                ' upd -006- a.
            .TextMatrix(.Rows - 1, colSectorEst) = IIf(ClearNull(aplRST!sec_nom_estado) = "", "-", ClearNull(aplRST!sec_nom_estado))     ' upd -006- a.
            .TextMatrix(.Rows - 1, colGrupo) = IIf(ClearNull(aplRST!nom_grupo) = "", "-", ClearNull(aplRST!nom_grupo))                   ' upd -006- a.
            .TextMatrix(.Rows - 1, colGrupoEst) = IIf(ClearNull(aplRST!gru_nom_estado) = "", "-", ClearNull(aplRST!gru_nom_estado))      ' upd -006- a.     ' ------------
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!hst_nrointerno)
            .TextMatrix(.Rows - 1, colUSRREPLCPERFIL) = IIf(ClearNull(aplRST!nom_perfil) = "", "-", ClearNull(aplRST!nom_perfil))
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)                       ' add -019- b.
            'If bSystemProcess Then Call PintarLinea(grdDatos, prmGridFillRowColorLightOrange)
            Select Case evento
                    Case "PAUTHOM", "GNEW001", "GCHGEST1", "GCHGEST2"
                        bSystemProcess = True
                End Select
            If ClearNull(aplRST!cod_evento) = "HSTADD" Then Call PintarLinea(grdDatos, prmGridFillRowColorLightBlue2)
            'If InStr(1, "PAUTHOM|GNEW001|GCHGEST1|GCHGEST2|", ClearNull(aplRST!cod_evento), vbTextCompare) > 0 Then Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorBlueLink)
            If chkEventosDeSistema.value = 1 And bSystemProcess Then
                Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorBlueLink)
            End If
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    If rowSel > 0 Then
        grdDatos.row = rowSel: grdDatos.ColSel = grdDatos.cols - 1
    End If
    grdDatos.visible = True
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .rowSel > 0 Then
            If .TextMatrix(.rowSel, colNroInterno) <> "" Then
                If lUltimoRegistroCargado <> CLng(.TextMatrix(.rowSel, colNroInterno)) Then
                    lUltimoRegistroCargado = CLng(.TextMatrix(.rowSel, colNroInterno))
                    txtMensaje = sp_GetHistorialMemo(.TextMatrix(.rowSel, colNroInterno))  ' del -010- a.
                    'txtMensaje = Trim(.TextMatrix(.RowSel, colObser))                       ' add -010- a.
                End If
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
'    With grdDatos
'        'If .MouseRow = 0 And .Rows > 1 Then
'        If .RowSel = 1 And .Rows > 1 Then
'           '.RowSel = 1
'            If .MouseCol = colFECHA Then
'                '.Col = colNroInterno
'                .Col = colFECHA
'                .Sort = flexSortStringNoCaseDescending
'            Else
'                .Col = .MouseCol
'                .Sort = flexSortStringNoCaseAscending
'            End If
'        End If
'    End With
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    If Not bFormateando Then    ' add -006- c.
        If grdDatos.col <> colFechaSort Then
            Call MostrarSeleccion
        End If
    End If                      ' add -006- c.
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

'{ add -005- a.
Private Sub GuardarPreferencias()
    'Call Status("Guardando preferencias...")
    With grdDatos
        ' Anchos de columnas
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth01", .ColWidth(colFECHA)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth02", .ColWidth(colEvento)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth03", .ColWidth(colESTADO)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth04", .ColWidth(colUsrRplc)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth05", .ColWidth(colUsrAudit)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth06", .ColWidth(colObser)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth07", .ColWidth(colSector)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth08", .ColWidth(colSectorEst)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth09", .ColWidth(colGrupo)
        SaveSetting "GesPet", "Preferencias\Historial", "ColWidth10", .ColWidth(colGrupoEst)
        ' Posicionamiento y tama�o
        SaveSetting "GesPet", "Preferencias\Historial\Posicion", "Top", Me.Top
        SaveSetting "GesPet", "Preferencias\Historial\Posicion", "Left", Me.Left
        SaveSetting "GesPet", "Preferencias\Historial\Tamanio", "Height", Me.Height
        SaveSetting "GesPet", "Preferencias\Historial\Tamanio", "Width", Me.Width
    End With
    'Call Status("Listo.")
End Sub
'}

'{ add -008- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}

Private Sub chkEventosDeSistema_Click()
    rowSel = -1
    If grdDatos.rowSel > 0 Then rowSel = grdDatos.rowSel
    Call CargarGrid(DateAdd("d", -900, date), date)
End Sub
