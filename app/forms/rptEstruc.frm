VERSION 5.00
Begin VB.Form rptEstruc 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4695
   ClientLeft      =   795
   ClientTop       =   2025
   ClientWidth     =   10305
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4695
   ScaleWidth      =   10305
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      Height          =   3225
      Left            =   0
      TabIndex        =   1
      Top             =   840
      Width           =   10260
      Begin VB.Frame fraNivel 
         Caption         =   "Nivel de detalle"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1605
         Left            =   900
         TabIndex        =   7
         Top             =   1080
         Width           =   3345
         Begin VB.OptionButton OptNivel 
            Caption         =   "�reas y responsables"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   300
            TabIndex        =   11
            Top             =   600
            Width           =   1965
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Perfiles"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   300
            TabIndex        =   10
            Top             =   1080
            Width           =   1245
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Recursos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   1
            Left            =   300
            TabIndex        =   9
            Top             =   840
            Width           =   1125
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "�reas y Referentes de Sistema"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   300
            TabIndex        =   8
            Top             =   360
            Width           =   2685
         End
      End
      Begin VB.ComboBox cboEstructura 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   900
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   540
         Width           =   9105
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "�rea"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   345
      End
   End
   Begin VB.Frame fraOpciones 
      Height          =   645
      Left            =   0
      TabIndex        =   2
      Top             =   4020
      Width           =   10260
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8280
         TabIndex        =   4
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9240
         TabIndex        =   3
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "5. ESTRUCTURA Y RECURSOS"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2715
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptEstruc.frx":0000
      Stretch         =   -1  'True
      Top             =   0
      Width           =   10260
   End
End
Attribute VB_Name = "rptEstruc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 15.05.2008 - Se cambia la asignaci�n de la propiedad 'ReportFileName' del control CrystalReport utilizando la funci�n de formato de ruta corto (8.3) porque por ejemplo, cuando el string tiene 127 caracteres da un error 20504 en runtime al momento de mostrar el reporte (da error de archivo de reporte no encontrado).
' -003- a. FJS 03.09.2009 - Se restringe la carga del combobox solo para �reas habilitadas, recursos habiltados, etc.
' -003- b. FJS 03.09.2009 - Se modifica el mensaje de status.
' -004- a. FJS 15.03.2010 - Se agrega un nuevo reporte para informar las �reas y los responsables de dichas �reas.
' -005- a. FJS 19.05.2015 - Nuevo: se agrega el llamado a la versi�n del reporte que informa los responsables de las �reas listadas.

Option Explicit

Dim bEnCarga As Boolean
Dim xNivel As Long

Private Sub Form_Load()
    bEnCarga = False
    Call InicializarCombos
    OptNivel(1).Value = True
End Sub

Private Sub cmdOK_Click()
    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        'Call ViejoReporte
    End If
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim vRetorno() As String
    Dim sReportName As String
        
    Call Puntero(True)
    sReportName = IIf(xNivel = 3, "estruct26.rpt", "\estruct6.rpt")         ' upd -005- a.
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crPortrait
        .ReportTitle = "Estructura y Recursos"
        .ReportComments = "Area: " & ClearNull(TextoCombo(cboEstructura))
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
 
    'Abrir el reporte
    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
        MsgBox ("Error")
        Exit Sub
    End If
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@cod_direccion"
                crParamDef.AddCurrentValue (vRetorno(1))
            Case "@cod_gerencia"
                crParamDef.AddCurrentValue (vRetorno(2))
            Case "@cod_sector"
                crParamDef.AddCurrentValue (vRetorno(3))
            Case "@cod_grupo"
                crParamDef.AddCurrentValue (vRetorno(4))
            Case "@detalle": crParamDef.AddCurrentValue (CStr(xNivel))
            Case "@flg_habil": crParamDef.AddCurrentValue ("S")
        End Select
    Next
   
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub InicializarCombos()
    Dim sDireccion As String
    Dim sGerencia As String
    
    bEnCarga = True
    Call Puntero(True)
    'Call Status("Inicializando...")         ' add -003- b.
    
    If sp_GetDireccion("", "S") Then        ' upd -003- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            cboEstructura.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_direccion) & "|NULL|NULL|NULL"
            'cboEstructura.AddItem Trim(aplRST!nom_direccion) & Space(150) & "||" & Trim(aplRST!cod_direccion) & "|NULL|NULL|NULL"
            aplRST.MoveNext
        Loop
        'aplRST.Close
    End If
    If sp_GetGerenciaXt("", "", "S") Then   ' upd -003- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cboEstructura.AddItem sDireccion & "� " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|NULL|NULL"
            'cboEstructura.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(100) & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|NULL|NULL"
            aplRST.MoveNext
        Loop
        'aplRST.Close
    End If
    If sp_GetSectorXt(Null, Null, Null, "S") Then   ' upd -003- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cboEstructura.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|" & Trim(aplRST!cod_sector) & "|NULL"
            'cboEstructura.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(100) & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|" & Trim(aplRST!cod_sector) & "|NULL"
            aplRST.MoveNext
        Loop
        'aplRST.Close
    End If
    If sp_GetGrupoXt("", "", "S") Then  ' upd -003- a.
        Do While Not aplRST.EOF
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cboEstructura.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
            'cboEstructura.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(80) & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
            aplRST.MoveNext
        Loop
        'aplRST.Close
    End If
    cboEstructura.AddItem "Todo el BBVA" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL|NULL|NULL|NULL", 0
    cboEstructura.ListIndex = 0
    
    bEnCarga = False
    'Call Status("Listo")
    Call Puntero(False)
End Sub

Private Sub OptNivel_Click(Index As Integer)
    xNivel = Index
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

'Private Sub ViejoReporte()
'    Dim txtAux As String
'    Dim sTitulo As String
'    Dim xRet As Integer
'    Dim vRetorno() As String
'
'    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
'        MsgBox ("Error")
'    End If
'    sTitulo = "Area: " & ClearNull(TextoCombo(cboEstructura))
'    'mdiPrincipal.CrystalReport1.Connect = "DSN=" & mdiPrincipal.AdoConnection.Servidor & ";SRVR=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'    'mdiPrincipal.CrystalReport1.UserName = mdiPrincipal.AdoConnection.UsuarioAplicacion
'    'mdiPrincipal.CrystalReport1.Password = mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad
'    'xRet = mdiPrincipal.CrystalReport1.LogOnServer("PdSODBC.DLL", "CRREP", "atr", "E037799", "123456pwddif")
'    'mdiPrincipal.CrystalReport1.ReportFileName = App.Path & "\" & "estruct.rpt"    ' del -002- a.
'    '{ add -002- a.
'    Dim cPathFileNameReport As String
'    '{ add -004- a.
'    If xNivel = 3 Then
'        cPathFileNameReport = App.Path & "\" & "estruct2.rpt"
'    Else
'    '}
'        cPathFileNameReport = App.Path & "\" & "estruct.rpt"
'    End If  ' add -004- a.
'    mdiPrincipal.CrystalReport1.ReportFileName = GetShortName(cPathFileNameReport)
'    '}
'    mdiPrincipal.CrystalReport1.Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'
'    'DSN = name;UID = userID;PWD = password;DSQ = database qualifier
'
'    mdiPrincipal.CrystalReport1.SelectionFormula = ""
'    mdiPrincipal.CrystalReport1.RetrieveStoredProcParams
'    mdiPrincipal.CrystalReport1.StoredProcParam(0) = vRetorno(1)
'    mdiPrincipal.CrystalReport1.StoredProcParam(1) = vRetorno(2)
'    mdiPrincipal.CrystalReport1.StoredProcParam(2) = vRetorno(3)
'    mdiPrincipal.CrystalReport1.StoredProcParam(3) = vRetorno(4)
'    mdiPrincipal.CrystalReport1.StoredProcParam(4) = IIf(xNivel = 3, "S", xNivel)   ' upd -004- a.
'
'    mdiPrincipal.CrystalReport1.Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'    mdiPrincipal.CrystalReport1.Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'    mdiPrincipal.CrystalReport1.Formulas(2) = "@0_TITULO='" & sTitulo & "'"
'    If xNivel <> 3 Then     ' add -004- a.
'        mdiPrincipal.CrystalReport1.Formulas(3) = "@1_RECURSO='" & IIf(xNivel = "2", "Referente de Sistema", "Recurso") & "'"
'    End If  ' add -004- a.
'    mdiPrincipal.CrystalReport1.WindowLeft = 1
'    mdiPrincipal.CrystalReport1.WindowTop = 1
'    mdiPrincipal.CrystalReport1.WindowState = crptMaximized
'
'    mdiPrincipal.CrystalReport1.Action = 1
'    mdiPrincipal.CrystalReport1.Formulas(2) = ""
'    mdiPrincipal.CrystalReport1.Formulas(3) = ""
'   'Call cmdCancelar_Click
'End Sub

'Private Sub Command1_Click()
'    Dim X As Printer
'
'    Debug.Print "Por defecto: " & Printer.DeviceName
'    For Each X In Printers
'        Debug.Print "Otras: " & X.DeviceName
'        'If X.Orientation = vbPRORPortrait Then
'        '    ' Set printer as system default.
'        '    Set Printer = X
'        '    ' Stop looking for a printer.
'        '    Exit For
'        'End If
'    Next
'End Sub

