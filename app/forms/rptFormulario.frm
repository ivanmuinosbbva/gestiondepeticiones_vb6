VERSION 5.00
Begin VB.Form rptFormulario 
   Appearance      =   0  'Flat
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Impresión de formularios"
   ClientHeight    =   2250
   ClientLeft      =   795
   ClientTop       =   2295
   ClientWidth     =   5940
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2250
   ScaleWidth      =   5940
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   5835
      Begin VB.ComboBox cboCopias 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "rptFormulario.frx":0000
         Left            =   1860
         List            =   "rptFormulario.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   900
         Width           =   705
      End
      Begin VB.CheckBox ckHisto 
         Caption         =   "Imprime historial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1860
         TabIndex        =   5
         Top             =   540
         Width           =   1515
      End
      Begin VB.CheckBox ckFormu 
         Caption         =   "Imprime formulario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1860
         TabIndex        =   4
         Top             =   240
         Width           =   1635
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "copias"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2640
         TabIndex        =   7
         Top             =   960
         Width           =   450
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   60
      TabIndex        =   1
      Top             =   1560
      Width           =   5835
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   385
         Left            =   3960
         TabIndex        =   3
         Top             =   180
         Width           =   855
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   385
         Left            =   4860
         TabIndex        =   2
         Top             =   180
         Width           =   855
      End
   End
End
Attribute VB_Name = "rptFormulario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Public m_seleccion As Boolean

Dim m_chkFormu As Boolean
Dim m_chkHisto As Boolean
Dim m_Copias As Integer

Private Sub Form_Load()
    cboCopias.Clear
    cboCopias.AddItem "1"
    cboCopias.AddItem "2"
    cboCopias.AddItem "3"
    cboCopias.AddItem "4"
    cboCopias.AddItem "5"
    
    m_seleccion = False
    m_chkHisto = False
    m_chkFormu = False
    m_Copias = 1
    cboCopias.ListIndex = 0
    cmdOk.Enabled = False
    ckFormu.Value = 1
End Sub

Public Property Get Copias() As Integer
    Copias = m_Copias
End Property

Public Property Get chkFormu() As Boolean
    chkFormu = m_chkFormu
End Property

Public Property Get chkHisto() As Boolean
    chkHisto = m_chkHisto
End Property

Private Sub cmdCancelar_Click()
    m_chkHisto = False
    m_chkFormu = False
    Unload Me
End Sub

Private Sub cmdOK_Click()
    m_seleccion = True
    Unload Me
End Sub

Private Sub cboCopias_Click()
    m_Copias = CInt(Val(cboCopias))
End Sub

Private Sub ckFormu_Click()
    If ckFormu.Value = 1 Or ckHisto.Value = 1 Then
        cmdOk.Enabled = True
    Else
        cmdOk.Enabled = False
    End If
    If ckFormu.Value = 1 Then
        m_chkFormu = True
    Else
        m_chkFormu = False
    End If
End Sub

Private Sub ckHisto_Click()
    If ckFormu.Value = 1 Or ckHisto.Value = 1 Then
        cmdOk.Enabled = True
    Else
        cmdOk.Enabled = False
    End If
    If ckHisto.Value = 1 Then
        m_chkHisto = True
    Else
        m_chkHisto = False
    End If
End Sub
