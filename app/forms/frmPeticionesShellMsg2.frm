VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Begin VB.Form frmPeticionesShellMsg2 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Bandeja de mensajes"
   ClientHeight    =   5655
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   8115
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5655
   ScaleWidth      =   8115
   ShowInTaskbar   =   0   'False
   Begin ComCtl3.CoolBar CoolBar1 
      Align           =   1  'Align Top
      Height          =   765
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   1349
      _CBWidth        =   8115
      _CBHeight       =   765
      _Version        =   "6.7.9782"
      Child1          =   "DTPicker1"
      MinWidth1       =   300
      MinHeight1      =   315
      Width1          =   810
      NewRow1         =   0   'False
      Child2          =   "cboPerfil"
      MinHeight2      =   315
      Width2          =   1770
      NewRow2         =   -1  'True
      Child3          =   "tbMain"
      MinHeight3      =   360
      Width3          =   2715
      NewRow3         =   0   'False
      Begin VB.ComboBox cboPerfil 
         Height          =   315
         Left            =   165
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   390
         Width           =   1575
      End
      Begin MSComctlLib.Toolbar tbMain 
         Height          =   360
         Left            =   1965
         TabIndex        =   2
         Top             =   375
         Width           =   6060
         _ExtentX        =   10689
         _ExtentY        =   635
         ButtonWidth     =   609
         Appearance      =   1
         Style           =   1
         ImageList       =   "imgMsg"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   6
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   4
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   5
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               ImageIndex      =   6
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
   End
   Begin MSComctlLib.ImageList imgMsg 
      Left            =   7380
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesShellMsg2.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesShellMsg2.frx":059A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesShellMsg2.frx":0B34
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesShellMsg2.frx":10CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesShellMsg2.frx":1668
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesShellMsg2.frx":1C02
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswDatos 
      Height          =   4755
      Left            =   60
      TabIndex        =   0
      Top             =   840
      Width           =   7995
      _ExtentX        =   14102
      _ExtentY        =   8387
      SortKey         =   3
      View            =   3
      SortOrder       =   -1  'True
      Sorted          =   -1  'True
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      SmallIcons      =   "imgMsg"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "frmPeticionesShellMsg2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Call Inicializar
End Sub

Private Sub Form_Resize()
    lswDatos.Height = Me.ScaleHeight - CoolBar1.Height - 100
    lswDatos.Width = Me.ScaleWidth - 100
End Sub

Private Sub Inicializar()
    Dim itmX As ListItem
    
    Me.Height = 3765
    Me.Width = 8235
    With lswDatos
        ' Agrega objetos ColumnHeaders
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "Petici�n", 1400
        .ColumnHeaders.Add , , "T�tulo", 5000 ', lvwColumnCenter
        .ColumnHeaders.Add , , "Propietario", 1000
        .ColumnHeaders.Add , , "Fecha y hora", 1200 ', lvwColumnCenter
        .ColumnHeaders.Add , , "Mensaje", 6000 '
        ' Asigna la propiedad View el valor Report.
        .View = lvwReport
        .ListItems.Clear
        
        .FullRowSelect = True
    End With
    
    'If Not sp_GetMensajeRecurso(glLOGIN_ID_REEMPLAZO, "NULL", "NULL") Then
    If Not sp_GetMensajeRecurso("A119411", "NULL", "NULL") Then
       
    End If
    Do While Not aplRST.EOF
        With lswDatos
            Set itmX = .ListItems.Add(, "A" & ClearNull(aplRST!msg_nrointerno), ClearNull(aplRST!pet_nroasignado), , 3)
            itmX.SubItems(1) = ClearNull(aplRST!Titulo)
            itmX.SubItems(2) = ClearNull(aplRST!cod_perfil)
            itmX.SubItems(3) = IIf(Not IsNull(aplRST!fe_mensaje), Format(aplRST!fe_mensaje, "dd/MM/yyyy"), "-")
            itmX.SubItems(4) = ClearNull(aplRST!msg_texto) & " " & ClearNull(aplRST!txt_txtmsg)
            itmX.Bold = True
        End With
        aplRST.MoveNext
        DoEvents
    Loop
End Sub
