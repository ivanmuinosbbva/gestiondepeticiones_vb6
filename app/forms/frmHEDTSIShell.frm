VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmHEDTSIShell 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Visado sobre el catálogo de enmascaramiento de datos"
   ClientHeight    =   7380
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10140
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHEDTSIShell.frx":0000
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7380
   ScaleWidth      =   10140
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraBotonera 
      Height          =   7335
      Left            =   8760
      TabIndex        =   0
      Top             =   0
      Width           =   1335
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   500
         Left            =   80
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operación"
         Top             =   6210
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   80
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   6720
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   500
         Left            =   80
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Recurso seleccionado"
         Top             =   5700
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   500
         Left            =   80
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el Recurso seleccionado"
         Top             =   5190
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   500
         Left            =   80
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Recurso"
         Top             =   4680
         Width           =   1170
      End
      Begin VB.CommandButton cmdVisar 
         Caption         =   "Visar"
         Height          =   500
         Left            =   80
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operación"
         Top             =   160
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   7095
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   12515
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmHEDTSIShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    'inCarga = True
    Inicializar
    Cargar_Grilla
    'HabilitarBotones 0, "NO"
    'inCarga = False
    Call Status("Listo.")
End Sub
