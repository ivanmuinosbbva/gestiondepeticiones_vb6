VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmRecursoPreferencias 
   Caption         =   "Preferencias del usuario"
   ClientHeight    =   6990
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8910
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   6990
   ScaleWidth      =   8910
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraPreferencias 
      Height          =   6975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8895
      Begin TabDlg.SSTab SSTab1 
         Height          =   6015
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   10610
         _Version        =   393216
         Style           =   1
         Tabs            =   9
         TabsPerRow      =   9
         TabHeight       =   520
         TabCaption(0)   =   " General "
         TabPicture(0)   =   "frmRecursoPreferencias.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "chkGeneral_IniciarTips"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Check1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraGeneral_Seguridad"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Check4"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   " Carga de horas "
         TabPicture(1)   =   "frmRecursoPreferencias.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "chkCargaHoras_Porcentajes"
         Tab(1).Control(1)=   "chkCargaHoras_Feriados"
         Tab(1).ControlCount=   2
         TabCaption(2)   =   " Adjuntar archivos "
         TabPicture(2)   =   "frmRecursoPreferencias.frx":0038
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "optModoSeleccion(2)"
         Tab(2).Control(1)=   "Frame1"
         Tab(2).Control(2)=   "Label1"
         Tab(2).ControlCount=   3
         TabCaption(3)   =   " Historial "
         TabPicture(3)   =   "frmRecursoPreferencias.frx":0054
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Check2"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "Tab 4"
         TabPicture(4)   =   "frmRecursoPreferencias.frx":0070
         Tab(4).ControlEnabled=   0   'False
         Tab(4).ControlCount=   0
         TabCaption(5)   =   "Tab 5"
         TabPicture(5)   =   "frmRecursoPreferencias.frx":008C
         Tab(5).ControlEnabled=   0   'False
         Tab(5).ControlCount=   0
         TabCaption(6)   =   "Tab 6"
         TabPicture(6)   =   "frmRecursoPreferencias.frx":00A8
         Tab(6).ControlEnabled=   0   'False
         Tab(6).ControlCount=   0
         TabCaption(7)   =   "Tab 7"
         TabPicture(7)   =   "frmRecursoPreferencias.frx":00C4
         Tab(7).ControlEnabled=   0   'False
         Tab(7).ControlCount=   0
         TabCaption(8)   =   "Tab 8"
         TabPicture(8)   =   "frmRecursoPreferencias.frx":00E0
         Tab(8).ControlEnabled=   0   'False
         Tab(8).ControlCount=   0
         Begin VB.OptionButton optModoSeleccion 
            Caption         =   "Modo multiple nativo de Windows �"
            Height          =   255
            Index           =   2
            Left            =   -74760
            TabIndex        =   16
            Top             =   1200
            Width           =   3015
         End
         Begin VB.CheckBox Check4 
            Caption         =   "Habilitar la carga autom�tica por defecto del usuario del puesto de trabajo como usuario en el aplicativo"
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   1080
            Width           =   8175
         End
         Begin VB.Frame fraGeneral_Seguridad 
            Caption         =   " Seguridad "
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1815
            Left            =   120
            TabIndex        =   13
            Top             =   3960
            Width           =   5895
            Begin VB.CheckBox Check3 
               Caption         =   "Bloquear la estaci�n de trabajo si transcurre un determinado tiempo"
               Height          =   255
               Left            =   240
               TabIndex        =   14
               Top             =   360
               Width           =   5415
            End
         End
         Begin VB.CheckBox Check2 
            Caption         =   "Recordar la �ltima posici�n y tama�o de la ventana del historial entre sesiones"
            Height          =   255
            Left            =   -74760
            TabIndex        =   12
            Top             =   600
            Width           =   6255
         End
         Begin VB.CheckBox Check1 
            Caption         =   "No contabilizar d�as no h�biles ni feriados"
            Height          =   255
            Left            =   240
            TabIndex        =   11
            Top             =   840
            Width           =   3495
         End
         Begin VB.CheckBox chkGeneral_IniciarTips 
            Caption         =   "Mostrar los tips al iniciar la aplicaci�n"
            Height          =   255
            Left            =   240
            TabIndex        =   10
            Top             =   600
            Width           =   3255
         End
         Begin VB.CheckBox chkCargaHoras_Porcentajes 
            Caption         =   "Visualizar los porcentajes de horas cargadas y faltantes para el per�odo actual"
            Height          =   180
            Left            =   -74760
            TabIndex        =   9
            Top             =   840
            Width           =   6255
         End
         Begin VB.CheckBox chkCargaHoras_Feriados 
            Caption         =   "Visualizar los d�as no h�biles y feriados grisados al cargar horas"
            Height          =   180
            Left            =   -74760
            TabIndex        =   8
            Top             =   600
            Width           =   5175
         End
         Begin VB.Frame Frame1 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   615
            Left            =   -74880
            TabIndex        =   4
            Top             =   720
            Width           =   2655
            Begin VB.OptionButton optModoSeleccion 
               Caption         =   "Modo multiple autom�tico"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   6
               Top             =   240
               Width           =   2295
            End
            Begin VB.OptionButton optModoSeleccion 
               Caption         =   "Modo simple"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   5
               Top             =   0
               Width           =   1335
            End
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Modo de selecci�n de archivos para adjuntar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   -74880
            TabIndex        =   7
            Top             =   480
            Width           =   3285
         End
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   5280
         TabIndex        =   2
         Top             =   6480
         Width           =   1695
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "Guardar y cerrar"
         Default         =   -1  'True
         Height          =   375
         Left            =   7080
         TabIndex        =   1
         Top             =   6480
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frmRecursoPreferencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Inicializar
End Sub

Private Sub Inicializar()
    ' GENERAL
    If GetSetting("GesPet", "Preferencias\General", "IniciarTips") = "S" Then
        chkGeneral_IniciarTips.Value = 1
    Else
        chkGeneral_IniciarTips.Value = 0
    End If
    
    ' ADJUNTOS
    Select Case GetSetting("GesPet", "Preferencias\Adjuntos", "ModoSeleccion", "N")
        Case "N"
            ' Modo normal
            optModoSeleccion(0).Value = True
            optModoSeleccion(1).Value = False
        Case "M"
            ' Modo de m�ltiples documentos
            optModoSeleccion(1).Value = True
            optModoSeleccion(0).Value = False
    End Select
    
    ' HORAS
    If GetSetting("GesPet", "Preferencias\Horas", "Feriados", "N") = "S" Then
        chkCargaHoras_Feriados.Value = 1
    Else
        chkCargaHoras_Feriados.Value = 0
    End If
    
    If GetSetting("GesPet", "Preferencias\Horas", "Porcentajes", "N") = "S" Then
        chkCargaHoras_Porcentajes.Value = 1
    Else
        chkCargaHoras_Porcentajes.Value = 0
    End If
End Sub

Private Sub cmdGuardar_Click()
    ' GENERAL
    SaveSetting "GesPet", "Preferencias\General", "IniciarTips", IIf(chkGeneral_IniciarTips.Value = 1, "S", "N")
    
    ' ADJUNTOS
    SaveSetting "GesPet", "Preferencias\Adjuntos", "ModoSeleccion", IIf(optModoSeleccion(0).Value, "N", "M")
    
    ' HORAS
    SaveSetting "GesPet", "Preferencias\Horas", "Feriados", IIf(chkCargaHoras_Feriados.Value, "S", "N")
    SaveSetting "GesPet", "Preferencias\Horas", "Porcentajes", IIf(chkCargaHoras_Porcentajes.Value, "S", "N")
    
    cmdCerrar_Click
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

