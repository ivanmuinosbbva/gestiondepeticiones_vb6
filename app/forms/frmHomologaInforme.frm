VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmHomologaInforme 
   BorderStyle     =   0  'None
   Caption         =   "Spitz, Fernando J."
   ClientHeight    =   9885
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10380
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9885
   ScaleWidth      =   10380
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   8520
      TabIndex        =   28
      Top             =   8760
      Width           =   1455
   End
   Begin MSFlexGridLib.MSFlexGrid grdCheckList 
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   3720
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   8281
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      HighLight       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin MSFlexGridLib.MSFlexGrid grdGrupos 
      Height          =   1695
      Left            =   120
      TabIndex        =   17
      Top             =   1920
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   2990
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      HighLight       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "17.11.2010"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   12
      Left            =   8280
      TabIndex        =   27
      Top             =   1320
      Width           =   930
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sosa, Eliana"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   11
      Left            =   8280
      TabIndex        =   26
      Top             =   1560
      Width           =   990
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Fecha del informe"
      Height          =   195
      Index           =   12
      Left            =   6840
      TabIndex        =   25
      Top             =   1320
      Width           =   1275
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Verificador"
      Height          =   195
      Index           =   11
      Left            =   6840
      TabIndex        =   24
      Top             =   1560
      Width           =   765
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Pintos, Daiana"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   10
      Left            =   1440
      TabIndex        =   23
      Top             =   1560
      Width           =   1200
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Homologador"
      Height          =   195
      Index           =   10
      Left            =   120
      TabIndex        =   22
      Top             =   1560
      Width           =   945
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "N/A"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   9
      Left            =   8280
      TabIndex        =   21
      Top             =   1080
      Width           =   315
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Spitz, Fernando J."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   8
      Left            =   8280
      TabIndex        =   20
      Top             =   840
      Width           =   1485
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "En ejecuci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   8280
      TabIndex        =   19
      Top             =   600
      Width           =   1050
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "No"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   6
      Left            =   8280
      TabIndex        =   18
      Top             =   360
      Width           =   210
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tipo de conforme"
      Height          =   195
      Index           =   9
      Left            =   6840
      TabIndex        =   16
      Top             =   1080
      Width           =   1245
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Usuario solicitante"
      Height          =   195
      Index           =   8
      Left            =   6840
      TabIndex        =   15
      Top             =   840
      Width           =   1305
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Estado"
      Height          =   195
      Index           =   7
      Left            =   6840
      TabIndex        =   14
      Top             =   600
      Width           =   495
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Impacto tecnol�g."
      Height          =   195
      Index           =   6
      Left            =   6840
      TabIndex        =   13
      Top             =   360
      Width           =   1305
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Icaro, Claudia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   5
      Left            =   1440
      TabIndex        =   12
      Top             =   1320
      Width           =   1155
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BBVA Banco Franc�s"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   4
      Left            =   1440
      TabIndex        =   11
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tecnolog�a y operaciones � DyD � Metodolog�a y F�bricas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   3
      Left            =   1440
      TabIndex        =   10
      Top             =   1080
      Width           =   4860
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Propia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   1440
      TabIndex        =   9
      Top             =   840
      Width           =   540
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Optimizaci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   1440
      TabIndex        =   8
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label lblDatos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "CGM: planificaci�n cuatrimestral DYD (2� etapa)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   1440
      TabIndex        =   7
      Top             =   360
      Width           =   4035
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Business Partner"
      Height          =   195
      Index           =   5
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   1200
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sector solicitante"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   5
      Top             =   1080
      Width           =   1230
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tipo"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   4
      Top             =   840
      Width           =   300
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Clase"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   390
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "T�tulo"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   360
      Width           =   390
   End
   Begin VB.Label lblHomologaInforme 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Entidad"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   540
   End
End
Attribute VB_Name = "frmHomologaInforme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    With grdGrupos
        .Visible = False
        .Clear
        .HighLight = flexHighlightWithFocus
        .Rows = 4 + 2
        .FixedRows = 2
        .Cols = 2
        .MergeCells = flexMergeFree
        '.CellFontBold = True
        .TextMatrix(0, 0) = "Grupos"
        .TextMatrix(0, 1) = "Grupos"
        .MergeRow(0) = True
        
        .TextMatrix(1, 0) = "Grupo de DyD"
        .TextMatrix(1, 1) = "Estado"
        .ColWidth(0) = 5000
        .ColWidth(1) = 4500
        .ColAlignment(0) = 0
        .ColAlignment(1) = 0
        .Visible = True
    End With
    
    Cargar_Grupos
End Sub

Private Sub Cargar_Grupos()
    With grdGrupos
        .TextMatrix(2, 0) = "Metodolog�a y F�bricas � Metodolog�a, CGM y Entornos previos"
        .TextMatrix(2, 1) = "En ejecuci�n"
        .TextMatrix(3, 0) = "Metodolog�a y F�bricas � Homologaci�n"
        .TextMatrix(3, 1) = "A planificar"
        .TextMatrix(4, 0) = "Metodolog�a y F�bricas � Seguimiento de Incidencias"
        .TextMatrix(4, 1) = "En ejecuci�n"
        .TextMatrix(5, 0) = "Metodolog�a y F�bricas � Oficina de Proyectos DYD Argentina"
        .TextMatrix(5, 1) = "Suspendido"
    End With
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub
