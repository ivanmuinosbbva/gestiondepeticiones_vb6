VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form fltProyecto 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Criterio selecci�n Proyecto"
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7290
   ControlBox      =   0   'False
   LinkTopic       =   "fltProyecto"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   7290
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cboEstado 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1530
      Style           =   2  'Dropdown List
      TabIndex        =   28
      Top             =   960
      Width           =   5215
   End
   Begin VB.ComboBox cboCorpLocal 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1530
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   1350
      Width           =   2025
   End
   Begin VB.ComboBox cboOrientacion 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1530
      Style           =   2  'Dropdown List
      TabIndex        =   20
      Top             =   1710
      Width           =   2025
   End
   Begin VB.ComboBox cboImportancia 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1530
      Style           =   2  'Dropdown List
      TabIndex        =   18
      Top             =   2100
      Width           =   1785
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   495
      Left            =   3750
      TabIndex        =   1
      Top             =   4440
      Width           =   1575
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   495
      Left            =   2070
      TabIndex        =   0
      Top             =   4440
      Width           =   1575
   End
   Begin AT_MaskText.MaskText txtDESDEiniplan 
      Height          =   315
      Left            =   1530
      TabIndex        =   2
      Top             =   2490
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtDESDEfinplan 
      Height          =   315
      Left            =   1530
      TabIndex        =   3
      Top             =   2865
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtHASTAiniplan 
      Height          =   315
      Left            =   3165
      TabIndex        =   4
      Top             =   2490
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtHASTAfinplan 
      Height          =   315
      Left            =   3165
      TabIndex        =   5
      Top             =   2865
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtDESDEinireal 
      Height          =   315
      Left            =   1530
      TabIndex        =   10
      Top             =   3210
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtHASTAinireal 
      Height          =   315
      Left            =   3165
      TabIndex        =   11
      Top             =   3210
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtDESDEfinreal 
      Height          =   315
      Left            =   1530
      TabIndex        =   12
      Top             =   3585
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtHASTAfinreal 
      Height          =   315
      Left            =   3165
      TabIndex        =   13
      Top             =   3585
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   10
      Align           =   1
      UpperCase       =   -1  'True
      DataType        =   3
      SpecialFeatures =   -1  'True
   End
   Begin AT_MaskText.MaskText txtTitulo 
      Height          =   315
      Left            =   1530
      TabIndex        =   24
      Top             =   600
      Width           =   5325
      _ExtentX        =   9393
      _ExtentY        =   556
      MaxLength       =   50
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   50
      AutoSelect      =   0   'False
      TabOnEnter      =   -1  'True
   End
   Begin AT_MaskText.MaskText txtNroInterno 
      Height          =   315
      Left            =   1560
      TabIndex        =   26
      ToolTipText     =   "N�mero asignado por el administrador"
      Top             =   240
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   556
      MaxLength       =   8
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   8
      DecimalPlaces   =   0
      UpperCase       =   -1  'True
      DataType        =   2
   End
   Begin AT_MaskText.MaskText txtPlanif 
      Height          =   315
      Left            =   1530
      TabIndex        =   30
      ToolTipText     =   "N�mero asignado por el administrador"
      Top             =   3960
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   556
      MaxLength       =   8
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   8
      DecimalPlaces   =   0
      UpperCase       =   -1  'True
      DataType        =   2
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Cant.Planif"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   525
      TabIndex        =   31
      Top             =   4020
      Width           =   870
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Estado"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   840
      TabIndex        =   29
      Top             =   1020
      Width           =   555
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      Caption         =   "Nro"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   1110
      TabIndex        =   27
      Top             =   300
      Width           =   285
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "T�tulo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   930
      TabIndex        =   25
      Top             =   645
      Width           =   465
   End
   Begin VB.Label Label33 
      AutoSize        =   -1  'True
      Caption         =   "Corp/Local"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   510
      TabIndex        =   23
      Top             =   1410
      Width           =   885
   End
   Begin VB.Label Label34 
      AutoSize        =   -1  'True
      Caption         =   "Orientaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   450
      TabIndex        =   22
      Top             =   1770
      Width           =   945
   End
   Begin VB.Label Label31 
      AutoSize        =   -1  'True
      Caption         =   "Importancia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   420
      TabIndex        =   19
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label22 
      AutoSize        =   -1  'True
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2700
      TabIndex        =   17
      Top             =   3255
      Width           =   420
   End
   Begin VB.Label Label23 
      AutoSize        =   -1  'True
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2700
      TabIndex        =   16
      Top             =   3630
      Width           =   420
   End
   Begin VB.Label Label24 
      AutoSize        =   -1  'True
      Caption         =   "F.Ini.Real Desde"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   105
      TabIndex        =   15
      Top             =   3255
      Width           =   1290
   End
   Begin VB.Label Label25 
      AutoSize        =   -1  'True
      Caption         =   "F.Fin.Real Desde"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   60
      TabIndex        =   14
      Top             =   3630
      Width           =   1335
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2715
      TabIndex        =   9
      Top             =   2910
      Width           =   420
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2715
      TabIndex        =   8
      Top             =   2535
      Width           =   420
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      Caption         =   "F.Fin Plan.Desde"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   60
      TabIndex        =   7
      Top             =   2910
      Width           =   1335
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      Caption         =   "F.Ini.Plan.Desde"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   105
      TabIndex        =   6
      Top             =   2535
      Width           =   1290
   End
End
Attribute VB_Name = "fltProyecto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim m_nrointerno As String
Dim m_titulo As String
Dim m_corp_local As String
Dim m_cod_orientacion As String
Dim m_importancia_cod As String
Dim m_cod_estado As String
Dim m_desde_ini_plan As Date
Dim m_hasta_ini_plan As Date
Dim m_desde_fin_plan As Date
Dim m_hasta_fin_plan As Date
Dim m_desde_ini_real As Date
Dim m_hasta_ini_real As Date
Dim m_desde_fin_real As Date
Dim m_hasta_fin_real As Date
Dim m_cant_planif As String

Public Property Get nrointerno() As String
    nrointerno = m_nrointerno
End Property
Public Property Let nrointerno(ByVal NewVal As Integer)
    m_nrointerno = NewVal
End Property
Public Property Get titulo() As String
    titulo = m_titulo
End Property
Public Property Let titulo(ByVal NewVal As Integer)
    m_titulo = NewVal
End Property
Public Property Get corp_local() As String
    corp_local = m_corp_local
End Property
Public Property Let corp_local(ByVal NewVal As Integer)
    m_corp_local = NewVal
End Property
Public Property Get cod_orientacion() As String
    cod_orientacion = m_cod_orientacion
End Property
Public Property Let cod_orientacion(ByVal NewVal As Integer)
    m_cod_orientacion = NewVal
End Property
Public Property Get importancia_cod() As String
    importancia_cod = m_importancia_cod
End Property
Public Property Let importancia_cod(ByVal NewVal As Integer)
    m_importancia_cod = NewVal
End Property
Public Property Get cod_estado() As String
    cod_estado = m_cod_estado
End Property
Public Property Let cod_estado(ByVal NewVal As Integer)
    m_cod_estado = NewVal
End Property
Public Property Get cant_planif() As String
    cant_planif = m_cant_planif
End Property
Public Property Let cant_planif(ByVal NewVal As Integer)
    m_cant_planif = NewVal
End Property
Public Property Get desde_ini_plan() As Date
    desde_ini_plan = m_desde_ini_plan
End Property
Public Property Let desde_ini_plan(ByVal NewVal As Date)
    m_desde_ini_plan = NewVal
End Property
Public Property Get hasta_ini_plan() As Date
    hasta_ini_plan = m_hasta_ini_plan
End Property
Public Property Let hasta_ini_plan(ByVal NewVal As Date)
    m_hasta_ini_plan = NewVal
End Property
Public Property Get desde_fin_plan() As Date
    desde_fin_plan = m_desde_fin_plan
End Property
Public Property Let desde_fin_plan(ByVal NewVal As Date)
    m_desde_fin_plan = NewVal
End Property
Public Property Get hasta_fin_plan() As Date
    hasta_fin_plan = m_hasta_fin_plan
End Property
Public Property Let hasta_fin_plan(ByVal NewVal As Date)
    m_hasta_fin_plan = NewVal
End Property
Public Property Get desde_ini_real() As Date
    desde_ini_real = m_desde_ini_real
End Property
Public Property Let desde_ini_real(ByVal NewVal As Date)
    m_desde_ini_real = NewVal
End Property
Public Property Get hasta_ini_real() As Date
    hasta_ini_real = m_hasta_ini_real
End Property
Public Property Let hasta_ini_real(ByVal NewVal As Date)
    m_hasta_ini_real = NewVal
End Property
Public Property Get desde_fin_real() As Date
    desde_fin_real = m_desde_fin_real
End Property
Public Property Let desde_fin_real(ByVal NewVal As Date)
    m_desde_fin_real = NewVal
End Property
Public Property Get hasta_fin_real() As Date
    hasta_fin_real = m_hasta_fin_real
End Property
Public Property Let hasta_fin_real(ByVal NewVal As Date)
    m_hasta_fin_real = NewVal
End Property


Private Sub cmdAceptar_Click()
    If MsgBox("�Confirma?", vbOKCancel, "ATENCION") = vbCancel Then
       Exit Sub
    End If
    m_nrointerno = txtNroInterno.Text
    m_titulo = txtTitulo.Text
    m_cant_planif = txtPlanif.Text
    
    m_desde_ini_plan = txtDESDEiniplan.DateValue
    m_hasta_ini_plan = txtHASTAiniplan.DateValue
    m_desde_fin_plan = txtDESDEfinplan.DateValue
    m_hasta_fin_plan = txtHASTAfinplan.DateValue
    m_desde_ini_real = txtDESDEinireal.DateValue
    m_hasta_ini_real = txtHASTAinireal.DateValue
    m_desde_fin_real = txtDESDEfinreal.DateValue
    m_hasta_fin_real = txtHASTAfinreal.DateValue
    
    m_importancia_cod = CodigoCombo(cboImportancia, True)
    m_corp_local = CodigoCombo(cboCorpLocal, True)
    m_cod_orientacion = CodigoCombo(cboOrientacion, True)
    m_cod_estado = CodigoCombo(cboEstado, True)
    
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    On Error Resume Next
    m_corp_local = "NULL"
    m_cod_orientacion = "NULL"
    m_importancia_cod = "NULL"
    m_cod_estado = "NULL"
    m_desde_ini_plan = Null
    m_hasta_ini_plan = Null
    m_desde_fin_plan = Null
    m_hasta_fin_plan = Null
    m_desde_ini_real = Null
    m_hasta_ini_real = Null
    m_desde_fin_real = Null
    m_hasta_fin_real = Null
    m_cant_planif = "NULL"
    
   
    
    cboCorpLocal.Clear
    cboCorpLocal.AddItem "Todas                                      ||NULL"
    cboCorpLocal.AddItem "Local                                      ||L"
    cboCorpLocal.AddItem "Corporativa                                ||C"
    
    cboImportancia.Clear
    If sp_GetImportancia(Null, Null) Then
        Do While Not aplRST.EOF
            cboImportancia.AddItem Trim(aplRST!nom_importancia) & Space(40) & "||" & Trim(aplRST!cod_importancia)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboImportancia.AddItem "Todas                             ||NULL", 0
    
    cboOrientacion.Clear
    If sp_GetOrientacion(Null, Null) Then
        Do While Not aplRST.EOF
            cboOrientacion.AddItem Trim(aplRST!nom_orientacion) & Space(40) & "||" & Trim(aplRST!cod_orientacion)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboOrientacion.AddItem "Todas                             ||NULL", 0
    
    cboCorpLocal.ListIndex = PosicionCombo(cboCorpLocal, m_corp_local, True)
    cboImportancia.ListIndex = PosicionCombo(cboImportancia, m_importancia_cod, True)
    cboOrientacion.ListIndex = PosicionCombo(cboOrientacion, m_cod_orientacion, True)
    
    cmdAceptar.Enabled = False
End Sub


