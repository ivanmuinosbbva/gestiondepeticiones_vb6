VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmPeticionesShell 
   Caption         =   "Consulta de peticiones seg�n Perfil"
   ClientHeight    =   7935
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13620
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesShell.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7935
   ScaleWidth      =   13620
   ShowInTaskbar   =   0   'False
   Begin TabDlg.SSTab sstDatos 
      Height          =   6075
      Left            =   60
      TabIndex        =   31
      Top             =   1800
      Width           =   12075
      _ExtentX        =   21299
      _ExtentY        =   10716
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmPeticionesShell.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "grdDatos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "An�lisis"
      TabPicture(1)   =   "frmPeticionesShell.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdAnalisis"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Planificaci�n"
      TabPicture(2)   =   "frmPeticionesShell.frx":05C2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "grdPlanificacion"
      Tab(2).ControlCount=   1
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdAnalisis 
         Height          =   5535
         Left            =   -74940
         TabIndex        =   34
         Top             =   360
         Width           =   11955
         _ExtentX        =   21087
         _ExtentY        =   9763
         _Version        =   393216
         Rows            =   3
         FixedRows       =   2
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin MSFlexGridLib.MSFlexGrid grdDatos 
         Height          =   5640
         Left            =   60
         TabIndex        =   32
         Top             =   360
         Width           =   11955
         _ExtentX        =   21087
         _ExtentY        =   9948
         _Version        =   393216
         Cols            =   31
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         OLEDropMode     =   1
      End
      Begin MSFlexGridLib.MSFlexGrid grdPlanificacion 
         Height          =   5640
         Left            =   -74940
         TabIndex        =   33
         Top             =   360
         Width           =   11955
         _ExtentX        =   21087
         _ExtentY        =   9948
         _Version        =   393216
         Cols            =   31
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         OLEDropMode     =   1
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   7875
      Left            =   12180
      TabIndex        =   17
      Top             =   0
      Width           =   1395
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Nu&eva"
         Height          =   500
         Left            =   120
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   5820
         Width           =   1170
      End
      Begin VB.CommandButton cmdPlanificar 
         Caption         =   "Pla&nificaci�n"
         Height          =   500
         Left            =   120
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Permite automatizar el traspaso de peticiones de un grupo a otro"
         Top             =   4620
         Width           =   1170
      End
      Begin VB.CommandButton cmdPrintBandeja 
         Caption         =   "Imprimir bandeja"
         Height          =   500
         Left            =   120
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Imprime un formulario con todas las peticiones que aparecen en esta bandeja de trabajo"
         Top             =   3480
         Width           =   1170
      End
      Begin VB.CommandButton cmdPrintPet 
         Caption         =   "Imprimir formulario"
         Height          =   500
         Left            =   120
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Imprime un formulario por cada Peticion seleccionada"
         Top             =   3000
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdGrupOper 
         Caption         =   "Cambio Est. Grupo"
         Height          =   500
         Left            =   120
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   1800
         Width           =   1170
      End
      Begin VB.CommandButton cmdGrupo 
         Caption         =   "Grupos"
         Height          =   500
         Left            =   120
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   1320
         Width           =   1170
      End
      Begin VB.CommandButton cmdPerfil 
         Caption         =   "Seleccionar"
         Height          =   315
         Left            =   180
         TabIndex        =   29
         ToolTipText     =   "Permite seleccionar otro perfil actuante para realizar la consulta"
         Top             =   390
         Width           =   1065
      End
      Begin VB.CommandButton cmdSector 
         Caption         =   "Cambio Est. Sector"
         Height          =   500
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   840
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "&Cerrar"
         Height          =   500
         Left            =   120
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   7260
         Width           =   1170
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "&Acceder"
         Height          =   500
         Left            =   120
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   5340
         Width           =   1170
      End
      Begin VB.CommandButton cmdTraspaso 
         Caption         =   "Traspaso de cartera"
         Height          =   500
         Left            =   120
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Permite automatizar el traspaso de peticiones de un grupo a otro"
         Top             =   4140
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgrupar 
         Caption         =   "Agrupar Pet."
         Height          =   500
         Left            =   120
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Adjunta las peticiones seleccionadas en un agrupamiento"
         Top             =   2520
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.Label lblSelPerf 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Otro Perfil"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   315
         TabIndex        =   30
         Top             =   180
         Width           =   735
      End
   End
   Begin VB.Frame fraFiltros 
      Height          =   1755
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   12075
      Begin VB.ComboBox cboFiltroGestion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6900
         Style           =   2  'Dropdown List
         TabIndex        =   36
         Top             =   1252
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdClrAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8670
         Picture         =   "frmPeticionesShell.frx":05DE
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Limpia agrupamiento"
         Top             =   900
         Width           =   375
      End
      Begin VB.CommandButton cmdAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8280
         Picture         =   "frmPeticionesShell.frx":0B68
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar agrupamiento para filtrar las peticiones"
         Top             =   900
         Width           =   375
      End
      Begin VB.CheckBox chkPropias 
         Caption         =   "S�lo propias"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   4200
         TabIndex        =   4
         ToolTipText     =   "Muestra solo las Peticiones en las que el usuario figura como solicitante"
         Top             =   1320
         Width           =   5295
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2820
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   540
         Width           =   6255
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   315
         Left            =   1260
         TabIndex        =   2
         Top             =   1260
         Width           =   850
      End
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "Filtrar"
         Height          =   315
         Left            =   1260
         TabIndex        =   1
         Top             =   540
         Width           =   850
      End
      Begin AT_MaskText.MaskText txtNroBuscar 
         Height          =   315
         Left            =   2820
         TabIndex        =   7
         Top             =   1260
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtAgrup 
         Height          =   315
         Left            =   2820
         TabIndex        =   8
         Top             =   900
         Width           =   5475
         _ExtentX        =   9657
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrioEjecuc 
         Height          =   315
         Left            =   9900
         TabIndex        =   9
         Top             =   900
         Visible         =   0   'False
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   556
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblFiltroGestion 
         AutoSize        =   -1  'True
         Caption         =   "Gesti�n:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   6240
         TabIndex        =   35
         Top             =   1320
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Label lblPrioEjecuc 
         AutoSize        =   -1  'True
         Caption         =   "Pri.Ejec.:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   9180
         TabIndex        =   16
         Top             =   960
         Visible         =   0   'False
         Width           =   660
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Nro.:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2205
         TabIndex        =   15
         Top             =   1320
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "Estado:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2205
         TabIndex        =   14
         Top             =   585
         Width           =   615
      End
      Begin VB.Label lblAgrup 
         AutoSize        =   -1  'True
         Caption         =   "Agrup.:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2205
         TabIndex        =   13
         Top             =   960
         Width           =   555
      End
      Begin VB.Label lblPerfil 
         Alignment       =   2  'Center
         Caption         =   "descripcion del perfil actuante"
         ForeColor       =   &H00FF0000&
         Height          =   225
         Left            =   180
         TabIndex        =   12
         Top             =   240
         Width           =   7845
      End
      Begin VB.Label Label3 
         Caption         =   "Buscar por N�mero"
         ForeColor       =   &H00FF0000&
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   1170
         Width           =   1140
      End
      Begin VB.Label Label2 
         Caption         =   "Filtrar por Estado/Agrup."
         ForeColor       =   &H00FF0000&
         Height          =   480
         Left            =   120
         TabIndex        =   10
         Top             =   450
         Width           =   1050
      End
   End
End
Attribute VB_Name = "frmPeticionesShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colMultiSelect = 0
Private Const colNroAsignado = 1
Private Const colTipoPet = 2
Private Const colPetClase = 3
Private Const colPetCategoria = 4           ' add -018- a. Se renumeran las constantes.
Private Const colPetImpTech = 5
Private Const colPetRegulatorio = 6
Private Const colPetRO = 7
Private Const colTitulo = 8
Private Const colPuntuacion = 9             ' add -017- a. Se renumeran las constantes.
Private Const colSolicitante = 10
Private Const colSectorSolicitante = 11
Private Const colPrioridad = 12
Private Const colPetFePedido = 13
Private Const colVisibilidad = 14
Private Const colESTADO = 15
Private Const colSituacion = 16
Private Const colRefSistema = 17
Private Const colAnalisis = 18
Private Const colPlanificado = 19
Private Const colEstHijo = 20
Private Const colSitHijo = 21
Private Const colPrioEjecuc = 22
Private Const colCantNieto = 23
Private Const colNomHijo = 24
Private Const colEsfuerzo = 25
Private Const colFinicio = 26
Private Const colFtermin = 27
Private Const colNroInterno = 28
Private Const colEstCod = 29
Private Const colSitCod = 30
Private Const colCodigoRecurso = 31
Private Const colNota = 32
Private Const colDerivo = 33
Private Const colCodHijo = 34
Private Const colFinicioSRT = 35
Private Const colFterminSRT = 36
Private Const colEnPlanificacion = 37
Private Const colPrioridadOrden = 38
Private Const colEstHijoCod = 39
Private Const TOTAL_COLS = 40

Private Const RPT_PERFIL_BP As String = "petperfilbp.rpt"
Private Const RPT_PERFIL_SOLI As String = "petperfilsoli.rpt"
Private Const RPT_PERFIL As String = "petperfil.rpt"

Private lPrevRow     As Long                        ' -- Almacenar la celda anterior
Private lPrevCol     As Long
Private lCurrentCol  As Long                        ' -- Almacenar la Celda actual
Private lCurrentRow  As Long
  
Private Const BACKCOLOR_CELL As Long = vbYellow     ' -- Color de la celda activa
Private Const TEXTCOLOR_CELL As Long = vbBlue       ' -- Color del texto de la celda activa

Public bSeleccion As Boolean
Public formPerfilActual As String           ' new

Dim ordenColumnas() As ORDENAMIENTO_TIPO

Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long
Dim flgEditarPrioridad As Boolean

Dim flgEnCarga As Boolean
Dim sOpcionSeleccionada As String
Dim sEstadoQuery As String
Dim flgNumero As Boolean
Dim flgInMultiselect As Boolean
Dim xPerfil As String
Dim xNivel As String
Dim xArea As String
Dim sArea As String

Dim bEsGrupoTecnologia As Boolean
Dim bEsGrupoSeguridadInformatica As Boolean

Private Sub Form_Load()
    Debug.Print "Cargando frmPeticionesShell..."
    flgNumero = False
    flgEnCarga = True
    
    If formPerfilActual <> glUsrPerfilActual Then
        Debug.Print "LOAD_FORM"
        xPerfil = glUsrPerfilActual
        xNivel = getPerfNivel(glUsrPerfilActual)
        xArea = getPerfArea(glUsrPerfilActual)
        Call InicializarCombos
        Call IniciarScroll(grdDatos)
        Call InicializarPantalla
        Call InicializarGrilla
        'Call IniciarScroll(grdAnalisis)
        flgEnCarga = False
        formPerfilActual = glUsrPerfilActual
    End If
    Me.WindowState = vbMaximized
    'If grdDatos.Enabled Then grdDatos.SetFocus
End Sub

Private Sub InicializarGrilla()
    ReDim Preserve ordenColumnas(TOTAL_COLS - 1)
    ordenColumnas(colPuntuacion) = NUMERICO
    ordenColumnas(colPrioridad) = ORDENAMIENTO_TIPO.NUMERICO
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    cmdGrupo.Enabled = False
    cmdSector.Enabled = False
    cmdGrupOper.Enabled = False
    
    cmdAgrupar.visible = True
    cmdPrintPet.visible = True
    txtAgrup.visible = True
    lblAgrup.visible = True
    cmdAgrup.visible = True
    cmdClrAgrup.visible = True
    cmdPlanificar.visible = False           ' add -012- a.
    cmdTraspaso.Enabled = False             ' add -011- a.
    
    With chkPropias
        Select Case xPerfil
            Case "SOLI"
                .visible = True
                .value = 1
                .Caption = "S�lo solicitante " & glLOGIN_ID_REEMPLAZO
                .ToolTipText = "Muestra s�lo las peticiones en las que �" & glLOGIN_NAME_REEMPLAZO & "� figura como solicitante"
            Case "ANAL"
                .visible = True
                .value = 0
                .Caption = "S�lo las que me asignaron a mi"
                .ToolTipText = "Muestra s�lo las peticiones en las que �" & glLOGIN_NAME_REEMPLAZO & "� figura como recurso asignado"
            Case Else
                .visible = False
                .value = 0
        End Select
    End With
    
    If xPerfil = "CGRU" Then cmdTraspaso.Enabled = True
    If InStr(1, "BPAR|CGRU|ANAL|", xPerfil, vbTextCompare) > 0 Then
        lblPrioEjecuc.visible = True
        txtPrioEjecuc.visible = True
    Else
        lblPrioEjecuc.visible = False
        txtPrioEjecuc.visible = False
    End If
    Call setLblPerfil
    
    bEsGrupoTecnologia = False
    bEsGrupoSeguridadInformatica = False
    If glLOGIN_Grupo <> "" Then
        If glTecnoGrupo = glLOGIN_Grupo Then bEsGrupoTecnologia = True
        If glSegInfGrupo = glLOGIN_Grupo Then bEsGrupoSeguridadInformatica = True
    End If
    Call HabilitarBotones(0)
    'If grdDatos.Enabled Then grdDatos.SetFocus
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    cmdPerfil.visible = False
    lblSelPerf.visible = False
    If glUsrPerfilActual = "ADMI" Or glUsrPerfilActual = "SADM" Then
        cmdPerfil.visible = True
        lblSelPerf.visible = True
    End If
    Select Case nOpcion
        Case 0
            cmdAgregar.Enabled = False
            cboEstado.Enabled = True
            cmdFiltro.Enabled = True
            grdDatos.Enabled = True
            sOpcionSeleccionada = ""
            cmdCerrar.visible = True
            cmdVisualizar.visible = True
            '{ add -003- a.
            If Not glUsrPerfilActual = "ADMI" And Not glUsrPerfilActual = "SADM" Then
                If Not sp_GetRecursoEsHomologacion(glLOGIN_ID_REEMPLAZO) Then
                    If sp_GetAccionPerfil("PNEW000", glUsrPerfilActual, Null, Null, Null) Then
                        Call setHabilCtrl(cmdAgregar, "NOR")
                    End If
                End If
            End If
            '}
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion(False)
            End If
'            '{ add -012- c.
'            Select Case glUsrPerfilActual
'                Case "BPAR"
'                    Call setHabilCtrl(cmdPlanificar, "NOR")
'                    'cmdPlanificar.visible = True
'                Case "CGRU", "CSEC"
'                    If glLOGIN_Gerencia = "DESA" Then
'                        If Not sp_GetRecursoEsHomologacion(glLOGIN_ID_REEMPLAZO) Then
'                            Call setHabilCtrl(cmdPlanificar, "NOR")
'                        End If
'                        'If Not EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
'                        '    cmdPlanificar.visible = True
'                        'End If
'                    End If
'                Case Else
'                    'cmdPlanificar.visible = False
'                    Call setHabilCtrl(cmdPlanificar, "DIS")
'            End Select
'            '}
            If grdDatos.visible Then grdDatos.SetFocus
            '{ del -012- c.
            ''{ add -012- a.
            'If InStr(1, "BPAR|CGRU|CSEC|", glUsrPerfilActual, vbTextCompare) > 0 Then
            '    If Not EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then      ' add -014- a.
            '        cmdPlanificar.Visible = True
            '    End If
            'End If
            ''}
            '}
        Case 1
            cboEstado.Enabled = False
            cmdFiltro.Enabled = False
            grdDatos.Enabled = False
            cmdAgregar.Caption = "CANCELAR"
            cmdAgregar.visible = True
            cmdCerrar.visible = False
            cmdVisualizar.visible = False
    End Select
    Call LockProceso(False)
End Sub

Sub InicializarCombos()
    Dim i As Integer
    Dim auxCod As String
    Dim auxCod2 As String
    
    auxCod = ""
    auxCod2 = ""
    
    'Call Status("Cargando acciones...")
    
    With cboFiltroGestion
        .Clear
        .AddItem "Todo" & ESPACIOS & "||NULL"
        .AddItem "Sistemas" & ESPACIOS & "||DESA"
        .AddItem "RGyP" & ESPACIOS & "||BPE"
        .ListIndex = 0
    End With
    
    If xPerfil = "SOLI" Then
        chkPropias.visible = True
        chkPropias.value = 1
    Else
        chkPropias.visible = False
        chkPropias.value = 0
    End If
    cboEstado.Clear
    If sp_GetAccionPerfil("PINTERV", xPerfil, Null, Null, Null) Then
        Do While Not aplRST.EOF
            auxCod = auxCod & aplRST!cod_estado & "|"
            cboEstado.AddItem Trim(aplRST!nom_estado) & ESPACIOS & "||" & aplRST!cod_estado
            aplRST.MoveNext
        Loop
    End If
    
    ''ESTO ES INCONFESABLE
    If xPerfil = "BPAR" Then
        'auxCod = "COMITE|APROBA|REVISA|"       ' upd -016- a.
        auxCod = "REVISA|COMITE|"               'APROBA|"               ' upd -016- a.
        'auxCod = "REVISA|COMITE|"
    '{ add -016- a.
    ElseIf xPerfil = "GBPE" Then
        'auxCod = "APROBA|REVISA|REFBPE|"
        'auxCod = "REVISA|"
        auxCod = "REVISA|REFBPE|"
        'auxCod = "REVISA|REFBPE|PLANOK|"
    '}
    End If
    
'    ''ESTO ES HORRIBLE
'    If InStr("CGRU|CSEC", xPerfil) > 0 Then
'       cboEstado.AddItem "Planificado" & ESPACIOS & "||" & "PLANOK"
'       cboEstado.AddItem "Ejecuci�n" & ESPACIOS & "||" & "EJECUC"
'       cboEstado.AddItem "Suspendido Temporariamente" & ESPACIOS & "||" & "SUSPEN"
'    End If
    
    If sp_GetEstado(Null) Then
        Do While Not aplRST.EOF
            If InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN", ClearNull(aplRST!cod_estado)) = 0 Then
                Select Case xPerfil
                    Case "BPAR"
                        If ClearNull(aplRST!cod_estado) <> "REFBPE" Then
                            auxCod2 = auxCod2 & aplRST!cod_estado & "|"
                        End If
                    Case "GBPE"
                        If InStr(1, "REFBPE|", ClearNull(aplRST!cod_estado), vbTextCompare) = 0 Then
                            auxCod2 = auxCod2 & aplRST!cod_estado & "|"
                        End If
                    Case Else
                        auxCod2 = auxCod2 & aplRST!cod_estado & "|"
                End Select
'                If xPerfil = "GBPE" And InStr(1, "REFBPE|", ClearNull(aplRST!cod_estado), vbTextCompare) = 0 Then
'                    auxCod2 = auxCod2 & aplRST!cod_estado & "|"
'                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    Debug.Print "Estados: " & auxCod2
    Select Case xPerfil
        'Case "BPAR"     ' Referente de Sistemas
            'cboEstado.AddItem "1. Pendientes de priorizar seg�n BPE" & ESPACIOS & "||" & "PENDI0", 0
            'cboEstado.AddItem "2. Pendientes de completar la validaci�n t�cnica" & ESPACIOS & "||" & "PENDI1", 1
        Case "CGRU"     ' Responsable de ejecuci�n
            'If Not sp_GetRecursoEsHomologacion(glLOGIN_ID_REEMPLAZO) Then
            If Not EsGrupoEspecial(glLOGIN_Grupo) Then
                cboEstado.AddItem "1. Pendientes de priorizar seg�n BPE" & ESPACIOS & "||" & "PENDI0", 0
                cboEstado.AddItem "2. Pendientes de estimar esfuerzo y planificar" & ESPACIOS & "||" & "PENDI1", 1
                cboEstado.AddItem "3. Propias pendientes de clasificar" & ESPACIOS & "||" & "PENDI2", 2
                cboEstado.AddItem "4. Peticiones pendientes de ejecutar" & ESPACIOS & "||" & "PENDI3", 3
                cboEstado.AddItem "5. Activas en las que participa con este perfil" & ESPACIOS & "||" & auxCod2, 4
                cboEstado.AddItem "Todas en las que participa con este perfil" & ESPACIOS & "||" & "NULL"
            Else
                cboEstado.AddItem "Pendientes para este perfil" & ESPACIOS & "||" & auxCod & "PENDIE|", 0
                cboEstado.AddItem "Activas en las que participa con este perfil" & ESPACIOS & "||" & auxCod2, 1
                cboEstado.AddItem "Todas en las que participa con este perfil" & ESPACIOS & "||" & "NULL"
            End If
        Case Else
            If xPerfil <> "SADM" Then
                cboEstado.AddItem "Pendientes para este perfil" & ESPACIOS & "||" & auxCod & "PENDIE|", 0
                cboEstado.AddItem "Activas en las que participa con este perfil" & ESPACIOS & "||" & auxCod2, 1
                cboEstado.AddItem "Todas en las que participa con este perfil" & ESPACIOS & "||" & "NULL"
            Else
                cboEstado.AddItem "Sin filtrar por estado" & ESPACIOS & "||" & "NULL"
            End If
    End Select
    cboEstado.ListIndex = 0
    sEstadoQuery = DatosCombo(cboEstado)
    Call Status("Listo.")
End Sub

Private Sub Form_Activate()
    'Me.WindowState = vbMaximized
    If grdDatos.Enabled Then grdDatos.SetFocus
End Sub

Private Sub Form_Resize()
    '{ add -019- a.
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            Me.Top = 0
            Me.Left = 0
            If Me.Height < 8445 Then Me.Height = 8445
            If Me.Width < 12425 Then Me.Width = 12425
        End If
        ' Ancho
        If Not ((fraBotonera.Width - 150) > Me.ScaleWidth) Then fraFiltros.Width = Me.ScaleWidth - fraBotonera.Width - 150
        'fraFiltros.Width = Me.Width - fraBotonera.Width - 150
        If Not ((fraBotonera.Width - 150) > Me.ScaleWidth) Then grdDatos.Width = Me.ScaleWidth - fraBotonera.Width - 350    '150
        If Not ((fraBotonera.Width - 150) > Me.ScaleWidth) Then grdAnalisis.Width = Me.ScaleWidth - fraBotonera.Width - 350    '150
        'grdDatos.Width = Me.Width - fraBotonera.Width - 150
        If Me.ScaleWidth > fraBotonera.Width - 200 Then sstDatos.Width = Me.ScaleWidth - fraBotonera.Width - 200
        
        fraBotonera.Left = fraFiltros.Width + 100
        ' Alto
        If Not ((fraFiltros.Height - 100) > Me.ScaleHeight) Then grdDatos.Height = Me.ScaleHeight - fraFiltros.Height - 550 '100
        If Not ((fraFiltros.Height - 100) > Me.ScaleHeight) Then grdAnalisis.Height = Me.ScaleHeight - fraFiltros.Height - 550 '100
        
        
        If Not ((fraFiltros.Height - 100) > Me.ScaleHeight) Then sstDatos.Height = Me.ScaleHeight - fraFiltros.Height - 100
        
        'grdDatos.Height = Me.Height - fraFiltros.Height - 100
        'If Not ((Me.ScaleHeight - 50) > Me.ScaleHeight) Then fraBotonera.Height = Me.ScaleHeight - 50
        If Not Me.ScaleHeight = 0 Then fraBotonera.Height = Me.ScaleHeight - 50
        'fraBotonera.Height = Me.Height - 50
        cmdCerrar.Top = fraBotonera.Height - cmdCerrar.Height - 100
    End If
    '}
End Sub

Private Sub Form_Unload(Cancel As Integer)
    glNumeroPeticion = ""
    formPerfilActual = ""
    Call DetenerScroll(grdDatos)      ' add -008- a.
    'Call DetenerScroll(grdAnalisis)
    Call LockProceso(False)
End Sub

Private Sub cmdAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glModoAgrup = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "VIEW"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    txtAgrup.text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdPrintPet_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim nCopias As Integer
    Dim i As Integer
    
    If grdDatos.Rows < 2 Then
        Exit Sub
    End If
    
    rptFormulario.Show vbModal
    If (Not rptFormulario.chkFormu) And (Not rptFormulario.chkHisto) Then
        Exit Sub
    End If
    
    Call Puntero(True)
    'Call Status("")
    flgMulti = False
    DoEvents
    nCopias = rptFormulario.Copias
    With grdDatos
        While nCopias > 0
            For i = 1 To .Rows - 1
               If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                    flgMulti = True
                    Call Status("Orden:" & i)
                    If rptFormulario.chkFormu Then
                        Call PrintFormulario(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If rptFormulario.chkHisto Then
                        Call PrintHitorial(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If nCopias = 1 Then
                        .TextMatrix(i, colMultiSelect) = ""
                    End If
               End If
            Next
            nCopias = nCopias - 1
        Wend
    End With
    Call Puntero(False)
    If flgMulti Then
        MsgBox ("Impresi�n finalizada")
    Else
        MsgBox ("Nada seleccionado para imprimir")
    End If
    Call Status("Listo.")
End Sub

Private Sub cmdAgrupar_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim i As Integer
    glSelectAgrup = ""
    glModoAgrup = "ALTAX"
''    glNivelAgrup = ""
''    glAreaAgrup = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "MODI"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    DoEvents
    
    If ClearNull(glSelectAgrup) = "" Then
        Exit Sub
    End If
    
    Dim nRwD As Integer, nRwH As Integer
    flgMulti = False
        
    With grdDatos
        If .Rows < 2 Then
            Exit Sub
        End If
        Call Puntero(True)
        Call Status("")
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                If (Not flgMulti) Then
                    'If MsgBox("�Desea que esta/s Peticion/es integren el Agrupamiento '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbNo Then
                    If MsgBox("�Desea que la selecci�n actual" & vbCrLf & "integre el agrupamiento '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo + vbQuestion) = vbNo Then
                        Exit For
                    End If
                End If
                flgMulti = True
                Call Status("Orden:" & i)
                'si ya existe agrup-petic no hace nada, ni siquiera avisa
                If sp_GetAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                    aplRST.Close
                Else
                    Call Puntero(True)
                    If sp_GetAgrupPeticDup(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                        Call Puntero(False)
                        If MsgBox("La Peticion:" & .TextMatrix(i, colTitulo) & Chr(13) & "ya est� declarada en el agrupamiento: " & ClearNull(aplRST!agr_titulo) & Chr(13) & "Desea igualmente que integre '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbYes Then
                           aplRST.Close
                           Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                        Else
                           aplRST.Close
                        End If
                    Else
                        Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                    End If
                    Call Puntero(False)
                End If
                .TextMatrix(i, colMultiSelect) = ""
           End If
        Next
    End With
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub cmdBuscar_Click()
    If flgEnCarga = True Then Exit Sub
    If Val(txtNroBuscar) > 100000 Then
        txtNroBuscar = ""
        Call Status("N�mero inv�lido")
        Exit Sub
    End If
    If Val(txtNroBuscar) > 0 Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        flgNumero = True
        txtAgrup.text = ""
        cboEstado.ListIndex = cboEstado.ListCount - 1
        sEstadoQuery = DatosCombo(cboEstado)
        Me.chkPropias.value = 0
        glNumeroPeticion = ""
        Call CargarGrid
    Else
        flgNumero = False
        glNumeroPeticion = ""
        Call CargarGrid
        Call CargarGridAnalisis         ' TODO: OJO
    End If
End Sub

Private Sub cmdClrAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = ""
    txtAgrup.text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdFiltro_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    flgNumero = False
    txtNroBuscar = ""
    sEstadoQuery = DatosCombo(cboEstado)
    Debug.Print sEstadoQuery
    glNumeroPeticion = ""
    Call CargarGrid
End Sub

Private Sub cmdPerfil_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    cmdPerfil.Enabled = False
    flgNumero = False
    Load auxSelPerfil
    'auxSelPerfil.iniEstado = EstadoPeticion
    'auxSelPerfil.setOption
    auxSelPerfil.pubPerfil = xPerfil
    auxSelPerfil.InicializarCombos
    auxSelPerfil.Show 1
    If auxSelPerfil.bAceptar = True Then
        xPerfil = auxSelPerfil.pubPerfil
        xNivel = auxSelPerfil.pubNivel
        xArea = auxSelPerfil.pubArea
    End If
    Unload auxSelPerfil
    cmdPerfil.Enabled = True
    Call setLblPerfil
    Call InicializarCombos
    Call LockProceso(False)
End Sub

Private Sub cmdAgregar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then Exit Sub
    Call Puntero(True)
    glNumeroPeticion = ""
    'glPeticion_sox001 = 1    ' add -001- e.
    glCodigoRecurso = glLOGIN_ID_REEMPLAZO
    glModoPeticion = "ALTA"
    frmPeticionesCarga.Show vbModal
    DoEvents
    Call LockProceso(False)
    If Me.Tag = "REFRESH" Then Call InicializarPantalla
End Sub

Private Sub cmdSector_Click()
    If flgEnCarga = True Then Exit Sub
    If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
        glSector = sArea
        If Not LockProceso(True) Then
            Exit Sub
        End If
        frmPeticionesSectorOperar.Show 1
        DoEvents
        Call LockProceso(False)
        If Me.Tag = "REFRESH" Then
            Call InicializarPantalla
        End If
    End If
End Sub

Private Sub cmdGrupOper_Click()
    If flgEnCarga Then Exit Sub
    glSector = ""
    
    '.TextMatrix(.Rows - 1, colPetClase) = IIf(ClearNull(aplRST!cod_clase) = "SINC", "-", ClearNull(aplRST!cod_clase))   ' upd -116- c.
    
    
    'If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
    With grdDatos
        If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" And .TextMatrix(.rowSel, colPetClase) <> "-" Then
            glGrupo = sArea
            If sp_GetGrupo(sArea, Null) Then
                If Not aplRST.EOF Then
                   glSector = ClearNull(aplRST!cod_sector)
                End If
            End If
            ' Antes de permitir el cambio de estado del grupo, valido que tenga al menos definida una opci�n
            If Not sp_GetAccionPerfilEstados(IIf(bEsGrupoTecnologia Or bEsGrupoSeguridadInformatica, 2, 1), "GCHGEST", glUsrPerfilActual, ClearNull(.TextMatrix(.rowSel, colEstHijoCod)), Null, Null) Then
                MsgBox "No puede cambiar de estado esta petici�n", vbExclamation + vbOKOnly
                Exit Sub
            End If
            
            If ClearNull(glSector) <> "" Then
                If Not LockProceso(True) Then
                    Exit Sub
                End If
                frmPeticionesGrupoOperar.Show 1
                DoEvents
                flgEnCarga = True
                Call LockProceso(False)
                If Me.Tag = "REFRESH" Then
                    Call InicializarPantalla
                    Call GridSeek(Me, grdDatos, colNroInterno, glNumeroPeticion, True)
                End If
                flgEnCarga = False
            End If
        Else
            If .TextMatrix(.rowSel, colPetClase) = "-" Then
                MsgBox "Primero debe clasificar la petici�n.", vbExclamation + vbOKOnly
            End If
        End If
    End With
End Sub

Private Sub cmdGrupo_Click()
    If flgEnCarga = True Then Exit Sub
    If xPerfil = "CGRU" Then
        If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
            If sp_GetGrupo(sArea, Null) Then
                If Not aplRST.EOF Then
                   glSector = ClearNull(aplRST!cod_sector)
                   aplRST.Close
                End If
            End If
            If ClearNull(glSector) <> "" Then
                If Not LockProceso(True) Then
                    Exit Sub
                End If
                frmPeticionesGrupo.Show 1
                DoEvents
                Call LockProceso(False)
                If Me.Tag = "REFRESH" Then
                    InicializarPantalla
                End If
            End If
        End If
        Exit Sub
    End If
    If xPerfil = "CSEC" Then
        If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
            glSector = sArea
            If Not LockProceso(True) Then
                Exit Sub
            End If
            frmPeticionesGrupo.Show vbModal
            DoEvents
            Call LockProceso(False)
            If Me.Tag = "REFRESH" Then
                InicializarPantalla
            End If
        End If
        Exit Sub
    End If
End Sub

Private Sub cmdVisualizar_Click()
    Call Visualizar
End Sub

Private Sub Visualizar()
    If flgEnCarga = True Then Exit Sub
    Call Puntero(True)
    Debug.Print "CARGA_INI: " & TimeToMillisecond()
    With grdDatos
        If .rowSel > 0 Then
            glNumeroPeticion = .TextMatrix(.rowSel, colNroInterno)
        End If
    End With
    If glNumeroPeticion <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoPeticion = "VIEW"
        On Error Resume Next
        frmPeticionesCarga.Show vbModal
        DoEvents
        On Error GoTo 0
        Call LockProceso(False)
        If Me.Tag = "REFRESH" Then
            Call InicializarPantalla
        End If
    End If
End Sub

Private Sub cmdCerrar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    'Unload Me              ' del -020- a.
    Call Status("Listo.")
    Me.Hide                 ' add -020- a.
    Call LockProceso(False)
End Sub

Sub setLblPerfil()
    Dim nomArea As String
    nomArea = ""
    
    Select Case xNivel
        Case "BBVA"
            nomArea = "Todo el BBVA"
        Case "DIRE"
            If sp_GetDireccion(xArea) Then
                nomArea = ClearNull(aplRST!nom_direccion)
                aplRST.Close
            End If
        Case "GERE"
            If sp_GetGerencia(xArea, Null) Then
                nomArea = ClearNull(aplRST!nom_gerencia)
                aplRST.Close
            End If
        Case "SECT"
            If sp_GetSector(xArea, Null) Then
                nomArea = ClearNull(aplRST!nom_sector)
                aplRST.Close
            End If
        Case "GRUP"
            If sp_GetGrupo(xArea, Null) Then
                nomArea = ClearNull(aplRST!nom_grupo)
                aplRST.Close
            End If
    End Select
    lblPerfil.Caption = "Perfil: " & getDescPerfil(xPerfil) & "     Nivel: " & getDescNivel(xNivel) & "     Area: " & nomArea
End Sub

Private Sub CargarGrid()
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
    Dim vRetorno() As String
    Dim flgPendientes As Boolean
    Dim auxAgrup As String
    
    sstDatos.TabVisible(1) = False
    sstDatos.TabVisible(2) = False
    flgEnCarga = True
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    cmdVisualizar.Enabled = False
    cmdAgrupar.Enabled = False
    cmdPrintPet.Enabled = False
    cmdGrupo.Enabled = False
    cmdSector.Enabled = False
    cmdGrupOper.Enabled = False
    cmdPerfil.Enabled = False
    cmdFiltro.Enabled = False
    cmdBuscar.Enabled = False
    If ClearNull(txtAgrup.text) <> "" Then
        If ParseString(vRetorno, txtAgrup.text, "|") > 0 Then
            auxAgrup = vRetorno(2)
        End If
    Else
        auxAgrup = ""
    End If
    
    If InStr(sEstadoQuery, "PENDIE") > 0 Then
        flgPendientes = True
    Else
        flgPendientes = False
    End If
    
    With grdDatos
        .visible = False
        .Clear
        DoEvents
        .HighLight = flexHighlightAlways
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .GridColor = &HE0E0E0
        .FocusRect = flexFocusNone
        .cols = TOTAL_COLS
        .RowHeight(0) = 300
        .RowHeightMin = 260
        .Rows = 1
        .TextMatrix(0, colNroAsignado) = "Petic.": .ColWidth(colNroAsignado) = 750: .ColAlignment(colNroAsignado) = flexAlignRightCenter
        .TextMatrix(0, colSolicitante) = "Solicitante": .ColWidth(colSolicitante) = 0: .ColAlignment(colSolicitante) = flexAlignLeftCenter
        .TextMatrix(0, colTipoPet) = "Tipo": .ColAlignment(colTipoPet) = flexAlignLeftCenter
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 4400: .ColAlignment(colTitulo) = flexAlignLeftCenter
        .TextMatrix(0, colESTADO) = "Estado Pet": .ColWidth(colESTADO) = 2500: .ColAlignment(colESTADO) = flexAlignLeftCenter
        .TextMatrix(0, colSituacion) = "Situaci�n Pet": .ColWidth(colSituacion) = 0 '1600
        .TextMatrix(0, colAnalisis) = "AN.": .ColWidth(colAnalisis) = 400: .ColAlignment(colAnalisis) = flexAlignCenterCenter
        .TextMatrix(0, colPlanificado) = "PL.": .ColWidth(colPlanificado) = 400: .ColAlignment(colPlanificado) = flexAlignCenterCenter
        .TextMatrix(0, colPrioridad) = "Prior.": .ColWidth(colPrioridad) = 600: .ColAlignment(colPrioridad) = flexAlignRightCenter
        .TextMatrix(0, colVisibilidad) = "Visib.": .ColWidth(colVisibilidad) = 0
        .TextMatrix(0, colEsfuerzo) = "Hs. Pet": .ColWidth(colEsfuerzo) = 800
        .TextMatrix(0, colFinicio) = "F.Ini.Planif Pet": .ColWidth(colFinicio) = 1400
        .TextMatrix(0, colFtermin) = "F.Fin Planif Pet": .ColWidth(colFtermin) = 1400
        .TextMatrix(0, colNroInterno) = "NroInt.": .ColWidth(colNroInterno) = 0: .ColAlignment(colNroInterno) = flexAlignRightCenter
        .TextMatrix(0, colCantNieto) = ""
        .TextMatrix(0, colPetClase) = "Clase": .ColWidth(colPetClase) = 600: .ColAlignment(colPetClase) = flexAlignLeftCenter
        .TextMatrix(0, colPetImpTech) = "I.T.": .ColWidth(colPetImpTech) = 500: .ColAlignment(colPetImpTech) = flexAlignLeftCenter
        .TextMatrix(0, colPetFePedido) = "Alta pet.": .ColWidth(colPetFePedido) = 0: .ColAlignment(colPetFePedido) = flexAlignLeftCenter
        .TextMatrix(0, colPetRegulatorio) = "Reg.": .ColWidth(colPetRegulatorio) = 500: .ColAlignment(colPetRegulatorio) = flexAlignLeftCenter
        .TextMatrix(0, colPetRO) = "RO": .ColWidth(colPetRO) = 500: .ColAlignment(colPetRO) = flexAlignLeftCenter
        .TextMatrix(0, colPetCategoria) = "Cat.": .ColWidth(colPetCategoria) = 650: .ColAlignment(colPetCategoria) = flexAlignLeftCenter        ' add -018- a.
        .TextMatrix(0, colPuntuacion) = "Ptos.": .ColWidth(colPuntuacion) = 700: .ColAlignment(colPuntuacion) = flexAlignRightCenter             ' add -017- a.
        .TextMatrix(0, colRefSistema) = "Ref.Sistema": .ColWidth(colRefSistema) = 2000: .ColAlignment(colRefSistema) = flexAlignLeftCenter
        .TextMatrix(0, colEnPlanificacion) = "En planif.": .ColWidth(colEnPlanificacion) = 0: .ColAlignment(colEnPlanificacion) = flexAlignLeftCenter
        .TextMatrix(0, colPrioridadOrden) = "": .ColWidth(colPrioridadOrden) = 0: .ColAlignment(colPrioridadOrden) = flexAlignLeftCenter
        .TextMatrix(0, colSectorSolicitante) = "Sector solicitante": .ColWidth(colSectorSolicitante) = 0: .ColAlignment(colSectorSolicitante) = flexAlignLeftCenter
        .ColWidth(colSitCod) = 0
        .ColWidth(colCodigoRecurso) = 0
        .ColWidth(colDerivo) = 0
        .ColWidth(colNota) = 0
        .ColWidth(colFinicioSRT) = 0
        .ColWidth(colFterminSRT) = 0
        .ColWidth(colEstCod) = 0
        .ColWidth(colMultiSelect) = 180
        .ColWidth(colEstHijo) = 0
        .ColWidth(colSitHijo) = 0
        .ColWidth(colNomHijo) = 0
        .ColWidth(colCodHijo) = 0
        .ColWidth(colCantNieto) = 0
        .ColWidth(colPrioEjecuc) = 0
        .ColWidth(colRefSistema) = 0
        .ColWidth(colAnalisis) = 0
        .ColWidth(colPlanificado) = 0
        .ColWidth(colEstHijoCod) = 0
        Select Case xPerfil
            Case "BPAR"
                .ColWidth(colTipoPet) = 550
                .ColWidth(colVisibilidad) = 550
                .ColWidth(colESTADO) = 2000
                .ColWidth(colRefSistema) = 2000
                .ColWidth(colPetFePedido) = 1200: .ColAlignment(colPetFePedido) = flexAlignLeftCenter
                .ColWidth(colAnalisis) = 400
                .ColWidth(colPlanificado) = 400
                .ColWidth(colPuntuacion) = 0
                .TextMatrix(0, colRefSistema) = "Ref. de RGyP": .ColWidth(colRefSistema) = 2000: .ColAlignment(colRefSistema) = flexAlignLeftCenter
            Case "GBPE"
                .ColWidth(colTipoPet) = 550
                .ColWidth(colVisibilidad) = 550
                .ColWidth(colESTADO) = 2000
                .ColWidth(colRefSistema) = 2000
                .ColWidth(colAnalisis) = 400: .ColAlignment(colAnalisis) = flexAlignCenterCenter
                .ColWidth(colPlanificado) = 400: .ColAlignment(colPlanificado) = flexAlignCenterCenter
                .TextMatrix(0, colRefSistema) = "Ref.Sistema": .ColWidth(colRefSistema) = 2000: .ColAlignment(colRefSistema) = flexAlignLeftCenter
                .ColWidth(colEnPlanificacion) = 1200
                .ColWidth(colSectorSolicitante) = 5000
            Case "CSEC", "CGCI", "CDIR"
                .ColWidth(colTipoPet) = 550
                .ColWidth(colESTADO) = 0
                .ColWidth(colSituacion) = 0
                .TextMatrix(0, colEstHijo) = "Est. Sector": .ColWidth(colEstHijo) = 1500
                .TextMatrix(0, colSitHijo) = "Sit. Sector": .ColWidth(colSitHijo) = 1200
                .TextMatrix(0, colNomHijo) = "Sector": .ColWidth(colNomHijo) = 1500
                .TextMatrix(0, colEsfuerzo) = "Hs Sec"
                .TextMatrix(0, colFinicio) = "F.Ini.Planif Sec"
                .TextMatrix(0, colFtermin) = "F.Fin Planif Sec"
                .TextMatrix(0, colCantNieto) = "Grupos": .ColWidth(colCantNieto) = 550: .ColAlignment(colCantNieto) = flexAlignLeftCenter
            Case "CGRU", "ANAL"
                .ColWidth(colTipoPet) = 550
                .ColWidth(colESTADO) = 0
                .ColWidth(colSituacion) = 0
                .ColWidth(colPrioridad) = 600
                .ColWidth(colPuntuacion) = 0
                .TextMatrix(0, colEstHijo) = "Est. Grup.": .ColWidth(colEstHijo) = 2500
                .TextMatrix(0, colSitHijo) = "Sit. Grup.": .ColWidth(colSitHijo) = 1200
                .TextMatrix(0, colNomHijo) = "Grupo": .ColWidth(colNomHijo) = IIf(xPerfil = "CGRU", 0, 2500)
                .TextMatrix(0, colEsfuerzo) = "Hs Grupo": .ColWidth(colEsfuerzo) = 1000
                .TextMatrix(0, colFinicio) = "F.Ini.Planif Gru": .ColWidth(colFinicio) = 1400
                .TextMatrix(0, colFtermin) = "F.Fin Planif Gru": .ColWidth(colFtermin) = 1400
                .TextMatrix(0, colPrioEjecuc) = "Pri": .ColWidth(colPrioEjecuc) = 450: .ColAlignment(colPrioEjecuc) = flexAlignCenterCenter
                .TextMatrix(0, colRefSistema) = "Ref.Sistema": .ColWidth(colRefSistema) = 2000: .ColAlignment(colRefSistema) = flexAlignLeftCenter
            Case "SOLI"
                .ColWidth(colTipoPet) = 550: .ColAlignment(colTipoPet) = flexAlignLeftCenter
                .ColWidth(colPetFePedido) = 1200: .ColAlignment(colPetFePedido) = flexAlignLeftCenter
                .ColWidth(colPuntuacion) = 0: .ColAlignment(colPuntuacion) = flexAlignLeftCenter
                .ColWidth(colPrioridad) = 0:: .ColAlignment(colPrioridad) = flexAlignLeftCenter
                .TextMatrix(0, colRefSistema) = "Ref. de RGyP": .ColWidth(colRefSistema) = 2000: .ColAlignment(colRefSistema) = flexAlignLeftCenter
            Case "REFE", "AUTO", "SUPE"
                .ColWidth(colTipoPet) = 550: .ColAlignment(colTipoPet) = flexAlignCenterCenter
                .ColWidth(colPetFePedido) = 1200: .ColAlignment(colPetFePedido) = flexAlignLeftCenter
                .ColWidth(colPuntuacion) = 0
                .ColWidth(colPrioridad) = 0
                .ColWidth(colPrioEjecuc) = 0
                .TextMatrix(0, colRefSistema) = "Ref. de RGyP": .ColWidth(colRefSistema) = 2000: .ColAlignment(colRefSistema) = flexAlignLeftCenter
            Case Else
                .ColWidth(colTipoPet) = 550: .ColAlignment(colTipoPet) = flexAlignCenterCenter
        End Select
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    
    Call Puntero(True)
    Call Status("Cargando peticiones...")
    Select Case xPerfil
        Case "GBPE": If Not sp_GetPeticionAreaBPE(IIf(flgNumero, txtNroBuscar, 0), glLOGIN_ID_REEMPLAZO, IIf(sEstadoQuery = "NULL", Null, sEstadoQuery), 0, "N", "", CodigoCombo(cboFiltroGestion, True)) Then GoTo finx
        Case "BPAR": If Not sp_GetPeticionAreaBP(IIf(flgNumero, txtNroBuscar, 0), glLOGIN_ID_REEMPLAZO, IIf(sEstadoQuery = "NULL", Null, sEstadoQuery), 0, "N", "1", CodigoCombo(cboFiltroGestion, True)) Then GoTo finx
        Case "SOLI": If Not sp_GetPeticionArea(IIf(flgNumero, txtNroBuscar, 0), xPerfil, xNivel, xArea, IIf(sEstadoQuery = "NULL", Null, sEstadoQuery), 0, "N", "", IIf(chkPropias.value = 1, glLOGIN_ID_REEMPLAZO, Null)) Then GoTo finx
        Case Else
            If Not sp_GetPeticionArea(IIf(flgNumero, txtNroBuscar, 0), xPerfil, xNivel, xArea, IIf(sEstadoQuery = "NULL", Null, sEstadoQuery), 0, "N", "", IIf(chkPropias.value = 1, glLOGIN_ID_REEMPLAZO, Null)) Then
                GoTo finx
            End If
    End Select
    
    Do While Not aplRST.EOF
        If xPerfil = "CSEC" And flgPendientes Then
            ' Pendientes - Aqu� se omite la carga de la petici�n si se dan todas estas condiciones:
            ' - El estado del sector es: En Opini�n, En Evaluaci�n, A Planificar o A Estimar Esfuerzo
            ' - El sector no tiene situaci�n definida y el sector tiene al menos 1 grupo asignado
            If (InStr("EVALUA|OPINIO|PLANIF|ESTIMA", ClearNull(aplRST!cod_estado_hij)) > 0) And ((ClearNull(aplRST!nom_situacion_hij) = "" And Val(ClearNull(aplRST!cantnieto)) > 0)) Then GoTo Skip
        End If
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
            .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!nom_estado_hij)
            .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!nom_situacion_hij)
            .TextMatrix(.Rows - 1, colNomHijo) = ClearNull(aplRST!nom_area_hij)
            .TextMatrix(.Rows - 1, colCodHijo) = ClearNull(aplRST!cod_area_hij)
            If InStr(1, "CGRU|CSEC|CGCI|CDIR|", xPerfil, vbTextCompare) > 0 Then
                .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
                .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup_hij)
                .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan_hij), Format(aplRST!fe_ini_plan_hij, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan_hij), Format(aplRST!fe_fin_plan_hij, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colCantNieto) = IIf(Val(ClearNull(aplRST!cantnieto)) > 0, ClearNull(aplRST!cantnieto), "")
                .TextMatrix(.Rows - 1, colPrioEjecuc) = IIf(Val(ClearNull(aplRST!prio_ejecuc)) > 0, padRight(ClearNull(aplRST!prio_ejecuc), 2), "")
            Else
                .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
                .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
            End If
            .TextMatrix(.Rows - 1, colVisibilidad) = Left(ClearNull(aplRST!nom_importancia), 2)
            .TextMatrix(.Rows - 1, colNroInterno) = padRight(ClearNull(aplRST!pet_nrointerno), 6)
            .TextMatrix(.Rows - 1, colCodigoRecurso) = ClearNull(aplRST!cod_solicitante)
            .TextMatrix(.Rows - 1, colEstCod) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colSitCod) = ClearNull(aplRST!cod_situacion)
            Select Case xPerfil
                Case "SOLI", "REFE", "SUPE", "AUTO"
                    .TextMatrix(.Rows - 1, colRefSistema) = ClearNull(aplRST!nom_BPE)
                Case "BPAR"
                    .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
                    .TextMatrix(.Rows - 1, colPrioridad) = IIf(ClearNull(aplRST!prioridadBPE) = "999999", "-", ClearNull(aplRST!prioridadBPE))
                    .TextMatrix(.Rows - 1, colRefSistema) = IIf(ClearNull(aplRST!nom_BPE) = "", "-", ClearNull(aplRST!nom_BPE))
                    .TextMatrix(.Rows - 1, colAnalisis) = ClearNull(aplRST!analisis)
                    .TextMatrix(.Rows - 1, colPlanificado) = ClearNull(aplRST!planificado)
                Case "GBPE"
                    .TextMatrix(.Rows - 1, colPrioridad) = IIf(ClearNull(aplRST!prioridadBPE) = "999999", "-", ClearNull(aplRST!prioridadBPE))
                    .TextMatrix(.Rows - 1, colRefSistema) = ClearNull(aplRST!nom_bpar)
                    .TextMatrix(.Rows - 1, colAnalisis) = ClearNull(aplRST!analisis)
                    .TextMatrix(.Rows - 1, colPlanificado) = ClearNull(aplRST!planificado)
                    .TextMatrix(.Rows - 1, colEnPlanificacion) = ClearNull(aplRST!en_planificacion)
                    .TextMatrix(.Rows - 1, colPuntuacion) = Format(ClearNull(aplRST!puntuacion), "###0.000")
                    .TextMatrix(.Rows - 1, colSectorSolicitante) = ClearNull(aplRST!nom_sector)
                Case "CGRU", "ANAL"
                    .TextMatrix(.Rows - 1, colRefSistema) = IIf(ClearNull(aplRST!nom_bpar) = "", "-", ClearNull(aplRST!nom_bpar))
                    If IsNull(aplRST!prioridadBPE) Then
                        .TextMatrix(.Rows - 1, colPrioridad) = "-"
                    Else
                        .TextMatrix(.Rows - 1, colPrioridad) = IIf(aplRST!prioridadBPE = 999999, "-", aplRST!prioridadBPE)
                    End If
            End Select
            .TextMatrix(.Rows - 1, colSolicitante) = ClearNull(aplRST!cod_solicitante)
            .TextMatrix(.Rows - 1, colPetClase) = IIf(ClearNull(aplRST!cod_clase) = "SINC", "-", ClearNull(aplRST!cod_clase))
            .TextMatrix(.Rows - 1, colPetImpTech) = ClearNull(aplRST!pet_imptech)
            .TextMatrix(.Rows - 1, colPetFePedido) = IIf(Not IsNull(aplRST!fe_pedido), Format(aplRST!fe_pedido, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colPetRegulatorio) = ClearNull(aplRST!pet_regulatorio)
            .TextMatrix(.Rows - 1, colPetRO) = ClearNull(aplRST!pet_riesgo)
            .TextMatrix(.Rows - 1, colPetCategoria) = IIf(ClearNull(aplRST!categoria) = "999", "---", ClearNull(aplRST!categoria))
            .TextMatrix(.Rows - 1, colPrioridadOrden) = ClearNull(aplRST!prioridadBPE)
            .TextMatrix(.Rows - 1, colEstHijoCod) = ClearNull(aplRST!cod_estado_hij)
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
        End With
Skip:
        aplRST.MoveNext
        DoEvents
    Loop
finx:
    grdDatos.visible = True
    flgEnCarga = False
    If glNumeroPeticion <> "" Then
        If grdDatos.Rows > 1 Then
            Call GridSeek(Me, grdDatos, colNroInterno, glNumeroPeticion, True)
            Call MostrarSeleccion(False)
        End If
    Else
        If grdDatos.Rows > 1 Then
            grdDatos.TopRow = 1
            grdDatos.row = 1
            grdDatos.rowSel = 1
            grdDatos.col = 0
            grdDatos.ColSel = grdDatos.cols - 1
            Call MostrarSeleccion(False)
        End If
    End If
    cmdPerfil.Enabled = True
    cmdFiltro.Enabled = True
    cmdBuscar.Enabled = True
    flgEnCarga = False
    Call Puntero(False)
    Call Status("Listo. " & grdDatos.Rows - grdDatos.FixedRows & " registro(s).")
    Call LockProceso(False)
End Sub

Private Sub CargarGridAnalisis()
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
    Dim vRetorno() As String
    Dim flgPendientes As Boolean
    Dim auxAgrup As String
    
    flgEnCarga = True
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents

    If ClearNull(txtAgrup.text) <> "" Then
        If ParseString(vRetorno, txtAgrup.text, "|") > 0 Then
            auxAgrup = vRetorno(2)
        End If
    Else
        auxAgrup = ""
    End If
    
'    If InStr(sEstadoQuery, "PENDIE") > 0 Then
'        flgPendientes = True
'    Else
'        flgPendientes = False
'    End If
    'flgEnCarga = True
    With grdAnalisis
        .visible = False
        .Clear
        DoEvents
        .HighLight = flexHighlightAlways
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        '.BackColorBkg = Me.BackColor
        .ColHeader(0) = flexColHeaderOn
        '.FontFixed.Bold = True
        
        .FixedRows = 1
        .FixedCols = 0
        .Font.name = "Tahoma"
        .Font.Size = 8
        .GridColor = &HE0E0E0
        .FocusRect = flexFocusNone
        .cols = 11 + 3 'TOTAL_COLS
        .RowHeightMin = 250
        '.ban
        .Rows = 1
        .ColHeaderCaption(0, 0) = "": .ColWidth(0) = 300: .ColAlignment(0) = flexAlignCenterCenter
        .ColHeaderCaption(0, 1) = "Petic.": .ColWidth(1) = 750: .ColAlignment(1) = flexAlignRightCenter
        .ColHeaderCaption(0, 2) = "Tipo": .ColWidth(2) = 600: .ColAlignment(2) = flexAlignLeftCenter
        .ColHeaderCaption(0, 3) = "Clase": .ColWidth(3) = 600: .ColAlignment(3) = flexAlignLeftCenter
        .ColHeaderCaption(0, 4) = "Cat.": .ColWidth(4) = 650: .ColAlignment(4) = flexAlignLeftCenter
        .ColHeaderCaption(0, 5) = "T�tulo": .ColWidth(5) = 4400: .ColAlignment(5) = flexAlignLeftCenter
        .ColHeaderCaption(0, 6) = "Ptos.": .ColWidth(6) = 700: .ColAlignment(6) = flexAlignRightCenter             ' add -017- a.
        .ColHeaderCaption(0, 7) = "Ref. RGyP": .ColWidth(7) = 2000: .ColAlignment(7) = flexAlignLeftCenter
        .ColHeaderCaption(0, 8) = "Ref. Sistemas": .ColWidth(8) = 2000: .ColAlignment(8) = flexAlignLeftCenter
        .ColHeaderCaption(0, 9) = "NroInt.": .ColWidth(9) = 0: .ColAlignment(9) = flexAlignRightCenter
        .ColHeaderCaption(0, 10) = "Prior.": .ColWidth(10) = 800: .ColAlignment(10) = flexAlignRightCenter
        
        .ColHeaderCaption(0, 11) = "Nov-16": .ColWidth(11) = 800: .ColAlignment(11) = flexAlignRightCenter
        .ColHeaderCaption(0, 12) = "Dic-16": .ColWidth(12) = 800: .ColAlignment(12) = flexAlignRightCenter
        .ColHeaderCaption(0, 13) = "Ene-17": .ColWidth(13) = 800: .ColAlignment(13) = flexAlignRightCenter
        'Call CambiarEfectoLinea2(grdAnalisis, prmGridEffectFontBold)
    End With
    Call Puntero(True)

    If Not sp_GetPeticionAreaBPE1(txtNroBuscar, Null, Null, 0, "N", "") Then
       GoTo finx
    End If
    Dim i As Long
    
    grdAnalisis.Rows = aplRST.RecordCount + 1
    
    Do While Not aplRST.EOF
        With grdAnalisis
            i = i + 1
            .TextMatrix(i, 0) = ""
            .TextMatrix(i, 1) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(i, 2) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(i, 3) = IIf(ClearNull(aplRST!cod_clase) = "SINC", "-", ClearNull(aplRST!cod_clase))   ' upd -116- c.
            .TextMatrix(i, 4) = ClearNull(aplRST!categoria)
            .TextMatrix(i, 5) = ClearNull(aplRST!Titulo)
            .TextMatrix(i, 6) = Format(ClearNull(aplRST!puntuacion), "###0.000")
            .TextMatrix(i, 7) = ClearNull(aplRST!nom_BPE)
            .TextMatrix(i, 8) = ClearNull(aplRST!nom_bpar)
            .TextMatrix(i, 9) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(i, 10) = IIf(ClearNull(aplRST!prioridadBPE) = 999999, "-", ClearNull(aplRST!prioridadBPE))
            .row = i
            If i Mod 2 > 0 Then Call PintarLinea2(grdAnalisis, prmGridFillRowColorLightGrey2)
        End With
Skip:
        aplRST.MoveNext
        DoEvents
    Loop
finx:
    With grdAnalisis
        .visible = True
        flgEnCarga = False
        If glNumeroPeticion <> "" Then
            If .Rows > 1 Then
                'Call GridSeek(Me, grdAnalisis, colNroInterno, glNumeroPeticion, True)
                'Call MostrarSeleccion(False)
            End If
        Else
            If .Rows > 1 Then
                .TopRow = 1
                .row = 1
                .rowSel = 1
                .col = 0
                .ColSel = .cols - 1
                Call MostrarSeleccion(False)
            End If
        End If
        flgEnCarga = False
        Call Puntero(False)
        Call LockProceso(False)
    End With
End Sub

Public Sub MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
'    cmdGrupo.Enabled = False
'    cmdSector.Enabled = False
'    cmdGrupOper.Enabled = False
'    cmdVisualizar.Enabled = False
'    cmdAgrupar.Enabled = False
'    cmdPrintPet.Enabled = False
    Call setHabilCtrl(cmdGrupo, DISABLE)
    Call setHabilCtrl(cmdSector, DISABLE)
    Call setHabilCtrl(cmdGrupOper, DISABLE)
    Call setHabilCtrl(cmdVisualizar, DISABLE)
    Call setHabilCtrl(cmdAgrupar, DISABLE)
    Call setHabilCtrl(cmdPrintPet, DISABLE)
    sArea = ""
    With grdDatos
        If .rowSel > 0 Then
            If .TextMatrix(.rowSel, colNroInterno) <> "" Then
                glNroAsignado = ClearNull(.TextMatrix(.rowSel, colNroAsignado))
                glNumeroPeticion = ClearNull(.TextMatrix(.rowSel, colNroInterno))
                glCodigoRecurso = .TextMatrix(.rowSel, colCodigoRecurso)
                glEstadoPeticion = .TextMatrix(.rowSel, colEstCod)
                sArea = .TextMatrix(.rowSel, colCodHijo)
                'glPeticion_sox001 = .TextMatrix(.RowSel, colPetFePedido)     ' add -001- c.
                Call setHabilCtrl(cmdVisualizar, NORMAL)
                Call setHabilCtrl(cmdAgrupar, NORMAL)
                Call setHabilCtrl(cmdPrintPet, NORMAL)
                If xPerfil = "CSEC" And ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
                    'Call setHabilCtrl(cmdSector, NORMAL)       ' TODO: 27.10.16
                    Call setHabilCtrl(cmdGrupo, NORMAL)
                    '{ add -004- a.
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(glEstadoPeticion), vbTextCompare) > 0 Then
                        Call setHabilCtrl(cmdSector, DISABLE)
                    End If
                    '}
                End If
                If xPerfil = "CGRU" And ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
                    Call setHabilCtrl(cmdGrupOper, NORMAL)
                    Call setHabilCtrl(cmdGrupo, NORMAL)
                    '{ add -004- a.
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(glEstadoPeticion), vbTextCompare) > 0 Then
                        If glEsSeguridadInformatica Or glEsHomologador Or glEsTecnologia Then
                            Call setHabilCtrl(cmdGrupOper, NORMAL)
                        Else
                            Call setHabilCtrl(cmdGrupOper, DISABLE)
                        End If
                    End If
                    '}
                End If
            End If
        End If
    End With
End Sub

'Private Sub grdDatos_Click()
'    If grdDatos.MouseRow = 0 And grdDatos.Rows > 1 Then
'       grdDatos.RowSel = 1
'       If grdDatos.MouseCol = colFinicio Then
'           grdDatos.Col = colFinicioSRT
'       ElseIf grdDatos.MouseCol = colFtermin Then
'           grdDatos.Col = colFterminSRT
'       Else
'           grdDatos.Col = grdDatos.MouseCol
'       End If
'       grdDatos.Sort = flexSortStringNoCaseAscending
'    End If
'    Call MostrarSeleccion
'    DoEvents
'End Sub

Private Sub grdDatos_Click()
    bSorting = True
    With grdDatos
        If .rowSel > 0 Then glNumeroPeticion = .TextMatrix(.rowSel, colNroInterno)
        Debug.Print .TextMatrix(.rowSel, colNroInterno)
        flgEnCarga = True
        'Debug.Print "Columna seleccionada para ordenar: " & .MouseCol & " - Titulo: " & .TextMatrix(0, .MouseCol)
        'If .MouseCol = 11 And .MouseRow = 0 Then .col = 37
        Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada, , , , ordenColumnas)
        bSorting = False
        flgEnCarga = False
    End With
End Sub

Private Sub grdDatos_DblClick()
    'Call MostrarSeleccion              ' OMG... para qu� esto???!!!
    DoEvents
    If glNumeroPeticion <> "" Then Call Visualizar            ' add -015- a.
    '{ del -015- a.
    'If glNumeroPeticion <> "" Then
    '    If Not LockProceso(True) Then
    '        Exit Sub
    '    End If
    '    glModoPeticion = "VIEW"
    '    On Error Resume Next
    '    frmPeticionesCarga.Show vbModal
    '    DoEvents
    '    If Me.Tag = "REFRESH" Then
    '        InicializarPantalla
    '    End If
    '    grdDatos.SetFocus      ' add -002- a.
    'End If
    '}
End Sub

'{ add -002- a.
'Private Sub grdDatos_KeyPress(KeyAscii As Integer)
'    If KeyAscii = 13 Then
'        grdDatos_DblClick
'    End If
'End Sub
'}

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    If grdDatos.row <= grdDatos.rowSel Then
        nRwD = grdDatos.row
        nRwH = grdDatos.rowSel
    Else
        nRwH = grdDatos.row
        nRwD = grdDatos.rowSel
    End If
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, colMultiSelect) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, colMultiSelect) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        If Button = vbLeftButton Then
            With grdDatos
                If .rowSel > 0 Then
                     If ClearNull(.TextMatrix(.rowSel, colMultiSelect)) = "" Then
                        .TextMatrix(.rowSel, colMultiSelect) = "�"
                     Else
                        .TextMatrix(.rowSel, colMultiSelect) = ""
                    End If
                End If
            End With
        End If
    End If
    If Shift = 0 Then
        If Button = vbLeftButton Then
            With grdDatos
                For i = 1 To .Rows - 1
                   .TextMatrix(i, colMultiSelect) = ""
                Next
                If .rowSel > 0 Then
                     .TextMatrix(.rowSel, colMultiSelect) = "�"
                End If
            End With
        End If
    End If
End Sub
 
Private Sub grdDatos_RowColChange()
    If flgInMultiselect Then Exit Sub
    '{ add
    If flgEnCarga Then Exit Sub
    With grdDatos
        glNumeroPeticion = .TextMatrix(.rowSel, colNroInterno)
        '.TextArray
        'Debug.Print "SELECCI�N ACTUAL: " & .TextMatrix(.RowSel, colNroAsignado)
        'If InStr(1, "SELECCI�N ACTUAL: " & .TextMatrix(.RowSel, colNroAsignado), "SELECCI�N ACTUAL: Petic.", vbTextCompare) > 0 Then Stop
    End With
    '}
    If Not flgEnCarga Then Call MostrarSeleccion
End Sub

'{ add -002- c.
Private Sub txtNroBuscar_KeyPress(KeyAscii As Integer)
    If KeyAscii = 22 Then 'paste(ctrl+ v)
        If IsNumeric(Clipboard.GetText(vbCFText)) Then
            KeyAscii = 0
            txtNroBuscar.text = Clipboard.GetText(vbCFText)
        End If
    End If
    If KeyAscii = 13 Then
        cmdBuscar_Click
    End If
End Sub

Private Sub cboEstado_Click()
    If Not flgEnCarga Then Call cmdFiltro_Click
End Sub

Private Sub cboFiltroGestion_Click()
    If Not flgEnCarga Then Call cmdFiltro_Click
End Sub

Private Sub cmdPrintBandeja_Click()
    'glCrystalNewVersion = True         ' Esto es para pruebas en desarrollo
    If glCrystalNewVersion Then         ' Nueva versi�n
        Call ReporteNuevo
    Else
'        Call ReporteAnterior
    End If
End Sub

Private Sub ReporteNuevo()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    ' Auxiliares para los titulos y subtitulos
    Dim cTitulo As String
    Dim cSubTitulo As String

    cTitulo = Trim(lblPerfil)
    cSubTitulo = "Estados filtrados: " & Trim(Mid(cboEstado, 1, InStr(1, cboEstado, "||", vbTextCompare) - 1))
    
    Select Case xPerfil
        Case "BPAR": sReportName = "petperfilbp6.rpt"
        'Case "SOLI", "REFE", "AUTO", "SUPE": sReportName = "petperfilsoli6.rpt"
        Case Else
            sReportName = "petperfil6.rpt"
    End Select

    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Peticiones en Bandeja de trabajo por perfil"
        .ReportComments = cTitulo & vbCrLf & cSubTitulo
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        If InStr(1, "BPAR", xPerfil, vbTextCompare) > 0 Then
            Select Case crParamDef.ParameterFieldName
                Case "@pet_nroasignado": crParamDef.AddCurrentValue (0)
                Case "@cod_bpar": crParamDef.AddCurrentValue (glLOGIN_ID_REEMPLAZO)
                Case "@cod_estado": crParamDef.AddCurrentValue (IIf(sEstadoQuery = "NULL", " ", sEstadoQuery))
                Case "@anx_especial": crParamDef.AddCurrentValue ("S")
                Case "@agr_nrointerno": crParamDef.AddCurrentValue (0)
                Case "@prio_ejecuc": crParamDef.AddCurrentValue (0)
            End Select
        Else
            Select Case crParamDef.ParameterFieldName
                Case "@pet_nroasignado": crParamDef.AddCurrentValue (0)
                Case "@modo_recurso": crParamDef.AddCurrentValue (xPerfil)
                Case "@cod_nivel": crParamDef.AddCurrentValue (xNivel)
                Case "@cod_area": crParamDef.AddCurrentValue (xArea)
                Case "@cod_estado": crParamDef.AddCurrentValue (IIf(sEstadoQuery = "NULL", " ", sEstadoQuery))
                Case "@anx_especial": crParamDef.AddCurrentValue ("S")
                Case "@agr_nrointerno": crParamDef.AddCurrentValue (0)
                Case "@prio_ejecuc": crParamDef.AddCurrentValue (0)
            End Select
        End If
    Next
 
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

'{ add -010- a.
'Private Sub ReporteAnterior()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String
'    Dim cSubTitulo As String
'
'    Call Status("Preparando el informe...")
'    Call Puntero(True)
'
'    cTitulo = Trim(lblPerfil)
'    cSubTitulo = "Estados filtrados: " & Trim(Mid(cboEstado, 1, InStr(1, cboEstado, "||", vbTextCompare) - 1))
'
'    With mdiPrincipal.CrystalReport1
'        Select Case xPerfil
'            Case "BPAR": cPathFileNameReport = App.Path & "\" & RPT_PERFIL_BP
'            Case "SOLI", "REFE", "AUTO", "SUPE": cPathFileNameReport = App.Path & "\" & RPT_PERFIL_SOLI
'            Case Else
'                cPathFileNameReport = App.Path & "\" & RPT_PERFIL
'        End Select
'        'If InStr(1, "BPAR", xPerfil, vbTextCompare) > 0 Then
'        '    cPathFileNameReport = App.Path & "\" & RPT_PERFIL_BP
'        'ElseIf InStr(1, "SOLI|REFE|AUTO|SUPE", xPerfil, vbTextCompare) > 0 Then
'        '    cPathFileNameReport = App.Path & "\" & RPT_PERFIL_SOLI
'        'Else
'        '    cPathFileNameReport = App.Path & "\" & RPT_PERFIL
'        'End If
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .RetrieveStoredProcParams
'        If InStr(1, "BPAR", xPerfil, vbTextCompare) > 0 Then
'            .StoredProcParam(0) = 0
'            .StoredProcParam(1) = glLOGIN_ID_REEMPLAZO
'            .StoredProcParam(2) = IIf(sEstadoQuery = "NULL", " ", sEstadoQuery)
'            .StoredProcParam(3) = "S"
'            .StoredProcParam(4) = 0
'            .StoredProcParam(5) = 0
'        Else
'            .StoredProcParam(0) = 0
'            .StoredProcParam(1) = xPerfil
'            .StoredProcParam(2) = xNivel
'            .StoredProcParam(3) = xArea
'            .StoredProcParam(4) = IIf(sEstadoQuery = "NULL", " ", sEstadoQuery)
'            .StoredProcParam(5) = "S"
'            .StoredProcParam(6) = 0
'            .StoredProcParam(7) = 0
'        End If
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & cTitulo & "'"
'        .Formulas(3) = "@1_TITULO='" & cSubTitulo & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Call Puntero(False)
'        Call Status("Listo.")
'    End With
'End Sub

'Private Sub chkPropias_Click()
'    { del -013- a.
'    If chkAutoFilter = 1 Then
'        cmdFiltro_Click
'    End If
'    }
'    cmdFiltro_Click     ' add -013- a.
'End Sub
'}

'{ add -011- a.
Private Sub cmdTraspaso_Click()
    Dim i As Long, x As Long
    Dim cMensaje As String
    Dim bCargar As Boolean
    
    bSeleccion = False
    bCargar = False
    
    With grdDatos
        For x = 2 To .Rows
            If .TextMatrix(x - 1, colMultiSelect) = "�" Then
                bCargar = True
                bSeleccion = True
                i = i + 1
                ReDim Preserve spGrupo.vPeticiones(i)
                spGrupo.vPeticiones(i) = .TextMatrix(x - 1, colNroInterno)
            End If
        Next x
        If bCargar Then
            frmPeticionesTraspaso.Show 1
        Else
            MsgBox "No ha seleccionado ninguna petici�n de la bandeja para transferir.", vbExclamation + vbOKOnly, "Selecci�n de peticiones"
        End If
    End With
End Sub
'}

Private Sub cmdPlanificar_Click()
    frmPeticionesPlanificar.Show 1
End Sub

' Copia en la clipboard la lista de peticiones asignadas con su numero interno (tienen que estar originalmente tambi�n en la clipboard)
'Private Sub Command1_Click()
'    Dim temp As String, res As String
'    Dim pet_nrointerno As Integer
'    Dim pet_nroasignado As Integer
'    Dim col As New Collection
'
'    res = ""
'    temp = Clipboard.GetText: Clipboard.Clear
'    If Len(Trim(temp)) > 0 Then
'        pet_nrointerno = 0
'        pet_nroasignado = 0
'        Do While Len(temp) > 0
'            If InStr(1, temp, vbCrLf, vbTextCompare) Then
'                pet_nroasignado = Trim(Mid(temp, 1, InStr(1, temp, vbCrLf, vbTextCompare)))
'            Else
'                pet_nroasignado = temp
'            End If
'            If IsNumeric(pet_nroasignado) Then
'                If sp_GetUnaPeticionAsig(pet_nroasignado) Then pet_nrointerno = aplRST.Fields!pet_nrointerno
'                If pet_nrointerno > 0 Then res = res & pet_nroasignado & "|" & pet_nrointerno & vbCrLf ' col.Add pet_nrointerno, pet_nroasignado
'            End If
'            If InStr(1, temp, vbCrLf, vbTextCompare) > 0 Then
'                temp = Mid(temp, InStr(1, temp, vbCrLf, vbTextCompare) + 2)
'            Else
'                temp = ""
'            End If
'            DoEvents
'        Loop
'    End If
'    Clipboard.SetText res
'    MsgBox "Proceso finalizado"
'End Sub

'Private Sub Command1_Click()
'    Dim nrosol() As Long
'    Dim i As Long
'
'    If sp_GetSolicitud(Null, Null, Null, Null, Null, Null, Null) Then
'        Do While Not aplRST.EOF
'            ReDim Preserve nrosol(i)
'            nrosol(i) = ClearNull(aplRST.Fields!sol_nrointerno)
'            i = i + 1
'            aplRST.MoveNext
'            DoEvents
'        Loop
'
'        For i = 0 To UBound(nrosol)
'            call sp_UpdateSolicitudField(
'        Next i
'    End If
'End Sub

'Private Sub Command1_Click()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String
'    Dim fe_desde As Date
'    Dim fe_hasta As Date
'
'
'    Load auxFechaDesdeHasta
'    auxFechaDesdeHasta.Show 1
'
'    fe_desde = auxFechaDesdeHasta.txtFechaDesde
'    fe_hasta = auxFechaDesdeHasta.txtFechaHasta
'
'    With mdiPrincipal.CrystalReport1
'        cPathFileNameReport = App.Path & "\" & "hspetterm01.rpt"
'        cTitulo = "Peticiones en estado terminal desde " & Format(fe_desde, "dd/mm/yyyy") & " y " & Format(fe_hasta, "dd/mm/yyyy")
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .RetrieveStoredProcParams
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .StoredProcParam(0) = IIf(IsNull(fe_desde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(fe_desde, "yyyymmdd"))
'        .StoredProcParam(1) = IIf(IsNull(fe_hasta), Format(date, "yyyymmdd"), Format(fe_hasta, "yyyymmdd"))
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & cTitulo & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Call Puntero(False)
'        Call Status("Listo.")
'    End With
'End Sub

'Private Sub Command1_Click()
'    If sp_GetUnaPeticion(12327) Then
'        If IsNull(aplRST.Fields!pet_prjid) Then
'            call sp_UpdatePetField(12327
'        End If
'    End If
'End Sub

'Private Sub Command1_Click()
'    Dim vPeticionAsig(33) As Long
'    Dim vPeticionInte(33) As Long
'    'Dim vRecurso(38) As String
'    'Dim vGrupo(38) As String
'    'Dim vSector(38) As String
'    Dim i As Long
'
'    vPeticionAsig(0) = 20633
'    vPeticionAsig(1) = 20966
'    vPeticionAsig(2) = 21399
'    vPeticionAsig(3) = 22539
'    vPeticionAsig(4) = 23852
'    vPeticionAsig(5) = 28090
'    vPeticionAsig(6) = 28092
'    vPeticionAsig(7) = 50067
'    vPeticionAsig(8) = 50824
'    vPeticionAsig(9) = 51110
'    vPeticionAsig(10) = 51643
'    vPeticionAsig(11) = 52504
'    vPeticionAsig(12) = 53735
'    vPeticionAsig(13) = 54849
'    vPeticionAsig(14) = 54950
'    vPeticionAsig(15) = 54959
'    vPeticionAsig(16) = 54964
'    vPeticionAsig(17) = 54965
'    vPeticionAsig(18) = 54966
'    vPeticionAsig(19) = 54967
'    vPeticionAsig(20) = 55086
'    vPeticionAsig(21) = 55444
'    vPeticionAsig(22) = 55717
'    vPeticionAsig(23) = 55802
'    vPeticionAsig(24) = 55843
'    vPeticionAsig(25) = 55972
'    vPeticionAsig(26) = 56028
'    vPeticionAsig(27) = 56469
'    vPeticionAsig(28) = 56591
'    vPeticionAsig(29) = 56703
'    vPeticionAsig(30) = 56721
'    vPeticionAsig(31) = 56807
'    vPeticionAsig(32) = 56984
'
'
'    For i = 0 To 33
'        Call Status("Procesando " & (i + 1) & "...")
'        If sp_GetUnaPeticionAsig(vPeticionAsig(i)) Then
'            vPeticionInte(i) = ClearNull(aplRST.Fields!pet_nrointerno)
'            'Call sp_InsertPeticionRecurso(vPeticionInte(i), vRecurso(i), vGrupo(i), vSector(i), "DESA", "MEDIO")
'            Call sp_InsertPeticionRecurso(vPeticionInte(i), "A126013", "21-01", "12", "DESA", "MEDIO")
'        End If
'        DoEvents
'    Next i
'    MsgBox "Proceso finalizado."
'    Call Status(CStr(i))
'End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        'app.StartLogging
        If glNumeroPeticion <> "" Then Call Visualizar
    End If
    If Not flgEditarPrioridad Then Exit Sub
    If Not IsNumeric(Chr(KeyAscii)) Then Exit Sub
    If KeyAscii >= 32 And KeyAscii <= 127 Then
        grdDatos.text = grdDatos.text & Chr(KeyAscii)
    End If
      
End Sub
  
Private Sub grdDatos_KeyUp(KeyCode As Integer, Shift As Integer)
    If Not flgEditarPrioridad Then Exit Sub
    Select Case KeyCode
        Case vbKeyDelete
            grdDatos.text = ""
        Case vbKeyBack
            If Len(grdDatos.text) > 0 Then
                grdDatos.text = Left(grdDatos.text, Len(grdDatos.text) - 1)
            End If
    End Select
End Sub

Private Sub grdDatos_OLEDragOver(Data As MSFlexGridLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, State As Integer)
    'Make sure that the format is text.
    If Data.GetFormat(vbCFText) Then
      ' If it is text, enable dropping for the second TextBox.
        grdDatos.OLEDropMode = vbOLEDropManual
    End If
    
    Dim lCol As Long
    Dim lRow As Long
    ' -- Obtener Indices
    lRow = pvGetRowIndex(grdDatos)
    lCol = pvGetColIndex(grdDatos)
    ' -- Comprobar que no sea la misma celda, si es la misma salir para evitar el parpadeo
    If lCurrentCol = lCol And lCurrentRow = lRow Then Exit Sub
    With grdDatos
        If lRow <> -1 And lCol <> -1 Then
            If lPrevCol <> -1 And lPrevRow <> -1 Then
                ' -- Restaurar celda
                .col = lPrevCol
                .row = lPrevRow
                '.CellBackColor = .BackColor
                '.CellForeColor = .ForeColor
                '.CellFontBold = False
                '.CellFontUnderline = False
            End If
            ' -- Cambiar y resaltar la celda activa ( texto y BackColor )
            .row = lRow
            .col = lCol
            '.CellForeColor = TEXTCOLOR_CELL
            '.CellBackColor = BACKCOLOR_CELL
            '.CellFontUnderline = True
            ' -- Guardar valores para luego poder restaurar
            lPrevCol = lCol
            lPrevRow = lRow
            lCurrentCol = lCol
            lCurrentRow = lRow
        End If
    End With
End Sub

'Private Sub grdDatos_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
'    ' Copy the text from the Data object to the second TextBox.
'    'Text2.text = Data.GetData(vbCFText)
'    grdDatos.AddItem Data.GetData(vbCFText)
'End Sub

Private Sub grdDatos_OLEDragDrop(Data As MSFlexGridLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
    ' Copy the text from the Data object to the second TextBox.
    Dim lCol As Long
    Dim lRow As Long
    ' -- Obtener Indices
    lRow = pvGetRowIndex(grdDatos)
    lCol = pvGetColIndex(grdDatos)
    'If lCurrentCol = lCol And lCurrentRow = lRow Then Exit Sub
    MsgBox "Agregando " & Data.GetData(vbCFText) & " a la petici�n " & grdDatos.TextMatrix(lRow, colNroAsignado), vbInformation
End Sub

Private Function pvGetColIndex(objFlex As Object) As Long
    pvGetColIndex = grdDatos.MouseCol
End Function

Private Function pvGetRowIndex(objFlex As Object) As Long
    With grdDatos
        If .MouseRow = 0 Then
           pvGetRowIndex = -1
        Else
           pvGetRowIndex = .MouseRow
        End If
    End With
End Function

' Calcula el �ndice que se usar� con la propiedad TextArray.
Private Function grdIndice(ByRef grd As MSFlexGrid, Fila As Integer, col As Integer) As Long
    grdIndice = Fila * grd.cols + col
End Function

'    Dim i As Integer
'    ' Llena el control MSFlexGrid con datos mediante la propiedad TextArray.
'    For i = MSHFlexGrid1.FixedRows To MSFlexGrid1.Rows - 1
'        ' ** columna 1
'        MSHFlexGrid1.TextArray(faIndice(i, 1)) = CualquierNombre()
'        ' Columna 2.
'        MSHFlexGrid1.TextArray(faIndice(i, 2)) = CualquierN�mero()
'    Next


