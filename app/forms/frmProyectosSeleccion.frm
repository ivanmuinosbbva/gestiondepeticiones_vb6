VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProyectosIDMSeleccion 
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selecci�n de proyecto"
   ClientHeight    =   7305
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10530
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmProyectosSeleccion.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7305
   ScaleWidth      =   10530
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar sbProyectos 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   7020
      Width           =   10530
      _ExtentX        =   18574
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imgProyectos 
      Left            =   9720
      Top             =   1680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosSeleccion.frx":014A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosSeleccion.frx":06E4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosSeleccion.frx":0C7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosSeleccion.frx":1218
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosSeleccion.frx":17B2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosSeleccion.frx":1D4C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosSeleccion.frx":22E6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trwProyectos 
      Height          =   5415
      Left            =   120
      TabIndex        =   4
      Top             =   1560
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   9551
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      ImageList       =   "imgProyectos"
      Appearance      =   1
   End
   Begin VB.Frame fraOpciones 
      Caption         =   " Opciones de filtro "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   10335
      Begin VB.CheckBox chkAgrupamiento 
         Caption         =   "Agrupar proyectos"
         Height          =   255
         Left            =   8520
         TabIndex        =   10
         Top             =   240
         Visible         =   0   'False
         Width           =   1695
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   300
         Left            =   1800
         TabIndex        =   0
         Top             =   360
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   529
         MaxLength       =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   4
         Validation      =   0   'False
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.TextBox txtPrjNom 
         Height          =   300
         Left            =   1800
         MaxLength       =   60
         TabIndex        =   3
         Top             =   720
         Width           =   5895
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   300
         Left            =   2400
         TabIndex        =   1
         Top             =   360
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   529
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   3
         Validation      =   0   'False
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   300
         Left            =   3000
         TabIndex        =   2
         Top             =   360
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   529
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   3
         Validation      =   0   'False
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "C�digo de proyecto:"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   413
         Width           =   1470
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "Para seleccionar un proyecto en particular, presione Enter sobre el proyecto selecionado o haga doble click"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   7680
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "Nombre o descripci�n:"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   6
         Top             =   773
         Width           =   1590
      End
   End
End
Attribute VB_Name = "frmProyectosIDMSeleccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -002- a. FJS 03.02.2016 - Se modifica la pantalla para utilizar un TreeView en vez de una grilla.

Option Explicit

Public lProjId As Long
Public lProjSubId As Long
Public lProjSubSId As Long
Public cProjNom As String
Public flgSeleccionado As Boolean

Const IMG_PROYECTO = 7
Const IMG_SUBPROY = 5
Const IMG_SUBSUBP = 4

Dim flgCargados As Boolean

Private Sub Form_Load()
    flgCargados = False
    flgSeleccionado = False
    Call CargarArbol
End Sub

Private Sub CargarArbol()
    Dim nodo As MSComctlLib.node
    Dim proyecto As String
    Dim nodoN1 As String
    Dim nodoN2 As String
    Dim icono As Byte
    
    With trwProyectos
        .Enabled = False
        .Nodes.Clear
        Set nodo = .Nodes.Add(, , "RAIZ", "Proyectos", 1)
        If Not flgCargados Then
            Call sp_GetProyectoIDM2(Null, Null, Null, Null, IIf(chkAgrupamiento.Value = 1, "P", "P"), Null)
            flgCargados = True
        End If
        nodoN2 = ""
        nodoN1 = ""
        If chkAgrupamiento.Value = 1 Then
            Do While Not aplRST.EOF
                proyecto = ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & "." & ClearNull(aplRST.Fields!ProjSubSId) & " - " & ClearNull(aplRST.Fields!projnom)
                Select Case ClearNull(aplRST.Fields!nivel)
                    Case "3"
                        icono = IMG_SUBSUBP
                        Set nodo = .Nodes.Add(IIf(nodoN2 = "", "RAIZ", nodoN2), tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
                    Case "2"
                        icono = IMG_SUBPROY
                        nodoN2 = "A" & ClearNull(aplRST.Fields!orden)
                        Set nodo = .Nodes.Add(IIf(nodoN1 = "", "RAIZ", nodoN1), tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
                    Case "1"
                        icono = IMG_PROYECTO
                        nodoN1 = "A" & ClearNull(aplRST.Fields!orden)
                        nodoN2 = ""
                        Set nodo = .Nodes.Add("RAIZ", tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
                End Select
                aplRST.MoveNext
                DoEvents
            Loop
        Else
            Do While Not aplRST.EOF
                proyecto = ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & "." & ClearNull(aplRST.Fields!ProjSubSId) & " - " & ClearNull(aplRST.Fields!projnom)
                Select Case ClearNull(aplRST.Fields!nivel)
                    Case "3": icono = IMG_SUBSUBP
                    Case "2": icono = IMG_SUBPROY
                    Case "1": icono = IMG_PROYECTO
                End Select
                Set nodo = .Nodes.Add("RAIZ", tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        sbProyectos.SimpleText = aplRST.RecordCount & " proyecto(s)."
        .Nodes(1).Expanded = True
        .Enabled = True
    End With
End Sub

Private Sub txtPrjId_Change()
    If Len(Trim(txtPrjId)) > 0 Then
        If Mid(txtPrjId, Len(txtPrjId), 1) = "'" Then
            txtPrjId = Mid(txtPrjId, 1, Len(txtPrjId) - 1)
        End If
    End If
    Call Filtrar
End Sub

Private Sub txtPrjSubId_Change()
    If Len(Trim(txtPrjSubId)) > 0 Then
        If Mid(txtPrjSubId, Len(txtPrjSubId), 1) = "'" Then
            txtPrjSubId = Mid(txtPrjSubId, 1, Len(txtPrjSubId) - 1)
        End If
    End If
    Call Filtrar
End Sub

Private Sub txtPrjSubsId_Change()
    If Len(Trim(txtPrjSubsId)) > 0 Then
        If Mid(txtPrjSubsId, Len(txtPrjSubsId), 1) = "'" Then
            txtPrjSubsId = Mid(txtPrjSubsId, 1, Len(txtPrjSubsId) - 1)
        End If
    End If
    Call Filtrar
End Sub

Private Sub txtPrjNom_Change()
    If Len(Trim(txtPrjNom)) > 0 Then
        If Mid(txtPrjNom, Len(txtPrjNom), 1) = "'" Then
            txtPrjNom = Mid(txtPrjNom, 1, Len(txtPrjNom) - 1)
        End If
    End If
    Call Filtrar
End Sub

Private Sub txtPrjId_GotFocus()
    With txtPrjId
        .SelStart = 0: .SelLength = Len(.Text)
    End With
End Sub

Private Sub txtPrjSubId_GotFocus()
    With txtPrjSubId
        .SelStart = 0: .SelLength = Len(.Text)
    End With
End Sub

Private Sub txtPrjSubsId_GotFocus()
    With txtPrjSubsId
        .SelStart = 0: .SelLength = Len(.Text)
    End With
End Sub

Private Sub txtPrjNom_GotFocus()
    With txtPrjNom
        .SelStart = 0: .SelLength = Len(.Text)
    End With
End Sub

Private Sub Filtrar()
    Dim cFilters As String
   
    ' Inicializa los filtros para abarcar todos los items
    aplRST.Filter = adFilterNone
    
    ' Master Project
    If Len(txtPrjId) > 0 Then
        cFilters = "ProjId = " & Trim(txtPrjId)
    End If
    ' Proyecto
    If Len(Trim(txtPrjSubId)) > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = Trim(cFilters) & " AND ProjSubId = " & Trim(txtPrjSubId)
        Else
            cFilters = "ProjSubId = " & Trim(txtPrjSubId)
        End If
    End If
    ' Subproyecto
    If Len(Trim(txtPrjSubsId)) > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = Trim(cFilters) & " AND ProjSubSId = " & Trim(txtPrjSubsId)
        Else
            cFilters = "ProjSubSId = " & Trim(txtPrjSubsId)
        End If
    End If
    ' Nombre o descripci�n del proyecto
    If Len(Trim(txtPrjNom)) > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = Trim(cFilters) & " AND ProjNom LIKE '*" & Trim(txtPrjNom) & "*'"
        Else
            cFilters = "ProjNom LIKE '*" & Trim(txtPrjNom) & "*'"
        End If
    End If
    ' Si existe alg�n criterio, realiza el filtro...
    If Len(cFilters) > 0 Then
        aplRST.Filter = cFilters
    End If
    Call CargarArbol
End Sub

Private Sub trwProyectos_DblClick()
    Call SeleccionarProyecto
End Sub

Private Sub trwProyectos_NodeClick(ByVal node As MSComctlLib.node)
    Call AsignarProyectoSeleccionado(node)
End Sub

Private Sub trwProyectos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call SeleccionarProyecto
    End If
End Sub

Private Sub SeleccionarProyecto()
    If sp_GetProyectoIDM(lProjId, lProjSubId, lProjSubSId, Null, Null, Null, Null, Null) Then
        flgSeleccionado = True
        Unload Me
    End If
End Sub

Private Sub AsignarProyectoSeleccionado(ByVal node As MSComctlLib.node)
    Dim inicio As Integer, largo As Integer
    Dim proyecto As String
    Dim i As Integer
    
    If node.Root.Selected = True Then Exit Sub
    proyecto = node.Text
    
    For i = 1 To 4
        inicio = 1
        Select Case i
            Case 1
                largo = InStr(inicio, proyecto, ".", vbTextCompare) - 1
                lProjId = CInt(Mid(proyecto, inicio, largo))
                proyecto = Mid(proyecto, largo + 2)
            Case 2
                largo = InStr(inicio, proyecto, ".", vbTextCompare) - 1
                lProjSubId = CInt(Mid(proyecto, inicio, largo))
                proyecto = Mid(proyecto, largo + 2)
            Case 3
                largo = InStr(inicio, proyecto, "-", vbTextCompare) - 1
                lProjSubSId = CInt(Mid(proyecto, inicio, largo))
                proyecto = Mid(proyecto, largo + 2)
            Case 4
                cProjNom = ClearNull(proyecto)
        End Select
    Next i
End Sub

Private Sub chkAgrupamiento_Click()
    flgCargados = False
    Call CargarArbol
End Sub
