VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPruebaFramesXP 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   3795
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7515
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3795
   ScaleWidth      =   7515
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   3615
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   3615
      Begin VB.TextBox USER_PASSWORD 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   1455
         PasswordChar    =   "�"
         TabIndex        =   4
         Top             =   1155
         Width           =   1485
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Acept&ar"
         Default         =   -1  'True
         Height          =   375
         Left            =   1695
         TabIndex        =   3
         Top             =   1920
         Width           =   885
      End
      Begin VB.CommandButton cmdContinuar 
         Caption         =   "C&ontinuar"
         Height          =   375
         Left            =   2655
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   1920
         Width           =   885
      End
      Begin AT_MaskText.MaskText USER_ID 
         Height          =   300
         Left            =   1455
         TabIndex        =   5
         Top             =   795
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
      End
      Begin VB.Label lblIdentificacion 
         AutoSize        =   -1  'True
         Caption         =   "&Usuario"
         Height          =   195
         Left            =   375
         TabIndex        =   8
         Top             =   840
         Width           =   540
      End
      Begin VB.Label lblContrasenia 
         AutoSize        =   -1  'True
         Caption         =   "Co&ntrase�a"
         Height          =   195
         Left            =   375
         TabIndex        =   7
         Top             =   1200
         Width           =   840
      End
      Begin VB.Label lblAplicacion 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Ingreso al sistema"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   165
         Left            =   240
         TabIndex        =   6
         Top             =   480
         Width           =   1155
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Frame2"
      Height          =   3615
      Left            =   3840
      TabIndex        =   0
      Top             =   120
      Width           =   3615
   End
End
Attribute VB_Name = "frmPruebaFramesXP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

