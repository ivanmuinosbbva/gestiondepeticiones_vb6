VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmAsignaNumero 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3465
   ClientLeft      =   795
   ClientTop       =   2025
   ClientWidth     =   6450
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3465
   ScaleWidth      =   6450
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      Height          =   2085
      Left            =   510
      TabIndex        =   3
      Top             =   390
      Width           =   5475
      Begin AT_MaskText.MaskText txtUltNro 
         Height          =   315
         Left            =   2220
         TabIndex        =   6
         Top             =   390
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Ultimo n�mero asignado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   330
         TabIndex        =   5
         Top             =   450
         Width           =   1695
      End
   End
   Begin VB.Frame fraOpciones 
      Height          =   945
      Left            =   1980
      TabIndex        =   0
      Top             =   2490
      Width           =   2535
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   555
         Left            =   1320
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Height          =   555
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Asignaci�n de N�mero de Petici�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   285
      Left            =   240
      TabIndex        =   4
      Top             =   90
      Width           =   6075
   End
End
Attribute VB_Name = "frmAsignaNumero"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Dim sNrointerno As String
Dim nUltimoAsignado As Long
Dim nNumeroAsignado As Long
Dim sCod_estado_orig As String
Dim flgComite As Boolean

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdOK_Click()
    txtUltNro = sp_ProximoNumero("ULPETASI")
    If Val(txtUltNro) = 0 Then
        MsgBox ("Error en numero")
        Exit Sub
    End If
    If sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, txtUltNro) Then
       'Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "ASIGNR", Null, Null)
       'Call sp_DelHistorial(glNumeroPeticion)
       If flgComite = True Then
            Call sp_UpdatePetField(glNumeroPeticion, "FECHACOM", Null, Format(date, "dd/mm/yyyy"), Null)
       End If
       Call sp_AddHistorial(glNumeroPeticion, "PASINRO", sCod_estado_orig, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Asignaci�n de N�mero de Petici�n: " & txtUltNro & " �")
       touchForms
       Unload Me
    Else
        MsgBox ("Error al asignar numero")
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    flgComite = False
    
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            sNrointerno = IIf(Not IsNull(aplRST!pet_nrointerno), aplRST!pet_nrointerno, "")
            sCod_estado_orig = ClearNull(aplRST!cod_estado)
            If IsNull(aplRST!fe_comite) Then
                flgComite = True
            End If
            aplRST.Close
        Else
            aplRST.Close
        End If
    End If
    If sp_GetVarios("ULPETASI") Then
        If Not aplRST.EOF Then
            nUltimoAsignado = IIf(Not IsNull(aplRST!var_numero), aplRST!var_numero, 0)
            aplRST.Close
        Else
            aplRST.Close
        End If
    End If
    txtUltNro = nUltimoAsignado
End Sub

