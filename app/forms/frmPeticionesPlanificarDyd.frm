VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesPlanificarDyd 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Planificaci�n - DYD"
   ClientHeight    =   7755
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10140
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7755
   ScaleWidth      =   10140
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton optAccion 
      Caption         =   "Replanificar"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   31
      Top             =   4080
      UseMaskColor    =   -1  'True
      Width           =   1335
   End
   Begin VB.OptionButton optAccion 
      Caption         =   "Seguimiento"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   30
      Top             =   2760
      Value           =   -1  'True
      Width           =   1335
   End
   Begin VB.Frame fraComentarios 
      Caption         =   " Comentarios "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   120
      TabIndex        =   25
      Top             =   6120
      Width           =   8655
      Begin VB.TextBox txtComentarios 
         Height          =   1215
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   26
         Top             =   240
         Width           =   8415
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   7695
      Left            =   8880
      TabIndex        =   7
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   60
         TabIndex        =   27
         Tag             =   "Guard&ar *"
         Top             =   860
         Width           =   1095
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "Guard&ar"
         Height          =   375
         Left            =   60
         TabIndex        =   6
         Tag             =   "Guard&ar *"
         Top             =   500
         Width           =   1095
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   375
         Left            =   60
         TabIndex        =   24
         Tag             =   "Guard&ar *"
         Top             =   140
         Width           =   1095
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "C&errar"
         Height          =   375
         Left            =   60
         TabIndex        =   18
         Top             =   7260
         Width           =   1095
      End
   End
   Begin VB.Frame fraPlanificacion 
      Caption         =   " Planificaci�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   120
      TabIndex        =   8
      Top             =   0
      Width           =   8655
      Begin AT_MaskText.MaskText txtHsPlanificadasPeriodo 
         Height          =   315
         Left            =   3480
         TabIndex        =   33
         Top             =   2160
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         MaxLength       =   12
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   12
         Align           =   1
         DataType        =   2
      End
      Begin VB.ComboBox cmbEstadoPlanificacion 
         Height          =   300
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   720
         Width           =   5055
      End
      Begin VB.ComboBox cmbMotivoNoPlanificacion 
         Height          =   300
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1080
         Width           =   5055
      End
      Begin AT_MaskText.MaskText txtFechaFinAlcance 
         Height          =   315
         Left            =   3480
         TabIndex        =   35
         Top             =   1440
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaFinPruebas 
         Height          =   315
         Left            =   3480
         TabIndex        =   36
         Top             =   1800
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaPlaniInicial 
         Height          =   315
         Left            =   7050
         TabIndex        =   42
         Top             =   1440
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUltimaPlanificacion 
         Height          =   315
         Left            =   7050
         TabIndex        =   44
         Top             =   1800
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "�ltima planificaci�n"
         Height          =   180
         Index           =   15
         Left            =   5040
         TabIndex        =   43
         Top             =   1860
         Width           =   1485
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Fecha planificaci�n inicial"
         Height          =   180
         Index           =   6
         Left            =   5040
         TabIndex        =   41
         Top             =   1500
         Width           =   1935
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Fin de definici�n de alcance"
         Height          =   180
         Index           =   3
         Left            =   120
         TabIndex        =   40
         Top             =   1507
         Width           =   2070
      End
      Begin VB.Label lblControlHorasCargadas 
         AutoSize        =   -1  'True
         Caption         =   "Las horas cargadas superan las de la petici�n"
         ForeColor       =   &H00000080&
         Height          =   180
         Left            =   4800
         TabIndex        =   29
         Top             =   2227
         Visible         =   0   'False
         Width           =   3435
      End
      Begin VB.Image imgWarning 
         Height          =   240
         Left            =   4440
         Picture         =   "frmPeticionesPlanificarDyd.frx":0000
         Top             =   2197
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Origen de planificaci�n"
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   427
         Width           =   1725
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Estado de planificaci�n"
         Height          =   180
         Index           =   1
         Left            =   120
         TabIndex        =   13
         Top             =   780
         Width           =   1740
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Motivo de No planificaci�n"
         Height          =   180
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   1140
         Width           =   1980
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Fin de pruebas funcionales"
         Height          =   180
         Index           =   4
         Left            =   120
         TabIndex        =   11
         Top             =   1867
         Width           =   1995
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Hs. planificadas para el presente per�odo"
         Height          =   180
         Index           =   5
         Left            =   120
         TabIndex        =   10
         Top             =   2227
         Width           =   3060
      End
      Begin VB.Label lblOrigenPlanificacion 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   3480
         TabIndex        =   9
         Top             =   360
         Width           =   5055
      End
   End
   Begin VB.Frame fraSeguimiento 
      Caption         =   " Seguimiento "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      TabIndex        =   15
      Top             =   2760
      Width           =   8655
      Begin VB.ComboBox cmbMotivosSusp 
         Height          =   300
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   720
         Width           =   5055
      End
      Begin VB.ComboBox cmbEstadosDesarrollo 
         Height          =   300
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   360
         Width           =   5055
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Motivos de susp. / Desplan. / Canc. / Rech."
         Height          =   180
         Index           =   8
         Left            =   120
         TabIndex        =   17
         Top             =   780
         Width           =   3240
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Estado actual del desarrollo"
         Height          =   180
         Index           =   7
         Left            =   120
         TabIndex        =   16
         Top             =   420
         Width           =   2100
      End
   End
   Begin VB.Frame fraReplanificar 
      Caption         =   " Replanificaci�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   120
      TabIndex        =   19
      Top             =   4080
      Width           =   8655
      Begin VB.TextBox txtReplanificaciones 
         Alignment       =   1  'Right Justify
         Height          =   315
         Left            =   7920
         TabIndex        =   5
         Top             =   1440
         Width           =   495
      End
      Begin VB.ComboBox cmbMotivosReplan 
         Height          =   300
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   360
         Width           =   5055
      End
      Begin AT_MaskText.MaskText txtHorasReplan 
         Height          =   315
         Left            =   3480
         TabIndex        =   34
         Top             =   1440
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         MaxLength       =   12
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   12
         Align           =   1
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtFchOriginal 
         Height          =   315
         Left            =   3480
         TabIndex        =   37
         Top             =   720
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNuevaFecha 
         Height          =   315
         Left            =   3480
         TabIndex        =   38
         Top             =   1080
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFchUltReplan 
         Height          =   315
         Left            =   6960
         TabIndex        =   39
         Top             =   1080
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Fch. Ult. Replanif."
         Height          =   180
         Index           =   14
         Left            =   5520
         TabIndex        =   32
         Top             =   1147
         Width           =   1320
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Replanificaciones"
         Height          =   180
         Index           =   13
         Left            =   5520
         TabIndex        =   28
         Top             =   1507
         Width           =   1320
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Horas replanificadas"
         Height          =   180
         Index           =   12
         Left            =   120
         TabIndex        =   23
         Top             =   1507
         Width           =   1545
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Nueva fecha"
         Height          =   180
         Index           =   11
         Left            =   120
         TabIndex        =   22
         Top             =   1147
         Width           =   930
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "�ltima fecha establecida"
         Height          =   180
         Index           =   10
         Left            =   120
         TabIndex        =   21
         Top             =   787
         Width           =   1860
      End
      Begin VB.Label lblPlanificarDyd 
         AutoSize        =   -1  'True
         Caption         =   "Motivo de replanificaci�n"
         Height          =   180
         Index           =   9
         Left            =   120
         TabIndex        =   20
         Top             =   420
         Width           =   1875
      End
   End
End
Attribute VB_Name = "frmPeticionesPlanificarDyd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 15.11.2010 - Planificaci�n DYD (2� etapa): nueva funcionalidad para planificar peticiones de OPTimizaci�n
'                           sin tener que pasar por el BP. Solo administra DYD pero BP puede ver.
' -002- a. FJS 19.11.2010 - Si se pretende replanificar y nunca se habia planificado (es decir, no exist�a la fecha original de
'                           planificaci�n) se asume como fecha de planificaci�n original la que esta cargando el usuario en fecha
'                           de replanificaci�n.
' -002- b. FJS 19.11.2010 - Bug: se cambia la variable glLOGIN_Grupo por cGrupo, como corresponde.
' -003- a. FJS 14.12.2010 - Mejora: si existen modificaciones, entonces se actualizan los datos de la grilla sin hacer el refresh (que resulta tortuoso de cara al usuario, lo cual es l�gico)
' -004- a. FJS 04.01.2011 - Arreglo: cuando se planifica peticiones OPTI, se cambia el estado en [PeticionPlandet].
' -005- a. FJS 11.03.2011 - Modificaci�n: si es una petici�n de la planificaci�n original (formal), y estamos en etapa de seguimiento, y la petici�n a�n est� sin planificar, entonces permitimos asignar
'                           la planificaci�n inicial como si fuera una agregada (a pedido de Jota).
' -006- a. FJS 11.03.2011 - Modificaci�n: agregado de Fecha de Planificaci�n Inicial (la primera) y fecha de ultimo cambio en planificaci�n.

Option Explicit

Public cModoTrabajo As String
Public bModificado As Boolean       ' add -003- a.

Private Const EMPTY_STRING = ""

Dim lPeriodoActual As Long
Dim EstadoPeriodoActual As String
Dim fec_ini_per As Date
Dim fec_fin_per As Date

Dim bLoading As Boolean             ' Variable auxiliar para indicar cuando se esta realizando la carga incial del form, para evitar disparar eventos de manera innecesaria.
Dim bAgregadaPorDyD As Boolean      ' add -001- a.
Dim plan_ori As Long                ' Or�gen: planificaci�n inicial o agregada
Dim cGrupo As String
Dim cSector As String

Private Sub Form_Load()
    Dim plan_estadoini As Long      ' Estado actual de planificaci�n
    bModificado = False             ' add -003- a.
    
    ' Formateos
    With txtHsPlanificadasPeriodo
        .AutoSelect = False
        .DecimalPlaces = 0
        .EmptyValue = True
        .NegativeValues = False
    End With
    With txtHorasReplan
        .AutoSelect = False
        .DecimalPlaces = 0
        .EmptyValue = True
        .NegativeValues = False
    End With
    
    bLoading = True
    bAgregadaPorDyD = False         ' add -001- a.
    imgWarning.visible = False
    lblOrigenPlanificacion.Caption = ""
    lblControlHorasCargadas.visible = False
    Cargar_PeriodoActual
    Select Case EstadoPeriodoActual
        Case "PLAN10", "PLAN20", "PLAN30"
            cModoTrabajo = "INICIAL"
            Me.Caption = "Pet. n� " & glNroAsignado & " - Planificaci�n: inicial"
        Case "PLAN36"   ' Seguimiento
            If optAccion(0).value Then
                cModoTrabajo = "SEGUIMIENTO"
            Else
                cModoTrabajo = "REPLANIFICACION"
            End If
            Me.Caption = "Petici�n n� " & glNroAsignado & " - Planificaci�n: Seguimiento"
    End Select
    
    Select Case glUsrPerfilActual
        Case "CGRU"     ' L�der
            ' Obtengo el sector y el grupo del l�der
            cGrupo = ClearNull(frmPeticionesPlanificar.grdDatos.TextMatrix(frmPeticionesPlanificar.grdDatos.rowSel, 2))    ' Antes 38
            If sp_GetGrupo(cGrupo, Null, Null) Then
                cSector = ClearNull(aplRST.Fields!cod_sector)
            End If
        Case "PRJ1"
            ' Obtengo el sector y el grupo del l�der
            cGrupo = ClearNull(frmPeticionesPlanificar.grdDatos.TextMatrix(frmPeticionesPlanificar.grdDatos.rowSel, 25))
            If sp_GetGrupo(cGrupo, Null, Null) Then
                cSector = ClearNull(aplRST.Fields!cod_sector)
            End If
    End Select
    
    ' Si la petici�n est� en etapa de Seguimiento, es Agregada y se encuentra sin planificar, entonces se habilita
    ' por �nica vez el recuadro de planificaci�n inicial.
    If sp_GetPeticionPlandet(lPeriodoActual, glNumeroPeticion, cGrupo) Then
        plan_ori = IIf(ClearNull(aplRST.Fields!plan_ori) = "", 0, aplRST.Fields!plan_ori)
        plan_estadoini = IIf(ClearNull(aplRST.Fields!plan_estadoini) = "", 0, aplRST.Fields!plan_estadoini)
        'If plan_ori = 2 And plan_estadoini = 1 Then                        ' del -005- a.
        'If (plan_ori = 1 Or plan_ori = 2) And plan_estadoini = 1 Then       ' add -005- a.
        If (plan_ori = 1 Or plan_ori = 2) And plan_estadoini < 2 Then       ' add -005- a.
            cModoTrabajo = "INICIAL"
            Me.Caption = "Petici�n n� " & glNroAsignado & " - Planificaci�n: inicial " & IIf(plan_ori = 2, "(por agregada)", "")
        End If
    '{ add -001- a. Si no existe, es una planificaci�n agregada x DYD
    Else
        bAgregadaPorDyD = True
        plan_estadoini = 0
        Select Case EstadoPeriodoActual
        Case "PLAN10", "PLAN20", "PLAN30"
            plan_ori = 3
        Case "PLAN36"               ' Etapa: Seguimiento
            plan_ori = 4
        End Select
        cModoTrabajo = "INICIAL"
        Me.Caption = "Petici�n n� " & glNroAsignado & " - Planificaci�n: inicial (agregada por DYD)"
    '}
    End If
    
    Select Case cModoTrabajo
        Case "INICIAL"
            HabilitarBotones 0
            CargarCombo_EstadoPlanificacion
            CargarCombo_MotivoNoPlanificacion
            CargarCombo_EstadosDesarrollo
            CargarCombo_MotivosSusp
            CargarCombo_MotivosReplan
            fraPlanificacion.Enabled = True
            fraSeguimiento.Enabled = False
            fraReplanificar.Enabled = False
            Cargar_Inicial
        Case "SEGUIMIENTO"
            HabilitarBotones 0
            CargarCombo_EstadoPlanificacion
            CargarCombo_MotivoNoPlanificacion
            CargarCombo_EstadosDesarrollo
            CargarCombo_MotivosSusp
            CargarCombo_MotivosReplan
            fraPlanificacion.Enabled = False
            fraSeguimiento.Enabled = True
            fraReplanificar.Enabled = False
            Cargar_Inicial
        Case "REPLANIFICACION"
            HabilitarBotones 0
            CargarCombo_EstadoPlanificacion
            CargarCombo_MotivoNoPlanificacion
            CargarCombo_EstadosDesarrollo
            CargarCombo_MotivosSusp
            CargarCombo_MotivosReplan
            fraPlanificacion.Enabled = False
            fraSeguimiento.Enabled = False
            fraReplanificar.Enabled = True
            Cargar_Inicial
    End Select
    bLoading = False
End Sub

Private Sub Cargar_Inicial()
    bLoading = True
    If sp_GetPeticionPlandet(lPeriodoActual, glNumeroPeticion, cGrupo) Then
        lblOrigenPlanificacion.Caption = ClearNull(aplRST.Fields!nom_stage)
        cmbEstadoPlanificacion.ListIndex = IIf(aplRST.Fields!plan_estadoini > 0, PosicionCombo(cmbEstadoPlanificacion, ClearNull(aplRST.Fields!plan_estadoini), True), -1)
        cmbMotivoNoPlanificacion.ListIndex = IIf(aplRST.Fields!plan_motnoplan > 0, PosicionCombo(cmbMotivoNoPlanificacion, ClearNull(aplRST.Fields!plan_motnoplan), True), -1)
        txtFechaFinAlcance = ClearNull(aplRST.Fields!plan_fealcadef)
        txtFechaFinPruebas = ClearNull(aplRST.Fields!plan_fefunctst)
        txtHsPlanificadasPeriodo = ClearNull(aplRST.Fields!plan_horasper)
        'cmbMotivosReplan.ListIndex = IIf(aplRST.Fields!plan_motivrepl > 0, PosicionCombo(cmbMotivosReplan, aplRST.Fields!plan_motivrepl, True), -1)
        If ClearNull(aplRST.Fields!plan_feplannue) = "" Then
            txtFchOriginal.DateValue = ClearNull(aplRST.Fields!plan_fefunctst)
        Else
            txtFchOriginal.DateValue = ClearNull(aplRST.Fields!plan_feplannue)
        End If
        txtFchOriginal = txtFchOriginal.DateValue
        txtReplanificaciones = IIf(ClearNull(aplRST.Fields!plan_cantreplan) = "", "", ClearNull(aplRST.Fields!plan_cantreplan))
        cmbEstadosDesarrollo.ListIndex = IIf(aplRST.Fields!plan_desaestado > 0, PosicionCombo(cmbEstadosDesarrollo, aplRST.Fields!plan_desaestado, True), -1)
        cmbMotivosSusp.ListIndex = IIf(aplRST.Fields!plan_motivsusp > 0, PosicionCombo(cmbMotivosSusp, aplRST.Fields!plan_motivsusp, True), -1)
        cmbMotivosReplan.ListIndex = IIf(aplRST.Fields!plan_motivrepl > 0, PosicionCombo(cmbMotivosReplan, aplRST.Fields!plan_motivrepl, True), -1)
        txtHorasReplan = IIf(ClearNull(aplRST.Fields!plan_horasreplan) = "", "", ClearNull(aplRST.Fields!plan_horasreplan))
        txtNuevaFecha.DateValue = IIf(ClearNull(aplRST.Fields!plan_feplannue) = "", "", aplRST.Fields!plan_feplannue)
        txtNuevaFecha.text = txtNuevaFecha.DateValue
        txtFchUltReplan.DateValue = IIf(ClearNull(aplRST.Fields!plan_fchreplan) = "", "", aplRST.Fields!plan_fchreplan)
        txtFchUltReplan.text = txtFchUltReplan.DateValue
        '{ add -006- a.
        txtFechaPlaniInicial = ClearNull(aplRST.Fields!plan_fchestadoini): txtFechaPlaniInicial.Tag = txtFechaPlaniInicial.text
        txtUltimaPlanificacion = ClearNull(aplRST.Fields!plan_act2fch)
        '}
        txtComentarios.text = sp_GetPlanMemo(lPeriodoActual, glNumeroPeticion, cGrupo, "PLANIF")
    End If
    If sp_GetPeticionGrupo(glNumeroPeticion, cSector, cGrupo) Then
        If CLng(ClearNull(aplRST.Fields!horaspresup)) > 0 Then
            If Val(txtHsPlanificadasPeriodo) > CLng(ClearNull(aplRST.Fields!horaspresup)) Then
                imgWarning.visible = True
                lblControlHorasCargadas.visible = True
            Else
                imgWarning.visible = False
                lblControlHorasCargadas.visible = False
            End If
        End If
    End If
    bLoading = False
End Sub

Private Sub cmdModificar_Click()
    HabilitarBotones 1
End Sub

Private Sub cmdCancelar_Click()
    HabilitarBotones 0
    Cargar_Inicial
End Sub

Private Sub HabilitarBotones(nOpcion As Integer)
    Dim i As Integer
    
    Select Case nOpcion
        Case 0
            HabilitarEtiquetas "TODOS"
            optAccion(0).value = True
            optAccion(0).Enabled = False
            optAccion(1).Enabled = False
            Call setHabilCtrl(cmbEstadoPlanificacion, "DIS")
            Call setHabilCtrl(cmbMotivoNoPlanificacion, "DIS")
            Call setHabilCtrl(txtFechaFinAlcance, "DIS")
            Call setHabilCtrl(txtFechaFinPruebas, "DIS")
            Call setHabilCtrl(txtHsPlanificadasPeriodo, "DIS")
            Call setHabilCtrl(cmbEstadosDesarrollo, "DIS")
            Call setHabilCtrl(cmbMotivosSusp, "DIS")
            Call setHabilCtrl(cmbMotivosReplan, "DIS")
            Call setHabilCtrl(txtFchOriginal, "DIS")
            Call setHabilCtrl(txtNuevaFecha, "DIS")
            Call setHabilCtrl(txtHorasReplan, "DIS")
            Call setHabilCtrl(txtReplanificaciones, "DIS")
            Call setHabilCtrl(txtFchUltReplan, "DIS")
            Call setHabilCtrl(txtComentarios, "DIS")
            Call setHabilCtrl(cmdModificar, "NOR")
            Call setHabilCtrl(cmdGuardar, "DIS")
            Call setHabilCtrl(cmdCancelar, "DIS")
            Call setHabilCtrl(cmdCerrar, "NOR")
            '{ add -006- a.
            Call setHabilCtrl(txtFechaPlaniInicial, "DIS")
            Call setHabilCtrl(txtUltimaPlanificacion, "DIS")
            '}
        Case 1
            Select Case cModoTrabajo
                Case "INICIAL"
                    HabilitarEtiquetas cModoTrabajo
                    Call setHabilCtrl(cmdModificar, "DIS")
                    Call setHabilCtrl(cmdCerrar, "DIS")
                    Call setHabilCtrl(cmdGuardar, "NOR")
                    Call setHabilCtrl(cmdCancelar, "NOR")
                    Call setHabilCtrl(cmbEstadoPlanificacion, "NOR")
                    If CodigoCombo(cmbEstadoPlanificacion, True) = "2" Then Call setHabilCtrl(txtFechaFinPruebas, "NOR")
                    If CodigoCombo(cmbEstadoPlanificacion, True) = "2" Then Call setHabilCtrl(txtHsPlanificadasPeriodo, "NOR")
                    Call setHabilCtrl(txtComentarios, "NOR")
                    If cmbMotivoNoPlanificacion.ListIndex > -1 Then Call setHabilCtrl(cmbMotivoNoPlanificacion, "NOR")
                    Call setHabilCtrl(txtComentarios, "NOR")
                    If cmbEstadoPlanificacion.ListIndex > -1 Then
                        'If CodigoCombo(cmbEstadoPlanificacion, True) = "1" Then txtFechaPlaniInicial.DateValue = date: txtFechaPlaniInicial.Text = txtFechaPlaniInicial.DateValue   ' add -006- a.
                        If CodigoCombo(cmbEstadoPlanificacion, True) = "3" Then Call setHabilCtrl(txtFechaFinAlcance, "NOR")
                        If CodigoCombo(cmbEstadoPlanificacion, True) = "3" Then Call setHabilCtrl(txtFechaFinPruebas, "NOR")
                        If CodigoCombo(cmbEstadoPlanificacion, True) = "3" Then Call setHabilCtrl(txtHsPlanificadasPeriodo, "NOR")
                        If CodigoCombo(cmbEstadoPlanificacion, True) = "7" Then Call setHabilCtrl(txtFechaFinPruebas, "NOR")
                        If CodigoCombo(cmbEstadoPlanificacion, True) = "7" Then Call setHabilCtrl(txtHsPlanificadasPeriodo, "NOR")
                    End If
                Case "SEGUIMIENTO"
                    HabilitarEtiquetas cModoTrabajo
                    Call setHabilCtrl(cmbEstadosDesarrollo, "NOR")
                    Select Case CodigoCombo(cmbEstadosDesarrollo, True)
                        Case "7", "9", "11"
                            Call setHabilCtrl(cmbMotivosSusp, "NOR")
                    End Select
                    Call setHabilCtrl(cmdModificar, "DIS")
                    Call setHabilCtrl(cmdGuardar, "NOR")
                    Call setHabilCtrl(cmdCancelar, "NOR")
                    Call setHabilCtrl(txtComentarios, "NOR")
                    Call setHabilCtrl(cmdCerrar, "DIS")
                    optAccion(0).Enabled = True
                    optAccion(1).Enabled = True
                    optAccion(0).value = True
                Case "REPLANIFICACION"
                    HabilitarEtiquetas cModoTrabajo
                    Call setHabilCtrl(cmdModificar, "DIS")
                    Call setHabilCtrl(cmdGuardar, "NOR")
                    Call setHabilCtrl(cmdCancelar, "NOR")
                    Call setHabilCtrl(cmbMotivosReplan, "NOR")
                    Call setHabilCtrl(txtNuevaFecha, "NOR")
                    Call setHabilCtrl(txtHorasReplan, "NOR")
                    Call setHabilCtrl(txtComentarios, "NOR")
                    Call setHabilCtrl(cmdCerrar, "DIS")
            End Select
        Case 2
        
    End Select
End Sub

Private Sub Cargar_PeriodoActual()
    If sp_GetPeriodo(glNumeroPeriodo, Null) Then
        Do While Not aplRST.EOF
            'If ClearNull(aplRST.Fields!vigente) = "S" Then
                lPeriodoActual = ClearNull(aplRST.Fields!per_nrointerno)
                EstadoPeriodoActual = ClearNull(aplRST.Fields!per_estado)
                fec_ini_per = ClearNull(aplRST.Fields!fe_desde)
                fec_fin_per = ClearNull(aplRST.Fields!fe_hasta)
                Exit Do
            'End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
End Sub

Private Sub CargarCombo_EstadoPlanificacion()
    With cmbEstadoPlanificacion
        If sp_GetEstadosPlanificacion(Null, "S") Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_estado_plan & ". " & ClearNull(aplRST.Fields!nom_estado_plan) & ESPACIOS & ESPACIOS & "||" & aplRST.Fields!cod_estado_plan
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
End Sub

Private Sub cmbEstadoPlanificacion_Click()
    Dim CodigoEstado As Long
    
    If Not bLoading And CodigoCombo(cmbEstadoPlanificacion, True) <> "" Then
        CodigoEstado = CodigoCombo(cmbEstadoPlanificacion, True)
        
        Call setHabilCtrl(txtFechaFinPruebas, "DIS")
        Call setHabilCtrl(txtFechaFinAlcance, "DIS")
        Call setHabilCtrl(txtHsPlanificadasPeriodo, "DIS")
        Call setHabilCtrl(cmbMotivoNoPlanificacion, "DIS"): cmbMotivoNoPlanificacion.ListIndex = -1
        'txtFechaPlaniInicial.DateValue = CDate(txtFechaPlaniInicial.Tag): txtFechaPlaniInicial.Text = txtFechaPlaniInicial.DateValue
        
        Select Case CodigoEstado
            Case 1  ' Sin planificar
                txtFechaFinAlcance.DateValue = EMPTY_STRING: txtFechaFinAlcance.text = EMPTY_STRING
                txtFechaFinPruebas.DateValue = EMPTY_STRING: txtFechaFinPruebas.text = EMPTY_STRING
                txtHsPlanificadasPeriodo.text = EMPTY_STRING
                'txtFechaPlaniInicial.DateValue = date: txtFechaPlaniInicial.Text = txtFechaPlaniInicial.DateValue   ' add -006- a.
            Case 2  ' Planificada
                Call setHabilCtrl(txtFechaFinPruebas, "NOR")
                Call setHabilCtrl(txtHsPlanificadasPeriodo, "NOR")
                txtFechaFinAlcance.DateValue = EMPTY_STRING: txtFechaFinAlcance.text = EMPTY_STRING
            Case 3  ' Planificada condicionalmente
                Call setHabilCtrl(txtFechaFinAlcance, "NOR")
                Call setHabilCtrl(txtFechaFinPruebas, "NOR")
                Call setHabilCtrl(txtHsPlanificadasPeriodo, "NOR")
            Case 4  ' No se puede planificar
                Call setHabilCtrl(cmbMotivoNoPlanificacion, "NOR")
                txtFechaFinAlcance.DateValue = EMPTY_STRING: txtFechaFinAlcance.text = EMPTY_STRING
                txtFechaFinPruebas.DateValue = EMPTY_STRING: txtFechaFinPruebas.text = EMPTY_STRING
                txtHsPlanificadasPeriodo.text = EMPTY_STRING
            Case 5  ' Desplanificada
                txtFechaFinAlcance.DateValue = EMPTY_STRING: txtFechaFinAlcance.text = EMPTY_STRING
                txtFechaFinPruebas.DateValue = EMPTY_STRING: txtFechaFinPruebas.text = EMPTY_STRING
                txtHsPlanificadasPeriodo.text = EMPTY_STRING
            Case 6  ' Rechazada
                txtFechaFinAlcance.DateValue = EMPTY_STRING: txtFechaFinAlcance.text = EMPTY_STRING
                txtFechaFinPruebas.DateValue = EMPTY_STRING: txtFechaFinPruebas.text = EMPTY_STRING
                txtHsPlanificadasPeriodo.text = EMPTY_STRING
            Case 7  ' Finalizado
                txtFechaFinAlcance.DateValue = EMPTY_STRING: txtFechaFinAlcance.text = EMPTY_STRING
                txtFechaFinPruebas.DateValue = EMPTY_STRING: txtFechaFinPruebas.text = EMPTY_STRING
                Call setHabilCtrl(txtFechaFinPruebas, "NOR")
                Call setHabilCtrl(txtHsPlanificadasPeriodo, "NOR")
        End Select
    End If
End Sub

Private Sub cmbEstadosDesarrollo_Click()
    Dim CodigoEstado As String
    
    If Not bLoading Then
        CodigoEstado = CodigoCombo(cmbEstadosDesarrollo, True)
        Call setHabilCtrl(cmbMotivosSusp, "DIS")
        
        Select Case CodigoEstado
            Case "7", "9", "11"
                Call setHabilCtrl(cmbMotivosSusp, "NOR")
            Case Else
                Call setHabilCtrl(cmbMotivosSusp, "DIS")
                cmbMotivosSusp.ListIndex = -1
        End Select
    End If
End Sub

Private Sub CargarCombo_MotivoNoPlanificacion()
    With cmbMotivoNoPlanificacion
        If sp_GetMotivosNoPlan(Null, "S") Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_motivo_noplan & ". " & ClearNull(aplRST.Fields!nom_motivo_noplan) & Space(100) & "||" & aplRST.Fields!cod_motivo_noplan
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
End Sub

Private Sub cmbMotivoNoPlanificacion_Click()
    If Not bLoading Then
        Call setHabilCtrl(txtComentarios, "DIS")
        If CodigoCombo(cmbMotivoNoPlanificacion, True) = "2" Then
            Call setHabilCtrl(txtComentarios, "NOR")
        End If
    End If
End Sub

Private Sub CargarCombo_EstadosDesarrollo()
    With cmbEstadosDesarrollo
        If sp_GetEstadosDesarrollo(Null, "S") Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!cod_estado_desa) & ". " & ClearNull(aplRST.Fields!nom_estado_desa) & Space(100) & "||" & ClearNull(aplRST.Fields!cod_estado_desa)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
End Sub

Private Sub CargarCombo_MotivosSusp()
    With cmbMotivosSusp
        If sp_GetMotivosSuspen(Null, "S") Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!cod_motivo_suspen) & ". " & ClearNull(aplRST.Fields!nom_motivo_suspen) & Space(100) & "||" & ClearNull(aplRST.Fields!cod_motivo_suspen)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
End Sub

Private Sub CargarCombo_MotivosReplan()
    With cmbMotivosReplan
        If sp_GetMotivosReplan(Null, "S") Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!cod_motivo_replan) & ". " & ClearNull(aplRST.Fields!nom_motivo_replan) & Space(100) & "||" & ClearNull(aplRST.Fields!cod_motivo_replan)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
End Sub

Private Sub cmdGuardar_Click()
    '{ add -001- a.
    Dim cod_bpar As String
    Dim cod_estado As String
    Dim lOrden As Long
    '}
    
    If ValidarControles Then
        If MsgBox("�Confirma guardar los datos modificados?", vbQuestion + vbYesNo) = vbYes Then
            Call Puntero(True)
            Select Case cModoTrabajo
                Case "INICIAL"
                    '{ add -001- a.
                    If bAgregadaPorDyD Then
                        ' Obtengo el BP de la petici�n
                        If sp_GetUnaPeticion(glNumeroPeticion) Then
                            cod_bpar = ClearNull(aplRST.Fields!cod_bpar)
                        End If
                        If sp_GetPeticionGrupo(glNumeroPeticion, cSector, cGrupo) Then
                            cod_estado = ClearNull(aplRST.Fields!cod_estado)
                        End If
                        ' Obtengo el nro. de orden (el �ltimo +1)
                        lOrden = 0
                        If cod_estado = "EJECUC" Then
                            lOrden = 0
                        Else
                            If sp_GetPeticionPlandet(lPeriodoActual, Null, cGrupo) Then
                                Do While Not aplRST.EOF
                                    If IsNumeric(ClearNull(aplRST.Fields!plan_ordenfin)) Then
                                        If aplRST.Fields!plan_ordenfin > lOrden Then
                                            lOrden = aplRST.Fields!plan_ordenfin    ' Guarda siempre el mayor
                                        End If
                                    End If
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                                lOrden = lOrden + 1
                            End If
                        End If
                        Call sp_InsertPeticionPlancab(lPeriodoActual, glNumeroPeticion, 1, cod_bpar)
                        Call sp_InsertPeticionPlandet(lPeriodoActual, cGrupo, glNumeroPeticion, lOrden)
                        Call sp_UpdatePeticionPlandetField(lPeriodoActual, glNumeroPeticion, cGrupo, "PLAN_ORI", Null, Null, plan_ori)
                        Call sp_UpdatePeticionPlandetField(lPeriodoActual, glNumeroPeticion, cGrupo, "PLAN_ORDENFIN", Null, Null, lOrden)
                        Call sp_UpdatePeticionPlandetField(lPeriodoActual, glNumeroPeticion, cGrupo, "PLAN_STAGE", Null, Null, 1)
                        Call sp_UpdatePeticionPlandetField(lPeriodoActual, glNumeroPeticion, cGrupo, "PLAN_ESTADO", "APROBA", Null, Null)   ' add -004- a.
                    End If
                    '}
                    Call sp_UpdatePeticionPlandet(cModoTrabajo, lPeriodoActual, glNumeroPeticion, cGrupo, CodigoCombo(cmbEstadoPlanificacion, True), CodigoCombo(cmbMotivoNoPlanificacion, True), txtFechaFinAlcance, txtFechaFinPruebas, txtHsPlanificadasPeriodo, Null, Null, Null, Null, Null, Null, txtFechaPlaniInicial)   ' upd -006- a.
                    Call sp_UpdatePlanMemo(lPeriodoActual, glNumeroPeticion, cGrupo, "PLANIF", txtComentarios.text)
                    '{ add -006- a. Guarda la �ltima modificaci�n de estado de planificaci�n (siempre)
                    If ClearNull(txtFechaPlaniInicial.text) = "" Or EstadoPeriodoActual = "PLAN36" Then
                        'If CodigoCombo(cmbEstadoPlanificacion, True) <> "1" Then
                        Call sp_UpdatePeticionPlandetField(lPeriodoActual, glNumeroPeticion, cGrupo, "PLAN_FCHESTADOINI", Null, date, Null)
                        'End If
                    End If
                    Call sp_UpdatePeticionPlandetField(lPeriodoActual, glNumeroPeticion, cGrupo, "PLAN_ACT2FCH", Null, date, Null)
                    Call sp_UpdatePeticionPlandetField(lPeriodoActual, glNumeroPeticion, cGrupo, "PLAN_ACT2USR", glLOGIN_ID_REEMPLAZO, Null, Null)
                    '}
                Case "SEGUIMIENTO"
                    Call sp_UpdatePeticionPlandet(cModoTrabajo, lPeriodoActual, glNumeroPeticion, cGrupo, Null, Null, Null, Null, Null, CodigoCombo(cmbEstadosDesarrollo, True), CodigoCombo(cmbMotivosSusp, True), Null, Null, Null, Null, Null)  ' upd -002- b.   ' upd -006- a.
                    Call sp_UpdatePlanMemo(lPeriodoActual, glNumeroPeticion, cGrupo, "PLANIF", txtComentarios.text)
                Case "REPLANIFICACION"
                    Call sp_UpdatePeticionPlandet(cModoTrabajo, lPeriodoActual, glNumeroPeticion, cGrupo, Null, Null, Null, Null, Null, Null, Null, CodigoCombo(cmbMotivosReplan, True), txtFchOriginal, txtNuevaFecha, txtHorasReplan, Null)     ' upd -002- b.    ' upd -006- a.
                    Call sp_UpdatePlanMemo(lPeriodoActual, glNumeroPeticion, cGrupo, "PLANIF", txtComentarios.text)
            End Select
            bModificado = True  ' add -003- a.
            'cmdCerrar_Click    ' El error m�s est�pido del siglo XXI
            Call Puntero(False)
        End If
        Cargar_Inicial
        HabilitarBotones 0
    End If
End Sub

Private Function ValidarControles() As Boolean
    ValidarControles = False
    Select Case cModoTrabajo
        Case "INICIAL"
            'Call sp_UpdatePeticionPlandet(cModoTrabajo, lPeriodoActual, glNumeroPeticion, glLOGIN_Grupo, CodigoCombo(cmbEstadoPlanificacion, True), CodigoCombo(cmbMotivoNoPlanificacion, True), txtFechaFinAlcance, txtFechaFinPruebas, txtHsPlanificadasPeriodo, Null, Null, Null, Null, Null, Null)
            If cmbEstadoPlanificacion.ListIndex = -1 Then
                MsgBox "Debe seleccionar un estado de planificaci�n.", vbExclamation + vbOKOnly, "Error: estado de planificaci�n"
                Exit Function
            End If
            Select Case CodigoCombo(cmbEstadoPlanificacion, True)
                Case "2"      ' Estado de planificaci�n: 2. Planificada
                    If Not IsDate(txtFechaFinPruebas.DateValue) Then
                        MsgBox "Debe ingresar una fecha v�lida.", vbExclamation + vbOKOnly, "Error: fin de pruebas funcionales"
                        Exit Function
                    End If
                    ' La fecha no debe ser menor a la fecha de inicio del cuatrimestre actual
                    If txtFechaFinPruebas.DateValue < fec_ini_per Then
                        MsgBox "Debe ingresar una fecha mayor o igual a la fecha de inicio del per�odo actual.", vbExclamation + vbOKOnly, "Error: fin de pruebas funcionales"
                        Exit Function
                    End If
                    If Len(txtHsPlanificadasPeriodo) = 0 Then
                        MsgBox "Debe indicar la cantidad de horas planificadas.", vbExclamation + vbOKOnly, "Error: horas planificadas"
                        Exit Function
                    End If
                    If Not IsNumeric(txtHsPlanificadasPeriodo) Or Val(txtHsPlanificadasPeriodo) <= 0 Then
                        MsgBox "Debe indicar la cantidad de horas planificadas.", vbExclamation + vbOKOnly, "Error: horas planificadas"
                        Exit Function
                    End If
                    If sp_GetPeticionGrupo(glNumeroPeticion, cSector, cGrupo) Then
                        If CLng(ClearNull(aplRST.Fields!horaspresup)) > 0 Then
                            If Val(txtHsPlanificadasPeriodo) > CLng(ClearNull(aplRST.Fields!horaspresup)) Then
                                imgWarning.visible = True
                                lblControlHorasCargadas.visible = True
                                MsgBox "La cantidad ingresada supera las estipuladas en la petici�n (" & ClearNull(aplRST.Fields!horaspresup) & " hs.)", vbInformation + vbOKOnly, "Aviso: horas planificadas superiores a las cargadas en la pet."
                            End If
                        End If
                    End If
                Case "3"      ' Estado de planificaci�n: 3. Planificada condicionalmente
                    If Not IsDate(txtFechaFinAlcance.DateValue) Then
                        MsgBox "Debe ingresar una fecha v�lida.", vbExclamation + vbOKOnly, "Error: fin de definici�n de alcance"
                        Exit Function
                    End If
                    If txtFechaFinAlcance.DateValue < date Then
                        MsgBox "Debe ingresar una fecha mayor o igual a la fecha de hoy.", vbExclamation + vbOKOnly, "Error: fin de definici�n de alcance"
                        Exit Function
                    End If
                    ' La fecha no debe ser mayor a la fecha de fin del cuatrimestre actual
                    If txtFechaFinAlcance.DateValue > fec_fin_per Then
                        MsgBox "Debe ingresar una fecha menor o igual a la fecha de fin del per�odo actual.", vbExclamation + vbOKOnly, "Error: fin de definici�n de alcance"
                        Exit Function
                    End If
                    ' Fecha de fin de pruebas func.
                    If Len(txtFechaFinPruebas.text) > 0 Then
                        If Not IsDate(txtFechaFinPruebas.DateValue) Then
                            MsgBox "Debe ingresar una fecha v�lida.", vbExclamation + vbOKOnly, "Error: fin de pruebas funcionales"
                            Exit Function
                        End If
                    End If
                Case "4"      ' Estado de planificaci�n: 4. No se puede planificar
                    If cmbMotivoNoPlanificacion.ListIndex = -1 Then
                        MsgBox "Debe seleccionar un motivo v�lido por el cual no planifica.", vbExclamation + vbOKOnly, "Error: motivo de no planificaci�n"
                        Exit Function
                    End If
                    If cmbMotivoNoPlanificacion.ListIndex = 1 Then
                        If Len(txtComentarios) = 0 Then
                            MsgBox "Debe especificar comentarios por no planificar.", vbExclamation + vbOKOnly, "Error: detalles y/o comentarios por no planificar"
                            Exit Function
                        End If
                    End If
                Case "7"
                    ' Fecha de fin de pruebas func.
                    If Len(txtFechaFinPruebas.text) > 0 Then
                        If Not IsDate(txtFechaFinPruebas.DateValue) Then
                            MsgBox "Debe ingresar una fecha v�lida.", vbExclamation + vbOKOnly, "Error: fin de pruebas funcionales"
                            Exit Function
                        End If
                    End If
                    If ClearNull(txtHsPlanificadasPeriodo.text) <> "" Then
                        If Not IsNumeric(txtHsPlanificadasPeriodo) Or Val(txtHsPlanificadasPeriodo) <= 0 Then
                            MsgBox "Debe indicar la cantidad de horas planificadas.", vbExclamation + vbOKOnly, "Error: horas planificadas"
                            Exit Function
                        End If
                    End If
            End Select
        Case "SEGUIMIENTO"
            Select Case CodigoCombo(cmbEstadosDesarrollo, True)
                Case "7", "9", "11"
                    If cmbMotivosSusp.ListIndex = -1 Then
                        MsgBox "Debe especificar un motivo v�lido.", vbExclamation + vbOKOnly, "Error: falta especificar motivo"
                        Exit Function
                    End If
            End Select
        Case "REPLANIFICACION"
            If cmbMotivosReplan.ListIndex = -1 Then
                MsgBox "Debe especificar un motivo v�lido.", vbExclamation + vbOKOnly, "Error: falta especificar motivo de replanificaci�n"
                Exit Function
            End If
            If CodigoCombo(cmbMotivosReplan, True) = "4" Then
                If Len(txtComentarios) = 0 Then
                    MsgBox "Debe especificar comentarios para este tipo de replanificaci�n.", vbExclamation + vbOKOnly, "Error: falta especificar comentarios de replanificaci�n"
                    Exit Function
                End If
            End If
            If Not IsDate(txtNuevaFecha.text) Then
                MsgBox "Debe especificar una fecha v�lida.", vbExclamation + vbOKOnly, "Error: falta especificar la fecha de replanificaci�n"
                Exit Function
            End If
            If txtNuevaFecha.DateValue < fec_ini_per Then
                MsgBox "Debe ingresar una fecha mayor o igual a la fecha de inicio del per�odo actual.", vbExclamation + vbOKOnly, "Error: nueva fecha de planificaci�n"
                Exit Function
            End If
            '{ add -002- a.
            txtFchOriginal.text = txtNuevaFecha.text
            txtFchOriginal.DateValue = txtNuevaFecha.DateValue
            '}
            If Len(Trim(txtHorasReplan.text)) > 0 Then
                If Not IsNumeric(txtHorasReplan.text) Or Val(txtHorasReplan) < 0 Then
                    MsgBox "La cantidad de horas replanificadas debe ser un n�mero entero positivo.", vbExclamation + vbOKOnly, "Error: horas replanificadas"
                    Exit Function
                End If
            End If
            If sp_GetPeticionGrupo(glNumeroPeticion, cSector, cGrupo) Then
                If CLng(ClearNull(aplRST.Fields!horaspresup)) > 0 Then
                    If Val(txtHorasReplan) > CLng(ClearNull(aplRST.Fields!horaspresup)) Then
                        MsgBox "La cantidad ingresada supera las estipuladas en la petici�n (" & ClearNull(aplRST.Fields!horaspresup) & " hs.)", vbInformation + vbOKOnly, "Aviso: horas planificadas superiores a las cargadas en la pet."
                    End If
                End If
            End If
    End Select
    ValidarControles = True
End Function

Private Sub optAccion_Click(Index As Integer)
    Dim i As Integer
    
    Select Case Index
        Case 0  ' Seguimiento
            cModoTrabajo = "SEGUIMIENTO"
            fraSeguimiento.Enabled = True
            fraReplanificar.Enabled = False
            HabilitarEtiquetas cModoTrabajo
            Call setHabilCtrl(cmbEstadosDesarrollo, "NOR")
            Select Case CodigoCombo(cmbEstadosDesarrollo, True)
                Case "7", "9", "11"
                    Call setHabilCtrl(cmbMotivosSusp, "NOR")
            End Select
            'Call setHabilCtrl(cmbEstadosDesarrollo, "NOR")
            'Call setHabilCtrl(cmbMotivosSusp, "NOR")
            'cmbMotivosReplan.ListIndex = -1
            'txtHorasReplan.Text = ""
            Call setHabilCtrl(cmbMotivosReplan, "DIS")
            Call setHabilCtrl(txtFchOriginal, "DIS")
            Call setHabilCtrl(txtNuevaFecha, "DIS")
            Call setHabilCtrl(txtHorasReplan, "DIS")
        Case 1  ' Replanificar
            cModoTrabajo = "REPLANIFICACION"
            fraSeguimiento.Enabled = False
            fraReplanificar.Enabled = True
            HabilitarEtiquetas cModoTrabajo
            cmbEstadosDesarrollo.ListIndex = -1
            cmbMotivosSusp.ListIndex = -1
            Call setHabilCtrl(cmbEstadosDesarrollo, "DIS")
            Call setHabilCtrl(cmbMotivosSusp, "DIS")
            Call setHabilCtrl(cmbMotivosReplan, "NOR")
            Call setHabilCtrl(txtFchOriginal, "DIS")
            Call setHabilCtrl(txtNuevaFecha, "NOR"): txtNuevaFecha.DateValue = date: txtNuevaFecha.text = txtNuevaFecha.DateValue
            Call setHabilCtrl(txtHorasReplan, "NOR")
    End Select
End Sub

Private Sub HabilitarEtiquetas(cModo As String)
    On Error GoTo Errores
    Dim i As Integer
    Dim Ini, Fin As Integer

    For i = 0 To 14
        lblPlanificarDyd(i).Enabled = False
    Next i
    Select Case cModo
        Case "INICIAL"
            Ini = 0: Fin = 6
        Case "SEGUIMIENTO"
            Ini = 7: Fin = 8
        Case "REPLANIFICACION"
            Ini = 9: Fin = 14
        Case "TODOS"
            Ini = 0: Fin = 14
    End Select
    For i = Ini To Fin
        lblPlanificarDyd(i).Enabled = True
    Next i
Exit Sub
Errores:
    Select Case Err.Number
        Case 340    ' Control array element % doesn't exist
            i = i + 1
            Resume
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub
