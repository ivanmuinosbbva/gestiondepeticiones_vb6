VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "comct332.ocx"
Begin VB.MDIForm mdiPrincipal 
   AutoShowChildren=   0   'False
   BackColor       =   &H8000000C&
   Caption         =   "Administraci�n de Peticiones"
   ClientHeight    =   6735
   ClientLeft      =   1785
   ClientTop       =   1815
   ClientWidth     =   13140
   Icon            =   "mdiPrincipal.frx":0000
   LinkTopic       =   "MDIForm1"
   Picture         =   "mdiPrincipal.frx":058A
   ScrollBars      =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ImageList imgSession 
      Left            =   60
      Top             =   5160
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiPrincipal.frx":160B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiPrincipal.frx":1664A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiPrincipal.frx":16BE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiPrincipal.frx":1717E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiPrincipal.frx":17718
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiPrincipal.frx":17CB2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar cbSession 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   13140
      _ExtentX        =   23178
      _ExtentY        =   1111
      BandCount       =   1
      VariantHeight   =   0   'False
      _CBWidth        =   13140
      _CBHeight       =   630
      _Version        =   "6.7.9782"
      Child1          =   "tbSession"
      MinHeight1      =   570
      Width1          =   13080
      NewRow1         =   0   'False
      Begin MSComctlLib.Toolbar tbSession 
         Height          =   570
         Left            =   30
         TabIndex        =   2
         Top             =   30
         Width           =   13020
         _ExtentX        =   22966
         _ExtentY        =   1005
         ButtonWidth     =   1905
         ButtonHeight    =   1005
         Wrappable       =   0   'False
         Style           =   1
         ImageList       =   "imgSession"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Conectar"
               Description     =   "Conectar"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Desconectar"
               Description     =   "Desconectar"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Mensajes"
               Key             =   "btnMensajes"
               Description     =   "Bandeja de mensajes"
               ImageIndex      =   6
               Style           =   1
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Preferencias"
               Description     =   "Preferencias del usuario"
               ImageIndex      =   1
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.StatusBar sbPrincipal 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   6480
      Width           =   13140
      _ExtentX        =   23178
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Bevel           =   0
            Object.Width           =   10583
            MinWidth        =   10583
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   2293
            MinWidth        =   2293
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   660
      Top             =   5820
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      MaxFileSize     =   256
   End
   Begin GesPet.AdoConnection AdoConnection 
      Left            =   60
      Top             =   5820
      _ExtentX        =   873
      _ExtentY        =   873
      Servidor        =   "DSPROD02"
      BaseAplicacion  =   "GesPet"
      Version         =   "4.0.81"
   End
   Begin VB.Menu mnuConexion 
      Caption         =   "Conexi�n"
      Begin VB.Menu mnuLogin 
         Caption         =   "Login"
         Enabled         =   0   'False
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuLogout 
         Caption         =   "Logout"
         Shortcut        =   ^O
      End
      Begin VB.Menu separador021 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCambioContrase�a 
         Caption         =   "Cambio de contrase�a"
      End
      Begin VB.Menu separador000 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSalir 
         Caption         =   "Salir"
      End
   End
   Begin VB.Menu mnuPrc 
      Caption         =   "Perfiles"
      Enabled         =   0   'False
      Begin VB.Menu mnuSoli 
         Caption         =   "Solicitante"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuRefe 
         Caption         =   "Referente"
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSupe 
         Caption         =   "Supervisor"
      End
      Begin VB.Menu mnuDire 
         Caption         =   "Autorizante"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuAdmi 
         Caption         =   "Administrador"
      End
      Begin VB.Menu mnuSuperAdmi 
         Caption         =   "Superadministrador"
      End
      Begin VB.Menu separador101 
         Caption         =   "-"
      End
      Begin VB.Menu mnuBpar 
         Caption         =   "Referente de Sistema"
         Shortcut        =   ^B
      End
      Begin VB.Menu mnuRGP 
         Caption         =   "Referente de RGyP"
      End
      Begin VB.Menu separador101a 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHomo 
         Caption         =   "Informes de homologaci�n"
      End
      Begin VB.Menu separador102 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAnal 
         Caption         =   "Analista Ejecutor"
         Shortcut        =   ^W
      End
      Begin VB.Menu mnuCgru 
         Caption         =   "Resp. Ejecuci�n"
         Shortcut        =   ^E
      End
      Begin VB.Menu mnuCsec 
         Caption         =   "Resp. Sector"
      End
      Begin VB.Menu mnuCger 
         Caption         =   "Resp. Gerencia"
      End
      Begin VB.Menu mnuCdir 
         Caption         =   "Resp. Direcci�n"
      End
      Begin VB.Menu separador001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAgrupShell 
         Caption         =   "Agrupamientos"
      End
      Begin VB.Menu mnuVisMsg 
         Caption         =   "Ver mensajes"
      End
      Begin VB.Menu separador002 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRemmpRecu 
         Caption         =   "Reemplazar a..."
      End
      Begin VB.Menu mnuRecDelega 
         Caption         =   "Delegar funciones"
      End
   End
   Begin VB.Menu mnuOtros 
      Caption         =   "Proyectos"
      Enabled         =   0   'False
      Begin VB.Menu mnuOtros_ProyectosIDM 
         Caption         =   "Administrar Proyectos IDM"
      End
   End
   Begin VB.Menu mnuMsg 
      Caption         =   "Consultas"
      Enabled         =   0   'False
      Begin VB.Menu mnuViewPet 
         Caption         =   "Consulta general peticiones"
      End
      Begin VB.Menu mnuViewPetRec 
         Caption         =   "Consulta peticiones por recurso"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewPetUna 
         Caption         =   "Buscar una petici�n..."
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuViewSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewPlanificar 
         Caption         =   "Planificaci�n && priorizaci�n"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuViewSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRptMinuta 
         Caption         =   "Generaci�n minuta"
      End
      Begin VB.Menu mnuRptPetFin 
         Caption         =   "Consulta de peticiones finalizadas"
      End
      Begin VB.Menu mnuViewPetHis 
         Caption         =   "Consulta de peticiones hist�ricas"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuCatalogacion_Visualizar 
         Caption         =   "Peticiones v�lidas para pasaje a Producci�n"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuViewGestionDemanda 
         Caption         =   "Gesti�n de la demanda"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuDYD 
      Caption         =   "Sistemas"
      Enabled         =   0   'False
      Begin VB.Menu mnuDYD_host 
         Caption         =   "Solicitudes ambiente HOST"
         Begin VB.Menu mnuDYD_host_shell1 
            Caption         =   "Solicitudes de archivos desde Producci�n"
         End
         Begin VB.Menu mnuDYD_host_shell2 
            Caption         =   "Solicitudes de archivos desde Desarrollo"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuDYD_host_separator1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuDYD_host_autorizar 
            Caption         =   "Autorizaciones"
         End
      End
      Begin VB.Menu mnuDYD_ssdd 
         Caption         =   "Solicitudes ambiente SSDD"
         Begin VB.Menu mnuDYD_ssdd_shell1 
            Caption         =   "Solicitudes de tabla desde Producci�n"
         End
         Begin VB.Menu mnuDYD_ssdd_separator1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuDYD_ssdd_autorizar 
            Caption         =   "Autorizaciones"
         End
      End
      Begin VB.Menu mnuDYD_separator1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDYD_catg_host 
         Caption         =   "Cat�logo HOST"
      End
      Begin VB.Menu mnuDYD_catg_ssdd 
         Caption         =   "Cat�logo SSDD"
      End
   End
   Begin VB.Menu mnuHoras 
      Caption         =   "Horas trabajadas"
      Enabled         =   0   'False
      Begin VB.Menu mnuCargaHoras 
         Caption         =   "Carga de horas"
         Shortcut        =   ^H
      End
   End
   Begin VB.Menu mnuRpt 
      Caption         =   "Reportes"
      Enabled         =   0   'False
      Begin VB.Menu rptHsPetTar 
         Caption         =   "1. Horas trabajadas por Pet/Tar"
      End
      Begin VB.Menu rptHsPetAreaSoli 
         Caption         =   "2. Horas trabajadas por �rea Solicitante"
      End
      Begin VB.Menu rptHsPetAreaEjec 
         Caption         =   "3. Horas trabajadas por �rea Ejecutora"
      End
      Begin VB.Menu rptHsPetRecuEjec 
         Caption         =   "4. Porcentaje horas informadas"
      End
      Begin VB.Menu mnuRptEstruc 
         Caption         =   "5. Estructura recursos"
      End
      Begin VB.Menu mnuRptPerfArea 
         Caption         =   "6. Perfiles actuantes por �rea"
      End
      Begin VB.Menu mnuRpt_Sep001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRptPetFchProd 
         Caption         =   "7. Peticiones con fecha prev. pasaje a Prod. x Fecha"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuRpt_PetFchProdSec 
         Caption         =   "8. Peticiones con fecha prev. pasaje a Prod. x Sector"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuRpt_Sep002 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRpt_PetProyIDM1 
         Caption         =   "9. Peticiones asociadas a proyectos"
      End
      Begin VB.Menu rptHsTrabPetProyecto 
         Caption         =   "10. Horas trabajadas por proyecto"
      End
      Begin VB.Menu mnuRpt_Sep003 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRpt_Solicitudes 
         Caption         =   "Informes de solicitudes"
         Enabled         =   0   'False
         Begin VB.Menu mnuRpt_Solicitudes_EME 
            Caption         =   "Solicitudes por EMErgencia a fecha"
         End
      End
      Begin VB.Menu mnuRpt_Otros 
         Caption         =   "Otros informes"
         Begin VB.Menu mnuRpt_Otros1 
            Caption         =   "Peticiones Finalizadas con Total de Hs."
         End
         Begin VB.Menu mnuRpt_Varios2 
            Caption         =   "Informes especiales"
         End
         Begin VB.Menu mnuRpt_Varios3 
            Caption         =   "Listado de peticiones por EMErgencia (Sgi)"
         End
         Begin VB.Menu mnuRpt_Varios4 
            Caption         =   "Listado de OME/OMA por responsable"
         End
         Begin VB.Menu mnuRpt_Varios5 
            Caption         =   "Listado de horas por refinamiento"
         End
      End
   End
   Begin VB.Menu mnuVentana 
      Caption         =   "Ventana"
      WindowList      =   -1  'True
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "?"
      Enabled         =   0   'False
      Begin VB.Menu mnuAyuda_DocumentoAyuda 
         Caption         =   "Ayuda en l�nea"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuAyuda_Sugerencias 
         Caption         =   "Sugerencias"
      End
      Begin VB.Menu mnuAyuda_Novedades 
         Caption         =   "Novedades de la versi�n"
      End
      Begin VB.Menu sep_002 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAyudaVersion 
         Caption         =   "Versi�n"
      End
   End
End
Attribute VB_Name = "mdiPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 02.07.2007 - Se agrega una nueva opci�n al men� para generar la consulta de peticiones finalizadas
' -001- b. FJS 16.07.2007 - Se limpia del control StatusBar los datos del usuario que estaba logueado.
' -001- c. FJS 23.07.2007 - Se agrega al menu principal, para las funciones LogIn y LogOut los accesos r�pidos de teclado (Shortcuts)
' -001- d. FJS 24.07.2007 - Se agrega el nombre de pila y apellido del perfil actual en el control StatusBar.
' -001- e. FJS 25.07.2007 - Se agrega una pantalla que despliega las caracter�sticas t�cnicas de ADO para la versi�n actual (es informaci�n para el programador)
' -001- f. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 30.07.2007 - Se agrega el llamado a un archivo de ayuda.
' -002- b. FJS 08.08.2007 - Se modifica el alto (propiedad Height) del formulario MDI incrementandolo en 100 twips para que se muestren completamente los formularios Children del mismo.
' -002- c. FJS 09.08.2007 - Se agrega la versi�n de la aplicaci�n en el encabezado del formulario principal.
' -003- a. FJS 20.12.2007 - Trazabilidad.
' -004- a. FJS 24.04.2008 - Homologaci�n.
' -005- a. FJS 08.08.2008 - Se habilitan menues especiales para Catalogadores (para la gente de Calidad)
' -006- a. FJS 20.11.2008 - Se habilita la opci�n de men� para establecer las preferencias del usuario.
' -007- a. FJS 21.11.2008 - Se agrega un nuevo men� para ejecutar el aplicativo web ADAH (Homologaci�n).
' -007- b. FJS 30.03.2009 - Se agrega un nuevo par�metro luego del usuario real, para indicar el legajo del usuario reemplazo.
' -008- a. FJS 24.11.2008 - Se agrega la opci�n de visualizar los TIPS del sistema/versi�n.
' -009- a. FJS 26.11.2008 - Se agrega a la rutina de cambio de contrase�a, el logout autom�tico en caso de cambio realizado.
' -010- a. FJS 10.12.2008 - Se agrega un nuevo reporte para peticiones con fecha prevista de pasaje a Producci�n pero agrupado por Sector.
' -011- a. FJS 23.12.2008 - Se agrega el llamado a la rutina que hace el logout en el registro de ingresos de usuarios.
' -011- b. FJS 08.01.2009 - Se quita el llamado a la rutina que hace el logout (no funciona como era esperado. Hay que perfeccionarla.)
' -012- a. FJS 13.01.2009 - Se agrega (a�n deshabilitado) la rutina para autolockear la workstation transcurrido N tiempo (idle time) (observaci�n de auditor�a).
' -013- a. FJS 15.01.2009 - Se modifica la rutina para considerar el cambio de contrase�a obligatorio.
' -014- a. FJS 10.03.2009 - Se agrega cosm�tica para mostrar que el equipo esta trabajando al iniciar el logout.
' -015- a. FJS 18.03.2009 - Manejo de peticiones hist�ricas.
' -016- a. FJS 15.04.2009 - Se corrige para mostrar el t�tulo de acuerdo a la cantidad de d�as parametrizados.
' -017- a. FJS 16.04.2009 - Se agrega un nuevo perfil para Analista ejecutor
' -018- a. FJS 07.05.2009 - Se agrega nuevo ABM de Proyectos IDM para los usuarios Administradores.
' -019- a. FJS 12.05.2009 - Se agrega nuevo ABM de aplicativos.
' -020- a. FJS 05.06.2009 - Se agrega la especificaci�n de un rango de fechas para la solicitud del reporte.
' -021- a. FJS 22.06.2009 - Nuevo informe de Peticiones asociadas a Proyectos IDM (filtro por proyecto).
' -022- a. FJS 25.06.2009 - Se agrega una nueva opci�n de men�, bajo el esquema de perfiles, para cursar solicitudes a Carga de M�quinas.
' -023- a. FJS 02.10.2009 - Cat�logos de enmascaramiento.
' -024- a. FJS 18.01.2010 - Nuevo informe de solicitudes solicitadas a Carga de M�quina.
' -025- a. FJS 28.01.2010 - Se elimina del men� principal el acceso al link del aplicativo ADAH (se descontinu� el mismo).
' -026- a. FJS 22.07.2010 - Planificaci�n: Se agrega una opci�n de perfil para Oficina de Proyecto.
' -027- a. FJS 20.08.2010 - IGM: Se agrega una opci�n para definir y configurar eventos para el administrador de IGM.
' -028- a. AA  29.11.2010 - Se agrega validacion para acceder a diferentes pantallas dependiendo el tipo de usuario (revisado/corregido).
' -029- a. FJS 27.09.2011 - Nueva opci�n de men� para la administraci�n de proyectos IDM por parte de perfiles BP.
' -030- a. FJS 16.04.2015 - Nuevo: se agrega un nuevo item de men� para el nuevo perfil de Homologador.
' -031- a. FJS 24.11.2015 - Nuevo: se agrega una opci�n para reportes de nuestra �rea.
' -032- a. FJS 28.01.2016 - Nuevo: se agrega la funcionalidad para la nueva gerencia BPE.
' -033- a. FJS 23.08.2016 - Nuevo: se quita la restricci�n de tama�o de la aplicaci�n (se permite la maximizaci�n de la pantalla).
' -033- b. FJS 23.08.2016 - Nuevo: se habilita el coloreo de filas en la grilla para mejorar la visibilidad de los datos.
' -034- a. FJS 26.08.2016 - Nuevo: se comenta el llamado a esta funci�n para agilizar la carga de formularios (evitando su descarga de memoria).

Option Explicit

'Obtiene el Handle de una ventana a partir de una coordenada
Private Declare Function WindowFromPoint _
    Lib "user32" ( _
        ByVal xPoint As Long, _
        ByVal yPoint As Long) As Long
          
'Recupera la coordenada del cursor
Private Declare Function GetCursorPos _
    Lib "user32" ( _
        lpPoint As POINTAPI) As Long
  
'Recupera el nombre de la clase de Ventana  a partir de su handle
Private Declare Function GetClassName _
    Lib "user32" _
    Alias "GetClassNameA" ( _
        ByVal hWnd As Long, _
        ByVal lpClassName As String, _
        ByVal nMaxCount As Long) As Long
  
'REcupera el Handle de la ventana padre de una ventana
Private Declare Function GetParent _
    Lib "user32" ( _
        ByVal hWnd As Long) As Long
        
'Estas 2 funciones obtienen el Caption de la ventana
Private Declare Function GetWindowText _
    Lib "user32" _
    Alias "GetWindowTextA" ( _
        ByVal hWnd As Long, _
        ByVal lpString As String, _
        ByVal cch As Long) As Long
  
' Retorna la cantidad de caracteres del caption de la ventana
Private Declare Function GetWindowTextLength _
    Lib "user32" _
    Alias "GetWindowTextLengthA" ( _
        ByVal hWnd As Long) As Long

' Estructura POINTAPI para usar con WindowFromPoint
Private Type POINTAPI
  x As Long
  y As Long
End Type

Private Enum R
    RPT_HsTrabajadasPorPeticionTarea = 0
    RPT_HsTrabajadasAreaSolicitante = 1
    RPT_HsTrabajadasAreaEjecutante = 2
    RPT_PorcentajeHorasInformadas = 3
    RPT_EstructuraDeRecursos = 4
    RPT_PerfilesActuantesPorArea = 5
    RPT_PeticionesFasajeProdPorFecha = 6
    RPT_PeticionesFasajeProdPorSector = 7
    RPT_PeticionesPorProyectoIDM = 8
    RPT_SolicitudesPorEMErgencia = 9
    RPT_PeticionesFinalizadasConHs = 10
    RPT_HorasTrabajadasPorProyecto = 11
End Enum

Dim bFlagBarra As Boolean

Private Sub MDIForm_Load()
'    Dim ret As Long, handle As Long, hParent
'    Dim Cor As POINTAPI
'    Dim buffer As String
'    Dim ClassName As String
'    Dim Caption_Ventana As String
    
    'Call InitCommonControlsVB                  ' Habilitar esta l�nea para usar el tema del XP (Look & Feel)
    Me.Height = 9100                            ' upd -002- b.
    Me.Width = 12000

'    Me.Height = 10100
'    Me.Width = 14000
    
    Me.Caption = Trim(glHEADAPLICACION)
    Me.BackColor = RGB(58, 110, 165)
    
    bFlagBarra = False
    
    'cbSession.Bands(2).MinWidth = 1500
    'cbSession.Bands(1).MinWidth = Me.ScaleWidth - cbSession.Bands(2).MinWidth

'    'Obtiene la coordenada del Mouse
'    ret = GetCursorPos(Cor)
'    'Recuperamos el HWND de la ventana asociada a esa coordenada
'    handle = WindowFromPoint(Cor.x, Cor.y)
'    'Handle de la ventana padre
'    hParent = GetParent(handle)
'    'Llenamos un Buffer
'    ClassName = Space$(128)
'    'Recupera el Classname y lo devuelve en el Buffer
'    ret = GetClassName(handle, ClassName, 128)
'
'    'Extraemos el nombre de la clase
'    ClassName = LCase(Left$(ClassName, ret))
'
'    ' Cantidad de caracteres del texto
'    Caption_Ventana = String(GetWindowTextLength(handle), Chr$(0))
'    'Retorna el caption
'    Call GetWindowText(handle, Caption_Ventana, 100)
'
'    'Imprimimos en el Form con los valores
'
'
'    Debug.Print " Hwnd : " & handle
'    Debug.Print " Hwnd Parent : " & hParent
'    Debug.Print " Nombre de clase : " & ClassName
'    Debug.Print " Caption de la ventana : " & Caption_Ventana

    'Call AplicarTransparencia(Me.hWnd, 230)

End Sub

Private Sub MDIForm_Resize()
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            If Me.Height < 8445 Then Me.Height = 8445
            If Me.Width < 12425 Then Me.Width = 12425
        End If
    End If
End Sub

'{ add -026- a.
Private Sub mnuViewPlanificar_Click()
'    Call Puntero(True)
    'Call Status("Inicializando... aguarde...")
'    If InPerfil("PRJ1") Then
'        glUsrPerfilActual = "PRJ1"
'    ElseIf InPerfil("PRJ2") Then
'        glUsrPerfilActual = "PRJ2"
'    End If
    frmPeticionesPlanificar.Show
End Sub
'}

''{ add -026- a.
'Private Sub mnuOfiProy_Click()
'    'Call CerrarForms
'    Call Puntero(True)
'    Call Status("Inicializando... aguarde...")
'    If InPerfil("PRJ1") Then
'        glUsrPerfilActual = "PRJ1"
'    ElseIf InPerfil("PRJ2") Then
'        glUsrPerfilActual = "PRJ2"
'    End If
'    frmPeticionesPlanificar.Show 1
'End Sub
''}

Sub Login()
    frmLogin.Show vbModal
    Call Status("Aguarde un instante...")
    Unload frmLogin
    Call Status("Listo.")
End Sub

Sub Logout()
    With mdiPrincipal
        On Error Resume Next
        Call CerrarForms
        Call Status("Cerrando conexi�n...")
        .sbPrincipal.Panels(2) = ""     ' Usuario. Ej.: XA00309
        .sbPrincipal.Panels(3) = ""     ' Usuario (?)
        .sbPrincipal.Panels(4) = ""     ' ???
        .sbPrincipal.Panels(5) = ""     ' Nombre del usuario
        .sbPrincipal.Panels(6) = ""     ' Servidor de conexi�n
        .sbPrincipal.Panels(6).ToolTipText = .sbPrincipal.Panels(6).text
        .AdoConnection.CerrarConexion
        glONLINE = False
        Call HabilitarMenues(False)
        Call Status("Listo.")
    End With
End Sub

Private Sub AdoConnection_ADOError(Codigo As Long, MensajeError As String, Modulo As String)
    If Codigo = 0 Then
        glAdoError = ""
    Else
        glAdoError = Modulo & vbCrLf & MensajeError
    End If
End Sub

'{ del -033- a.
'Private Sub MDIForm_Resize()
'    If Me.WindowState = vbMaximized Then
'        Me.WindowState = vbNormal
'    End If
'End Sub
'}

Private Sub MDIForm_Unload(Cancel As Integer)
    Call Logout
End Sub

Private Sub mnuAgrupShell_Click()
    'Call CerrarForms               ' del -034- a.
    If glUsrPerfilActual <> "SADM" Then glUsrPerfilActual = "VIEW"
    'glUsrPerfilActual = "VIEW"
    frmAgrupShell.Show
End Sub

'{ add -002- a.
Private Sub mnuAyuda_Novedades_Click()
    On Error GoTo Errores
    'Load frmAcercaNovedades
    frmAcercaNovedades.Show     'vbModal
    Exit Sub
Errores:
    If Err.Number = 364 Then
        Exit Sub
    End If
End Sub
'}

Private Sub mnuAyuda_Sugerencias_Click()
    Load frmTip
    frmTip.Show vbModal
End Sub

Private Sub mnuAyudaVersion_Click()
    Dim cOSVersion As String
    'cOSVersion = getVersionNew()
    cOSVersion = getOSVersion()
    frmAcerca.Show vbModal
End Sub

Private Sub mnuLogin_Click()
    Call Login
End Sub

Private Sub mnuLogout_Click()
    On Error Resume Next
    Call Puntero(True)      ' add -014- a.
    Call CerrarForms
    Call Status("Cerrando conexi�n...")
    'Call AdoConnection.CerrarConexion
    With sbPrincipal
        .Panels(2).text = ""        ' Usuario
        .Panels(3).text = ""        ' Usuario (Reemplazo)
        .Panels(4).text = ""        ' Perfil
        .Panels(5).text = ""        ' Nombre del usuario
        .Panels(6).text = ""        ' Servidor
    End With
    'mdiPrincipal.cboSeleccionarPerfil.ListIndex = -1
    ' En un logout, inicializo la lista de perfiles
    Call HabilitarMenues(False)
    Call Status("Listo.")
    Call Puntero(False)     ' add -014- a.
End Sub

Private Sub mnuCambioContrase�a_Click()
    'Call CambioPassword
    MsgBox "Para cambiar la contrase�a gestione el BLANQUEO" & vbCrLf & "de la misma mediante el portal de Seg. Inf. (CRM).", vbInformation + vbOKOnly
End Sub

Private Sub mnuRpt_Otros1_Click()
    Call GenerarReportes(R.RPT_PeticionesFinalizadasConHs)
End Sub

Private Sub mnuRptMinuta_Click()
    'Call CerrarForms
    rptMinuta.Show vbModal
End Sub

Private Sub mnuRptPetFin_Click()
    'Call CerrarForms
    rptPetFin.Show vbModal
End Sub

Private Sub mnuSagrup_Click()
    'Call CerrarForms
    glUsrPerfilActual = "SADM"
    frmAgrupShell.Show
End Sub

Private Sub mnuSalir_Click()
    Call AdoConnection.CerrarConexion
    End
End Sub

Private Sub mnuRecDelega_Click()
    'Call CerrarForms
    glCodigoRecurso = glLOGIN_ID_REEMPLAZO
    auxRecDelegar.Show 'vbModal
End Sub

Private Sub mnuRemmpRecu_Click()
    'Call CerrarForms
    auxSelReemplazar.Show vbModal
End Sub

Private Sub mnuSoli_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "SOLI"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuRefe_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "REFE"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuDire_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "AUTO"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuSupe_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "SUPE"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuAdmi_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "ADMI"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuSuperAdmi_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "SADM"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuBpar_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "BPAR"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

'{ add -017- a.
Private Sub mnuAnal_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "ANAL"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub
'}

Private Sub mnuCgru_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "CGRU"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuCsec_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "CSEC"
'    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
'    mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuCdir_Click()
    'Call CerrarForms                       ' del -034- a.
    glUsrPerfilActual = "CDIR"
    'mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
    'mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuCger_Click()
    'Call CerrarForms                   ' del -034- a.
    glUsrPerfilActual = "CGCI"
    'mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
    'mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    'frmPeticionesShell.Show                ' del -034- a.
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub

Private Sub mnuCargaHoras_Click()
    'Call CerrarForms
    frmCargaHoras.Show
End Sub

Private Sub mnuViewPet_Click()
    'Call CerrarForms
    frmPeticionesShellView01.Show
End Sub

Private Sub mnuViewPetRec_Click()
    'Call CerrarForms                   ' del -034- a.
    frmPeticionesShellView03.Show
End Sub

Private Sub mnuVisMsg_Click()
    'Call CerrarForms                   ' del -034- a.
    frmPeticionesShellMsg.Show
End Sub

'{ add -005- a.
Private Sub mnuCatalogacion_Visualizar_Click()
    Load frmPeticionesValidas
    frmPeticionesValidas.Show vbModal
End Sub
'}

Sub CambioPassword()
    auxCambioPass.Show vbModal
    '{ add -013- a.
    If auxCambioPass.bolCambioObligatorio = True Then
        mnuLogout_Click
    Else
    '}
        '{ add -009- a.
        If auxCambioPass.bolPasswordChanged Then
            mnuLogout_Click
        End If
        '}
    End If  ' add -013- a.
End Sub

'{ add -013- b.
Public Sub Invisible_Logout()
    Call CerrarForms
    Call Status("Cerrando conexi�n...")
    mdiPrincipal.AdoConnection.CerrarConexion
    If Not mdiPrincipal.AdoConnection.IsOnLine Then
        With sbPrincipal
            .Panels(2).text = ""        ' Usuario
            .Panels(3).text = ""        ' Usuario (Reemplazo)
            .Panels(4).text = ""        ' Perfil
            .Panels(5).text = ""        ' Nombre del usuario
            .Panels(6).text = ""        ' Servidor
        End With
        Call HabilitarMenues(False)
        Call Status("Listo.")
    End If
End Sub
'}

'Private Sub mnuDYD_Catalogo_ABM_Click()
'    If InPerfil("ASEG") Then
'        glCatalogoENM = "ABM"
'        Load frmHEDT1ShellSI
'        frmHEDT1ShellSI.Show
'        'frmHEDT1ShellSI2.Show
'    Else
'        glCatalogoENM = "ABM"
'        'frmCLISTHelp.Show 1
'        Load frmHEDT1ShellDyD
'        frmHEDT1ShellDyD.Show
'    End If
'End Sub

'{ add -029- a.
Private Sub mnuOtros_ProyectosIDM_Click()
    frmProyectosIDMShell.Show              ' Versi�n NUEVA
End Sub
'}

Private Sub mnuViewPetUna_Click()
    Dim sNroPeticionAsig As String
    
    sNroPeticionAsig = InputBox("Ingrese el nro. de la petici�n a buscar:", "Buscar petici�n")
    If IsNumeric(sNroPeticionAsig) Then
        If Val(sNroPeticionAsig) <= 999999 Then
            Call Puntero(True)
            If sp_GetUnaPeticionAsig(sNroPeticionAsig) Then
                glNumeroPeticion = aplRST.Fields!pet_nrointerno
                If glNumeroPeticion <> "" Then
                    If Not LockProceso(True) Then
                        Exit Sub
                    End If
                    glModoPeticion = "VIEW"
                    Call Puntero(False)
                    frmPeticionesCarga.Show vbModal
                    DoEvents
                End If
            Else
                MsgBox "No existe la petici�n " & Trim(sNroPeticionAsig) & ". Revise.", vbExclamation + vbOKOnly
            End If
        Else
            MsgBox "El nro. de petici�n a buscar debe ser menor o igual a 999999. Revise.", vbExclamation + vbOKOnly
        End If
    Else
        If sNroPeticionAsig <> "" Then MsgBox "Nro. de petici�n incorrecto o inv�lido.", vbExclamation + vbOKOnly
    End If
    Call Puntero(False)
End Sub

' *** ESTE FALTA DEFINIRLO ***
'Private Sub mnuRpt_Otros1_Click()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String
'    Dim fe_desde As Date
'    Dim fe_hasta As Date
'
'    fe_desde = DateAdd("m", -1, Now)
'    fe_hasta = Now
'
'    Load auxFechaDesdeHasta
'    auxFechaDesdeHasta.Show 1
'
'    If auxFechaDesdeHasta.bAceptar Then
'        fe_desde = IIf(auxFechaDesdeHasta.txtFechaDesde = "", fe_desde, auxFechaDesdeHasta.txtFechaDesde)
'        fe_hasta = IIf(auxFechaDesdeHasta.txtFechaHasta = "", fe_hasta, auxFechaDesdeHasta.txtFechaHasta)
'
'        With mdiPrincipal.CrystalReport1
'            cPathFileNameReport = App.Path & "\" & "hspetterm01.rpt"
'            cTitulo = "Peticiones en estado terminal desde " & Format(fe_desde, "dd/mm/yyyy") & " y " & Format(fe_hasta, "dd/mm/yyyy")
'            .ReportFileName = GetShortName(cPathFileNameReport)
'            .RetrieveStoredProcParams
'            .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'            .SelectionFormula = ""
'            .StoredProcParam(0) = IIf(IsNull(fe_desde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(fe_desde, "yyyymmdd"))
'            .StoredProcParam(1) = IIf(IsNull(fe_hasta), Format(date, "yyyymmdd"), Format(fe_hasta, "yyyymmdd"))
'            .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'            .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'            .Formulas(2) = "@0_TITULO='" & cTitulo & "'"
'            .WindowLeft = 1
'            .WindowTop = 1
'            .WindowState = crptMaximized
'            .Action = 1
'            Call Puntero(False)
'            Call Status("Listo.")
'        End With
'    End If
'End Sub

Private Sub rptHsPetTar_Click()
    Call GenerarReportes(R.RPT_HsTrabajadasPorPeticionTarea)
End Sub

Private Sub rptHsPetAreaSoli_Click()
    Call GenerarReportes(R.RPT_HsTrabajadasAreaSolicitante)
End Sub

Private Sub rptHsPetAreaEjec_Click()
    Call GenerarReportes(R.RPT_HsTrabajadasAreaEjecutante)
End Sub

Private Sub rptHsPetRecuEjec_Click()
    Call GenerarReportes(RPT_PorcentajeHorasInformadas)
End Sub

Private Sub mnuRptEstruc_Click()
    Call GenerarReportes(R.RPT_EstructuraDeRecursos)
End Sub

Private Sub mnuRptPerfArea_Click()
    Call GenerarReportes(R.RPT_PerfilesActuantesPorArea)
End Sub

Private Sub mnuRptPetFchProd_Click()
    Call GenerarReportes(R.RPT_PeticionesFasajeProdPorFecha)
End Sub

Private Sub mnuRpt_PetFchProdSec_Click()
    Call GenerarReportes(R.RPT_PeticionesFasajeProdPorSector)
End Sub

Private Sub mnuRpt_PetProyIDM1_Click()
    Call GenerarReportes(R.RPT_PeticionesPorProyectoIDM)
End Sub

Private Sub mnuRpt_Solicitudes_EME_Click()
    Call GenerarReportes(R.RPT_SolicitudesPorEMErgencia)
End Sub

Private Sub GenerarReportes(iReporte As R)
    'Call CerrarForms
    Select Case iReporte
        Case 0     ' RPT_HsTrabajadasPorPeticionTarea = 0
            rptPetiTare.Show vbModal
        Case 1     ' RPT_HsTrabajadasAreaSolicitante = 1
            rptPetAreaSoli.Show vbModal
        Case 2     ' RPT_HsTrabajadasAreaEjecutante = 2
            rptPetAreaEjec.Show vbModal
        Case 3     ' RPT_PorcentajeHorasInformadas = 3
            rptPetRecHs.Show vbModal
        Case 4     ' RPT_EstructuraDeRecursos = 4
            rptEstruc.Show vbModal
        Case 5     ' RPT_PerfilesActuantesPorArea = 5
            rptPerfArea.Show vbModal
        Case 6     ' RPT_PeticionesFasajeProdPorFecha = 6
            Call RptPetFchProd
        Case 7     ' RPT_PeticionesFasajeProdPorSector = 7
            Call RptPetFchProdSec
        Case 8     ' RPT_PeticionesPorProyectoIDM = 8
            rptPetiProyIDM1.Show vbModal
        Case 9     ' RPT_SolicitudesPorEMErgencia = 9
            rptSoliEME.Show vbModal
        Case 10
            'Call RptPeticionesFinalizadasConHs
        Case RPT_HorasTrabajadasPorProyecto
            rptPetiProyecto.Show vbModal
    End Select
End Sub

'Private Sub RptPeticionesFinalizadasConHs()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String
'    Dim fe_desde As Date
'    Dim fe_hasta As Date
'
'    fe_desde = DateAdd("m", -1, Now)
'    fe_hasta = Now
'
'    Load auxFechaDesdeHasta
'    auxFechaDesdeHasta.Show 1
'
'    If auxFechaDesdeHasta.bAceptar Then
'        fe_desde = IIf(auxFechaDesdeHasta.txtFechaDesde = "", fe_desde, auxFechaDesdeHasta.txtFechaDesde)
'        fe_hasta = IIf(auxFechaDesdeHasta.txtFechaHasta = "", fe_hasta, auxFechaDesdeHasta.txtFechaHasta)
'
'        'glCrystalNewVersion = True
'
'        If glCrystalNewVersion Then     ' Nueva versi�n
'            Call NuevoReporte_Otros1(fe_desde, fe_hasta)
'        Else
'            With mdiPrincipal.CrystalReport1
'                cPathFileNameReport = App.Path & "\" & "hspetterm01.rpt"
'                cTitulo = "Peticiones en estado terminal desde " & Format(fe_desde, "dd/mm/yyyy") & " y " & Format(fe_hasta, "dd/mm/yyyy")
'                .ReportFileName = GetShortName(cPathFileNameReport)
'                .RetrieveStoredProcParams
'                .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'                .SelectionFormula = ""
'                .StoredProcParam(0) = IIf(IsNull(fe_desde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(fe_desde, "yyyymmdd"))
'                .StoredProcParam(1) = IIf(IsNull(fe_hasta), Format(date, "yyyymmdd"), Format(fe_hasta, "yyyymmdd"))
'                .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'                .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'                .Formulas(2) = "@0_TITULO='" & cTitulo & "'"
'                .WindowLeft = 1
'                .WindowTop = 1
'                .WindowState = crptMaximized
'                .Action = 1
'                Call Puntero(False)
'                Call Status("Listo.")
'            End With
'        End If
'    End If
'End Sub

Private Sub NuevoReporte_Otros1(fe_desde As Date, fe_hasta As Date)
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    
    sReportName = "\hspetterm016.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Horas acumuladas para DyD y BP de peticiones en estado terminal"
        .ReportComments = "Peticiones en estado terminal desde " & Format(fe_desde, "dd/mm/yyyy") & " y " & Format(fe_hasta, "dd/mm/yyyy")
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fe_desde": crParamDef.AddCurrentValue (IIf(IsNull(fe_desde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(fe_desde, "yyyymmdd")))
            Case "@fe_hasta": crParamDef.AddCurrentValue (IIf(IsNull(fe_hasta), Format(date, "yyyymmdd"), Format(fe_hasta, "yyyymmdd")))
        End Select
    Next
 
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

''{ add -012- a.
'Private Declare Function GetTickCount Lib "kernel32" () As Long
'Private Declare Function GetLastInputInfo Lib "user32" (plii As Any) As Long
'Private Declare Function LockWorkStation Lib "user32.dll" () As Long
'
'Private Type LASTINPUTINFO
'    cbSize As Long
'    dwTime As Long
'End Type
''}
''{ add -012- a.
'Private Sub Timer_Timer()
'    Dim lii As LASTINPUTINFO
'
'    lii.cbSize = Len(lii)
'    Call GetLastInputInfo(lii)
'    With sbPrincipal.Panels(1)
'        .Text = Format((GetTickCount() - lii.dwTime) / 1000, "00")
'        sbPrincipal.Refresh
'        If .Text = "10" Then
'            'MsgBox "hi"
'            Inhabilitar
'        End If
'    End With
'End Sub
'
'Private Sub Inhabilitar()
'    Dim ret As Long
'    ret = Shell("rundll32.exe user32.dll LockWorkStation")
'End Sub
''}

'{ add -004- a.
Private Sub RptPetFchProd()

End Sub

'Private Sub RptPetFchProd()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String           ' add -016- a.
'    '{ add -020- a.
'    Dim dFecha_Desde As Date
'    Dim dFecha_Hasta As Date
'    Dim iCantidad_Dias As Integer
'    '}
'
'    Call Status("Preparando el informe...")
'
'    '{ add -016- a.
'    If sp_GetVarios("RPT8") Then
'        iCantidad_Dias = aplRST.Fields!var_numero
'        If aplRST.Fields!var_numero > 1 Then
'            cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n dentro de los pr�ximos " & Trim(aplRST.Fields!var_numero) & " d�as"
'        ElseIf aplRST.Fields!var_numero = 1 Then
'            cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n para ma�ana"
'        Else
'            cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n para hoy"
'        End If
'    Else
'        cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n dentro de los pr�ximos quince d�as"
'    End If
'    '}
'    '{ add -020- a.
'    auxFechaDesdeHasta.txtFechaDesde = date
'    auxFechaDesdeHasta.txtFechaHasta = DateAdd("d", 6, date)
'    auxFechaDesdeHasta.Show 1
'    If Not auxFechaDesdeHasta.bAceptar Then
'        Call Status("Listo.")
'        Exit Sub
'    Else
'        dFecha_Desde = auxFechaDesdeHasta.txtFechaDesde
'        dFecha_Hasta = auxFechaDesdeHasta.txtFechaHasta
'    End If
'    If Not (IsDate(dFecha_Desde) And IsDate(dFecha_Hasta)) Then
'        dFecha_Desde = date
'        dFecha_Hasta = DateAdd("d", IIf(iCantidad_Dias > 0, iCantidad_Dias, 6), dFecha_Desde)
'    End If
'    cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n entre el " & Format(dFecha_Desde, "dd/mm/yyyy") & " y " & Format(dFecha_Hasta, "dd/mm/yyyy")
'    '}
'
'    Call Puntero(True)
'    With mdiPrincipal.CrystalReport1
'        cPathFileNameReport = App.Path & "\" & "petpubs.rpt"
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .SelectionFormula = ""      ' add -020- a.
'        .RetrieveStoredProcParams
'        '{ add -020- a.
'        .StoredProcParam(0) = IIf(IsNull(dFecha_Desde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(dFecha_Desde, "yyyymmdd"))
'        .StoredProcParam(1) = IIf(IsNull(dFecha_Hasta), Format(date, "yyyymmdd"), Format(dFecha_Hasta, "yyyymmdd"))
'        '}
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & cTitulo & "'"        ' upd -016- a.
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Call Puntero(False)
'        Call Status("Listo.")
'    End With
'End Sub
''}

'{ add -010- a.
Private Sub RptPetFchProdSec()

End Sub

'Private Sub RptPetFchProdSec()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String   ' add -016- a.
'    '{ add -020- a.
'    Dim dFecha_Desde As Date
'    Dim dFecha_Hasta As Date
'    Dim iCantidad_Dias As Integer
'    '}
'
'    Call Status("Preparando el informe...")
'
'    '{ add -016- a.
'    If sp_GetVarios("RPT8") Then
'        If aplRST.Fields!var_numero > 1 Then
'            cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n dentro de los pr�ximos " & Trim(aplRST.Fields!var_numero) & " d�as"
'        ElseIf aplRST.Fields!var_numero = 1 Then
'            cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n para ma�ana"
'        Else
'            cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n para hoy"
'        End If
'    Else
'        cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n dentro de los pr�ximos quince d�as"
'    End If
'    '}
'
'    '{ add -020- a.
'    auxFechaDesdeHasta.txtFechaDesde = date
'    auxFechaDesdeHasta.txtFechaHasta = DateAdd("d", 6, date)
'    auxFechaDesdeHasta.Show 1
'    If Not auxFechaDesdeHasta.bAceptar Then
'        Call Status("Listo.")
'        Exit Sub
'    Else
'        dFecha_Desde = auxFechaDesdeHasta.txtFechaDesde
'        dFecha_Hasta = auxFechaDesdeHasta.txtFechaHasta
'    End If
'    If Not (IsDate(dFecha_Desde) And IsDate(dFecha_Hasta)) Then
'        dFecha_Desde = date
'        dFecha_Hasta = DateAdd("d", IIf(iCantidad_Dias > 0, iCantidad_Dias, 6), dFecha_Desde)
'    End If
'    cTitulo = "Peticiones con fecha prevista de pasaje a Producci�n entre el " & Format(dFecha_Desde, "dd/mm/yyyy") & " y " & Format(dFecha_Hasta, "dd/mm/yyyy")
'    '}
'
'    Call Puntero(True)
'    With mdiPrincipal.CrystalReport1
'        cPathFileNameReport = App.Path & "\" & "petpubssec.rpt"
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .RetrieveStoredProcParams
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        '{ add -020- a.
'        .StoredProcParam(0) = IIf(IsNull(dFecha_Desde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(dFecha_Desde, "yyyymmdd"))
'        .StoredProcParam(1) = IIf(IsNull(dFecha_Hasta), Format(date, "yyyymmdd"), Format(dFecha_Hasta, "yyyymmdd"))
'        '}
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & cTitulo & "'"        ' upd -016- a.
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Call Puntero(False)
'        Call Status("Listo.")
'    End With
'End Sub
''}

Private Sub rptHsTrabPetProyecto_Click()
    Call GenerarReportes(RPT_HorasTrabajadasPorProyecto)
End Sub

'{ add -030- a.
Private Sub mnuHomo_Click()
    'Call CerrarForms
    'Call Puntero(True)
    'Call Status("Inicializando... aguarde...")
    If InPerfil("HOMO") Then
        glUsrPerfilActual = "HOMO"
    End If
    frmPeticionesShellHomo.Show
End Sub
'}

'{ add -031- a.
Private Sub mnuRpt_Varios2_Click()
    rptPetiIrregulares.Show ' vbModal
End Sub
'}

'{ add -032- a.
Private Sub mnuRGP_Click()
    'Call CerrarForms
    glUsrPerfilActual = "GBPE"
    'mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
    'mdiPrincipal.sbPrincipal.Panels(5) = Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "     ' add -001- d.
    frmPeticionesShell.Show
    Call CargarBandejaDeTrabajo             ' add -034- a.
End Sub
'}

' ******************************************************************
' SOLICITUDES DE ARCHIVOS ENMASCARADOS
' ******************************************************************
Private Sub mnuDYD_host_shell1_Click()
    'Call CerrarForms
    frmCLISTShell.Show
End Sub

Private Sub mnuDYD_host_autorizar_Click()
    'Call CerrarForms
    frmCLISTAutor.Show
End Sub

Private Sub mnuDYD_catg_host_Click()
    If InPerfil("ASEG") Then
        glCatalogoENM = "ABM"
        Load frmHEDT1ShellSI
        frmHEDT1ShellSI.Show
    Else
        glCatalogoENM = "ABM"
        Load frmHEDT1ShellDyD
        frmHEDT1ShellDyD.Show
    End If
End Sub

Private Sub mnuDYD_ssdd_shell1_Click()
    ' ABM para SSDD
    Load frmSSDDSolicitud
    frmSSDDSolicitud.Show
End Sub

Private Sub mnuDYD_ssdd_autorizar_Click()
    ' Autorizaciones para SSDD
End Sub

Private Sub mnuDYD_catg_ssdd_Click()
    ' Acceso al cat�logo SSDD
    Load frmSSDDCatalogoShell
    frmSSDDCatalogoShell.Show
End Sub

Private Sub mnuViewGestionDemanda_Click()
    Load frmPeticionesShellView05
    frmPeticionesShellView05.Show
End Sub

'{ add -034- a.
Private Sub CargarBandejaDeTrabajo()
    mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
    mdiPrincipal.sbPrincipal.Panels(5) = glLOGIN_NAME_REEMPLAZO      'Trim(sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)) & " "
    If glUsrPerfilActual <> frmPeticionesShell.formPerfilActual Then
        Unload frmPeticionesShell
        frmPeticionesShell.Show
    Else
        frmPeticionesShell.Show
    End If
End Sub
'}
Private Sub tbSession_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnMensajes"
            'Load frmPeticionesShellMsg2            ' Nueva bandeja de mensajes
            frmPeticionesShellMsg2.Show
    End Select
End Sub

' Listado de peticiones dadas de alta desde SGI por EMErgencias
Private Sub mnuRpt_Varios3_Click()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    ' Auxiliares para los titulos y subtitulos
    Dim cTitulo As String
    Dim cSubTitulo As String
    Dim fe_desde As Date
    Dim fe_hasta As Date

    Load auxFechaDesdeHasta
    auxFechaDesdeHasta.Show 1
    If Not auxFechaDesdeHasta.bAceptar Then
        Exit Sub
    End If

    fe_desde = auxFechaDesdeHasta.txtFechaDesde
    fe_hasta = auxFechaDesdeHasta.txtFechaHasta
    
    cTitulo = "Informe de peticiones por Emergencia (SGI)"
    cSubTitulo = "Desde: " & fe_desde & " hasta " & fe_hasta
    sReportName = "petporeme6.rpt"

    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = cTitulo
        .ReportComments = cTitulo & vbCrLf & cSubTitulo
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fe_desde": crParamDef.AddCurrentValue fe_desde   '(Format(fe_desde, "yyyymmdd"))
            Case "@fe_hasta": crParamDef.AddCurrentValue fe_hasta   '(Format(fe_hasta, "yyyymmdd"))
        End Select
    Next
 
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub mnuRpt_Varios4_Click()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    ' Auxiliares para los titulos y subtitulos
    Dim cTitulo As String
    Dim cSubTitulo As String

    cTitulo = "Informe de OMEs y OMAs en peticiones"
    cSubTitulo = ""
    
    sReportName = "infohomo16.rpt"

    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = cTitulo
        .ReportComments = cTitulo & IIf(Len(Trim(cSubTitulo)) > 0, vbCrLf & cSubTitulo, "")
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)

    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub mnuRpt_Varios5_Click()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    ' Auxiliares para los titulos y subtitulos
    Dim cTitulo As String
    Dim cSubTitulo As String
    Dim fe_desde As Date
    Dim fe_hasta As Date

    Load auxFechaDesdeHasta
    auxFechaDesdeHasta.Show 1
    If Not auxFechaDesdeHasta.bAceptar Then
        Exit Sub
    End If

    fe_desde = auxFechaDesdeHasta.txtFechaDesde
    fe_hasta = auxFechaDesdeHasta.txtFechaHasta

    cTitulo = "Informe de horas de refinamiento"
    cSubTitulo = ""
    
    sReportName = "rptG1.rpt"

    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = cTitulo
        .ReportComments = cTitulo & IIf(Len(Trim(cSubTitulo)) > 0, vbCrLf & cSubTitulo, "")
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fe_desde": crParamDef.AddCurrentValue Format(fe_desde, "yyyyMMdd hh:mm:ss")
            Case "@fe_hasta": crParamDef.AddCurrentValue Format(fe_hasta, "yyyyMMdd hh:mm:ss")
        End Select
    Next

    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub
