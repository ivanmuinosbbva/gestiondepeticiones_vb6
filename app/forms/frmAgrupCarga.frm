VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAgrupCarga 
   Caption         =   "Datos del agrupamiento"
   ClientHeight    =   4350
   ClientLeft      =   645
   ClientTop       =   345
   ClientWidth     =   8415
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAgrupCarga.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   290
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   561
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab sstAgrupamientos 
      Height          =   4215
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   7435
      _Version        =   393216
      Style           =   1
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos"
      TabPicture(0)   =   "frmAgrupCarga.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label9"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label8"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label3"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label7"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label27"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label5"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label4"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblAyuda"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "txtNomUsualta"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "txtNomPadre"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtNroPadre"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "txtUsualta"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtTitulo"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "cboArea"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "cboAdmPet"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "cboEstado"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "cboActualiza"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "cboVisibilidad"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "cmdModif"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "cmdBorrar"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "cmdOk"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "cmdCancel"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).ControlCount=   23
      TabCaption(1)   =   "Sub Agrup."
      TabPicture(1)   =   "frmAgrupCarga.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdCerrar(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cmdAgregarAgr"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "cmdQuitarAgr"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lswDatos"
      Tab(1).Control(4)=   "grdHijAgru"
      Tab(1).Control(5)=   "imgAgrupamientos"
      Tab(1).Control(6)=   "Label2"
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "Peticiones"
      TabPicture(2)   =   "frmAgrupCarga.frx":05C2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdImportarPeticiones"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "cmdCerrar(0)"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "cmdAgregarPet"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "grdHijPeti"
      Tab(2).Control(4)=   "cmdQuitarPet"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "Label6"
      Tab(2).ControlCount=   6
      Begin VB.CommandButton cmdImportarPeticiones 
         Caption         =   "Importar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   -67740
         TabIndex        =   35
         TabStop         =   0   'False
         ToolTipText     =   "Quita esta Petici�n del Agrupamiento"
         Top             =   720
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Index           =   0
         Left            =   -67740
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   3600
         Width           =   885
      End
      Begin VB.CommandButton cmdAgregarPet 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   -67740
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Incorpora una Petici�n a este Agrupamiento"
         Top             =   3240
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Index           =   1
         Left            =   -67740
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   3600
         Width           =   885
      End
      Begin VB.CommandButton cmdAgregarAgr 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   -67740
         TabIndex        =   31
         TabStop         =   0   'False
         ToolTipText     =   "Incorpora otro Agrupamiento"
         Top             =   3240
         Width           =   885
      End
      Begin VB.CommandButton cmdQuitarAgr 
         Caption         =   "Quitar"
         Height          =   375
         Left            =   -67740
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Quita este Agrupamiento"
         Top             =   2880
         Width           =   885
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cerrar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   7260
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Terminar la operaci�n"
         Top             =   3600
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Guardar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   7260
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Confirma los cambios ralizados"
         Top             =   3240
         Width           =   885
      End
      Begin VB.CommandButton cmdBorrar 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   7260
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Elimina este agrupamiento"
         Top             =   2880
         Width           =   885
      End
      Begin VB.CommandButton cmdModif 
         Caption         =   "Editar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   7260
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Confirma los cambios ralizados"
         Top             =   2520
         Width           =   885
      End
      Begin VB.ComboBox cboVisibilidad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   9
         ToolTipText     =   "Nivel a partir del cual este agrupamiento es visible"
         Top             =   1860
         Width           =   2295
      End
      Begin VB.ComboBox cboActualiza 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   8
         ToolTipText     =   "Nivel a partir del cual se pueden incorporar Peticiones"
         Top             =   2580
         Width           =   2295
      End
      Begin VB.ComboBox cboEstado 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   3255
         Width           =   2295
      End
      Begin VB.ComboBox cboAdmPet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   2
         ToolTipText     =   "Admite, o no, la incorporaci�n de peticiones en el agrupamiento"
         Top             =   2220
         Width           =   2295
      End
      Begin VB.ComboBox cboArea 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1680
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1200
         Width           =   6285
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   1680
         TabIndex        =   10
         Top             =   540
         Width           =   6285
         _ExtentX        =   11086
         _ExtentY        =   556
         MaxLength       =   50
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUsualta 
         Height          =   315
         Left            =   1680
         TabIndex        =   11
         Top             =   870
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         AutoSelect      =   0   'False
      End
      Begin AT_MaskText.MaskText txtNroPadre 
         Height          =   315
         Left            =   1680
         TabIndex        =   12
         ToolTipText     =   "N�mero asignado por el administrador"
         Top             =   3600
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtNomPadre 
         Height          =   315
         Left            =   2460
         TabIndex        =   13
         Top             =   3600
         Width           =   4035
         _ExtentX        =   7117
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNomUsualta 
         Height          =   315
         Left            =   2880
         TabIndex        =   14
         Top             =   870
         Width           =   5085
         _ExtentX        =   8969
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin MSFlexGridLib.MSFlexGrid grdHijPeti 
         Height          =   3300
         Left            =   -74820
         TabIndex        =   26
         Top             =   750
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   5821
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ListView lswDatos 
         Height          =   3315
         Left            =   -74880
         TabIndex        =   28
         Top             =   720
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   5847
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "imgAgrupamientos"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin MSFlexGridLib.MSFlexGrid grdHijAgru 
         Height          =   3270
         Left            =   -74880
         TabIndex        =   32
         Top             =   720
         Visible         =   0   'False
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   5768
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList imgAgrupamientos 
         Left            =   -67380
         Top             =   420
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   3
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAgrupCarga.frx":05DE
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAgrupCarga.frx":0B78
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAgrupCarga.frx":1112
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.CommandButton cmdQuitarPet 
         Caption         =   "Quitar"
         Height          =   375
         Left            =   -67740
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Quita esta Petici�n del Agrupamiento"
         Top             =   2880
         Width           =   885
      End
      Begin VB.Label lblAyuda 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Ayuda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   7740
         MouseIcon       =   "frmAgrupCarga.frx":16AC
         TabIndex        =   34
         Top             =   0
         Width           =   465
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamientos que integran al agrupamiento seleccionado"
         Height          =   195
         Left            =   -74880
         TabIndex        =   33
         Top             =   480
         Width           =   4200
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Peticiones que integran al agrupamiento seleccionado"
         Height          =   195
         Left            =   -74820
         TabIndex        =   27
         Top             =   540
         Width           =   3840
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Vigente"
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   3330
         Width           =   540
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   555
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Nivel de visibilidad"
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   1935
         Width           =   1290
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "�rea"
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   1260
         Width           =   345
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Nivel Incorp. Petic."
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   2655
         Width           =   1365
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Propietario"
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   930
         Width           =   780
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Integra el Agrup."
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   3660
         Width           =   1245
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Admite peticiones"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   2295
         Width           =   1260
      End
   End
End
Attribute VB_Name = "frmAgrupCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 22.10.2010 - Se elimina la confirmaci�n para agregar la petici�n al agrupamiento.
' -003- a. FJS 31.01.2011 - Arreglo: si el perfil actuante es el SADM, no lo estaba tomando por inicializarse antes de llamar al shell
'                           con otro perfil (VIEW).

Option Explicit

Const colNroAsignado = 0
Const colTitulo = 1
Const colNroInterno = 2

Const colAgrTitulo = 0
Const colAgrVigente = 1
Const colAgrNroInterno = 2
Const colPetNroAsignado = 0
Const colPetTitulo = 1
Const colPetNroInterno = 2

Const IMAGEN_AGRUP_PRIVADO = 3
Const IMAGEN_AGRUP_NORMAL = 2
Const IMAGEN_AGRUP_PADRE_CERRADO = 1

Dim sUsualta As String
Dim flgEnCarga As Boolean
Dim xDir As String, xGer As String, xSec As String, xGru As String
Dim wDir As String, wGer As String, wSec As String, wGru As String
Dim sArea As String
Dim sVisibilidad As String, sActualiza As String, sEstado As String
Dim pdrVigente As String
Dim hijAgrNrointerno As String, hijPetNrointerno As String
Dim esOwner As Boolean
Public bEdicion As Boolean

Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long

Private Sub Form_Load()
    flgEnCarga = True
    bEdicion = False
   'Me.AutoRedraw = True
    Dim i As Integer
    
    esOwner = False
    cboEstado.Clear
    cboEstado.AddItem "Si" & ESPACIOS & "||S"
    cboEstado.AddItem "No" & ESPACIOS & "||N"
    cboEstado.ListIndex = 0
    
    cboAdmPet.Clear
    cboAdmPet.AddItem "Admite" & ESPACIOS & "||S"
    cboAdmPet.AddItem "No admite" & ESPACIOS & "||N"
    cboAdmPet.ListIndex = 0
    
'    For i = 0 To orjBtn.Count - 1
'        orjFiller(i).Width = orjBtn(i).Width - 48
'        orjFiller(i).Left = orjBtn(i).Left + 16
'        orjFiller(i).Top = orjBtn(i).Top + orjBtn(i).Height - 32
'    Next
    Call IniciarScroll(grdHijPeti)
    Call InicializarPantalla
End Sub

Public Sub InicializarPantalla()
    Me.Tag = ""
    flgEnCarga = True
    Call LockProceso(True)
    'DoEvents
    
    cmdOk.Enabled = False
    
    Call CargaEstructura
    
'    Call orjBtn_Click(0)
    
    Select Case glModoAgrup
    Case "ALTA", "ALTAX"
'        orjFiller(1).visible = False
'        orjBtn(1).visible = False
'        orjFrame(1).visible = False
'        orjFiller(2).visible = False
'        orjBtn(2).visible = False
'        orjFrame(2).visible = False
        'sstAgrupamientos.Tab
        sstAgrupamientos.TabVisible(1) = False          ' Subagrupamientos
        sstAgrupamientos.TabVisible(2) = False          ' Peticiones
    
        sVisibilidad = "USR"
        sActualiza = "USR"
        txtUsualta.Text = glLOGIN_ID_REEMPLAZO
        txtNomUsualta.Text = glLOGIN_NAME_REEMPLAZO
        xDir = glLOGIN_Direccion
        xGer = glLOGIN_Gerencia
        xSec = glLOGIN_Sector
        xGru = glLOGIN_Grupo
        
        
''         cboVisibilidad.Clear
''         cboActualiza.Clear
''         cboVisibilidad.AddItem getDescNvAgrup("USR") & space(20) & "||USR"
''         cboActualiza.AddItem getDescNvAgrup("USR") & space(20) & "||USR"
''         If InPerfil("ADMI") Then
''            cboVisibilidad.AddItem getDescNvAgrup("PUB") & space(20) & "||PUB"
''            cboActualiza.AddItem getDescNvAgrup("PUB") & space(20) & "||PUB"
''         End If
''
''         If ClearNull(xDir) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("DIR") & space(20) & "||DIR"
''             cboActualiza.AddItem getDescNvAgrup("DIR") & space(20) & "||DIR"
''         End If
''         If ClearNull(xGer) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("GER") & space(20) & "||GER"
''            cboActualiza.AddItem getDescNvAgrup("GER") & space(20) & "||GER"
''         End If
''         If ClearNull(xSec) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("SEC") & space(20) & "||SEC"
''            cboActualiza.AddItem getDescNvAgrup("SEC") & space(20) & "||SEC"
''         End If
''         If ClearNull(xGru) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("GRU") & space(20) & "||GRU"
''            cboActualiza.AddItem getDescNvAgrup("GRU") & space(20) & "||GRU"
''         End If
        
        Call ArmaArea
    Case "MODI"
'        orjFiller(1).visible = True
'        orjBtn(1).visible = True
'        orjFrame(1).visible = True
'        orjFiller(2).visible = True
'        orjBtn(2).visible = True
'        orjFrame(2).visible = True
        
''         cboVisibilidad.Clear
''         cboActualiza.Clear
''         cboVisibilidad.AddItem getDescNvAgrup("USR") & space(20) & "||USR"
''         cboActualiza.AddItem getDescNvAgrup("USR") & space(20) & "||USR"
''         If InPerfil("ADMI") Then
''            cboVisibilidad.AddItem getDescNvAgrup("PUB") & space(20) & "||PUB"
''            cboActualiza.AddItem getDescNvAgrup("PUB") & space(20) & "||PUB"
''         End If
''         If ClearNull(xDir) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("DIR") & space(20) & "||DIR"
''             cboActualiza.AddItem getDescNvAgrup("DIR") & space(20) & "||DIR"
''         End If
''         If ClearNull(xGer) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("GER") & space(20) & "||GER"
''            cboActualiza.AddItem getDescNvAgrup("GER") & space(20) & "||GER"
''         End If
''         If ClearNull(xSec) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("SEC") & space(20) & "||SEC"
''            cboActualiza.AddItem getDescNvAgrup("SEC") & space(20) & "||SEC"
''         End If
''         If ClearNull(xGru) <> "" Then
''             cboVisibilidad.AddItem getDescNvAgrup("GRU") & space(20) & "||GRU"
''            cboActualiza.AddItem getDescNvAgrup("GRU") & space(20) & "||GRU"
''         End If
        sstAgrupamientos.TabVisible(1) = True          ' Subagrupamientos
        sstAgrupamientos.TabVisible(2) = True          ' Peticiones
        Call CargaAgrup
    Case "VIEW"
'        orjFiller(1).visible = True
'        orjBtn(1).visible = True
'        orjFrame(1).visible = True
'        orjFiller(2).visible = True
'        orjBtn(2).visible = True
'        orjFrame(2).visible = True
        
'''        cboVisibilidad.Clear
'''        cboActualiza.Clear
'''        cboVisibilidad.AddItem getDescNvAgrup("USR") & space(20) & "||USR"
'''        cboActualiza.AddItem getDescNvAgrup("USR") & space(20) & "||USR"
'''        cboVisibilidad.AddItem getDescNvAgrup("PUB") & space(20) & "||PUB"
'''        cboActualiza.AddItem getDescNvAgrup("PUB") & space(20) & "||PUB"
'''        cboVisibilidad.AddItem getDescNvAgrup("DIR") & space(20) & "||DIR"
'''        cboVisibilidad.AddItem getDescNvAgrup("GER") & space(20) & "||GER"
'''        cboVisibilidad.AddItem getDescNvAgrup("SEC") & space(20) & "||SEC"
'''        cboVisibilidad.AddItem getDescNvAgrup("GRU") & space(20) & "||GRU"
'''        cboActualiza.AddItem getDescNvAgrup("DIR") & space(20) & "||DIR"
'''        cboActualiza.AddItem getDescNvAgrup("GER") & space(20) & "||GER"
'''        cboActualiza.AddItem getDescNvAgrup("SEC") & space(20) & "||SEC"
'''        cboActualiza.AddItem getDescNvAgrup("GRU") & space(20) & "||GRU"
        sstAgrupamientos.TabVisible(1) = True          ' Subagrupamientos
        sstAgrupamientos.TabVisible(2) = True          ' Peticiones
        Call CargaAgrup
    End Select
    
'    Call orjBtn_Click(0)
    Call HabilitarBotones

    On Error Resume Next
    txtTitulo.SetFocus
    'Show
    DoEvents
    Call LockProceso(False)
    flgEnCarga = False
End Sub

Private Sub cboAdmPet_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If CodigoCombo(cboAdmPet, True) = "N" Then
        Call setHabilCtrl(cboActualiza, "DIS")
    Else
        Call setHabilCtrl(cboActualiza, "NOR")
    End If
    Call LockProceso(False)
End Sub

Private Sub cboActualiza_click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call SetCombo(cboActualiza, chkActuCombo(CodigoCombo(cboVisibilidad, True), CodigoCombo(cboActualiza, True)), True)
    Call LockProceso(False)
End Sub

Private Sub cboVisibilidad_click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call SetCombo(cboActualiza, chkActuCombo(CodigoCombo(cboVisibilidad, True), CodigoCombo(cboActualiza, True)), True)
    Call LockProceso(False)
End Sub

Private Sub cmdBorrar_Click()
    If flgEnCarga = True Then Exit Sub
    If MsgBox("�Confirma la eliminaci�n de este agrupamiento?", vbQuestion + vbYesNo) = vbNo Then
        Exit Sub
    End If
    If sp_DelAgrup(glNumeroAgrup) Then
        touchForms
        Unload Me
    End If
End Sub

Private Sub cmdCancel_Click()
    If flgEnCarga = True Then Exit Sub
    Select Case glModoAgrup
        Case "ALTA", "ALTAX", "VIEW"
            touchForms
            Unload Me
        Case "MODI"
            glModoAgrup = "VIEW"
            Call CargaAgrup
            Call HabilitarBotones
    End Select
End Sub

Private Sub cmdCerrar_Click(Index As Integer)
    If flgEnCarga = True Then Exit Sub
    touchForms
    Unload Me
End Sub

'Private Sub cmdHlp_Click()
'    If flgEnCarga = True Then Exit Sub
'    frmHelp.Show vbModal
'End Sub

Private Sub cmdModif_Click()
    If flgEnCarga = True Then Exit Sub
    glModoAgrup = "MODI"
    Call HabilitarBotones
End Sub

'Private Sub orjBtn_Click(Index As Integer)
'    If flgEnCarga = True Then Exit Sub
'    Dim i As Integer
'    For i = 0 To orjBtn.Count - 1
'        orjFiller(i).visible = False
'        orjBtn(i).Font.Bold = False
'        orjBtn(i).BackColor = RGB(208, 208, 208)
'        orjFrame(i).visible = False
'    Next
'    orjFrame(Index).visible = True
'    orjFiller(Index).visible = True
'    orjBtn(Index).Font.Bold = True
'    orjBtn(Index).BackColor = orjFrame(Index).BackColor
'End Sub

Private Sub cmdOK_Click()
    If flgEnCarga = True Then Exit Sub
    Dim auxNro As String
    Dim flgEstado As Boolean
    Dim nNumeroAsignado As Integer
    Dim vRetorno() As String
    Dim auxAgrup As String
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    
    auxAgrup = DatosCombo(cboArea)
    xDir = ""
    xGer = ""
    xSec = ""
    xGru = ""
    If ParseString(vRetorno, auxAgrup, "|") > 0 Then
        xDir = vRetorno(2)
        xGer = vRetorno(3)
        xSec = vRetorno(4)
        xGru = vRetorno(5)
    Else
        MsgBox ("Error en Area")
    End If
    
    Select Case glModoAgrup
    Case "ALTA", "ALTAX"
        If CamposObligatorios Then
            glNumeroAgrup = sp_InsAgrup(0, txtTitulo.Text, CodigoCombo(cboEstado, True), xDir, xGer, xSec, xGru, txtUsualta.Text, CodigoCombo(cboVisibilidad, True), CodigoCombo(cboActualiza, True), CodigoCombo(cboAdmPet, True))
            If Not aplRST.EOF Then
                glNumeroAgrup = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
                bEdicion = True
                'aplRST.Close
            End If
            If Val(glNumeroAgrup) = 0 Then
                MsgBox ("Error al generar Numero Interno")
            End If
            ReDim vAgrPublico(1 To 1, 1 To 1)
            'touchForms
            If glModoAgrup = "ALTAX" Then
                Unload Me
                Exit Sub
            End If
            glModoAgrup = "VIEW"
            Call InicializarPantalla
        End If
    Case "MODI"
        If CamposObligatorios Then
            touchForms
        End If
        flgEstado = False
        If sVisibilidad <> CodigoCombo(cboVisibilidad, True) Or _
            sEstado = CodigoCombo(cboEstado, True) Or _
            xDir <> wDir Or xGer <> wGer Or xSec <> wSec Or xGru <> wGru Or _
            sActualiza = CodigoCombo(cboActualiza, True) Then
            If MsgBox("Al cambiar el �rea, la vigencia, o el alcance de visibilidad/actualizaci�n," & Chr(13) & "se modificaran los de Agrupamientos dependienes." & Chr(13) & "�Confirma?", vbQuestion + vbYesNo, vbYesNo) = vbNo Then
                Call LockProceso(False)
                Exit Sub
            End If
            flgEstado = True
        End If
        If sp_UpdAgrup(glNumeroAgrup, txtNroPadre.Text, txtTitulo.Text, CodigoCombo(cboEstado, True), xDir, xGer, xSec, xGru, txtUsualta.Text, CodigoCombo(cboVisibilidad, True), CodigoCombo(cboActualiza, True), CodigoCombo(cboAdmPet, True)) Then
            If flgEstado Then
                Call sp_UpdAgrupAlcance(glNumeroAgrup, txtUsualta.Text, CodigoCombo(cboVisibilidad, True), CodigoCombo(cboActualiza, True), CodigoCombo(cboEstado, True), xDir, xGer, xSec, xGru)
                bEdicion = True
            End If
            ReDim vAgrPublico(1 To 1, 1 To 1)
            glModoAgrup = "VIEW"
            Call InicializarPantalla
        End If
    End Select
    Call LockProceso(False)
End Sub

Private Sub AdecuaCombos()
    cboVisibilidad.Clear
    cboActualiza.Clear
    cboVisibilidad.AddItem getDescNvAgrup("USR") & Space(20) & "||USR"
    cboActualiza.AddItem getDescNvAgrup("USR") & Space(20) & "||USR"
    
    Select Case glModoAgrup
    Case "ALTA", "ALTAX"
''        xDir = glLOGIN_Direccion
''        xGer = glLOGIN_Gerencia
''        xSec = glLOGIN_Sector
''        xGru = glLOGIN_Grupo
        
         If ClearNull(xGru) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("GRU") & Space(20) & "||GRU"
            cboActualiza.AddItem getDescNvAgrup("GRU") & Space(20) & "||GRU"
         End If
         If ClearNull(xSec) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("SEC") & Space(20) & "||SEC"
            cboActualiza.AddItem getDescNvAgrup("SEC") & Space(20) & "||SEC"
         End If
         If ClearNull(xGer) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("GER") & Space(20) & "||GER"
            cboActualiza.AddItem getDescNvAgrup("GER") & Space(20) & "||GER"
         End If
         If ClearNull(xDir) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("DIR") & Space(20) & "||DIR"
             cboActualiza.AddItem getDescNvAgrup("DIR") & Space(20) & "||DIR"
         End If
         If InPerfil("ADMI") Then
            cboVisibilidad.AddItem getDescNvAgrup("PUB") & Space(20) & "||PUB"
            cboActualiza.AddItem getDescNvAgrup("PUB") & Space(20) & "||PUB"
         End If
         
         Call SetCombo(cboVisibilidad, "USR", True)
         Call SetCombo(cboActualiza, "USR", True)
        
    Case "MODI"
         If ClearNull(xGru) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("GRU") & Space(20) & "||GRU"
            cboActualiza.AddItem getDescNvAgrup("GRU") & Space(20) & "||GRU"
         End If
         If ClearNull(xSec) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("SEC") & Space(20) & "||SEC"
            cboActualiza.AddItem getDescNvAgrup("SEC") & Space(20) & "||SEC"
         End If
         If ClearNull(xGer) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("GER") & Space(20) & "||GER"
            cboActualiza.AddItem getDescNvAgrup("GER") & Space(20) & "||GER"
         End If
         If ClearNull(xDir) <> "" Then
             cboVisibilidad.AddItem getDescNvAgrup("DIR") & Space(20) & "||DIR"
             cboActualiza.AddItem getDescNvAgrup("DIR") & Space(20) & "||DIR"
         End If
         If InPerfil("ADMI") Then
            cboVisibilidad.AddItem getDescNvAgrup("PUB") & Space(20) & "||PUB"
            cboActualiza.AddItem getDescNvAgrup("PUB") & Space(20) & "||PUB"
         End If
    Case "VIEW"
        cboVisibilidad.AddItem getDescNvAgrup("GRU") & Space(20) & "||GRU"
        cboActualiza.AddItem getDescNvAgrup("GRU") & Space(20) & "||GRU"
        cboActualiza.AddItem getDescNvAgrup("SEC") & Space(20) & "||SEC"
        cboVisibilidad.AddItem getDescNvAgrup("SEC") & Space(20) & "||SEC"
        cboVisibilidad.AddItem getDescNvAgrup("GER") & Space(20) & "||GER"
        cboActualiza.AddItem getDescNvAgrup("GER") & Space(20) & "||GER"
        cboVisibilidad.AddItem getDescNvAgrup("DIR") & Space(20) & "||DIR"
        cboActualiza.AddItem getDescNvAgrup("DIR") & Space(20) & "||DIR"
        cboVisibilidad.AddItem getDescNvAgrup("PUB") & Space(20) & "||PUB"
        cboActualiza.AddItem getDescNvAgrup("PUB") & Space(20) & "||PUB"
    End Select
    Call SetCombo(cboVisibilidad, sVisibilidad, True)
    Call SetCombo(cboActualiza, sActualiza, True)

End Sub

Private Sub HabilitarBotones()
    cmdOk.Enabled = False
    cmdCancel.Enabled = True
    
    Call AdecuaCombos
    
    Call setHabilCtrl(txtUsualta, "DIS")
    Call setHabilCtrl(txtNomUsualta, "DIS")
    Call setHabilCtrl(txtNroPadre, "DIS")
    Call setHabilCtrl(txtNomPadre, "DIS")
    Call setHabilCtrl(txtTitulo, "DIS")
    Call setHabilCtrl(cboVisibilidad, "DIS")
    Call setHabilCtrl(cboActualiza, "DIS")
    Call setHabilCtrl(cboEstado, "DIS")
    Call setHabilCtrl(cboAdmPet, "DIS")
    Call setHabilCtrl(cboArea, "DIS")

    Select Case glModoAgrup
    Case "ALTA", "ALTAX"
        Call setHabilCtrl(txtTitulo, "NOR")
        Call setHabilCtrl(cboActualiza, "NOR")
        Call setHabilCtrl(cboVisibilidad, "NOR")
        Call setHabilCtrl(cboEstado, "NOR")
        Call setHabilCtrl(cboAdmPet, "NOR")
        If InPerfil("ADMI") Or InPerfil("SADM") Then
            Call setHabilCtrl(cboArea, "NOR")
        End If
        cmdOk.Enabled = True
        cmdModif.Enabled = False
        cmdBorrar.Enabled = False
        cmdCancel.ToolTipText = "Cancelar el alta"
    Case "MODI"
'        orjBtn(1).Enabled = False
'        orjBtn(2).Enabled = False
'        Call orjBtn_Click(0)
        cmdOk.Enabled = True
        cmdModif.Enabled = False
        cmdBorrar.Enabled = False
        cmdCancel.Caption = "Cancelar"
        cmdCancel.ToolTipText = "Cancelar la modificaci�n"
        Call setHabilCtrl(txtTitulo, "NOR")
        Call setHabilCtrl(cboAdmPet, "NOR")
        If Val(txtNroPadre.Text) = 0 Then
            Call setHabilCtrl(cboVisibilidad, "NOR")
            Call setHabilCtrl(cboEstado, "NOR")
            If CodigoCombo(cboAdmPet, True) = "S" Then
                Call setHabilCtrl(cboActualiza, "NOR")
            End If
            If glUsrPerfilActual = "SADM" Then
                Call setHabilCtrl(cboArea, "NOR")
            End If
        Else
            If pdrVigente = "S" Then
                Call setHabilCtrl(cboEstado, "NOR")
            End If
        End If
        cmdOk.Enabled = True
    Case "VIEW"
        'hay que hacer las preguntas correespondientes
        'respecto a la actualizabilidad
        cmdAgregarAgr.visible = False
        cmdQuitarAgr.visible = False
        cmdModif.Enabled = False
        cmdAgregarPet.visible = False
        cmdQuitarPet.visible = False
        If esOwner Then
            cmdBorrar.Enabled = True
            cmdModif.Enabled = True
            'If Val(txtNroPadre.Text) = 0 Then
                cmdAgregarAgr.visible = True
                cmdQuitarAgr.visible = True
            'End If
        End If
        
        If CodigoCombo(cboAdmPet, True) = "S" Then
            If chkNvAgrup(sActualiza, xDir, xGer, xSec, xGru, sUsualta, glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REEMPLAZO) Then
                cmdAgregarPet.visible = True
                cmdQuitarPet.visible = True
            End If
        End If
        cmdOk.Enabled = False
        cmdCancel.Caption = "Cerrar"
        cmdCancel.ToolTipText = "Salir de esta pantalla"
'        orjBtn(1).Enabled = True
'        orjBtn(2).Enabled = True
'        Call orjBtn_Click(0)
    End Select
'    Me.orjFrame(0).Refresh
    DoEvents
    Call LockProceso(False)
End Sub

Function CamposObligatorios() As Boolean
    If ClearNull(txtTitulo.Text) = "" Then
       MsgBox "Falta Especificar T�tulo", vbOKOnly + vbInformation
       CamposObligatorios = False
       Exit Function
    End If
    If cboVisibilidad.ListIndex = -1 Then
       MsgBox "Falta Especificar Visibilidad", vbOKOnly + vbInformation
       CamposObligatorios = False
       Exit Function
    End If
    If cboActualiza.ListIndex = -1 Then
       MsgBox "Falta Especificar Nivel Incorporaci�n Peticiones", vbOKOnly + vbInformation
       CamposObligatorios = False
       Exit Function
    End If
    
    CamposObligatorios = True
End Function

Private Sub CargaAgrup()
    Dim xDescri As String
    Dim xAsig As String
    Call Puntero(True)
  
    If sp_GetAgrup(glNumeroAgrup) Then
        If Not aplRST.EOF Then
            txtTitulo.Text = ClearNull(aplRST!agr_titulo)
            cboEstado.ListIndex = PosicionCombo(cboEstado, ClearNull(aplRST!agr_vigente), True)
            cboAdmPet.ListIndex = PosicionCombo(cboAdmPet, ClearNull(aplRST!agr_admpet), True)
            txtUsualta.Text = ClearNull(aplRST!cod_usualta)
            txtNomUsualta.Text = ClearNull(aplRST!nom_usualta)
            txtNroPadre.Text = ClearNull(aplRST!agr_nropadre)
            txtNomPadre.Text = ClearNull(aplRST!nom_nropadre)
         
            sEstado = ClearNull(aplRST!agr_vigente)
            pdrVigente = ClearNull(aplRST!pdr_vigente)
            sVisibilidad = ClearNull(aplRST!agr_visibilidad)
            sActualiza = ClearNull(aplRST!agr_actualiza)
            sUsualta = ClearNull(aplRST!cod_usualta)
            If txtUsualta.Text = glLOGIN_ID_REEMPLAZO Then
                esOwner = True
            End If
            '{ add -003- a.
            If InPerfil("SADM") Then
                esOwner = True
            End If
            '}
            '{ del -003- a.
            'If glUsrPerfilActual = "SADM" Then
            '    esOwner = True
            'End If
            '}
            xDir = ClearNull(aplRST!cod_direccion)
            xGer = ClearNull(aplRST!cod_gerencia)
            xSec = ClearNull(aplRST!cod_sector)
            xGru = ClearNull(aplRST!cod_grupo)
            wDir = ClearNull(aplRST!cod_direccion)
            wGer = ClearNull(aplRST!cod_gerencia)
            wSec = ClearNull(aplRST!cod_sector)
            wGru = ClearNull(aplRST!cod_grupo)
        End If
        Call ArmaArea
        Call CargaHijAgru
        Call CargaHijPeti
    End If
    Call Puntero(False)
End Sub

'======================================================
Sub CargaHijAgru()
    Dim itmX As ListItem
    
    With grdHijAgru
        .Clear
        DoEvents
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colAgrTitulo) = "Agrupamiento"
        .TextMatrix(0, colAgrVigente) = "Vig."
        .ColWidth(colAgrTitulo) = 5000
        .ColWidth(colAgrVigente) = 600
        .ColWidth(colAgrNroInterno) = 0
    End With
    
    With lswDatos
        ' Agrega objetos ColumnHeaders
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "T�tulo", 4000
        ' Asigna la propiedad View el valor Report.
        .View = lvwReport
        .ListItems.Clear
    End With
    
    If Not sp_GetAgrupAgrup(glNumeroAgrup) Then
       GoTo finx
    End If
    Do While Not aplRST.EOF
        With grdHijAgru
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colAgrTitulo) = ClearNull(aplRST!agr_titulo)
            .TextMatrix(.Rows - 1, colAgrVigente) = ClearNull(aplRST!agr_vigente)
            .TextMatrix(.Rows - 1, colAgrNroInterno) = ClearNull(aplRST!agr_nrointerno)
        End With
        
        With lswDatos
            Set itmX = .ListItems.Add(, , ClearNull(aplRST!agr_titulo), , IMAGEN_AGRUP_NORMAL)
        End With
        aplRST.MoveNext
    Loop
    'aplRST.Close
finx:
    If grdHijAgru.Rows > 1 Then
        grdHijAgru.TopRow = 1
        grdHijAgru.Row = 1
        grdHijAgru.RowSel = 1
        grdHijAgru.Col = 0
        grdHijAgru.ColSel = grdHijAgru.cols - 1
        Call MostrarSelAgru
    End If
End Sub

Sub MostrarSelAgru()
    cmdQuitarAgr.Enabled = False
    hijAgrNrointerno = ""
    With grdHijAgru
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colAgrNroInterno) <> "" Then
                hijAgrNrointerno = .TextMatrix(.RowSel, colAgrNroInterno)
                cmdQuitarAgr.Enabled = True
            End If
        End If
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdHijPeti)
End Sub

Private Sub grdHijAgru_Click()
    'Debug.Print "grdDatos_Click"
    Call MostrarSelAgru
    DoEvents
End Sub

Private Sub grdHijAgru_RowColChange()
    Call MostrarSelAgru
End Sub

Private Sub cmdAgregarAgr_Click()
''    glNivelAgrup = CodigoCombo(cboVisibilidad,true)
''    glAreaAgrup = xDir & xGer & xSec & xGru
    glModoAgrup = "SELECT"
    glSelectAgrup = ""
''    glNivelAgrup = ""
''    glAreaAgrup = ""
    
    Dim Padre1, Padre2
    
    glModoSelectAgrup = "OWNR"
    frmAgrupSelect.xNivelVisi = CodigoCombo(cboVisibilidad, True)
    frmAgrupSelect.xNivelModi = CodigoCombo(cboActualiza, True)
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    DoEvents
    glModoAgrup = "VIEW"
    
    If glSelectAgrup = "" Then
        Exit Sub
    End If
    
    Padre1 = 0
    Padre2 = 0
    
    If sp_GetAgrupPadre(glNumeroAgrup) Then
        If Not aplRST.EOF Then
            Padre1 = ClearNull(aplRST!agr_nrointerno)
        End If
    End If
    If sp_GetAgrupPadre(glSelectAgrup) Then
        If Not aplRST.EOF Then
            Padre2 = ClearNull(aplRST!agr_nrointerno)
        End If
    End If

    If (Val(Padre1) <> 0 And Val(Padre2) <> 0) And (Val(Padre1) = Val(Padre2)) Then
       MsgBox ("Este Agrupamiento posee el mismo padre que el receptor")
       Exit Sub
    End If
        
    If MsgBox("Desea que el Agrupamiento '" & Trim(Left(glSelectAgrupStr, 40)) & "' integre este Agrupamiento?", vbQuestion + vbYesNo) = vbNo Then
        Exit Sub
    End If
    Call sp_InsAgrupAgrup(glSelectAgrup, glNumeroAgrup)
    Call CargaHijAgru
    bEdicion = True
End Sub

Private Sub cmdQuitarAgr_Click()
    If flgEnCarga = True Then Exit Sub
    If hijAgrNrointerno = "" Then
        Exit Sub
    End If
    If MsgBox("Desea quitar este sub-agrupamiento?", vbYesNo) = vbNo Then
        Exit Sub
    End If
    Call sp_DelAgrupAgrup(hijAgrNrointerno, glNumeroAgrup)
    Call CargaHijAgru
End Sub

Sub CargaHijPeti()
    With grdHijPeti
        .Clear
        'DoEvents
        .HighLight = flexHighlightWithFocus
        .Rows = 1
        .TextMatrix(0, colPetNroAsignado) = "Peticion": .ColWidth(colPetNroAsignado) = 900
        .TextMatrix(0, colPetTitulo) = "T�tulo": .ColAlignment(colPetTitulo) = flexAlignLeftCenter: .ColWidth(colPetTitulo) = 5500
        .ColWidth(colPetNroInterno) = 0
        Call CambiarEfectoLinea(grdHijPeti, prmGridEffectFontBold)
    End With
    If Not sp_GetAgrupPetic(glNumeroAgrup, 0) Then
        GoTo finx
    End If
    Do While Not aplRST.EOF
        With grdHijPeti
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colPetTitulo) = ClearNull(aplRST!pet_titulo)
            .TextMatrix(.Rows - 1, colPetNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colPetNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdHijPeti, prmGridFillRowColorLightGrey2)
        End With
        aplRST.MoveNext
        DoEvents
    Loop
finx:
    With grdHijPeti
        If .Rows > 1 Then
            .TopRow = 1
            .Row = 1
            .RowSel = 1
            .Col = 0
            .ColSel = .cols - 1
            Call MostrarSelPeti
        End If
    End With
End Sub

Sub MostrarSelPeti()
    cmdQuitarPet.Enabled = False
    hijPetNrointerno = ""
    With grdHijPeti
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, colPetNroInterno) <> "" Then
                hijPetNrointerno = .TextMatrix(.RowSel, colPetNroInterno)
                cmdQuitarPet.Enabled = True
            End If
        End If
    End With
End Sub

Private Sub grdHijPeti_Click()
    'Debug.Print "grdDatos_Click"
    'Call MostrarSelPeti
    DoEvents
    ' Agregado 20.07.16
    bSorting = True
    Call OrdenarGrilla(grdHijPeti, lUltimaColumnaOrdenada)
    bSorting = False
End Sub

Private Sub grdHijPeti_RowColChange()
    If Not flgEnCarga Then Call MostrarSelPeti
End Sub

Private Sub cmdAgregarPet_Click()
    Dim vRetorno() As String
    Dim rtAux As String
    
    glAuxRetorno = ""
    glAuxEstado = "CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|RECHAZ|RECHTE|CANCEL|SUSPEN|REVISA|TERMIN"
'''    glAuxEstado = "CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC"
    frmSelPet.Show 1
    DoEvents
    If glAuxRetorno = "" Then
        Exit Sub
    End If
        
    Call ParseString(vRetorno, glAuxRetorno, "||")
    rtAux = vRetorno(2)
    If rtAux = "" Then
        Exit Sub
    End If
    Call ParseString(vRetorno, rtAux, "|")
    
    'si ya existe agrup-petic no hace nada, ni siquiera avisa
    If sp_GetAgrupPetic(glNumeroAgrup, vRetorno(2)) Then
        aplRST.Close
    Else
        Call Puntero(True)
        If sp_GetAgrupPeticDup(glNumeroAgrup, vRetorno(2)) Then
            Call Puntero(False)
            If MsgBox("Esta Peticion ya est� declarada en el agrupamiento: " & ClearNull(aplRST!agr_titulo) & Chr(13) & "Desea igualmente que integre " & txtTitulo.Text & "?", vbYesNo) = vbNo Then
               aplRST.Close
                Exit Sub
            End If
            aplRST.Close
        Else
            Call Puntero(False)
        '{ del -002- a.
        '    If MsgBox("Desea que esta Petici�n integre el Agrupamiento " & txtTitulo.Text & "?", vbYesNo) = vbNo Then
        '        Exit Sub
        '    End If
        '}
        End If
        Call sp_InsAgrupPetic(glNumeroAgrup, vRetorno(2))
        Call CargaHijPeti
    End If
End Sub

Private Sub cmdQuitarPet_Click()
    If hijPetNrointerno = "" Then
        Exit Sub
    End If
    If MsgBox("Desea que esta Petici�n no integre m�s el Agrupamiento?", vbYesNo) = vbNo Then
        Exit Sub
    End If
    Call sp_DelAgrupPetic(glNumeroAgrup, hijPetNrointerno)
    Call CargaHijPeti
End Sub

'======================================================
Private Sub ArmaArea()
    cboArea.ListIndex = PosicionCombo(cboArea, xDir & xGer & xSec & xGru, True)
End Sub

Private Function chkActuCombo(xVisi, xActu) As String
    Select Case xVisi
        Case "PUB"
            chkActuCombo = xActu
        Case "DIR"
            If InStr("DIR|GER|SEC|GRU|USR", xActu) > 0 Then
                chkActuCombo = xActu
            Else
                chkActuCombo = "DIR"
            End If
        Case "GER"
            If InStr("GER|SEC|GRU|USR", xActu) > 0 Then
                chkActuCombo = xActu
            Else
                chkActuCombo = "GER"
            End If
        Case "SEC"
            If InStr("SEC|GRU|USR", xActu) > 0 Then
                chkActuCombo = xActu
            Else
                chkActuCombo = "SEC"
            End If
        Case "GRU"
            If InStr("GRU|USR", xActu) > 0 Then
                chkActuCombo = xActu
            Else
                chkActuCombo = "GRU"
            End If
        Case "USR"
            chkActuCombo = "USR"
    End Select
End Function

Private Sub CargaEstructura()
    Dim sDireccion As String
    Dim sGerencia As String
    
    If sp_GetDireccion("") Then
        Do While Not aplRST.EOF
            'cboArea.AddItem Trim(aplRST!nom_direccion) & Space(150) & "||" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_direccion) & "|||"
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            cboArea.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_direccion) & "|||"
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGerenciaXt("", "") Then
        Do While Not aplRST.EOF
            'cboArea.AddItem Trim(aplRST!nom_direccion) & ">> " & Trim(aplRST!nom_gerencia) & Space(120) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "||"
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cboArea.AddItem sDireccion & " � " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "||"
            aplRST.MoveNext
        Loop
    End If
    If sp_GetSectorXt(Null, Null, Null) Then
        Do While Not aplRST.EOF
            'cboArea.AddItem Trim(aplRST!nom_direccion) & ">> " & Trim(aplRST!nom_gerencia) & ">> " & Trim(aplRST!nom_sector) & Space(120) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|"
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cboArea.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|"
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGrupoXt("", "") Then
        Do While Not aplRST.EOF
            'cboArea.AddItem Trim(aplRST!nom_direccion) & ">> " & Trim(aplRST!nom_gerencia) & ">> " & Trim(aplRST!nom_sector) & ">> " & Trim(aplRST!nom_grupo) & Space(120) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & ClearNull(aplRST!cod_grupo) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_grupo)
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cboArea.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & ClearNull(aplRST!cod_grupo) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_grupo)
            aplRST.MoveNext
        Loop
    End If
End Sub

Private Sub cboArea_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Dim vRetorno() As String
    Dim auxAgrup As String
    auxAgrup = DatosCombo(cboArea)
    xDir = ""
    xGer = ""
    xSec = ""
    xGru = ""
    If ParseString(vRetorno, auxAgrup, "|") > 0 Then
        xDir = vRetorno(2)
        xGer = vRetorno(3)
        xSec = vRetorno(4)
        xGru = vRetorno(5)
    End If
    Call AdecuaCombos
    DoEvents
    Call LockProceso(False)
End Sub

Private Sub lblAyuda_Click()
    If flgEnCarga = True Then Exit Sub
    frmHelp.Show vbModal
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call InicializarLinks
End Sub

Private Sub sstAgrupamientos_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Call InicializarLinks
End Sub

Private Sub InicializarLinks()
    With lblAyuda
        .ForeColor = vbBlue
        .MousePointer = 0     ' Default
    End With
End Sub

Private Sub lblAyuda_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    With lblAyuda
        .ForeColor = vbRed
        .MousePointer = 99
    End With
End Sub

Private Sub cmdImportarPeticiones_Click()
    Dim vPeticionNroInterno() As Long
    Dim i As Long
    
    Call ImportarListadoPeticiones(vPeticionNroInterno)
    For i = 0 To UBound(vPeticionNroInterno)
        Call Status("Procesando... " & i + 1)
        Call sp_InsAgrupPetic(glNumeroAgrup, vPeticionNroInterno(i))
    Next i
    Call Puntero(False)
    Call CargaHijPeti
    Call Status("Listo.")
    MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
End Sub
