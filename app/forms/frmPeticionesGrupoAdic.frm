VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesGrupoAdic 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Datos Adicionales Grupo Interviniente"
   ClientHeight    =   5355
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   8295
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5355
   ScaleWidth      =   8295
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   795
      Left            =   60
      TabIndex        =   3
      Top             =   -60
      Width           =   8190
      Begin VB.Label Label2 
         Caption         =   "Petición"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   9
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6705
         TabIndex        =   8
         Top             =   480
         Width           =   765
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1300
         TabIndex        =   7
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1305
         TabIndex        =   6
         Top             =   240
         Width           =   6705
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   7620
         TabIndex        =   5
         Top             =   480
         Width           =   480
      End
      Begin VB.Label Label6 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   4
         Top             =   480
         Width           =   1200
      End
   End
   Begin VB.Frame pnlBtnControl 
      Height          =   645
      Left            =   60
      TabIndex        =   0
      Top             =   4680
      Width           =   8190
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6240
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7170
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   10
      Top             =   660
      Width           =   8190
      Begin VB.Label lblSector 
         Caption         =   "sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   14
         Top             =   180
         Width           =   2745
      End
      Begin VB.Label Label9 
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   13
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoSector 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5565
         TabIndex        =   12
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label4 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   11
         Top             =   180
         Width           =   1200
      End
   End
   Begin VB.Frame Frame3 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   15
      Top             =   1100
      Width           =   8190
      Begin VB.Label lblGrupo 
         Caption         =   "grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   16
         Top             =   180
         Width           =   2745
      End
      Begin VB.Label Label8 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   19
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoGrupo 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5580
         TabIndex        =   18
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label5 
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   17
         Top             =   180
         Width           =   1200
      End
   End
   Begin VB.Frame fraSector 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Left            =   60
      TabIndex        =   20
      Top             =   1530
      Width           =   8190
      Begin VB.TextBox txtInfoAdic 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1140
         Left            =   1455
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   23
         Top             =   1155
         Width           =   6585
      End
      Begin AT_MaskText.MaskText txtPrioEjecuc 
         Height          =   315
         Left            =   1440
         TabIndex        =   21
         Top             =   480
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   556
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblHoras 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad Ejec."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   25
         Top             =   540
         Width           =   1200
      End
      Begin VB.Label lblAccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   6240
         TabIndex        =   24
         Top             =   180
         Width           =   495
      End
      Begin VB.Label lblSolic 
         Caption         =   "Información Adicional"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         TabIndex        =   22
         Top             =   1260
         Width           =   1110
      End
   End
End
Attribute VB_Name = "frmPeticionesGrupoAdic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Dim sOpcionSeleccionada As String
Dim EstadoSolicitud As String
Dim sGrupo As String
Dim sSector As String
Dim sGerencia As String
Dim sDireccion As String
Dim sCoorSect As String
Dim EstadoPeticion As String
Dim NuevoEstadoPeticion As String
Dim EstadoSector As String
Dim NuevoEstadoSector As String
Dim NuevoEstadoGrupo As String
Dim xPerfNivel, xPerfArea As String
Dim hst_nrointerno_sol
Dim sec_nrointerno_sol
Dim flgAccion As Boolean

Private Sub Form_Load()
    Call InicializarPantalla
    Call Habilitar
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub cmdConfirmar_Click()
    Dim NroHistorial As Long
    Dim xAccion As String
    Dim sMsgPeticion As String
    Dim xSituacion As String

    If CamposObligatorios Then
        cmdConfirmar.Enabled = False
        cmdCerrar.Enabled = False
        Call Puntero(True)
        Call Status("Procesando")
        Call sp_UpdatePeticionGrupoAdic(glNumeroPeticion, glGrupo, txtPrioEjecuc.Text, txtInfoAdic.Text)
        Call touchForms
        Call Puntero(False)
        Call Status("Listo")
        Unload Me
    End If
End Sub

Sub InicializarPantalla()
   Dim auxNro As String
    Me.Tag = ""
''   xPerfNivel = getPerfNivel(glUsrPerfilActual)
''   xPerfArea = getPerfArea(glUsrPerfilActual)
    
    cmdConfirmar.Enabled = False
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNro = "S/N"
            Else
                auxNro = ClearNull(aplRST!pet_nroasignado)
            End If
            lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
            lblPrioridad = ClearNull(aplRST!prioridad)
            EstadoPeticion = ClearNull(aplRST!cod_estado)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
        End If
        aplRST.Close
    End If
    If sp_GetPeticionSector(glNumeroPeticion, glSector) Then
       If Not aplRST.EOF Then
          sec_nrointerno_sol = ClearNull(aplRST!hst_nrointerno_sol)
          lblSector = ClearNull(aplRST!nom_sector)
          lblEstadoSector = ClearNull(aplRST!nom_estado)
          EstadoSector = ClearNull(aplRST!cod_estado)
        End If
        aplRST.Close
    End If

    txtInfoAdic.Text = ""
    txtPrioEjecuc.Text = "0"

    If sp_GetPeticionGrupoXt(glNumeroPeticion, glSector, glGrupo) Then
       If Not aplRST.EOF Then
            txtPrioEjecuc.Text = Val(ClearNull(aplRST!prio_ejecuc))
            txtInfoAdic.Text = ClearNull(aplRST!info_adicio)
            lblGrupo = ClearNull(aplRST!nom_grupo)
            lblEstadoGrupo = ClearNull(aplRST!nom_estado)
            sGerencia = ClearNull(aplRST!cod_gerencia)
            sDireccion = ClearNull(aplRST!cod_direccion)
            sSector = ClearNull(aplRST!cod_sector)
            sGrupo = ClearNull(aplRST!cod_grupo)
        End If
        aplRST.Close
    End If
End Sub

Sub Habilitar()
    cmdConfirmar.Enabled = True
    cmdCerrar.Enabled = True
    fraSector.Enabled = True
    Call setHabilCtrl(txtInfoAdic, "NOR")
    Call setHabilCtrl(txtPrioEjecuc, "NOR")
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = False
    CamposObligatorios = True
End Function
