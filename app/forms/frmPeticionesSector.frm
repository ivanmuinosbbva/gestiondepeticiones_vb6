VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionesSector 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Sectores Intervinientes"
   ClientHeight    =   6675
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   10500
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6675
   ScaleWidth      =   10500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Left            =   45
      TabIndex        =   14
      Top             =   4320
      Width           =   9015
      Begin VB.ComboBox cboSector 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1035
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   240
         Width           =   7455
      End
      Begin VB.TextBox obsSolic 
         Height          =   1260
         Left            =   1020
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   15
         Top             =   960
         Width           =   7425
      End
      Begin VB.Label lblAccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5940
         TabIndex        =   19
         Top             =   300
         Width           =   585
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   555
      End
      Begin VB.Label Label3 
         Caption         =   "Observaci�n al estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   720
         Width           =   2610
      End
   End
   Begin VB.Frame fraEncabezado 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   45
      TabIndex        =   7
      Top             =   0
      Width           =   9015
      Begin VB.Label lblResponsableNombre 
         Caption         =   "lblResponsableNombre"
         Height          =   195
         Left            =   1440
         TabIndex        =   24
         Top             =   735
         Width           =   6540
      End
      Begin VB.Label lblEncabezado 
         AutoSize        =   -1  'True
         Caption         =   "Responsable"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   60
         TabIndex        =   23
         Top             =   720
         Width           =   1080
      End
      Begin VB.Label lblEncabezado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   60
         TabIndex        =   13
         Top             =   480
         Width           =   645
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         Height          =   195
         Left            =   8400
         TabIndex        =   12
         Top             =   480
         Width           =   480
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         Height          =   195
         Left            =   1440
         TabIndex        =   11
         Top             =   240
         Width           =   7365
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         Height          =   195
         Left            =   1440
         TabIndex        =   10
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label lblEncabezado 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   7485
         TabIndex        =   9
         Top             =   480
         Width           =   765
      End
      Begin VB.Label lblEncabezado 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   60
         TabIndex        =   8
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame fraBotoneraSuperior 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4300
      Left            =   9120
      TabIndex        =   6
      Top             =   0
      Width           =   1335
      Begin VB.CommandButton cmdGrupo 
         Caption         =   "Grupos del sector"
         Height          =   675
         Left            =   60
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   1560
         Width           =   1170
      End
      Begin VB.CommandButton cmdCambiarEstado 
         Caption         =   "Cambio estado del sector"
         Height          =   675
         Left            =   60
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   855
         Width           =   1170
      End
      Begin VB.CommandButton cmdReasignar 
         Caption         =   "Reasignar sector"
         Height          =   675
         Left            =   60
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   150
         Width           =   1170
      End
   End
   Begin VB.Frame fraBotoneraInferior 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Left            =   9120
      TabIndex        =   1
      Top             =   4320
      Width           =   1335
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "Cerrar"
         Height          =   495
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   1740
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   495
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1260
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   495
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   780
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   495
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   300
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdSector 
      Height          =   2730
      Left            =   0
      TabIndex        =   0
      Top             =   1560
      Width           =   9060
      _ExtentX        =   15981
      _ExtentY        =   4815
      _Version        =   393216
      Cols            =   14
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPeticionesSector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 28.02.2008 - Homologaci�n
' -004- a. FJS 27.05.2008 - Cambio de estado para FINALIZADO
' -004- b. FJS 17.05.2010 - Se agrega el estado ANEXAD (peticiones Anexadas) que tambi�n indica un estado terminal.
' -004- c. FJS 18.05.2010 - Se agrega la parametr�a necesaria para inhabilitar el c�digo -004- a.
' -005- a. FJS 09.06.2008 - Se agrega un mensaje personalizado para el grupo Homologador cuando un nuevo sector es agregado a la petici�n.
' -006- a. FJS 31.12.2008 - Se agrega para evitar la perdida del dato en el historial.
' -007- a. FJS 08.01.2009 - Se quita para optimizar la carga de la grilla (se comenta por si resulta util en otro momento).
' -007- b. FJS 08.01.2009 - Se optimiza el SP de sectores y se obtienen los datos del SP original de la petici�n (performance).
' -008- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -009- a. FJS 28.01.2009 - Se agregan algunas mejoras a la usabilidad de la pantalla para mejorar la experiencia del usuario.
' -010- a. FJS 15.04.2009 - Replanificaciones.
' -011- a. FJS 01.09.2009 - Se agrega una columna nueva para mostrar la fecha en que el sector adopt� su �ltimo estado.
' -011- b. FJS 01.09.2009 - Se modifica el texto por defecto que se guarda al eliminar un Sector existente para dejar clarificado el motivo.
' -011- c. FJS 01.09.2009 - Se agrega c�digo para regularizar el estado de la petici�n cuando un sector es eliminado.
' -012- a. FJS 28.01.2010 - Se agrega la visualizaci�n del responsable del sector seleccionado.
' -013- a. FJS 05.03.2010 - Se agrega un indicador al usuario cuando los datos estan siendo cargados.
' -014- a. FJS 30.03.2010 - Se modifica el separador de textos (en vez de >> se usa �).
' -015- a. FJS 30.03.2010 - Cuando se intente agregar un sector, se vuelve a cargar el combo solo con los sectores habilitados.
' -016- a. FJS 04.06.2010 - Cuando la petici�n se encuentre en estado "Al Business Partner", solo se permite agregar sectores en estado "En opini�n".
' -017- a. FJS 15.09.2010 - Planificaci�n DYD.
' -018- a. FJS 03.08.2015 - Arreglo: se evita la sobrecarga del m�todo con el doble click sobre la grilla, que provoca un error de runtime.
' -019- a. FJS 10.02.2016 - Nuevo: se agrega la funcionalidad para la nueva gerencia de BPE.

Option Explicit

Private Const colGrCodsector = 0
Private Const colGrSector = 1
Private Const colGrEstado = 2
Private Const colGrFeEstado = 3
Private Const colGrSituacion = 4
Private Const colGrHoras = 5
Private Const colGrFinicio = 6
Private Const colGrFfin = 7
Private Const colGrFiniReal = 8
Private Const colGrFfinReal = 9
Private Const colGrDireccion = 10
Private Const colGrGerencia = 11
Private Const colGrCodestado = 12
Private Const colGrhst_nrointerno_sol = 13

Dim sOpcionSeleccionada As String
Dim EstadoPeticion As String
Dim EstadoSolicitud As String
Dim sSector As String
Dim sGerencia As String
Dim sDireccion As String
Dim sCoorSect As String
Dim xPerfNivel, xPerfArea As String
Dim sPrioridad As String
Dim sTipoPet As String
Dim cPeticionClase As String            ' add -003- a.
Dim cEstadoOriginal As String           ' add -new- !!!
Dim sGestion As String                  ' add -019- a.
Dim flgIngerencia As Boolean

Private Sub Form_Load()
    Call InicializarCombos
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdSector)
End Sub

Sub InicializarCombos(Optional sOpcion As String)    ' upd -015- a.
    Dim sDireccion As String
    Dim sGerencia As String
    
    If IsMissing(sOpcion) Or sOpcion = "" Then      ' add -015- a.
        If sp_GetSectorXt(Null, Null, Null) Then
            cboSector.Clear     ' add -015- a.
            Do While Not aplRST.EOF
                If ClearNull(aplRST!es_ejecutor) = "S" Then
                    'cboSector.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(150) & "||" & aplRST(0) & "||" & aplRST(2)     ' add -004- a.  ' upd -014- a.
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboSector.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_sector)
                End If
                aplRST.MoveNext
            Loop
        End If
    '{ add -015- a.
    Else
        If sp_GetSectorXt(Null, Null, Null, "S") Then
            cboSector.Clear
            Do While Not aplRST.EOF
                If ClearNull(aplRST!es_ejecutor) = "S" Then
                    'cboSector.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(150) & "||" & aplRST(0) & "||" & aplRST(2)
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboSector.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_sector)
                End If
                aplRST.MoveNext
            Loop
        End If
    End If
    '}
End Sub

Public Sub InicializarPantalla()
    Dim auxNro As String
    
    Me.Tag = ""
    Call LockProceso(True)
    
    xPerfNivel = getPerfNivel(glUsrPerfilActual)
    xPerfArea = getPerfArea(glUsrPerfilActual)
    sSector = ""
    cmdAgregar.Enabled = False
    glForzarEvaluar = False
    lblResponsableNombre = "-"
    
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
            auxNro = "S/N"
        Else
            auxNro = ClearNull(aplRST!pet_nroasignado)
        End If
        lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
        lblPrioridad = IIf(ClearNull(aplRST!prioridad) = "", "-", ClearNull(aplRST!prioridad))
        sPrioridad = ClearNull(aplRST!prioridad)
        EstadoPeticion = ClearNull(aplRST!cod_estado)
        lblEstado.Caption = ClearNull(aplRST!nom_estado)
        sTipoPet = ClearNull(aplRST!cod_tipo_peticion)
        cPeticionClase = ClearNull(aplRST!cod_clase)                    ' add -003- a.
        sGestion = ClearNull(aplRST!pet_driver)                         ' add -019- a.
    End If
    cmdReasignar.visible = IIf(InStr(1, "ADMI|SADM|", glUsrPerfilActual, vbTextCompare) > 0, True, False)
    EstadoSolicitud = ""
    Select Case EstadoPeticion
        Case "OPINIO", "EVALUA", "ESTIMA", "PLANIF"
            EstadoSolicitud = EstadoPeticion
        Case "ESTIOK"
            EstadoSolicitud = "ESTIMA"
        Case "PLANOK"
            EstadoSolicitud = "PLANIF"
    End Select
    Call HabilitarBotones(0)
    Call LockProceso(False)
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    cmdAgregar.Enabled = False
    cmdEliminar.Enabled = False
    cmdConfirmar.Enabled = False
    cmdGrupo.Enabled = False
    cmdCambiarEstado.Enabled = False
    cmdReasignar.Enabled = False
    cmdCerrar.Enabled = True
    lblAccion = ""
    cboSector.Enabled = False
    obsSolic.Locked = True
    Select Case nOpcion
        Case 0
            cmdCerrar.Caption = "Cerrar"
            Call InicializarCombos                                      ' add -015- a.
            cboSector.ListIndex = -1
            grdSector.Enabled = True
            Call VerAgregar
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            cmdAgregar.Enabled = False
            cmdEliminar.Enabled = False                                 ' add -019- a.
            grdSector.Enabled = False
            Select Case sOpcionSeleccionada
                Case "A"
                    Call InicializarCombos(sOpcionSeleccionada)        ' add -015- a.
                    cboSector.Enabled = True
                    cmdConfirmar.Enabled = True
                    obsSolic.Locked = False
                    obsSolic.text = ""
                    cmdConfirmar.Enabled = True
                    cmdCerrar.Caption = "Cancelar"
                    lblAccion = "Nuevo Sector"
                Case "E"
                    cmdConfirmar.Enabled = True
                    cmdCerrar.Caption = "Cancelar"
                    lblAccion = "ELIMINAR SECTOR"
            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Sub cboSector_Click()
    'glGerencia = Trim(Right(Me.cboSector, 8))       ' add -004- a.
    'glSector = CodigoCombo(Me.cboSector, True)
End Sub

Private Sub cmdCerrar_Click()
    If sOpcionSeleccionada = "" Then
        Unload Me
    Else
        sOpcionSeleccionada = ""
        cmdCerrar.Caption = "Cerrar"
        Call HabilitarBotones(0, False)
        EstadoSolicitud = cEstadoOriginal   ' add -new-!!!
    End If
End Sub

Private Sub cmdAgregar_Click()
    '{ add -003- a. Si la petici�n es "Sin clase", solo el Business Partner es el autorizado
    '               para agregar nuevos sectores a la petici�n. Si la petici�n esta clasificada
    '               entonces pueden agregar sectores todos los involucrados (del esquema actual)
    '               Agregado: una vez que una petici�n recibe el conforme de pruebas del usuario
    '               de caracter final (TEST) no pueden agregarse nuevos Sectores a la petici�n.
    If Not (glUsrPerfilActual = "ADMI" Or glUsrPerfilActual = "SADM") Then
        If sp_GetPeticionConf(glNumeroPeticion, "TEST", Null) Then
            MsgBox "La petici�n ya posee un conforme final a las pruebas de usuario." & vbCrLf & _
                    "No pueden agregarse nuevos sectores.", vbExclamation + vbOKOnly, "Agregar nuevo sector"
            Exit Sub
        End If
    End If
    '}
    cEstadoOriginal = EstadoSolicitud       ' add -new-!!!
    If EstadoSolicitud = "" Then
        EstadoSolicitud = PedirEstado()
        If EstadoSolicitud = "" Then
            'MsgBox ("Falta Asignar Estado Solicitud")
            Call LockProceso(False)
            Exit Sub
        End If
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Dim NroHistorial As Long
    Dim NuevoEstadoPeticion As String
    
    If Not LockProceso(True) Then Exit Sub
    glSector = CodigoCombo(Me.cboSector, True)
    Select Case sOpcionSeleccionada
        Case "A"
           If CamposObligatorios Then
                If Not valSectorHabil(glSector) Then
                    MsgBox "El sector est� marcado como inhabilitado.", vbExclamation + vbOKOnly
                    Call LockProceso(False)
                    Exit Sub
                End If
                If EstadoSolicitud = "ACONFI" Then
                    If Not sp_GetRecursoActuanteEstado("SEC", "SECT", glSector, "PLANIF") Then
                        MsgBox "No existen recursos que puedan procesar esta petici�n.", vbExclamation + vbOKOnly
                        Call LockProceso(False)
                        Exit Sub
                    End If
                Else
                    If Not sp_GetRecursoActuanteEstado("SEC", "SECT", glSector, EstadoSolicitud) Then
                        MsgBox "No existen recursos que puedan procesar esta petici�n.", vbExclamation + vbOKOnly
                        Call LockProceso(False)
                        Exit Sub
                    End If
                End If
                Call Puntero(True)
                Call Status("Alta de un sector...")
                If sp_InsertPeticionSector(glNumeroPeticion, glSector, Null, Null, Null, Null, 0, EstadoSolicitud, date, "", 0, "0") Then
                    If EstadoSolicitud <> "ACONFI" Then
                        Call sp_DoMensaje("SEC", "SNEW000", glNumeroPeticion, glSector, EstadoSolicitud, "", "")
                    End If
'                    '{ add -005- a. Mensaje al grupo Homologador
'                    ' Si existe vinculado a la petici�n en estado activo el grupo Homologador,
'                    ' envio un mensaje personalizado avisando del agregado del nuevo grupo.
'                    Dim cGrupoHomologador As String
'                    Dim cGrupoHomologadorSector As String
'                    Dim cGrupoHomologadorEstado As String
'                    Dim cMensaje As String
'                    ' 1. Primero determina si existe para la petici�n el grupo Homologador,
'                    ' y si existe envia el mensaje
'                    If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
'                        'If Not aplRST.EOF Then
'                        ' 2. Obtengo el grupo Homologador
'                        If sp_GetGrupoHomologacion Then
'                            'If Not aplRST.EOF Then
'                            cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
'                            cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
'                            'End If
'                        End If
'                        ' 4. Genero el mensaje para los Responsables de Grupo Homologador
'                        cMensaje = "El nuevo sector agregado a la petici�n es: " & Trim(Left(cboSector, Len(cboSector) - 20))
'                        Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 111, EstadoSolicitud, cMensaje)
'                        'End If
'                    End If
'                    '}
                    'If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 111, EstadoSolicitud, "El sector agregado a la petici�n es: " & TextoCombo(Me.cboSector, CodigoCombo(Me.cboSector, True)))
                    If sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 111, EstadoSolicitud, "El sector agregado a la petici�n es: " & TextoCombo(Me.cboSector, CodigoCombo(Me.cboSector, True)))
                    Call Status("Integrando el estado de la petici�n...")
                    NuevoEstadoPeticion = EstadoPeticion
                    If EstadoSolicitud <> "ACONFI" Then
                        If InStr(1, "COMITE|REFBPE|", EstadoPeticion, vbTextCompare) > 0 Then           ' Si la petici�n se encuentra en estado "Al referente de Sistemas/RGP"
                            NuevoEstadoPeticion = EstadoSolicitud                                       ' El nuevo estado ser� el seleccionado en la solicitud de nuevo estado
                        ElseIf EstadoSolicitud <> "OPINIO" And EstadoSolicitud <> "EVALUA" Then
                            NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
                        End If
                    End If
                    Call Status("Historial...")
                    NroHistorial = sp_AddHistorial(glNumeroPeticion, "SNEW000", NuevoEstadoPeticion, glSector, EstadoSolicitud, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, IIf(ClearNull(obsSolic.text) = "", "", "� " & Trim(obsSolic.text) & " �"))
                    'esta llamada es solo para darle el numero de historial
                    Call sp_UpdatePeticionSector(glNumeroPeticion, glSector, Null, Null, Null, Null, 0, EstadoSolicitud, date, "", NroHistorial, "0")
                    Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
                    Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)     ' Ver por qu� Cancela 03.04.2012!!!
                    If NuevoEstadoPeticion <> EstadoPeticion Then
                        'integra fechas de sectores
                        Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
                        If NuevoEstadoPeticion <> "TERMIN" Then
                            fFinReal = Null
                        End If
                        If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoPeticion) > 0 Then
                            Call sp_UpdatePetFechas(glNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
                        Else
                            Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
                        End If
                    End If
                    Call InicializarPantalla
                    sOpcionSeleccionada = ""
                    Call touchForms ':flgAccion = True
                    Call Status("Listo.")
                End If
            End If
        Case "E"
            ' ***************************************
            ' FALTA ELIMINAR DE PLANIFICACION!!!
            ' ***************************************
            Call Puntero(True)
            ' Aqu� es eliminado el sector (y todos los grupos que pertenecen al sector)
            Call sp_DeletePeticionSector(glNumeroPeticion, ClearNull(grdSector.TextMatrix(grdSector.rowSel, colGrCodsector)))
            Call Status("Integraci�n petici�n...")
            Call chkSectorOpiEva(glNumeroPeticion)
            '{ add -011- c. Es evaluado el estado de la petici�n a partir de los sectores remanentes. Si corresponde cambiar el estado de la petici�n, se hace.
            NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
            If EstadoPeticion <> NuevoEstadoPeticion Then
                Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
            End If
            '}
            'NroHistorial = sp_AddHistorial(glNumeroPeticion, "SDEL000", EstadoPeticion, glSector, "", "", "", glLOGIN_ID_REEMPLAZO, "� " & Trim(obsSolic.Text) & " �") ' del -011- b.
            'NroHistorial = sp_AddHistorial(glNumeroPeticion, "SDEL000", EstadoPeticion, glSector, grdSector.TextMatrix(grdSector.RowSel, colGrCodestado), "", "", glLOGIN_ID_REEMPLAZO, "� Sector eliminado � " & IIf(obsSolic = "�  �", "", Trim(obsSolic.text)))
            NroHistorial = sp_AddHistorial(glNumeroPeticion, "SDEL000", EstadoPeticion, glSector, grdSector.TextMatrix(grdSector.rowSel, colGrCodestado), "", "", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Sector eliminado" & IIf(obsSolic = "", " �", Trim(obsSolic.text) & " �"))
            Call InicializarPantalla
            sOpcionSeleccionada = ""
            Call touchForms
            Call Status("Listo.")
    End Select
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Private Sub cmdCambiarEstado_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If sSector = "" Then
        MsgBox ("Debe Seleccionar un Sector")
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    glSector = sSector
    Load frmPeticionesSectorOperar
    frmPeticionesSectorOperar.Show 1
    DoEvents
    Call InicializarPantalla
End Sub

Private Sub cmdReasignar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If sSector = "" Then
        Call LockProceso(False)
        MsgBox ("Debe Seleccionar un Sector")
        Exit Sub
    End If
    Call LockProceso(False)
    glSector = sSector
    frmPeticionesSectorReasignar.Show vbModal
    DoEvents
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla
    End If
End Sub

Private Sub cmdGrupo_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If sSector = "" Then
        Call LockProceso(False)
        'MsgBox ("Debe Seleccionar un Sector")
        Exit Sub
    End If
    glSector = sSector
    Call Puntero(True)
    frmPeticionesGrupo.Show vbModal
    DoEvents
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla
    End If
End Sub

Function CamposObligatorios() As Boolean
    Dim cMensaje As String      ' add -004- a.
    
    CamposObligatorios = False
    sDireccion = ""
    sGerencia = ""
    
    '{ add -004- a.
    If cPeticionClase = "SINC" Then
        If InStr(1, "GBPE|BPAR|SADM|ADMI|", glUsrPerfilActual, vbTextCompare) = 0 Then
            If InStr(1, "NOR|PRJ|ESP|AUI|AUX|", sTipoPet, vbTextCompare) > 0 And sGestion = "BPE" Then
                cMensaje = "No puede agregar sectores a una petici�n sin clase." & vbCrLf & vbCrLf & _
                           "Solo Ref. de Sistema o el Ref. de RGP puede agregar" & vbCrLf & _
                           "sectores en peticiones que a�n no han sido clasificadas."
                MsgBox cMensaje, vbExclamation + vbOKOnly, "Agregar sector"
                Exit Function
            ElseIf InStr(1, "PRO|ESP|", sTipoPet, vbTextCompare) > 0 And sGestion = "DESA" Then
                cMensaje = "No puede agregar sectores a una petici�n sin clase." & vbCrLf & vbCrLf & _
                           "Solo Resp. de Ejecuci�n o Resp. de Sector puede agregar" & vbCrLf & _
                           "sectores en peticiones que a�n no han sido clasificadas."
                MsgBox cMensaje, vbExclamation + vbOKOnly, "Agregar sector en Propias"
                Exit Function
            End If
        End If
    End If
    '}
    If cboSector.ListIndex < 0 Then
        MsgBox "Debe especificar un sector.", vbExclamation, vbOKOnly
        Exit Function
    End If

    If EstadoSolicitud = "" Then
        MsgBox "Debe especificar estado solicitud.", vbExclamation, vbOKOnly
        Exit Function
    End If
    '{ del -017- a.
    'If (EstadoSolicitud = "ESTIMA" Or EstadoSolicitud = "PLANIF") And sPrioridad = "" Then
    '    MsgBox ("La Petici�n debe tener Prioridad asignada para Estimar/Planificar")
    '    Exit Function
    'End If
    '}
    CamposObligatorios = True
End Function

Private Sub CargarGrid()
    With grdSector
        .Clear
        .Rows = 1
        .RowHeightMin = 265
        .BackColorSel = RGB(58, 110, 165)
        .TextMatrix(0, colGrCodsector) = "Cod.Sec.": .ColWidth(colGrCodsector) = 800: .ColAlignment(colGrCodsector) = flexAlignLeftCenter
        .TextMatrix(0, colGrSector) = "Sector": .ColWidth(colGrSector) = 2800: .ColAlignment(colGrSector) = flexAlignLeftCenter
        .TextMatrix(0, colGrEstado) = "Estado": .ColWidth(colGrEstado) = 2300: .ColAlignment(colGrEstado) = flexAlignLeftCenter
        .TextMatrix(0, colGrFeEstado) = "F.Estado": .ColWidth(colGrFeEstado) = 950: .ColAlignment(colGrFeEstado) = flexAlignLeftCenter  ' add -011- a.
        .TextMatrix(0, colGrSituacion) = "Situaci�n": .ColWidth(colGrSituacion) = 1300: .ColAlignment(colGrSituacion) = flexAlignLeftCenter
        .TextMatrix(0, colGrHoras) = "Horas": .ColWidth(colGrHoras) = 700:
        .TextMatrix(0, colGrFinicio) = "F.Ini.Planif": .ColWidth(colGrFinicio) = 1200:
        .TextMatrix(0, colGrFfin) = "F.Fin Planif.": .ColWidth(colGrFfin) = 1200:
        .TextMatrix(0, colGrFiniReal) = "F.Ini.Real": .ColWidth(colGrFiniReal) = 1200:
        .TextMatrix(0, colGrFfinReal) = "F.Fin.Real": .ColWidth(colGrFfinReal) = 1200:
        .TextMatrix(0, colGrDireccion) = "Dir": .ColWidth(colGrDireccion) = 0       '400:
        .TextMatrix(0, colGrGerencia) = "Ger": .ColWidth(colGrGerencia) = 0     '400:
        .TextMatrix(0, colGrhst_nrointerno_sol) = "TxtSol": .ColWidth(colGrhst_nrointerno_sol) = flexAlignLeftCenter      '400:
        .ColWidth(colGrCodestado) = 0       '400:
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusNone
        Call CambiarEfectoLinea(grdSector, prmGridEffectFontBold)
    End With
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Sub
    Do While Not aplRST.EOF
        With grdSector
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colGrSector) = ClearNull(aplRST!nom_sector)
            .TextMatrix(.Rows - 1, colGrEstado) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colGrFeEstado) = ClearNull(aplRST!fe_estado)  ' add -011- a.
            .TextMatrix(.Rows - 1, colGrSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colGrFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrFfin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrFiniReal) = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrFfinReal) = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrHoras) = IIf(Not IsNull(aplRST!horaspresup), aplRST!horaspresup, "")
            .TextMatrix(.Rows - 1, colGrGerencia) = ClearNull(aplRST!cod_gerencia)
            .TextMatrix(.Rows - 1, colGrCodestado) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colGrDireccion) = ClearNull(aplRST!cod_direccion)
            .TextMatrix(.Rows - 1, colGrCodsector) = ClearNull(aplRST!cod_sector)
            .TextMatrix(.Rows - 1, colGrhst_nrointerno_sol) = ClearNull(aplRST!hst_nrointerno_sol)
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdSector, prmGridFillRowColorLightGrey2)
            'if ClearNull(aplRST!cod_sector)
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    Dim sEstado As String
    Dim bPuedoCambiarEstadoSector As Boolean
    
    'cmdEliminar.Enabled = False
    cmdCambiarEstado.Enabled = False
    cmdReasignar.Enabled = False
    sSector = ""
    grdSector.Enabled = False

    With grdSector
        If .rowSel > 0 Then
            If .TextMatrix(.rowSel, colGrCodsector) <> "" Then
                glGerencia = .TextMatrix(.rowSel, colGrGerencia)
                glDireccion = .TextMatrix(.rowSel, colGrDireccion)
                glSector = .TextMatrix(.rowSel, colGrCodsector)
                sEstado = .TextMatrix(.rowSel, colGrCodestado)
                sSector = .TextMatrix(.rowSel, colGrCodsector)
                glSector = sSector
                cboSector.ListIndex = PosicionCombo(Me.cboSector, .TextMatrix(.rowSel, colGrCodsector), True)
                If Val(.TextMatrix(.rowSel, colGrhst_nrointerno_sol)) > 0 Then
                    'Call Puntero(True)              ' add -013- a.
                    obsSolic.text = sp_GetHistorialMemo(.TextMatrix(.rowSel, colGrhst_nrointerno_sol))
                    'Call Puntero(False)             ' add -013- a.
                Else
                    obsSolic.text = ""
                End If
                cmdGrupo.Enabled = True
                'cmdEliminar.Enabled = False
                Select Case glUsrPerfilActual
                    Case "ADMI"
                        If sEstado = "OPINIO" Or sEstado = "EVALUA" Then
                            cmdEliminar.Enabled = True
                        End If
                        If sEstado <> "OPINIO" And sEstado <> "EVALUA" Then
                            cmdReasignar.Enabled = True
                        End If
                    Case "SADM"
                        cmdReasignar.Enabled = True
                        cmdEliminar.Enabled = True
                End Select
                bPuedoCambiarEstadoSector = False
                If xPerfNivel = "BBVA" Or _
                    xPerfNivel = "DIRE" And glDireccion = xPerfArea Or _
                    xPerfNivel = "GERE" And glGerencia = xPerfArea Or _
                    xPerfNivel = "SECT" And glSector = xPerfArea Then
                    'If sp_GetAccionPerfil("SCHGEST", glUsrPerfilActual, sEstado, Null, Null) Then
                    If sp_GetAccionPerfilEstados(1, "SCHGEST", glUsrPerfilActual, sEstado, Null, Null) Then
                        bPuedoCambiarEstadoSector = True
                        'Call setHabilCtrl(cmdCambiarEstado, NORMAL)
                    End If
                    If bPuedoCambiarEstadoSector Then
                        If Not sp_GetPeticionGrupo(glNumeroPeticion, .TextMatrix(.rowSel, colGrCodsector), Null) Then
                            'Call setHabilCtrl(cmdCambiarEstado, DISABLE)
                            Call setHabilCtrl(cmdCambiarEstado, NORMAL)
                        End If
                    End If
                End If
                '{ add -012- a.
                If sp_GetRecursoAcargoArea(.TextMatrix(.rowSel, colGrDireccion), .TextMatrix(.rowSel, colGrGerencia), .TextMatrix(.rowSel, colGrCodsector), "") Then
                    lblResponsableNombre = ClearNull(aplRST.Fields!nom_recurso)
                Else
                    lblResponsableNombre = "-"
                End If
                '}
            End If
        End If
        .Enabled = True
    End With
    grdSector.Enabled = True
End Sub

Private Sub grdSector_Click()
    DoEvents
    Call MostrarSeleccion
    DoEvents
End Sub

'{ add -002- a.
Private Sub grdSector_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdSector_Click
    End If
End Sub
'}

Private Sub grdSector_DblClick()
    DoEvents
    Call cmdGrupo_Click     ' add -018- a.
    '{ del -018- a.
    'Debug.Print "Doble click en grdSector"
    'Call MostrarSeleccion
    'DoEvents
    'If sSector <> "" Then
    '    If Not LockProceso(True) Then
    '        Exit Sub
    '    End If
    '    glSector = sSector
    '    On Error Resume Next
    '    frmPeticionesGrupo.Show vbModal
    '    DoEvents
    '    InicializarPantalla
    'End If
    '}
End Sub

Function PedirEstado() As String
    ' Si el estado de la petici�n es:
    ' - En confecci�n
    ' - Devuelto al solicitante
    ' - Al referente
    ' - Devuelto al referente
    ' Entonces el estado es "A confirmar", lo cual paraliza la operatoria hasta
    ' que no se reciba confirmaci�n del solicitante o referente.
    If InStr("CONFEC|DEVSOL|REFERE|DEVREF", EstadoPeticion) > 0 Then
        PedirEstado = "ACONFI"
        Exit Function
    End If
    'PedirEstado = "OPINIO"      ' add -016- a.
    '{ del -016- a.
    PedirEstado = ""
    Load auxEstadoSolicitud
    auxEstadoSolicitud.iniEstado = EstadoPeticion
    auxEstadoSolicitud.setOption
    auxEstadoSolicitud.Show 1
    If auxEstadoSolicitud.bAceptar = True Then
        PedirEstado = auxEstadoSolicitud.pubEstado
    End If
    Unload auxEstadoSolicitud
    '}
End Function

Private Sub VerAgregar()
    '{ del -019- a.
    'If sp_GetAccionPerfil("SNEW000", glUsrPerfilActual, EstadoPeticion, Null, Null) Then
    '    If Not aplRST.EOF Then
    '        cmdAgregar.Enabled = True
    '    End If
    'End If
    '}
    '{ del -004- c.
    ''{ add -004- a. Se fuerza que nunca se pueda agregar un nuevo sector a una petici�n en estados "terminales"
    'If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD|", ClearNull(EstadoPeticion), vbTextCompare) > 0 Then    ' upd -004- b.
    '    If glUsrPerfilActual = "SADM" Then
    '        cmdAgregar.Enabled = True
    '    Else
    '        cmdAgregar.Enabled = False
    '    End If
    'End If
    '}
    '}
'    '{ add -019- a.
'    Call setHabilCtrl(cmdAgregar, "NOR")
'    If sp_GetAccionPerfil("SNEW000", glUsrPerfilActual, EstadoPeticion, Null, Null) Then
'        If InStr(1, "GBPE|BPAR|SADM|ADMI|", glUsrPerfilActual, vbTextCompare) = 0 Then
'            If cPeticionClase = "SINC" Then
'                Call setHabilCtrl(cmdAgregar, "DIS")
'            End If
'        End If
'    End If
'    '}
    
    '{ add -019- a.
    If sp_GetAccionPerfil("SNEW000", glUsrPerfilActual, EstadoPeticion, Null, Null) Then
        Call setHabilCtrl(cmdAgregar, "NOR")
    End If
    If sp_GetAccionPerfil("SDEL000", glUsrPerfilActual, EstadoPeticion, Null, Null) Then
        Call setHabilCtrl(cmdEliminar, "NOR")
    End If
    '}
End Sub

'{ add -008- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdSector)
End Sub
'}

'{ add -009- a.
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then       ' Tecla: ESCAPE
        cmdCerrar_Click
    End If
End Sub
'}

