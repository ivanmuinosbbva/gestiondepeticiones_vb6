VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSector 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "SECTORES"
   ClientHeight    =   7170
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   11565
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7170
   ScaleWidth      =   11565
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame pnlBotones 
      Height          =   7140
      Left            =   10140
      TabIndex        =   9
      Top             =   0
      Width           =   1395
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Imprimir"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector"
         Top             =   780
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.CheckBox chkHab 
         Caption         =   "Solo habilitados"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   6060
         Width           =   1275
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   6570
         Width           =   1275
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Sector seleccionado"
         Top             =   5550
         Width           =   1275
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el Sector seleccionado"
         Top             =   5040
         Width           =   1275
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector"
         Top             =   4530
         Width           =   1275
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3750
      Left            =   0
      TabIndex        =   4
      Top             =   3390
      Width           =   10125
      Begin VB.ComboBox cboDemanda 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   2400
         Width           =   2500
      End
      Begin VB.ComboBox cboHabilitadoParaSolicitante 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   3120
         Width           =   2500
      End
      Begin VB.ComboBox cboBpar 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   2760
         Width           =   5145
      End
      Begin VB.ComboBox cboEjecutor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   2040
         Width           =   2500
      End
      Begin VB.ComboBox cboHabil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   1680
         Width           =   2505
      End
      Begin VB.ComboBox cboGerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1320
         Width           =   5625
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   315
         Left            =   2880
         TabIndex        =   2
         Top             =   600
         Width           =   5625
         _ExtentX        =   9922
         _ExtentY        =   556
         MaxLength       =   80
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   80
      End
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   315
         Left            =   2880
         TabIndex        =   1
         Top             =   240
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtCodAbrev 
         Height          =   315
         Left            =   2880
         TabIndex        =   3
         Top             =   960
         Width           =   705
         _ExtentX        =   1244
         _ExtentY        =   556
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   3
         UpperCase       =   -1  'True
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Recepci�n de la demanda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   240
         TabIndex        =   26
         Top             =   2348
         Width           =   1575
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Sector habilitado para peticiones especiales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   240
         TabIndex        =   25
         Top             =   3120
         Width           =   1800
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblReferenteDesignado 
         Caption         =   "R. Sistema por defecto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   22
         ToolTipText     =   "Referente de Sistema"
         Top             =   2835
         Width           =   2550
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Ejecutor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   20
         Top             =   2108
         Width           =   600
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Abreviatura"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   18
         Top             =   1020
         Width           =   855
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   17
         Top             =   300
         Width           =   495
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   16
         Top             =   1748
         Width           =   495
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   8
         Top             =   1388
         Width           =   630
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   6
         Top             =   660
         Width           =   810
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   5
         Top             =   30
         Visible         =   0   'False
         Width           =   45
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3360
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   5927
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -004- a. FJS 03.09.2009 - Se modifica la llamada a el SP que trae las gerencias para evitar mostrar y/o utilizar aquellas que ya se encuentran inhabilitadas.
' -005- a. FJS 06.10.2009 - Se agrega el par�metro que filtra por grupos habilitados o no.
' -005- b. FJS 06.10.2009 - Se agrega el coloreado din�mico para resaltar los grupos inhabilitados.
' -006- a. FJS 20.04.2015 - Nuevo: se agrega el atributo para habilitar al sector para ser seleccionado en las peticiones especiales.
' -007- a. FJS 22.01.2016 - Nuevo: se permite prefijar para un sector tanto Ref. de sistema (DyD) como Ref. de Gesti�n de Procesos (RGP).
' -007- b. FJS 01.02.2016 - Nuevo: para evitar ver el combo vacio cuando el referente designado ya dej� de tener el perfil, se guardan al inicio
'                           los datos del mismo, para agregar al combo, solo a efectos de visualizar los datos correctamente.

Option Explicit

Private Const colCODSECTOR = 0
Private Const colNOMSECTOR = 1
Private Const colCODABREV = 2
Private Const colCODGERENCIA = 3
Private Const colNOMGERENCIA = 4
Private Const colBPAR = 5
Private Const colEjecutor = 6
Private Const colHabil = 7
Private Const colPERFIL = 8
Private Const colBPARNOM = 9
'{ add -006- a.
Private Const colHABIL_SOLI = 10
Private Const colTOTAL_COLS = 11
'}

Dim sOpcionSeleccionada As String
Dim sOldGerencia As String
Dim bLoading As Boolean
'Dim cBparAnterior(2) As String
'Dim cBpeAnterior(2) As String

Private Sub Form_Load()
    Call InicializarCombos
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)                    ' add -003- a.
    Me.Left = 0
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    Call LimpiarForm(Me)
    Call HabilitarBotones(0)
End Sub

Private Sub InicializarCombos()
    'If sp_GetGerencia("", "", "S") Then     ' upd -004- a. Se agrega el par�metro opcional "S" para traer solo las habilitadas
    If sp_GetGerencia("", "") Then
        Do While Not aplRST.EOF
            cboGerencia.AddItem aplRST(0) & " : " & ClearNull(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
    cboHabil.AddItem "S : Habilitado"
    cboHabil.AddItem "N : Inhabilitado"
    
    With cboBpar
        .Clear
        If sp_GetRecursoPerfil(Null, "BPAR") Then
            Do While Not aplRST.EOF
                cboBpar.AddItem aplRST!cod_recurso & " : " & ClearNull(aplRST!nom_recurso)
                aplRST.MoveNext
            Loop
        End If
        '{ add -007- a.
        If sp_GetRecursoPerfil(Null, "GBPE") Then
            Do While Not aplRST.EOF
                cboBpar.AddItem aplRST!cod_recurso & " : " & ClearNull(aplRST!nom_recurso)
                aplRST.MoveNext
            Loop
        End If
        '.AddItem "-- SIN REFERENTE --" & ESPACIOS & "||NULL", 0
        '}
    End With
    cboEjecutor.AddItem "S : Ejecuta pedidos"
    cboEjecutor.AddItem "N : No ejecuta"
    '{ add -006- a.
    With cboHabilitadoParaSolicitante
        .Clear
        .AddItem "S : Habilitado"
        .AddItem "N : Inhabilitado"
    End With
    '}
    
    '{ add -007- a.
    With cboDemanda
        .Clear
        .AddItem "Ref. de Sistemas" & ESPACIOS & "||BPAR"
        .AddItem "Ref. de RGP" & ESPACIOS & "||GBPE"
        .ListIndex = 0
    End With
    '}
End Sub

Private Sub CargarCombo_Gerencia()
    With cboGerencia
        If sp_GetGerencia("", "", "S") Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem aplRST(0) & " : " & Trim(aplRST(1))
                aplRST.MoveNext
            Loop
            .ListIndex = PosicionCombo(cboGerencia, grdDatos.TextMatrix(grdDatos.RowSel, colCODGERENCIA), False)
        End If
    End With
End Sub

'{ add -007- a.
Private Sub CargarCombo_Referente()
    'Debug.Print "CargarCombo_Referente()"
    With cboBpar
        .Clear
        If CodigoCombo(cboDemanda, True) = "BPAR" Then
            If sp_GetRecursoPerfil(Null, "BPAR") Then
                Do While Not aplRST.EOF
                    .AddItem aplRST(0) & " : " & Trim(aplRST(1))
                    aplRST.MoveNext
                Loop
                .ListIndex = PosicionCombo(cboBpar, grdDatos.TextMatrix(grdDatos.RowSel, colBPAR), False)
            End If
        Else
            If sp_GetRecursoPerfil(Null, "GBPE") Then
                Do While Not aplRST.EOF
                    .AddItem aplRST(0) & " : " & Trim(aplRST(1))
                    aplRST.MoveNext
                Loop
                .ListIndex = PosicionCombo(cboBpar, grdDatos.TextMatrix(grdDatos.RowSel, colBPAR), False)
            End If
        End If
        '.AddItem "-- SIN REFERENTE --" & ESPACIOS & "||NULL", 0
    End With
End Sub
'}

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_UpdateSector("A", txtCodigo, txtDescripcion, CodigoCombo(Me.cboGerencia), txtCodAbrev, CodigoCombo(Me.cboHabil), CodigoCombo(Me.cboEjecutor), CodigoCombo(Me.cboBpar), CodigoCombo(Me.cboDemanda, True), CodigoCombo(cboHabilitadoParaSolicitante)) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sOldGerencia <> CodigoCombo(Me.cboGerencia) Then
                    If MsgBox("Est� cambiando la Gerencia, esto modificar� la estructura de dependencias. �Contin�a?", vbYesNo) = vbNo Then
                        Exit Sub
                    End If
                End If
                If CodigoCombo(Me.cboHabil) <> "S" Then
                    If MsgBox("Al inhabilitar el Sector dejar� inhabilitado todo su �rbol jer�rquico. �Contin�a?", vbYesNo) = vbNo Then
                        Exit Sub
                    End If
                End If
                If sp_UpdateSector("M", txtCodigo, txtDescripcion, CodigoCombo(Me.cboGerencia), txtCodAbrev, CodigoCombo(Me.cboHabil), CodigoCombo(Me.cboEjecutor), CodigoCombo(Me.cboBpar), CodigoCombo(Me.cboDemanda, True), CodigoCombo(cboHabilitadoParaSolicitante)) Then
                    If sOldGerencia <> CodigoCombo(Me.cboGerencia) Then
                        If Not sp_ChangeSectorGerencia(txtCodigo, CodigoCombo(Me.cboGerencia)) Then
                            MsgBox ("Hubo errores al cambiar el Sector de Gerencia")
                        End If
                    End If
                    With grdDatos
                        If .RowSel > 0 Then
                           .TextMatrix(.RowSel, colNOMSECTOR) = txtDescripcion
                           .TextMatrix(.RowSel, colCODABREV) = txtCodAbrev
                           .TextMatrix(.RowSel, colCODGERENCIA) = CodigoCombo(Me.cboGerencia)
                           .TextMatrix(.RowSel, colNOMGERENCIA) = TextoCombo(Me.cboGerencia)
                           .TextMatrix(.RowSel, colBPAR) = CodigoCombo(Me.cboBpar)
                           .TextMatrix(.RowSel, colHabil) = CodigoCombo(Me.cboHabil)
                           .TextMatrix(.RowSel, colEjecutor) = CodigoCombo(Me.cboEjecutor)
                           .TextMatrix(.RowSel, colHABIL_SOLI) = CodigoCombo(Me.cboHabilitadoParaSolicitante)       ' add -006- a.
                           .TextMatrix(.RowSel, colPERFIL) = CodigoCombo(Me.cboDemanda, True)                       ' add -007- a.
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeleteSector(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    txtCodigo = "": txtCodAbrev = "": txtDescripcion = ""
                    cboGerencia.ListIndex = -1
                    cboHabil.ListIndex = 0
                    cboDemanda.ListIndex = 0                                ' add -007- a.
                    Call CargarCombo_Referente                              ' add -007- b.
                    cboBpar.ListIndex = -1
                    cboEjecutor.ListIndex = 1
                    cboHabilitadoParaSolicitante.ListIndex = -1             ' add -006- a.
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    Call CargarCombo_Gerencia
                    Call CargarCombo_Referente                              ' add -007- b.
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(txtCodigo.text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtDescripcion.text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtDescripcion.text, ":") > 0 Then
        MsgBox ("La Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If cboGerencia.ListIndex < 0 Then
        MsgBox ("Debe asignarle una Gerencia")
        CamposObligatorios = False
        Exit Function
    End If
    If Not valGerenciaHabil(CodigoCombo(cboGerencia)) Then
        MsgBox ("No puede depender de una Gerencia inhabilitada")
        CamposObligatorios = False
        Exit Function
    End If
    If cboBpar.ListIndex < 0 Then
        If CodigoCombo(cboDemanda, True) = "BPAR" Then
            MsgBox ("Debe especificar el Referente de Sistema")
        Else
            MsgBox ("Debe especificar el Referente de RGP")
        End If
        CamposObligatorios = False
        Exit Function
    End If
    '{ add -006- a.
    If cboHabilitadoParaSolicitante.ListIndex < 0 Then
        MsgBox ("Debe especificar si el sector est� habilitado" & vbCrLf & "para ser usado en peticiones especiales.")
        CamposObligatorios = False
        Exit Function
    End If
    '}
End Function

Private Sub CargarGrid()
    bLoading = True
    With grdDatos
        .visible = False
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .cols = colTOTAL_COLS           ' upd -006- a.
        .TextMatrix(0, colCODSECTOR) = "CODIGO": .ColWidth(colCODSECTOR) = 930: .ColAlignment(colCODSECTOR) = 0
        .TextMatrix(0, colNOMSECTOR) = "DESCRIPCION": .ColWidth(colNOMSECTOR) = 3250: .ColAlignment(colNOMSECTOR) = 0
        .TextMatrix(0, colCODGERENCIA) = "": .ColWidth(colCODGERENCIA) = 930: .ColAlignment(colCODGERENCIA) = 0
        .TextMatrix(0, colCODABREV) = "Abrev.": .ColWidth(colCODABREV) = 800: .ColAlignment(colCODABREV) = flexAlignCenterCenter
        .TextMatrix(0, colNOMGERENCIA) = "GERENCIA": .ColWidth(colNOMGERENCIA) = 3250: .ColAlignment(colNOMGERENCIA) = 0
        '.TextMatrix(0, colBPAR) = "R.SISTEMA": .ColWidth(colBPAR) = 1000: .ColAlignment(colBPAR) = 0       ' del -007- a.
        .TextMatrix(0, colBPAR) = "REFERENTE": .ColWidth(colBPAR) = 1000: .ColAlignment(colBPAR) = 0        ' add -007- a.
        .TextMatrix(0, colHabil) = "HABILIT": .ColWidth(colHabil) = 800: .ColAlignment(colHabil) = flexAlignCenterCenter
        .TextMatrix(0, colEjecutor) = "EJEC": .ColWidth(colEjecutor) = 800: .ColAlignment(colEjecutor) = flexAlignCenterCenter
        '.TextMatrix(0, colABREVIATURA) = "Abrev.": .ColWidth(colABREVIATURA) = 0: .ColAlignment(colABREVIATURA) = 0
        .TextMatrix(0, colPERFIL) = "Perfil": .ColWidth(colPERFIL) = 1000: .ColAlignment(colPERFIL) = 0
        .TextMatrix(0, colHABIL_SOLI) = "Soli.": .ColWidth(colHABIL_SOLI) = 800: .ColAlignment(colHABIL_SOLI) = flexAlignLeftCenter     ' add -006- a.
        .TextMatrix(0, colBPARNOM) = "": .ColWidth(colBPARNOM) = 0: .ColAlignment(colBPARNOM) = flexAlignLeftCenter                     ' add -007- b.
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetSectorXt("", "", "", IIf(chkHab.value = 1, "S", Null)) Then Exit Sub   ' upd -005- a.
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODSECTOR) = ClearNull(aplRST.Fields.Item("cod_sector"))
            .TextMatrix(.Rows - 1, colCODABREV) = ClearNull(aplRST.Fields.Item("cod_abrev"))
            .TextMatrix(.Rows - 1, colNOMSECTOR) = ClearNull(aplRST.Fields.Item("nom_sector"))
            .TextMatrix(.Rows - 1, colCODGERENCIA) = ClearNull(aplRST.Fields.Item("cod_gerencia"))
            .TextMatrix(.Rows - 1, colNOMGERENCIA) = ClearNull(aplRST.Fields.Item("nom_gerencia"))
            .TextMatrix(.Rows - 1, colBPAR) = ClearNull(aplRST.Fields.Item("cod_bpar"))
            .TextMatrix(.Rows - 1, colEjecutor) = ClearNull(aplRST.Fields.Item("es_ejecutor"))
            .TextMatrix(.Rows - 1, colHabil) = IIf(ClearNull(aplRST.Fields.Item("flg_habil")) = "N", "N", "S")
            .TextMatrix(.Rows - 1, colHABIL_SOLI) = IIf(ClearNull(aplRST.Fields.Item("soli_hab")) = "S", "S", "N")  ' add -006- a.
            .TextMatrix(.Rows - 1, colPERFIL) = ClearNull(aplRST.Fields!cod_perfil)
            .TextMatrix(.Rows - 1, colBPARNOM) = ClearNull(aplRST.Fields!nom_bpar)                                  ' add -007- b.
            If .TextMatrix(.Rows - 1, colHabil) = "N" Then Call PintarLinea(grdDatos, prmGridFillRowColorRed)       ' add -005- b.
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    grdDatos.visible = True
    DoEvents
    Call MostrarSeleccion
    bLoading = False
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then grdDatos_Click
End Sub
'}

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 0) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODSECTOR))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNOMSECTOR))
                txtCodAbrev = ClearNull(.TextMatrix(.RowSel, colCODABREV))
                sOldGerencia = .TextMatrix(.RowSel, colCODGERENCIA)
                'cboGerencia.ListIndex = PosicionCombo(Me.cboGerencia, .TextMatrix(.RowSel, colCODGERENCIA))        ' del -007- b.
                '{ add -007- b.
                If PosicionCombo(cboGerencia, .TextMatrix(.RowSel, colCODGERENCIA)) = -1 Then
                    If ClearNull(.TextMatrix(.RowSel, colCODGERENCIA)) <> "" Then
                        cboGerencia.AddItem padLeft(.TextMatrix(.RowSel, colCODGERENCIA), 10) & " : " & .TextMatrix(.RowSel, colNOMGERENCIA)
                        cboGerencia.ListIndex = PosicionCombo(cboGerencia, .TextMatrix(.RowSel, colCODGERENCIA))
                    End If
                Else
                    cboGerencia.ListIndex = PosicionCombo(cboGerencia, .TextMatrix(.RowSel, colCODGERENCIA))
                End If
                '}
                cboHabil.ListIndex = PosicionCombo(Me.cboHabil, .TextMatrix(.RowSel, colHabil))
                cboEjecutor.ListIndex = PosicionCombo(Me.cboEjecutor, .TextMatrix(.RowSel, colEjecutor))
                'cboBpar.ListIndex = IIf(PosicionCombo(Me.cboBpar, .TextMatrix(.RowSel, colBPAR)) = -1, 0, PosicionCombo(Me.cboBpar, .TextMatrix(.RowSel, colBPAR)))
                'cboBpar.ListIndex = PosicionCombo(Me.cboBpar, .TextMatrix(.RowSel, colBPAR))                       ' del -007- b.
                '{ add -007- b.
                If PosicionCombo(Me.cboBpar, .TextMatrix(.RowSel, colBPAR)) = -1 Then
                    'Debug.Print .TextMatrix(.RowSel, colBPAR) & " : " & .TextMatrix(.RowSel, colBPARNOM)
                    If ClearNull(.TextMatrix(.RowSel, colBPAR)) <> "" Then
                        cboBpar.AddItem padLeft(.TextMatrix(.RowSel, colBPAR), 10) & " : " & .TextMatrix(.RowSel, colBPARNOM)
                        cboBpar.ListIndex = PosicionCombo(Me.cboBpar, .TextMatrix(.RowSel, colBPAR))
                    End If
                Else
                    cboBpar.ListIndex = PosicionCombo(Me.cboBpar, .TextMatrix(.RowSel, colBPAR))
                End If
                '}
                cboDemanda.ListIndex = PosicionCombo(Me.cboDemanda, .TextMatrix(.RowSel, colPERFIL), True)
                If CodigoCombo(cboDemanda, True) = "BPAR" Then
                    lblReferenteDesignado = "Ref. de Sistema por defecto"
                Else
                    lblReferenteDesignado = "Ref. de RGP por defecto"
                End If
                cboHabilitadoParaSolicitante.ListIndex = PosicionCombo(cboHabilitadoParaSolicitante, .TextMatrix(.RowSel, colHABIL_SOLI))       ' add -006- a.
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    If Not bLoading Then Call MostrarSeleccion
End Sub

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}

'{ add -005- a.
Private Sub chkHab_Click()
    Call InicializarPantalla
End Sub
'}

'{ add -007- a.
Private Sub cboDemanda_Click()
    'Debug.Print "cboDemanda_Click()"
    Call CargarCombo_Referente
End Sub
'}

Private Sub cmdImprimir_Click()
    ' Listado
End Sub
