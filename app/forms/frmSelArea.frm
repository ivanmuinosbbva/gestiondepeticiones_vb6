VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSelArea 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selecci�n de �rea"
   ClientHeight    =   7935
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7875
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSelArea.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7935
   ScaleWidth      =   7875
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraPie 
      Height          =   675
      Left            =   60
      TabIndex        =   10
      Top             =   6960
      Width           =   7755
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   6780
         TabIndex        =   11
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame fraOpciones 
      Caption         =   " Opciones de filtro "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   7755
      Begin VB.CheckBox chkSoloEjecutores 
         Caption         =   "S�lo �reas ejecutoras"
         Height          =   255
         Left            =   1680
         TabIndex        =   12
         Top             =   1320
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.ComboBox cboArea 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   720
         Width           =   5655
      End
      Begin VB.CheckBox chkExpandir 
         Caption         =   "Expandir los nodos al inicio"
         Height          =   255
         Left            =   1680
         TabIndex        =   7
         Top             =   1080
         Width           =   2235
      End
      Begin VB.CheckBox chkNombres 
         Caption         =   "S�lo nombres"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1320
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.CheckBox chkHabilitadas 
         Caption         =   "S�lo habilitadas"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1080
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.ComboBox cboNivel 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   360
         Width           =   2055
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "Filtrar por �rea:"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   8
         Top             =   780
         Width           =   1140
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "Listar desde el nivel:"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   420
         Width           =   1470
      End
   End
   Begin MSComctlLib.ImageList imgAreas 
      Left            =   7080
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":0B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":10BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":1658
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":1BF2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":218C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":2726
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelArea.frx":2CC0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar sbMain 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   7650
      Width           =   7875
      _ExtentX        =   13891
      _ExtentY        =   503
      Style           =   1
      SimpleText      =   "Para seleccionar un �rea en particular, presione Enter sobre la misma o haga doble click"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trwAreas 
      Height          =   5175
      Left            =   60
      TabIndex        =   0
      Top             =   1800
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   9128
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   706
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      SingleSel       =   -1  'True
      ImageList       =   "imgAreas"
      Appearance      =   1
      OLEDragMode     =   1
      OLEDropMode     =   1
   End
End
Attribute VB_Name = "frmSelArea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const TREE_ROOT = "RAIZ"
Const COLOR_INHABILITADO = &H8000000C

Const IMG_RAIZ = 5
Const IMG_DIRECCION = 1
Const IMG_GERENCIA = 2
Const IMG_SECTOR = 4
Const IMG_GRUPO = 8

Public flgSeleccionado As Boolean

Dim flgEnCarga As Boolean
Dim flgCargados As Boolean
Dim codigoArea As String

Private Sub Form_Load()
    Debug.Print "Cargando frmSelArea..."
    flgEnCarga = True
    flgCargados = False
    flgSeleccionado = False
    Call InicializarControles
    Call CargarAreas
    Call CargarArbol
    flgEnCarga = False
End Sub

Private Sub InicializarControles()
    With cboNivel
        .Clear
        .AddItem "Todo" & ESPACIOS & "||NULL"
        .AddItem "Direcci�n" & ESPACIOS & "||DIRE"
        .AddItem "Gerencia" & ESPACIOS & "||GERE"
        .AddItem "Sector" & ESPACIOS & "||SECT"
        .AddItem "Grupo" & ESPACIOS & "||GRUP"
        .ListIndex = 0
    End With
    Call setHabilCtrl(cboNivel, DISABLE)
End Sub

Private Sub CargarArbol()
    Dim nodo As MSComctlLib.node
    
    With trwAreas
        .Nodes.Clear
        .visible = False
        .HotTracking = True
        .FullRowSelect = False
        .Indentation = 300
        
        'codigoArea = "NULL"
        
        Set nodo = .Nodes.Add(, , TREE_ROOT, "Estructura organizacional", IMG_RAIZ)
        Select Case CodigoCombo(cboNivel, True)
            Case "NULL"
                Call CargarNodoDireccion
                'Call CargarNodoGerencia
                'Call CargarNodoSector
                'Call CargarNodoGrupo
            Case "DIRE"
                Call CargarNodoDireccion
                Call CargarNodoGerencia
                Call CargarNodoSector
                Call CargarNodoGrupo
            Case "GERE"
                Call CargarNodoGerencia
                Call CargarNodoSector
                Call CargarNodoGrupo
            Case "SECT"
                Call CargarNodoSector
                Call CargarNodoGrupo
            Case "GRUP"
                Call CargarNodoGrupo
        End Select
        .Nodes(1).Expanded = True
        .visible = True
    End With
    
            'flgCargados = True
        'End If
'        nodoN2 = ""
'        nodoN1 = ""
'        If chkAgrupamiento.Value = 1 Then
'            Do While Not aplRST.EOF
'                proyecto = ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & "." & ClearNull(aplRST.Fields!ProjSubSId) & " - " & ClearNull(aplRST.Fields!projnom)
'                Select Case ClearNull(aplRST.Fields!nivel)
'                    Case "3"
'                        icono = IMG_SUBSUBP
'                        Set nodo = .Nodes.Add(IIf(nodoN2 = "", "RAIZ", nodoN2), tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
'                    Case "2"
'                        icono = IMG_SUBPROY
'                        nodoN2 = "A" & ClearNull(aplRST.Fields!orden)
'                        Set nodo = .Nodes.Add(IIf(nodoN1 = "", "RAIZ", nodoN1), tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
'                    Case "1"
'                        icono = IMG_PROYECTO
'                        nodoN1 = "A" & ClearNull(aplRST.Fields!orden)
'                        nodoN2 = ""
'                        Set nodo = .Nodes.Add("RAIZ", tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
'                End Select
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        Else
'            Do While Not aplRST.EOF
'                proyecto = ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & "." & ClearNull(aplRST.Fields!ProjSubSId) & " - " & ClearNull(aplRST.Fields!projnom)
'                Select Case ClearNull(aplRST.Fields!nivel)
'                    Case "3": icono = IMG_SUBSUBP
'                    Case "2": icono = IMG_SUBPROY
'                    Case "1": icono = IMG_PROYECTO
'                End Select
'                Set nodo = .Nodes.Add("RAIZ", tvwChild, "A" & ClearNull(aplRST.Fields!orden), proyecto, icono)
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        sbProyectos.SimpleText = aplRST.RecordCount & " proyecto(s)."
End Sub

Private Sub chkHabilitadas_Click()
    Call CargarArbol
End Sub

Private Sub chkNombres_Click()
    Call CargarArbol
End Sub

Private Sub cboNivel_Click()
    If flgEnCarga Then Exit Sub
    Call CargarAreas
    Call CargarArbol
End Sub

Private Sub CargarAreas()
    'codigoArea = ""
    'codigoArea = "NULL"
    Dim direcciones As New Collection
    Dim gerencias As New Collection
    
    Dim direccion As direccion
    Dim gerencia As gerencia
    
    ' Direcciones
    If sp_GetDireccion(Null, IIf(chkHabilitadas.value = 1, "S", Null)) Then
        Do While Not aplRST.EOF
            Set direccion = New direccion
            direccion.Codigo = ClearNull(aplRST.Fields!cod_direccion)
            direccion.Nombre = ClearNull(aplRST.Fields!nom_direccion)
            direccion.Ejecutor = IIf(ClearNull(aplRST.Fields!es_ejecutor) = "S", True, False)
            direccion.Habilitado = IIf(ClearNull(aplRST.Fields!flg_habil) = "S", True, False)
            direccion.Abreviatura = ClearNull(aplRST.Fields!abrev_direccion)
            direcciones.Add direccion
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    ' Gerencias
    If sp_GetGerencia(Null, Null, IIf(chkHabilitadas.value = 1, "S", Null)) Then
        Do While Not aplRST.EOF
            Set gerencia = New gerencia
            gerencia.Codigo = ClearNull(aplRST.Fields!cod_gerencia)
            gerencia.Nombre = ClearNull(aplRST.Fields!nom_gerencia)
            gerencia.Ejecutor = IIf(ClearNull(aplRST.Fields!es_ejecutor) = "S", True, False)
            gerencia.Habilitado = IIf(ClearNull(aplRST.Fields!flg_habil) = "S", True, False)
            gerencia.Abreviatura = ClearNull(aplRST.Fields!abrev_gerencia)
            Set direccion = New direccion
            direccion.Codigo = ClearNull(aplRST.Fields!cod_direccion)
            Set gerencia.direccion = direccion
            gerencias.Add gerencia
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    Debug.Print gerencias.Count
    
    With cboArea
        .Clear
        Select Case CodigoCombo(cboNivel, True)
            Case "NULL"
                Call setHabilCtrl(cboArea, "DIS")
                .AddItem "* Todas las �reas" & ESPACIOS & "||" & "NULL", 0
                .ListIndex = 0
                Exit Sub
            Case "DIRE"
                If sp_GetDireccion(Null, IIf(chkHabilitadas.value = 1, "S", Null)) Then
                    Do While Not aplRST.EOF
                        '.AddItem ClearNull(aplRST.Fields!nom_direccion) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_direccion)
                        .AddItem IIf(ClearNull(aplRST.Fields!abrev_direccion) = "", ClearNull(aplRST.Fields!nom_direccion), ClearNull(aplRST.Fields!abrev_direccion)) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_direccion)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    .AddItem "* Todas las direcciones" & ESPACIOS & "||" & "NULL", 0
                    .ListIndex = 0
                End If
            Case "GERE"
                If sp_GetGerencia(Null, Null, IIf(chkHabilitadas.value = 1, "S", Null)) Then
                    Do While Not aplRST.EOF
                        '.AddItem ClearNull(aplRST.Fields!nom_gerencia) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_gerencia)
                        .AddItem IIf(ClearNull(aplRST.Fields!abrev_gerencia) = "", ClearNull(aplRST.Fields!nom_gerencia), ClearNull(aplRST.Fields!abrev_gerencia)) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_gerencia)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    .AddItem "* Todas las gerencias" & ESPACIOS & "||" & "NULL", 0
                    .ListIndex = 0
                End If
            Case "SECT"
                If sp_GetSector(Null, Null, IIf(chkHabilitadas.value = 1, "S", Null)) Then
                    Do While Not aplRST.EOF
                        .AddItem ClearNull(aplRST.Fields!nom_sector) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_sector)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    .AddItem "* Todos los sectores" & ESPACIOS & "||" & "NULL", 0
                    .ListIndex = 0
                End If
            Case "GRUP"
                If sp_GetGrupo(Null, Null, IIf(chkHabilitadas.value = 1, "S", Null)) Then
                    Do While Not aplRST.EOF
                        .AddItem ClearNull(aplRST.Fields!nom_grupo) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_grupo)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    .AddItem "* Todos los grupos" & ESPACIOS & "||" & "NULL", 0
                    .ListIndex = 0
                End If
        End Select
    End With
    'Call setHabilCtrl(cboArea, "NOR")
    Call setHabilCtrl(cboArea, "DIS")
End Sub

Private Sub cboArea_Click()
    If flgEnCarga Then Exit Sub
    codigoArea = CodigoCombo(cboArea, True)
    Call CargarArbol
End Sub

'Private Sub CargarNodoDireccion()
'    Dim nodo As MSComctlLib.node
'    'Dim i As Long
'
'    ' Direcciones
'    With trwAreas
'        'If sp_GetDireccion(IIf(codigoArea = "NULL" Or codigoArea = "", Null, codigoArea), IIf(chkHabilitadas.value = 1, "S", Null)) Then
'        If sp_GetDireccion(IIf(CodigoCombo(cboArea, True) = "NULL", Null, CodigoCombo(cboArea, True)), IIf(chkHabilitadas.value = 1, "S", Null)) Then
'            Do While Not aplRST.EOF
'                If chkSoloEjecutores.value = 0 Or (chkSoloEjecutores.value = 1 And ClearNull(aplRST.Fields!es_ejecutor) = "S") Then
'                    Debug.Print "CargarNodoDireccion() - Key: " & "D" & ClearNull(aplRST.Fields!cod_direccion)
'                    Set nodo = .Nodes.Add(TREE_ROOT, tvwChild, "D" & ClearNull(aplRST.Fields!cod_direccion), _
'                            IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_direccion), ClearNull(aplRST.Fields!cod_direccion) & ": " & ClearNull(aplRST.Fields!nom_direccion)), IMG_DIRECCION)
'                    If ClearNull(aplRST.Fields!flg_habil) <> "S" Then
'                        nodo.ForeColor = COLOR_INHABILITADO
'                    End If
'                    nodo.Expanded = IIf(chkExpandir.value = 1, True, False)
'                End If
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'    End With
'
'    ' Busco las gerencias
'
'    codigoArea = ""
'End Sub

Private Sub CargarNodoDireccion()
    Dim nodo As MSComctlLib.node
    Dim nodoGerencia As MSComctlLib.node
    Dim i As Long, j As Long
    
    ' Armo el nodo de Direcciones
    With trwAreas
        If sp_GetDireccion(IIf(CodigoCombo(cboArea, True) = "NULL", Null, CodigoCombo(cboArea, True)), IIf(chkHabilitadas.value = 1, "S", Null)) Then
            Do While Not aplRST.EOF
                If chkSoloEjecutores.value = 0 Or (chkSoloEjecutores.value = 1 And ClearNull(aplRST.Fields!es_ejecutor) = "S") Then
                    Set nodo = .Nodes.Add(TREE_ROOT, tvwChild, "D" & ClearNull(aplRST.Fields!cod_direccion), _
                            IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_direccion), ClearNull(aplRST.Fields!cod_direccion) & ": " & ClearNull(aplRST.Fields!nom_direccion)), IMG_DIRECCION)
                    If ClearNull(aplRST.Fields!flg_habil) <> "S" Then
                        nodo.ForeColor = COLOR_INHABILITADO
                    End If
                    nodo.Expanded = IIf(chkExpandir.value = 1, True, False)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        
        Dim n As Integer
        n = nodo.FirstSibling.Index
        
        While n <> nodo.LastSibling.Index
            'If sp_GetSector(Null, Mid(.Nodes(n).Next.Key, 2), IIf(chkHabilitadas.value = 1, "S", Null)) Then
            If sp_GetGerencia(Null, Mid(.Nodes(n).Next.Key, 2), IIf(chkHabilitadas.value = 1, "S", Null)) Then
                Do While Not aplRST.EOF
                    'Set nodo = .Nodes.Add(.Nodes(n).Next.Key, tvwChild, "S" & ClearNull(aplRST.Fields!cod_sector), _
                    '            IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_sector), ClearNull(aplRST.Fields!cod_sector) & ": " & ClearNull(aplRST.Fields!nom_sector)), IMG_SECTOR)
                    Set nodo = .Nodes.Add(.Nodes(i).Next.Key, tvwChild, "G" & ClearNull(aplRST.Fields!cod_gerencia), _
                                IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_gerencia), ClearNull(aplRST.Fields!cod_gerencia) & ": " & ClearNull(aplRST.Fields!nom_gerencia)), IMG_GERENCIA)
                    aplRST.MoveNext
                    DoEvents
                Loop
                'strText = strText & .Nodes(n).Next.text & vbLf
                ' Establece a n el �ndice del siguiente nodo.
            End If
            n = .Nodes(n).Next.Index
        Wend
        
        
        ' Por cada direccion, armo un nodo para las gerencias
        For i = 2 To .Nodes.Count - 1
            If sp_GetGerencia(Null, Mid(.Nodes(i).Key, 2), IIf(chkHabilitadas.value = 1, "S", Null)) Then
                Do While Not aplRST.EOF
                    Set nodo = .Nodes.Add(.Nodes(i).Key, tvwChild, "G" & ClearNull(aplRST.Fields!cod_gerencia), _
                                IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_gerencia), ClearNull(aplRST.Fields!cod_gerencia) & ": " & ClearNull(aplRST.Fields!nom_gerencia)), IMG_GERENCIA)
                    aplRST.MoveNext
                    DoEvents
                Loop
                ' Por cada nueva gerencia, agrego los sectores
                
                'Dim n As Integer
                n = nodo.FirstSibling.Index
                
                While n <> nodo.LastSibling.Index
                    If sp_GetSector(Null, Mid(.Nodes(n).Next.Key, 2), IIf(chkHabilitadas.value = 1, "S", Null)) Then
                        Do While Not aplRST.EOF
                            Set nodo = .Nodes.Add(.Nodes(n).Next.Key, tvwChild, "S" & ClearNull(aplRST.Fields!cod_sector), _
                                        IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_sector), ClearNull(aplRST.Fields!cod_sector) & ": " & ClearNull(aplRST.Fields!nom_sector)), IMG_SECTOR)
                            aplRST.MoveNext
                            DoEvents
                        Loop
                        'strText = strText & .Nodes(n).Next.text & vbLf
                        ' Establece a n el �ndice del siguiente nodo.
                    End If
                    n = .Nodes(n).Next.Index
                Wend

'                For j = 1 To .Nodes(i).Children
'                    Set nodoGerencia = .Nodes(i).Next
'                    If sp_GetSector(Null, nodoGerencia.Key, IIf(chkHabilitadas.value = 1, "S", Null)) Then
'                        Do While Not aplRST.EOF
'                            Set nodo = .Nodes.Add(nodoGerencia.Key, tvwChild, "S" & ClearNull(aplRST.Fields!cod_sector), _
'                                        IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_sector), ClearNull(aplRST.Fields!cod_sector) & ": " & ClearNull(aplRST.Fields!nom_sector)), IMG_SECTOR)
'                            aplRST.MoveNext
'                            DoEvents
'                        Loop
'                    End If
'                Next j
                
            End If
        Next i
        
        
        
        
        
'        ' Busco las gerencias
'        For i = 2 To .Nodes.Count - 1
'            If sp_GetGerencia(Null, Mid(.Nodes(i).Key, 2), IIf(chkHabilitadas.value = 1, "S", Null)) Then
'                Do While Not aplRST.EOF
'                    If chkSoloEjecutores.value = 0 Or (chkSoloEjecutores.value = 1 And ClearNull(aplRST.Fields!es_ejecutor) = "S") Then
'                        nodoRAIZ = IIf(CodigoCombo(cboNivel, True) = "GERE", TREE_ROOT, "D" & ClearNull(aplRST.Fields!cod_direccion))
'                        Set nodo = .Nodes.Add(nodoRAIZ, tvwChild, "G" & ClearNull(aplRST.Fields!cod_gerencia), _
'                                    IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_gerencia), ClearNull(aplRST.Fields!cod_gerencia) & ": " & ClearNull(aplRST.Fields!nom_gerencia)), IMG_GERENCIA)
'                        If ClearNull(aplRST.Fields!flg_habil) <> "S" Then
'                            nodo.ForeColor = COLOR_INHABILITADO
'                        End If
'                        nodo.Expanded = IIf(chkExpandir.value = 1, True, False)
'                    End If
'                    aplRST.MoveNext
'                    DoEvents
'                Loop
'            End If
'        Next i
'
'        ' Busca los sectores
'        For i = 2 To .Nodes.Count - 1
'            For j = 1 To .Nodes(i)
        
        
    End With
    
    
    
    codigoArea = ""
End Sub

Private Sub CargarNodoGerencia()
    Dim nodo As MSComctlLib.node
    Dim nodoRaiz As String
    Dim i As Long
    
    ' Gerencias
    With trwAreas
        For i = 2 To .Nodes.Count
            'If sp_GetGerencia(IIf(codigoArea = "NULL" Or codigoArea = "", Null, codigoArea), Null, IIf(chkHabilitadas.value = 1, "S", Null)) Then
            If sp_GetGerencia(Null, Mid(.Nodes(i).Key, 2), IIf(chkHabilitadas.value = 1, "S", Null)) Then
                Do While Not aplRST.EOF
                    If chkSoloEjecutores.value = 0 Or (chkSoloEjecutores.value = 1 And ClearNull(aplRST.Fields!es_ejecutor) = "S") Then
                        nodoRaiz = IIf(CodigoCombo(cboNivel, True) = "GERE", TREE_ROOT, "D" & ClearNull(aplRST.Fields!cod_direccion))
                        Set nodo = .Nodes.Add(nodoRaiz, tvwChild, "G" & ClearNull(aplRST.Fields!cod_gerencia), _
                                    IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_gerencia), ClearNull(aplRST.Fields!cod_gerencia) & ": " & ClearNull(aplRST.Fields!nom_gerencia)), IMG_GERENCIA)
                        If ClearNull(aplRST.Fields!flg_habil) <> "S" Then
                            nodo.ForeColor = COLOR_INHABILITADO
                        End If
                        nodo.Expanded = IIf(chkExpandir.value = 1, True, False)
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next i
    End With
    codigoArea = ""
End Sub

Private Sub CargarNodoSector()
    Dim nodo As MSComctlLib.node
    Dim nodoRaiz As String
    Dim i As Long
    Dim j As Long
    
    'Dim nodoRaiz As MSComctlLib.Nodes
    
    'Set nodoRaiz = New MSComctlLib.Nodes
    
    
    
    
    ' Sectores
    With trwAreas
        For i = 2 To .Nodes.Count       ' Direcci�n
            'If sp_GetSector(IIf(codigoArea = "NULL" Or codigoArea = "", Null, codigoArea), IIf(cboNivel.ListIndex <> 0, IIf(CodigoCombo(cboArea, True) = "", Null, CodigoCombo(cboArea, True)), Null), IIf(chkHabilitadas.value = 1, "S", Null)) Then
            codigoArea = Mid(.Nodes(i).Key, 2)
            If sp_GetSector(IIf(codigoArea = "NULL" Or codigoArea = "", Null, codigoArea), IIf(cboNivel.ListIndex <> 0, IIf(CodigoCombo(cboArea, True) = "", Null, CodigoCombo(cboArea, True)), Null), IIf(chkHabilitadas.value = 1, "S", Null)) Then
                Do While Not aplRST.EOF
                    If chkSoloEjecutores.value = 0 Or (chkSoloEjecutores.value = 1 And ClearNull(aplRST.Fields!es_ejecutor) = "S") Then
                        nodoRaiz = IIf(CodigoCombo(cboNivel, True) = "SECT", TREE_ROOT, "G" & ClearNull(aplRST.Fields!cod_gerencia))
                        Set nodo = .Nodes.Add(nodoRaiz, tvwChild, "S" & ClearNull(aplRST.Fields!cod_sector), _
                                    IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_sector), ClearNull(aplRST.Fields!cod_sector) & ": " & ClearNull(aplRST.Fields!nom_sector)), IMG_SECTOR)
                        If ClearNull(aplRST.Fields!flg_habil) <> "S" Then
                            nodo.ForeColor = COLOR_INHABILITADO
                        End If
                        nodo.Expanded = IIf(chkExpandir.value = 1, True, False)
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next i
    End With
    codigoArea = ""
End Sub

Private Sub CargarNodoGrupo()
    Dim nodo As MSComctlLib.node
    Dim nodoRaiz As String
    
    ' Grupos
    With trwAreas
        If sp_GetGrupo(IIf(codigoArea = "NULL" Or codigoArea = "", Null, codigoArea), IIf(cboNivel.ListIndex <> 0, IIf(CodigoCombo(cboArea, True) = "NULL", Null, CodigoCombo(cboArea, True)), Null), IIf(chkHabilitadas.value = 1, "S", Null)) Then
            Do While Not aplRST.EOF
                If chkSoloEjecutores.value = 0 Or (chkSoloEjecutores.value = 1 And ClearNull(aplRST.Fields!es_ejecutor) = "S") Then
                    nodoRaiz = IIf(CodigoCombo(cboNivel, True) = "GRUP", TREE_ROOT, "S" & ClearNull(aplRST.Fields!cod_sector))
                    Set nodo = .Nodes.Add(nodoRaiz, tvwChild, "G" & ClearNull(aplRST.Fields!cod_grupo), _
                                IIf(chkNombres.value = 1, ClearNull(aplRST.Fields!nom_grupo), ClearNull(aplRST.Fields!cod_grupo) & ": " & ClearNull(aplRST.Fields!nom_grupo)), IMG_GRUPO)
                    If ClearNull(aplRST.Fields!flg_habil) <> "S" Then
                        nodo.ForeColor = COLOR_INHABILITADO
                    End If
                    nodo.Expanded = IIf(chkExpandir.value = 1, True, False)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    codigoArea = ""
End Sub

Private Sub chkSoloEjecutores_Click()
    Call CargarArbol
End Sub

Private Sub cmdCerrar_Click()
    Me.Hide
End Sub

Private Sub trwAreas_NodeClick(ByVal node As MSComctlLib.node)
    sbMain.SimpleText = node.Parent.text & " - " & node.text
End Sub

'Private Sub trwAreas_NodeClick(ByVal Node As MSComctlLib.Node)
'    'trwAreas.Drag vbBeginDrag
'    'trwAreas.Drag vbBeginDrag
'    trwAreas.OLEDrag
'End Sub

Private Sub trwAreas_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)
    ' Only allow copying.
    AllowedEffects = vbDropEffectCopy
    Data.Clear
    ' Populate the Data object with the text to copy and the format.
    Data.SetData trwAreas.SelectedItem.text, vbCFText
End Sub

'Private Sub trwAreas_DragDrop(Source As Control, x As Single, y As Single)
'    ' DropHighlight devuelve una referencia al objeto sobre el que se ha colocado.
'   Me.Caption = trwAreas.DropHighlight.text
'   ' Para liberar la referencia de DropHighlight, se
'   ' establece a Nothing.
'   Set trwAreas.DropHighlight = Nothing
'
'End Sub
'
'Private Sub trwAreas_DragOver(Source As Control, x As Single, y As Single, State As Integer)
'   Set trwAreas.DropHighlight = trwAreas.HitTest(x, y)
'End Sub


