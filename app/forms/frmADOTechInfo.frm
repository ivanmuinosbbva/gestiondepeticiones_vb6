VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.Form frmADOTechInfo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   5145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9255
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADOTechInfo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5145
   ScaleWidth      =   9255
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdClose 
      Caption         =   "C&errar"
      Height          =   375
      Left            =   7920
      TabIndex        =   3
      Top             =   4680
      Width           =   1215
   End
   Begin VB.ComboBox cmbADOObject 
      Height          =   315
      ItemData        =   "frmADOTechInfo.frx":0442
      Left            =   840
      List            =   "frmADOTechInfo.frx":044F
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   1935
   End
   Begin ComctlLib.ListView lswADOObjectProperties 
      Height          =   3975
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   7011
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   327682
      SmallIcons      =   "imgProperties"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Propiedad"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(2) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   1
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Tipo"
         Object.Width           =   1941
      EndProperty
      BeginProperty ColumnHeader(3) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         SubItemIndex    =   2
         Key             =   ""
         Object.Tag             =   ""
         Text            =   "Valor"
         Object.Width           =   4410
      EndProperty
   End
   Begin ComctlLib.ImageList imgProperties 
      Left            =   8520
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   2
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmADOTechInfo.frx":0473
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmADOTechInfo.frx":078D
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblADOTechInfo 
      AutoSize        =   -1  'True
      Caption         =   "Objeto:"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   555
   End
End
Attribute VB_Name = "frmADOTechInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Dim oItem As ListItem
Dim rsObjGen As New ADODB.Recordset
Dim cmdObjGen As New ADODB.Command

Private Sub Form_Load()
    fjsNewStatusMessage Trim(Me.Name), 7    ' add -001- a.
    Me.Caption = "Propiedades y caracter�sticas t�cnicas del modelo de objeto Microsoft ADO v" & aplCONN.Version
    cmbADOObject.ListIndex = 0
    InitializeData
End Sub

Private Sub cmbADOObject_Click()
    InitializeData
End Sub

Private Sub InitializeData()
    Dim i As Long
    
    lswADOObjectProperties.ListItems.Clear      ' Limpia el ListView
    Select Case cmbADOObject.ListIndex
        Case 0      ' Objecto: Connection
            With lswADOObjectProperties
                aplCONN.Properties.Refresh
                For i = 0 To aplCONN.Properties.Count - 1
                    Set oItem = .ListItems.Add(, aplCONN.Properties(i).Name, aplCONN.Properties(i).Name, , 2)
                    Select Case aplCONN.Properties(i).Type
                        Case 3
                            oItem.SubItems(1) = "Num�rico"
                        Case 11
                            oItem.SubItems(1) = "Booleano"
                        Case 8
                            oItem.SubItems(1) = "Caracter"
                        Case Else
                            oItem.SubItems(1) = "Otro"
                    End Select
                    oItem.SubItems(2) = aplCONN.Properties(i).Value
                Next i
            End With
        Case 1      ' Objecto: RecordSet
            Set rsObjGen.ActiveConnection = aplCONN
            rsObjGen.Properties.Refresh
            'rsObjGen.Properties("IMultipleResults") = True
            With lswADOObjectProperties
                For i = 0 To rsObjGen.Properties.Count - 1
                    Set oItem = .ListItems.Add(, rsObjGen.Properties(i).Name, rsObjGen.Properties(i).Name, , 2)
                    Select Case rsObjGen.Properties(i).Type
                        Case 3
                            oItem.SubItems(1) = "Num�rico"
                        Case 11
                            oItem.SubItems(1) = "Booleano"
                        Case 8
                            oItem.SubItems(1) = "Caracter"
                    End Select
                    oItem.SubItems(2) = rsObjGen.Properties(i).Value
                Next i
            End With
        Case 2      ' Objecto: Command
            Set cmdObjGen.ActiveConnection = aplCONN
            cmdObjGen.Properties.Refresh
            With lswADOObjectProperties
                For i = 0 To cmdObjGen.Properties.Count - 1
                    Set oItem = .ListItems.Add(, cmdObjGen.Properties(i).Name, cmdObjGen.Properties(i).Name, , 2)
                    Select Case cmdObjGen.Properties(i).Type
                        Case 3
                            oItem.SubItems(1) = "Num�rico"
                        Case 11
                            oItem.SubItems(1) = "Booleano"
                        Case 8
                            oItem.SubItems(1) = "Caracter"
                    End Select
                    oItem.SubItems(2) = cmdObjGen.Properties(i).Value
                Next i
            End With
    End Select
End Sub

Private Sub cmdClose_Click()
    Set rsObjGen = Nothing
    Set cmdObjGen = Nothing
    Unload Me
End Sub
