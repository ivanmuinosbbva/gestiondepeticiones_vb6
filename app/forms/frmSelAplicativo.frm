VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSelAplicativo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccionar aplicativo"
   ClientHeight    =   6480
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8865
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6480
   ScaleWidth      =   8865
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5415
      Left            =   7530
      TabIndex        =   1
      Top             =   1020
      Width           =   1275
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   3
         Top             =   1900
         Width           =   1170
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   1400
         Width           =   1170
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   8745
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5295
      Left            =   60
      TabIndex        =   4
      Top             =   1080
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   9340
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelAplicativo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const colCODAPP = 0
Const colNOMAPP = 1
Const colFLGHABIL = 2
Const colAMBIENTE = 3

Public cAmbiente As String

Private Sub Form_Load()
    glAuxRetorno = ""
    Call CargarGrid
    Call IniciarScroll(grdDatos)
End Sub

Private Sub cmdAceptar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    glAuxRetorno = ""
    Unload Me
End Sub

Private Sub CargarGrid()
    Call Puntero(True)
    Call Status("Cargando aplicativos...")
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .cols = 4
        .Rows = 1
        .TextMatrix(0, colCODAPP) = "C�digo"
        .TextMatrix(0, colNOMAPP) = "Descripci�n"
        .TextMatrix(0, colFLGHABIL) = "Hab."
        .TextMatrix(0, colAMBIENTE) = "Amb."
        .ColWidth(colCODAPP) = 2000: .ColAlignment(colCODAPP) = 0
        .ColWidth(colNOMAPP) = 5500: .ColAlignment(colNOMAPP) = 0
        .ColWidth(colFLGHABIL) = 600: .ColAlignment(colFLGHABIL) = 0
        .ColWidth(colAMBIENTE) = 2000: .ColAlignment(colAMBIENTE) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetAplicativo(Null, Null, "S") Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            If (cAmbiente = "T") Or (ClearNull(aplRST.Fields.item("app_amb")) = cAmbiente Or aplRST.Fields.item("app_amb") = "B") Then
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colCODAPP) = IIf(Not IsNull(aplRST.Fields.item("app_id")), aplRST.Fields.item("app_id"), "")
                .TextMatrix(.Rows - 1, colNOMAPP) = IIf(Not IsNull(aplRST.Fields.item("app_nombre")), aplRST.Fields.item("app_nombre"), "")
                .TextMatrix(.Rows - 1, colFLGHABIL) = IIf(Not IsNull(aplRST.Fields.item("app_hab")), aplRST.Fields.item("app_hab"), "S")
                Select Case ClearNull(aplRST.Fields.item("app_amb"))
                    Case "H": .TextMatrix(.Rows - 1, colAMBIENTE) = "HOST"
                    Case "D": .TextMatrix(.Rows - 1, colAMBIENTE) = "SSDD"
                    Case "B": .TextMatrix(.Rows - 1, colAMBIENTE) = "HOST/SSDD"
                End Select
            End If
        End With
        DoEvents
        aplRST.MoveNext
    Loop
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
                .Col = .MouseCol
                .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colCODAPP) <> "" Then
                glAuxRetorno = ClearNull(.TextMatrix(.RowSel, colCODAPP)) & " : " & ClearNull(.TextMatrix(.RowSel, colNOMAPP)) & "||" & ClearNull(.TextMatrix(.RowSel, colCODAPP))
            End If
        End If
        .Refresh
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
