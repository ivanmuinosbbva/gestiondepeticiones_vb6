VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmIndicadores 
   Caption         =   "Trabajar con Indicadores"
   ClientHeight    =   5565
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9465
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmIndicadores.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5565
   ScaleWidth      =   9465
   Begin VB.Frame fraBotonera 
      Height          =   5535
      Left            =   8280
      TabIndex        =   7
      Top             =   0
      Width           =   1155
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   435
         Left            =   120
         TabIndex        =   12
         Top             =   4980
         Width           =   945
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   435
         Left            =   120
         TabIndex        =   11
         Top             =   4560
         Width           =   945
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   435
         Left            =   120
         TabIndex        =   10
         Top             =   4140
         Width           =   945
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   435
         Left            =   120
         TabIndex        =   9
         Top             =   3720
         Width           =   945
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   435
         Left            =   120
         TabIndex        =   8
         Top             =   3300
         Width           =   945
      End
   End
   Begin VB.Frame fraDatos 
      Height          =   2055
      Left            =   60
      TabIndex        =   1
      Top             =   3480
      Width           =   8175
      Begin VB.TextBox txtCodigo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   13
         Top             =   420
         Width           =   1095
      End
      Begin VB.ComboBox cboHabilitado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1140
         Width           =   1215
      End
      Begin VB.TextBox txtNombre 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   60
         TabIndex        =   5
         Top             =   780
         Width           =   5535
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Habilitado"
         Height          =   195
         Index           =   2
         Left            =   300
         TabIndex        =   4
         Top             =   1208
         Width           =   705
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   3
         Top             =   840
         Width           =   555
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   2
         Top             =   480
         Width           =   495
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3360
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   5927
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmIndicadores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const col_Codigo = 0
Private Const COL_NOMBRE = 1
Private Const COL_HABILITADO = 2
Private Const COL_TOTALCOLS = 3

Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarControles
    Call IniciarScroll(grdDatos)
    Call InicializarPantalla
End Sub

Private Sub InicializarControles()
    ' Inicializa el tama�o del form
    Me.Top = 0
    Me.Left = 0
    Me.Height = 6075
    Me.Width = 9585
    
    With cboHabilitado
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
    End With
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    'Show
    Call LimpiarForm(Me)
    Call CargarGrilla
    Call HabilitarBotones(0)
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        Me.Height = 6075
        Me.Width = 9585
    End If
End Sub

Private Sub Form_DblClick()
    Me.Top = 0
    Me.Left = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub CargarGrilla()
    With grdDatos
        .Clear
        .Rows = 1
        .cols = 3
        .RowHeightMin = 300
        .TextMatrix(0, col_Codigo) = "C�digo": .ColWidth(col_Codigo) = 1000: .ColAlignment(col_Codigo) = flexAlignLeftCenter
        .TextMatrix(0, COL_NOMBRE) = "Nombre": .ColWidth(COL_NOMBRE) = 5000: .ColAlignment(COL_NOMBRE) = flexAlignLeftCenter
        .TextMatrix(0, COL_HABILITADO) = "Habilitado": .ColWidth(COL_HABILITADO) = 1000: .ColAlignment(COL_HABILITADO) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
        If sp_GetIndicador(Null, Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, col_Codigo) = ClearNull(aplRST.Fields!indicadorId)
                .TextMatrix(.Rows - 1, COL_NOMBRE) = ClearNull(aplRST.Fields!indicadorNom)
                .TextMatrix(.Rows - 1, COL_HABILITADO) = ClearNull(aplRST.Fields!indicadorHab)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, col_Codigo) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, col_Codigo))
                txtNombre = ClearNull(.TextMatrix(.RowSel, COL_NOMBRE))
                Call SetCombo(cboHabilitado, ClearNull(.TextMatrix(.RowSel, COL_HABILITADO)), True)
            End If
        End If
    End With
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True

    If ClearNull(txtCodigo.text) = "" Then
        MsgBox "Debe completar el c�digo.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.text, ":") > 0 Then
        MsgBox "El c�digo no pude contener el caracter ':'.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If ClearNull(txtNombre.text) = "" Then
        MsgBox "Debe completar el nombre.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtNombre.text, ":") > 0 Then
        MsgBox "El nombre no pude contener el caracter ':'.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If cboHabilitado.ListIndex < 0 Then
        MsgBox "Debe seleccionar si est� habilitado o no.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            'lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    fraDatos.Caption = " Agregar "
                    txtCodigo = "": txtNombre = ""
                    cboHabilitado.ListIndex = 0
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    fraDatos.Caption = " Modificar "
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtNombre.SetFocus
                Case "E"
                    fraDatos.Caption = " Eliminar "
                    fraDatos.Enabled = False
            End Select
    End Select
End Sub

Private Sub cmdConfirmar_Click()
    Call GuardarDatos
End Sub

Private Sub GuardarDatos()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                Call sp_InsertIndicador(txtCodigo, txtNombre, CodigoCombo(cboHabilitado, True))
                Call HabilitarBotones(0)
            End If
        Case "M"
            If CamposObligatorios Then
                Call sp_UpdateIndicador(txtCodigo, txtNombre, CodigoCombo(cboHabilitado, True))
                With grdDatos
                    If .RowSel > 0 Then
                       .TextMatrix(.RowSel, col_Codigo) = ClearNull(txtCodigo)
                       .TextMatrix(.RowSel, COL_NOMBRE) = ClearNull(txtNombre)
                       .TextMatrix(.RowSel, COL_HABILITADO) = CodigoCombo(cboHabilitado, True)
                    End If
                End With
                Call HabilitarBotones(0, False)
            End If
        Case "E"
            If sp_DeleteIndicador(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub
