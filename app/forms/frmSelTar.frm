VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSelTar 
   Caption         =   "Seleccionar tarea"
   ClientHeight    =   5850
   ClientLeft      =   1155
   ClientTop       =   345
   ClientWidth     =   8835
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "frmSelTar.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   8835
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   60
      TabIndex        =   4
      Top             =   0
      Width           =   8745
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4875
      Left            =   7770
      TabIndex        =   0
      Top             =   960
      Width           =   1035
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   4035
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   1
         Top             =   4425
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4710
      Left            =   60
      TabIndex        =   2
      Top             =   1080
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   8308
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelTar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Const colCODTAREA = 0
Const colNOMTAREA = 1
Const colFLGHABIL = 2
Dim m_SoloHabil As Boolean

Private Sub Form_Load()
    glAuxRetorno = ""
    Call CargarGrid
    Call IniciarScroll(grdDatos)         ' add -003- a.
End Sub

Private Sub CargarGrid()
    Call Puntero(True)
    Call Status("Cargando Tareas")
    With grdDatos
        .Clear
        .cols = 3
        .HighLight = flexHighlightNever
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Rows = 1
        .TextMatrix(0, colCODTAREA) = "CODIGO": .ColWidth(colCODTAREA) = 1000: .ColAlignment(colCODTAREA) = 0
        .TextMatrix(0, colNOMTAREA) = "DESCRIPCION": .ColWidth(colNOMTAREA) = 5500: .ColAlignment(colNOMTAREA) = 0
        .TextMatrix(0, colFLGHABIL) = "HAB": .ColWidth(colFLGHABIL) = 500: .ColAlignment(colFLGHABIL) = 0
        CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    End With
    If Not sp_GetTarea("") Then Exit Sub
    Do While Not aplRST.EOF
        If (m_SoloHabil = False) Or (m_SoloHabil = True And ClearNull(aplRST.Fields.Item("flg_habil")) <> "N") Then
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODTAREA) = IIf(Not IsNull(aplRST.Fields.Item("cod_tarea")), aplRST.Fields.Item("cod_tarea"), "")
            .TextMatrix(.Rows - 1, colNOMTAREA) = IIf(Not IsNull(aplRST.Fields.Item("nom_tarea")), aplRST.Fields.Item("nom_tarea"), "")
            .TextMatrix(.Rows - 1, colFLGHABIL) = IIf(Not IsNull(aplRST.Fields.Item("flg_habil")), aplRST.Fields.Item("flg_habil"), "S")
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
        End With
        End If
        
        aplRST.MoveNext
        DoEvents
    Loop
    Call Puntero(False)
    Call Status("Listo.")
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
            .RowSel = 1
            .Col = .MouseCol
            .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colCODTAREA) <> "" Then
                'glAuxRetorno = padLeft(ClearNull(.TextMatrix(.RowSel, colCODTAREA)), 6) & " : " & .TextMatrix(.RowSel, colNOMTAREA) & Space(40) & "||S"
                glAuxRetorno = .TextMatrix(.RowSel, colCODTAREA) & " : " & .TextMatrix(.RowSel, colNOMTAREA) & ESPACIOS & ESPACIOS & ESPACIOS & "||S"
            End If
        End If
        .Refresh
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Public Property Let prpSoloHabil(ByVal NewVal As Boolean)
    m_SoloHabil = NewVal
End Property

Public Property Get prpSoloHabil() As Boolean
    prpSoloHabil = m_SoloHabil
End Property

Private Sub cmdAceptar_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    glAuxRetorno = ""
    Unload Me
End Sub

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
