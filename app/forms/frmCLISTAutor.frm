VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmCLISTAutor 
   Caption         =   "Autorizaciones para solicitudes a Carga de M�quina"
   ClientHeight    =   8010
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11865
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCLISTAutor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   8010
   ScaleWidth      =   11865
   Begin VB.Frame fraMain 
      Height          =   855
      Left            =   120
      TabIndex        =   5
      Top             =   0
      Width           =   10395
      Begin VB.ComboBox cmbFiltroPendientes 
         Height          =   300
         Left            =   2160
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   240
         Width           =   2535
      End
      Begin AT_MaskText.MaskText mskFechaIni 
         Height          =   315
         Left            =   8760
         TabIndex        =   8
         ToolTipText     =   "Ver solicitudes desde fecha"
         Top             =   240
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         AutoTab         =   -1  'True
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Pendientes de aprobaci�n:"
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   300
         Width           =   1995
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Desde:"
         Height          =   180
         Index           =   3
         Left            =   7680
         TabIndex        =   9
         Top             =   300
         Width           =   1035
      End
   End
   Begin VB.TextBox txtJustificacion 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1155
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   6720
      Width           =   10335
   End
   Begin VB.Frame pnlBotones 
      Height          =   7995
      Left            =   10560
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdActualizar 
         Caption         =   "Actualizar"
         Height          =   495
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Actualizar la grilla"
         Top             =   160
         Width           =   1170
      End
      Begin VB.CommandButton cmdReenviar 
         Caption         =   "Reenviar"
         Height          =   495
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Reenviar correo a Carga de M�quina"
         Top             =   2160
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   495
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   7440
         Width           =   1170
      End
      Begin VB.CommandButton cmdAprobar 
         Caption         =   "Aprobar"
         Height          =   495
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Aprobar las solicitudes seleccionadas"
         Top             =   1680
         Width           =   1170
      End
      Begin VB.CommandButton cmdAprobarTodo 
         Caption         =   "Aprobar todo lo pendiente"
         Height          =   495
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Aprueba todas las solicitudes que aparecen en la pantalla"
         Top             =   1200
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5460
      Left            =   120
      TabIndex        =   3
      Top             =   960
      Width           =   10395
      _ExtentX        =   18336
      _ExtentY        =   9631
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Justificaci�n de la solicitud"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   6480
      Width           =   2175
   End
End
Attribute VB_Name = "frmCLISTAutor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 21.09.2009 - Se agrega la visualizaci�n de la solicitud que se desea aprobar (sugerencia de G. Gurvich)
' -002- a. FJS 03.11.2009 - Se corrige el modo de m�ltiple selecci�n de celdas.
' -003- a. FJS 12.01.2010 - Se agrega funcionalidad para gestionar los restore de backup.
' -004- a. FJS 20.08.2010 - Se agregan algunas mejoras est�ticas menores.
' -005- a. FJS 08.07.2014 - Nuevo: se habilita la funcionalidad de autorizaci�n para perfiles de responsable de ejecuci�n
'                           (realizan una preaprobaci�n que luego deber� ser aprobada por los CSEC para solicitudes normales
'                           sin enmascaramiento).
' Proceso de aprobaci�n:
'
' El sp que realiza la aprobaci�n es
'
'A = "Pendiente aprobaci�n niv. 4" (L�der)
'B = "Pendiente aprobaci�n niv. 3" (Supervisor)
'C = "Aprobado"
'D = "Aprobado y en proceso de envio"
'E = "Enviado y finalizado"
'F = "En espera de Restore"
'G = "En espera (ejecuci�n diferida)"

Option Explicit

Private Const colSOL_MultiSelect = 0
Private Const colSOL_Nro = 1
Private Const colSOL_EME = 2
Private Const colSOL_Tipo = 3
Private Const colSOL_Enm = 4
Private Const colSOL_Estado = 5
Private Const colSOL_Solicitada = 6
Private Const colSOL_SolicitadaPor = 7
Private Const colSOL_RespEjec = 8
Private Const colSOL_RespSect = 9
Private Const colSOL_Peticion = 10
Private Const colSOL_Titulo = 11
Private Const colSOL_FCHAUT1 = 12
Private Const colSOL_FCHAUT2 = 13
Private Const colSOL_cod_estado = 14
Private Const colSOL_new_estado = 15
Private Const colSOL_NOMPROD = 16
Private Const colSOL_JUSTIFI = 17
Private Const colSOL_JUSTTXT = 18
Private Const colSOL_TOTCOLS = 19

Dim bFlagCargaInicial As Boolean
Dim lRowSel_Ini As Long
Dim lRowSel_Fin As Long
Dim pUsrPerfilActual As String

Private Sub Form_Load()
    Me.Height = FORM_HEIGHT
    Me.Width = FORM_WIDTH
    Me.WindowState = vbMaximized
        
    mskFechaIni.DateValue = DateAdd("m", -3, Now)
    mskFechaIni.Text = mskFechaIni.DateValue
    bFlagCargaInicial = True                        ' Flag para evitar la sobrecarga de eventos
    If InPerfil("CSEC") Then
        pUsrPerfilActual = "CSEC"
        cmdReenviar.ToolTipText = "Reenvia el mail para CM sobre la solicitud aprobada"
    '{ add -005- a.
    Else
        If InPerfil("CGRU") Then
            pUsrPerfilActual = "CGRU"
            cmdReenviar.ToolTipText = "Reenvia un recordatorio por mail al N3 para la aprobaci�n de la solicitud seleccionada"
        End If
    '}
    End If
    Call InicializarPantalla
    Call CargarGrilla
    bFlagCargaInicial = False
End Sub

'{ add -005- a.
Private Sub InicializarPantalla()
    Call setHabilCtrl(cmdAprobar, "DIS")
    Call setHabilCtrl(cmdAprobarTodo, "DIS")
    Call setHabilCtrl(cmdReenviar, "DIS")
    
    With cmbFiltroPendientes
        .Clear
        Select Case pUsrPerfilActual
            Case "CGRU"
                .AddItem "Todo" & Space(100) & "||T"
                .AddItem "Solo lo pendiente" & Space(100) & "||P"
                .ListIndex = 1
                'Call setHabilCtrl(cmbFiltroPendientes, "DIS")
                'Call setHabilCtrl(cmdReenviar, "DIS")           ' L�deres no deben reenviar ning�n tipo de correo
            Case "CSEC"
                .AddItem "Todo" & Space(100) & "||T"
                .AddItem "Solo lo m�o" & Space(100) & "||P"
                .AddItem "Lo m�o y lo de mis reportes" & Space(100) & "||Q"
                .ListIndex = 1
        End Select
    End With
End Sub
'}

Private Sub InicializarGrilla()
    With grdDatos
        .Visible = False
        .Clear
        .cols = colSOL_TOTCOLS: .Rows = 1
        .Font = "Tahoma"
        .Font.Size = 8
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusNone
        .ColWidth(colSOL_MultiSelect) = 150
        .TextMatrix(0, colSOL_Nro) = "N�": .ColWidth(colSOL_Nro) = 800
        .TextMatrix(0, colSOL_EME) = "EME": .ColWidth(colSOL_EME) = 500: .ColAlignment(colSOL_EME) = flexAlignCenterCenter          ' add -005- a.
        .TextMatrix(0, colSOL_Tipo) = "Tipo": .ColWidth(colSOL_Tipo) = 800: .ColAlignment(colSOL_Tipo) = flexAlignLeftCenter 'flexAlignCenterCenter
        .TextMatrix(0, colSOL_Enm) = "Enm.": .ColWidth(colSOL_Enm) = 500: .ColAlignment(colSOL_Enm) = flexAlignCenterCenter
        .TextMatrix(0, colSOL_Estado) = "Estado de la solicitud": .ColWidth(colSOL_Estado) = 2500: .ColAlignment(colSOL_Estado) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_Solicitada) = "Solicitada": .ColWidth(colSOL_Solicitada) = 1200
        .TextMatrix(0, colSOL_SolicitadaPor) = "Solicitado por": .ColWidth(colSOL_SolicitadaPor) = 1800
        .TextMatrix(0, colSOL_Peticion) = "Petici�n": .ColWidth(colSOL_Peticion) = 1000
        .TextMatrix(0, colSOL_Titulo) = "T�tulo de la petici�n": .ColWidth(colSOL_Titulo) = 5000: .ColAlignment(colSOL_Titulo) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_RespEjec) = "Resp. Ejec.": .ColWidth(colSOL_RespEjec) = 2000: .ColAlignment(colSOL_RespEjec) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_RespSect) = "Resp. Sect.": .ColWidth(colSOL_RespSect) = 2000: .ColAlignment(colSOL_RespSect) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_FCHAUT1) = "Fch. N4": .ColWidth(colSOL_FCHAUT1) = 1200: .ColAlignment(colSOL_FCHAUT1) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_FCHAUT2) = "Fch. N3": .ColWidth(colSOL_FCHAUT2) = 1200: .ColAlignment(colSOL_FCHAUT2) = flexAlignLeftCenter
        ' Invisibles
        .TextMatrix(0, colSOL_cod_estado) = "cod_estado": .ColWidth(colSOL_cod_estado) = 0: .ColAlignment(colSOL_cod_estado) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_new_estado) = "Req. aprob.": .ColWidth(colSOL_new_estado) = 0: .ColAlignment(colSOL_new_estado) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_NOMPROD) = "ARCHIVO PROD": .ColWidth(colSOL_NOMPROD) = 0: .ColAlignment(colSOL_NOMPROD) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_JUSTIFI) = "JUST.": .ColWidth(colSOL_JUSTIFI) = 0: .ColAlignment(colSOL_JUSTIFI) = flexAlignLeftCenter
        .TextMatrix(0, colSOL_JUSTTXT) = "Explicaci�n": .ColWidth(colSOL_JUSTTXT) = 0: .ColAlignment(colSOL_JUSTTXT) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub cmdActualizar_Click()
    Call CargarGrilla
End Sub

Private Sub CargarGrilla()
    Call Puntero(True)      ' add -004- a.
    Call InicializarGrilla
    bFlagCargaInicial = True
    'If sp_GetSolicitudAuto(glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, IIf(IsDate(mskFechaIni.DateValue), mskFechaIni.DateValue, Null), chkVerSoloPendientes.Value, chkSoloPropias.Value) Then     ' del -005- a.
    'If sp_GetSolicitudAuto(glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, IIf(IsDate(mskFechaIni.DateValue), mskFechaIni.DateValue, Null), chkVerSoloPendientes.Value, CodigoCombo(cmbFiltroPendientes, True)) Then      ' add -005- a.
    If sp_GetSolicitudAuto(glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, IIf(IsDate(mskFechaIni.DateValue), mskFechaIni.DateValue, Null), CodigoCombo(cmbFiltroPendientes, True)) Then      ' add -005- a.
        Do While Not aplRST.EOF
            With grdDatos
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colSOL_Nro) = aplRST.Fields!sol_nroasignado
                .TextMatrix(.Rows - 1, colSOL_EME) = IIf(aplRST.Fields!sol_eme = "S", "Si", "No")       ' add -005- a.
                .TextMatrix(.Rows - 1, colSOL_Tipo) = aplRST.Fields!sol_tipo
                .TextMatrix(.Rows - 1, colSOL_Enm) = IIf(aplRST.Fields!sol_mask = "S", "Si", "No")
                .TextMatrix(.Rows - 1, colSOL_Solicitada) = Format(aplRST.Fields!SOL_FECHA, "dd/mm/yyyy")
                .TextMatrix(.Rows - 1, colSOL_SolicitadaPor) = ClearNull(aplRST.Fields!nom_recurso)
                .TextMatrix(.Rows - 1, colSOL_Peticion) = IIf(IsNull(aplRST.Fields!pet_nroasignado), "", aplRST.Fields!pet_nroasignado)
                .TextMatrix(.Rows - 1, colSOL_Titulo) = ClearNull(aplRST.Fields!pet_titulo)
                .TextMatrix(.Rows - 1, colSOL_RespEjec) = ClearNull(aplRST.Fields!nom_resp_ejec)
                .TextMatrix(.Rows - 1, colSOL_RespSect) = ClearNull(aplRST.Fields!nom_resp_sect)
                .TextMatrix(.Rows - 1, colSOL_Estado) = ClearNull(aplRST.Fields!nom_estado)
                .TextMatrix(.Rows - 1, colSOL_cod_estado) = ClearNull(aplRST.Fields!SOL_ESTADO)
                .TextMatrix(.Rows - 1, colSOL_NOMPROD) = ClearNull(aplRST.Fields!FILE_PROD)
                '.TextMatrix(.Rows - 1, colSOL_NOMPROD) = ClearNull(aplRST.Fields!sol_file_prod)
                .TextMatrix(.Rows - 1, colSOL_JUSTIFI) = ClearNull(aplRST.Fields!jus_desc)
                .TextMatrix(.Rows - 1, colSOL_JUSTTXT) = ClearNull(aplRST.Fields!sol_texto)
                .TextMatrix(.Rows - 1, colSOL_new_estado) = IIf(.TextMatrix(.Rows - 1, colSOL_cod_estado) = "A", "Si", "No")    ' add -005- a.
                .TextMatrix(.Rows - 1, colSOL_FCHAUT1) = Format(ClearNull(aplRST.Fields!sol_fe_auto1), "dd/MM/yyyy")
                .TextMatrix(.Rows - 1, colSOL_FCHAUT2) = Format(ClearNull(aplRST.Fields!sol_fe_auto2), "dd/MM/yyyy")
            End With
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    grdDatos.Visible = True
    Call Puntero(False)     ' add -004- a.
    bFlagCargaInicial = False
End Sub

'{ del -005- a.
'Private Sub chkVerSoloPendientes_Click()
'    If Not bFlagCargaInicial Then Call CargarGrilla
'End Sub
'
'Private Sub chkSoloPropias_Click()
'    If Not bFlagCargaInicial Then Call CargarGrilla
'End Sub
'}

'{ add -005- a.
Private Sub cmbFiltroPendientes_Click()
    If Not bFlagCargaInicial Then Call CargarGrilla
End Sub
'}

Private Sub grdDatos_RowColChange()
    If Not bFlagCargaInicial Then MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    If Not bFlagCargaInicial Then
        With grdDatos
            If .RowSel > 0 Then
                If .TextMatrix(.RowSel, 0) <> "" Then
                    txtJustificacion = "ARCHIVO: " & ClearNull(.TextMatrix(.RowSel, colSOL_NOMPROD)) & vbCrLf & vbCrLf & _
                                        .TextMatrix(.RowSel, colSOL_JUSTIFI) & vbCrLf & vbCrLf & _
                                        .TextMatrix(.RowSel, colSOL_JUSTTXT)
                End If
            End If
        End With
    End If
End Sub

Private Sub cmdAprobar_Click()
    Call Aprobar
End Sub

Private Sub Aprobar()
    Dim bEnviados As Boolean
    Dim i As Long
    
    With grdDatos
        If .RowSel > 0 Then
            bEnviados = False
            If MsgBox("�Confirma la aprobaci�n de las solicitudes seleccionadas?", vbQuestion + vbYesNo, "Aprobaci�n") = vbYes Then
                Call Puntero(True)
                Call Status("Generando aprobaci�n...")
                For i = 1 To .Rows - 1
                    If ClearNull(.TextMatrix(i, colSOL_MultiSelect)) = "�" Then
                        Call sp_UpdateSolicitudAuto(.TextMatrix(i, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date)
                        Select Case pUsrPerfilActual
                            Case "CSEC"
                                If .TextMatrix(i, colSOL_EME) <> "Si" Then
                                    If .TextMatrix(i, colSOL_Tipo) = "XCOM*" Or _
                                        .TextMatrix(i, colSOL_Tipo) = "UNLO*" Or _
                                        .TextMatrix(i, colSOL_Tipo) = "TCOR*" Or _
                                        .TextMatrix(i, colSOL_Tipo) = "SORT*" Then
                                            Call EnviarCorreo_Restore(.TextMatrix(i, colSOL_Nro), False)
                                    Else
                                        Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(i, colSOL_Nro), False)
                                    End If
                                    bEnviados = True
                                End If
                            Case "CGRU"         ' S�lo envia aviso de pendientes al Supervisor tras su aprobaci�n
                                Call EnviarCorreo_Recordatorio(.TextMatrix(i, colSOL_Nro), False)
                                bEnviados = True
                        End Select
                    End If
                Next i
                If bEnviados Then MsgBox "Email(s) enviado(s).", vbInformation
                Call CargarGrilla
                Call Status("Listo.")
                Call Puntero(False)
            End If
        End If
    End With
End Sub

' *******************************************************
' FUNCIONA OK: anterior al multiselect (13.08.2014)
' *******************************************************
'Private Sub Aprobar()
'    Dim bEnviados As Boolean
'
'    With grdDatos
'        If .RowSel > 0 Then
'            bEnviados = False
'            '{ del -005- b.
'            'If .TextMatrix(.RowSel, colSOL_cod_estado) <> "A" Then
'            '    MsgBox "La solicitud n� " & Trim(.TextMatrix(.RowSel, colSOL_Nro)) & " no requiere aprobaci�n.", vbExclamation + vbOKOnly, "Aprobaci�n de solicitudes"
'            '    Exit Sub
'            'End If
'            '}
'            If MsgBox("�Confirma la aprobaci�n de esta solicitud " & Trim(.TextMatrix(.RowSel, colSOL_Nro)) & "?", vbQuestion + vbYesNo, "Aprobaci�n") = vbYes Then
'                Call Puntero(True)
'                Call Status("Generando aprobaci�n...")
'                '{ add -005- a.
'                Call sp_UpdateSolicitudAuto(.TextMatrix(.RowSel, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date)
'                Select Case pUsrPerfilActual
'                    Case "CSEC"
'                        'If InStr(1, "XCOM*|UNLO*|TCOR*|SORT*|", .TextMatrix(.RowSel, colSOL_Tipo), vbTextCompare) > 0 Then
'                        If .TextMatrix(.RowSel, colSOL_EME) <> "Si" Then
'                            If .TextMatrix(.RowSel, colSOL_Tipo) = "XCOM*" Or _
'                                .TextMatrix(.RowSel, colSOL_Tipo) = "UNLO*" Or _
'                                .TextMatrix(.RowSel, colSOL_Tipo) = "TCOR*" Or _
'                                .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                                    Call EnviarCorreo_Restore(.TextMatrix(.RowSel, colSOL_Nro), False)
'                            Else
'                                Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(.RowSel, colSOL_Nro), False)
'                            End If
'                            bEnviados = True
'                        End If
'                    Case "CGRU"
'                        ' S�lo envia aviso de pendientes al Supervisor tras su aprobaci�n
'                        Call EnviarCorreo_Recordatorio(.TextMatrix(.RowSel, colSOL_Nro), False)          ' add -021- b.
'                        bEnviados = True
'                End Select
'                'If pUsrPerfilActual = "CSEC" Then
'                '    If .TextMatrix(.RowSel, colSOL_Tipo) = "XCOM*" Or .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                '        Call EnviarCorreo_Restore(.TextMatrix(.RowSel, colSOL_Nro), False)
'                '    Else
'                '        Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(.RowSel, colSOL_Nro), False)
'                '    End If
'                '    bEnviados = True
'                'End If
'                '}
'                '{ del -005- a.
'                'If .TextMatrix(.RowSel, colSOL_cod_estado) = "A" Then
'                '    Call sp_UpdateSolicitudAuto(.TextMatrix(.RowSel, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date)
'                '    If .TextMatrix(.RowSel, colSOL_Tipo) = "XCOM*" Or .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                '        Call EnviarCorreo_Restore(.TextMatrix(.RowSel, colSOL_Nro), False)
'                '    Else
'                '        Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(.RowSel, colSOL_Nro), False)
'                '    End If
'                '    bEnviados = True
'                'Else
'                '    MsgBox "La solicitud n� " & Trim(.TextMatrix(.RowSel, colSOL_Nro)) & " no requiere aprobaci�n.", vbExclamation + vbOKOnly, "Aprobaci�n de solicitudes"
'                'End If
'                '}
'                If bEnviados Then MsgBox "Email(s) enviado(s).", vbInformation
'                Call CargarGrilla
'                Call Status("Listo.")
'                Call Puntero(False)
'            End If
'        End If
'    End With
'End Sub

Private Sub cmdAprobarTodo_Click()
    Dim bEnviados As Boolean
    Dim i As Long
    'Dim bFlagOK As Boolean
        
    'bFlagOK = False
    bEnviados = False
    
    If grdDatos.Rows > 1 Then
        With grdDatos
            If MsgBox("�Confirma aprobar todo lo pendiente en pantalla?", vbQuestion + vbYesNo, "Aprobaci�n masiva") = vbYes Then
                Call Puntero(True)
                Call Status("Generando aprobaciones...")
                'For i = lRowSel_Ini To lRowSel_Fin
                For i = 1 To .Rows - 1
                    If InStr(1, "A|B|", ClearNull(.TextMatrix(i, colSOL_cod_estado)), vbTextCompare) > 0 Then
                        Call sp_UpdateSolicitudAuto(.TextMatrix(i, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date)
                        If pUsrPerfilActual = "CSEC" Then
                            If .TextMatrix(i, colSOL_EME) <> "Si" Then
                            ' El supervisor envia un mail por cada aprobaci�n que hace (tanto restores como aprobaciones)
                                If .TextMatrix(i, colSOL_Tipo) = "XCOM*" Or _
                                    .TextMatrix(i, colSOL_Tipo) = "UNLO*" Or _
                                    .TextMatrix(i, colSOL_Tipo) = "TCOR*" Or _
                                    .TextMatrix(i, colSOL_Tipo) = "SORT*" Then
                                        Call EnviarCorreo_Restore(.TextMatrix(i, colSOL_Nro), False)
                                Else
                                    Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(i, colSOL_Nro), False)
                                End If
                                bEnviados = True
                            End If
                        End If
                    End If
                Next i
                If pUsrPerfilActual = "CGRU" Then
                    Call EnviarCorreo_Recordatorio(.TextMatrix(i - 1, colSOL_Nro), False)
                    bEnviados = True
                End If
                Call CargarGrilla
                If bEnviados Then MsgBox "Email(s) enviado(s).", vbInformation
                Call Status("Listo.")
                Call Puntero(False)
            End If
        End With
    End If
End Sub

Private Sub cmdReenviar_Click()
    Call ReenviarCorreos
End Sub

Private Sub ReenviarCorreos()
    Dim bEnviados As Boolean
    Dim i As Long
    'Dim bFlagOK As Boolean
    'Dim cPara As String, cCC As String, cAsunto As String, cMensaje As String
        
    'bFlagOK = False
    bEnviados = False
    
    If grdDatos.Rows > 1 Then
        With grdDatos
            'If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje2, cMensaje1), vbQuestion + vbYesNo, "Reenvio") = vbYes Then
            'If MsgBox("�Confirma el reenvio de correo para la selecci�n?", vbQuestion + vbYesNo, "Reenvio") = vbYes Then
            If MsgBox("�Confirma el reenvio de correo de la solicitud " & Trim(.TextMatrix(.RowSel, colSOL_Nro)) & "?", vbQuestion + vbYesNo, "Reenvio") = vbYes Then
                Call Puntero(True)
                'Call Status("Generando correos...")
                Call Status("Reenviando...")
                For i = lRowSel_Ini To lRowSel_Fin
                    'If .TextMatrix(i, colSOL_cod_estado) = "E" And .TextMatrix(i, colSOL_Enm) = "No" Then
                        'If .TextMatrix(.RowSel, colSOL_Tipo) = "XCOM*" Or .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
                    Select Case pUsrPerfilActual
                        Case "CSEC"
                            If .TextMatrix(i, colSOL_Tipo) = "XCOM*" Or _
                                    .TextMatrix(i, colSOL_Tipo) = "UNLO*" Or _
                                    .TextMatrix(i, colSOL_Tipo) = "TCOR*" Or _
                                    .TextMatrix(i, colSOL_Tipo) = "SORT*" Then
                                    
                                    Call EnviarCorreo_Restore(.TextMatrix(i, colSOL_Nro), False)
                            Else
                                Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(i, colSOL_Nro), False)
                            End If
                            bEnviados = True
                        Case "CGRU"
                            Call EnviarCorreo_Recordatorio(.TextMatrix(i, colSOL_Nro), False)
                            bEnviados = True
                    End Select
                    'Else
                    '    Select Case .TextMatrix(i, colSOL_cod_estado)
                    '        Case "A", "B": MsgBox "La solicitud n� " & Trim(.TextMatrix(i, colSOL_Nro)) & " no est� aprobada.", vbExclamation + vbOKOnly, "Solicitud sin aprobaci�n"
                    '        Case Else
                    '            MsgBox "La solicitud n� " & Trim(.TextMatrix(i, colSOL_Nro)) & " no requiere envio de correo.", vbExclamation + vbOKOnly, "Reenvio de correos"
                    '    End Select
                    'End If
                Next i
                If bEnviados Then MsgBox "Email(s) enviado(s).", vbInformation
                Call Status("Listo.")
                Call Puntero(False)
            End If
        End With
    End If
End Sub

Private Sub grdDatos_Click()
    'Debug.Print "Hizo click...grdDatos_Click"
    With grdDatos
        lRowSel_Ini = .Row
        lRowSel_Fin = .RowSel
        If lRowSel_Ini > lRowSel_Fin Then   ' Invierto los valores seg�n corresponda
            lRowSel_Ini = .RowSel
            lRowSel_Fin = .Row
        Else
            lRowSel_Ini = .RowSel
            lRowSel_Fin = .RowSel
        End If
        'Label1 = "1. INI: " & lRowSel_Ini & " / FIN: " & lRowSel_Fin
        Call MostrarSeleccion
    End With
End Sub

'{ add -002- a.
Private Sub grdDatos_SelChange()
    With grdDatos
        lRowSel_Ini = .Row
        lRowSel_Fin = .RowSel
        If lRowSel_Ini > lRowSel_Fin Then   ' Invierto los valores seg�n corresponda
            lRowSel_Ini = .RowSel
            lRowSel_Fin = .Row
        End If
        
        If Not bFlagCargaInicial Then
            If .Rows > 1 Then
                Select Case pUsrPerfilActual
                    Case "CSEC"
                        If InStr(1, "A|B|", ClearNull(.TextMatrix(.RowSel, colSOL_cod_estado)), vbTextCompare) > 0 Then
                            Call setHabilCtrl(cmdAprobar, "NOR")
                            Call setHabilCtrl(cmdAprobarTodo, "NOR")
                            Call setHabilCtrl(cmdReenviar, "DIS")
                        Else
                            Call setHabilCtrl(cmdAprobar, "DIS")
                            Call setHabilCtrl(cmdAprobarTodo, "DIS")
                            Call setHabilCtrl(cmdReenviar, "NOR")
                        End If
                    Case "CGRU"
                        'Call setHabilCtrl(cmdReenviar, "DIS")
                        If ClearNull(.TextMatrix(.RowSel, colSOL_cod_estado)) = "A" Then
                            Call setHabilCtrl(cmdAprobar, "NOR")
                            Call setHabilCtrl(cmdAprobarTodo, "NOR")
                            Call setHabilCtrl(cmdReenviar, "DIS")
                        Else
                            Call setHabilCtrl(cmdAprobar, "DIS")
                            Call setHabilCtrl(cmdAprobarTodo, "DIS")
                            If ClearNull(.TextMatrix(.RowSel, colSOL_cod_estado)) <> "E" Then
                                Call setHabilCtrl(cmdReenviar, "NOR")
                            Else
                                Call setHabilCtrl(cmdReenviar, "DIS")
                            End If
                        End If
                End Select
            End If
        End If
    End With
    'Label1 = "2. INI: " & lRowSel_Ini & " / FIN: " & lRowSel_Fin
End Sub
'}

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    If grdDatos.Row <= grdDatos.RowSel Then
        nRwD = grdDatos.Row
        nRwH = grdDatos.RowSel
    Else
        nRwH = grdDatos.Row
        nRwD = grdDatos.RowSel
    End If
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, colSOL_MultiSelect) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, colSOL_MultiSelect) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        If Button = vbLeftButton Then
            With grdDatos
                If .RowSel > 0 Then
                     If ClearNull(.TextMatrix(.RowSel, colSOL_MultiSelect)) = "" Then
                        .TextMatrix(.RowSel, colSOL_MultiSelect) = "�"
                     Else
                        .TextMatrix(.RowSel, colSOL_MultiSelect) = ""
                    End If
                End If
            End With
        End If
    End If
    If Shift = 0 Then
        If Button = vbLeftButton Then
            With grdDatos
                For i = 1 To .Rows - 1
                   .TextMatrix(i, colSOL_MultiSelect) = ""
                Next
                If .RowSel > 0 Then
                     .TextMatrix(.RowSel, colSOL_MultiSelect) = "�"
                End If
            End With
        End If
    End If
End Sub

'Private Sub cmdAprobar_Click()
'    Dim i As Long
'    Dim bFlagOK As Boolean
'    Const cMensaje1 = "�Confirma la aprobaci�n de esta solicitud?"
'    Const cMensaje2 = "�Confirma la aprobaci�n de estas solicitudes?"
'    Dim cPara As String, cCC As String, cAsunto As String, cMensaje As String
'
'    bFlagOK = False
'
'    If grdDatos.Rows > 1 Then
'        If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje2, cMensaje1), vbQuestion + vbYesNo, "Aprobaci�n") = vbYes Then
'            With grdDatos
'                Call Puntero(True)
'                Call Status("Generando aprobaciones...")
'                If lRowSel_Ini <> lRowSel_Fin Then
'                    For i = lRowSel_Ini To lRowSel_Fin
'                        If .TextMatrix(i, colSOL_cod_estado) = "A" Then
'                            If Not sp_UpdateSolicitudAuto(.TextMatrix(i, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date) Then
'                                MsgBox "Error al procesar " & i
'                            Else
'                                bFlagOK = True
'                                '{ add -003- a.
'                                If .TextMatrix(.RowSel, 1) = "XCOM*" Or .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                                    Call FuncionesCorreo2.EnviarCorreo_Restore(grdDatos.TextMatrix(i, colSOL_Nro), cPara, cCC, cAsunto, cMensaje)
'                                    Call Puntero(False)
'                                    With auxMensajeCorreo
'                                        .cPara = cPara
'                                        .cCC = cCC
'                                        .cAsunto = cAsunto
'                                        .cMensaje = cMensaje
'                                        .Show 1
'                                    End With
'                                Else
'                                    Call FuncionesCorreo2.EnviarCorreo_CargaDeMaquinas(grdDatos.TextMatrix(i, colSOL_Nro), cPara, cCC, cAsunto, cMensaje)
'                                    Call Puntero(False)
'                                    With auxMensajeCorreo
'                                        .cPara = cPara
'                                        .cCC = cCC
'                                        .cAsunto = cAsunto
'                                        .cMensaje = cMensaje
'                                        .Show 1
'                                    End With
'                                End If
'                                '}
'                            End If
'                        Else
'                            MsgBox "La solicitud n� " & Trim(.TextMatrix(i, colSOL_Nro)) & " ya se encuentra aprobada.", vbExclamation + vbOKOnly, "Solicitud aprobada"
'                        End If
'                    Next i
'                Else
'                    If .TextMatrix(.RowSel, colSOL_cod_estado) = "A" Then
'                        If Not sp_UpdateSolicitudAuto(.TextMatrix(.RowSel, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date) Then
'                            MsgBox "Error al procesar " & .RowSel
'                        Else
'                            bFlagOK = True
'                            '{ add -003- a.
'                            If .TextMatrix(.RowSel, colSOL_Tipo) = "XCOM*" Or _
'                                .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                                Call FuncionesCorreo2.EnviarCorreo_Restore(grdDatos.TextMatrix(grdDatos.RowSel, colSOL_Nro), cPara, cCC, cAsunto, cMensaje)
'                                Call Puntero(False)
'                                With auxMensajeCorreo
'                                    .cPara = cPara
'                                    .cCC = cCC
'                                    .cAsunto = cAsunto
'                                    .cMensaje = cMensaje
'                                    .Show 1
'                                End With
'                            Else
'                                Call FuncionesCorreo2.EnviarCorreo_CargaDeMaquinas(grdDatos.TextMatrix(grdDatos.RowSel, colSOL_Nro), cPara, cCC, cAsunto, cMensaje)
'                                Call Puntero(False)
'                                With auxMensajeCorreo
'                                    .cPara = cPara
'                                    .cCC = cCC
'                                    .cAsunto = cAsunto
'                                    .cMensaje = cMensaje
'                                    .Show 1
'                                End With
'                            End If
'                            '}
'                        End If
'                    Else
'                        MsgBox "La solicitud no se encuentra pendiente de aprobaci�n. Revise.", vbExclamation + vbOKOnly, "Aprobaci�n"
'                    End If
'                End If
'            End With
'            If bFlagOK Then CargarGrilla
'            Call Status("Listo.")
'            Call Puntero(False)
'        End If
'    End If
'End Sub

'Private Sub Aprobar()
'    Dim i As Long
'    Dim bFlagOK As Boolean
'    Const cMensaje1 = "�Confirma la aprobaci�n de esta solicitud?"
'    Const cMensaje2 = "�Confirma la aprobaci�n de estas solicitudes?"
'    'Dim cPara As String, cCC As String, cAsunto As String, cMensaje As String
'
'    bFlagOK = False
'
'    If grdDatos.Rows > 1 Then
'        If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje2, cMensaje1), vbQuestion + vbYesNo, "Aprobaci�n") = vbYes Then
'            With grdDatos
'                Call Puntero(True)
'                Call Status("Generando aprobaciones...")
'                If lRowSel_Ini <> lRowSel_Fin Then
'                    For i = lRowSel_Ini To lRowSel_Fin
'                        If .TextMatrix(i, colSOL_cod_estado) = "A" Then
'                            If Not sp_UpdateSolicitudAuto(.TextMatrix(i, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date) Then
'                                MsgBox "Error al procesar " & i
'                            Else
'                                bFlagOK = True
'                                If .TextMatrix(.RowSel, 1) = "XCOM*" Or .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                                    Call EnviarCorreo_Restore(.TextMatrix(i, colSOL_Nro))
'                                Else
'                                    Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(i, colSOL_Nro))
'                                End If
'                            End If
'                        Else
'                            MsgBox "La solicitud n� " & Trim(.TextMatrix(i, colSOL_Nro)) & " ya se encuentra aprobada.", vbExclamation + vbOKOnly, "Solicitud aprobada"
'                        End If
'                    Next i
'                Else
'                    If .TextMatrix(.RowSel, colSOL_cod_estado) = "A" Then
'                        If Not sp_UpdateSolicitudAuto(.TextMatrix(.RowSel, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date) Then
'                            MsgBox "Error al procesar " & .RowSel
'                        Else
'                            bFlagOK = True
'                            If .TextMatrix(.RowSel, colSOL_Tipo) = "XCOM*" Or _
'                                .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                                Call EnviarCorreo_Restore(grdDatos.TextMatrix(grdDatos.RowSel, colSOL_Nro))
'                            Else
'                                Call EnviarCorreo_CargaDeMaquinas(grdDatos.TextMatrix(grdDatos.RowSel, colSOL_Nro))
'                            End If
'                        End If
'                    Else
'                        MsgBox "La solicitud no se encuentra pendiente de aprobaci�n. Revise.", vbExclamation + vbOKOnly, "Aprobaci�n"
'                    End If
'                End If
'            End With
'            If bFlagOK Then CargarGrilla
'            Call Status("Listo.")
'            Call Puntero(False)
'        End If
'    End If
'End Sub

'Private Sub ReenviarCorreos()
'    Dim i As Long
'    Dim bFlagOK As Boolean
'    Dim cPara As String, cCC As String, cAsunto As String, cMensaje As String
'
'    bFlagOK = False
'
'    If grdDatos.Rows > 1 Then
'        With grdDatos
'            Call Puntero(True)
'            Call Status("Generando correos...")
'            If lRowSel_Ini <> lRowSel_Fin Then
'                For i = lRowSel_Ini To lRowSel_Fin
'                    If .TextMatrix(i, 14) = "E" Then
'                        'Call FuncionesCorreo2.EnviarCorreo_CargaDeMaquinas(.TextMatrix(i, colSOL_Nro), cPara, cCC, cAsunto, cMensaje)
'                        Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(i, colSOL_Nro))
'                        Call Puntero(False)
'                    Else
'                        MsgBox "La solicitud n� " & Trim(.TextMatrix(i, colSOL_Nro)) & " no se encuentra aprobada.", vbExclamation + vbOKOnly, "Solicitud sin aprobaci�n"
'                    End If
'                Next i
'            Else
'                If .TextMatrix(.RowSel, colSOL_cod_estado) = "E" Then
'                    Select Case .TextMatrix(.RowSel, colSOL_Tipo)
'                        'Case "XCOM*", "SORT*": Call EnviarCorreo_Restore(.TextMatrix(.RowSel, colSOL_Nro), cPara, cCC, cAsunto, cMensaje)
'                        Case "XCOM*", "SORT*": Call EnviarCorreo_Restore(.TextMatrix(.RowSel, colSOL_Nro))
'                        Case Else: Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(.RowSel, colSOL_Nro))
'                    End Select
'                    Call Puntero(False)
'                Else
'                    If .TextMatrix(.RowSel, colSOL_cod_estado) = "A" Then
'                        MsgBox "La solicitud seleccionada no ha sido aprobada. Revise.", vbExclamation + vbOKOnly, "Solicitud sin aprobaci�n"
'                    Else
'                        MsgBox "La solicitud seleccionada fu� generada por los canales normales. Revise.", vbExclamation + vbOKOnly, "Solicitud normal"
'                    End If
'                End If
'            End If
'        End With
'        Call Status("Listo.")
'        Call Puntero(False)
'    End If
'End Sub

'Private Sub Aprobar()
'    Dim bEnviados As Boolean
'    Dim i As Long
'    Const cMensaje1 = "�Confirma la aprobaci�n de esta solicitud?"
'    Const cMensaje2 = "�Confirma la aprobaci�n de estas solicitudes?"
'
'    bEnviados = False
'    If grdDatos.Rows > 1 Then
'        If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje2, cMensaje1), vbQuestion + vbYesNo, "Aprobaci�n") = vbYes Then
'            With grdDatos
'                Call Puntero(True)
'                Call Status("Generando aprobaciones...")
'                For i = lRowSel_Ini To lRowSel_Fin
'                    If .TextMatrix(i, colSOL_cod_estado) = "A" Then
'                        Call sp_UpdateSolicitudAuto(.TextMatrix(i, colSOL_Nro), glLOGIN_ID_REEMPLAZO, pUsrPerfilActual, date)
'                        If .TextMatrix(.RowSel, colSOL_Tipo) = "XCOM*" Or .TextMatrix(.RowSel, colSOL_Tipo) = "SORT*" Then
'                            Call EnviarCorreo_Restore(.TextMatrix(i, colSOL_Nro), False)
'                        Else
'                            Call EnviarCorreo_CargaDeMaquinas(.TextMatrix(i, colSOL_Nro), False)
'                        End If
'                        bEnviados = True
'                    Else
'                        MsgBox "La solicitud n� " & Trim(.TextMatrix(i, colSOL_Nro)) & " no requiere aprobaci�n.", vbExclamation + vbOKOnly, "Aprobaci�n de solicitudes"
'                    End If
'                Next i
'                If bEnviados Then MsgBox "Email(s) enviado(s).", vbInformation
'            End With
'            Call CargarGrilla
'            Call Status("Listo.")
'            Call Puntero(False)
'        End If
'    End If
'End Sub
