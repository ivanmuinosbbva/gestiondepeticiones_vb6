VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesShellView01ExpoXP 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Campos a incluir en la exportaci�n"
   ClientHeight    =   8130
   ClientLeft      =   3780
   ClientTop       =   5415
   ClientWidth     =   11880
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesShellView01ExpoXP.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   8130
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame frmPet 
      Caption         =   " Petici�n "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00D28D55&
      Height          =   6555
      Left            =   30
      TabIndex        =   29
      Top             =   120
      Width           =   6945
      Begin MSFlexGridLib.MSFlexGrid grdDatos 
         Height          =   6135
         Left            =   240
         TabIndex        =   95
         Top             =   240
         Width           =   6585
         _ExtentX        =   11615
         _ExtentY        =   10821
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.CheckBox chkPetProyectoIDM 
         Caption         =   "Proyecto IDM"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   5160
         TabIndex        =   91
         Top             =   540
         Width           =   1380
      End
      Begin VB.CheckBox chkPetConfHomo 
         Caption         =   "Conformes de homologaci�n"
         ForeColor       =   &H00000000&
         Height          =   280
         Left            =   270
         TabIndex        =   90
         Top             =   5640
         Width           =   2340
      End
      Begin VB.CheckBox chkPetRegulatorio 
         Caption         =   "Regulatorio"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   5160
         TabIndex        =   88
         Top             =   270
         Width           =   1140
      End
      Begin VB.CheckBox chkHorasRealesOtros 
         Caption         =   "Incluir total de horas reales no asignadas"
         Enabled         =   0   'False
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   3120
         TabIndex        =   79
         Top             =   5400
         Width           =   3345
      End
      Begin VB.CheckBox chkHorasReales 
         Caption         =   "Total de horas reales asignadas a la petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   78
         Top             =   5130
         Width           =   4080
      End
      Begin VB.CommandButton cmdStdMark 
         Caption         =   "Marcar todos"
         Height          =   420
         Left            =   5265
         TabIndex        =   75
         Top             =   5805
         Width           =   1500
      End
      Begin VB.CheckBox CHKrepla 
         Caption         =   "Cant. Replanificaciones"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   67
         Top             =   2970
         Width           =   2400
      End
      Begin VB.CheckBox CHKfe_fin_orig 
         Caption         =   "F.Fin Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   66
         Top             =   2700
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_ini_orig 
         Caption         =   "F.Ini Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   65
         Top             =   2430
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_ini_real 
         Caption         =   "F.Ini Real. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   64
         Top             =   1890
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_fin_real 
         Caption         =   "F.Fin Real. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   63
         Top             =   2160
         Width           =   2100
      End
      Begin VB.CheckBox CHKnom_orientacion 
         Caption         =   "Orientaci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   62
         Top             =   3780
         Width           =   1410
      End
      Begin VB.CheckBox CHKfe_fin_plan 
         Caption         =   "F.Fin Planif. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   61
         Top             =   1620
         Width           =   2100
      End
      Begin VB.CheckBox CHKfe_ini_plan 
         Caption         =   "F.Ini Planif. Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   60
         Top             =   1350
         Width           =   2100
      End
      Begin VB.CheckBox CHKhoraspresup 
         Caption         =   "Horas Presup. Pet."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   59
         Top             =   1080
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_situacion 
         Caption         =   "Situaci�n Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   58
         Top             =   3780
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_fe_estado 
         Caption         =   "Fec. Estado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   57
         Top             =   3510
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_estado 
         Caption         =   "Estado Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   56
         Top             =   3240
         Width           =   2100
      End
      Begin VB.CheckBox CHKprioridad 
         Caption         =   "Prioridad"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   55
         ToolTipText     =   "Categor�a"
         Top             =   2700
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_nroasignado 
         Caption         =   "Petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   54
         ToolTipText     =   "N�mero de Petici�n"
         Top             =   270
         Width           =   2100
      End
      Begin VB.CheckBox CHKcod_tipo_peticion 
         Caption         =   "Tipo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   53
         ToolTipText     =   "Tipo"
         Top             =   540
         Width           =   2100
      End
      Begin VB.CheckBox CHKtitulo 
         Caption         =   "T�tulo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   52
         ToolTipText     =   "T�tulo"
         Top             =   810
         Width           =   2100
      End
      Begin VB.CheckBox CHKpet_sector 
         Caption         =   "Sector solicitante"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   51
         Top             =   4050
         Width           =   2100
      End
      Begin VB.CheckBox CHKimportancia_nom 
         Caption         =   "Visibilidad"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   50
         Top             =   3510
         Width           =   2100
      End
      Begin VB.CheckBox CHKcorp_local 
         Caption         =   "C/L"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   49
         ToolTipText     =   "Corporativa / Local"
         Top             =   3240
         Width           =   2100
      End
      Begin VB.CheckBox CHKdetalle 
         Caption         =   "Descripci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   48
         ToolTipText     =   "T�tulo"
         Top             =   1080
         Width           =   2100
      End
      Begin VB.CheckBox CHKcaracte 
         Caption         =   "Caracter�sticas"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   47
         ToolTipText     =   "T�tulo"
         Top             =   1350
         Width           =   2100
      End
      Begin VB.CheckBox CHKmotivos 
         Caption         =   "Motivos"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   46
         ToolTipText     =   "T�tulo"
         Top             =   1620
         Width           =   2100
      End
      Begin VB.CheckBox CHKbenefic 
         Caption         =   "Beneficios"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   45
         ToolTipText     =   "T�tulo"
         Top             =   1890
         Width           =   2100
      End
      Begin VB.CheckBox CHKrelacio 
         Caption         =   "Relac c/Otros Req."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   44
         ToolTipText     =   "T�tulo"
         Top             =   2160
         Width           =   2190
      End
      Begin VB.CheckBox CHKobserva 
         Caption         =   "Observaciones"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   43
         ToolTipText     =   "T�tulo"
         Top             =   2430
         Width           =   2100
      End
      Begin VB.CheckBox CHKsoli 
         Caption         =   "Solicitante"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   42
         Top             =   4590
         Width           =   2100
      End
      Begin VB.CheckBox CHKrefe 
         Caption         =   "Referente"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   41
         Top             =   270
         Width           =   2100
      End
      Begin VB.CheckBox CHKsupe 
         Caption         =   "Supervisor"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   40
         Top             =   540
         Width           =   2100
      End
      Begin VB.CheckBox CHKauto 
         Caption         =   "Autorizante"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   39
         Top             =   810
         Width           =   2100
      End
      Begin VB.CheckBox CHKhistorial 
         Caption         =   "Historial"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   38
         Top             =   4050
         Width           =   1100
      End
      Begin VB.CheckBox CHKfe_ing_comi 
         Caption         =   "F.Env�o B.Partner"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   37
         Top             =   2970
         Width           =   2100
      End
      Begin VB.CheckBox CHKpcf 
         Caption         =   "Conformes"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   36
         Top             =   4320
         Width           =   1155
      End
      Begin VB.CheckBox CHKbpar 
         Caption         =   "B.Partner"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   35
         Top             =   4320
         Width           =   2100
      End
      Begin VB.CheckBox CHKseguimN 
         Caption         =   "Nro Seguimiento Asociado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   34
         Top             =   4590
         Width           =   2655
      End
      Begin VB.CheckBox CHKseguimT 
         Caption         =   "Tit. Seguimiento Asociado"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   2760
         TabIndex        =   33
         Top             =   4860
         Width           =   2655
      End
      Begin VB.CheckBox chkPetDocu 
         Caption         =   "Documentos adjuntos"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   32
         Top             =   5400
         Width           =   2265
      End
      Begin VB.CheckBox chkPetImpTech 
         Caption         =   "Impacto tecnol�gico"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   31
         Top             =   5130
         Width           =   2145
      End
      Begin VB.CheckBox chkPetClass 
         Caption         =   "Clase de petici�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   270
         TabIndex        =   30
         Top             =   4860
         Width           =   1905
      End
      Begin VB.Label lblAdvertencia 
         AutoSize        =   -1  'True
         Caption         =   "Advertencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   87
         Top             =   6000
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label lblAdvertencia 
         AutoSize        =   -1  'True
         Caption         =   "Esta opci�n puede incrementar demoras importantes para finalizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   86
         Top             =   6240
         Visible         =   0   'False
         Width           =   4755
      End
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7965
      Left            =   10590
      TabIndex        =   0
      Top             =   120
      Width           =   1275
      Begin VB.CommandButton cmdScreen 
         Caption         =   "Generar y Visualizar"
         Height          =   500
         Left            =   60
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Efectua la exportaci�n incluyendo los campos seleccionados y abre la planilla resultante luego de generar"
         Top             =   660
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Generar"
         Default         =   -1  'True
         Height          =   500
         Left            =   60
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Efectua la exportaci�n incluyendo los campos seleccionados"
         Top             =   150
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Sale de la pantalla"
         Top             =   7380
         Width           =   1170
      End
   End
   Begin VB.Frame frmSec 
      Caption         =   " Sector "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6555
      Left            =   7020
      TabIndex        =   26
      Top             =   120
      Visible         =   0   'False
      Width           =   3525
      Begin VB.CheckBox chkHorasRealesSector 
         Caption         =   "Horas reales x Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   80
         Top             =   2760
         Width           =   1980
      End
      Begin VB.CommandButton cmdSecMark 
         Caption         =   "Marcar todos"
         Height          =   420
         Left            =   1890
         TabIndex        =   77
         Top             =   5805
         Width           =   1500
      End
      Begin VB.CheckBox CHKsec_fe_fin_orig 
         Caption         =   "F.Fin Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   25
         Top             =   2520
         Width           =   2100
      End
      Begin VB.CheckBox CHKsec_fe_ini_orig 
         Caption         =   "F.Ini Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   24
         Top             =   2280
         Width           =   2100
      End
      Begin VB.CheckBox CHKsec_fe_ini_real 
         Caption         =   "F.Ini Real. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   22
         Top             =   1800
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_fe_fin_real 
         Caption         =   "F.Fin Real. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   23
         Top             =   2040
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_fe_fin_plan 
         Caption         =   "F.Fin Planif. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   21
         Top             =   1560
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_fe_ini_plan 
         Caption         =   "F.Ini Planif. Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   20
         Top             =   1320
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_horaspresup 
         Caption         =   "Horas Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   19
         Top             =   1080
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_situacion 
         Caption         =   "Situaci�n Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   18
         Top             =   840
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_estado 
         Caption         =   "Estado Sector"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   17
         Top             =   600
         Width           =   2500
      End
      Begin VB.CheckBox CHKsec_sector 
         Caption         =   "Sector Ejecuci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Width           =   2500
      End
   End
   Begin VB.Frame frmAgrup 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1480
      Left            =   30
      TabIndex        =   68
      Top             =   6600
      Width           =   10515
      Begin VB.CheckBox CHKgantt 
         Caption         =   "Diagrama de Gantt"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   6600
         TabIndex        =   93
         Top             =   285
         Width           =   2025
      End
      Begin VB.ComboBox cboGantt 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmPeticionesShellView01ExpoXP.frx":014A
         Left            =   8895
         List            =   "frmPeticionesShellView01ExpoXP.frx":014C
         Style           =   2  'Dropdown List
         TabIndex        =   92
         Top             =   240
         Width           =   1410
      End
      Begin VB.CommandButton cmdOpen 
         Height          =   315
         Left            =   5520
         Picture         =   "frmPeticionesShellView01ExpoXP.frx":014E
         Style           =   1  'Graphical
         TabIndex        =   89
         Top             =   1080
         Width           =   375
      End
      Begin VB.CommandButton cmdClrAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5880
         Picture         =   "frmPeticionesShellView01ExpoXP.frx":0490
         Style           =   1  'Graphical
         TabIndex        =   70
         TabStop         =   0   'False
         ToolTipText     =   "Limpia agrupamiento"
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton cmdAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5520
         Picture         =   "frmPeticionesShellView01ExpoXP.frx":07D2
         Style           =   1  'Graphical
         TabIndex        =   69
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar agrupamiento para incluir informaci�n de pertenencia"
         Top             =   480
         Width           =   375
      End
      Begin AT_MaskText.MaskText txtAgrup 
         Height          =   315
         Left            =   120
         TabIndex        =   71
         Top             =   480
         Width           =   5385
         _ExtentX        =   9499
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFile 
         Height          =   315
         Left            =   120
         TabIndex        =   72
         Top             =   1080
         Width           =   5385
         _ExtentX        =   9499
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label lblGantt 
         AutoSize        =   -1  'True
         Caption         =   "Fechas planificadas"
         Height          =   195
         Left            =   6870
         TabIndex        =   94
         Top             =   525
         Width           =   1395
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Incluir informaci�n de pertenencia por Agrupamiento"
         Height          =   195
         Left            =   120
         TabIndex        =   74
         Top             =   240
         Width           =   3765
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Ubicaci�n y nombre del archivo de salida de la exportaci�n"
         Height          =   195
         Left            =   120
         TabIndex        =   73
         Top             =   840
         Width           =   4185
      End
   End
   Begin VB.Frame frmGru 
      Caption         =   " Grupo "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6555
      Left            =   7020
      TabIndex        =   27
      Top             =   120
      Visible         =   0   'False
      Width           =   3525
      Begin VB.CheckBox chkObservEstado 
         Caption         =   "Observaciones del estado actual"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   85
         Top             =   4440
         Width           =   2715
      End
      Begin VB.CheckBox chkMotivoSuspension 
         Caption         =   "Motivo de suspensi�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   84
         Top             =   4200
         Width           =   1905
      End
      Begin VB.CheckBox chkFechaSuspension 
         Caption         =   "F. Fin. Suspensi�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   83
         Top             =   3960
         Width           =   1665
      End
      Begin VB.CheckBox chkFechaProduccion 
         Caption         =   "F. Prev. pasaje a Producci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   82
         Top             =   3720
         Width           =   2385
      End
      Begin VB.CheckBox chkHorasRealesGrupo 
         Caption         =   "Horas reales x Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   81
         Top             =   3480
         Width           =   1980
      End
      Begin VB.CommandButton cmdGrpMark 
         Caption         =   "Marcar todos"
         Height          =   420
         Left            =   1890
         TabIndex        =   76
         Top             =   5805
         Width           =   1500
      End
      Begin VB.CheckBox CHKgru_fe_fin_orig 
         Caption         =   "F.Fin Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   11
         Top             =   2760
         Width           =   2100
      End
      Begin VB.CheckBox CHKgru_fe_ini_orig 
         Caption         =   "F.Ini Planif. Orig."
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   10
         Top             =   2520
         Width           =   2100
      End
      Begin VB.CheckBox CHKgru_fe_ini_real 
         Caption         =   "F.Ini Real. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   8
         Top             =   2040
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_fe_fin_real 
         Caption         =   "F.Fin real. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   9
         Top             =   2280
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_recursos 
         Caption         =   "Recursos asignados"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   12
         Top             =   3000
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_info_adic 
         Caption         =   "Info Adic. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   15
         Top             =   3240
         Width           =   1545
      End
      Begin VB.CheckBox CHKgru_fe_fin_plan 
         Caption         =   "F.Fin Planif. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   7
         Top             =   1800
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_fe_ini_plan 
         Caption         =   "F.Ini Planif. Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   6
         Top             =   1560
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_horaspresup 
         Caption         =   "Horas Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   5
         Top             =   1320
         Width           =   1305
      End
      Begin VB.CheckBox CHKgru_situacion 
         Caption         =   "Situaci�n Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_estado 
         Caption         =   "Estado Grupo"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   3
         Top             =   840
         Width           =   2500
      End
      Begin VB.CheckBox CHKprio_ejecuc 
         Caption         =   "Prioridad Ejecuci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   2500
      End
      Begin VB.CheckBox CHKgru_sector 
         Caption         =   "Grupo Ejecuci�n"
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   2500
      End
   End
End
Attribute VB_Name = "frmPeticionesShellView01ExpoXP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 04.07.2007 - Se agregan tres controles checkbox para soportar los nuevos campos y la opci�n de incluir los documentos adjuntos de la petici�n
' -001- b. FJS 06.07.2007 - Se agregan los nuevos par�metros a la llamada al SP.
' -001- c. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -001- d. FJS 14.08.2007 - Se modifica el Look & Feel de la pantalla y se agregan algunas opciones para facilitar las tareas del usuario.
' -002- a. FJS 18.09.2007 - Se mueve esta columna al final de la planilla (columna de documentos adjuntos).
' -003- a. FJS 11.03.2008 - Se agrega la opci�n de exportaci�n de una columna que sumariza las horas reales al momento para una petici�n cuando el pedido es a nivel de petici�n.
' -003- b. FJS 07.04.2008 -
' -004- a. FJS 16.05.2008 - Se completa en la exportaci�n los indicadores de documentos adjuntos que posee la petici�n
' -005- a. FJS 16.05.2008 - Se agregan los defaults para las opciones de campos de exportaci�n que estaban faltando
' -006- a. FJS 23.05.2008 - Se agregan 4 campos nuevos a exportar por el tema de homologaci�n (todos a nivel de grupo): Fecha de puesta en Producci�n, Fecha de fin de suspensi�n, motivo de suspensi�n y las observaciones del estado actual.
' -007- a. FJS 06.08.2008 - Se reemplaza el T700 anterior por el nuevo producto T710.
' -008- a. FJS 10.09.2008 - Se agrega el identificador de Regulatorio como dato a exportar.
' -009- a. FJS 12.09.2008 - Se agregan las opciones de exportar si tiene conformes de homologaci�n.
' -010- a. FJS 25.09.2008 - Se agrega la opci�n de exportar el proyecto IDM relacionado si tiene.
' -011- a. FJS 10.12.2008 - Se corrige que al seleccionar todas las opciones, la de exportar el historial queda seleccionada (y solo deber�a valer para los perfiles habilitados).
' -011- b. FJS 10.12.2008 - Se agrega que contemple el perfil de SADM tambi�n.
' -012- a. FJS 27.01.2009 - Optimizaci�n de armado del historial para la exportaci�n: problemas de performance con el algoritmo anterior.
' -013- a. FJS 26.03.2009 - Agregado de peticiones hist�ricas. Se tuvo que abrir el procedimiento principal (Generar) porque excedia el tama�o m�ximo soportado por el compilador (64K).
' -014- a. FJS 26.02.2010 - En el momento de la generaci�n de la planilla, se inhabilitan los frames para evitar que se modifiquen las opciones de generaci�n original.
' -015- a. FJS 14.04.2010 - Arreglo: se cambia el evento donde se cargan las opciones de selecci�n porque no esta tomando correctamente los cambios en los par�metros en el evento Load.
' -016- a. FJS 10.11.2010 - Mejora: se permite usar el Common Dialog para seleccionar la ubicaci�n y el nombre dle archivo a exportar.

Option Explicit

Dim flgEnCarga As Boolean

Private petTipo As String
Private petPrioridad As String
Private petNivel As String
Private petArea As String
Private petAreaTxt As String
Private petEstado As String
Private petSituacion As String
Private petTitu As String
Private petImportancia As String
'{ add -001- a.
Private petClase As String
Private petImpTech As String
Private petDocument As String
'}
Private secNivel As String
'Private secArea As String
Private secAreaTxt As String
Private secEstado As String
Private secSituacion As String
Private auxFiltro As String
Private flgHijos As Boolean
Private auxAgrup As String
Private secDire As String
Private secGere As String
Private secSect As String
Private secGrup As String
'{ add -003- a.
Dim rsAux As ADODB.Recordset
Dim rsPet As ADODB.Recordset
Dim nPet_NroInterno_Actual As Long
Dim nPet_NroInterno_Anterior As Long
'}

Private bScreen As Boolean

'{ add -013- a.
Dim ExcelApp, ExcelWrk, xlSheet As Object
Dim iContador As Long
Dim cntRows As Integer
Dim cntLast As Integer
Dim grantBeg As Integer
Dim grantEnd As Integer
Dim auxDate As Date
Dim j As Integer
'}

Private Const colNroInterno = 1
Private Const colFecPlanIni = 2
Private Const colFecPlanFin = 3
Private Const colEstadoPet = 4

Private Const XLleft = -4131
Private Const XLcenter = -4108
Private Const XLright = -4152

Private Const grdEXP_ID = 0
Private Const grdEXP_GRUPO = 1
Private Const grdEXP_DESC = 2
Private Const grdEXP_CODIGO = 3
Private Const grdEXP_ORDEN = 4
Private Const grdEXP_SI = 5
Private Const grdEXP_NO = 6
Private Const grdEXP_TAG = 7
Private Const grdEXP_ALIGNMENT = 8
Private Const grdEXP_WIDTH = 9
Private Const grdEXP_WRAPTEXT = 10
Private Const grdEXP_ORIENTATION = 11
Private Const grdEXP_INDENTLEVEL = 12
Private Const grdEXP_SHRINKTOFIT = 13
Private Const grdEXP_MERGECELLS = 14
Private Const grdEXP_NOMCAMPO = 15

Private Sub Form_Load()
    Call IniciarScroll(grdDatos)
    Call Inicializar                        ' add -015- a.
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
End Sub

'{ add -015- a.
Private Sub Inicializar()
    Dim vestados() As String
    Dim vRetorno() As String
    
    Cargar_Grilla
    
    With frmPeticionesShellView01
        petTipo = CodigoCombo(.cboTipopet, True)
        petClase = CodigoCombo(.cmbPetClass, True)
        petImpTech = IIf(.cmbPetImpTech.Text = "Si", "S", "N")
        petDocument = IIf(.cmbPetDocu.Text = "Si", "S", "N")
        petPrioridad = CodigoCombo(.cboPrioridad, True)
        petNivel = .verNivelPet()
        petArea = .verAreaPet()
        petEstado = DatosCombo(.cboEstadoPet)
        petImportancia = CodigoCombo(.cboImportancia, True)
        petSituacion = CodigoCombo(.cboSituacionPet, True)
        secNivel = .verNivelSec()
        Call .verAreaSec
        secDire = .secDire
        secGere = .secGere
        secSect = .secSect
        secGrup = .secGrup
        secEstado = DatosCombo(.cboEstadoSec)
        secSituacion = CodigoCombo(.cboSituacionSec, True)
        If ClearNull(.txtTitulo.Text) = "" Then
            petTitu = "NULL"
        Else
            petTitu = ClearNull(.txtTitulo.Text)
        End If
        If ClearNull(.txtAgrup.Text) <> "" Then
            If ParseString(vRetorno, .txtAgrup.Text, "|") > 0 Then
                auxAgrup = vRetorno(2)
            End If
        Else
            auxAgrup = ""
        End If
        If secNivel = "NULL" And secDire = "NULL" And secGere = "NULL" And secSect = "NULL" And secGrup = "NULL" And secEstado = "NULL" And secSituacion = "NULL" And _
            IsNull(.txtDESDEiniplan.DateValue) And IsNull(.txtHASTAiniplan.DateValue) And IsNull(.txtDESDEfinplan.DateValue) And IsNull(.txtHASTAfinplan.DateValue) And _
            IsNull(.txtDESDEinireal.DateValue) And IsNull(.txtHASTAinireal.DateValue) And IsNull(.txtDESDEfinreal.DateValue) And IsNull(.txtHASTAfinreal.DateValue) Then
            flgHijos = False
        Else
            flgHijos = True
        End If
        If flgHijos Then
            If secNivel = "GRUP" Then
                frmGru.Visible = True
                frmGru.Enabled = True
            Else
                frmSec.Visible = True
                frmSec.Enabled = True
            End If
            If secGrup = "NULL" Then
                CHKgru_recursos.Value = 0
                CHKgru_recursos.Enabled = False
            End If
        End If
    End With
    If secNivel = "NULL" Then
        cboGantt.AddItem "Petici�n" & ESPACIOS & "||PETI"
    End If
    If secNivel = "SECT" Then
        cboGantt.AddItem "Sector" & ESPACIOS & "||SECT"
        cboGantt.AddItem "Petici�n" & ESPACIOS & "||PETI"
    End If
    If secNivel = "GRUP" Then
        cboGantt.AddItem "Grupo" & ESPACIOS & "||GRUP"
        cboGantt.AddItem "Petici�n" & ESPACIOS & "||PETI"
    End If
    If CHKgantt.Value = 1 Then
        cboGantt.ListIndex = 0
        cboGantt.Enabled = True
    Else
        cboGantt.ListIndex = -1
        cboGantt.Enabled = False
    End If
    If chkHorasReales.Value = 1 Then
        chkHorasRealesOtros.Enabled = True
    Else
        chkHorasRealesOtros.Value = 0
        chkHorasRealesOtros.Enabled = False
    End If
End Sub
'}

'{ add -003- a.
Private Sub chkHorasReales_Click()
    If chkHorasReales.Value = 1 Then
        lblAdvertencia(0).Visible = True
        lblAdvertencia(1).Visible = True
        chkHorasReales.FontBold = True
        chkHorasReales.ForeColor = vbRed
        chkHorasRealesOtros.Enabled = True
    Else
        lblAdvertencia(0).Visible = False
        lblAdvertencia(1).Visible = False
        chkHorasReales.FontBold = False
        chkHorasReales.ForeColor = vbBlack
        chkHorasRealesOtros.Value = 0
        chkHorasRealesOtros.Enabled = False
    End If
End Sub
'}

'{ add -001- d.
Private Sub cmdGrpMark_Click()
    If cmdGrpMark.Caption = "Marcar todos" Then
        cmdGrpMark.Caption = "Desmarcar todos"
        CHKgru_sector.Value = 1
        CHKprio_ejecuc.Value = 1
        CHKgru_estado.Value = 1
        CHKgru_situacion.Value = 1
        CHKgru_horaspresup.Value = 1
        CHKgru_fe_ini_plan.Value = 1
        CHKgru_fe_fin_plan.Value = 1
        CHKgru_fe_ini_real.Value = 1
        CHKgru_fe_fin_real.Value = 1
        CHKgru_fe_ini_orig.Value = 1
        CHKgru_fe_fin_orig.Value = 1
        CHKgru_info_adic.Value = 1
        CHKgru_recursos.Value = 1
        chkHorasRealesGrupo.Value = 1   ' add -003- a.
        '{ add -006- a.
        chkFechaProduccion.Value = 1
        chkFechaSuspension.Value = 1
        chkMotivoSuspension.Value = 1
        chkObservEstado.Value = 1
        '}
    Else
        cmdGrpMark.Caption = "Marcar todos"
        CHKgru_sector.Value = 0
        CHKprio_ejecuc.Value = 0
        CHKgru_estado.Value = 0
        CHKgru_situacion.Value = 0
        CHKgru_horaspresup.Value = 0
        CHKgru_fe_ini_plan.Value = 0
        CHKgru_fe_fin_plan.Value = 0
        CHKgru_fe_ini_real.Value = 0
        CHKgru_fe_fin_real.Value = 0
        CHKgru_fe_ini_orig.Value = 0
        CHKgru_fe_fin_orig.Value = 0
        CHKgru_info_adic.Value = 0
        CHKgru_recursos.Value = 0
        chkHorasRealesGrupo.Value = 0   ' add -003- a.
        '{ add -006- a.
        chkFechaProduccion.Value = 0
        chkFechaSuspension.Value = 0
        chkMotivoSuspension.Value = 0
        chkObservEstado.Value = 0
        '}
    End If
End Sub
'}

'{ add -001- d.
Private Sub cmdSecMark_Click()
    If cmdSecMark.Caption = "Marcar todos" Then
        cmdSecMark.Caption = "Desmarcar todos"
        CHKsec_sector.Value = 1
        CHKsec_estado.Value = 1
        CHKsec_situacion.Value = 1
        CHKsec_horaspresup.Value = 1
        CHKsec_fe_ini_plan.Value = 1
        CHKsec_fe_fin_plan.Value = 1
        CHKsec_fe_ini_real.Value = 1
        CHKsec_fe_fin_real.Value = 1
        CHKsec_fe_ini_orig.Value = 1
        CHKsec_fe_fin_orig.Value = 1
        chkHorasRealesSector.Value = 1   ' add -003- a.
    Else
        cmdSecMark.Caption = "Marcar todos"
        CHKsec_sector.Value = 0
        CHKsec_estado.Value = 0
        CHKsec_situacion.Value = 0
        CHKsec_horaspresup.Value = 0
        CHKsec_fe_ini_plan.Value = 0
        CHKsec_fe_fin_plan.Value = 0
        CHKsec_fe_ini_real.Value = 0
        CHKsec_fe_fin_real.Value = 0
        CHKsec_fe_ini_orig.Value = 0
        CHKsec_fe_fin_orig.Value = 0
        chkHorasRealesSector.Value = 0   ' add -003- a.
    End If
End Sub
'}

'{ add -001- d.
Private Sub cmdStdMark_Click()
    If cmdStdMark.Caption = "Marcar todos" Then
        cmdStdMark.Caption = "Desmarcar todos"
        CHKpet_nroasignado.Value = 1
        CHKcod_tipo_peticion.Value = 1
        CHKtitulo.Value = 1
        CHKdetalle.Value = 1
        CHKcaracte.Value = 1
        CHKmotivos.Value = 1
        CHKbenefic.Value = 1
        CHKrelacio.Value = 1
        CHKobserva.Value = 1
        CHKprioridad.Value = 1
        CHKfe_ing_comi.Value = 1
        CHKpet_estado.Value = 1
        CHKpet_fe_estado.Value = 1
        CHKpet_situacion.Value = 1
        CHKpet_sector.Value = 1
        CHKbpar.Value = 1
        CHKsoli.Value = 1
        CHKrefe.Value = 1
        CHKsupe.Value = 1
        CHKauto.Value = 1
        CHKhoraspresup.Value = 1
        CHKfe_ini_plan.Value = 1
        CHKfe_fin_plan.Value = 1
        CHKfe_ini_real.Value = 1
        CHKfe_fin_real.Value = 1
        CHKfe_ini_orig.Value = 1
        CHKfe_fin_orig.Value = 1
        CHKrepla.Value = 1
        CHKimportancia_nom.Value = 1
        CHKcorp_local.Value = 1
        CHKnom_orientacion.Value = 1
        'CHKhistorial.Value = 1     ' del -011- a.
        '{ add -011- a.
        If InPerfil("SADM") Or InPerfil("ADMI") Or InPerfil("BPAR") Or SoyElAnalista() Then
            CHKhistorial.Value = 1
        Else
            CHKhistorial.Value = 0
        End If
        '}
        chkPetClass.Value = 1
        chkPetImpTech.Value = 1
        chkPetDocu.Value = 1
        CHKpcf.Value = 1
        CHKseguimN.Value = 1
        CHKseguimT.Value = 1
        '{ add -003- a.
        chkHorasReales.Value = 1
        chkHorasRealesOtros.Value = 1
        '}
        chkPetRegulatorio.Value = 1     ' add -008- a.
        chkPetConfHomo.Value = 1        ' add -009- a.
        chkPetProyectoIDM.Value = 1     ' add -010- a.
        'CHKPeticionesHistoricas.Value = 1   ' add -013- a.
    Else
        cmdStdMark.Caption = "Marcar todos"
        CHKpet_nroasignado.Value = 0
        CHKcod_tipo_peticion.Value = 0
        CHKtitulo.Value = 0
        CHKdetalle.Value = 0
        CHKcaracte.Value = 0
        CHKmotivos.Value = 0
        CHKbenefic.Value = 0
        CHKrelacio.Value = 0
        CHKobserva.Value = 0
        CHKprioridad.Value = 0
        CHKfe_ing_comi.Value = 0
        CHKpet_estado.Value = 0
        CHKpet_fe_estado.Value = 0
        CHKpet_situacion.Value = 0
        CHKpet_sector.Value = 0
        CHKbpar.Value = 0
        CHKsoli.Value = 0
        CHKrefe.Value = 0
        CHKsupe.Value = 0
        CHKauto.Value = 0
        CHKhoraspresup.Value = 0
        CHKfe_ini_plan.Value = 0
        CHKfe_fin_plan.Value = 0
        CHKfe_ini_real.Value = 0
        CHKfe_fin_real.Value = 0
        CHKfe_ini_orig.Value = 0
        CHKfe_fin_orig.Value = 0
        CHKrepla.Value = 0
        CHKimportancia_nom.Value = 0
        CHKcorp_local.Value = 0
        CHKnom_orientacion.Value = 0
        CHKhistorial.Value = 0
        chkPetClass.Value = 0
        chkPetImpTech.Value = 0
        chkPetDocu.Value = 0
        CHKpcf.Value = 0
        CHKseguimN.Value = 0
        CHKseguimT.Value = 0
        '{ add -003- a.
        chkHorasReales.Value = 0
        chkHorasRealesOtros.Value = 0
        '}
        chkPetRegulatorio.Value = 0     ' add -008- a.
        chkPetConfHomo.Value = 0        ' add -009- a.
        chkPetProyectoIDM.Value = 0     ' add -010- a.
        'CHKPeticionesHistoricas.Value = 0   ' add -013- a.
    End If
End Sub
'}

Private Sub InicializarPantalla()
    Dim i As Integer
    ' Opciones de exportaci�n
    With grdDatos
        i = i + 1
        If sp_GetExport_Peticion(Null, "PET") Then
            Do While Not aplRST.EOF
                .TextMatrix(i, grdEXP_SI) = IIf(GetSetting("GesPet", "Export01", ClearNull(aplRST.Fields!exp_codigo)) = "SI", "�", "")
                .TextMatrix(i, grdEXP_NO) = IIf(.TextMatrix(i, grdEXP_SI) = "�", "", "�")
                i = i + 1
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    If secNivel = "SECT" Then
        i = i + 1
        With grdDatos
            If sp_GetExport_Peticion(Null, "SEC") Then
                Do While Not aplRST.EOF
                    .TextMatrix(i, grdEXP_SI) = IIf(GetSetting("GesPet", "Export01", ClearNull(aplRST.Fields!exp_codigo)) = "SI", "�", "")
                    .TextMatrix(i, grdEXP_NO) = IIf(.TextMatrix(i, grdEXP_SI) = "�", "", "�")
                    i = i + 1
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        End With
    ElseIf secNivel = "GRUP" Then
        i = i + 1
        With grdDatos
            If sp_GetExport_Peticion(Null, "GRU") Then
                Do While Not aplRST.EOF
                    .TextMatrix(i, grdEXP_SI) = IIf(GetSetting("GesPet", "Export01", ClearNull(aplRST.Fields!exp_codigo)) = "SI", "�", "")
                    .TextMatrix(i, grdEXP_NO) = IIf(.TextMatrix(i, grdEXP_SI) = "�", "", "�")
                    i = i + 1
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        End With
    End If
    ' Otros
    txtFile.Text = GetSetting("GesPet", "Export01", "FileExport", glWRKDIR & "EXPORTACION.XLS")
End Sub

Private Sub CHKgantt_Click()
    If CHKgantt.Value = 1 Then
        cboGantt.Enabled = True
        If cboGantt.ListCount > 0 Then
            cboGantt.ListIndex = 0
        End If
    Else
        cboGantt.ListIndex = -1
        cboGantt.Enabled = False
    End If
End Sub

Private Sub cmdAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "EXPO"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    txtAgrup.Text = ClearNull(Left(glSelectAgrupStr, 50))
    DoEvents
End Sub

Private Sub cmdClrAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = ""
    txtAgrup.Text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdConfirmar_Click()
    If MsgBox("�Confirma la generaci�n de la exportaci�n?", vbQuestion + vbOKCancel, "Exportaci�n de la consulta") = vbOK Then
        Inicializar                 ' add -015- a.
        HabiltarPaneles (False)     ' add -014- a.
        bScreen = False
        Call Generar
        HabiltarPaneles (True)      ' add -014- a.
    End If
End Sub

Private Sub cmdScreen_Click()
    If MsgBox("�Confirma la generaci�n de la exportaci�n?", vbQuestion + vbOKCancel, "Exportaci�n de la consulta") = vbOK Then
        Inicializar                 ' add -015- a.
        HabiltarPaneles (False)     ' add -014- a.
        bScreen = True
        Call Generar
        HabiltarPaneles (True)      ' add -014- a.
    End If
End Sub

'{ add
Private Sub HabiltarPaneles(Modo As Boolean)
    frmPet.Enabled = Modo
    frmGru.Enabled = Modo
    frmSec.Enabled = Modo
    frmAgrup.Enabled = Modo
End Sub
'}

Private Sub Generar()
    'On Error GoTo Errores
    Dim cntField As Integer
    Dim sTexto As String
    Dim sTtaux As String
    Dim sTttmp As String
    Dim sPetic As String
    Dim auxPrjFilter As String
    Dim auxPlanIni As Date
    Dim auxPlanFin As Date
    Dim hAux As Integer
    Dim aHisto()
    Dim strMes As String
    Dim flg2003 As Boolean
    Dim sFileName As String
    'Dim lRetCode As Long
    Dim lSumaHoras As Long          ' add -003- a.
    Dim i As Integer
    
    If flgEnCarga = True Then Exit Sub
    cntField = 0
    cntRows = 0
   
    strMes = "EneFebMarAbrMayJunJulAgoSetOctNovDic"
    flg2003 = False
    
    sFileName = txtFile.Text
    If Dir(sFileName) <> "" Then
        If MsgBox("El archivo ya existe." & Chr(13) & "�Sobreescribir el archivo?", vbYesNo) = vbNo Then
            GoTo finx
        End If
        On Error Resume Next
        Kill sFileName
    End If
    If Dir(sFileName) <> "" Then
        MsgBox ("No puede sobreescribir el archivo, verifique que no est� en uso")
        GoTo finx
    End If
    Call Puntero(True)
    cntField = 4
    
    ' Se inicializan los encabezados (* NUEVO *)
    With grdDatos
        For i = 1 To .Rows - 1
            If .TextMatrix(i, grdEXP_GRUPO) = "PET" Then
                If .TextMatrix(i, grdEXP_SI) = "�" Then cntField = cntField + 1: .TextMatrix(i, grdEXP_TAG) = cntField
            End If
        Next i
        If flgHijos Then
            If secNivel = "GRUP" Then
                For i = 1 To .Rows - 1
                    If .TextMatrix(i, grdEXP_GRUPO) = "GRP" Then
                        If .TextMatrix(i, grdEXP_SI) = "�" Then cntField = cntField + 1: .TextMatrix(i, grdEXP_TAG) = cntField
                    End If
                Next i
            Else
                For i = 1 To .Rows - 1
                    If .TextMatrix(i, grdEXP_GRUPO) = "SEC" Then
                        If .TextMatrix(i, grdEXP_SI) = "�" Then cntField = cntField + 1: .TextMatrix(i, grdEXP_TAG) = cntField
                    End If
                Next i
            End If
        End If
    End With
    If Val(glSelectAgrup) > 0 Then cntField = cntField + 1: txtAgrup.Tag = cntField
    cntLast = cntField + 1
    
        
'    ' Se inicializan los encabezados
'    If CHKpet_nroasignado.Value = 1 Then cntField = cntField + 1: CHKpet_nroasignado.Tag = cntField
'    If CHKcod_tipo_peticion.Value = 1 Then cntField = cntField + 1: CHKcod_tipo_peticion.Tag = cntField
'    '{ add -001- a.
'    If chkPetClass.Value = 1 Then cntField = cntField + 1: chkPetClass.Tag = cntField
'    If chkPetImpTech.Value = 1 Then cntField = cntField + 1: chkPetImpTech.Tag = cntField
'    'If chkPetDocu.Value = 1 Then cntField = cntField + 1: chkPetDocu.Tag = cntField    ' del -002- a.
'    '}
'    If CHKtitulo.Value = 1 Then cntField = cntField + 1: CHKtitulo.Tag = cntField
'    If CHKdetalle.Value = 1 Then cntField = cntField + 1: CHKdetalle.Tag = cntField
'    If CHKcaracte.Value = 1 Then cntField = cntField + 1: CHKcaracte.Tag = cntField
'    If CHKmotivos.Value = 1 Then cntField = cntField + 1: CHKmotivos.Tag = cntField
'    If CHKbenefic.Value = 1 Then cntField = cntField + 1: CHKbenefic.Tag = cntField
'    If CHKrelacio.Value = 1 Then cntField = cntField + 1: CHKrelacio.Tag = cntField
'    If CHKobserva.Value = 1 Then cntField = cntField + 1: CHKobserva.Tag = cntField
'    If CHKprioridad.Value = 1 Then cntField = cntField + 1: CHKprioridad.Tag = cntField
'    If CHKfe_ing_comi.Value = 1 Then cntField = cntField + 1: CHKfe_ing_comi.Tag = cntField
'    If CHKpet_estado.Value = 1 Then cntField = cntField + 1: CHKpet_estado.Tag = cntField
'    If CHKpet_fe_estado.Value = 1 Then cntField = cntField + 1: CHKpet_fe_estado.Tag = cntField
'    If CHKpet_situacion.Value = 1 Then cntField = cntField + 1: CHKpet_situacion.Tag = cntField
'    If CHKpet_sector.Value = 1 Then cntField = cntField + 1: CHKpet_sector.Tag = cntField
'    If CHKbpar.Value = 1 Then cntField = cntField + 1: CHKbpar.Tag = cntField
'    If CHKsoli.Value = 1 Then cntField = cntField + 1: CHKsoli.Tag = cntField
'    If CHKrefe.Value = 1 Then cntField = cntField + 1: CHKrefe.Tag = cntField
'    If CHKsupe.Value = 1 Then cntField = cntField + 1: CHKsupe.Tag = cntField
'    If CHKauto.Value = 1 Then cntField = cntField + 1: CHKauto.Tag = cntField
'    If CHKhoraspresup.Value = 1 Then cntField = cntField + 1: CHKhoraspresup.Tag = cntField
'    '{ add -003- a.
'    If chkHorasReales.Value = 1 Then cntField = cntField + 2: chkHorasReales.Tag = cntField - 1
'    If chkHorasRealesOtros.Value = 1 Then cntField = cntField + 2: chkHorasRealesOtros.Tag = cntField - 1
'    '}
'    If CHKfe_ini_plan.Value = 1 Then cntField = cntField + 1: CHKfe_ini_plan.Tag = cntField
'    If CHKfe_fin_plan.Value = 1 Then cntField = cntField + 1: CHKfe_fin_plan.Tag = cntField
'    If CHKfe_ini_real.Value = 1 Then cntField = cntField + 1: CHKfe_ini_real.Tag = cntField
'    If CHKfe_fin_real.Value = 1 Then cntField = cntField + 1: CHKfe_fin_real.Tag = cntField
'    If CHKfe_ini_orig.Value = 1 Then cntField = cntField + 1: CHKfe_ini_orig.Tag = cntField
'    If CHKfe_fin_orig.Value = 1 Then cntField = cntField + 1: CHKfe_fin_orig.Tag = cntField
'    If CHKrepla.Value = 1 Then cntField = cntField + 1: CHKrepla.Tag = cntField
'    If CHKcorp_local.Value = 1 Then cntField = cntField + 1: CHKcorp_local.Tag = cntField
'    If CHKimportancia_nom.Value = 1 Then cntField = cntField + 1: CHKimportancia_nom.Tag = cntField
'    If CHKnom_orientacion.Value = 1 Then cntField = cntField + 1: CHKnom_orientacion.Tag = cntField
'    If CHKhistorial.Value = 1 Then cntField = cntField + 1: CHKhistorial.Tag = cntField
'    If CHKpcf.Value = 1 Then cntField = cntField + 1: CHKpcf.Tag = cntField
'    If CHKseguimN.Value = 1 Then cntField = cntField + 1: CHKseguimN.Tag = cntField
'    If CHKseguimT.Value = 1 Then cntField = cntField + 1: CHKseguimT.Tag = cntField
'    If chkPetDocu.Value = 1 Then cntField = cntField + 11: chkPetDocu.Tag = cntField - 10   ' add -002- a.    ' upd -004- a. Se adecua para mostrar 9 columnas seg�n el documento de alcance  ' upd -009- a. Se agrega una columna m�s para identificar documentos de homologaci�n
'    If chkPetRegulatorio.Value = 1 Then cntField = cntField + 1: chkPetRegulatorio.Tag = cntField       ' add -008- a.
'    If chkPetConfHomo.Value = 1 Then cntField = cntField + 4: chkPetConfHomo.Tag = cntField - 3         ' add -009- a.
'    If chkPetProyectoIDM.Value = 1 Then cntField = cntField + 2: chkPetProyectoIDM.Tag = cntField - 1   ' add -010- a.
'    If flgHijos Then
'        If secNivel = "GRUP" Then
'            ' Abierto por grupos
'            If CHKgru_sector.Value = 1 Then cntField = cntField + 1: CHKgru_sector.Tag = cntField
'            If CHKprio_ejecuc.Value = 1 Then cntField = cntField + 1: CHKprio_ejecuc.Tag = cntField
'            If CHKgru_estado.Value = 1 Then cntField = cntField + 1: CHKgru_estado.Tag = cntField
'            If CHKgru_situacion.Value = 1 Then cntField = cntField + 1: CHKgru_situacion.Tag = cntField
'            If CHKgru_horaspresup.Value = 1 Then cntField = cntField + 1: CHKgru_horaspresup.Tag = cntField
'            If chkHorasRealesGrupo.Value = 1 Then cntField = cntField + 2: chkHorasRealesGrupo.Tag = cntField - 1   ' add -003- a.
'            If CHKgru_fe_ini_plan.Value = 1 Then cntField = cntField + 1: CHKgru_fe_ini_plan.Tag = cntField
'            If CHKgru_fe_fin_plan.Value = 1 Then cntField = cntField + 1: CHKgru_fe_fin_plan.Tag = cntField
'            If CHKgru_fe_ini_real.Value = 1 Then cntField = cntField + 1: CHKgru_fe_ini_real.Tag = cntField
'            If CHKgru_fe_fin_real.Value = 1 Then cntField = cntField + 1: CHKgru_fe_fin_real.Tag = cntField
'            If CHKgru_fe_ini_orig.Value = 1 Then cntField = cntField + 1: CHKgru_fe_ini_orig.Tag = cntField
'            If CHKgru_fe_fin_orig.Value = 1 Then cntField = cntField + 1: CHKgru_fe_fin_orig.Tag = cntField
'            If CHKgru_info_adic.Value = 1 Then cntField = cntField + 1: CHKgru_info_adic.Tag = cntField
'            '{ add -006- a.
'            If chkFechaProduccion.Value = 1 Then cntField = cntField + 1: chkFechaProduccion.Tag = cntField
'            If chkFechaSuspension.Value = 1 Then cntField = cntField + 1: chkFechaSuspension.Tag = cntField
'            If chkMotivoSuspension.Value = 1 Then cntField = cntField + 1: chkMotivoSuspension.Tag = cntField
'            If chkObservEstado.Value = 1 Then cntField = cntField + 1: chkObservEstado.Tag = cntField
'            '}
'        Else
'            ' Abierto por sectores
'            If CHKsec_sector.Value = 1 Then cntField = cntField + 1: CHKsec_sector.Tag = cntField
'            If CHKsec_estado.Value = 1 Then cntField = cntField + 1: CHKsec_estado.Tag = cntField
'            If CHKsec_situacion.Value = 1 Then cntField = cntField + 1: CHKsec_situacion.Tag = cntField
'            If CHKsec_horaspresup.Value = 1 Then cntField = cntField + 1: CHKsec_horaspresup.Tag = cntField
'            If chkHorasRealesSector.Value = 1 Then cntField = cntField + 2: chkHorasRealesSector.Tag = cntField - 1 ' add -003- a.
'            If CHKsec_fe_ini_plan.Value = 1 Then cntField = cntField + 1: CHKsec_fe_ini_plan.Tag = cntField
'            If CHKsec_fe_fin_plan.Value = 1 Then cntField = cntField + 1: CHKsec_fe_fin_plan.Tag = cntField
'            If CHKsec_fe_ini_real.Value = 1 Then cntField = cntField + 1: CHKsec_fe_ini_real.Tag = cntField
'            If CHKsec_fe_fin_real.Value = 1 Then cntField = cntField + 1: CHKsec_fe_fin_real.Tag = cntField
'            If CHKsec_fe_ini_orig.Value = 1 Then cntField = cntField + 1: CHKsec_fe_ini_orig.Tag = cntField
'            If CHKsec_fe_fin_orig.Value = 1 Then cntField = cntField + 1: CHKsec_fe_fin_orig.Tag = cntField
'        End If
'    End If
'    If Val(glSelectAgrup) > 0 Then cntField = cntField + 1: txtAgrup.Tag = cntField
'
'    cntLast = cntField + 1
    
    Call Status("Generando planilla...")
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.Visible = False
    On Error Resume Next
    Set ExcelWrk = ExcelApp.Workbooks.Add
    If ExcelWrk Is Nothing Then
        MsgBox "Error al generar planilla.", vbCritical + vbOKOnly, "Generando exportaci�n"
        GoTo finx
    End If
    
    ExcelWrk.SaveAs FileName:=sFileName
    
    Set xlSheet = ExcelWrk.Sheets(1)
    ExcelApp.ActiveWindow.Zoom = 75
    xlSheet.Cells.VerticalAlignment = -4160
    xlSheet.Rows(1).Font.Bold = True
 
    With xlSheet.Rows(1)
        .HorizontalAlignment = 4131
        .WrapText = True
        .Orientation = 0
        .IndentLevel = 0
        .ShrinkToFit = False
        .MergeCells = False
        .RowHeight = 30
    End With
    
    With grdDatos
        For i = 1 To .Rows - 1
            If .TextMatrix(i, grdEXP_SI) = "�" Then
                xlSheet.Cells(1, Val(.TextMatrix(i, grdEXP_TAG))) = .TextMatrix(i, grdEXP_DESC)
                xlSheet.Columns(Val(.TextMatrix(i, grdEXP_TAG))).HorizontalAlignment = .TextMatrix(i, grdEXP_ALIGNMENT)
                xlSheet.Columns(Val(.TextMatrix(i, grdEXP_TAG))).ColumnWidth = .TextMatrix(i, grdEXP_WIDTH)
                xlSheet.Columns(Val(.TextMatrix(i, grdEXP_TAG))).WrapText = .TextMatrix(i, grdEXP_WRAPTEXT)
                xlSheet.Columns(Val(.TextMatrix(i, grdEXP_TAG))).Orientation = .TextMatrix(i, grdEXP_ORIENTATION)
                xlSheet.Columns(Val(.TextMatrix(i, grdEXP_TAG))).IndentLevel = .TextMatrix(i, grdEXP_INDENTLEVEL)
                xlSheet.Columns(Val(.TextMatrix(i, grdEXP_TAG))).ShrinkToFit = .TextMatrix(i, grdEXP_SHRINKTOFIT)
                xlSheet.Columns(Val(.TextMatrix(i, grdEXP_TAG))).MergeCells = .TextMatrix(i, grdEXP_MERGECELLS)
            End If
        Next i
        'End With
    'End With
    
    
'    'encabezados
'    With xlSheet
'        .Columns(colNroInterno).ColumnWidth = 3
'        If CHKpet_nroasignado.Value = 1 Then .Cells(1, Val(CHKpet_nroasignado.Tag)) = CHKpet_nroasignado.Caption: .Columns(Val(CHKpet_nroasignado.Tag)).HorizontalAlignment = XLright:    .Columns(Val(CHKpet_nroasignado.Tag)).ColumnWidth = 9
'        If CHKcod_tipo_peticion.Value = 1 Then .Cells(1, Val(CHKcod_tipo_peticion.Tag)) = CHKcod_tipo_peticion.Caption: .Columns(Val(CHKcod_tipo_peticion.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKcod_tipo_peticion.Tag)).ColumnWidth = 5
'        If chkPetClass.Value = 1 Then .Cells(1, Val(chkPetClass.Tag)) = chkPetClass.Caption: .Columns(Val(chkPetClass.Tag)).HorizontalAlignment = XLright:    .Columns(Val(chkPetClass.Tag)).ColumnWidth = 9
'        If chkPetImpTech.Value = 1 Then .Cells(1, Val(chkPetImpTech.Tag)) = chkPetImpTech.Caption: .Columns(Val(chkPetImpTech.Tag)).HorizontalAlignment = XLleft: .Columns(Val(chkPetImpTech.Tag)).ColumnWidth = 15
'        If CHKtitulo.Value = 1 Then .Cells(1, Val(CHKtitulo.Tag)) = CHKtitulo.Caption: .Columns(Val(CHKtitulo.Tag)).HorizontalAlignment = XLleft: .Columns(Val(CHKtitulo.Tag)).ColumnWidth = 40
'        If CHKdetalle.Value = 1 Then
'            .Cells(1, Val(CHKdetalle.Tag)) = CHKdetalle.Caption
'            With xlSheet.Columns(Val(CHKdetalle.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 40
'            End With
'        End If
'        If CHKcaracte.Value = 1 Then
'            .Cells(1, Val(CHKcaracte.Tag)) = CHKcaracte.Caption
'            With xlSheet.Columns(Val(CHKcaracte.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 40
'            End With
'        End If
'        If CHKmotivos.Value = 1 Then
'            .Cells(1, Val(CHKmotivos.Tag)) = CHKmotivos.Caption
'            With xlSheet.Columns(Val(CHKmotivos.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 40
'            End With
'        End If
'        If CHKbenefic.Value = 1 Then
'            .Cells(1, Val(CHKbenefic.Tag)) = CHKbenefic.Caption
'            With xlSheet.Columns(Val(CHKbenefic.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 40
'            End With
'        End If
'        If CHKrelacio.Value = 1 Then
'            .Cells(1, Val(CHKrelacio.Tag)) = CHKrelacio.Caption
'            With xlSheet.Columns(Val(CHKrelacio.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 40
'            End With
'        End If
'        If CHKobserva.Value = 1 Then
'            .Cells(1, Val(CHKobserva.Tag)) = CHKobserva.Caption
'            With xlSheet.Columns(Val(CHKobserva.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 40
'            End With
'        End If
'        If CHKhistorial.Value = 1 Then
'            .Cells(1, Val(CHKhistorial.Tag)) = CHKhistorial.Caption
'            With xlSheet.Columns(Val(CHKhistorial.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 50
'            End With
'        End If
'        If CHKpcf.Value = 1 Then
'            .Cells(1, Val(CHKpcf.Tag)) = CHKpcf.Caption
'            With xlSheet.Columns(Val(CHKpcf.Tag))
'                .HorizontalAlignment = 4131
'                .WrapText = True
'                .Orientation = 0
'                .IndentLevel = 0
'                .ShrinkToFit = False
'                .MergeCells = False
'                .ColumnWidth = 40
'            End With
'        End If
'        If CHKprioridad.Value = 1 Then .Cells(1, Val(CHKprioridad.Tag)) = CHKprioridad.Caption: .Columns(Val(CHKprioridad.Tag)).HorizontalAlignment = XLcenter
'        If CHKfe_ing_comi.Value = 1 Then .Cells(1, Val(CHKfe_ing_comi.Tag)) = CHKfe_ing_comi.Caption: .Columns(Val(CHKfe_ing_comi.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKfe_ing_comi.Tag)).ColumnWidth = 15
'        If CHKpet_estado.Value = 1 Then .Cells(1, Val(CHKpet_estado.Tag)) = CHKpet_estado.Caption: .Columns(Val(CHKpet_estado.Tag)).ColumnWidth = 23
'        If CHKpet_fe_estado.Value = 1 Then .Cells(1, Val(CHKpet_fe_estado.Tag)) = CHKpet_fe_estado.Caption: .Columns(Val(CHKpet_fe_estado.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKpet_fe_estado.Tag)).ColumnWidth = 15
'        If CHKpet_situacion.Value = 1 Then .Cells(1, Val(CHKpet_situacion.Tag)) = CHKpet_situacion.Caption
'        If CHKpet_sector.Value = 1 Then .Cells(1, Val(CHKpet_sector.Tag)) = CHKpet_sector.Caption: .Columns(Val(CHKpet_sector.Tag)).ColumnWidth = 60
'        If CHKbpar.Value = 1 Then .Cells(1, Val(CHKbpar.Tag)) = CHKbpar.Caption: .Columns(Val(CHKbpar.Tag)).ColumnWidth = 30
'        If CHKsoli.Value = 1 Then .Cells(1, Val(CHKsoli.Tag)) = CHKsoli.Caption: .Columns(Val(CHKsoli.Tag)).ColumnWidth = 30
'        If CHKrefe.Value = 1 Then .Cells(1, Val(CHKrefe.Tag)) = CHKrefe.Caption: .Columns(Val(CHKrefe.Tag)).ColumnWidth = 30
'        If CHKsupe.Value = 1 Then .Cells(1, Val(CHKsupe.Tag)) = CHKsupe.Caption: .Columns(Val(CHKsupe.Tag)).ColumnWidth = 30
'        If CHKauto.Value = 1 Then .Cells(1, Val(CHKauto.Tag)) = CHKauto.Caption: .Columns(Val(CHKauto.Tag)).ColumnWidth = 30
'        If CHKhoraspresup.Value = 1 Then .Cells(1, Val(CHKhoraspresup.Tag)) = CHKhoraspresup.Caption
'        '{ add -003- a. - Coloca los t�tulos en la primera l�nea
'        If chkHorasReales.Value = 1 Then
'            .Cells(1, Val(chkHorasReales.Tag)) = "Hs. x Responsables": xlSheet.Columns(Val(chkHorasReales.Tag)).NumberFormat = "###,###,##0.00"
'            .Cells(1, Val(chkHorasReales.Tag) + 1) = "Hs. x Recursos": xlSheet.Columns(Val(chkHorasReales.Tag) + 1).NumberFormat = "###,###,##0.00"
'            If chkHorasRealesOtros.Value = 1 Then
'                .Cells(1, Val(chkHorasRealesOtros.Tag)) = "Otras Hs. Responsables ": xlSheet.Columns(Val(chkHorasRealesOtros.Tag)).NumberFormat = "###,###,##0.00"
'                .Cells(1, Val(chkHorasRealesOtros.Tag) + 1) = "Otras Hs. Recursos": xlSheet.Columns(Val(chkHorasRealesOtros.Tag) + 1).NumberFormat = "###,###,##0.00"
'            End If
'        End If
'        '}
'        If CHKfe_ini_plan.Value = 1 Then .Cells(1, Val(CHKfe_ini_plan.Tag)) = CHKfe_ini_plan.Caption: .Columns(Val(CHKfe_ini_plan.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKfe_ini_plan.Tag)).ColumnWidth = 15
'        If CHKfe_fin_plan.Value = 1 Then .Cells(1, Val(CHKfe_fin_plan.Tag)) = CHKfe_fin_plan.Caption: .Columns(Val(CHKfe_fin_plan.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKfe_fin_plan.Tag)).ColumnWidth = 15
'        If CHKfe_ini_real.Value = 1 Then .Cells(1, Val(CHKfe_ini_real.Tag)) = CHKfe_ini_real.Caption: .Columns(Val(CHKfe_ini_real.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKfe_ini_real.Tag)).ColumnWidth = 15
'        If CHKfe_fin_real.Value = 1 Then .Cells(1, Val(CHKfe_fin_real.Tag)) = CHKfe_fin_real.Caption: .Columns(Val(CHKfe_fin_real.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKfe_fin_real.Tag)).ColumnWidth = 15
'        If CHKfe_ini_orig.Value = 1 Then .Cells(1, Val(CHKfe_ini_orig.Tag)) = CHKfe_ini_orig.Caption: .Columns(Val(CHKfe_ini_orig.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKfe_ini_orig.Tag)).ColumnWidth = 15
'        If CHKfe_fin_orig.Value = 1 Then .Cells(1, Val(CHKfe_fin_orig.Tag)) = CHKfe_fin_orig.Caption: .Columns(Val(CHKfe_fin_orig.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKfe_fin_orig.Tag)).ColumnWidth = 15
'        If CHKrepla.Value = 1 Then .Cells(1, Val(CHKrepla.Tag)) = CHKrepla.Caption: .Columns(Val(CHKrepla.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKrepla.Tag)).ColumnWidth = 15
'        If CHKimportancia_nom.Value = 1 Then .Cells(1, Val(CHKimportancia_nom.Tag)) = CHKimportancia_nom.Caption: .Columns(Val(CHKimportancia_nom.Tag)).ColumnWidth = 15
'        If CHKcorp_local.Value = 1 Then .Cells(1, Val(CHKcorp_local.Tag)) = CHKcorp_local.Caption: .Columns(Val(CHKcorp_local.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKcorp_local.Tag)).ColumnWidth = 4
'        If CHKnom_orientacion.Value = 1 Then .Cells(1, Val(CHKnom_orientacion.Tag)) = CHKnom_orientacion.Caption: .Columns(Val(CHKnom_orientacion.Tag)).ColumnWidth = 15
'        If CHKseguimN.Value = 1 Then .Cells(1, Val(CHKseguimN.Tag)) = CHKseguimN.Caption: .Columns(Val(CHKseguimN.Tag)).HorizontalAlignment = XLleft: .Columns(Val(CHKseguimN.Tag)).ColumnWidth = 9
'        If CHKseguimT.Value = 1 Then .Cells(1, Val(CHKseguimT.Tag)) = CHKseguimT.Caption: .Columns(Val(CHKseguimT.Tag)).HorizontalAlignment = XLleft: .Columns(Val(CHKseguimT.Tag)).ColumnWidth = 40
'        If flgHijos Then
'            If secNivel = "GRUP" Then
'                If CHKgru_sector.Value = 1 Then .Cells(1, Val(CHKgru_sector.Tag)) = CHKgru_sector.Caption: .Columns(Val(CHKgru_sector.Tag)).ColumnWidth = 25
'                If CHKgru_estado.Value = 1 Then .Cells(1, Val(CHKgru_estado.Tag)) = CHKgru_estado.Caption: .Columns(Val(CHKgru_estado.Tag)).ColumnWidth = 23
'                If CHKgru_situacion.Value = 1 Then .Cells(1, Val(CHKgru_situacion.Tag)) = CHKgru_situacion.Caption
'                If CHKgru_horaspresup.Value = 1 Then .Cells(1, Val(CHKgru_horaspresup.Tag)) = CHKgru_horaspresup.Caption
'                If chkHorasRealesGrupo.Value = 1 Then
'                    .Cells(1, Val(chkHorasRealesGrupo.Tag)) = "Hs. x Resp. Grupo": xlSheet.Columns(Val(chkHorasRealesGrupo.Tag)).NumberFormat = "###,###,##0.00"
'                    .Cells(1, Val(chkHorasRealesGrupo.Tag) + 1) = "Hs. x Rec. Grupo": xlSheet.Columns(Val(chkHorasRealesGrupo.Tag) + 1).NumberFormat = "###,###,##0.00"
'                End If
'                If CHKgru_fe_ini_plan.Value = 1 Then .Cells(1, Val(CHKgru_fe_ini_plan.Tag)) = CHKgru_fe_ini_plan.Caption: .Columns(Val(CHKgru_fe_ini_plan.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKgru_fe_ini_plan.Tag)).ColumnWidth = 15
'                If CHKgru_fe_fin_plan.Value = 1 Then .Cells(1, Val(CHKgru_fe_fin_plan.Tag)) = CHKgru_fe_fin_plan.Caption: .Columns(Val(CHKgru_fe_fin_plan.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKgru_fe_fin_plan.Tag)).ColumnWidth = 15
'                If CHKgru_fe_ini_real.Value = 1 Then .Cells(1, Val(CHKgru_fe_ini_real.Tag)) = CHKgru_fe_ini_real.Caption: .Columns(Val(CHKgru_fe_ini_real.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKgru_fe_ini_real.Tag)).ColumnWidth = 15
'                If CHKgru_fe_fin_real.Value = 1 Then .Cells(1, Val(CHKgru_fe_fin_real.Tag)) = CHKgru_fe_fin_real.Caption: .Columns(Val(CHKgru_fe_fin_real.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKgru_fe_fin_real.Tag)).ColumnWidth = 15
'                If CHKgru_fe_ini_orig.Value = 1 Then .Cells(1, Val(CHKgru_fe_ini_orig.Tag)) = CHKgru_fe_ini_orig.Caption: .Columns(Val(CHKgru_fe_ini_orig.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKgru_fe_ini_orig.Tag)).ColumnWidth = 15
'                If CHKgru_fe_fin_orig.Value = 1 Then .Cells(1, Val(CHKgru_fe_fin_orig.Tag)) = CHKgru_fe_fin_orig.Caption: .Columns(Val(CHKgru_fe_fin_orig.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKgru_fe_fin_orig.Tag)).ColumnWidth = 15
'                If (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
'                    If CHKprio_ejecuc.Value = 1 Then .Cells(1, Val(CHKprio_ejecuc.Tag)) = CHKprio_ejecuc.Caption: .Columns(Val(CHKprio_ejecuc.Tag)).HorizontalAlignment = XLcenter
'                    If CHKgru_info_adic.Value = 1 Then
'                        .Cells(1, Val(CHKgru_info_adic.Tag)) = CHKgru_info_adic.Caption
'                        .Columns(Val(CHKgru_info_adic.Tag)).ColumnWidth = 50
'                        With xlSheet.Columns(Val(CHKgru_info_adic.Tag))
'                            .HorizontalAlignment = 4131
'                            .WrapText = True
'                            .Orientation = 0
'                            .IndentLevel = 0
'                            .ShrinkToFit = False
'                            .MergeCells = False
'                        End With
'                    End If
'                End If
'                If chkFechaProduccion.Value = 1 Then .Cells(1, Val(chkFechaProduccion.Tag)) = chkFechaProduccion.Caption: .Columns(Val(chkFechaProduccion.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(chkFechaProduccion.Tag)).ColumnWidth = 15
'                If chkFechaSuspension.Value = 1 Then .Cells(1, Val(chkFechaSuspension.Tag)) = chkFechaSuspension.Caption: .Columns(Val(chkFechaSuspension.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(chkFechaSuspension.Tag)).ColumnWidth = 15
'                If chkMotivoSuspension.Value = 1 Then .Cells(1, Val(chkMotivoSuspension.Tag)) = chkMotivoSuspension.Caption: .Columns(Val(chkMotivoSuspension.Tag)).HorizontalAlignment = XLleft: .Columns(Val(chkMotivoSuspension.Tag)).ColumnWidth = 30
'                If chkObservEstado.Value = 1 Then .Cells(1, Val(chkObservEstado.Tag)) = chkObservEstado.Caption: .Columns(Val(chkObservEstado.Tag)).HorizontalAlignment = XLleft: .Columns(Val(chkObservEstado.Tag)).ColumnWidth = 40
'            Else
'                If CHKsec_sector.Value = 1 Then .Cells(1, Val(CHKsec_sector.Tag)) = CHKsec_sector.Caption: .Columns(Val(CHKsec_sector.Tag)).ColumnWidth = 25
'                If CHKsec_estado.Value = 1 Then .Cells(1, Val(CHKsec_estado.Tag)) = CHKsec_estado.Caption: .Columns(Val(CHKsec_estado.Tag)).ColumnWidth = 23
'                If CHKsec_situacion.Value = 1 Then .Cells(1, Val(CHKsec_situacion.Tag)) = CHKsec_situacion.Caption
'                If CHKsec_horaspresup.Value = 1 Then .Cells(1, Val(CHKsec_horaspresup.Tag)) = CHKsec_horaspresup.Caption
'                If chkHorasRealesSector.Value = 1 Then
'                    .Cells(1, Val(chkHorasRealesSector.Tag)) = "Hs. x Resp. Sector": xlSheet.Columns(Val(chkHorasRealesSector.Tag)).NumberFormat = "###,###,##0.00"
'                    .Cells(1, Val(chkHorasRealesSector.Tag) + 1) = "Hs. x Rec. Sector": xlSheet.Columns(Val(chkHorasRealesSector.Tag) + 1).NumberFormat = "###,###,##0.00"
'                End If
'                If CHKsec_fe_ini_plan.Value = 1 Then .Cells(1, Val(CHKsec_fe_ini_plan.Tag)) = CHKsec_fe_ini_plan.Caption: .Columns(Val(CHKsec_fe_ini_plan.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKsec_fe_ini_plan.Tag)).ColumnWidth = 15
'                If CHKsec_fe_fin_plan.Value = 1 Then .Cells(1, Val(CHKsec_fe_fin_plan.Tag)) = CHKsec_fe_fin_plan.Caption: .Columns(Val(CHKsec_fe_fin_plan.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKsec_fe_fin_plan.Tag)).ColumnWidth = 15
'                If CHKsec_fe_ini_real.Value = 1 Then .Cells(1, Val(CHKsec_fe_ini_real.Tag)) = CHKsec_fe_ini_real.Caption: .Columns(Val(CHKsec_fe_ini_real.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKsec_fe_ini_real.Tag)).ColumnWidth = 15
'                If CHKsec_fe_fin_real.Value = 1 Then .Cells(1, Val(CHKsec_fe_fin_real.Tag)) = CHKsec_fe_fin_real.Caption: .Columns(Val(CHKsec_fe_fin_real.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKsec_fe_fin_real.Tag)).ColumnWidth = 15
'                If CHKsec_fe_ini_orig.Value = 1 Then .Cells(1, Val(CHKsec_fe_ini_orig.Tag)) = CHKsec_fe_ini_orig.Caption: .Columns(Val(CHKsec_fe_ini_orig.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKsec_fe_ini_orig.Tag)).ColumnWidth = 15
'                If CHKsec_fe_fin_orig.Value = 1 Then .Cells(1, Val(CHKsec_fe_fin_orig.Tag)) = CHKsec_fe_fin_orig.Caption: .Columns(Val(CHKsec_fe_fin_orig.Tag)).HorizontalAlignment = XLcenter: .Columns(Val(CHKsec_fe_fin_orig.Tag)).ColumnWidth = 15
'            End If
'        End If
'        If chkPetDocu.Value = 1 Then
'            .Cells(1, Val(chkPetDocu.Tag)) = "Doc. Adjuntos": .Columns(Val(chkPetDocu.Tag)).HorizontalAlignment = XLleft: .Columns(Val(chkPetDocu.Tag)).ColumnWidth = 20
'            .Cells(1, Val(chkPetDocu.Tag + 1)) = "C100": .Columns(Val(chkPetDocu.Tag + 1)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 1)).ColumnWidth = 7
'            .Cells(1, Val(chkPetDocu.Tag + 2)) = "C102": .Columns(Val(chkPetDocu.Tag + 2)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 2)).ColumnWidth = 7
'            .Cells(1, Val(chkPetDocu.Tag + 3)) = "P950": .Columns(Val(chkPetDocu.Tag + 3)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 3)).ColumnWidth = 7
'            .Cells(1, Val(chkPetDocu.Tag + 4)) = "C204": .Columns(Val(chkPetDocu.Tag + 4)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 4)).ColumnWidth = 7
'            .Cells(1, Val(chkPetDocu.Tag + 5)) = "CG04": .Columns(Val(chkPetDocu.Tag + 5)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 5)).ColumnWidth = 7
'            .Cells(1, Val(chkPetDocu.Tag + 6)) = "T710": .Columns(Val(chkPetDocu.Tag + 6)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 6)).ColumnWidth = 7    ' add -007- a.
'            .Cells(1, Val(chkPetDocu.Tag + 7)) = "EML1": .Columns(Val(chkPetDocu.Tag + 7)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 7)).ColumnWidth = 7
'            .Cells(1, Val(chkPetDocu.Tag + 8)) = "EML2": .Columns(Val(chkPetDocu.Tag + 8)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 8)).ColumnWidth = 7
'            .Cells(1, Val(chkPetDocu.Tag + 9)) = "IDH": .Columns(Val(chkPetDocu.Tag + 9)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 9)).ColumnWidth = 7     ' add -009- a.
'            .Cells(1, Val(chkPetDocu.Tag + 10)) = "OTRO": .Columns(Val(chkPetDocu.Tag + 10)).HorizontalAlignment = XLcenter: .Columns(Val(chkPetDocu.Tag + 10)).ColumnWidth = 7 ' upd -009- a.
'        End If
'        If chkPetRegulatorio.Value = 1 Then .Cells(1, Val(chkPetRegulatorio.Tag)) = chkPetRegulatorio.Caption: .Columns(Val(chkPetRegulatorio.Tag)).HorizontalAlignment = XLleft: .Columns(Val(chkPetRegulatorio.Tag)).ColumnWidth = 7      ' add -008- a.
'        If chkPetConfHomo.Value = 1 Then
'            .Cells(1, Val(chkPetConfHomo.Tag)) = "Conf.HSOB": .Columns(Val(chkPetConfHomo.Tag)).HorizontalAlignment = XLleft: .Columns(Val(chkPetConfHomo.Tag)).ColumnWidth = 7
'            .Cells(1, Val(chkPetConfHomo.Tag + 1)) = "Conf.HOME": .Columns(Val(chkPetConfHomo.Tag) + 1).HorizontalAlignment = XLleft: .Columns(Val(chkPetConfHomo.Tag) + 1).ColumnWidth = 7
'            .Cells(1, Val(chkPetConfHomo.Tag + 2)) = "Conf.HOMA": .Columns(Val(chkPetConfHomo.Tag) + 2).HorizontalAlignment = XLleft: .Columns(Val(chkPetConfHomo.Tag) + 2).ColumnWidth = 7
'        End If
'        If chkPetProyectoIDM.Value = 1 Then
'            .Cells(1, Val(chkPetProyectoIDM.Tag)) = "Proyecto": .Columns(Val(chkPetProyectoIDM.Tag)).HorizontalAlignment = XLleft: .Columns(Val(chkPetProyectoIDM.Tag)).ColumnWidth = 7
'            .Cells(1, Val(chkPetProyectoIDM.Tag + 1)) = "Nombre del Proyecto": .Columns(Val(chkPetProyectoIDM.Tag) + 1).HorizontalAlignment = XLleft: .Columns(Val(chkPetProyectoIDM.Tag) + 1).ColumnWidth = 15
'        End If
        
        ' Esto es para la parte de Grupos...
        If Val(glSelectAgrup) > 0 Then
            xlSheet.Cells(1, Val(txtAgrup.Tag)) = txtAgrup.Text
            With xlSheet.Columns(Val(txtAgrup.Tag))
                .HorizontalAlignment = 4131
                .WrapText = True
                .Orientation = 0
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .ColumnWidth = 40
            End With
        End If
    End With
    
    'Call Puntero(True)
    
    iContador = 1
    
    ' Opci�n de recursos asignados
    If secNivel = "GRUP" And CHKgru_recursos.Value = 1 Then
        Call Status("Cargando recursos...")
        If sp_GetRecurso(secGrup, "SU", "A") Then
            Do While Not aplRST.EOF
                For j = cntField + 1 To cntLast
                    If xlSheet.Cells(1, j) = ClearNull(aplRST(1)) Then
                        xlSheet.Columns(j).HorizontalAlignment = XLcenter
                        Exit For
                    End If
                Next
                If j > cntLast Then
                    xlSheet.Cells(1, cntLast) = ClearNull(aplRST(1))
                    xlSheet.Columns(cntLast).HorizontalAlignment = XLcenter
                    xlSheet.Columns(cntLast).ColumnWidth = 2
                    xlSheet.Columns(cntLast).EntireColumn.AutoFit
                    cntLast = cntLast + 1
                End If
                aplRST.MoveNext
            Loop
            'aplRST.Close
        End If
    End If
    
    '{ add -013- a.
'    Call status("Realizando la consulta...")
'    'If CHKPeticionesHistoricas.Value = 1 Then
'    If frmPeticionesShellView01.chkHistorico = 1 Then
'        Hacer_Historico
'    End If

    ' Aqu� ejecuta la consulta (seg�n la opci�n solicitada) para armar la planilla Excel (peticiones online)
    With frmPeticionesShellView01
        If chkHorasReales.Value = 1 Then
            If Not sp_rptPeticion03(0, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), _
                                    CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), IIf(CodigoCombo(.cmbRegulatorio, True) = "NULL", Null, CodigoCombo(.cmbRegulatorio, True)), IIf(CodigoCombo(.cboEmp, True) = "NULL", Null, CodigoCombo(.cboEmp, True)), IIf(CodigoCombo(.cboRO, True) = "NULL", Null, CodigoCombo(.cboRO, True)), .txtPrjId, .txtPrjSubId, .txtPrjSubsId) Then   ' upd -001- b.
               GoTo finx
            End If
        Else
            If Not sp_rptPeticion01(0, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), _
                                    CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), IIf(CodigoCombo(.cmbRegulatorio, True) = "NULL", Null, CodigoCombo(.cmbRegulatorio, True)), IIf(CodigoCombo(.cboEmp, True) = "NULL", Null, CodigoCombo(.cboEmp, True)), IIf(CodigoCombo(.cboRO, True) = "NULL", Null, CodigoCombo(.cboRO, True)), .txtPrjId, .txtPrjSubId, .txtPrjSubsId) Then   ' upd -001- b.
               GoTo finx
            End If
        End If
    End With
    
    nPet_NroInterno_Actual = 0
    nPet_NroInterno_Anterior = 0
    Do While Not aplRST.EOF
        iContador = iContador + 1
        lSumaHoras = 0              ' add -003-
        Call Status("Peticiones exportadas: " & iContador)
        xlSheet.Cells(iContador, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
        With grdDatos
            For i = 1 To .Rows
                If .TextMatrix(i, grdEXP_SI) = "�" Then
                    xlSheet.Cells(iContador, .TextMatrix(i, grdEXP_TAG)) = ClearNull(aplRST.Fields.item(.TextMatrix(i, grdEXP_NOMCAMPO)))
                End If
            Next i
        End With
        If CHKfe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
        If CHKfe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
        'se guarda para grantt
        If CodigoCombo(cboGantt, True) = "PETI" Then
            xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
            xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
            xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!cod_estado)
        Else
            xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
            xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
            xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!sec_cod_estado)
        End If
        If CHKfe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_real.Tag)) = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "yyyy-mm-dd"), "")
        If CHKfe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_real.Tag)) = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "yyyy-mm-dd"), "")
        If CHKfe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!fe_ini_orig), Format(aplRST!fe_ini_orig, "yyyy-mm-dd"), "")
        If CHKfe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!fe_fin_orig), Format(aplRST!fe_fin_orig, "yyyy-mm-dd"), "")
        If CHKrepla.Value = 1 Then xlSheet.Cells(iContador, Val(CHKrepla.Tag)) = IIf(Val(ClearNull(aplRST!cant_planif)) < 2, "", Val(ClearNull(aplRST!cant_planif)) - 1)
        If CHKimportancia_nom.Value = 1 Then xlSheet.Cells(iContador, Val(CHKimportancia_nom.Tag)) = ClearNull(aplRST!importancia_nom)
        If CHKcorp_local.Value = 1 Then xlSheet.Cells(iContador, Val(CHKcorp_local.Tag)) = ClearNull(aplRST!corp_local)
        If CHKnom_orientacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKnom_orientacion.Tag)) = ClearNull(aplRST!nom_orientacion)
        If CHKseguimN.Value = 1 Then xlSheet.Cells(iContador, Val(CHKseguimN.Tag)) = ClearNull(aplRST!prj_nrointerno)
        If CHKseguimT.Value = 1 Then xlSheet.Cells(iContador, Val(CHKseguimT.Tag)) = ClearNull(aplRST!prj_titulo)
        If flgHijos Then
            If secNivel = "GRUP" Then
                If CHKgru_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_sector.Tag)) = ClearNull(aplRST!sec_nom_sector) & ">> " & ClearNull(aplRST!sec_nom_grupo)
                If (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                    If CHKprio_ejecuc.Value = 1 Then xlSheet.Cells(iContador, Val(CHKprio_ejecuc.Tag)) = ClearNull(aplRST!prio_ejecuc) & " "
                    If CHKgru_info_adic.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_info_adic.Tag)) = ClearNull(aplRST!info_adicio) & " "
                End If
                If CHKgru_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_estado.Tag)) = ClearNull(aplRST!sec_nom_estado)
                If CHKgru_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
                If CHKgru_horaspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
                If CHKgru_fe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
                If CHKgru_fe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
                If CHKgru_fe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
                If CHKgru_fe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
                If CHKgru_fe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
                If CHKgru_fe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
                If chkFechaProduccion.Value = 1 Then xlSheet.Cells(iContador, Val(chkFechaProduccion.Tag)) = IIf(Not IsNull(aplRST!fe_produccion), Format(aplRST!fe_produccion, "yyyy-mm-dd"), "")
                If chkFechaSuspension.Value = 1 Then xlSheet.Cells(iContador, Val(chkFechaSuspension.Tag)) = IIf(Not IsNull(aplRST!fe_suspension), Format(aplRST!fe_suspension, "yyyy-mm-dd"), "")
                If chkMotivoSuspension.Value = 1 Then xlSheet.Cells(iContador, Val(chkMotivoSuspension.Tag)) = ClearNull(aplRST!nom_motivo) & " "
                If chkObservEstado.Value = 1 Then xlSheet.Cells(iContador, Val(chkObservEstado.Tag)) = ClearNull(aplRST!ObsEstado) & " "
            Else
                If CHKsec_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_sector.Tag)) = ClearNull(aplRST!sec_nom_sector)
                If CHKsec_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_estado.Tag)) = ClearNull(aplRST!sec_nom_estado) & " "
                If CHKsec_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
                If CHKsec_horaspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
                If CHKsec_fe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
                If CHKsec_fe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
                If CHKsec_fe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
                If CHKsec_fe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
                If CHKsec_fe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
                If CHKsec_fe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
            End If
        End If
        If chkPetDocu.Value = 1 Then
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag)) = IIf(ClearNull(aplRST!pet_mod1None) = 0, "No hay adjuntos", "Tiene adjuntos") & " "
            ' Esto es para las peticiones del nuevo modelo (SOx)
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 1)) = IIf(Not IsNull(aplRST!pet_mod1C100) And aplRST!pet_mod1C100 > 0, "X", " ") & " "
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 2)) = IIf(Not IsNull(aplRST!pet_mod1C102) And aplRST!pet_mod1C102 > 0, "X", " ") & " "
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 3)) = IIf(Not IsNull(aplRST!pet_mod1P950) And aplRST!pet_mod1P950 > 0, "X", " ") & " "
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 4)) = IIf(Not IsNull(aplRST!pet_mod1C204) And aplRST!pet_mod1C204 > 0, "X", " ") & " "
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 5)) = IIf(Not IsNull(aplRST!pet_mod1CG04) And aplRST!pet_mod1CG04 > 0, "X", " ") & " "
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 6)) = IIf(Not IsNull(aplRST!pet_mod1T710) And aplRST!pet_mod1T710 > 0, "X", " ") & " "    ' add -007- a.
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 7)) = IIf(Not IsNull(aplRST!pet_mod1EML1) And aplRST!pet_mod1EML1 > 0, "X", " ") & " "
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 8)) = IIf(Not IsNull(aplRST!pet_mod1EML2) And aplRST!pet_mod1EML2 > 0, "X", " ") & " "
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 9)) = IIf(Not IsNull(aplRST!pet_mod1IDHX) And aplRST!pet_mod1IDHX > 0, "X", " ") & " "    ' add -009- a.
            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10)) = IIf(Not IsNull(aplRST!pet_mod1OTRO) And aplRST!pet_mod1OTRO > 0, "X", " ") & " "
            ' Para las peticiones del viejo modelo de control, se agregan a OTROs
            If aplRST!pet_mod1OTRO = 0 Then
                If Not Trim(xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10))) = "X" Then
                    xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10)) = IIf(Not IsNull(aplRST!pet_mod0TODO) And aplRST!pet_mod0TODO > 0, "X", " ") & " "
                End If
            End If
        End If
        If chkPetRegulatorio.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetRegulatorio.Tag)) = ClearNull(aplRST!pet_regulatorio)    ' add -009- a.
        If chkPetProyectoIDM.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag)) = ClearNull(aplRST!pet_projid) & "." & ClearNull(aplRST!pet_projsubid) & "." & ClearNull(aplRST!pet_projsubsid)
        If chkPetProyectoIDM.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag + 1)) = ClearNull(aplRST!projnom)
        aplRST.MoveNext
        DoEvents
    Loop
    'aplRST.Close
    cntRows = cntRows + iContador       ' upd -013- a. Se cambi� de "cntRows = iContador"
    Set rsAux = Nothing                 ' add -003- a.
    
    ' Aqu� agrega la parte de Solicitante, Referente, Autorizante y Supervisor
    If CHKsoli.Value = 1 Or CHKrefe.Value = 1 Or CHKsupe.Value = 1 Or CHKauto.Value = 1 Then
        For iContador = 2 To cntRows
            Call Status("Solicitantes:" & iContador)
            sPetic = xlSheet.Cells(iContador, colNroInterno)
            Call sp_GetUnaPeticionXt(sPetic)
            If Not aplRST.EOF Then
                If CHKsoli.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsoli.Tag)) = ClearNull(aplRST!nom_solicitante) & " "
                If CHKrefe.Value = 1 Then xlSheet.Cells(iContador, Val(CHKrefe.Tag)) = ClearNull(aplRST!nom_referente) & " "
                If CHKsupe.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsupe.Tag)) = ClearNull(aplRST!nom_supervisor) & " "
                If CHKauto.Value = 1 Then xlSheet.Cells(iContador, Val(CHKauto.Tag)) = ClearNull(aplRST!nom_director) & " "
            End If
            'aplRST.Close
        Next
    End If
    ' Detalles y dem�s textos...
    For iContador = 2 To cntRows
        Call Status("Detalles: " & Format(iContador, "###,###,##0"))
        sPetic = xlSheet.Cells(iContador, colNroInterno)
        If IsNumeric(sPetic) Then
            If Not sp_GetPeticionEsHistorica(sPetic) Then   ' add -013- a.
                If CHKdetalle.Value = 1 Then
                    sTtaux = sp_GetMemo(sPetic, "DESCRIPCIO")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKdetalle.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKdetalle.Tag)) = " "
                    End If
                End If
                If CHKcaracte.Value = 1 Then
                    sTtaux = sp_GetMemo(sPetic, "CARACTERIS")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKcaracte.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKcaracte.Tag)) = " "
                    End If
                End If
                If CHKmotivos.Value = 1 Then
                    sTtaux = sp_GetMemo(sPetic, "MOTIVOS")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKmotivos.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKmotivos.Tag)) = " "
                    End If
                End If
                If CHKbenefic.Value = 1 Then
                    sTtaux = sp_GetMemo(sPetic, "BENEFICIOS")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKbenefic.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKbenefic.Tag)) = " "
                    End If
                End If
                If CHKrelacio.Value = 1 Then
                    sTtaux = sp_GetMemo(sPetic, "RELACIONES")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKrelacio.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKrelacio.Tag)) = " "
                    End If
                End If
                If CHKobserva.Value = 1 Then
                    sTtaux = sp_GetMemo(sPetic, "OBSERVACIO")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKobserva.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKobserva.Tag)) = " "
                    End If
                End If
            Else
                If CHKdetalle.Value = 1 Then
                    sTtaux = sp_GetMemoH(sPetic, "DESCRIPCIO")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKdetalle.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKdetalle.Tag)) = " "
                    End If
                End If
                If CHKcaracte.Value = 1 Then
                    sTtaux = sp_GetMemoH(sPetic, "CARACTERIS")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKcaracte.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKcaracte.Tag)) = " "
                    End If
                End If
                If CHKmotivos.Value = 1 Then
                    sTtaux = sp_GetMemoH(sPetic, "MOTIVOS")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKmotivos.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKmotivos.Tag)) = " "
                    End If
                End If
                If CHKbenefic.Value = 1 Then
                    sTtaux = sp_GetMemoH(sPetic, "BENEFICIOS")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKbenefic.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKbenefic.Tag)) = " "
                    End If
                End If
                If CHKrelacio.Value = 1 Then
                    sTtaux = sp_GetMemoH(sPetic, "RELACIONES")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKrelacio.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKrelacio.Tag)) = " "
                    End If
                End If
                If CHKobserva.Value = 1 Then
                    sTtaux = sp_GetMemoH(sPetic, "OBSERVACIO")
                    sTtaux = ReplaceString(sTtaux, 13, "")
                    sTtaux = cutString(sTtaux, 10)
                    If Len(sTtaux) > 2 Then
                        sTexto = ReplaceString(sTtaux, 13, "")
                        sTexto = cutString(sTexto, 10)
                        xlSheet.Cells(iContador, Val(CHKobserva.Tag)) = sTexto
                    Else
                        xlSheet.Cells(iContador, Val(CHKobserva.Tag)) = " "
                    End If
                End If
            End If
        End If
    Next
    
    ' Agrupamientos...
    If Val(glSelectAgrup) > 0 Then
        For iContador = 2 To cntRows
            Call Status("Agrupamiento:" & iContador)
            sPetic = xlSheet.Cells(iContador, colNroInterno)
            sTexto = ""
            
            If sp_GetAgrupPeticHijo(glSelectAgrup, sPetic) Then
                Do While Not aplRST.EOF
                    sTexto = sTexto & ClearNull(aplRST!agr_titulo) & Chr(10)
                    aplRST.MoveNext
                Loop
                aplRST.Close
                sTexto = cutString(sTexto, 10)
                xlSheet.Cells(iContador, Val(txtAgrup.Tag)) = sTexto
            End If
        Next
    End If
    
    ' Detalles del historial
    '{ add -012- a.
    Dim cNombreEvento As String
    
    If CHKhistorial.Value = 1 Then
        For iContador = 2 To cntRows
            ReDim aHisto(1 To 1)
            hAux = 0
            sTexto = " "
            Call Status("Historial: " & iContador)
            If sp_GetPeticionEsHistorica(xlSheet.Cells(iContador, colNroInterno)) Then
                Call sp_GetHistorialXtH(xlSheet.Cells(iContador, colNroInterno), Null)
                Do While Not aplRST.EOF
                    hAux = hAux + 1
                    ReDim Preserve aHisto(1 To hAux)
                    aHisto(hAux) = aplRST!hst_nrointerno
                    aplRST.MoveNext
                    DoEvents
                Loop
                ' Por cada n�mero interno busca el texto correspondiente
                For j = 1 To hAux
                    If sp_GetHistorialXtH(xlSheet.Cells(iContador, colNroInterno), aHisto(j)) Then
                        Call Status("Historial: " & iContador & " (" & j & ")")
                        sTttmp = Format(aplRST!hst_fecha, "dd/mm/yyyy hh:mm") & ", " & ClearNull(aplRST!nom_audit) & Chr(10)
                        cNombreEvento = ClearNull(aplRST.Fields!nom_accion)
                        aplRST.Close
                        sTtaux = sp_GetHistorialMemoH(aHisto(j))
                        sTtaux = ReplaceString(sTtaux, 13, "")      ' Esto reemplaza los retornos de carro
                        ' Aqu� evito cargar en el texto del historial un registro que no tiene la descripci�n detallada del evento.
                        ' En su lugar, guardo el nombre del evento
                        If InStr(1, sTtaux, "--->", vbTextCompare) > 0 And Len(sTtaux) = 4 Then
                            sTtaux = cNombreEvento
                        End If
                        ' Aqu� evito cargar en el texto del historial un registro que no tiene datos
                        If Not (InStr(1, sTtaux, "<<>>", vbTextCompare) > 0) Or (InStr(1, sTtaux, "��", vbTextCompare) > 0) Then
                            sTexto = Trim(sTexto) & sTttmp & sTtaux & Chr(10) & Chr(10)
                        End If
                        DoEvents
                    End If
                    DoEvents
                Next
                xlSheet.Cells(iContador, Val(CHKhistorial.Tag)) = sTexto
            Else
            '}
                Call sp_GetHistorialXt(xlSheet.Cells(iContador, colNroInterno), Null)
                ' Guarda en un vector todos los n�meros internos del historial para esa petici�n
                Do While Not aplRST.EOF
                    hAux = hAux + 1
                    ReDim Preserve aHisto(1 To hAux)
                    aHisto(hAux) = aplRST!hst_nrointerno
                    aplRST.MoveNext
                    DoEvents
                Loop
                ' Por cada n�mero interno busca el texto correspondiente
                For j = 1 To hAux
                    If sp_GetHistorialXt(xlSheet.Cells(iContador, colNroInterno), aHisto(j)) Then
                        Call Status("Historial: " & iContador & " (" & j & ")")
                        sTttmp = Format(aplRST!hst_fecha, "dd/mm/yyyy hh:mm") & ", " & ClearNull(aplRST!nom_audit) & Chr(10)
                        cNombreEvento = ClearNull(aplRST.Fields!nom_accion)
                        aplRST.Close
                        sTtaux = sp_GetHistorialMemo(aHisto(j))
                        sTtaux = ReplaceString(sTtaux, 13, "")      ' Esto reemplaza los retornos de carro
                        ' Aqu� evito cargar en el texto del historial un registro que no tiene la descripci�n detallada del evento.
                        ' En su lugar, guardo el nombre del evento
                        If InStr(1, sTtaux, "--->", vbTextCompare) > 0 And Len(sTtaux) = 4 Then
                            sTtaux = cNombreEvento
                        End If
                        ' Aqu� evito cargar en el texto del historial un registro que no tiene datos
                        If Not (InStr(1, sTtaux, "<<>>", vbTextCompare) > 0) Or (InStr(1, sTtaux, "��", vbTextCompare) > 0) Then
                            sTexto = Trim(sTexto) & sTttmp & sTtaux & Chr(10) & Chr(10)
                        End If
                        DoEvents
                    End If
                    DoEvents
                Next
                xlSheet.Cells(iContador, Val(CHKhistorial.Tag)) = sTexto
            End If
        Next
    End If
    
    ' Conformes de usuario
    If CHKpcf.Value = 1 Then
        For iContador = 2 To cntRows
            sTexto = " "
            Call Status("Conformes:" & iContador)
            '{ add -013- a.
            If sp_GetPeticionEsHistorica(xlSheet.Cells(iContador, colNroInterno)) Then
                Call sp_GetPeticionConfH(xlSheet.Cells(iContador, colNroInterno), Null, Null)
                Do While Not aplRST.EOF
                    sTexto = ClearNull(sTexto) & ClearNull(oTipoConforme.GetNombreModo((aplRST!ok_modo))) & " - " & ClearNull(oTipoConforme.GetNombreTipo((aplRST!ok_tipo))) & " - " & ClearNull(aplRST!nom_audit) & Chr(10)
                    aplRST.MoveNext
                Loop
                xlSheet.Cells(iContador, Val(CHKpcf.Tag)) = sTexto
            Else
            '}
                Call sp_GetPeticionConf(xlSheet.Cells(iContador, colNroInterno), Null, Null)
                Do While Not aplRST.EOF
                    sTexto = ClearNull(sTexto) & ClearNull(oTipoConforme.GetNombreModo((aplRST!ok_modo))) & " - " & ClearNull(oTipoConforme.GetNombreTipo((aplRST!ok_tipo))) & " - " & ClearNull(aplRST!nom_audit) & Chr(10)
                    aplRST.MoveNext
                Loop
                xlSheet.Cells(iContador, Val(CHKpcf.Tag)) = sTexto
            End If      ' add -013- a.
        Next
    End If
    
    ' Conformes de homologaci�n
    If chkPetConfHomo.Value = 1 Then
        For iContador = 2 To cntRows
            Call Status("Conformes homologaci�n: " & iContador)
            If sp_GetPeticionEsHistorica(xlSheet.Cells(iContador, colNroInterno)) Then
                Call sp_GetPeticionConfHomoH(xlSheet.Cells(iContador, colNroInterno))
                Do While Not aplRST.EOF
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag)) = IIf(Not IsNull(aplRST!HSOB) And aplRST!HSOB > 0, "X", " ") & " "
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag + 1)) = IIf(Not IsNull(aplRST!HOME) And aplRST!HOME > 0, "X", " ") & " "
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag + 2)) = IIf(Not IsNull(aplRST!HOMA) And aplRST!HOMA > 0, "X", " ") & " "
                    aplRST.MoveNext
                    DoEvents
                Loop
            Else
                Call sp_GetPeticionConfHomo(xlSheet.Cells(iContador, colNroInterno))
                Do While Not aplRST.EOF
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag)) = IIf(Not IsNull(aplRST!HSOB) And aplRST!HSOB > 0, "X", " ") & " "
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag + 1)) = IIf(Not IsNull(aplRST!HOME) And aplRST!HOME > 0, "X", " ") & " "
                    xlSheet.Cells(iContador, Val(chkPetConfHomo.Tag + 2)) = IIf(Not IsNull(aplRST!HOMA) And aplRST!HOMA > 0, "X", " ") & " "
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next
    End If
    
    '{ add -010- a. Nombre extendido de Proyectos IDM
    If chkPetProyectoIDM.Value = 1 Then
        For iContador = 2 To cntRows
            Call Status("Proyectos IDM: " & iContador)
            If sp_GetPeticionEsHistorica(xlSheet.Cells(iContador, colNroInterno)) Then
                Call sp_GetPeticionProyectoNombreH(xlSheet.Cells(iContador, colNroInterno))
                Do While Not aplRST.EOF
                    xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag + 1)) = ClearNull(aplRST!projnom) & " "
                    aplRST.MoveNext
                    DoEvents
                Loop
            Else
                Call sp_GetPeticionProyectoNombre(xlSheet.Cells(iContador, colNroInterno))
                Do While Not aplRST.EOF
                    xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag + 1)) = ClearNull(aplRST!projnom) & " "
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next
    End If
    '}
    
    ' Recursos asignados...
    If secNivel = "GRUP" And secGrup <> "NULL" And CHKgru_recursos.Value = 1 Then
        For iContador = 2 To cntRows
            Call Status("Recursos:" & iContador)
            If sp_GetPeticionEsHistorica(xlSheet.Cells(iContador, colNroInterno)) Then
                If sp_GetPeticionRecursoH(xlSheet.Cells(iContador, colNroInterno), secGrup) Then
                    Do While Not aplRST.EOF
                        For j = cntField + 1 To cntLast
                            If xlSheet.Cells(1, j) = ClearNull(aplRST.Fields.item("nom_recurso")) Then
                                xlSheet.Cells(iContador, j) = "X"
                                xlSheet.Columns(j).HorizontalAlignment = XLcenter
                                Exit For
                            End If
                        Next
                        If j > cntLast Then
                            xlSheet.Cells(1, cntLast) = ClearNull(aplRST.Fields.item("nom_recurso"))
                            xlSheet.Cells(iContador, cntLast) = "X"
                            xlSheet.Columns(cntLast).HorizontalAlignment = XLcenter
                            xlSheet.Columns(cntLast).ColumnWidth = 2
                            xlSheet.Columns(cntLast).EntireColumn.AutoFit
                            cntLast = cntLast + 1
                        End If
                        aplRST.MoveNext
                    Loop
                End If
            Else

                If sp_GetPeticionRecurso(xlSheet.Cells(iContador, colNroInterno), secGrup) Then
                    Do While Not aplRST.EOF
                        For j = cntField + 1 To cntLast
                            If xlSheet.Cells(1, j) = ClearNull(aplRST.Fields.item("nom_recurso")) Then
                                xlSheet.Cells(iContador, j) = "X"
                                xlSheet.Columns(j).HorizontalAlignment = XLcenter
                                Exit For
                            End If
                        Next
                        If j > cntLast Then
                            xlSheet.Cells(1, cntLast) = ClearNull(aplRST.Fields.item("nom_recurso"))
                            xlSheet.Cells(iContador, cntLast) = "X"
                            xlSheet.Columns(cntLast).HorizontalAlignment = XLcenter
                            xlSheet.Columns(cntLast).ColumnWidth = 2
                            xlSheet.Columns(cntLast).EntireColumn.AutoFit
                            cntLast = cntLast + 1
                        End If
                        aplRST.MoveNext
                    Loop
                End If
            End If
        Next
    End If
    
    'comenzamos con el GANTT
    If CHKgantt.Value = 1 Then Hacer_Gannt
    xlSheet.Columns(colEstadoPet).Delete
    xlSheet.Columns(colFecPlanFin).Delete
    xlSheet.Columns(colFecPlanIni).Delete
    xlSheet.Columns(colNroInterno).Delete
    Call GuardarPantalla
errOpen:
    ExcelWrk.Save
    ExcelWrk.Close
    ExcelApp.Application.Quit
    Screen.MousePointer = vbNormal
    Status ("Listo.")
    If bScreen Then
        'lRetCode = ShellExecute(Me.HWnd, "open", sFilename, "", "C:\", 1)
        'If lRetCode <= 31 Then MsgBox "Error al Abrir el Documento Adjunto"
        Call EXEC_ShellExecute(Me.hwnd, sFileName)
    End If
    If Not bScreen Then
        MsgBox "Se ha generado exitosamente el archivo: " & Trim(sFileName) & vbCrLf & "Proceso de exportaci�n finalizado.", vbInformation + vbOKOnly, "Exportaci�n finalizada"
    End If
    Unload Me
finx:
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub GuardarPantalla()
    Dim i As Integer
    
    i = i + 1
    With grdDatos
        For i = 1 To .Rows - 1
            SaveSetting "GesPet", "Export01", .TextMatrix(i, grdEXP_CODIGO), IIf(.TextMatrix(i, grdEXP_SI) = "�", "SI", "NO")
            DoEvents
        Next i
    End With
End Sub

'{ add -016- a.
Private Sub cmdOpen_Click()
    On Error GoTo Errores
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = "Exportar consulta a Microsoft Excel"
        .Filter = "Microsoft Excel (*.xls)|*.xls"
        .FilterIndex = 1
        .InitDir = glWRKDIR
        .Flags = cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowSave
        txtFile = .FileName
    End With
Fin:
    Call Puntero(False)
    Exit Sub
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
    End Select
End Sub
'}

Private Sub Inicializar_Grilla()
    With grdDatos
        .Clear
        .Cols = 16
        .Rows = 1
        .Font = "Tahoma"
        .Font.Size = 8
        .SelectionMode = flexSelectionFree
        .TextMatrix(0, grdEXP_ID) = "": .ColWidth(grdEXP_ID) = 0
        .TextMatrix(0, grdEXP_GRUPO) = "": .ColWidth(grdEXP_GRUPO) = 0
        .TextMatrix(0, grdEXP_DESC) = "Opci�n": .ColWidth(grdEXP_DESC) = 4000
        .TextMatrix(0, grdEXP_CODIGO) = "": .ColWidth(grdEXP_CODIGO) = 0
        .TextMatrix(0, grdEXP_ORDEN) = "": .ColWidth(grdEXP_ORDEN) = 0
        .TextMatrix(0, grdEXP_SI) = "Si": .ColWidth(grdEXP_SI) = 600: .ColAlignment(grdEXP_SI) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_NO) = "No": .ColWidth(grdEXP_NO) = 600: .ColAlignment(grdEXP_NO) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_TAG) = "Tag": .ColWidth(grdEXP_TAG) = 0: .ColAlignment(grdEXP_TAG) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_ALIGNMENT) = "Alineaci�n": .ColWidth(grdEXP_ALIGNMENT) = 600: .ColAlignment(grdEXP_ALIGNMENT) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_WIDTH) = "Ancho": .ColWidth(grdEXP_WIDTH) = 600: .ColAlignment(grdEXP_WIDTH) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_WRAPTEXT) = "WrapText": .ColWidth(grdEXP_WRAPTEXT) = 600: .ColAlignment(grdEXP_WRAPTEXT) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_ORIENTATION) = "Orientaci�n": .ColWidth(grdEXP_ORIENTATION) = 600: .ColAlignment(grdEXP_ORIENTATION) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_INDENTLEVEL) = "Indentaci�n": .ColWidth(grdEXP_INDENTLEVEL) = 600: .ColAlignment(grdEXP_INDENTLEVEL) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_SHRINKTOFIT) = "ShrinkToFit": .ColWidth(grdEXP_SHRINKTOFIT) = 600: .ColAlignment(grdEXP_SHRINKTOFIT) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_MERGECELLS) = "MergeCells": .ColWidth(grdEXP_MERGECELLS) = 600: .ColAlignment(grdEXP_MERGECELLS) = flexAlignCenterCenter
        .TextMatrix(0, grdEXP_NOMCAMPO) = "": .ColWidth(grdEXP_NOMCAMPO) = 0: .ColAlignment(grdEXP_NOMCAMPO) = flexAlignCenterCenter
        CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    End With
End Sub

Private Sub Cargar_Grilla()
    Dim i As Integer
    Dim vOpcion(42) As String
    Dim vOpcionSector(11) As String
    Dim vOpcionGrupo(18) As String
    
    Inicializar_Grilla
    
    With grdDatos
        If sp_GetExport_Peticion(Null, "PET") Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, grdEXP_ID) = ClearNull(aplRST.Fields!exp_id)
                .TextMatrix(.Rows - 1, grdEXP_DESC) = ClearNull(aplRST.Fields!exp_desc)
                .TextMatrix(.Rows - 1, grdEXP_CODIGO) = ClearNull(aplRST.Fields!exp_codigo)
                .TextMatrix(.Rows - 1, grdEXP_ALIGNMENT) = ClearNull(aplRST.Fields!exp_colalinea)
                .TextMatrix(.Rows - 1, grdEXP_WIDTH) = ClearNull(aplRST.Fields!exp_colancho)
                .TextMatrix(.Rows - 1, grdEXP_WRAPTEXT) = ClearNull(aplRST.Fields!exp_colwraptext)
                .TextMatrix(.Rows - 1, grdEXP_ORIENTATION) = ClearNull(aplRST.Fields!exp_colorient)
                .TextMatrix(.Rows - 1, grdEXP_INDENTLEVEL) = ClearNull(aplRST.Fields!exp_colindent)
                .TextMatrix(.Rows - 1, grdEXP_SHRINKTOFIT) = ClearNull(aplRST.Fields!exp_colShrinkToFit)
                .TextMatrix(.Rows - 1, grdEXP_MERGECELLS) = ClearNull(aplRST.Fields!exp_colMergeCells)
                .TextMatrix(.Rows - 1, grdEXP_NOMCAMPO) = ClearNull(aplRST.Fields!exp_nomcampo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If secNivel = "SECT" Then
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, grdEXP_DESC) = "Opciones para Sectores"
            .TextMatrix(.Rows - 1, grdEXP_SI) = ""
            CambiarEfectoLinea grdDatos, prmGridEffectFontBold
            PintarLinea grdDatos, prmGridFillRowColorGreen
            If sp_GetExport_Peticion(Null, "SEC") Then
                Do While Not aplRST.EOF
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, grdEXP_ID) = ClearNull(aplRST.Fields!exp_id)
                    .TextMatrix(.Rows - 1, grdEXP_DESC) = ClearNull(aplRST.Fields!exp_desc)
                    .TextMatrix(.Rows - 1, grdEXP_CODIGO) = ClearNull(aplRST.Fields!exp_codigo)
                    .TextMatrix(.Rows - 1, grdEXP_ALIGNMENT) = ClearNull(aplRST.Fields!exp_colalinea)
                    .TextMatrix(.Rows - 1, grdEXP_WIDTH) = ClearNull(aplRST.Fields!colancho)
                    .TextMatrix(.Rows - 1, grdEXP_WRAPTEXT) = ClearNull(aplRST.Fields!exp_colwraptext)
                    .TextMatrix(.Rows - 1, grdEXP_ORIENTATION) = ClearNull(aplRST.Fields!exp_colorient)
                    .TextMatrix(.Rows - 1, grdEXP_INDENTLEVEL) = ClearNull(aplRST.Fields!exp_colindent)
                    .TextMatrix(.Rows - 1, grdEXP_SHRINKTOFIT) = ClearNull(aplRST.Fields!exp_colShrinkToFit)
                    .TextMatrix(.Rows - 1, grdEXP_MERGECELLS) = ClearNull(aplRST.Fields!exp_colMergeCells)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        ElseIf secNivel = "GRUP" Then
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, grdEXP_DESC) = "Opciones para Grupos"
            .TextMatrix(.Rows - 1, grdEXP_SI) = ""
            CambiarEfectoLinea grdDatos, prmGridEffectFontBold
            PintarLinea grdDatos, prmGridFillRowColorGreen
            If sp_GetExport_Peticion(Null, "GRP") Then
                Do While Not aplRST.EOF
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, grdEXP_ID) = ClearNull(aplRST.Fields!exp_id)
                    .TextMatrix(.Rows - 1, grdEXP_DESC) = ClearNull(aplRST.Fields!exp_desc)
                    .TextMatrix(.Rows - 1, grdEXP_CODIGO) = ClearNull(aplRST.Fields!exp_codigo)
                    .TextMatrix(.Rows - 1, grdEXP_ALIGNMENT) = ClearNull(aplRST.Fields!exp_colalinea)
                    .TextMatrix(.Rows - 1, grdEXP_WIDTH) = ClearNull(aplRST.Fields!colancho)
                    .TextMatrix(.Rows - 1, grdEXP_WRAPTEXT) = ClearNull(aplRST.Fields!exp_colwraptext)
                    .TextMatrix(.Rows - 1, grdEXP_ORIENTATION) = ClearNull(aplRST.Fields!exp_colorient)
                    .TextMatrix(.Rows - 1, grdEXP_INDENTLEVEL) = ClearNull(aplRST.Fields!exp_colindent)
                    .TextMatrix(.Rows - 1, grdEXP_SHRINKTOFIT) = ClearNull(aplRST.Fields!exp_colShrinkToFit)
                    .TextMatrix(.Rows - 1, grdEXP_MERGECELLS) = ClearNull(aplRST.Fields!exp_colMergeCells)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        End If
    End With
End Sub

Private Sub GuardarOpciones()
    Dim i As Integer
    
    Call Puntero(True)
    With grdDatos
        For i = 1 To .Rows - 1
            Call sp_UpdateExport_PeticionField(.TextMatrix(i, grdEXP_ID), "EXP_SELECCION", IIf(.TextMatrix(i, grdEXP_SI) = "�", "S", "N"), Null, Null)
        Next i
    End With
    Call Puntero(False)
End Sub

Private Sub grdDatos_DblClick()
    With grdDatos
        .TextMatrix(.RowSel, IIf(.ColSel = grdEXP_NO, grdEXP_SI, grdEXP_NO)) = ""
        .TextMatrix(.RowSel, IIf(.ColSel = grdEXP_NO, grdEXP_NO, grdEXP_SI)) = "�"
    End With
End Sub

Private Sub grdDatos_KeyDown(KeyCode As Integer, Shift As Integer)
    With grdDatos
        If KeyCode = 32 Then
            .TextMatrix(.RowSel, IIf(.ColSel = grdEXP_NO, grdEXP_SI, grdEXP_NO)) = ""
            .TextMatrix(.RowSel, IIf(.ColSel = grdEXP_NO, grdEXP_NO, grdEXP_SI)) = "�"
        End If
    End With
End Sub

'{ add -013- a.
Private Sub Hacer_Gannt()
    Dim auxPlanIni As Date
    Dim auxPlanFin As Date
    Dim strMes As String
    Dim flg2003 As Boolean

    Call Status("Diagramando Gantt...")
    auxPlanFin = #1/1/1980#
    auxPlanIni = #12/12/2010#
    For iContador = 2 To cntRows
        'averiguamos primero y �ltimo
        If xlSheet.Cells(iContador, colFecPlanIni) <> "" Then
            If xlSheet.Cells(iContador, colFecPlanIni) < auxPlanIni Then
                auxPlanIni = xlSheet.Cells(iContador, colFecPlanIni)
            End If
        End If
        If xlSheet.Cells(iContador, colFecPlanFin) <> "" Then
            If xlSheet.Cells(iContador, colFecPlanFin) > auxPlanFin Then
                auxPlanFin = xlSheet.Cells(iContador, colFecPlanFin)
            End If
        End If
    Next
    auxPlanIni = auxPlanIni - Day(auxPlanIni) + 1
    'OJO CON ESTO
    If auxPlanIni < #1/1/2004# Then
        flg2003 = True
        cntLast = cntLast + 1
        auxPlanIni = #1/1/2004#
    End If
    auxPlanFin = DateAdd("m", 1, auxPlanFin)
    auxPlanFin = auxPlanFin - Day(auxPlanFin)
    grantBeg = 0
    grantEnd = calColFecha(auxPlanIni, auxPlanFin)
    auxDate = auxPlanIni
    
    If grantEnd + cntLast > 250 Then
        MsgBox ("El rango de fechas seleccionado es muy grande" & Chr$(13) & "Debe seleccione otro")
    End If
    
    'agrupa de a 4 columnitas
    For j = grantBeg To grantEnd Step 4
        Call Status("Diagramando Gantt: " & j)
        xlSheet.Range(xlSheet.Cells(1, j + cntLast), xlSheet.Cells(1, j + cntLast - 1 + 4)).MergeCells = True
        
        xlSheet.Cells(1, j + cntLast) = "'" & Mid(strMes, (Month(auxDate) - 1) * 3 + 1, 3) & Chr$(10) & Right("" & Year(auxDate), 2)
        auxDate = DateAdd("m", 1, auxDate)
        xlSheet.Range(xlSheet.Cells(1, j + cntLast - 1 + 4), xlSheet.Cells(cntRows, j + cntLast - 1)).Borders(10).LineStyle = 1
    Next
    
    xlSheet.Range(xlSheet.Cells(1, grantBeg + cntLast), xlSheet.Cells(1, grantEnd + cntLast)).ColumnWidth = (7 / 10)
    xlSheet.Range(xlSheet.Cells(1, grantBeg + cntLast), xlSheet.Cells(1, grantEnd + cntLast)).HorizontalAlignment = XLcenter
    xlSheet.Range(xlSheet.Cells(1, cntLast), xlSheet.Cells(cntRows, grantEnd + cntLast)).Borders(9).LineStyle = 1
    xlSheet.Range(xlSheet.Cells(1, cntLast), xlSheet.Cells(1, grantEnd + cntLast)).Font.Bold = False
    
    xlSheet.Range(xlSheet.Cells(1, cntLast), xlSheet.Cells(cntRows, grantEnd + cntLast)).Borders(12).LineStyle = 1
    xlSheet.Range(xlSheet.Cells(1, cntLast - 1), xlSheet.Cells(cntRows, cntLast - 1)).Borders(10).LineStyle = 1
        
    'ojo con esto
    If flg2003 Then
        cntLast = cntLast - 1
        auxPlanIni = #12/31/2003#
        xlSheet.Range(xlSheet.Cells(1, grantBeg + cntLast), xlSheet.Cells(1, grantBeg + cntLast)).ColumnWidth = (7 / 10) * 14
        xlSheet.Range(xlSheet.Cells(1, grantBeg + cntLast), xlSheet.Cells(1, grantBeg + cntLast)).HorizontalAlignment = XLcenter
        'xlSheet.Range(xlSheet.Cells(1, cntLast), xlSheet.Cells(cntRows, grantBeg + cntLast)).Borders(9).LineStyle = 1
        xlSheet.Range(xlSheet.Cells(1, cntLast), xlSheet.Cells(1, grantBeg + cntLast)).Font.Bold = False
        
        xlSheet.Cells(1, cntLast) = "'" & "A�o 2003" & Chr$(10) & "y anterior"
        
        xlSheet.Range(xlSheet.Cells(1, cntLast), xlSheet.Cells(cntRows, grantBeg + cntLast)).Borders(12).LineStyle = 1
        xlSheet.Range(xlSheet.Cells(1, cntLast - 1), xlSheet.Cells(cntRows, cntLast - 1)).Borders(10).LineStyle = 1
    End If

    For iContador = 2 To cntRows
        Call Status("Diagrama:" & iContador)
        If xlSheet.Cells(iContador, colFecPlanIni) <> "" Then
            If xlSheet.Cells(iContador, colFecPlanFin) = "" Then
                xlSheet.Cells(iContador, colFecPlanFin) = xlSheet.Cells(iContador, colFecPlanIni)
            End If
            grantBeg = calColFecha(auxPlanIni, xlSheet.Cells(iContador, colFecPlanIni))
            grantEnd = calColFecha(auxPlanIni, xlSheet.Cells(iContador, colFecPlanFin))
            
            If xlSheet.Cells(iContador, colEstadoPet) = "EJECUC" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 46 'rojo
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "PLANOK" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 43 'verde
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "SUSPEN" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 15 'gris
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "REVISA" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 15 'gris
            ElseIf xlSheet.Cells(iContador, colEstadoPet) = "TERMIN" Then
                xlSheet.Range(xlSheet.Cells(iContador, grantBeg + cntLast), xlSheet.Cells(iContador, grantEnd + cntLast)).Interior.ColorIndex = 37  'azul
            End If
        End If
    Next
End Sub

Private Sub cmdCerrar_Click()
    'GuardarOpciones
    GuardarPantalla
    If flgEnCarga = True Then Exit Sub
    Call DetenerScroll(grdDatos)
    Unload Me
End Sub

' *********************************************************************************************************************
' *************************************************** FIN DEL CODIGO **************************************************
' *********************************************************************************************************************
















'    vOpcion(0) = "Nro. de petici�n"
'    vOpcion(1) = "Tipo"
'    vOpcion(2) = "Clase"
'    vOpcion(3) = "Impacto tecnol�gico"
'    vOpcion(4) = "Regulatorio"
'    vOpcion(5) = "T�tulo"
'    vOpcion(6) = "Descripci�n"
'    vOpcion(7) = "Caracter�sticas"
'    vOpcion(8) = "Motivos"
'    vOpcion(9) = "Beneficios"
'    vOpcion(10) = "Relac. con otros req."
'    vOpcion(11) = "Observaciones"
'    vOpcion(12) = "Prioridad"
'    vOpcion(13) = "F. envio al BP"
'    vOpcion(14) = "Estado"
'    vOpcion(15) = "Fec.Estado"
'    vOpcion(16) = "Situaci�n"
'    vOpcion(17) = "Sector Solicitante"
'    vOpcion(18) = "Business Partner"
'    vOpcion(19) = "Documentos adjuntos"
'    vOpcion(20) = "Conformes homologaci�n"
'    vOpcion(21) = "Solicitante"
'    vOpcion(22) = "Referente"
'    vOpcion(23) = "Supervisor"
'    vOpcion(24) = "Autorizante"
'    vOpcion(25) = "horas presup.p / petici�n"
'    vOpcion(26) = "Fecha inicio planificaci�n"
'    vOpcion(27) = "Fecha fin planificaci�n"
'    vOpcion(28) = "Fecha inicio real"
'    vOpcion(29) = "Fecha fin real"
'    vOpcion(30) = "Fecha inicio planificaci�n original"
'    vOpcion(31) = "Fecha fin planificaci�n original"
'    vOpcion(32) = "Cant.de replanificaciones"
'    vOpcion(33) = "C/L"
'    vOpcion(34) = "visibilidad"
'    vOpcion(35) = "Orientaci�n"
'    vOpcion(36) = "Historial"
'    vOpcion(37) = "Conformes"
'    vOpcion(38) = "Nro. de seguimiento asociado"
'    vOpcion(39) = "Tit. de seguimiento asociado"
'    vOpcion(40) = "Total de horas reales asignadas"
'    vOpcion(41) = "Proyecto IDM"
'
'    vOpcionSector(0) = "Sector ejecutor"
'    vOpcionSector(1) = "Estado"
'    vOpcionSector(2) = "Situaci�n"
'    vOpcionSector(3) = "Horas"
'    vOpcionSector(4) = "Fecha inicio planificaci�n"
'    vOpcionSector(5) = "Fecha fin planificaci�n"
'    vOpcionSector(6) = "Fecha inicio real"
'    vOpcionSector(7) = "Fecha fin real"
'    vOpcionSector(8) = "Fecha inicio planificaci�n original"
'    vOpcionSector(9) = "Fecha fin planificaci�n original"
'    vOpcionSector(10) = "Horas reales x Sector"
'
'    vOpcionGrupo(0) = "Grupo ejecutor"
'    vOpcionGrupo(1) = "Prioridad"
'    vOpcionGrupo(2) = "Estado"
'    vOpcionGrupo(3) = "Situaci�n"
'    vOpcionGrupo(4) = "Horas"
'    vOpcionGrupo(5) = "Fecha inicio planificaci�n"
'    vOpcionGrupo(6) = "Fecha fin planificaci�n"
'    vOpcionGrupo(7) = "Fecha inicio real"
'    vOpcionGrupo(8) = "Fecha fin real"
'    vOpcionGrupo(9) = "Fecha inicio planificaci�n original"
'    vOpcionGrupo(10) = "Fecha fin planificaci�n original"
'    vOpcionGrupo(11) = "Recursos asignados"
'    vOpcionGrupo(12) = "Informaci�n adicional"
'    vOpcionGrupo(13) = "Horas reales x Grupo"
'    vOpcionGrupo(14) = "F. prevista de pasaje a Prod."
'    vOpcionGrupo(15) = "F. de Fin de suspensi�n"
'    vOpcionGrupo(16) = "Motivo de suspensi�n"
'    vOpcionGrupo(17) = "Observaciones del estado actual"
'
'    With grdDatos
'        For i = 0 To UBound(vOpcion) - 1
'            .Rows = .Rows + 1
'            .TextMatrix(.Rows - 1, 0) = vOpcion(i)
'            .TextMatrix(.Rows - 1, 1) = "�"
'            .TextMatrix(.Rows - 1, 2) = ""
'        Next i
'        If secNivel = "GRUP" Then
'            .Rows = .Rows + 1
'            .TextMatrix(.Rows - 1, 0) = "Opciones para Grupos"
'            .TextMatrix(.Rows - 1, 1) = ""
'            .TextMatrix(.Rows - 1, 2) = ""
'            CambiarEfectoLinea grdDatos, prmGridEffectFontBold
'            PintarLinea grdDatos, prmGridFillRowColorGreen
'            For i = 0 To UBound(vOpcionGrupo) - 1
'                .Rows = .Rows + 1
'                .TextMatrix(.Rows - 1, 0) = vOpcionGrupo(i)
'                .TextMatrix(.Rows - 1, 1) = "�"
'                .TextMatrix(.Rows - 1, 2) = ""
'            Next i
'        ElseIf secNivel = "SECT" Then
'            .Rows = .Rows + 1
'            .TextMatrix(.Rows - 1, 0) = "Opciones para Sectores"
'            .TextMatrix(.Rows - 1, 1) = ""
'            .TextMatrix(.Rows - 1, 2) = ""
'            CambiarEfectoLinea grdDatos, prmGridEffectFontBold
'            PintarLinea grdDatos, prmGridFillRowColorGreen
'            For i = 0 To UBound(vOpcionSector) - 1
'                .Rows = .Rows + 1
'                .TextMatrix(.Rows - 1, 0) = vOpcionSector(i)
'                .TextMatrix(.Rows - 1, 1) = ""
'                .TextMatrix(.Rows - 1, 2) = "�"
'            Next i
'        End If
'    End With


'Private Sub Hacer_Historico()
'    Dim lSumaHoras As Long
'
'    With frmPeticionesShellView01
'        If chkHorasReales.Value = 1 Then
'            'If Not sp_rptPeticion03(1, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), "N", .prj_NroProyecto, .prj_tipo, .prj_titulo, .prj_bpar, .prj_Orientacion, .prj_Importancia, .prj_estado, .prj_nivel, .prj_area, .prj_DESDEiniplan, .prj_HASTAiniplan, .prj_DESDEfinplan, .prj_HASTAfinplan, .prj_DESDEinireal, .prj_HASTAinireal, .prj_DESDEfinreal, .prj_HASTAfinreal, _
'            '                        CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), Null) Then   ' upd -001- b. - Se agregan los par�metros necesarios para la consulta (se agregaron al final de la declaraci�n del procedimiento los controles para clase de petici�n e indicador de impacto tecnol�gico).
'            If Not sp_rptPeticion03(1, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), _
'                                    CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), Null) Then   ' upd -001- b. - Se agregan los par�metros necesarios para la consulta (se agregaron al final de la declaraci�n del procedimiento los controles para clase de petici�n e indicador de impacto tecnol�gico).
'               'GoTo finx
'            End If
'        Else
'            'If Not sp_rptPeticion01(1, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), "N", .prj_NroProyecto, .prj_tipo, .prj_titulo, .prj_bpar, .prj_Orientacion, .prj_Importancia, .prj_estado, .prj_nivel, .prj_area, .prj_DESDEiniplan, .prj_HASTAiniplan, .prj_DESDEfinplan, .prj_HASTAfinplan, .prj_DESDEinireal, .prj_HASTAinireal, .prj_DESDEfinreal, .prj_HASTAfinreal, _
'            '                        CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), Null) Then   ' upd -001- b. - Se agregan los par�metros necesarios para la consulta (se agregaron al final de la declaraci�n del procedimiento los controles para clase de petici�n e indicador de impacto tecnol�gico).
'            If Not sp_rptPeticion01(1, .txtNroDesde.Text, .txtNroHasta.Text, petTipo, petPrioridad, .txtDESDEalta.DateValue, .txtHASTAalta.DateValue, petNivel, petArea, petEstado, .txtDESDEestado.DateValue, .txtHASTAestado.DateValue, petSituacion, petTitu, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, .txtDESDEiniplan.DateValue, .txtHASTAiniplan.DateValue, .txtDESDEfinplan.DateValue, .txtHASTAfinplan.DateValue, .txtDESDEinireal.DateValue, .txtHASTAinireal.DateValue, .txtDESDEfinreal.DateValue, .txtHASTAfinreal.DateValue, auxAgrup, petImportancia, CodigoCombo(.cboBpar, True), _
'                                    CodigoCombo(frmPeticionesShellView01.cmbPetClass, True), UCase(Left(frmPeticionesShellView01.cmbPetImpTech.Text, 1)), Null) Then   ' upd -001- b. - Se agregan los par�metros necesarios para la consulta (se agregaron al final de la declaraci�n del procedimiento los controles para clase de petici�n e indicador de impacto tecnol�gico).
'               'GoTo finx
'            End If
'        End If
'    End With
'
'    Do While Not aplRST.EOF
'        iContador = iContador + 1
'        lSumaHoras = 0
'        Call status("Cantidad de peticiones exportadas: " & iContador)
'        xlSheet.Cells(iContador, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
'        If CHKpet_nroasignado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_nroasignado.Tag)) = ClearNull(aplRST!pet_nroasignado)
'        If CHKcod_tipo_peticion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKcod_tipo_peticion.Tag)) = ClearNull(aplRST!cod_tipo_peticion)
'        If chkPetClass.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetClass.Tag)) = ClearNull(aplRST!cod_clase)
'        If chkPetImpTech.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetImpTech.Tag)) = ClearNull(aplRST!pet_imptech)
'        If CHKtitulo.Value = 1 Then xlSheet.Cells(iContador, Val(CHKtitulo.Tag)) = ClearNull(aplRST!titulo)
'        If CHKprioridad.Value = 1 Then xlSheet.Cells(iContador, Val(CHKprioridad.Tag)) = ClearNull(aplRST!prioridad) & " "
'        If CHKfe_ing_comi.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ing_comi.Tag)) = IIf(Not IsNull(aplRST!fe_comite), Format(aplRST!fe_comite, "yyyy-mm-dd"), "")
'        If CHKpet_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_estado.Tag)) = ClearNull(aplRST!nom_estado)
'        If CHKpet_fe_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_fe_estado.Tag)) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
'        If CHKpet_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_situacion.Tag)) = ClearNull(aplRST!nom_situacion) & " "
'        If CHKpet_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKpet_sector.Tag)) = ClearNull(aplRST!sol_direccion) & " >> " & ClearNull(aplRST!sol_gerencia) & " >> " & ClearNull(aplRST!sol_sector)
'        If CHKbpar.Value = 1 Then xlSheet.Cells(iContador, Val(CHKbpar.Tag)) = ClearNull(aplRST!nom_bpar)
'        If CHKhoraspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKhoraspresup.Tag)) = ClearNull(aplRST!horaspresup)
'        ' Horas Reales (Cabecera de Peticiones)
'        If chkHorasReales.Value = 1 Then
'            If Not IsNull(aplRST!Horas_Responsables) Then
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag)) = CDbl(ClearNull(aplRST!Horas_Responsables))
'            Else
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag)) = "=" & CDbl(0)
'            End If
'            If Not IsNull(aplRST!Horas_Recursos) Then
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag) + 1) = CDbl(ClearNull(aplRST!Horas_Recursos))
'            Else
'                xlSheet.Cells(iContador, Val(chkHorasReales.Tag) + 1) = "=" & CDbl(0)
'            End If
'            If chkHorasRealesOtros.Value = 1 Then
'                If Not IsNull(aplRST!Horas_Responsables_NoAsignadas) Then
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag)) = CDbl(ClearNull(aplRST!Horas_Responsables_NoAsignadas))
'                Else
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag)) = "=" & CDbl(0)
'                End If
'                If Not IsNull(aplRST!Horas_Recursos_NoAsignadas) Then
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag) + 1) = CDbl(ClearNull(aplRST!Horas_Recursos_NoAsignadas))
'                Else
'                    xlSheet.Cells(iContador, Val(chkHorasRealesOtros.Tag) + 1) = "=" & CDbl(0)
'                End If
'            End If
'        End If
'        If chkHorasReales.Value = 1 Then
'            If flgHijos Then
'                If secNivel = "GRUP" Then
'                    If chkHorasRealesGrupo.Value = 1 Then
'                        ' *** Abierto por Grupos ***
'                        ' Primero las horas de responsables (para asignados)
'                        Set rsAux = sp_GetHorasReales("GRU", "RSP", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), ClearNull(aplRST!sec_cod_grupo))
'                        If Not rsAux.EOF Then
'                            Do While Not rsAux.EOF
'                                lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                                DoEvents
'                                rsAux.MoveNext
'                            Loop
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag)) = ClearNull(lSumaHoras)
'                        Else
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag)) = 0
'                        End If
'                        rsAux.Close: lSumaHoras = 0
'                        ' Luego las horas de recursos (asignados)
'                        Set rsAux = sp_GetHorasReales("GRU", "REC", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), ClearNull(aplRST!sec_cod_grupo))
'                        If Not rsAux.EOF Then
'                            Do While Not rsAux.EOF
'                                lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                                DoEvents
'                                rsAux.MoveNext
'                            Loop
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag) + 1) = ClearNull(lSumaHoras)
'                        Else
'                            xlSheet.Cells(iContador, Val(chkHorasRealesGrupo.Tag) + 1) = 0
'                        End If
'                        rsAux.Close: lSumaHoras = 0
'                    End If
'                ElseIf secNivel = "SECT" Then
'                    ' *** Abierto por Sectores ***
'                    ' Primero las horas de responsables (asignadas)
'                    Set rsAux = sp_GetHorasReales("SEC", "RSP", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), Null)
'                    If Not rsAux.EOF Then
'                        Do While Not rsAux.EOF
'                            lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                            DoEvents
'                            rsAux.MoveNext
'                        Loop
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag)) = ClearNull(lSumaHoras)
'                    Else
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag)) = 0
'                    End If
'                    rsAux.Close: lSumaHoras = 0
'                    ' Luego las horas de recursos
'                    Set rsAux = sp_GetHorasReales("SEC", "REC", ClearNull(aplRST!pet_nrointerno), ClearNull(aplRST!sec_cod_sector), Null)
'                    If Not rsAux.EOF Then
'                        Do While Not rsAux.EOF
'                            lSumaHoras = lSumaHoras + CLng(rsAux!horas)
'                            DoEvents
'                            rsAux.MoveNext
'                        Loop
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag) + 1) = ClearNull(lSumaHoras)
'                    Else
'                        xlSheet.Cells(iContador, Val(chkHorasRealesSector.Tag) + 1) = 0
'                    End If
'                    rsAux.Close: lSumaHoras = 0
'                End If
'            End If
'        End If
'        If CHKfe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
'        If CHKfe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
'        ' Se guarda para grantt
'        If CodigoCombo(cboGantt, True) = "PETI" Then
'            xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!cod_estado)
'        Else
'            xlSheet.Cells(iContador, colFecPlanIni) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colFecPlanFin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'            xlSheet.Cells(iContador, colEstadoPet) = ClearNull(aplRST!sec_cod_estado)
'        End If
'        If CHKfe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_real.Tag)) = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "yyyy-mm-dd"), "")
'        If CHKfe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_real.Tag)) = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "yyyy-mm-dd"), "")
'        If CHKfe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!fe_ini_orig), Format(aplRST!fe_ini_orig, "yyyy-mm-dd"), "")
'        If CHKfe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKfe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!fe_fin_orig), Format(aplRST!fe_fin_orig, "yyyy-mm-dd"), "")
'        If CHKrepla.Value = 1 Then xlSheet.Cells(iContador, Val(CHKrepla.Tag)) = IIf(Val(ClearNull(aplRST!cant_planif)) < 2, "", Val(ClearNull(aplRST!cant_planif)) - 1)
'        If CHKimportancia_nom.Value = 1 Then xlSheet.Cells(iContador, Val(CHKimportancia_nom.Tag)) = ClearNull(aplRST!importancia_nom)
'        If CHKcorp_local.Value = 1 Then xlSheet.Cells(iContador, Val(CHKcorp_local.Tag)) = ClearNull(aplRST!corp_local)
'        If CHKnom_orientacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKnom_orientacion.Tag)) = ClearNull(aplRST!nom_orientacion)
'        If CHKseguimN.Value = 1 Then xlSheet.Cells(iContador, Val(CHKseguimN.Tag)) = ClearNull(aplRST!prj_nrointerno)
'        If CHKseguimT.Value = 1 Then xlSheet.Cells(iContador, Val(CHKseguimT.Tag)) = ClearNull(aplRST!prj_titulo)
'        If flgHijos Then
'            If secNivel = "GRUP" Then
'                If CHKgru_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_sector.Tag)) = ClearNull(aplRST!sec_nom_sector) & ">> " & ClearNull(aplRST!sec_nom_grupo)
'                If (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
'                    If CHKprio_ejecuc.Value = 1 Then xlSheet.Cells(iContador, Val(CHKprio_ejecuc.Tag)) = ClearNull(aplRST!prio_ejecuc) & " "
'                    If CHKgru_info_adic.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_info_adic.Tag)) = ClearNull(aplRST!info_adicio) & " "
'                End If
'                If CHKgru_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_estado.Tag)) = ClearNull(aplRST!sec_nom_estado)
'                If CHKgru_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
'                If CHKgru_horaspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
'                If CHKgru_fe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'                If CHKgru_fe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'                If CHKgru_fe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
'                If CHKgru_fe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
'                If CHKgru_fe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
'                If CHKgru_fe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKgru_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
'                If chkFechaProduccion.Value = 1 Then xlSheet.Cells(iContador, Val(chkFechaProduccion.Tag)) = IIf(Not IsNull(aplRST!fe_produccion), Format(aplRST!fe_produccion, "yyyy-mm-dd"), "")
'                If chkFechaSuspension.Value = 1 Then xlSheet.Cells(iContador, Val(chkFechaSuspension.Tag)) = IIf(Not IsNull(aplRST!fe_suspension), Format(aplRST!fe_suspension, "yyyy-mm-dd"), "")
'                If chkMotivoSuspension.Value = 1 Then xlSheet.Cells(iContador, Val(chkMotivoSuspension.Tag)) = ClearNull(aplRST!nom_motivo) & " "
'                If chkObservEstado.Value = 1 Then xlSheet.Cells(iContador, Val(chkObservEstado.Tag)) = ClearNull(aplRST!ObsEstado) & " "
'            Else
'                If CHKsec_sector.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_sector.Tag)) = ClearNull(aplRST!sec_nom_sector)
'                If CHKsec_estado.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_estado.Tag)) = ClearNull(aplRST!sec_nom_estado) & " "
'                If CHKsec_situacion.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_situacion.Tag)) = ClearNull(aplRST!sec_nom_situacion) & " "
'                If CHKsec_horaspresup.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_horaspresup.Tag)) = ClearNull(aplRST!sec_horaspresup) & " "
'                If CHKsec_fe_ini_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
'                If CHKsec_fe_fin_plan.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_plan.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
'                If CHKsec_fe_ini_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_real), Format(aplRST!sec_fe_ini_real, "yyyy-mm-dd"), "")
'                If CHKsec_fe_fin_real.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_real.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
'                If CHKsec_fe_ini_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_ini_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_ini_orig), Format(aplRST!sec_fe_ini_orig, "yyyy-mm-dd"), "")
'                If CHKsec_fe_fin_orig.Value = 1 Then xlSheet.Cells(iContador, Val(CHKsec_fe_fin_orig.Tag)) = IIf(Not IsNull(aplRST!sec_fe_fin_orig), Format(aplRST!sec_fe_fin_orig, "yyyy-mm-dd"), "")
'            End If
'        End If
'        If chkPetDocu.Value = 1 Then
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag)) = IIf(ClearNull(aplRST!pet_mod1None) = 0, "No hay adjuntos", "Tiene adjuntos") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 1)) = IIf(Not IsNull(aplRST!pet_mod1C100) And aplRST!pet_mod1C100 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 2)) = IIf(Not IsNull(aplRST!pet_mod1C102) And aplRST!pet_mod1C102 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 3)) = IIf(Not IsNull(aplRST!pet_mod1P950) And aplRST!pet_mod1P950 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 4)) = IIf(Not IsNull(aplRST!pet_mod1C204) And aplRST!pet_mod1C204 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 5)) = IIf(Not IsNull(aplRST!pet_mod1CG04) And aplRST!pet_mod1CG04 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 6)) = IIf(Not IsNull(aplRST!pet_mod1T710) And aplRST!pet_mod1T710 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 7)) = IIf(Not IsNull(aplRST!pet_mod1EML1) And aplRST!pet_mod1EML1 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 8)) = IIf(Not IsNull(aplRST!pet_mod1EML2) And aplRST!pet_mod1EML2 > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 9)) = IIf(Not IsNull(aplRST!pet_mod1IDHX) And aplRST!pet_mod1IDHX > 0, "X", " ") & " "
'            xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10)) = IIf(Not IsNull(aplRST!pet_mod1OTRO) And aplRST!pet_mod1OTRO > 0, "X", " ") & " "
'            ' Para las peticiones del viejo modelo de control, se agregan a OTROs
'            If aplRST!pet_mod1OTRO = 0 Then
'                If Not Trim(xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10))) = "X" Then
'                    xlSheet.Cells(iContador, Val(chkPetDocu.Tag + 10)) = IIf(Not IsNull(aplRST!pet_mod0TODO) And aplRST!pet_mod0TODO > 0, "X", " ") & " "
'                End If
'            End If
'        End If
'        '}
'        If chkPetRegulatorio.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetRegulatorio.Tag)) = ClearNull(aplRST!pet_regulatorio)    ' add -009- a.
'        If chkPetProyectoIDM.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag)) = ClearNull(aplRST!pet_projid) & "." & ClearNull(aplRST!pet_projsubid) & "." & ClearNull(aplRST!pet_projsubsid)
'        If chkPetProyectoIDM.Value = 1 Then xlSheet.Cells(iContador, Val(chkPetProyectoIDM.Tag + 1)) = ClearNull(aplRST!ProjNom)
'        aplRST.MoveNext
'        DoEvents
'    Loop
'End Sub
''}



'Sub InicializarPantalla()
'    Cargar_Grilla
'    txtFile.Text = GetSetting("GesPet", "Export01", "FileExport", glWRKDIR & "EXPORTACION.XLS")
'    CHKpet_nroasignado.Value = IIf(GetSetting("GesPet", "Export01", "CHKpet_nroasignado") = "SI", 1, 0)
'    CHKcod_tipo_peticion.Value = IIf(GetSetting("GesPet", "Export01", "CHKcod_tipo_peticion") = "SI", 1, 0)
'    CHKtitulo.Value = IIf(GetSetting("GesPet", "Export01", "CHKtitulo") = "SI", 1, 0)
'    CHKdetalle.Value = IIf(GetSetting("GesPet", "Export01", "CHKdetalle") = "SI", 1, 0)
'    CHKcaracte.Value = IIf(GetSetting("GesPet", "Export01", "CHKcaracte") = "SI", 1, 0)
'    CHKmotivos.Value = IIf(GetSetting("GesPet", "Export01", "CHKmotivos") = "SI", 1, 0)
'    CHKbenefic.Value = IIf(GetSetting("GesPet", "Export01", "CHKbenefic") = "SI", 1, 0)
'    CHKrelacio.Value = IIf(GetSetting("GesPet", "Export01", "CHKrelacio") = "SI", 1, 0)
'    CHKobserva.Value = IIf(GetSetting("GesPet", "Export01", "CHKobserva") = "SI", 1, 0)
'    CHKprioridad.Value = IIf(GetSetting("GesPet", "Export01", "CHKprioridad") = "SI", 1, 0)
'    CHKfe_ing_comi.Value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ing_comi") = "SI", 1, 0)
'    CHKpet_estado.Value = IIf(GetSetting("GesPet", "Export01", "CHKpet_estado") = "SI", 1, 0)
'    CHKpet_fe_estado.Value = IIf(GetSetting("GesPet", "Export01", "CHKpet_fe_estado") = "SI", 1, 0)
'    CHKpet_situacion.Value = IIf(GetSetting("GesPet", "Export01", "CHKpet_situacion") = "SI", 1, 0)
'    CHKpet_sector.Value = IIf(GetSetting("GesPet", "Export01", "CHKpet_sector") = "SI", 1, 0)
'    CHKbpar.Value = IIf(GetSetting("GesPet", "Export01", "CHKbpar") = "SI", 1, 0)
'    CHKsoli.Value = IIf(GetSetting("GesPet", "Export01", "CHKsoli") = "SI", 1, 0)
'    CHKrefe.Value = IIf(GetSetting("GesPet", "Export01", "CHKrefe") = "SI", 1, 0)
'    CHKsupe.Value = IIf(GetSetting("GesPet", "Export01", "CHKsupe") = "SI", 1, 0)
'    CHKauto.Value = IIf(GetSetting("GesPet", "Export01", "CHKauto") = "SI", 1, 0)
'    CHKhoraspresup.Value = IIf(GetSetting("GesPet", "Export01", "CHKhoraspresup") = "SI", 1, 0)
'    CHKfe_ini_plan.Value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ini_plan") = "SI", 1, 0)
'    CHKfe_fin_plan.Value = IIf(GetSetting("GesPet", "Export01", "CHKfe_fin_plan") = "SI", 1, 0)
'    CHKfe_ini_real.Value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ini_real") = "SI", 1, 0)
'    CHKfe_fin_real.Value = IIf(GetSetting("GesPet", "Export01", "CHKfe_fin_real") = "SI", 1, 0)
'    CHKfe_ini_orig.Value = IIf(GetSetting("GesPet", "Export01", "CHKfe_ini_orig") = "SI", 1, 0)
'    CHKfe_fin_orig.Value = IIf(GetSetting("GesPet", "Export01", "CHKfe_fin_orig") = "SI", 1, 0)
'    CHKrepla.Value = IIf(GetSetting("GesPet", "Export01", "CHKrepla") = "SI", 1, 0)
'    CHKimportancia_nom.Value = IIf(GetSetting("GesPet", "Export01", "CHKimportancia_nom") = "SI", 1, 0)
'    CHKcorp_local.Value = IIf(GetSetting("GesPet", "Export01", "CHKcorp_local") = "SI", 1, 0)
'    CHKnom_orientacion.Value = IIf(GetSetting("GesPet", "Export01", "CHKnom_orientacion") = "SI", 1, 0)
'    CHKsec_sector.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_sector") = "SI", 1, 0)
'    CHKsec_estado.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_estado") = "SI", 1, 0)
'    CHKsec_situacion.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_situacion") = "SI", 1, 0)
'    CHKsec_horaspresup.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_horaspresup") = "SI", 1, 0)
'    CHKsec_fe_ini_plan.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_ini_plan") = "SI", 1, 0)
'    CHKsec_fe_fin_plan.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_fin_plan") = "SI", 1, 0)
'    CHKsec_fe_ini_real.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_ini_real") = "SI", 1, 0)
'    CHKsec_fe_fin_real.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_fin_real") = "SI", 1, 0)
'    CHKsec_fe_ini_orig.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_ini_orig") = "SI", 1, 0)
'    CHKsec_fe_fin_orig.Value = IIf(GetSetting("GesPet", "Export01", "CHKsec_fe_fin_orig") = "SI", 1, 0)
'    CHKgru_sector.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_sector") = "SI", 1, 0)
'    CHKprio_ejecuc.Value = IIf(GetSetting("GesPet", "Export01", "CHKprio_ejecuc") = "SI", 1, 0)
'    CHKgru_estado.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_estado") = "SI", 1, 0)
'    CHKgru_situacion.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_situacion") = "SI", 1, 0)
'    CHKgru_horaspresup.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_horaspresup") = "SI", 1, 0)
'    CHKgru_fe_ini_plan.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_ini_plan") = "SI", 1, 0)
'    CHKgru_fe_fin_plan.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_fin_plan") = "SI", 1, 0)
'    CHKgru_fe_ini_real.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_ini_real") = "SI", 1, 0)
'    CHKgru_fe_fin_real.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_fin_real") = "SI", 1, 0)
'    CHKgru_fe_ini_orig.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_ini_orig") = "SI", 1, 0)
'    CHKgru_fe_fin_orig.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_fe_fin_orig") = "SI", 1, 0)
'    CHKgru_info_adic.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_info_adic") = "SI", 1, 0)
'    CHKgru_recursos.Value = IIf(GetSetting("GesPet", "Export01", "CHKgru_recursos") = "SI", 1, 0)
'    CHKgantt.Value = IIf(GetSetting("GesPet", "Export01", "CHKgantt") = "SI", 1, 0)
'    glSelectAgrupStr = GetSetting("GesPet", "Export01", "glSelectAgrupStr", "")
'    glSelectAgrup = GetSetting("GesPet", "Export01", "glSelectAgrup", "")
'    txtAgrup.Text = ClearNull(Left(glSelectAgrupStr, 50))
'    CHKhistorial.Value = 0
'    'CHKgantt.Value = 0
'    If InPerfil("SADM") Or InPerfil("ADMI") Or InPerfil("BPAR") Or SoyElAnalista() Then    ' upd -011- b. Se agrega que contemple el perfil de SADM.
'        CHKhistorial.Visible = True
'    Else
'        CHKhistorial.Visible = False
'    End If
'    If InPerfil("CGRU") Or InPerfil("CSEC") Then
'        CHKgru_info_adic.Enabled = True
'        CHKprio_ejecuc.Enabled = True
'    Else
'        CHKgru_info_adic.Value = 0
'        CHKgru_info_adic.Enabled = False
'        CHKprio_ejecuc.Value = 0
'        CHKprio_ejecuc.Enabled = False
'    End If
'    '{ add -001- a.
'    chkPetClass.Value = IIf(GetSetting("GesPet", "Export01", "chkPetClass") = "SI", 1, 0)
'    chkPetImpTech.Value = IIf(GetSetting("GesPet", "Export01", "chkPetImpTech") = "SI", 1, 0)
'    chkPetDocu.Value = IIf(GetSetting("GesPet", "Export01", "chkPetDocu") = "SI", 1, 0)
'    '}
'    chkHorasReales.Value = IIf(GetSetting("GesPet", "Export01", "chkHorasReales") = "SI", 1, 0)     ' add -003- a.
'    '{ add -005- a.
'    CHKpcf.Value = IIf(GetSetting("GesPet", "Export01", "CHKpcf") = "SI", 1, 0)
'    CHKseguimN.Value = IIf(GetSetting("GesPet", "Export01", "CHKseguimN") = "SI", 1, 0)
'    CHKseguimT.Value = IIf(GetSetting("GesPet", "Export01", "CHKseguimT") = "SI", 1, 0)
'    ' Se agregan los comentarios pertinentes a las opciones a seleccionar
'    chkPetDocu.ToolTipText = "Esta opci�n agrega la identificaci�n de qu� documentos adjuntos de la metodolog�a se encuentran adjuntos a la petici�n"
'    '}
'    '{ add -006- a.
'    chkFechaProduccion.Value = IIf(GetSetting("GesPet", "Export01", "chkFechaProduccion") = "SI", 1, 0)
'    chkFechaSuspension.Value = IIf(GetSetting("GesPet", "Export01", "chkFechaSuspension") = "SI", 1, 0)
'    chkMotivoSuspension.Value = IIf(GetSetting("GesPet", "Export01", "chkMotivoSuspension") = "SI", 1, 0)
'    chkObservEstado.Value = IIf(GetSetting("GesPet", "Export01", "chkObservEstado") = "SI", 1, 0)
'    '}
'    chkPetRegulatorio.Value = IIf(GetSetting("GesPet", "Export01", "chkPetRegulatorio") = "SI", 1, 0)       ' add -008- a.
'    chkPetConfHomo.Value = IIf(GetSetting("GesPet", "Export01", "chkPetConfHomo") = "SI", 1, 0)             ' add -009- a.
'    chkPetProyectoIDM.Value = IIf(GetSetting("GesPet", "Export01", "chkPetProyectoIDM") = "SI", 1, 0)             ' add -010- a.
'    'CHKPeticionesHistoricas.Value = IIf(GetSetting("GesPet", "Export01", "chkPeticionesHistoricas") = "SI", 1, 0)   ' add -013- a.
'End Sub
'

'Sub GuardarPantalla()
'    SaveSetting "GesPet", "Export01", "CHKpet_nroasignado", IIf(CHKpet_nroasignado.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKcod_tipo_peticion", IIf(CHKcod_tipo_peticion.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKtitulo", IIf(CHKtitulo.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKdetalle", IIf(CHKdetalle.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKcaracte", IIf(CHKcaracte.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKmotivos", IIf(CHKmotivos.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKbenefic", IIf(CHKbenefic.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKrelacio", IIf(CHKrelacio.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKobserva", IIf(CHKobserva.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKprioridad", IIf(CHKprioridad.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKfe_ing_comi", IIf(CHKfe_ing_comi.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKpet_estado", IIf(CHKpet_estado.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKpet_fe_estado", IIf(CHKpet_fe_estado.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKpet_situacion", IIf(CHKpet_situacion.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKpet_sector", IIf(CHKpet_sector.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKbpar", IIf(CHKbpar.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsoli", IIf(CHKsoli.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKrefe", IIf(CHKrefe.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsupe", IIf(CHKsupe.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKauto", IIf(CHKauto.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKhoraspresup", IIf(CHKhoraspresup.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKfe_ini_plan", IIf(CHKfe_ini_plan.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKfe_fin_plan", IIf(CHKfe_fin_plan.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKfe_ini_real", IIf(CHKfe_ini_real.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKfe_fin_real", IIf(CHKfe_fin_real.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKfe_ini_orig", IIf(CHKfe_ini_orig.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKfe_fin_orig", IIf(CHKfe_fin_orig.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKrepla", IIf(CHKrepla.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKimportancia_nom", IIf(CHKimportancia_nom.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKcorp_local", IIf(CHKcorp_local.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKnom_orientacion", IIf(CHKnom_orientacion.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_sector", IIf(CHKsec_sector.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_estado", IIf(CHKsec_estado.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_situacion", IIf(CHKsec_situacion.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_horaspresup", IIf(CHKsec_horaspresup.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_plan", IIf(CHKsec_fe_ini_plan.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_plan", IIf(CHKsec_fe_fin_plan.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_real", IIf(CHKsec_fe_ini_real.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_real", IIf(CHKsec_fe_fin_real.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_orig", IIf(CHKsec_fe_ini_orig.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_orig", IIf(CHKsec_fe_fin_orig.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_sector", IIf(CHKgru_sector.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKprio_ejecuc", IIf(CHKprio_ejecuc.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_estado", IIf(CHKgru_estado.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_situacion", IIf(CHKgru_situacion.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_horaspresup", IIf(CHKgru_horaspresup.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_plan", IIf(CHKgru_fe_ini_plan.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_plan", IIf(CHKgru_fe_fin_plan.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_real", IIf(CHKgru_fe_ini_real.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_real", IIf(CHKgru_fe_fin_real.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_orig", IIf(CHKgru_fe_ini_orig.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_orig", IIf(CHKgru_fe_fin_orig.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_info_adic", IIf(CHKgru_info_adic.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgru_recursos", IIf(CHKgru_recursos.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKgantt", IIf(CHKgantt.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "FileExport", ClearNull(txtFile.Text)
'    SaveSetting "GesPet", "Export01", "glSelectAgrupStr", ClearNull(glSelectAgrupStr)
'    SaveSetting "GesPet", "Export01", "glSelectAgrup", ClearNull(glSelectAgrup)
'    '{ add -001- a.
'    SaveSetting "GesPet", "Export01", "chkPetClass", IIf(chkPetClass.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkPetImpTech", IIf(chkPetImpTech.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkPetDocu", IIf(chkPetDocu.Value = 1, "SI", "NO")
'    '}
'    '{ add -003- a.
'    SaveSetting "GesPet", "Export01", "chkHorasReales", IIf(chkHorasReales.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkHorasRealesOtros", IIf(chkHorasRealesOtros.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkHorasRealesSector", IIf(chkHorasRealesSector.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkHorasRealesGrupo", IIf(chkHorasRealesGrupo.Value = 1, "SI", "NO")
'    '}
'    '{ add -005- a.
'    SaveSetting "GesPet", "Export01", "CHKpcf", IIf(CHKpcf.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKseguimN", IIf(CHKseguimN.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "CHKseguimT", IIf(CHKseguimT.Value = 1, "SI", "NO")
'    '}
'    '{ add -006- a.
'    SaveSetting "GesPet", "Export01", "chkFechaProduccion", IIf(chkFechaProduccion.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkFechaSuspension", IIf(chkFechaSuspension.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkMotivoSuspension", IIf(chkMotivoSuspension.Value = 1, "SI", "NO")
'    SaveSetting "GesPet", "Export01", "chkObservEstado", IIf(chkObservEstado.Value = 1, "SI", "NO")
'    '}
'    SaveSetting "GesPet", "Export01", "chkPetRegulatorio", IIf(chkPetRegulatorio.Value = 1, "SI", "NO")     ' add -008- a.
'    SaveSetting "GesPet", "Export01", "chkPetConfHomo", IIf(chkPetConfHomo.Value = 1, "SI", "NO")     ' add -009- a.
'    SaveSetting "GesPet", "Export01", "chkPetProyectoIDM", IIf(chkPetProyectoIDM.Value = 1, "SI", "NO")     ' add -010- a.
'End Sub

