VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmHEDT2Shell 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Definir COPYs para el DSN"
   ClientHeight    =   6900
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10140
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHEDT2Shell.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   10140
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraOtro 
      Height          =   1095
      Left            =   3600
      TabIndex        =   20
      Top             =   5760
      Width           =   5055
   End
   Begin VB.Frame fraDatos 
      Caption         =   " Modo "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      TabIndex        =   13
      Top             =   4560
      Width           =   8535
      Begin AT_MaskText.MaskText txtIdCpy 
         Height          =   300
         Left            =   840
         TabIndex        =   0
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtRutina 
         Height          =   300
         Left            =   840
         TabIndex        =   2
         Top             =   720
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   300
         Left            =   3000
         TabIndex        =   1
         Top             =   360
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   529
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   19
         Top             =   420
         Width           =   525
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Rutina"
         Height          =   180
         Index           =   3
         Left            =   120
         TabIndex        =   18
         Top             =   780
         Width           =   480
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         Height          =   180
         Index           =   8
         Left            =   2040
         TabIndex        =   17
         Top             =   420
         Width           =   900
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   6855
      Left            =   8760
      TabIndex        =   7
      Top             =   0
      Width           =   1335
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   500
         Left            =   80
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n actual"
         Top             =   5730
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   80
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   6240
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   500
         Left            =   80
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Copy seleccionado"
         Top             =   5220
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   500
         Left            =   80
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el Copy seleccionado"
         Top             =   4710
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   500
         Left            =   80
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Agregar un nuevo Copy"
         Top             =   4200
         Width           =   1170
      End
      Begin VB.CommandButton cmdCampos 
         Caption         =   "Campos"
         Height          =   500
         Left            =   80
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Accede a la pantalla para definir los Campos del Copy seleccionado"
         Top             =   160
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4455
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   7858
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraAudit 
      Height          =   1095
      Left            =   120
      TabIndex        =   14
      Top             =   5760
      Width           =   3375
      Begin AT_MaskText.MaskText txtUltMod 
         Height          =   300
         Left            =   1800
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   660
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DataType        =   3
         SpecialFeatures =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUsuario 
         Height          =   300
         Left            =   1800
         TabIndex        =   3
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Usuario que realiz� �ltima modificaci�n"
         Height          =   360
         Index           =   5
         Left            =   120
         TabIndex        =   16
         Top             =   180
         Width           =   1560
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de �ltima modificaci�n"
         Height          =   420
         Index           =   4
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   1695
         WordWrap        =   -1  'True
      End
   End
End
Attribute VB_Name = "frmHEDT2Shell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.10.2009 - Se agrega el manejo de formulario para perfiles de Administradores de Seg. Inform�tica
' -002- a. FJS 12.11.2009 - Se agregan columnas �tiles a la grilla.
' -003- a. FJS 12.01.2011 - Nuevo: nuevas funcionalidades para Seguridad Inform�tica.

Option Explicit

Private Const COL_DSNID = 0
Private Const COL_CPYID = 1
Private Const COL_CPYDSC = 2
Private Const COL_CPYNOMRUT = 3
Private Const COL_CPYFEULT = 4
Private Const COL_CPYUSER = 5
Private Const COL_ESTADO = 6
Private Const COL_ESTADO2 = 7
Private Const TOTAL_COLS = 8

Public sDSNId, sCpyId As String

Dim bActualizar As Boolean
Dim sOpcionSeleccionada As String
Dim inCarga As Boolean

Private Sub Form_Load()
    Call HabilitarBotones(0, "NO")
    Call Inicializar
    Call Cargar_Grilla
    Call Status("Listo.")
End Sub

Private Sub Inicializar()
    Me.Top = 360
    Me.Left = 360
    Call LockProceso(True)
    bActualizar = False
    DoEvents
    Call setHabilCtrl(txtUsuario, "DIS")
    Call setHabilCtrl(txtUltMod, "DIS")
    Call IniciarScroll(grdDatos)
    Call LockProceso(False)
End Sub

Private Sub Inicializar_Grilla()
    Call Puntero(True)
    With grdDatos
        .Visible = False
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Clear
        .cols = TOTAL_COLS
        .Rows = 1
        .TextMatrix(0, COL_DSNID) = "DSN": .ColWidth(COL_DSNID) = 1000
        .TextMatrix(0, COL_CPYID) = "C�digo": .ColWidth(COL_CPYID) = 1000
        .TextMatrix(0, COL_CPYDSC) = "Descripci�n": .ColWidth(COL_CPYDSC) = 3000
        .TextMatrix(0, COL_CPYNOMRUT) = "Rutina": .ColWidth(COL_CPYNOMRUT) = 1000
        .TextMatrix(0, COL_CPYFEULT) = "Ult. mod.": .ColWidth(COL_CPYFEULT) = 1800
        .TextMatrix(0, COL_CPYUSER) = "Usuario": .ColWidth(COL_CPYUSER) = 1000
        .TextMatrix(0, COL_ESTADO) = "": .ColWidth(COL_ESTADO) = 0
        .TextMatrix(0, COL_ESTADO2) = "": .ColWidth(COL_ESTADO2) = 0
        '.TextMatrix(0, 7) = "": .ColWidth(7) = 0
        '.BackColorBkg = Me.BackColor
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub Cargar_Grilla()
    Call Inicializar_Grilla
    With grdDatos
        If sp_GetHEDT002(sDSNId, Null, Null) Then  ' upd -003- a.
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, COL_DSNID) = ClearNull(aplRST.Fields!dsn_id)
                .TextMatrix(.Rows - 1, COL_CPYID) = ClearNull(aplRST.Fields!cpy_id)
                .TextMatrix(.Rows - 1, COL_CPYDSC) = ClearNull(aplRST.Fields!cpy_dsc)
                .TextMatrix(.Rows - 1, COL_CPYNOMRUT) = ClearNull(aplRST.Fields!cpy_nomrut)
                .TextMatrix(.Rows - 1, COL_CPYFEULT) = ClearNull(aplRST.Fields!cpy_feult)
                .TextMatrix(.Rows - 1, COL_CPYUSER) = ClearNull(aplRST.Fields!cpy_userid)
                .TextMatrix(.Rows - 1, COL_ESTADO2) = ClearNull(aplRST.Fields!Estado)     ' add -002- a.
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .Visible = True
    End With
    Call MostrarSeleccion
    Call Puntero(False)
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Call HabilitarBotones(0)
End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertHEDT002(sDSNId, txtIdCpy, txtDescripcion, txtRutina) Then
                    bActualizar = True
                    Call HabilitarBotones(0)
                    Call cmdCampos_Click
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdateHEDT002(frmHEDT1ShellDyD.sDSNId, txtIdCpy, txtDescripcion, txtRutina) Then  ' upd -003- a.
                    bActualizar = True
                    With grdDatos
                        If .RowSel > 0 Then
                            If sp_GetHEDT002(sDSNId, txtIdCpy, Null) Then
                                .TextMatrix(.RowSel, COL_DSNID) = ClearNull(aplRST.Fields!dsn_id)
                                .TextMatrix(.RowSel, COL_CPYID) = ClearNull(aplRST.Fields!cpy_id)
                                .TextMatrix(.RowSel, COL_CPYDSC) = ClearNull(aplRST.Fields!cpy_dsc)
                                .TextMatrix(.RowSel, COL_CPYNOMRUT) = ClearNull(aplRST.Fields!cpy_nomrut)
                                .TextMatrix(.RowSel, COL_CPYFEULT) = ClearNull(aplRST.Fields!cpy_feult)
                                .TextMatrix(.RowSel, COL_CPYUSER) = ClearNull(aplRST.Fields!cpy_userid)
                                .TextMatrix(.RowSel, COL_ESTADO2) = ClearNull(aplRST.Fields!Estado)       ' add -002- a.
                            End If
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If MsgBox("�Esta seguro de eliminar definitivamente el COPY de la base?", vbQuestion + vbYesNo, "Eliminar COPY") = vbYes Then
                If sp_DeleteHEDT002(sDSNId, txtIdCpy) Then     ' upd -003- a.
                    bActualizar = True
                    Call HabilitarBotones(0)
                    Call Cargar_Grilla
                End If
            End If
    End Select
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Len(txtIdCpy) > 0 Then
    
    End If
End Function

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdCampos.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            ' Controles de datos
            Call setHabilCtrl(txtIdCpy, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtUltMod, "DIS")
            Call setHabilCtrl(txtUsuario, "DIS")
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call Cargar_Grilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdCampos.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    ' Controles de datos
                    Call setHabilCtrl(txtIdCpy, "NOR")
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(txtRutina, "NOR")
                    fraDatos.Caption = " AGREGAR "
                    txtIdCpy = ""
                    txtDescripcion = ""
                    txtRutina = ""
                    txtUsuario = ""
                    txtUltMod = ""
                    fraDatos.Enabled = True
                    txtIdCpy.SetFocus
                Case "M"
                    ' Controles de datos
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(txtRutina, "NOR")
                    fraDatos.Caption = " MODIFICAR "
                    txtIdCpy.Enabled = False
                    fraDatos.Enabled = True
                    txtDescripcion.SetFocus
                Case "E"
                    fraDatos.Caption = " ELIMINAR "
                    fraDatos.Enabled = False
            End Select
        '{ add -001- a.
        Case 2
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdCampos.Enabled = True
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            ' Controles de datos
            Call setHabilCtrl(txtIdCpy, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtUltMod, "DIS")
            Call setHabilCtrl(txtUsuario, "DIS")
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call Cargar_Grilla
            Else
                Call MostrarSeleccion
            End If
        '}
    End Select
    Call LockProceso(False)
End Sub

Private Function MostrarSeleccion()
    inCarga = True
    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, COL_DSNID) <> "" Then
                txtIdCpy = ClearNull(.TextMatrix(.RowSel, COL_CPYID))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, COL_CPYDSC))
                txtRutina = ClearNull(.TextMatrix(.RowSel, COL_CPYNOMRUT))
                txtUltMod.DateValue = ClearNull(.TextMatrix(.RowSel, COL_CPYFEULT)): txtUltMod = txtUltMod.DateValue
                txtUsuario = ClearNull(.TextMatrix(.RowSel, COL_CPYUSER))
                If .TextMatrix(.RowSel, COL_ESTADO2) = "A" Then
                    Call setHabilCtrl(cmdAgregar, "NOR")
                    Call setHabilCtrl(cmdModificar, "NOR")
                    Call setHabilCtrl(cmdEliminar, "NOR")
                Else
                    Call setHabilCtrl(cmdAgregar, "DIS")
                    Call setHabilCtrl(cmdModificar, "DIS")
                    Call setHabilCtrl(cmdEliminar, "DIS")
                End If
            End If
        End If
    End With
    inCarga = False
End Function

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            frmHEDT1ShellDyD.CargarGrilla
            Unload Me
    End Select
    LockProceso (False)
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub cmdCampos_Click()
    If Len(txtIdCpy) > 0 Then
        frmHEDT3Shell.sDSNId = sDSNId
        frmHEDT3Shell.sCpyId = txtIdCpy
        frmHEDT3Shell.Show
    Else
        MsgBox "Debe seleccionar un COPY para definir los Campos del mismo.", vbExclamation + vbOKOnly, "Sin COPY seleccionado"
    End If
End Sub
