VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmHEDT1Shell 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cat�logo de DSN para procesos de enmascaramiento de datos"
   ClientHeight    =   7380
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10260
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHEDT1Shell.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7380
   ScaleWidth      =   10260
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraBotonera 
      Height          =   7335
      Left            =   8760
      TabIndex        =   24
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Informe"
         Height          =   500
         Left            =   80
         TabIndex        =   43
         Top             =   2560
         Width           =   1290
      End
      Begin VB.CommandButton cmdVisar 
         Caption         =   "Visar"
         Height          =   500
         Left            =   80
         TabIndex        =   31
         TabStop         =   0   'False
         ToolTipText     =   "Visa el archivo DSN seleccionado"
         Top             =   2080
         Visible         =   0   'False
         Width           =   1290
      End
      Begin VB.CommandButton cmdMarcar 
         Caption         =   "Marcar como ya utilizado"
         Height          =   500
         Left            =   80
         TabIndex        =   33
         TabStop         =   0   'False
         ToolTipText     =   "Marca el archivo seleccionado como ya utilizado"
         Top             =   1600
         Width           =   1290
      End
      Begin VB.CommandButton cmdCopiar 
         Caption         =   "Copiar nombres"
         Height          =   500
         Left            =   80
         TabIndex        =   39
         TabStop         =   0   'False
         ToolTipText     =   "Puede copiar en el Portapapeles los nombres de los archivos seleccionados para pegar donde necesite"
         Top             =   1130
         Width           =   1290
      End
      Begin VB.CheckBox chkBuscar 
         Caption         =   "B&uscar"
         Height          =   500
         Left            =   80
         Style           =   1  'Graphical
         TabIndex        =   34
         TabStop         =   0   'False
         ToolTipText     =   "Despliega un buscador secuencial de nombres de DSN que filtra la grilla a medida que escribe"
         Top             =   640
         Width           =   1290
      End
      Begin VB.CheckBox chkSoloPendientes 
         Caption         =   "Solo pendientes de visado"
         Height          =   495
         Left            =   120
         TabIndex        =   32
         ToolTipText     =   "Con esta opci�n seleccionada, solo se ver�n en la grilla los archivos pendientes de visado"
         Top             =   3840
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.CommandButton cmdCOPYS 
         Caption         =   "COPYs"
         Height          =   500
         Left            =   80
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Accede a la pantalla para definir los Copys del DSN seleccionado"
         Top             =   160
         Width           =   1290
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   500
         Left            =   80
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Agregar un nuevo archivo"
         Top             =   4680
         Width           =   1290
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   500
         Left            =   80
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el archivo seleccionado"
         Top             =   5190
         Width           =   1290
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   500
         Left            =   80
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el archivo seleccionado"
         Top             =   5700
         Width           =   1290
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   80
         TabIndex        =   26
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   6720
         Width           =   1290
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   500
         Left            =   80
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n actual"
         Top             =   6210
         Width           =   1290
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3855
      Left            =   120
      TabIndex        =   23
      Top             =   120
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   6800
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraBusqueda 
      Enabled         =   0   'False
      Height          =   615
      Left            =   120
      TabIndex        =   20
      Top             =   60
      Width           =   8535
      Begin AT_MaskText.MaskText txtBuscarDSNNombre 
         Height          =   300
         Left            =   1440
         TabIndex        =   21
         Top             =   195
         Width           =   6975
         _ExtentX        =   12303
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin VB.OptionButton optWildCardAligment 
         Caption         =   "DER"
         Height          =   255
         Index           =   2
         Left            =   7680
         TabIndex        =   38
         Top             =   240
         Width           =   735
      End
      Begin VB.OptionButton optWildCardAligment 
         Caption         =   "MEDIO"
         Height          =   255
         Index           =   1
         Left            =   6720
         TabIndex        =   37
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optWildCardAligment 
         Caption         =   "IZQ"
         Height          =   255
         Index           =   0
         Left            =   6000
         TabIndex        =   36
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Buscar nombre de archivo :"
         Height          =   360
         Index           =   9
         Left            =   120
         TabIndex        =   22
         Top             =   165
         Width           =   1185
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraDatos 
      Caption         =   " Modo "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1660
      Left            =   120
      TabIndex        =   9
      Top             =   4560
      Width           =   8535
      Begin VB.CommandButton cmdAppSearch 
         Caption         =   "..."
         Height          =   300
         Left            =   3600
         TabIndex        =   18
         Top             =   1080
         Width           =   300
      End
      Begin VB.CheckBox chkHEDT1Enmascarable 
         Caption         =   "Datos enmascarables"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   1200
         Width           =   1575
      End
      Begin AT_MaskText.MaskText txtIdArch 
         Height          =   300
         Left            =   840
         TabIndex        =   0
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAppId 
         Height          =   300
         Left            =   3000
         TabIndex        =   5
         Top             =   1080
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNombre 
         Height          =   300
         Left            =   3000
         TabIndex        =   1
         Top             =   360
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   529
         MaxLength       =   50
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtRutina 
         Height          =   300
         Left            =   840
         TabIndex        =   2
         Top             =   720
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAppName 
         Height          =   300
         Left            =   3960
         TabIndex        =   6
         Top             =   1080
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   300
         Left            =   3000
         TabIndex        =   3
         Top             =   720
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   529
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         Height          =   180
         Index           =   8
         Left            =   2040
         TabIndex        =   14
         Top             =   780
         Width           =   900
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Rutina"
         Height          =   180
         Index           =   3
         Left            =   120
         TabIndex        =   13
         Top             =   780
         Width           =   480
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   180
         Index           =   2
         Left            =   2040
         TabIndex        =   12
         Top             =   420
         Width           =   585
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Aplicativo"
         Height          =   180
         Index           =   1
         Left            =   2040
         TabIndex        =   11
         Top             =   1140
         Width           =   765
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   420
         Width           =   525
      End
   End
   Begin VB.Frame fraVisado 
      Caption         =   " Seguridad Inform�tica "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   120
      TabIndex        =   15
      Top             =   6240
      Width           =   8535
      Begin VB.ComboBox cmbDSNEstado 
         Height          =   300
         Left            =   720
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   240
         Width           =   1815
      End
      Begin AT_MaskText.MaskText txtFechaVB 
         Height          =   300
         Left            =   6960
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   660
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DataType        =   3
         SpecialFeatures =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUsuarioVB 
         Height          =   300
         Left            =   6960
         TabIndex        =   7
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText txtMotivoRechazo 
         Height          =   660
         Left            =   2640
         TabIndex        =   42
         Top             =   240
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   1164
         MaxLength       =   255
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   180
         Left            =   120
         TabIndex        =   44
         Top             =   720
         Width           =   510
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         Height          =   180
         Index           =   4
         Left            =   120
         TabIndex        =   41
         Top             =   300
         Width           =   525
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Visado por"
         Height          =   180
         Index           =   6
         Left            =   5880
         TabIndex        =   17
         Top             =   300
         Width           =   810
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Fecha visado"
         Height          =   180
         Index           =   7
         Left            =   5880
         TabIndex        =   16
         Top             =   720
         Width           =   990
      End
   End
   Begin VB.Label lblMensaje 
      Caption         =   $"frmHEDT1Shell.frx":014A
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   4080
      Width           =   8535
   End
   Begin VB.Label lblBusqueda 
      ForeColor       =   &H000000C0&
      Height          =   375
      Left            =   120
      TabIndex        =   35
      Top             =   4080
      Width           =   8535
   End
End
Attribute VB_Name = "frmHEDT1Shell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.10.2009 - Se agrega el manejo de formulario para perfiles de Administradores de Seg. Inform�tica
' -002- a. FJS 28.10.2009 - Se agrega un manejador global para el modo de utilizaci�n del ABM.
' -003- a. FJS 28.10.2009 - Se inhabilita la posibilidad de agregar el c�digo manualmente.
' -004- a. FJS 03.11.2009 - Se agrega la opci�n de visar simultaneamente m�s de un archivo.
' -005- a. FJS 03.11.2009 - Se agrega un bot�n para realizar una b�squeda en la grilla por nombre de DSN.
' -005- b. FJS 05.11.2009 - Se toma el nombre del estado del SP, no se esp�cifica en runtime.
' -005- c. FJS 16.11.2009 - Se cambia por el nombre de los campos directos del SP.
' -005- d. FJS 17.11.2009 - Se declara un Recordset auxiliar porque se generan problemas al reutilizar el aplRST y filtrar (si agregu� o modifiqu� datos en medio).
' -006- a. FJS 20.11.2009 - Si se trata de modificar un archivo no enviado, permito editar el nombre del archivo. Si ya ha sido enviado o esta en proceso de envio, no lo permito.
' -006- b. FJS 20.11.2009 - Si el archivo se encuentra enviado en proceso, no puede modificarse nada.
' -006- c. FJS 20.11.2009 - Se el archivo ya fu� visado por SI, no puede editarse.
' -006- d. FJS 23.11.2009 - Se inhabilita la posibilidad de eliminar un DSN si ya ha sido enviado a HOST.
' -007- a. FJS 16.12.2009 - Se agrega un indicador para aquellos archivos declarados con datos enmascarables inv�lidos (porque le falta copys y/o campos).
' -007- b. FJS 16.12.2009 - Se agrega la opci�n de "desvisar" un archivo visado.
' -007- c. FJS 16.12.2009 - Se agrega funcionalidad para SI.
' -008- a. FJS 17.06.2010 - Nuevas funcionalidades para SI: se elimina el frame con los datos de auditor�a. Se cambia el checkbox del visado por un combo (para soportar nuevo estado de rechazado).
'                           Se agrega leyenda para que SI pueda explicar porque rechaza un DSN. Adem�s, envio autom�tico de email al propietario de un archivo para que conozca los motivos del rechazo.
' -008- b. FJS 17.06.2010 - Se cambia el control checkbox por un combobox para el estado de visado. Ahora se contempan 3 estado: pendiente de visado, visado y rechazado.
' -008- c. FJS 17.06.2010 - Se agrega un nuevo campo de texto para que SI informe el motivo del rechazo al DSN.
' -008- d. FJS 17.06.2010 - Se modifica la manera de actualizar los datos de visado y dem�s para los perfiles de SI.

Option Explicit

Public sDSNId As String

Dim inCarga As Boolean
Dim sOpcionSeleccionada As String
Dim bSegInf As Boolean              ' add -001- a.
Dim cLastSearchPattern As String    ' add -005- a.
Dim iCont As Long

'{ add -001- a.
Dim lRowSel_Ini As Long
Dim lRowSel_Fin As Long
'}

Dim auxRST As New ADODB.Recordset   ' add -005- d.

Private Sub Form_Load()
    inCarga = True
    '{ add -001- a. Si el usuario tiene perfil de Adm. de Seg. Inform�tica, no puede realizar ninguna de las otras
    '               tareas respecto del cat�logo. Solo puede visar elementos.
    If InPerfil("ASEG") Then
        bSegInf = True
        HabilitarBotones 2
    Else
        bSegInf = False
        HabilitarBotones 0, "NO"
    End If
    '}
    Inicializar
    Cargar_Grilla
    'HabilitarBotones 0, "NO"   ' del -001- a.
    inCarga = False
    Call Status("Listo.")
End Sub

Private Sub Inicializar()
    Call Status("Inicializando...")
    Me.Top = 0
    Me.Left = 0
    LockProceso (True)
    
    'chkSegInfVB.BackColor = Me.BackColor   ' del -008- b.
    DoEvents
    'inCarga = True
    Call setHabilCtrl(txtIdArch, "DIS")     ' add -003- a.
    Call setHabilCtrl(txtAppName, "DIS")
    'Call setHabilCtrl(txtUsuario, "DIS")   ' del -008- a.
    'Call setHabilCtrl(txtUltMod, "DIS")    ' del -008- a.
    'Call setHabilCtrl(chkSegInfVB, "DIS")  ' del -008- b.
    Call setHabilCtrl(txtUsuarioVB, "DIS")
    Call setHabilCtrl(txtFechaVB, "DIS")
    Call setHabilCtrl(cmbDSNEstado, "DIS")  ' add -008- b.
    Call setHabilCtrl(txtMotivoRechazo, "DIS")  ' add -008- b.
    'Call setHabilCtrl(txtUsuarioNombre, "DIS")  ' add -007- c. ' del -008- a.
    '{ add -008- b.
    With cmbDSNEstado
        .Clear
        .AddItem "Pendiente" & Space(100) & "||N"
        .AddItem "Rechazado" & Space(100) & "||R"
        .AddItem "Visado" & Space(100) & "||S"
        .ListIndex = 0
    End With
    '}
    modUser32.IniciarScroll grdDatos
    LockProceso (False)
End Sub

Private Sub Inicializar_Grilla()
    With grdDatos
        .Visible = False
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .Clear
        .Cols = 17  ' upd -007- a. - Antes 14   ' upd -008- b. Antes 15 ' upd -008- c. Antes 16.
        .Rows = 1
        .TextMatrix(0, 0) = "C�digo"
        .TextMatrix(0, 1) = "Nombre del archivo"
        .TextMatrix(0, 2) = "ENM."
        .TextMatrix(0, 3) = "Rutina"
        .TextMatrix(0, 4) = "Ult. mod."
        .TextMatrix(0, 5) = "Propietario"       ' upd -008- a. Antes Usuario, ahora Propietario
        .TextMatrix(0, 6) = "Visado"
        .TextMatrix(0, 7) = "Por"
        .TextMatrix(0, 8) = "Fch. Visado"
        .TextMatrix(0, 9) = "Aplicativo"
        .TextMatrix(0, 10) = "Nombre del aplicativo"
        .TextMatrix(0, 11) = "Descripci�n del archivo"
        .TextMatrix(0, 12) = "Estado"
        .TextMatrix(0, 13) = ""             ' Estado real
        .TextMatrix(0, 14) = "Validez"      ' add -007- a. - Validez del archivo declarado
        .TextMatrix(0, 15) = "dsn_vb"                   ' add -008- b.
        .TextMatrix(0, 16) = "Motivo de rechazo"        ' add -008- c.
        .ColWidth(0) = 1000
        .ColWidth(1) = 4000
        .ColWidth(2) = 800
        .ColWidth(3) = 1000
        .ColWidth(4) = 1700
        .ColWidth(5) = 1500                     ' upd -008- a. Antes 1000
        .ColWidth(6) = 1000
        .ColWidth(7) = 1200
        .ColWidth(8) = 1700
        .ColWidth(9) = 1000
        .ColWidth(10) = 2000
        .ColWidth(11) = 4000
        .ColWidth(12) = 1600
        .ColWidth(13) = 0               ' Estado real
        .ColWidth(14) = 800             ' add -007- a. - Validez del archivo declarado
        .ColWidth(15) = 0               ' add -008- b.
        .ColWidth(16) = 2000            ' add -008- c.
        .BackColorBkg = Me.BackColor
    End With
    CambiarEfectoLinea grdDatos, prmGridEffectFontBold
End Sub

Private Sub Cargar_Grilla()
    Inicializar_Grilla
    '{ add -001- a.
    If bSegInf Then     ' SEGURIDAD INFORMATICA
        If sp_GetHEDT101(Null, IIf(chkSoloPendientes.Value = 0, Null, "N|R|")) Then
            Call puntero(True)
            With grdDatos
                Do While Not aplRST.EOF
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST.Fields!dsn_id)
                    .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST.Fields!dsn_nom)
                    '.TextMatrix(.Rows - 1, 2) = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "N", "No", "Si")  ' del -005- c.
                    .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST.Fields!dsn_enmas)                          ' add -005- c.
                    .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST.Fields!dsn_nomrut)
                    .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST.Fields!dsn_feult)
                    .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST.Fields!nom_usuario)                         ' upd -008- a. Antes, dsn_userid, ahora nom_usuario
                    '.TextMatrix(.Rows - 1, 6) = IIf(ClearNull(aplRST.Fields!dsn_vb) = "N", "No", "Si")     ' del -005- c.
                    'If .TextMatrix(.Rows - 1, 0) = "A0000011" Then
                    '    Stop
                    'End If
                    .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST.Fields!dsn_vb)                             ' add -005- c.
                    .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST.Fields!nom_vb_usuario)                     ' upd -008- a. Antes dsn_vb_userid, ahora nom_vb_usuario
                    .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST.Fields!dsn_vb_fe)
                    .TextMatrix(.Rows - 1, 9) = ClearNull(aplRST.Fields!app_id)
                    .TextMatrix(.Rows - 1, 10) = ClearNull(aplRST.Fields!app_name)
                    .TextMatrix(.Rows - 1, 11) = ClearNull(aplRST.Fields!dsn_desc)
                    '{ del -005- b.
                    'Select Case ClearNull(auxRST.Fields!Estado)
                    '    Case "A"
                    '        .TextMatrix(.Rows - 1, 13) = "A�n sin utilizar"
                    '    Case "B"
                    '        .TextMatrix(.Rows - 1, 13) = "Ya utilizado"
                    '    Case "C"
                    '        .TextMatrix(.Rows - 1, 13) = "Ya utilizado"
                    'End Select
                    '}
                    .TextMatrix(.Rows - 1, 12) = ClearNull(aplRST.Fields!nom_estado)    ' add -005- b.
                    .TextMatrix(.Rows - 1, 13) = ClearNull(aplRST.Fields!Estado)        ' Estado real
                    .TextMatrix(.Rows - 1, 14) = ClearNull(aplRST.Fields!valido)        ' add -007- a. - Validez del archivo
                    .TextMatrix(.Rows - 1, 15) = ClearNull(aplRST.Fields!dsn_vb_id)        ' add -008- b.
                    .TextMatrix(.Rows - 1, 16) = ClearNull(aplRST.Fields!dsn_txt)        ' add -008- c.
                    aplRST.MoveNext
                    DoEvents
                Loop
                .Visible = True
            End With
        End If
    Else
    '}
        If sp_GetHEDT001(Null, Null) Then   ' NORMAL
            Call puntero(True)
            With grdDatos
                Do While Not aplRST.EOF
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST.Fields!dsn_id)
                    .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST.Fields!dsn_nom)
                    '.TextMatrix(.Rows - 1, 2) = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "N", "No", "Si")  ' del -005- c.
                    .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST.Fields!dsn_enmas)                          ' add -005- c.
                    .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST.Fields!dsn_nomrut)
                    .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST.Fields!dsn_feult)
                    .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST.Fields!nom_usuario)                         ' upd -008- a. Antes, dsn_userid, ahora nom_usuario
                    '.TextMatrix(.Rows - 1, 6) = IIf(ClearNull(aplRST.Fields!dsn_vb) = "N", "No", "Si")     ' del -005- c.
                    .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST.Fields!dsn_vb)                             ' add -005- c.
                    .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST.Fields!nom_vb_usuario)                     ' upd -008- a. Antes dsn_vb_userid, ahora nom_vb_usuario
                    .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST.Fields!dsn_vb_fe)
                    .TextMatrix(.Rows - 1, 9) = ClearNull(aplRST.Fields!app_id)
                    .TextMatrix(.Rows - 1, 10) = ClearNull(aplRST.Fields!app_name)
                    .TextMatrix(.Rows - 1, 11) = ClearNull(aplRST.Fields!dsn_desc)
                    '{ del -005- b.
                    'Select Case ClearNull(auxRST.Fields!Estado)
                    '    Case "A"
                    '        .TextMatrix(.Rows - 1, 13) = "A�n sin utilizar"
                    '    Case "B"
                    '        .TextMatrix(.Rows - 1, 13) = "Ya utilizado"
                    '    Case "C"
                    '        .TextMatrix(.Rows - 1, 13) = "Ya utilizado"
                    'End Select
                    '}
                    .TextMatrix(.Rows - 1, 12) = ClearNull(aplRST.Fields!nom_estado)    ' add -005- b.
                    .TextMatrix(.Rows - 1, 13) = ClearNull(aplRST.Fields!Estado)        ' Estado real
                    .TextMatrix(.Rows - 1, 14) = ClearNull(aplRST.Fields!valido)        ' add -007- a. - Validez del archivo
                    .TextMatrix(.Rows - 1, 15) = ClearNull(aplRST.Fields!dsn_vb_id)        ' add -008- b.
                    .TextMatrix(.Rows - 1, 16) = ClearNull(aplRST.Fields!dsn_txt)        ' add -008- c.
                    aplRST.MoveNext
                    DoEvents
                Loop
                .Visible = True
            End With
        End If
    End If      ' add -001- a.
    If aplRST.State = 1 Then Label1.Caption = aplRST.RecordCount & " dsn(s)."
    Me.Tag = aplRST.RecordCount     ' Guardo el total de registros del cat�logo
    MostrarSeleccion
    Dim i As Long
    With grdDatos
        .FocusRect = flexFocusNone
        .BackColorFixed = Me.BackColor
        .BackColorBkg = Me.BackColor
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .Col = .Cols - 1
        .Row = .Rows - 1
        .Visible = True
    End With
    Set auxRST = aplRST.Clone       ' add -005- d. "Clonamos" el RS original
    Call puntero(False)
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Call HabilitarBotones(0)
End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdModificar_Click()
    If grdDatos.TextMatrix(grdDatos.RowSel, 13) = "En proceso..." Then
        MsgBox "El archivo se encuentra en proceso de actualizaci�n en HOST. No puede ser modificado.", vbExclamation + vbOKOnly, "Actualizando cat�logo HOST"
    Else
        If Not LockProceso(True) Then
            Exit Sub
        End If
        sOpcionSeleccionada = "M"
        Call HabilitarBotones(1, False)
        LockProceso (False)
    End If
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    'txtUsuario = glLOGIN_ID_REEMPLAZO  ' del -008- a.
    '{ del -008- a.
    'txtUltMod.DateValue = Now
    'txtUltMod.Text = txtUltMod.DateValue
    '}
    '{ add -008- d.
    If bSegInf Then
        With grdDatos
            Call Status("Actualizando..."): Screen.MousePointer = vbHourglass
            If Not sp_UpdateHEDT101(Trim(.TextMatrix(.RowSel, 0)), CodigoCombo(cmbDSNEstado, True), txtMotivoRechazo) Then
                MsgBox "Error al procesar " & .RowSel
            Else
                .TextMatrix(.RowSel, 6) = TextoCombo(cmbDSNEstado, CodigoCombo(cmbDSNEstado, True), True)
                .TextMatrix(.RowSel, 7) = glLOGIN_ID_REEMPLAZO
                .TextMatrix(.RowSel, 8) = Now
                .TextMatrix(.RowSel, 14) = Trim(txtMotivoRechazo)
            End If
            Call HabilitarBotones(0)
            Call Status("Listo."): Screen.MousePointer = vbNormal
            Exit Sub
        End With
    End If
    '}
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertHEDT001(txtIdArch, txtNombre, txtDescripcion, IIf(chkHEDT1Enmascarable.Value = 1, "S", "N"), txtRutina, txtAppId) Then
                    '{ del -003- a.
                    'If sp_GetHEDT001(txtIdArch, Null) Then
                    '    With grdDatos
                    '        .Rows = .Rows + 1
                    '        .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST.Fields!dsn_id)
                    '        .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST.Fields!dsn_nom)
                    '        .TextMatrix(.Rows - 1, 2) = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "N", "No", "Si")
                    '        .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST.Fields!dsn_nomrut)
                    '        .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST.Fields!dsn_feult)
                    '        .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST.Fields!dsn_userid)
                    '        .TextMatrix(.Rows - 1, 6) = IIf(ClearNull(aplRST.Fields!dsn_vb) = "N", "No", "Si")
                    '        .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST.Fields!dsn_vb_userid)
                    '        .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST.Fields!dsn_vb_fe)
                    '        .TextMatrix(.Rows - 1, 9) = ClearNull(aplRST.Fields!app_id)
                    '        .TextMatrix(.Rows - 1, 10) = ClearNull(aplRST.Fields!app_name)
                    '        .TextMatrix(.Rows - 1, 11) = ClearNull(aplRST.Fields!dsn_desc)
                    '    End With
                    'End If
                    '}
                    '{ add -003- a.
                    inCarga = True
                    Cargar_Grilla
                    inCarga = False
                    Call GridSeek(Me, grdDatos, 1, txtNombre, True)
                    Call HabilitarBotones(0, "A")
                    If chkHEDT1Enmascarable.Value = 1 Then
                        cmdCOPYS_Click
                    End If
                    '}
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdateHEDT001(txtIdArch, txtNombre, txtDescripcion, IIf(chkHEDT1Enmascarable.Value = 1, "S", "N"), txtRutina, txtAppId) Then
                    With grdDatos
                        If .RowSel > 0 Then
                            If sp_GetHEDT001(txtIdArch, Null) Then
                                .TextMatrix(.RowSel, 1) = ClearNull(aplRST.Fields!dsn_nom)
                                '.TextMatrix(.RowSel, 2) = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "N", "No", "Si")    ' del -005- c.
                                .TextMatrix(.RowSel, 2) = ClearNull(aplRST.Fields!dsn_enmas)                            ' add -005- c.
                                .TextMatrix(.RowSel, 3) = ClearNull(aplRST.Fields!dsn_nomrut)
                                .TextMatrix(.RowSel, 4) = ClearNull(aplRST.Fields!dsn_feult)
                                .TextMatrix(.RowSel, 5) = ClearNull(aplRST.Fields!nom_usuario)          ' upd -008- c. Antes dsn_userid, ahora nom_usuario
                                .TextMatrix(.RowSel, 6) = ClearNull(aplRST.Fields!dsn_vb)
                                .TextMatrix(.RowSel, 7) = ClearNull(aplRST.Fields!nom_vb_usuario)        ' upd -008- c. Antes dsn_vb_userid, ahora nom_vb_usuario
                                .TextMatrix(.RowSel, 8) = ClearNull(aplRST.Fields!dsn_vb_fe)
                                .TextMatrix(.RowSel, 9) = ClearNull(aplRST.Fields!app_id)
                                .TextMatrix(.RowSel, 10) = ClearNull(aplRST.Fields!app_name)
                                .TextMatrix(.RowSel, 11) = ClearNull(aplRST.Fields!dsn_desc)
                                .TextMatrix(.RowSel, 12) = ClearNull(aplRST.Fields!nom_estado)
                                .TextMatrix(.RowSel, 13) = ClearNull(aplRST.Fields!Estado)        ' C�digo de estado
                                .TextMatrix(.RowSel, 14) = ClearNull(aplRST.Fields!valido)        ' add -007- a. - Validez del archivo
                                .TextMatrix(.RowSel, 16) = ClearNull(aplRST.Fields!dsn_txt)        ' add -008- c.
                            End If
                        End If
                    End With
                    '{ add -005- e. Actualizo el recordset
'                    With auxRST
'                        ' Se posiciona sobre el registro modificado
'                        .Find "dsn_nom = '" & Trim(grdDatos.TextMatrix(grdDatos.RowSel, 1)) & "'"
'                        ' Se actualizan los datos que se han modificado
'                        .Fields!dsn_nom = grdDatos.TextMatrix(grdDatos.RowSel, 1)
'                        .Fields!dsn_enmas = Left(grdDatos.TextMatrix(grdDatos.RowSel, 2), 1)
'                        .Fields!dsn_nomrut = grdDatos.TextMatrix(grdDatos.RowSel, 3)
'                        .Fields!app_id = grdDatos.TextMatrix(grdDatos.RowSel, 9)
'                        .Fields!app_name = grdDatos.TextMatrix(grdDatos.RowSel, 10)
'                        .Fields!dsn_desc = grdDatos.TextMatrix(grdDatos.RowSel, 11)
'                        .Fields!nom_estado = grdDatos.TextMatrix(grdDatos.RowSel, 12)
'                        .Fields!Estado = grdDatos.TextMatrix(grdDatos.RowSel, 13)
'                        .Update
'                    End With
                    '}
                    Call HabilitarBotones(0, False)
                    '{ add -00x-
                    If chkHEDT1Enmascarable.Value = 1 Then
                        cmdCOPYS_Click
                    End If
                    '}
                End If
            End If
        Case "E"
            '{ add -006- d.
            If grdDatos.TextMatrix(grdDatos.RowSel, 13) = "A" Then
                If MsgBox("�Esta seguro de eliminar definitivamente el DSN de la base?", vbQuestion + vbYesNo, "Eliminar DSN") = vbYes Then
                    If sp_DeleteHEDT001(txtIdArch) Then
                        Call HabilitarBotones(0)
                    End If
                End If
            Else
                MsgBox "No puede eliminar este archivo, porque ya fu� utilizado.", vbExclamation + vbOKOnly, "Eliminar DSN"
                Call HabilitarBotones(0)
            End If
            '}
            '{ del -006- d.
            'If MsgBox("�Esta seguro de eliminar definitivamente el DSN de la base?", vbQuestion + vbYesNo, "Eliminar DSN") = vbYes Then
            '    If sp_DeleteHEDT001(txtIdArch) Then
            '        Call HabilitarBotones(0)
            '    End If
            'End If
            '}
    End Select
    LockProceso (False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Len(Trim(txtNombre)) = 0 Then
        MsgBox "Debe especificar el nombre del archivo.", vbExclamation + vbOKOnly, "Nombre de archivo"
        CamposObligatorios = False
        Exit Function
    End If
    If Len(Trim(txtNombre)) > 50 Then
        MsgBox "la longitud del nombre del archivo no debe ser mayor a 50 posiciones.", vbExclamation + vbOKOnly, "Nombre de archivo"
        CamposObligatorios = False
        Exit Function
    End If
    If Len(Trim(txtDescripcion)) = 0 Then
        MsgBox "Debe especificar una descripci�n para el archivo.", vbExclamation + vbOKOnly, "Descripci�n de archivo"
        CamposObligatorios = False
        Exit Function
    End If
End Function

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            '{ add -001- a.
            'cmdVisar.Visible = False       ' del -008- d.
            cmdCopiar.Visible = True
            chkSoloPendientes.Visible = False
            '}
            '{ add -00x-
            chkBuscar.Enabled = True
            cmdCopiar.Enabled = True
            '}
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdCOPYS.Enabled = True
            cmdEliminar.Enabled = True
            cmdMarcar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            cmdImprimir.Visible = IIf(InPerfil("ASEG"), True, False)    ' add -008- d. Antes: cmdImprimir.Visible = False
            ' Controles de datos
            Call setHabilCtrl(txtIdArch, "DIS")
            Call setHabilCtrl(txtNombre, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtAppId, "DIS")
            Call setHabilCtrl(txtAppName, "DIS")
            Call setHabilCtrl(cmdAppSearch, "DIS")
            Call setHabilCtrl(chkHEDT1Enmascarable, "DIS")
            '{ add -008- d.
            Call setHabilCtrl(cmbDSNEstado, "DIS")
            Call setHabilCtrl(txtMotivoRechazo, "DIS")
            '}
            'Call setHabilCtrl(txtUltMod, "DIS")        ' del -008- a.
            'Call setHabilCtrl(txtUsuario, "DIS")       ' del -008- a.
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call Cargar_Grilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdMarcar.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdImprimir.Visible = False          ' add -008- d.
            '{ add -00x-
            chkBuscar.Enabled = False
            cmdCopiar.Enabled = False
            '}
            cmdCOPYS.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    ' Controles de datos
                    'Call setHabilCtrl(txtIdArch, "NOR")    ' del -003- a.
                    Call setHabilCtrl(txtNombre, "NOR")
                    Call setHabilCtrl(txtRutina, "NOR")
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(txtAppId, "NOR")
                    Call setHabilCtrl(chkHEDT1Enmascarable, "NOR")
                    Call setHabilCtrl(cmdAppSearch, "NOR")
                    fraDatos.Caption = " AGREGAR "
                    txtIdArch = ""
                    txtRutina = ""
                    txtAppId = ""
                    txtAppName = ""
                    chkHEDT1Enmascarable.Value = 0
                    txtNombre = ""
                    txtDescripcion = ""
                    'txtUsuario = ""    ' del -008- a.
                    'txtUltMod = ""     ' del -008- a.
                    txtUsuarioVB = ""
                    txtFechaVB.DateValue = ""
                    txtFechaVB = ""
                    fraDatos.Enabled = True
                    txtIdArch.SetFocus  ' del -003- a.
                    txtNombre.SetFocus  ' add -003- a.
                Case "M"
                    ' Controles de datos
                    'Call setHabilCtrl(txtNombre, "NOR")    ' del -006- a.
                    If Not bSegInf Then     ' add -008- d.
                        '{ add -006- a.
                        If grdDatos.TextMatrix(grdDatos.RowSel, 13) = "A" Then
                            Call setHabilCtrl(txtNombre, "NOR")
                        Else
                            Call setHabilCtrl(txtNombre, "DIS")
                        End If
                        '}
                        Call setHabilCtrl(txtRutina, "NOR")
                        Call setHabilCtrl(txtDescripcion, "NOR")
                        Call setHabilCtrl(txtAppId, "NOR")
                        Call setHabilCtrl(cmdAppSearch, "NOR")
                        Call setHabilCtrl(chkHEDT1Enmascarable, "NOR")
                        fraDatos.Caption = " MODIFICAR "
                        txtIdArch.Enabled = False
                        fraDatos.Enabled = True
                        txtNombre.SetFocus
                    '{ add -008- d.
                    Else
                        Call setHabilCtrl(txtNombre, "DIS")
                        Call setHabilCtrl(txtRutina, "DIS")
                        Call setHabilCtrl(txtDescripcion, "DIS")
                        Call setHabilCtrl(txtAppId, "DIS")
                        Call setHabilCtrl(cmdAppSearch, "DIS")
                        Call setHabilCtrl(chkHEDT1Enmascarable, "DIS")
                        Call setHabilCtrl(cmbDSNEstado, "NOR")
                        Call setHabilCtrl(txtMotivoRechazo, "NOR")
                        fraDatos.Caption = " MODIFICAR "
                        txtIdArch.Enabled = False
                        fraDatos.Enabled = True
                        cmbDSNEstado.SetFocus
                    End If
                    '}
                Case "E"
                    fraDatos.Caption = " ELIMINAR "
                    fraDatos.Enabled = False
            End Select
        '{ add -001- a.
        Case 2
            fraDatos.Caption = ""
            grdDatos.Enabled = True
            'cmdVisar.Visible = True            ' del -008- d.
            cmdCopiar.Visible = True
            chkSoloPendientes.Visible = True
            cmdAgregar.Visible = True           ' upd -008- d. Antes False
            cmdAgregar.Enabled = False          ' add -008- d.
            cmdModificar.Visible = True         ' upd -008- d. Antes False
            cmdCOPYS.Enabled = True
            cmdCopiar.Enabled = True
            cmdMarcar.Enabled = False
            cmdEliminar.Visible = False
            cmdConfirmar.Visible = True         ' upd -008- d. Antes False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            cmdImprimir.Visible = True          ' add -008- d.
            ' Controles de datos
            Call setHabilCtrl(txtIdArch, "DIS")
            Call setHabilCtrl(txtNombre, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(cmdAppSearch, "DIS")
            Call setHabilCtrl(txtAppId, "DIS")
            Call setHabilCtrl(txtAppName, "DIS")
            Call setHabilCtrl(chkHEDT1Enmascarable, "DIS")
            '{ add -008- d.
            Call setHabilCtrl(cmbDSNEstado, "NOR")
            Call setHabilCtrl(txtMotivoRechazo, "NOR")
            '}
            'Call setHabilCtrl(txtUltMod, "DIS")    ' del -008- a.
            'Call setHabilCtrl(txtUsuario, "DIS")   ' del -008- a.
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call Cargar_Grilla
            Else
                Call MostrarSeleccion
            End If
        '}
    End Select
    Call LockProceso(False)
End Sub

Public Function MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    If Not inCarga Then
        With grdDatos
            If .RowSel > 0 Then
                If .TextMatrix(.RowSel, 0) <> "" Then
                    txtIdArch = ClearNull(.TextMatrix(.RowSel, 0))
                    txtNombre = ClearNull(.TextMatrix(.RowSel, 1))
                    txtRutina = ClearNull(.TextMatrix(.RowSel, 3))
                    txtDescripcion = ClearNull(.TextMatrix(.RowSel, 11))
                    txtAppId = ClearNull(.TextMatrix(.RowSel, 9))
                    txtAppName = ClearNull(.TextMatrix(.RowSel, 10))
                    chkHEDT1Enmascarable.Value = IIf(Left(ClearNull(.TextMatrix(.RowSel, 2)), 1) = "S", 1, 0)
                    'txtUltMod.DateValue = ClearNull(.TextMatrix(.RowSel, 4)): txtUltMod = txtUltMod.DateValue  ' del -008- a.
                    'txtUsuario = ClearNull(.TextMatrix(.RowSel, 5))    ' del -008- a.
                    '{ add -006- b.
                    If chkBuscar.Value = 0 Then
                        If .TextMatrix(.RowSel, 13) = "B" Then
                            Call setHabilCtrl(cmdModificar, "DIS")
                        Else
                            Call setHabilCtrl(cmdModificar, "NOR")
                        End If
                    End If
                    '}
                    ' Datos de visado por Seguridad Inform�tica
                    'chkSegInfVB = IIf(.TextMatrix(.RowSel, 6) = "No", 0, 1)    ' del -008- b.
                    cmbDSNEstado.ListIndex = PosicionCombo(cmbDSNEstado, ClearNull(.TextMatrix(.RowSel, 15)), True) ' add -008- b.
                    txtUsuarioVB = .TextMatrix(.RowSel, 7)
                    txtFechaVB.DateValue = .TextMatrix(.RowSel, 8): txtFechaVB.Text = txtFechaVB.DateValue
                    txtMotivoRechazo.Text = .TextMatrix(.RowSel, 16)        ' add -008- c.
                    '{ add -006- c.
                    If Not bSegInf Then     ' add -008- d.
                        If .TextMatrix(.RowSel, 15) = "S" Then      ' upd -008- b.
                            Call setHabilCtrl(cmdModificar, "DIS")
                        End If      ' add -008- d.
                    End If      ' add -008- d.
                        'cmdVisar.Caption = "Quitar visado"      ' add -007- b. ' del -008- d.
                    '{ del -008- d.
                    ''{ add -007- b.
                    'Else
                    '    cmdVisar.Caption = "Visar"
                    ''}
                    'End If
                    ''}
                    '}
                    'txtUsuarioNombre = sp_GetRecursoNombre(Trim(txtUsuario))    ' add -007- c. ' del -008- a.
                End If
            End If
        End With
    End If
End Function

Private Sub grdDatos_Click()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           If .MouseCol = 0 Then
                .Col = 0
               .Sort = flexSortStringNoCaseAscending
           ElseIf .MouseCol = 1 Then
                .Col = 1
               .Sort = flexSortStringNoCaseAscending
           Else
                .Col = .MouseCol
               .Sort = flexSortStringNoCaseAscending
           End If
        End If
    End With
    Call MostrarSeleccion
    'MostrarSeleccion
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A", "M"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            '{ add -002- a.
            If glCatalogoENM <> "ABM" Then
                glCatalogoNombre = grdDatos.TextMatrix(grdDatos.RowSel, 1)
            End If
            '}
            Set auxRST = Nothing    ' add -005- d.
            Unload Me
    End Select
    LockProceso (False)
End Sub

Private Sub grdDatos_RowColChange()
    MostrarSeleccion
End Sub

Private Sub cmdAppSearch_Click()
    Dim vRetorno() As String
    frmSelAplicativo.cAmbiente = "H"
    frmSelAplicativo.Show 1
    If ParseString(vRetorno, glAuxRetorno, ":") > 0 Then
        txtAppId = vRetorno(1)
        txtAppName = Mid(vRetorno(2), 1, InStr(1, vRetorno(2), "|", vbTextCompare) - 1)
    End If
End Sub

Private Sub txtAppId_LostFocus()
    If Len(txtAppId) > 0 And Trim(txtAppId) <> "" Then
        If sp_GetAplicativo(txtAppId, Null, "S") Then
            txtAppName = ClearNull(aplRST.Fields!app_nombre)
        Else
            MsgBox "El aplicativo ingresado no existe o se encuentra inhabilitado.", vbExclamation + vbOKOnly, "Aplicativo inexistente o inv�lido"
            txtAppId.SetFocus
        End If
    Else
        txtAppName = ""
    End If
End Sub

Private Sub cmdCOPYS_Click()
    sDSNId = txtIdArch
    If Len(sDSNId) > 0 Then
        frmHEDT2Shell.Show
    Else
        MsgBox "Debe seleccionar un DSN para definir los COPYs del mismo.", vbExclamation + vbOKOnly, "Sin DSN seleccionado"
    End If
End Sub

Private Sub chkSoloPendientes_Click()
    Cargar_Grilla
End Sub

Private Sub cmdMarcar_Click()
    Dim i As Long
    Const cMensaje1 = "�Confirma el cambio de marca del archivo seleccionado?"
    Const cMensaje2 = "�Confirma el cambio de marca de los archivos seleccionados?"
    
    If grdDatos.Rows > 1 Then
        If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje2, cMensaje1), vbQuestion + vbYesNo, "Marcar archivos") = vbYes Then
            Call Status("Procesando..."): Screen.MousePointer = vbHourglass
            ' Genero las marcas
            For i = lRowSel_Ini To lRowSel_Fin
                If Not sp_UpdateHEDT001Field(grdDatos.TextMatrix(i, 0), "ESTADO", "B", Null, Null) Then
                    MsgBox "Error al procesar " & grdDatos.TextMatrix(i, 1)
                Else
                    grdDatos.TextMatrix(i, 12) = "Ya utilizado"
                    grdDatos.TextMatrix(i, 13) = "B"
                End If
            Next i
        End If
    Else
        MsgBox "Debe seleccionar un archivo para utilizar.", vbExclamation + vbOKOnly
    End If
    Call Status("Listo."): Screen.MousePointer = vbNormal
End Sub

'{ add -004- a.
Private Sub grdDatos_SelChange()
    With grdDatos
        lRowSel_Ini = .Row
        lRowSel_Fin = .RowSel
        ' Invierto los valores seg�n corresponda
        If lRowSel_Ini > lRowSel_Fin Then
            lRowSel_Ini = .RowSel
            lRowSel_Fin = .Row
        End If
    End With
End Sub
'}

'{ add -005- a.
Private Sub txtBuscarDSNNombre_Change()
    ' Ejecuto el filtrado de la grilla
    Filtrar
End Sub

Private Sub Filtrar()
    On Error GoTo Errores
    
    Dim cFilters As String
    
    ' Inicializa los filtros para abarcar todos los items
    'aplRST.Filter = adFilterNone   ' del -005- d.
    auxRST.Filter = adFilterNone    ' add -005- d.
    
    If Len(txtBuscarDSNNombre) > 0 Then
        cFilters = "dsn_nom LIKE '" & Trim(txtBuscarDSNNombre) & "*'"
    End If
    
    ' Si existe alg�n criterio, realiza el filtro...
    If Len(cFilters) > 0 Then
        'aplRST.Filter = cFilters       ' del -005- d.
        auxRST.Filter = cFilters        ' add -005- d.
    End If
    'ReCargar_Grilla aplRST     ' del -005- d.
    ReCargar_Grilla auxRST     ' add -005- d.
Exit Sub
Errores:
    Select Case Err.Number
        Case 3001   ' Argumentos incorrectos, fuera del intervalo permitido o en conflicto con otros.
            MsgBox "Esta intentando utilizar para el filtrado un caracter inv�lido. Revise.", vbExclamation + vbOKOnly, "Caracter inv�lido"
            cFilters = ""
            Resume
        Case Else
            Stop
            MsgBox Err.DESCRIPTION & "(" & Err.Number & ") - Or�gen: " & Err.Source, vbCritical + vbOKOnly, "Error al filtrar la grilla"
            cFilters = ""
            Resume Next
    End Select
End Sub

Private Sub ReCargar_Grilla(auxRST As ADODB.Recordset)
    Inicializar_Grilla
    '{ add -001- a.
'    If bSegInf Then     ' SEGURIDAD INFORMATICA
'        Call puntero(True)
'        With grdDatos
'            .Redraw = False     ' Deshabilitar el repintado del control
'            Do While Not auxRST.EOF
'                .Rows = .Rows + 1
'                .TextMatrix(.Rows - 1, 0) = ClearNull(auxRST.Fields!dsn_id)
'                .TextMatrix(.Rows - 1, 1) = ClearNull(auxRST.Fields!dsn_nom)
'                .TextMatrix(.Rows - 1, 2) = IIf(ClearNull(auxRST.Fields!dsn_enmas) = "N", "No", "Si")
'                .TextMatrix(.Rows - 1, 3) = ClearNull(auxRST.Fields!dsn_nomrut)
'                .TextMatrix(.Rows - 1, 4) = ClearNull(auxRST.Fields!dsn_feult)
'                .TextMatrix(.Rows - 1, 5) = ClearNull(auxRST.Fields!dsn_userid)
'                .TextMatrix(.Rows - 1, 6) = IIf(ClearNull(auxRST.Fields!dsn_vb) = "N", "No", "Si")
'                .TextMatrix(.Rows - 1, 7) = ClearNull(auxRST.Fields!dsn_vb_userid)
'                .TextMatrix(.Rows - 1, 8) = ClearNull(auxRST.Fields!dsn_vb_fe)
'                .TextMatrix(.Rows - 1, 9) = ClearNull(auxRST.Fields!app_id)
'                .TextMatrix(.Rows - 1, 10) = ClearNull(auxRST.Fields!app_name)
'                .TextMatrix(.Rows - 1, 11) = ClearNull(auxRST.Fields!dsn_desc)
'                '{ del -005- b.
'                'Select Case ClearNull(auxRST.Fields!Estado)
'                '    Case "A"
'                '        .TextMatrix(.Rows - 1, 13) = "A�n sin utilizar"
'                '    Case "B"
'                '        .TextMatrix(.Rows - 1, 13) = "Ya utilizado"
'                '    Case "C"
'                '        .TextMatrix(.Rows - 1, 13) = "Ya utilizado"
'                'End Select
'                '}
'                .TextMatrix(.Rows - 1, 12) = ClearNull(auxRST.Fields!nom_estado)    ' add -005- b.
'                .TextMatrix(.Rows - 1, 13) = ClearNull(auxRST.Fields!Estado)        ' C�digo de estado
'                auxRST.MoveNext
'                DoEvents
'            Loop
'            .Visible = True
'        End With
'    Else
    '}
        Call puntero(True)
        If Not auxRST.EOF Then
            With grdDatos
                .Redraw = False                 ' Deshabilitar el repintado del control
                auxRST.MoveFirst                ' Mueve el recordset al primer registro
                .Rows = auxRST.RecordCount + 1  ' Agrega las filas necesarias en el FlexGRid
                .Cols = auxRST.Fields.Count     ' Agrega las columnas necesarias
                ' Selecciona
                .Row = 1: .Col = 0
                .RowSel = .Rows - 1: .ColSel = .Cols - 1
                ' Esta linea de c�digo es la que carga los registros
                .Clip = auxRST.GetString(adClipString, -1, Chr(9), Chr(13), vbNullString)
                .Row = 1
                .Redraw = True
                Inicializar_Columnas        ' NEW
                .Visible = True
            End With
            If Val(Me.Tag) = auxRST.RecordCount Then
                Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos"
                Status ("Listo.")
            Else
                Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos (filtrado...)"
                Status (auxRST.RecordCount & " archivo(s) filtrado(s) de " & Me.Tag)
            End If
        Else
            If Len(aplRST.Filter) > 0 Then
                Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos (filtrado...)"
                Status (auxRST.RecordCount & " archivo(s) filtrado(s) de " & Me.Tag)
            Else
                Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos"
                Status ("Listo.")
            End If
        End If
'    End If      ' add -001- a.
    Call puntero(False)
End Sub
'}

'{ add -005- a.
Private Sub chkBuscar_Click()
    With grdDatos
        If chkBuscar.Value = 1 Then
            'lblBusqueda.Caption = "Puede incluir en la cadena de b�squeda comodines para ampliar las opciones de filtrado y b�squeda. Puede utilizar el asterisco (*) pero solo al principio, al final o ambos, nunca en medio de otro patr�n de b�squeda."
            lblBusqueda.Caption = ""
            lblBusqueda.Visible = True
            lblMensaje.Visible = False
            fraBusqueda.Enabled = True
            
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdCOPYS.Enabled = False
            
            .Top = 720
            '.Height = 3255
            txtBuscarDSNNombre.SetFocus
        Else
            lblBusqueda.Visible = False
            lblMensaje.Visible = True
            fraBusqueda.Enabled = False
            .Top = 120
            .Height = 3855
            
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdCOPYS.Enabled = True
        End If
    End With
End Sub
'}

Private Sub Inicializar_Columnas()
    With grdDatos
        .Visible = False
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .Cols = 17      ' upd -007- a. - Antes 14       ' upd -008- d.
        .TextMatrix(0, 0) = "C�digo"
        .TextMatrix(0, 1) = "Nombre del archivo"
        .TextMatrix(0, 2) = "ENM."
        .TextMatrix(0, 3) = "Rutina"
        .TextMatrix(0, 4) = "Ult. mod."
        .TextMatrix(0, 5) = "Propietario"                   ' upd -008- a. Antes Usuario, ahora Propietario
        .TextMatrix(0, 6) = "Visado"
        .TextMatrix(0, 7) = "Por"
        .TextMatrix(0, 8) = "Fch. Visado"
        .TextMatrix(0, 9) = "Aplicativo"
        .TextMatrix(0, 10) = "Nombre del aplicativo"
        .TextMatrix(0, 11) = "Descripci�n del archivo"
        .TextMatrix(0, 12) = "Estado"
        .TextMatrix(0, 13) = ""                     ' C�digo de estado
        .TextMatrix(0, 14) = "Validez"              ' add -007- a. - Validez del archivo
        .TextMatrix(0, 15) = "dsn_vb"                   ' add -008- b.
        .TextMatrix(0, 16) = "Motivo de rechazo"        ' add -008- c.
        .ColWidth(0) = 1000
        .ColWidth(1) = 4000
        .ColWidth(2) = 800
        .ColWidth(3) = 1000
        .ColWidth(4) = 1700
        .ColWidth(5) = 1500                             ' upd -008- a. Antes 1000, ahora 1500
        .ColWidth(6) = 1000
        .ColWidth(7) = 1200
        .ColWidth(8) = 1700
        .ColWidth(9) = 1000
        .ColWidth(10) = 2000
        .ColWidth(11) = 4000
        .ColWidth(12) = 1600
        .ColWidth(13) = 0                   ' C�digo de estado
        .ColWidth(14) = 800                 ' add -007- a. - Validez del archivo
        .ColWidth(15) = 0               ' add -008- b.
        .ColWidth(16) = 2000            ' add -008- c.
    End With
End Sub

Private Sub cmdCopiar_Click()
    Dim cBuffer As String
    Dim i As Long
    
    Clipboard.Clear
    If grdDatos.Rows > 1 Then
        For i = lRowSel_Ini To lRowSel_Fin
            cBuffer = cBuffer & grdDatos.TextMatrix(i, 1) & IIf(lRowSel_Ini = lRowSel_Fin, "", vbCrLf)
        Next i
    End If
    Clipboard.SetText Trim(cBuffer), vbCFText
End Sub

Private Sub cmdImprimir_Click()
    Dim cPathFileNameReport As String

    Call Status("Preparando el informe...")
    Screen.MousePointer = vbHourglass
   
    With mdiPrincipal.CrystalReport1
        cPathFileNameReport = App.Path & "\" & "rptdsn2.rpt"
        .ReportFileName = GetShortName(cPathFileNameReport)
        .RetrieveStoredProcParams
        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
        .SelectionFormula = ""
        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
        .Formulas(2) = "@0_TITULO='" & "DSN ya utilizados" & "'"
        .WindowLeft = 1
        .WindowTop = 1
        .WindowState = crptMaximized
        .Action = 1
        Screen.MousePointer = vbNormal
        Call Status("Listo.")
    End With
End Sub

'{ del -008- a.
''{ add -007- c.
'Private Sub cmdVerDatos_Click()
'    If fraAudit.Width = 3375 Then
'        fraAudit.Width = 8535
'    Else
'        fraAudit.Width = 3375
'    End If
'End Sub
''}
'}

''{ add -001- a.
'Private Sub cmdVisar_Click()
'    Dim i As Long
'    Dim bFlagOK As Boolean
'    Const cMensaje1 = "�Confirma el visado del archivo seleccionado?"
'    Const cMensaje2 = "�Confirma el visado de los archivos seleccionados?"
'    Const cMensaje3 = "�Confirma remover el visado del archivo seleccionado?"
'    Const cMensaje4 = "�Confirma remover el visado de los archivos seleccionados?"
'
'    bFlagOK = False
'
'    If grdDatos.Rows > 1 Then
'        '{ add -007- b.
'        If cmdVisar.Caption = "Quitar visado" Then
'            If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje4, cMensaje3), vbQuestion + vbYesNo, "Quitar visado de archivos") = vbYes Then
'                With grdDatos
'                    Call Status("Quitando visado(s)..."): Screen.MousePointer = vbHourglass
'                    If lRowSel_Ini <> lRowSel_Fin Then
'                        For i = lRowSel_Ini To lRowSel_Fin
'                            If InPerfil("ASEG") Then
'                                If .TextMatrix(i, 6) = "Si" Then
'                                    If Not sp_UpdateHEDT101(Trim(.TextMatrix(i, 0))) Then
'                                        MsgBox "Error al procesar " & i
'                                    Else
'                                        .TextMatrix(i, 6) = "No"
'                                        .TextMatrix(i, 7) = ""
'                                        .TextMatrix(i, 8) = ""
'                                        bFlagOK = True
'                                    End If
'                                End If
'                            Else
'                                MsgBox "Vd. no tiene perfil adecuado para trabajar con el visado de archivos.", vbExclamation + vbOKOnly, "Quitar visado de archivos"    ' add -007- b.
'                            End If
'                        Next i
'                    Else    ' Selecci�n simple
'                        If InPerfil("ASEG") Then
'                            If .TextMatrix(.RowSel, 6) = "Si" Then
'                                If Not sp_UpdateHEDT101(Trim(.TextMatrix(.RowSel, 0))) Then
'                                    MsgBox "Error al procesar " & .RowSel
'                                Else
'                                    .TextMatrix(.RowSel, 6) = "No"
'                                    .TextMatrix(.RowSel, 7) = ""
'                                    .TextMatrix(.RowSel, 8) = ""
'                                    bFlagOK = True
'                                End If
'                            End If
'                        Else
'                            MsgBox "Vd. no tiene perfil adecuado para trabajar con el visado de archivos.", vbExclamation + vbOKOnly, "Quitar visado de archivos"    ' add -007- b.
'                        End If
'                    End If
'                End With
'                'If bFlagOK Then Cargar_Grilla  ' del -007- b.
'                Call Status("Listo."): Screen.MousePointer = vbNormal
'            End If
'        Else
'        '}
'            If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje2, cMensaje1), vbQuestion + vbYesNo, "Quitar visado de archivos") = vbYes Then
'                With grdDatos
'                    Call Status("Generando visado(s)..."): Screen.MousePointer = vbHourglass
'                    If lRowSel_Ini <> lRowSel_Fin Then
'                        ' M�ltiple selecci�n
'                        '{ del -004- a.
'                        'If lRowSel_Ini > lRowSel_Fin Then
'                        '    For i = lRowSel_Fin To lRowSel_Ini Step -1
'                        '        If (.TextMatrix(i, 6) = "No" And InPerfil("ASEG")) Then
'                        '            If Not sp_UpdateHEDT101(Trim(.TextMatrix(i, 0))) Then
'                        '                MsgBox "Error al procesar " & i
'                        '            Else
'                        '                bFlagOK = True
'                        '            End If
'                        '        Else
'                        '            MsgBox "El archivo " & Trim(.TextMatrix(i, 0)) & " ya se encuentra visado.", vbExclamation + vbOKOnly, "Archivo visado"
'                        '        End If
'                        '    Next i
'                        'Else
'                        '}
'                        For i = lRowSel_Ini To lRowSel_Fin
'                            'If .TextMatrix(i, 6) = "No" And InPerfil("ASEG") Then  ' del -007- b.
'                            '{ add -007- b.
'                            If InPerfil("ASEG") Then
'                                If .TextMatrix(i, 6) = "No" Then
'                            '}
'                                    If Not sp_UpdateHEDT101(Trim(.TextMatrix(i, 0))) Then
'                                        MsgBox "Error al procesar " & i
'                                    Else
'                                        '{ add -007- b.
'                                        .TextMatrix(i, 6) = "Si"
'                                        .TextMatrix(i, 7) = glLOGIN_ID_REEMPLAZO
'                                        .TextMatrix(i, 8) = Now
'                                        '}
'                                        bFlagOK = True
'                                    End If
'                                End If  ' add -007- b.
'                            Else
'                                'MsgBox "El archivo " & Trim(.TextMatrix(i, 0)) & " ya se encuentra visado.", vbExclamation + vbOKOnly, "Archivo visado"    ' del -007- b.
'                                MsgBox "Vd. no tiene perfil adecuado para trabajar con el visado de archivos.", vbExclamation + vbOKOnly, "Visar archivos"    ' add -007- b.
'                            End If
'                        Next i
'                        'End If     ' del -004- a.
'                    Else    ' Selecci�n simple
'                        'If .TextMatrix(.RowSel, 6) = "No" And InPerfil("ASEG") Then    ' del -007- b.
'                        '{ add -007- b.
'                        If InPerfil("ASEG") Then
'                            If .TextMatrix(.RowSel, 6) = "No" Then
'                        '}
'                                If Not sp_UpdateHEDT101(Trim(.TextMatrix(.RowSel, 0))) Then
'                                    MsgBox "Error al procesar " & .RowSel
'                                Else
'                                    '{ add -007- a.
'                                    .TextMatrix(.RowSel, 6) = "Si"
'                                    .TextMatrix(.RowSel, 7) = glLOGIN_ID_REEMPLAZO
'                                    .TextMatrix(.RowSel, 8) = Now
'                                    '}
'                                    bFlagOK = True
'                                End If
'                            End If  ' add -007- b.
'                        Else
'                            'MsgBox "El archivo " & Trim(.TextMatrix(i, 0)) & " ya se encuentra visado.", vbExclamation + vbOKOnly, "Archivo visado"    ' del -007- b.
'                            MsgBox "Vd. no tiene perfil adecuado para trabajar con el visado de archivos.", vbExclamation + vbOKOnly, "Visar archivos"    ' add -007- b.
'                        End If
'                    End If
'                End With
'                'If bFlagOK Then Cargar_Grilla  ' del -007- b.
'                Call Status("Listo."): Screen.MousePointer = vbNormal
'            End If
'        End If  ' add -007- b.
'    End If
'End Sub
'
''{ del -004- a.
''Private Sub grdDatos_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
''    With grdDatos
''        lRowSel_Ini = .RowSel
''    End With
''End Sub
''
''Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
''    With grdDatos
''        lRowSel_Fin = .RowSel
''    End With
''End Sub
''}
''}
