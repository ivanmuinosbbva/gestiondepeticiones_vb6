VERSION 5.00
Begin VB.Form frmPeticionesPlanificarCambioEstado 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Planificaci�n - Cambio de estado"
   ClientHeight    =   2955
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6645
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2955
   ScaleWidth      =   6645
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbPeriodo 
      Height          =   300
      Left            =   840
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   120
      Width           =   5415
   End
   Begin VB.ComboBox cmbPeriodoEstado 
      Height          =   300
      Left            =   840
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1800
      Width           =   5415
   End
   Begin VB.Frame pnlBtnControl 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   0
      Top             =   2280
      Width           =   6615
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   500
         Left            =   1920
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   120
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   3630
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Width           =   1170
      End
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Desde:"
      ForeColor       =   &H00FF0000&
      Height          =   180
      Index           =   3
      Left            =   840
      TabIndex        =   14
      Top             =   1080
      Width           =   540
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Hasta:"
      ForeColor       =   &H00FF0000&
      Height          =   180
      Index           =   4
      Left            =   840
      TabIndex        =   13
      Top             =   1320
      Width           =   510
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Fecha desde"
      ForeColor       =   &H00000000&
      Height          =   180
      Index           =   5
      Left            =   1560
      TabIndex        =   12
      Top             =   1080
      Width           =   945
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Fecha hasta"
      ForeColor       =   &H00000000&
      Height          =   180
      Index           =   6
      Left            =   1560
      TabIndex        =   11
      Top             =   1320
      Width           =   915
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Estado:"
      ForeColor       =   &H00FF0000&
      Height          =   180
      Index           =   7
      Left            =   840
      TabIndex        =   10
      Top             =   600
      Width           =   585
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Estado"
      ForeColor       =   &H00000000&
      Height          =   180
      Index           =   8
      Left            =   1560
      TabIndex        =   9
      Top             =   600
      Width           =   525
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Vigente:"
      ForeColor       =   &H00FF0000&
      Height          =   180
      Index           =   2
      Left            =   840
      TabIndex        =   8
      Top             =   840
      Width           =   645
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Vigencia"
      ForeColor       =   &H00000000&
      Height          =   180
      Index           =   9
      Left            =   1560
      TabIndex        =   7
      Top             =   840
      Width           =   660
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Acci�n:"
      ForeColor       =   &H00FF0000&
      Height          =   180
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   1860
      Width           =   585
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Per�odo:"
      ForeColor       =   &H00FF0000&
      Height          =   180
      Index           =   1
      Left            =   120
      TabIndex        =   5
      Top             =   180
      Width           =   630
   End
End
Attribute VB_Name = "frmPeticionesPlanificarCambioEstado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 18.11.2010 - Se agrega la rutina que cierra el per�odo, identificando aquellas peticiones del per�odo
'                           a cerrar para ser "transferidas" al nuevo per�odo autom�ticamente.
 
Option Explicit

Dim Periodo As Long
Dim PeriodoEstado As String

Private Sub Form_Load()
    Inicializar
End Sub

Private Sub Inicializar()
    Dim iIndex As Integer
    
    Periodo = 0
    PeriodoEstado = ""
    
    Call setHabilCtrl(cmbPeriodo, "DIS")
    Call setHabilCtrl(cmbPeriodoEstado, "DIS")
    
    With cmbPeriodoEstado
        .Clear
        If sp_GetPeriodoEstado(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_estado_per) & Space(150) & "||" & ClearNull(aplRST.Fields!cod_estado_per)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            Call setHabilCtrl(cmbPeriodoEstado, "NOR")
        End If
    End With
    
    With cmbPeriodo
        .Clear
        If sp_GetPeriodo(Null, Null) Then
            Do While Not aplRST.EOF
                '.AddItem ClearNull(aplRST.Fields!per_nrointerno) & ". " & ClearNull(aplRST.Fields!per_nombre) & Space(150) & "||" & ClearNull(aplRST.Fields!per_nrointerno)
                .AddItem ClearNull(aplRST.Fields!per_abrev) & ". " & ClearNull(aplRST.Fields!per_nombre) & Space(150) & "||" & ClearNull(aplRST.Fields!per_nrointerno)
                If ClearNull(aplRST.Fields!vigente) = "S" Then iIndex = cmbPeriodo.ListCount - 1
                aplRST.MoveNext
                DoEvents
            Loop
            Call setHabilCtrl(cmbPeriodo, "NOR")
            .ListIndex = iIndex
        End If
    End With
End Sub

Private Sub cmbPeriodo_Click()
    If sp_GetPeriodo(CodigoCombo(cmbPeriodo, True), Null) Then
        lblEncabezadoPrincipal(5) = ClearNull(aplRST.Fields!fe_desde)
        lblEncabezadoPrincipal(6) = ClearNull(aplRST.Fields!fe_hasta)
        lblEncabezadoPrincipal(8) = ClearNull(aplRST.Fields!nom_estado)
        lblEncabezadoPrincipal(9) = IIf(ClearNull(aplRST.Fields!vigente) = "S", "Si", "No")
        cmbPeriodoEstado.ListIndex = PosicionCombo(cmbPeriodoEstado, ClearNull(aplRST.Fields!per_estado), True)
    End If
End Sub

Private Sub cmdConfirmar_Click()
    Dim bDoIt As Boolean
    Dim PeriodoSiguiente As Long
    Dim Dias As Long
    Dim fe_hasta_peract As Date
    
    bDoIt = False
    
    Periodo = CodigoCombo(cmbPeriodo, True)
    PeriodoEstado = CodigoCombo(cmbPeriodoEstado, True)
    
    '{ del
'    If PeriodoEstado = "PLAN40" Then
'        ' 1. Verificar que exista el siguiente per�odo, si no, no se puede cerrar. Dar aviso.
'        If sp_GetPeriodo(Periodo) Then
'            fe_hasta_peract = ClearNull(aplRST.Fields!fe_hasta)
'        End If
'        ' Aqu� identifico al posible siguiente per�odo
'        If sp_GetPeriodo(Null) Then
'            Do While Not aplRST.EOF
'                If aplRST.Fields!per_nrointerno <> Periodo Then
'                    If aplRST.Fields!fe_desde > fe_hasta_peract Then
'                        Dias = DateDiff("d", fe_hasta_peract, aplRST.Fields!fe_desde)
'                        If Dias < DateDiff("d", fe_hasta_peract, aplRST.Fields!fe_desde) Then
'                            PeriodoSiguiente = aplRST.Fields!per_nrointerno
'                        End If
'                    End If
'                End If
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        ' Si PeriodoSiguiente = 0 es porque no hay definido un siguiente per�odo
'        If PeriodoSiguiente = 0 Then
'            MsgBox "No se ha definido a�n el siguiente per�odo de planificaci�n. Para cerrar el per�odo actual debe ser definido.", vbExclamation + vbOKOnly, "Falta siguiente per�odo de planificaci�n"
'            Exit Sub
'        End If
'
'        ' 2. Aviso de lo que va a hacer el proceso
'        With cstmMessageBox
'            .bContinuar = False
'            .Caption = ""
'            .msgTitle = "Cierre del per�odo actual"
'            .msgText = "Al cambiar el estado del per�odo actual a cerrado, ya no podr�n planificarse para �ste per�odo. Aquellas peticiones que hayan quedado pendientes de planificar, ser�n identificadas para el per�odo inmediato posterior seg�n el siguiente detalle:" & vbCrLf & vbCrLf & "- Peticiones con prioridad 1, pasar�n a tener prioridad 3" & vbCrLf & "- Peticiones con prioridad 2, pasar�n a tener prioridad 4" & vbCrLf & vbCrLf & "�Confirma el cierre?"
'            .Show 1
'        End With
'        If cstmMessageBox.bContinuar Then
'            Call puntero(True)
'            Call Status("Cerrando per�odo actual...")
'
'
'        End If
'    End If

    If MsgBox("�Confirma el cambio de estado del per�odo?", vbQuestion + vbYesNo, "Cambio de estado del per�odo actual") = vbYes Then
        '{ add -001- a. Si el estado es de cierre de un per�odo
        bDoIt = True
        If PeriodoEstado = "PLAN40" Then
            ' 3. Buscar todas las peticiones/grupos del per�odo a cerrar, que tengan prioridad 1 y estado de planificaci�n 1 +
            '    aquellas que tengan prioridad 2 y estado de planificaci�n <> 2
            ' 4. Proceso que pasa las de prioridad 1 a 3 y las de prioridad 2 a 4
            ' 5. Proceso que a �stas les cambia el per�odo a cerrar al siguiente
            ' 6. Aviso x mail a los l�deres involucrados de que la lista de peticiones armada se ha pasado de per�odo
            '    por el cierre autom�tico (esto hacerlo por el daemon de java)
            ' 7. Por �ltimo, actualizar el estado del per�odo a cerrar + el estado del nuevo per�odo siguiente "actual"

        End If
        '}
        If bDoIt Then
            Call sp_UpdatePeriodoField(Periodo, "PER_ESTADO", PeriodoEstado, Null, Null)
            Call sp_UpdatePeriodoField(Periodo, "PER_ULTUSRID", glLOGIN_ID_REEMPLAZO, Null, Null)
            ' Falta habilitar el pasaje de las peticiones/grupos sin planificaci�n al siguiente per�odo (las 1 con 3 y las 2 con 4)
            Unload Me
        End If
    End If
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub
