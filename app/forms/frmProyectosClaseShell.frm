VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmProyectosClaseShell 
   Caption         =   "Clasificaci�n"
   ClientHeight    =   6600
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9675
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6600
   ScaleWidth      =   9675
   Begin VB.Frame pnlBotones 
      Height          =   6545
      Left            =   8340
      TabIndex        =   6
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5460
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   5970
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Gerencia seleccionada"
         Top             =   4950
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la Gerencia selecionada"
         Top             =   4440
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Gerencia"
         Top             =   3930
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1860
      Left            =   120
      TabIndex        =   0
      Top             =   4680
      Width           =   8205
      Begin VB.ComboBox cmbCategoria 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1080
         Width           =   6015
      End
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   315
         Left            =   1200
         TabIndex        =   1
         Top             =   360
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   315
         Left            =   1200
         TabIndex        =   2
         Top             =   720
         Width           =   5985
         _ExtentX        =   10557
         _ExtentY        =   556
         MaxLength       =   80
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   80
      End
      Begin VB.Label Label3 
         Caption         =   "Categor�a"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   14
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   345
         Width           =   1100
      End
      Begin VB.Label Label2 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   690
         Width           =   975
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   3
         Top             =   30
         Visible         =   0   'False
         Width           =   45
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4650
      Left            =   120
      TabIndex        =   12
      Top             =   30
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   8202
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmProyectosClaseShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colCODCLASE = 0
Private Const colNOMCLASE = 1
Private Const colCODCATEGORIA = 2
Private Const colNOMCATEGORIA = 3

Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarPantalla
    modUser32.IniciarScroll grdDatos
End Sub

Sub InicializarPantalla()
    Me.Top = 0
    Me.Left = 0
    Me.Height = 7095
    Me.Width = 9780
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    Call HabilitarBotones(0)
    With cmbCategoria
        .Clear
        'If spProyectoIDMAnterior.sp_GetProyIDMCategoria(Null, Null) Then
        If sp_GetProyIDMCategoria(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!ProjCatId) & ". " & ClearNull(aplRST.Fields!ProjCatNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!ProjCatId)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.Visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.Visible = True
                    txtCodigo = "": txtDescripcion = "": cmbCategoria.ListIndex = -1
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(txtCodigo.Text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.Text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtDescripcion.Text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtDescripcion.Text, ":") > 0 Then
        MsgBox ("a Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If cmbCategoria.ListIndex = -1 Then
        MsgBox ("Debe seleccionar una categor�a v�lida.")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub CargarGrid()
    With grdDatos
        .Clear
        '.HighLight = flexHighlightNever
        .HighLight = flexHighlightWithFocus
        .FocusRect = flexFocusNone
        .FillStyle = flexFillSingle
        .SelectionMode = flexSelectionByRow
        .Rows = 1
        .Cols = 4
        .TextMatrix(0, colCODCLASE) = "CODIGO"
        .TextMatrix(0, colNOMCLASE) = "DESCRIPCION"
        .TextMatrix(0, colCODCATEGORIA) = "CODIGO CAT"
        .TextMatrix(0, colNOMCATEGORIA) = "DESCRIPCION CAT"
        .ColWidth(colCODCLASE) = 1000: .ColAlignment(colCODCLASE) = 0
        .ColWidth(colNOMCLASE) = 7000: .ColAlignment(colNOMCLASE) = 0
        .ColWidth(colCODCATEGORIA) = 0: .ColAlignment(colCODCATEGORIA) = 0
        .ColWidth(colNOMCATEGORIA) = 0: .ColAlignment(colNOMCATEGORIA) = 0
    End With
    CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    
    If Not sp_GetProyIDMClase(Null, Null, Null) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODCLASE) = ClearNull(aplRST.Fields!ProjClaseId)
            .TextMatrix(.Rows - 1, colNOMCLASE) = ClearNull(aplRST.Fields!ProjClaseNom)
            .TextMatrix(.Rows - 1, colCODCATEGORIA) = ClearNull(aplRST.Fields!ProjCatId)
            .TextMatrix(.Rows - 1, colNOMCATEGORIA) = ""
        End With
        aplRST.MoveNext
    Loop
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colCODCLASE) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODCLASE))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNOMCLASE))
                cmbCategoria.ListIndex = PosicionCombo(cmbCategoria, .TextMatrix(.RowSel, colCODCATEGORIA), True)
            End If
        End If
    End With
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertProyectoClase(CodigoCombo(cmbCategoria, True), txtCodigo, txtDescripcion) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdateProyectoClase(CodigoCombo(cmbCategoria, True), txtCodigo, txtDescripcion) Then
                    With grdDatos
                        If .RowSel > 0 Then
                           .TextMatrix(.RowSel, colNOMCLASE) = txtDescripcion
                           .TextMatrix(.RowSel, colCODCATEGORIA) = CodigoCombo(cmbCategoria, True)
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeleteProyectoClase(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub
