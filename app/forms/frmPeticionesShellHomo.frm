VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmPeticionesShellHomo 
   Caption         =   "Consulta de peticiones para Homologaci�n"
   ClientHeight    =   8025
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12825
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesShellHomo.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8025
   ScaleWidth      =   12825
   Begin VB.Frame fraFiltros 
      Height          =   1755
      Left            =   60
      TabIndex        =   6
      Top             =   0
      Width           =   11355
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "Filtrar"
         Height          =   315
         Left            =   1320
         TabIndex        =   12
         Top             =   600
         Width           =   850
      End
      Begin VB.CommandButton cmdFiltrarNumero 
         Caption         =   "Filtrar"
         Height          =   315
         Left            =   1320
         TabIndex        =   11
         Top             =   1320
         Width           =   850
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   600
         Width           =   4755
      End
      Begin VB.ComboBox cboFiltroProceso 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5640
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1320
         Width           =   1995
      End
      Begin VB.ComboBox cboFiltroHomologador 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   960
         Width           =   4755
      End
      Begin MSComCtl2.DTPicker dtpFechaDesde 
         Height          =   330
         Left            =   8520
         TabIndex        =   7
         Top             =   600
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   582
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   121962497
         CurrentDate     =   42230
      End
      Begin AT_MaskText.MaskText txtNroBuscar 
         Height          =   315
         Left            =   2880
         TabIndex        =   13
         Top             =   1320
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin MSComCtl2.DTPicker dtpFechaHasta 
         Height          =   330
         Left            =   8520
         TabIndex        =   14
         Top             =   960
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   582
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   121962497
         CurrentDate     =   42230
      End
      Begin VB.Label Label2 
         Caption         =   "Filtrar por estado"
         ForeColor       =   &H00FF0000&
         Height          =   480
         Left            =   180
         TabIndex        =   23
         Top             =   510
         Width           =   1050
      End
      Begin VB.Label Label3 
         Caption         =   "Filtrar por n�mero"
         ForeColor       =   &H00FF0000&
         Height          =   375
         Left            =   180
         TabIndex        =   22
         Top             =   1230
         Width           =   1140
      End
      Begin VB.Label lblPerfil 
         Alignment       =   2  'Center
         Caption         =   "descripcion del perfil actuante"
         ForeColor       =   &H00FF0000&
         Height          =   225
         Left            =   240
         TabIndex        =   21
         Top             =   300
         Width           =   7845
      End
      Begin VB.Label lblAgrup 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Filtrar por homologador:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   1140
         TabIndex        =   20
         Top             =   1020
         Width           =   1740
      End
      Begin VB.Label Label1 
         Caption         =   "Estado:"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2265
         TabIndex        =   19
         Top             =   645
         Width           =   615
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Nro.:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2265
         TabIndex        =   18
         Top             =   1380
         Width           =   375
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Proceso:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   0
         Left            =   4920
         TabIndex        =   17
         Top             =   1380
         Width           =   630
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   1
         Left            =   7800
         TabIndex        =   16
         Top             =   675
         Width           =   510
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   2
         Left            =   7800
         TabIndex        =   15
         Top             =   1035
         Width           =   480
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   7995
      Left            =   11460
      TabIndex        =   1
      Top             =   0
      Width           =   1335
      Begin VB.CommandButton cmdRptOmeOma 
         Caption         =   "Informe OME/OMA"
         Height          =   500
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Lista de todos los informes los OME y OMA encontrados"
         Top             =   1800
         Width           =   1170
      End
      Begin VB.CommandButton cmdGenerarInformes 
         Caption         =   "Informes"
         Height          =   500
         Left            =   60
         Picture         =   "frmPeticionesShellHomo.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Generar la exportaci�n a PDF de los informes seleccionados"
         Top             =   1260
         Width           =   1170
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "&Acceder"
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   5100
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "&Cerrar"
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   7440
         Width           =   1170
      End
      Begin VB.CommandButton cmdGrupo 
         Caption         =   "Grupos"
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Width           =   1170
      End
      Begin VB.CommandButton cmdGrupOper 
         Caption         =   "Cambio Est. Grupo"
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   720
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   6180
      Left            =   60
      TabIndex        =   0
      Top             =   1800
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   10901
      _Version        =   393216
      Cols            =   31
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPeticionesShellHomo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colMultiSelect = 0
Private Const colEmpresa = 1
Private Const colNroAsignado = 2
Private Const colNroInterno = 3
Private Const colTipoPet = 4
Private Const colPetClass = 5
Private Const colPetImpTech = 6
Private Const colPetRegulatorio = 7
Private Const colPetRO = 8
Private Const colTitulo = 9
Private Const colINFOId = 10
Private Const colINFOIdVer = 11
Private Const colINFONom = 12
Private Const colINFOTipo = 13
Private Const colINFOTipoNom = 14
Private Const colINFOPuntoControl = 15
Private Const colINFOFechaAlta = 16
Private Const colINFOHomologador = 17
Private Const colINFOHomologaNom = 18
Private Const colINFOEstado = 19
Private Const colINFOEstadoNom = 20
Private Const colINFOProtoBase = 21
Private Const colSolicitante = 22
Private Const colSoliNom = 23
Private Const colEstCod = 24
Private Const colEstNom = 25
Private Const colRefeSis = 26
Private Const colRefeSisNom = 27
Private Const colTotInfos = 28
Private Const colTOTAL_COLUMNAS = 29

Private Const RPT_PERFIL_BP As String = "petperfilbp.rpt"
Private Const RPT_PERFIL_SOLI As String = "petperfilsoli.rpt"
Private Const RPT_PERFIL As String = "petperfil.rpt"

Public bSeleccion As Boolean

Dim flgEnCarga As Boolean
Dim sOpcionSeleccionada As String
Dim sEstadoQuery As String
Dim flgNumero As Boolean
Dim flgInMultiselect As Boolean
Dim xPerfil As String
Dim xNivel As String
Dim xArea As String
Dim sArea As String

Dim vPeticionNroInterno() As Long
Dim vPeticionInforme() As Long
Dim vNombreInforme() As String

Dim sUbicacionAnterior As String
Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long

Private Sub Form_Load()
    flgNumero = False
    flgEnCarga = True
    xPerfil = glUsrPerfilActual
    xNivel = getPerfNivel(glUsrPerfilActual)
    xArea = getPerfArea(glUsrPerfilActual)
    sUbicacionAnterior = ""
    Call InicializarCombos
    Call InicializarPantalla
    Call IniciarScroll(grdDatos)
    flgEnCarga = False
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    cmdGrupo.Enabled = False
    cmdGrupOper.Enabled = False
   
    Call setLblPerfil
    Call HabilitarBotones(0)
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            cboEstado.Enabled = True
            cmdFiltro.Enabled = True
            grdDatos.Enabled = True
            sOpcionSeleccionada = ""
            cmdCerrar.visible = True
            cmdVisualizar.visible = True
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion(False)
            End If
        Case 1
            cboEstado.Enabled = False
            cmdFiltro.Enabled = False
            grdDatos.Enabled = False
            cmdCerrar.visible = False
            cmdVisualizar.visible = False
    End Select
    Call LockProceso(False)
End Sub

Private Sub InicializarCombos()
    Dim i As Integer
    Dim auxCod As String
    Dim auxCod2 As String
    
    auxCod = ""
    auxCod2 = ""
    
    Call Status("Cargando acciones...")
        
    dtpFechaDesde.value = DateAdd("m", -12, Now)
    dtpFechaHasta.value = Now
    
    With cboEstado
        .Clear
        .AddItem "Solo informes pendientes de homologar" & ESPACIOS & "||CONFEC"
        .AddItem "Solo informes verificados" & ESPACIOS & "||VERIFI"
        .AddItem "Solo informes finalizados" & ESPACIOS & "||TERMIN"
        .AddItem "* Todos los informes" & ESPACIOS & "||NULL"
        '.AddItem "Pendientes para este perfil" & Space(60) & "||" & auxCod & "PENDIE|", 0
        '.AddItem "Activas en las que participa con este perfil" & Space(60) & "||" & auxCod2, 1
        '.AddItem "Todas en las que participa con este perfil" & Space(60) & "||" & "NULL"
        .ListIndex = 0
    End With
    
    With cboFiltroProceso
        .Clear
        .AddItem "* Todos" & ESPACIOS & "||NULL"
        .AddItem "Reducido" & ESPACIOS & "||R"
        .AddItem "Completo" & ESPACIOS & "||C"
        .ListIndex = 0
    End With
    
    With cboFiltroHomologador
        .Clear
        .AddItem "* Todos los homologadores" & ESPACIOS & "||NULL"
        If sp_GetRecursoPerfil(Null, "HOMO") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_recurso) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = 0
    End With
    Call Status("Listo.")
End Sub

Private Sub Form_Activate()
    Me.WindowState = vbMaximized
End Sub

Private Sub Form_Resize()
    'Me.WindowState = vbMaximized               ' del -019- a.
    '{ add -019- a.
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            Me.Top = 0
            Me.Left = 0
            If Me.Height < 8535 Then Me.Height = 8535
            If Me.Width < 12945 Then Me.Width = 12945
        End If
        ' Ancho
        fraFiltros.Width = Me.ScaleWidth - fraBotonera.Width - 150
        grdDatos.Width = Me.ScaleWidth - fraBotonera.Width - 150
        fraBotonera.Left = fraFiltros.Width + 100
        ' Alto
        grdDatos.Height = Me.ScaleHeight - fraFiltros.Height - 100
        fraBotonera.Height = Me.ScaleHeight - 50
        cmdCerrar.Top = fraBotonera.Height - cmdCerrar.Height - 100
    End If
    '}
End Sub

Private Sub Form_Unload(Cancel As Integer)
    glNumeroPeticion = ""
    Call DetenerScroll(grdDatos)      ' add -008- a.
End Sub

Private Sub cmdPrintPet_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim nCopias As Integer
    Dim i As Integer
    
    If grdDatos.Rows < 2 Then
        Exit Sub
    End If
    
    rptFormulario.Show vbModal
    If (Not rptFormulario.chkFormu) And (Not rptFormulario.chkHisto) Then
        Exit Sub
    End If
    
    Call Puntero(True)
    Call Status("")
    flgMulti = False
    DoEvents
    nCopias = rptFormulario.Copias
    With grdDatos
        While nCopias > 0
            For i = 1 To .Rows - 1
               If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                    flgMulti = True
                    Call Status("Orden:" & i)
                    If rptFormulario.chkFormu Then
                        Call PrintFormulario(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If rptFormulario.chkHisto Then
                        Call PrintHitorial(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If nCopias = 1 Then
                        .TextMatrix(i, colMultiSelect) = ""
                    End If
               End If
            Next
            nCopias = nCopias - 1
        Wend
    End With
    Call Puntero(False)
    If flgMulti Then
        MsgBox ("Impresi�n finalizada")
    Else
        MsgBox ("Nada seleccionado para imprimir")
    End If
    Call Status("Listo.")
End Sub

Private Sub cmdAgrupar_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim i As Integer
    glSelectAgrup = ""
    glModoAgrup = "ALTAX"
''    glNivelAgrup = ""
''    glAreaAgrup = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "MODI"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    DoEvents
    
    If ClearNull(glSelectAgrup) = "" Then
        Exit Sub
    End If
    
    Dim nRwD As Integer, nRwH As Integer
    flgMulti = False
        
    With grdDatos
        If .Rows < 2 Then
            Exit Sub
        End If
        Call Puntero(True)
        Call Status("")
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                If (Not flgMulti) Then
                    If MsgBox("Desea que esta/s Peticion/es integren el Agrupamiento '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbNo Then
                        Exit For
                    End If
                End If
                flgMulti = True
                Call Status("Orden:" & i)
                'si ya existe agrup-petic no hace nada, ni siquiera avisa
                If sp_GetAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                    aplRST.Close
                Else
                    Call Puntero(True)
                    If sp_GetAgrupPeticDup(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                        Call Puntero(False)
                        If MsgBox("La Peticion:" & .TextMatrix(i, colTitulo) & Chr(13) & "ya est� declarada en el agrupamiento: " & ClearNull(aplRST!agr_titulo) & Chr(13) & "Desea igualmente que integre '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbYes Then
                           aplRST.Close
                           Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                        Else
                           aplRST.Close
                        End If
                    Else
                        Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                    End If
                    Call Puntero(False)
                End If
                .TextMatrix(i, colMultiSelect) = ""
           End If
        Next
    End With
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub cmdFiltrarNumero_Click()
    If flgEnCarga = True Then Exit Sub
    If Val(txtNroBuscar) > 100000 Then
        txtNroBuscar = ""
        Call Status("N�mero inv�lido!")
        Exit Sub
    End If
    If Val(txtNroBuscar) > 0 Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        flgNumero = True
        'cboEstado.ListIndex = cboEstado.ListCount - 1
        'sEstadoQuery = DatosCombo(cboEstado)
        glNumeroPeticion = ""
        CargarGrid
    Else
        flgNumero = False
        glNumeroPeticion = ""
        CargarGrid
    End If
End Sub

Private Sub cmdFiltro_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    flgNumero = False
    txtNroBuscar = ""
    sEstadoQuery = DatosCombo(cboEstado)
    glNumeroPeticion = ""
    Call CargarGrid
End Sub

Private Sub cboFiltroHomologador_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    flgNumero = False
    txtNroBuscar = ""
    'sEstadoQuery = DatosCombo(cboEstado)
    glNumeroPeticion = ""
    Call CargarGrid
End Sub

Private Sub cmdGrupOper_Click()
    If flgEnCarga = True Then Exit Sub
    glSector = ""
    If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
        glGrupo = sArea
        If sp_GetGrupo(sArea, Null) Then
            If Not aplRST.EOF Then
               glSector = ClearNull(aplRST!cod_sector)
               aplRST.Close
            End If
        End If
        If ClearNull(glSector) <> "" Then
            If Not LockProceso(True) Then
                Exit Sub
            End If
            frmPeticionesGrupoOperar.Show 1
            DoEvents
            Call LockProceso(False)
            If Me.Tag = "REFRESH" Then
                InicializarPantalla
            End If
        End If
    End If
End Sub

Private Sub cmdGrupo_Click()
    If flgEnCarga = True Then Exit Sub
    If xPerfil = "CGRU" Then
        If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
            If sp_GetGrupo(sArea, Null) Then
                If Not aplRST.EOF Then
                   glSector = ClearNull(aplRST!cod_sector)
                   aplRST.Close
                End If
            End If
            If ClearNull(glSector) <> "" Then
                If Not LockProceso(True) Then
                    Exit Sub
                End If
                frmPeticionesGrupo.Show 1
                DoEvents
                Call LockProceso(False)
                If Me.Tag = "REFRESH" Then
                    InicializarPantalla
                End If
            End If
        End If
        Exit Sub
    End If
    If xPerfil = "CSEC" Then
        If ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
            glSector = sArea
            If Not LockProceso(True) Then
                Exit Sub
            End If
            frmPeticionesGrupo.Show vbModal
            DoEvents
            Call LockProceso(False)
            If Me.Tag = "REFRESH" Then
                InicializarPantalla
            End If
        End If
        Exit Sub
    End If
End Sub

Private Sub cmdVisualizar_Click()
    If flgEnCarga = True Then Exit Sub
    If glNumeroPeticion <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoPeticion = "VIEW"
        On Error Resume Next
        frmPeticionesCarga.Show vbModal
        'frmPeticionesCargaNew.Show vbModal
        DoEvents
        On Error GoTo 0
        Call LockProceso(False)
        If Me.Tag = "REFRESH" Then
            InicializarPantalla
        End If
    End If
End Sub

Private Sub cmdCerrar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    'Unload Me
    Me.Hide
    Call Status("Listo.")
    Call LockProceso(False)
End Sub

Sub setLblPerfil()
    Dim nomArea As String
    nomArea = ""
    
    Select Case xNivel
        Case "BBVA"
            nomArea = "Todo el BBVA"
        Case "DIRE"
            If sp_GetDireccion(xArea) Then
                nomArea = ClearNull(aplRST!nom_direccion)
                aplRST.Close
            End If
        Case "GERE"
            If sp_GetGerencia(xArea, Null) Then
                nomArea = ClearNull(aplRST!nom_gerencia)
                aplRST.Close
            End If
        Case "SECT"
            If sp_GetSector(xArea, Null) Then
                nomArea = ClearNull(aplRST!nom_sector)
                aplRST.Close
            End If
        Case "GRUP"
            If sp_GetGrupo(xArea, Null) Then
                nomArea = ClearNull(aplRST!nom_grupo)
                aplRST.Close
            End If
    End Select
    lblPerfil.Caption = "Perfil: " & getDescPerfil(xPerfil) & "     Nivel: " & getDescNivel(xNivel) & "     Area: " & nomArea
End Sub

Private Sub CargarGrid()
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
    Dim vRetorno() As String
    Dim flgPendientes As Boolean
    Dim auxAgrup As String
    Dim pet_nrointerno As Long

    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    cmdVisualizar.Enabled = False
    cmdGrupo.Enabled = False
    cmdGrupOper.Enabled = False
    cmdFiltro.Enabled = False
    cmdFiltrarNumero.Enabled = False
    If InStr(sEstadoQuery, "PENDIE") > 0 Then
        flgPendientes = True
    Else
        flgPendientes = False
    End If
    'flgEnCarga = True
    With grdDatos
        .visible = False
        .Clear
        DoEvents
        .HighLight = flexHighlightAlways
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .GridColor = &HE0E0E0
        .FocusRect = flexFocusNone
        .cols = colTOTAL_COLUMNAS
        .Rows = 1
        .TextMatrix(0, colEmpresa) = "Emp.": .ColWidth(colEmpresa) = 450: .ColAlignment(colEmpresa) = flexAlignLeftCenter
        .TextMatrix(0, colNroAsignado) = "Petic.": .ColWidth(colNroAsignado) = 750: .ColAlignment(colNroAsignado) = flexAlignRightCenter
        .TextMatrix(0, colNroInterno) = "NroInt.": .ColWidth(colNroInterno) = 0
        .TextMatrix(0, colTipoPet) = "Tipo": .ColAlignment(colTipoPet) = flexAlignLeftCenter: .ColWidth(colTipoPet) = 450
        .TextMatrix(0, colPetClass) = "Clase": .ColWidth(colPetClass) = 600: .ColAlignment(colPetClass) = flexAlignLeftCenter
        .TextMatrix(0, colPetImpTech) = "I.T.": .ColWidth(colPetImpTech) = 500: .ColAlignment(colPetImpTech) = flexAlignLeftCenter
        .TextMatrix(0, colPetRegulatorio) = "Reg.": .ColWidth(colPetRegulatorio) = 500: .ColAlignment(colPetRegulatorio) = flexAlignLeftCenter
        .TextMatrix(0, colPetRO) = "RO": .ColWidth(colPetRO) = 500: .ColAlignment(colPetRO) = flexAlignLeftCenter
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 3400: .ColAlignment(colTitulo) = 0
        .TextMatrix(0, colINFOId) = "Inf. N�": .ColWidth(colINFOId) = 800: .ColAlignment(colINFOId) = flexAlignRightCenter
        .TextMatrix(0, colINFOIdVer) = "Ver.": .ColWidth(colINFOIdVer) = 400: .ColAlignment(colINFOIdVer) = flexAlignRightCenter
        .TextMatrix(0, colINFONom) = "Descripci�n": .ColWidth(colINFONom) = 2000: .ColAlignment(colINFONom) = 0
        .TextMatrix(0, colINFOTipo) = "Tpo.": .ColWidth(colINFOTipo) = 0: .ColAlignment(colINFOTipo) = 0
        .TextMatrix(0, colINFOTipoNom) = "Proceso": .ColWidth(colINFOTipoNom) = 1000: .ColAlignment(colINFOTipoNom) = 0
        .TextMatrix(0, colINFOPuntoControl) = "PC": .ColWidth(colINFOPuntoControl) = 500: .ColAlignment(colINFOPuntoControl) = 0
        .TextMatrix(0, colINFOFechaAlta) = "F.Alta": .ColWidth(colINFOFechaAlta) = 1400: .ColAlignment(colINFOFechaAlta) = 0
        .TextMatrix(0, colINFOHomologador) = "cod_homo": .ColWidth(colINFOHomologador) = 0: .ColAlignment(colINFOHomologador) = 0
        .TextMatrix(0, colINFOHomologaNom) = "Homologador": .ColWidth(colINFOHomologaNom) = 2000: .ColAlignment(colINFOHomologaNom) = 0
        .TextMatrix(0, colINFOEstado) = "Estado": .ColWidth(colINFOEstado) = 0: .ColAlignment(colINFOEstado) = 0
        .TextMatrix(0, colINFOEstadoNom) = "Estado info.": .ColWidth(colINFOEstadoNom) = 1400: .ColAlignment(colINFOEstadoNom) = 0
        .TextMatrix(0, colINFOProtoBase) = "Base": .ColWidth(colINFOProtoBase) = 0: .ColAlignment(colINFOProtoBase) = 0
        .TextMatrix(0, colSolicitante) = "": .ColWidth(colSolicitante) = 0: .ColAlignment(colSolicitante) = 0
        .TextMatrix(0, colSoliNom) = "Solicitante": .ColWidth(colSoliNom) = 2000: .ColAlignment(colSoliNom) = 0
        .TextMatrix(0, colEstCod) = "": .ColWidth(colEstCod) = 0: .ColAlignment(colEstCod) = 0
        .TextMatrix(0, colEstNom) = "Est.Pet.": .ColWidth(colEstNom) = 1600: .ColAlignment(colEstNom) = 0
        .TextMatrix(0, colRefeSis) = "RS": .ColWidth(colRefeSis) = 0: .ColAlignment(colRefeSis) = 0
        .TextMatrix(0, colRefeSisNom) = "Referente de Sist.": .ColWidth(colRefeSisNom) = 2000: .ColAlignment(colRefeSisNom) = 0
        .TextMatrix(0, colTotInfos) = "Cnt.inf.": .ColWidth(colTotInfos) = 700: .ColAlignment(colTotInfos) = flexAlignRightCenter
        .ColWidth(colMultiSelect) = 180
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    
    Call Puntero(True)
    Call Status("Cargando peticiones...")
    
    If flgNumero Then
        If sp_GetUnaPeticionAsig(Val(txtNroBuscar)) Then
            pet_nrointerno = CLng(ClearNull(aplRST.Fields!pet_nrointerno))
        End If
        If Not sp_GetPeticionInformes(pet_nrointerno, Null, Null, Null, Null) Then
            GoTo finx
        End If
    Else
        If Not sp_GetPeticionInformes( _
            Null, _
            CodigoCombo(cboEstado, True), _
            CodigoCombo(cboFiltroHomologador, True), _
            IIf(dtpFechaDesde.CheckBox, dtpFechaDesde.value, Null), _
            IIf(dtpFechaHasta.CheckBox, dtpFechaHasta.value, Null)) Then
            GoTo finx
        End If
    End If
    
    Call Status(aplRST.RecordCount & " peticiones.")
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colEmpresa) = ClearNull(aplRST.Fields!pet_emp)
            .TextMatrix(.Rows - 1, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
            .TextMatrix(.Rows - 1, colNroInterno) = padRight(ClearNull(aplRST!pet_nrointerno), 6)
            .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, colPetClass) = ClearNull(aplRST!cod_clase)
            .TextMatrix(.Rows - 1, colPetImpTech) = ClearNull(aplRST!pet_imptech)
            .TextMatrix(.Rows - 1, colPetRegulatorio) = ClearNull(aplRST!pet_regulatorio)
            .TextMatrix(.Rows - 1, colPetRO) = ClearNull(aplRST!pet_ro)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, colINFOId) = ClearNull(aplRST!info_id)
            .TextMatrix(.Rows - 1, colINFOIdVer) = ClearNull(aplRST!info_idver)
            .TextMatrix(.Rows - 1, colINFONom) = ClearNull(aplRST!infoprot_nom)
            .TextMatrix(.Rows - 1, colINFOTipo) = ClearNull(aplRST!info_tipo)
            .TextMatrix(.Rows - 1, colINFOTipoNom) = ClearNull(aplRST!info_tiponom)
            .TextMatrix(.Rows - 1, colINFOPuntoControl) = ClearNull(aplRST!info_puntonom)
            .TextMatrix(.Rows - 1, colINFOFechaAlta) = ClearNull(aplRST!info_fecha)
            .TextMatrix(.Rows - 1, colINFOHomologador) = ClearNull(aplRST!info_recurso)
            .TextMatrix(.Rows - 1, colINFOHomologaNom) = ClearNull(aplRST!nom_homologador)
            .TextMatrix(.Rows - 1, colINFOEstado) = ClearNull(aplRST!info_estado)
            .TextMatrix(.Rows - 1, colINFOEstadoNom) = ClearNull(aplRST!info_estadonom)
            .TextMatrix(.Rows - 1, colINFOProtoBase) = ClearNull(aplRST!infoprot_id)
            .TextMatrix(.Rows - 1, colSolicitante) = ClearNull(aplRST!cod_solicitante)
            .TextMatrix(.Rows - 1, colSoliNom) = ClearNull(aplRST!nom_solicitante)
            .TextMatrix(.Rows - 1, colEstCod) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colEstNom) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colRefeSis) = ClearNull(aplRST!cod_bpar)
            .TextMatrix(.Rows - 1, colRefeSisNom) = ClearNull(aplRST!nom_bpar)
            .TextMatrix(.Rows - 1, colTotInfos) = ClearNull(aplRST!info_cantidad)
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
        End With
Skip:
        aplRST.MoveNext
        DoEvents
    Loop
finx:
    grdDatos.visible = True
    If glNumeroPeticion <> "" Then
        If grdDatos.Rows > 1 Then
            Call GridSeek(Me, grdDatos, colNroInterno, glNumeroPeticion, True)
            Call MostrarSeleccion(False)
        End If
    Else
        If grdDatos.Rows > 1 Then
            grdDatos.TopRow = 1
            grdDatos.row = 1
            grdDatos.rowSel = 1
            grdDatos.col = 0
            grdDatos.ColSel = grdDatos.cols - 1
        End If
    End If
    cmdFiltro.Enabled = True
    cmdFiltrarNumero.Enabled = True
    flgEnCarga = False
    Call Puntero(False)
    'Call Status("Listo.")
    Call LockProceso(False)
End Sub

Public Sub MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    cmdGrupo.Enabled = False
    'cmdSector.Enabled = False
    cmdGrupOper.Enabled = False
    cmdVisualizar.Enabled = False
    sArea = ""
    With grdDatos
        If .rowSel > 0 Then
            If .TextMatrix(.rowSel, colNroInterno) <> "" Then
                glNroAsignado = ClearNull(.TextMatrix(.rowSel, colNroAsignado))
                glNumeroPeticion = ClearNull(.TextMatrix(.rowSel, colNroInterno))
                'glCodigoRecurso = .TextMatrix(.RowSel, colCodigoRecurso)
                glEstadoPeticion = .TextMatrix(.rowSel, colEstCod)
                'glPeticion_sox001 = .TextMatrix(.RowSel, colPetSOx)     ' add -001- c.
                'sArea = .TextMatrix(.RowSel, colCodHijo)
                cmdVisualizar.Enabled = True
                If xPerfil = "CSEC" And ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
                    'cmdSector.Enabled = True
                    cmdGrupo.Enabled = True
                End If
                If xPerfil = "CGRU" And ClearNull(glNumeroPeticion) <> "" And ClearNull(sArea) <> "" Then
                    cmdGrupOper.Enabled = True
                    cmdGrupo.Enabled = True
                    '{ add -004- a.
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(glEstadoPeticion), vbTextCompare) > 0 Then
                        cmdGrupOper.Enabled = False
                    End If
                    '}
                End If
            End If
            '.HighLight = flexHighlightAlways
        End If
    End With
End Sub

Private Sub grdDatos_Click()
'    If grdDatos.MouseRow = 0 And grdDatos.Rows > 1 Then
'       grdDatos.RowSel = 1
''       If grdDatos.MouseCol = colFinicio Then
''           grdDatos.col = colFinicioSRT
''       ElseIf grdDatos.MouseCol = colFtermin Then
''           grdDatos.col = colFterminSRT
''       Else
''           grdDatos.col = grdDatos.MouseCol
''       End If
'       grdDatos.Sort = flexSortStringNoCaseAscending
'    End If
'    Call MostrarSeleccion
'    DoEvents
    bSorting = True
'    With grdDatos
'        glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
'    End With
    flgEnCarga = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    flgEnCarga = False
    bSorting = False
End Sub

Private Sub grdDatos_DblClick()
    Call MostrarSeleccion
    DoEvents
    If glNumeroPeticion <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoPeticion = "VIEW"
        On Error Resume Next
        frmPeticionesCarga.Show vbModal
        DoEvents
        If Me.Tag = "REFRESH" Then
            InicializarPantalla
        End If
        grdDatos.SetFocus      ' add -002- a.
    End If
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_DblClick
    End If
End Sub
'}

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    If grdDatos.row <= grdDatos.rowSel Then
        nRwD = grdDatos.row
        nRwH = grdDatos.rowSel
    Else
        nRwH = grdDatos.row
        nRwD = grdDatos.rowSel
    End If
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, colMultiSelect) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, colMultiSelect) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        If Button = vbLeftButton Then
            With grdDatos
                If .rowSel > 0 Then
                     If ClearNull(.TextMatrix(.rowSel, colMultiSelect)) = "" Then
                        .TextMatrix(.rowSel, colMultiSelect) = "�"
                     Else
                        .TextMatrix(.rowSel, colMultiSelect) = ""
                    End If
                End If
            End With
        End If
    End If
    If Shift = 0 Then
        If Button = vbLeftButton Then
            With grdDatos
                For i = 1 To .Rows - 1
                   .TextMatrix(i, colMultiSelect) = ""
                Next
                If .rowSel > 0 Then
                     .TextMatrix(.rowSel, colMultiSelect) = "�"
                End If
            End With
        End If
    End If
End Sub
 
Private Sub grdDatos_RowColChange()
    If flgInMultiselect Then Exit Sub
    If Not flgEnCarga Then Call MostrarSeleccion
End Sub

'{ add -002- c.
Private Sub txtNroBuscar_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cmdFiltrarNumero_Click
    End If
End Sub

Private Sub cboEstado_Click()
    If Not flgEnCarga Then Call cmdFiltro_Click
End Sub

Private Sub cmdPrintBandeja_Click()
    'glCrystalNewVersion = True         ' Esto es para pruebas en desarrollo
    If glCrystalNewVersion Then         ' Nueva versi�n
        Call ReporteNuevo
    Else
        'Call ReporteAnterior
    End If
End Sub

Private Sub ReporteNuevo()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    ' Auxiliares para los titulos y subtitulos
    Dim cTitulo As String
    Dim cSubTitulo As String

    cTitulo = Trim(lblPerfil)
    cSubTitulo = "Estados filtrados: " & Trim(Mid(cboEstado, 1, InStr(1, cboEstado, "||", vbTextCompare) - 1))
    
    Select Case xPerfil
        Case "BPAR": sReportName = "petperfilbp6.rpt"
        'Case "SOLI", "REFE", "AUTO", "SUPE": sReportName = "petperfilsoli6.rpt"
        Case Else
            sReportName = "petperfil6.rpt"
    End Select

    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Peticiones en Bandeja de trabajo por perfil"
        .ReportComments = cTitulo & vbCrLf & cSubTitulo
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        If InStr(1, "BPAR", xPerfil, vbTextCompare) > 0 Then
            Select Case crParamDef.ParameterFieldName
                Case "@pet_nroasignado": crParamDef.AddCurrentValue (0)
                Case "@cod_bpar": crParamDef.AddCurrentValue (glLOGIN_ID_REEMPLAZO)
                Case "@cod_estado": crParamDef.AddCurrentValue (IIf(sEstadoQuery = "NULL", " ", sEstadoQuery))
                Case "@anx_especial": crParamDef.AddCurrentValue ("S")
                Case "@agr_nrointerno": crParamDef.AddCurrentValue (0)
                Case "@prio_ejecuc": crParamDef.AddCurrentValue (0)
            End Select
        Else
            Select Case crParamDef.ParameterFieldName
                Case "@pet_nroasignado": crParamDef.AddCurrentValue (0)
                Case "@modo_recurso": crParamDef.AddCurrentValue (xPerfil)
                Case "@cod_nivel": crParamDef.AddCurrentValue (xNivel)
                Case "@cod_area": crParamDef.AddCurrentValue (xArea)
                Case "@cod_estado": crParamDef.AddCurrentValue (IIf(sEstadoQuery = "NULL", " ", sEstadoQuery))
                Case "@anx_especial": crParamDef.AddCurrentValue ("S")
                Case "@agr_nrointerno": crParamDef.AddCurrentValue (0)
                Case "@prio_ejecuc": crParamDef.AddCurrentValue (0)
            End Select
        End If
    Next
 
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub cmdGenerarInformes_Click()
    Dim i As Long
    Dim J As Long
    Dim vInternoPeticionNroInterno() As Long
    Dim vInternoPeticionNroAsignado() As Long
    Dim sUbicacion As String
    
    Erase vPeticionNroInterno
    Erase vPeticionInforme
    Erase vNombreInforme
    
    Erase vInternoPeticionNroInterno
    Erase vInternoPeticionNroAsignado
    
    'If ClearNull(sUbicacionAnterior) <> "" Then sUbicacion = Trim(sUbicacionAnterior)
    'sUbicacion = ""
    'sUbicacion = Dialogo_EstablecerUbicacion("Seleccione la ubicaci�n para generar los archivos...", Environ$("HOMESHARE"))
    sUbicacion = Dialogo_EstablecerUbicacion("Seleccione la ubicaci�n para generar los archivos...", sUbicacion)
    'sUbicacionAnterior = sUbicacion
    If Trim(sUbicacion) <> "" Then
        Call Puntero(True)
        J = 0
'        With grdDatos
'            For i = 1 To .Rows - 1
'                If .TextMatrix(i, colMultiSelect) = "�" Then
'                    ReDim Preserve vPeticionNroInterno(j)
'                    ReDim Preserve vPeticionInforme(j)
'                    ReDim Preserve vNombreInforme(j)
'                    vPeticionNroInterno(j) = .TextMatrix(i, colNroInterno)
'                    vPeticionInforme(j) = .TextMatrix(i, colINFOId)
'                    vNombreInforme(j) = ClearNull(.TextMatrix(i, colNroAsignado)) & " - " & ClearNull(.TextMatrix(i, colINFONom)) & " " & .TextMatrix(i, colINFOId) & " " & .TextMatrix(i, colINFOTipoNom)
'                    j = j + 1
'                End If
'            Next i
'        End With
        
        ' Primero obtengo todos los nro. de petici�n para de cada una de ellas obtener todos los informes.
        With grdDatos
            For i = 1 To .Rows - 1
                If .TextMatrix(i, colMultiSelect) = "�" Then
                    ReDim Preserve vInternoPeticionNroInterno(J)
                    ReDim Preserve vInternoPeticionNroAsignado(J)
                    vInternoPeticionNroInterno(J) = .TextMatrix(i, colNroInterno)
                    vInternoPeticionNroAsignado(J) = .TextMatrix(i, colNroAsignado)
                    'vInternoPeticionTitulo(j) = CStr(.TextMatrix(i, colNroAsignado))
                    J = J + 1
                End If
            Next i
        End With
        
        J = 0
        For i = 0 To UBound(vInternoPeticionNroInterno)
            If sp_GetPeticionInfohc(vInternoPeticionNroInterno(i), Null, Null, Null, "VERIFI|TERMIN|") Then
                Do While Not aplRST.EOF
                    ReDim Preserve vPeticionNroInterno(J)
                    ReDim Preserve vPeticionInforme(J)
                    ReDim Preserve vNombreInforme(J)
                    vPeticionNroInterno(J) = ClearNull(aplRST.Fields!pet_nrointerno)
                    vPeticionInforme(J) = ClearNull(aplRST.Fields!info_id)
                    vNombreInforme(J) = vInternoPeticionNroAsignado(i) & " - " & Replace(ClearNull(aplRST.Fields!infoprot_nom), "/", "-", 1) & " " & ClearNull(aplRST.Fields!info_id) & " " & ClearNull(aplRST.Fields!info_tipodsc)
                    J = J + 1
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next i
        
        For i = 0 To UBound(vPeticionNroInterno)
            Call Status("Generando informe: " & vPeticionInforme(i))
            Call ExportarInforme(sUbicacion, vNombreInforme(i), vPeticionNroInterno(i), vPeticionInforme(i))
            DoEvents
        Next i
        MsgBox "Proceso finalizado." & vbCrLf & vbCrLf & "Se generaron los archivos en: " & vbCrLf & sUbicacion, vbInformation + vbOKOnly
        Call Puntero(False)
        Call Status("Listo.")
    End If
End Sub

Private Sub ExportarInforme(sUbicacion, sNombre, pet_nrointerno, informeId)
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    
    Call Puntero(True)
    sReportName = "\petinfoh16.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crPortrait
        .ReportTitle = "Informe de homologaci�n"
        .ReportComments = "Informe de homologaci�n"        ' "Peticiones en estado terminal desde " & Format(fe_desde, "dd/mm/yyyy") & " y " & Format(fe_hasta, "dd/mm/yyyy")
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    Set crParamDefs = crReport.ParameterFields              ' Parametros del reporte
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@pet_nrointerno": crParamDef.AddCurrentValue (Val(pet_nrointerno))
            Case "@info_id": crParamDef.AddCurrentValue (Val(informeId))
            Case "@info_idver": crParamDef.AddCurrentValue (Val(0))
            Case "@info_req": crParamDef.AddCurrentValue ("S")
        End Select
    Next
    ' Subreporte de observaciones
    Set crParamDefs = crReport.OpenSubreport("HomoObservaciones").ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@pet_nrointerno": crParamDef.AddCurrentValue (Val(pet_nrointerno))
            Case "@info_id": crParamDef.AddCurrentValue (Val(informeId))
            Case "@info_idver": crParamDef.AddCurrentValue (Val(0))
        End Select
    Next
    
    ' Subreporte de responsables
    Set crParamDefs = crReport.OpenSubreport("HomoResponsables").ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@pet_nrointerno": crParamDef.AddCurrentValue (Val(pet_nrointerno))
            Case "@info_id": crParamDef.AddCurrentValue (Val(informeId))
            Case "@info_idver": crParamDef.AddCurrentValue (Val(0))
        End Select
    Next
    
    With crReport.ExportOptions
        .DestinationType = crEDTDiskFile
        '.DiskFileName = Environ$("HOMESHARE") & "\Pepe.pdf"
        .DiskFileName = IIf(sUbicacion = "", Environ$("HOMESHARE") & "\" & sNombre & ".pdf", sUbicacion & "\" & sNombre & ".pdf")
        .FormatType = crEFTPortableDocFormat
        .PDFExportAllPages = True
    End With
    crReport.Export (False)
    Call Puntero(False)
 
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub cmdRptOmeOma_Click()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    ' Auxiliares para los titulos y subtitulos
    Dim cTitulo As String
    Dim cSubTitulo As String

    cTitulo = "Informe de OMEs y OMAs en peticiones"
    cSubTitulo = ""
    
    sReportName = "infohomo16.rpt"

    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = cTitulo
        .ReportComments = cTitulo & IIf(Len(Trim(cSubTitulo)) > 0, vbCrLf & cSubTitulo, "")
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    'Abrir el reporte
    Call Puntero(True)

    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

''{ add -010- a.
'Private Sub ReporteAnterior()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String
'    Dim cSubTitulo As String
'
'    Call Status("Preparando el informe...")
'    Call Puntero(True)
'
'    cTitulo = Trim(lblPerfil)
'    cSubTitulo = "Estados filtrados: " & Trim(Mid(cboEstado, 1, InStr(1, cboEstado, "||", vbTextCompare) - 1))
'
'    With mdiPrincipal.CrystalReport1
'        Select Case xPerfil
'            Case "BPAR": cPathFileNameReport = App.Path & "\" & RPT_PERFIL_BP
'            Case "SOLI", "REFE", "AUTO", "SUPE": cPathFileNameReport = App.Path & "\" & RPT_PERFIL_SOLI
'            Case Else
'                cPathFileNameReport = App.Path & "\" & RPT_PERFIL
'        End Select
'        'If InStr(1, "BPAR", xPerfil, vbTextCompare) > 0 Then
'        '    cPathFileNameReport = App.Path & "\" & RPT_PERFIL_BP
'        'ElseIf InStr(1, "SOLI|REFE|AUTO|SUPE", xPerfil, vbTextCompare) > 0 Then
'        '    cPathFileNameReport = App.Path & "\" & RPT_PERFIL_SOLI
'        'Else
'        '    cPathFileNameReport = App.Path & "\" & RPT_PERFIL
'        'End If
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .RetrieveStoredProcParams
'        If InStr(1, "BPAR", xPerfil, vbTextCompare) > 0 Then
'            .StoredProcParam(0) = 0
'            .StoredProcParam(1) = glLOGIN_ID_REEMPLAZO
'            .StoredProcParam(2) = IIf(sEstadoQuery = "NULL", " ", sEstadoQuery)
'            .StoredProcParam(3) = "S"
'            .StoredProcParam(4) = 0
'            .StoredProcParam(5) = 0
'        Else
'            .StoredProcParam(0) = 0
'            .StoredProcParam(1) = xPerfil
'            .StoredProcParam(2) = xNivel
'            .StoredProcParam(3) = xArea
'            .StoredProcParam(4) = IIf(sEstadoQuery = "NULL", " ", sEstadoQuery)
'            .StoredProcParam(5) = "S"
'            .StoredProcParam(6) = 0
'            .StoredProcParam(7) = 0
'        End If
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & cTitulo & "'"
'        .Formulas(3) = "@1_TITULO='" & cSubTitulo & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Call Puntero(False)
'        Call Status("Listo.")
'    End With
'End Sub

