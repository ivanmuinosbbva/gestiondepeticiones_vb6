VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form selMultipleFile 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selecci�n de archivos para adjuntar a la petici�n"
   ClientHeight    =   8580
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10440
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "selFile"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8580
   ScaleWidth      =   10440
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.ProgressBar pbAdjuntarArchivos 
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   8280
      Visible         =   0   'False
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   450
      _Version        =   327682
      Appearance      =   0
   End
   Begin VB.ComboBox cmbDocType 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   240
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   6720
      Width           =   5415
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9360
      TabIndex        =   4
      Top             =   8160
      Width           =   975
   End
   Begin VB.FileListBox objFile 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7290
      Left            =   5880
      MultiSelect     =   2  'Extended
      TabIndex        =   3
      Top             =   120
      Width           =   4485
   End
   Begin VB.DirListBox objPath 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5130
      Left            =   120
      TabIndex        =   2
      Top             =   510
      Width           =   5655
   End
   Begin VB.DriveListBox objDrive 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   5655
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   0
      Top             =   8160
      Width           =   975
   End
   Begin VB.Line Line1 
      X1              =   10335
      X2              =   135
      Y1              =   7920
      Y2              =   7920
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      X1              =   10335
      X2              =   135
      Y1              =   7930
      Y2              =   7930
   End
   Begin VB.Label lblPrjTexto 
      AutoSize        =   -1  'True
      Caption         =   "Adjuntando... aguarde por favor"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   180
      Left            =   120
      TabIndex        =   14
      Top             =   8040
      Visible         =   0   'False
      Width           =   2340
   End
   Begin VB.Label lblPrjLey 
      AutoSize        =   -1  'True
      Caption         =   "lblPrjLey"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   180
      Left            =   5880
      TabIndex        =   13
      Top             =   8280
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Label lblTipoAdjunto 
      AutoSize        =   -1  'True
      Caption         =   "Tipo de Adjunto"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   180
      Left            =   240
      TabIndex        =   12
      Top             =   6480
      Width           =   1200
   End
   Begin VB.Shape Shape2 
      BorderColor     =   &H00808080&
      Height          =   2110
      Left            =   120
      Top             =   5700
      Width           =   5655
   End
   Begin VB.Label lblMultipleAyuda 
      Caption         =   "lblMultipleAyuda"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   240
      TabIndex        =   10
      Top             =   7080
      Width           =   5325
   End
   Begin VB.Label lblExplicacion 
      Caption         =   "lblExplicacion"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   9
      Top             =   5760
      Width           =   5370
   End
   Begin VB.Label lblFileSize 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "lblFileSize"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   180
      Left            =   9435
      TabIndex        =   7
      Top             =   7530
      Width           =   750
   End
   Begin VB.Label lblFileSelection 
      AutoSize        =   -1  'True
      Caption         =   "Tama�o del archivo seleccionado:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   180
      Left            =   6000
      TabIndex        =   6
      Top             =   7530
      Width           =   2565
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00808080&
      Height          =   375
      Left            =   5880
      Top             =   7440
      Width           =   4455
   End
   Begin VB.Label lblWarning 
      Caption         =   "lblWarning"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   8280
      Visible         =   0   'False
      Width           =   5535
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "selMultipleFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -001- b. FJS 13.08.2007 - Se agrega un mensaje de advertencia para indicar que un archivo posible de seleccionar esta vacio (esto generar� un error e impedir� adjuntar dicho archivo).
' -002- a. FJS 21.07.2008 - Se agrega la visualizaci�n del tama�o en KB del archivo seleccionado.
' -003- a. FJS 27.08.2008 - Se mejora el mensaje de error al cambiar la unidad de disco o red.
' -004- a. FJS 03.10.2008 - Se adapta para multiple selecci�n de documentos.
' -005- a. FJS 17.03.2009 - Se guarda la �ltima ruta de asignaci�n exitosa.
' -006- a. FJS 23.03.2009 - Se modifica para que ante la primera cancelaci�n de agregado de documentos, deje de intentar adjuntar los documentos.
' -007- a. FJS 20.09.2010 - Bug: no debe ejecutarse esto cuando el documento es un CG04 y adem�s, debe registrarse en el historial el evento.

Option Explicit

Dim m_Capture As Boolean
Dim m_Drive As String
Dim m_Path As String
Dim m_File As String
Dim m_PathFile As String
Dim m_Pattern As String
Dim m_Caption As String
Dim m_Level As Integer
Dim m_Network As Boolean
Dim bModoMultiple As Boolean        ' add -004- a.

Dim cMensajeError As String

Private Sub Form_Load()
    Dim sPath As String
    On Error Resume Next
    fjsNewStatusMessage Trim(Me.name), 7    ' add -001- a.
    lblFileSize.Caption = ""    ' add -002- a.
    If m_Path <> "" Then
        m_Drive = extractDrive(m_Path)
    End If
    sPath = m_Path
    If Not m_Network Then
        objDrive.Drive = m_Drive
        If Err > 0 Then
            objDrive.Drive = "c:"
        End If
    End If
    objFile.Pattern = m_Pattern
    objPath.Path = sPath
    If Err > 0 Then
        objPath.Path = "C:\"
    End If
    If Not m_Network Then
        m_Drive = objDrive.Drive
    End If
    m_Path = objPath.Path
    objFile.Path = objPath.Path
    If Val(m_Level) = 0 Then
        m_Level = 3
    End If
    If Val(m_Level) < 3 Then
        objFile.Visible = False
    End If
    If Val(m_Level) < 2 Then
        objPath.Visible = False
    End If
    If m_Network Then
        objDrive.Visible = False
    End If
    If m_Caption <> "" Then
        Me.Caption = m_Caption
    End If
    cmdAceptar.Enabled = False
    '{ add -004- a.
    lblExplicacion.Caption = "Ahora puede seleccionar m�ltiples archivos para adjuntar a la petici�n. Para hacerlo, debe seleccionar cada archivo mientras mantiene presionada la tecla CTRL (uno a uno) o SHIFT (para rangos)."
    lblMultipleAyuda.Caption = "Debe seleccionar el tipo de adjunto que ser� aplicado a todos los archivos seleccionados (todos deben ser del mismo tipo) o seleccionar la opci�n Asociaci�n Autom�tica de Tipos."
    Call setHabilCtrl(cmbDocType, "DIS")
    With cmbDocType
        .Clear
        .AddItem "Asociaci�n Autom�tica de Tipos"
        .AddItem "C102 : Dise�o de soluci�n"
        .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
        .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
        .AddItem "C204 : Casos de prueba"
        .AddItem "CG04 : Documento de cambio de alcance"
        .AddItem "T700 : Plan de implantaci�n Desarrollo"
        .AddItem "T710 : Principios de Desarrollo Aplicativo"
        .AddItem "C310 : Definici�n de Base de Datos"
        .AddItem "EML1 : Mail de OK alcance"
        .AddItem "EML2 : Mail de OK prueba de usuario"
        .AddItem "OTRO : Otros documentos"
        .ListIndex = 0
    End With
    bModoMultiple = False
    '}
End Sub

Public Property Let prpNetwork(ByVal NewVal As Boolean)
    m_Network = NewVal
End Property

Public Property Get prpNetwork() As Boolean
    prpNetwork = m_Network
End Property

Public Property Let prpCaption(ByVal NewVal As String)
    m_Caption = NewVal
End Property

Public Property Let prpLevel(ByVal NewVal As Integer)
    m_Level = NewVal
End Property

Public Property Get prpPattern() As String
    prpPattern = m_Pattern
End Property

Public Property Let prpPattern(ByVal NewVal As String)
    m_Pattern = NewVal
End Property

Public Property Get prpDrive() As String
    prpDrive = m_Drive
End Property

Public Property Let prpDrive(ByVal NewVal As String)
    m_Drive = NewVal
End Property

Public Property Get prpPath() As String
    prpPath = m_Path
End Property

Public Property Let prpPath(ByVal NewVal As String)
    m_Path = NewVal
End Property

Public Property Get prpFile() As String
    prpFile = m_File
End Property

Public Property Let prpFile(ByVal NewVal As String)
    m_File = NewVal
End Property

Public Property Get prpPathFile() As String
    prpPathFile = m_PathFile
End Property

Public Property Let prpPathFile(ByVal NewVal As String)
    m_PathFile = NewVal
End Property

Private Sub cmdAceptar_Click()
    Dim cMensaje As String      ' add -004- a.
    
    ' Por seguridad se inhabilitan los botones hasta que finalice el proceso.
    cmdAceptar.Enabled = False
    cmdCancelar.Enabled = False
    
    '{ add -001- b.
    'If Len(lblWarning) > 0 Then
    '    MsgBox "Esta intentando adjuntar un archivo sin datos (0 Kb. de tama�o) a la petici�n", vbExclamation + vbOKOnly, "Adjunto sin datos"
    'End If
    '}
    '{ add -004- a. Cargo el vector con los archivos seleccionados (as� sea solo uno)
    Dim x As Integer
    Dim y As Integer
    
    cMensajeError = ""
    With objFile
        Erase cArchivos
        For y = 0 To .ListCount - 1
            If .Selected(y) Then
                ReDim Preserve cArchivos(x)
                cArchivos(x).Nombre = .List(y)
                cArchivos(x).Tipo = TipoDeAdjunto(.List(y))
                cArchivos(x).Extension = ExtensionDelAdjunto(.List(y))
                cArchivos(x).Tamanio = FileLen(objFile.Path & "\" & .List(y))
                cArchivos(x).Ubicacion = objFile.Path & "\"
                x = x + 1
            End If
        Next y
    End With
    If bModoMultiple Then
        If cmbDocType.ListIndex > 0 Then
            ' Modo: selecci�n por tipo �nico
            If Not ControlDeTipos Then
                MsgBox "Los archivos seleccionados no pertenecen al mismo tipo de adjunto seleccionado. Revise.", vbExclamation + vbOKOnly, "M�ltiples tipos"
            Else
                If ValidarArchivos Then
                    GuardarArchivos
                    SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", prpPath  ' add -005- a.
                    Unload Me
                Else
                    MsgBox cMensajeError, vbExclamation + vbOKOnly, "Problemas encontrados"
                End If
            End If
            ' Se reestablece la habilitaci�n de los botones de operaci�n
            cmdAceptar.Enabled = True
            cmdCancelar.Enabled = True
        Else
            ' Modo: Asociaci�n Autom�tica de Tipos
            cMensaje = ""
            If ValidarArchivos Then
                For x = 0 To UBound(cArchivos)
                    cMensaje = cMensaje & "Tipo : " & cArchivos(x).Tipo & "  ==>  Archivo : " & cArchivos(x).Nombre & vbCrLf
                Next x
                cMensaje = cMensaje & vbCrLf & "�Es correcta la asociaci�n autom�tica?"
                If MsgBox(cMensaje, vbQuestion + vbYesNo, "Asociaci�n autom�tica de Tipos") = vbYes Then
                    GuardarArchivos
                    SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", prpPath  ' add -005- a.
                    Unload Me
                End If
            Else
                MsgBox cMensajeError, vbExclamation + vbOKOnly, "Problemas encontrados"
            End If
            ' Se reestablece la habilitaci�n de los botones de operaci�n
            cmdAceptar.Enabled = True
            cmdCancelar.Enabled = True
        End If
    Else
        If ValidarArchivos Then
            GuardarArchivos
            SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", prpPath  ' add -005- a.
            Unload Me
        Else
            MsgBox cMensajeError, vbExclamation + vbOKOnly, "Problemas encontrados"
        End If
        ' Se reestablece la habilitaci�n de los botones de operaci�n
        cmdAceptar.Enabled = True
        cmdCancelar.Enabled = True
    End If
    '}
End Sub

Private Sub cmdCancelar_Click()
    m_Drive = ""
    m_Path = ""
    m_File = ""
    m_PathFile = ""
    ' Guardo la �ltima ubicaci�n de red de los archivos adjuntos
    'SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", prpPath
    Unload Me
End Sub

Private Sub objDrive_Change()
    On Error GoTo Errores
    objPath.Path = objDrive.Drive
    m_Drive = objDrive.Drive
    m_Path = "\"
    m_File = ""
    m_PathFile = ""
    CheckFileSize ""    ' add -001- b.
    Exit Sub
Errores:
    '{ add -003- a.
    If Err.Number = 68 Then
        If InStr(1, UCase(objDrive.Drive), "A:", vbTextCompare) > 0 Then
            If MsgBox("Asegurese de poner un disco en la disquetera. Puede reintentar o cancelar.", vbCritical + vbRetryCancel, "Error al acceder al dispositivo") = vbRetry Then
                Resume
            Else
                objDrive.Drive = m_Drive
                objPath.Path = "C:\"
                Resume Next
            End If
        Else
            If MsgBox("Asegurese que la unidad seleccionada exista o se encuentre accesible. Puede reintentar o cancelar.", vbCritical + vbRetryCancel, "Error al acceder al dispositivo") = vbRetry Then
                Resume
            Else
                objDrive.Drive = m_Drive
                objPath.Path = "C:\"
                Resume Next
            End If
        End If
    End If
    '}
End Sub

Private Sub objPath_Change()
    objFile.Path = objPath.Path
    m_Path = objPath.Path
    m_File = ""
    m_PathFile = ""
    cmdAceptar.Enabled = False
    CheckFileSize ""    ' add -001- b.
End Sub

Private Sub objFile_Click()
    
    'cmdAceptar_Click
    
    m_Path = objFile.Path
    m_File = objFile.filename
    'If m_File = "" Then
    '    Exit Sub
    'End If
    If Right(m_Path, 1) <> "\" Then
        m_Path = m_Path & "\"
    End If
    m_PathFile = m_Path & m_File
    cmdAceptar.Enabled = True
    '{ add -002- a.
    If FileLen(m_PathFile) < 1024 Then
        lblFileSize = "< 1 KB"
    Else
        lblFileSize = Format(Int(FileLen(m_PathFile) / 1024), "###,###,###,##0") & " KB"
    End If
    '}
    CheckFileSize m_PathFile    ' add -001- b.
    DeterminarModo              ' add -004- a.
End Sub

Private Sub objFile_DblClick()
    cmdAceptar_Click
    '{ add -002- a.
    '{ nuevo nuevo
    m_Path = objFile.Path
    m_File = objFile.filename
    'If m_File = "" Then
    '    Exit Sub
    'End If
    If Right(m_Path, 1) <> "\" Then
        m_Path = m_Path & "\"
    End If
    m_PathFile = m_Path & m_File
    '} hasta aca
    
    If FileLen(m_PathFile) < 1024 Then
        lblFileSize = "< 1 KB"
    Else
        lblFileSize = Format(Int(FileLen(m_PathFile) / 1024), "###,###,###,##0") & " KB"
    End If
    '}
    'm_Path = objFile.Path
    'm_File = objFile.filename
    'If m_File = "" Then
    '    Exit Sub
    'End If
    'If Right(m_Path, 1) <> "\" Then
    '    m_Path = m_Path & "\"
    'End If
    'm_PathFile = m_Path & m_File
    'CheckFileSize m_PathFile    ' add -001- b.
    'Unload Me
End Sub

'{ add -001- b.
Private Sub CheckFileSize(sFileToCheck As String)
    If Len(sFileToCheck) > 0 Then
        If FileLen(sFileToCheck) <= 0 Then
            With lblWarning
                .Caption = "Atenci�n: el archivo seleccionado esta vacio (0 KB)"
                .Visible = True
            End With
        Else
            With lblWarning
                .Caption = ""
                .Visible = False
            End With
        End If
    Else
        With lblWarning
            .Caption = ""
            .Visible = False
        End With
    End If
End Sub
'}

Public Function extractDrive(sPath As String) As String
    If InStr(1, sPath, ":") = 0 Then
        extractDrive = ""
    Else
        extractDrive = Trim(Left(sPath, InStr(1, sPath, ":") - 1)) & ":\"
    End If
End Function

'{ add -002- b.
Private Sub Form_Activate()
    StatusBarObjectIdentity Me
End Sub
'}

'{ add -004- a.
Private Sub DeterminarModo()
    Dim i As Integer
    Dim x As Integer
    
    x = 0
    Call setHabilCtrl(cmbDocType, "DIS")
    bModoMultiple = False
    lblFileSelection = "Tama�o del archivo seleccionado:"
    
    For i = 0 To objFile.ListCount - 1
        If objFile.Selected(i) Then
            x = x + 1
            If x > 1 Then
                Call setHabilCtrl(cmbDocType, "NOR")
                bModoMultiple = True
                lblFileSelection = "Tama�o del �ltimo archivo seleccionado:"
                Exit For
            End If
        End If
    Next i
End Sub

Public Function ExtensionDelAdjunto(cArchivo As String) As String
    Dim i As Long
    Dim lPos As Long
    Dim cAux As String
    
    For i = Len(cArchivo) To 1 Step -1
        cAux = cAux & Mid(cArchivo, i, 1)
    Next i
    If InStr(1, cAux, ".", vbTextCompare) > 0 Then
        lPos = Len(cArchivo) - InStr(1, cAux, ".", vbTextCompare) + 1
        ExtensionDelAdjunto = Mid(cArchivo, lPos + 1)
    Else
        ExtensionDelAdjunto = ""
    End If
    'ExtensionDelAdjunto = cAux
End Function

Private Function TipoDeAdjunto(cArchivo As String) As String
    ' Por defecto
    TipoDeAdjunto = "OTRO"
    ' Evaluaciones por cada tipo
    If InStr(1, cArchivo, "C102", vbTextCompare) > 0 Then
        TipoDeAdjunto = "C102"
    ElseIf InStr(1, cArchivo, "C100", vbTextCompare) > 0 Then
        TipoDeAdjunto = "C100"
    ElseIf InStr(1, cArchivo, "P950", vbTextCompare) > 0 Then
        TipoDeAdjunto = "P950"
    ElseIf InStr(1, cArchivo, "C204", vbTextCompare) > 0 Then
        TipoDeAdjunto = "C204"
    ElseIf InStr(1, cArchivo, "CG04", vbTextCompare) > 0 Then
        TipoDeAdjunto = "CG04"
    ElseIf InStr(1, cArchivo, "T700", vbTextCompare) > 0 Then
        TipoDeAdjunto = "T700"
    ElseIf InStr(1, cArchivo, "T710", vbTextCompare) > 0 Then
        TipoDeAdjunto = "T710"
    ElseIf InStr(1, cArchivo, "C310", vbTextCompare) > 0 Then
        TipoDeAdjunto = "C310"
    ElseIf InStr(1, cArchivo, "EML1", vbTextCompare) > 0 Then
        TipoDeAdjunto = "EML1"
    ElseIf InStr(1, cArchivo, "EML2", vbTextCompare) > 0 Then
        TipoDeAdjunto = "EML2"
    ' Esto es para Homologaci�n
    ElseIf InStr(1, cArchivo, "IDH1", vbTextCompare) > 0 Then
        TipoDeAdjunto = "IDH1"
    ElseIf InStr(1, cArchivo, "IDH2", vbTextCompare) > 0 Then
        TipoDeAdjunto = "IDH2"
    ElseIf InStr(1, cArchivo, "IDH3", vbTextCompare) > 0 Then
        TipoDeAdjunto = "IDH3"
    ElseIf InStr(1, cArchivo, "IDH", vbTextCompare) > 0 Then
        If InStr(1, cArchivo, "SOB", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH1"
        ElseIf InStr(1, cArchivo, "OME", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH2"
        ElseIf InStr(1, cArchivo, "OMA", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH3"
        End If
    ElseIf InStr(1, cArchivo, "IHB", vbTextCompare) > 0 Then
        If InStr(1, cArchivo, "SOB", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH1"
        ElseIf InStr(1, cArchivo, "OME", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH2"
        ElseIf InStr(1, cArchivo, "OMA", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH3"
        End If
    ElseIf InStr(1, cArchivo, "IHC", vbTextCompare) > 0 Then
        If InStr(1, cArchivo, "SOB", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH1"
        ElseIf InStr(1, cArchivo, "OME", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH2"
        ElseIf InStr(1, cArchivo, "OMA", vbTextCompare) > 0 Then
            TipoDeAdjunto = "IDH3"
        End If
    End If
End Function

Private Function ControlDeTipos() As Boolean
    Dim i As Integer
    
    For i = 0 To UBound(cArchivos)
        If Not InStr(1, cArchivos(i).Nombre, CodigoCombo(cmbDocType, False), vbTextCompare) > 0 Then
            ControlDeTipos = False
            Exit Function
        End If
    Next i
    ControlDeTipos = True
End Function

Private Function ValidarArchivos() As Boolean
    Dim i As Integer
    
    ValidarArchivos = True

    ' Comprobaci�n de tama�o m�ximo
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).Tamanio > ARCHIVOS_ADJUNTOS_MAXSIZE Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " supera el tama�o m�ximo permitido por la aplicaci�n."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " supera el tama�o m�ximo permitido por la aplicaci�n."
            End If
            ValidarArchivos = False
            Exit For
            'Exit Function
        End If
    Next i
    
    ' Comprobaci�n de tama�o m�nimo
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).Tamanio < 1 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " est� vacio. No puede ser adjuntado."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " est� vacio. No puede ser adjuntado."
            End If
            ValidarArchivos = False
            Exit For
            'Exit Function
        End If
    Next i
    
    ' Comprobaci�n de largo del nombre del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Nombre) > 100 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El nombre del archivo " & cArchivos(i).Nombre & " excede los 100 caracteres. No puede ser adjuntado."
            Else
                cMensajeError = "El nombre del archivo " & cArchivos(i).Nombre & " excede los 100 caracteres. No puede ser adjuntado."
            End If
            ValidarArchivos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de largo del path del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Ubicacion) > 250 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser adjuntado."
            Else
                cMensajeError = "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser adjuntado."
            End If
            ValidarArchivos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de or�gen
    For i = 0 To UBound(cArchivos)
        If Dir(cArchivos(i).Ubicacion & cArchivos(i).Nombre) = "" Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            End If
            ValidarArchivos = False
            Exit For
        End If
    Next i
    
    ' NUEVO
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de destino (control de duplicidad)
    For i = 0 To UBound(cArchivos)
        If sp_GetAdjuntosPet(glNumeroPeticion, cArchivos(i).Tipo, Null, Null) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!adj_file) = cArchivos(i).Nombre Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & "El archivo " & cArchivos(i).Nombre & " ya existe adjuntado a esta petici�n."
                    Else
                        cMensajeError = "El archivo " & cArchivos(i).Nombre & " ya existe adjuntado a esta petici�n."
                    End If
                    ValidarArchivos = False
                    Exit Do
                Else
                    aplRST.MoveNext
                End If
                DoEvents
            Loop
        End If
    Next i
        
    If glPETModelo Then
        ' Control de documentos de alcance
        For i = 0 To UBound(cArchivos)
            If InStr(1, "P950|C100", cArchivos(i).Tipo, vbTextCompare) > 0 Then
                If glPETType = "PRJ" Or glPETImpTech = "S" Then
                    If cArchivos(i).Tipo <> "C100" Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Por el tipo y/o clase, la petici�n debe informar el documento de alcance tipo C100."
                        Else
                            cMensajeError = "Por el tipo y/o clase, la petici�n debe informar el documento de alcance tipo C100."
                        End If
                        ValidarArchivos = False
                        Exit For
                        'Exit Function
                    End If
                Else
                    If InStr(1, "EVOL|OPTI|NUEV", glPETClass, vbTextCompare) > 0 Then
                        If cArchivos(i).Tipo <> "P950" Then
                            If Len(cMensajeError) > 0 Then
                                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Por el tipo y/o clase de la petici�n, el documento de alcance requerido para la misma es de tipo P950."
                            Else
                                cMensajeError = "Por el tipo y/o clase de la petici�n, el documento de alcance requerido para la misma es de tipo P950."
                            End If
                            ValidarArchivos = False
                            Exit For
                            'Exit Function
                        End If
                    Else
                        ' Si por la clase no corresponde documento de alcance, entonces solo si el
                        ' usuario intenta agregar un C100 lo detengo
                        If cArchivos(i).Tipo = "C100" Then
                            If Len(cMensajeError) > 0 Then
                                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "La petici�n, por su tipo y/o clase, no requiere documento de alcance," & vbCrLf & "pero si desea agregar uno, debe ser de tipo P950."
                            Else
                                cMensajeError = "La petici�n, por su tipo y/o clase, no requiere documento de alcance," & vbCrLf & "pero si desea agregar uno, debe ser de tipo P950."
                            End If
                            ValidarArchivos = False
                            Exit For
                            'Exit Function
                        End If
                    End If
                End If
            End If
        Next i
        ' Control de documento CG04: Pruebas del Usuario
        For i = 0 To UBound(cArchivos)
            If InStr(1, "CG04", cArchivos(i).Tipo, vbTextCompare) > 0 Then
                If glPETEstado <> "EJECUC" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo puede adjuntar un CG04 cuando la petici�n de encuentra en estado EN EJECUCION."
                    Else
                        cMensajeError = "Solo puede adjuntar un CG04 cuando la petici�n de encuentra en estado EN EJECUCION."
                    End If
                    ValidarArchivos = False
                    Exit For
                Else
                    ' Evaluo si ya existe al menos un documento de alcance anterior
                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) And _
                       Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                            If Len(cMensajeError) > 0 Then
                                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo puede adjuntar un CG04 cuando la petici�n ya posee alg�n documento de alcance."
                            Else
                                cMensajeError = "Solo puede adjuntar un CG04 cuando la petici�n ya posee alg�n documento de alcance."
                            End If
                            ValidarArchivos = False
                            Exit For
                    End If
                End If
            End If
        Next i
        
        ' Otro control de documento de Alcance
        For i = 0 To UBound(cArchivos)
            If glUsrPerfilActual <> "SADM" And glLOGIN_Direccion <> "MEDIO" Then
                If cArchivos(i).Tipo = "P950" Or _
                   cArchivos(i).Tipo = "C100" Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Un Documento de Alcance solo puede ser adjuntado por usuarios de la direcci�n de Medios."
                        Else
                            cMensajeError = "Un Documento de Alcance solo puede ser adjuntado por usuarios de la direcci�n de Medios."
                        End If
                        ValidarArchivos = False
                        Exit For
                End If
            End If
        Next i
        
        For i = 0 To UBound(cArchivos)
            If cArchivos(i).Tipo = "T710" Then
                If glUsrPerfilActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Un documento Principios de Desarrollo Aplicativo (T710), solo puede ser adjuntado por usuarios de DyD."
                    Else
                        cMensajeError = "Un documento Principios de Desarrollo Aplicativo (T710), solo puede ser adjuntado por usuarios de DyD."
                    End If
                    ValidarArchivos = False
                    Exit For
                End If
            End If
        Next i
        
        For i = 0 To UBound(cArchivos)
            If cArchivos(i).Tipo <> "OTRO" And cArchivos(i).Tipo <> "EML1" And cArchivos(i).Tipo <> "EML2" And Left(cArchivos(i).Tipo, 3) <> "IDH" And Left(cArchivos(i).Tipo, 3) <> "IHB" And Left(cArchivos(i).Tipo, 3) <> "IHC" Then
                ' Control de c�digo en el documento a adjuntar
                If InStr(1, Trim(cArchivos(i).Nombre), cArchivos(i).Tipo, vbTextCompare) = 0 Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El documento a adjuntar debe contener la clave " & cArchivos(i).Tipo & " dentro del nombre del archivo."
                    Else
                        cMensajeError = "El documento a adjuntar debe contener la clave " & cArchivos(i).Tipo & " dentro del nombre del archivo."
                    End If
                    ValidarArchivos = False
                    Exit For
                End If
            Else
                ' Si se encuentra la clave de alguno de los Informes de Homologaci�n...
                If InStr(1, "IDH|IHB|IHC", Left(cArchivos(i).Tipo, 3), vbTextCompare) > 0 Then
                    ' Valido que solo miembros del equipo de homologaci�n puedan adjuntarlos
                    If Not EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo miembros del grupo Homologador pueden adjuntar Informes de Homologaci�n."
                        Else
                            cMensajeError = "Solo miembros del grupo Homologador pueden adjuntar Informes de Homologaci�n."
                        End If
                        ValidarArchivos = False
                        Exit For
                    End If
                End If
            End If
        Next i
    Else    ' Controles NO SOx
        ' Control de documento de Alcance: modelo anterior
        For i = 0 To UBound(cArchivos)
            If glUsrPerfilActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                If cArchivos(i).Tipo = "ALCA" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & "Un Documento de Alcance solo puede ser adjuntado por usuarios de DyD."
                    Else
                        cMensajeError = "Un Documento de Alcance solo puede ser adjuntado por usuarios de DyD."
                    End If
                    ValidarArchivos = False
                    Exit For
                End If
            End If
        Next i
        ' Controles sobre Plan de Implantaci�n (viejo modelo de control)
        For i = 0 To UBound(cArchivos)
            If cArchivos(i).Tipo = "PLIM" Then
                If glUsrPerfilActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & "Un documento Plan de Implantaci�n (PLIM), solo puede ser adjuntado por usuarios de DyD."
                    Else
                        cMensajeError = "Un documento Plan de Implantaci�n (PLIM), solo puede ser adjuntado por usuarios de DyD."
                    End If
                    ValidarArchivos = False
                    Exit For
                End If
            End If
        Next i
    End If
    If Len(cMensajeError) > 0 Then
        ValidarArchivos = False
    End If
End Function

Private Sub GuardarArchivos()
    Dim i As Integer
    Dim bRespuestaControl As Byte    ' add -006- a.
    
    Call Status("Adjuntando... aguarde, por favor...")
    
    Screen.MousePointer = vbHourglass
    
    pbAdjuntarArchivos.Max = UBound(cArchivos) + 1
    pbAdjuntarArchivos.Value = 0
    pbAdjuntarArchivos.Visible = True
    lblPrjTexto.Visible = True
    lblPrjLey.Caption = ""
    lblPrjLey.Visible = True
    bRespuestaControl = 0           ' add -006- a. - Se inicializa antes de comenzar el ciclo (indeterminado)
    
    ' Antes que nada averiguo si se va a adjuntar un documento de alcance.
    ' Si es as�, pregunto si continuo a pesar de existir un conforme a a la documentaci�n de alcance.
    For i = 0 To UBound(cArchivos)
        If InStr(1, "P950|C100|ALCA|", cArchivos(i).Tipo, vbTextCompare) > 0 Then   ' upd -007- a. Se cambia CG04 por ALCA
            If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
                If MsgBox("La petici�n tiene ingresado un conforme final al documento de alcance." & vbCrLf & _
                          "Si continua, ser� eliminado dicho conforme y deber� gestionar la obtenci�n de uno nuevo." & vbCrLf & _
                          "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Agregado de documentaci�n de Alcance") = vbYes Then
                    ' Elimina el conforme de alcance final de la petici�n
                    Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
                    bRespuestaControl = 1       ' Acuerdo con el usuario en adjuntar y eliminar el conforme
                Else
                    bRespuestaControl = 2       ' No hay acuerdo. Se cancela el adjunto de documentos y se preserva el conforme
                End If
                Exit For
            End If
        End If
    Next i
    If bRespuestaControl <> 2 Then
        For i = 0 To UBound(cArchivos)
            pbAdjuntarArchivos.Value = pbAdjuntarArchivos.Value + 1
            lblPrjLey.Caption = Format((i + 1) / pbAdjuntarArchivos.Max, "Percent")
            lblPrjLey.Refresh
            If InStr(1, "P950|C100|ALCA|", cArchivos(i).Tipo, vbTextCompare) > 0 Then    ' upd -007- a. Se cambia CG04 por ALCA
                Select Case cArchivos(i).Tipo
                    Case "P950"
                        Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 44, "Nombre del documento: " & Trim(cArchivos(i).Nombre))
                    Case "C100"
                        Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 45, "Nombre del documento: " & Trim(cArchivos(i).Nombre))
                    Case "ALCA"     ' upd -007- a. Se cambia CG04 por ALCA
                        Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 46, Trim(cArchivos(i).Nombre))
                End Select
            End If
            ' Agrega al historial el mensaje de agregado del nuevo documento
            If sp_InsertPeticionAdjunto(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo, cArchivos(i).Tipo, cArchivos(i).Ubicacion, cArchivos(i).Nombre, "") Then
                Call sp_AddHistorial(glNumeroPeticion, "ADJADD", glPETEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, cArchivos(i).Tipo & "-->" & cArchivos(i).Nombre)
            End If
        Next i
    End If
    Screen.MousePointer = vbDefault
    pbAdjuntarArchivos.Visible = False
    lblPrjTexto.Visible = False
    lblPrjLey.Visible = False
    Call Status("Listo.")
End Sub
'}
