VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesShellView03 
   Caption         =   "Consulta Peticiones por recurso asignado"
   ClientHeight    =   7965
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11940
   ClipControls    =   0   'False
   Icon            =   "frmPeticionesShellView03.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   11940
   WindowState     =   2  'Maximized
   Begin VB.Frame fraBotonera 
      Height          =   7900
      Left            =   10620
      TabIndex        =   3
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   7320
         Width           =   1170
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "Acceder"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   4470
         Width           =   1170
      End
   End
   Begin VB.Frame fraFiltros 
      Height          =   1575
      Left            =   75
      TabIndex        =   1
      Top             =   0
      Width           =   10515
      Begin VB.ComboBox cboGrup 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   450
         Width           =   8685
      End
      Begin VB.ComboBox cboRecurso 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   1170
         Width           =   5715
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   660
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   810
         Width           =   5715
      End
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "Filtrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8520
         TabIndex        =   2
         Top             =   1200
         Width           =   850
      End
      Begin AT_MaskText.MaskText txtPrioEjecuc 
         Height          =   315
         Left            =   8880
         TabIndex        =   9
         Top             =   825
         Visible         =   0   'False
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   556
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   90
         TabIndex        =   11
         Top             =   510
         Width           =   495
      End
      Begin VB.Label lblPrioEjecuc 
         AutoSize        =   -1  'True
         Caption         =   "Pri.Ejec.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   8220
         TabIndex        =   10
         Top             =   870
         Visible         =   0   'False
         Width           =   660
      End
      Begin VB.Label Label1 
         Caption         =   "Estado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   90
         TabIndex        =   8
         Top             =   848
         Width           =   615
      End
      Begin VB.Label lblAgrup 
         AutoSize        =   -1  'True
         Caption         =   "Recur.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   90
         TabIndex        =   7
         Top             =   1230
         Width           =   540
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   6300
      Left            =   30
      TabIndex        =   0
      Top             =   1590
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   11113
      _Version        =   393216
      Cols            =   28
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmPeticionesShellView03"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 06.07.2007 - Se agrega la columna con el c�digo de Clase de petici�n al control MSFlexGrid, luego de la columna de tipo de petici�n.
' -001- b. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 18.09.2007 - Se agrega la columna de indicador de esquema anterior o nuevo (pet_sox001). Tener en cuenta la propiedad Cols del control MSFlexGrid al agregar columnas.
' -004- a. FJS 11.09.2008 - Se agrega la columna de indicador de Regulatorio.
' -005- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -006- a. FJS 27.11.2014 - Nuevo: para el perfil de superadministrador, se permite filtrar por cualquier �rea.
' -007- a. FJS 30.06.2016 - Nuevo: nuevos campos calculados para determinar la puntuaci�n de una petici�n (basado en f�rmula de valorizaci�n) y para determinar la categor�a a la cual pertenece la petici�n.
' -008- a. FJS 30.06.2016 - Nuevo: se agrega la columna de impacto tecnol�gico.
' -009- a. FJS 23.08.2016 - Nuevo: se quita la restricci�n de tama�o de la aplicaci�n (se permite la maximizaci�n de la pantalla).
' -009- b. FJS 23.08.2016 - Nuevo: se habilita el coloreo de filas en la grilla para mejorar la visibilidad de los datos.

Option Explicit

'{ add -001- a. Se cambia la asignaci�n de la numeraci�n para las constantes
Const colMultiSelect = 0
Const colNroAsignado = 1
Const colTipoPet = 2
Const colPetClass = 3
'{ add -007- a.
Const colPetCategoria = 4
Const colPetPuntuacion = 5
Const colPetImpTech = 6         ' add -008- a.
'}
Const colPetRegulatorio = 7     ' add -004- a.
Const colTitulo = 8
Const colSolicitante = 9
Const colPrioridad = 10
Const colESTADO = 11
Const colSituacion = 12
Const colEstHijo = 13
Const colSitHijo = 14
Const colPrioEjecuc = 15
Const colCantNieto = 16
Const colNomHijo = 17
Const colEsfuerzo = 18
Const colFinicio = 19
Const colFtermin = 20
Const colNroInterno = 21
Const colEstCod = 22
Const colSitCod = 23
Const colCodigoRecurso = 24
Const colNota = 25
Const colDerivo = 26
Const colCodHijo = 27
Const colFinicioSRT = 28
Const colFterminSRT = 29
Const colPetSOx = 30        ' add -003- a.
Const colTOT_COLS = 31      ' add -008- a.
'}

Dim sOpcionSeleccionada As String
Dim sEstadoQuery As String
Dim flgNumero As Boolean
Dim flgInMultiselect As Boolean
Dim xPerfil As String
Dim xNivel As String
Dim xArea As String
Dim flgEnCarga As Boolean
Dim sArea As String
Dim xxx As String

Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long

'{ del -001- a.
'Const colMultiSelect = 0
'Const colNroAsignado = 1
'Const colTipoPet = 2
'Const colTitulo = 3
'Const colSolicitante = 4
'Const colPrioridad = 5
'Const colEstado = 6
'Const colSituacion = 7
'Const colEstHijo = 8
'Const colSitHijo = 9
'Const colPrioEjecuc = 10
'Const colCantNieto = 11
'Const colNomHijo = 12
'Const colEsfuerzo = 13
'Const colFinicio = 14
'Const colFtermin = 15
'Const colNroInterno = 16
'Const colEstCod = 17
'Const colSitCod = 18
'Const colCodigoRecurso = 19
'Const colNota = 20
'Const colDerivo = 21
'Const colCodHijo = 22
'Const colFinicioSRT = 23
'Const colFterminSRT = 24
'}

Private Sub Form_Load()
    flgEnCarga = True
    flgNumero = False
    xPerfil = glUsrPerfilActual
    xNivel = getPerfNivel(glUsrPerfilActual)
    xArea = getPerfArea(glUsrPerfilActual)
    Call InicializarCombos
    Call InicializarPantalla
    flgEnCarga = False
    'Call FormCenter(Me, mdiPrincipal, False)       ' del -009- a.
    Call IniciarScroll(grdDatos)                    ' add -005- a.
End Sub

Private Sub Form_Activate()
   Me.WindowState = vbMaximized
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    lblPrioEjecuc.visible = True
    txtPrioEjecuc.visible = True
    Call HabilitarBotones(0)
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            cboEstado.Enabled = True
            cmdFiltro.Enabled = True
            grdDatos.Enabled = True
            sOpcionSeleccionada = ""
            cmdCerrar.visible = True
            cmdVisualizar.visible = True
            If IsMissing(vCargaGrid) Then
                CargarGrid
            Else
                Call MostrarSeleccion(False)
            End If
        Case 1
            cboEstado.Enabled = False
            cmdFiltro.Enabled = False
            grdDatos.Enabled = False
            cmdCerrar.visible = False
            cmdVisualizar.visible = False
    End Select
    Call LockProceso(False)
End Sub

Sub InicializarCombos()
    Dim i As Integer
    Dim auxCod As String
    Dim auxCod2 As String
    Dim sDireccion As String
    Dim sGerencia As String
    
    auxCod = ""
    auxCod2 = ""
    
    Call Status("Cargando...")
    'If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) = 0 Then
    Select Case glUsrPerfilActual
        Case "SADM", "ADMI"
            If sp_GetGrupoXt("", "", "S") Then
                Do While Not aplRST.EOF
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboGrup.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_grupo)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Case "CSEC"
            If sp_GetGrupoXt("", IIf(ClearNull(glLOGIN_Sector) <> "", glLOGIN_Sector, "NULL"), "S") Then
                Do While Not aplRST.EOF
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboGrup.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_grupo)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Case Else
            If sp_GetGrupoXt(IIf(ClearNull(glLOGIN_Grupo) <> "", glLOGIN_Grupo, "NULL"), IIf(ClearNull(glLOGIN_Sector) <> "", glLOGIN_Sector, "NULL"), "S") Then
                Do While Not aplRST.EOF
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboGrup.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_grupo)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
    End Select

'    If glUsrPerfilActual <> "SADM" Then
'        If sp_rptEstructura(IIf(ClearNull(glLOGIN_Direccion) <> "", glLOGIN_Direccion, "NULL"), IIf(ClearNull(glLOGIN_Gerencia) <> "", glLOGIN_Gerencia, "NULL"), IIf(ClearNull(glLOGIN_Sector) <> "", glLOGIN_Sector, "NULL"), IIf(ClearNull(glLOGIN_Grupo) <> "", glLOGIN_Grupo, "NULL"), "2") Then
'            Do While Not aplRST.EOF
'                auxCod = ""
'                auxCod2 = ""
'                If ClearNull(aplRST!cod_grupo) <> "" Then
'                    auxCod = ClearNull(aplRST!cod_grupo)
'                    'auxCod2 = ClearNull(aplRST!nom_direccion) & " >> " & ClearNull(aplRST!nom_gerencia) & " >> " & ClearNull(aplRST!nom_sector) & " >> " & ClearNull(aplRST!nom_grupo)
'                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
'                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
'                    auxCod2 = sDireccion & "� " & sGerencia & "� " & ClearNull(aplRST!nom_sector) & "� " & ClearNull(aplRST!nom_grupo)
'                    cboGrup.AddItem auxCod2 & ESPACIOS & ESPACIOS & ESPACIOS & "||" & auxCod
'                End If
'                aplRST.MoveNext
'            Loop
'        End If
'    Else
'        If sp_rptEstructura("NULL", "NULL", "NULL", "NULL", "2") Then
'            Do While Not aplRST.EOF
'                auxCod = ""
'                auxCod2 = ""
'                If ClearNull(aplRST!cod_grupo) <> "" Then
'                    auxCod = ClearNull(aplRST!cod_grupo)
'                    'auxCod2 = ClearNull(aplRST!nom_direccion) & " >> " & ClearNull(aplRST!nom_gerencia) & " >> " & ClearNull(aplRST!nom_sector) & " >> " & ClearNull(aplRST!nom_grupo)
'                    'cboGrup.AddItem auxCod2 & Space(130) & "||" & auxCod
'                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
'                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
'                    auxCod2 = sDireccion & "� " & sGerencia & "� " & ClearNull(aplRST!nom_sector) & "� " & ClearNull(aplRST!nom_grupo)
'                    cboGrup.AddItem auxCod2 & ESPACIOS & ESPACIOS & ESPACIOS & "||" & auxCod
'                End If
'                aplRST.MoveNext
'            Loop
'        End If
'    End If
    If cboGrup.ListCount = 0 Then
       cboGrup.AddItem "No existen grupos ejecutores en su l�nea" & ESPACIOS & ESPACIOS & "||" & "NONES"
    End If
    If cboGrup.ListCount > 0 Then
        cboGrup.ListIndex = 0
    End If
    If cboGrup.ListCount > 1 Then
        cboGrup.Enabled = True
    End If
    
    auxCod = ""
    auxCod2 = ""
    cboEstado.Clear
    If sp_GetEstado(Null) Then
        Do While Not aplRST.EOF
            If InStr("PLANRK|EJECRK|ESTIRK", ClearNull(aplRST!cod_estado)) = 0 Then
                If Val(ClearNull(aplRST!flg_secgru)) > 0 Then
                    cboEstado.AddItem Trim(aplRST!nom_estado) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_estado)
                    If Val(ClearNull(aplRST!flg_secgru)) > 1 Then
                        auxCod = auxCod & aplRST!cod_estado & "|"
                    End If
                End If
            End If
            aplRST.MoveNext
        Loop
        'aplRST.Close
    End If
    
    cboEstado.AddItem "Sin filtrar por estado" & Space(90) & "||" & "NULL", 0
    cboEstado.AddItem "Grupo en estado Pendiente" & Space(90) & "||OPINIO|EVALUA|PLANIF|ESTIMA|", 0
    cboEstado.AddItem "Grupo en estado Activo" & Space(90) & "||" & auxCod, 0
    cboEstado.ListIndex = 0
    
    sEstadoQuery = DatosCombo(cboEstado)
    
    initCboRecurso (CodigoCombo(cboGrup, True))
    
    Call Status("Listo.")
End Sub

Private Sub cboGrup_Click()
    If flgEnCarga Then Exit Sub
    Call initCboRecurso(CodigoCombo(cboGrup, True))
    DoEvents
End Sub

Private Sub cmdFiltro_Click()
    If cboEstado.ListIndex = -1 Then
        MsgBox ("Debe seleccionar un estado")
        Exit Sub
    End If
    If Not LockProceso(True) Then
        Exit Sub
    End If
    flgNumero = False
    sEstadoQuery = DatosCombo(cboEstado)
    glNumeroPeticion = ""
    Call CargarGrid
End Sub

Private Sub cmdVisualizar_Click()
    If glNumeroPeticion <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoPeticion = "VIEW"
        On Error Resume Next
        frmPeticionesCarga.Show vbModal
        DoEvents
        On Error GoTo 0
        Call LockProceso(False)
        If Me.Tag = "REFRESH" Then
            InicializarPantalla
        End If
    End If
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    'Unload Me
    Me.Hide
    Call LockProceso(False)
End Sub

'{ add -009- a.
Private Sub Form_Resize()
    'Debug.Print "Height: " & Me.Height & " / Width: " & Me.Width
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            Me.Top = 0
            Me.Left = 0
            If Me.Height < 6135 Then Me.Height = 6135
            If Me.Width < 11355 Then Me.Width = 11355
        End If
        ' Ancho
        fraFiltros.Width = Me.ScaleWidth - fraBotonera.Width - 150
        grdDatos.Width = Me.ScaleWidth - fraBotonera.Width - 150
        fraBotonera.Left = fraFiltros.Width + 100
        ' Alto
        grdDatos.Height = Me.ScaleHeight - fraFiltros.Height - 100
        fraBotonera.Height = Me.ScaleHeight - 50
        cmdCerrar.Top = fraBotonera.Height - cmdCerrar.Height - 100
    End If
End Sub
'}

Private Sub Form_Unload(Cancel As Integer)
    glNumeroPeticion = ""
    Call DetenerScroll(grdDatos)      ' add -005- a.
End Sub

Sub initCboRecurso(xGrupo)
    Dim auxCod As String
    auxCod = ""
    cboRecurso.Clear
    cboRecurso.visible = False
    DoEvents
    If sp_GetRecurso(xGrupo, "SU", "A") Then
        Do While Not aplRST.EOF
            cboRecurso.AddItem ClearNull(aplRST(1)) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST(0))
            auxCod = auxCod & ClearNull(aplRST(0)) & "|"
            aplRST.MoveNext
            DoEvents
        Loop
        'aplRST.Close
    End If
    cboRecurso.AddItem "<todos los recursos>" & ESPACIOS & ESPACIOS & "||" & auxCod, 0
    cboRecurso.AddItem "<sin filtrar por recurso>" & ESPACIOS & ESPACIOS & "||NULL", 0
    If cboRecurso.ListCount > 0 Then
        cboRecurso.ListIndex = 0
    End If
    cboRecurso.visible = True
    DoEvents
End Sub

Sub CargarGrid()
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
    Dim vRetorno() As String
    Dim auxAgrup As String
    Dim i As Long
    
    flgEnCarga = True                   ' add -009- b.
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    cmdVisualizar.Enabled = False
    cmdFiltro.Enabled = False
    With grdDatos
        .visible = False
        .Clear
        DoEvents
        .HighLight = flexHighlightAlways
        .RowHeight(0) = 300
        .Rows = 1
        .cols = colTOT_COLS
        .Font.name = "Tahoma": .Font.Size = 8
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .TextMatrix(0, colNroAsignado) = "Petic.": .ColWidth(colNroAsignado) = 700: .ColAlignment(colNroAsignado) = flexAlignRightCenter
        .TextMatrix(0, colSolicitante) = "Solicitante": .ColWidth(colSolicitante) = 0
        .TextMatrix(0, colTipoPet) = "Tipo": .ColWidth(colTipoPet) = 450: .ColAlignment(colTipoPet) = flexAlignLeftCenter
        .TextMatrix(0, colPetClass) = "Clase": .ColWidth(colPetClass) = 550       ' add -001- a.
        '{ add -001- a.
        .TextMatrix(0, colPetCategoria) = "Categ.": .ColWidth(colPetCategoria) = 650: .ColAlignment(colPetCategoria) = flexAlignLeftCenter
        .TextMatrix(0, colPetPuntuacion) = "Ptos.": .ColWidth(colPetPuntuacion) = 700
        .TextMatrix(0, colPetImpTech) = "I.T.": .ColWidth(colPetImpTech) = 550
        '}
        .TextMatrix(0, colPetRegulatorio) = "Reg.": .ColWidth(colPetRegulatorio) = 500: .ColAlignment(colPetRegulatorio) = flexAlignLeftCenter       ' add -004- a.
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 5400: .ColAlignment(colTitulo) = flexAlignLeftCenter
        .TextMatrix(0, colESTADO) = "Estado Pet": .ColWidth(colESTADO) = 2300
        .TextMatrix(0, colSituacion) = "Situaci�n Pet": .ColWidth(colSituacion) = 1200
        .TextMatrix(0, colPrioridad) = "Pri.": .ColWidth(colPrioridad) = 400
        .TextMatrix(0, colEsfuerzo) = "Hs Pet"
        .TextMatrix(0, colFinicio) = "F.Ini.Planif Pet"
        .TextMatrix(0, colFtermin) = "F.Fin Planif Pet"
        .TextMatrix(0, colNroInterno) = "NroInt.": .ColWidth(colNroInterno) = 0
        .TextMatrix(0, colCantNieto) = ""
        .TextMatrix(0, colPetSOx) = "MN.": .ColWidth(colPetSOx) = 0: .ColAlignment(colPetSOx) = flexAlignLeftCenter   ' add -003- a.
        .ColWidth(colMultiSelect) = 200
        
        .ColWidth(colNomHijo) = 0
        .ColWidth(colCodHijo) = 0
        .ColWidth(colCantNieto) = 0
        .TextMatrix(0, colPrioEjecuc) = "Pri.Ejec.": .ColWidth(colPrioEjecuc) = 600: .ColAlignment(colPrioEjecuc) = flexAlignLeftCenter '.ColWidth(colPrioEjecuc) = 0
        '.ColWidth(colSituacion) = 0
        .ColWidth(colEstHijo) = 1500
        .ColWidth(colSitHijo) = 1200
        .TextMatrix(0, colEstHijo) = "Est. Grup.": .ColWidth(colEstHijo) = 0
        .TextMatrix(0, colSitHijo) = "Sit. Grup.": .ColWidth(colSitHijo) = 0
        .TextMatrix(0, colEsfuerzo) = "Hs. Grup": .ColWidth(colEsfuerzo) = 700: .ColAlignment(colEsfuerzo) = flexAlignRightCenter
        .TextMatrix(0, colFinicio) = "F.Ini.Planif Gru": .ColWidth(colFinicio) = 1400
        .TextMatrix(0, colFtermin) = "F.Fin Planif Gru": .ColWidth(colFtermin) = 1400
        '.ColAlignment(colTipoPet) = flexAlignCenterCenter
        '.ColAlignment(colPrioEjecuc) = flexAlignCenterCenter
        '.ColWidth(colPrioridad) = 350
        '.ColAlignment(colPrioridad) = flexAlignCenterCenter
        '.ColWidth(colEsfuerzo) = 600
        '.ColWidth(colNroInterno) = 700
        .ColWidth(colEstCod) = 0
        .ColWidth(colSitCod) = 0
        .ColWidth(colCodigoRecurso) = 0
        .ColWidth(colDerivo) = 0
        .ColWidth(colNota) = 0
        '.ColAlignment(colPrioridad) = 3
        '.ColAlignment(colTitulo) = 0
        '.ColAlignment(colPetClass) = vbCenter       ' add -001- a.
        .ColWidth(colFinicioSRT) = 0
        .ColWidth(colFterminSRT) = 0
        i = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    
    If CodigoCombo(cboGrup, True) = "NONES" Then Exit Sub
    
    Call Puntero(True)
    Call Status("Cargando peticiones...")
    If sEstadoQuery = "NULL" Then
        If Not sp_GetPeticionAreaRec(CodigoCombo(cboGrup, True), Null, DatosCombo(cboRecurso), txtPrioEjecuc.text) Then
           GoTo finx
        End If
    Else
        If Not sp_GetPeticionAreaRec(CodigoCombo(cboGrup, True), sEstadoQuery, DatosCombo(cboRecurso), txtPrioEjecuc.text) Then
           GoTo finx
        End If
    End If
    Call Status(aplRST.RecordCount & IIf(aplRST.RecordCount > 1, " peticiones.", " petici�n."))
    
    'grdDatos.visible = False
    Do While Not aplRST.EOF
        With grdDatos
            i = i + 1
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!nom_estado_hij)
            .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!nom_situacion_hij)
            .TextMatrix(.Rows - 1, colNomHijo) = ClearNull(aplRST!nom_area_hij)
            .TextMatrix(.Rows - 1, colCodHijo) = ClearNull(aplRST!cod_area_hij)
            .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, colPetClass) = ClearNull(aplRST!cod_clase)                       ' add -001- a.
            .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup_hij)
            '.TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan_hij), Format(aplRST!fe_ini_plan_hij, "dd/mm/yyyy"), "")
            '.TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan_hij), Format(aplRST!fe_fin_plan_hij, "dd/mm/yyyy"), "")
            
            .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan_hij), Format(aplRST!fe_ini_plan_hij, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan_hij), Format(aplRST!fe_fin_plan_hij, "yyyy-mm-dd"), "")
            
            .TextMatrix(.Rows - 1, colFinicioSRT) = IIf(Not IsNull(aplRST!fe_ini_plan_hij), Format(aplRST!fe_ini_plan_hij, "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colFterminSRT) = IIf(Not IsNull(aplRST!fe_fin_plan_hij), Format(aplRST!fe_fin_plan_hij, "yyyymmdd"), "")
            '.TextMatrix(.Rows - 1, colPrioEjecuc) = IIf(Val(ClearNull(aplRST!prio_ejecuc)) > 0, padRight(ClearNull(aplRST!prio_ejecuc), 2), "")
            .TextMatrix(.Rows - 1, colPrioEjecuc) = ClearNull(aplRST!prio_ejecuc)
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colCodigoRecurso) = ClearNull(aplRST!cod_solicitante)
            .TextMatrix(.Rows - 1, colEstCod) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colSitCod) = ClearNull(aplRST!cod_situacion)
            .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
            .TextMatrix(.Rows - 1, colSolicitante) = ClearNull(aplRST!cod_solicitante)
            .TextMatrix(.Rows - 1, colPetSOx) = ClearNull(aplRST!pet_sox001)                ' add -003- a.
            .TextMatrix(.Rows - 1, colPetRegulatorio) = ClearNull(aplRST!pet_regulatorio)   ' add -004- a.
            '{ add -007- a.
            .TextMatrix(.Rows - 1, colPetImpTech) = ClearNull(aplRST!pet_imptech)           ' add -008- a.
            .TextMatrix(.Rows - 1, colPetCategoria) = ClearNull(aplRST!categoria)
            .TextMatrix(.Rows - 1, colPetPuntuacion) = Format(ClearNull(aplRST!puntuacion), "###0.000")
            '}
        End With
        If i Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)                       ' add -009- b.
        aplRST.MoveNext
        DoEvents
    Loop
finx:
    ' TODO: problemas de performance en esta carga 23.08.2016
    flgEnCarga = False                   ' add -009- b.
    grdDatos.visible = True
    If grdDatos.Rows < 2 Then Call Status("Listo.")
    If glNumeroPeticion <> "" Then
        If grdDatos.Rows > 1 Then
            Call GridSeek(Me, grdDatos, colNroInterno, glNumeroPeticion, True)
            Call MostrarSeleccion(False)
            'cmdVisualizar.Enabled = True
        End If
    Else
        If grdDatos.Rows > 1 Then
            grdDatos.TopRow = 1
            grdDatos.row = 1
            grdDatos.RowSel = 1
            grdDatos.col = 0
            grdDatos.ColSel = grdDatos.cols - 1
            Call MostrarSeleccion(False)
            'cmdVisualizar.Enabled = True
        End If
    End If
    cmdFiltro.Enabled = True
    Call Puntero(False)
    'Call Status("Listo.")
    Call LockProceso(False)
    'Call MostrarSeleccion
    'If grdDatos.Rows > 1 Then
    '   grdDatos.RowSel = 1
    '   grdDatos.Col = 0
    '   grdDatos.Sort = flexSortStringnocaseAscending
    'End If
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Public Sub MostrarSeleccion(Optional flgSort)
    Debug.Print "MostrarSeleccion"
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    cmdVisualizar.Enabled = False
    sArea = ""
    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                glNroAsignado = ClearNull(.TextMatrix(.RowSel, colNroAsignado))
                glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
                glCodigoRecurso = .TextMatrix(.RowSel, colCodigoRecurso)
                glEstadoPeticion = .TextMatrix(.RowSel, colEstCod)
                sArea = .TextMatrix(.RowSel, colCodHijo)
                cmdVisualizar.Enabled = True
            End If
        End If
    End With
End Sub

'Private Sub grdDatos_Click()
'    If grdDatos.MouseRow = 0 And grdDatos.Rows > 1 Then
'       grdDatos.RowSel = 1
'       If grdDatos.MouseCol = colFinicio Then
'           grdDatos.Col = colFinicioSRT
'       ElseIf grdDatos.MouseCol = colFtermin Then
'           grdDatos.Col = colFterminSRT
'       Else
'           grdDatos.Col = grdDatos.MouseCol
'       End If
'       grdDatos.Sort = flexSortStringNoCaseAscending
'    End If
'    'Debug.Print "grdDatos_Click"
'    Call MostrarSeleccion
'    DoEvents
'End Sub

Private Sub grdDatos_Click()
    bSorting = True
    flgEnCarga = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    flgEnCarga = False
    bSorting = False
End Sub

Private Sub grdDatos_DblClick()
    'Debug.Print "grdDatos_DblClick"
    Call MostrarSeleccion
    DoEvents
    If glNumeroPeticion <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoPeticion = "VIEW"
        On Error Resume Next
        frmPeticionesCarga.Show vbModal
        DoEvents
        If Me.Tag = "REFRESH" Then
             Call InicializarPantalla
        End If
    End If
End Sub

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    'Debug.Print "grdDatos_MouseUp"
    If grdDatos.row <= grdDatos.RowSel Then
        nRwD = grdDatos.row
        nRwH = grdDatos.RowSel
    Else
        nRwH = grdDatos.row
        nRwD = grdDatos.RowSel
    End If
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, colMultiSelect) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, colMultiSelect) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        If Button = vbLeftButton Then
        With grdDatos
        If .RowSel > 0 Then
             If ClearNull(.TextMatrix(.RowSel, colMultiSelect)) = "" Then
                .TextMatrix(.RowSel, colMultiSelect) = "�"
             Else
                .TextMatrix(.RowSel, colMultiSelect) = ""
            End If
        End If
        End With
        End If
    End If
    If Shift = 0 Then
        If Button = vbLeftButton Then
        With grdDatos
        For i = 1 To .Rows - 1
           .TextMatrix(i, colMultiSelect) = ""
        Next
        If .RowSel > 0 Then
             .TextMatrix(.RowSel, colMultiSelect) = "�"
        End If
        End With
        End If
    End If
End Sub
 
Private Sub grdDatos_RowColChange()
    If flgInMultiselect Then Exit Sub
    If Not flgEnCarga Then Call MostrarSeleccion
End Sub

'Private Sub Command1_Click()
'    Dim cXMLDocument As String
'
'    Dim cComponentName As String
'
'    ' Encabezado (fijo)
'    cXMLDocument = cXMLDocument & "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?><?mso-infoPathSolution solutionVersion=" & Chr(34) & "1.0.0.38" & Chr(34) & " productVersion=" & Chr(34) & "11.0.8165" & Chr(34) & " PIVersion=" & Chr(34) & "1.0.0.0" & Chr(34) & vbCrLf
'    cXMLDocument = cXMLDocument & "href=" & Chr(34) & "file:///\\bbvdfs\dfs\eCM\eCM\CALIDAD\Formularios\NOHOST.xsn" & Chr(34) & " name=" & Chr(34) & "urn:schemas-microsoft-com:office:infopath:NOHOST:-myXSD-2009-06-22T21-57-23" & Chr(34) & " ?><?mso-application" & vbCrLf
'    cXMLDocument = cXMLDocument & "progid=" & Chr(34) & "InfoPath.Document" & Chr(34) & "?><my:misCampos xmlns:xsi=" & Chr(34) & "http://www.w3.org/2001/XMLSchema-instance" & Chr(34) & vbCrLf
'    cXMLDocument = cXMLDocument & "xmlns:my=" & Chr(34) & "http://schemas.microsoft.com/office/infopath/2003/myXSD/2009-06-22T21:57:23" & Chr(34) & " xmlns:xd=" & Chr(34) & "http://schemas.microsoft.com/office/infopath/2003" & Chr(34) & " xml:lang=" & Chr(34) & "es" & Chr(34) & ">" & vbCrLf
'
'    ' Detalle del documento
'    cXMLDocument = cXMLDocument & vbTab & "<my:Aplicacion>GesPet</my:Aplicacion>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Plataforma>Win32</my:Plataforma>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:FechaEntrega>2009-09-17</my:FechaEntrega>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:FechaInstalacion>2009-09-17</my:FechaInstalacion>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Solicitante>Spitz, Fernando Javier</my:Solicitante>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:solInterno>22167</my:solInterno>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Lider>Ocampo, Alberto</my:Lider>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:lidInterno>22677</my:lidInterno>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Motivo>CGM: Catalogación para ver que onda con esto</my:Motivo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Peticion>20145</my:Peticion>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Proyecto>GesPet_200909171600</my:Proyecto>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:AmbDestino>1</my:AmbDestino>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Ambiente>PROD</my:Ambiente>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:Prioridad>EXC</my:Prioridad>" & vbCrLf
'    ' Grupos
'    cXMLDocument = cXMLDocument & vbTab & "<my:grupo1>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & "<my:Componentes>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:ordenCarga>1</my:ordenCarga>" & vbCrLf
'    ' Detalle de los componentes
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Componente>\Forms\frmPeticionesCarga.frm</my:Componente>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Tipo></my:Tipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Nuevo></my:Nuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Ejecutable></my:Ejecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esEjecutable>false</my:esEjecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esNuevo>false</my:esNuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:selTipo>VB</my:selTipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Orden>1</my:Orden>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "</my:Componentes><my:Componentes>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:ordenCarga>1</my:ordenCarga>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Componente>\Forms\frmPeticionesCarga.frx</my:Componente>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Tipo></my:Tipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Nuevo></my:Nuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Ejecutable></my:Ejecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esEjecutable>false</my:esEjecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esNuevo>false</my:esNuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:selTipo>VB</my:selTipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Orden>1</my:Orden>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "</my:Componentes><my:Componentes>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:ordenCarga>1</my:ordenCarga>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Componente>\Forms\frmCargaHoras.frm</my:Componente>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Tipo></my:Tipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Nuevo></my:Nuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Ejecutable></my:Ejecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esEjecutable>false</my:esEjecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esNuevo>false</my:esNuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:selTipo>VB</my:selTipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Orden>2</my:Orden>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "</my:Componentes><my:Componentes>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:ordenCarga>1</my:ordenCarga>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Componente>\Modulos\modMain.bas</my:Componente>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Tipo></my:Tipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Nuevo></my:Nuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Ejecutable></my:Ejecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esEjecutable>false</my:esEjecutable>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:esNuevo>false</my:esNuevo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:selTipo>VB</my:selTipo>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & vbTab & vbTab & "<my:Orden>3</my:Orden>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & vbTab & "</my:Componentes></my:grupo1>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & "<my:Comentarios>Sin comentarios</my:Comentarios>" & vbCrLf
'    cXMLDocument = cXMLDocument & vbTab & "<my:PlanContingencia>Ante cualquier eventualidad, comunicarse con el analista del aplicativo Spitz, Fernando al interno 22167 o al celular (15)5800.6875</my:PlanContingencia>" & vbCrLf
'
'    cXMLDocument = cXMLDocument & vbTab & "<my:Item_Count>4</my:Item_Count>" & vbCrLf
'    cXMLDocument = cXMLDocument & "</my:misCampos>" & vbCrLf
'
'    doFile "solicitu_manual.xml", cXMLDocument
'    MsgBox "Formulario creado"
'End Sub
'
'
''    Dim objApp As InfoPath.Application
''    Dim objXDoc As InfoPath.XDocument
''    Dim objXML As InfoPath.DataDOM
''
''    Set objApp = New InfoPath.Application
''    Set objXDoc = objApp.XDocuments.NewFromSolution(App.Path & "\plantillas\NOHOST.xsn")
''
''    'objXDoc.SaveAs App.Path & "\plantillas\Nuevo.xsn"
''    'Set objXML = objXDoc.GetDOM
''    MsgBox "Formulario creado"

