VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmCLISTShell 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Solicitudes a Carga de M�quina - De PROD a DESA"
   ClientHeight    =   7995
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11865
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCLISTShell.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7995
   ScaleWidth      =   11865
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtSolicitudJustificacion 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1605
      Left            =   6840
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   6240
      Width           =   3375
   End
   Begin VB.TextBox txtSolicitudGenerada 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1605
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   6240
      Width           =   6615
   End
   Begin VB.Frame fraOpciones 
      Height          =   1695
      Left            =   120
      TabIndex        =   5
      Top             =   0
      Width           =   10155
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "Filtrar"
         Height          =   375
         Left            =   8760
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   240
         Width           =   1170
      End
      Begin VB.ComboBox cmbEnm 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   960
         Width           =   1215
      End
      Begin VB.CheckBox chkOnlyPending 
         Alignment       =   1  'Right Justify
         Caption         =   "Solo pendientes de aprobaci�n"
         Height          =   255
         Left            =   7320
         TabIndex        =   14
         Top             =   1080
         Width           =   2655
      End
      Begin VB.ComboBox cmbFiltroEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   600
         Width           =   4215
      End
      Begin VB.ComboBox cmbTipoSolicitud 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   4215
      End
      Begin AT_MaskText.MaskText mskFechaIni 
         Height          =   315
         Left            =   6960
         TabIndex        =   20
         Top             =   240
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         AutoTab         =   -1  'True
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText mskFechaFin 
         Height          =   315
         Left            =   6960
         TabIndex        =   21
         Top             =   600
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         AutoTab         =   -1  'True
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaPrueba 
         Height          =   315
         Left            =   4290
         TabIndex        =   24
         Top             =   960
         Visible         =   0   'False
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BackColor       =   8454143
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   8454143
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         AutoTab         =   -1  'True
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblNoMeLlegaElArchivo 
         AutoSize        =   -1  'True
         Caption         =   "�No te lleg� el archivo?"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   180
         Left            =   120
         MouseIcon       =   "frmCLISTShell.frx":014A
         TabIndex        =   29
         Top             =   1440
         Width           =   1770
      End
      Begin VB.Label lblGuiaRapida 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Transmisi�n de archivos a Desarrollo (Gu�a r�pida)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   180
         Left            =   6240
         MouseIcon       =   "frmCLISTShell.frx":02A8
         TabIndex        =   28
         Top             =   1440
         Width           =   3825
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Enmascaramiento:"
         Height          =   180
         Index           =   6
         Left            =   120
         TabIndex        =   23
         Top             =   1035
         Width           =   1410
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Desde:"
         Height          =   180
         Index           =   3
         Left            =   5880
         TabIndex        =   19
         Top             =   300
         Width           =   1035
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Hasta:"
         Height          =   180
         Index           =   5
         Left            =   5880
         TabIndex        =   18
         Top             =   660
         Width           =   1005
      End
      Begin VB.Label lblFiltros 
         Caption         =   "Estado:"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Top             =   638
         Width           =   1335
      End
      Begin VB.Label lblFiltros 
         Caption         =   "Tipo de solicitud:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   278
         Width           =   1335
      End
   End
   Begin VB.Frame pnlBotones 
      Height          =   7905
      Left            =   10320
      TabIndex        =   1
      Top             =   0
      Width           =   1515
      Begin VB.CheckBox chkEmergencia 
         Caption         =   "EMErgencia"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   1800
         Width           =   1215
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar"
         Height          =   495
         Left            =   60
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Genera una exportaci�n a una planilla en Excel de los datos en pantalla"
         Top             =   4320
         Width           =   1395
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   495
         Left            =   60
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Cancelar y eliminar la solicitud seleccionada"
         Top             =   1080
         Width           =   1395
      End
      Begin VB.CommandButton cmdCopiar 
         Caption         =   "Copiar"
         Height          =   495
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Copiar la solicitud seleccionada en una nueva con los mismos datos"
         Top             =   600
         Width           =   1395
      End
      Begin VB.CommandButton cmdGenerar 
         Caption         =   "Generar"
         Default         =   -1  'True
         Height          =   495
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Generar una nueva solicitud"
         Top             =   120
         Width           =   1395
      End
      Begin VB.CommandButton cmdRelease 
         Caption         =   "Liberar"
         Height          =   495
         Left            =   60
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Liberar una solicitud pendiente de restore de backup a fecha para su procesamiento en Carga de M�quina"
         Top             =   3240
         Width           =   1395
      End
      Begin VB.CommandButton cmdEnviarCorreo 
         Caption         =   "Reenviar correo"
         Height          =   495
         Left            =   60
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Reenviar el correo con la solicitud de restore del archivo seleccionado"
         Top             =   2760
         Width           =   1395
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   495
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   7320
         Width           =   1395
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4125
      Left            =   120
      TabIndex        =   0
      Top             =   1800
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   7276
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Justificaci�n de la solicitud"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6840
      TabIndex        =   11
      Top             =   6000
      Width           =   2175
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Datos de la solicitud generada"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   180
      Left            =   120
      TabIndex        =   10
      Top             =   6000
      Width           =   2250
   End
End
Attribute VB_Name = "frmCLISTShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Estados de una solicitud:
'**************************
'
' A: Pendiente de aprobaci�n
' B: Pendiente de aprobaci�n SI
' C: Aprobado
' D: Aprobado y en proceso de envio
' E: Enviado y finalizado
' F: En espera de Restore
' G: En espera (ejecuci�n diferida)         ' Aquellas que nacen como de ejecuci�n diferida, luego pasan a estado "C"

' * El proceso que se usa para generar el bcpout para las solicitudes a transmitir es el sp_UpdateSoliEnviadas
' * El proceso que genera la tabla temporal con los archivos del cat�logo para la posterior transmisi�n es el sp_HEDTTransmitir/sp_HEDTArchivos

' -001- a. FJS 18.11.2009 - Se modifica la rutina para cancelar una solicitud a Carga de M�quina.
' -002- a. FJS 07.12.2009 - Se permite cancelar una solicitud sin enmascaramiento por parte del usuario antes de que sea aprobada por un supervisor.
' -003- a. FJS 28.12.2009 - Nueva funcionalidad RESTORE: opcionalmente, para las opciones 1 y 4, podr� solicitarse la restituci�n de un backup (restore).
' -003- b. FJS 28.12.2009 - Permitir cancelar solicitudes que soliciten RESTORE de tipo 4 (SORT).
' -004- a. FJS 20.01.2010 - Nueva opci�n de solicitud de ejecuci�n diferida (fin de semana).
' -005- a. FJS 06.07.2010 - UPD: Se modifica la leyenda del estado E porque trae confusi�n en los usuarios.
' -006- a. AA  24.11.2010 - Se agregan nuevos filtros para la consulta de datos.
' -007- a. AA  24.11.2010 - Deshabilita los botones "Generar", "Copiar", "Cancelar", "Enviar Correo", "Liberar" para el perfil de Seguridad Inform�tica.
' -008- a. AA  25.11.2010 - Se agrega validaciones para los filtros ingresados(fecha inicio del filtro).
' -008- b. AA  25.11.2010 - Se agrega validaciones para los filtros ingresados(fecha fin del filtro).
' -009- a. FJS 29.04.2011 - Se agrega una validaci�n que determina si pueden generarse solicitudes o no.
' -009- b. FJS 02.05.2011 - Correcci�n: la fecha anterior, se estaba determinando mal. Debe descontar de la fecha de inicio.
' -010- a. FJS 27.06.2014 - Nuevo: se agrega funcionalidad para generar solicitudes por EMErgencia.
' -011- a. FJS 28.07.2014 - Nuevo: se agrega Help Inicial (seg�n configuraci�n).

Option Explicit

Private Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Long
Private Declare Function GetProcAddress Lib "kernel32" (ByVal hModule As Long, ByVal lpProcName As String) As Long
Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

Private Const sMsgXCOM = "(Copia de archivo completo a desarrollo)"
Private Const sMsgUNLO = "(UNLOAD de tabla)"
Private Const sMsgTCOR = "(Extracci�n de VSAM tablas corporativas)"
Private Const sMsgSORT = "(Copia de selecci�n de datos SORT)"
    
Private Const SOL_NUMERO = 0
Private Const sol_eme = 1
Private Const sol_tipo = 2
Private Const SOL_ENM = 3
Private Const sol_diferida = 4
Private Const SOL_FECHA = 5
Private Const SOL_POR = 6
Private Const sol_estado = 7
Private Const SOL_PETICION = 8
Private Const SOL_RESP_EJEC = 9
Private Const SOL_RESP_SECT = 10
Private Const SOL_SEG = 11
Private Const SOL_SOLESTADO = 12
Private Const SOL_APROBADO = 13
Private Const SOL_FCHAPROB1 = 14
Private Const SOL_FCHAPROB2 = 15
Private Const SOL_SOLICITANTE = 16
Private Const SOL_JUSCOD = 17
Private Const SOL_JUSDSC = 18
Private Const SOL_SOLITEXTO = 19
Private Const SOL_ARCHIVOPROD = 20
Private Const SOL_DSN_NOM = 21
Private Const SOL_ARCHIVO_OUT = 22
Private Const SOL_DSNID = 23
Private Const SOL_CPYBBL = 24
Private Const SOL_CPYNOM = 25
Private Const SOL_NOMSOLICITA = 26
Private Const SOL_NOMRESPEJEC = 27
Private Const SOL_NOMRESPSECT = 28
Private Const sol_LIB_SYSIN = 29
Private Const SOL_MEM_SYSIN = 30
Private Const SOL_JOIN = 31
Private Const SOL_NROTABLA = 32
Private Const SOL_FCHABACKUP = 33
Private Const SOL_ARCHIVOINTERMEDIO = 34
Private Const SOL_TABLA_BACKUP = 35
Private Const SOL_JOB_BACKUP = 36
Private Const SOL_DSN_DS = 37
Private Const SOL_ARCHIVODESA = 38
Private Const SOL_PETICIONTITULO = 39

Private Const CANTIDAD_COLUMNAS = 40

Dim flgEnCarga As Boolean
Dim lUltimaColumnaOrdenada As Long
Dim bSorting As Boolean
Dim bHabilitadoParaGenerarSolicitudes As Boolean
Dim cantidadDiasAnteriores As Long
Dim cantidadDiasPosteriores As Long
Dim cantidadDiaHabilInicial As Long
Dim bMostrarHelpInicial As Boolean
Dim sMensaje As String
Dim bFlagGuiaAyuda As Boolean

Private Const ESPACIOS_COMBOS = 50

'Private Sub cmdOpciones_Click()
'    Load frmCLISTOpciones
'    frmCLISTOpciones.Show
'End Sub

Private Sub Form_Load()
    Call Puntero(True)
    Call Inicializar
    Call CargarGrilla
    Call Puntero(False)
End Sub

Private Sub Inicializar()
    Call Status("Inicializando...")
    Me.Top = 0
    Me.Left = 0
    Me.Height = FORM_HEIGHT
    Me.Width = FORM_WIDTH
    'Me.WindowState = vbMaximized
    bFlagGuiaAyuda = True
    Call IniciarScroll(grdDatos)
    chkEmergencia.ToolTipText = "Si se encuentra deshabilitado temporalmente puede activar este checkbox para generar de todos modos una solicitud con caracter de EMErgencia."
    cmdGenerar.ToolTipText = "Generar una nueva solicitud"
    cmdCopiar.ToolTipText = "Generar nueva solicitud copiando una existente"
    cmdCancelar.ToolTipText = "Cancelar una solicitud generada"
    cmdRelease.ToolTipText = "Liberar una solicitud pendiente de restore para procesamiento en CM"
    flgEnCarga = True
    
    '{ add -011- a.
    ' Mostrar la gu�a r�pida para solicitudes al generar una solicitud
    If sp_GetVarios("CLIST010") Then
        bMostrarHelpInicial = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, True, False)
    End If
    If bMostrarHelpInicial Then
        Load frmCLISTHelp               ' Cargo el formulario con la gu�a r�pida
    End If
    '}
    ' Mensaje de ayuda
    sMensaje = "El siguiente proceso ejecuta a partir de las 8:00, cada hora, hasta las 18:00 hs. inclusive;" & vbCrLf & "con una ejecuci�n adicional a las 17:30 hs." & vbCrLf & vbCrLf & _
            "1. CGM genera y transmite al host los siguientes archivos con las solicitudes:" & vbCrLf & _
            vbTab & "- ARBP.BC.XCOM.PEDIDOS.CGM" & vbTab & sMsgXCOM & vbCrLf & _
            vbTab & "- ARBP.BC.UNLO.PEDIDOS.CGM" & vbTab & sMsgUNLO & vbCrLf & _
            vbTab & "- ARBP.BC.TCOR.PEDIDOS.CGM" & vbTab & sMsgTCOR & vbCrLf & _
            vbTab & "- ARBP.BC.SORT.PEDIDOS.CGM" & vbTab & sMsgSORT & vbCrLf & vbCrLf & _
            "2. Corren los jobs (sss=iniciales subgerente):" & vbCrLf & _
            vbTab & "- XCOMDESX:" & vbTab & "submite un DYDOsssK por c/pedido" & vbTab & sMsgXCOM & vbCrLf & _
            vbTab & "- UNLODESX:" & vbTab & "submite un DYDOsssK por c/pedido" & vbTab & sMsgUNLO & vbCrLf & _
            vbTab & "- TCORDESX:" & vbTab & "submite un DYDOsssK por c/pedido" & vbTab & sMsgTCOR & vbCrLf & _
            vbTab & "- SORTARCX:" & vbTab & "submite un DYDOsssK por c/pedido" & vbTab & sMsgSORT & vbCrLf & vbCrLf & _
            "3. Luego, por cada transmisi�n corre un JOB que genera el ARBP.ENM. Ese JOB ser� (sss=iniciales subgerente):" & vbCrLf & _
            vbTab & "- DYDOsssC" & vbTab & sMsgXCOM & vbCrLf & _
            vbTab & "- DYDOsssU" & vbTab & sMsgUNLO & vbCrLf & _
            vbTab & "- DYDOsssT" & vbTab & sMsgTCOR & vbCrLf & _
            vbTab & "- DYDOsssS" & vbTab & sMsgSORT & vbCrLf & vbCrLf & _
            "4. Corre enmascarador. Jobs involucrados:" & vbCrLf & _
            vbTab & "- ENMASCAR" & vbCrLf & vbTab & "- DYDENMAK" & vbCrLf & vbTab & "- DYDENMAA" & vbCrLf & vbTab & "- DYDENMAB" & vbCrLf & vbCrLf & _
            "5. Los resultados quedan en:" & vbCrLf & _
            vbTab & "- ARBZ.BC.XCOM.RESULTA.Dddmmaa" & vbTab & sMsgXCOM & vbCrLf & _
            vbTab & "- ARBZ.BC.UNLO.RESULTA.Dddmmaa" & vbTab & sMsgUNLO & vbCrLf & _
            vbTab & "- ARBZ.BC.TCOR.RESULTA.Dddmmaa" & vbTab & sMsgTCOR & vbCrLf & _
            vbTab & "- ARBZ.BC.SORT.RESULTA.Dddmmaa" & vbTab & sMsgSORT
    
    ' Inicializo las fechas para evitar un desbordarmiento del FlexGrid
    mskFechaIni.ToolTipText = "Formato de ingreso: dd/mm/aaaa"
    mskFechaFin.ToolTipText = "Formato de ingreso: dd/mm/aaaa"
    mskFechaIni.text = Format(DateAdd("m", -3, date), "dd/mm/yyyy"): mskFechaIni.DateValue = mskFechaIni.text
    mskFechaFin.text = Format(date, "dd/mm/yyyy"): mskFechaFin.DateValue = mskFechaFin.text
    ' Filtros
    With cmbTipoSolicitud
        .Clear
        .AddItem "* Todos" & Space(ESPACIOS_COMBOS) & "||N"
        If sp_GetTipoSolicitud(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_tipo) & " : " & ClearNull(aplRST.Fields!dsc_tipo) & Space(ESPACIOS_COMBOS) & "||" & ClearNull(aplRST.Fields!cod_tipo)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
    With cmbFiltroEstado
        .Clear
        .AddItem "* Todos" & Space(ESPACIOS_COMBOS) & "||N"
        .AddItem "Pendiente aprobaci�n" & Space(ESPACIOS_COMBOS) & "||A"
        .AddItem "Aprobado" & Space(ESPACIOS_COMBOS) & "||C"
        .AddItem "En proceso de envio" & Space(ESPACIOS_COMBOS) & "||D"
        .AddItem "Enviado y finalizado" & Space(ESPACIOS_COMBOS) & "||E"                ' add -005- a.
        .AddItem "En espera (Restore)" & Space(ESPACIOS_COMBOS) & "||F"                ' add -003- b.
        .AddItem "En espera (ejecuci�n diferida)" & Space(ESPACIOS_COMBOS) & "||G"      ' add -003- b.
        .ListIndex = 0
    End With
    With cmbEnm
        .Clear
        .AddItem "* Todos" & Space(ESPACIOS_COMBOS) & "||T"
        .AddItem "Si" & Space(ESPACIOS_COMBOS) & "||S"
        .AddItem "No" & Space(ESPACIOS_COMBOS) & "||N"
        .ListIndex = 0
    End With
    chkOnlyPending.value = 0
    If InPerfil("ASEG") Then Call setHabilCtrl(chkOnlyPending, "DIS")
    flgEnCarga = False
    If glLOGIN_Gerencia <> "DESA" Then
        cmdGenerar.Enabled = False
        cmdCopiar.Enabled = False
        cmdCancelar.Enabled = False
    End If
    '{ del -007- a.
    If InPerfil("ASEG") Then
        cmdGenerar.Default = False
        cmdGenerar.Enabled = False
        cmdCopiar.Enabled = False
        cmdCancelar.Enabled = False
        cmdEnviarCorreo.Enabled = False
        cmdRelease.Enabled = False
    Else
        cmdGenerar.Enabled = True
        cmdCopiar.Enabled = True
        cmdCancelar.Enabled = True
    End If
    cmdGenerar.Enabled = True
    '}
    ' Cargo los par�metros
    If sp_GetVarios("CLIST002") Then cantidadDiasAnteriores = IIf(ClearNull(aplRST.Fields!var_numero) = "", 0, ClearNull(aplRST.Fields!var_numero))
    If sp_GetVarios("CLIST001") Then cantidadDiasPosteriores = IIf(ClearNull(aplRST.Fields!var_numero) = "", 0, ClearNull(aplRST.Fields!var_numero))
    If sp_GetVarios("CLIST003") Then cantidadDiaHabilInicial = IIf(ClearNull(aplRST.Fields!var_numero) = "", 0, ClearNull(aplRST.Fields!var_numero))
    
    If sp_GetVarios("CLIST000") Then
        If ClearNull(aplRST.Fields!var_numero) = "1" Then chkEmergencia.Enabled = False
    End If
    
    If ValidarEmailDeRecurso Then
        bHabilitadoParaGenerarSolicitudes = True
    End If
    Call Status("Listo.")
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .visible = False
        .Clear
        .Font = "Tahoma"
        .Font.Size = 8
        .BackColorFixed = &H8000000F
        .cols = CANTIDAD_COLUMNAS
        .Rows = 1
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusNone
        .TextMatrix(0, SOL_NUMERO) = "N�": .ColWidth(SOL_NUMERO) = 800: .ColAlignment(SOL_NUMERO) = flexAlignRightCenter
        .TextMatrix(0, SOL_ENM) = "Enm.": .ColWidth(SOL_ENM) = 500: .ColAlignment(SOL_ENM) = flexAlignCenterCenter
        .TextMatrix(0, sol_tipo) = "Tipo": .ColWidth(sol_tipo) = 700: .ColAlignment(sol_tipo) = flexAlignLeftCenter
        .TextMatrix(0, sol_diferida) = "Dif.": .ColWidth(sol_diferida) = 500: .ColAlignment(sol_diferida) = flexAlignCenterCenter
        .TextMatrix(0, SOL_FECHA) = "Fecha": .ColWidth(SOL_FECHA) = 1200: .ColAlignment(SOL_FECHA) = flexAlignCenterCenter
        .TextMatrix(0, sol_estado) = "Estado": .ColWidth(sol_estado) = 2250: .ColAlignment(sol_estado) = flexAlignLeftCenter
        .TextMatrix(0, sol_eme) = "EME": .ColWidth(sol_eme) = 500: .ColAlignment(sol_eme) = flexAlignCenterCenter      ' add -010- a.
        .TextMatrix(0, SOL_PETICION) = "Petici�n": .ColWidth(SOL_PETICION) = 800: .ColAlignment(SOL_PETICION) = flexAlignRightCenter
        .TextMatrix(0, SOL_RESP_EJEC) = "Resp. Ejec.": .ColWidth(SOL_RESP_EJEC) = 1200: .ColAlignment(SOL_RESP_EJEC) = flexAlignLeftCenter
        .TextMatrix(0, SOL_RESP_SECT) = "Resp. Sect.": .ColWidth(SOL_RESP_SECT) = 1200: .ColAlignment(SOL_RESP_SECT) = flexAlignLeftCenter
        .TextMatrix(0, SOL_APROBADO) = "Aprobaci�n requerida": .ColWidth(SOL_APROBADO) = 3000: .ColAlignment(SOL_APROBADO) = flexAlignLeftCenter
        '{ add -010- a.
        .TextMatrix(0, SOL_FCHAPROB1) = "Fch. N4": .ColWidth(SOL_FCHAPROB1) = 1200: .ColAlignment(SOL_FCHAPROB1) = flexAlignCenterCenter
        .TextMatrix(0, SOL_FCHAPROB2) = "Fch. N3": .ColWidth(SOL_FCHAPROB2) = 1200: .ColAlignment(SOL_FCHAPROB2) = flexAlignCenterCenter
        '}
        .TextMatrix(0, SOL_DSN_NOM) = "DSN utilizado": .ColWidth(SOL_DSN_NOM) = 2500: .ColAlignment(SOL_DSN_NOM) = flexAlignLeftCenter
        .TextMatrix(0, SOL_DSN_DS) = "DS": .ColWidth(SOL_DSN_DS) = 500: .ColAlignment(SOL_DSN_DS) = flexAlignCenterCenter
        ' INVISIBLE a partir de aqu�
        .TextMatrix(0, SOL_POR) = "Solicitado por": .ColWidth(SOL_POR) = 0: .ColAlignment(SOL_POR) = flexAlignLeftCenter
        .TextMatrix(0, SOL_SOLESTADO) = "": .ColWidth(SOL_SOLESTADO) = 0: .ColAlignment(SOL_SOLESTADO) = flexAlignLeftCenter
        .TextMatrix(0, SOL_SEG) = "Seguridad": .ColWidth(SOL_SEG) = 0: .ColAlignment(SOL_SEG) = flexAlignLeftCenter
        .TextMatrix(0, SOL_SOLICITANTE) = "Solicitante": .ColWidth(SOL_SOLICITANTE) = 0: .ColAlignment(SOL_SOLICITANTE) = flexAlignLeftCenter
        .TextMatrix(0, SOL_JUSCOD) = "JUSCOD": .ColWidth(SOL_JUSCOD) = 0
        .TextMatrix(0, SOL_JUSDSC) = "JUSDSC": .ColWidth(SOL_JUSDSC) = 0
        .TextMatrix(0, SOL_SOLITEXTO) = "Descripci�n de motivo": .ColWidth(SOL_SOLITEXTO) = 0
        .TextMatrix(0, SOL_ARCHIVOPROD) = "Archivo producci�n": .ColWidth(SOL_ARCHIVOPROD) = 0
        .TextMatrix(0, SOL_ARCHIVODESA) = "Archivo desarrollo": .ColWidth(SOL_ARCHIVODESA) = 0
        .TextMatrix(0, SOL_ARCHIVO_OUT) = "SOL_ARCHIVO_OUT": .ColWidth(SOL_ARCHIVO_OUT) = 0
        .TextMatrix(0, SOL_DSNID) = "DSN_ID": .ColWidth(SOL_DSNID) = 0
        .TextMatrix(0, SOL_CPYBBL) = "SOL_CPYBBL": .ColWidth(SOL_CPYBBL) = 0
        .TextMatrix(0, SOL_CPYNOM) = "SOL_CPYNOM": .ColWidth(SOL_CPYNOM) = 0
        .TextMatrix(0, SOL_NOMSOLICITA) = "SOL_NOMSOLICITA": .ColWidth(SOL_NOMSOLICITA) = 0
        .TextMatrix(0, SOL_NOMRESPEJEC) = "SOL_NOMRESPEJEC": .ColWidth(SOL_NOMRESPEJEC) = 0
        .TextMatrix(0, SOL_NOMRESPSECT) = "SOL_NOMRESPSECT": .ColWidth(SOL_NOMRESPSECT) = 0
        .TextMatrix(0, sol_LIB_SYSIN) = "SOL_LIB_SYSIN": .ColWidth(sol_LIB_SYSIN) = 0
        .TextMatrix(0, SOL_MEM_SYSIN) = "SOL_MEM_SYSIN": .ColWidth(SOL_MEM_SYSIN) = 0
        .TextMatrix(0, SOL_JOIN) = "SOL_JOIN": .ColWidth(SOL_JOIN) = 0
        .TextMatrix(0, SOL_NROTABLA) = "SOL_NROTABLA": .ColWidth(SOL_NROTABLA) = 0
        .TextMatrix(0, SOL_FCHABACKUP) = "SOL_FCHABACKUP": .ColWidth(SOL_FCHABACKUP) = 0
        .TextMatrix(0, SOL_ARCHIVOINTERMEDIO) = "SOL_ARCHIVOINTERMEDIO": .ColWidth(SOL_ARCHIVOINTERMEDIO) = 0
        .TextMatrix(0, SOL_TABLA_BACKUP) = "SOL_TABLA_BACKUP": .ColWidth(SOL_TABLA_BACKUP) = 0
        .TextMatrix(0, SOL_JOB_BACKUP) = "SOL_JOB_BACKUP": .ColWidth(SOL_JOB_BACKUP) = 0
        .TextMatrix(0, SOL_PETICIONTITULO) = "SOL_PETICIONTITULO": .ColWidth(SOL_PETICIONTITULO) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Public Sub CargarGrilla()
    On Error GoTo Errores
    If Not flgEnCarga Then
        Call Puntero(True)
        Call Status("Cargando...")
        Call InicializarGrilla
        Call sp_GetSolicitud(IIf(InPerfil("ASEG"), Null, glLOGIN_ID_REEMPLAZO), IIf(CodigoCombo(cmbTipoSolicitud, True) = "N", Null, CodigoCombo(cmbTipoSolicitud, True)), Null, CodigoCombo(cmbFiltroEstado, True), CodigoCombo(cmbEnm, True), IIf(IsDate(mskFechaIni.DateValue), mskFechaIni.DateValue, Null), IIf(IsDate(mskFechaFin.DateValue), mskFechaFin.DateValue, Null))
        If Not aplRST.EOF Then
            Do While Not aplRST.EOF
                With grdDatos
                    If (chkOnlyPending.value = 1 And (ClearNull(aplRST.Fields!sol_estado) = "A" Or ClearNull(aplRST.Fields!sol_estado) = "B")) Or (chkOnlyPending.value <> 1) Then
                        grdDatos.Rows = grdDatos.Rows + 1
                        .TextMatrix(.Rows - 1, SOL_NUMERO) = aplRST.Fields!sol_nroasignado
                        .TextMatrix(.Rows - 1, sol_eme) = IIf(ClearNull(aplRST.Fields!sol_eme) = "S", "�", "")                       ' add -010- a.
                        .TextMatrix(.Rows - 1, sol_tipo) = ClearNull(aplRST.Fields!sol_tipo)
                        .TextMatrix(.Rows - 1, SOL_ENM) = IIf(aplRST.Fields!sol_mask = "S", "Si", "No")
                        .TextMatrix(.Rows - 1, sol_diferida) = IIf(ClearNull(aplRST.Fields!sol_diferida) = "S", "�", "")
                        .TextMatrix(.Rows - 1, SOL_FECHA) = Format(aplRST.Fields!SOL_FECHA, "yyyy/mm/dd")
                        .TextMatrix(.Rows - 1, SOL_POR) = Trim(aplRST.Fields!nom_recurso)
                        .TextMatrix(.Rows - 1, sol_estado) = Trim(aplRST.Fields!nom_estado)
                        .TextMatrix(.Rows - 1, SOL_PETICION) = IIf(IsNull(aplRST.Fields!pet_nroasignado), "-", ClearNull(aplRST.Fields!pet_nroasignado))
                        '.TextMatrix(.Rows - 1, SOL_RESP_EJEC) = IIf(ClearNull(aplRST.Fields!SOL_RESP_EJEC) = "", "-", ClearNull(aplRST.Fields!nom_resp_ejec))
                        .TextMatrix(.Rows - 1, SOL_RESP_EJEC) = IIf(ClearNull(aplRST.Fields!SOL_RESP_EJEC) = "", "-", ClearNull(aplRST.Fields!SOL_RESP_EJEC))
                        
                        '.TextMatrix(.Rows - 1, SOL_RESP_SECT) = ClearNull(aplRST.Fields!nom_resp_sect)
                        .TextMatrix(.Rows - 1, SOL_RESP_SECT) = IIf(ClearNull(aplRST.Fields!SOL_RESP_SECT) = "", "-", ClearNull(aplRST.Fields!SOL_RESP_SECT))
                        
                        .TextMatrix(.Rows - 1, SOL_SEG) = ClearNull(aplRST.Fields!nom_sol_seguridad)
                        .TextMatrix(.Rows - 1, SOL_SOLESTADO) = ClearNull(aplRST.Fields!sol_estado)
                        .TextMatrix(.Rows - 1, SOL_APROBADO) = ClearNull(aplRST.Fields!Aprobado)
                        '{ add -010- a.
                        .TextMatrix(.Rows - 1, SOL_FCHAPROB1) = IIf(IsNull(aplRST.Fields!sol_fe_auto1), "-", Format(aplRST.Fields!sol_fe_auto1, "yyyy/mm/dd"))
                        .TextMatrix(.Rows - 1, SOL_FCHAPROB2) = IIf(IsNull(aplRST.Fields!sol_fe_auto2), "-", Format(aplRST.Fields!sol_fe_auto2, "yyyy/mm/dd"))
                        '}
                        .TextMatrix(.Rows - 1, SOL_SOLICITANTE) = ClearNull(aplRST.Fields!sol_recurso)
                        .TextMatrix(.Rows - 1, SOL_JUSCOD) = ClearNull(aplRST.Fields!jus_codigo)
                        .TextMatrix(.Rows - 1, SOL_JUSDSC) = ClearNull(aplRST.Fields!jus_desc)
                        .TextMatrix(.Rows - 1, SOL_SOLITEXTO) = ClearNull(aplRST.Fields!sol_texto)
                        .TextMatrix(.Rows - 1, SOL_DSN_DS) = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "S", "�", "")
                        .TextMatrix(.Rows - 1, SOL_ARCHIVOPROD) = ClearNull(aplRST.Fields!sol_file_prod)
                        .TextMatrix(.Rows - 1, SOL_ARCHIVODESA) = ClearNull(aplRST.Fields!sol_file_desa)
                        .TextMatrix(.Rows - 1, SOL_ARCHIVO_OUT) = ClearNull(aplRST.Fields!sol_file_out)
                        .TextMatrix(.Rows - 1, SOL_ARCHIVOINTERMEDIO) = ClearNull(aplRST.Fields!sol_file_inter)
                        .TextMatrix(.Rows - 1, SOL_DSNID) = ClearNull(aplRST.Fields!dsn_id)
                        .TextMatrix(.Rows - 1, SOL_DSN_NOM) = ClearNull(aplRST.Fields!dsn_nom)
                        .TextMatrix(.Rows - 1, SOL_CPYBBL) = ClearNull(aplRST.Fields!sol_prod_cpybbl)
                        .TextMatrix(.Rows - 1, SOL_CPYNOM) = ClearNull(aplRST.Fields!sol_prod_cpynom)
                        .TextMatrix(.Rows - 1, SOL_NOMSOLICITA) = ClearNull(aplRST.Fields!nom_recurso)
                        .TextMatrix(.Rows - 1, SOL_NOMRESPEJEC) = ClearNull(aplRST.Fields!nom_resp_ejec)
                        .TextMatrix(.Rows - 1, SOL_NOMRESPSECT) = ClearNull(aplRST.Fields!nom_resp_sect)
                        .TextMatrix(.Rows - 1, sol_LIB_SYSIN) = ClearNull(aplRST.Fields!sol_LIB_SYSIN)
                        .TextMatrix(.Rows - 1, SOL_MEM_SYSIN) = ClearNull(aplRST.Fields!SOL_MEM_SYSIN)
                        .TextMatrix(.Rows - 1, SOL_JOIN) = ClearNull(aplRST.Fields!SOL_JOIN)
                        .TextMatrix(.Rows - 1, SOL_NROTABLA) = ClearNull(aplRST.Fields!SOL_NROTABLA)
                        .TextMatrix(.Rows - 1, SOL_FCHABACKUP) = ClearNull(aplRST.Fields!sol_fe_auto3)
                        .TextMatrix(.Rows - 1, SOL_TABLA_BACKUP) = ClearNull(aplRST.Fields!sol_bckuptabla)
                        .TextMatrix(.Rows - 1, SOL_JOB_BACKUP) = ClearNull(aplRST.Fields!sol_bckupjob)
                        .TextMatrix(.Rows - 1, SOL_PETICIONTITULO) = ClearNull(aplRST.Fields!pet_titulo)
                    End If
                End With
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If aplRST.State = 1 Then Call Status(aplRST.RecordCount & " solicitudes")
        grdDatos.visible = True
        Call MostrarSeleccion
    End If
    Call Status("Listo.")
    Call Puntero(False)
Exit Sub
Errores:
    If Err.Number = 30006 Then
        MsgBox "No hay espacio suficiente para visualizar todos los datos seleccionados." & vbCrLf & "Utilice los filtros para acotar la informaci�n mostrada.", vbExclamation + vbOKOnly, "Memoria insuficiente"
        Call Status(grdDatos.Rows + 1 & " solicitudes")
        grdDatos.visible = True
        Call MostrarSeleccion
        Call Status("Listo.")
        Call Puntero(False)
    Else
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        grdDatos.visible = True
        Call MostrarSeleccion
        Call Status("Listo.")
        Call Puntero(False)
    End If
End Sub

Private Sub MostrarSeleccion()
    txtSolicitudGenerada.text = ""
    txtSolicitudJustificacion.text = ""
    Call setHabilCtrl(cmdEnviarCorreo, "DIS")
    Call setHabilCtrl(cmdRelease, "DIS")
    With grdDatos
        If .rowSel > 0 Then
            If .TextMatrix(.rowSel, SOL_NUMERO) <> "" Then
                txtSolicitudGenerada.visible = False
                '{ add -003- a.
                If .TextMatrix(.rowSel, sol_tipo) = "XCOM*" Or _
                    .TextMatrix(.rowSel, sol_tipo) = "UNLO*" Or _
                    .TextMatrix(.rowSel, sol_tipo) = "SORT*" Then
                    ' Cualquiera de las dos opciones con Restore, si estan pendientes de liberar,
                    ' entonces debe permitirse el reenvio del correo (por cualquier error) y la liberaci�n
                    ' (para cuando corresponda)
                        If .TextMatrix(.rowSel, SOL_SOLESTADO) = "F" Then
                            Call setHabilCtrl(cmdEnviarCorreo, "NOR")
                            Call setHabilCtrl(cmdRelease, "NOR")
                        End If
                End If
                '}
                Call MostrarDatosSolicitud
                txtSolicitudGenerada.visible = True
            End If
        End If
    End With
End Sub

Private Sub MostrarDatosSolicitud()
    Dim cTexto As String
    
    cTexto = ""
    With grdDatos
        Select Case .TextMatrix(.rowSel, sol_tipo)
            Case "XCOM"
                cTexto = cTexto & " ARCHIVO IN  = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVOPROD)) & vbCrLf
                cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVODESA)) & vbCrLf
                If ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) <> "" Then
                    cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) & vbCrLf
                    cTexto = cTexto & " NOMBRE COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYNOM)) & vbCrLf
                End If
                cTexto = cTexto & " SOLICITANTE = " & ClearNull(.TextMatrix(.rowSel, SOL_SOLICITANTE)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMSOLICITA)) & ")" & vbCrLf
                cTexto = cTexto & " SUPERVISOR  = " & ClearNull(.TextMatrix(.rowSel, SOL_RESP_SECT)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMRESPSECT)) & ")" & vbCrLf
                cTexto = cTexto & " PETICI�N    = " & IIf(ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) = "-", "-", ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_PETICIONTITULO)) & ")") & vbCrLf
                cTexto = cTexto & " FECHA       = " & Format(ClearNull(.TextMatrix(.rowSel, SOL_FECHA)), "dd/MM/yy")
            Case "UNLO"
                cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVODESA)) & vbCrLf
                If ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) <> "" Then
                    cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) & vbCrLf
                    cTexto = cTexto & " NOMBRE COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYNOM)) & vbCrLf
                End If
                cTexto = cTexto & " LIB SYSIN   = " & ClearNull(.TextMatrix(.rowSel, sol_LIB_SYSIN)) & vbCrLf
                cTexto = cTexto & " MBR SYSIN   = " & ClearNull(.TextMatrix(.rowSel, SOL_MEM_SYSIN)) & vbCrLf
                cTexto = cTexto & " JOIN        = " & ClearNull(.TextMatrix(.rowSel, SOL_JOIN)) & vbCrLf
                cTexto = cTexto & " SOLICITANTE = " & ClearNull(.TextMatrix(.rowSel, SOL_SOLICITANTE)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMSOLICITA)) & ")" & vbCrLf
                cTexto = cTexto & " SUPERVISOR  = " & ClearNull(.TextMatrix(.rowSel, SOL_RESP_SECT)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMRESPSECT)) & ")" & vbCrLf
                cTexto = cTexto & " PETICI�N    = " & ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_PETICIONTITULO)) & ")" & vbCrLf
                cTexto = cTexto & " FECHA       = " & Format(ClearNull(.TextMatrix(.rowSel, SOL_FECHA)), "dd/MM/yy")
            Case "TCOR"
                cTexto = cTexto & " INPUT SORT  = " & Left(ClearNull(.TextMatrix(.rowSel, SOL_NROTABLA)), 4) & vbCrLf
                If ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) <> "" Then
                    cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) & vbCrLf
                    cTexto = cTexto & " NOMBRE COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYNOM)) & vbCrLf
                End If
                cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVODESA)) & vbCrLf
                cTexto = cTexto & " SOLICITANTE = " & ClearNull(.TextMatrix(.rowSel, SOL_SOLICITANTE)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMSOLICITA)) & ")" & vbCrLf
                cTexto = cTexto & " SUPERVISOR  = " & ClearNull(.TextMatrix(.rowSel, SOL_RESP_SECT)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMRESPSECT)) & ")" & vbCrLf
                cTexto = cTexto & " PETICI�N    = " & ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_PETICIONTITULO)) & ")" & vbCrLf
                cTexto = cTexto & " FECHA       = " & Format(ClearNull(.TextMatrix(.rowSel, SOL_FECHA)), "dd/MM/yy")
            Case "SORT"
                cTexto = cTexto & " ARCHIVO INP = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVOPROD)) & vbCrLf
                If ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) <> "" Then
                    cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) & vbCrLf
                    cTexto = cTexto & " NOMBRE COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYNOM)) & vbCrLf
                End If
                cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVODESA)) & vbCrLf
                cTexto = cTexto & " SYSIN       = " & ClearNull(.TextMatrix(.rowSel, SOL_MEM_SYSIN)) & vbCrLf
                cTexto = cTexto & " SOLICITANTE = " & ClearNull(.TextMatrix(.rowSel, SOL_SOLICITANTE)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMSOLICITA)) & ")" & vbCrLf
                cTexto = cTexto & " SUPERVISOR  = " & ClearNull(.TextMatrix(.rowSel, SOL_RESP_SECT)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMRESPSECT)) & ")" & vbCrLf
                cTexto = cTexto & " PETICI�N    = " & ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_PETICIONTITULO)) & ")" & vbCrLf
                cTexto = cTexto & " FECHA       = " & Format(ClearNull(.TextMatrix(.rowSel, SOL_FECHA)), "dd/MM/yy")
            Case "XCOM*"
                'cTexto = cTexto & " ARCHIVO IN  = " & ClearNull(.TextMatrix(.RowSel, SOL_ARCHIVO_OUT)) & vbCrLf
                cTexto = cTexto & " ARCHIVO IN  = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVOPROD)) & vbCrLf
                cTexto = cTexto & " FECHA / BKP = " & ClearNull(.TextMatrix(.rowSel, SOL_FCHABACKUP)) & vbCrLf
                If ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) <> "" Then
                    cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) & vbCrLf
                    cTexto = cTexto & " NOMBRE COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYNOM)) & vbCrLf
                End If
                cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVODESA)) & vbCrLf
                cTexto = cTexto & " SOLICITANTE = " & ClearNull(.TextMatrix(.rowSel, SOL_SOLICITANTE)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMSOLICITA)) & ")" & vbCrLf
                cTexto = cTexto & " SUPERVISOR  = " & ClearNull(.TextMatrix(.rowSel, SOL_RESP_SECT)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMRESPSECT)) & ")" & vbCrLf
                cTexto = cTexto & " PETICI�N    = " & ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_PETICIONTITULO)) & ")" & vbCrLf
                cTexto = cTexto & " FECHA       = " & Format(ClearNull(.TextMatrix(.rowSel, SOL_FECHA)), "dd/MM/yy")
            Case "UNLO*"
                cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVODESA)) & vbCrLf
                'cTexto = cTexto & " ARCHIVO BKP = " & ClearNull(.TextMatrix(.RowSel, SOL_ARCHIVOINTERMEDIO)) & vbCrLf
                If ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) <> "" Then
                    cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) & vbCrLf
                    cTexto = cTexto & " NOMBRE COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYNOM)) & vbCrLf
                End If
                cTexto = cTexto & " LIB SYSIN   = " & ClearNull(.TextMatrix(.rowSel, sol_LIB_SYSIN)) & vbCrLf
                cTexto = cTexto & " MBR SYSIN   = " & ClearNull(.TextMatrix(.rowSel, SOL_MEM_SYSIN)) & vbCrLf
                cTexto = cTexto & " TABLA       = " & ClearNull(.TextMatrix(.rowSel, SOL_TABLA_BACKUP)) & vbCrLf
                cTexto = cTexto & " FECHA / BKP = " & ClearNull(.TextMatrix(.rowSel, SOL_FCHABACKUP)) & vbCrLf
                cTexto = cTexto & " JOB   / BKP = " & ClearNull(.TextMatrix(.rowSel, SOL_JOB_BACKUP)) & vbCrLf
                cTexto = cTexto & " JOIN        = " & ClearNull(.TextMatrix(.rowSel, SOL_JOIN)) & vbCrLf
                cTexto = cTexto & " SOLICITANTE = " & ClearNull(.TextMatrix(.rowSel, SOL_SOLICITANTE)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMSOLICITA)) & ")" & vbCrLf
                cTexto = cTexto & " SUPERVISOR  = " & ClearNull(.TextMatrix(.rowSel, SOL_RESP_SECT)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMRESPSECT)) & ")" & vbCrLf
                cTexto = cTexto & " PETICI�N    = " & ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_PETICIONTITULO)) & ")" & vbCrLf
                cTexto = cTexto & " FECHA       = " & Format(ClearNull(.TextMatrix(.rowSel, SOL_FECHA)), "dd/MM/yy")
            Case "SORT*"
                cTexto = cTexto & " FECHA / BKP = " & ClearNull(.TextMatrix(.rowSel, SOL_FCHABACKUP)) & vbCrLf
                cTexto = cTexto & " ARCHIVO INP = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVOPROD)) & vbCrLf
                If ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) <> "" Then
                    cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYBBL)) & vbCrLf
                    cTexto = cTexto & " NOMBRE COPY = " & ClearNull(.TextMatrix(.rowSel, SOL_CPYNOM)) & vbCrLf
                End If
                'cTexto = cTexto & " ARCHIVO INT = " & ClearNull(.TextMatrix(.RowSel, SOL_ARCHIVODESA)) & vbCrLf
                cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(.TextMatrix(.rowSel, SOL_ARCHIVODESA)) & vbCrLf
                cTexto = cTexto & " SYSIN       = " & ClearNull(.TextMatrix(.rowSel, SOL_MEM_SYSIN)) & vbCrLf
                cTexto = cTexto & " SOLICITANTE = " & ClearNull(.TextMatrix(.rowSel, SOL_SOLICITANTE)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMSOLICITA)) & ")" & vbCrLf
                cTexto = cTexto & " SUPERVISOR  = " & ClearNull(.TextMatrix(.rowSel, SOL_RESP_SECT)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_NOMRESPSECT)) & ")" & vbCrLf
                cTexto = cTexto & " PETICI�N    = " & ClearNull(.TextMatrix(.rowSel, SOL_PETICION)) & " (" & ClearNull(.TextMatrix(.rowSel, SOL_PETICIONTITULO)) & ")" & vbCrLf
                cTexto = cTexto & " FECHA       = " & Format(ClearNull(.TextMatrix(.rowSel, SOL_FECHA)), "dd/MM/yy")
        End Select
        txtSolicitudGenerada = cTexto
        txtSolicitudJustificacion = ClearNull(.TextMatrix(.rowSel, SOL_JUSCOD)) & ". " & ClearNull(.TextMatrix(.rowSel, SOL_JUSDSC)) & vbCrLf & vbCrLf & ClearNull(.TextMatrix(.rowSel, SOL_SOLITEXTO))
    End With
End Sub

Private Sub cmdGenerar_Click()
    If bHabilitadoParaGenerarSolicitudes Then
        'If HabilitarCarga(txtFechaPrueba) Then
        If HabilitarCarga(Now, True) Then
            If bMostrarHelpInicial Then
                If bFlagGuiaAyuda Then
                    frmCLISTHelp.Show 1             ' add -011- a.
                    bFlagGuiaAyuda = False
                End If
            End If
            frmCLISTCarga.cModo = "AGREGAR"
            frmCLISTCarga.Show
        '{ add -010- a.
        Else
            If chkEmergencia.value = 1 Then
                If bMostrarHelpInicial Then frmCLISTHelp.Show 1         ' add -011- a.
                frmCLISTCarga.cModo = "EMERGENCIA"
                frmCLISTCarga.Show
            Else
                MsgBox "Generaci�n de solicitudes inhabilitada temporalmente.", vbExclamation + vbOKOnly, "Generaci�n de solicitudes"
            End If
        '}
        End If
    Else
        MsgBox "El usuario no tiene habilitado las opciones de correo" & vbCrLf & "electr�nico o no tiene una direcci�n v�lida." & vbCrLf & vbCrLf & "Verifique la situaci�n a trav�s de Gesti�n de la Demanda.", vbExclamation + vbOKOnly, "Direcci�n de mail"
    End If
End Sub

Private Sub cmdCopiar_Click()
    Dim lSolicitud As Long
    Dim sMensaje As String
    
    sMensaje = "Debe seleccionar una solicitud de origen para iniciar la copia. Revise."
    
    If bHabilitadoParaGenerarSolicitudes Then
        If HabilitarCarga(Now, True) Then       ' add -009- a.
            With grdDatos
                If IsNumeric(.TextMatrix(.rowSel, SOL_NUMERO)) Then
                    If .TextMatrix(.rowSel, SOL_NUMERO) > 0 Then
                        If bMostrarHelpInicial Then
                            If bFlagGuiaAyuda Then
                                frmCLISTHelp.Show 1             ' add -011- a.
                                bFlagGuiaAyuda = False
                            End If
                        End If
                        lSolicitud = .TextMatrix(.rowSel, SOL_NUMERO)
                        frmCLISTCarga.lSol_NroAsignado = lSolicitud
                        frmCLISTCarga.cModo = "EDICION"
                        frmCLISTCarga.Show
                    Else
                        MsgBox sMensaje, vbExclamation + vbOKOnly, "Copia de solicitud"
                    End If
                Else
                    MsgBox sMensaje, vbExclamation + vbOKOnly, "Copia de solicitud"
                End If
            End With
        Else
            With grdDatos
                If chkEmergencia.value = 1 Then
                    If IsNumeric(.TextMatrix(.rowSel, SOL_NUMERO)) Then
                        If .TextMatrix(.rowSel, SOL_NUMERO) > 0 Then
                            lSolicitud = .TextMatrix(.rowSel, SOL_NUMERO)
                            frmCLISTCarga.lSol_NroAsignado = lSolicitud
                            frmCLISTCarga.cModo = "EDICION"
                            frmCLISTCarga.Show
                        Else
                            MsgBox sMensaje, vbExclamation + vbOKOnly, "Copia de solicitud"
                        End If
                    Else
                        MsgBox sMensaje, vbExclamation + vbOKOnly, "Copia de solicitud"
                    End If
                Else
                    MsgBox "Generaci�n de solicitudes inhabilitada temporalmente.", vbExclamation + vbOKOnly, "Generaci�n de solicitudes"
                End If
            End With
        End If                          ' add -009- a.
    Else
        MsgBox "El usuario no tiene habilitado las opciones de correo" & vbCrLf & "electr�nico o no tiene una direcci�n v�lida." & vbCrLf & vbCrLf & "Verifique la situaci�n a trav�s de Gesti�n de la Demanda.", vbExclamation + vbOKOnly, "Direcci�n de mail"
    End If
End Sub

Private Function ValidarEmailDeRecurso() As Boolean
    ValidarEmailDeRecurso = False
    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        If ClearNull(aplRST.Fields!euser) = "S" And Len(ClearNull(aplRST.Fields!email)) > 0 Then
            ValidarEmailDeRecurso = True
        End If
    End If
End Function

Private Sub Form_Resize()
    'Me.WindowState = vbMaximized
End Sub

Private Sub grdDatos_RowColChange()
    If Not bFormateando Then Call MostrarSeleccion
End Sub

Private Sub cmdCancelar_Click()
    Dim lSolicitud As Long
    
    With grdDatos
        If .rowSel > 0 Then         ' ERROR EN PROD!!! 08.08.2014
            If .TextMatrix(.rowSel, SOL_NUMERO) > 0 Then
                If Trim(.TextMatrix(.rowSel, SOL_SOLICITANTE)) = glLOGIN_ID_REEMPLAZO Then
                    If InStr(1, "C|A|B|G|", Trim(.TextMatrix(.rowSel, SOL_SOLESTADO))) > 0 Then   ' upd -002- a. Se agrega el estado "Pendiente de aprobaci�n" (|A) ' upd -004- a.
                        lSolicitud = .TextMatrix(.rowSel, SOL_NUMERO)
                        If MsgBox("Al cancelar una solicitud generada, el sistema elimina el registro de manera que no sea" & vbCrLf & "enviado a Carga de M�quina para su ejecuci�n." & vbCrLf & vbCrLf & "�Confirma la cancelaci�n de la solicitud seleccionada?", vbQuestion + vbYesNo) = vbYes Then
                            Call sp_DeleteSolicitudes(lSolicitud)
                            Call sp_UpdateHEDT001Field(.TextMatrix(.rowSel, SOL_DSNID), "suma_dsn_cantuso", Null, Null, -1)
                            Call CargarGrilla
                        End If
                    Else
                        MsgBox "La solicitud ya ha sido enviada. No puede ser cancelada.", vbExclamation + vbOKOnly, "Cancelar solicitud"
                    End If
                Else
                    MsgBox "No puede cancelar solicitudes generadas por otros recursos.", vbExclamation + vbOKOnly, "Cancelar solicitud"
                End If
            Else
                MsgBox "Debe seleccionar una solicitud para cancelar. Revise.", vbExclamation + vbOKOnly, "Cancelar solicitud"
            End If
        End If
    End With
End Sub

'{ add -003- a.
Private Sub cmdEnviarCorreo_Click()
    With grdDatos
        If .TextMatrix(.rowSel, sol_tipo) = "XCOM*" Or _
            .TextMatrix(.rowSel, sol_tipo) = "UNLO*" Or _
            .TextMatrix(.rowSel, sol_tipo) = "SORT*" Then
                Call Puntero(True)
                If .TextMatrix(.rowSel, sol_eme) = "S" Then
                    Call EnviarCorreo_EMERGENCIA(.TextMatrix(.rowSel, SOL_NUMERO), True)
                Else
                    Call EnviarCorreo_Restore(.TextMatrix(.rowSel, SOL_NUMERO), True)
                    'MsgBox "Recuerdo que una vez hecho el RESTORE debe liberar la solicitud.", vbInformation + vbOKOnly, "Restore"
                End If
            Call Puntero(False)
        End If
    End With
End Sub

Private Sub cmdRelease_Click()
    With grdDatos
        If .TextMatrix(.rowSel, sol_tipo) = "XCOM*" Or _
            .TextMatrix(.rowSel, sol_tipo) = "UNLO*" Or _
            .TextMatrix(.rowSel, sol_tipo) = "SORT*" Then
                If MsgBox("�Confirma liberar la solicitud seleccionada?", vbQuestion + vbYesNo, "Liberar solicitud de restore") = vbYes Then
                    Call sp_UpdateSolicitudAuto(.TextMatrix(.rowSel, SOL_NUMERO), Null, "REST", Null)
                    Call CargarGrilla
                End If
        End If
    End With
End Sub
'}

Private Sub cmdFiltrar_Click()
    Call CargarGrilla
End Sub

Private Sub cmdExportar_Click()
    Call Exportar_Excel(grdDatos)
End Sub

Private Sub Exportar_Excel(ByVal FlexGrid As MSFlexGrid)
    Dim o_Excel As Object, o_Libro As Object, o_Hoja As Object
    Dim sNombreArchivo As String
    Dim Fila As Long

    sNombreArchivo = Dialogo_GuardarExportacion("GUARDAR")
    If sNombreArchivo = "" Then Exit Sub
    If CrearObjetoExcel(o_Excel, o_Libro, o_Hoja, sNombreArchivo) Then
        Call Puntero(True)
        Call Status("Generando planilla... aguarde...")
        o_Excel.ActiveWindow.Zoom = 80
        o_Libro.Sheets(1).name = "Solicitudes"
        
        Set o_Hoja = o_Libro.Sheets(1)
        o_Hoja.Rows(1).Font.Bold = True
        With FlexGrid
            For Fila = 0 To .Rows - 1
                Call Status("Generando planilla... " & Fila & " de " & .Rows - 1)
                o_Hoja.Cells(Fila + 1, 1).value = .TextMatrix(Fila, SOL_NUMERO)
                o_Hoja.Cells(Fila + 1, 2).value = .TextMatrix(Fila, sol_tipo)
                o_Hoja.Cells(Fila + 1, 3).value = .TextMatrix(Fila, SOL_FECHA)
                o_Hoja.Cells(Fila + 1, 4).value = .TextMatrix(Fila, SOL_ENM)
                o_Hoja.Cells(Fila + 1, 5).value = .TextMatrix(Fila, SOL_JUSDSC)
                o_Hoja.Cells(Fila + 1, 6).value = .TextMatrix(Fila, SOL_SOLITEXTO)
                o_Hoja.Cells(Fila + 1, 7).value = .TextMatrix(Fila, SOL_FECHA)
                o_Hoja.Cells(Fila + 1, 8).value = .TextMatrix(Fila, SOL_RESP_EJEC)
                o_Hoja.Cells(Fila + 1, 9).value = .TextMatrix(Fila, SOL_RESP_SECT)
                o_Hoja.Cells(Fila + 1, 10).value = .TextMatrix(Fila, SOL_ARCHIVOPROD)
                o_Hoja.Cells(Fila + 1, 11).value = .TextMatrix(Fila, SOL_ARCHIVODESA)
                o_Hoja.Cells(Fila + 1, 12).value = .TextMatrix(Fila, SOL_POR)
                o_Hoja.Cells(Fila + 1, 13).value = .TextMatrix(Fila, sol_estado)
                o_Hoja.Cells(Fila + 1, 14).value = .TextMatrix(Fila, SOL_PETICION)
                o_Hoja.Cells(Fila + 1, 15).value = .TextMatrix(Fila, sol_eme)
            Next Fila
        End With
        With o_Hoja
            .Columns(1).ColumnWidth = 10
            .Columns(2).ColumnWidth = 7
            .Columns(3).ColumnWidth = 13
            .Columns(4).ColumnWidth = 7
            .Columns(5).ColumnWidth = 55
            .Columns(6).ColumnWidth = 25
            .Columns(7).ColumnWidth = 13
            .Columns(8).ColumnWidth = 22
            .Columns(9).ColumnWidth = 22
            .Columns(10).ColumnWidth = 50
            .Columns(11).ColumnWidth = 50
            .Columns(12).ColumnWidth = 30
            .Columns(13).ColumnWidth = 20
            .Columns(14).ColumnWidth = 10
            .Columns(15).ColumnWidth = 10
        End With
        Call CerrarObjectoExcel(o_Excel, o_Libro, o_Hoja, sNombreArchivo)
    End If
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub cmdCerrar_Click()
    Call DetenerScroll(grdDatos)
    Unload Me
End Sub

' ************************************************************************************************************************************
' ORDENAMIENTO
' ************************************************************************************************************************************
Private Sub grdDatos_Click()
    bSorting = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    bSorting = False
 End Sub

'{ add -009- a.
Private Function HabilitarCarga(ByVal dFechaAux As Date, Optional bSilentOption As Boolean) As Boolean
    Dim sMensaje As String
    
    HabilitarCarga = False
    
    If sp_GetVarios("CLIST000") Then
        If ClearNull(aplRST.Fields!var_numero) = "0" Then
            sMensaje = "Generaci�n de solicitudes inhabilitada temporalmente."
            If Not bSilentOption Then MsgBox sMensaje, vbExclamation + vbOKOnly, "Generaci�n de solicitudes"
            Exit Function
        End If
    End If
    
    If Evaluar(dFechaAux, sMensaje) Then
        HabilitarCarga = True
        Exit Function
    Else
        If Not bSilentOption Then MsgBox sMensaje, vbExclamation + vbOKOnly, "Generaci�n de solicitudes inhabilitada temporalmente"
    End If
    'If Not bSilentOption Then MsgBox sMensaje, vbExclamation + vbOKOnly, "Generaci�n de solicitudes inhabilitada temporalmente"
End Function
'}

Private Sub lblNoMeLlegaElArchivo_Click()
    'MsgBox sMensaje, vbOKOnly, "Ayuda"
    Call ntvMessageBox("Ayuda", sMensaje, vbInformation + vbOKOnly)
End Sub

Private Sub lblGuiaRapida_Click()
    frmCLISTHelp.Show 1
    bFlagGuiaAyuda = False
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call InicializarLinks
End Sub

Private Sub grdDatos_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call InicializarLinks
End Sub

Private Sub fraOpciones_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call InicializarLinks
End Sub

Private Sub pnlBotones_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call InicializarLinks
End Sub

Private Sub InicializarLinks()
    lblNoMeLlegaElArchivo.ForeColor = vbBlue
    lblNoMeLlegaElArchivo.MousePointer = 0     ' Default
    
    lblGuiaRapida.ForeColor = vbBlue
    lblGuiaRapida.MousePointer = 0     ' Default
End Sub

Private Sub lblNoMeLlegaElArchivo_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblNoMeLlegaElArchivo.ForeColor = vbRed
    lblNoMeLlegaElArchivo.MousePointer = 99
End Sub

Private Sub lblGuiaRapida_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblGuiaRapida.ForeColor = vbRed
    lblGuiaRapida.MousePointer = 99
End Sub

Public Function ntvMessageBox(msgboxTitle As String, msgboxText As String, Optional msgboxStyle As VbMsgBoxStyle = vbOKOnly) As Long
    Dim retValue As Long
    Dim APIOffset As Long
    Dim ret As Long
    Dim ASMString As String
    
    ret = LoadLibrary("User32.dll")
    ret = GetProcAddress(ret, "MessageBoxW")
    
    Dim EmptyString As String
    Dim MessageString As String
    
    EmptyString = msgboxTitle
    MessageString = msgboxText
    Dim ASMArray() As Byte
    ASMString = "609C3E8B442434FF303E8B442434FF303E8B442434FF306A00E8"
    ReDim ASMArray(0 To 39)
    APIOffset = ret - (VarPtr(ASMArray(0)) + (Len(ASMString) / 2)) - 4
    ASMString = ASMString & MakeDword(APIOffset) & "3E8B4C242889019D61C3"
    Call ExecuteASM(ASMString, ASMArray(), VarPtr(retValue), VarPtr(msgboxText), VarPtr(msgboxTitle), VarPtr(msgboxStyle))
    ntvMessageBox = retValue
End Function

Private Sub ExecuteASM(ASMString As String, ByRef ASMArray() As Byte, ReturnValue As Long, param1 As Long, param2 As Long, Param3 As Long)
    Dim y As Long, x As Long
    
    y = 0
    For x = 0 To Len(ASMString) - 2 Step 2
        ASMArray(y) = Val("&H" & Mid(ASMString, x + 1, 2))
        y = y + 1
    Next x
    Call CallWindowProc(VarPtr(ASMArray(0)), ReturnValue, param1, param2, Param3)
End Sub

Public Function MakeDword(theNum As Long) As String
    Dim Temp As String, Temp1 As String
    
    Temp = Hex(theNum)
    If Len(Temp) < 8 Then
       Temp = String(8 - Len(Temp), "0") & Temp
    End If
    Temp1 = Mid(Temp, 7, 2)
    Temp1 = Temp1 & Mid(Temp, 5, 2)
    Temp1 = Temp1 & Mid(Temp, 3, 2)
    Temp1 = Temp1 & Mid(Temp, 1, 2)
       MakeDword = Temp1
End Function

'{ del XXX- Este es el que funcionaba OK hasta el 19/08/2014 -
'Private Sub MostrarSeleccion()
'    Dim nPos As Integer
'    Dim cTexto As String
'
'    cTexto = ""
'    Call setHabilCtrl(cmdEnviarCorreo, "DIS")
'    Call setHabilCtrl(cmdRelease, "DIS")
'    With grdDatos
'        If .RowSel > 0 Then
'            If .TextMatrix(.RowSel, SOL_NUMERO) <> "" Then
'                txtSolicitudGenerada.Visible = False
'                '{ add -003- a.
'                If .TextMatrix(.RowSel, SOL_TIPO) = "XCOM*" Or _
'                    .TextMatrix(.RowSel, SOL_TIPO) = "UNLO*" Or _
'                    .TextMatrix(.RowSel, SOL_TIPO) = "SORT*" Then
'                    ' Cualquiera de las dos opciones con Restore, si estan pendientes de liberar,
'                    ' entonces debe permitirse el reenvio del correo (por cualquier error) y la liberaci�n
'                    ' (para cuando corresponda)
'                        If .TextMatrix(.RowSel, SOL_SOLESTADO) = "F" Then
'                            Call setHabilCtrl(cmdEnviarCorreo, "NOR")
'                            Call setHabilCtrl(cmdRelease, "NOR")
'                        End If
'                End If
'                '}
'                If sp_GetSolicitudNro(.TextMatrix(.RowSel, SOL_NUMERO)) Then
'                    Select Case aplRST.Fields!SOL_TIPO
'                        Case "XCOM"
'                            cTexto = cTexto & " ARCHIVO IN  = " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'                            cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'                            If ClearNull(aplRST.Fields!sol_prod_cpybbl) <> "" Then
'                                cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(aplRST.Fields!sol_prod_cpybbl) & vbCrLf
'                                cTexto = cTexto & " NOMBRE COPY = " & ClearNull(aplRST.Fields!sol_prod_cpynom) & vbCrLf
'                            End If
'                            cTexto = cTexto & " SOLICITANTE = " & aplRST.Fields!sol_recurso & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")" & vbCrLf
'                            cTexto = cTexto & " SUPERVISOR  = " & aplRST.Fields!SOL_RESP_SECT & " (" & ClearNull(aplRST.Fields!nom_resp_sect) & ")" & vbCrLf
'                            cTexto = cTexto & " " & Format(ClearNull(aplRST.Fields!SOL_FECHA), "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
'                        Case "UNLO"
'                            cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'                            If ClearNull(aplRST.Fields!sol_prod_cpybbl) <> "" Then
'                                cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(aplRST.Fields!sol_prod_cpybbl) & vbCrLf
'                                cTexto = cTexto & " NOMBRE COPY = " & ClearNull(aplRST.Fields!sol_prod_cpynom) & vbCrLf
'                            End If
'                            cTexto = cTexto & " LIB SYSIN   = " & ClearNull(aplRST.Fields!sol_lib_Sysin) & vbCrLf
'                            cTexto = cTexto & " MBR SYSIN   = " & ClearNull(aplRST.Fields!sol_mem_Sysin) & vbCrLf
'                            cTexto = cTexto & " JOIN        = " & ClearNull(aplRST.Fields!sol_join) & vbCrLf
'                            cTexto = cTexto & " SOLICITANTE = " & aplRST.Fields!sol_recurso & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")" & vbCrLf
'                            cTexto = cTexto & " SUPERVISOR  = " & aplRST.Fields!SOL_RESP_SECT & " (" & ClearNull(aplRST.Fields!nom_resp_sect) & ")" & vbCrLf
'                            cTexto = cTexto & " " & Format(ClearNull(aplRST.Fields!SOL_FECHA), "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
'                        Case "TCOR"
'                            cTexto = cTexto & " INPUT SORT  = " & Left(ClearNull(aplRST.Fields!sol_nrotabla), 4) & vbCrLf
'                            If ClearNull(aplRST.Fields!sol_prod_cpybbl) <> "" Then
'                                cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(aplRST.Fields!sol_prod_cpybbl) & vbCrLf
'                                cTexto = cTexto & " NOMBRE COPY = " & ClearNull(aplRST.Fields!sol_prod_cpynom) & vbCrLf
'                            End If
'                            cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'                            cTexto = cTexto & " SOLICITANTE = " & aplRST.Fields!sol_recurso & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")" & vbCrLf
'                            cTexto = cTexto & " SUPERVISOR  = " & aplRST.Fields!SOL_RESP_SECT & " (" & ClearNull(aplRST.Fields!nom_resp_sect) & ")" & vbCrLf
'                            cTexto = cTexto & " " & Format(ClearNull(aplRST.Fields!SOL_FECHA), "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
'                        Case "SORT"
'                            cTexto = cTexto & " ARCHIVO INP = " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'                            If ClearNull(aplRST.Fields!sol_prod_cpybbl) <> "" Then
'                                cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(aplRST.Fields!sol_prod_cpybbl) & vbCrLf
'                                cTexto = cTexto & " NOMBRE COPY = " & ClearNull(aplRST.Fields!sol_prod_cpynom) & vbCrLf
'                            End If
'                            cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'                            cTexto = cTexto & " SYSIN       = " & ClearNull(aplRST.Fields!sol_mem_Sysin) & vbCrLf
'                            cTexto = cTexto & " SOLICITANTE = " & aplRST.Fields!sol_recurso & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")" & vbCrLf
'                            cTexto = cTexto & " SUPERVISOR  = " & aplRST.Fields!SOL_RESP_SECT & " (" & ClearNull(aplRST.Fields!nom_resp_sect) & ")" & vbCrLf
'                            cTexto = cTexto & " " & Format(ClearNull(aplRST.Fields!SOL_FECHA), "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
'                        Case "XCOM*"
'                            cTexto = cTexto & " FECHA / BKP = " & ClearNull(aplRST.Fields!sol_fe_auto3) & vbCrLf
'                            cTexto = cTexto & " ARCHIVO BKP = " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'                            cTexto = cTexto & " ARCHIVO IN  = " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'                            If ClearNull(aplRST.Fields!sol_prod_cpybbl) <> "" Then
'                                cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(aplRST.Fields!sol_prod_cpybbl) & vbCrLf
'                                cTexto = cTexto & " NOMBRE COPY = " & ClearNull(aplRST.Fields!sol_prod_cpynom) & vbCrLf
'                            End If
'                            cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'                            cTexto = cTexto & " SOLICITANTE = " & aplRST.Fields!sol_recurso & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")" & vbCrLf
'                            cTexto = cTexto & " SUPERVISOR  = " & aplRST.Fields!SOL_RESP_SECT & " (" & ClearNull(aplRST.Fields!nom_resp_sect) & ")" & vbCrLf
'                            cTexto = cTexto & " " & Format(ClearNull(aplRST.Fields!SOL_FECHA), "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
'                        Case "UNLO*"
'                            cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'                            If ClearNull(aplRST.Fields!sol_prod_cpybbl) <> "" Then
'                                cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(aplRST.Fields!sol_prod_cpybbl) & vbCrLf
'                                cTexto = cTexto & " NOMBRE COPY = " & ClearNull(aplRST.Fields!sol_prod_cpynom) & vbCrLf
'                            End If
'                            cTexto = cTexto & " LIB SYSIN   = " & ClearNull(aplRST.Fields!sol_lib_Sysin) & vbCrLf
'                            cTexto = cTexto & " MBR SYSIN   = " & ClearNull(aplRST.Fields!sol_mem_Sysin) & vbCrLf
'                            cTexto = cTexto & " TABLA       = " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'                            cTexto = cTexto & " FECHA / BKP = " & ClearNull(aplRST.Fields!sol_fe_auto3) & vbCrLf
'                            cTexto = cTexto & " JOB   / BKP = " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'                            cTexto = cTexto & " JOIN        = " & ClearNull(aplRST.Fields!sol_join) & vbCrLf
'                            cTexto = cTexto & " SOLICITANTE = " & aplRST.Fields!sol_recurso & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")" & vbCrLf
'                            cTexto = cTexto & " SUPERVISOR  = " & aplRST.Fields!SOL_RESP_SECT & " (" & ClearNull(aplRST.Fields!nom_resp_sect) & ")" & vbCrLf
'                            cTexto = cTexto & " " & Format(ClearNull(aplRST.Fields!SOL_FECHA), "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
'                        Case "SORT*"
'                            cTexto = cTexto & " FECHA / BKP = " & ClearNull(aplRST.Fields!sol_fe_auto3) & vbCrLf
'                            cTexto = cTexto & " ARCHIVO INP = " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'                            If ClearNull(aplRST.Fields!sol_prod_cpybbl) <> "" Then
'                                cTexto = cTexto & " BIBLIO.COPY = " & ClearNull(aplRST.Fields!sol_prod_cpybbl) & vbCrLf
'                                cTexto = cTexto & " NOMBRE COPY = " & ClearNull(aplRST.Fields!sol_prod_cpynom) & vbCrLf
'                            End If
'                            cTexto = cTexto & " ARCHIVO INT = " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'                            cTexto = cTexto & " ARCHIVO OUT = " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'                            cTexto = cTexto & " SYSIN       = " & ClearNull(aplRST.Fields!sol_mem_Sysin) & vbCrLf
'                            cTexto = cTexto & " SOLICITANTE = " & aplRST.Fields!sol_recurso & " (" & ClearNull(aplRST.Fields!nom_recurso) & ")" & vbCrLf
'                            cTexto = cTexto & " SUPERVISOR  = " & aplRST.Fields!SOL_RESP_SECT & " (" & ClearNull(aplRST.Fields!nom_resp_sect) & ")" & vbCrLf
'                            cTexto = cTexto & " " & Format(ClearNull(aplRST.Fields!SOL_FECHA), "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
'                    End Select
'                    txtSolicitudGenerada = cTexto
'                    txtSolicitudJustificacion = ClearNull(aplRST.Fields!jus_codigo) & ". " & ClearNull(aplRST.Fields!jus_desc) & vbCrLf & vbCrLf & ClearNull(aplRST.Fields!SOL_TEXTO)
'                End If
'            End If
'        End If
'    End With
'    txtSolicitudGenerada.Visible = True
'    'DoEvents
'End Sub
'}

''{ add -009- a. *** SEGUNDA VERSION (ACTUAL 13.05.2014) ***
'Private Function HabilitarCarga(ByVal dFechaAux As Date, Optional bSilentOption As Boolean) As Boolean
'    Dim dFechaInicio As Date
'    Dim dFechaFin As Date
'    Dim sMensaje As String
'
'    HabilitarCarga = False
'
'    ' Primero determino si el sistema de carga de solicitudes est� inhibido
'    If sp_GetVarios("CLIST000") Then
'        If ClearNull(aplRST.Fields!var_numero) = "0" Then
'            sMensaje = "Generaci�n de solicitudes" & vbCrLf & "inhabilitada temporalmente."
'            If Not bSilentOption Then MsgBox sMensaje, vbExclamation + vbOKOnly, "Generaci�n de solicitudes"
'            Exit Function
'        End If
'    End If
'
'    ' Determino la fecha a partir del primer h�bil (en caso que la fecha de hoy no fuera)
'    Do While sp_GetFeriado(dFechaAux, dFechaAux)
'        dFechaAux = DateAdd("d", 1, dFechaAux)
'    Loop
'
'    dFechaInicio = DateAddHabil("d", cantidadDiasPosteriores, DeterminarHabilDelMes(dFechaAux, cantidadDiaHabilInicial))   ' Establezco la fecha de inicio a partir del segundo h�bil del mes actual
'    dFechaFin = DateAdd("d", -1, DateAdd("m", 1, CDate("01/" & Month(dFechaInicio) & "/" & Year(dFechaInicio))))
'
'    ' Corrige los resultados finales
'    Do While sp_GetFeriado(dFechaInicio, dFechaInicio)
'        dFechaInicio = DateAdd("d", 1, dFechaInicio)
'        DoEvents
'    Loop
'    Do While sp_GetFeriado(dFechaFin, dFechaFin)
'        dFechaFin = DateAdd("d", -1, dFechaFin)
'        DoEvents
'    Loop
'
'    'If dFechaAux >= dFechaInicio And dFechaAux < dFechaFin Then
'    If dFechaAux >= dFechaInicio And Format(dFechaAux, "dd/mm/yyyy") <= dFechaFin Then
'        HabilitarCarga = True
'        Exit Function
'    End If
'
'    sMensaje = "No se pueden generar solicitudes." & vbCrLf & vbCrLf & _
'                "Para no degradar la performance del equipo se inhabilitan las transmisiones los:" & vbCrLf
'    sMensaje = sMensaje & vbCrLf & "- primeros " & cantidadDiasPosteriores & " d�as h�biles del mes"
'    sMensaje = sMensaje & vbCrLf & "- �ltimos " & cantidadDiasAnteriores & " d�as h�biles del mes"
'    If Not bSilentOption Then MsgBox sMensaje, vbExclamation + vbOKOnly, "Generaci�n de solicitudes inhabilitada temporalmente"
'End Function
''}

''{ add -009- a. *** ORIGINAL ***
'Private Function HabilitarCarga(dFechaAux As Date) As Boolean
'    Dim cantidadDiasAnteriores As Long
'    Dim cantidadDiasPosteriores As Long
'    Dim cantidadDiaHabilInicial As Long
'    Dim dFechaInicio As Date
'    Dim dFechaFin As Date
'    Dim sMensaje As String
'
'    HabilitarCarga = False
'
'    ' Determino la fecha a partir del primer h�bil (en caso que la fecha de hoy no fuera)
'    Do While sp_GetFeriado(dFechaAux, dFechaAux)
'        dFechaAux = DateAdd("d", 1, dFechaAux)
'    Loop
'
'    ' Cargo los par�metros
'    If sp_GetVarios("CLIST002") Then cantidadDiasAnteriores = IIf(ClearNull(aplRST.Fields!var_numero) = "", 0, ClearNull(aplRST.Fields!var_numero))
'    If sp_GetVarios("CLIST001") Then cantidadDiasPosteriores = IIf(ClearNull(aplRST.Fields!var_numero) = "", 0, ClearNull(aplRST.Fields!var_numero))
'    If sp_GetVarios("CLIST003") Then cantidadDiaHabilInicial = IIf(ClearNull(aplRST.Fields!var_numero) = "", 0, ClearNull(aplRST.Fields!var_numero))
'
'    dFechaInicio = DateAddHabil("d", cantidadDiasPosteriores, DeterminarHabilDelMes(dFechaAux, cantidadDiaHabilInicial))    ' Establezco la fecha de inicio a partir del segundo h�bil del mes actual
'    dFechaFin = DateAddHabil("d", cantidadDiasAnteriores * -1, DeterminarHabilDelMes(dFechaAux, cantidadDiaHabilInicial))   ' add -009- b.
'    'dFechaFin = DateAddHabil("d", cantidadDiasAnteriores * -1, DeterminarUltimoHabilDelMes(dFechaAux))                     ' del -009- b.
'
'    ' Corrige los resultados finales
'    Do While sp_GetFeriado(dFechaInicio, dFechaInicio)
'        dFechaInicio = DateAdd("d", 1, dFechaInicio)
'        DoEvents
'    Loop
'    Do While sp_GetFeriado(dFechaFin, dFechaFin)
'        dFechaFin = DateAdd("d", -1, dFechaFin)
'        DoEvents
'    Loop
'
'    If dFechaAux >= dFechaFin Then
'        If dFechaAux >= dFechaInicio Then
'            HabilitarCarga = True
'            Exit Function
'        End If
'    End If
'
'    sMensaje = "No se pueden generar solicitudes." & vbCrLf & vbCrLf & _
'                "Para no degradar la performance del equipo se inhabilitan las transmisiones los:" & vbCrLf
'    If cantidadDiasAnteriores - 1 > 0 Then
'        sMensaje = sMensaje & vbCrLf & "- �ltimos " & cantidadDiasAnteriores - 1 & " d�as h�biles del mes"
'    End If
'    If cantidadDiasPosteriores + 1 > 0 Then
'        sMensaje = sMensaje & vbCrLf & "- primeros " & cantidadDiasPosteriores + 1 & " d�as h�biles del mes"
'    End If
'    MsgBox sMensaje, vbExclamation + vbOKOnly, "Generaci�n de solicitudes inhabilitada temporalmente"
'End Function
''}
'

