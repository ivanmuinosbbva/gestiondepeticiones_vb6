VERSION 5.00
Begin VB.Form auxEsquemas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de acciones por perfil para peticiones"
   ClientHeight    =   7770
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13830
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   13830
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbEstadosPosibles 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8880
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   960
      Width           =   4095
   End
   Begin VB.ComboBox cmbEstadoActual 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8880
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   600
      Width           =   4095
   End
   Begin VB.Frame Frame2 
      Caption         =   " Datos de contexto "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Left            =   120
      TabIndex        =   3
      Top             =   2040
      Width           =   6735
      Begin VB.ComboBox cmbPerfil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1560
         Width           =   4095
      End
      Begin VB.ComboBox cmbArea 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   480
         Width           =   4095
      End
      Begin VB.ComboBox cmbDireccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   840
         Width           =   4095
      End
      Begin VB.ComboBox cmbGerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1200
         Width           =   4095
      End
      Begin VB.ComboBox cmbTipo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1920
         Width           =   4095
      End
      Begin VB.ComboBox cmbClase 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   2280
         Width           =   4095
      End
      Begin VB.Label lblEtiquetas 
         AutoSize        =   -1  'True
         Caption         =   "Perfil"
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   15
         Top             =   1560
         Width           =   390
      End
      Begin VB.Label lblEtiquetas 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         Height          =   180
         Index           =   1
         Left            =   240
         TabIndex        =   14
         Top             =   1920
         Width           =   330
      End
      Begin VB.Label lblEtiquetas 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         Height          =   180
         Index           =   2
         Left            =   240
         TabIndex        =   13
         Top             =   2280
         Width           =   435
      End
      Begin VB.Label lblEtiquetas 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         Height          =   180
         Index           =   5
         Left            =   240
         TabIndex        =   12
         Top             =   480
         Width           =   360
      End
      Begin VB.Label lblEtiquetas 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         Height          =   180
         Index           =   6
         Left            =   240
         TabIndex        =   11
         Top             =   840
         Width           =   720
      End
      Begin VB.Label lblEtiquetas 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         Height          =   180
         Index           =   7
         Left            =   240
         TabIndex        =   10
         Top             =   1200
         Width           =   675
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   " Acci�n a realizar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   6735
      Begin VB.ComboBox cmbAccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   4095
      End
      Begin VB.Label lblEtiquetas 
         AutoSize        =   -1  'True
         Caption         =   "Acci�n"
         Height          =   180
         Index           =   8
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   525
      End
   End
   Begin VB.Label lblEtiquetas 
      AutoSize        =   -1  'True
      Caption         =   "Estados subsiguientes"
      Height          =   465
      Index           =   4
      Left            =   7320
      TabIndex        =   19
      Top             =   900
      Width           =   1455
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblEtiquetas 
      AutoSize        =   -1  'True
      Caption         =   "Estados posibles"
      Height          =   180
      Index           =   3
      Left            =   7320
      TabIndex        =   18
      Top             =   600
      Width           =   1290
   End
End
Attribute VB_Name = "auxEsquemas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim gbArea As String
Dim gbDireccion As String
Dim gbGerencia As String
Dim gbPerfil As String
Dim gbTipo As String
Dim gbClase As String
Dim gbEstadoActual As String

Dim cAccion As String

Dim bInicializando As Boolean

Private Sub cmbArea_Click()
    gbArea = CodigoCombo(cmbArea, True)
    Call Actualizar_Direccion
End Sub

Private Sub cmbDireccion_Click()
    gbDireccion = CodigoCombo(cmbDireccion, False)
    Call Actualizar_Gerencia
End Sub

Private Sub Actualizar_Direccion()
    With cmbDireccion
        .Clear
        If sp_GetDireccion(Null, "S") Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!es_ejecutor) = gbArea Then
                    .AddItem ClearNull(aplRST.Fields!cod_direccion) & ": " & ClearNull(aplRST.Fields!nom_direccion)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            cmbDireccion.Enabled = IIf(.ListCount = 1, False, True)
            gbDireccion = CodigoCombo(cmbDireccion, False)
            Call Actualizar_Gerencia
        End If
    End With
End Sub

Private Sub Actualizar_Gerencia()
    With cmbGerencia
        .Clear
        If sp_GetGerencia(Null, gbDireccion, "S") Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!es_ejecutor) = gbArea Then
                    .AddItem ClearNull(aplRST.Fields!cod_gerencia) & ": " & ClearNull(aplRST.Fields!nom_gerencia)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            cmbGerencia.Enabled = IIf(.ListCount = 1, False, True)
            gbGerencia = CodigoCombo(cmbGerencia, False)
            'Call Actualizar_Gerencia
        End If
    End With
End Sub

Private Sub Form_Load()
    bInicializando = True
    Inicializar
    bInicializando = False
End Sub

Private Sub Inicializar()
    Inicializar_Combos
End Sub

Private Sub Inicializar_Combos()
    Dim i As Long
    
    'i = 1
    
    With cmbAccion
        .Clear
        .AddItem " 1. Alta de petici�n" & Space(100) & "||PNEW000"
        .AddItem " 2. Modificaci�n de datos de petici�n" & Space(100) & "||PMOD000"
        
        .AddItem " 3. Agregar sector" & Space(100) & "||PSNEW000"
        .AddItem " 4. Quitar sector" & Space(100) & "||PSDEL000"
        
        .AddItem " 5. Agregar grupo" & Space(100) & "||PGNEW000"
        .AddItem " 6. Quitar grupo" & Space(100) & "||PGDEL000"
        
        .AddItem " 7. Cambio de estado de petici�n" & Space(100) & "||CHGEST0"
        .AddItem " 8. Cambio de estado de sector" & Space(100) & "||CHGEST1"
        .AddItem " 9. Cambio de estado de grupo" & Space(100) & "||CHGEST2"
        
        .AddItem "10. Adjuntar documentaci�n metodol�gica" & Space(100) & "||ADJDOCSMET"
        .AddItem "11. Adjuntar documentaci�n t�cnica" & Space(100) & "||ADJDOCSTEC"
        
        .AddItem "12. Adjuntar conformes de alcance" & Space(100) & "||ADJALCA"
        .AddItem "13. Adjuntar conformes de usuario" & Space(100) & "||ADJTEST"
        .ListIndex = 0
    End With

    With cmbArea
        .Clear
        .AddItem "Solicitante" & Space(100) & "||N"
        .AddItem "Ejecutora" & Space(100) & "||S"
        .ListIndex = 1
        '.Enabled = False
    End With
    
    With cmbDireccion
        .Clear
        .AddItem "Direcci�n de Medios"
        .AddItem "Otras..."
        .ListIndex = 0
        '.Enabled = False
    End With
    
    With cmbGerencia
        .Clear
        .AddItem "Dise�o y Desarrollo" & Space(100) & "||DESA"
        .AddItem "Business Partner" & Space(100) & "||ORG."
        .AddItem "Otras" & Space(100) & "||NONE"
        .ListIndex = 0
        .Enabled = False
    End With
    
    With cmbPerfil
        .Clear
        .AddItem "SOLI: Solicitante"
        .AddItem "ANAL: Analista Ejecutor"
        .AddItem "CGRU: Resp.Ejecuci�n"
        .AddItem "CSEC: Resp.Sector"
        '.AddItem "ASEG: Admin. Seg.Inf."
        '.AddItem "ADMI: Administrador"
        '.AddItem "AUTO: Autorizante"
        '.AddItem "BPAR: Business Partner"
        '.AddItem "CHRS: Carga de Horas"
        '.AddItem "CTLG: Catalogador"
        '.AddItem "REFE: Referente"
        '.AddItem "CDIR: Resp.Direcci�n"
        '.AddItem "CGCI: Resp.Gerencia"
        '.AddItem "SADM: Superdministador"
        '.AddItem "SUPE: Supervisor"
        .ListIndex = 0
    End With
    
    With cmbTipo
        .Clear
        If sp_GetTipoPet(Null) Then
            Do While Not aplRST.EOF
                '.AddItem ClearNull(aplRST.Fields!nom_tipopet) & Space(100) & "||" & ClearNull(aplRST.Fields!cod_tipopet)
                .AddItem ClearNull(aplRST.Fields!cod_tipopet) & ": " & ClearNull(aplRST.Fields!nom_tipopet)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = 0
'        .AddItem "Normal" & Space(100) & "||NOR"
'        .AddItem "Propia" & Space(100) & "||PRO"
'        .AddItem "Especial" & Space(100) & "||ESP"
'        .AddItem "Proyecto" & Space(100) & "||PRJ"
'        .AddItem "Auditor�a interna" & Space(100) & "||AUI"
'        .AddItem "Auditor�a externa" & Space(100) & "||AUX"
'        .ListIndex = 0
    End With
    
    With cmbClase
        .Clear
        If sp_GetClase(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!cod_clase) & ": " & ClearNull(aplRST.Fields!nom_clase)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = 0
'        .AddItem "CORR: Mant. Correctivo"
'        .AddItem "ATEN: Atenci�n a usuario"
'        .AddItem "OPTI: Optimizaci�n"
'        .AddItem "EVOL: Mant. evolutivo"
'        .AddItem "NUEV: Nuevo desarrollo"
'        .AddItem "SPUF: SPUFI"
'        .AddItem "SINC: Sin clase"
'        .ListIndex = 0
    End With
    
    With cmbEstadoActual
        .Clear
        If sp_GetEstado(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!cod_estado) & ": " & ClearNull(aplRST.Fields!nom_estado)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
    
    With cmbEstadosPosibles
        .Clear
        If sp_GetEstado(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!cod_estado) & ": " & ClearNull(aplRST.Fields!nom_estado)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
End Sub

' ******************************************************************************************************************
' Cambios en los combos
' ******************************************************************************************************************
Private Sub cmbPerfil_Click()
    cAccion = "PERFIL"
    gbPerfil = CodigoCombo(cmbPerfil, False)
    If Not bInicializando Then Actualizar
End Sub

Private Sub cmbTipo_Click()
    cAccion = "TIPO"
    gbTipo = CodigoCombo(cmbTipo, False)
    'If Not bInicializando Then Actualizar
    
    Call Actualizar_Clase
End Sub

Private Sub Actualizar_Clase()
    With cmbClase
        .Clear
        If sp_GetTipoPetClase(gbTipo, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!cod_clase) & ": " & ClearNull(aplRST.Fields!nom_clase)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
End Sub

Private Sub cmbClase_Click()
    cAccion = "CLASE"
    gbClase = CodigoCombo(cmbClase, False)
    If Not bInicializando Then Actualizar
End Sub

Private Sub cmbEstadoActual_Click()
    cAccion = "ESTADO"
    gbEstadoActual = CodigoCombo(cmbEstadoActual, False)
    If Not bInicializando Then Actualizar
End Sub

Private Sub Actualizar()

    bInicializando = True
    
    Select Case cAccion
        Case "PERFIL"
            Select Case gbPerfil
                Case "ANAL"     ' Analista ejecutor
                    With cmbTipo
                        .Clear
                        .AddItem "Propia" & Space(100) & "||PRO"
                        .ListIndex = 0
                        If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                    End With
                    With cmbClase
                        .Clear
                        .AddItem "SINC: Sin clase"
                        .ListIndex = 0
                        If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                    End With
                    
                Case "CGRU"     ' Responsable de ejecuci�n"
                    With cmbTipo
                        .Clear
                        .AddItem "Propia" & Space(100) & "||PRO"
                        .AddItem "Especial" & Space(100) & "||ESP"
                        .ListIndex = 0
                        If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                    End With
                    
                    Select Case gbTipo
                        Case "PRO"
                            With cmbClase
                                .Clear
                                .AddItem "CORR: Mant. Correctivo"
                                .AddItem "OPTI: Optimizaci�n"
                                .AddItem "SPUF: SPUFI"
                                .ListIndex = 0
                                If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                            End With
                        Case "ESP"
                            With cmbClase
                                .Clear
                                .AddItem "ATEN: Atenci�n a usuario"
                                .ListIndex = 0
                                If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                            End With
                    End Select
            End Select
        Case "TIPO"
            Select Case gbPerfil
                Case "ANAL"     ' Analista ejecutor
                    With cmbTipo
                        .Clear
                        .AddItem "Propia" & Space(100) & "||PRO"
                        .ListIndex = 0
                        If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                    End With
                    With cmbClase
                        .Clear
                        .AddItem "SINC: Sin clase"
                        .ListIndex = 0
                        If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                    End With
                    
                Case "CGRU"     ' Responsable de ejecuci�n"
                    Select Case gbTipo
                        Case "PRO"
                            With cmbClase
                                .Clear
                                .AddItem "CORR: Mant. Correctivo"
                                .AddItem "OPTI: Optimizaci�n"
                                .AddItem "SPUF: SPUFI"
                                .ListIndex = 0
                                If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                            End With
                        Case "ESP"
                            With cmbClase
                                .Clear
                                .AddItem "ATEN: Atenci�n a usuario"
                                .ListIndex = 0
                                If .ListCount < 2 Then .Enabled = False Else .Enabled = True
                            End With
                    End Select
            End Select
        Case "CLASE"
        
        Case "ESTADO"
        
    End Select
    bInicializando = False
End Sub
