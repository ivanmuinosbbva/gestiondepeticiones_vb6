VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm mdiPrincipal 
   AutoShowChildren=   0   'False
   BackColor       =   &H8000000C&
   Caption         =   "Administraci�n de Peticiones"
   ClientHeight    =   6765
   ClientLeft      =   1785
   ClientTop       =   1815
   ClientWidth     =   10875
   Icon            =   "mdiPrincipalCfg.frx":0000
   LinkTopic       =   "MDIForm1"
   Picture         =   "mdiPrincipalCfg.frx":058A
   ScrollBars      =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin GesPetCfg.AdoConnection AdoConnection 
      Left            =   120
      Top             =   120
      _ExtentX        =   873
      _ExtentY        =   873
      Version         =   "1.0.0"
   End
   Begin VB.Timer Timer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   840
      Top             =   720
   End
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   840
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar sbPrincipal 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   6510
      Width           =   10875
      _ExtentX        =   19182
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Bevel           =   0
            Object.Width           =   4728
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   4410
            MinWidth        =   4410
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   2293
            MinWidth        =   2293
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuConexion 
      Caption         =   "Conexi�n"
      Begin VB.Menu mnuLogin 
         Caption         =   "Login"
         Enabled         =   0   'False
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuLogout 
         Caption         =   "Logout"
         Shortcut        =   ^O
      End
      Begin VB.Menu separador021 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCambioContrase�a 
         Caption         =   "Cambio de contrase�a"
      End
      Begin VB.Menu mnuConexion_Preferencias 
         Caption         =   "Preferencias..."
      End
      Begin VB.Menu separador000 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSalir 
         Caption         =   "Salir"
      End
   End
   Begin VB.Menu mnuTablas 
      Caption         =   "Tablas"
      Enabled         =   0   'False
      Begin VB.Menu mnuRecurso 
         Caption         =   "Recursos"
      End
      Begin VB.Menu mnuGrupo 
         Caption         =   "Grupos"
      End
      Begin VB.Menu mnuSector 
         Caption         =   "Sectores"
      End
      Begin VB.Menu mnuGerencia 
         Caption         =   "Gerencias"
      End
      Begin VB.Menu mnuDireccion 
         Caption         =   "Direcciones"
      End
      Begin VB.Menu mnuPeriodos 
         Caption         =   "Periodos de planificaci�n"
      End
      Begin VB.Menu mnuVarios 
         Caption         =   "Varios"
      End
      Begin VB.Menu mnuTablasDocumentos 
         Caption         =   "Documentos"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuTablasFabricas 
         Caption         =   "F�bricas"
      End
      Begin VB.Menu mnuTablasPerfiles 
         Caption         =   "Perfiles"
      End
      Begin VB.Menu mnuAcciones 
         Caption         =   "Acciones"
      End
      Begin VB.Menu mnuAccPerf 
         Caption         =   "Acciones por perfil"
      End
      Begin VB.Menu mnuTablasTips 
         Caption         =   "Administracion de Tips"
      End
      Begin VB.Menu mnuTablasConfiguracion 
         Caption         =   "Configuraci�n"
      End
      Begin VB.Menu mnuTablasPeticion 
         Caption         =   "Peticiones"
         Begin VB.Menu mnuTablasPeticion_Tipos 
            Caption         =   "Tipos"
         End
         Begin VB.Menu mnuTablasPeticion_Clases 
            Caption         =   "Clases"
         End
         Begin VB.Menu mnuTablasPeticion_TipoClase 
            Caption         =   "Relaci�n Tipo - Clase"
         End
      End
      Begin VB.Menu mnuTablas_Sep001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTarea 
         Caption         =   "Tareas"
      End
      Begin VB.Menu mnuTareaSector 
         Caption         =   "Tareas por Sector"
      End
      Begin VB.Menu mnuFeriado 
         Caption         =   "Feriados"
      End
      Begin VB.Menu mnuMotivos 
         Caption         =   "Motivos de suspensi�n"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuTablas_ProyectosIDM 
         Caption         =   "Proyectos IDM"
         Begin VB.Menu mnuTablas_ProyectosIDM_ABM 
            Caption         =   "Administrar"
         End
         Begin VB.Menu separador020 
            Caption         =   "-"
         End
         Begin VB.Menu mnuTablas_ProyectosIDM_Categorias 
            Caption         =   "Categor�as"
         End
         Begin VB.Menu mnuTablas_ProyectosIDM_Clasificacion 
            Caption         =   "Clasificaci�n"
         End
         Begin VB.Menu mnuTablas_ProyectosIDM_Hitos 
            Caption         =   "Hitos"
            Enabled         =   0   'False
         End
      End
      Begin VB.Menu mnuTablas_Aplicativos 
         Caption         =   "Aplicativos"
         Begin VB.Menu mnuTablas_Aplicativos_ABM 
            Caption         =   "Administrar cat�logo de aplicativos"
         End
      End
      Begin VB.Menu sep_003 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuTablas_Reglas 
         Caption         =   "Definir reglas de negocio"
         Visible         =   0   'False
         Begin VB.Menu mnuTablas_Reglas_Tipos 
            Caption         =   "Tipos de peticiones"
         End
         Begin VB.Menu mnuTablas_Reglas_Clases 
            Caption         =   "Clases de peticiones"
         End
         Begin VB.Menu mnuTablas_Reglas_Adjuntos 
            Caption         =   "Documentos adjuntos"
            Enabled         =   0   'False
         End
         Begin VB.Menu mnuTablas_Reglas_Conformes 
            Caption         =   "Conformes a documentos"
            Enabled         =   0   'False
         End
         Begin VB.Menu sep_004 
            Caption         =   "-"
         End
         Begin VB.Menu mnuTablas_Reglas_Definir 
            Caption         =   "Definir vigencias..."
         End
         Begin VB.Menu mnuTablas_Reglas_Tratamiento 
            Caption         =   "Tratamiento de peticiones"
         End
         Begin VB.Menu mnuTablas_Reglas_TratamientoAdjuntos 
            Caption         =   "Tratamiento de documentos adjuntos..."
         End
      End
   End
   Begin VB.Menu mnuBPE 
      Caption         =   "BPE"
      Enabled         =   0   'False
      Begin VB.Menu mnuBPE_Indicadores 
         Caption         =   "Indicadores"
      End
      Begin VB.Menu mnuBPE_Drivers 
         Caption         =   "Drivers"
      End
      Begin VB.Menu mnuBPE_KPIs 
         Caption         =   "KPIs"
      End
      Begin VB.Menu mnuBPE_UnidadMedida 
         Caption         =   "Unidades de medida"
      End
   End
   Begin VB.Menu mnuPrcEsp 
      Caption         =   "Procesos especiales"
      Enabled         =   0   'False
      Begin VB.Menu mnuSagrup 
         Caption         =   "Agrupamientos"
      End
      Begin VB.Menu mnuPrcEsp_Sep001 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPrcEsp_Escalar 
         Caption         =   "Escalamientos"
         Enabled         =   0   'False
         Begin VB.Menu mnuMsgEsc 
            Caption         =   "Escalamientos"
         End
         Begin VB.Menu mnuPrcEsp_Sep002 
            Caption         =   "-"
         End
         Begin VB.Menu mnuMsgTxt 
            Caption         =   "Texto de mensajes"
         End
         Begin VB.Menu mnuMsgEvt 
            Caption         =   "Mensajes por evento"
         End
      End
      Begin VB.Menu sep005 
         Caption         =   "-"
      End
      Begin VB.Menu mnuImport 
         Caption         =   "Importaci�n de peticiones"
      End
      Begin VB.Menu mnuImportView 
         Caption         =   "Ver importaci�n realizada"
      End
      Begin VB.Menu sep006 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExpPetic 
         Caption         =   "Exportacion de peticiones"
      End
      Begin VB.Menu mnuBatchPeticProc 
         Caption         =   "Proceso masivo"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuCleanEscal 
         Caption         =   "LIMPIAR ESCALAMIENTOS"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuInformes 
      Caption         =   "Informes"
      Enabled         =   0   'False
      Begin VB.Menu mnuInformes_AccionesPorPerfil 
         Caption         =   "1. Acciones por perfil"
      End
   End
   Begin VB.Menu mnuDevelopment 
      Caption         =   "Desarrollo"
      Enabled         =   0   'False
      Begin VB.Menu mnuDevelopment_Changeman 
         Caption         =   "Trazabilidad..."
      End
      Begin VB.Menu mnuDevelopment_Esquemas 
         Caption         =   "Esquemas..."
      End
      Begin VB.Menu mnuDevelopment_VencimientosConf 
         Caption         =   "Vencimiento de conformes a peticiones..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuDevelopment_Horas 
         Caption         =   "Desarrollo de tareas de mantenimiento..."
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuDevelopment_ImplantaShell 
         Caption         =   "Generaci�n de pasajes a Producci�n"
         Enabled         =   0   'False
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "?"
      Begin VB.Menu mnuAyuda_Documents 
         Caption         =   "Ayuda en l�nea"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuAyuda_Sugerencias 
         Caption         =   "Sugerencias"
      End
      Begin VB.Menu mnuAyuda_Novedades 
         Caption         =   "Novedades de la versi�n"
      End
      Begin VB.Menu sep_002 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAyudaVersion 
         Caption         =   "Versi�n"
      End
   End
End
Attribute VB_Name = "mdiPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 02.07.2007 - Se agrega una nueva opci�n al men� para generar la consulta de peticiones finalizadas
' -001- b. FJS 16.07.2007 - Se limpia del control StatusBar los datos del usuario que estaba logueado.
' -001- c. FJS 23.07.2007 - Se agrega al menu principal, para las funciones LogIn y LogOut los accesos r�pidos de teclado (Shortcuts)
' -001- d. FJS 24.07.2007 - Se agrega el nombre de pila y apellido del perfil actual en el control StatusBar.
' -001- e. FJS 25.07.2007 - Se agrega una pantalla que despliega las caracter�sticas t�cnicas de ADO para la versi�n actual (es informaci�n para el programador)
' -001- f. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 30.07.2007 - Se agrega el llamado a un archivo de ayuda.
' -002- b. FJS 08.08.2007 - Se modifica el alto (propiedad Height) del formulario MDI incrementandolo en 100 twips para que se muestren completamente los formularios Children del mismo.
' -002- c. FJS 09.08.2007 - Se agrega la versi�n de la aplicaci�n en el encabezado del formulario principal.
' -003- a. FJS 20.12.2007 - Trazabilidad
' -004- a. FJS 24.04.2008 - Homologaci�n:
' -005- a. FJS 08.08.2008 - Se habilitan menues especiales para Catalogadores (para la gente de Calidad)
' -006- a. FJS 20.11.2008 - Se habilita la opci�n de men� para establecer las preferencias del usuario.
' -007- a. FJS 21.11.2008 - Se agrega un nuevo men� para ejecutar el aplicativo web ADAH (Homologaci�n).
' -007- b. FJS 30.03.2009 - Se agrega un nuevo par�metro luego del usuario real, para indicar el legajo del usuario reemplazo.
' -008- a. FJS 24.11.2008 - Se agrega la opci�n de visualizar los TIPS del sistema/versi�n.
' -009- a. FJS 26.11.2008 - Se agrega a la rutina de cambio de contrase�a, el logout autom�tico en caso de cambio realizado.
' -010- a. FJS 10.12.2008 - Se agrega un nuevo reporte para peticiones con fecha prevista de pasaje a Producci�n pero agrupado por Sector.
' -011- a. FJS 23.12.2008 - Se agrega el llamado a la rutina que hace el logout en el registro de ingresos de usuarios.
' -011- b. FJS 08.01.2009 - Se quita el llamado a la rutina que hace el logout (no funciona como era esperado. Hay que perfeccionarla.)
' -012- a. FJS 13.01.2009 - Se agrega (a�n deshabilitado) la rutina para autolockear la workstation transcurrido N tiempo (idle time) (observaci�n de auditor�a).
' -013- a. FJS 15.01.2009 - Se modifica la rutina para considerar el cambio de contrase�a obligatorio.
' -014- a. FJS 10.03.2009 - Se agrega cosm�tica para mostrar que el equipo esta trabajando al iniciar el logout.
' -015- a. FJS 18.03.2009 - Manejo de peticiones hist�ricas.
' -016- a. FJS 15.04.2009 - Se corrige para mostrar el t�tulo de acuerdo a la cantidad de d�as parametrizados.
' -017- a. FJS 16.04.2009 - Se agrega un nuevo perfil para Analista ejecutor
' -018- a. FJS 07.05.2009 - Se agrega nuevo ABM de Proyectos IDM para los usuarios Administradores.
' -019- a. FJS 12.05.2009 - Se agrega nuevo ABM de aplicativos.
' -020- a. FJS 05.06.2009 - Se agrega la especificaci�n de un rango de fechas para la solicitud del reporte.
' -021- a. FJS 22.06.2009 - Nuevo informe de Peticiones asociadas a Proyectos IDM (filtro por proyecto).
' -022- a. FJS 25.06.2009 - Se agrega una nueva opci�n de men�, bajo el esquema de perfiles, para cursar solicitudes a Carga de M�quinas.
' -023- a. FJS 02.10.2009 - Cat�logos de enmascaramiento.
' -024- a. FJS 18.01.2010 - Nuevo informe de solicitudes solicitadas a Carga de M�quina.
' -025- a. FJS 28.01.2010 - Se elimina del men� principal el acceso al link del aplicativo ADAH (se descontinu� el mismo).
' -026- a. FJS 22.07.2010 - Planificaci�n: Se agrega una opci�n de perfil para Oficina de Proyecto.
' -027- a. FJS 20.08.2010 - IGM: Se agrega una opci�n para definir y configurar eventos para el administrador de IGM.
' -028- a. FJS 31.08.2011 - Planificaci�n: se agrega el ABM para administrar los per�odos.
' -029- a. FJS 05.09.2011 - Nuevo: se agrega el ABM para administrar los eventos/acciones.
' -030- GMT01  28.10.2019 - Nueva pantalla para cargar TIPS de ayuda.

Option Explicit

'{ add -012- a.
'Public Declare Function GetTickCount Lib "kernel32" () As Long
'Private Declare Function GetLastInputInfo Lib "user32" (plii As Any) As Long
Private Declare Function LockWorkStation Lib "user32.dll" () As Long

'Private Type LASTINPUTINFO
'    cbSize As Long
'    dwTime As Long
'End Type
'}

' Declaraci�n de las API's de Windows para el manejo y control de la apariencia de Windows XP
Private Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As Long
Private Declare Function FreeLibrary Lib "kernel32" (ByVal hLibModule As Long) As Long
Private Declare Function SetErrorMode Lib "kernel32" (ByVal wMode As Long) As Long
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Private Const SEM_FAILCRITICALERRORS = &H1
Private Const SEM_NOGPFAULTERRORBOX = &H2
Private Const SEM_NOOPENFILEERRORBOX = &H8000&

Private m_hMod As Long

Private Sub MDIForm_Load()
    'Call InitCommonControlsVB
    m_hMod = LoadLibrary("shell32.dll")
    Me.Height = 9100        ' upd -002- b.
    Me.Width = 12000
    Me.Caption = Trim(glHEADAPLICACION)
    Me.BackColor = RGB(58, 110, 165)
    'Timer.Enabled = True    ' add -012- a.
End Sub

Private Sub mnuTablasConfiguracion_Click()
    Call CerrarForms
    frmConfiguracion.Show
End Sub

Private Sub mnuVarios_Click()
    Call CerrarForms
    frmVarios.Show
End Sub

Private Sub mnuTablas_ProyectosIDM_Categorias_Click()
    frmProyectosCategoriaShell.Show
End Sub

Private Sub mnuTablas_ProyectosIDM_Clasificacion_Click()
    frmProyectosClaseShell.Show
End Sub

Sub Login()
    frmLogin.Show 1
    Call Status("Aguarde un instante...")
    Unload frmLogin
    Call Status("Listo.")
End Sub

Sub Logout()
    On Error Resume Next
    Call CerrarForms
    Call Status("Cerrando conexi�n...")
    mdiPrincipal.sbPrincipal.Panels(2) = ""     ' Usuario. Ej.: XA00309
    mdiPrincipal.sbPrincipal.Panels(3) = ""     ' Usuario (?)
    mdiPrincipal.sbPrincipal.Panels(4) = ""     ' ???
    mdiPrincipal.sbPrincipal.Panels(5) = ""     ' Nombre del usuario
    mdiPrincipal.sbPrincipal.Panels(6) = ""     ' add -001- d. Servidor de conexi�n
    mdiPrincipal.AdoConnection.CerrarConexion   ' add -new-!!!
    glONLINE = False
    Call HabilitarMenues(False)
    Call Status("Listo.")
End Sub

Private Sub AdoConnection_ADOError(Codigo As Long, MensajeError As String, Modulo As String)
    If Codigo = 0 Then
        glAdoError = ""
    Else
        glAdoError = Modulo & vbCrLf & MensajeError
    End If
End Sub

Private Sub MDIForm_Resize()
    If Me.WindowState = vbMaximized Then
        Me.WindowState = vbNormal
    End If
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    Call Logout
End Sub

Private Sub mnuAccPerf_Click()
    Call CerrarForms
    frmAccionesPerfil.Show
End Sub

Private Sub mnuAyuda_Sugerencias_Click()
    Load frmTip
    frmTip.Show 1
End Sub

Private Sub mnuImport_Click()
    Call CerrarForms
    'GMT01 - INI - SE COMENTA ESTO PARA PROBAR EXACTAMENTE COMO ESTA EN PRODUCCION
    'Call Importar
    'If InStr(1, "DESA|TEST|", glENTORNO, vbTextCompare) > 0 Then
    '    Call ImportarNuevo
    'Else
        Call Importar
    'End If
    'GMT01 - FIN
End Sub

Private Sub mnuImportView_Click()
    Call CerrarForms
    frmPeticionesShellImpor.Show
End Sub

Private Sub mnuMsgEsc_Click()
    Call CerrarForms
    frmEscalamiento.Show
End Sub

Private Sub mnuMsgEvt_Click()
    Call CerrarForms
    frmMensajesEvento.Show
End Sub

Private Sub mnuMsgTxt_Click()
    Call CerrarForms
    frmMensajesTexto.Show
End Sub

Private Sub mnuAyudaVersion_Click()
    Dim cOSVersion As String
    
    cOSVersion = getOSVersion()
    frmAcercaCfg.Show 1
End Sub

Private Sub mnuLogin_Click()
    Call Login
End Sub

Private Sub mnuLogout_Click()
    On Error Resume Next
    
    Call Puntero(True)
    Call CerrarForms
    Call Status("Cerrando conexi�n...")
    With sbPrincipal
        .Panels(2).text = ""        ' Usuario
        .Panels(3).text = ""        ' Usuario (Reemplazo)
        .Panels(4).text = ""        ' Perfil
        .Panels(5).text = ""        ' Nombre del usuario
        .Panels(6).text = ""        ' Servidor
    End With
    Call HabilitarMenues(False)
    Call Status("Listo.")
    Call Puntero(False)
End Sub

Private Sub mnuCambioContrase�a_Click()
    Call CambioPassword
End Sub

Private Sub mnuPerfilCostBenef_Click()
    Call CerrarForms
    frmPerfilCostBenef.Show
End Sub

Private Sub mnuSalir_Click()
    End
End Sub

Private Sub mnuRecurso_Click()
   Call CerrarForms
   frmRecurso.Show
End Sub

Private Sub mnuGrupo_Click()
    Call CerrarForms
    frmGrupo.Show
End Sub

Private Sub mnuSector_Click()
    Call CerrarForms
    frmSector.Show
End Sub

Private Sub mnuGerencia_Click()
    Call CerrarForms
    frmGerencia.Show
End Sub

Private Sub mnuDireccion_Click()
    Call CerrarForms
    frmDireccion.Show
End Sub

Private Sub mnuTipo_Click()
    Call CerrarForms
    frmTipo.Show
End Sub

Private Sub mnuTarea_Click()
    Call CerrarForms
    frmTarea.Show
End Sub

Private Sub mnuTareaSector_Click()
    Call CerrarForms
    frmTareaSector.Show
End Sub

Private Sub mnuFeriado_Click()
    Call CerrarForms
    auxFeriado.Show
End Sub

Private Sub mnuDevelopment_Changeman_Click()
    Load frmPeticionChangeman
    frmPeticionChangeman.Show
End Sub

'{ add -006- a.
Private Sub mnuConexion_Preferencias_Click()
    Load frmRecursoPreferencias
    frmRecursoPreferencias.Show vbModal, Me
End Sub
'}

Sub CambioPassword()
    auxCambioPass.Show vbModal
    '{ add -013- a.
    If auxCambioPass.bolCambioObligatorio = True Then
        mnuLogout_Click
    Else
    '}
        '{ add -009- a.
        If auxCambioPass.bolPasswordChanged Then
            mnuLogout_Click
        End If
        '}
    End If  ' add -013- a.
End Sub

'{ add -013- b.
Public Sub Invisible_Logout()
    Call CerrarForms
    Call Status("Cerrando conexi�n...")
    mdiPrincipal.AdoConnection.CerrarConexion
    If Not mdiPrincipal.AdoConnection.IsOnLine Then
        With sbPrincipal
            .Panels(2).text = ""        ' Usuario
            .Panels(3).text = ""        ' Usuario (Reemplazo)
            .Panels(4).text = ""        ' Perfil
            .Panels(5).text = ""        ' Nombre del usuario
            .Panels(6).text = ""        ' Servidor
        End With
        Call HabilitarMenues(False)
        Call Status("Listo.")
    End If
End Sub
'}

'{ add -018- a.
Private Sub mnuTablas_ProyectosIDM_ABM_Click()
    frmProyectosIDMShell.Show
End Sub
'}

'{ add -019- a.
Private Sub mnuTablas_Aplicativos_ABM_Click()
    Load frmAplicativosShell
    frmAplicativosShell.Show
End Sub
'}

Private Sub mnuDevelopment_Esquemas_Click()
    Load auxEsquemas
    auxEsquemas.Show
End Sub

'{ add -028- a.
Private Sub mnuPeriodos_Click()
    Load frmPeriodos
    frmPeriodos.Show
End Sub
'}

Private Sub mnuAcciones_Click()
    Load frmAcciones
    frmAcciones.Show
End Sub

'{ add -012- a.
Private Sub Timer_Timer()
    Dim lii As LASTINPUTINFO
    
    lii.cbSize = Len(lii)
    Call GetLastInputInfo(lii)
    With sbPrincipal.Panels(1)
        .text = Format((GetTickCount() - lii.dwTime) / 1000, "00")
        sbPrincipal.Refresh
        If .text = "10" Then
            Inhabilitar
        End If
    End With
End Sub

Private Sub Inhabilitar()
    Dim ret As Long
    ret = Shell("rundll32.exe user32.dll LockWorkStation")
End Sub
'}

Private Sub mnuBatchPeticProc_Click()
    Dim file As String
    Dim datos As String
    Dim NroHistorial As Long
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim i As Long
    
    file = AbrirArchivo()
    If Len(file) > 0 Then
        Call Puntero(True)
        Call Status("Procesando...")
        datos = LeerArchivo(file)
        If Len(datos) > 0 Then
            Call Split(datos, vbCrLf)   ' Arma el vector vElementos con los n�meros de peticiones
            For i = 0 To UBound(vElemento)
                If IsNumeric(vElemento(i)) Then
                    Call Status("Procesando " & i + 1 & " de " & UBound(vElemento) + 1)
                    If sp_GetPeticionGrupo2(vElemento(i), Null, Null) Then
                        Do While Not aplRST1.EOF
                            If InStr(1, "106|116|24-03|", ClearNull(aplRST1.Fields!cod_grupo), vbTextCompare) > 0 Then
                                Call sp_UpdatePetSubField(vElemento(i), ClearNull(aplRST1.Fields!cod_grupo), "ESTADO", "CANCEL", Null, Null)
                                NuevoEstadoSector = getNuevoEstadoSector(vElemento(i), ClearNull(aplRST1.Fields!cod_sector))
                                Call sp_UpdatePetSecField(vElemento(i), ClearNull(aplRST1.Fields!cod_sector), "ESTADO", NuevoEstadoSector, Null, Null)
                                NuevoEstadoPeticion = getNuevoEstadoPeticion(vElemento(i))
                                Call sp_UpdatePetField(vElemento(i), "ESTADO", NuevoEstadoPeticion, Null, Null)
                                NroHistorial = sp_AddHistorial(vElemento(i), "GCHGEST", NuevoEstadoPeticion, ClearNull(aplRST1.Fields!cod_sector), NuevoEstadoSector, ClearNull(aplRST1.Fields!cod_grupo), "CANCEL", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Cancelado definitivamente por venta ART.")
                                Call sp_UpdatePetSubField(vElemento(i), ClearNull(aplRST1.Fields!cod_grupo), "HSTSOL", Null, Null, NroHistorial)
                                Call sp_DelMensajePerfil(vElemento(i), "CGRU", "GRUP", ClearNull(aplRST1.Fields!cod_grupo))     ' Borra todos los mensajes para el grupo actual
                            End If
                            aplRST1.MoveNext
                            DoEvents
                        Loop
                    End If
                End If
            Next i
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly, "Proceso masivo"
        End If
    End If
    Call Status("Listo.")
    Call Puntero(False)
End Sub

Private Function LeerArchivo(Ruta As String) As String
    On Error GoTo Error_Function:
    Dim fso As New FileSystemObject
    Dim ObjDatos As TextStream     ' Objeto Fso para el Contenido del archivo
    Dim datos As String            ' datos del fichero
      
    datos = ""
    Set ObjDatos = fso.OpenTextFile(Ruta, ForReading, False, TristateMixed)
    While Not ObjDatos.AtEndOfStream
        datos = datos & ObjDatos.ReadAll
    Wend
    ' retorna el contenido leido
    LeerArchivo = datos
    ' cierra el archivo abierto
    ObjDatos.Close
    Set fso = Nothing
Exit Function
Error_Function:
    MsgBox Err.DESCRIPTION
    On Local Error Resume Next
    Set fso = Nothing
End Function

Private Function sp_GetPeticionGrupo2(pet_nrointerno, cod_sector, cod_grupo) As Boolean
    On Error Resume Next
    aplRST1.Close
    Set aplRST1 = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionGrupo"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST1 = .Execute
    End With
    If Not (aplRST1.EOF) Then
        sp_GetPeticionGrupo2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST1, Err, aplCONN))
    sp_GetPeticionGrupo2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Private Sub mnuTablasFabricas_Click()
    Load frmFabrica
    frmFabrica.Show
End Sub

Private Sub mnuInformes_AccionesPorPerfil_Click()
    Load rptAccionesPorPerfil
    rptAccionesPorPerfil.Show
End Sub

Private Sub mnuBPE_Indicadores_Click()
    Call frmIndicadores.Show
End Sub

Private Sub mnuBPE_Drivers_Click()
    Call frmDrivers.Show
End Sub

'GMT01 - INI
Private Sub mnuTablasTips_Click()
    FrmCargaTips.Show
End Sub
'GMT01 - FIN
