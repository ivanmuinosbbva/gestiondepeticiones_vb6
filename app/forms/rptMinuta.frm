VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptMinuta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Generaci�n Minuta para Comit�"
   ClientHeight    =   3840
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9885
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3840
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   8400
      TabIndex        =   1
      Top             =   30
      Width           =   1395
      Begin VB.CommandButton cmdMinuta 
         Caption         =   "GENERAR"
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Genera un archivo Excel con los datos utilizados en la Minuta"
         Top             =   240
         Width           =   1275
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   3120
         Width           =   1275
      End
   End
   Begin VB.Frame frmGral 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3705
      Left            =   120
      TabIndex        =   0
      Top             =   30
      Width           =   8205
      Begin VB.CommandButton cmdGuardar 
         Height          =   315
         Left            =   7560
         Picture         =   "rptMinuta.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Seleccionar un archivo de destino para el proceso de exportaci�n"
         Top             =   2520
         Width           =   350
      End
      Begin VB.Frame frmEjec 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   215
         Left            =   1440
         TabIndex        =   6
         Top             =   660
         Width           =   2160
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Sector"
            Height          =   195
            Index           =   1
            Left            =   1080
            TabIndex        =   8
            Top             =   0
            Width           =   885
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Gerencia"
            Height          =   195
            Index           =   0
            Left            =   0
            TabIndex        =   7
            Top             =   0
            Width           =   975
         End
      End
      Begin VB.ComboBox cboGerenciaSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   960
         Width           =   6375
      End
      Begin VB.ComboBox cboSectorSec 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   960
         Width           =   6375
      End
      Begin AT_MaskText.MaskText txtDESDEfinreal 
         Height          =   315
         Left            =   1440
         TabIndex        =   9
         Top             =   1365
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAfinreal 
         Height          =   315
         Left            =   1440
         TabIndex        =   10
         Top             =   1725
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFile 
         Height          =   315
         Left            =   1440
         TabIndex        =   12
         Top             =   2520
         Width           =   6070
         _ExtentX        =   10716
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin AT_MaskText.MaskText txtHoras 
         Height          =   315
         Left            =   1440
         TabIndex        =   18
         Top             =   2115
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   556
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   3
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "M�nimo Horas"
         Height          =   180
         Left            =   300
         TabIndex        =   19
         Top             =   2182
         Width           =   1035
      End
      Begin VB.Label Lbl7 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         Height          =   180
         Left            =   900
         TabIndex        =   17
         Top             =   660
         Width           =   390
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         Height          =   180
         Left            =   930
         TabIndex        =   16
         Top             =   1035
         Width           =   360
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   180
         Left            =   870
         TabIndex        =   15
         Top             =   1792
         Width           =   450
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "Informado Desde"
         Height          =   180
         Left            =   60
         TabIndex        =   14
         Top             =   1432
         Width           =   1290
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Salida"
         Height          =   180
         Left            =   855
         TabIndex        =   13
         Top             =   2587
         Width           =   465
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "�rea ejecutora"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   1125
      End
   End
End
Attribute VB_Name = "rptMinuta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 06.07.2007 - Se agrega la columna con el c�digo de la clase de la petici�n a la generaci�n de la planilla Excel.
' -001- b. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 15.12.2015 - Mejora: se modifica para igualarlo al frmPetFin.

Option Explicit

Dim flgEnCarga As Boolean
Dim secNivel As String
Dim secArea As String
Dim secDire As String
Dim secGere As String
Dim secSect As String
Dim secGrup As String

Private auxAgrup As String
Private xSelectAgrup As String

Const XLleft = -4131
Const XLcenter = -4108
Const XLright = -4152

Const colNroInterno = 1
Const colNroAsignado = 2
Const colTipoPet = 3
Const colPetClass = 4
Const colTitulo = 5
Const colDetalle = 6
Const colSector = 7
Const colEjecuc = 8
Const colAgrup = 9
Const colFFin = 10
Const colFInf = 11
Const colHs = 12

Private Sub Form_Load()
    Call InicializarCombos
     xSelectAgrup = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "AGR_SISTEMA", "0", glArchivoINI)

    'inicializo desde el lunes hasta el domingo proximo pasado
    txtDESDEfinreal.Text = Format(date - Weekday(date, vbSunday) - 5, "dd/mm/yyyy")
    txtHASTAfinreal.Text = Format(date - Weekday(date, vbSunday) + 1, "dd/mm/yyyy")
    
    If sp_GetVarios("HSMINU") Then
        txtHoras.Text = CInt(Val(ClearNull(aplRST!var_numero)))
    Else
        txtHoras.Text = "0"
    End If
    OptNivelSec(0).Value = True
End Sub

Private Sub InicializarCombos()
    Dim i As Integer
    Dim auxCodPet, auxCodSec As String
    Dim xDireSec, xGereSec, xSectSec As String
    
    flgEnCarga = True
    
    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        xGereSec = ClearNull(aplRST!cod_gerencia)
        xSectSec = ClearNull(aplRST!cod_sector)
        xDireSec = ClearNull(aplRST!cod_direccion)
    End If
    
    If sp_GetGerenciaXt("", "") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboGerenciaSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|NULL|NULL"
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetSectorXt(Null, Null, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboSectorSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|NULL"
            End If
            aplRST.MoveNext
        Loop
    End If
    cboGerenciaSec.ListIndex = PosicionCombo(Me.cboGerenciaSec, xDireSec & xGereSec, True)
    cboSectorSec.ListIndex = PosicionCombo(Me.cboSectorSec, xDireSec & xGereSec & xSectSec, True)
    txtFile.Text = GetSetting("GesPet", "Export01", "FileMinuta", glWRKDIR & "Implementaciones_" & glBASE & ".xls")
    flgEnCarga = False
    Call Status("Listo.")
End Sub

Private Sub cmdMinuta_Click()
    If ClearNull(txtFile.Text) <> "" Then
        If MsgBox("�Confirma el inicio de proceso?", vbQuestion + vbYesNo) = vbYes Then
            Call Procesar
        End If
    Else
        txtFile.SetFocus
        MsgBox "No se defini� el archivo de salida.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub Procesar()
    If flgEnCarga = True Then Exit Sub
    
    Const MENSAJE_SINDATOS = "La consulta no arroja resultados."
    Dim cntRows As Integer, i As Integer, j As Integer
    Dim cntLast As Integer
    Dim cntField As Integer
    Dim sTexto As String
    Dim sTtaux As String
    Dim sPetic As String
    Dim sAuxPetic As String
    Dim flgGcia As Boolean
    Dim auxHoras As Long
    
    secNivel = verNivelSec()
    secArea = verAreaSec()
    cntField = 0
    cntRows = 0
    Dim ExcelApp, ExcelWrk, xlSheet As Object

    If Dir(txtFile.Text) <> "" Then
        If MsgBox("El archivo ya existe." & Chr(13) & "�Sobreescribe?", vbYesNo) = vbNo Then
            GoTo finx
        End If
        On Error Resume Next
        Kill txtFile
    End If
    If Dir(txtFile.Text) <> "" Then
        MsgBox ("No puede sobreescribir el archivo, verifique que no est� en uso")
        GoTo finx
    End If
    
    Call Puntero(True)
    If OptNivelSec(1).Value = True Then
        If Not sp_GetPeticionSectorMinuta(0, secGere, secSect, "TERMIN", txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue) Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
        If aplRST.EOF Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
    Else
        If Not sp_GetPeticionSectorMinuta(0, secGere, "NULL", "ANEXAD|ANULAD|TERMIN|CANCEL|RECHAZ|RECHTE", txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue) Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
        If aplRST.EOF Then
           Call Puntero(False)
           MsgBox MENSAJE_SINDATOS, vbExclamation + vbOKOnly
           GoTo finx
        End If
    End If
    
    Call Status("Generando la planilla...")
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.Visible = False
    On Error Resume Next
    Set ExcelWrk = ExcelApp.Workbooks.Add
    If ExcelWrk Is Nothing Then
        MsgBox "Error al abrir planilla", vbCritical + vbOKOnly
        GoTo finx
    End If
    
    ExcelWrk.SaveAs FileName:=txtFile.Text
    Set xlSheet = ExcelWrk.Sheets(1)
    Call Puntero(True)
    
    xlSheet.Cells.VerticalAlignment = -4160
    xlSheet.Cells.Font.name = "Tahoma"
    xlSheet.Cells.Font.Size = 7

    'encabezados
    With xlSheet
        .Columns(colNroInterno).ColumnWidth = 3
        .Cells(1, colNroAsignado) = "Nro": .Columns(colNroAsignado).HorizontalAlignment = XLright:    .Columns(colNroAsignado).ColumnWidth = 4
        .Cells(1, colTipoPet) = "Tipo": .Columns(colTipoPet).HorizontalAlignment = XLcenter: .Columns(colTipoPet).ColumnWidth = 3.5
        .Cells(1, colPetClass) = "Clase": .Columns(colPetClass).HorizontalAlignment = XLcenter: .Columns(colPetClass).ColumnWidth = 4.5     ' add -001- a.
        .Cells(1, colTitulo) = "T�tulo": .Columns(colTitulo).HorizontalAlignment = XLleft:  .Columns(colTitulo).ColumnWidth = 23
        .Cells(1, colDetalle) = "Descripci�n": xlSheet.Columns(colDetalle).HorizontalAlignment = XLleft: xlSheet.Columns(colDetalle).ColumnWidth = 40
        .Cells(1, colSector) = "Sector Solicitante": .Columns(colSector).ColumnWidth = 0
        .Cells(1, colEjecuc) = "Sector Ejecuci�n": .Columns(colEjecuc).ColumnWidth = 25
        If OptNivelSec(1).Value = True Then
            .Cells(1, colFFin) = "Fecha Fin Sector": .Columns(colFFin).HorizontalAlignment = XLcenter: .Columns(colFFin).ColumnWidth = 7.5: .Columns(colFFin).NumberFormat = "dd/mm/yy"
        Else
            .Cells(1, colFFin) = "Fecha Fin Gerencia": .Columns(colFFin).HorizontalAlignment = XLcenter: .Columns(colFFin).ColumnWidth = 7.5: .Columns(colFFin).NumberFormat = "dd/mm/yy"
        End If
        .Cells(1, colFInf) = "Fecha Informado": .Columns(colFInf).HorizontalAlignment = XLcenter: .Columns(colFInf).ColumnWidth = 7.5: .Columns(colFInf).NumberFormat = "dd/mm/yy"
        .Cells(1, colAgrup) = "Sistema": xlSheet.Columns(colAgrup).HorizontalAlignment = XLleft: xlSheet.Columns(colAgrup).HorizontalAlignment = XLleft: xlSheet.Columns(colAgrup).ColumnWidth = 25
   End With
    
    'xlSheet.Cells.RowHeight = 30
    xlSheet.Rows(1).RowHeight = 30
    xlSheet.Rows(1).Font.Bold = True
    xlSheet.Rows(1).HorizontalAlignment = XLleft
    ExcelApp.ActiveWindow.Zoom = 75
    
    'Call Puntero(True)
    
    i = 1: sAuxPetic = ""
       
    Do While Not aplRST.EOF
        If OptNivelSec(1).Value = True Then
            i = i + 1
            Call Status("" & i)
            xlSheet.Cells(i, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            xlSheet.Cells(i, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            xlSheet.Cells(i, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
            xlSheet.Cells(i, colPetClass) = ClearNull(aplRST!cod_clase)      ' add -001- a.
            xlSheet.Cells(i, colTitulo) = ClearNull(aplRST!Titulo)
            'xlSheet.Cells(I, colSector) = ClearNull(aplRST!sol_direccion) & " " & Chr(10) & ClearNull(aplRST!sol_gerencia) & " " & Chr(10) & ClearNull(aplRST!sol_sector)
            xlSheet.Cells(i, colEjecuc) = ClearNull(aplRST!sec_nom_sector)
            xlSheet.Cells(i, colFFin) = IIf(Not IsNull(aplRST!sec_fe_fin_real), Format(aplRST!sec_fe_fin_real, "yyyy-mm-dd"), "")
            xlSheet.Cells(i, colFInf) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
            xlSheet.Cells(i, colHs) = ClearNull(aplRST!horaspresup)
        Else
            If sAuxPetic <> ClearNull(aplRST!pet_nrointerno) Then
                i = i + 1
                Call Status("" & i)
                xlSheet.Cells(i, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
                xlSheet.Cells(i, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
                xlSheet.Cells(i, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
                xlSheet.Cells(i, colPetClass) = ClearNull(aplRST!cod_clase)      ' add -001- a.
                xlSheet.Cells(i, colTitulo) = ClearNull(aplRST!Titulo)
                'xlSheet.Cells(I, colSector) = ClearNull(aplRST!sol_direccion) & " " & Chr(10) & ClearNull(aplRST!sol_gerencia) & " " & Chr(10) & ClearNull(aplRST!sol_sector)
                xlSheet.Cells(i, colEjecuc) = ""
                xlSheet.Cells(i, colFFin) = ""
                xlSheet.Cells(i, colFInf) = ""
                If InStr("|TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then
                    xlSheet.Cells(i, colHs) = ClearNull(aplRST!horaspresup)
                End If
            Else
                If InStr("|TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then
                    xlSheet.Cells(i, colHs) = xlSheet.Cells(i, colHs) + ClearNull(aplRST!horaspresup)
                End If
            End If
            sAuxPetic = ClearNull(aplRST!pet_nrointerno)
        End If
        aplRST.MoveNext
        DoEvents
    Loop
    cntRows = i
    
    If OptNivelSec(0).Value = True Then
        For i = cntRows To 2 Step -1
            sAuxPetic = getNuevoEstadoGerencia(xlSheet.Cells(i, colNroInterno), secGere)
            If sAuxPetic <> "TERMIN" Then
                xlSheet.Rows(i).Delete
                cntRows = cntRows - 1
            End If
        Next
    End If
    
    For i = cntRows To 2 Step -1
        If CInt(Val(txtHoras)) > CInt(Val(ClearNull(xlSheet.Cells(i, colHs)))) Then
            xlSheet.Rows(i).Delete
            cntRows = cntRows - 1
        End If
    Next
    
    If OptNivelSec(0).Value = True Then
        For i = 2 To cntRows
            If sp_GetPeticionSectorXt(xlSheet.Cells(i, colNroInterno), Null) Then
                Do While Not aplRST.EOF
                    If ClearNull(aplRST!cod_gerencia) = secGere Then
                         If ClearNull(aplRST!cod_estado) = "TERMIN" Then
                            xlSheet.Cells(i, colEjecuc) = xlSheet.Cells(i, colEjecuc) & ClearNull(aplRST!nom_sector) & Chr(10)
                            sTtaux = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "yyyy-mm-dd"), "")
                            If sTtaux > Format(xlSheet.Cells(i, colFFin), "yyyy-mm-dd") Then
                                xlSheet.Cells(i, colFFin) = sTtaux
                            End If
                         End If
                         If InStr("ANEXAD|ANULAD|TERMIN|CANCEL|RECHAZ|RECHTE", ClearNull(aplRST!cod_estado)) > 0 Then
                            sTtaux = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
                            If sTtaux > Format(xlSheet.Cells(i, colFInf), "yyyy-mm-dd") Then
                                xlSheet.Cells(i, colFInf) = sTtaux
                            End If
                         End If
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next
    End If
    
    For i = 2 To cntRows
        'Call Status("Descripci�n:" & i)
        sPetic = xlSheet.Cells(i, colNroInterno)
        sTexto = ""
        sTtaux = sp_GetMemo(sPetic, "DESCRIPCIO")
        sTtaux = ReplaceString(sTtaux, 13, "")
        sTtaux = cutString(sTtaux, 10)
        If Len(sTtaux) > 2 Then
            sTexto = sTexto & sTtaux & Chr(10)
        End If
        sTexto = ReplaceString(sTexto, 13, "")
        sTexto = cutString(sTexto, 10)
        xlSheet.Cells(i, colDetalle) = sTexto
    Next
    
    If Val(xSelectAgrup) > 0 Then
        For i = 2 To cntRows
            'Call Status("Agrupamiento:" & i)
            sPetic = xlSheet.Cells(i, colNroInterno)
            sTexto = ""
            If sp_GetAgrupPeticHijo(xSelectAgrup, sPetic) Then
                'Call Status("Agrupamiento:" & I & " OK")
                Do While Not aplRST.EOF
                    sTexto = sTexto & ClearNull(aplRST!agr_titulo) & Chr(10)
                    'Call Status("Agrupamiento:" & I & " OK 1")
                    aplRST.MoveNext
                Loop
                 'aplRST.Close
                sTexto = cutString(sTexto, 10)
                xlSheet.Cells(i, colAgrup) = sTexto
            End If
        Next
    End If
   
    'Call Status("Columna Horas")
    xlSheet.Columns(colHs).Delete
    'Call Status("Columna Int")
    xlSheet.Columns(colNroInterno).Delete
    'Call Status("HorizontalAlignment")
    With xlSheet.Rows(1)
        .HorizontalAlignment = 4131
        .RowHeight = 30
    End With
    
    With xlSheet.Range(xlSheet.Cells(1, 1), xlSheet.Cells(cntRows, colFInf - 1))
        .Borders(5).LineStyle = -4142
        .Borders(6).LineStyle = -4142
        .Borders(7).LineStyle = 1: .Borders(7).Weight = 1
        .Borders(8).LineStyle = 1: .Borders(8).Weight = 1
        .Borders(9).LineStyle = 1: .Borders(9).Weight = 1
        .Borders(10).LineStyle = 1: .Borders(10).Weight = 1
        .Borders(11).LineStyle = 1: .Borders(11).Weight = 1
        .Borders(12).LineStyle = 1: .Borders(12).Weight = 1
    End With
    
    xlSheet.Cells.WrapText = True
    xlSheet.Cells.Select
    xlSheet.Cells.EntireRow.AutoFit
    
    On Error Resume Next
    With xlSheet.PageSetup
        .PrintTitleRows = ""
        .PrintTitleColumns = ""
    End With
    With xlSheet.PageSetup
'        .LeftHeader = ""
'        .CenterHeader = ""
'        .RightHeader = ""
'        .LeftFooter = ""
'        .CenterFooter = ""
'        .RightFooter = ""
'        .LeftMargin = 0
'        .RightMargin = 0
'        .TopMargin = 0
'        .BottomMargin = 0
'        .HeaderMargin = 0
'        .FooterMargin = 0
'        .PrintHeadings = False
'        .PrintGridlines = False
'        .PrintComments = -4142
'        .CenterHorizontally = False
'        .CenterVertically = False
        .Orientation = 2
        .Zoom = 100
    End With
errOpen:
    ExcelWrk.Save
    ExcelWrk.Close
    ExcelApp.Application.Quit
    SaveSetting "GesPet", "Export01", "FileMinuta", ClearNull(txtFile.Text)
    'Screen.MousePointer = vbNormal
    Call Puntero(False)
    Call Status("Listo.")
    If cntRows < 2 Then
       Call Puntero(False)
       MsgBox "La consulta no arroja resultados que cumplan con el criterio", vbInformation + vbOKOnly
    Else
       MsgBox "Se ha generado exitosamente el archivo: " & _
            Trim(txtFile.Text) & vbCrLf & "Proceso de exportaci�n finalizado.", vbInformation + vbOKOnly, "Exportaci�n finalizada"
    End If
    Unload Me
    Exit Sub
finx:
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub cmdCerrar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    LockProceso (False)
    Unload Me
End Sub

Private Sub OptNivelSec_Click(Index As Integer)
    cboGerenciaSec.Visible = False
    cboSectorSec.Visible = False
    cboGerenciaSec.Enabled = False
    cboSectorSec.Enabled = False
    Select Case Index
        Case 0
            cboGerenciaSec.Visible = True
            cboGerenciaSec.Enabled = True
        Case 1
            cboSectorSec.Visible = True
            cboSectorSec.Enabled = True
    End Select
End Sub

Function verAreaSec() As Variant
    Dim vRetorno() As String
    Dim auxAgrup As String
       
    verAreaSec = "NULL"
    secDire = "NULL"
    secGere = "NULL"
    secSect = "NULL"
    secGrup = "NULL"
       
    If OptNivelSec(0).Value = True Then
         If cboGerenciaSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Gerencia")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboGerenciaSec, True)
         auxAgrup = DatosCombo(Me.cboGerenciaSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(1).Value = True Then
         If cboSectorSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Sector")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboSectorSec, True)
         auxAgrup = DatosCombo(Me.cboSectorSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
End Function

Function verNivelSec() As Variant
       verNivelSec = "SECT"
End Function
'******************************************************************

Private Sub txtHoras_GotFocus()
    txtHoras.MaxLength = 3
End Sub

Private Sub txtHoras_LostFocus()
    txtHoras.Text = ClearNull(CInt(Val(txtHoras)))
End Sub

Private Sub cmdGuardar_Click()
    Dim tmpFile As String
    
    tmpFile = Dialogo_GuardarExportacion("GUARDAR")
    txtFile = IIf(tmpFile = "", txtFile, tmpFile)
End Sub

'{ add -002- b.
'Private Sub Form_Activate()
'    StatusBarObjectIdentity Me
'End Sub
'}

