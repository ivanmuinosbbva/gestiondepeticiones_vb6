VERSION 5.00
Begin VB.Form frmProyectosIDMEditNovedades 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Carga de novedades"
   ClientHeight    =   1800
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8805
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1800
   ScaleWidth      =   8805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraBotones 
      Height          =   1695
      Left            =   7560
      TabIndex        =   1
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   495
         Left            =   120
         TabIndex        =   3
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "Guardar"
         Height          =   495
         Left            =   120
         TabIndex        =   2
         Top             =   120
         Width           =   975
      End
   End
   Begin VB.TextBox txtNovedades 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1575
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   7335
   End
End
Attribute VB_Name = "frmProyectosIDMEditNovedades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public cModo As String
Public cTexto As String

Private Sub Form_Load()
    Select Case cModo
        Case "ALTA"
            txtNovedades.Text = ""
        Case "EDICION"
            txtNovedades.Text = cTexto
    End Select
End Sub

Private Sub cmdGuardar_Click()
    frmProyectosIDMShellNovedades.cNuevaNovedad = ClearNull(txtNovedades)
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    frmProyectosIDMShellNovedades.cNuevaNovedad = ""
    Unload Me
End Sub
