VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmCLISTCarga 
   Caption         =   "Form1"
   ClientHeight    =   5730
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9960
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCLISTCarga.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5730
   ScaleWidth      =   9960
   Begin VB.CommandButton cmdCatalogo 
      Caption         =   "C&at�logo"
      Height          =   375
      Left            =   120
      TabIndex        =   119
      TabStop         =   0   'False
      ToolTipText     =   "Agregar el archivo en el cat�logo de enmascaramiento"
      Top             =   5280
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Adelante 
      Caption         =   "Sigu&iente �"
      Height          =   375
      Left            =   7440
      TabIndex        =   43
      Top             =   5280
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Atras 
      Caption         =   "� Ant&erior"
      Height          =   375
      Left            =   6360
      TabIndex        =   46
      Top             =   5280
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   7440
      TabIndex        =   44
      Top             =   5280
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Cancelar 
      Caption         =   "Ca&ncelar"
      Height          =   375
      Left            =   5160
      TabIndex        =   45
      Top             =   5280
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Terminar 
      Caption         =   "Finalizar"
      Height          =   375
      Left            =   8640
      TabIndex        =   47
      Top             =   5280
      Width           =   1095
   End
   Begin VB.Frame fraStep1 
      Caption         =   " Paso 1. Opciones de solicitud "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   120
      TabIndex        =   48
      Top             =   120
      Width           =   9735
      Begin VB.CheckBox chkEmergencia 
         Caption         =   "EMErgencia"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   255
         Left            =   240
         TabIndex        =   132
         Top             =   4080
         Width           =   1335
      End
      Begin VB.CheckBox chkDiferido 
         Caption         =   "Ejecuci�n diferida (fin de semana)"
         Height          =   255
         Left            =   240
         TabIndex        =   123
         Top             =   3840
         Width           =   3015
      End
      Begin VB.CheckBox chkRestore 
         Caption         =   "Restore previo"
         Height          =   255
         Left            =   240
         TabIndex        =   120
         Top             =   3600
         Width           =   1455
      End
      Begin VB.CheckBox chkSINEnmascararDatos 
         Caption         =   "Forzar la transmisi�n sin enmascaramiento de los datos"
         Height          =   255
         Left            =   240
         TabIndex        =   57
         Top             =   3360
         Width           =   4575
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "&4. Copia con selecci�n de Datos (SORT)"
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   55
         Top             =   2880
         Width           =   3495
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "&3. Extracci�n del VSAM Tablas Corporativas"
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   54
         Top             =   2520
         Width           =   3975
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "&2. UNLOAD de tabla"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   53
         Top             =   2160
         Width           =   2055
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "&1. Copia de archivo completo a DESARROLLO"
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   52
         Top             =   1800
         Value           =   -1  'True
         Width           =   3975
      End
      Begin VB.CommandButton cmdAyuda 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9240
         Picture         =   "frmCLISTCarga.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   49
         TabStop         =   0   'False
         Top             =   3480
         Width           =   375
      End
      Begin VB.Label lblEMEMsg 
         Alignment       =   2  'Center
         Caption         =   $"frmCLISTCarga.frx":0294
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   540
         Left            =   4200
         TabIndex        =   133
         Top             =   3720
         Visible         =   0   'False
         Width           =   5040
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblAdvertirSegInf 
         Alignment       =   2  'Center
         Caption         =   "EL ARCHIVO CONTIENE DATOS SENSIBLES QUE NO DEBEN ENMASCARARSE. Requerir� autorizaci�n expl�cita del Supervisor."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   420
         Left            =   4200
         TabIndex        =   108
         Top             =   3720
         Visible         =   0   'False
         Width           =   4920
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblWizard 
         AutoSize        =   -1  'True
         Caption         =   "�Qu� desea generar?"
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   51
         Top             =   1440
         Width           =   1605
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmCLISTCarga.frx":0320
         Height          =   735
         Index           =   0
         Left            =   240
         TabIndex        =   50
         Top             =   480
         Width           =   9255
      End
   End
   Begin VB.Frame fraStep2 
      Caption         =   " Paso 2. Especificaciones de generaci�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   120
      TabIndex        =   56
      Top             =   120
      Width           =   9735
      Begin VB.Frame fraStep2Step 
         Height          =   3975
         Index           =   2
         Left            =   240
         TabIndex        =   59
         Top             =   480
         Visible         =   0   'False
         Width           =   9255
         Begin VB.TextBox txtOpc2Peticion 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            MaxLength       =   50
            TabIndex        =   18
            Top             =   3000
            Width           =   1215
         End
         Begin VB.TextBox txtOpc2Tabla 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            TabIndex        =   14
            Top             =   1920
            Width           =   5415
         End
         Begin VB.TextBox txtOpc2JobBACKUP 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            TabIndex        =   16
            Top             =   2640
            Width           =   4815
         End
         Begin VB.CheckBox chk_CopyUNLO 
            Caption         =   "Sin COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7920
            TabIndex        =   10
            Top             =   600
            Width           =   735
         End
         Begin VB.TextBox txtOpc2FileOUT_BBLCPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            MaxLength       =   50
            TabIndex        =   9
            Top             =   480
            Width           =   5415
         End
         Begin VB.TextBox txtOpc2FileOUT_NMECPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            MaxLength       =   50
            TabIndex        =   11
            Top             =   840
            Width           =   5415
         End
         Begin VB.CommandButton cmdSeleccionarPeticion 
            Caption         =   "..."
            Height          =   330
            Index           =   2
            Left            =   3600
            TabIndex        =   19
            Top             =   3000
            Width           =   375
         End
         Begin VB.TextBox txtOpc2PeticionTitulo 
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4080
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   76
            TabStop         =   0   'False
            Top             =   3000
            Width           =   4455
         End
         Begin VB.CheckBox chkOpc2Join 
            Caption         =   "Join de tablas"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   7320
            TabIndex        =   17
            Top             =   2678
            Width           =   1815
         End
         Begin VB.TextBox txtOpc2PeticionInterno 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            MaxLength       =   50
            TabIndex        =   84
            Top             =   2640
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.TextBox txtOpc2FileOUT 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            MaxLength       =   50
            TabIndex        =   8
            Top             =   120
            Width           =   5415
         End
         Begin VB.TextBox txtOpc2LibSysin 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            TabIndex        =   12
            Top             =   1200
            Width           =   5415
         End
         Begin VB.TextBox txtOpc2MemSysin 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            TabIndex        =   13
            Top             =   1560
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc2FileOUT_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            TabIndex        =   81
            TabStop         =   0   'False
            Text            =   "cmbOpc2FileOUT_Sel"
            Top             =   120
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc2LibSysin_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            TabIndex        =   82
            TabStop         =   0   'False
            Text            =   "cmbOpc2FileOUT_Sel"
            Top             =   1200
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc2MemSysin_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2400
            TabIndex        =   83
            TabStop         =   0   'False
            Text            =   "cmbOpc2FileOUT_Sel"
            Top             =   1560
            Width           =   5415
         End
         Begin AT_MaskText.MaskText txtOpc2FechaBackUp 
            Height          =   330
            Left            =   2400
            TabIndex        =   15
            Top             =   2280
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   582
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            DataType        =   3
            SpecialFeatures =   -1  'True
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "4. Tabla"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   37
            Left            =   0
            TabIndex        =   136
            Top             =   1980
            Width           =   840
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "6. JOB de backup"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   36
            Left            =   0
            TabIndex        =   135
            Top             =   2700
            Width           =   1680
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "5. Fecha de backup"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   35
            Left            =   0
            TabIndex        =   134
            Top             =   2340
            Width           =   1890
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Biblioteca de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   34
            Left            =   210
            TabIndex        =   131
            Top             =   540
            Width           =   1890
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Nombre de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   31
            Left            =   630
            TabIndex        =   126
            Top             =   900
            Width           =   1470
         End
         Begin VB.Label lblOpc2MemSysin 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   7920
            TabIndex        =   112
            Top             =   1680
            Width           =   105
         End
         Begin VB.Label lblOpc2LibSysin 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   7920
            TabIndex        =   111
            Top             =   1275
            Width           =   105
         End
         Begin VB.Label lblOpc2FileOut 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   7920
            TabIndex        =   110
            Top             =   195
            Width           =   105
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "UNLO"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Index           =   18
            Left            =   8640
            TabIndex        =   78
            Top             =   3720
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "7. Petici�n N�"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   17
            Left            =   0
            TabIndex        =   77
            Top             =   3060
            Width           =   1470
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "3. Miembro de Sysin"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   7
            Left            =   0
            TabIndex        =   63
            Top             =   1620
            Width           =   1995
         End
         Begin VB.Label lblWarningUNLO 
            AutoSize        =   -1  'True
            Caption         =   "Advertencias"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   180
            Left            =   120
            TabIndex        =   62
            Top             =   3720
            Visible         =   0   'False
            Width           =   990
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "2. Librer�a de Sysin"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   5
            Left            =   0
            TabIndex        =   61
            Top             =   1260
            Width           =   2100
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "1. Archivo de salida"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   0
            TabIndex        =   60
            Top             =   180
            Width           =   2100
         End
      End
      Begin VB.Frame fraStep2Step 
         Height          =   3975
         Index           =   1
         Left            =   240
         TabIndex        =   93
         Top             =   480
         Visible         =   0   'False
         Width           =   9255
         Begin AT_MaskText.MaskText txtOpc1Peticion 
            Height          =   330
            Left            =   2760
            TabIndex        =   6
            Top             =   1920
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.CheckBox chk_CopyXCOM 
            Caption         =   "Sin COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   8280
            TabIndex        =   2
            Top             =   600
            Width           =   735
         End
         Begin VB.TextBox txtOpc1FilePROD_NMECPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   3
            Top             =   840
            Width           =   5415
         End
         Begin VB.TextBox txtOpc1FilePROD_BBLCPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   1
            Top             =   480
            Width           =   5415
         End
         Begin VB.TextBox txtOpc1FilePROD 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   0
            Top             =   140
            Width           =   5415
         End
         Begin VB.CommandButton cmdSeleccionarPeticion 
            Caption         =   "..."
            Height          =   330
            Index           =   1
            Left            =   3960
            TabIndex        =   7
            Top             =   1920
            Width           =   375
         End
         Begin VB.TextBox txtOpc1PeticionTitulo 
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4440
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   94
            TabStop         =   0   'False
            Top             =   1920
            Width           =   4335
         End
         Begin VB.ComboBox cmbOpc1FilePROD_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   97
            Text            =   "cmbOpc1FilePROD_Sel"
            Top             =   140
            Visible         =   0   'False
            Width           =   5415
         End
         Begin VB.TextBox txtOpc1PeticionInterno 
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2760
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   95
            TabStop         =   0   'False
            Top             =   1920
            Visible         =   0   'False
            Width           =   1215
         End
         Begin AT_MaskText.MaskText txtOpc1FechaBackUp 
            Height          =   330
            Left            =   2760
            TabIndex        =   4
            Top             =   1200
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   582
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            DataType        =   3
            SpecialFeatures =   -1  'True
         End
         Begin AT_MaskText.MaskText txtOpc1FileDESA 
            Height          =   330
            Left            =   2760
            TabIndex        =   5
            Top             =   1560
            Width           =   5415
            _ExtentX        =   9551
            _ExtentY        =   582
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.ComboBox cmbOpc1FileDESA_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   96
            Text            =   "cmbOpc1FileDESA_Sel"
            Top             =   1560
            Visible         =   0   'False
            Width           =   5415
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Biblioteca de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   27
            Left            =   630
            TabIndex        =   128
            Top             =   540
            Width           =   1890
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Nombre de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   28
            Left            =   1050
            TabIndex        =   124
            Top             =   900
            Width           =   1470
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "2. Fecha de BACKUP"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   24
            Left            =   0
            TabIndex        =   121
            Top             =   1260
            Width           =   1890
         End
         Begin VB.Label lblOpc1FileDesa 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   8280
            TabIndex        =   114
            Top             =   1635
            Width           =   105
         End
         Begin VB.Label lblOpc1FileProd 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   8280
            TabIndex        =   113
            Top             =   215
            Width           =   105
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "3. Archivo de Desarrollo"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   2
            Left            =   0
            TabIndex        =   102
            Top             =   1620
            Width           =   2520
         End
         Begin VB.Label lblWarning 
            AutoSize        =   -1  'True
            Caption         =   "Advertencias"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   180
            Left            =   120
            TabIndex        =   101
            Top             =   3720
            Visible         =   0   'False
            Width           =   990
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "1. Archivo de Producci�n"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   1
            Left            =   0
            TabIndex        =   100
            Top             =   200
            Width           =   2520
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "4. Petici�n N�"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   16
            Left            =   0
            TabIndex        =   99
            Top             =   1980
            Width           =   1470
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "XCOM"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Index           =   19
            Left            =   8520
            TabIndex        =   98
            Top             =   3720
            Visible         =   0   'False
            Width           =   525
         End
      End
      Begin VB.Frame fraStep2Step 
         Height          =   4005
         Index           =   4
         Left            =   240
         TabIndex        =   68
         Top             =   480
         Visible         =   0   'False
         Width           =   9255
         Begin VB.TextBox txtOpc4FilePROD_BBLCPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   28
            Top             =   480
            Width           =   5415
         End
         Begin VB.CheckBox chk_CopySORT 
            Caption         =   "Sin COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   8280
            TabIndex        =   29
            Top             =   600
            Width           =   735
         End
         Begin VB.TextBox txtOpc4FilePROD_NMECPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   30
            Top             =   840
            Width           =   5415
         End
         Begin VB.CommandButton cmdSeleccionarPeticion 
            Caption         =   "..."
            Height          =   330
            Index           =   4
            Left            =   3960
            TabIndex        =   36
            Top             =   2640
            Width           =   375
         End
         Begin VB.TextBox txtOpc4PeticionTitulo 
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4440
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   40
            TabStop         =   0   'False
            Top             =   2640
            Width           =   4335
         End
         Begin AT_MaskText.MaskText txtOpc4FechaBackup 
            Height          =   330
            Left            =   2760
            TabIndex        =   31
            Top             =   1200
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   582
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            DataType        =   3
            SpecialFeatures =   -1  'True
         End
         Begin VB.TextBox txtOpc4Peticion 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   35
            Top             =   2640
            Width           =   1215
         End
         Begin VB.TextBox txtOpc4FileDESA 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   32
            Top             =   1560
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc4FileDESA_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   38
            TabStop         =   0   'False
            Text            =   "cmbOpc4FileDESA_Sel"
            Top             =   1560
            Width           =   5415
         End
         Begin VB.TextBox txtOpc4FilePROD 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   27
            Top             =   120
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc4FilePROD_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmCLISTCarga.frx":04BA
            Left            =   2760
            List            =   "frmCLISTCarga.frx":04BC
            TabIndex        =   37
            TabStop         =   0   'False
            Text            =   "cmbOpc4FilePROD_Sel"
            Top             =   120
            Width           =   5415
         End
         Begin VB.TextBox txtOpc4MemSysin 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   34
            Top             =   2280
            Width           =   5415
         End
         Begin VB.TextBox txtOpc4LibSysin 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   33
            Top             =   1920
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc4LibSysin_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   91
            TabStop         =   0   'False
            Text            =   "cmbOpc4LibSysin_Sel"
            Top             =   1920
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc4MemSysin_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            TabIndex        =   92
            TabStop         =   0   'False
            Text            =   "cmbOpc4MemSysin_Sel"
            Top             =   2280
            Width           =   5415
         End
         Begin VB.TextBox txtOpc4PeticionInterno 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2760
            MaxLength       =   50
            TabIndex        =   39
            Top             =   2640
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Biblioteca de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   30
            Left            =   630
            TabIndex        =   129
            Top             =   540
            Width           =   1890
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Nombre de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   33
            Left            =   1050
            TabIndex        =   127
            Top             =   900
            Width           =   1470
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "2. Fecha de backup"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   25
            Left            =   0
            TabIndex        =   122
            Top             =   1260
            Width           =   1890
         End
         Begin VB.Label lblOpc4MemSysin 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   8280
            TabIndex        =   118
            Top             =   2355
            Width           =   105
         End
         Begin VB.Label lblOpc4LibSysin 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   8280
            TabIndex        =   117
            Top             =   1995
            Width           =   105
         End
         Begin VB.Label lblOpc4FileDesa 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   8280
            TabIndex        =   116
            Top             =   1635
            Width           =   105
         End
         Begin VB.Label lblOpc4FileProd 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   8280
            TabIndex        =   115
            Top             =   195
            Width           =   105
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "6. Petici�n N�"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   23
            Left            =   0
            TabIndex        =   90
            Top             =   2700
            Width           =   1470
         End
         Begin VB.Label lblWizard 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "SORT"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Index           =   20
            Left            =   8640
            TabIndex        =   79
            Top             =   3720
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "5. Miembro con Sysin"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   15
            Left            =   0
            TabIndex        =   73
            Top             =   2340
            Width           =   2100
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "4. Librer�a del Sysin"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   14
            Left            =   0
            TabIndex        =   72
            Top             =   1980
            Width           =   2205
         End
         Begin VB.Label lblWarningSORT 
            AutoSize        =   -1  'True
            Caption         =   "Advertencias"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   180
            Left            =   120
            TabIndex        =   71
            Top             =   3745
            Visible         =   0   'False
            Width           =   990
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "3. Archivo de Desarrollo"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   12
            Left            =   0
            TabIndex        =   70
            Top             =   1620
            Width           =   2520
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "1. Archivo de Producci�n"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   8
            Left            =   0
            TabIndex        =   69
            Top             =   180
            Width           =   2520
         End
      End
      Begin VB.Frame fraStep2Step 
         Height          =   3975
         Index           =   3
         Left            =   240
         TabIndex        =   64
         Top             =   480
         Visible         =   0   'False
         Width           =   9255
         Begin VB.TextBox txtOpc3Peticion 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            MaxLength       =   50
            TabIndex        =   25
            Top             =   1560
            Width           =   1215
         End
         Begin VB.CheckBox chk_CopyTCOR 
            Caption         =   "Sin COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Left            =   8520
            TabIndex        =   22
            Top             =   600
            Width           =   735
         End
         Begin VB.TextBox txtOpc3FileExtract_BBLCPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            MaxLength       =   50
            TabIndex        =   21
            Top             =   480
            Width           =   5415
         End
         Begin VB.TextBox txtOpc3FileExtract_NMECPY 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            MaxLength       =   50
            TabIndex        =   23
            Top             =   840
            Width           =   5415
         End
         Begin VB.TextBox txtOpc3PeticionTitulo 
            BackColor       =   &H8000000F&
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4680
            Locked          =   -1  'True
            MaxLength       =   50
            TabIndex        =   85
            TabStop         =   0   'False
            Top             =   1560
            Width           =   4095
         End
         Begin VB.CommandButton cmdSeleccionarPeticion 
            Caption         =   "..."
            Height          =   330
            Index           =   3
            Left            =   4200
            TabIndex        =   26
            Top             =   1560
            Width           =   375
         End
         Begin VB.TextBox txtOpc3FileExtract 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            MaxLength       =   10
            TabIndex        =   20
            Top             =   120
            Width           =   5415
         End
         Begin VB.TextBox txtOpc3FileDESA 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            MaxLength       =   50
            TabIndex        =   24
            Top             =   1200
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc3FileDESA_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            TabIndex        =   89
            TabStop         =   0   'False
            Top             =   1200
            Width           =   5415
         End
         Begin VB.ComboBox cmbOpc3FileExtract_Sel 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            TabIndex        =   88
            TabStop         =   0   'False
            Top             =   120
            Width           =   5415
         End
         Begin VB.TextBox txtOpc3PeticionInterno 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3000
            MaxLength       =   50
            TabIndex        =   87
            Top             =   1560
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Biblioteca de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   32
            Left            =   840
            TabIndex        =   130
            Top             =   540
            Width           =   1890
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Nombre de COPY"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   29
            Left            =   1260
            TabIndex        =   125
            Top             =   900
            Width           =   1470
         End
         Begin VB.Label lblOpc3FileDesa 
            AutoSize        =   -1  'True
            Caption         =   "0"
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   8520
            TabIndex        =   109
            Top             =   1275
            Width           =   105
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "3. Petici�n N�"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   22
            Left            =   0
            TabIndex        =   86
            Top             =   1620
            Width           =   1470
         End
         Begin VB.Label lblWizard 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "TCOR"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Index           =   21
            Left            =   8520
            TabIndex        =   80
            Top             =   3720
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "1. Nro. de tabla a extraer"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   11
            Left            =   0
            TabIndex        =   67
            Top             =   180
            Width           =   2730
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "2. Archivo de Desarrollo"
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   10
            Left            =   0
            TabIndex        =   66
            Top             =   1260
            Width           =   2520
         End
         Begin VB.Label lblWarningTCOR 
            AutoSize        =   -1  'True
            Caption         =   "Advertencias"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000080&
            Height          =   180
            Left            =   120
            TabIndex        =   65
            Top             =   3720
            Visible         =   0   'False
            Width           =   990
         End
      End
   End
   Begin VB.Frame fraStep4 
      Caption         =   " Paso 4. Finalizar la generaci�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   120
      TabIndex        =   103
      Top             =   120
      Width           =   9735
      Begin VB.TextBox txtScript 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2895
         Left            =   240
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   104
         Top             =   840
         Width           =   9255
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmCLISTCarga.frx":04BE
         Height          =   375
         Index           =   2
         Left            =   240
         TabIndex        =   105
         Top             =   360
         Width           =   9135
      End
   End
   Begin VB.Frame fraStep3 
      Caption         =   " Paso 3. Justificaci�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   120
      TabIndex        =   58
      Top             =   120
      Width           =   9735
      Begin VB.ComboBox cmbJustificacion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   41
         Top             =   960
         Width           =   8175
      End
      Begin VB.TextBox txtJustificacion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2295
         Left            =   1320
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   42
         Top             =   1440
         Width           =   8175
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "Ampliaci�n detallada del motivo"
         Height          =   540
         Index           =   1
         Left            =   240
         TabIndex        =   107
         Top             =   1440
         Width           =   960
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "Motivos:"
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   106
         Top             =   960
         Width           =   660
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmCLISTCarga.frx":058F
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   74
         Top             =   360
         Width           =   9135
      End
   End
   Begin VB.Label lblAyuda 
      ForeColor       =   &H00808080&
      Height          =   375
      Left            =   120
      TabIndex        =   75
      Top             =   4800
      Width           =   9735
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmCLISTCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 08.09.2009 - Se agregan nuevas restricciones (solicitado por email. Definici�n de Dante Novara).
' -001- b. FJS 08.09.2009 - Se corrige un error: este tipo tiene que ser char a los efectos de guardar el cero a la izquierda del n�mero.
' -002- a. FJS 22.10.2009 - Validaci�n contra el Cat�logo de archivos de enmascaramiento.
' -003- a. FJS 26.10.2009 - Se agrega por default que aparezca el ARBP.ENM.Legajo.
' -004- a. FJS 28.10.2009 - Cuando los datos se solicitan SIN ENMASCARAMIENTO, se inhabilita el acceso al cat�logo de archivos de enmascaramiento desde aqu�.
' -004- b. FJS 28.10.2009 - Cuando la solicitud es SIN ENMASCARAMIENTO, de obliga al usuario a describir ampliando el motivo o justificaci�n de la solicitud.
' -004- c. FJS 28.10.2009 - Nuevo: cuando es SIN ENMASCARAMIENTO, es obligatorio seleccionar un motivo.
' -004- d. FJS 28.10.2009 - Se guarda en una variable de objeto el �ltimo control que tuvo el foco para poder recordar la posici�n al volver a la pantalla.
' -005- a. FJS 03.11.2009 - Se valida que la petici�n seleccionada o ingresada se corresponda con el grupo del usuario que solicita (adem�s del estado "En ejecuci�n").
' -006- a. FJS 09.11.2009 - Se permite que al ingresar datos en un combo, se precarguen los nombres de archivo del cat�logo para agilizar la carga de datos.
' -007- a. FJS 18.11.2009 - Se modifica el default para cuando los datos deben venir sin enmascaramiento.
' -008- a. FJS 23.11.2009 - Se agrega la marca de si era enmascarado o no, cuando se pide una copia (estaba faltando).
' -008- b. FJS 15.01.2010 - Bug: se corrige, porque no esta permitiendo realizar modificicaciones sobre la copia.
' -009- a. FJS 02.12.2009 - Se agrega una restricci�n al nombre del archivo de origen en todos los casos (no puede ser ARBP.ENM).
' -009- b. FJS 02.12.2009 - Nueva restricci�n para los archivos de salida o destino (no pueden contener en ninguna parte del nombre las siglas .VSM).
' -009- c. FJS 02.12.2009 - Validaci�n adicional: los nombres de los archivos de origen y destino no pueden ser iguales.
' -009- d. FJS 02.12.2009 - Validaciones del primer calificador (por cambio de nomenclatura estandar): en opciones 1 y 4, el primer calificador puede ser ARBP � ABP*.xxx (entre punto y punto nunca m�s de 8).
' -009- e. FJS 11.01.2010 - Se agrega el calificador o prefijo ARMP como v�lido (archivos productivos de multiempresa). A pedido de Dopazo y Gurvich.
' -009- f. FJS 13.09.2010 - Se corrige la rutina -009- d. porque esta funcionando mal.
' -010- a. FJS 02.12.2009 - Nueva funcionalidad RESTORE: opcionalmente, para las opciones 1 y 4, podr� solicitarse la restituci�n de un backup (restore).
' -011- a. FJS 16.12.2009 - Si el archivo existe, se valida que este completa su definici�n para ser invocado (si no tiene copys y/o campos declarados, el archivo no puede usarse).
' -012- a. FJS 08.01.2010 - Se agrega una validaci�n adicional para evitar que con los accesos r�pidos de teclado se "avoiden" los controles.
' -013- a. FJS 12.01.2010 - Si el archivo solicitado es SIN enmascaramiento de datos (autorizaci�n nivel III) entonces no se permite que el segundo calificador sea .ENM
' -014- a. FJS 14.01.2010 - Se agrega validaci�n para forzar a que el usuario especifique el nombre de un archivo cuando solicita los datos SIN enmascarar.
' -015- a. FJS 20.01.2010 - Se agrega la opci�n de solicitud para ejecutar en diferido (el fin de semana).
' -016- a. FJS 15.02.2010 - Se restringe el m�ximo de caracteres del control TextBox "txtJustificacion" para evitar el error de overflow del campo (255 m�x.)
' -017- a. FJS 01.03.2010 - Se agrega control para evitar que se genere una solicitud en una misma tanda con el mismo destino.
' -018- a. FJS 12.01.2011 - Nuevo: nuevas funcionalidades para Seguridad Inform�tica.
' -019- a. FJS 08.02.2011 - Nuevo: se agrega soporte para avisar al usuario que un DSN est� rechazado por SI.
' -020- a. FJS 11.09.2012 - Nuevo: se agregan los datos para XCOM de biblioteca y nombre de COPY del archivo de producci�n.
' -021- a. FJS 27.06.2014 - Nuevo: se agrega la opci�n de generar una solicitud de EMErgencia (no se baja al archivo de transmisi�n, sino que va directamente mail con copia a todos, incluso Gustavo Siciliano). La aprobaci�n es ex post.
' -021- b. FJS 17.07.2014 - Nuevo: cuando la solicitud no es por EMErgencia, pero sin enmascarar datos, se envia recordatorio de aprobaci�n al L�der. Cuando es por emergencia, se envia pedido de autorizaci�n al Supervisor.
' -022- a. FJS 28.07.2014 - Nuevo: Habilitar UNLOAD con RESTORE.
' -023- a. FJS 30.07.2014 - Nuevo: Se cambia la frecuencia de los envios a CM (desde las 8 AM cada 1 hora hasta las 18 PM inclusive).
' -024- a. FJS 15.08.2014 - Nuevo: se adapta para que puedan realizar pedidos los supervisores (N3).
' -025- a. FJS 22.08.2014 - Nuevo: se agregan 3 nuevos par�metros para los UNLOAD con Restore.
' -026- a. FJS 01.09.2014 - Nuevo: se quita el archivo intermedio para carga del usuario, y se reemplaza el procedimiento de la misma manera que en XCOM y UNLO.
' -027- a. FJS 14.10.2014 - Corregido: al copiar una solicitud en otra, esta dejando mal las opciones de los checkbox (enmascaramiento y emergencia).
' -028- a. FJS 25.03.2015 - Nuevo: se cambia la llamada al sp_GetHEDT001 por sp_GetHEDT001a.

Option Explicit

Private Const AYUDA_SELECCION_ARCHIVO = "Puede ingresar manualmente el nombre del archivo o pulsar dos veces F4 para activar un combobox con los nombres utilizados hasta el momento por Vd."
Private Const AYUDA_PETICION = "Puede ingresar manualmente el n�mero de la petici�n de referencia o pulsar el bot�n de selecci�n de peticiones para activar la pantalla de selecci�n de peticiones activas. Solo peticiones en estado En ejecuci�n pueden ser referenciadas."
Private Const AYUDA_JOIN = "Indica si el UNLOAD a solicitar es un join entre tablas. Exclusivamente declarativo."
Private Const AYUDA_COMBO = "Desplegable activado. Pulse nuevamente F4 para desplegar los nombres. Puede seleccionar alguno de los nombres de archivos utilizados con anterioridad y luego modificarlo. Si no desea utilizar esta funci�n, escriba un nuevo nombre de archivo."
'{ add -021- a.
Private Const WZRD_PASO0_INICIAR = 0
Private Const WZRD_PASO1_OPCIONES = 1
Private Const WZRD_PASO2_DATOS = 2
Private Const WZRD_PASO3_JUSTIFICACION = 3
Private Const WZRD_PASO4_CONFIRMACION = 4

Private Const TIPOSOL_XCOM = 1
Private Const TIPOSOL_UNLO = 2
Private Const TIPOSOL_TCOR = 3
Private Const TIPOSOL_SORT = 4
Private Const TIPOSOL_XCOMR = 5
Private Const TIPOSOL_SORTR = 6
Private Const TIPOSOL_UNLOR = 7
'}

Private Const CARACTERES_INVALIDOS = "!#$%&()*+,/:;<=>?@[\]^_`{|}'~��������������������������������������������������������������������������������������������������������������������������������"   ' upd -Se quita el gui�n 22.08.2012
Private Const FECHA_NULL = "01/01/1900 00:00:00"

' UDTs: Solicitud
Private Type udtSOLICITUD
    Item As Integer
    tipo As Byte
    Diferida As Boolean
    Emergencia As Boolean               ' add -021- a.
    Masked As Boolean
    fecha As Date
    Recurso As String
    Peticion As Long
    CodigoJus As Integer
    Justificacion As String
    ArchivoProd As String
    ArchivoDesa As String
    ArchivoOut As String
    ArchivoIntermedio As String
    FechaBckUp As Date
    Lib_Sysin As String
    Mem_Sysin As String
    Join As Boolean
    NroTabla As String                  ' upd -001- b. Se cambia el tipo de Long a String
    CopyBiblioteca As String
    NameBiblioteca As String
    dsn_id As String
    '{ add -025- a.
    TablaBackup As String
    JobBackup As String
    '}
End Type

Public cModo As String
Public lSol_NroAsignado As Long

Dim Solicitud As udtSOLICITUD
Dim oLastControlUsed As Control     ' add -004- d.
Dim bCopying As Boolean             ' add -008- b.
Dim cntSolicitudes As Integer
Dim cTipoSolicitud As String
Dim iTipoSolicitud As Integer
Dim bFlagLoad As Boolean
Dim sDSN_ID As String               ' Auxiliar para poder guardar el DSN en la tabla Solicitudes
Dim bDatosSensibles As Boolean      ' Auxiliar para determinar si un DSN seleccionado tiene datos sensibles
Dim sEncabezadoArchivo As String    ' Auxiliar para armar el encabezado del archivo de cat�logo (Ej.: ARBP.ENM.XA00309.XXXX)
Dim bControlDuplicidad As Boolean   ' Auxiliar: se carga para habilitar el control de pedido de archivos duplicados
    
Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    
    bFlagLoad = True
    bCopying = False                ' add -008- b.
    bControlDuplicidad = False
    
    fraStep2Step(1).BorderStyle = 0
    fraStep2Step(2).BorderStyle = 0
    fraStep2Step(3).BorderStyle = 0
    fraStep2Step(4).BorderStyle = 0
    
    chk_CopyXCOM.Value = 1
    chk_CopyUNLO.Value = 1
    chk_CopyTCOR.Value = 1
    chk_CopySORT.Value = 1
    
    Call setHabilCtrl(txtOpc2LibSysin, "DIS")
    Call setHabilCtrl(txtOpc4LibSysin, "DIS")
    
    ' Configuraci�n: Otros controles
    If sp_GetVarios("CLIST012") Then
        bControlDuplicidad = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, True, False)
    End If
    
    Select Case cModo
        Case "AGREGAR"
            Me.Caption = "Solicitudes a Carga de M�quina - Generando nueva solicitud"
            Call UDT_Inicializar        ' Inicializamos el UDT
            optOpciones_Click 0         ' Seteo inicial y ejecuci�n de <<Inicializar_Asistente>>
            Call HabilitarWizard(1)
        Case "EMERGENCIA"
            Me.Caption = "Solicitudes a Carga de M�quina - Solicitud por EMErgencia"
            Call UDT_Inicializar                ' Inicializamos el UDT
            Call setHabilCtrl(chkSINEnmascararDatos, "DIS")
            chkSINEnmascararDatos.Value = 1     ' Sin enmascaramiento
            
            Call setHabilCtrl(chkDiferido, "DIS")
            chkDiferido.Value = 0
            
            Call setHabilCtrl(chkEmergencia, "DIS")
            chkEmergencia.Value = 1

            Set oLastControlUsed = txtOpc1FilePROD
            optOpciones_Click 0     ' Seteo inicial y ejecuci�n de <<Inicializar_Asistente>>
            Call HabilitarWizard(1)
        Case Else
            bCopying = True
            Me.Caption = "Solicitudes a CM - Copiando solicitud n� " & Trim(lSol_NroAsignado) & " en nueva..."
            Call InicializarCopia
            Call HabilitarWizard(2)
    End Select
    '{ del -021- a.
    'If cModo = "AGREGAR" Then
    '    Me.Caption = "Solicitudes a Carga de M�quina - Generando nueva solicitud"
    '    cntSolicitudes = 0
    '    optOpciones_Click 0     ' Seteo inicial y ejecuci�n de <<Inicializar_Asistente>>
    '    HabilitarWizard 1
    'Else
    '    bCopying = True     ' add -008- b.
    '    Me.Caption = "Solicitudes a Carga de M�quina - Copiando solicitud n� " & Trim(lSol_NroAsignado) & " en nueva..."
    '    InicializarCopia
    '    HabilitarWizard 2
    '    'bCopying = False    ' add -008- b.
    'End If
    '}
    'cmbOpc4FilePROD_Sel.Style = vbComboDropdownList
    bFlagLoad = False
End Sub

Private Sub InicializarCopia()
    Dim i As Integer

    If sp_GetSolicitud(Null, Null, lSol_NroAsignado, Null, Null, Null, Null) Then   ' upd -018- a.
        chkSINEnmascararDatos.Value = IIf(aplRST.Fields!sol_mask = "N", 1, 0)
        With Solicitud
            .tipo = Switch(aplRST.Fields!sol_tipo = "XCOM", 1, aplRST.Fields!sol_tipo = "UNLO", 2, aplRST.Fields!sol_tipo = "TCOR", 3, aplRST.Fields!sol_tipo = "SORT", 4, aplRST.Fields!sol_tipo = "XCOM*", 5, aplRST.Fields!sol_tipo = "SORT*", 6, aplRST.Fields!sol_tipo = "UNLO*", 7)    ' upd -010- a.
            '{ add -010- a.
            .FechaBckUp = IIf(Len(ClearNull(aplRST.Fields!sol_fe_auto3)) = 0, CDate(FECHA_NULL), ClearNull(aplRST.Fields!sol_fe_auto3))
            '.FechaBckUp = IIf(Len(ClearNull(aplRST.Fields!sol_fe_auto3)) = 0, date, ClearNull(aplRST.Fields!sol_fe_auto3))
            If CInt(.tipo) > 4 Then                         ' Los "con RESTORE"
                Select Case .tipo
                    Case TIPOSOL_XCOMR
                        txtOpc1FechaBackUp.Text = .FechaBckUp
                        optOpciones(0).Value = True
                        optOpciones_Click 0                 ' Genera los eventos y carga de variables correspondientes
                        chkRestore.Value = 1
                    Case TIPOSOL_UNLOR
                        txtOpc2FechaBackUp.Text = .FechaBckUp
                        optOpciones(1).Value = True
                        optOpciones_Click 1
                        chkRestore.Value = 1
                        txtOpc2FileOUT = ClearNull(aplRST.Fields!sol_file_desa)
                        txtOpc2Tabla = ClearNull(aplRST.Fields!sol_bckuptabla)
                        txtOpc2JobBACKUP = ClearNull(aplRST.Fields!sol_bckupjob)
                    Case TIPOSOL_SORTR
                        txtOpc4FechaBackup.Text = .FechaBckUp
                        txtOpc4FileDESA.Text = .ArchivoDesa
                        optOpciones(3).Value = True
                        optOpciones_Click 3                 ' Genera los eventos y carga de variables correspondientes
                        chkRestore.Value = 1
                End Select
            Else
                Select Case .tipo
                    Case 1
                        lblWizard(1).Caption = "1. Archivo de Producci�n"
                        Call setHabilCtrl(txtOpc1FechaBackUp, "DIS")
'                    Case 2
'                        txtOpc2FechaBackUp.Text = .FechaBckUp
'                        txtOpc2FileOUT = ClearNull(aplRST.Fields!sol_file_out)
'                        txtOpc2Tabla = ClearNull(aplRST.Fields!sol_bckuptabla)
'                        txtOpc2JobBACKUP = ClearNull(aplRST.Fields!sol_bckupjob)
                    Case 4
                        Call setHabilCtrl(txtOpc4FechaBackup, "DIS")
                End Select
                optOpciones(CInt(.tipo) - 1).Value = True       ' Selecciona expl�citamente la opci�n
                optOpciones_Click CInt(.tipo) - 1               ' Genera los eventos y carga de variables correspondientes
            End If
            '}
            '{ del -010- a.
            'optOpciones(CInt(.Tipo) - 1).Value = True       ' Selecciona expl�citamente la opci�n
            'optOpciones_Click CInt(.Tipo) - 1               ' Genera los eventos y carga de variables correspondientes
            '}
            .Item = 1
            '{ del -027- a.
            '.Masked = IIf(aplRST.Fields!sol_mask = "N", True, False)
            '.Emergencia = IIf(aplRST.Fields!sol_mask = "N", True, False)
            '}
            '{ add -027- a.
            .Masked = IIf(aplRST.Fields!sol_mask = "N", True, False)
            .Emergencia = IIf(aplRST.Fields!sol_eme = "N", False, True)
            '}
            .fecha = aplRST.Fields!SOL_FECHA
            .Recurso = glLOGIN_ID_REEMPLAZO
            .Peticion = aplRST.Fields!pet_nrointerno
            .CodigoJus = aplRST.Fields!jus_codigo
            .Justificacion = aplRST.Fields!sol_texto
            '{ add -010- a.
            If CInt(.tipo) > 4 Then
                Select Case .tipo
                    Case TIPOSOL_XCOMR
                        .ArchivoProd = ClearNull(aplRST.Fields!sol_file_prod)
                        .ArchivoDesa = ClearNull(aplRST.Fields!sol_file_desa)
                        '.ArchivoOut = ClearNull(aplRST.Fields!sol_file_prod)
                    Case TIPOSOL_UNLOR
                        .ArchivoProd = ClearNull(aplRST.Fields!sol_file_prod)
                        .ArchivoDesa = ClearNull(aplRST.Fields!sol_file_desa)
                        '.ArchivoOut = ClearNull(aplRST.Fields!sol_file_out)
                        .ArchivoIntermedio = ClearNull(aplRST.Fields!sol_file_inter)
                        .TablaBackup = ClearNull(aplRST.Fields!sol_bckuptabla)
                        .JobBackup = ClearNull(aplRST.Fields!sol_bckupjob)
                    Case TIPOSOL_SORTR
                        .ArchivoProd = ClearNull(aplRST.Fields!sol_file_prod)
                        .ArchivoDesa = ClearNull(aplRST.Fields!sol_file_desa)
                        '.ArchivoOut = ClearNull(aplRST.Fields!sol_file_out)
                        .ArchivoIntermedio = ClearNull(aplRST.Fields!sol_file_inter)
                End Select
            Else
            '}
                .ArchivoProd = ClearNull(aplRST.Fields!sol_file_prod)
                .ArchivoDesa = ClearNull(aplRST.Fields!sol_file_desa)
                '.ArchivoOut = ClearNull(aplRST.Fields!sol_file_out)
            End If
            .Lib_Sysin = ClearNull(aplRST.Fields!sol_LIB_SYSIN)
            .Mem_Sysin = ClearNull(aplRST.Fields!SOL_MEM_SYSIN)
            .Join = IIf(aplRST.Fields!SOL_JOIN = "S", True, False)
            .NroTabla = ClearNull(aplRST.Fields!SOL_NROTABLA)
            '{ add -020- a.
            .CopyBiblioteca = ClearNull(aplRST.Fields!sol_prod_cpybbl)
            .NameBiblioteca = ClearNull(aplRST.Fields!sol_prod_cpynom)
            .dsn_id = ClearNull(aplRST.Fields!dsn_id)
            '}
        End With
    End If
    If cModo = "EDICION" Then
        For i = 0 To optOpciones.Count - 1
            optOpciones(i).Enabled = False
        Next i
    End If
End Sub

Private Sub cmdWizard_Adelante_Click()
    Select Case StepId      ' Determino en que paso del proceso me encuento
        Case 1: If ValidarPaso(1) Then Call HabilitarWizard(2)
        Case 2: If ValidarPaso(2) Then Call HabilitarWizard(3)
        Case 3
            ' Determino si se ha cargado correctamente la justificaci�n pertinente
            If cmbJustificacion.ListIndex = -1 Then
                MsgBox "Debe seleccionar un motivo v�lido de justificaci�n para la solicitud a generar.", vbExclamation + vbOKOnly, "Advertencia"
                Exit Sub    ' add -004- b.
            End If
            '{ add -004- b.
            ' Valido que se cargue una ampliaci�n del motivo o justificativo de la solicitud si es con datos SIN ENMASCARAR
            If chkSINEnmascararDatos.Value = 1 Or chkEmergencia.Value = 1 Then    ' La solicitud se pide CON enmascaramiento       ' upd -021- a.
                If Len(txtJustificacion.Text) < 1 Then
                    MsgBox "Debe ingresar un detalle o ampliaci�n para generar la solicitud.", vbExclamation + vbOKOnly, "Solicitud sin enmascaramiento de datos"
                    Exit Sub
                End If
            End If
            '}
            Call UDT_Actualizar
            If ValidarPaso(3) Then Call HabilitarWizard(4)
    End Select
End Sub

Private Sub cmdWizard_Atras_Click()
    bFlagLoad = True
    Select Case StepId
        Case WZRD_PASO2_DATOS: Call HabilitarWizard(WZRD_PASO1_OPCIONES)
        Case WZRD_PASO3_JUSTIFICACION: Call HabilitarWizard(WZRD_PASO2_DATOS)
        Case WZRD_PASO4_CONFIRMACION: Call HabilitarWizard(WZRD_PASO3_JUSTIFICACION)
    End Select
    bFlagLoad = False
End Sub

Private Sub cmdWizard_Terminar_Click()
    '{ add -017- a.
    If Guardar Then
        frmCLISTShell.CargarGrilla
        Unload Me
    End If
    '}
    '{ del -017- a.
    'Guardar
    'frmCLISTShell.CargarGrilla
    'Unload Me
    '}
End Sub

Private Sub HabilitarWizard(WZRD_PASO As Byte)
    cmdWizard_Cancelar.visible = True
    cmdWizard_Atras.visible = True
    cmdWizard_Adelante.visible = True
    cmdWizard_Terminar.visible = True
    cmdAceptar.visible = False
    lblAyuda = ""
    Select Case WZRD_PASO
        Case WZRD_PASO0_INICIAR
            ' Paneles
            fraStep1.visible = False
            fraStep2.visible = False
            fraStep3.visible = False
            fraStep4.visible = False
            ' Botones
            cmdWizard_Cancelar.visible = False
            cmdWizard_Atras.visible = False
            cmdWizard_Adelante.visible = False
            cmdWizard_Terminar.visible = False
            cmdAceptar.visible = True
            cmdWizard_Cancelar.Enabled = False
            cmdWizard_Atras.Enabled = False
            cmdWizard_Adelante.Enabled = False
            cmdWizard_Terminar.Enabled = False
            cmdAceptar.Enabled = True
        Case WZRD_PASO1_OPCIONES
            fraStep1.visible = True
            fraStep2.visible = False
            fraStep3.visible = False
            fraStep4.visible = False
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = False
            cmdWizard_Adelante.Enabled = True
            cmdWizard_Terminar.Enabled = False
        Case WZRD_PASO2_DATOS
            With Solicitud
                Select Case iTipoSolicitud
                    Case TIPOSOL_XCOM
                        'If ClearNull(Solicitud.tipo) = "0" Then
                            With cmbOpc1FilePROD_Sel            ' Cargo las opciones iniciales de los combos
                                .Clear
                                If sp_GetSolicitudRecurso(glLOGIN_ID_REEMPLAZO, iTipoSolicitud, "sol_file_prod") Then
                                    Do While Not aplRST.EOF
                                        .AddItem Trim(ClearNull(aplRST.Fields!sol_file_prod))
                                        aplRST.MoveNext
                                        DoEvents
                                    Loop
                                End If
                            End With
                            '{ add -020- a.
                            If chkSINEnmascararDatos.Value = 1 Then
                                Call setHabilCtrl(txtOpc1FilePROD_BBLCPY, "NOR")
                                Call setHabilCtrl(txtOpc1FilePROD_NMECPY, "NOR")
                                Call setHabilCtrl(chk_CopyXCOM, "NOR")
                                txtOpc1FilePROD_BBLCPY.Text = "CMN.ABASE.CPY"
                                txtOpc1FilePROD_NMECPY.Text = ""
                                chk_CopyXCOM.Value = 0
                            Else
                                Call setHabilCtrl(txtOpc1FilePROD_BBLCPY, "DIS")
                                Call setHabilCtrl(txtOpc1FilePROD_NMECPY, "DIS")
                                Call setHabilCtrl(chk_CopyXCOM, "DIS")
                                txtOpc1FilePROD_BBLCPY.Text = ""
                                txtOpc1FilePROD_NMECPY.Text = ""
                                chk_CopyXCOM.Value = 1
                            End If
                            '}
                        'End If
                        If cModo = "EDICION" Then               ' Cargo los controles si estoy haciendo una copia
                            '{ add -008- b.
                            If bCopying Then
                                'chkSINEnmascararDatos.Value = IIf(Solicitud(cntSolicitudes).Masked, 1, 0)     ' add -008- a.
                                txtOpc1FilePROD.Text = .ArchivoProd
                                txtOpc1FileDESA.Text = .ArchivoDesa
                                txtOpc1FilePROD_BBLCPY.Text = .CopyBiblioteca
                                txtOpc1FilePROD_NMECPY.Text = .NameBiblioteca
                                If ClearNull(.NameBiblioteca) = "" Then chk_CopyXCOM.Value = 0
                                txtOpc1PeticionInterno.Text = .Peticion
                                '{ add -010- a.
                                If chkRestore.Value = 1 Then
                                    lblWizard(1).Caption = "1. Archivo en Backup"
                                    Call setHabilCtrl(txtOpc1FechaBackUp, "NOR")
                                Else
                                    lblWizard(1).Caption = "1. Archivo de Producci�n"
                                    Call setHabilCtrl(txtOpc1FechaBackUp, "DIS")
                                End If
                                '}
                                If sp_GetUnaPeticion(txtOpc1PeticionInterno.Text) Then
                                    txtOpc1Peticion.Text = ClearNull(aplRST.Fields!pet_nroasignado)
                                End If
                                chkEmergencia.Value = IIf(.Emergencia, 1, 0)       ' add -021- a.
                            End If
                            '}
                        '{ add -003- a.
                        Else
                            If Len(txtOpc1FileDESA.Text) < 1 Or Len(txtOpc1FileDESA.Text) = Len("ARBX.ENM." & glLOGIN_ID_REEMPLAZO) Then
                                txtOpc1FileDESA.Text = IIf(chkSINEnmascararDatos.Value = 0, "ARBP.ENM." & glLOGIN_ID_REEMPLAZO, "ARBZ.SEN." & glLOGIN_ID_REEMPLAZO)
                                txtOpc1FileDESA.Tag = txtOpc1FileDESA.Text
                            End If
                            '{ add -010- a.
                            If chkRestore.Value = 1 Then
                                lblWizard(1).Caption = "1. Archivo en Backup"
                                Call setHabilCtrl(txtOpc1FechaBackUp, "NOR")
                            Else
                                lblWizard(1).Caption = "1. Archivo de Producci�n"
                                Call setHabilCtrl(txtOpc1FechaBackUp, "DIS")
                            End If
                            '}
                        '}
                        End If
                        Call HabilitarWizard_SubStep(CByte(iTipoSolicitud))        ' Muestra las opciones correspondientes seg�n la opci�n seleccionada
                    Case TIPOSOL_UNLO
                        'If ClearNull(Solicitud.tipo) = "0" Then
                            '{ add -020- a.
                            If chkSINEnmascararDatos.Value = 1 Then
                                Call setHabilCtrl(txtOpc2FileOUT_BBLCPY, "NOR")
                                Call setHabilCtrl(txtOpc2FileOUT_NMECPY, "NOR")
                                Call setHabilCtrl(chk_CopyUNLO, "NOR")
                                txtOpc2FileOUT_BBLCPY.Text = "CMN.ABASE.CPY"
                                txtOpc2FileOUT_NMECPY.Text = ""
                                chk_CopyUNLO.Value = 0
                            Else
                                Call setHabilCtrl(txtOpc2FileOUT_BBLCPY, "DIS")
                                Call setHabilCtrl(txtOpc2FileOUT_NMECPY, "DIS")
                                Call setHabilCtrl(chk_CopyUNLO, "DIS")
                                txtOpc2FileOUT_BBLCPY.Text = ""
                                txtOpc2FileOUT_NMECPY.Text = ""
                                chk_CopyUNLO.Value = 1
                            End If
                            '}
                        'End If
                        With cmbOpc2LibSysin_Sel
                            .Clear
                            If sp_GetSolicitudRecurso(glLOGIN_ID_REEMPLAZO, iTipoSolicitud, "sol_lib_Sysin") Then
                                Do While Not aplRST.EOF
                                    .AddItem Trim(ClearNull(aplRST.Fields!sol_LIB_SYSIN))
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                        End With
                        With cmbOpc2MemSysin_Sel
                            .Clear
                            If sp_GetSolicitudRecurso(glLOGIN_ID_REEMPLAZO, iTipoSolicitud, "sol_mem_Sysin") Then
                                Do While Not aplRST.EOF
                                    .AddItem Trim(ClearNull(aplRST.Fields!SOL_MEM_SYSIN))
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                        End With
                        ' Cargo los controles si estoy haciendo una copia
                        If cModo = "EDICION" Then
                            '{ add -008- b.
                            If bCopying Then
                                chkEmergencia.Value = IIf(.Emergencia, 1, 0)            ' add -021- a.
                                'chkSINEnmascararDatos.Value = IIf(.Masked, 1, 0)        ' add -008- a.
                                txtOpc2FileOUT.Text = .ArchivoDesa
                                txtOpc2LibSysin.Text = .Lib_Sysin
                                txtOpc2MemSysin.Text = .Mem_Sysin
                                txtOpc2FileOUT_BBLCPY.Text = .CopyBiblioteca
                                txtOpc2FileOUT_NMECPY.Text = .NameBiblioteca
                                'If ClearNull(.NameBiblioteca) = "" Then chk_CopyUNLO.Value = 0
                                txtOpc2PeticionInterno.Text = .Peticion
                                chkOpc2Join.Value = IIf(.Join, "1", "0")
                                '{ add -022- a.
                                If chkRestore.Value = 1 Then
                                    lblWizard(37).Caption = "4. Tabla en Backup"
                                    Call setHabilCtrl(txtOpc2Tabla, "NOR")
                                    Call setHabilCtrl(txtOpc2JobBACKUP, "NOR")
                                    Call setHabilCtrl(txtOpc2FechaBackUp, "NOR")
                                Else
                                    lblWizard(37).Caption = "4. Tabla en Producci�n"
                                    Call setHabilCtrl(txtOpc2Tabla, "DIS")
                                    Call setHabilCtrl(txtOpc2JobBACKUP, "DIS")
                                    Call setHabilCtrl(txtOpc2FechaBackUp, "DIS")
                                End If
                                '}
                                If sp_GetUnaPeticion(txtOpc2PeticionInterno.Text) Then
                                    txtOpc2Peticion.Text = aplRST.Fields!pet_nroasignado
                                End If
                            End If
                            '}
                        Else
                            txtOpc2LibSysin.Text = "ARBD.BC.SYSIN"
                            If Len(txtOpc2FileOUT) < 1 Or Len(txtOpc2FileOUT) = Len("ARBX.ENM." & glLOGIN_ID_REEMPLAZO) Then
                                If chkSINEnmascararDatos.Value = 0 Then    ' La solicitud se pide CON enmascaramiento
                                    txtOpc2FileOUT.Text = "ARBP.ENM." & glLOGIN_ID_REEMPLAZO   ' add -003- a.
                                Else
                                    txtOpc2FileOUT.Text = "ARBZ.SEN." & glLOGIN_ID_REEMPLAZO   ' add -003- a.   ' upd -007- a.
                                End If
                            End If
                            '{ add -022- a.
                            If chkRestore.Value = 1 Then
                                lblWizard(37).Caption = "4. Tabla en Backup"
                                Call setHabilCtrl(txtOpc2Tabla, "NOR")
                                Call setHabilCtrl(txtOpc2FechaBackUp, "NOR")
                                Call setHabilCtrl(txtOpc2JobBACKUP, "NOR")
                            Else
                                lblWizard(37).Caption = "4. Tabla en Producci�n"
                                Call setHabilCtrl(txtOpc2Tabla, "DIS")
                                Call setHabilCtrl(txtOpc2FechaBackUp, "DIS")
                                Call setHabilCtrl(txtOpc2JobBACKUP, "DIS")
                            End If
                            '}
                        End If
                        Call HabilitarWizard_SubStep(CByte(iTipoSolicitud))        ' Muestra las opciones correspondientes seg�n la opci�n seleccionada
                    Case TIPOSOL_TCOR
                        ' Cargo las opciones iniciales de los combos
                        With cmbOpc3FileExtract_Sel
                            .Clear
                            If sp_GetSolicitudRecurso(glLOGIN_ID_REEMPLAZO, iTipoSolicitud, "sol_nrotabla") Then
                                Do While Not aplRST.EOF
                                    .AddItem Trim(ClearNull(aplRST.Fields!SOL_NROTABLA))
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                        End With
                        If ClearNull(Solicitud.tipo) = "0" Then
                            '{ add -020- a.
                            If chkSINEnmascararDatos.Value = 1 Then
                                Call setHabilCtrl(txtOpc3FileExtract_BBLCPY, "NOR")
                                Call setHabilCtrl(txtOpc3FileExtract_NMECPY, "NOR")
                                Call setHabilCtrl(chk_CopyTCOR, "NOR")
                                txtOpc3FileExtract_BBLCPY.Text = "CMN.ABASE.CPY"
                                txtOpc3FileExtract_NMECPY.Text = ""
                                chk_CopyTCOR.Value = 0
                            Else
                                Call setHabilCtrl(txtOpc3FileExtract_BBLCPY, "DIS")
                                Call setHabilCtrl(txtOpc3FileExtract_NMECPY, "DIS")
                                Call setHabilCtrl(chk_CopyTCOR, "DIS")
                                txtOpc3FileExtract_BBLCPY.Text = ""
                                txtOpc3FileExtract_NMECPY.Text = ""
                                chk_CopyTCOR.Value = 1
                            End If
                            '}
                        End If
                        ' Cargo los controles si estoy haciendo una copia
                        If cModo = "EDICION" Then
                            '{ add -008- b.
                            If bCopying Then
                                chkEmergencia.Value = IIf(.Emergencia, 1, 0)       ' add -021- a.
                                'chkSINEnmascararDatos.Value = IIf(.Masked, 1, 0)      ' add -008- a.
                                txtOpc3FileExtract.Text = .NroTabla
                                txtOpc3FileDESA.Text = .ArchivoDesa     '.ArchivoOut
                                txtOpc3FileExtract_BBLCPY = .CopyBiblioteca
                                txtOpc3FileExtract_NMECPY = .NameBiblioteca
                                If ClearNull(.NameBiblioteca) = "" Then chk_CopyTCOR.Value = 0
                                txtOpc3PeticionInterno.Text = .Peticion
                                If sp_GetUnaPeticion(txtOpc3PeticionInterno.Text) Then
                                    txtOpc3Peticion.Text = aplRST.Fields!pet_nroasignado
                                End If
                            End If
                            '}
                        '{ add -003- a.
                        Else
                            If Len(txtOpc3FileDESA.Text) < 1 Or Len(txtOpc3FileDESA) = Len("ARBX.ENM." & glLOGIN_ID_REEMPLAZO) Then
                                If chkSINEnmascararDatos.Value = 0 Then    ' La solicitud se pide CON enmascaramiento
                                    txtOpc3FileDESA.Text = "ARBP.ENM." & glLOGIN_ID_REEMPLAZO
                                Else
                                    txtOpc3FileDESA.Text = "ARBZ.SEN." & glLOGIN_ID_REEMPLAZO       ' upd -007- a.
                                End If
                            End If
                        '}
                        End If
                        Call HabilitarWizard_SubStep(CByte(iTipoSolicitud))  'optOpciones.Item + 1     ' Muestra las opciones correspondientes seg�n la opci�n seleccionada
                    Case TIPOSOL_SORT
                        ' Cargo las opciones iniciales de los combos
                        With cmbOpc4FilePROD_Sel
                            .Clear
                            If sp_GetSolicitudRecurso(glLOGIN_ID_REEMPLAZO, iTipoSolicitud, "sol_file_prod") Then
                                Do While Not aplRST.EOF
                                    .AddItem Trim(ClearNull(aplRST.Fields!sol_file_prod))
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                        End With
                        'If ClearNull(Solicitud.tipo) = "0" Then
                        '{ add -020- a.
                        If chkSINEnmascararDatos.Value = 1 Then
                            Call setHabilCtrl(txtOpc4FilePROD_BBLCPY, "NOR")
                            Call setHabilCtrl(txtOpc4FilePROD_NMECPY, "NOR")
                            Call setHabilCtrl(chk_CopySORT, "NOR")
                            txtOpc4FilePROD_BBLCPY.Text = "CMN.ABASE.CPY"
                            txtOpc4FilePROD_NMECPY.Text = ""
                            chk_CopySORT.Value = 0
                        Else
                            Call setHabilCtrl(txtOpc4FilePROD_BBLCPY, "DIS")
                            Call setHabilCtrl(txtOpc4FilePROD_NMECPY, "DIS")
                            Call setHabilCtrl(chk_CopySORT, "DIS")
                            txtOpc4FilePROD_BBLCPY.Text = ""
                            txtOpc4FilePROD_NMECPY.Text = ""
                            chk_CopySORT.Value = 1
                        End If
                        '}
                        'End If
                        With cmbOpc4LibSysin_Sel
                            .Clear
                            If sp_GetSolicitudRecurso(glLOGIN_ID_REEMPLAZO, iTipoSolicitud, "sol_lib_Sysin") Then
                                Do While Not aplRST.EOF
                                    .AddItem Trim(ClearNull(aplRST.Fields!sol_LIB_SYSIN))
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                        End With
                        With cmbOpc4MemSysin_Sel
                            .Clear
                            If sp_GetSolicitudRecurso(glLOGIN_ID_REEMPLAZO, iTipoSolicitud, "sol_mem_Sysin") Then
                                Do While Not aplRST.EOF
                                    .AddItem Trim(ClearNull(aplRST.Fields!SOL_MEM_SYSIN))
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                        End With
                        ' Cargo los controles si estoy haciendo una copia
                        If cModo = "EDICION" Then
                            '{ add -008- b.
                            If bCopying Then
                                If chkSINEnmascararDatos.Value = 0 Then    ' La solicitud se pide CON enmascaramiento
                                    txtOpc4FileDESA.Text = "ARBP.ENM." & glLOGIN_ID_REEMPLAZO   ' add -003- a.
                                Else
                                    txtOpc4FileDESA.Text = "ARBZ.SEN." & glLOGIN_ID_REEMPLAZO   ' add -003- a.  ' upd -007- a.
                                End If
                                txtOpc4FileDESA.Tag = txtOpc4FileDESA.Text
                                
                                chkEmergencia.Value = IIf(.Emergencia, 1, 0)       ' add -021- a.
                                'chkSINEnmascararDatos.Value = IIf(.Masked, 1, 0)      ' add -008- a.
                                txtOpc4FilePROD.Text = .ArchivoProd
                                txtOpc4FileDESA.Text = .ArchivoDesa     ' .ArchivoOut                ' upd -010- a. Antes Solicitud(cntSolicitudes).ArchivoDesa
                                txtOpc4LibSysin.Text = .Lib_Sysin
                                txtOpc4MemSysin.Text = .Mem_Sysin
                                txtOpc4FilePROD_BBLCPY.Text = .CopyBiblioteca
                                txtOpc4FilePROD_NMECPY.Text = .NameBiblioteca
                                'If ClearNull(.NameBiblioteca) = "" Then chk_CopySORT.Value = 0
                                txtOpc4PeticionInterno.Text = .Peticion
                                If sp_GetUnaPeticion(txtOpc4PeticionInterno.Text) Then
                                    txtOpc4Peticion.Text = aplRST.Fields!pet_nroasignado
                                End If
                            End If
                            '}
                        Else
                            txtOpc4LibSysin.Text = "ARBD.BC.SYSIN"
                            If Len(txtOpc4FileDESA.Text) < 1 Or Len(txtOpc4FileDESA) = Len("ARBX.ENM." & glLOGIN_ID_REEMPLAZO) Then
                                If chkSINEnmascararDatos.Value = 0 Then    ' La solicitud se pide CON enmascaramiento
                                    txtOpc4FileDESA.Text = "ARBP.ENM." & glLOGIN_ID_REEMPLAZO   ' add -003- a.
                                Else
                                    txtOpc4FileDESA.Text = "ARBZ.SEN." & glLOGIN_ID_REEMPLAZO   ' add -003- a.  ' upd -007- a.
                                End If
                                txtOpc4FileDESA.Tag = txtOpc4FileDESA.Text
                            End If
                            '{ add -010- a.
                            If chkRestore.Value = 1 Then
                                Call setHabilCtrl(txtOpc4FechaBackup, "NOR")
                            Else
                                Call setHabilCtrl(txtOpc4FechaBackup, "DIS")
                            End If
                            '}
                        End If
                        Call HabilitarWizard_SubStep(CByte(iTipoSolicitud))     ' Muestra las opciones correspondientes seg�n la opci�n seleccionada
                End Select
            End With
            ' Paneles
            fraStep1.visible = False
            fraStep2.visible = True
            fraStep3.visible = False
            fraStep4.visible = False
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = True
            cmdWizard_Adelante.Enabled = False      ' OJO!!!
            cmdWizard_Terminar.Enabled = False
            
            Call Validar_Siguiente
            
            If Not bFlagLoad Then                   ' Posicionamiento del foco al primer elemento de la carga
                If cModo = "AGREGAR" Then
                    If oLastControlUsed.visible Then oLastControlUsed.SetFocus       ' add -004- d.
                End If
            End If
        Case WZRD_PASO3_JUSTIFICACION
            'If ClearNull(Solicitud.tipo) = "0" Then
            If cmbJustificacion.ListCount = 0 Then
                If sp_GetJustificativos(Null, IIf(chkSINEnmascararDatos.Value = 0, "C", "S")) Then     ' Controles con datos
                    cmbJustificacion.Clear
                    Do While Not aplRST.EOF
                        cmbJustificacion.AddItem ClearNull(aplRST.Fields!jus_descripcion) & Space(100) & "||" & ClearNull(aplRST.Fields!jus_codigo)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    '{ add -004- c.
                    If chkSINEnmascararDatos.Value = 1 Or chkEmergencia.Value = 1 Then    ' La solicitud se pide SIN enmascaramiento       ' upd -021- a.
                        cmbJustificacion.ListIndex = -1
                        lblAyuda = "El motivo de justificaci�n es obligatorio. La ampliaci�n detallada del mismo para solicitudes SIN enmascaramiento de datos tambi�n."
                    Else
                        cmbJustificacion.ListIndex = 0
                        lblAyuda = "El motivo de justificaci�n es obligatorio. La ampliaci�n detallada del mismo es opcional."
                    End If
                    '}
                    'cmbJustificacion.ListIndex = 0     ' del -004- c.
                End If
            End If
            ' Paneles
            fraStep1.visible = False
            fraStep2.visible = False
            fraStep3.visible = True
            fraStep4.visible = False
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = True
            cmdWizard_Adelante.Enabled = True
            cmdWizard_Terminar.Enabled = False
            If cModo = "EDICION" Then
                cmbJustificacion.ListIndex = PosicionCombo(cmbJustificacion, Solicitud.CodigoJus, True)
                txtJustificacion.Text = Solicitud.Justificacion
            End If
            If cmbJustificacion.visible Then cmbJustificacion.SetFocus
        Case WZRD_PASO4_CONFIRMACION
            ' Paneles
            fraStep1.visible = False
            fraStep2.visible = False
            fraStep3.visible = False
            fraStep4.visible = True
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = True
            cmdWizard_Adelante.Enabled = False
            cmdWizard_Terminar.Enabled = True
            
            cmdWizard_Terminar.SetFocus
    End Select
End Sub

Private Sub HabilitarWizard_SubStep(Step As Byte)
    Dim i As Integer
    
    For i = 1 To fraStep2Step.Count
        If fraStep2Step(i).Index = Step Then
            fraStep2Step(i).visible = True
            fraStep2Step(i).Tag = "S"
        Else
            fraStep2Step(i).visible = False
            fraStep2Step(i).Tag = "N"
        End If
    Next i
End Sub

'Private Sub Cargar_Supervisores(oControl As Control)
'    oControl.Clear
'    If sp_GetRecursoPerfilArea(Null, "CSEC", "GERE", "DESA") Then
'        Do While Not aplRST.EOF
'            oControl.AddItem aplRST.Fields!cod_recurso & " : " & Trim(aplRST.Fields!nom_recurso)
'            aplRST.MoveNext
'            DoEvents
'        Loop
'        oControl.ListIndex = 0
'    End If
'End Sub

Private Function StepId() As Byte
    ' Esta funci�n devuelve el nro. de paso en el que se encuentra el usuario
    If fraStep1.visible Then
        StepId = 1
    Else
        If fraStep2.visible Then
            StepId = 2
        ElseIf fraStep3.visible Then
            StepId = 3
        Else
            StepId = 4
        End If
    End If
End Function

Private Sub UDT_Actualizar()
    With Solicitud
        .Item = 1
        .Emergencia = IIf(chkEmergencia.Value = 1, True, False)
        .Masked = IIf(chkSINEnmascararDatos.Value = 1, False, True)
        .Diferida = IIf(chkDiferido.Value = 1, True, False)
        .fecha = Now
        .Recurso = glLOGIN_ID_REEMPLAZO
        .CodigoJus = CodigoCombo(cmbJustificacion, True)
        .Justificacion = Trim(txtJustificacion)
        .dsn_id = sDSN_ID
        Select Case iTipoSolicitud
            Case TIPOSOL_XCOM
                .tipo = IIf(chkRestore.Value = 1, 5, 1)
                .Peticion = IIf(txtOpc1PeticionInterno.Text = "", 0, txtOpc1PeticionInterno.Text)
                If chkRestore.Value = 1 Then
                    .FechaBckUp = txtOpc1FechaBackUp.Text
                    .ArchivoProd = txtOpc1FilePROD
                    .ArchivoDesa = txtOpc1FileDESA
                    .ArchivoOut = ""
                    ' Cambiar ".ENM." por "ARBP.ENM." !!!
                    If InStr(1, txtOpc1FileDESA, ".ENM.", vbTextCompare) > 0 Then
                        .ArchivoIntermedio = Mid(txtOpc1FileDESA, 1, InStr(1, txtOpc1FileDESA, ".ENM.", vbTextCompare) - 1) & ".DYD." & Mid(txtOpc1FileDESA, InStr(1, txtOpc1FileDESA, ".ENM.", vbTextCompare) + 5, Len(txtOpc1FileDESA))
                    Else
                        .ArchivoIntermedio = ""
                    End If
                    .TablaBackup = ""
                    .JobBackup = ""
                Else
                    .FechaBckUp = FECHA_NULL
                    .ArchivoProd = txtOpc1FilePROD
                    .ArchivoDesa = txtOpc1FileDESA
                    .ArchivoOut = ""
                    .ArchivoIntermedio = ""
                    .TablaBackup = ""
                    .JobBackup = ""
                End If
                .Lib_Sysin = ""
                .Mem_Sysin = ""
                .Join = False
                .NroTabla = 0
                .CopyBiblioteca = ClearNull(txtOpc1FilePROD_BBLCPY)
                .NameBiblioteca = ClearNull(txtOpc1FilePROD_NMECPY)
            Case TIPOSOL_UNLO
                .tipo = IIf(chkRestore.Value = 1, 7, 2)
                .Peticion = IIf(txtOpc2PeticionInterno.Text = "", 0, txtOpc2PeticionInterno.Text)
                If chkRestore.Value = 1 Then
                    .FechaBckUp = txtOpc2FechaBackUp.Text
                    .TablaBackup = txtOpc2Tabla
                    .ArchivoProd = ""
                    .ArchivoDesa = txtOpc2FileOUT
                    .JobBackup = txtOpc2JobBACKUP
                    .ArchivoOut = ""
                    If InStr(1, txtOpc2FileOUT, ".ENM.", vbTextCompare) > 0 Then
                        .ArchivoIntermedio = Mid(txtOpc2FileOUT, 1, InStr(1, txtOpc2FileOUT, ".ENM.", vbTextCompare) - 1) & ".DYD." & Mid(txtOpc2FileOUT, InStr(1, txtOpc2FileOUT, ".ENM.", vbTextCompare) + 5, Len(txtOpc2FileOUT))
                    Else
                        .ArchivoIntermedio = ""
                    End If
                Else
                    .FechaBckUp = FECHA_NULL
                    .ArchivoProd = ""
                    .ArchivoDesa = txtOpc2FileOUT
                    .ArchivoOut = ""
                    .ArchivoIntermedio = ""
                    .TablaBackup = ""
                    .JobBackup = ""
                End If
                .Lib_Sysin = txtOpc2LibSysin
                .Mem_Sysin = txtOpc2MemSysin
                .Join = IIf(chkOpc2Join.Value = 1, True, False)
                .NroTabla = 0
                .CopyBiblioteca = ClearNull(txtOpc2FileOUT_BBLCPY)
                .NameBiblioteca = ClearNull(txtOpc2FileOUT_NMECPY)
            Case TIPOSOL_TCOR
                .tipo = 3
                .Peticion = IIf(txtOpc3PeticionInterno.Text = "", 0, txtOpc3PeticionInterno.Text)
                .ArchivoProd = ""
                .ArchivoDesa = txtOpc3FileDESA
                .ArchivoOut = ""
                .ArchivoIntermedio = ""
                .FechaBckUp = FECHA_NULL
                .TablaBackup = ""
                .JobBackup = ""
                .Lib_Sysin = ""
                .Mem_Sysin = ""
                .Join = False
                .NroTabla = txtOpc3FileExtract
                .CopyBiblioteca = ClearNull(txtOpc3FileExtract_BBLCPY)
                .NameBiblioteca = ClearNull(txtOpc3FileExtract_NMECPY)
            Case TIPOSOL_SORT
                .tipo = IIf(chkRestore.Value = 1, 6, 4)
                .Peticion = IIf(txtOpc4PeticionInterno.Text = "", 0, txtOpc4PeticionInterno.Text)
                If chkRestore.Value = 1 Then
                    .FechaBckUp = txtOpc4FechaBackup.Text
                    .ArchivoProd = txtOpc4FilePROD
                    .ArchivoDesa = txtOpc4FileDESA
                    .ArchivoOut = ""
                    If InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) > 0 Then
                        .ArchivoIntermedio = Mid(txtOpc4FileDESA, 1, InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) - 1) & ".DYD." & Mid(txtOpc4FileDESA, InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) + 5, Len(txtOpc4FileDESA))
                    Else
                        .ArchivoIntermedio = ""
                    End If
                Else
                    .FechaBckUp = FECHA_NULL
                    .ArchivoProd = txtOpc4FilePROD
                    .ArchivoDesa = txtOpc4FileDESA
                    .ArchivoOut = ""
                    .ArchivoIntermedio = ""
                End If
                .TablaBackup = ""
                .JobBackup = ""
                .Lib_Sysin = txtOpc4LibSysin
                .Mem_Sysin = txtOpc4MemSysin
                .Join = False
                .NroTabla = 0
                .CopyBiblioteca = ClearNull(txtOpc4FilePROD_BBLCPY)
                .NameBiblioteca = ClearNull(txtOpc4FilePROD_NMECPY)
        End Select
    End With
End Sub

Private Sub UDT_Inicializar()
    With Solicitud
        .tipo = 0
        .Item = 0
        .Emergencia = False
        .Masked = False
        .Diferida = False
        .fecha = CDate(vbNull)
        .Recurso = ""
        .CodigoJus = 0
        .Justificacion = ""
        .dsn_id = ""
        .Peticion = 0
        .FechaBckUp = CDate(vbNull)
        .ArchivoProd = ""
        .ArchivoOut = ""
        .ArchivoDesa = ""
        .ArchivoIntermedio = ""
        .TablaBackup = ""
        .JobBackup = ""
        .Lib_Sysin = ""
        .Mem_Sysin = ""
        .Join = False
        .NroTabla = 0
        .CopyBiblioteca = ""
        .NameBiblioteca = ""
    End With
End Sub

Private Function ValidarPaso(Step As Byte) As Boolean
    Dim i As Integer
    Dim cTexto As String
    
    ValidarPaso = False
    
    Select Case Step
        Case 1
            ValidarPaso = True
        Case 2
            '{ add -012- a.
            Call Validar_Siguiente
            If cmdWizard_Adelante.Enabled Then
                ValidarPaso = True
            End If
            '}
            'ValidarPaso = True     ' del -012- a.
        Case 3
            For i = 1 To fraStep2Step.Count ' - 1       ' Determina en que sub-paso se encuentra (?)
                If fraStep2Step(i).Tag = "S" Then
                    Exit For
                End If
            Next i
            
            With Solicitud
                Select Case fraStep2Step(i).Index
                    Case TIPOSOL_XCOM
                        cTexto = ""
                        txtScript.Text = ""
                        '{ add -010- a.
                        cTexto = cTexto & " TIPO        = " & TipoDescripcion(.tipo) & vbCrLf
                        cTexto = cTexto & " EMERGENCIA  = " & IIf(.Emergencia, "Si", "No") & vbCrLf
                        cTexto = cTexto & " ENMASCARA   = " & IIf(.Masked, "Si", "No") & vbCrLf
                        cTexto = cTexto & " DIFERIDA    = " & IIf(.Diferida, "Si", "No") & vbCrLf
                        If chkRestore.Value = 1 Then
                            cTexto = cTexto & " ARCHIVO IN  = " & .ArchivoProd & vbCrLf
                            cTexto = cTexto & " FECHA / BKP = " & .FechaBckUp & vbCrLf
                        Else
                            cTexto = cTexto & " ARCHIVO IN  = " & .ArchivoProd & vbCrLf
                        End If
                        '}
                        'cTexto = cTexto & " ARCHIVO IN  = " & Solicitud(i).ArchivoProd & vbCrLf    ' del -010- a.
                        If chk_CopyXCOM.Value <> 1 Then
                            cTexto = cTexto & " BIBLIO.COPY = " & .CopyBiblioteca & vbCrLf
                            cTexto = cTexto & " NOMBRE COPY = " & .NameBiblioteca & vbCrLf
                        End If
                        cTexto = cTexto & " ARCHIVO OUT = " & .ArchivoDesa & vbCrLf
                        cTexto = cTexto & " SOLICITANTE = " & .Recurso & " (" & sp_GetRecursoNombre(.Recurso) & ")" & vbCrLf
                        cTexto = cTexto & " " & Format(.fecha, "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
                        cTexto = cTexto & " ----------------------------------------------------"
                        cTexto = cTexto & vbCrLf
                        txtScript.Text = cTexto
                    Case TIPOSOL_UNLO
                        cTexto = ""
                        txtScript.Text = ""
                        cTexto = cTexto & " TIPO        = " & TipoDescripcion(.tipo) & vbCrLf
                        cTexto = cTexto & " EMERGENCIA  = " & IIf(.Emergencia, "Si", "No") & vbCrLf
                        cTexto = cTexto & " ENMASCARA   = " & IIf(.Masked, "Si", "No") & vbCrLf
                        cTexto = cTexto & " DIFERIDA    = " & IIf(.Diferida, "Si", "No") & vbCrLf
                        cTexto = cTexto & " ARCHIVO OUT = " & .ArchivoDesa & vbCrLf
                        '{ add -022- a.
                        If chkRestore.Value = 1 Then
                            'cTexto = cTexto & " ARCHIVO BKP = " & .ArchivoIntermedio & vbCrLf
                            cTexto = cTexto & " TABLA       = " & .TablaBackup & vbCrLf
                            cTexto = cTexto & " FECHA / BKP = " & .FechaBckUp & vbCrLf
                            cTexto = cTexto & " JOB   / BKP = " & .JobBackup & vbCrLf
                        End If
                        '}
                        If chk_CopyUNLO.Value <> 1 Then
                            cTexto = cTexto & " BIBLIO.COPY = " & .CopyBiblioteca & vbCrLf
                            cTexto = cTexto & " NOMBRE COPY = " & .NameBiblioteca & vbCrLf
                        End If
                        cTexto = cTexto & " LIB SYSIN   = " & .Lib_Sysin & vbCrLf
                        cTexto = cTexto & " SYSIN       = " & .Mem_Sysin & vbCrLf
                        cTexto = cTexto & " JOIN        = " & IIf(.Join, "S", "N") & vbCrLf
                        cTexto = cTexto & " SOLICITANTE = " & .Recurso & " (" & sp_GetRecursoNombre(.Recurso) & ")" & vbCrLf
                        cTexto = cTexto & " " & Format(.fecha, "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
                        cTexto = cTexto & " ----------------------------------------------------"
                        cTexto = cTexto & vbCrLf
                        txtScript.Text = cTexto
                    Case TIPOSOL_TCOR
                        cTexto = ""
                        txtScript.Text = ""
                        cTexto = cTexto & " TIPO        = " & TipoDescripcion(.tipo) & vbCrLf
                        cTexto = cTexto & " EMERGENCIA  = " & IIf(.Emergencia, "Si", "No") & vbCrLf
                        cTexto = cTexto & " ENMASCARA   = " & IIf(.Masked, "Si", "No") & vbCrLf
                        cTexto = cTexto & " DIFERIDA    = " & IIf(.Diferida, "Si", "No") & vbCrLf
                        cTexto = cTexto & " INPUT SORT  = " & Left(.NroTabla, 4) & vbCrLf
                        If chk_CopyTCOR.Value <> 1 Then
                            cTexto = cTexto & " BIBLIO.COPY = " & .CopyBiblioteca & vbCrLf
                            cTexto = cTexto & " NOMBRE COPY = " & .NameBiblioteca & vbCrLf
                        End If
                        cTexto = cTexto & " ARCHIVO OUT = " & .ArchivoDesa & vbCrLf
                        cTexto = cTexto & " SOLICITANTE = " & .Recurso & " (" & sp_GetRecursoNombre(.Recurso) & ")" & vbCrLf
                        cTexto = cTexto & " " & Format(.fecha, "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
                        cTexto = cTexto & " ----------------------------------------------------"
                        cTexto = cTexto & vbCrLf
                        txtScript.Text = cTexto
                    Case TIPOSOL_SORT
                        cTexto = ""
                        txtScript.Text = ""
                        cTexto = cTexto & " TIPO        = " & TipoDescripcion(.tipo) & vbCrLf
                        cTexto = cTexto & " EMERGENCIA  = " & IIf(.Emergencia, "Si", "No") & vbCrLf
                        cTexto = cTexto & " ENMASCARA   = " & IIf(.Masked, "Si", "No") & vbCrLf
                        cTexto = cTexto & " DIFERIDA    = " & IIf(.Diferida, "Si", "No") & vbCrLf
                        '{ add -010- a.
                        If chkRestore.Value = 1 Then
                            cTexto = cTexto & " FECHA / BKP = " & .FechaBckUp & vbCrLf
                            'cTexto = cTexto & " ARCHIVO INT = " & .ArchivoDesa & vbCrLf
                        End If
                        '}
                        cTexto = cTexto & " ARCHIVO INP = " & .ArchivoProd & vbCrLf
                        If chk_CopySORT.Value <> 1 Then
                            cTexto = cTexto & " BIBLIO.COPY = " & .CopyBiblioteca & vbCrLf
                            cTexto = cTexto & " NOMBRE COPY = " & .NameBiblioteca & vbCrLf
                        End If
                        cTexto = cTexto & " ARCHIVO OUT = " & .ArchivoDesa & vbCrLf
                        cTexto = cTexto & " SYSIN       = " & .Mem_Sysin & vbCrLf
                        cTexto = cTexto & " SOLICITANTE = " & .Recurso & " (" & sp_GetRecursoNombre(.Recurso) & ")" & vbCrLf
                        cTexto = cTexto & " " & Format(.fecha, "dd / mm / yy - hh:mm:ss") & " -" & vbCrLf
                        cTexto = cTexto & " ----------------------------------------------------"
                        cTexto = cTexto & vbCrLf
                        txtScript.Text = cTexto
                End Select
            End With
            ValidarPaso = True
    End Select
End Function

Private Function TipoDescripcion(tipo As Byte) As String
    Select Case tipo
        Case 1: TipoDescripcion = "XCOM"
        Case 2: TipoDescripcion = "UNLO"
        Case 3: TipoDescripcion = "TCOR"
        Case 4: TipoDescripcion = "SORT"
        Case 5: TipoDescripcion = "XCOM con RESTORE"
        Case 6: TipoDescripcion = "SORT con RESTORE"
        Case 7: TipoDescripcion = "UNLO con RESTORE"
    End Select
End Function

Private Sub optOpciones_Click(Index As Integer)
    Select Case Index
        Case 0  ' XCOM
            cTipoSolicitud = "XCOM"
            iTipoSolicitud = 1
            chkRestore.Enabled = True: chkRestore.Value = 0     ' add -009- a.
            Set oLastControlUsed = txtOpc1FilePROD              ' add -004- d.
        Case 1  ' UNLO
            cTipoSolicitud = "UNLO"
            iTipoSolicitud = 2
            'chkRestore.Enabled = False: chkRestore.Value = 0    ' add -009- a.
            chkRestore.Enabled = True: chkRestore.Value = 0    ' add -009- a.
            Set oLastControlUsed = txtOpc2FileOUT               ' add -004- d.
        Case 2  ' TCOR
            cTipoSolicitud = "TCOR"
            iTipoSolicitud = 3
            chkRestore.Enabled = False: chkRestore.Value = 0    ' add -009- a.
            Set oLastControlUsed = txtOpc3FileExtract           ' add -004- d.
        Case 3  ' SORT
            cTipoSolicitud = "SORT"
            iTipoSolicitud = 4
            chkRestore.Enabled = True: chkRestore.Value = 0     ' add -009- a.
            Set oLastControlUsed = txtOpc4FilePROD              ' add -004- d.
    End Select
End Sub

Private Sub Validar_Siguiente()
    cmdWizard_Adelante.Enabled = False
    
    Select Case iTipoSolicitud
        Case TIPOSOL_XCOM
            If Validar_Archivo_PROD(1, txtOpc1FilePROD) Then
                If Validar_BibliotecaCopy(1, txtOpc1FilePROD_BBLCPY, chk_CopyXCOM) Then
                    If Validar_NombreCopy(1, txtOpc1FilePROD_NMECPY, chk_CopyXCOM) Then
                        If Validar_Backup(1, txtOpc1FechaBackUp) Then                           ' add -010- a.
                            If Len(txtOpc1FileDESA) > 0 Then
                                If Validar_Archivo_DESA(1, txtOpc1FileDESA) Then
                                    If Validar_AMBOS(txtOpc1FilePROD, txtOpc1FileDESA) Then     ' add -009- c.
                                        'If Validar_Supervisor(1, cmbOpc1Supervisor) Then
                                            If Validar_Peticion(TIPOSOL_XCOM) Then
                                                cmdWizard_Adelante.Enabled = True
                                            End If
                                        'End If
                                    End If                                                      ' add -009- c.
                                End If
                            End If
                        End If                                                                  ' add -010- a.
                    End If
                End If
            End If
        Case TIPOSOL_UNLO
            'If Validar_Archivo_OUT(2, txtOpc2FileOUT) Then
            If Validar_Archivo_DESA(2, txtOpc2FileOUT) Then
                If Validar_BibliotecaCopy(2, txtOpc2FileOUT_BBLCPY, chk_CopyUNLO) Then
                    If Validar_NombreCopy(2, txtOpc2FileOUT_NMECPY, chk_CopyUNLO) Then
                        If Len(txtOpc2LibSysin) > 0 Then
                            If Validar_Archivo_LIB(2, txtOpc2LibSysin) Then
                                If Len(txtOpc2MemSysin) > 0 Then
                                    If Validar_Archivo_MEM(2, txtOpc2MemSysin) Then
                                        If Validar_Backup(2, txtOpc2FechaBackUp) Then                           ' add -022- a.
                                            'If Validar_Supervisor(2, cmbOpc2Supervisor) Then
                                                If Validar_Peticion(2) Then
                                                    cmdWizard_Adelante.Enabled = True
                                                End If
                                            'End If
                                        End If          ' add -022- a.
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Case TIPOSOL_TCOR
            If Len(txtOpc3FileExtract) > 0 Then
                If Validar_BibliotecaCopy(3, txtOpc3FileExtract_BBLCPY, chk_CopyTCOR) Then
                    If Validar_NombreCopy(3, txtOpc3FileExtract_NMECPY, chk_CopyTCOR) Then
                        If Validar_NroTabla(3, txtOpc3FileExtract) Then
                            If Len(txtOpc3FileDESA) > 0 Then
                                'If Validar_Archivo_OUT(3, txtOpc3FileDESA) Then
                                If Validar_Archivo_DESA(3, txtOpc3FileDESA) Then
                                    'If Validar_Supervisor(3, cmbOpc3Supervisor) Then
                                        If Validar_Peticion(3) Then
                                            cmdWizard_Adelante.Enabled = True
                                        End If
                                    'End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Case TIPOSOL_SORT
            If Validar_Archivo_PROD(4, txtOpc4FilePROD) Then
                If Validar_BibliotecaCopy(4, txtOpc4FilePROD_BBLCPY, chk_CopySORT) Then
                    If Validar_NombreCopy(4, txtOpc4FilePROD_NMECPY, chk_CopySORT) Then
                        'If Validar_INTERMEDIO(4, txtOpc4FileTMP) Then                          ' del -026- a.
                            If Validar_Backup(4, txtOpc4FechaBackup) Then                       ' add -010- a.
                                If Validar_Archivo_DESA(4, txtOpc4FileDESA) Then
                                    If Validar_AMBOS(txtOpc4FilePROD, txtOpc4FileDESA) Then     ' add -009- c.
                                        If Len(txtOpc4LibSysin) > 0 Then
                                            If Validar_Archivo_LIB(4, txtOpc4LibSysin) Then
                                                If Len(txtOpc4MemSysin) > 0 Then
                                                    If Validar_Archivo_MEM(4, txtOpc4MemSysin) Then
                                                        'If Validar_Supervisor(4, cmbOpc4Supervisor) Then
                                                            If Validar_Peticion(4) Then
                                                                cmdWizard_Adelante.Enabled = True
                                                            End If
                                                        'End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If                                                      ' add -009- c.
                                End If
                            End If                                                              ' add -010- a.
                        'End If                                                                 ' del -026- a.
                    End If
                End If
            End If
    End Select
End Sub

Private Function Guardar() As Boolean     ' upd -017- a. Se cambia de proceso a funci�n booleana
    Dim sol_nroasignado As Long
    Dim cArchivoSalida As String
    Dim cHora As String
    Dim cMensaje As String
    
    Guardar = False       ' add -017- a.
    Call Puntero(True)
    
    With Solicitud
        '{ add -017- a.
        Call Status("Realizando validaciones... aguarde...")
        cArchivoSalida = .ArchivoDesa
        If bControlDuplicidad Then
            If sp_GetSolicitudValida(cArchivoSalida) Then
                '{ add -023- a.
                cMensaje = "El archivo de salida " & Trim(cArchivoSalida) & " ha sido solicitado en estos momentos." & vbCrLf & _
                           "Recuerde que los horarios de envio de solicitudes a Carga de M�quina son de Lunes a Viernes de 8 a 18 (cada hora)" & vbCrLf & vbCrLf & _
                           "Puede cambiar el nombre del archivo de salida o volver a generar la solicitud."
                '}
                MsgBox cMensaje, vbExclamation + vbOKOnly, "Nombre de archivo de salida repetido"
                Call Puntero(False)
                Call Status("Listo.")
                Exit Function
            End If
        End If
        '}
        '.Lider, .Supervisor, .SI
        Call Status("Guardando solicitud... aguarde...")
        If Not sp_InsertSolicitud(IIf(.Diferida, "S", "N"), .tipo, IIf(.Masked, "S", "N"), _
                                  .Recurso, .Peticion, .CodigoJus, .Justificacion, Null, Null, .FechaBckUp, _
                                  .ArchivoProd, .ArchivoDesa, .ArchivoOut, _
                                  .Lib_Sysin, .Mem_Sysin, IIf(.Join, "S", "N"), _
                                  .NroTabla, .CopyBiblioteca, .NameBiblioteca, _
                                  .dsn_id, .Emergencia, .ArchivoIntermedio, _
                                  .TablaBackup, .JobBackup, sol_nroasignado) Then
            MsgBox "Error al querer ingresar solicitud.", vbCritical + vbOKOnly
            '{ add -017- a.
            Call Puntero(False)
            Call Status("Listo.")
            Exit Function
            '}
        Else
            Call sp_UpdateHEDT001Field(.dsn_id, "suma_dsn_cantuso", Null, Null, 1)
        End If
        
        '{ add -003- c. - Solo enviamos el correo cuando se piden datos SIN enmascarar en un RESTORE
        If chkEmergencia.Value = 1 Then
            If glUsrPerfilActual <> "CSEC" Then Call EnviarCorreo_Recordatorio(sol_nroasignado, False)          ' add -021- b.
            Call EnviarCorreo_EMERGENCIA(sol_nroasignado, True)
            Call Puntero(False)
        Else
            If chkSINEnmascararDatos.Value = 0 Then         ' CON enmascaramiento
                If chkRestore.Value = 1 Then
                    Call EnviarCorreo_Restore(sol_nroasignado, False)
                    Call Puntero(False)
                    MsgBox "Email(s) enviado(s).", vbInformation
                End If
            '{ add -021- b. SIN ENMASCARAMIENTO
            Else
                If chkRestore.Value = 1 Then
                    If glUsrPerfilActual <> "CSEC" Then
                        Call EnviarCorreo_Recordatorio(sol_nroasignado, False)
                        Call Puntero(False)
                        MsgBox "Email(s) enviados(s) al autorizante.", vbInformation
                    End If
                Else
                    If glUsrPerfilActual <> "CSEC" Then
                        Call EnviarCorreo_Recordatorio(sol_nroasignado, False)
                        Call Puntero(False)
                        MsgBox "Email(s) enviados(s) al autorizante.", vbInformation
                    End If
                End If
            '}
            End If
        End If
        '}
    End With
    
    Call Puntero(False)
    Call Status("Listo.")
    Guardar = True        ' add -017- a.
End Function

Private Sub cmdWizard_Cancelar_Click()
    If MsgBox("�Cancelar la generaci�n de la solicitud?", vbQuestion + vbYesNo, "Cancelar") = vbYes Then
        Unload Me
    End If
End Sub

'{ del -023- a.
'If CDate(Format(Now, "hh:mm:ss")) >= CDate(Format(CDate("17:30:00"), "hh:mm:ss")) Then      'Armo el mensaje al usuario
'    cHora = IIf(Weekday(Now, vbSunday) = 6, "08:00 hs. del Lunes.", "08:00 hs. de ma�ana.")
'ElseIf CDate(Format(Now, "hh:mm:ss")) >= CDate(Format(CDate("16:00:00"), "hh:mm:ss")) Then
'    cHora = "17:30 hs. de hoy."
'ElseIf CDate(Format(Now, "hh:mm:ss")) >= CDate(Format(CDate("14:00:00"), "hh:mm:ss")) Then
'    cHora = "16:00 hs. de hoy."
'ElseIf CDate(Format(Now, "hh:mm:ss")) >= CDate(Format(CDate("12:00:00"), "hh:mm:ss")) Then
'    cHora = "14:00 hs. de hoy."
'ElseIf CDate(Format(Now, "hh:mm:ss")) >= CDate(Format(CDate("10:00:00"), "hh:mm:ss")) Then
'    cHora = "12:00 hs. de hoy."
'ElseIf CDate(Format(Now, "hh:mm:ss")) >= CDate(Format(CDate("08:00:00"), "hh:mm:ss")) Then
'    cHora = "10:00 hs. de hoy."
'Else
'    cHora = "08:00 hs. de hoy."
'End If
'cMensaje = "El archivo de salida " & Trim(cArchivoSalida) & " ha sido solicitado en estos momentos." & vbCrLf & _
'           "Recuerde que los horarios de envio de solicitudes a Carga de M�quina son de Lunes a Viernes:" & vbCrLf & vbCrLf & _
'           "08:00 hs." & vbCrLf & _
'           "10:00 hs." & vbCrLf & _
'           "12:00 hs." & vbCrLf & _
'           "14:00 hs." & vbCrLf & _
'           "16:00 hs." & vbCrLf & _
'           "17:30 hs." & vbCrLf & vbCrLf & _
'           "Puede cambiar el nombre del archivo de salida o volver a generar la solicitud antes de las " & cHora
'}

' ================================================================================================================================================
' ************************************************************************************************************************************************
' ================================================================================================================================================
'
'                               ***   V A L I D A C I O N E S   S O B R E   L O S   C A M P O S ***
'
' ================================================================================================================================================
' ************************************************************************************************************************************************
' ================================================================================================================================================


' ============================================================================================================================================
' *. COMUNES
' ============================================================================================================================================
Private Function Validar_Archivo_PROD(Numero As Byte, oControl As Control) As Boolean
    Validar_Archivo_PROD = False

    ' 1.1.1. Longitud m�nima
    If Len(Trim(oControl)) > 0 Then
        If Len(Trim(oControl)) < 5 Then
            MensajeError Numero, False, "1. El nombre del archivo debe tener al menos una longitud de 5 caracteres."
            Exit Function
        Else
            MensajeError Numero, True
        End If
    Else
        MensajeError Numero, False, ""
        Exit Function
    End If
    
    ' 1.1.2. El nombre del archivo no puede comenzar y/o finalizar con un punto
    If Mid(oControl, 1, 1) = "." Then
        MensajeError Numero, False, "1. El nombre del archivo no puede comenzar con un caracter de calificador."
        Exit Function
    Else
        MensajeError Numero, True
    End If
    
    If Mid(oControl, Len(oControl), 1) = "." Then
        MensajeError Numero, False, "1. El nombre del archivo no puede terminar con un caracter de calificador."
        Exit Function
    Else
        MensajeError Numero, True
    End If
    
    ' Longitud del nombre del archivo
    If Len(Trim(oControl)) > 44 Then
        MensajeError Numero, False, "1. El nombre del archivo no puede superar la longitud de 44 caracteres."
        Exit Function
    Else
        MensajeError Numero, True
    End If
    
    ' Combinaciones prohibidas
    ' 27.10.2014
    ' Se actualiza este control: a todos los c�digos se los delimita con un punto (antes y despu�s).
    ' Ej.: VM01 pasa a ser .VM01.
    If InStr(1, oControl, ".VM01.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".VM03.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".VA01.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".VA12.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".VD07.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".RM01.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".RM03.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".RA01.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".RA12.", vbTextCompare) > 0 Or _
       InStr(1, oControl, ".RD07.", vbTextCompare) > 0 Then   ' upd -001- a. Se agregaron las restricciones RM01, RM03, RA01, RA12 y RD07.
            MensajeError Numero, False, "1. El archivo no puede ser un BACKUP (VM01, VM03, VA01, VA12, VD07, RM01, RM03, RA01, RA12, RD07)."    ' upd -001- a.
            Exit Function
    Else
        MensajeError Numero, True
    End If
    
    ' Expresi�n %%ODATE
    If InStr(1, oControl, "%%ODATE", vbTextCompare) > 0 Then
        MensajeError Numero, False, "1. El archivo no puede contener dentro del nombre la expresi�n %%ODATE."
        Exit Function
    Else
        MensajeError Numero, True
    End If
    
    If Validar_CaracteresValidos(Numero, oControl, "ENTRADA") Then
        If Validar_Calificadores(Numero, oControl, "ENTRADA") Then
            Validar_Archivo_PROD = True
        End If
    End If
    
    '{ add 01.10.2012 - Carga DSN correspondientes al archivo de producci�n
    If chkSINEnmascararDatos.Value = 0 Then
        Call Puntero(True)
        Select Case Numero
            Case TIPOSOL_XCOM, TIPOSOL_XCOMR
                If txtOpc1FilePROD.Tag <> oControl Then
                    txtOpc1FilePROD.Tag = oControl
                    If sp_GetHEDT101d(oControl) Then
                        With cmbOpc1FileDESA_Sel
                            .Clear
                            Do While Not aplRST.EOF
                                .AddItem txtOpc1FileDESA.Tag & "." & ClearNull(aplRST.Fields!dsn_nom)
                                aplRST.MoveNext
                                DoEvents
                            Loop
                        End With
                    Else
                        cmbOpc1FileDESA_Sel.Clear
                    End If
                    txtOpc1FileDESA_KeyDown vbKeyF4, 0
                End If
            Case TIPOSOL_SORT, TIPOSOL_SORTR:
                If txtOpc4FilePROD.Tag <> oControl Then
                    txtOpc4FilePROD.Tag = oControl
                    If sp_GetHEDT101d(oControl) Then
                        With cmbOpc4FileDESA_Sel
                            .Clear
                            Do While Not aplRST.EOF
                                .AddItem txtOpc4FileDESA.Tag & "." & ClearNull(aplRST.Fields!dsn_nom)
                                aplRST.MoveNext
                                DoEvents
                            Loop
                        End With
                    Else
                        cmbOpc4FileDESA_Sel.Clear
                    End If
                    'txtOpc4FileDESA_KeyDown vbKeyF4, 0
                End If
        End Select
        Call Puntero(False)
    End If
    '}
End Function

'{ add -010- a.
Private Function Validar_Backup(Numero As Byte, oControl As Control) As Boolean
    Validar_Backup = False
    
    If chkRestore.Value = 1 Then
        If Len(oControl.Text) > 0 Then
            Select Case Numero
                Case 1
                    If Not IsDate(oControl.DateValue) Then
                        MensajeError Numero, False, "2. Debe especificar una fecha v�lida."
                        Exit Function
                    Else
                        If oControl.DateValue > date Then
                            MensajeError Numero, False, "2. Debe especificar una fecha menor o igual a la fecha del d�a."
                            Exit Function
                        Else
                            Validar_Backup = True
                        End If
                    End If
                '{ add -022- a.
                Case 2
                    If Not IsDate(oControl.DateValue) Then
                        MensajeError Numero, False, "5. Debe especificar una fecha v�lida."
                        Exit Function
                    Else
                        If oControl.DateValue > date Then
                            MensajeError Numero, False, "5. Debe especificar una fecha menor o igual a la fecha del d�a."
                            Exit Function
                        Else
                            If ClearNull(txtOpc2JobBACKUP) = "" Then
                                MensajeError Numero, False, "6. Debe especificar el JOB de backup."
                                Exit Function
                            Else
                                Validar_Backup = True
                            End If
                        End If
                    End If
                '}
                Case 4
                    If Not IsDate(oControl.DateValue) Then
                        MensajeError Numero, False, "3. Debe especificar una fecha v�lida."
                        Exit Function
                    Else
                        If oControl.DateValue > date Then
                            MensajeError Numero, False, "3. Debe especificar una fecha menor o igual a la fecha del d�a."
                            Exit Function
                        Else
                            Validar_Backup = True
                        End If
                    End If
            End Select
        Else
            Select Case Numero
                Case 2: MensajeError Numero, False, "5. Debe especificar una fecha v�lida."
                Case Else
                    MensajeError Numero, False, "2. Debe especificar una fecha v�lida."
            End Select
            Exit Function
        End If
    Else
        Validar_Backup = True
    End If
End Function

Private Function Validar_INTERMEDIO(Numero As Byte, oControl As Control) As Boolean
    Validar_INTERMEDIO = False
    
    If chkRestore.Value = 1 Then
        ' 1.1.1. Longitud m�nima
        If Len(Trim(oControl)) > 0 Then
            If Len(Trim(oControl)) < 5 Then
                MensajeError Numero, False, "2. El nombre del archivo debe tener al menos una longitud de 5 caracteres."
                Exit Function
            Else
                MensajeError Numero, True
            End If
        Else
            MensajeError Numero, False, ""
            Exit Function
        End If
        
        ' 1.1.2. El nombre del archivo no puede comenzar y/o finalizar con un punto
        If Mid(oControl, 1, 1) = "." Then
            MensajeError Numero, False, "2. El nombre del archivo no puede comenzar con un caracter de calificador."
            Exit Function
        Else
            MensajeError Numero, True
        End If
        
        If Mid(oControl, Len(oControl), 1) = "." Then
            MensajeError Numero, False, "2. El nombre del archivo no puede terminar con un caracter de calificador."
            Exit Function
        Else
            MensajeError Numero, True
        End If
        
        ' Longitud del nombre del archivo
        If Len(Trim(oControl)) > 44 Then
            MensajeError Numero, False, "2. El nombre del archivo no puede superar la longitud de 44 caracteres."
            Exit Function
        Else
            MensajeError Numero, True
        End If
        
        ' Combinaciones prohibidas
        If InStr(1, oControl, "VM01", vbTextCompare) > 0 Or _
           InStr(1, oControl, "VM03", vbTextCompare) > 0 Or _
           InStr(1, oControl, "VA01", vbTextCompare) > 0 Or _
           InStr(1, oControl, "VA12", vbTextCompare) > 0 Or _
           InStr(1, oControl, "VD07", vbTextCompare) > 0 Or _
           InStr(1, oControl, "RM01", vbTextCompare) > 0 Or _
           InStr(1, oControl, "RM03", vbTextCompare) > 0 Or _
           InStr(1, oControl, "RA01", vbTextCompare) > 0 Or _
           InStr(1, oControl, "RA12", vbTextCompare) > 0 Or _
           InStr(1, oControl, "RD07", vbTextCompare) > 0 Then
            MensajeError Numero, False, "2. El archivo no puede ser un BACKUP (VM01, VM03, VA01, VA12, VD07, RM01, RM03, RA01, RA12, RD07)."
            Exit Function
        Else
            MensajeError Numero, True
        End If
        
        ' Expresi�n %%ODATE
        If InStr(1, oControl, "%%ODATE", vbTextCompare) > 0 Then
            MensajeError Numero, False, "2. El archivo no puede contener dentro del nombre la expresi�n %%ODATE."
            Exit Function
        Else
            MensajeError Numero, True
        End If
        
        If Validar_CaracteresValidos(Numero, oControl, "INTERMEDIO") Then
            If Validar_Calificadores(Numero, oControl, "INTERMEDIO") Then
                Validar_INTERMEDIO = True
            End If
        End If
    Else
        Validar_INTERMEDIO = True
    End If
End Function
'}

Private Function Validar_Archivo_DESA(Numero As Byte, oControl As Control) As Boolean
    '{ add -002- a.
    Dim cArchivoNombre As String
    Dim cReemplazoStr As String
    '}
    
    Validar_Archivo_DESA = False
    
    ' Longitud cero
    If Len(Trim(oControl)) < 1 Then
        MensajeError Numero, True
        Exit Function
    End If
    
    ' 1.1.2. El nombre del archivo no puede comenzar y/o finalizar con un punto
    If Mid(oControl, 1, 1) = "." Then
        'MensajeError Numero, False, "2. El nombre del archivo no puede comenzar con un caracter de calificador."   ' del -010- a.
        '{ add -010- a.
        If Numero = 1 Then
            MensajeError Numero, False, "3. El nombre del archivo no puede comenzar con un caracter de calificador."
        ElseIf Numero = 4 Then
            MensajeError Numero, False, "4. El nombre del archivo no puede comenzar con un caracter de calificador."
        Else
            MensajeError Numero, False, "2. El nombre del archivo no puede comenzar con un caracter de calificador."
        End If
        '}
        Exit Function
    Else
        MensajeError Numero, True
    End If
    
    If Mid(oControl, Len(oControl), 1) = "." Then
        'MensajeError Numero, False, "2. El nombre del archivo no puede terminar con un caracter de calificador."   ' del -010- a.
        '{ add -010- a.
        If Numero = 1 Then
            MensajeError Numero, False, "3. El nombre del archivo no puede terminar con un caracter de calificador."
        ElseIf Numero = 4 Then
            MensajeError Numero, False, "4. El nombre del archivo no puede terminar con un caracter de calificador."
        Else
            MensajeError Numero, False, "2. El nombre del archivo no puede terminar con un caracter de calificador."
        End If
        '}
        Exit Function
    Else
        MensajeError Numero, True
    End If
    
    ' Longitud del nombre del archivo
    If Len(Trim(oControl)) > 44 Then
        'MensajeError Numero, False, "2. El nombre del archivo no puede superar la longitud de 44 caracteres."  ' del -010- a.
        '{ add -010- a.
        If Numero = 1 Then
            MensajeError Numero, False, "3. El nombre del archivo no puede superar la longitud de 44 caracteres."
        ElseIf Numero = 4 Then
            MensajeError Numero, False, "4. El nombre del archivo no puede superar la longitud de 44 caracteres."
        Else
            MensajeError Numero, False, "2. El nombre del archivo no puede superar la longitud de 44 caracteres."
        End If
        '}
        Exit Function
    Else
        MensajeError Numero, True
    End If
    
    '{ add -009- b.
    ' Combinaciones prohibidas
    If InStr(1, oControl, "VSM", vbTextCompare) > 0 Then
        'MensajeError Numero, False, "2. El archivo no puede contener VSM en ninguna parte de su nombre."   ' del -010- a.
        '{ add -010- a.
        If Numero = 1 Then
            MensajeError Numero, False, "3. El archivo no puede contener VSM en ninguna parte de su nombre."
        ElseIf Numero = 4 Then
            MensajeError Numero, False, "4. El archivo no puede contener VSM en ninguna parte de su nombre."
        Else
            MensajeError Numero, False, "2. El archivo no puede contener VSM en ninguna parte de su nombre."
        End If
        '}
        Exit Function
    Else
        MensajeError Numero, True
    End If
    '}
    
    sDSN_ID = ""
    cReemplazoStr = IIf(bDatosSensibles, "ARBP.ENM.", "ARBZ.ENM.") & glLOGIN_ID_REEMPLAZO & "."
    cArchivoNombre = Mid(oControl, Len(cReemplazoStr) + 1)
    'cArchivoNombre = Mid(oControl, InStr(1, oControl, cReemplazoStr, vbTextCompare) + Len(cReemplazoStr))
    
    'If sp_GetHEDT001(Null, cArchivoNombre, Null, Null) Then                                ' del -017- a.
    If sp_GetHEDT001a(cArchivoNombre) Then                                                  ' add -017- a.
        sDSN_ID = ClearNull(aplRST.Fields!dsn_id)
        'bDatosSensibles = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "Si", True, False)      ' del -017- a.
        bDatosSensibles = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "S", True, False)        ' add -017- a.
    End If
    If chkSINEnmascararDatos.Value = 0 Then
        oControl = IIf(bDatosSensibles, "ARBP.ENM", "ARBZ.ENM") & Mid(oControl, 9)
    End If

    If Validar_CaracteresValidos(Numero, oControl, "SALIDA") Then
        If Validar_Calificadores(Numero, oControl, "SALIDA") Then
            MensajeError Numero, True       ' add -002- a.
        Else
            Exit Function
        End If
    Else
        Exit Function
    End If
    
    '{ add -002- a. Validaci�n de existencia en el cat�logo
    If chkSINEnmascararDatos.Value = 0 Then
        'cReemplazoStr = "ARBP.ENM." & glLOGIN_ID_REEMPLAZO & "."
        'cArchivoNombre = Mid(oControl, InStr(1, oControl, cReemplazoStr, vbTextCompare) + Len(cReemplazoStr))
        If cArchivoNombre <> "" Then
            'If sp_GetHEDT001(Null, cArchivoNombre, Null, Null) Then    ' del -028- a.
            If sp_GetHEDT001a(cArchivoNombre) Then                      ' add -028- a.
                '{ add -011- a.
                If ClearNull(aplRST.Fields!valido) = "Si" Then
                    '{ add -019- a.
                    If InStr(1, "N|P|S|", ClearNull(aplRST.Fields!dsn_vb_id), vbTextCompare) = 0 Then   ' add 08.04.2013
                        If Numero = 1 Then
                            MensajeError Numero, False, "3. Solo archivos Pendiente de revisi�n, Visado o Pendiente pos-revisi�n. Debe ser corregido antes de generar la solicitud."
                        ElseIf Numero = 4 Then
                            MensajeError Numero, False, "4. Solo archivos Pendiente de revisi�n, Visado o Pendiente pos-revisi�n. Debe ser corregido antes de generar la solicitud."
                        Else
                            MensajeError Numero, False, "2. Solo archivos Pendiente de revisi�n, Visado o Pendiente pos-revisi�n. Debe ser corregido antes de generar la solicitud."
                        End If
                    Else
                    '}
                        Validar_Archivo_DESA = True
                    End If      ' add -019- a.
                Else
                    'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El archivo ingresado no es v�lido (faltan copys y/o campos). Debe ser completado antes de generar la solicitud."
                    '{ add -010- a.
                    If Numero = 1 Then
                        MensajeError Numero, False, "3. El archivo ingresado no es v�lido (faltan copys y/o campos). Debe ser completado antes de generar la solicitud."
                    ElseIf Numero = 4 Then
                        MensajeError Numero, False, "4. El archivo ingresado no es v�lido (faltan copys y/o campos). Debe ser completado antes de generar la solicitud."
                    Else
                        MensajeError Numero, False, "2. El archivo ingresado no es v�lido (faltan copys y/o campos). Debe ser completado antes de generar la solicitud."
                    End If
                    '}
                    Exit Function
                End If
                '}
            Else            ' No existe
                '{ add -010- a.
                If Numero = 1 Then
                    MensajeError Numero, False, "3. El archivo " & Trim(cArchivoNombre) & " no existe en el cat�logo de enmascaramiento. Debe ser agregado."
                ElseIf Numero = 4 Then
                    MensajeError Numero, False, "4. El archivo " & Trim(cArchivoNombre) & " no existe en el cat�logo de enmascaramiento. Debe ser agregado."
                Else
                    MensajeError Numero, False, "2. El archivo " & Trim(cArchivoNombre) & " no existe en el cat�logo de enmascaramiento. Debe ser agregado."
                End If
                '}
                Exit Function
            End If
        Else
            MensajeError Numero, False, ""
            Exit Function
        End If
    Else
        '{ add -014- a.
        ' Si el archivo pedido no debe enmascararse, se valida que se haya ingresado algo...
        ' Si ya puso el calificador del nombre del usuario solicitante
        If InStr(1, oControl, glLOGIN_ID_REEMPLAZO, vbTextCompare) > 0 Then     ' Evalua si existe algo definido posterior al nombre del usuario
            If Mid(oControl, (InStr(1, oControl, glLOGIN_ID_REEMPLAZO, vbTextCompare) + Len(glLOGIN_ID_REEMPLAZO) + 1)) = "" Then
                MensajeError Numero, False
                Exit Function
            End If
        End If
        '}
        MensajeError Numero, True
        Validar_Archivo_DESA = True
    End If
    '}
End Function

'{ add -009- c.
Private Function Validar_AMBOS(cOrigen As String, cDestino As String) As Boolean
    Validar_AMBOS = False
    If Trim(UCase(cOrigen)) <> Trim(UCase(cDestino)) Then
        Validar_AMBOS = True
    End If
End Function
'}

Private Function Validar_Archivo_LIB(Numero As Byte, oControl As Control) As Boolean
    Validar_Archivo_LIB = True
End Function

Private Function Validar_Archivo_MEM(Numero As Byte, oControl As Control) As Boolean
    Validar_Archivo_MEM = True
End Function

Private Function Validar_CaracteresValidos(Numero As Byte, ByVal cNombre As String, tipo As String) As Boolean
    Dim i As Long
    Dim Pos As Long
    Dim car As String
    
    Validar_CaracteresValidos = False
    
    Select Case tipo
        Case "ENTRADA"
            For i = 1 To Len(cNombre)
                If InStr(1, CARACTERES_INVALIDOS, Mid(cNombre, i, 1), vbTextCompare) > 0 Then
                    car = Mid(CARACTERES_INVALIDOS, InStr(1, CARACTERES_INVALIDOS, Mid(cNombre, i, 1), vbTextCompare), 1)
                    Pos = InStr(1, cNombre, car, vbTextCompare)
                    '{ add -010- a.
                    If Numero = 6 Then
                        MensajeError Numero, False, "2. El nombre del archivo de intermedio contiene al menos un caract�r inv�lido en la posici�n " & Trim(Pos) & ":  " & Mid(cNombre, Pos, 1)
                    Else
                    '}
                        MensajeError Numero, False, "1. El nombre del archivo de producci�n contiene al menos un caract�r inv�lido en la posici�n " & Trim(Pos) & ":  " & Mid(cNombre, Pos, 1)
                    End If  ' add -010- a.
                    Exit Function
                End If
            Next i
        Case "SALIDA"
            For i = 1 To Len(cNombre)
                If InStr(1, CARACTERES_INVALIDOS, Mid(cNombre, i, 1), vbTextCompare) > 0 Then
                    car = Mid(CARACTERES_INVALIDOS, InStr(1, CARACTERES_INVALIDOS, Mid(cNombre, i, 1), vbTextCompare), 1)
                    Pos = InStr(1, cNombre, car, vbTextCompare)
                    'MensajeError Numero, False, "2. El nombre del archivo de desarrollo contiene al menos un caract�r inv�lido en la posici�n " & Trim(Pos) & ":  " & Mid(cNombre, Pos, 1) ' del -010- a.
                    '{ add -010- a.
                    If Numero = 1 Then
                        MensajeError Numero, False, "3. El nombre del archivo de desarrollo contiene al menos un caract�r inv�lido en la posici�n " & Trim(Pos) & ":  " & Mid(cNombre, Pos, 1)
                    ElseIf Numero = 4 Then
                        MensajeError Numero, False, "4. El nombre del archivo de desarrollo contiene al menos un caract�r inv�lido en la posici�n " & Trim(Pos) & ":  " & Mid(cNombre, Pos, 1)
                    Else
                        MensajeError Numero, False, "2. El nombre del archivo de desarrollo contiene al menos un caract�r inv�lido en la posici�n " & Trim(Pos) & ":  " & Mid(cNombre, Pos, 1)
                    End If
                    '}
                    Exit Function
                End If
            Next i
        '{ add -010- a.
        Case "INTERMEDIO"
            For i = 1 To Len(cNombre)
                If InStr(1, CARACTERES_INVALIDOS, Mid(cNombre, i, 1), vbTextCompare) > 0 Then
                    car = Mid(CARACTERES_INVALIDOS, InStr(1, CARACTERES_INVALIDOS, Mid(cNombre, i, 1), vbTextCompare), 1)
                    Pos = InStr(1, cNombre, car, vbTextCompare)
                    MensajeError Numero, False, "2. El nombre del archivo de intermedio contiene al menos un caract�r inv�lido en la posici�n " & Trim(Pos) & ":  " & Mid(cNombre, Pos, 1)
                    Exit Function
                End If
            Next i
        '}
    End Select
    Validar_CaracteresValidos = True
End Function

Public Function Validar_Calificadores(Numero As Byte, ByVal cNombre As String, tipo As String) As Boolean
    Dim cCalificador() As String
    Dim l_pos As Long
    Dim i As Long
    
    Validar_Calificadores = False
    l_pos = 1
    
    If Len(cNombre) > 0 Then
        Do While Len(cNombre) > 0
            l_pos = InStr(1, cNombre, ".", vbTextCompare)
            i = i + 1
            ReDim Preserve cCalificador(i)
            If l_pos > 0 Then
                cCalificador(i) = Mid(cNombre, 1, l_pos - 1): cNombre = Mid(cNombre, l_pos + 1)
            Else
                cCalificador(i) = Mid(cNombre, 1): cNombre = ""
            End If
        Loop

        Select Case tipo
            Case "ENTRADA"
                ' Control de cantidad m�nima de calificadores (al menos debe tener 2)
                If i < 2 Then
                    MensajeError Numero, False, "1. El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."
                    Exit Function
                End If
                
                For i = 1 To UBound(cCalificador)
                    ' Validaciones gen�ricas
                    If Len(cCalificador(i)) < 1 Then
                        MensajeError Numero, False, "1. Los calificadores deben tener al menos una longitud de un (1) caracter."
                        Exit Function
                    End If
                    '{ add -009- d.
                    If Len(cCalificador(i)) > 8 Then
                        MensajeError Numero, False, "1. Cualquiera de los calificadores deben tener una longitud menor o igual a ocho (8) caracteres."
                        Exit Function
                    End If
                    '}
                    ' Validaciones seg�n ordinal del calificador
                    Select Case i
                        Case 1  ' Primer calificador
                            '{ add -009- f.
                            If Not (cCalificador(i) = "GDES" Or cCalificador(i) = "ARBP" Or cCalificador(i) = "ARMP" Or (InStr(1, cCalificador(i), "ABP", vbTextCompare) > 0)) Then ' upd -009- e.
                                MensajeError Numero, False, "1. El primer calificador debe ser ARBP, ARMP o ABP*."
                                Exit Function
                            End If
                            '}
                        Case 2  ' Segundo calificador
                            If Len(cCalificador(i)) > 8 Then
                                MensajeError Numero, False, "1. El segundo calificador no puede ser mayor a ocho (8) posiciones."
                                Exit Function
                            End If
                            '{ add -009- a.
                            If cCalificador(i) = "ENM" Then
                                MensajeError Numero, False, "1. El segundo calificador no puede ser ENM."
                                Exit Function
                            End If
                            '}
                        Case Else   ' Tercer calificador y sucesivos
                            If Len(cCalificador(i)) > 8 Then
                                MensajeError Numero, False, "1. El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones."
                                Exit Function
                            End If
                    End Select
                Next i
            Case "SALIDA"
                ' Control de cantidad m�nima de calificadores (al menos debe tener 2)
                If i < 2 Then
                    'MensajeError Numero, False, "2. El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."  ' del -010- a.
                    'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."   ' add -010- a.
                    '{ add -010- a.
                    If Numero = 1 Then
                        MensajeError Numero, False, "3. El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."
                    ElseIf Numero = 4 Then
                        MensajeError Numero, False, "4. El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."
                    Else
                        MensajeError Numero, False, "2. El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."
                    End If
                    '}
                    Exit Function
                End If
                
                For i = 1 To UBound(cCalificador)
                    ' Validaciones gen�ricas
                    If Len(cCalificador(i)) < 1 Then
                        'MensajeError Numero, False, "2. Los calificadores deben tener al menos una longitud de un (1) caracter."   ' del -010- a.
                        'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". Los calificadores deben tener al menos una longitud de un (1) caracter."   ' add -010- a.
                        '{ add -010- a.
                        If Numero = 1 Then
                            MensajeError Numero, False, "3. Los calificadores deben tener al menos una longitud de un (1) caracter."
                        ElseIf Numero = 4 Then
                            MensajeError Numero, False, "4. Los calificadores deben tener al menos una longitud de un (1) caracter."
                        Else
                            MensajeError Numero, False, "2. Los calificadores deben tener al menos una longitud de un (1) caracter."
                        End If
                        '}
                        Exit Function
                    End If
                    ' Validaciones seg�n ordinal del calificador
                    Select Case i
                        Case 1      ' Primer calificador
                            If Len(cCalificador(i)) > 4 Then
                                '{ add -010- a.
                                If Numero = 1 Then
                                    MensajeError Numero, False, "3. El primer calificador no puede ser mayor a cuatro (4) posiciones."
                                ElseIf Numero = 4 Then
                                    MensajeError Numero, False, "4. El primer calificador no puede ser mayor a cuatro (4) posiciones."
                                Else
                                    MensajeError Numero, False, "2. El primer calificador no puede ser mayor a cuatro (4) posiciones."
                                End If
                                '}
                                Exit Function
                            End If
                            ' Si el Enmascaramiento est�:
                            ' ---------------------------
                            ' ACTIVADO   : El primer calificador del archivo de salida debe ser ARBZ"
                            ' NO ACTIVADO: El primer calificador del archivo de salida debe ser ARBP"
                            '{ add -002- a.
                            If chkSINEnmascararDatos.Value = 0 Then    ' La solicitud se pide CON enmascaramiento (normal)
                                If bDatosSensibles And cCalificador(i) <> "ARBP" Then
                                    'MensajeError Numero, False, "2. El primer calificador debe ser obligatoriamente ARBP." ' del -010- a.
                                    'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El primer calificador debe ser obligatoriamente ARBP."  ' add -010- a.
                                    '{ add -010- a.
                                    If Numero = 1 Then
                                        MensajeError Numero, False, "3. El primer calificador debe ser obligatoriamente ARBP."
                                    ElseIf Numero = 4 Then
                                        MensajeError Numero, False, "4. El primer calificador debe ser obligatoriamente ARBP."
                                    Else
                                        MensajeError Numero, False, "2. El primer calificador debe ser obligatoriamente ARBP."
                                    End If
                                    '}
                                    Exit Function
                                End If
                            Else
                            '}
                                If Not bDatosSensibles And cCalificador(i) <> "ARBZ" Then
                                    'MensajeError Numero, False, "2. El primer calificador debe ser obligatoriamente ARBZ." ' del -010- a.
                                    'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El primer calificador debe ser obligatoriamente ARBZ."  ' add -010- a.
                                    '{ add -010- a.
                                    If Numero = 1 Then
                                        MensajeError Numero, False, "3. El primer calificador debe ser obligatoriamente ARBZ."
                                    ElseIf Numero = 4 Then
                                        MensajeError Numero, False, "4. El primer calificador debe ser obligatoriamente ARBZ."
                                    Else
                                        MensajeError Numero, False, "2. El primer calificador debe ser obligatoriamente ARBZ."
                                    End If
                                    '}
                                    Exit Function
                                End If
                            End If      ' add -002- a.
                        Case 2      ' Segundo calificador
                            If chkSINEnmascararDatos.Value = 0 Then    ' La solicitud se pide CON enmascaramiento
                                If cCalificador(i) <> "ENM" Then
                                    'MensajeError Numero, False, "2. El segundo calificador debe ser ENM."  ' del -010- a.
                                    'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El segundo calificador debe ser ENM."   ' add -010- a.
                                    '{ add -010- a.
                                    If Numero = 1 Then
                                        MensajeError Numero, False, "3. El segundo calificador debe ser ENM."
                                    ElseIf Numero = 4 Then
                                        MensajeError Numero, False, "3. El segundo calificador debe ser ENM."   ' upd -026- a.
                                    Else
                                        MensajeError Numero, False, "2. El segundo calificador debe ser ENM."
                                    End If
                                    '}
                                    Exit Function
                                End If
                            Else
                                ' La solicitud se pide SIN enmascaramiento
                                '{ add -013- a.
                                If cCalificador(i) = "ENM" Then
                                    'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El segundo calificador no puede ser ENM."
                                    '{ add -010- a.
                                    If Numero = 1 Then
                                        MensajeError Numero, False, "3. El segundo calificador no puede ser ENM."
                                    ElseIf Numero = 4 Then
                                        MensajeError Numero, False, "4. El segundo calificador no puede ser ENM."
                                    Else
                                        MensajeError Numero, False, "2. El segundo calificador no puede ser ENM."
                                    End If
                                    '}
                                    Exit Function
                                End If
                                '}
                                If Len(cCalificador(i)) > 4 Then
                                    'MensajeError Numero, False, "2. El segundo calificador no puede ser mayor a cuatro (4) posiciones."    ' del -010- a.
                                    'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El segundo calificador no puede ser mayor a cuatro (4) posiciones."     ' add -010- a.
                                    '{ add -010- a.
                                    If Numero = 1 Then
                                        MensajeError Numero, False, "3. El segundo calificador no puede ser mayor a cuatro (4) posiciones."
                                    ElseIf Numero = 4 Then
                                        MensajeError Numero, False, "4. El segundo calificador no puede ser mayor a cuatro (4) posiciones."
                                    Else
                                        MensajeError Numero, False, "2. El segundo calificador no puede ser mayor a cuatro (4) posiciones."
                                    End If
                                    '}
                                    Exit Function
                                End If
                            End If
                        Case 3
                            If UCase(cCalificador(i)) <> UCase(glLOGIN_ID_REEMPLAZO) Then
                                'MensajeError Numero, False, "2. El tercer calificador debe ser el legajo del usuario que carga la solicitud."  ' del -010- a.
                                'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El tercer calificador debe ser el legajo del usuario que carga la solicitud."   ' add -010- a.
                                '{ add -010- a.
                                If Numero = 1 Then
                                    MensajeError Numero, False, "3. El tercer calificador debe ser el legajo del usuario que carga la solicitud."
                                ElseIf Numero = 4 Then
                                    MensajeError Numero, False, "4. El tercer calificador debe ser el legajo del usuario que carga la solicitud."
                                Else
                                    MensajeError Numero, False, "2. El tercer calificador debe ser el legajo del usuario que carga la solicitud."
                                End If
                                '}
                                Exit Function
                            End If
                        Case Else   ' Tercer calificador y sucesivos
                            If Len(cCalificador(i)) > 8 Then
                                'MensajeError Numero, False, "2. El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones." ' del -010- a.
                                'MensajeError Numero, False, IIf(Numero = 1 Or Numero = 4, "3", "2") & ". El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones."  ' add -010- a.
                                '{ add -010- a.
                                If Numero = 1 Then
                                    MensajeError Numero, False, "3. El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones."
                                ElseIf Numero = 4 Then
                                    MensajeError Numero, False, "4. El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones."
                                Else
                                    MensajeError Numero, False, "2. El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones."
                                End If
                                '}
                                Exit Function
                            End If
                    End Select
                Next i
            Case "OUT"
                ' Control de cantidad m�nima de calificadores (al menos debe tener 2)
                If i < 2 Then
                    MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."
                    Exit Function
                End If
                
                For i = 1 To UBound(cCalificador)
                    ' Validaciones gen�ricas
                    If Len(cCalificador(i)) < 1 Then
                        MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". Los calificadores deben tener al menos una longitud de un (1) caracter."
                        Exit Function
                    End If
                    
                    ' Validaciones seg�n el ordinal del calificador
                    Select Case i
                        Case 1      ' Primer calificador
                            If Len(cCalificador(i)) > 4 Then
                                MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El primer calificador no puede ser mayor a cuatro (4) posiciones."
                                Exit Function
                            End If
                            '{ add -002- a.
                            If chkSINEnmascararDatos.Value = 0 Then    ' La solicitud se pide CON enmascaramiento
                                If bDatosSensibles And cCalificador(i) <> "ARBP" Then
                                    MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El primer calificador debe ser obligatoriamente ARBP."
                                    Exit Function
                                End If
                            Else
                            '}
                                If bDatosSensibles And cCalificador(i) <> "ARBZ" Then
                                    MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El primer calificador debe ser obligatoriamente ARBZ."
                                    Exit Function
                                End If
                            End If      ' add -002- a.
                        Case 2      ' Segundo calificador
                            If chkSINEnmascararDatos.Value = 0 Then    ' CON ENMascaramiento
                                If cCalificador(i) <> "ENM" Then
                                    MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El segundo calificador debe ser ENM (enmascaramiento de datos)."
                                    Exit Function
                                End If
                            Else
                                '{ add -013- a.
                                If cCalificador(i) = "ENM" Then
                                    MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El segundo calificador no puede ser ENM."
                                    Exit Function
                                End If
                                '}
                                If Len(cCalificador(i)) > 4 Then
                                    MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El segundo calificador no puede ser mayor a cuatro (4) posiciones."
                                    Exit Function
                                End If
                            End If
                        Case 3
                            If UCase(cCalificador(i)) <> UCase(glLOGIN_ID_REEMPLAZO) Then
                                MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El tercer calificador debe ser el legajo del usuario que carga la solicitud."
                                Exit Function
                            End If
                        Case Else   ' Tercer calificador y sucesivos
                            If Len(cCalificador(i)) > 8 Then
                                MensajeError Numero, False, IIf(Numero = 2, "1", "2") & ". El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones."
                                Exit Function
                            End If
                    End Select
                Next i
            '{ add -010- a.
            Case "INTERMEDIO"
                ' Control de cantidad m�nima de calificadores (al menos debe tener 2)
                If i < 2 Then
                    MensajeError Numero, False, "2. El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido."
                    Exit Function
                End If
                
                For i = 1 To UBound(cCalificador)
                    ' Validaciones gen�ricas
                    If Len(cCalificador(i)) < 1 Then
                        MensajeError Numero, False, "2. Los calificadores deben tener al menos una longitud de un (1) caracter."
                        Exit Function
                    End If
                    '{ add -009- d.
                    If Len(cCalificador(i)) > 8 Then
                        MensajeError Numero, False, "2. Cualquiera de los calificadores deben tener una longitud menor o igual a ocho (8) caracteres."
                        Exit Function
                    End If
                    '}
                    ' Validaciones seg�n ordinal del calificador
                    Select Case i
                        Case 1  ' Primer calificador
                            '{ add -009- d.
                            If Len(cCalificador(i)) > 4 Then
                                MensajeError Numero, False, "2. El primer calificador no puede ser mayor a cuatro (4) posiciones."
                                Exit Function
                            End If
                            If Not (cCalificador(i) = "ARBP" Or cCalificador(i) = "ARMP" Or (InStr(1, cCalificador(i), "ABP", vbTextCompare) > 0)) Then ' upd -009- e.
                                MensajeError Numero, False, "2. El primer calificador debe ser ARBP, ARMP o ABP*."
                                Exit Function
                            End If
                            '}
                        Case 2  ' Segundo calificador
                            If Len(cCalificador(i)) > 4 Then
                                MensajeError Numero, False, "2. El segundo calificador no puede ser mayor a cuatro (4) posiciones."
                                Exit Function
                            End If
                            '{ add -009- a.
                            If cCalificador(i) = "ENM" Then
                                MensajeError Numero, False, "2. El segundo calificador no puede ser ENM."
                                Exit Function
                            End If
                            '}
                        Case Else   ' Tercer calificador y sucesivos
                            If Len(cCalificador(i)) > 8 Then
                                MensajeError Numero, False, "2. El tercer y sucesivos calificadores no pueden ser mayores de ocho (8) posiciones."
                                Exit Function
                            End If
                    End Select
                Next i
            '}
        End Select
    End If
    lblWarning.visible = False
    Validar_Calificadores = True
End Function

' ============================================================================================================================================
' 1. XCOM
' ============================================================================================================================================
Private Sub txtOpc1FilePROD_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc1FilePROD.visible = False
        With cmbOpc1FilePROD_Sel
            .Text = Trim(txtOpc1FilePROD)
            .visible = True
            If txtOpc1FilePROD.visible Then .SetFocus
        End With
    End If
End Sub

Private Sub txtOpc1FileDESA_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        If cmbOpc1FileDESA_Sel.ListCount > 0 Then
            txtOpc1FileDESA.visible = False
            With cmbOpc1FileDESA_Sel
                '.Text = Trim(txtOpc1FileDESA)      ' del -01.10.2012-
                If .ListCount > 0 Then .ListIndex = 0
                .visible = True
                If txtOpc1FileDESA.visible Then .SetFocus
            End With
        End If
    End If
End Sub

Private Sub txtOpc1FilePROD_Change()
    lblOpc1FileProd = Len(txtOpc1FilePROD)
End Sub

Private Sub txtOpc1FileDESA_Change()
    lblOpc1FileDesa = Len(txtOpc1FileDESA)
End Sub

'Private Sub cmbOpc1Supervisor_Click()
'    Call Validar_Siguiente
'End Sub

Private Sub txtOpc1Peticion_LostFocus()
    Set oLastControlUsed = txtOpc1Peticion      ' add -004- d.
    Call Validar_Siguiente
End Sub

Private Sub cmbOpc1FilePROD_Sel_LostFocus()
    txtOpc1FilePROD = IIf(Len(cmbOpc1FilePROD_Sel) = 0, cmbOpc1FilePROD_Sel.List(cmbOpc1FilePROD_Sel.ListIndex), UCase(cmbOpc1FilePROD_Sel.Text))
    cmbOpc1FilePROD_Sel.visible = False
    txtOpc1FilePROD.visible = True
    Call Validar_Siguiente
End Sub

Private Sub cmbOpc1FileDESA_Sel_LostFocus()
    txtOpc1FileDESA = IIf(Len(cmbOpc1FileDESA_Sel) = 0, cmbOpc1FileDESA_Sel.List(cmbOpc1FileDESA_Sel.ListIndex), UCase(cmbOpc1FileDESA_Sel.Text))
    cmbOpc1FileDESA_Sel.visible = False
    txtOpc1FileDESA.visible = True
    Call Validar_Siguiente
End Sub

Private Sub txtOpc1FilePROD_LostFocus()
    Set oLastControlUsed = txtOpc1FilePROD      ' add -004- d.
    lblAyuda = ""
    txtOpc1FilePROD = UCase(txtOpc1FilePROD)
    Call Validar_Siguiente
End Sub

'{ add -020- a.
Private Sub txtOpc1FilePROD_BBLCPY_LostFocus()
    Set oLastControlUsed = txtOpc1FilePROD_BBLCPY
    lblAyuda = ""
    txtOpc1FilePROD_BBLCPY = UCase(txtOpc1FilePROD_BBLCPY)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc1FilePROD_NMECPY_LostFocus()
    Set oLastControlUsed = txtOpc1FilePROD_NMECPY
    lblAyuda = ""
    txtOpc1FilePROD_NMECPY = UCase(txtOpc1FilePROD_NMECPY)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc2FileOUT_BBLCPY_LostFocus()
    Set oLastControlUsed = txtOpc2FileOUT_BBLCPY
    lblAyuda = ""
    txtOpc2FileOUT_BBLCPY = UCase(txtOpc2FileOUT_BBLCPY)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc2FileOUT_NMECPY_LostFocus()
    Set oLastControlUsed = txtOpc2FileOUT_NMECPY
    lblAyuda = ""
    txtOpc2FileOUT_NMECPY = UCase(txtOpc2FileOUT_NMECPY)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc2Tabla_LostFocus()
    Set oLastControlUsed = txtOpc2Tabla
    txtOpc2Tabla = UCase(txtOpc2Tabla)
    Validar_Siguiente
End Sub

Private Sub txtOpc2FechaBackUp_LostFocus()
    Set oLastControlUsed = txtOpc2FechaBackUp
    Validar_Siguiente
End Sub

Private Sub txtOpc2JobBACKUP_LostFocus()
    Set oLastControlUsed = txtOpc2JobBACKUP
    txtOpc2JobBACKUP = UCase(txtOpc2JobBACKUP)
    Validar_Siguiente
End Sub

Private Sub txtOpc3FileExtract_BBLCPY_LostFocus()
    Set oLastControlUsed = txtOpc3FileExtract_BBLCPY
    lblAyuda = ""
    txtOpc3FileExtract_BBLCPY = UCase(txtOpc3FileExtract_BBLCPY)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc3FileExtract_NMECPY_LostFocus()
    Set oLastControlUsed = txtOpc3FileExtract_NMECPY
    lblAyuda = ""
    txtOpc3FileExtract_NMECPY = UCase(txtOpc3FileExtract_NMECPY)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc4FilePROD_BBLCPY_LostFocus()
    Set oLastControlUsed = txtOpc4FilePROD_BBLCPY
    lblAyuda = ""
    txtOpc4FilePROD_BBLCPY = UCase(txtOpc4FilePROD_BBLCPY)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc4FilePROD_NMECPY_LostFocus()
    Set oLastControlUsed = txtOpc4FilePROD_NMECPY
    lblAyuda = ""
    txtOpc4FilePROD_NMECPY = UCase(txtOpc4FilePROD_NMECPY)
    Call Validar_Siguiente
End Sub
'}

Private Sub txtOpc1FileDESA_LostFocus()
    Set oLastControlUsed = txtOpc1FileDESA      ' add -004- d.
    lblAyuda = ""
    txtOpc1FileDESA = UCase(txtOpc1FileDESA)
    Call Validar_Siguiente
End Sub

'{ add -010- a.
Private Sub txtOpc1FechaBackUp_LostFocus()
    Set oLastControlUsed = txtOpc1FechaBackUp
    lblAyuda = ""
    Call Validar_Siguiente
End Sub
'}

Private Sub txtOpc1FilePROD_DblClick()
    txtOpc1FilePROD_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc1FileDESA_DblClick()
    txtOpc1FileDESA_KeyDown vbKeyF4, 0
End Sub

Private Sub chk_CopyXCOM_Click()
    If Not bFlagLoad Then
        If chk_CopyXCOM.Value = 0 Then
            Call setHabilCtrl(txtOpc1FilePROD_BBLCPY, "NOR")
            Call setHabilCtrl(txtOpc1FilePROD_NMECPY, "NOR")
            txtOpc1FilePROD_BBLCPY = IIf(Len(txtOpc1FilePROD_BBLCPY) > 0, txtOpc1FilePROD_BBLCPY, "CMN.ABASE.CPY")
            txtOpc1FilePROD_NMECPY = ""
        Else
            Call setHabilCtrl(txtOpc1FilePROD_BBLCPY, "DIS")
            Call setHabilCtrl(txtOpc1FilePROD_NMECPY, "DIS")
            txtOpc1FilePROD_BBLCPY = ""
            txtOpc1FilePROD_NMECPY = ""
        End If
        Call Validar_NombreCopy(TIPOSOL_XCOM, txtOpc1FilePROD_NMECPY, chk_CopyXCOM)
    End If
End Sub

' ============================================================================================================================================
' 2. UNLO
' ============================================================================================================================================
Private Sub txtOpc2FileOUT_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc2FileOUT.visible = False
        With cmbOpc2FileOUT_Sel
            .Text = Trim(txtOpc2FileOUT)
            .visible = True
            If txtOpc2FileOUT.visible Then .SetFocus
        End With
    End If
End Sub

Private Sub txtOpc2LibSysin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc2LibSysin.visible = False
        With cmbOpc2LibSysin_Sel
            .Text = Trim(txtOpc2LibSysin)
            .visible = True
            If txtOpc2LibSysin.visible Then .SetFocus
        End With
    End If
End Sub

Private Sub txtOpc2MemSysin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc2MemSysin.visible = False
        With cmbOpc2MemSysin_Sel
            .Text = Trim(txtOpc2MemSysin)
            .visible = True
            If txtOpc2MemSysin.visible Then .SetFocus
        End With
    End If
End Sub

Private Sub txtOpc2FileOUT_LostFocus()
    Set oLastControlUsed = txtOpc2FileOUT      ' add -004- d.
    lblAyuda = ""
    txtOpc2FileOUT = UCase(txtOpc2FileOUT)
    Validar_Siguiente
End Sub

Private Sub txtOpc2LibSysin_LostFocus()
    Set oLastControlUsed = txtOpc2LibSysin      ' add -004- d.
    lblAyuda = ""
    txtOpc2LibSysin = UCase(txtOpc2LibSysin)
    Validar_Siguiente
End Sub

Private Sub txtOpc2MemSysin_LostFocus()
    Set oLastControlUsed = txtOpc2MemSysin      ' add -004- d.
    lblAyuda = ""
    txtOpc2MemSysin = UCase(txtOpc2MemSysin)
    Validar_Siguiente
End Sub

Private Sub cmbOpc2FileOUT_Sel_LostFocus()
    txtOpc2FileOUT = IIf(Len(cmbOpc2FileOUT_Sel) = 0, cmbOpc2FileOUT_Sel.List(cmbOpc2FileOUT_Sel.ListIndex), UCase(cmbOpc2FileOUT_Sel.Text))
    cmbOpc2FileOUT_Sel.visible = False
    txtOpc2FileOUT.visible = True
End Sub

Private Sub cmbOpc2LibSysin_Sel_LostFocus()
    txtOpc2LibSysin = IIf(Len(cmbOpc2LibSysin_Sel) = 0, cmbOpc2LibSysin_Sel.List(cmbOpc2LibSysin_Sel.ListIndex), UCase(cmbOpc2LibSysin_Sel.Text))
    cmbOpc2LibSysin_Sel.visible = False
    txtOpc2LibSysin.visible = True
End Sub

Private Sub cmbOpc2MemSysin_Sel_LostFocus()
    txtOpc2MemSysin = IIf(Len(cmbOpc2MemSysin_Sel) = 0, cmbOpc2MemSysin_Sel.List(cmbOpc2MemSysin_Sel.ListIndex), UCase(cmbOpc2MemSysin_Sel.Text))
    cmbOpc2MemSysin_Sel.visible = False
    txtOpc2MemSysin.visible = True
End Sub

Private Sub txtOpc2FileOUT_DblClick()
    txtOpc2FileOUT_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc2MemSysin_DblClick()
    txtOpc2MemSysin_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc2Peticion_LostFocus()
    Set oLastControlUsed = txtOpc2Peticion      ' add -004- d.
    Validar_Siguiente
End Sub

Private Sub txtOpc2FileOUT_Change()
    lblOpc2FileOut = Len(txtOpc2FileOUT)
End Sub

Private Sub txtOpc2LibSysin_Change()
    lblOpc2LibSysin = Len(txtOpc2LibSysin)
End Sub

Private Sub txtOpc2MemSysin_Change()
    lblOpc2MemSysin = Len(txtOpc2MemSysin)
End Sub

Private Sub cmbOpc2FileOUT_Sel_Change()
    lblOpc2FileOut = Len(cmbOpc2FileOUT_Sel)
    Validar_Siguiente
End Sub

Private Sub cmbOpc2LibSysin_Sel_Change()
    lblOpc2LibSysin = Len(cmbOpc2LibSysin_Sel)
End Sub

Private Sub cmbOpc2MemSysin_Sel_Change()
    lblOpc2MemSysin = Len(cmbOpc2MemSysin_Sel)
End Sub

Private Sub chk_CopyUNLO_Click()
    If Not bFlagLoad Then
        If chk_CopyUNLO.Value = 0 Then
            Call setHabilCtrl(txtOpc2FileOUT_BBLCPY, "NOR")
            Call setHabilCtrl(txtOpc2FileOUT_NMECPY, "NOR")
            txtOpc2FileOUT_BBLCPY = IIf(Len(txtOpc2FileOUT_BBLCPY) > 0, txtOpc2FileOUT_BBLCPY, "CMN.ABASE.CPY")
            txtOpc2FileOUT_NMECPY = ""
        Else
            Call setHabilCtrl(txtOpc2FileOUT_BBLCPY, "DIS")
            Call setHabilCtrl(txtOpc2FileOUT_NMECPY, "DIS")
            txtOpc2FileOUT_BBLCPY = ""
            txtOpc2FileOUT_NMECPY = ""
        End If
        Call Validar_NombreCopy(TIPOSOL_UNLO, txtOpc2FileOUT_NMECPY, chk_CopyUNLO)
    End If
End Sub

' ============================================================================================================================================
' 3. TCOR
' ============================================================================================================================================
Private Sub txtOpc3FileExtract_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc3FileExtract.visible = False
        With cmbOpc3FileExtract_Sel
            .Text = Trim(txtOpc3FileExtract)
            .visible = True
            If cmbOpc3FileExtract_Sel.visible Then .SetFocus
        End With
    End If
End Sub

Private Sub txtOpc3FileDESA_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc3FileDESA.visible = False
        With cmbOpc3FileDESA_Sel
            .Text = Trim(txtOpc3FileDESA)
            .visible = True
            If cmbOpc3FileDESA_Sel.visible Then .SetFocus
        End With
    End If
End Sub

Private Sub cmbOpc3FileExtract_Sel_LostFocus()
    txtOpc3FileExtract = IIf(Len(cmbOpc3FileExtract_Sel) = 0, cmbOpc3FileExtract_Sel.List(cmbOpc3FileExtract_Sel.ListIndex), UCase(cmbOpc3FileExtract_Sel.Text))
    cmbOpc3FileExtract_Sel.visible = False
    txtOpc3FileExtract.visible = True
End Sub

Private Sub cmbOpc3FileDESA_Sel_LostFocus()
    txtOpc3FileDESA = IIf(Len(cmbOpc3FileDESA_Sel) = 0, cmbOpc3FileDESA_Sel.List(cmbOpc3FileDESA_Sel.ListIndex), UCase(cmbOpc3FileDESA_Sel.Text))
    cmbOpc3FileDESA_Sel.visible = False
    txtOpc3FileDESA.visible = True
    Validar_Siguiente
End Sub

Private Sub txtOpc3FileExtract_DblClick()
    txtOpc3FileExtract_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc3FileDESA_DblClick()
    txtOpc3FileDESA_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc3FileExtract_LostFocus()
    Set oLastControlUsed = txtOpc3FileExtract      ' add -004- d.
    lblAyuda = ""
    txtOpc3FileExtract = UCase(txtOpc3FileExtract)
    Validar_Siguiente
End Sub

Private Sub txtOpc3FileDESA_LostFocus()
    Set oLastControlUsed = txtOpc3FileDESA      ' add -004- d.
    lblAyuda = ""
    txtOpc3FileDESA = UCase(txtOpc3FileDESA)
    Validar_Siguiente
End Sub

'Private Sub cmbOpc3Supervisor_LostFocus()
'    lblAyuda = ""
'    Validar_Siguiente
'End Sub

Private Sub txtOpc3Peticion_LostFocus()
    Set oLastControlUsed = txtOpc3Peticion      ' add -004- d.
    lblAyuda = ""
    Validar_Siguiente
End Sub

Private Sub txtOpc3FileDesa_Change()
    lblOpc3FileDesa = Len(txtOpc3FileDESA)
End Sub

Private Function Validar_NroTabla(Numero As Byte, oControl As Control) As Boolean
    Validar_NroTabla = False
    
    If Len(oControl) > 0 Then
        If IsNumeric(oControl) Then
            If oControl < 10000 Then
                Validar_NroTabla = True
                MensajeError Numero, True
                Exit Function
            Else
                MensajeError Numero, False, "1. El n�mero debe ser inferior a 10000."
            End If
        Else
            MensajeError Numero, False, "1. El valor ingresado no es un n�mero v�lido."
        End If
    Else
        MensajeError Numero, False, "1. Debe ingresar un n�mero entero positivo."
    End If
End Function

Private Sub chk_CopyTCOR_Click()
    If Not bFlagLoad Then
        If chk_CopyTCOR.Value = 0 Then
            Call setHabilCtrl(txtOpc3FileExtract_BBLCPY, "NOR")
            Call setHabilCtrl(txtOpc3FileExtract_NMECPY, "NOR")
            txtOpc3FileExtract_BBLCPY = IIf(Len(txtOpc3FileExtract_BBLCPY) > 0, txtOpc3FileExtract_BBLCPY, "CMN.ABASE.CPY")
            txtOpc3FileExtract_NMECPY = ""
        Else
            Call setHabilCtrl(txtOpc3FileExtract_BBLCPY, "DIS")
            Call setHabilCtrl(txtOpc3FileExtract_NMECPY, "DIS")
            txtOpc3FileExtract_BBLCPY = ""
            txtOpc3FileExtract_NMECPY = ""
        End If
        Call Validar_NombreCopy(TIPOSOL_TCOR, txtOpc3FileExtract_NMECPY, chk_CopyTCOR)
    End If
End Sub

' ============================================================================================================================================
' 4. SORT
' ============================================================================================================================================
Private Sub txtOpc4FilePROD_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc4FilePROD.visible = False
        With cmbOpc4FilePROD_Sel
            .Text = Trim(txtOpc4FilePROD)
            .visible = True
            'If cmbOpc4FilePROD_Sel.Visible Then .SetFocus
        End With
    End If
End Sub

Private Sub txtOpc4FileDESA_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        If cmbOpc4FileDESA_Sel.ListCount > 0 Then
            txtOpc4FileDESA.visible = False
            With cmbOpc4FileDESA_Sel
                '.Text = Trim(txtOpc4FileDESA)
                If .ListCount > 0 Then .ListIndex = 0
                .visible = True
                'If cmbOpc4FileDESA_Sel.Visible Then .SetFocus
            End With
        End If
    End If
End Sub

Private Sub txtOpc4LibSysin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc4LibSysin.visible = False
        With cmbOpc4LibSysin_Sel
            .Text = Trim(txtOpc4LibSysin)
            .visible = True
            'If cmbOpc4LibSysin_Sel.Visible Then .SetFocus
        End With
    End If
End Sub

Private Sub txtOpc4MemSysin_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF4 Then
        lblAyuda = AYUDA_COMBO
        txtOpc4MemSysin.visible = False
        With cmbOpc4MemSysin_Sel
            .Text = Trim(txtOpc4MemSysin)
            .visible = True
            'If cmbOpc4MemSysin_Sel.Visible Then .SetFocus
        End With
    End If
End Sub

Private Sub cmbOpc4FilePROD_Sel_LostFocus()
    txtOpc4FilePROD = IIf(Len(cmbOpc4FilePROD_Sel) = 0, cmbOpc4FilePROD_Sel.List(cmbOpc4FilePROD_Sel.ListIndex), UCase(cmbOpc4FilePROD_Sel.Text))
    txtOpc4FilePROD.visible = True
    cmbOpc4FilePROD_Sel.visible = False
    'If txtOpc4FileDESA.Visible Then txtOpc4FileDESA.SetFocus
    Validar_Siguiente
End Sub

Private Sub cmbOpc4FileDESA_Sel_LostFocus()
    txtOpc4FileDESA = IIf(Len(cmbOpc4FileDESA_Sel) = 0, cmbOpc4FileDESA_Sel.List(cmbOpc4FileDESA_Sel.ListIndex), UCase(cmbOpc4FileDESA_Sel.Text))
    txtOpc4FileDESA.visible = True
    cmbOpc4FileDESA_Sel.visible = False
    Validar_Siguiente
End Sub

Private Sub cmbOpc4LibSysin_Sel_LostFocus()
    txtOpc4LibSysin = IIf(Len(cmbOpc4LibSysin_Sel) = 0, cmbOpc4LibSysin_Sel.List(cmbOpc4LibSysin_Sel.ListIndex), UCase(cmbOpc4LibSysin_Sel.Text))
    txtOpc4LibSysin.visible = True
    cmbOpc4LibSysin_Sel.visible = False
    'If txtOpc4MemSysin.Visible Then txtOpc4MemSysin.SetFocus
End Sub

Private Sub cmbOpc4MemSysin_Sel_LostFocus()
    txtOpc4MemSysin = IIf(Len(cmbOpc4MemSysin_Sel) = 0, cmbOpc4MemSysin_Sel.List(cmbOpc4MemSysin_Sel.ListIndex), UCase(cmbOpc4MemSysin_Sel.Text))
    txtOpc4MemSysin.visible = True
    cmbOpc4MemSysin_Sel.visible = False
End Sub

'{ add -010- a.
Private Sub txtOpc4FechaBackup_LostFocus()
    Set oLastControlUsed = txtOpc4FechaBackup
    lblAyuda = ""
    Validar_Siguiente
End Sub
'}

Private Sub txtOpc4FilePROD_DblClick()
    txtOpc4FilePROD_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc4FileDESA_DblClick()
    txtOpc4FileDESA_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc4MemSysin_DblClick()
    txtOpc4MemSysin_KeyDown vbKeyF4, 0
End Sub

Private Sub txtOpc4FilePROD_LostFocus()
    Set oLastControlUsed = txtOpc4FilePROD      ' add -004- d.
    lblAyuda = ""
    txtOpc4FilePROD = UCase(txtOpc4FilePROD)
    Call Validar_Siguiente
End Sub

Private Sub txtOpc4FileDESA_LostFocus()
    lblAyuda = ""
    txtOpc4FileDESA = UCase(txtOpc4FileDESA)
    Set oLastControlUsed = txtOpc4FileDESA      ' add -004- d.
    Call Validar_Siguiente
End Sub

Private Sub txtOpc4LibSysin_LostFocus()
    Set oLastControlUsed = txtOpc4LibSysin      ' add -004- d.
    lblAyuda = ""
    txtOpc4LibSysin = UCase(txtOpc4LibSysin)
    Validar_Siguiente
End Sub

Private Sub txtOpc4MemSysin_LostFocus()
    Set oLastControlUsed = txtOpc4MemSysin      ' add -004- d.
    lblAyuda = ""
    txtOpc4MemSysin = UCase(txtOpc4MemSysin)
    Validar_Siguiente
End Sub

'Private Sub cmbOpc4Supervisor_LostFocus()
'    lblAyuda = ""
'    Validar_Siguiente
'End Sub

Private Sub txtOpc4Peticion_LostFocus()
    Set oLastControlUsed = txtOpc4Peticion      ' add -004- d.
    lblAyuda = ""
    Validar_Siguiente
End Sub

Private Sub txtOpc4FilePROD_Change()
    lblOpc4FileProd = Len(txtOpc4FilePROD)
End Sub

Private Sub txtOpc4FileDESA_Change()
    lblOpc4FileDesa = Len(txtOpc4FileDESA)
End Sub

Private Sub txtOpc4LibSysin_Change()
    lblOpc4LibSysin = Len(txtOpc4LibSysin)
End Sub

Private Sub txtOpc4MemSysin_Change()
    lblOpc4MemSysin = Len(txtOpc4MemSysin)
End Sub

Private Sub chk_CopySORT_Click()
    If Not bFlagLoad Then
        If chk_CopySORT.Value = 0 Then
            Call setHabilCtrl(txtOpc4FilePROD_BBLCPY, "NOR")
            Call setHabilCtrl(txtOpc4FilePROD_NMECPY, "NOR")
            txtOpc4FilePROD_BBLCPY = IIf(Len(txtOpc4FilePROD_BBLCPY) > 0, txtOpc4FilePROD_BBLCPY, "CMN.ABASE.CPY")
            txtOpc4FilePROD_NMECPY = ""
        Else
            Call setHabilCtrl(txtOpc4FilePROD_BBLCPY, "DIS")
            Call setHabilCtrl(txtOpc4FilePROD_NMECPY, "DIS")
            txtOpc4FilePROD_BBLCPY = ""
            txtOpc4FilePROD_NMECPY = ""
        End If
        Call Validar_NombreCopy(TIPOSOL_SORT, txtOpc4FilePROD_NMECPY, chk_CopySORT)
    End If
End Sub

' ==================================================================================================================================
' MENSAJES DE ERROR (GENERICO)
' ==================================================================================================================================
Private Sub MensajeError(Numero As Byte, bEstado As Boolean, Optional cMensaje As String)
    Select Case Numero
        Case TIPOSOL_XCOM
            If bEstado Then
                lblWarning.Caption = ""
                lblWarning.visible = False
            Else
                lblWarning.Caption = Trim(cMensaje)
                lblWarning.visible = True
            End If
        Case TIPOSOL_UNLO
            If bEstado Then
                lblWarningUNLO.Caption = ""
                lblWarningUNLO.visible = False
            Else
                lblWarningUNLO.Caption = Trim(cMensaje)
                lblWarningUNLO.visible = True
            End If
        Case TIPOSOL_TCOR
            If bEstado Then
                lblWarningTCOR.Caption = ""
                lblWarningTCOR.visible = False
            Else
                lblWarningTCOR.Caption = Trim(cMensaje)
                lblWarningTCOR.visible = True
            End If
        Case TIPOSOL_SORT
            If bEstado Then
                lblWarningSORT.Caption = ""
                lblWarningSORT.visible = False
            Else
                lblWarningSORT.Caption = Trim(cMensaje)
                lblWarningSORT.visible = True
            End If
        Case Else
            lblWarningSORT.Caption = ""
            lblWarningSORT.visible = False
    End Select
End Sub

' ==================================================================================================================================
' FUNCIONALIDAD DE SELECCION DE PETICION DESDE UN BOTON (GEN�RICO)
' ==================================================================================================================================
Private Sub cmdSeleccionarPeticion_Click(Index As Integer)
    Dim vRetorno() As String
    Dim nPos As Integer
    
    glAuxRetorno = ""
    
    '{ del -024- a.
    ''frmSelPet.Show 1       ' del -005- a.
    'frmSelPetGrp.Show 1     ' add -005- a.
    '}
    '{ -024- a.
    Select Case glUsrPerfilActual
        Case "CSEC": frmSelPet.Show 1
        Case Else
            frmSelPetGrp.Show 1
    End Select
    '}
    DoEvents
    
    Select Case Index
        Case TIPOSOL_XCOM
            If glAuxRetorno <> "" Then
                If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
                    txtOpc1Peticion = Mid(vRetorno(1), 1, InStr(1, vRetorno(1), ":", vbTextCompare) - 1)
                    txtOpc1PeticionInterno = vRetorno(3)
                    If sp_GetUnaPeticion(vRetorno(3)) Then
                        txtOpc1PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                        txtOpc1PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                        MensajeError CByte(Index), True
                        Validar_Siguiente
                    Else
                        MensajeError CByte(Index), False, "La petici�n ingresada no existe o no es v�lida."
                    End If
                    DoEvents
                End If
            End If
        Case TIPOSOL_UNLO
            If glAuxRetorno <> "" Then
                If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
                    txtOpc2Peticion = Mid(vRetorno(1), 1, InStr(1, vRetorno(1), ":", vbTextCompare) - 1)
                    txtOpc2PeticionInterno = vRetorno(3)
                    If sp_GetUnaPeticion(vRetorno(3)) Then
                        txtOpc2PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                        txtOpc2PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                        MensajeError CByte(Index), True
                        Validar_Siguiente
                    Else
                        MensajeError CByte(Index), False, "La petici�n ingresada no existe o no es v�lida."
                    End If
                    DoEvents
                End If
            End If
        Case TIPOSOL_TCOR
            If glAuxRetorno <> "" Then
                If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
                    txtOpc3Peticion = Mid(vRetorno(1), 1, InStr(1, vRetorno(1), ":", vbTextCompare) - 1)
                    txtOpc3PeticionInterno = vRetorno(3)
                    If sp_GetUnaPeticion(vRetorno(3)) Then
                        txtOpc3PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                        txtOpc3PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                        MensajeError CByte(Index), True
                        Validar_Siguiente
                    Else
                        MensajeError CByte(Index), False, "La petici�n ingresada no existe o no es v�lida."
                    End If
                    DoEvents
                End If
            End If
        Case TIPOSOL_SORT
            If glAuxRetorno <> "" Then
                If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
                    txtOpc4Peticion = Mid(vRetorno(1), 1, InStr(1, vRetorno(1), ":", vbTextCompare) - 1)
                    txtOpc4PeticionInterno = vRetorno(3)
                    If sp_GetUnaPeticion(vRetorno(3)) Then
                        txtOpc4PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                        txtOpc4PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                        MensajeError CByte(Index), True
                        Validar_Siguiente
                    Else
                        MensajeError CByte(Index), False, "La petici�n ingresada no existe o no es v�lida."
                    End If
                    DoEvents
                End If
            End If
    End Select
End Sub

' ==================================================================================================================================
' VALIDEZ DE LA PETICION INGRESADA O SELECCIONADA (GENERICO)
' ==================================================================================================================================
Private Function Validar_Peticion(Numero As Byte) As Boolean
    Validar_Peticion = False
    
    Select Case Numero
        Case TIPOSOL_XCOM
            With txtOpc1Peticion
                '{ add -021- a. Si es una EMErgencia, solo se valida que si se ingresa un n�mero de petici�n,
                '               la misma exista, sino, puede dejarse en blanco el campo.
                If chkEmergencia.Value = 1 Then
                    If Len(.Text) > 0 And IsNumeric(.Text) And Val(.Text) < 999999999 Then
                        If sp_GetUnaPeticionAsig(.Text) Then
                            txtOpc1PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                            txtOpc1PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                            MensajeError Numero, True
                            Validar_Peticion = True
                            Exit Function
                        Else
                            txtOpc1PeticionTitulo = ""
                            MensajeError 1, False, "4. Debe seleccionar una petici�n existente o dejar vac�o este campo."
                            Exit Function
                        End If
                    Else
                        MensajeError Numero, True
                        Validar_Peticion = True
                        Exit Function
                    End If
                Else
                '}
                    If Len(.Text) > 0 Then
                        If IsNumeric(.Text) And Val(.Text) < 999999999 Then             ' upd -005- a.
                            'If sp_GetUnaPeticionAsig(.Text) Then                       ' del -005- a.
                            Select Case glUsrPerfilActual
                                Case "CSEC"
                                    If sp_GetUnaPeticionAsig(.Text) Then
                                        txtOpc1PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                        txtOpc1PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                        MensajeError Numero, True
                                        Validar_Peticion = True
                                        Exit Function
                                    Else
                                        txtOpc1PeticionTitulo.Text = ""
                                        MensajeError 1, False, "4. Debe ingresar un n�mero v�lido de petici�n."
                                        Exit Function
                                    End If
                                Case Else
                                    If sp_GetPeticionGrupoSol(.Text, glLOGIN_Grupo) Then        ' add -005- a.
                                        If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                            txtOpc1PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                            txtOpc1PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError Numero, True
                                            Validar_Peticion = True
                                            Exit Function
                                        Else
                                            txtOpc1PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError 1, False, "4. Debe seleccionar una petici�n en estado " & Chr(34) & "En ejecuci�n" & Chr(34) & " en la que se encuentre como grupo Ejecutor."  ' upd -005- a.  ' upd -010- a.
                                            Exit Function
                                        End If
                                    Else
                                        txtOpc1PeticionTitulo.Text = ""
                                        MensajeError 1, False, "4. Debe ingresar un n�mero v�lido de petici�n en la que se encuentre como grupo Ejecutor."  ' upd -005- a.  ' upd -010- a.
                                        Exit Function
                                    End If
                            End Select
                        '{ add -005- a.
                        Else
                            txtOpc1PeticionTitulo.Text = ""
                            MensajeError 1, False, "4. Debe ingresar un n�mero v�lido para la petici�n (menor a 999999999)."    ' upd -010- a.
                            Exit Function
                        '}
                        End If
                    Else
                        txtOpc1PeticionTitulo.Text = ""
                        MensajeError 1, False, "4. Falta especificar la petici�n en la que se encuentre como grupo Ejecutor."
                        Exit Function
                    End If
                End If      ' add -021- a.
            End With
        Case TIPOSOL_UNLO
            With txtOpc2Peticion
                '{ add -021- a. Si es una EMErgencia, solo se valida que si se ingresa un n�mero de petici�n,
                '               la misma exista, sino, puede dejarse en blanco el campo.
                If chkEmergencia.Value = 1 Then
                    If Len(.Text) > 0 And IsNumeric(.Text) And Val(.Text) < 999999999 Then
                        If sp_GetUnaPeticionAsig(.Text) Then
                            txtOpc2PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                            txtOpc2PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                            MensajeError Numero, True
                            Validar_Peticion = True
                            Exit Function
                        Else
                            txtOpc2PeticionTitulo = ""
                            MensajeError TIPOSOL_UNLO, False, "8. Debe seleccionar una petici�n existente o dejar vac�o este campo."
                            Exit Function
                        End If
                    Else
                        MensajeError Numero, True
                        Validar_Peticion = True
                        Exit Function
                    End If
                Else
                '}
                    If Len(.Text) > 0 Then
                        If IsNumeric(.Text) And Val(.Text) < 999999999 Then ' upd -005- a.
                            'If sp_GetUnaPeticionAsig(.Text) Then           ' del -005- a.
                            Select Case glUsrPerfilActual
                                Case "CSEC"
                                    If sp_GetUnaPeticionAsig(.Text) Then
                                        txtOpc2PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                        txtOpc2PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                        MensajeError 2, True
                                        Validar_Peticion = True
                                    Else
                                        txtOpc2PeticionTitulo.Text = ""
                                        MensajeError 2, False, "8. Debe ingresar un n�mero v�lido de petici�n."
                                        Exit Function
                                    End If
                                Case Else
                                    If sp_GetPeticionGrupoSol(.Text, glLOGIN_Grupo) Then    ' add -005- a.
                                        If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                            txtOpc2PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                            txtOpc2PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError 2, True
                                            Validar_Peticion = True
                                        Else
                                            txtOpc2PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError 2, False, "8. Debe seleccionar una petici�n en estado " & Chr(34) & "En ejecuci�n" & Chr(34) & " en la que se encuentre como grupo Ejecutor."  ' upd -005- a.
                                            Exit Function
                                        End If
                                    Else
                                        txtOpc2PeticionTitulo.Text = ""
                                        MensajeError 2, False, "8. Debe ingresar un n�mero v�lido de petici�n en la que se encuentre como grupo Ejecutor."  ' upd -005- a.
                                        Exit Function
                                    End If
                            End Select
                        '{ add -005- a.
                        Else
                            txtOpc2PeticionTitulo.Text = ""
                            MensajeError 2, False, "8. Debe ingresar un n�mero v�lido para la petici�n (menor a 999999999)."
                            Exit Function
                        '}
                        End If
                    Else
                        txtOpc1PeticionTitulo.Text = ""
                        MensajeError 2, False, "5. Falta especificar la petici�n en la que se encuentre como grupo Ejecutor."
                        Exit Function
                    End If
                End If          ' add -021- a.
            End With
        Case TIPOSOL_TCOR
            With txtOpc3Peticion
                '{ add -021- a. Si es una EMErgencia, solo se valida que si se ingresa un n�mero de petici�n,
                '               la misma exista, sino, puede dejarse en blanco el campo.
                If chkEmergencia.Value = 1 Then
                    If Len(.Text) > 0 And IsNumeric(.Text) And Val(.Text) < 999999999 Then
                        If sp_GetUnaPeticionAsig(.Text) Then
                            txtOpc3PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                            txtOpc3PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                            MensajeError Numero, True
                            Validar_Peticion = True
                            Exit Function
                        Else
                            txtOpc3PeticionTitulo = ""
                            MensajeError TIPOSOL_TCOR, False, "4. Debe seleccionar una petici�n existente o dejar vac�o este campo."
                            Exit Function
                        End If
                    Else
                        MensajeError Numero, True
                        Validar_Peticion = True
                        Exit Function
                    End If
                Else
                '}
                    If Len(.Text) > 0 Then
                        If IsNumeric(.Text) And Val(.Text) < 999999999 Then ' upd -005- a.
                            'If sp_GetUnaPeticionAsig(.Text) Then           ' del -005- a.
                            Select Case glUsrPerfilActual
                                Case "CSEC"
                                    If sp_GetUnaPeticionAsig(.Text) Then
                                        txtOpc3PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                        txtOpc3PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                        MensajeError 3, True
                                        Validar_Peticion = True
                                        Exit Function
                                    Else
                                        txtOpc3PeticionInterno = ""
                                        txtOpc3PeticionTitulo.Text = ""
                                        MensajeError 3, False, "4. Debe ingresar un n�mero v�lido de petici�n."  ' upd -005- a.
                                        Exit Function
                                    End If
                                Case Else
                                    If sp_GetPeticionGrupoSol(.Text, glLOGIN_Grupo) Then  ' add -005- a.
                                        If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                            txtOpc3PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                            txtOpc3PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError 3, True
                                            Validar_Peticion = True
                                            Exit Function
                                        Else
                                            txtOpc3PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError 3, False, "4. Debe seleccionar una petici�n en estado " & Chr(34) & "En ejecuci�n" & Chr(34) & " en la que se encuentre como grupo Ejecutor."  ' upd -005- a.
                                            Exit Function
                                        End If
                                    Else
                                        txtOpc3PeticionInterno = ""
                                        txtOpc3PeticionTitulo.Text = ""
                                        MensajeError 3, False, "4. Debe ingresar un n�mero v�lido de petici�n en la que se encuentre como grupo Ejecutor."  ' upd -005- a.
                                        Exit Function
                                    End If
                            End Select
                        '{ add -005- a.
                        Else
                            txtOpc3PeticionTitulo.Text = ""
                            MensajeError 3, False, "4. Debe ingresar un n�mero v�lido para la petici�n (menor a 999999999)."
                            Exit Function
                        '}
                        End If
                    End If
                End If          ' add -021- a.
            End With
        Case TIPOSOL_SORT
            With txtOpc4Peticion
                '{ add -021- a. Si es una EMErgencia, solo se valida que si se ingresa un n�mero de petici�n,
                '               la misma exista, sino, puede dejarse en blanco el campo.
                If chkEmergencia.Value = 1 Then
                    If Len(.Text) > 0 And IsNumeric(.Text) And Val(.Text) < 999999999 Then
                        If sp_GetUnaPeticionAsig(.Text) Then
                            txtOpc4PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                            txtOpc4PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                            MensajeError Numero, True
                            Validar_Peticion = True
                            Exit Function
                        Else
                            txtOpc4PeticionTitulo = ""
                            MensajeError TIPOSOL_SORT, False, "8. Debe seleccionar una petici�n existente o dejar vac�o este campo."
                            Exit Function
                        End If
                    Else
                        MensajeError Numero, True
                        Validar_Peticion = True
                        Exit Function
                    End If
                Else
                '}
                    If Len(.Text) > 0 Then
                        If IsNumeric(.Text) And Val(.Text) < 999999999 Then ' upd -005- a.
                            'If sp_GetUnaPeticionAsig(.Text) Then           ' del -005- a.
                            Select Case glUsrPerfilActual
                                Case "CSEC"
                                    If sp_GetUnaPeticionAsig(.Text) Then
                                        txtOpc4PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                        txtOpc4PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                        MensajeError Numero, True
                                        Validar_Peticion = True
                                        Exit Function
                                    Else
                                        txtOpc4PeticionInterno = ""
                                        txtOpc4PeticionTitulo.Text = ""
                                        MensajeError Numero, False, "8. Debe ingresar un n�mero v�lido de petici�n."
                                        Exit Function
                                    End If
                                Case Else
                                    If sp_GetPeticionGrupoSol(.Text, glLOGIN_Grupo) Then        ' add -005- a.
                                        If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                            txtOpc4PeticionInterno = ClearNull(aplRST.Fields!pet_nrointerno)
                                            txtOpc4PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError Numero, True
                                            Validar_Peticion = True
                                            Exit Function
                                        Else
                                            txtOpc4PeticionTitulo = ClearNull(aplRST.Fields!Titulo)
                                            MensajeError Numero, False, "8. Debe seleccionar una petici�n en estado " & Chr(34) & "En ejecuci�n" & Chr(34) & " en la que se encuentre como grupo Ejecutor."  ' upd -005- a. ' upd -010- a.
                                            Exit Function
                                        End If
                                    Else
                                        txtOpc4PeticionTitulo.Text = ""
                                        MensajeError Numero, False, "8. Debe ingresar un n�mero v�lido de petici�n en la que se encuentre como grupo Ejecutor."  ' upd -005- a. ' upd -010- a.
                                        Exit Function
                                    End If
                            End Select
                        '{ add -005- a.
                        Else
                            txtOpc4PeticionInterno = ""
                            txtOpc4PeticionTitulo.Text = ""
                            MensajeError Numero, False, "7. Debe ingresar un n�mero v�lido para la petici�n (menor a 999999999)."   ' upd -010- a.  ' upd -026- a.
                            Exit Function
                        '}
                        End If
                    End If
                End If          ' add -021- a.
            End With
    End Select
End Function

'' ==================================================================================================================================
'' VALIDACION DEL SUPERVISOR SELECCIONADO (GENERICO)
'' ==================================================================================================================================
'Private Function Validar_Supervisor(Numero As Byte, oControl As Control) As Boolean
'    Validar_Supervisor = False
'    With oControl
'        If .ListIndex = -1 Then
'            Select Case Numero
'                Case 1: MensajeError Numero, False, "3. Debe seleccionar un supervisor v�lido."
'                Case 2: MensajeError Numero, False, "4. Debe seleccionar un supervisor v�lido."
'                Case 3: MensajeError Numero, False, "3. Debe seleccionar un supervisor v�lido."
'                Case 4: MensajeError Numero, False, "5. Debe seleccionar un supervisor v�lido."
'            End Select
'            Exit Function
'        Else
'            MensajeError Numero, True
'            Validar_Supervisor = True
'        End If
'    End With
'End Function

'{ add -020- a.
' ==================================================================================================================================
' VALIDACION DE LA BIBLIOTECA DE COPY
' ==================================================================================================================================
Private Function Validar_BibliotecaCopy(Numero As Byte, oControl As Control, oControlChk As Control) As Boolean
    'Dim pControl As Control
    
    Validar_BibliotecaCopy = False
    
    If chkSINEnmascararDatos.Value = 1 Then
        If oControlChk.Value = 0 Then
            With oControl
                If Len(oControl) < 1 Then
                    MensajeError Numero, False, "Debe ingresar una biblioteca de COPY v�lida."
                    Exit Function
                Else
                    MensajeError Numero, True
                    Validar_BibliotecaCopy = True
                End If
            End With
        Else
            Validar_BibliotecaCopy = True
        End If
    Else
        Validar_BibliotecaCopy = True
    End If
End Function

' ==================================================================================================================================
' VALIDACION DE LA NOMBRE DE COPY
' ==================================================================================================================================
Private Function Validar_NombreCopy(Numero As Byte, oControl As Control, oControlChk As Control) As Boolean
    Validar_NombreCopy = False
    
    If chkSINEnmascararDatos.Value = 1 Then
        If oControlChk.Value = 0 Then
            With oControl
                If Len(oControl) < 1 Then
                    MensajeError Numero, False, "Debe ingresar el nombre de COPY v�lido."
                    Exit Function
                Else
                    MensajeError Numero, True
                    Validar_NombreCopy = True
                End If
            End With
        Else
            MensajeError Numero, True       ' Estaba faltando para limpiar el mensaje
            Validar_NombreCopy = True
        End If
    Else
        Validar_NombreCopy = True
    End If
End Function
'}

' ============================================================================================================================================================
' AYUDAS EN PANTALLA CUANDO SE SELECCIONA UN CAMPO DETERMINADO
' ============================================================================================================================================================

' ============================================================================================================================================================
' 1. XCOM
' ============================================================================================================================================================
Private Sub txtOpc1FilePROD_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub cmbOpc1FilePROD_Sel_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub txtOpc1FileDESA_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
    
    'Debug.Print vbCrLf & Now
    'Debug.Print "cmdWizard_Cancelar  : " & cmdWizard_Cancelar.Enabled
    'Debug.Print "cmdWizard_Anterior  : " & cmdWizard_Atras.Enabled
    'Debug.Print "cmdWizard_Siguiente : " & cmdWizard_Adelante.Enabled
    'Debug.Print "cmdWizard_Finalizar : " & cmdWizard_Terminar.Enabled
    
    
    '{ add -004- d.
'    If Len(glCatalogoNombre) > 0 Then   ' Tiene un nombre de archivo cargado
'        ' Me fijo si el usuario ya lo digit� en el textbox
'        If Not InStr(1, txtOpc1FileDESA, glCatalogoNombre, vbTextCompare) > 0 Then  ' Si no lo ingres�, veo si lo puedo concatenar directamente o agregado el calificador
'            If Right(Trim(txtOpc1FileDESA), 1) = "." Then
'                txtOpc1FileDESA.Text = Trim(txtOpc1FileDESA.Text) & Trim(glCatalogoNombre)
'            Else
'                txtOpc1FileDESA.Text = Trim(txtOpc1FileDESA.Text) & "." & Trim(glCatalogoNombre)
'            End If
'        End If
'        glCatalogoNombre = ""
'    End If
    '}
End Sub

Private Sub cmbOpc1FileDESA_Sel_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

'Private Sub chkOpc1MoreFiles_GotFocus()
'    lblAyuda = AYUDA_MAS_ARCHIVOS
'End Sub

'Private Sub cmbOpc1Supervisor_GotFocus()
'    lblAyuda = AYUDA_SUPERVISOR
'End Sub

Private Sub txtOpc1Peticion_GotFocus()
    lblAyuda = AYUDA_PETICION
End Sub

' ============================================================================================================================================================
' 2. UNLO
' ============================================================================================================================================================
Private Sub txtOpc2FileOUT_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub txtOpc2LibSysin_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub txtOpc2MemSysin_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

'Private Sub cmbOpc2Supervisor_GotFocus()
'    lblAyuda = AYUDA_SUPERVISOR
'End Sub

Private Sub txtOpc2Peticion_GotFocus()
    lblAyuda = AYUDA_PETICION
End Sub

Private Sub chkOpc2Join_GotFocus()
    lblAyuda = AYUDA_JOIN
End Sub

' ============================================================================================================================================================
' 3. TCOR
' ============================================================================================================================================================
Private Sub txtOpc3FileExtract_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub txtOpc3FileDESA_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

'Private Sub cmbOpc3Supervisor_GotFocus()
'    lblAyuda = AYUDA_SUPERVISOR
'End Sub

Private Sub txtOpc3Peticion_GotFocus()
    lblAyuda = AYUDA_PETICION
End Sub

' ============================================================================================================================================================
' 4. SORT
' ============================================================================================================================================================
Private Sub txtOpc4FilePROD_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub txtOpc4FileDESA_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub txtOpc4LibSysin_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

Private Sub txtOpc4MemSysin_GotFocus()
    lblAyuda = AYUDA_SELECCION_ARCHIVO
End Sub

'Private Sub cmbOpc4Supervisor_GotFocus()
'    lblAyuda = AYUDA_SUPERVISOR
'End Sub

Private Sub txtOpc4Peticion_GotFocus()
    lblAyuda = AYUDA_PETICION
End Sub

' Otros
Private Sub chkSINEnmascararDatos_Click()
    If chkSINEnmascararDatos.Value = 1 Then
        If chkEmergencia.Value = 1 Then
            lblAdvertirSegInf.visible = False
            lblEMEMsg.visible = True
        Else
            lblAdvertirSegInf.visible = True
            lblEMEMsg.visible = False
        End If
        cmdCatalogo.Enabled = False     ' add -004- a.
        '{ add -015- a.
        chkDiferido.Value = 0
        chkDiferido.Enabled = False
        '}
        '{ add -021- a.
        'chkEmergencia.Value = 0
        'chkEmergencia.Enabled = False
        '}
    Else
        lblAdvertirSegInf.visible = False
        cmdCatalogo.Enabled = True      ' add -004- a.
        chkDiferido.Enabled = True      ' add -015- a.
    End If
End Sub

'{ add -021- a.
Private Sub chkEmergencia_Click()

    ' Com�n a todos
    If chkEmergencia.Value = 1 Then
        lblAdvertirSegInf.visible = False
        lblEMEMsg.visible = True
        chkSINEnmascararDatos.Enabled = False
    Else
        lblAdvertirSegInf.visible = True
        lblEMEMsg.visible = False
        chkSINEnmascararDatos.Enabled = True
    End If
    
    If optOpciones(0).Value Then              ' XCOM
        If chkEmergencia.Value = 1 Then
            chkRestore.Enabled = True               ' Una EMErgencia puede ser un restore
            chkDiferido.Enabled = False
            chkSINEnmascararDatos.Value = 1         ' Por EMErgencia, los datos salen sin enmascarar
        Else
            chkRestore.Enabled = True
            chkDiferido.Enabled = True
            'chkSINEnmascararDatos.Enabled = True
        End If
    ElseIf optOpciones(1).Value Then          ' UNLO
        If chkEmergencia.Value = 1 Then
            chkRestore.Enabled = True
            chkDiferido.Enabled = False
            chkSINEnmascararDatos.Value = 1        ' Por EMErgencia, los datos salen sin enmascarar
        Else
            chkRestore.Enabled = True
            chkDiferido.Enabled = True
            'chkSINEnmascararDatos.Enabled = True
        End If
    ElseIf optOpciones(2).Value Then          ' TCOR
        If chkEmergencia.Value = 1 Then
            chkRestore.Enabled = False
            chkDiferido.Enabled = False
            chkSINEnmascararDatos.Value = 1        ' Por EMErgencia, los datos salen sin enmascarar
        Else
            chkRestore.Enabled = False
            chkDiferido.Enabled = True
            'chkSINEnmascararDatos.Enabled = True
        End If
    ElseIf optOpciones(3).Value Then          ' SORT
        If chkEmergencia.Value = 1 Then
            chkRestore.Enabled = True               ' Una EMErgencia puede ser un restore
            chkDiferido.Enabled = False
            chkSINEnmascararDatos.Value = 1        ' Por EMErgencia, los datos salen sin enmascarar
        Else
            chkRestore.Enabled = True
            chkDiferido.Enabled = True
            'chkSINEnmascararDatos.Enabled = True
        End If
    End If

'    ' VIEJO
'    If chkEmergencia.Value = 1 Then
'        lblAdvertirSegInf.Visible = False
'        lblEMEMsg.Visible = True
'        chkSINEnmascararDatos.Enabled = False
'        If optOpciones(2).Enabled Then
'            chkRestore.Enabled = False              ' Una EMErgencia puede ser un restore
'        Else
'            chkRestore.Enabled = True               ' Una EMErgencia puede ser un restore
'        End If
'        chkDiferido.Enabled = False
'        chkSINEnmascararDatos.Value = 1        ' Por EMErgencia, los datos salen sin enmascarar
'    Else
'        If optOpciones(2).Enabled Then
'            chkRestore.Enabled = False
'        Else
'            chkRestore.Enabled = True
'        End If
'        If chkSINEnmascararDatos.Value = 1 Then
'            lblAdvertirSegInf.Visible = True
'            lblEMEMsg.Visible = False
'
'            chkDiferido.Value = 0
'            chkDiferido.Enabled = False
'        Else
'            chkRestore.Enabled = True
'            chkDiferido.Enabled = True
'        End If
'        chkSINEnmascararDatos.Enabled = True
'    End If
End Sub
'}

'{ add -002- a.
Private Sub cmdCatalogo_Click()
    glCatalogoENM = "SEL"
    frmHEDT1ShellDyD.Show   ' upd -018- a. Antes frmHEDT1Shell.Show
End Sub
'}

'{ add -004- d.
Private Sub Form_Activate()
    If oLastControlUsed <> "" Then
        'oLastControlUsed.SetFocus
    End If
End Sub
'}

'{ add -015- a.
Private Sub chkDiferido_Click()
    If chkDiferido.Value = 1 Then
        chkSINEnmascararDatos.Value = 0
        chkSINEnmascararDatos.Enabled = False
        
        chkEmergencia.Value = 0
        chkEmergencia.Enabled = False
    Else
        chkSINEnmascararDatos.Enabled = True
        chkEmergencia.Value = 0
        chkEmergencia.Enabled = True
    End If
End Sub
'}

'{ del -026- a.
''{ add -010- a.
'Private Sub txtOpc4FileTMP_LostFocus()
'    Set oLastControlUsed = txtOpc4FileTMP
'    lblAyuda = ""
'    txtOpc4FileTMP = UCase(txtOpc4FileTMP)
'    Validar_Siguiente
'End Sub
''}
'}

' Ya no se usa
'Public Function Hora(HoraActual) As String
'    Dim cHora As String
'
'    If CDate(Format(CDate(HoraActual), "hh:mm:ss")) >= CDate(Format(CDate("17:30:00"), "hh:mm:ss")) Then
'        cHora = "Antes de las 08:00 de ma�ana"
'    ElseIf CDate(Format(CDate(HoraActual), "hh:mm:ss")) >= CDate(Format(CDate("16:00:00"), "hh:mm:ss")) Then
'        cHora = "Antes de las 17:30 de hoy"
'    ElseIf CDate(Format(CDate(HoraActual), "hh:mm:ss")) >= CDate(Format(CDate("14:00:00"), "hh:mm:ss")) Then
'        cHora = "Antes de las 16:00 de hoy"
'    ElseIf CDate(Format(CDate(HoraActual), "hh:mm:ss")) >= CDate(Format(CDate("12:00:00"), "hh:mm:ss")) Then
'        cHora = "Antes de las 14:00 de hoy"
'    ElseIf CDate(Format(CDate(HoraActual), "hh:mm:ss")) >= CDate(Format(CDate("10:00:00"), "hh:mm:ss")) Then
'        cHora = "Antes de las 12:00 de hoy"
'    ElseIf CDate(Format(CDate(HoraActual), "hh:mm:ss")) >= CDate(Format(CDate("08:00:00"), "hh:mm:ss")) Then
'        cHora = "Antes de las 10:00 de hoy"
'    Else
'        cHora = "Antes de las 08:00 de hoy"
'    End If
'    Hora = cHora
'End Function

' FUNCIONABA OK al 21/08/2014
'Private Sub UDT_Actualizar()
'    ' Incremento el contador global de solicitudes
'    'cntSolicitudes = cntSolicitudes + 1    ' Por ahora comentado
'    cntSolicitudes = 1
'
'    ' Muestro el nro. de solicitud en carga (si la opci�n de "M�s archivos" esta chekeada, si no, no tiene sentido mostrar porque es la �nica solicitud
'    If chkOpc1MoreFiles.Value = 1 Then
'        lblContador = cntSolicitudes
'    End If
'    ' Redimensiono el vector de solicitudes
'    ReDim Preserve Solicitud(cntSolicitudes)   ' Por ahora
'
'    Select Case iTipoSolicitud
'        Case TIPOSOL_XCOM
'            With Solicitud(cntSolicitudes)
'                .Emergencia = IIf(chkEmergencia.Value = 1, True, False)     ' add -021- a.
'                '{ add -010- a.
'                .tipo = IIf(chkRestore.Value = 1, 5, 1)
'                If chkRestore.Value = 1 Then
'                    .FechaBckUp = txtOpc1FechaBackUp.Text
'                End If
'                '}
'                .item = cntSolicitudes
'                .Masked = IIf(chkSINEnmascararDatos.Value = 1, False, True)
'                .fecha = Now
'                .Recurso = glLOGIN_ID_REEMPLAZO
'                .Peticion = IIf(txtOpc1PeticionInterno.Text = "", 0, txtOpc1PeticionInterno.Text)
'                .Lider = ""
'                .Supervisor = CodigoCombo(cmbOpc1Supervisor, False)
'                .SI = ""
'                .CodigoJus = CodigoCombo(cmbJustificacion, True)
'                .Justificacion = Trim(txtJustificacion)
'                '{ add -010- a.
'                If chkRestore.Value = 1 Then
'                    .ArchivoOut = txtOpc1FilePROD
'                    .ArchivoDesa = txtOpc1FileDESA
'                    If InStr(1, .ArchivoDesa, ".ENM.", vbTextCompare) > 0 Then
'                        .ArchivoProd = Mid(.ArchivoDesa, 1, InStr(1, .ArchivoDesa, ".ENM.", vbTextCompare) - 1) & ".DYD." & _
'                                      Mid(.ArchivoDesa, InStr(1, .ArchivoDesa, ".ENM.", vbTextCompare) + 5, Len(.ArchivoDesa))
'                    Else
'                        .ArchivoProd = .ArchivoDesa
'                    End If
'                Else
'                '}
'                    .ArchivoProd = txtOpc1FilePROD
'                    .ArchivoDesa = txtOpc1FileDESA
'                    .ArchivoOut = ""
'                End If      ' add -010- a.
'                .Lib_Sysin = ""
'                .Mem_Sysin = ""
'                .Join = False
'                .NroTabla = 0
'                '{ add -020- a.
'                .CopyBiblioteca = ClearNull(txtOpc1FilePROD_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc1FilePROD_NMECPY)
'                .dsn_id = sDSN_ID
'                '}
'            End With
'        Case TIPOSOL_UNLO
'            With Solicitud(cntSolicitudes)
'                .Emergencia = IIf(chkEmergencia.Value = 1, True, False)     ' add -021- a.
'                '.tipo = 2          ' del -022- a.
'                '{ add -022- a.
'                .tipo = IIf(chkRestore.Value = 1, 7, 2)
'                If chkRestore.Value = 1 Then
'                    .FechaBckUp = txtOpc2FechaBackUp.Text
'                End If
'                '}
'                .item = cntSolicitudes
'                .Masked = IIf(chkSINEnmascararDatos.Value = 1, False, True)
'                .fecha = Now
'                .Recurso = glLOGIN_ID_REEMPLAZO
'                .Peticion = IIf(txtOpc2PeticionInterno.Text = "", 0, txtOpc2PeticionInterno.Text)
'                .Lider = ""
'                .Supervisor = CodigoCombo(cmbOpc2Supervisor, False)
'                .SI = ""
'                .CodigoJus = CodigoCombo(cmbJustificacion, True)
'                .Justificacion = Trim(txtJustificacion)
'                '{ add -022- a.
'                If chkRestore.Value = 1 Then
'                    .ArchivoProd = txtOpc2Tabla
'                    .ArchivoDesa = txtOpc2JobBACKUP
'                    .ArchivoOut = txtOpc2FileOUT
'                Else
'                    .ArchivoProd = ""
'                    .ArchivoDesa = ""
'                    .ArchivoOut = txtOpc2FileOUT
'                End If
'                '}
'                '{ del -022- a.
'                '.ArchivoProd = ""
'                '.ArchivoDesa = ""
'                '.ArchivoOut = txtOpc2FileOUT
'                '}
'                .Lib_Sysin = txtOpc2LibSysin
'                .Mem_Sysin = txtOpc2MemSysin
'                .Join = IIf(chkOpc2Join.Value = 1, True, False)
'                .NroTabla = 0
'                '{ add -020- a.
'                .CopyBiblioteca = ClearNull(txtOpc2FileOUT_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc2FileOUT_NMECPY)
'                .dsn_id = sDSN_ID
'                '}
'            End With
'        Case TIPOSOL_TCOR
'            With Solicitud(cntSolicitudes)
'                .Emergencia = IIf(chkEmergencia.Value = 1, True, False)     ' add -021- a.
'                .tipo = 3
'                .item = cntSolicitudes
'                .Masked = IIf(chkSINEnmascararDatos.Value = 1, False, True)
'                .fecha = Now
'                .Recurso = glLOGIN_ID_REEMPLAZO
'                .Peticion = IIf(txtOpc3PeticionInterno.Text = "", 0, txtOpc3PeticionInterno.Text)
'                .Lider = ""
'                .Supervisor = CodigoCombo(cmbOpc3Supervisor, False)
'                .SI = ""
'                .CodigoJus = CodigoCombo(cmbJustificacion, True)
'                .Justificacion = Trim(txtJustificacion)
'                .ArchivoProd = ""
'                .ArchivoDesa = ""
'                .ArchivoOut = txtOpc3FileDESA
'                .Lib_Sysin = ""
'                .Mem_Sysin = ""
'                .Join = IIf(chkOpc2Join.Value = 1, True, False)
'                .NroTabla = txtOpc3FileExtract
'                '{ add -020- a.
'                .CopyBiblioteca = ClearNull(txtOpc3FileExtract_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc3FileExtract_NMECPY)
'                .dsn_id = sDSN_ID
'                '}
'            End With
'        Case TIPOSOL_SORT
'            With Solicitud(cntSolicitudes)
'                '.Tipo = 4                                  ' del -010- a.
'                .Emergencia = IIf(chkEmergencia.Value = 1, True, False)     ' add -021- a.
'                '{ add -010- a.
'                .tipo = IIf(chkRestore.Value = 1, 6, 4)
'                If chkRestore.Value = 1 Then
'                    .FechaBckUp = txtOpc4FechaBackup.Text
'                End If
'                '}
'                .item = cntSolicitudes
'                .Masked = IIf(chkSINEnmascararDatos.Value = 1, False, True)
'                .fecha = Now
'                .Recurso = glLOGIN_ID_REEMPLAZO
'                .Peticion = IIf(txtOpc4PeticionInterno.Text = "", 0, txtOpc4PeticionInterno.Text)
'                .Lider = ""
'                .Supervisor = CodigoCombo(cmbOpc4Supervisor, False)
'                .SI = ""
'                .CodigoJus = CodigoCombo(cmbJustificacion, True)
'                .Justificacion = Trim(txtJustificacion)
'                .ArchivoProd = txtOpc4FilePROD
'                .ArchivoDesa = txtOpc4FileTMP.Text      ' upd -010- a.
'                .ArchivoOut = txtOpc4FileDESA
'                .Lib_Sysin = txtOpc4LibSysin
'                .Mem_Sysin = txtOpc4MemSysin
'                .Join = False
'                .NroTabla = 0
'                '{ add -020- a.
'                .CopyBiblioteca = ClearNull(txtOpc4FilePROD_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc4FilePROD_NMECPY)
'                .dsn_id = sDSN_ID
'                '}
'            End With
'    End Select
'    ' Establezco el Lider del recurso
'    With Solicitud(cntSolicitudes)
'        If ClearNull(glLOGIN_Grupo) = "" Then
'            .Lider = ""         ' Porque es el supervisor el usuario solicitante
'        Else
'            If sp_GetResponsableAreaFinal("GRUP", glLOGIN_Grupo) Then
'                .Lider = ClearNull(aplRST.Fields!cod_recurso)
'            End If
'        End If
'    End With
'End Sub

'Private Const AYUDA_MAS_ARCHIVOS = "Activando esta casilla, luego de la carga actual, podr� realizar nuevas solicitudes del mismo tipo. Desact�vela si solo desea generar una �nica solicitud para el tipo actual."
'Private Const AYUDA_SUPERVISOR = "Por defecto, el supervisor designado es su propio Supervisor. En caso que desee cambiarlo, seleccione la opci�n deseada."
'Private Const AYUDA_SUPERVISOR2 = "Por defecto, el supervisor designado es su propio Supervisor. En caso que desee cambiarlo, seleccione la opci�n deseada. Si no hubiere supervisor para el usuario actual, se deber� seleccionar alguno de los supervisores del �rea de DYD."

'Private Const CARACTERES_INVALIDOS = "!#$%&()*+,-/:;<=>?@[\]^_`{|}'~��������������������������������������������������������������������������������������������������������������������������������"

'Dim Solicitud() As udt_Solicitud

'Private Sub UDT_Actualizar()
'    With Solicitud
'        .item = 1
'        .Emergencia = IIf(chkEmergencia.Value = 1, True, False)
'        .Masked = IIf(chkSINEnmascararDatos.Value = 1, False, True)
'        .Diferida = IIf(chkDiferido.Value = 1, True, False)
'        .fecha = Now
'        .Recurso = glLOGIN_ID_REEMPLAZO
'        .CodigoJus = CodigoCombo(cmbJustificacion, True)
'        .Justificacion = Trim(txtJustificacion)
'        .dsn_id = sDSN_ID
'        Select Case iTipoSolicitud
'            Case TIPOSOL_XCOM
'                .tipo = IIf(chkRestore.Value = 1, 5, 1)
'                .Peticion = IIf(txtOpc1PeticionInterno.Text = "", 0, txtOpc1PeticionInterno.Text)
'                If chkRestore.Value = 1 Then
'                    .FechaBckUp = txtOpc1FechaBackUp.Text
'                    .ArchivoProd = ""
'                    .ArchivoOut = txtOpc1FilePROD
'                    .ArchivoDesa = txtOpc1FileDESA
'                    If InStr(1, txtOpc1FileDESA, ".ENM.", vbTextCompare) > 0 Then
'                        .ArchivoIntermedio = Mid(txtOpc1FileDESA, 1, InStr(1, txtOpc1FileDESA, ".ENM.", vbTextCompare) - 1) & ".DYD." & Mid(txtOpc1FileDESA, InStr(1, txtOpc1FileDESA, ".ENM.", vbTextCompare) + 5, Len(txtOpc1FileDESA))
'                    Else
'                        .ArchivoIntermedio = .ArchivoOut
'                    End If
'                    '.ArchivoIntermedio = ""
'                    .TablaBackup = ""
'                    .JobBackup = ""
'                Else
'                    .FechaBckUp = FECHA_NULL
'                    .ArchivoProd = txtOpc1FilePROD
'                    .ArchivoDesa = txtOpc1FileDESA
'                    .ArchivoOut = ""
'                    .ArchivoIntermedio = ""
'                    .TablaBackup = ""
'                    .JobBackup = ""
'                End If
'                .Lib_Sysin = ""
'                .Mem_Sysin = ""
'                .Join = False
'                .NroTabla = 0
'                .CopyBiblioteca = ClearNull(txtOpc1FilePROD_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc1FilePROD_NMECPY)
'            Case TIPOSOL_UNLO
'                .tipo = IIf(chkRestore.Value = 1, 7, 2)
'                .Peticion = IIf(txtOpc2PeticionInterno.Text = "", 0, txtOpc2PeticionInterno.Text)
'                If chkRestore.Value = 1 Then
'                    .FechaBckUp = txtOpc2FechaBackUp.Text
'                    .TablaBackup = txtOpc2Tabla
'                    .ArchivoProd = ""
'                    .ArchivoDesa = ""
'                    .JobBackup = txtOpc2JobBACKUP
'                    .ArchivoOut = txtOpc2FileOUT
'                    If InStr(1, txtOpc2FileOUT, ".ENM.", vbTextCompare) > 0 Then
'                        .ArchivoIntermedio = Mid(txtOpc2FileOUT, 1, InStr(1, txtOpc2FileOUT, ".ENM.", vbTextCompare) - 1) & ".DYD." & Mid(txtOpc2FileOUT, InStr(1, txtOpc2FileOUT, ".ENM.", vbTextCompare) + 5, Len(txtOpc2FileOUT))
'                    Else
'                        .ArchivoIntermedio = .ArchivoOut
'                    End If
'                Else
'                    .FechaBckUp = FECHA_NULL
'                    .ArchivoProd = ""
'                    .ArchivoDesa = ""
'                    .ArchivoOut = txtOpc2FileOUT
'                    .ArchivoIntermedio = ""
'                    .TablaBackup = ""
'                    .JobBackup = ""
'                End If
'                .Lib_Sysin = txtOpc2LibSysin
'                .Mem_Sysin = txtOpc2MemSysin
'                .Join = IIf(chkOpc2Join.Value = 1, True, False)
'                .NroTabla = 0
'                .CopyBiblioteca = ClearNull(txtOpc2FileOUT_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc2FileOUT_NMECPY)
'            Case TIPOSOL_TCOR
'                .tipo = 3
'                .Peticion = IIf(txtOpc3PeticionInterno.Text = "", 0, txtOpc3PeticionInterno.Text)
'                .ArchivoProd = ""
'                .ArchivoDesa = ""
'                .ArchivoOut = txtOpc3FileDESA
'                .ArchivoIntermedio = ""
'                .TablaBackup = ""
'                .JobBackup = ""
'                .Lib_Sysin = ""
'                .Mem_Sysin = ""
'                .Join = False
'                .NroTabla = txtOpc3FileExtract
'                .CopyBiblioteca = ClearNull(txtOpc3FileExtract_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc3FileExtract_NMECPY)
'            Case TIPOSOL_SORT
'                .tipo = IIf(chkRestore.Value = 1, 6, 4)
'                'If chkRestore.Value = 1 Then .FechaBckUp = txtOpc4FechaBackup.Text
'                .Peticion = IIf(txtOpc4PeticionInterno.Text = "", 0, txtOpc4PeticionInterno.Text)
'                '.ArchivoProd = txtOpc4FilePROD
'                '.ArchivoDesa = ""
'                '.ArchivoOut = ""
'
'
'                If chkRestore.Value = 1 Then
'                    .FechaBckUp = txtOpc4FechaBackup.Text
'                    .ArchivoProd = txtOpc4FilePROD
'                    .ArchivoDesa = ""
'                    .ArchivoOut = txtOpc4FileDESA
'                    If InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) > 0 Then
'                        .ArchivoIntermedio = Mid(txtOpc4FileDESA, 1, InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) - 1) & ".DYD." & Mid(txtOpc4FileDESA, InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) + 5, Len(txtOpc4FileDESA))
'                    Else
'                        .ArchivoIntermedio = .ArchivoOut
'                    End If
'                Else
'                    .FechaBckUp = FECHA_NULL
'                    .ArchivoProd = txtOpc4FilePROD
'                    .ArchivoDesa = ""
'                    .ArchivoOut = txtOpc4FileDESA
'                    .ArchivoIntermedio = ""
'                    '.TablaBackup = ""
'                    '.JobBackup = ""
'                End If
'
'                '.ArchivoDesa = txtOpc4FileTMP      ' del -026- a.
'                '{ add -026- a.
'                '.ArchivoOut = txtOpc4FileDESA
'
'
'                'If InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) > 0 Then
'                '    .ArchivoDesa = Mid(txtOpc4FileDESA, 1, InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) - 1) & ".DYD." & Mid(txtOpc4FileDESA, InStr(1, txtOpc4FileDESA, ".ENM.", vbTextCompare) + 5, Len(txtOpc4FileDESA))
'                'Else
'                '    .ArchivoDesa = .ArchivoOut
'                'End If
'                '}
'                '.ArchivoIntermedio = ""
'                .TablaBackup = ""
'                .JobBackup = ""
'                .Lib_Sysin = txtOpc4LibSysin
'                .Mem_Sysin = txtOpc4MemSysin
'                .Join = False
'                .NroTabla = 0
'                .CopyBiblioteca = ClearNull(txtOpc4FilePROD_BBLCPY)
'                .NameBiblioteca = ClearNull(txtOpc4FilePROD_NMECPY)
'        End Select
'    End With
'End Sub

