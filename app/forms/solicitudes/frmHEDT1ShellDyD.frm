VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmHEDT1ShellDyD 
   Caption         =   "Cat�logo de DSN para procesos de enmascaramiento de datos"
   ClientHeight    =   7560
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10260
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHEDT1ShellDyD.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   10260
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3615
      Left            =   60
      TabIndex        =   25
      Top             =   60
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   6376
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.Frame fraBotonera 
      Height          =   7515
      Left            =   8760
      TabIndex        =   26
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton cmdMarcar 
         Caption         =   "Marcar como ya utilizado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   32
         TabStop         =   0   'False
         ToolTipText     =   "Marca el archivo seleccionado como ya utilizado"
         Top             =   1125
         Width           =   1290
      End
      Begin VB.CommandButton cmdActivar 
         Caption         =   "Pasar a revisi�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   45
         TabStop         =   0   'False
         ToolTipText     =   "Quedar� pendiente de revisi�n por parte de Seg. Inform�tica"
         Top             =   3000
         Width           =   1290
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   2520
         Width           =   1290
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "C&errar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   6960
         Width           =   1290
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n actual"
         Top             =   6480
         Width           =   1290
      End
      Begin VB.CommandButton cmdCopiar 
         Caption         =   "Copiar nombres"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   34
         TabStop         =   0   'False
         ToolTipText     =   "Puede copiar en el Portapapeles los nombres de los archivos seleccionados para pegar donde necesite"
         Top             =   645
         Width           =   1290
      End
      Begin VB.CommandButton cmdCOPYS 
         Caption         =   "COPYs"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   31
         TabStop         =   0   'False
         ToolTipText     =   "Accede a la pantalla para definir los Copys del DSN seleccionado"
         Top             =   160
         Width           =   1290
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el archivo seleccionado"
         Top             =   6000
         Width           =   1290
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el archivo seleccionado"
         Top             =   5520
         Width           =   1290
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   80
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Agregar un nuevo archivo"
         Top             =   5040
         Width           =   1290
      End
      Begin VB.CheckBox chkFiltros 
         Caption         =   "Filtros"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   500
         Left            =   80
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   2040
         Width           =   1290
      End
   End
   Begin VB.Frame fraBusqueda 
      Height          =   1335
      Left            =   60
      TabIndex        =   22
      Top             =   0
      Width           =   8655
      Begin VB.ComboBox cboFiltroEstados 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4440
         Style           =   2  'Dropdown List
         TabIndex        =   55
         Top             =   600
         Width           =   3135
      End
      Begin VB.CheckBox chkSoloValidos 
         Caption         =   "V�lidos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1080
         TabIndex        =   50
         ToolTipText     =   "Solo aparecer�n en la grilla los DSNs en estado v�lido"
         Top             =   960
         Value           =   1  'Checked
         Width           =   975
      End
      Begin VB.CheckBox chkSoloDSNPropios 
         Caption         =   "Propios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   49
         ToolTipText     =   "Solo aparecer�n en la grilla los DSNs que Vd. haya dado de alta"
         Top             =   960
         Value           =   1  'Checked
         Width           =   975
      End
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "Filtrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7680
         TabIndex        =   48
         Top             =   720
         Width           =   735
      End
      Begin AT_MaskText.MaskText txtBuscarDSNNombre 
         Height          =   315
         Left            =   1560
         TabIndex        =   23
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtBuscarNomprod 
         Height          =   315
         Left            =   4440
         TabIndex        =   47
         Top             =   240
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText mskFechaAlta 
         Height          =   315
         Left            =   1560
         TabIndex        =   53
         Top             =   600
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         AutoTab         =   -1  'True
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Estado:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   3720
         TabIndex        =   54
         Top             =   660
         Width           =   555
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Fecha alta desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   120
         TabIndex        =   52
         Top             =   660
         Width           =   1290
      End
      Begin VB.Label lblHEDT1Labels 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Buscar nombre productivo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Index           =   12
         Left            =   3120
         TabIndex        =   46
         Top             =   165
         Width           =   1185
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Buscar nombre de archivo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Index           =   9
         Left            =   120
         TabIndex        =   24
         Top             =   165
         Width           =   1185
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraDatos 
      Caption         =   " Modo "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2985
      Left            =   60
      TabIndex        =   15
      Top             =   4530
      Width           =   8640
      Begin VB.CheckBox chkNoCopy 
         Caption         =   "No tiene COPY"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   2100
         Width           =   1395
      End
      Begin VB.ComboBox cmbDatosSensibles 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton cmdAppSearch 
         Caption         =   "..."
         Height          =   300
         Left            =   3600
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   960
         Width           =   300
      End
      Begin AT_MaskText.MaskText txtIdArch 
         Height          =   300
         Left            =   960
         TabIndex        =   0
         Top             =   240
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAppId 
         Height          =   300
         Left            =   3120
         TabIndex        =   5
         Top             =   960
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNombre 
         Height          =   300
         Left            =   3120
         TabIndex        =   1
         Top             =   240
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         MaxLength       =   50
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtRutina 
         Height          =   300
         Left            =   960
         TabIndex        =   2
         Top             =   600
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAppName 
         Height          =   300
         Left            =   3960
         TabIndex        =   7
         Top             =   960
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   300
         Left            =   3120
         TabIndex        =   3
         Top             =   600
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin AT_MaskText.MaskText txtBlibliotecaCopy 
         Height          =   300
         Left            =   3120
         TabIndex        =   12
         Top             =   1680
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         MaxLength       =   50
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNombreCopy 
         Height          =   300
         Left            =   3120
         TabIndex        =   13
         Top             =   2040
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtValidez 
         Height          =   300
         Left            =   960
         TabIndex        =   8
         Top             =   1320
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUtilizado 
         Height          =   300
         Left            =   960
         TabIndex        =   10
         Top             =   1680
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNomProd 
         Height          =   300
         Left            =   3120
         TabIndex        =   9
         Top             =   1320
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         MaxLength       =   100
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtMotivoSegInf 
         Height          =   465
         Left            =   1320
         TabIndex        =   44
         Top             =   2400
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   820
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Narrow"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones de Seg. Inf."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Index           =   11
         Left            =   120
         TabIndex        =   43
         Top             =   2415
         Width           =   1200
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Utilizado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   41
         Top             =   1740
         Width           =   600
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Validez"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   120
         TabIndex        =   40
         Top             =   1373
         Width           =   495
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Nombre de copy"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   1860
         TabIndex        =   39
         Top             =   2100
         Width           =   1170
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Biblioteca de copy"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   1740
         TabIndex        =   38
         Top             =   1740
         Width           =   1290
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Datos sensibles"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   390
         Index           =   5
         Left            =   120
         TabIndex        =   37
         Top             =   900
         Width           =   735
         WordWrap        =   -1  'True
      End
      Begin VB.Label NomProd 
         AutoSize        =   -1  'True
         Caption         =   "Nombre Productivo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1665
         TabIndex        =   36
         Top             =   1380
         Width           =   1365
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   2220
         TabIndex        =   20
         Top             =   653
         Width           =   810
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Rutina"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   19
         Top             =   660
         Width           =   465
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Nombre DSN"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   2130
         TabIndex        =   18
         Top             =   293
         Width           =   900
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Aplicativo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   2340
         TabIndex        =   17
         Top             =   1013
         Width           =   690
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   300
         Width           =   495
      End
   End
   Begin VB.Label lblMensaje2 
      AutoSize        =   -1  'True
      Caption         =   "Recuerde que al completar los datos no debe olvidar ingresar el Nombre Productivo."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   195
      Left            =   120
      TabIndex        =   35
      Top             =   4200
      Width           =   7095
   End
   Begin VB.Label lblMensaje 
      Caption         =   $"frmHEDT1ShellDyD.frx":058A
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   435
      Left            =   120
      TabIndex        =   21
      Top             =   3720
      Width           =   8535
   End
   Begin VB.Label lblBusqueda 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   375
      Left            =   120
      TabIndex        =   33
      Top             =   4080
      Width           =   8535
   End
End
Attribute VB_Name = "frmHEDT1ShellDyD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.10.2009 - Se agrega el manejo de formulario para perfiles de Administradores de Seg. Inform�tica
' -002- a. FJS 28.10.2009 - Se agrega un manejador global para el modo de utilizaci�n del ABM.
' -003- a. FJS 28.10.2009 - Se inhabilita la posibilidad de agregar el c�digo manualmente.
' -004- a. FJS 03.11.2009 - Se agrega la opci�n de visar simultaneamente m�s de un archivo.
' -005- a. FJS 03.11.2009 - Se agrega un bot�n para realizar una b�squeda en la grilla por nombre de DSN.
' -005- b. FJS 05.11.2009 - Se toma el nombre del estado del SP, no se esp�cifica en runtime.
' -005- c. FJS 16.11.2009 - Se cambia por el nombre de los campos directos del SP.
' -005- d. FJS 17.11.2009 - Se declara un Recordset auxiliar porque se generan problemas al reutilizar el aplRST y filtrar (si agregu� o modifiqu� datos en medio).
' -006- a. FJS 20.11.2009 - Si se trata de modificar un archivo no enviado, permito editar el nombre del archivo. Si ya ha sido enviado o esta en proceso de envio, no lo permito.
' -006- b. FJS 20.11.2009 - Si el archivo se encuentra enviado en proceso, no puede modificarse nada.
' -006- c. FJS 20.11.2009 - Se el archivo ya fu� visado por SI, no puede editarse.
' -006- d. FJS 23.11.2009 - Se inhabilita la posibilidad de eliminar un DSN si ya ha sido enviado a HOST.
' -007- a. FJS 16.12.2009 - Se agrega un indicador para aquellos archivos declarados con datos enmascarables inv�lidos (porque le falta copys y/o campos).
' -007- b. FJS 16.12.2009 - Se agrega la opci�n de "desvisar" un archivo visado.
' -007- c. FJS 16.12.2009 - Se agrega funcionalidad para SI.
' -008- a. FJS 17.06.2010 - Nuevas funcionalidades para SI: se elimina el frame con los datos de auditor�a. Se cambia el checkbox del visado por un combo (para soportar nuevo estado de rechazado).
'                           Se agrega leyenda para que SI pueda explicar porque rechaza un DSN. Adem�s, envio autom�tico de email al propietario de un archivo para que conozca los motivos del rechazo.
' -008- b. FJS 17.06.2010 - Se cambia el control checkbox por un combobox para el estado de visado. Ahora se contempan 3 estado: pendiente de visado, visado y rechazado.
' -008- c. FJS 17.06.2010 - Se agrega un nuevo campo de texto para que SI informe el motivo del rechazo al DSN.
' -008- d. FJS 17.06.2010 - Se modifica la manera de actualizar los datos de visado y dem�s para los perfiles de SI.
' -008- e. FJS 07.01.2010 - Esto en DYD no aplica. Se comenta.
' -009- a. AA  30.11.2010 - Se modifica el llamado al stored sp_UpdateHEDT101 ingresandole dos parametros mas de entrada correspondientes a las modificaciones CGM CGM-111111-1 - Sistemas Activos S.R.L..
' -010- a. AA  30.11.2010 - Se agrega un campo mas a la tabla (referente a nombre productivo).
' -011- a. AA  21.12.2010 - Se agrega el MaskedBox "txtNomProd" para el ingreso del nombre productivo, se valida como el resto de los campos(dependiendo el usuario que ingrese y la opcion que se elija).
' -012- a. FJS 07.01.2011 - Arreglo: estaban faltando las rutinas para el mantenimiento del nuevo dato de Nombre Productivo.
' -013- a. FJS 21.01.2011 - Arreglo: se agrega la obligatoriedad de carga del nuevo dato Nombre Productivo.
' -014- a. FJS 02.02.2011 - Nuevo: se cambia el control checkbox por un combobox.
' -015- a. FJS 09.02.2011 - Nuevo: se agrega checkbox para cargar en primera instancia, los DSNs propios. Luego, si el usuario lo requiere, el resto del cat�logo (performance).
' -016- a. FJS 22.03.2011 - Nuevo: se agrega men� contextual para b�squedas en la grilla.
' -017- a. FJS 25.03.2015 - Nuevo: se agrega funcionalidad para filtrar la grilla por distintos atributos.
' -018- a. FJS 26.03.2015 - Nuevo: se agrega un manejador de errores por sobrecarga de la grilla (Error 30006: Unable to allocate memory for Flexgrid).

Option Explicit

Public sDSNId As String

Private Const BIBLIOTECA_DEFAULT As String = "CMN.ABASE.CPY"
Private Const COL_DSN_ID = 0
Private Const COL_DSN_NOM = 1
Private Const COL_DSN_ENMAS = 2
Private Const COL_DSN_VB = 3
Private Const COL_NOM_VB_USUARIO = 4
Private Const COL_DSN_VB_FE = 5
Private Const COL_DSN_NOMRUT = 6
Private Const COL_DSN_FEULT = 7
Private Const COL_NOM_USUARIO = 8
Private Const COL_APP_ID = 9
Private Const COL_APP_NAME = 10
Private Const COL_DSN_DESC = 11
Private Const COL_NOM_ESTADO = 12
Private Const COL_ESTADO = 13
Private Const COL_VALIDO = 14
Private Const COL_DSN_VB_ID = 15
Private Const COL_DSN_TXT = 16
Private Const COL_DSN_NOMPROD = 17
Private Const COL_DSN_COPY = 18
Private Const COL_DSN_COPYBIBLIO = 19
Private Const COL_DSN_COPYNOMBRE = 20
Private Const COL_DSN_COD_RECURSO = 21
Private Const COL_DSN_COD_SECTOR = 22
Private Const COL_CANTIDAD = 23

Dim sAux As String                                      ' Auxiliar para tener el nombre del DSN editado
Dim lUltimaColumnaOrdenada As Long                      ' Variable auxiliar para el ordenamiento de la grilla
Dim bSorting As Boolean
Dim sOpcionSeleccionada As String
Dim cLastSearchPattern As String
Dim sSupervisor As String
Dim iCont As Long
Dim lRowSel_Ini As Long
Dim lRowSel_Fin As Long
Dim auxRST As New ADODB.Recordset

Private Sub Form_Load()
    bSorting = True
    If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, "") Then
        sSupervisor = ClearNull(aplRST.Fields!cod_recurso)
    End If
    Call HabilitarBotones(0, "NO")
    Call Inicializar
    bSorting = False
    Call CargarGrilla
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Call HabilitarBotones(0)
End Sub

Private Sub chkFiltros_Click()
    With grdDatos
        If chkFiltros.Value = 1 Then
            .Height = 2255
            .Top = 1440
            Call HabilitarBotones(3)
        Else
            .Height = 3615
            .Top = 80      ' 120
            Call HabilitarBotones(4)
        End If
    End With
End Sub

Private Sub Inicializar()
    Call Status("Inicializando...")
    Me.Top = 0
    Me.Left = 0
    Call LockProceso(True)
    DoEvents
    Call setHabilCtrl(txtIdArch, "DIS")
    Call setHabilCtrl(txtAppName, "DIS")
    Call setHabilCtrl(txtUtilizado, "DIS")
    Call setHabilCtrl(cmdModificar, "DIS")
    Call setHabilCtrl(cmdEliminar, "DIS")
    '{ add -014- a.
    With cmbDatosSensibles
        .Clear
        .AddItem "Si" & Space(100) & "||S"
        .AddItem "No" & Space(100) & "||N"
        .ListIndex = -1
    End With
    '}
    '{ add -018- a.
    chkSoloDSNPropios.Value = 1
    chkSoloValidos.Value = 1
    
    With cboFiltroEstados
        .Clear
        .AddItem "* Todos" & ESPACIOS & "||NULL"
        .AddItem "Borrador" & ESPACIOS & "||B"
        
        .AddItem "Pendiente revisi�n" & ESPACIOS & "||N"
        .AddItem "A revisar DyD" & ESPACIOS & "||D"
        .AddItem "Pendiente pos-revisi�n" & ESPACIOS & "||P"
        .AddItem "Rechazado" & ESPACIOS & "||R"
        .AddItem "Visado" & ESPACIOS & "||S"
        .ListIndex = 0
    End With
    '}
    Call IniciarScroll(grdDatos)
    Call LockProceso(False)
End Sub

Private Sub Inicializar_Grilla()
    With grdDatos
        bSorting = True
        .visible = False
        .Clear
        .Font.name = "Tahoma"
        .Font.Size = 8
        .cols = COL_CANTIDAD
        .Rows = 1
        .TextMatrix(0, COL_DSN_ID) = "C�digo": .ColWidth(COL_DSN_ID) = 1000
        .TextMatrix(0, COL_DSN_NOM) = "Nombre del archivo": .ColWidth(COL_DSN_NOM) = 3500
        .TextMatrix(0, COL_DSN_ENMAS) = "DS": .ColWidth(COL_DSN_ENMAS) = 500
        .TextMatrix(0, COL_DSN_NOMRUT) = "Rutina": .ColWidth(COL_DSN_NOMRUT) = 1000
        .TextMatrix(0, COL_DSN_FEULT) = "Ult. mod.": .ColWidth(COL_DSN_FEULT) = 1200
        .TextMatrix(0, COL_NOM_USUARIO) = "Propietario": .ColWidth(COL_NOM_USUARIO) = 1500
        .TextMatrix(0, COL_DSN_VB) = "Visado": .ColWidth(COL_DSN_VB) = 2000
        .TextMatrix(0, COL_NOM_VB_USUARIO) = "Por": .ColWidth(COL_NOM_VB_USUARIO) = 1200
        .TextMatrix(0, COL_DSN_VB_FE) = "Fch. Visado": .ColWidth(COL_DSN_VB_FE) = 1200
        .TextMatrix(0, COL_APP_ID) = "Aplic.": .ColWidth(COL_APP_ID) = 800
        .TextMatrix(0, COL_APP_NAME) = "Nombre del aplicativo": .ColWidth(COL_APP_NAME) = 2000
        .TextMatrix(0, COL_DSN_DESC) = "Descripci�n del archivo": .ColWidth(COL_DSN_DESC) = 4000
        .TextMatrix(0, COL_NOM_ESTADO) = "Estado": .ColWidth(COL_NOM_ESTADO) = 1400
        .TextMatrix(0, COL_ESTADO) = "": .ColWidth(COL_ESTADO) = 0
        .TextMatrix(0, COL_VALIDO) = "V�lido": .ColWidth(COL_VALIDO) = 600: .ColAlignment(COL_VALIDO) = flexAlignCenterCenter
        .TextMatrix(0, COL_DSN_VB_ID) = "dsn_vb": .ColWidth(COL_DSN_VB_ID) = 0
        .TextMatrix(0, COL_DSN_TXT) = "Motivo de rechazo": .ColWidth(COL_DSN_TXT) = 3000
        .TextMatrix(0, COL_DSN_NOMPROD) = "Nombre productivo": .ColWidth(COL_DSN_NOMPROD) = 5000
        .TextMatrix(0, COL_DSN_COPY) = "": .ColWidth(COL_DSN_COPY) = 0
        .TextMatrix(0, COL_DSN_COPYBIBLIO) = "Biblioteca de COPY": .ColWidth(COL_DSN_COPYBIBLIO) = 3000
        .TextMatrix(0, COL_DSN_COPYNOMBRE) = "Nombre de COPY": .ColWidth(COL_DSN_COPYNOMBRE) = 3000
        .TextMatrix(0, COL_DSN_COD_RECURSO) = "": .ColWidth(COL_DSN_COD_RECURSO) = 0
        .TextMatrix(0, COL_DSN_COD_SECTOR) = "": .ColWidth(COL_DSN_COD_SECTOR) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Public Sub CargarGrilla()
    '{ add -018- a.
    On Error GoTo ErrHandler
    
    Dim lRecCount As Long
    Dim bGridOverflow As Boolean
    Dim bRegenerate As Boolean
    Dim i As Long, j As Long
    
    bRegenerate = False
    bGridOverflow = False
    '}
    
    'Dim i As Long          ' del -018- a.
    'Debug.Print "CargarGrilla (" & Now & ")"
    
    i = 0
    Call Inicializar_Grilla
    Call Puntero(True)
    Call Status("Cargando...")
    If sp_GetHEDT001(Null, ClearNull(txtBuscarDSNNombre), ClearNull(txtBuscarNomprod), IIf(chkSoloDSNPropios.Value = 1, "S", "N"), IIf(IsDate(mskFechaAlta.DateValue), mskFechaAlta.DateValue, Null), CodigoCombo(cboFiltroEstados, True)) Then     ' upd -015- a.      ' upd -017- a.
        With grdDatos
            Do While Not aplRST.EOF
                If chkSoloValidos.Value = 0 Or (chkSoloValidos.Value = 1 And Mid(ClearNull(aplRST.Fields!valido), 1, 1) = "S") Then
                    i = i + 1
                    .Rows = .Rows + 1
                    '{ add -018- a.
                    If bGridOverflow Then
                        Exit Do
                    End If
                    '}
                    .TextMatrix(.Rows - 1, COL_DSN_ID) = ClearNull(aplRST.Fields!dsn_id)
                    .TextMatrix(.Rows - 1, COL_DSN_NOM) = ClearNull(aplRST.Fields!dsn_nom)
                    .TextMatrix(.Rows - 1, COL_DSN_ENMAS) = ClearNull(aplRST.Fields!dsn_enmas)
                    .TextMatrix(.Rows - 1, COL_DSN_NOMRUT) = ClearNull(aplRST.Fields!dsn_nomrut)
                    .TextMatrix(.Rows - 1, COL_DSN_FEULT) = Format(ClearNull(aplRST.Fields!dsn_feult), "yyyy/mm/dd")
                    .TextMatrix(.Rows - 1, COL_NOM_USUARIO) = ClearNull(aplRST.Fields!nom_usuario)
                    .TextMatrix(.Rows - 1, COL_DSN_VB) = ClearNull(aplRST.Fields!dsn_vb)
                    .TextMatrix(.Rows - 1, COL_NOM_VB_USUARIO) = ClearNull(aplRST.Fields!nom_vb_usuario)
                    .TextMatrix(.Rows - 1, COL_DSN_VB_FE) = Format(ClearNull(aplRST.Fields!dsn_vb_fe), "yyyy/mm/dd")
                    .TextMatrix(.Rows - 1, COL_APP_ID) = ClearNull(aplRST.Fields!app_id)
                    .TextMatrix(.Rows - 1, COL_APP_NAME) = ClearNull(aplRST.Fields!app_name)
                    .TextMatrix(.Rows - 1, COL_DSN_DESC) = ClearNull(aplRST.Fields!dsn_desc)
                    .TextMatrix(.Rows - 1, COL_NOM_ESTADO) = ClearNull(aplRST.Fields!nom_estado)
                    .TextMatrix(.Rows - 1, COL_ESTADO) = ClearNull(aplRST.Fields!Estado)
                    .TextMatrix(.Rows - 1, COL_VALIDO) = ClearNull(aplRST.Fields!valido)
                    .TextMatrix(.Rows - 1, COL_DSN_VB_ID) = ClearNull(aplRST.Fields!dsn_vb_id)
                    .TextMatrix(.Rows - 1, COL_DSN_TXT) = ClearNull(aplRST.Fields!dsn_txt)
                    .TextMatrix(.Rows - 1, COL_DSN_NOMPROD) = ClearNull(aplRST.Fields!dsn_nomprod)
                    .TextMatrix(.Rows - 1, COL_DSN_COPY) = ClearNull(aplRST.Fields!dsn_cpy)
                    .TextMatrix(.Rows - 1, COL_DSN_COPYBIBLIO) = ClearNull(aplRST.Fields!dsn_cpybbl)
                    .TextMatrix(.Rows - 1, COL_DSN_COPYNOMBRE) = ClearNull(aplRST.Fields!dsn_cpynom)
                    .TextMatrix(.Rows - 1, COL_DSN_COD_RECURSO) = ClearNull(aplRST.Fields!cod_recurso)
                    .TextMatrix(.Rows - 1, COL_DSN_COD_SECTOR) = ClearNull(aplRST.Fields!cod_sector)
                    If Mid(ClearNull(aplRST.Fields!valido), 1, 1) = "N" Then
                        Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorDarkGrey)
                    End If
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
    If i > 0 Then
        Call Status(i & " dsn(s).")
    Else
        Call Status("Listo.")
    End If
    Me.Tag = aplRST.RecordCount     ' Guardo el total de registros del cat�logo
    grdDatos.visible = True
    bSorting = False
    Call Puntero(False)
    If chkFiltros.Value = 0 Then Call MostrarSeleccion(False)
Exit Sub
'{ add -018- a.
ErrHandler:
    Dim sErrMenssage As String
    
    Select Case Err.Number
        Case 30006      ' No queda memoria para seguir agregando filas al MSFlexGrid
            bGridOverflow = True
            sErrMenssage = "Se alcanz� el l�mite de registros que la grilla puede mostrar." & vbCrLf & _
                            "Se recomienda volver a consultar utilizando alguno de los filtros disponibles." & vbCrLf
            'sErrMenssage = "La consulta que intenta generar es demasiado grande (excede la cantidad de " & Trim(Format(lRecCount, "###,###,###")) & " filas)." & vbCrLf
            sErrMenssage = sErrMenssage & "�Desea volver a generar la consulta agregando restricciones para devolver una menor cantidad de filas?"
            If MsgBox(sErrMenssage, vbInformation + vbYesNo, "Consulta demasiado grande") = vbYes Then
                bRegenerate = True
                Resume Next
            Else
                bRegenerate = False
                MsgBox "IMPORTANTE" & vbCrLf & "La informaci�n mostrada en pantalla ser� parcial. No se ha podido cargar todo el conjunto de resultados esperado.", vbExclamation + vbOKOnly, "Resultado parcial"
                Me.Caption = Me.Tag & " (" & Trim(CStr(Format(lRecCount, "###,###,###"))) & " filas)"
                Resume Next
            End If
        Case 3265       ' No se encontr� el elemento en la colecci�n que corresponde con el nombre o el ordinal pedido.
            Resume Next
        Case Else
            Call Puntero(False)
            MsgBox Err.DESCRIPTION & " (" & Err.Number & ")", vbExclamation + vbOKOnly
    End Select
'}
End Sub

Private Sub cmdAgregar_Click()
    Dim bContinuar As Boolean
    
    bContinuar = True
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    ' Control para que el usuario no tenga entradas pendientes de correcci�n
    If sp_GetHEDT001(Null, Null, Null, "S", Null, Null) Then     ' upd -017- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!dsn_vb_id) = "D" Then
                bContinuar = False
                Exit Do
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    If bContinuar Then
        frmCLISTHelp.Show 1                 ' Ayuda inicial
        frmHEDT1ShellDyDSel.Show 1
        If frmHEDT1ShellDyDSel.sModo = "ADD" Then
            sOpcionSeleccionada = "A"
            Call HabilitarBotones(1, False)
        ElseIf frmHEDT1ShellDyDSel.sModo = "CANCEL" Then
            Call HabilitarBotones(0, False)
        Else
            If ClearNull(frmHEDT1ShellDyDSel.sDSN_ID) <> "" Then
                If Find(grdDatos, 1, 0, frmHEDT1ShellDyDSel.sDSN_ID) Then
                Else                                    ' Puede no encontrarlo porque el DSN no es propio
                    chkSoloDSNPropios.Value = 0         ' Fuerzo a buscar en toda la grilla
                    'Call chkSoloDSNPropios_Click       ' del -017- a.
                    Call CargarGrilla                   ' add -017- a.
                    Call Find(grdDatos, 1, 0, frmHEDT1ShellDyDSel.sDSN_ID)
                End If
            End If
        End If
    Else
        MsgBox "Antes de poder agregar nuevos DSN al cat�logo," & vbCrLf & _
               "debe corregir las entradas pendientes (A revisar DyD)", vbExclamation + vbOKOnly
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdModificar_Click()
    If grdDatos.TextMatrix(grdDatos.RowSel, 13) = "En proceso..." Then
        MsgBox "El archivo se encuentra en proceso de actualizaci�n en HOST. No puede ser modificado.", vbExclamation + vbOKOnly, "Actualizando cat�logo HOST"
    Else
        If Not LockProceso(True) Then
            Exit Sub
        End If
        sOpcionSeleccionada = "M"
        Call HabilitarBotones(1, False)
        Call LockProceso(False)
    End If
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    Call LockProceso(False)
End Sub

Private Sub cmdConfirmar_Click()
    Dim bContinuar As Boolean
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                Call Puntero(True)
                If sp_InsertHEDT001(txtIdArch, txtNombre, txtDescripcion, glLOGIN_ID_REEMPLAZO, CodigoCombo(cmbDatosSensibles, True), txtRutina, txtAppId, UCase(txtNomProd), IIf(chkNoCopy.Value = 1, "S", "N"), txtBlibliotecaCopy, txtNombreCopy) Then ' upd -012- a. Se agreg� el par�metro txtNomProd              ' add -014- a.
                    'If sp_GetHEDT001(Null, txtNombre, Null, Null, Null, Null) Then  ' upd -017- a.
                    If sp_GetHEDT001a(txtNombre) Then
                        txtIdArch = ClearNull(aplRST.Fields!dsn_id)
                    End If
                    If CodigoCombo(cmbDatosSensibles, True) = "S" Then  ' add -014- a.
                        Call Puntero(False)
                        frmHEDT2Shell.sDSNId = txtIdArch
                        frmHEDT2Shell.Show
                    Else
                        Call CargarGrilla
                        Call GridSeek(Me, grdDatos, COL_DSN_ID, txtIdArch, True)
                    End If
                    Call HabilitarBotones(0, "A")
                End If
            End If
        Case "M"
            bContinuar = True
            If CamposObligatorios Then          ' Si antes ten�a Datos Sensibles y ahora no, advertir que se perderan los datos de copys y campos
                If cmbDatosSensibles.Tag = "S" And _
                   CodigoCombo(cmbDatosSensibles, True) = "N" And _
                   grdDatos.TextMatrix(grdDatos.RowSel, COL_VALIDO) = "Si" Then
                        If MsgBox("Al realizar este cambio, perder� los campos " & vbCrLf & _
                                  "sensibles definidos anteriormente." & vbCrLf & _
                                  "�Desea continuar?", vbExclamation + vbYesNo, "Advertencia") = vbYes Then
                            Call sp_DeleteHEDT002(txtIdArch, Null)
                        Else
                            bContinuar = False
                        End If
                End If
                If bContinuar Then
                    Call Puntero(True)
                    If sp_UpdateHEDT001(txtIdArch, txtNombre, txtDescripcion, glLOGIN_ID_REEMPLAZO, CodigoCombo(cmbDatosSensibles, True), txtRutina, txtAppId, UCase(txtNomProd), IIf(chkNoCopy.Value = 1, "S", "N"), ClearNull(txtBlibliotecaCopy), ClearNull(txtNombreCopy)) Then   ' upd -012- a. Se agreg� el par�metro txtNomProd              ' add -014- a.
                        'If grdDatos.TextMatrix(grdDatos.RowSel, COL_DSN_VB_ID) = "D" Then
                        '    Call sp_UpdateHEDT001Field(txtIdArch, "DSN_VB", "P", Null, Null)
                        'End If
                        With grdDatos
                            If .RowSel > 0 Then
                                If sp_GetHEDT001(txtIdArch, Null, Null, Null, Null, Null) Then    ' upd -015- a.  ' upd -017- a.
                                    .TextMatrix(.RowSel, COL_DSN_NOM) = ClearNull(aplRST.Fields!dsn_nom)
                                    .TextMatrix(.RowSel, COL_DSN_ENMAS) = ClearNull(aplRST.Fields!dsn_enmas)
                                    .TextMatrix(.RowSel, COL_DSN_NOMRUT) = ClearNull(aplRST.Fields!dsn_nomrut)
                                    .TextMatrix(.RowSel, COL_DSN_FEULT) = Format(ClearNull(aplRST.Fields!dsn_feult), "yyyy/mm/dd")
                                    .TextMatrix(.RowSel, COL_NOM_USUARIO) = ClearNull(aplRST.Fields!nom_usuario)
                                    .TextMatrix(.RowSel, COL_DSN_VB) = ClearNull(aplRST.Fields!dsn_vb)
                                    .TextMatrix(.RowSel, COL_NOM_VB_USUARIO) = ClearNull(aplRST.Fields!nom_vb_usuario)
                                    .TextMatrix(.RowSel, COL_DSN_VB_FE) = Format(ClearNull(aplRST.Fields!dsn_vb_fe), "yyyy/mm/dd")
                                    .TextMatrix(.RowSel, COL_APP_ID) = ClearNull(aplRST.Fields!app_id)
                                    .TextMatrix(.RowSel, COL_APP_NAME) = ClearNull(aplRST.Fields!app_name)
                                    .TextMatrix(.RowSel, COL_DSN_DESC) = ClearNull(aplRST.Fields!dsn_desc)
                                    .TextMatrix(.RowSel, COL_NOM_ESTADO) = ClearNull(aplRST.Fields!nom_estado)
                                    .TextMatrix(.RowSel, COL_ESTADO) = ClearNull(aplRST.Fields!Estado)
                                    .TextMatrix(.RowSel, COL_VALIDO) = ClearNull(aplRST.Fields!valido)
                                    .TextMatrix(.RowSel, COL_DSN_VB_ID) = ClearNull(aplRST.Fields!dsn_vb_id)
                                    .TextMatrix(.RowSel, COL_DSN_TXT) = ClearNull(aplRST.Fields!dsn_txt)
                                    .TextMatrix(.RowSel, COL_DSN_NOMPROD) = ClearNull(aplRST.Fields!dsn_nomprod)         ' add -012- a.
                                    .TextMatrix(.RowSel, COL_DSN_COPY) = ClearNull(aplRST.Fields!dsn_cpy)
                                    .TextMatrix(.RowSel, COL_DSN_COPYBIBLIO) = ClearNull(aplRST.Fields!dsn_cpybbl)
                                    .TextMatrix(.RowSel, COL_DSN_COPYNOMBRE) = ClearNull(aplRST.Fields!dsn_cpynom)
                                    .TextMatrix(.RowSel, COL_DSN_COD_RECURSO) = ClearNull(aplRST.Fields!cod_recurso)
                                    .TextMatrix(.RowSel, COL_DSN_COD_SECTOR) = ClearNull(aplRST.Fields!cod_sector)
                                End If
                            End If
                        End With
                    End If
                End If
                Call HabilitarBotones(0, False)
            End If
        Case "E"
            If grdDatos.TextMatrix(grdDatos.RowSel, 13) = "A" Then
                If MsgBox("�Esta seguro de eliminar definitivamente el DSN de la base?", vbQuestion + vbYesNo, "Eliminar DSN") = vbYes Then
                    If sp_DeleteHEDT001(txtIdArch) Then
                        Call HabilitarBotones(0, "")
                        grdDatos.RemoveItem (grdDatos.RowSel)
                    End If
                End If
            Else
                MsgBox "No puede eliminar este archivo, porque ya fu� utilizado.", vbExclamation + vbOKOnly, "Eliminar DSN"
                Call HabilitarBotones(0)
            End If
    End Select
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Len(ClearNull(txtNombre)) = 0 Then
        MsgBox "Debe especificar el nombre del archivo.", vbExclamation + vbOKOnly, "Nombre de archivo"
        CamposObligatorios = False
        Exit Function
    End If
    If Len(ClearNull(txtNombre)) > 50 Then
        MsgBox "La longitud del nombre del archivo no debe ser mayor a 50 posiciones.", vbExclamation + vbOKOnly, "Nombre de archivo"
        CamposObligatorios = False
        Exit Function
    End If
    If Not ValidarCalificadores(txtNombre) Then
        CamposObligatorios = False
        Exit Function
    End If
    If Len(ClearNull(txtDescripcion)) = 0 Then
        MsgBox "Debe especificar una descripci�n para el archivo.", vbExclamation + vbOKOnly, "Descripci�n de archivo"
        CamposObligatorios = False
        Exit Function
    End If
    '{ add -013- a.
    If Len(ClearNull(txtNomProd)) = 0 Then
        MsgBox "Debe especificar el nombre productivo del archivo.", vbExclamation + vbOKOnly, "Nombre productivo de archivo"
        CamposObligatorios = False
        Exit Function
    End If
    If Len(ClearNull(txtNomProd)) > 50 Then
        MsgBox "La longitud del nombre productivo del archivo no debe ser mayor a 50 posiciones.", vbExclamation + vbOKOnly, "Nombre productivo de archivo"
        CamposObligatorios = False
        Exit Function
    End If
    '}
    If Len(ClearNull(txtNomProd)) > 0 Then
        If chkNoCopy.Value = 0 Then
            If Len(ClearNull(txtBlibliotecaCopy)) = 0 Then
                MsgBox "Debe ingresar el nombre de la biblioteca de COPY.", vbExclamation + vbOKOnly, "Nombre de biblioteca de COPY"
                CamposObligatorios = False
                txtBlibliotecaCopy.SetFocus
                Exit Function
            End If
            If Len(ClearNull(txtBlibliotecaCopy)) > 50 Then
                MsgBox "La longitud del nombre de la biblioteca de COPY no debe ser mayor a 50 posiciones.", vbExclamation + vbOKOnly, "Nombre de biblioteca de COPY"
                CamposObligatorios = False
                txtBlibliotecaCopy.SetFocus
                Exit Function
            End If
        End If
    End If
    If Len(ClearNull(txtNomProd)) > 0 Then
        If chkNoCopy.Value = 0 Then
            If Len(ClearNull(txtNombreCopy)) = 0 Then
                MsgBox "Debe ingresar el nombre de COPY.", vbExclamation + vbOKOnly, "Nombre de COPY"
                CamposObligatorios = False
                txtNombreCopy.SetFocus
                Exit Function
            End If
            If Len(ClearNull(txtNombreCopy)) > 50 Then
                MsgBox "La longitud del nombre de COPY no debe ser mayor a 50 posiciones.", vbExclamation + vbOKOnly, "Nombre de COPY"
                CamposObligatorios = False
                txtNombreCopy.SetFocus
                Exit Function
            End If
        End If
    End If
End Function

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdCopiar.visible = True
            chkSoloValidos.Enabled = True
            cmdCopiar.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdCOPYS.Enabled = True
            cmdEliminar.Enabled = True
            Call setHabilCtrl(cmdExportar, "NOR")
            cmdMarcar.Enabled = True
            cmdConfirmar.Enabled = False
            chkFiltros.Enabled = True
            cmdCerrar.Caption = "C&errar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            chkSoloDSNPropios.Enabled = True
            Call setHabilCtrl(txtIdArch, "DIS")
            Call setHabilCtrl(txtNombre, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtAppId, "DIS")
            Call setHabilCtrl(txtAppName, "DIS")
            Call setHabilCtrl(cmdAppSearch, "DIS")
            Call setHabilCtrl(cmbDatosSensibles, "DIS")     ' add -014- a.
            Call setHabilCtrl(txtNomProd, "DIS")            ' add -012- a.
            Call setHabilCtrl(chkNoCopy, "DIS")
            Call setHabilCtrl(txtBlibliotecaCopy, "DIS")
            Call setHabilCtrl(txtNombreCopy, "DIS")
            Call setHabilCtrl(txtValidez, "DIS")
            Call setHabilCtrl(txtMotivoSegInf, "DIS")
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdMarcar.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            chkSoloValidos.Enabled = False
            cmdCopiar.Enabled = False
            cmdCOPYS.Enabled = False
            cmdActivar.Enabled = False
            chkFiltros.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            chkSoloDSNPropios.Enabled = False
            Select Case sOpcionSeleccionada
                Case "A"
                    Call setHabilCtrl(cmdExportar, "DIS")
                    Call setHabilCtrl(txtNombre, "NOR")
                    Call setHabilCtrl(txtNomProd, "NOR")                ' add -011- a.
                    Call setHabilCtrl(txtRutina, "NOR")
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(txtAppId, "NOR")
                    Call setHabilCtrl(chkNoCopy, "NOR")
                    Call setHabilCtrl(txtBlibliotecaCopy, "NOR")
                    Call setHabilCtrl(txtNombreCopy, "NOR")
                    Call setHabilCtrl(cmbDatosSensibles, "NOR")         ' add -014- a.
                    Call setHabilCtrl(cmdAppSearch, "NOR")
                    fraDatos.Caption = " AGREGAR "
                    txtIdArch = ""
                    txtRutina = ""
                    txtAppId = ""
                    txtAppName = ""
                    cmbDatosSensibles.ListIndex = PosicionCombo(cmbDatosSensibles, "S", True)   ' add -014- a.
                    txtNombre = ""
                    txtNomProd = ""                                     ' add -011- a.
                    txtDescripcion = ""
                    fraDatos.Enabled = True
                    chkNoCopy.Value = 0
                    txtBlibliotecaCopy = BIBLIOTECA_DEFAULT
                    txtNombreCopy = ""
                    txtValidez = ""
                    txtMotivoSegInf = ""
                    txtNombre.SetFocus                                  ' add -003- a.
                Case "M"
                    Call setHabilCtrl(cmdExportar, "DIS")
                    Call setHabilCtrl(txtNombre, "DIS")
                    Call setHabilCtrl(txtRutina, "NOR")
                    Call setHabilCtrl(txtDescripcion, "NOR")
                    Call setHabilCtrl(txtAppId, "NOR")
                    Call setHabilCtrl(cmdAppSearch, "NOR")
                    Call setHabilCtrl(cmbDatosSensibles, "NOR")     ' add -014- a.
                    Call setHabilCtrl(txtNomProd, "NOR")            ' add -012- a.
                    Call setHabilCtrl(chkNoCopy, "NOR")
                    If chkNoCopy.Value = 1 Then
                        Call setHabilCtrl(txtBlibliotecaCopy, "DIS")
                        Call setHabilCtrl(txtNombreCopy, "DIS")
                    Else
                        Call setHabilCtrl(txtBlibliotecaCopy, "NOR")
                        Call setHabilCtrl(txtNombreCopy, "NOR")
                    End If
                    fraDatos.Caption = " MODIFICAR "
                    txtIdArch.Enabled = False
                    fraDatos.Enabled = True
                    txtRutina.SetFocus
                Case "E"
                    Call setHabilCtrl(cmdExportar, "DIS")
                    fraDatos.Caption = " ELIMINAR "
                    fraDatos.Enabled = False
            End Select
        Case 2
            fraDatos.Caption = ""
            grdDatos.Enabled = True
            cmdCopiar.visible = True
            cmdAgregar.visible = True                           ' upd -008- d. Antes False
            cmdAgregar.Enabled = False                          ' add -008- d.
            cmdModificar.visible = True                         ' upd -008- d. Antes False
            cmdCOPYS.Enabled = True
            Call setHabilCtrl(cmdExportar, "NOR")
            cmdCopiar.Enabled = True
            cmdMarcar.Enabled = False
            cmdEliminar.visible = False
            cmdConfirmar.visible = True                         ' upd -008- d. Antes False
            chkFiltros.Enabled = True
            cmdCerrar.Caption = "C&errar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            Call setHabilCtrl(txtIdArch, "DIS")
            Call setHabilCtrl(txtNombre, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(cmdAppSearch, "DIS")
            Call setHabilCtrl(txtAppId, "DIS")
            Call setHabilCtrl(txtAppName, "DIS")
            Call setHabilCtrl(cmbDatosSensibles, "DIS")
            Call setHabilCtrl(txtNomProd, "DIS")                ' add -011- a.
            Call setHabilCtrl(chkNoCopy, "DIS")
            Call setHabilCtrl(txtBlibliotecaCopy, "DIS")
            Call setHabilCtrl(txtNombreCopy, "DIS")
            chkSoloDSNPropios.Enabled = True
            chkSoloValidos.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
        Case 3
            Call setHabilCtrl(cmdCOPYS, "DIS")
            Call setHabilCtrl(cmdCopiar, "DIS")
            Call setHabilCtrl(cmdMarcar, "DIS")
            Call setHabilCtrl(cmdExportar, "DIS")
            Call setHabilCtrl(cmdActivar, "DIS")
            Call setHabilCtrl(cmdAgregar, "DIS")
            Call setHabilCtrl(cmdModificar, "DIS")
            Call setHabilCtrl(cmdEliminar, "DIS")
            'Call setHabilCtrl(cmdConfirmar, "DIS")
            Call setHabilCtrl(cmdCerrar, "DIS")
        Case 4
            Call setHabilCtrl(cmdCOPYS, "NOR")
            Call setHabilCtrl(cmdCopiar, "NOR")
            Call setHabilCtrl(cmdMarcar, "NOR")
            Call setHabilCtrl(cmdExportar, "NOR")
            Call setHabilCtrl(cmdActivar, "NOR")
            Call setHabilCtrl(cmdAgregar, "NOR")
            Call setHabilCtrl(cmdModificar, "NOR")
            Call setHabilCtrl(cmdEliminar, "NOR")
            'Call setHabilCtrl(cmdConfirmar, "NOR")
            Call setHabilCtrl(cmdCerrar, "NOR")
            Call MostrarSeleccion
    End Select
    Call LockProceso(False)
End Sub

Public Function MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    If Not bSorting Then
        bSorting = True
        Call setHabilCtrl(cmdActivar, "DIS")
        Call setHabilCtrl(cmdModificar, "DIS")
        Call setHabilCtrl(cmdEliminar, "DIS")
        With grdDatos
            If .RowSel > 0 Then
                If .TextMatrix(.RowSel, COL_DSN_ID) <> "" Then
                    txtIdArch = ClearNull(.TextMatrix(.RowSel, COL_DSN_ID))
                    txtNombre = ClearNull(.TextMatrix(.RowSel, COL_DSN_NOM))
                    txtRutina = ClearNull(.TextMatrix(.RowSel, COL_DSN_NOMRUT))
                    txtDescripcion = ClearNull(.TextMatrix(.RowSel, COL_DSN_DESC))
                    txtAppId = ClearNull(.TextMatrix(.RowSel, COL_APP_ID))
                    txtAppName = ClearNull(.TextMatrix(.RowSel, COL_APP_NAME))
                    cmbDatosSensibles.ListIndex = PosicionCombo(cmbDatosSensibles, Left(ClearNull(.TextMatrix(.RowSel, COL_DSN_ENMAS)), 1), True)   ' add -014- a.
                    cmbDatosSensibles.Tag = Left(ClearNull(.TextMatrix(.RowSel, COL_DSN_ENMAS)), 1)         ' Guardo el valor original, para en caso de modificaci�n, saber que hacer
                    txtNomProd = ClearNull(.TextMatrix(.RowSel, COL_DSN_NOMPROD))
                    chkNoCopy.Value = IIf(ClearNull(.TextMatrix(.RowSel, COL_DSN_COPY)) = "S", 1, 0)
                    txtBlibliotecaCopy = ClearNull(.TextMatrix(.RowSel, COL_DSN_COPYBIBLIO))
                    txtNombreCopy = ClearNull(.TextMatrix(.RowSel, COL_DSN_COPYNOMBRE))
                    txtValidez = ClearNull(.TextMatrix(.RowSel, COL_VALIDO))
                    txtUtilizado = IIf(ClearNull(.TextMatrix(.RowSel, COL_ESTADO)) = "A", "No", "Si")
                    txtMotivoSegInf = ClearNull(.TextMatrix(.RowSel, COL_DSN_TXT))
                    ' Solo el propietario o el supervisor podr� modificar el DSN. En su defecto, CyPTF podr� pasarlo a Rechazado
                    If ClearNull(.TextMatrix(.RowSel, COL_DSN_COD_RECURSO)) = glLOGIN_ID_REEMPLAZO Then
                        If InStr(1, "B|D|", .TextMatrix(.RowSel, COL_DSN_VB_ID), vbTextCompare) > 0 Then
                            Call setHabilCtrl(cmdModificar, "NOR")
                            Call setHabilCtrl(cmdEliminar, "NOR")
                            If .TextMatrix(.RowSel, COL_VALIDO) = "Si" Then
                                Call setHabilCtrl(cmdActivar, "NOR")
                            End If
                        End If
                    Else
                        If ClearNull(sSupervisor) = glLOGIN_ID_REEMPLAZO And _
                           ClearNull(.TextMatrix(.RowSel, COL_DSN_COD_SECTOR)) = glLOGIN_Sector Then
                            If InStr(1, "B|D|", .TextMatrix(.RowSel, COL_DSN_VB_ID), vbTextCompare) > 0 Then
                                Call setHabilCtrl(cmdModificar, "NOR")
                                Call setHabilCtrl(cmdEliminar, "NOR")
                                If .TextMatrix(.RowSel, COL_VALIDO) = "Si" Then
                                    Call setHabilCtrl(cmdActivar, "NOR")
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End With
        bSorting = False
    End If
End Function

Private Sub Form_Resize()
    Me.Top = 0
    Me.Left = 0
    Me.Height = 8070
    Me.Width = 10380
End Sub

Private Sub grdDatos_Click()
    If grdDatos.MouseRow = 0 Then
        bSorting = True
        Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
        bSorting = False
    End If
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A", "M"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            If glCatalogoENM <> "ABM" Then
                glCatalogoNombre = grdDatos.TextMatrix(grdDatos.RowSel, 1)
            End If
            Set auxRST = Nothing    ' add -005- d.
            Unload Me
    End Select
    LockProceso (False)
End Sub

Private Sub grdDatos_RowColChange()
    If Not bSorting Then
        Call MostrarSeleccion
    End If
End Sub

Private Sub cmdAppSearch_Click()
    Dim vRetorno() As String
    frmSelAplicativo.cAmbiente = "H"
    frmSelAplicativo.Show 1
    If ParseString(vRetorno, glAuxRetorno, ":") > 0 Then
        txtAppId = vRetorno(1)
        'txtAppName = Mid(vRetorno(2), 1, InStr(1, vRetorno(2), "|", vbTextCompare) - 1)
        txtAppName = ClearNull(vRetorno(2))
    End If
End Sub

Private Sub txtAppId_LostFocus()
    If Len(txtAppId) > 0 And Trim(txtAppId) <> "" Then
        If sp_GetAplicativo(txtAppId, Null, "S") Then
            txtAppName = ClearNull(aplRST.Fields!app_nombre)
        Else
            MsgBox "El aplicativo ingresado no existe o se encuentra inhabilitado.", vbExclamation + vbOKOnly, "Aplicativo inexistente o inv�lido"
            txtAppId.SetFocus
        End If
    Else
        txtAppName = ""
    End If
End Sub

Private Sub cmbDatosSensibles_Click()
    If CodigoCombo(cmbDatosSensibles, True) = "N" Then
        chkNoCopy.Value = 1
    Else
        chkNoCopy.Value = 0
    End If
End Sub

Private Sub cmdCOPYS_Click()
    If Len(txtIdArch) > 0 Then
        frmHEDT2Shell.sDSNId = txtIdArch
        frmHEDT2Shell.Show
    Else
        MsgBox "Debe seleccionar un DSN para definir los COPYs del mismo.", vbExclamation + vbOKOnly, "Sin DSN seleccionado"
    End If
End Sub

Private Sub cmdMarcar_Click()
    Dim i As Long
    Const cMensaje1 = "�Confirma el cambio de marca del archivo seleccionado?"
    Const cMensaje2 = "�Confirma el cambio de marca de los archivos seleccionados?"
    
    If grdDatos.Rows > 1 Then
        If MsgBox(IIf(lRowSel_Ini <> lRowSel_Fin, cMensaje2, cMensaje1), vbQuestion + vbYesNo, "Marcar archivos") = vbYes Then
            Call Status("Procesando...")
            Call Puntero(True)
            For i = lRowSel_Ini To lRowSel_Fin      ' Genero las marcas
                If Not sp_UpdateHEDT001Field(grdDatos.TextMatrix(i, COL_DSN_ID), "ESTADO", "B", Null, Null) Then
                    MsgBox "Error al procesar " & grdDatos.TextMatrix(i, COL_DSN_NOM)
                Else
                    grdDatos.TextMatrix(i, COL_NOM_ESTADO) = "Ya utilizado"
                    grdDatos.TextMatrix(i, COL_ESTADO) = "B"
                End If
            Next i
        End If
    Else
        MsgBox "Debe seleccionar un archivo para utilizar.", vbExclamation + vbOKOnly
    End If
    Call Status("Listo.")
    Call Puntero(False)
End Sub

'{ add -004- a.
Private Sub grdDatos_SelChange()
    With grdDatos
        lRowSel_Ini = .Row
        lRowSel_Fin = .RowSel
        If lRowSel_Ini > lRowSel_Fin Then       ' Invierto los valores seg�n corresponda
            lRowSel_Ini = .RowSel
            lRowSel_Fin = .Row
        End If
    End With
End Sub
'}

'Private Sub txtBuscarDSNNombre_Change()
'    Call Filtrar        ' Ejecuto el filtrado de la grilla
'End Sub
'
'Private Sub Filtrar()
'    On Error GoTo Errores
'
'    Dim cFilters As String
'    aplRST.Filter = adFilterNone
'
'    If Len(txtBuscarDSNNombre) > 0 Then
'        cFilters = "dsn_nom LIKE '" & Trim(txtBuscarDSNNombre) & "*'"
'    End If
'    If Len(cFilters) > 0 Then           ' Si existe alg�n criterio, realiza el filtro...
'        aplRST.Filter = cFilters
'    End If
'    Call CargarGrilla
'Exit Sub
'Errores:
'    Select Case Err.Number
'        Case 3001   ' Argumentos incorrectos, fuera del intervalo permitido o en conflicto con otros.
'            MsgBox "Esta intentando utilizar para el filtrado un caracter inv�lido. Revise.", vbExclamation + vbOKOnly, "Caracter inv�lido"
'            cFilters = ""
'            Resume
'        Case Else
'            Stop
'            MsgBox Err.DESCRIPTION & "(" & Err.Number & ") - Or�gen: " & Err.source, vbCritical + vbOKOnly, "Error al filtrar la grilla"
'            cFilters = ""
'            Resume Next
'    End Select
'End Sub

Private Sub chkNoCopy_Click()
    If Not bSorting Then
        If chkNoCopy.Value = 1 Then
            Call setHabilCtrl(txtBlibliotecaCopy, "DIS")
            Call setHabilCtrl(txtNombreCopy, "DIS")
            txtBlibliotecaCopy = ""
            txtNombreCopy = ""
        Else
            Call setHabilCtrl(txtBlibliotecaCopy, "NOR")
            Call setHabilCtrl(txtNombreCopy, "NOR")
            If Len(ClearNull(txtBlibliotecaCopy)) = 0 Then
                txtBlibliotecaCopy = BIBLIOTECA_DEFAULT
            End If
        End If
    End If
End Sub

'{ del -017- a.
''{ add -015- a.
'Private Sub chkSoloDSNPropios_Click()
'    Call CargarGrilla
'End Sub
''}
'
'Private Sub chkSoloValidos_Click()
'    Call CargarGrilla
'End Sub
'}

'{ add -016- a.
Private Sub grdDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        Call MenuGridFind(grdDatos)
    End If
End Sub
'}

Private Sub cmdExportar_Click()
    On Error GoTo ErrHandler
    Dim cNombreArchivo As String
    
'    Dim sNewName As String
'
'    sNewName = Dialogo_GuardarExportacion("GUARDAR")
'    If Len(Trim(sNewName)) > 0 Then
'        txtFile = sNewName
'    End If

    cNombreArchivo = Format(Now, "yyyymmdd_hhmm_") & glLOGIN_ID_REEMPLAZO
    cNombreArchivo = Dialogo_GuardarExportacion("GUARDAR", cNombreArchivo)

'    On Error GoTo ErrHandler
'    Dim cNombreArchivo As String

'    cNombreArchivo = Format(Now, "yyyymmdd_hhmm_") & glLOGIN_ID_REEMPLAZO & ".xls"
'    With mdiPrincipal.CommonDialog
'        .DialogTitle = "Archivo de exportaci�n"
'        .CancelError = True
'        .Filter = "Microsoft Excel (*.xls)|*.xls"
'        .FilterIndex = 1
'        .InitDir = glWRKDIR
'        .Flags = cdlOFNCreatePrompt + cdlOFNHideReadOnly + cdlOFNLongNames + cdlOFNOverwritePrompt
'        .ShowSave
'        cNombreArchivo = .FileName
'    End With
    
    If Len(cNombreArchivo) > 0 Then
        Call Status("Preparando exportaci�n... aguarde...")
        Call ExportarExcel(cNombreArchivo, grdDatos)
'    Else
'        MsgBox "Debe especificar un nombre de archivo v�lido", vbExclamation + vbOKOnly, "Error en el nombre del archivo"
    End If
    Exit Sub
ErrHandler:
    Exit Sub        ' El usuario ha hecho click en el bot�n Cancelar
End Sub

Private Sub ExportarExcel(sNombreArchivo As String, ByVal FlexGrid As MSFlexGrid)
    On Error GoTo Error_Handler
    Dim o_Excel As Object, o_Libro As Object, o_Hoja As Object
    Dim Fila As Long, Fila2 As Long, Columna As Long
    Dim Limite As Long
    Dim xlsCol As Long
    Dim i As Integer
    
    Call Puntero(True)
    Call Status("Generando planilla... aguarde...")
    
    If CrearObjetoExcel(o_Excel, o_Libro, o_Hoja, sNombreArchivo) Then           ' Crea la planilla para la exportaci�n
    'Set o_Excel = CreateObject("Excel.Application")     ' Crea el objeto Excel, el objeto workBook y dos objeto sheet
    
        'Set o_Libro = o_Excel.Workbooks.Add
        
        If o_Libro.WorkSheets.Count > 1 Then
            For i = 1 To o_Libro.WorkSheets.Count - 1
                o_Libro.WorkSheets(i).Delete
            Next i
        End If
        'Debug.Print o_Libro.WorkSheets.Count
        
        'o_Libro.Sheets.Add
        'Debug.Print o_Libro.WorkSheets.Count
        o_Libro.Sheets(1).name = "HEDT001"
        o_Libro.Sheets(2).name = "HEDT003"
    
        Set o_Hoja = o_Libro.Sheets(1)
        o_Hoja.Rows(1).Font.Bold = True
        With FlexGrid
            For Fila = 0 To .Rows - 1
                Call Status("Generando planilla... " & Fila & " de " & .Rows - 1)
                o_Hoja.Cells(Fila + 1, 1).Value = .TextMatrix(Fila, COL_DSN_ID)
                o_Hoja.Cells(Fila + 1, 2).Value = .TextMatrix(Fila, COL_DSN_NOM)
                o_Hoja.Cells(Fila + 1, 3).Value = .TextMatrix(Fila, COL_DSN_ENMAS)
                o_Hoja.Cells(Fila + 1, 4).Value = .TextMatrix(Fila, COL_DSN_NOMRUT)
                o_Hoja.Cells(Fila + 1, 5).Value = .TextMatrix(Fila, COL_DSN_FEULT)
                o_Hoja.Cells(Fila + 1, 6).Value = .TextMatrix(Fila, COL_NOM_USUARIO)
                o_Hoja.Cells(Fila + 1, 7).Value = .TextMatrix(Fila, COL_DSN_VB)
                o_Hoja.Cells(Fila + 1, 8).Value = .TextMatrix(Fila, COL_NOM_VB_USUARIO)
                o_Hoja.Cells(Fila + 1, 9).Value = .TextMatrix(Fila, COL_DSN_VB_FE)
                o_Hoja.Cells(Fila + 1, 10).Value = .TextMatrix(Fila, COL_APP_ID)
                o_Hoja.Cells(Fila + 1, 11).Value = .TextMatrix(Fila, COL_APP_NAME)
                o_Hoja.Cells(Fila + 1, 12).Value = .TextMatrix(Fila, COL_DSN_DESC)
                o_Hoja.Cells(Fila + 1, 13).Value = .TextMatrix(Fila, COL_NOM_ESTADO)
                o_Hoja.Cells(Fila + 1, 14).Value = .TextMatrix(Fila, COL_VALIDO)
                o_Hoja.Cells(Fila + 1, 15).Value = .TextMatrix(Fila, COL_DSN_NOMPROD)
                o_Hoja.Cells(Fila + 1, 16).Value = .TextMatrix(Fila, COL_DSN_COPYBIBLIO)
                o_Hoja.Cells(Fila + 1, 17).Value = .TextMatrix(Fila, COL_DSN_COPYNOMBRE)
            Next Fila
        End With
        With o_Hoja                             ' Formato final (las columnas comienzan a numerarse de 1 para la propiedad Columns)
            .Columns(1).ColumnWidth = 10        ' C�digo
            .Columns(2).ColumnWidth = 48        ' Nombre DSN
            .Columns(3).ColumnWidth = 8.5       ' Datos Sensibles
            .Columns(4).ColumnWidth = 7         ' Rutina
            .Columns(5).ColumnWidth = 17        ' �ltima modificaci�n
            .Columns(6).ColumnWidth = 25        ' Propietario
            .Columns(7).ColumnWidth = 11        ' Estado
            .Columns(8).ColumnWidth = 19        ' Por
            .Columns(9).ColumnWidth = 17        ' Fch. visado
            .Columns(10).ColumnWidth = 10       ' Aplicativo
            .Columns(11).ColumnWidth = 35       ' Nombre de aplicativo
            .Columns(12).ColumnWidth = 50       ' Descripci�n del DSN
            .Columns(13).ColumnWidth = 0
            .Columns(14).ColumnWidth = 8        ' Validez
            .Columns(15).ColumnWidth = 48       ' Nombre productivo
            .Columns(16).ColumnWidth = 20       ' Biblioteca de COPY
            .Columns(17).ColumnWidth = 20       ' Nombre de COPY
        End With
        
        Set o_Hoja = o_Libro.Sheets(2)
        
        Fila2 = 1
        o_Hoja.Rows(Fila2).Font.Bold = True
        With FlexGrid
            o_Hoja.Cells(1, 1).Value = "C�digo"
            o_Hoja.Cells(1, 2).Value = "Nombre del archivo"
            o_Hoja.Cells(1, 3).Value = "Campo"
            o_Hoja.Cells(1, 4).Value = "Descripci�n"
            o_Hoja.Cells(1, 5).Value = "Posici�n"
            o_Hoja.Cells(1, 6).Value = "Longitud"
            o_Hoja.Cells(1, 7).Value = "Decimales"
            o_Hoja.Cells(1, 8).Value = "Tipo de dato"
            o_Hoja.Cells(1, 9).Value = "Tipo"
            o_Hoja.Cells(1, 10).Value = "Subtipo"
            o_Hoja.Cells(1, 11).Value = "Rutina"
            For Fila = 0 To .Rows - 1
                Call Status("Generando planilla... " & Fila & " de " & .Rows - 1)
                If sp_GetHEDT003a(.TextMatrix(Fila, COL_DSN_ID)) Then
                    Do While Not aplRST.EOF
                        o_Hoja.Cells(Fila2 + 1, 1).Value = .TextMatrix(Fila, COL_DSN_ID)
                        o_Hoja.Cells(Fila2 + 1, 2).Value = .TextMatrix(Fila, COL_DSN_NOM)
                        o_Hoja.Cells(Fila2 + 1, 3).Value = ClearNull(aplRST.Fields!CPO_ID)
                        o_Hoja.Cells(Fila2 + 1, 4).Value = ClearNull(aplRST.Fields!CPO_DSC)
                        o_Hoja.Cells(Fila2 + 1, 5).Value = ClearNull(aplRST.Fields!cpo_nrobyte)
                        o_Hoja.Cells(Fila2 + 1, 6).Value = ClearNull(aplRST.Fields!cpo_canbyte)
                        o_Hoja.Cells(Fila2 + 1, 7).Value = ClearNull(aplRST.Fields!cpo_decimals)
                        o_Hoja.Cells(Fila2 + 1, 8).Value = ClearNull(aplRST.Fields!cpo_type)
                        o_Hoja.Cells(Fila2 + 1, 9).Value = ClearNull(aplRST.Fields!tipo)
                        o_Hoja.Cells(Fila2 + 1, 10).Value = ClearNull(aplRST.Fields!subtipo)
                        o_Hoja.Cells(Fila2 + 1, 11).Value = ClearNull(aplRST.Fields!cpo_nomrut)
                        Fila2 = Fila2 + 1
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            Next Fila
        End With
        With o_Hoja                             ' Formato final (las columnas comienzan a numerarse de 1 para la propiedad Columns)
            .Columns(1).ColumnWidth = 10        ' C�digo
            .Columns(2).ColumnWidth = 48        ' Nombre DSN
            .Columns(3).ColumnWidth = 23
            .Columns(4).ColumnWidth = 20
            .Columns(5).ColumnWidth = 8
            .Columns(6).ColumnWidth = 8
            .Columns(7).ColumnWidth = 10
            .Columns(8).ColumnWidth = 15
            .Columns(9).ColumnWidth = 15
            .Columns(10).ColumnWidth = 23
            .Columns(11).ColumnWidth = 23
        End With
        'o_Libro.Save
        
'        For i = 1 To o_Libro.Worksheets.Count
'            'With
'            o_Libro.Worksheets(i).PageSetup.Zoom = 75
'            '.Zoom = False
'            '.FitToPagesTall = 1
'            '.FitToPagesWide = 1
'            '.Orientation = xlLandscape
'            'End With
'        Next i
        
        Call CerrarObjectoExcel(o_Excel, o_Libro, o_Hoja, sNombreArchivo)
    'o_Libro.Close True, sNombreArchivo
    'o_Excel.Quit
    End If
    
    
    
    'Call ReleaseObjects(o_Excel, o_Libro, o_Hoja, sNombreArchivo)   ' Terminar instancias
    
    Call Puntero(False)
    Call Status("Listo.")
    'MsgBox "Exportaci�n realizada en " & sNombreArchivo, vbInformation + vbOKOnly
Exit Sub
Error_Handler:          ' Cierra la hoja y el la aplicaci�n Excel
    If Not o_Libro Is Nothing Then: o_Libro.Close False
    If Not o_Excel Is Nothing Then: o_Excel.Quit
    'Call ReleaseObjects(o_Excel, o_Libro, o_Hoja)
    Call Puntero(False)
    Call Status("Listo.")
    If Err.Number <> 1004 Then MsgBox Err.DESCRIPTION, vbCritical
End Sub

Private Function ValidarCalificadores(ByVal cNombre As String) As Boolean
    Dim cCalificador() As String
    Dim l_pos As Long
    Dim i As Long
    
    ValidarCalificadores = False
    l_pos = 1
    
    If Len(cNombre) > 0 Then
        Do While Len(cNombre) > 0
            l_pos = InStr(1, cNombre, ".", vbTextCompare)
            i = i + 1
            ReDim Preserve cCalificador(i)
            If l_pos > 0 Then
                cCalificador(i) = Mid(cNombre, 1, l_pos - 1): cNombre = Mid(cNombre, l_pos + 1)
            Else
                cCalificador(i) = Mid(cNombre, 1): cNombre = ""
            End If
        Loop
        ' del -05.02.2013-
        'If i < 2 Then                               ' Control de cantidad m�nima de calificadores (al menos debe tener 2)
        '    MsgBox "El nombre del archivo debe tener al menos dos (2) calificadores para ser v�lido.", vbExclamation + vbOKOnly, "Calificadores del nombre de archivo"
        '    Exit Function
        'End If
        For i = 1 To UBound(cCalificador)
            If Len(cCalificador(i)) < 1 Then        ' Validaciones gen�ricas
                MsgBox "Los calificadores deben tener al menos una longitud de un (1) caracter.", vbExclamation + vbOKOnly, "Calificadores del nombre de archivo"
                Exit Function
            End If
            If Len(cCalificador(i)) > 8 Then
                MsgBox "Cualquiera de los calificadores deben tener una longitud menor o igual a ocho (8) caracteres.", vbExclamation + vbOKOnly, "Calificadores del nombre de archivo"
                Exit Function
            End If
            Select Case i                           ' Validaciones seg�n ordinal del calificador
                Case 1  ' Primer calificador
                    If InStr(1, "0123456789", Left(cCalificador(i), 1), vbTextCompare) > 0 Then
                        MsgBox "El primer caracter del primer calificador no puede ser un n�mero.", vbExclamation + vbOKOnly, "Calificadores del nombre de archivo"
                    End If
            End Select
        Next i
    End If
    ValidarCalificadores = True
End Function

Private Sub cmdCopiar_Click()
    Dim cBuffer As String
    Dim i As Long
    
    Clipboard.Clear
    If grdDatos.Rows > 1 Then
        For i = lRowSel_Ini To lRowSel_Fin
            cBuffer = cBuffer & grdDatos.TextMatrix(i, COL_DSN_NOM) & IIf(lRowSel_Ini = lRowSel_Fin, "", vbCrLf)
        Next i
    End If
    Clipboard.SetText Trim(cBuffer), vbCFText
End Sub

Private Sub cmdActivar_Click()
    With grdDatos           ' Pasar a pendiente de revisi�n o pendiente pos-revisi�n
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, COL_DSN_ID) <> "" Then
                If InStr(1, "B|D|", .TextMatrix(.RowSel, COL_DSN_VB_ID), vbTextCompare) > 0 Then
                    If MsgBox("�Confirma pasar a pendiente de revisi�n por S.I.?", vbQuestion + vbOKCancel) = vbOK Then
                        Call Puntero(True)
                        If .TextMatrix(.RowSel, COL_VALIDO) = "Si" Then
                            Call sp_UpdateHEDT001Field(.TextMatrix(.RowSel, COL_DSN_ID), "DSN_VB", IIf(.TextMatrix(.RowSel, COL_DSN_VB_ID) = "B", "N", "P"), Null, Null)
                            Call CargarGrilla
                        End If
                        Call Puntero(False)
                    End If
                End If
            End If
        End If
    End With
End Sub

'{ add -017- a.
Private Sub cmdFiltrar_Click()
    Call CargarGrilla
End Sub
'}

'' Actualiza la cantidad de utilizaci�n del DSN (para ver desde SI)
'Private Sub Command1_Click()
'    Dim I As Long
'    With grdDatos
'        Call Puntero(True)
'        For I = 1 To .Rows - 1
'            If sp_GetHEDTSolInt(.TextMatrix(I, COL_DSN_NOM)) Then
'                Call sp_UpdateHEDT001Field(.TextMatrix(I, COL_DSN_ID), "dsn_cantuso", Null, Null, aplRST.RecordCount)
'            Else
'                Call sp_UpdateHEDT001Field(.TextMatrix(I, COL_DSN_ID), "dsn_cantuso", Null, Null, 0)
'            End If
'            DoEvents
'        Next I
'    End With
'    Call Puntero(False)
'    MsgBox ("Proceso finalizado.")
'End Sub

'Private Sub ReCargarGrilla(auxRST As ADODB.Recordset)
'    bSorting = True
'    Inicializar_Grilla
'    Call Puntero(True)
'    If Not auxRST.EOF Then
'        With grdDatos
'            .Redraw = False                 ' Deshabilitar el repintado del control
'            auxRST.MoveFirst                ' Mueve el recordset al primer registro
'            .Rows = auxRST.RecordCount + 1  ' Agrega las filas necesarias en el FlexGRid
'            .Cols = auxRST.Fields.Count     ' Agrega las columnas necesarias
'            .Row = 1: .Col = 0              ' Selecciona
'            .RowSel = .Rows - 1: .ColSel = .Cols - 1
'            .Clip = auxRST.GetString(adClipString, -1, Chr(9), Chr(13), vbNullString)       ' Esta linea de c�digo es la que carga los registros
'            .Row = 1
'            .Redraw = True
'            Call Inicializar_Columnas
'            'Call Inicializar_Grilla
'            .Visible = True: fraBusqueda.Visible = .Visible
'        End With
'        'If Val(Me.Tag) = auxRST.RecordCount Then
'        '    Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos"
'        '    Status ("Listo.")
'        'Else
'        '    Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos (filtrado...)"
'        '    Status (auxRST.RecordCount & " archivo(s) filtrado(s) de " & Me.Tag)
'        'End If
'    Else
'        'If Len(aplRST.Filter) > 0 Then
'        '    Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos (filtrado...)"
'        '    Status (auxRST.RecordCount & " archivo(s) filtrado(s) de " & Me.Tag)
'        'Else
'        '    Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos"
'        '    Status ("Listo.")
'        'End If
'    End If
'    bSorting = False
'    Call Puntero(False)
'End Sub

'Private Sub chkBuscar_Click()
'    With grdDatos
'        If chkBuscar.Value = 1 Then
'            lblBusqueda.Caption = ""
'            lblBusqueda.Visible = True
'            lblMensaje.Visible = False
'            fraBusqueda.Enabled = True
'            cmdAgregar.Enabled = False
'            cmdModificar.Enabled = False
'            cmdEliminar.Enabled = False
'            cmdCOPYS.Enabled = False
'            .Top = 760
'            txtBuscarDSNNombre.Enabled = True
'            chkSoloDSNPropios.Enabled = False
'            chkSoloValidos.Enabled = False
'            txtBuscarDSNNombre.SetFocus
'        Else
'            lblBusqueda.Visible = False
'            lblMensaje.Visible = True
'            fraBusqueda.Enabled = False
'            .Top = 120
'            cmdAgregar.Enabled = True
'            cmdModificar.Enabled = True
'            cmdEliminar.Enabled = True
'            cmdCOPYS.Enabled = True
'            chkSoloDSNPropios.Enabled = True
'            chkSoloValidos.Enabled = True
'        End If
'    End With
'End Sub
'}

'Private Sub ReCargarGrilla(auxRST As ADODB.Recordset)
'    bSorting = True
'    Call Inicializar_Grilla
'    Call Puntero(True)
'    If Not auxRST.EOF Then
'        With grdDatos
'            Do While Not auxRST.EOF
'                If chkSoloValidos.Value = 0 Or (chkSoloValidos.Value = 1 And Mid(ClearNull(auxRST.Fields!valido), 1, 1) = "S") Then
'                    .Rows = .Rows + 1
'                    .TextMatrix(.Rows - 1, COL_DSN_ID) = ClearNull(auxRST.Fields!dsn_id)
'                    .TextMatrix(.Rows - 1, COL_DSN_NOM) = ClearNull(auxRST.Fields!dsn_nom)
'                    .TextMatrix(.Rows - 1, COL_DSN_ENMAS) = ClearNull(auxRST.Fields!dsn_enmas)
'                    .TextMatrix(.Rows - 1, COL_DSN_NOMRUT) = ClearNull(auxRST.Fields!dsn_nomrut)
'                    .TextMatrix(.Rows - 1, COL_DSN_FEULT) = Format(ClearNull(auxRST.Fields!dsn_feult), "yyyy/mm/dd")
'                    .TextMatrix(.Rows - 1, COL_NOM_USUARIO) = ClearNull(auxRST.Fields!nom_usuario)
'                    .TextMatrix(.Rows - 1, COL_DSN_VB) = ClearNull(auxRST.Fields!dsn_vb)
'                    .TextMatrix(.Rows - 1, COL_NOM_VB_USUARIO) = ClearNull(auxRST.Fields!nom_vb_usuario)
'                    .TextMatrix(.Rows - 1, COL_DSN_VB_FE) = Format(ClearNull(auxRST.Fields!dsn_vb_fe), "yyyy/mm/dd")
'                    .TextMatrix(.Rows - 1, COL_APP_ID) = ClearNull(auxRST.Fields!app_id)
'                    .TextMatrix(.Rows - 1, COL_APP_NAME) = ClearNull(auxRST.Fields!app_name)
'                    .TextMatrix(.Rows - 1, COL_DSN_DESC) = ClearNull(auxRST.Fields!dsn_desc)
'                    .TextMatrix(.Rows - 1, COL_NOM_ESTADO) = ClearNull(auxRST.Fields!nom_estado)
'                    .TextMatrix(.Rows - 1, COL_ESTADO) = ClearNull(auxRST.Fields!Estado)
'                    .TextMatrix(.Rows - 1, COL_VALIDO) = ClearNull(auxRST.Fields!valido)
'                    .TextMatrix(.Rows - 1, COL_DSN_VB_ID) = ClearNull(auxRST.Fields!dsn_vb_id)
'                    .TextMatrix(.Rows - 1, COL_DSN_TXT) = ClearNull(auxRST.Fields!dsn_txt)
'                    .TextMatrix(.Rows - 1, COL_DSN_NOMPROD) = ClearNull(auxRST.Fields!dsn_nomprod)
'                    .TextMatrix(.Rows - 1, COL_DSN_COPY) = ClearNull(auxRST.Fields!dsn_cpy)
'                    .TextMatrix(.Rows - 1, COL_DSN_COPYBIBLIO) = ClearNull(auxRST.Fields!dsn_cpybbl)
'                    .TextMatrix(.Rows - 1, COL_DSN_COPYNOMBRE) = ClearNull(auxRST.Fields!dsn_cpynom)
'                    .TextMatrix(.Rows - 1, COL_DSN_COD_RECURSO) = ClearNull(auxRST.Fields!cod_recurso)
'                    If Mid(ClearNull(auxRST.Fields!valido), 1, 1) = "N" Then
'                        Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorDarkGrey)
'                    End If
'                End If
'                auxRST.MoveNext
'                DoEvents
'            Loop
'        End With
'        If Val(Me.Tag) = auxRST.RecordCount Then
'            Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos"
'            Call Status(aplRST.RecordCount & " dsn(s).")
'        Else
'            Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos (filtrado...)"
'            Status (auxRST.RecordCount & " archivo(s) filtrado(s) de " & Me.Tag)
'        End If
'    Else
'        If Len(aplRST.Filter) > 0 Then
'            Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos (filtrado...)"
'            Status (auxRST.RecordCount & " archivo(s) filtrado(s) de " & Me.Tag)
'        Else
'            Me.Caption = "Cat�logo de DSN para procesos de enmascaramiento de datos"
'            Call Status(aplRST.RecordCount & " dsn(s).")
'        End If
'    End If
'    'fraBusqueda.Visible = True
'    grdDatos.Visible = True
'    bSorting = False
'    Call Puntero(False)
'End Sub

'Private Sub Inicializar_Columnas()
'    With grdDatos
'        .Visible = False    ': fraBusqueda.Visible = .Visible
'        .Cols = COL_CANTIDAD        ' 18
'        .TextMatrix(0, 0) = "C�digo": .ColWidth(0) = 1000
'        .TextMatrix(0, 1) = "Nombre del archivo": .ColWidth(1) = 4000
'        .TextMatrix(0, 2) = "ENM.": .ColWidth(2) = 800
'        .TextMatrix(0, 3) = "Rutina": .ColWidth(3) = 1000
'        .TextMatrix(0, 4) = "Ult. mod.": .ColWidth(4) = 1700
'        .TextMatrix(0, 5) = "Propietario": .ColWidth(5) = 1500
'        .TextMatrix(0, 6) = "Visado": .ColWidth(6) = 1000
'        .TextMatrix(0, 7) = "Por": .ColWidth(7) = 1200
'        .TextMatrix(0, 8) = "Fch. Visado": .ColWidth(8) = 1700
'        .TextMatrix(0, 9) = "Aplicativo": .ColWidth(9) = 1000
'        .TextMatrix(0, 10) = "Nombre del aplicativo": .ColWidth(10) = 2000
'        .TextMatrix(0, 11) = "Descripci�n del archivo": .ColWidth(11) = 4000
'        .TextMatrix(0, 12) = "Estado": .ColWidth(12) = 1600
'        .TextMatrix(0, 13) = "": .ColWidth(13) = 0
'        .TextMatrix(0, 14) = "Validez": .ColWidth(14) = 800
'        .TextMatrix(0, 15) = "dsn_vb": .ColWidth(15) = 0
'        .TextMatrix(0, 16) = "Motivo de rechazo": .ColWidth(16) = 2000
'        .TextMatrix(0, 17) = "Nombre productivo": .ColWidth(17) = 3000
'        .TextMatrix(0, COL_DSN_COPY) = "": .ColWidth(COL_DSN_COPY) = 0
'        .TextMatrix(0, COL_DSN_COPYBIBLIO) = "Biblioteca de COPY": .ColWidth(COL_DSN_COPYBIBLIO) = 3000
'        .TextMatrix(0, COL_DSN_COPYNOMBRE) = "Nombre de COPY": .ColWidth(COL_DSN_COPYNOMBRE) = 3000
'        .TextMatrix(0, COL_DSN_COD_RECURSO) = "": .ColWidth(COL_DSN_COD_RECURSO) = 0
'        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
'    End With
'End Sub

'' *** PRUEBA DE CORREO ***
'Private Sub Command1_Click()
'    'Call Enviar_Mail_CDO("76.253.40.22", "diego.marcet@bbva.com", "fjspitz@gmail.com;fernando.spitz@bbvafrances.com.ar", "Prueba 1 de SMTP", "Hola mundo!", vbNullString, 25, InputBox("Usuario:"), InputBox("Contrase�a:"), False, False)
'    ' SMTP anterior:76.253.40.22
'    Call Enviar_Mail_CDO("gatewaymailgw.arg.igrupobbva", _
'                         "fernando.spitz@correo1.arg.igrupobbva; fernando.spitz.contractor@bbva.com; fjspitz@gmail.com", _
'                         "CGM@bbva.com", _
'                         "Prueba 2 de SMTP", _
'                         "Hola mundo!", _
'                         vbNullString, 25, "", "", False, False)
'    MsgBox "Hecho.", vbInformation
'End Sub

'Private Sub Command1_Click()
'    'Call Enviar_Mail_CDO("76.253.40.22", "diego.marcet@bbva.com", "fjspitz@gmail.com;fernando.spitz@bbvafrances.com.ar", "Prueba 1 de SMTP", "Hola mundo!", vbNullString, 25, InputBox("Usuario:"), InputBox("Contrase�a:"), False, False)
'    ' SMTP anterior:76.253.40.22
'    Call EnviarCorreo("fernando.spitz@correo1.arg.igrupobbva; fernando.spitz.contractor@bbva.com; fjspitz@gmail.com", _
'                      "CGM@bbva.com", _
'                      "Prueba 2 de SMTP", _
'                      "Hola mundo!", _
'                      vbNullString)
'    MsgBox "Hecho.", vbInformation
'End Sub


