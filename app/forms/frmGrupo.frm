VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmGrupo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "GRUPOS"
   ClientHeight    =   7260
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   11760
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   11760
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab sstGrupo 
      Height          =   3555
      Left            =   60
      TabIndex        =   18
      Top             =   3660
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   6271
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmGrupo.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraDatos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Caracter�sticas"
      TabPicture(1)   =   "frmGrupo.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label7"
      Tab(1).Control(1)=   "Label8"
      Tab(1).Control(2)=   "Label11"
      Tab(1).Control(3)=   "fraOpcionPerfil"
      Tab(1).Control(4)=   "fraOpcionDatosPeticion"
      Tab(1).Control(5)=   "fraOpcionAltaPeticion"
      Tab(1).ControlCount=   6
      Begin VB.Frame fraOpcionAltaPeticion 
         Caption         =   "Alta de peticiones especiales"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   -67740
         TabIndex        =   9
         ToolTipText     =   "S�lo aplica para grupos ejecutores de la gerencia de sistemas"
         Top             =   480
         Width           =   2775
         Begin VB.OptionButton optPeticiones 
            Caption         =   "Por defecto"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   38
            Top             =   360
            Width           =   1935
         End
         Begin VB.OptionButton optPeticiones 
            Caption         =   "Propias"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   37
            Top             =   600
            Width           =   975
         End
         Begin VB.OptionButton optPeticiones 
            Caption         =   "Propias y especiales"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   36
            Top             =   840
            Width           =   1935
         End
      End
      Begin VB.Frame fraOpcionDatosPeticion 
         Caption         =   "Incidencia en peticiones"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   -71340
         TabIndex        =   8
         Top             =   480
         Width           =   3495
         Begin VB.CheckBox chkCaracteristicas 
            Caption         =   "No considerar horas estimadas"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   35
            Top             =   960
            Width           =   3195
         End
         Begin VB.CheckBox chkCaracteristicas 
            Caption         =   "No considerar fechas de planificaci�n"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   34
            Top             =   1200
            Width           =   3255
         End
         Begin VB.CheckBox chkCaracteristicas 
            Caption         =   "No considerar consolidaci�n de estados"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   33
            Top             =   1440
            Width           =   3315
         End
         Begin VB.Label Label9 
            Caption         =   "Cuando est�n activas, las horas totales, las fechas de planificaci�n y los estados del grupo NO afectan a la petici�n."
            Height          =   615
            Left            =   120
            TabIndex        =   41
            Top             =   300
            Width           =   3255
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Frame fraOpcionPerfil 
         Caption         =   "Perfil para grupos ejecutores"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   -74820
         TabIndex        =   7
         Top             =   480
         Width           =   3375
         Begin VB.OptionButton optHomologacion 
            Caption         =   "Homologaci�n"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   32
            Top             =   840
            Width           =   1575
         End
         Begin VB.OptionButton optHomologacion 
            Caption         =   "T�cnico"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   31
            Top             =   600
            Width           =   1575
         End
         Begin VB.OptionButton optHomologacion 
            Caption         =   "Funcional"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   30
            Top             =   360
            Width           =   1575
         End
         Begin VB.OptionButton optHomologacion 
            Caption         =   "Seguridad Inform�tica"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   29
            Top             =   1080
            Width           =   1995
         End
         Begin VB.OptionButton optHomologacion 
            Caption         =   "Arquitectura t�cnica"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   28
            Top             =   1320
            Width           =   2115
         End
      End
      Begin VB.Frame fraDatos 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3120
         Left            =   60
         TabIndex        =   19
         Top             =   360
         Width           =   9975
         Begin VB.ComboBox cboBpar 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1350
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   2100
            Width           =   4185
         End
         Begin VB.ComboBox cboEjecutor 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1350
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   1740
            Width           =   2500
         End
         Begin VB.ComboBox cboHabil 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1350
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   1380
            Width           =   2505
         End
         Begin VB.ComboBox cboSector 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1350
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   1020
            Width           =   5985
         End
         Begin AT_MaskText.MaskText txtCodigo 
            Height          =   315
            Left            =   1350
            TabIndex        =   0
            Top             =   330
            Width           =   1000
            _ExtentX        =   1773
            _ExtentY        =   556
            MaxLength       =   8
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   8
            UpperCase       =   -1  'True
         End
         Begin AT_MaskText.MaskText txtDescripcion 
            Height          =   315
            Left            =   1350
            TabIndex        =   1
            Top             =   675
            Width           =   5985
            _ExtentX        =   10557
            _ExtentY        =   556
            MaxLength       =   80
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   80
         End
         Begin AT_MaskText.MaskText txtAbreviatura 
            Height          =   315
            Left            =   1350
            TabIndex        =   6
            Top             =   2520
            Width           =   2500
            _ExtentX        =   4419
            _ExtentY        =   556
            MaxLength       =   30
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   30
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Abreviatura"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   27
            Top             =   2587
            Width           =   900
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "R. de Sistema"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   26
            ToolTipText     =   "Referente de Sistema"
            Top             =   2175
            Width           =   1020
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Ejecutor"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   25
            Top             =   1815
            Width           =   630
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   24
            Top             =   1455
            Width           =   510
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   23
            Top             =   1095
            Width           =   480
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   22
            Top             =   397
            Width           =   495
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   21
            Top             =   742
            Width           =   840
         End
         Begin VB.Label lblSeleccion 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   20
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
      End
      Begin VB.Label Label11 
         Caption         =   $"frmGrupo.frx":0038
         Height          =   1095
         Left            =   -67740
         TabIndex        =   42
         Top             =   2340
         Width           =   2775
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label8 
         Caption         =   $"frmGrupo.frx":00FE
         Height          =   1095
         Left            =   -71340
         TabIndex        =   40
         Top             =   2340
         Width           =   3495
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label7 
         Caption         =   $"frmGrupo.frx":01CB
         Height          =   1095
         Left            =   -74820
         TabIndex        =   39
         Top             =   2340
         Width           =   3075
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7215
      Left            =   10320
      TabIndex        =   11
      Top             =   0
      Width           =   1395
      Begin VB.CheckBox chkHab 
         Caption         =   "Solo habilitados"
         Height          =   375
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   60
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   6060
         Width           =   1275
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   6600
         Width           =   1275
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   60
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Grupo seleccionado"
         Top             =   5550
         Width           =   1275
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Height          =   500
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el Grupo seleccionado"
         Top             =   5040
         Width           =   1275
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector"
         Top             =   4530
         Width           =   1275
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3570
      Left            =   60
      TabIndex        =   10
      Top             =   60
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   6297
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.02.2008 - Homologaci�n: glUsrPerfilActual
' -004- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -005- a. FJS 05.06.2009 - Se fuerza que el alta de un grupo especifique que el grupo no es homologable.
' -006- a. FJS 03.09.2009 - Se modifica la llamada a el SP que trae los sectores para evitar mostrar y/o utilizar aquellos que ya se encuentran inhabilitados.
' -007- a. FJS 06.10.2009 - Se agrega el par�metro que filtra por grupos habilitados o no.
' -007- b. FJS 06.10.2009 - Se agrega el coloreado din�mico para resaltar los grupos inhabilitados.
' -008- a. FJS 13.10.2009 - Se agrega un par�metro que indica el origen del cambio a realizar. Ver opciones en la definici�n del SP.
' -009- a. FJS 14.08.2014 - Nuevo: antes de deshabilitar un grupo, si este estuviera vinculado a las �reas responsables de un proyecto, preguntamos si deseamos
'                           eliminar las referencias para no dejar inconsistencias.

Option Explicit

Private Const colCODGRUPO = 0
Private Const colNOMGRUPO = 1
Private Const colCODSECTOR = 2
Private Const colNOMSECTOR = 3
Private Const colBPAR = 4
Private Const colEjecutor = 5
Private Const colHabil = 6
Private Const colHomologacion = 7                   ' add -003- a.
Private Const colABREVIATURA = 8
Private Const colGrupoDesc = 9
Private Const colGrupoTipoPet = 10
Private Const colGrupoTipoPetDesc = 11
Private Const colGrupoIncHoras = 12
Private Const colGrupoIncHorasDesc = 13
Private Const colGrupoIncFechas = 14
Private Const colGrupoIncFechasdesc = 15
Private Const colGrupoIncEstado = 16
Private Const colGrupoIncEstadoDesc = 17
Private Const colTOTALCOLS = 18

Dim sOpcionSeleccionada As String
Dim sOldSector As String

Private Sub Form_Load()
    Call InicializarCombos
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)                    ' add -004- a.
    Me.Left = 0
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Dim nElements As Integer
    Dim vPetic() As Long            ' upd Integer x Long
    Dim cHomologacion As String     ' add -003- a.
    Dim cPeticiones As String
    Dim flgOk As Boolean
    Dim ProjId() As Long, ProjSubId() As Long, ProjSubSId() As Long
    Dim i As Long, J As Long
    
    
    '{ add -003- a.
    If optHomologacion(0).value = True Then
        cHomologacion = "N"
    End If
    If optHomologacion(1).value = True Then
        cHomologacion = "S"
    End If
    If optHomologacion(2).value = True Then
        cHomologacion = "H"
    End If
    If optHomologacion(3).value = True Then
        cHomologacion = "1"
    End If
    If optHomologacion(4).value = True Then
        cHomologacion = "2"
    End If
    '}
    
    If optPeticiones(0).value Then
        cPeticiones = ""
    End If
    If optPeticiones(1).value Then
        cPeticiones = "1"
    End If
    If optPeticiones(2).value Then
        cPeticiones = "2"
    End If
    
    nElements = 0
    flgOk = True
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_UpdateGrupo("A", txtCodigo, txtDescripcion, CodigoCombo(Me.cboSector), CodigoCombo(Me.cboHabil), CodigoCombo(Me.cboEjecutor), CodigoCombo(Me.cboBpar), cHomologacion, txtAbreviatura, cPeticiones, chkCaracteristicas(0), chkCaracteristicas(1), chkCaracteristicas(2)) Then    ' upd -003- a.
                    Call HabilitarBotones(0)
                    sstGrupo.Tab = 0
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sOldSector <> CodigoCombo(Me.cboSector) Then
                    If MsgBox("Est� cambiando el Sector, esto modificar� la estructura de dependencias. �Contin�a?", vbYesNo) = vbNo Then
                        Exit Sub
                    End If
                    If Not sp_ChangeGrupoSector(txtCodigo, CodigoCombo(Me.cboSector)) Then
                        MsgBox ("Hubo errores al cambiar el Grupo de Sector")
                        flgOk = False
                    Else
                        Call Puntero(True)
                        Call Status("Procesando...")
                        If sp_GetPeticionGrupo(0, Null, txtCodigo) Then
                            ' Guarda en el vector cada uno de los n�meros internos de petici�n
                            ' en los que se ve involucrado el grupo a reestructurar
                            Do While Not aplRST.EOF
                                nElements = nElements + 1
                                ReDim Preserve vPetic(nElements)
                                vPetic(nElements) = aplRST.Fields.Item("pet_nrointerno")
                                aplRST.MoveNext
                            Loop
                            aplRST.Close
                            If nElements > 0 Then 'RP Optimizacion
                                For nElements = 1 To UBound(vPetic)
                                    Call Status("Procesando: " & UBound(vPetic) & ":" & nElements)
                                    reasignarPeticionGrupo vPetic(nElements), sOldSector, txtCodigo, CodigoCombo(Me.cboSector), txtCodigo, "ORG"       ' upd -008- a.
                                Next
                            End If
                        End If
                        Call Puntero(False)
                        Call Status("Listo.")
                        MsgBox "Proceso de reestructuraci�n finalizado.", vbInformation + vbOKOnly, "Cambio de sector"
                    End If
                End If
                '{ add -008- a.
                ' Si qued� deshabilitado, busco si est� en alguno de los proyectos como grupo responsable
                If CodigoCombo(cboHabil, False) = "N" Then
                    If sp_GetProyectoIDMResponsables(Null, Null, Null, "GRUP", txtCodigo) Then
                        If MsgBox("El grupo se encuentra designado como �rea responsable" & vbCrLf & "en uno o m�s proyectos IDM." & vbCrLf & "�Desea desvincular autom�ticamentede el grupo de estos proyectos?", vbQuestion + vbYesNo, "Grupo en Proyectos IDM") = vbYes Then
                            Do While Not aplRST.EOF
                                ReDim Preserve ProjId(i)
                                ReDim Preserve ProjSubId(i)
                                ReDim Preserve ProjSubSId(i)
                                ProjId(i) = aplRST.Fields!ProjId
                                ProjSubId(i) = aplRST.Fields!ProjSubId
                                ProjSubSId(i) = aplRST.Fields!ProjSubSId
                                aplRST.MoveNext
                                i = i + 1
                                DoEvents
                            Loop
                            ' Ahora quitamos todas las referencias de este grupo en los proyectos
                            For J = 0 To UBound(ProjId)
                                Call sp_DeleteProyectoIDMResponsables(ProjId(J), ProjSubId(J), ProjSubSId(J), "GRUP", txtCodigo)
                            Next J
                        End If
                    End If
                End If
                '}
                If flgOk Then
                    If sp_UpdateGrupo("M", txtCodigo, txtDescripcion, CodigoCombo(Me.cboSector), CodigoCombo(Me.cboHabil), CodigoCombo(Me.cboEjecutor), CodigoCombo(Me.cboBpar), cHomologacion, txtAbreviatura, cPeticiones, chkCaracteristicas(0), chkCaracteristicas(1), chkCaracteristicas(2)) Then   ' upd -003- a.
                        With grdDatos
                            If .RowSel > 0 Then
                               .TextMatrix(.RowSel, colNOMGRUPO) = txtDescripcion
                               .TextMatrix(.RowSel, colCODSECTOR) = CodigoCombo(Me.cboSector)
                               .TextMatrix(.RowSel, colNOMSECTOR) = TextoCombo(Me.cboSector)
                               .TextMatrix(.RowSel, colBPAR) = CodigoCombo(Me.cboBpar)
                               .TextMatrix(.RowSel, colHabil) = CodigoCombo(Me.cboHabil)
                               .TextMatrix(.RowSel, colEjecutor) = CodigoCombo(Me.cboEjecutor)
                               .TextMatrix(.RowSel, colABREVIATURA) = txtAbreviatura
                               .TextMatrix(.RowSel, colHomologacion) = cHomologacion
                                Select Case cHomologacion
                                    Case "N": .TextMatrix(.RowSel, colGrupoDesc) = "Funcional"
                                    Case "S": .TextMatrix(.RowSel, colGrupoDesc) = "T�cnico"
                                    Case "H": .TextMatrix(.RowSel, colGrupoDesc) = "Homologador"
                                    Case "1": .TextMatrix(.RowSel, colGrupoDesc) = "Seguridad Inform�tica"
                                    Case "2": .TextMatrix(.RowSel, colGrupoDesc) = "Arquitectura & Infraestructura"
                                End Select
                               .TextMatrix(.RowSel, colGrupoTipoPet) = cPeticiones
                                Select Case cPeticiones
                                    Case "0", "": .TextMatrix(.RowSel, colGrupoTipoPetDesc) = "Por defecto"
                                    Case "1": .TextMatrix(.RowSel, colGrupoTipoPetDesc) = "Propias"
                                    Case "2": .TextMatrix(.RowSel, colGrupoTipoPetDesc) = "Propias y especiales"
                                End Select
                                .TextMatrix(.RowSel, colGrupoIncHoras) = IIf(chkCaracteristicas(0).value = 1, "S", "N")
                                .TextMatrix(.RowSel, colGrupoIncFechas) = IIf(chkCaracteristicas(1).value = 1, "S", "N")
                                .TextMatrix(.RowSel, colGrupoIncEstado) = IIf(chkCaracteristicas(2).value = 1, "S", "N")
                            End If
                        End With
                        Call HabilitarBotones(0, False)
                        sstGrupo.Tab = 0
                    End If
                End If
            End If
        Case "E"
            If sp_DeleteGrupo(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    Call LimpiarForm(Me)
    Call HabilitarBotones(0)
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            fraOpcionPerfil.Enabled = False
            fraOpcionDatosPeticion.Enabled = False
            fraOpcionAltaPeticion.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
            sstGrupo.Tab = 0
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    txtCodigo = "": txtDescripcion = ""
                    cboSector.ListIndex = -1
                    cboBpar.ListIndex = -1
                    cboHabil.ListIndex = 0
                    cboEjecutor.ListIndex = 1
                    fraDatos.Enabled = True
                    fraOpcionPerfil.Enabled = True
                    fraOpcionDatosPeticion.Enabled = True
                    fraOpcionAltaPeticion.Enabled = True
                    txtCodigo.Enabled = True
                    optHomologacion(0).value = True     ' add -005- a.
                    optPeticiones(0).value = True
                    txtAbreviatura = ""
                    sstGrupo.Tab = 0
                    txtCodigo.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = True
                    fraOpcionPerfil.Enabled = True
                    fraOpcionDatosPeticion.Enabled = True
                    fraOpcionAltaPeticion.Enabled = True
                    txtCodigo.Enabled = False
                    sstGrupo.Tab = 0
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
                    fraOpcionPerfil.Enabled = False
                    fraOpcionDatosPeticion.Enabled = False
                    fraOpcionAltaPeticion.Enabled = False
                    sstGrupo.Tab = 0
            End Select
    End Select
    Call LockProceso(False)
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(txtCodigo.text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtDescripcion.text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtDescripcion.text, ":") > 0 Then
        MsgBox ("La Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If cboSector.ListIndex < 0 Then
        MsgBox ("Debe asignarle un Sector")
        CamposObligatorios = False
        Exit Function
    End If
    If Not valSectorHabil(CodigoCombo(cboSector)) Then
        MsgBox ("No puede depender de un Sector inhabilitado")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Sub InicializarCombos()
    If sp_GetSector("", "", "S") Then       ' upd -006- a. Se agrega el par�metro opcional "S" para traer solo los habilitados
        Do While Not aplRST.EOF
            cboSector.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
    End If
    
    cboBpar.Clear
    If sp_GetRecursoPerfil(Null, "BPAR") Then
        Do While Not aplRST.EOF
            cboBpar.AddItem aplRST!cod_recurso & " : " & Trim(aplRST!nom_recurso)
            aplRST.MoveNext
        Loop
    End If
    
    cboHabil.AddItem "S : Habilitado"
    cboHabil.AddItem "N : Inhabilitado"

    cboEjecutor.AddItem "S : Ejecuta pedidos"
    cboEjecutor.AddItem "N : No ejecuta"
End Sub

Sub CargarGrid()
    Call Puntero(True)
    With grdDatos
        .visible = False
        .Clear
        .Font.name = "Tahoma"
        .Font.Size = 8
        .cols = colTOTALCOLS
        .HighLight = flexHighlightNever
        .FocusRect = flexFocusNone
        .Rows = 1
        .TextMatrix(0, colCODGRUPO) = "CODIGO": .ColWidth(colCODGRUPO) = 930: .ColAlignment(colCODGRUPO) = 0
        .TextMatrix(0, colNOMGRUPO) = "DESCRIPCION": .ColWidth(colNOMGRUPO) = 3250: .ColAlignment(colNOMGRUPO) = 0
        .TextMatrix(0, colCODSECTOR) = "CODIGO": .ColWidth(colCODSECTOR) = 930: .ColAlignment(colCODSECTOR) = 0
        .TextMatrix(0, colNOMSECTOR) = "SECTOR": .ColWidth(colNOMSECTOR) = 3250: .ColAlignment(colNOMSECTOR) = 0
        .TextMatrix(0, colBPAR) = "REF. SISTEMA": .ColWidth(colBPAR) = 1000: .ColAlignment(colBPAR) = 0
        .TextMatrix(0, colHabil) = "HABILIT": .ColWidth(colHabil) = 800: .ColAlignment(colHabil) = flexAlignCenterCenter
        .TextMatrix(0, colEjecutor) = "EJEC": .ColWidth(colEjecutor) = 800: .ColAlignment(colEjecutor) = flexAlignCenterCenter
        .TextMatrix(0, colHomologacion) = "homo": .ColWidth(colHomologacion) = 0: .ColAlignment(colHomologacion) = 0             ' add -003- a.
        .TextMatrix(0, colABREVIATURA) = "ABREV.": .ColWidth(colABREVIATURA) = 1000: .ColAlignment(colABREVIATURA) = 0
        .TextMatrix(0, colGrupoDesc) = "PERFIL DEL GRUPO": .ColWidth(colGrupoDesc) = 2000: .ColAlignment(colGrupoDesc) = 0
        .TextMatrix(0, colGrupoTipoPet) = "TIPOPET": .ColWidth(colGrupoTipoPet) = 0: .ColAlignment(colGrupoTipoPet) = 0
        .TextMatrix(0, colGrupoTipoPetDesc) = "Alta pet. ESP": .ColWidth(colGrupoTipoPetDesc) = 1600: .ColAlignment(colGrupoTipoPetDesc) = 0
        .TextMatrix(0, colGrupoIncHoras) = "Inc. horas": .ColWidth(colGrupoIncHoras) = 1000: .ColAlignment(colGrupoIncHoras) = 0
        .TextMatrix(0, colGrupoIncHorasDesc) = "Inc. horas": .ColWidth(colGrupoIncHorasDesc) = 0: .ColAlignment(colGrupoIncHorasDesc) = 0
        .TextMatrix(0, colGrupoIncFechas) = "Inc. fechas": .ColWidth(colGrupoIncFechas) = 1000: .ColAlignment(colGrupoIncFechas) = 0
        .TextMatrix(0, colGrupoIncFechasdesc) = "Inc. fechas": .ColWidth(colGrupoIncFechasdesc) = 0: .ColAlignment(colGrupoIncFechasdesc) = 0
        .TextMatrix(0, colGrupoIncEstado) = "Inc. estados": .ColWidth(colGrupoIncEstado) = 1000: .ColAlignment(colGrupoIncEstado) = 0
        .TextMatrix(0, colGrupoIncEstadoDesc) = "Inc. estados": .ColWidth(colGrupoIncEstadoDesc) = 0: .ColAlignment(colGrupoIncEstadoDesc) = 0
    End With
    Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    'If Not sp_GetGrupoXt("", "", IIf(chkHab.Value = 1, "S", Null)) Then Exit Sub ' upd -007- a.
    If sp_GetGrupoXt("", "", IIf(chkHab.value = 1, "S", Null)) Then
        Do While Not aplRST.EOF
            With grdDatos
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colCODGRUPO) = ClearNull(aplRST.Fields.Item("cod_grupo"))
                .TextMatrix(.Rows - 1, colNOMGRUPO) = ClearNull(aplRST.Fields.Item("nom_grupo"))
                .TextMatrix(.Rows - 1, colCODSECTOR) = ClearNull(aplRST.Fields.Item("cod_sector"))
                .TextMatrix(.Rows - 1, colNOMSECTOR) = ClearNull(aplRST.Fields.Item("nom_sector"))
                .TextMatrix(.Rows - 1, colBPAR) = ClearNull(aplRST.Fields.Item("cod_bpar"))
                .TextMatrix(.Rows - 1, colEjecutor) = ClearNull(aplRST.Fields.Item("es_ejecutor"))
                .TextMatrix(.Rows - 1, colHabil) = IIf(ClearNull(aplRST.Fields.Item("flg_habil")) = "N", "N", "S")
                If .TextMatrix(.Rows - 1, colHabil) = "N" Then
                    PintarLinea grdDatos, prmGridFillRowColorRed
                End If
                .TextMatrix(.Rows - 1, colABREVIATURA) = ClearNull(aplRST.Fields!abrev_grupo)
                .TextMatrix(.Rows - 1, colHomologacion) = ClearNull(aplRST.Fields!grupo_homologacion)
                .TextMatrix(.Rows - 1, colGrupoDesc) = ClearNull(aplRST.Fields!grupo_desc)
                .TextMatrix(.Rows - 1, colGrupoTipoPet) = ClearNull(aplRST.Fields!grupo_tipopet)
                .TextMatrix(.Rows - 1, colGrupoTipoPetDesc) = ClearNull(aplRST.Fields!grupo_tipopetdesc)
                .TextMatrix(.Rows - 1, colGrupoIncHoras) = ClearNull(aplRST.Fields!grupo_inchoras)
                .TextMatrix(.Rows - 1, colGrupoIncFechas) = ClearNull(aplRST.Fields!grupo_incfechas)
                .TextMatrix(.Rows - 1, colGrupoIncEstado) = ClearNull(aplRST.Fields!grupo_incestado)
            End With
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    grdDatos.visible = True
    DoEvents
    Call Puntero(False)
    Call MostrarSeleccion
End Sub

Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 0) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODGRUPO))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNOMGRUPO))
                cboSector.ListIndex = PosicionCombo(Me.cboSector, .TextMatrix(.RowSel, colCODSECTOR))
                sOldSector = .TextMatrix(.RowSel, colCODSECTOR)
                cboBpar.ListIndex = PosicionCombo(Me.cboBpar, .TextMatrix(.RowSel, colBPAR))
                cboHabil.ListIndex = PosicionCombo(Me.cboHabil, .TextMatrix(.RowSel, colHabil))
                cboEjecutor.ListIndex = PosicionCombo(Me.cboEjecutor, .TextMatrix(.RowSel, colEjecutor))
                txtAbreviatura = .TextMatrix(.RowSel, colABREVIATURA)
                Select Case .TextMatrix(.RowSel, colHomologacion)
                    Case "N": optHomologacion(0).value = True
                    Case "S": optHomologacion(1).value = True
                    Case "H": optHomologacion(2).value = True
                    Case "1": optHomologacion(3).value = True
                    Case "2": optHomologacion(4).value = True
                End Select
                Select Case .TextMatrix(.RowSel, colGrupoTipoPet)
                    Case "", "0": optPeticiones(0).value = True
                    Case "1":: optPeticiones(1).value = True
                    Case "2":: optPeticiones(2).value = True
                End Select
                chkCaracteristicas(0).value = IIf(.TextMatrix(.RowSel, colGrupoIncHoras) = "S", 1, 0)
                chkCaracteristicas(1).value = IIf(.TextMatrix(.RowSel, colGrupoIncFechas) = "S", 1, 0)
                chkCaracteristicas(2).value = IIf(.TextMatrix(.RowSel, colGrupoIncEstado) = "S", 1, 0)
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_SelChange()
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

'{ add -004- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}

'{ add -007- a.
Private Sub chkHab_Click()
    Call InicializarPantalla
End Sub
'}

Private Sub optHomologacion_Click(Index As Integer)
    Select Case Index
        Case 0, 1       ' Funcional, T�cnico
            chkCaracteristicas(0).Enabled = True
            chkCaracteristicas(1).Enabled = True
            chkCaracteristicas(2).Enabled = True
        Case 2, 3, 4    ' Homologaci�n, Seguridad Inform�tica y Arquitectura (respectivamente)
            chkCaracteristicas(0).value = 1: chkCaracteristicas(0).Enabled = False
            chkCaracteristicas(1).value = 1: chkCaracteristicas(1).Enabled = False
            chkCaracteristicas(2).value = 1: chkCaracteristicas(2).Enabled = False
    End Select
End Sub
