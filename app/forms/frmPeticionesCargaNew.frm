VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPeticionesCargaNew 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8220
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraButtPpal 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8160
      Left            =   10320
      TabIndex        =   1
      Top             =   0
      Width           =   1515
      Begin VB.CommandButton cmdHistView 
         Caption         =   "Historial"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Muestra el historial de la Petici�n"
         Top             =   3720
         Width           =   1410
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Terminar la operaci�n"
         Top             =   7640
         Width           =   1410
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Guardar"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Confirma y guarda los cambios realizados"
         Top             =   5760
         Width           =   1410
      End
      Begin VB.CommandButton cmdCambioEstado 
         Caption         =   "Cambio de estado"
         Height          =   450
         Left            =   40
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Permite cambiar el estado de la Petici�n"
         Top             =   3120
         Width           =   1410
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Imprimir"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Imprime un formulario con los datos de la petici�n"
         Top             =   6210
         Width           =   1410
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Permite completar los datos de la Petici�n"
         Top             =   5310
         Width           =   1410
      End
      Begin VB.CommandButton cmdSector 
         Caption         =   "Sectores"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   1200
         Width           =   1410
      End
      Begin VB.CommandButton cmdAgrupar 
         Caption         =   "Agrupamientos"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Visualiza los agrupamientos que referencian esta Petici�n"
         Top             =   600
         Width           =   1410
      End
      Begin VB.CommandButton cmdRptHs 
         Caption         =   "Horas trabajadas"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Invoca el reporte de Horas rabajadas"
         Top             =   4120
         Width           =   1410
      End
      Begin VB.CommandButton cmdGrupos 
         Caption         =   "Grupos"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   1640
         Width           =   1410
      End
      Begin VB.CommandButton cmdPlanificar 
         Caption         =   "Planificaci�n"
         Height          =   450
         Left            =   40
         Picture         =   "frmPeticionesCargaNew.frx":0000
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Permite realizar una validaci�n ad-hoc de integridad para la petici�n actual"
         Top             =   2070
         Visible         =   0   'False
         Width           =   1410
      End
      Begin VB.CommandButton cmdAsigNro 
         Caption         =   "Asig. N�mero"
         Enabled         =   0   'False
         Height          =   400
         Left            =   40
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Invoca el reporte de Horas rabajadas"
         Top             =   4560
         Visible         =   0   'False
         Width           =   1410
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "Modo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   60
         TabIndex        =   15
         Top             =   120
         Width           =   1395
      End
      Begin VB.Label lblModo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   142
         TabIndex        =   14
         Top             =   330
         Width           =   1200
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   8055
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   14208
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "1. Datos"
      TabPicture(0)   =   "frmPeticionesCargaNew.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1(1)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1(2)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label1(3)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label1(4)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label1(5)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label1(6)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label1(7)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label1(8)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label1(9)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label1(10)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label1(11)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label1(12)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Text1"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Text2"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Combo1"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Combo2"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Combo3"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Combo4"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Combo5"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Combo6"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Combo7"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Text3"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Combo8"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "Combo9"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "Combo10"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "cboAdjHomoClass"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).ControlCount=   27
      TabCaption(1)   =   "2. Detalles"
      TabPicture(1)   =   "frmPeticionesCargaNew.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "3. Anexas"
      TabPicture(2)   =   "frmPeticionesCargaNew.frx":0182
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "4. Documentaci�n"
      TabPicture(3)   =   "frmPeticionesCargaNew.frx":019E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "5. Adjuntos"
      TabPicture(4)   =   "frmPeticionesCargaNew.frx":01BA
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      TabCaption(5)   =   "6. Conformes"
      TabPicture(5)   =   "frmPeticionesCargaNew.frx":01D6
      Tab(5).ControlEnabled=   0   'False
      Tab(5).ControlCount=   0
      TabCaption(6)   =   "7. Controles"
      TabPicture(6)   =   "frmPeticionesCargaNew.frx":01F2
      Tab(6).ControlEnabled=   0   'False
      Tab(6).ControlCount=   0
      Begin VB.ComboBox cboAdjHomoClass 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmPeticionesCargaNew.frx":020E
         Left            =   1080
         List            =   "frmPeticionesCargaNew.frx":0210
         Style           =   2  'Dropdown List
         TabIndex        =   42
         Top             =   5160
         Width           =   6975
      End
      Begin VB.ComboBox Combo10 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4320
         TabIndex        =   41
         Text            =   "Combo1"
         Top             =   2040
         Width           =   1695
      End
      Begin VB.ComboBox Combo9 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   40
         Text            =   "Combo1"
         Top             =   1920
         Width           =   1695
      End
      Begin VB.ComboBox Combo8 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7680
         TabIndex        =   39
         Text            =   "Combo1"
         Top             =   1920
         Width           =   1695
      End
      Begin VB.TextBox Text3 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4080
         TabIndex        =   35
         Top             =   1560
         Width           =   3015
      End
      Begin VB.ComboBox Combo7 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   33
         Text            =   "Combo1"
         Top             =   1560
         Width           =   1455
      End
      Begin VB.ComboBox Combo6 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5880
         TabIndex        =   31
         Text            =   "Combo1"
         Top             =   1200
         Width           =   1455
      End
      Begin VB.ComboBox Combo5 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3240
         TabIndex        =   30
         Text            =   "Combo1"
         Top             =   1200
         Width           =   1695
      End
      Begin VB.ComboBox Combo4 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   29
         Text            =   "Combo1"
         Top             =   1200
         Width           =   855
      End
      Begin VB.ComboBox Combo3 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6480
         TabIndex        =   25
         Text            =   "Combo1"
         Top             =   840
         Width           =   855
      End
      Begin VB.ComboBox Combo2 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3240
         TabIndex        =   23
         Text            =   "Combo1"
         Top             =   840
         Width           =   1695
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   720
         TabIndex        =   22
         Text            =   "Combo1"
         Top             =   840
         Width           =   1695
      End
      Begin VB.TextBox Text2 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8760
         TabIndex        =   19
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   720
         TabIndex        =   17
         Top             =   480
         Width           =   6615
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Orientaci�n"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   6720
         TabIndex        =   38
         Top             =   2040
         Width           =   840
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Corp. / Local"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   3360
         TabIndex        =   37
         Top             =   2160
         Width           =   870
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Proyecto IDM"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   36
         Top             =   2040
         Width           =   945
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Asignada por"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   3000
         TabIndex        =   34
         Top             =   1680
         Width           =   915
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   32
         Top             =   1620
         Width           =   750
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Empresa"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   5040
         TabIndex        =   28
         Top             =   1260
         Width           =   615
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   2040
         TabIndex        =   27
         Top             =   1260
         Width           =   660
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Regulatorio"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   26
         Top             =   1260
         Width           =   825
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Impacto tecn."
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   5040
         TabIndex        =   24
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   2640
         TabIndex        =   21
         Top             =   900
         Width           =   390
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   20
         Top             =   900
         Width           =   300
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nro. Asignado"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   7680
         TabIndex        =   18
         Top             =   525
         Width           =   975
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   525
         Width           =   405
      End
   End
End
Attribute VB_Name = "frmPeticionesCargaNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub SSTab1_DblClick()

End Sub
