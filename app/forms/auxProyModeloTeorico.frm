VERSION 5.00
Begin VB.Form auxProyModeloTeorico 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccione el Costo y Beneficio"
   ClientHeight    =   3180
   ClientLeft      =   1140
   ClientTop       =   330
   ClientWidth     =   5505
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3180
   ScaleWidth      =   5505
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox Beneficio 
      Height          =   735
      Left            =   240
      TabIndex        =   5
      Top             =   1440
      Width           =   4935
   End
   Begin VB.ComboBox cboModeloBeneficio 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   240
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   960
      Width           =   4995
   End
   Begin VB.ComboBox cboModeloCosto 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   240
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   480
      Width           =   4995
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   240
      TabIndex        =   0
      Top             =   2280
      Width           =   4935
      Begin VB.CommandButton cmdBD 
         Caption         =   "CERRAR"
         Height          =   435
         Left            =   2490
         TabIndex        =   2
         Top             =   180
         Width           =   1170
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "ACEPTAR"
         Height          =   435
         Left            =   1290
         TabIndex        =   1
         Top             =   180
         Width           =   1170
      End
   End
End
Attribute VB_Name = "auxProyModeloTeorico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Dim m_CodModeloCosto As String
Dim m_NomModeloCosto As String
Dim m_CodModeloBeneficio As String
Dim m_NomModeloBeneficio As String
Dim m_Beneficio As String
Dim m_salida As Integer

Public Property Get NomModeloCosto() As String
    NomModeloCosto = m_NomModeloCosto
End Property
Public Property Get CodModeloCosto() As String
    CodModeloCosto = m_CodModeloCosto
End Property
Public Property Get NomModeloBeneficio() As String
    NomModeloBeneficio = m_NomModeloBeneficio
End Property
Public Property Get CodModeloBeneficio() As String
    CodModeloBeneficio = m_CodModeloBeneficio
End Property

Public Property Let CodModeloCosto(ByVal NewCodModeloCosto As String)
    m_CodModeloCosto = NewCodModeloCosto
End Property
Public Property Let CodModeloBeneficio(ByVal NewCodModeloBeneficio As String)
    m_CodModeloBeneficio = NewCodModeloBeneficio
End Property

Public Property Let txtBeneficio(ByVal NewtxtBeneficio As String)
    m_Beneficio = NewtxtBeneficio
End Property
Public Property Get txtBeneficio() As String
    txtBeneficio = m_Beneficio
End Property

Public Property Let txtSalida(ByVal NewtxtSalida As String)
    m_salida = NewtxtSalida
End Property
Public Property Get txtSalida() As String
    txtSalida = m_salida
End Property

Private Sub cboModeloBeneficio_Click()
    m_CodModeloBeneficio = CodigoCombo(cboModeloBeneficio, True)
    m_NomModeloBeneficio = TextoCombo(cboModeloBeneficio)
End Sub

Private Sub cboModeloCosto_Click()
    m_CodModeloCosto = CodigoCombo(cboModeloCosto, True)
    m_NomModeloCosto = TextoCombo(cboModeloCosto)
End Sub

Private Sub cmdOK_Click()
    m_Beneficio = Beneficio.Text
    m_salida = 1
    If CamposObligatorios Then
        If sp_UpdateProyectoBalanceCab(glNumeroProyecto, m_CodModeloCosto, m_CodModeloBeneficio, Beneficio.Text) Then
            'Call HabilitarBotones(0)
        End If
    End If
    Unload Me
End Sub
Private Sub cmdBD_Click()
    m_salida = 0
    Unload Me
End Sub

Private Sub Form_Initialize()
''    cboModeloCosto.Clear
''    cboModeloBeneficio.Clear
''    If sp_GetPerfilCostBenef(glNumeroProyecto) Then
''        Do While Not aplRST.EOF
''            cboModeloCosto.AddItem Trim(aplRST!nom_PerfilCostBenef) & Space(100) & "||" & Trim(aplRST!cod_PerfilCostBenef)
''            cboModeloBeneficio.AddItem Trim(aplRST!nom_PerfilCostBenef) & Space(100) & "||" & Trim(aplRST!cod_PerfilCostBenef)
''            cboModeloCosto.ListIndex = PosicionCombo(Me.cboModeloCosto, frmProyectosCarga.CodigoModeloCosto, True)
''            cboModeloBeneficio.ListIndex = PosicionCombo(Me.cboModeloBeneficio, frmProyectosCarga.CodigoModeloBeneficio, True)
''            Beneficio = Trim(aplRST!txt_Beneficio)
''            aplRST.MoveNext
''        Loop
''        aplRST.Close
''    End If

End Sub

Private Sub Form_Load()
    fjsNewStatusMessage Trim(Me.Name), 7    ' add -001- a.
'    cboModeloCosto.Clear
'    cboModeloBeneficio.Clear
    If sp_GetPerfilCostBenef(Null, Null) Then
        Do While Not aplRST.EOF
            cboModeloCosto.AddItem Trim(aplRST!nom_PerfilCostBenef) & Space(100) & "||" & Trim(aplRST!cod_PerfilCostBenef)
            cboModeloBeneficio.AddItem Trim(aplRST!nom_PerfilCostBenef) & Space(100) & "||" & Trim(aplRST!cod_PerfilCostBenef)
            aplRST.MoveNext
        Loop
        Beneficio = frmProyectosCarga.txt_Beneficio
        cboModeloCosto.ListIndex = PosicionCombo(Me.cboModeloCosto, frmProyectosCarga.CodigoModeloCosto, True)
        cboModeloBeneficio.ListIndex = PosicionCombo(Me.cboModeloBeneficio, frmProyectosCarga.CodigoModeloBeneficio, True)
        aplRST.Close
    End If
    Call FormCenter(Me, mdiPrincipal, False)
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(cboModeloCosto) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(cboModeloBeneficio) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(Beneficio.Text) = "" Then
        MsgBox ("Debe completar el Beneficio")
        CamposObligatorios = False
        Exit Function
    End If
End Function

