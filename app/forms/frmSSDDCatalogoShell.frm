VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSSDDCatalogoShell 
   Caption         =   "Trabajar con el cat�logo de tablas"
   ClientHeight    =   7380
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11085
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSSDDCatalogoShell.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7380
   ScaleWidth      =   11085
   Begin VB.Frame fraDatos 
      Caption         =   " Informaci�n "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Left            =   60
      TabIndex        =   22
      Top             =   4740
      Width           =   9915
      Begin VB.TextBox txtServidor 
         Height          =   315
         Left            =   1740
         MaxLength       =   40
         TabIndex        =   9
         Top             =   2100
         Width           =   2175
      End
      Begin VB.TextBox txtVisadoPor 
         Height          =   315
         Left            =   7140
         TabIndex        =   14
         Top             =   1740
         Width           =   2535
      End
      Begin VB.TextBox txtFechaVisado 
         Height          =   315
         Left            =   7140
         TabIndex        =   13
         Top             =   1380
         Width           =   1275
      End
      Begin VB.ComboBox cboVisado 
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7140
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   1020
         Width           =   855
      End
      Begin VB.ComboBox cboDatosSensibles 
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7140
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   660
         Width           =   855
      End
      Begin VB.ComboBox cboAplicativo 
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7140
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   300
         Width           =   2535
      End
      Begin VB.ComboBox cboDBMS 
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1740
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1740
         Width           =   2235
      End
      Begin VB.TextBox txtEsquema 
         Height          =   315
         Left            =   1740
         MaxLength       =   40
         TabIndex        =   7
         Top             =   1380
         Width           =   3315
      End
      Begin VB.TextBox txtBaseDeDatos 
         Height          =   315
         Left            =   1740
         MaxLength       =   40
         TabIndex        =   6
         Top             =   1020
         Width           =   3315
      End
      Begin VB.TextBox txtNombre 
         Height          =   315
         Left            =   1740
         MaxLength       =   40
         TabIndex        =   5
         Top             =   660
         Width           =   3315
      End
      Begin VB.TextBox txtCodigo 
         Height          =   315
         Left            =   1740
         TabIndex        =   4
         Top             =   300
         Width           =   1155
      End
      Begin AT_MaskText.MaskText txtPuerto 
         Height          =   315
         Left            =   4560
         TabIndex        =   37
         Top             =   2100
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Por"
         Height          =   195
         Index           =   11
         Left            =   5640
         TabIndex        =   34
         Top             =   1740
         Width           =   240
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Fecha visado"
         Height          =   195
         Index           =   10
         Left            =   5640
         TabIndex        =   33
         Top             =   1380
         Width           =   945
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Visado"
         Height          =   195
         Index           =   9
         Left            =   5640
         TabIndex        =   32
         Top             =   1020
         Width           =   465
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Datos sensibles"
         Height          =   195
         Index           =   8
         Left            =   5640
         TabIndex        =   31
         Top             =   660
         Width           =   1110
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Aplicativo"
         Height          =   195
         Index           =   7
         Left            =   5640
         TabIndex        =   30
         Top             =   300
         Width           =   690
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Puerto"
         Height          =   195
         Index           =   6
         Left            =   4020
         TabIndex        =   29
         Top             =   2160
         Width           =   480
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Servidor/Dataserver"
         Height          =   195
         Index           =   5
         Left            =   180
         TabIndex        =   28
         Top             =   2160
         Width           =   1470
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "DBMS"
         Height          =   195
         Index           =   4
         Left            =   180
         TabIndex        =   27
         Top             =   1800
         Width           =   405
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Esquema"
         Height          =   195
         Index           =   3
         Left            =   180
         TabIndex        =   26
         Top             =   1440
         Width           =   645
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Base de datos"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   25
         Top             =   1080
         Width           =   1020
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Nombre de la tabla"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   24
         Top             =   720
         Width           =   1350
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   23
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   7275
      Left            =   10020
      TabIndex        =   17
      Top             =   60
      Width           =   1035
      Begin VB.CommandButton cmdCampos 
         Caption         =   "Campos"
         Height          =   375
         Left            =   60
         TabIndex        =   38
         Top             =   2820
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   60
         TabIndex        =   21
         Top             =   6840
         Width           =   885
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   375
         Left            =   60
         TabIndex        =   15
         Top             =   6480
         Width           =   885
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   60
         TabIndex        =   20
         Top             =   6120
         Width           =   885
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Editar"
         Height          =   375
         Left            =   60
         TabIndex        =   19
         Top             =   5760
         Width           =   885
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Nuevo"
         Height          =   375
         Left            =   60
         TabIndex        =   18
         Top             =   5400
         Width           =   885
      End
   End
   Begin MSComctlLib.ImageList imgDatos 
      Left            =   9300
      Top             =   1260
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSSDDCatalogoShell.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSSDDCatalogoShell.frx":0B24
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswDatos 
      Height          =   3555
      Left            =   60
      TabIndex        =   16
      Top             =   1140
      Width           =   9915
      _ExtentX        =   17489
      _ExtentY        =   6271
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "imgDatos"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame fraFiltros 
      Caption         =   " Filtros "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9915
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "Filtrar"
         Height          =   375
         Left            =   8940
         TabIndex        =   3
         Top             =   180
         Width           =   885
      End
      Begin VB.ComboBox cboFiltroAplicativo 
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2460
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   600
         Width           =   2835
      End
      Begin VB.TextBox txtFiltroDBName 
         Height          =   315
         Left            =   2460
         TabIndex        =   1
         Top             =   240
         Width           =   3915
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Por c�digo de aplicativo:"
         Height          =   195
         Left            =   180
         TabIndex        =   36
         Top             =   660
         Width           =   1755
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Por nombre de base de datos:"
         Height          =   195
         Left            =   180
         TabIndex        =   35
         Top             =   300
         Width           =   2175
      End
   End
End
Attribute VB_Name = "frmSSDDCatalogoShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarPantalla
End Sub

Private Sub InicializarPantalla()
    Me.Top = 0
    Me.Left = 0
    Call InicializarCombos
    Call HabilitarBotones(0)
End Sub

Private Sub InicializarCombos()
    With cboDBMS
        .Clear
        If sp_GetDBMS(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!dbmsNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!dbmsId)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    
    With cboAplicativo
        .Clear
        If sp_GetAplicativo(Null, Null, "S") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!app_id)
                cboFiltroAplicativo.AddItem ClearNull(aplRST.Fields!app_id) & ESPACIOS & "||" & ClearNull(aplRST.Fields!app_id)
                aplRST.MoveNext
                DoEvents
            Loop
            cboFiltroAplicativo.AddItem "* Todos" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL", 0
            cboFiltroAplicativo.ListIndex = 0
        End If
    End With
    
    With cboDatosSensibles
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
    End With
    
    With cboVisado
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
    End With
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaLista As Variant)
    Call setHabilCtrl(txtCodigo, DISABLE)
    Call setHabilCtrl(txtNombre, DISABLE)
    Call setHabilCtrl(txtBaseDeDatos, DISABLE)
    Call setHabilCtrl(txtEsquema, DISABLE)
    Call setHabilCtrl(txtServidor, DISABLE)
    Call setHabilCtrl(txtPuerto, DISABLE)
    Call setHabilCtrl(cboDBMS, DISABLE)
    Call setHabilCtrl(cboAplicativo, DISABLE)
    Call setHabilCtrl(cboDatosSensibles, DISABLE)
    Call setHabilCtrl(cboVisado, DISABLE)
    Call setHabilCtrl(txtFechaVisado, DISABLE)
    Call setHabilCtrl(txtVisadoPor, DISABLE)
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            lswDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            Call setHabilCtrl(cmdCampos, NORMAL)
            sOpcionSeleccionada = ""
            If IsMissing(vCargaLista) Then
                Call CargarLista
            Else
                'Call MostrarSeleccion
            End If
        Case 1
            lswDatos.Enabled = False
            fraDatos.Enabled = True
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            Call setHabilCtrl(cmdCampos, DISABLE)
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    fraDatos.Caption = " AGREGAR "
                    txtCodigo = "": txtNombre = "": txtBaseDeDatos = "": txtEsquema = "": txtServidor = "": txtPuerto = "": txtFechaVisado = "": txtVisadoPor = ""
                    cboDBMS.ListIndex = -1
                    cboAplicativo.ListIndex = -1
                    cboDatosSensibles.ListIndex = -1
                    cboVisado.ListIndex = -1
                    Call setHabilCtrl(txtCodigo, NORMAL)
                    Call setHabilCtrl(txtNombre, NORMAL)
                    Call setHabilCtrl(txtBaseDeDatos, NORMAL)
                    Call setHabilCtrl(txtEsquema, NORMAL)
                    Call setHabilCtrl(txtServidor, NORMAL)
                    Call setHabilCtrl(txtPuerto, NORMAL)
                    Call setHabilCtrl(cboDBMS, NORMAL)
                    Call setHabilCtrl(cboAplicativo, NORMAL)
                    Call setHabilCtrl(cboDatosSensibles, NORMAL)
                    Call setHabilCtrl(cboVisado, DISABLE)
                    Call setHabilCtrl(txtFechaVisado, DISABLE)
                    Call setHabilCtrl(txtVisadoPor, DISABLE)
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    fraDatos.Caption = " MODIFICAR "
                    fraDatos.Enabled = True
                    Call setHabilCtrl(txtCodigo, DISABLE)
                    Call setHabilCtrl(txtNombre, NORMAL)
                    Call setHabilCtrl(txtBaseDeDatos, NORMAL)
                    Call setHabilCtrl(txtEsquema, NORMAL)
                    Call setHabilCtrl(txtServidor, NORMAL)
                    Call setHabilCtrl(txtPuerto, NORMAL)
                    Call setHabilCtrl(cboDBMS, NORMAL)
                    Call setHabilCtrl(cboAplicativo, NORMAL)
                    Call setHabilCtrl(cboDatosSensibles, NORMAL)
                    Call setHabilCtrl(cboVisado, DISABLE)
                    Call setHabilCtrl(txtFechaVisado, DISABLE)
                    Call setHabilCtrl(txtVisadoPor, DISABLE)
                    txtNombre.SetFocus
                Case "E"
                    fraDatos.Caption = " ELIMINAR "
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Sub InicializarLista()
    With lswDatos
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "Id.", 800
        .ColumnHeaders.Add , , "Tabla", 1600
        .ColumnHeaders.Add , , "Base de datos", 1400
        .ColumnHeaders.Add , , "Esquema", 800
        .ColumnHeaders.Add , , "DBMS", 1000
        .ColumnHeaders.Add , , "Server", 1000
        .ColumnHeaders.Add , , "Puerto", 800
        .ColumnHeaders.Add , , "App.", 1400
        .ColumnHeaders.Add , , "DS", 800
        .ColumnHeaders.Add , , "Visado", 800
        .ColumnHeaders.Add , , "codigo_dbms", 0
        .View = lvwReport
        .ListItems.Clear
    End With
End Sub

Private Sub CargarLista()
    Dim itmX As ListItem
    
    Call Puntero(True)
    Call InicializarLista
    Call Status("Cargando cat�logos...")
    With lswDatos
        If sp_GetCatalogoTabla(Null, txtFiltroDBName, CodigoCombo(cboFiltroAplicativo, True), Null) Then
            Do While Not aplRST.EOF
                Set itmX = .ListItems.Add(, , ClearNull(aplRST!id), , 2)
                itmX.SubItems(1) = ClearNull(aplRST!nombreTabla)
                itmX.SubItems(2) = ClearNull(aplRST!baseDeDatos)
                itmX.SubItems(3) = ClearNull(aplRST!Esquema)
                itmX.SubItems(4) = ClearNull(aplRST!DBMS_nombre)
                itmX.SubItems(5) = ClearNull(aplRST!servidorNombre)
                itmX.SubItems(6) = ClearNull(aplRST!servidorPuerto)
                itmX.SubItems(7) = ClearNull(aplRST!app_id)
                itmX.SubItems(8) = ClearNull(aplRST!datosSensibles)
                itmX.SubItems(9) = ClearNull(aplRST!visado)
                itmX.SubItems(10) = ClearNull(aplRST!DBMS)
                aplRST.MoveNext
                DoEvents
            Loop
            If .ListItems.Count > 0 Then
                .ListItems(1).Selected = True
                Set itmX = .ListItems(1)
                Call MostrarSeleccion(itmX)
            End If
        End If
    End With
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        Me.Height = 7890
        Me.Width = 11205
    End If
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertCatalogoTabla(txtCodigo, txtNombre, txtBaseDeDatos, txtEsquema, CodigoCombo(cboDBMS, True), txtServidor, txtPuerto, CodigoCombo(cboAplicativo), CodigoCombo(cboDatosSensibles, True)) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                Call sp_UpdateCatalogoTabla(txtCodigo, txtNombre, txtBaseDeDatos, txtEsquema, CodigoCombo(cboDBMS, True), txtServidor, txtPuerto, CodigoCombo(cboAplicativo), CodigoCombo(cboDatosSensibles, True))
                Call HabilitarBotones(0)
            End If
        Case "E"
            If sp_DeleteCatalogoTabla(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub lswDatos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Call MostrarSeleccion(Item)
End Sub

Sub MostrarSeleccion(ByVal Item As MSComctlLib.ListItem)
    With lswDatos
        If ClearNull(Item.Text) <> "" Then
            txtCodigo = Item.Text
            txtNombre = Item.SubItems(1)
            txtBaseDeDatos = Item.SubItems(2)
            txtEsquema = Item.SubItems(3)
            txtServidor = Item.SubItems(5)
            txtPuerto = Item.SubItems(6)
            cboDBMS.ListIndex = PosicionCombo(cboDBMS, Item.SubItems(10), True)
            cboAplicativo.ListIndex = PosicionCombo(cboAplicativo, Item.SubItems(7))
            cboDatosSensibles.ListIndex = PosicionCombo(cboDatosSensibles, Item.SubItems(8), True)
            cboVisado.ListIndex = PosicionCombo(cboVisado, Item.SubItems(9), True)
        End If
        If Item.ListSubItems(8).Text = "S" Then
            Call setHabilCtrl(cmdCampos, NORMAL)
        Else
            Call setHabilCtrl(cmdCampos, DISABLE)
        End If
    End With
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    
    If Trim(txtCodigo.Text) = "" Then
        MsgBox "Debe completar el c�digo", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.Text, ":") > 0 Then
        MsgBox "El C�digo no pude contener el caracter ':'", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtNombre.Text) = "" Then
        MsgBox "Debe completar el nombre de la tabla", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtNombre.Text, ":") > 0 Then
        MsgBox "La descripci�n no pude contener el caracter ':'", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If Trim(txtBaseDeDatos.Text) = "" Then
        MsgBox "Debe completar el nombre de la base de datos", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtBaseDeDatos.Text, ":") > 0 Then
        MsgBox "El nombre de la base de datos no pude contener el caracter ':'", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If Trim(txtEsquema.Text) = "" Then
        MsgBox "Debe completar el nombre del esquema", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtEsquema.Text, ":") > 0 Then
        MsgBox "El nombre del esquema no pude contener el caracter ':'", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If cboDBMS.ListIndex = -1 Then
        MsgBox "Debe seleccionar un DBMS", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If Trim(txtServidor.Text) = "" Then
        MsgBox "Debe completar el nombre del servidor/dataserver", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtServidor.Text, ":") > 0 Then
        MsgBox "El nombre del servidor/dataserver no pude contener el caracter ':'", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If Trim(txtPuerto.Text) = "" Then
        MsgBox "Debe completar el puerto del servidor/dataserver", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtPuerto.Text, ":") > 0 Then
        MsgBox "El puerto del servidor/dataserver no pude contener el caracter ':'", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If cboDatosSensibles.ListIndex = -1 Then
        MsgBox "Debe seleccionar si la tabla tiene datos sensibles", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub cmdCampos_Click()
    With lswDatos
        frmSSDDCatalogoDetalle.id = Val(.SelectedItem.Text)
        frmSSDDCatalogoDetalle.Show 1
    End With
End Sub

Private Sub cmdFiltrar_Click()
    Call CargarLista
End Sub
