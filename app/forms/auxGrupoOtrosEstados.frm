VERSION 5.00
Begin VB.Form auxSectorOtrosEstados 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3825
   ClientLeft      =   3345
   ClientTop       =   3375
   ClientWidth     =   4440
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3825
   ScaleWidth      =   4440
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraModoSolic 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1395
      Left            =   1320
      TabIndex        =   3
      Top             =   660
      Width           =   1545
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Opinión"
         Height          =   225
         Index           =   0
         Left            =   90
         TabIndex        =   7
         Top             =   240
         Width           =   1000
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Evaluacón"
         Height          =   225
         Index           =   1
         Left            =   90
         TabIndex        =   6
         Top             =   510
         Width           =   1335
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Estimación"
         Height          =   225
         Index           =   2
         Left            =   90
         TabIndex        =   5
         Top             =   780
         Width           =   1335
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Planificación"
         Height          =   225
         Index           =   3
         Left            =   90
         TabIndex        =   4
         Top             =   1050
         Width           =   1335
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   435
      Left            =   780
      TabIndex        =   2
      Top             =   2280
      Width           =   1365
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   435
      Left            =   2370
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2280
      Width           =   1365
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Seleccione que tipo de requerimiento  realizará"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   240
      TabIndex        =   0
      Top             =   210
      Width           =   4035
   End
End
Attribute VB_Name = "auxSectorOtrosEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public bAceptar As Boolean
Public pubEstado As String

Private Sub cmdAceptar_Click()
    pubEstado = getEstadoSolic
    bAceptar = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    bAceptar = False
    Me.Hide
End Sub

Private Sub Form_Load()
    bAceptar = False
End Sub

Sub setearEstadoSolic()
'    Select Case pubEstado
'    Case "OPINIO"
        OptModoSolic(0).Value = True
'    Case "EVALUA"
'        OptModoSolic(1).Value = True
'    Case "ESTIMA"
'        OptModoSolic(2).Value = True
'    Case "PLANIF"
'        OptModoSolic(3).Value = True
'    Case Else
'        OptModoSolic(0).Value = True
'    End Select
End Sub
Function getEstadoSolic()
    getEstadoSolic = ""
    If OptModoSolic(0).Value = True Then
        getEstadoSolic = "OPINIO"
    End If
    If OptModoSolic(1).Value = True Then
        getEstadoSolic = "EVALUA"
    End If
    If OptModoSolic(2).Value = True Then
        getEstadoSolic = "ESTIMA"
    End If
    If OptModoSolic(3).Value = True Then
        getEstadoSolic = "PLANIF"
    End If
End Function

