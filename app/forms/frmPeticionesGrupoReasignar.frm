VERSION 5.00
Begin VB.Form frmPeticionesGrupoReasignar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Reasignación Grupo Interviniente"
   ClientHeight    =   3270
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   8280
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   8280
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cboGrupo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   90
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   16
      ToolTipText     =   "Sector Solicitante"
      Top             =   2040
      Width           =   8010
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   795
      Left            =   60
      TabIndex        =   4
      Top             =   -60
      Width           =   8190
      Begin VB.Label Label6 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   10
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   7620
         TabIndex        =   9
         Top             =   480
         Width           =   480
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1305
         TabIndex        =   8
         Top             =   240
         Width           =   6705
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1300
         TabIndex        =   7
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6705
         TabIndex        =   6
         Top             =   480
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Petición"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   5
         Top             =   240
         Width           =   1200
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   11
      Top             =   660
      Width           =   8190
      Begin VB.Label Label4 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   15
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoSector 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5565
         TabIndex        =   14
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label9 
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   13
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblSector 
         Caption         =   "sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   12
         Top             =   180
         Width           =   2745
      End
   End
   Begin VB.Frame pnlBtnControl 
      Height          =   645
      Left            =   60
      TabIndex        =   0
      Top             =   2610
      Width           =   8190
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6210
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7170
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame Frame3 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   17
      Top             =   1100
      Width           =   8190
      Begin VB.Label Label5 
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   21
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoGrupo 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5580
         TabIndex        =   20
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label8 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   19
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblGrupo 
         Caption         =   "grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   18
         Top             =   180
         Width           =   2745
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Reasignar al grupo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   90
      TabIndex        =   3
      Top             =   1710
      Width           =   1635
   End
End
Attribute VB_Name = "frmPeticionesGrupoReasignar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 13.10.2009 - Se agrega un parámetro que indica el origen del cambio a realizar. Ver opciones en la definición del SP.

Option Explicit

Dim sOpcionSeleccionada As String
Dim EstadoGrupo As String
Dim sGrupo As String
Dim sSector As String
Dim sGerencia As String
Dim sDireccion As String
Dim sCoorSect As String
Dim EstadoPeticion As String
Dim NuevoEstadoPeticion As String
Dim EstadoSector As String
Dim NuevoEstadoSector As String
Dim NuevoEstadoGrupo As String
Dim xPerfNivel, xPerfArea As String
Dim hst_nrointerno_sol
Dim flgAccion As Boolean

Private Sub Form_Load()
    Call InicializarPantalla
    Call InicializarCombos
    cmdConfirmar.Enabled = True
End Sub

Sub InicializarPantalla()
   Dim auxNro As String
    Me.Tag = ""
    
    cmdConfirmar.Enabled = False
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNro = "S/N"
            Else
                auxNro = ClearNull(aplRST!pet_nroasignado)
            End If
            lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
            lblPrioridad = ClearNull(aplRST!prioridad)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
        End If
        'aplRST.Close
    End If

    If sp_GetPeticionSector(glNumeroPeticion, glSector) Then
       If Not aplRST.EOF Then
          lblSector = ClearNull(aplRST!cod_sector) & " : " & ClearNull(aplRST!nom_sector)
          lblEstadoSector = ClearNull(aplRST!nom_estado)
        End If
        'aplRST.Close
    End If

    If sp_GetPeticionGrupo(glNumeroPeticion, glSector, glGrupo) Then
        If Not aplRST.EOF Then
            fIniPlan = aplRST!fe_ini_plan
            fFinPlan = aplRST!fe_fin_plan
            fIniReal = aplRST!fe_ini_real
            fFinReal = aplRST!fe_fin_real
            hsPresup = Val(ClearNull(aplRST!horaspresup))
            EstadoGrupo = ClearNull(aplRST!cod_estado)
            lblGrupo = ClearNull(aplRST!nom_grupo)
            lblEstadoGrupo = ClearNull(aplRST!nom_estado)
            sGerencia = ClearNull(aplRST!cod_gerencia)
            sDireccion = ClearNull(aplRST!cod_direccion)
            sSector = ClearNull(aplRST!cod_sector)
            sGrupo = ClearNull(aplRST!cod_grupo)
        End If
        'aplRST.Close
    End If
End Sub

Sub InicializarCombos()
    Dim flgExtendido As Boolean
    Dim xAccion As String
    Dim sDireccion As String
    Dim sGerencia As String
    
    'si tiene ingerencia en el area del sector
    cboGrupo.Clear
    If sp_GetGrupoXt("", "", "") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboGrupo.AddItem Trim(aplRST!nom_direccion) & "> " & Trim(aplRST!nom_gerencia) & "> " & Trim(aplRST!nom_sector) & "> " & Trim(aplRST!nom_grupo) & Space(100) & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboGrupo.AddItem sDireccion & " » " & sGerencia & " » " & Trim(aplRST!nom_sector) & " » " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
            End If
            aplRST.MoveNext
        Loop
        'aplRST.Close
        cboGrupo.ListIndex = 0
    End If
End Sub

Private Sub cmdConfirmar_Click()
    Dim vRetorno() As String
    Dim nwSector As String, nwGrupo As String
    Dim NroHistorial As Long
    
    If cboGrupo.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un Grupo")
        Exit Sub
    End If

    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = False
    Call Puntero(True)
    Call Status("Procesando...")
    If ParseString(vRetorno, DatosCombo(cboGrupo), "|") > 0 Then
        nwGrupo = vRetorno(2)
        nwSector = vRetorno(1)
    End If
    Call reasignarPeticionGrupo(glNumeroPeticion, glSector, glGrupo, nwSector, nwGrupo, "PET")    ' upd -002- a.
    Call Puntero(False)
    Call Status("Listo.")
    Call touchForms
    Unload Me
End Sub

'Private Sub cboGrupo_Click()
    ' *** No funciona. Hay que evitar la sobrecarga del control porque pierde el índice ***
    'cboGrupo.ToolTipText = TextoCombo(cboGrupo, CodigoCombo(cboGrupo, True), True)
'End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub
