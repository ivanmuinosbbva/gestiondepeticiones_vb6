VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form rptPetRecHs 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5385
   ClientLeft      =   795
   ClientTop       =   2025
   ClientWidth     =   10185
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5385
   ScaleWidth      =   10185
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      Height          =   3945
      Left            =   0
      TabIndex        =   7
      Top             =   840
      Width           =   10155
      Begin MSComCtl2.UpDown udPorcentaje 
         Height          =   315
         Left            =   3600
         TabIndex        =   16
         Top             =   2400
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         _Version        =   393216
         BuddyControl    =   "txtPorc"
         BuddyDispid     =   196624
         OrigLeft        =   3840
         OrigTop         =   2580
         OrigRight       =   4095
         OrigBottom      =   2895
         Max             =   100
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin VB.OptionButton chkOpt 
         Caption         =   "S�lo recursos con carga inferior a"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   15
         Top             =   2460
         Value           =   -1  'True
         Width           =   2775
      End
      Begin VB.OptionButton chkOpt 
         Caption         =   "Totales por grupo c/detalle por recurso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   5
         Top             =   2100
         Width           =   3255
      End
      Begin VB.OptionButton chkOpt 
         Caption         =   "Totales por grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   4
         Top             =   1740
         Width           =   1725
      End
      Begin VB.ComboBox cboEstructura 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   900
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   420
         Width           =   8925
      End
      Begin AT_MaskText.MaskText txtDESDE 
         Height          =   315
         Left            =   900
         TabIndex        =   2
         Top             =   780
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTA 
         Height          =   315
         Left            =   900
         TabIndex        =   3
         Top             =   1140
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPorc 
         Height          =   315
         Left            =   3150
         TabIndex        =   6
         Top             =   2400
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   556
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   3
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3960
         TabIndex        =   14
         Top             =   2460
         Width           =   165
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "F.Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   13
         Top             =   1200
         Width           =   570
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "F.Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   12
         Top             =   840
         Width           =   600
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   11
         Top             =   480
         Width           =   345
      End
   End
   Begin VB.Frame fraOpciones 
      Height          =   630
      Left            =   0
      TabIndex        =   8
      Top             =   4755
      Width           =   10155
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8160
         TabIndex        =   10
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9120
         TabIndex        =   9
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "4. HORAS INFORMADAS POR �REA EJECUTORA - % de horas informadas"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6915
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptPetRecHs.frx":0000
      Stretch         =   -1  'True
      Top             =   0
      Width           =   10140
   End
End
Attribute VB_Name = "rptPetRecHs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 15.05.2008 - Se cambia la asignaci�n de la propiedad 'ReportFileName' del control CrystalReport utilizando la funci�n de formato de ruta corto (8.3) porque por ejemplo, cuando el string tiene 127 caracteres da un error 20504 en runtime al momento de mostrar el reporte (da error de archivo de reporte no encontrado).
' -003- a. FJS 03.09.2009 - Se restringe la carga del combobox solo para �reas habilitadas, recursos habiltados, etc.
' -003- b. FJS 03.09.2009 - Se modifica el mensaje de status.

Option Explicit

Dim flgEnCarga As Boolean
Dim vRetorno() As String
Dim xNivel As Long
Dim Porcentaje As Integer

Private Sub Form_Load()
    flgEnCarga = False
    Call InicializarCombos
    txtDESDE.Text = Format(DateAdd("m", -1, date), "dd/mm/yyyy")
    txtHASTA.Text = Format(date, "dd/mm/yyyy")
    txtPorc.Text = "70": Porcentaje = 70
End Sub

Private Sub InicializarCombos()
    Dim idxEstruc As Integer
    Dim sDireccion As String
    Dim sGerencia As String
    
    flgEnCarga = True
    Call Puntero(True)
    Call Status("Inicializando...")
    
    If sp_GetDireccion("", "S") Then        ' upd -003- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                cboEstructura.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & Space(150) & "||DIRE|" & Trim(aplRST!cod_direccion)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGerenciaXt("", "", "S") Then   ' upd -003- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboEstructura.AddItem sDireccion & "� " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||GERE|" & Trim(aplRST!cod_gerencia)
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(100) & "||GERE|" & Trim(aplRST!cod_gerencia)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetSectorXt(Null, Null, Null, "S") Then   ' upd -003- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboEstructura.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||SECT|" & Trim(aplRST!cod_sector)
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(100) & "||SECT|" & Trim(aplRST!cod_sector)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGrupoXt("", "", "S") Then  ' upd -003- a.
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboEstructura.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||GRUP|" & Trim(aplRST!cod_grupo)
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(80) & "||GRUP|" & Trim(aplRST!cod_grupo)
            End If
            aplRST.MoveNext
        Loop
    End If
   cboEstructura.AddItem "Todo el BBVA" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL|NULL", 0
   cboEstructura.ListIndex = -1         ' Antes: 0
    
    flgEnCarga = False
    Call Status("Listo.")
    Call Puntero(False)
End Sub

Private Sub cboEstructura_Click()
    'Dim vRetorno() As String
    Dim xNivel As String
    Dim flgRecurso As Boolean
    
    If flgEnCarga = True Then
        Exit Sub
    End If
        
    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
        MsgBox ("Error")
        Exit Sub
    End If
    Call Puntero(True)
    
    flgEnCarga = True
    flgRecurso = True
    
    Call Puntero(False)
    flgEnCarga = False
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdOK_Click()
    If CamposObligatorios Then
        If glCrystalNewVersion Then
            Call NuevoReporte
        Else
            'Call ViejoReporte
        End If
    End If
    'Call ProcesarReporte
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    Dim sTitulo(3) As String
    
    Call Puntero(True)
    sReportName = "\hstrarec6.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
        
    sTitulo(0) = "�rea ejecutora: " & ClearNull(TextoCombo(cboEstructura))
    If chkOpt(0).Value = True Then
        sTitulo(1) = "Totales por grupo"
    ElseIf chkOpt(1).Value = True Then
        sTitulo(1) = "Totales por grupo c/detalle x Recurso"
    ElseIf chkOpt(2).Value = True Then
        sTitulo(1) = "Solo recursos con carga inferior al " & txtPorc & "%"
    End If
    sTitulo(2) = "Entre: " & txtDESDE & " y " & txtHASTA
        
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crPortrait
        .ReportTitle = "Horas informadas por �rea Ejecutora"
        .ReportComments = sTitulo(0) & vbCrLf & sTitulo(1) & ". " & sTitulo(2)
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
    
    'Abrir el reporte
    'Call Puntero(True)
    
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fdesde": crParamDef.AddCurrentValue (Format(txtDESDE, "yyyymmdd"))
            Case "@fhasta": crParamDef.AddCurrentValue (Format(txtHASTA, "yyyymmdd"))
            Case "@nivel": crParamDef.AddCurrentValue (CStr(vRetorno(1)))
            Case "@area": crParamDef.AddCurrentValue (CStr(vRetorno(2)))
            Case "@recurso": crParamDef.AddCurrentValue ("NULL")
            Case "@porcmin": crParamDef.AddCurrentValue (CStr(Porcentaje))
            'Case "@porcmin": crParamDef.AddCurrentValue (CStr(txtPorc))
        End Select
    Next

    With crReport.FormulaFields
        .Item(5).Text = Chr(34) & sTitulo(0) & Chr(34)
        .Item(6).Text = Chr(34) & sTitulo(1) & ". " & sTitulo(2) & Chr(34)
    End With
    
    With crReport
        If chkOpt(0).Value = True Then              ' Totales por grupo
            .Sections("GF1").Suppress = False
            .Sections("D").Suppress = True
            .Sections("GH1").Suppress = True
        ElseIf chkOpt(1).Value = True Then          ' Totales por grupo c/ detalle por recurso
            .Sections("GF1").Suppress = False
            .Sections("D").Suppress = False
            .Sections("GH1").Suppress = False
        ElseIf chkOpt(2).Value = True Then          ' Solo recursos con carga inferior a %
            .Sections("GF1").Suppress = True
            .Sections("D").Suppress = False
            .Sections("GH1").Suppress = False
        End If
    End With
    
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show vbModal
    
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

'Private Sub ViejoReporte()
'    Dim cPathFileNameReport As String
'    Dim txtAux As String
'    Dim sTitulo As String
'    Dim xRet As Integer
'    Dim vRetorno() As String
'
'    If Not CamposObligatorios Then Exit Sub
'
'    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then MsgBox ("Error")
'    cPathFileNameReport = App.Path & "\" & "hstrarec.rpt"
'
'    With mdiPrincipal.CrystalReport1
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .RetrieveStoredProcParams
'        .StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
'        .StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
'        .StoredProcParam(2) = vRetorno(1)
'        .StoredProcParam(3) = vRetorno(2)
'        .StoredProcParam(4) = "NULL"
'        If chkOpt(0).Value = True Then
'            .StoredProcParam(5) = "999"
'            txtAux = "Totales por Grupo, "
'            .SectionFormat(0) = "GF1;T;X;X;X;X;X;X"
'            .SectionFormat(1) = "DETAIL;F;X;X;X;X;X;X"
'            .SectionFormat(2) = "GH1;F;X;X;X;X;X;X"
'        End If
'        If chkOpt(1).Value = True Then
'            .StoredProcParam(5) = "999"
'            txtAux = "Totales por Grupo c/Detalle x Recurso, "
'            .SectionFormat(0) = "GF1;T;X;X;X;X;X;X"
'            .SectionFormat(1) = "DETAIL;T;X;X;X;X;X;X"
'            .SectionFormat(2) = "GH1;T;X;X;X;X;X;X"
'        End If
'        If chkOpt(2).Value = True Then
'            .StoredProcParam(5) = txtPorc.Text
'            txtAux = "Solo recursos con carga inferior al " & ClearNull(txtPorc.Text) & "%, "
'            .SectionFormat(0) = "GF1;F;X;X;X;X;X;X"
'            .SectionFormat(1) = "DETAIL;T;X;X;X;X;X;X"
'            .SectionFormat(2) = "GH1;T;X;X;X;X;X;X"
'        End If
'        .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'        .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'        .Formulas(2) = "@0_TITULO='" & "Area Ejecutora: " & ClearNull(TextoCombo(cboEstructura)) & "'"
'        .Formulas(3) = "@1_TITULO='" & txtAux & "Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy")) & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        .Formulas(2) = ""
'        .Formulas(3) = ""
'        .Formulas(4) = ""
'        .SectionFormat(0) = "GF1;X;X;X;X;X;X;X"
'        .SectionFormat(1) = "DETAIL;X;X;X;X;X;X;X"
'        .SectionFormat(2) = "GH1;X;X;X;X;X;X;X"
'    End With
'   'Call cmdCancelar_Click
'End Sub

Private Sub chkOpt_Click(Index As Integer)
    If chkOpt(0).Value = True Or chkOpt(1).Value = True Then
        txtPorc.Text = ""
        Call setHabilCtrl(txtPorc, "DIS")
        Porcentaje = 999
    End If
    If chkOpt(2).Value = True Then
        Call setHabilCtrl(txtPorc, "NOR")
        txtPorc.Text = "70"                 ' Por defecto
        Porcentaje = 70
    End If
End Sub

Private Sub txtPorc_LostFocus()
    If ClearNull(txtPorc) <> "" Then
        If IsNumeric(txtPorc) Then
            Debug.Print "Asignando porcentaje: " & txtPorc
            Porcentaje = CInt(txtPorc)
        End If
    End If
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    
    If cboEstructura.ListIndex < 0 Then
        MsgBox "Debe seleccionar el �rea para el informe", vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    If txtDESDE = "" Then
        MsgBox "Falta Fecha inicio informe", vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA = "" Then
        MsgBox "Falta Fecha fin informe", vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox "Fecha Hasta menor que Fecha Desde", vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    If chkOpt(2).Value Then
        If txtPorc.Text = "" Then
            MsgBox "Debe especificar porcentaje", vbExclamation
            CamposObligatorios = False
            Exit Function
        Else
            If CInt(txtPorc) > 100 Or CInt(txtPorc) < 1 Then
                MsgBox "El porcentaje debe ser un entero comprendido entre 1 y 100", vbExclamation
                CamposObligatorios = False
                txtPorc.SetFocus
                Exit Function
            End If
        End If
    End If
End Function

Private Sub udPorcentaje_Change()
    If Trim(txtPorc) <> "" Then
        Porcentaje = CInt(txtPorc.Text)
        Debug.Print "Cambio: " + CStr(Porcentaje)
    End If
End Sub
