VERSION 5.00
Begin VB.Form auxSetHorasApp 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Horas trabajadas por Aplicativo"
   ClientHeight    =   2985
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5040
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   5040
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraNivel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   4815
      Begin VB.OptionButton OptNivel 
         Caption         =   "Mantener el detalle cargado de horas trabajadas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   150
         TabIndex        =   2
         Top             =   600
         Width           =   3975
      End
      Begin VB.OptionButton OptNivel 
         Caption         =   "Borrar el detalle anterior y volver a desglosar las horas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   150
         TabIndex        =   1
         Top             =   840
         Width           =   4485
      End
      Begin VB.Label LblDatosRec 
         Caption         =   "Opciones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   3
         Top             =   180
         Width           =   1290
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   7
      Top             =   0
      Width           =   4815
      Begin VB.Label lblWarning 
         Caption         =   "lblWarning"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   615
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   4575
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   4
      Top             =   2190
      Width           =   4815
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   1290
         TabIndex        =   6
         Top             =   180
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   2490
         TabIndex        =   5
         Top             =   180
         Width           =   1170
      End
   End
End
Attribute VB_Name = "auxSetHorasApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    lblWarning.Caption = "Ya existe un desglose previo por aplicativo para esta carga de horas. Deber� especificar las modificaciones para el detalle cargado. Caso contrario, cancele."
    OptNivel(0).Value = True
    OptNivel(1).Value = False
End Sub

Private Sub cmdOk_Click()
    ' Mantener el desglose anterior
    If OptNivel(1).Value = True Then
        'frmCargaHoras.bMantenerDesglose = False
    Else
        'frmCargaHoras.bMantenerDesglose = True
    End If
    Unload Me
End Sub

Private Sub cmdCerrar_Click()
    'frmCargaHoras.bDesgloseCancelado = True
    Unload Me
End Sub
