VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCLISTSoli 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Solicitudes que intervienen en el DSN seleccionado"
   ClientHeight    =   6810
   ClientLeft      =   4365
   ClientTop       =   2805
   ClientWidth     =   10245
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6810
   ScaleWidth      =   10245
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraSecundario 
      Height          =   1335
      Left            =   120
      TabIndex        =   3
      Top             =   5400
      Width           =   8655
   End
   Begin VB.Frame fraBotonera 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Left            =   8880
      TabIndex        =   1
      Top             =   0
      Width           =   1335
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "C&errar"
         Height          =   500
         Left            =   80
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Cerrar esta pantalla"
         Top             =   6120
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5280
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8700
      _ExtentX        =   15346
      _ExtentY        =   9313
      _Version        =   393216
      FixedCols       =   0
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      FocusRect       =   0
      GridLinesFixed  =   1
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCLISTSoli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public cDSN_Nom As String
Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long

Private Sub Form_Load()
    Call IniciarScroll(grdDatos)
    Call Inicializar_Grilla
    Call CargarGrilla
End Sub

Private Sub Inicializar_Grilla()
    With grdDatos
        .Visible = False
        .Clear
        .Font.name = "Tahoma"
        .Font.Size = 8
        .cols = 9
        .Rows = 1
        .TextMatrix(0, 0) = "N�": .ColWidth(0) = 1000
        .TextMatrix(0, 1) = "EME": .ColWidth(1) = 500: .ColAlignment(1) = flexAlignCenterCenter
        .TextMatrix(0, 2) = "Tipo": .ColWidth(2) = 800
        .TextMatrix(0, 3) = "Enm.": .ColWidth(3) = 600
        .TextMatrix(0, 4) = "Archivo": .ColWidth(4) = 3500: .ColAlignment(4) = flexAlignLeftCenter
        .TextMatrix(0, 5) = "Fecha": .ColWidth(5) = 1600
        .TextMatrix(0, 6) = "Peticion": .ColWidth(6) = 1000
        .TextMatrix(0, 7) = "Solicitante": .ColWidth(7) = 2000
        .TextMatrix(0, 8) = "Estado": .ColWidth(8) = 2500
        .FocusRect = flexFocusNone
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub CargarGrilla()
    If sp_GetHEDTSolInt(cDSN_Nom) Then
        Call Puntero(True)
        With grdDatos
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST.Fields!sol_nroasignado)
                '.TextMatrix(.Rows - 1, 1) = ClearNull(aplRST.Fields!sol_eme)
                .TextMatrix(.Rows - 1, 1) = IIf(ClearNull(aplRST.Fields!sol_eme) = "S", "�", "")
                .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST.Fields!sol_tipo)
                .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST.Fields!sol_mask)
                .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST.Fields!nom_archivo_prod)
                .TextMatrix(.Rows - 1, 5) = Format(aplRST.Fields!SOL_FECHA, "dd/MM/yyyy hh:mm")
                .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST.Fields!pet_nroasignado)
                .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST.Fields!nom_recurso)
                .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST.Fields!nom_estado)
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
    With grdDatos
        .Visible = True
        .Col = .cols - 1
        .Row = .Rows - 1
    End With
    Call Puntero(False)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub grdDatos_Click()
    bSorting = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    bSorting = False
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub
