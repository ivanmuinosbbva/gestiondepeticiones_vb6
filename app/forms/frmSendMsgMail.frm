VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmSendMsgMail 
   Caption         =   "Consulta Mensajes"
   ClientHeight    =   6195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6195
   ScaleWidth      =   9480
   WindowState     =   2  'Maximized
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1995
      Left            =   0
      TabIndex        =   6
      Top             =   4200
      Width           =   8160
      Begin VB.TextBox txtMensaje1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   750
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         Top             =   360
         Width           =   7935
      End
      Begin VB.TextBox txtMensaje2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   690
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   7
         Top             =   1200
         Width           =   7935
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Observaciones"
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   180
         Width           =   1065
      End
   End
   Begin VB.Frame Frame2 
      Height          =   6165
      Left            =   8190
      TabIndex        =   1
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Cierra esta consulta"
         Top             =   5640
         Width           =   1170
      End
      Begin VB.CommandButton cmdBegin 
         Caption         =   "COMENZAR"
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Accede a la Petici�n invoucrada en este Mensaje"
         Top             =   4500
         Width           =   1170
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1035
      Left            =   10
      TabIndex        =   0
      Top             =   0
      Width           =   8145
      Begin AT_MaskText.MaskText txtFecha 
         Height          =   315
         Left            =   3915
         TabIndex        =   10
         ToolTipText     =   "Fecha en que se remiti� al Administrador"
         Top             =   540
         Width           =   1530
         _ExtentX        =   2699
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2850
         TabIndex        =   11
         Top             =   615
         Width           =   480
      End
      Begin VB.Label lblPerfil 
         Alignment       =   2  'Center
         Caption         =   "GENERACION AUTOMATICA DE MAILS"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   225
         Left            =   180
         TabIndex        =   5
         Top             =   240
         Width           =   7845
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3210
      Left            =   -30
      TabIndex        =   4
      Top             =   1020
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   5662
      _Version        =   65541
      Cols            =   10
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmSendMsgMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim sOpcionSeleccionada As String
Dim sEstadoQuery As String
Dim flgNumero As Boolean

Dim sNumeroMensaje
Dim sPerfil As String
Dim fPerfil As String


Const colFechaMsg = 0
Const colNomPerfil = 1
Const colNroAsignado = 2
Const colTitulo = 3
Const colTexto1 = 4
Const colTexto2 = 5
Const colNroInterno = 6
Const colNromensaje = 7
Const colFechaSrt = 8
Const colCODPERFIL = 9


Private Sub cmdBegin_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    CargarGrid
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call LockProceso(False)
   Unload Me
End Sub

Private Sub Form_Load()
    txtFecha.Text = Format(Date, "dd/mm/yyyy")
End Sub
Private Sub Form_Resize()
   Me.WindowState = vbMaximized
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
End Sub

Sub CargarGrid()
    Dim sDatRecurso()
    ReDim sDatRecurso(1 To 1)
    Dim nRowRecurso, nPtrRecurso As Integer
    nRowRecurso = 0
    
    If sp_GetRecurso("", "R", "A") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields.Item("email")) <> "" Then
                nRowRecurso = nRowRecurso + 1
                ReDim Preserve sDatRecurso(1 To nRowRecurso)
                sDatRecurso(nRowRecurso) = ClearNull(aplRST.Fields.Item("cod_recurso"))
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
        
            
   For nPtrRecurso = 1 To nRowRecurso
            DoEvents
            Call puntero(True)
            Call Status("Cargando Mensajes: " & sDatRecurso(nPtrRecurso))
            If Not sp_GetMensajeRecurso(sDatRecurso(nPtrRecurso), "NULL") Then
'''            If Not sp_GetMensajeRecurso(sDatRecurso(nPtrRecurso), Format(txtFecha.DateValue, "yyyymmdd")) Then
               GoTo finx
            End If
            With grdDatos
                .Clear
                .HighLight = flexHighlightNever
                .Rows = 1
                .TextMatrix(0, colNroAsignado) = "Petici�n"
                .TextMatrix(0, colNomPerfil) = "Perfil"
                .TextMatrix(0, colFechaMsg) = "Fecha"
                .TextMatrix(0, colTitulo) = "T�tulo"
                .TextMatrix(0, colNroInterno) = "NroInt."
                .TextMatrix(0, colNromensaje) = "NroMsg."
        
                .ColWidth(colFechaMsg) = 1000
                .ColWidth(colNroAsignado) = 600
                .ColWidth(colTitulo) = 5600
                .ColWidth(colNomPerfil) = 1000
                .ColWidth(colNroInterno) = 0
                .ColWidth(colNromensaje) = 0
                .ColWidth(colFechaSrt) = 0
                .ColWidth(colTexto1) = 0
                .ColWidth(colTexto2) = 0
                .ColWidth(colCODPERFIL) = 0
            End With
            DoEvents
            Do While Not aplRST.EOF
                With grdDatos
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, colFechaMsg) = IIf(Not IsNull(aplRST!fe_mensaje), Format(aplRST!fe_mensaje, "dd/mm/yyyy"), "")
                    .TextMatrix(.Rows - 1, colNomPerfil) = ClearNull(aplRST!nom_perfil)
                    .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
                    .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!titulo)
                    .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
                    .TextMatrix(.Rows - 1, colNromensaje) = ClearNull(aplRST!msg_nrointerno)
                    .TextMatrix(.Rows - 1, colFechaSrt) = IIf(Not IsNull(aplRST!fe_mensaje), "X" & Format(aplRST!fe_mensaje, "yyyymmdd") & ClearNull(aplRST!msg_nrointerno), "")
                    .TextMatrix(.Rows - 1, colTexto1) = ClearNull(aplRST!msg_texto)
                    .TextMatrix(.Rows - 1, colTexto2) = ClearNull(aplRST!txt_txtmsg)
                    .TextMatrix(.Rows - 1, colCODPERFIL) = ClearNull(aplRST!cod_perfil)
                End With
                aplRST.MoveNext
            Loop
            aplRST.Close
    
finx:
            Call puntero(False)
            Call Status("Listo")
    Next

End Sub

'''Public Sub MostrarSeleccion(Optional flgSort)
'''    If IsMissing(flgSort) Then
'''        flgSort = True
'''    End If
'''
'''    sNumeroMensaje = ""
'''    glNumeroPeticion = ""
'''
'''    cmdVisualizar.Enabled = False
'''    cmdEliminar.Enabled = False
'''
'''    With grdDatos
'''        If .RowSel > 0 Then
'''            .HighLight = flexHighlightAlways
'''            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
'''                glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
'''                sNumeroMensaje = .TextMatrix(.RowSel, colNromensaje)
'''                sPerfil = .TextMatrix(.RowSel, colCODPERFIL)
'''                txtMensaje1.Text = .TextMatrix(.RowSel, colTexto1)
'''                txtMensaje2.Text = .TextMatrix(.RowSel, colTexto2)
'''                cmdVisualizar.Enabled = True
'''                cmdEliminar.Enabled = True
'''            End If
'''        End If
'''    End With
'''End Sub
'''Sub FiltrarGrid()
'''    Dim nRows As Integer
'''
'''    nRows = 1
'''    With grdDatos
'''        If .Rows > 1 Then
'''            Do While nRows < .Rows
'''                If fPerfil = "NULL" Then
'''                    .RowHeight(nRows) = -1
'''                ElseIf ClearNull(.TextMatrix(nRows, colCODPERFIL)) = fPerfil Then
'''                    .RowHeight(nRows) = -1
'''                Else
'''                    .RowHeight(nRows) = 0
'''                End If
'''                nRows = nRows + 1
'''            Loop
'''            .RowSel = 0
'''            Call MostrarSeleccion
'''        End If
'''    End With
'''    Call LockProceso(False)
'''    Call puntero(False)
'''    Call Status("Listo")
'''End Sub

