VERSION 5.00
Begin VB.Form rptAccionesPorPerfil 
   BorderStyle     =   0  'None
   Caption         =   "Form2"
   ClientHeight    =   4710
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7905
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   7905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraRpt 
      Height          =   2925
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   7860
      Begin VB.CheckBox chkSoloAccionesHabilitadas 
         Caption         =   "S�lo acciones habilitadas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1320
         Width           =   2175
      End
      Begin VB.ComboBox cboAgrupador 
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   720
         Width           =   3045
      End
      Begin VB.ComboBox cboPerfil 
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   3045
      End
      Begin VB.Label lblAguarde 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   2400
         Width           =   7515
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   8
         Top             =   795
         Width           =   1065
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Perfil:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   240
         TabIndex        =   2
         Top             =   435
         Width           =   420
      End
   End
   Begin VB.Frame fraOpciones 
      Height          =   945
      Left            =   0
      TabIndex        =   4
      Top             =   3720
      Width           =   7860
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   5280
         TabIndex        =   6
         Top             =   360
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6480
         TabIndex        =   5
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "DETALLE DE ACCIONES POR PERFIL"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   9.75
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   3840
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptAccionesPorPerfil.frx":0000
      Top             =   0
      Width           =   8700
   End
End
Attribute VB_Name = "rptAccionesPorPerfil"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Call InicializarCombos
End Sub

Private Sub InicializarCombos()
    With cboPerfil
        .Clear
        If sp_GetPerfil(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_perfil) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_perfil)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
    
    With cboAgrupador
        .Clear
        If sp_GetAccionesGrupo(Null, Null, "S") Then
            
        End If
    End With
End Sub

Private Sub cmdOk_Click()
    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        'Call ViejoReporte
        Call Status("Falta el reporte: /rptAccionesPerfil.rpt")
    End If
End Sub

Private Sub NuevoReporte()
'    Dim crApp As New CRAXDRT.Application
'    Dim crReport As New CRAXDRT.Report
'    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
'    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
'    Dim sReportName As String
'
'    Call Puntero(True)
'    sReportName = "\rptAccionesPerfil.rpt"
'    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
'    With crReport
'        .PaperSize = crPaperA4
'        .PaperOrientation = crPortrait
'        .ReportTitle = "Detalle de acciones por perfil"
''        If OptNivel(1).Value Then
''            .ReportComments = "Solicitudes por L�der entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
''                        IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
''        ElseIf OptNivel(2).Value Then
''            .ReportComments = "Solicitudes por Supervisor entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
''                        IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
''        Else
''            .ReportComments = "Solicitudes entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
''                        IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
''        End If
'    End With
'
'    Call FuncionesCrystal10.ConectarDB(crReport)
'    On Error GoTo ErrHandler
'
'    'Abrir el reporte
'    ' Parametros del reporte
'    Set crParamDefs = crReport.ParameterFields
'    For Each crParamDef In crParamDefs
'        crParamDef.ClearCurrentValueAndRange
'        Select Case crParamDef.ParameterFieldName
'            Case "@cod_recurso": crParamDef.AddCurrentValue (IIf(OptNivel(0).Value <> True, CodigoCombo(cboRecurso, True), "NULL"))
'            Case "@cod_recurso": crParamDef.AddCurrentValue (IIf(OptNivel(0).Value <> True, CodigoCombo(cboRecurso, True), "NULL"))
'        End Select
'    Next
'
'    Set rptVisorRpt.crReport = crReport
'    rptVisorRpt.CargarReporte
'    rptVisorRpt.Show 1
'
'    Call Puntero(False)
'    Set crParamDefs = Nothing
'    Set crParamDef = Nothing
'    Set crApp = Nothing
'    Set crReport = Nothing
'Exit Sub
'ErrHandler:
'    If Err.Number = -2147206461 Then
'        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
'            vbCritical + vbOKOnly
'    Else
'        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
'    End If
'    Call Puntero(False)
End Sub
