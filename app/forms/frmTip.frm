VERSION 5.00
Begin VB.Form frmTip 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tip del d�a"
   ClientHeight    =   3630
   ClientLeft      =   2355
   ClientTop       =   2385
   ClientWidth     =   6540
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmTip.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   6540
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton cmdNextTip 
      Caption         =   "&Siguiente TIP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5280
      TabIndex        =   2
      Top             =   720
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "Cerrar"
      Default         =   -1  'True
      Height          =   375
      Left            =   5280
      TabIndex        =   0
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CheckBox chkLoadTipsAtStartup 
      Caption         =   "&Mostrar sugerencias al iniciar la aplicaci�n"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   3240
      Width           =   3315
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3075
      Left            =   120
      Picture         =   "frmTip.frx":014A
      ScaleHeight     =   3015
      ScaleWidth      =   4995
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   120
      Width           =   5055
      Begin VB.TextBox Txtmensaje 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H00C00000&
         Height          =   2505
         Left            =   510
         MultiLine       =   -1  'True
         TabIndex        =   5
         Top             =   480
         Width           =   4455
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Sab�as que..."
         Height          =   255
         Left            =   540
         TabIndex        =   4
         Top             =   180
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmTip"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'GMT01 SE RECUPERAN LOS TIPS, SIEMPRE Y CUANDO ESTEN HABILIDATOS
Option Explicit

' The in-memory database of tips.
Dim Tips() As oTip
Dim bFirstTime As Boolean

' Contador de los tips existentes en la base
Public iTipsCount As Integer
' Index in collection of tip currently being displayed.
Dim CurrentTip As Long
Dim Hab_Tips As Integer 'GMT01
Dim Hab_TipsFecha As String  'GMT01

Private Sub Form_Load()
    'Dim ShowAtStartup As String
    
    ' See if we should be shown at startup
    'ShowAtStartup = GetSetting("GesPet", "Preferencias\General", "IniciarTips", "N")
    'If ShowAtStartup = "N" Then
    '    Unload Me
    '    Exit Sub
    'End If
        
    ' Set the checkbox, this will force the value to be written back out to the registry
    'Me.chkLoadTipsAtStartup.Value = vbChecked
    Me.chkLoadTipsAtStartup.value = IIf(GetSetting("GesPet", "Preferencias\General", "IniciarTips", "N") = "S", 1, 0)
    
    bFirstTime = True
    
    ' Seed Rnd
    Randomize
    
    ' Read in the tips file and display a tip at random.
    
    Call LoadTips
End Sub

Private Sub LoadTips()
    Dim NextTip As String   ' Each tip read in from file.
    If InicializarTips Then
    'InicializarTips
    ' Display a tip at random.
        DoNextTip
    End If
End Sub

Private Sub DoNextTip()
    If bFirstTime Then
        ' Select a tip at random.
        CurrentTip = Int((iTipsCount * Rnd) + 1)
        bFirstTime = False
    Else
        ' Or, you could cycle through the Tips in order
        CurrentTip = CurrentTip + 1
        If iTipsCount < CurrentTip Then
            CurrentTip = 1
        End If
    End If
    ' Show it.
    frmTip.DisplayCurrentTip
End Sub

Private Sub chkLoadTipsAtStartup_Click()
    ' Save whether or not this form should be displayed at startup
    SaveSetting "GesPet", "Preferencias\General", "IniciarTips", IIf(chkLoadTipsAtStartup.value = 1, "S", "N")
End Sub

Private Sub cmdNextTip_Click()
    DoNextTip
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub

Public Sub DisplayCurrentTip()
    If iTipsCount > 0 Then
        Txtmensaje.text = Tips(CurrentTip).Texto
    End If
End Sub

Private Function InicializarTips() As Boolean
    Dim I As Integer
    Dim cVersion As String
    Dim fechaHoyChar As String
    Dim fechaHoy As Integer
    
    InicializarTips = True
    
    cVersion = "v" & App.Major & "." & App.Minor & "." & App.Revision
    
    ' GMT01 - INI
    'If sp_GetTips(cVersion, "TIP", Null, Null) Then
    'Se dejan mensajes especiales a determinados grupos
    Call recupHabilitaciones
    
    fechaHoyChar = Format$(date, "yyyymmdd")

    If Hab_Tips = 3 Then
        If glLOGIN_Grupo = "11-21" Or glLOGIN_Grupo = "24-03" Then
        ' a partir de que fecha se muestran los mensajes
           If fechaHoyChar > Hab_TipsFecha Then
             If sp_GetTips(Null, "BROMA", Null, Null, "S") Then
                Do While Not aplRST.EOF
                    I = I + 1
                    ReDim Preserve Tips(I)
                    Tips(I).Version = aplRST.Fields!tips_version
                    Tips(I).Nro = aplRST.Fields!tips_nro
                    Tips(I).Rng = aplRST.Fields!tips_rng
                    Tips(I).Texto = ClearNull(aplRST.Fields!tips_txt)
                    Tips(I).fecha = aplRST.Fields!tips_date
                    aplRST.MoveNext
                    DoEvents
                Loop
                aplRST.Close
            Else
                InicializarTips = False
                'Unload Me
            End If
          End If
        Else
         If sp_GetTips(Null, "TIP", Null, Null, "S") Then
           Do While Not aplRST.EOF
              I = I + 1
              ReDim Preserve Tips(I)
              Tips(I).Version = aplRST.Fields!tips_version
              Tips(I).Nro = aplRST.Fields!tips_nro
              Tips(I).Rng = aplRST.Fields!tips_rng
              Tips(I).Texto = ClearNull(aplRST.Fields!tips_txt)
              Tips(I).fecha = aplRST.Fields!tips_date
              aplRST.MoveNext
              DoEvents
              Loop
           aplRST.Close
        Else
           InicializarTips = False
           'Unload Me
        End If
        End If
    Else
        If sp_GetTips(Null, "TIP", Null, Null, "S") Then
           Do While Not aplRST.EOF
              I = I + 1
              ReDim Preserve Tips(I)
              Tips(I).Version = aplRST.Fields!tips_version
              Tips(I).Nro = aplRST.Fields!tips_nro
              Tips(I).Rng = aplRST.Fields!tips_rng
              Tips(I).Texto = ClearNull(aplRST.Fields!tips_txt)
              Tips(I).fecha = aplRST.Fields!tips_date
              aplRST.MoveNext
              DoEvents
              Loop
           aplRST.Close
        Else
           InicializarTips = False
           'Unload Me
        End If
    End If
    
    If I = 1 Then cmdNextTip.Enabled = False
    iTipsCount = I
End Function

'GMT01 - INI
Private Function recupHabilitaciones() As Boolean
    Dim Hab_TipsFechaChar As String
    
    'se recuperan los valores para saber que flujo de trabajo esta habilitado
    recupHabilitaciones = False
    Hab_Tips = 0

    'indica si ver la solapa de vinculados
    If sp_GetVarios("HAB_TIPS") Then
        Hab_Tips = ClearNull(aplRST.Fields!var_numero)
        Hab_TipsFechaChar = Format$(ClearNull(aplRST.Fields!var_fecha), "yyyymmdd")
        Hab_TipsFecha = Hab_TipsFechaChar
    End If
    
    recupHabilitaciones = True
 End Function
'GMT01 - FIN


