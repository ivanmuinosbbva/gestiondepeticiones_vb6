VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptPetiProyIDM1 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3840
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   8715
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3840
   ScaleWidth      =   8715
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2085
      Left            =   0
      TabIndex        =   9
      Top             =   840
      Width           =   8715
      Begin VB.OptionButton optProyecto 
         Caption         =   "De todos los proyectos"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   0
         Top             =   840
         Value           =   -1  'True
         Width           =   2055
      End
      Begin VB.OptionButton optProyecto 
         Caption         =   "De un proyecto espec�fico"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   1200
         Width           =   2295
      End
      Begin VB.CommandButton cmdEraseProyectos 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3960
         Picture         =   "rptPetiProyIDM1.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Quitar el filtro actual de proyecto IDM"
         Top             =   1200
         Width           =   390
      End
      Begin VB.CommandButton cmdPrjFilter 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4320
         Picture         =   "rptPetiProyIDM1.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto IDM"
         Top             =   1200
         Width           =   390
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   315
         Left            =   3600
         TabIndex        =   4
         Top             =   1200
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   315
         Left            =   3240
         TabIndex        =   3
         Top             =   1200
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   315
         Left            =   2640
         TabIndex        =   2
         Top             =   1200
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   556
         MaxLength       =   11
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   11
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjNom 
         Height          =   315
         Left            =   2640
         TabIndex        =   7
         Top             =   1560
         Width           =   5895
         _ExtentX        =   10398
         _ExtentY        =   556
         MaxLength       =   100
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Opciones de generaci�n de informe de peticiones asociadas a proyectos del plan de medios"
         Height          =   180
         Left            =   120
         TabIndex        =   10
         Top             =   405
         Width           =   6870
      End
   End
   Begin VB.Frame fraOpciones 
      Height          =   945
      Left            =   0
      TabIndex        =   12
      Top             =   2880
      Width           =   8715
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   435
         Left            =   7440
         TabIndex        =   13
         Top             =   360
         Width           =   975
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   435
         Left            =   6360
         TabIndex        =   8
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "9. PETICIONES ASOCIADAS A PROYECTOS DEL PLAN DE MEDIOS"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   5025
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptPetiProyIDM1.frx":0B14
      Top             =   0
      Width           =   8700
   End
End
Attribute VB_Name = "rptPetiProyIDM1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Call setHabilCtrl(txtPrjNom, "DIS")
    optProyecto_Click (1)
End Sub

Private Sub cmdOK_Click()
    If ControlPedido Then
        If glCrystalNewVersion Then
            Call NuevoReporte
        Else
            'Call ViejoReporte
        End If
    End If
End Sub

Private Function ControlPedido() As Boolean
    ControlPedido = True
    If optProyecto(0).Value Then
        If ClearNull(txtPrjId) = "" Then
            MsgBox "Debe ingresar o seleccionar un proyecto existente.", vbExclamation + vbOKOnly
            ControlPedido = False
        End If
    End If
End Function

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    
    Call Puntero(True)
    sReportName = "\petproyidm16.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Peticiones relacionadas a Proyectos del Plan de Medios"
        If optProyecto(0).Value Then
            .ReportComments = optProyecto(0).Caption & ": " & txtPrjId & "." & txtPrjSubId & "." & txtPrjSubsId & " - " & ClearNull(txtPrjNom)
        Else
            .ReportComments = optProyecto(1).Caption
        End If
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
 
    'Abrir el reporte
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@projid": crParamDef.AddCurrentValue (Val(txtPrjId.Text))
            Case "@projsubid": crParamDef.AddCurrentValue (Val(txtPrjSubId.Text))
            Case "@projsubsid": crParamDef.AddCurrentValue (Val(txtPrjSubsId.Text))
        End Select
    Next
   
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

'Private Sub ViejoReporte()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String
'    Dim dFecha_Desde As Date
'    Dim dFecha_Hasta As Date
'    Dim iCantidad_Dias As Integer
'
'    Call Status("Preparando el informe...")
'
'    cTitulo = "Peticiones relacionadas a Proyectos del Plan de Medios"
'
'    Screen.MousePointer = vbHourglass
'    With mdiPrincipal.CrystalReport1
'        cPathFileNameReport = App.Path & "\" & "petproyidm1.rpt"
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .SelectionFormula = ""
'        .RetrieveStoredProcParams
'        .StoredProcParam(0) = Val(txtPrjId.Text)
'        .StoredProcParam(1) = Val(txtPrjSubId.Text)
'        .StoredProcParam(2) = Val(txtPrjSubsId.Text)
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & cTitulo & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Screen.MousePointer = vbNormal
'        Call Status("Listo.")
'    End With
'End Sub

Private Sub cmdPrjFilter_Click()
    Load frmProyectosIDMSeleccion
    frmProyectosIDMSeleccion.Show 1
    With frmProyectosIDMSeleccion
        txtPrjId.Text = .lProjId
        txtPrjSubId.Text = .lProjSubId
        txtPrjSubsId.Text = .lProjSubSId
        txtPrjNom.Text = Trim(.cProjNom)
    End With
End Sub

Private Sub cmdEraseProyectos_Click()
    txtPrjId = ""
    txtPrjSubId = ""
    txtPrjSubsId = ""
    txtPrjNom = ""
End Sub

Private Sub optProyecto_Click(Index As Integer)
    Select Case Index
        Case 0
            Call setHabilCtrl(txtPrjId, "NOR")
            Call setHabilCtrl(txtPrjSubId, "NOR")
            Call setHabilCtrl(txtPrjSubsId, "NOR")
            'Call setHabilCtrl(txtPrjNom, "NOR")
            Call setHabilCtrl(cmdPrjFilter, "NOR")
            Call setHabilCtrl(cmdEraseProyectos, "NOR")
        Case 1
            cmdEraseProyectos_Click
            Call setHabilCtrl(txtPrjId, "DIS")
            Call setHabilCtrl(txtPrjSubId, "DIS")
            Call setHabilCtrl(txtPrjSubsId, "DIS")
            'Call setHabilCtrl(txtPrjNom, "DIS")
            Call setHabilCtrl(cmdPrjFilter, "DIS")
            Call setHabilCtrl(cmdEraseProyectos, "DIS")
    End Select
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
