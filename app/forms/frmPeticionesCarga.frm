VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPeticionesCarga 
   Caption         =   " Nueva petici�n"
   ClientHeight    =   8715
   ClientLeft      =   5010
   ClientTop       =   2295
   ClientWidth     =   14685
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesCarga.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   8715
   ScaleWidth      =   14685
   Tag             =   "Datos Petici�n: N�"
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   0
      Left            =   60
      TabIndex        =   84
      Top             =   660
      Width           =   1275
   End
   Begin VB.CommandButton cmdAyudaCarga 
      Appearance      =   0  'Flat
      Caption         =   "Ayuda"
      Height          =   525
      Index           =   8
      Left            =   12360
      Picture         =   "frmPeticionesCarga.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   251
      Top             =   120
      Visible         =   0   'False
      Width           =   855
   End
   Begin MSComctlLib.StatusBar sbMainPeticion 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   235
      Top             =   8415
      Width           =   14685
      _ExtentX        =   25903
      _ExtentY        =   529
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   7
      Left            =   9240
      TabIndex        =   142
      Top             =   675
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   6
      Left            =   7920
      TabIndex        =   130
      Top             =   675
      Width           =   1335
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F7 - Homolog."
      Height          =   525
      Index           =   6
      Left            =   7980
      Style           =   1  'Graphical
      TabIndex        =   61
      Top             =   120
      Width           =   1275
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   5
      Left            =   6600
      TabIndex        =   98
      Top             =   675
      Width           =   1350
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F8 - Control"
      Height          =   525
      Index           =   7
      Left            =   9300
      Style           =   1  'Graphical
      TabIndex        =   62
      Top             =   120
      Width           =   1275
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   4
      Left            =   5280
      TabIndex        =   89
      Top             =   675
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   3
      Left            =   3960
      TabIndex        =   83
      Top             =   675
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   2
      Left            =   2640
      TabIndex        =   86
      Top             =   675
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   1
      Left            =   1320
      TabIndex        =   85
      Top             =   675
      Width           =   1350
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F6 - Conformes"
      Height          =   525
      Index           =   5
      Left            =   6660
      Style           =   1  'Graphical
      TabIndex        =   60
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F5 - Adjuntos"
      Height          =   525
      Index           =   4
      Left            =   5340
      Style           =   1  'Graphical
      TabIndex        =   59
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F4 - Vinculados"
      Height          =   525
      Index           =   3
      Left            =   4020
      Style           =   1  'Graphical
      TabIndex        =   58
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F3 - Validaci�n t�cnica"
      Height          =   525
      Index           =   2
      Left            =   2700
      Style           =   1  'Graphical
      TabIndex        =   57
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton orjBtn 
      Caption         =   "F2 - Detalle"
      Height          =   525
      Index           =   1
      Left            =   1380
      MaskColor       =   &H00E0E0E0&
      Style           =   1  'Graphical
      TabIndex        =   56
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton orjBtn 
      Caption         =   "F1 - Datos"
      Height          =   525
      Index           =   0
      Left            =   60
      Picture         =   "frmPeticionesCarga.frx":0B14
      TabIndex        =   55
      Top             =   120
      Width           =   1275
   End
   Begin VB.Frame fraButtPpal 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8415
      Left            =   13280
      TabIndex        =   38
      Top             =   0
      Width           =   1365
      Begin VB.CommandButton cmdAsigNro 
         Caption         =   "Asig. N�mero"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   138
         TabStop         =   0   'False
         Top             =   4680
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CommandButton cmdRecursos 
         Caption         =   "Recursos"
         Height          =   450
         Left            =   40
         Picture         =   "frmPeticionesCarga.frx":109E
         TabIndex        =   114
         TabStop         =   0   'False
         Top             =   2160
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CommandButton cmdGrupos 
         Caption         =   "Grupos"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   75
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   1680
         Width           =   1245
      End
      Begin VB.CommandButton cmdRptHs 
         Caption         =   "Horas trabajadas"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   74
         TabStop         =   0   'False
         ToolTipText     =   "Invoca el reporte de Horas rabajadas"
         Top             =   4200
         Width           =   1245
      End
      Begin VB.CommandButton cmdAgrupar 
         Caption         =   "Agrupamientos"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   69
         TabStop         =   0   'False
         ToolTipText     =   "Visualiza los agrupamientos que referencian esta Petici�n"
         Top             =   600
         Width           =   1245
      End
      Begin VB.CommandButton cmdSector 
         Caption         =   "Sectores"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   45
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   1200
         Width           =   1245
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   44
         TabStop         =   0   'False
         ToolTipText     =   "Permite completar los datos de la Petici�n"
         Top             =   5310
         Width           =   1245
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Imprimir"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   43
         TabStop         =   0   'False
         ToolTipText     =   "Imprime un formulario con los datos de la petici�n"
         Top             =   6210
         Width           =   1245
      End
      Begin VB.CommandButton cmdCambioEstado 
         Caption         =   "Cambio de estado"
         Height          =   450
         Left            =   40
         TabIndex        =   42
         TabStop         =   0   'False
         ToolTipText     =   "Permite cambiar el estado de la Petici�n"
         Top             =   3000
         Width           =   1245
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Guardar"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   41
         TabStop         =   0   'False
         ToolTipText     =   "Confirma y guarda los cambios realizados"
         Top             =   5760
         Width           =   1245
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   40
         TabStop         =   0   'False
         ToolTipText     =   "Terminar la operaci�n"
         Top             =   7875
         Width           =   1245
      End
      Begin VB.CommandButton cmdHistView 
         Caption         =   "Historial"
         Enabled         =   0   'False
         Height          =   450
         Left            =   40
         TabIndex        =   39
         TabStop         =   0   'False
         ToolTipText     =   "Muestra el historial de la Petici�n"
         Top             =   3720
         Width           =   1245
      End
      Begin VB.Label lblModo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   60
         TabIndex        =   116
         Top             =   330
         Width           =   1200
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "Modo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   60
         TabIndex        =   115
         Top             =   120
         Width           =   1155
      End
   End
   Begin MSComctlLib.ImageList imgPeticiones 
      Left            =   10620
      Top             =   60
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   42
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":11E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":1782
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":1D1C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":22B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":2850
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":2DEA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":3384
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":391E
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":3EB8
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":4452
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":49EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":4F86
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":5520
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":5ABA
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":6054
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":65EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":6B88
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":7122
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":76BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":7C56
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":7FF0
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":858A
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":8B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":90BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":9658
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":9BF2
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":A18C
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":A726
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":ACC0
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":B25A
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":B5F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":B98E
            Key             =   ""
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":BF28
            Key             =   ""
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":C4C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":CA5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":CFF6
            Key             =   ""
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":D590
            Key             =   ""
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":DB2A
            Key             =   ""
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":E0C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":E65E
            Key             =   ""
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":EBF8
            Key             =   ""
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPeticionesCarga.frx":F192
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   6
      Left            =   60
      TabIndex        =   141
      Top             =   600
      Width           =   13185
      Begin VB.ComboBox cboHomoRecursoHomologador 
         ForeColor       =   &H00000000&
         Height          =   315
         ItemData        =   "frmPeticionesCarga.frx":F72C
         Left            =   5220
         List            =   "frmPeticionesCarga.frx":F72E
         Style           =   2  'Dropdown List
         TabIndex        =   159
         Top             =   1560
         Width           =   2835
      End
      Begin VB.ComboBox cboHomoProceso 
         Height          =   315
         ItemData        =   "frmPeticionesCarga.frx":F730
         Left            =   1440
         List            =   "frmPeticionesCarga.frx":F732
         Style           =   2  'Dropdown List
         TabIndex        =   148
         Top             =   1200
         Width           =   1815
      End
      Begin VB.ComboBox cboPuntoControl 
         Height          =   315
         ItemData        =   "frmPeticionesCarga.frx":F734
         Left            =   1440
         List            =   "frmPeticionesCarga.frx":F736
         Style           =   2  'Dropdown List
         TabIndex        =   147
         Top             =   1560
         Width           =   1815
      End
      Begin VB.ComboBox cboInforme 
         Height          =   315
         ItemData        =   "frmPeticionesCarga.frx":F738
         Left            =   1440
         List            =   "frmPeticionesCarga.frx":F73A
         Style           =   2  'Dropdown List
         TabIndex        =   146
         Top             =   840
         Width           =   5355
      End
      Begin VB.CheckBox chkIncluirItemsOpcionales 
         Caption         =   "Incluir items opcionales"
         Height          =   255
         Left            =   6900
         TabIndex        =   145
         ToolTipText     =   "Muestra solo los items del informe que son obligatorios de completar."
         Top             =   870
         Width           =   2055
      End
      Begin VB.ComboBox cboHomoResultado 
         Height          =   315
         ItemData        =   "frmPeticionesCarga.frx":F73C
         Left            =   11220
         List            =   "frmPeticionesCarga.frx":F73E
         Style           =   2  'Dropdown List
         TabIndex        =   144
         Top             =   1200
         Width           =   1815
      End
      Begin VB.ComboBox cboHomoEstadoInforme 
         ForeColor       =   &H00000000&
         Height          =   315
         ItemData        =   "frmPeticionesCarga.frx":F740
         Left            =   11220
         List            =   "frmPeticionesCarga.frx":F742
         Style           =   2  'Dropdown List
         TabIndex        =   143
         Top             =   840
         Width           =   1815
      End
      Begin MSComCtl2.DTPicker dtpHomoFechaInforme 
         Height          =   315
         Left            =   5220
         TabIndex        =   149
         Top             =   1200
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   96468993
         CurrentDate     =   42111
      End
      Begin TabDlg.SSTab sstInformeHomo 
         Height          =   5655
         Left            =   120
         TabIndex        =   157
         Top             =   1980
         Width           =   12735
         _ExtentX        =   22463
         _ExtentY        =   9975
         _Version        =   393216
         Style           =   1
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Informe"
         TabPicture(0)   =   "frmPeticionesCarga.frx":F744
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblHomoModo"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "grdHomologacion"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Observaciones"
         TabPicture(1)   =   "frmPeticionesCarga.frx":F760
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtHomoInfoObservaciones"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Detalles y responsables"
         TabPicture(2)   =   "frmPeticionesCarga.frx":F77C
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "grdHomoResponsables"
         Tab(2).Control(1)=   "txtItemRespuesta"
         Tab(2).Control(2)=   "cboItemResponsable"
         Tab(2).Control(3)=   "tbHomologacionResponsables"
         Tab(2).Control(4)=   "lblHomoRespoModo"
         Tab(2).Control(5)=   "lblItemTexto"
         Tab(2).Control(6)=   "lblResponsable"
         Tab(2).Control(7)=   "lblCantidadRestante"
         Tab(2).ControlCount=   8
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdHomoResponsables 
            Height          =   5175
            Left            =   -74880
            TabIndex        =   160
            Top             =   360
            Width           =   12495
            _ExtentX        =   22040
            _ExtentY        =   9128
            _Version        =   393216
            Rows            =   0
            FixedRows       =   0
            FixedCols       =   0
            GridColor       =   -2147483633
            ScrollBars      =   2
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
         Begin VB.TextBox txtHomoInfoObservaciones 
            Height          =   5175
            Left            =   -74880
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   166
            Top             =   360
            Width           =   12495
         End
         Begin VB.TextBox txtItemRespuesta 
            Height          =   615
            Left            =   -74880
            MaxLength       =   255
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   162
            Top             =   4920
            Width           =   12375
         End
         Begin VB.ComboBox cboItemResponsable 
            ForeColor       =   &H00000000&
            Height          =   315
            ItemData        =   "frmPeticionesCarga.frx":F798
            Left            =   -72480
            List            =   "frmPeticionesCarga.frx":F79A
            Style           =   2  'Dropdown List
            TabIndex        =   161
            Top             =   4200
            Width           =   3015
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdHomologacion 
            CausesValidation=   0   'False
            Height          =   5175
            Left            =   120
            TabIndex        =   158
            Top             =   360
            Width           =   12495
            _ExtentX        =   22040
            _ExtentY        =   9128
            _Version        =   393216
            Rows            =   0
            FixedRows       =   0
            FixedCols       =   0
            GridColor       =   -2147483633
            WordWrap        =   -1  'True
            GridLinesFixed  =   1
            _NumberOfBands  =   1
            _Band(0).Cols   =   2
         End
         Begin MSComctlLib.Toolbar tbHomologacionResponsables 
            Height          =   570
            Left            =   -66300
            TabIndex        =   239
            Top             =   4080
            Width           =   3915
            _ExtentX        =   6906
            _ExtentY        =   1005
            ButtonWidth     =   1614
            ButtonHeight    =   1005
            Appearance      =   1
            Style           =   1
            ImageList       =   "imgPeticiones"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Quitar"
                  Key             =   "btnQuitar"
                  Object.ToolTipText     =   "Quitar los responsables seleccionados"
                  ImageIndex      =   31
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Editar"
                  Key             =   "btnEditar"
                  ImageIndex      =   32
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Confirmar"
                  Key             =   "btnConfirmar"
                  ImageIndex      =   12
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Cancelar"
                  Key             =   "btnCancelar"
                  ImageIndex      =   11
               EndProperty
            EndProperty
         End
         Begin VB.Label lblHomoRespoModo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblHomoRespoModo"
            BeginProperty Font 
               Name            =   "Calibri"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C2752E&
            Height          =   195
            Left            =   -63780
            TabIndex        =   168
            Top             =   45
            Width           =   1440
         End
         Begin VB.Label lblHomoModo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblHomoModo"
            BeginProperty Font 
               Name            =   "Calibri"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C2752E&
            Height          =   195
            Left            =   11640
            TabIndex        =   167
            Top             =   45
            Width           =   1020
         End
         Begin VB.Label lblItemTexto 
            Caption         =   "�El campo descripci�n del pedido es claro y describe razonablemente un objetivo?"
            ForeColor       =   &H8000000D&
            Height          =   195
            Left            =   -74880
            TabIndex        =   165
            Top             =   4680
            Width           =   8145
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblResponsable 
            AutoSize        =   -1  'True
            Caption         =   "Responsable de la observaci�n:"
            Height          =   195
            Left            =   -74880
            TabIndex        =   164
            Top             =   4260
            Width           =   2280
         End
         Begin VB.Label lblCantidadRestante 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblCantidadRestante"
            ForeColor       =   &H00808080&
            Height          =   195
            Left            =   -64020
            TabIndex        =   163
            Top             =   4680
            Width           =   1455
         End
      End
      Begin MSComctlLib.Toolbar tbHomologacion 
         Height          =   600
         Left            =   60
         TabIndex        =   237
         Top             =   180
         Width           =   13035
         _ExtentX        =   22992
         _ExtentY        =   1058
         ButtonWidth     =   1614
         ButtonHeight    =   1005
         Appearance      =   1
         Style           =   1
         ImageList       =   "imgPeticiones"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   11
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Confirmar"
               Key             =   "btnConfirmar"
               Object.ToolTipText     =   "Finaliza el informe"
               ImageIndex      =   24
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
               Object.Width           =   1e-4
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Actualizar"
               Key             =   "btnActualizar"
               Object.ToolTipText     =   "Generar un nuevo informe"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Nuevo"
               Key             =   "btnAgregar"
               Object.ToolTipText     =   "Actualizar los datos de la grilla"
               ImageIndex      =   23
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Editar"
               Key             =   "btnEditar"
               Object.ToolTipText     =   "Editar los datos del documento adjunto"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Quitar"
               Key             =   "btnQuitar"
               Object.ToolTipText     =   "Remover de la petici�n el documento seleccionado"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Informe"
               Key             =   "btnInforme"
               ImageIndex      =   29
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Guardar"
               Key             =   "btnGuardar"
               Object.ToolTipText     =   "Guarda el informe actual"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Cancelar"
               Key             =   "btnCancelar"
               Object.ToolTipText     =   "Cancelar la operaci�n"
               ImageIndex      =   11
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
      Begin VB.Label lblHomo 
         AutoSize        =   -1  'True
         Caption         =   "Proceso de homologaci�n:"
         Height          =   390
         Index           =   0
         Left            =   120
         TabIndex        =   156
         Top             =   1140
         Width           =   1260
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHomo 
         AutoSize        =   -1  'True
         Caption         =   "Punto de control:"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   155
         Top             =   1620
         Width           =   1245
      End
      Begin VB.Label lblHomo 
         AutoSize        =   -1  'True
         Caption         =   "Informe:"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   154
         Top             =   900
         Width           =   630
      End
      Begin VB.Label lblHomo 
         AutoSize        =   -1  'True
         Caption         =   "Resultado:"
         Height          =   195
         Index           =   3
         Left            =   10380
         TabIndex        =   153
         Top             =   1260
         Width           =   780
      End
      Begin VB.Label lblHomo 
         AutoSize        =   -1  'True
         Caption         =   "Estado de proceso:"
         Height          =   195
         Index           =   4
         Left            =   9765
         TabIndex        =   152
         Top             =   900
         Width           =   1395
      End
      Begin VB.Label lblHomo 
         AutoSize        =   -1  'True
         Caption         =   "Homologador:"
         Height          =   195
         Index           =   5
         Left            =   4140
         TabIndex        =   151
         Top             =   1620
         Width           =   1005
      End
      Begin VB.Label lblHomo 
         AutoSize        =   -1  'True
         Caption         =   "Fecha informe:"
         Height          =   195
         Index           =   6
         Left            =   4065
         TabIndex        =   150
         Top             =   1260
         Width           =   1080
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   0
      Left            =   60
      TabIndex        =   34
      Top             =   600
      Width           =   13185
      Begin AT_MaskText.MaskText txtNroAsignado 
         Height          =   315
         Left            =   11820
         TabIndex        =   32
         ToolTipText     =   "N�mero asignado por el administrador"
         Top             =   240
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin VB.TextBox txtAgrupamientoSDA 
         Enabled         =   0   'False
         Height          =   315
         Left            =   11640
         TabIndex        =   263
         Top             =   5160
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtSgiIncidencia 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11640
         TabIndex        =   262
         Top             =   4800
         Width           =   1335
      End
      Begin VB.TextBox txtPetPrioridad 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11595
         TabIndex        =   261
         Text            =   "-"
         Top             =   2040
         Width           =   855
      End
      Begin VB.TextBox txtPuntuacion 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11595
         TabIndex        =   221
         Text            =   "-"
         Top             =   1680
         Width           =   855
      End
      Begin VB.TextBox txtValorFacilidad 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11460
         TabIndex        =   219
         Text            =   "-"
         Top             =   3780
         Width           =   735
      End
      Begin VB.TextBox txtValorImpacto 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11460
         TabIndex        =   218
         Text            =   "-"
         Top             =   3420
         Width           =   735
      End
      Begin VB.ComboBox cboGestion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCarga.frx":F79C
         Left            =   11460
         List            =   "frmPeticionesCarga.frx":F7A9
         Style           =   2  'Dropdown List
         TabIndex        =   174
         Top             =   3060
         Width           =   1575
      End
      Begin VB.ComboBox cboReferenteBPE 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   169
         ToolTipText     =   "Recurso que actu� como Referente de Sistema"
         Top             =   6480
         Width           =   6660
      End
      Begin VB.ComboBox cboRO 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7155
         Style           =   2  'Dropdown List
         TabIndex        =   140
         Top             =   960
         Width           =   2040
      End
      Begin VB.ComboBox cboEmp 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCarga.frx":F7B9
         Left            =   11595
         List            =   "frmPeticionesCarga.frx":F7C6
         Style           =   2  'Dropdown List
         TabIndex        =   137
         Top             =   960
         Width           =   1380
      End
      Begin VB.CommandButton cmdEraseProyectos 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8445
         Picture         =   "frmPeticionesCarga.frx":F7D6
         Style           =   1  'Graphical
         TabIndex        =   129
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto para asociar a la petici�n"
         Top             =   1680
         Width           =   350
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   315
         Left            =   12660
         TabIndex        =   9
         Top             =   5940
         Visible         =   0   'False
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         BackColor       =   -2147483648
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   -2147483648
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   315
         Left            =   12300
         TabIndex        =   8
         Top             =   5940
         Visible         =   0   'False
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         BackColor       =   -2147483648
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   -2147483648
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   315
         Left            =   11460
         TabIndex        =   7
         Top             =   5940
         Visible         =   0   'False
         Width           =   840
         _ExtentX        =   1482
         _ExtentY        =   556
         MaxLength       =   11
         BackColor       =   -2147483648
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   -2147483648
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   11
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.CommandButton cmdSelProyectos 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8805
         Picture         =   "frmPeticionesCarga.frx":FD60
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto para asociar a la petici�n"
         Top             =   1680
         Width           =   350
      End
      Begin VB.ComboBox cboSector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   24
         ToolTipText     =   "Sector Solicitante"
         Top             =   4440
         Width           =   11895
      End
      Begin VB.ComboBox cboRegulatorio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCarga.frx":102EA
         Left            =   1080
         List            =   "frmPeticionesCarga.frx":102F7
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   960
         Width           =   1035
      End
      Begin VB.ComboBox cboImpacto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCarga.frx":10307
         Left            =   11820
         List            =   "frmPeticionesCarga.frx":10314
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   600
         Width           =   1155
      End
      Begin VB.ComboBox cboClase 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6075
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   600
         Width           =   3135
      End
      Begin VB.ComboBox cboTipopet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   2
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   600
         Width           =   2460
      End
      Begin VB.ComboBox cboImportancia 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1312
         Width           =   2460
      End
      Begin VB.ComboBox cboBpar 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   30
         ToolTipText     =   "Recurso que actu� como Referente de Sistema"
         Top             =   6840
         Width           =   6660
      End
      Begin VB.ComboBox cboOrientacion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   2040
         Width           =   8115
      End
      Begin VB.ComboBox cboCorpLocal 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6435
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   1320
         Width           =   2775
      End
      Begin VB.CommandButton cmdVerAnexora 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12675
         Picture         =   "frmPeticionesCarga.frx":10324
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Abrir la petici�n de referencia"
         Top             =   2700
         Width           =   350
      End
      Begin VB.ComboBox cboSupervisor 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   28
         ToolTipText     =   "Recurso que actu� como Supervisor"
         Top             =   5520
         Width           =   6660
      End
      Begin VB.ComboBox cboDirector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   29
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   5880
         Width           =   6660
      End
      Begin VB.ComboBox cboSolicitante 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   26
         ToolTipText     =   "Recurso que actu� como Solicitante"
         Top             =   4800
         Width           =   6660
      End
      Begin VB.ComboBox cboReferente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   27
         ToolTipText     =   "Recurso que actu� como Referente"
         Top             =   5160
         Width           =   6660
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   8715
         _ExtentX        =   15372
         _ExtentY        =   556
         MaxLength       =   120
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   120
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtSituacion 
         Height          =   315
         Left            =   1080
         TabIndex        =   17
         ToolTipText     =   "Situaci�n particular"
         Top             =   3060
         Width           =   4995
         _ExtentX        =   8811
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin AT_MaskText.MaskText txtFiniplan 
         Height          =   315
         Left            =   1080
         TabIndex        =   19
         ToolTipText     =   "Fecha de Inicio Planificada"
         Top             =   3420
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFinireal 
         Height          =   315
         Left            =   1080
         TabIndex        =   22
         ToolTipText     =   "Fecha de Inicio Real"
         Top             =   3780
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFfinplan 
         Height          =   315
         Left            =   4020
         TabIndex        =   20
         ToolTipText     =   "Fecha de finalizaci�n Planificada"
         Top             =   3420
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFfinreal 
         Height          =   315
         Left            =   4020
         TabIndex        =   23
         ToolTipText     =   "Fecha de Finalizaci�n Real"
         Top             =   3780
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaComite 
         Height          =   315
         Left            =   11580
         TabIndex        =   31
         ToolTipText     =   "Fecha en que se remiti� al Referente de Sistemas"
         Top             =   6840
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaPedido 
         Height          =   315
         Left            =   8700
         TabIndex        =   25
         ToolTipText     =   "Fecha en que se inici� la Peticion "
         Top             =   3060
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Locked          =   -1  'True
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNroAnexada 
         Height          =   315
         Left            =   11460
         TabIndex        =   15
         ToolTipText     =   "Petici�n a la cual est� anexada"
         Top             =   2700
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   556
         MaxLength       =   30
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         AutoSelect      =   0   'False
      End
      Begin AT_MaskText.MaskText txtHsPlanif 
         Height          =   315
         Left            =   8700
         TabIndex        =   21
         ToolTipText     =   "Cantidad total de planificaciones"
         Top             =   3780
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtPrjNom 
         Height          =   315
         Left            =   1080
         TabIndex        =   11
         Top             =   1680
         Width           =   7335
         _ExtentX        =   12938
         _ExtentY        =   556
         MaxLength       =   50
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         Locked          =   -1  'True
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNrointerno 
         Height          =   315
         Left            =   11820
         TabIndex        =   1
         Top             =   240
         Visible         =   0   'False
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Locked          =   -1  'True
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtFechaRefBPE 
         Height          =   315
         Left            =   11580
         TabIndex        =   172
         ToolTipText     =   "Fecha en que se remiti� al Referente de RGP"
         Top             =   6480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaEstado 
         Height          =   315
         Left            =   8700
         TabIndex        =   215
         ToolTipText     =   "Fecha en que se inici� la Peticion "
         Top             =   2700
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Locked          =   -1  'True
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtEstado 
         Height          =   315
         Left            =   1080
         TabIndex        =   14
         ToolTipText     =   "Estado en que se encuentra la Petici�n"
         Top             =   2700
         Width           =   4995
         _ExtentX        =   8811
         _ExtentY        =   556
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin AT_MaskText.MaskText txtCategoria 
         Height          =   315
         Left            =   11595
         TabIndex        =   222
         ToolTipText     =   "N�mero asignado por el administrador"
         Top             =   1320
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtHspresup 
         Height          =   315
         Left            =   8700
         TabIndex        =   18
         ToolTipText     =   "Esfuerzo presupuestado"
         Top             =   3420
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Incidencia relacionada"
         Height          =   195
         Index           =   4
         Left            =   9900
         TabIndex        =   264
         Top             =   4868
         Width           =   1590
      End
      Begin VB.Image imgFechaAdvertencia 
         Height          =   240
         Index           =   2
         Left            =   5520
         Picture         =   "frmPeticionesCarga.frx":108AE
         Top             =   3810
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Image imgFechaAdvertencia 
         Height          =   240
         Index           =   0
         Left            =   5520
         Picture         =   "frmPeticionesCarga.frx":10E38
         Top             =   3450
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad BPE"
         Height          =   195
         Left            =   10320
         TabIndex        =   260
         Top             =   2100
         Width           =   945
      End
      Begin VB.Label lblPeticionPuntuacion 
         AutoSize        =   -1  'True
         Caption         =   "Puntuaci�n"
         Height          =   195
         Left            =   10305
         TabIndex        =   220
         Top             =   1740
         Width           =   795
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Facilidad"
         Height          =   195
         Index           =   3
         Left            =   10620
         TabIndex        =   189
         Top             =   3840
         Width           =   615
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Impacto"
         Height          =   195
         Index           =   2
         Left            =   10620
         TabIndex        =   188
         Top             =   3480
         Width           =   585
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Origen"
         Height          =   195
         Index           =   1
         Left            =   10620
         TabIndex        =   173
         Top             =   3120
         Width           =   480
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00808080&
         X1              =   120
         X2              =   13020
         Y1              =   6345
         Y2              =   6345
      End
      Begin VB.Label lblPeticionBloque3 
         AutoSize        =   -1  'True
         Caption         =   "Fecha recibido por Ref. RGyP"
         Height          =   195
         Index           =   1
         Left            =   9120
         TabIndex        =   171
         Top             =   6540
         Width           =   2115
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Ref. RGyP"
         Height          =   195
         Left            =   90
         TabIndex        =   170
         Top             =   6555
         Width           =   750
      End
      Begin VB.Label lblRO 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "Vinculada a RO / Criticidad"
         Height          =   195
         Left            =   4980
         TabIndex        =   139
         Top             =   1035
         Width           =   1905
      End
      Begin VB.Label Label54 
         AutoSize        =   -1  'True
         Caption         =   "Empresa"
         Height          =   195
         Left            =   10305
         TabIndex        =   136
         Top             =   1028
         Width           =   615
      End
      Begin VB.Image imgPeticionPlanificada 
         Height          =   240
         Left            =   12420
         Picture         =   "frmPeticionesCarga.frx":113C2
         Top             =   7455
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label lblPeticionNumero 
         AutoSize        =   -1  'True
         Caption         =   "N� Asignado"
         Height          =   195
         Left            =   10305
         TabIndex        =   128
         Top             =   300
         Width           =   885
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Sector solicitante"
         Height          =   390
         Left            =   90
         TabIndex        =   126
         Top             =   4395
         Width           =   990
         WordWrap        =   -1  'True
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00808080&
         X1              =   120
         X2              =   13020
         Y1              =   4275
         Y2              =   4275
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         X1              =   120
         X2              =   13020
         Y1              =   2520
         Y2              =   2520
      End
      Begin VB.Label Label49 
         AutoSize        =   -1  'True
         Caption         =   "Proyecto IDM"
         Height          =   390
         Left            =   120
         TabIndex        =   125
         Top             =   1665
         Width           =   810
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label48 
         AutoSize        =   -1  'True
         Caption         =   "Regulatorio"
         Height          =   195
         Left            =   120
         TabIndex        =   124
         Top             =   1035
         Width           =   825
      End
      Begin VB.Label Label44 
         AutoSize        =   -1  'True
         Caption         =   "Cantidad de replanificaciones"
         Height          =   195
         Left            =   6495
         TabIndex        =   117
         ToolTipText     =   "Cantidad total de planificaciones"
         Top             =   3840
         Width           =   2100
      End
      Begin VB.Image imgTrazabilidad 
         Height          =   240
         Left            =   12780
         Picture         =   "frmPeticionesCarga.frx":1194C
         Top             =   7455
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label lblPetEquipo 
         AutoSize        =   -1  'True
         Caption         =   "Impacto tecnol�gico"
         Height          =   210
         Left            =   10305
         TabIndex        =   108
         Top             =   660
         Width           =   1500
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblPetClass 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         Height          =   195
         Left            =   5490
         TabIndex        =   107
         Top             =   668
         Width           =   390
      End
      Begin VB.Label Label41 
         AutoSize        =   -1  'True
         Caption         =   "Ref. Sistema"
         Height          =   195
         Left            =   90
         TabIndex        =   106
         Top             =   6915
         Width           =   915
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         Caption         =   "Orientaci�n"
         Height          =   195
         Left            =   120
         TabIndex        =   73
         Top             =   2115
         Width           =   825
      End
      Begin VB.Label Label33 
         AutoSize        =   -1  'True
         Caption         =   "Corporativo / Local"
         Height          =   195
         Left            =   4980
         TabIndex        =   72
         Top             =   1395
         Width           =   1365
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad"
         Height          =   195
         Left            =   120
         TabIndex        =   71
         Top             =   1380
         Width           =   675
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "F. Fin Planif."
         Height          =   195
         Left            =   3060
         TabIndex        =   70
         Top             =   3480
         Width           =   900
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Anexada a"
         Height          =   195
         Index           =   0
         Left            =   10620
         TabIndex        =   67
         Top             =   2760
         Width           =   780
      End
      Begin VB.Label lblPeticionCategoria 
         AutoSize        =   -1  'True
         Caption         =   "Categor�a"
         Height          =   195
         Left            =   10305
         TabIndex        =   66
         Top             =   1380
         Width           =   705
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de alta de la petici�n"
         Height          =   195
         Left            =   6630
         TabIndex        =   65
         Top             =   3120
         Width           =   1965
      End
      Begin VB.Label lblPeticionBloque3 
         AutoSize        =   -1  'True
         Caption         =   "Fecha recibido por Ref. Sistema"
         Height          =   195
         Index           =   0
         Left            =   9120
         TabIndex        =   64
         Top             =   6900
         Width           =   2280
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         Height          =   195
         Left            =   120
         TabIndex        =   63
         Top             =   675
         Width           =   300
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "F. Ini. Planif."
         Height          =   195
         Left            =   120
         TabIndex        =   54
         Top             =   3480
         Width           =   930
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "F. Fin Real"
         Height          =   195
         Left            =   3060
         TabIndex        =   53
         Top             =   3840
         Width           =   765
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "F. Ini. Real"
         Height          =   195
         Left            =   120
         TabIndex        =   52
         Top             =   3840
         Width           =   795
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Total de hs. presupuestadas"
         Height          =   195
         Left            =   6540
         TabIndex        =   51
         ToolTipText     =   "Esfuerzo presupuestado"
         Top             =   3480
         Width           =   2055
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Solicitante"
         Height          =   195
         Left            =   90
         TabIndex        =   50
         Top             =   4875
         Width           =   735
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Supervisor"
         Height          =   195
         Left            =   90
         TabIndex        =   49
         Top             =   5595
         Width           =   765
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Referente"
         Height          =   195
         Left            =   90
         TabIndex        =   48
         Top             =   5235
         Width           =   735
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Autorizante"
         Height          =   195
         Left            =   90
         TabIndex        =   47
         Top             =   5955
         Width           =   840
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n"
         Height          =   195
         Left            =   120
         TabIndex        =   46
         Top             =   3120
         Width           =   645
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de estado actual"
         Height          =   195
         Left            =   6915
         TabIndex        =   37
         Top             =   2760
         Width           =   1680
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo"
         Height          =   195
         Left            =   120
         TabIndex        =   36
         Top             =   300
         Width           =   390
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         Height          =   195
         Left            =   120
         TabIndex        =   35
         Top             =   2760
         Width           =   495
      End
      Begin VB.Label lblCustomToolTip 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H00404040&
         Height          =   405
         Left            =   120
         TabIndex        =   127
         Top             =   7320
         Width           =   12195
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   1
      Left            =   60
      TabIndex        =   33
      Top             =   600
      Width           =   13185
      Begin TabDlg.SSTab sstDetalles 
         Height          =   7485
         Left            =   180
         TabIndex        =   175
         Top             =   180
         Width           =   12885
         _ExtentX        =   22728
         _ExtentY        =   13203
         _Version        =   393216
         TabOrientation  =   2
         Tab             =   1
         TabHeight       =   520
         WordWrap        =   0   'False
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "KPI"
         TabPicture(0)   =   "frmPeticionesCarga.frx":11C8E
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "udKPIValorEsperado"
         Tab(0).Control(1)=   "txtKPITiempoParaEsperado"
         Tab(0).Control(2)=   "txtKPIValorPartida"
         Tab(0).Control(3)=   "txtKPIValorEsperado"
         Tab(0).Control(4)=   "cboKPIUniMedTiempo"
         Tab(0).Control(5)=   "txtKPIDescripcion"
         Tab(0).Control(6)=   "cboKPIIndicador"
         Tab(0).Control(7)=   "grdKPI"
         Tab(0).Control(8)=   "cboKPIUniMed"
         Tab(0).Control(9)=   "tbKKPIs"
         Tab(0).Control(10)=   "udKPIValorPartida"
         Tab(0).Control(11)=   "udKPITiempoParaEsperado"
         Tab(0).Control(12)=   "txtKPI(8)"
         Tab(0).Control(13)=   "lblKPIModo"
         Tab(0).Control(14)=   "Line6"
         Tab(0).Control(15)=   "txtKPI(7)"
         Tab(0).Control(16)=   "txtKPIAyuda(6)"
         Tab(0).Control(17)=   "txtKPIAyuda(5)"
         Tab(0).Control(18)=   "txtKPIAyuda(4)"
         Tab(0).Control(19)=   "txtKPIAyuda(3)"
         Tab(0).Control(20)=   "txtKPIAyuda(2)"
         Tab(0).Control(21)=   "txtKPIAyuda(1)"
         Tab(0).Control(22)=   "txtKPI(6)"
         Tab(0).Control(23)=   "txtKPI(5)"
         Tab(0).Control(24)=   "txtKPI(4)"
         Tab(0).Control(25)=   "txtKPI(3)"
         Tab(0).Control(26)=   "txtKPI(2)"
         Tab(0).Control(27)=   "txtKPI(1)"
         Tab(0).Control(28)=   "txtKPIAyuda(0)"
         Tab(0).Control(29)=   "txtKPI(0)"
         Tab(0).ControlCount=   30
         TabCaption(1)   =   "Indicadores"
         TabPicture(1)   =   "frmPeticionesCarga.frx":11CAA
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "lblBeneficios"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "imgCursor(0)"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "lblEmpresa"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "grdBeneficios"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).ControlCount=   4
         TabCaption(2)   =   "Detalles"
         TabPicture(2)   =   "frmPeticionesCarga.frx":11CC6
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "lblTextoDetalle(0)"
         Tab(2).Control(1)=   "lblTextoDetalle(1)"
         Tab(2).Control(2)=   "lblTextoDetalle(2)"
         Tab(2).Control(3)=   "lblTextoDetalle(3)"
         Tab(2).Control(4)=   "lblTextoDetalle(4)"
         Tab(2).Control(5)=   "lblFontSize"
         Tab(2).Control(6)=   "imgFontSize"
         Tab(2).Control(7)=   "Line3(1)"
         Tab(2).Control(8)=   "Line3(0)"
         Tab(2).Control(9)=   "Line3(2)"
         Tab(2).Control(10)=   "Line3(3)"
         Tab(2).Control(11)=   "Line3(4)"
         Tab(2).Control(12)=   "memCaracteristicas"
         Tab(2).Control(13)=   "memRelaciones"
         Tab(2).Control(14)=   "memObservaciones"
         Tab(2).Control(15)=   "memMotivos"
         Tab(2).Control(16)=   "memDescripcion"
         Tab(2).Control(17)=   "chkCargarBeneficios"
         Tab(2).Control(18)=   "chkPeticionDisclaimer"
         Tab(2).Control(19)=   "chkTextoAmpliar(0)"
         Tab(2).Control(20)=   "chkTextoAmpliar(1)"
         Tab(2).Control(21)=   "chkTextoAmpliar(2)"
         Tab(2).Control(22)=   "chkTextoAmpliar(3)"
         Tab(2).Control(23)=   "chkTextoAmpliar(4)"
         Tab(2).Control(24)=   "txtFontSize"
         Tab(2).Control(25)=   "updFontSize"
         Tab(2).ControlCount=   26
         Begin MSComCtl2.UpDown updFontSize 
            Height          =   315
            Left            =   -62850
            TabIndex        =   259
            Top             =   6780
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   556
            _Version        =   393216
            Value           =   7
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtFontSize"
            BuddyDispid     =   196711
            OrigLeft        =   4380
            OrigTop         =   120
            OrigRight       =   4635
            OrigBottom      =   435
            Max             =   24
            Min             =   7
            SyncBuddy       =   -1  'True
            Wrap            =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtFontSize 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   -63240
            Locked          =   -1  'True
            TabIndex        =   258
            Text            =   "8"
            Top             =   6780
            Width           =   390
         End
         Begin VB.CheckBox chkTextoAmpliar 
            Height          =   315
            Index           =   4
            Left            =   -62580
            Picture         =   "frmPeticionesCarga.frx":11CE2
            Style           =   1  'Graphical
            TabIndex        =   256
            Top             =   5460
            Width           =   315
         End
         Begin VB.CheckBox chkTextoAmpliar 
            Height          =   315
            Index           =   3
            Left            =   -62580
            Picture         =   "frmPeticionesCarga.frx":1226C
            Style           =   1  'Graphical
            TabIndex        =   255
            Top             =   4140
            Width           =   315
         End
         Begin VB.CheckBox chkTextoAmpliar 
            Height          =   315
            Index           =   2
            Left            =   -62580
            Picture         =   "frmPeticionesCarga.frx":127F6
            Style           =   1  'Graphical
            TabIndex        =   254
            Top             =   2820
            Width           =   315
         End
         Begin VB.CheckBox chkTextoAmpliar 
            Height          =   315
            Index           =   1
            Left            =   -62580
            Picture         =   "frmPeticionesCarga.frx":12D80
            Style           =   1  'Graphical
            TabIndex        =   253
            Top             =   1500
            Width           =   315
         End
         Begin VB.CheckBox chkTextoAmpliar 
            Height          =   315
            Index           =   0
            Left            =   -62580
            Picture         =   "frmPeticionesCarga.frx":1330A
            Style           =   1  'Graphical
            TabIndex        =   252
            Top             =   180
            Width           =   315
         End
         Begin MSComCtl2.UpDown udKPIValorEsperado 
            Height          =   315
            Left            =   -64860
            TabIndex        =   241
            Top             =   5160
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   556
            _Version        =   393216
            BuddyControl    =   "txtKPIValorEsperado"
            BuddyDispid     =   196716
            OrigLeft        =   10200
            OrigTop         =   5160
            OrigRight       =   10455
            OrigBottom      =   5475
            Max             =   999999
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.CheckBox chkPeticionDisclaimer 
            Caption         =   "Declaro que se encuentra evaluada de forma positiva la factibilidad jur�dica e impositiva del desarrollo solicitado"
            Height          =   195
            Left            =   -74490
            TabIndex        =   216
            Top             =   6840
            Width           =   8295
         End
         Begin VB.TextBox txtKPITiempoParaEsperado 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -66240
            MaxLength       =   12
            TabIndex        =   197
            Top             =   6360
            Width           =   1380
         End
         Begin VB.TextBox txtKPIValorPartida 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -66240
            MaxLength       =   12
            TabIndex        =   196
            Top             =   5760
            Width           =   1380
         End
         Begin VB.TextBox txtKPIValorEsperado 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   -66240
            MaxLength       =   12
            TabIndex        =   195
            Top             =   5160
            Width           =   1380
         End
         Begin VB.ComboBox cboKPIUniMedTiempo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   -66240
            Style           =   2  'Dropdown List
            TabIndex        =   198
            Top             =   6960
            Width           =   1695
         End
         Begin VB.TextBox txtKPIDescripcion 
            Height          =   735
            Left            =   -66240
            MaxLength       =   150
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   193
            Top             =   3720
            Width           =   3375
         End
         Begin VB.ComboBox cboKPIIndicador 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   -66240
            Style           =   2  'Dropdown List
            TabIndex        =   192
            Top             =   3240
            Width           =   3375
         End
         Begin VB.CheckBox chkCargarBeneficios 
            Caption         =   "No quiero cuantificar los beneficios"
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   -74490
            TabIndex        =   187
            Top             =   7080
            Width           =   2835
         End
         Begin VB.TextBox memDescripcion 
            BackColor       =   &H00FFFFFF&
            Height          =   1215
            Left            =   -73050
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   180
            Top             =   180
            Width           =   10485
         End
         Begin VB.TextBox memMotivos 
            Height          =   1215
            Left            =   -73050
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   179
            Top             =   2820
            Width           =   10485
         End
         Begin VB.TextBox memObservaciones 
            Height          =   1215
            Left            =   -73050
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   178
            Top             =   5460
            Width           =   10485
         End
         Begin VB.TextBox memRelaciones 
            Height          =   1215
            Left            =   -73050
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   177
            Top             =   4140
            Width           =   10485
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdBeneficios 
            CausesValidation=   0   'False
            Height          =   6735
            Left            =   480
            TabIndex        =   186
            Top             =   600
            Width           =   12255
            _ExtentX        =   21616
            _ExtentY        =   11880
            _Version        =   393216
            Rows            =   5
            Cols            =   10
            GridColor       =   14737632
            WordWrap        =   -1  'True
            GridLinesFixed  =   1
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "frmPeticionesCarga.frx":13894
            _NumberOfBands  =   1
            _Band(0).Cols   =   10
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdKPI 
            CausesValidation=   0   'False
            Height          =   1995
            Left            =   -74520
            TabIndex        =   191
            Top             =   660
            Width           =   11655
            _ExtentX        =   20558
            _ExtentY        =   3519
            _Version        =   393216
            Rows            =   6
            Cols            =   10
            GridColor       =   14737632
            WordWrap        =   -1  'True
            FocusRect       =   0
            HighLight       =   2
            GridLinesFixed  =   1
            SelectionMode   =   1
            RowSizingMode   =   1
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "frmPeticionesCarga.frx":13E2E
            _NumberOfBands  =   1
            _Band(0).Cols   =   10
         End
         Begin VB.ComboBox cboKPIUniMed 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   -66240
            Style           =   2  'Dropdown List
            TabIndex        =   194
            Top             =   4560
            Width           =   3375
         End
         Begin MSComctlLib.Toolbar tbKKPIs 
            Height          =   600
            Left            =   -74520
            TabIndex        =   240
            Top             =   60
            Width           =   11655
            _ExtentX        =   20558
            _ExtentY        =   1058
            ButtonWidth     =   1614
            ButtonHeight    =   1005
            Appearance      =   1
            Style           =   1
            ImageList       =   "imgPeticiones"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   8
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Actualizar"
                  Key             =   "btnActualizar"
                  Object.ToolTipText     =   "Visualizar el archivo seleccionado"
                  ImageIndex      =   14
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
                  Object.Width           =   1e-4
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Agregar"
                  Key             =   "btnAgregar"
                  Object.ToolTipText     =   "Vincular a la petici�n un archivo"
                  ImageIndex      =   35
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Editar"
                  Key             =   "btnEditar"
                  Object.ToolTipText     =   "Editar los datos del documento adjunto"
                  ImageIndex      =   8
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Quitar"
                  Key             =   "btnQuitar"
                  Object.ToolTipText     =   "Remover de la petici�n el documento seleccionado"
                  ImageIndex      =   9
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Confirmar"
                  Key             =   "btnConfirmar"
                  Object.ToolTipText     =   "Confirmar la operaci�n"
                  ImageIndex      =   12
               EndProperty
               BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "Cancelar"
                  Key             =   "btnCancelar"
                  Object.ToolTipText     =   "Cancelar la operaci�n"
                  ImageIndex      =   11
               EndProperty
            EndProperty
            BorderStyle     =   1
         End
         Begin MSComCtl2.UpDown udKPIValorPartida 
            Height          =   315
            Left            =   -64860
            TabIndex        =   242
            Top             =   5760
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   556
            _Version        =   393216
            BuddyControl    =   "txtKPIValorPartida"
            BuddyDispid     =   196715
            OrigLeft        =   10200
            OrigTop         =   5760
            OrigRight       =   10455
            OrigBottom      =   6075
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin MSComCtl2.UpDown udKPITiempoParaEsperado 
            Height          =   315
            Left            =   -64860
            TabIndex        =   243
            Top             =   6360
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   556
            _Version        =   393216
            BuddyControl    =   "txtKPITiempoParaEsperado"
            BuddyDispid     =   196714
            OrigLeft        =   10200
            OrigTop         =   6360
            OrigRight       =   10455
            OrigBottom      =   6675
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox memCaracteristicas 
            Height          =   1215
            Left            =   -73050
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   176
            Top             =   1500
            Width           =   10485
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00C0C0C0&
            Index           =   4
            X1              =   -62580
            X2              =   -74580
            Y1              =   5400
            Y2              =   5400
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00C0C0C0&
            Index           =   3
            X1              =   -62580
            X2              =   -74580
            Y1              =   4080
            Y2              =   4080
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00C0C0C0&
            Index           =   2
            X1              =   -62580
            X2              =   -74580
            Y1              =   2760
            Y2              =   2760
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00C0C0C0&
            Index           =   0
            X1              =   -62580
            X2              =   -74580
            Y1              =   120
            Y2              =   120
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00C0C0C0&
            Index           =   1
            X1              =   -62580
            X2              =   -74580
            Y1              =   1440
            Y2              =   1440
         End
         Begin VB.Image imgFontSize 
            Height          =   240
            Left            =   -64980
            Picture         =   "frmPeticionesCarga.frx":143C8
            Top             =   6810
            Width           =   240
         End
         Begin VB.Label lblFontSize 
            AutoSize        =   -1  'True
            Caption         =   "Tama�o de fuente:"
            ForeColor       =   &H00404040&
            Height          =   195
            Left            =   -64680
            TabIndex        =   257
            Top             =   6840
            Width           =   1380
         End
         Begin VB.Label lblEmpresa 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblEmpresa"
            ForeColor       =   &H00C2752E&
            Height          =   195
            Left            =   11970
            TabIndex        =   245
            Top             =   120
            Width           =   765
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "lblEmpresa"
            ForeColor       =   &H00C2752E&
            Height          =   195
            Index           =   8
            Left            =   -71880
            TabIndex        =   244
            Top             =   2775
            Width           =   765
         End
         Begin VB.Label lblKPIModo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Indicador"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   195
            Left            =   -63675
            TabIndex        =   214
            Top             =   2760
            Visible         =   0   'False
            Width           =   810
            WordWrap        =   -1  'True
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00808080&
            X1              =   -74520
            X2              =   -62880
            Y1              =   3000
            Y2              =   3000
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Key Performance Indicators"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C2752E&
            Height          =   210
            Index           =   7
            Left            =   -74520
            TabIndex        =   213
            Top             =   2760
            Width           =   2535
         End
         Begin VB.Label txtKPIAyuda 
            Caption         =   "Unidad de medida para el tiempo"
            Height          =   375
            Index           =   6
            Left            =   -72240
            TabIndex        =   212
            Top             =   6960
            Width           =   2895
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPIAyuda 
            Caption         =   "Tiempo que llevar� llegar al indicador a partir de la implementaci�n del desarrollo"
            Height          =   495
            Index           =   5
            Left            =   -72240
            TabIndex        =   211
            Top             =   6360
            Width           =   3255
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPIAyuda 
            Caption         =   "Desempe�o original del indicador"
            Height          =   375
            Index           =   4
            Left            =   -72240
            TabIndex        =   210
            Top             =   5760
            Width           =   2895
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPIAyuda 
            Caption         =   "Desempe�o que se espera obtener"
            Height          =   255
            Index           =   3
            Left            =   -72240
            TabIndex        =   209
            Top             =   5160
            Width           =   2895
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPIAyuda 
            Caption         =   "Unidad de medida del indicador"
            Height          =   255
            Index           =   2
            Left            =   -72240
            TabIndex        =   208
            Top             =   4560
            Width           =   2535
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPIAyuda 
            Caption         =   "Informaci�n adicional sobre el indicador"
            Height          =   495
            Index           =   1
            Left            =   -72240
            TabIndex        =   207
            Top             =   3840
            Width           =   3495
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Unidad de medida"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   -74400
            TabIndex        =   206
            Top             =   6960
            Width           =   1515
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Tiempo para alcanzar el valor esperado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   5
            Left            =   -74400
            TabIndex        =   205
            Top             =   6360
            Width           =   1935
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Valor de partida del indicador"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Index           =   4
            Left            =   -74400
            TabIndex        =   204
            Top             =   5760
            Width           =   1995
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Valor esperado del indicador"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Index           =   3
            Left            =   -74400
            TabIndex        =   203
            Top             =   5160
            Width           =   1920
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Unidad de medida del indicador"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   2
            Left            =   -74400
            TabIndex        =   202
            Top             =   4560
            Width           =   1935
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n del indicador"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   1
            Left            =   -74400
            TabIndex        =   201
            Top             =   3840
            Width           =   1830
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPIAyuda 
            Caption         =   "Completar los indicadores claves del desempe�o para medir el resultado esperado del desarrollo"
            Height          =   495
            Index           =   0
            Left            =   -72240
            TabIndex        =   200
            Top             =   3240
            Width           =   3615
            WordWrap        =   -1  'True
         End
         Begin VB.Label txtKPI 
            AutoSize        =   -1  'True
            Caption         =   "Indicador"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   -74400
            TabIndex        =   199
            Top             =   3240
            Width           =   810
            WordWrap        =   -1  'True
         End
         Begin VB.Image imgCursor 
            Height          =   240
            Index           =   0
            Left            =   8205
            Picture         =   "frmPeticionesCarga.frx":14952
            Top             =   315
            Width           =   240
         End
         Begin VB.Label lblBeneficios 
            AutoSize        =   -1  'True
            Caption         =   $"frmPeticionesCarga.frx":14EDC
            ForeColor       =   &H00000000&
            Height          =   390
            Left            =   480
            TabIndex        =   190
            Top             =   120
            Width           =   8865
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblTextoDetalle 
            Alignment       =   1  'Right Justify
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   4
            Left            =   -74580
            TabIndex        =   185
            Top             =   5460
            Width           =   1320
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblTextoDetalle 
            Alignment       =   1  'Right Justify
            Caption         =   "Relaci�n con otros requerimientos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   660
            Index           =   3
            Left            =   -74565
            TabIndex        =   184
            Top             =   4140
            Width           =   1440
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblTextoDetalle 
            Alignment       =   1  'Right Justify
            Caption         =   "Motivos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   2
            Left            =   -74565
            TabIndex        =   183
            Top             =   2880
            Width           =   1320
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblTextoDetalle 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Caracter�sticas principales"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   1
            Left            =   -74565
            TabIndex        =   182
            Top             =   1500
            Width           =   1320
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblTextoDetalle 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00808080&
            BackStyle       =   0  'Transparent
            Caption         =   "Descripci�n del pedido"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   495
            Index           =   0
            Left            =   -74295
            TabIndex        =   181
            Top             =   180
            Width           =   1050
            WordWrap        =   -1  'True
         End
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   3
      Left            =   60
      TabIndex        =   76
      Top             =   600
      Width           =   13185
      Begin VB.Frame fraDocMet 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3075
         Left            =   60
         TabIndex        =   77
         Top             =   4650
         Width           =   10245
         Begin VB.TextBox docFile 
            Height          =   315
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   88
            Top             =   315
            Width           =   7095
         End
         Begin VB.TextBox docPath 
            Height          =   315
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   87
            Top             =   679
            Width           =   7095
         End
         Begin VB.CommandButton docSel 
            Caption         =   "..."
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   8115
            TabIndex        =   82
            TabStop         =   0   'False
            Top             =   315
            Width           =   330
         End
         Begin VB.TextBox docComent 
            Height          =   1200
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   81
            Top             =   1050
            Width           =   7095
         End
         Begin VB.Label lblMensajeNoVinculados 
            AutoSize        =   -1  'True
            Caption         =   "La petici�n se encuentra en alg�n estado terminal. No es posible modificar documentos vinculados."
            Height          =   195
            Left            =   120
            TabIndex        =   229
            Top             =   2760
            Visible         =   0   'False
            Width           =   7080
         End
         Begin VB.Label Label42 
            AutoSize        =   -1  'True
            Caption         =   "Archivo"
            Height          =   195
            Left            =   120
            TabIndex        =   112
            Top             =   360
            Width           =   540
         End
         Begin VB.Label lblDocMetod 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   120
            TabIndex        =   80
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label Label39 
            AutoSize        =   -1  'True
            Caption         =   "Ubicaci�n"
            Height          =   195
            Left            =   120
            TabIndex        =   79
            Top             =   690
            Width           =   675
         End
         Begin VB.Label Label38 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            Height          =   195
            Left            =   120
            TabIndex        =   78
            Top             =   1050
            Width           =   900
         End
      End
      Begin MSComctlLib.Toolbar tbVinculados 
         Height          =   600
         Left            =   60
         TabIndex        =   227
         Top             =   180
         Width           =   10275
         _ExtentX        =   18124
         _ExtentY        =   1058
         ButtonWidth     =   1614
         ButtonHeight    =   1005
         Appearance      =   1
         Style           =   1
         ImageList       =   "imgPeticiones"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   9
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Ver"
               Key             =   "btnVer"
               Object.ToolTipText     =   "Visualizar el archivo seleccionado"
               ImageIndex      =   10
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
               Object.Width           =   1e-4
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Vincular"
               Key             =   "btnVincular"
               Object.ToolTipText     =   "Vincular a la petici�n un archivo"
               ImageIndex      =   15
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Actualizar"
               Key             =   "btnActualizar"
               Object.ToolTipText     =   "Actualizar los datos de la grilla"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Editar"
               Key             =   "btnEditar"
               Object.ToolTipText     =   "Editar los datos del documento adjunto"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Quitar"
               Key             =   "btnQuitar"
               Object.ToolTipText     =   "Remover de la petici�n el documento seleccionado"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Confirmar"
               Key             =   "btnConfirmar"
               Object.ToolTipText     =   "Confirmar la operaci�n"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Cancelar"
               Key             =   "btnCancelar"
               Object.ToolTipText     =   "Cancelar la operaci�n"
               ImageIndex      =   11
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
      Begin MSComctlLib.ListView lvwVinculados 
         Height          =   3915
         Left            =   60
         TabIndex        =   228
         Top             =   780
         Width           =   10275
         _ExtentX        =   18124
         _ExtentY        =   6906
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "imgPeticiones"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Tipo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Archivo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Subido por"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Sector"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Grupo"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   4
      Left            =   60
      TabIndex        =   90
      Top             =   600
      Width           =   13185
      Begin MSComctlLib.Toolbar tbAdjPet 
         Height          =   600
         Left            =   60
         TabIndex        =   223
         Top             =   180
         Width           =   10275
         _ExtentX        =   18124
         _ExtentY        =   1058
         ButtonWidth     =   1614
         ButtonHeight    =   1005
         Appearance      =   1
         Style           =   1
         ImageList       =   "imgPeticiones"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   9
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Ver"
               Key             =   "btnVer"
               Object.ToolTipText     =   "Visualizar el archivo seleccionado"
               ImageIndex      =   10
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
               Object.Width           =   1e-4
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Adjuntar"
               Key             =   "btnAdjuntar"
               Object.ToolTipText     =   "Adjuntar a la petici�n un archivo"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Actualizar"
               Key             =   "btnActualizar"
               Object.ToolTipText     =   "Actualizar los datos de la grilla"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Editar"
               Key             =   "btnEditar"
               Object.ToolTipText     =   "Editar los datos del documento adjunto"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Quitar"
               Key             =   "btnQuitar"
               Object.ToolTipText     =   "Remover de la petici�n el documento seleccionado"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Confirmar"
               Key             =   "btnConfirmar"
               Object.ToolTipText     =   "Confirmar la operaci�n"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Cancelar"
               Key             =   "btnCancelar"
               Object.ToolTipText     =   "Cancelar la operaci�n"
               ImageIndex      =   11
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
      Begin VB.Frame fraAdjPet 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3660
         Left            =   60
         TabIndex        =   91
         Top             =   4080
         Width           =   10275
         Begin VB.TextBox txtAdjPropietario 
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            TabIndex        =   225
            Top             =   1740
            Width           =   4875
         End
         Begin VB.TextBox txtAdjSize 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            TabIndex        =   122
            Top             =   2100
            Width           =   1035
         End
         Begin VB.TextBox txtAdjGrupo 
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            TabIndex        =   121
            Top             =   1380
            Width           =   4875
         End
         Begin VB.TextBox txtAdjSector 
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            TabIndex        =   120
            Top             =   1020
            Width           =   4875
         End
         Begin VB.TextBox adjFile 
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   92
            Top             =   300
            Width           =   6975
         End
         Begin VB.TextBox adjComent 
            Height          =   660
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   94
            Top             =   2460
            Width           =   6975
         End
         Begin VB.ComboBox cboAdjClass 
            BeginProperty Font 
               Name            =   "Consolas"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            ItemData        =   "frmPeticionesCarga.frx":14FC5
            Left            =   1680
            List            =   "frmPeticionesCarga.frx":14FC7
            Style           =   2  'Dropdown List
            TabIndex        =   93
            Top             =   660
            Width           =   6975
         End
         Begin VB.TextBox adjPath 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   102
            Top             =   2820
            Visible         =   0   'False
            Width           =   6975
         End
         Begin VB.Label Label53 
            AutoSize        =   -1  'True
            Caption         =   "Se sugiere que comprima los documentos antes de adjuntarlos para optimizar el espacio de almacenamiento."
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   120
            TabIndex        =   226
            Top             =   3360
            Width           =   7770
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Propietario"
            Height          =   195
            Left            =   120
            TabIndex        =   224
            Top             =   1800
            Width           =   780
         End
         Begin VB.Label lblPesoEstDoc 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblPesoEstDoc"
            ForeColor       =   &H00808080&
            Height          =   195
            Left            =   7665
            TabIndex        =   135
            Top             =   2160
            Width           =   990
         End
         Begin VB.Label Label47 
            AutoSize        =   -1  'True
            Caption         =   "Tama�o en KB"
            Height          =   195
            Left            =   120
            TabIndex        =   123
            Top             =   2160
            Width           =   1020
         End
         Begin VB.Label Label46 
            AutoSize        =   -1  'True
            Caption         =   "Grupo"
            Height          =   195
            Left            =   120
            TabIndex        =   119
            Top             =   1440
            Width           =   435
         End
         Begin VB.Label Label45 
            AutoSize        =   -1  'True
            Caption         =   "Sector"
            Height          =   195
            Left            =   120
            TabIndex        =   118
            Top             =   1080
            Width           =   465
         End
         Begin VB.Label Label43 
            AutoSize        =   -1  'True
            Caption         =   "Archivo"
            Height          =   195
            Left            =   120
            TabIndex        =   113
            Top             =   360
            Width           =   540
         End
         Begin VB.Label lblAdjPet 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   120
            TabIndex        =   95
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label Label37 
            AutoSize        =   -1  'True
            Caption         =   "Tipo documento"
            Height          =   195
            Left            =   120
            TabIndex        =   96
            Top             =   728
            Width           =   1140
         End
         Begin VB.Label Label35 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            Height          =   195
            Left            =   120
            TabIndex        =   97
            Top             =   2520
            Width           =   900
         End
      End
      Begin MSComctlLib.ListView lvwAdjuntos 
         Height          =   3315
         Left            =   60
         TabIndex        =   217
         Top             =   780
         Width           =   10275
         _ExtentX        =   18124
         _ExtentY        =   5847
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "imgPeticiones"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Tipo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Archivo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Subido por"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Sector"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Grupo"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   7
      Left            =   60
      TabIndex        =   131
      Top             =   600
      Width           =   13185
      Begin VB.ComboBox cmbValidarEstado 
         Height          =   315
         ItemData        =   "frmPeticionesCarga.frx":14FC9
         Left            =   9780
         List            =   "frmPeticionesCarga.frx":14FCB
         Style           =   2  'Dropdown List
         TabIndex        =   134
         Top             =   7200
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.CheckBox chkControl1 
         Caption         =   "Conformes de pruebas de usuario dependen del validaci�n de Homologaci�n"
         Enabled         =   0   'False
         Height          =   255
         Left            =   60
         TabIndex        =   133
         Top             =   7200
         Width           =   5895
      End
      Begin MSFlexGridLib.MSFlexGrid grdControl 
         Height          =   6315
         Left            =   60
         TabIndex        =   132
         Top             =   780
         Width           =   13035
         _ExtentX        =   22992
         _ExtentY        =   11139
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin MSComctlLib.Toolbar tbControl 
         Height          =   600
         Left            =   60
         TabIndex        =   238
         Top             =   180
         Width           =   13035
         _ExtentX        =   22992
         _ExtentY        =   1058
         ButtonWidth     =   1561
         ButtonHeight    =   1005
         Appearance      =   1
         Style           =   1
         ImageList       =   "imgPeticiones"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Actualizar"
               Key             =   "btnActualizar"
               Object.ToolTipText     =   "Actualizar los datos de control de la petici�n"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
               Object.Width           =   1e-4
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   2
      Left            =   60
      TabIndex        =   68
      Top             =   600
      Width           =   13185
      Begin TabDlg.SSTab sstValidacionTecnica 
         Height          =   7515
         Left            =   60
         TabIndex        =   246
         Top             =   180
         Width           =   13035
         _ExtentX        =   22992
         _ExtentY        =   13256
         _Version        =   393216
         TabOrientation  =   2
         Tab             =   2
         TabHeight       =   520
         ForeColor       =   8388608
         TabCaption(0)   =   "Seguridad Inform�tica"
         TabPicture(0)   =   "frmPeticionesCarga.frx":14FCD
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "grdSeguridadInformatica"
         Tab(0).Control(1)=   "imgCursor(2)"
         Tab(0).Control(2)=   "lblValidacionTecnica(1)"
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Arquitectura T�cnica"
         TabPicture(1)   =   "frmPeticionesCarga.frx":14FE9
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdArquitectura"
         Tab(1).Control(1)=   "imgCursor(1)"
         Tab(1).Control(2)=   "lblValidacionTecnica(0)"
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "Data"
         TabPicture(2)   =   "frmPeticionesCarga.frx":15005
         Tab(2).ControlEnabled=   -1  'True
         Tab(2).Control(0)=   "lblValidacionTecnica(2)"
         Tab(2).Control(0).Enabled=   0   'False
         Tab(2).Control(1)=   "imgCursor(3)"
         Tab(2).Control(1).Enabled=   0   'False
         Tab(2).Control(2)=   "Label13"
         Tab(2).Control(2).Enabled=   0   'False
         Tab(2).Control(3)=   "Label19"
         Tab(2).Control(3).Enabled=   0   'False
         Tab(2).Control(4)=   "Label20"
         Tab(2).Control(4).Enabled=   0   'False
         Tab(2).Control(5)=   "Label21"
         Tab(2).Control(5).Enabled=   0   'False
         Tab(2).Control(6)=   "Label22"
         Tab(2).Control(6).Enabled=   0   'False
         Tab(2).Control(7)=   "Label23"
         Tab(2).Control(7).Enabled=   0   'False
         Tab(2).Control(8)=   "Label32"
         Tab(2).Control(8).Enabled=   0   'False
         Tab(2).Control(9)=   "Label50"
         Tab(2).Control(9).Enabled=   0   'False
         Tab(2).Control(10)=   "Label51"
         Tab(2).Control(10).Enabled=   0   'False
         Tab(2).Control(11)=   "Label52"
         Tab(2).Control(11).Enabled=   0   'False
         Tab(2).Control(12)=   "Label55"
         Tab(2).Control(12).Enabled=   0   'False
         Tab(2).Control(13)=   "Label56"
         Tab(2).Control(13).Enabled=   0   'False
         Tab(2).Control(14)=   "Label57"
         Tab(2).Control(14).Enabled=   0   'False
         Tab(2).Control(15)=   "grdData"
         Tab(2).Control(15).Enabled=   0   'False
         Tab(2).ControlCount=   16
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdArquitectura 
            CausesValidation=   0   'False
            Height          =   6795
            Left            =   -74100
            TabIndex        =   247
            Top             =   600
            Width           =   12000
            _ExtentX        =   21167
            _ExtentY        =   11986
            _Version        =   393216
            Rows            =   5
            Cols            =   10
            GridColor       =   14737632
            WordWrap        =   -1  'True
            GridLinesFixed  =   1
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "frmPeticionesCarga.frx":15021
            _NumberOfBands  =   1
            _Band(0).Cols   =   10
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdSeguridadInformatica 
            CausesValidation=   0   'False
            Height          =   6795
            Left            =   -74100
            TabIndex        =   248
            Top             =   600
            Width           =   12000
            _ExtentX        =   21167
            _ExtentY        =   11986
            _Version        =   393216
            Rows            =   5
            Cols            =   10
            GridColor       =   14737632
            WordWrap        =   -1  'True
            GridLinesFixed  =   1
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "frmPeticionesCarga.frx":155BB
            _NumberOfBands  =   1
            _Band(0).Cols   =   10
         End
         Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdData 
            CausesValidation=   0   'False
            Height          =   3360
            Left            =   900
            TabIndex        =   266
            Top             =   600
            Width           =   12000
            _ExtentX        =   21167
            _ExtentY        =   5927
            _Version        =   393216
            Rows            =   5
            Cols            =   10
            GridColor       =   14737632
            WordWrap        =   -1  'True
            GridLinesFixed  =   1
            BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "frmPeticionesCarga.frx":15B55
            _NumberOfBands  =   1
            _Band(0).Cols   =   10
         End
         Begin VB.Label Label57 
            Caption         =   $"frmPeticionesCarga.frx":160EF
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   960
            TabIndex        =   279
            Top             =   6840
            Width           =   10695
         End
         Begin VB.Label Label56 
            Caption         =   "Eliminaci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   900
            TabIndex        =   278
            Top             =   6650
            Width           =   1935
         End
         Begin VB.Label Label55 
            Caption         =   $"frmPeticionesCarga.frx":16181
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   960
            TabIndex        =   277
            Top             =   6450
            Width           =   10695
         End
         Begin VB.Label Label52 
            Caption         =   "Administraci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   900
            TabIndex        =   276
            Top             =   6250
            Width           =   1935
         End
         Begin VB.Label Label51 
            Caption         =   $"frmPeticionesCarga.frx":16219
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   960
            TabIndex        =   275
            Top             =   6050
            Width           =   10695
         End
         Begin VB.Label Label50 
            Caption         =   "Consumo o Uso:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   900
            TabIndex        =   274
            Top             =   5850
            Width           =   1935
         End
         Begin VB.Label Label32 
            Caption         =   "�El proyecto/iniciativa genera informaci�n aplicando procesos de transformaci�n a los datos dentro del sistema?"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   960
            TabIndex        =   273
            Top             =   5650
            Width           =   7215
         End
         Begin VB.Label Label23 
            Caption         =   "Transformaci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   900
            TabIndex        =   272
            Top             =   5450
            Width           =   1935
         End
         Begin VB.Label Label22 
            Caption         =   $"frmPeticionesCarga.frx":162AD
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   960
            TabIndex        =   271
            Top             =   5050
            Width           =   10815
         End
         Begin VB.Label Label21 
            Caption         =   "Aprovisionamiento / Ingesta / Transferencia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   900
            TabIndex        =   270
            Top             =   4850
            Width           =   4095
         End
         Begin VB.Label Label20 
            Caption         =   $"frmPeticionesCarga.frx":1638E
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   960
            TabIndex        =   269
            Top             =   4500
            Width           =   10815
         End
         Begin VB.Label Label19 
            Caption         =   "Creaci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   900
            TabIndex        =   268
            Top             =   4300
            Width           =   1095
         End
         Begin VB.Label Label13 
            Caption         =   "(*) Entendiendo por fases del Ciclo de Vida del dato los siguientes casos:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   900
            TabIndex        =   267
            Top             =   4050
            Width           =   5175
         End
         Begin VB.Image imgCursor 
            Height          =   240
            Index           =   3
            Left            =   6105
            Picture         =   "frmPeticionesCarga.frx":16488
            Top             =   315
            Width           =   240
         End
         Begin VB.Label lblValidacionTecnica 
            AutoSize        =   -1  'True
            Caption         =   $"frmPeticionesCarga.frx":16A12
            ForeColor       =   &H00000000&
            Height          =   390
            Index           =   2
            Left            =   900
            TabIndex        =   265
            Top             =   120
            Width           =   12000
            WordWrap        =   -1  'True
         End
         Begin VB.Image imgCursor 
            Height          =   240
            Index           =   2
            Left            =   -68895
            Picture         =   "frmPeticionesCarga.frx":16B42
            Top             =   315
            Width           =   240
         End
         Begin VB.Label lblValidacionTecnica 
            AutoSize        =   -1  'True
            Caption         =   $"frmPeticionesCarga.frx":170CC
            ForeColor       =   &H00000000&
            Height          =   390
            Index           =   1
            Left            =   -74100
            TabIndex        =   250
            Top             =   120
            Width           =   12000
            WordWrap        =   -1  'True
         End
         Begin VB.Image imgCursor 
            Height          =   240
            Index           =   1
            Left            =   -68895
            Picture         =   "frmPeticionesCarga.frx":171FC
            Top             =   315
            Width           =   240
         End
         Begin VB.Label lblValidacionTecnica 
            AutoSize        =   -1  'True
            Caption         =   $"frmPeticionesCarga.frx":17786
            ForeColor       =   &H00000000&
            Height          =   390
            Index           =   0
            Left            =   -74100
            TabIndex        =   249
            Top             =   120
            Width           =   12000
            WordWrap        =   -1  'True
         End
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Usar este espacio para ER&&CA & Tecnolog�a!"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   26.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   630
         Left            =   120
         TabIndex        =   236
         Top             =   7080
         Visible         =   0   'False
         Width           =   10005
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   5
      Left            =   60
      TabIndex        =   99
      Top             =   600
      Width           =   13185
      Begin VB.Frame fraConforme 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3480
         Left            =   60
         TabIndex        =   100
         Top             =   4260
         Width           =   10275
         Begin VB.ComboBox cboPcfClase 
            Height          =   315
            ItemData        =   "frmPeticionesCarga.frx":178B6
            Left            =   1800
            List            =   "frmPeticionesCarga.frx":178B8
            Style           =   2  'Dropdown List
            TabIndex        =   232
            Top             =   690
            Width           =   3555
         End
         Begin VB.ComboBox cboPcfTipo 
            Height          =   315
            ItemData        =   "frmPeticionesCarga.frx":178BA
            Left            =   1800
            List            =   "frmPeticionesCarga.frx":178BC
            Style           =   2  'Dropdown List
            TabIndex        =   111
            Top             =   300
            Width           =   3555
         End
         Begin VB.TextBox pcfTexto 
            Height          =   1035
            Left            =   1800
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   110
            Top             =   1440
            Width           =   7845
         End
         Begin VB.ComboBox cboPcfModo 
            Height          =   315
            ItemData        =   "frmPeticionesCarga.frx":178BE
            Left            =   1800
            List            =   "frmPeticionesCarga.frx":178C0
            Style           =   2  'Dropdown List
            TabIndex        =   109
            Top             =   1050
            Width           =   3555
         End
         Begin VB.Label lblMensajeNoConformes 
            AutoSize        =   -1  'True
            Caption         =   "La petici�n se encuentra en alguno de los estados terminales, por lo cual no es posible modificar conformes."
            Height          =   195
            Left            =   120
            TabIndex        =   234
            Top             =   3180
            Visible         =   0   'False
            Width           =   7725
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Clase de conforme"
            Height          =   195
            Left            =   120
            TabIndex        =   233
            Top             =   765
            Width           =   1335
         End
         Begin VB.Label Label40 
            AutoSize        =   -1  'True
            Caption         =   "Req."
            Height          =   195
            Left            =   120
            TabIndex        =   105
            Top             =   1118
            Width           =   345
         End
         Begin VB.Label Label36 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            Height          =   195
            Left            =   120
            TabIndex        =   104
            Top             =   1440
            Width           =   900
         End
         Begin VB.Label Label28 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de conforme"
            Height          =   195
            Left            =   120
            TabIndex        =   103
            Top             =   368
            Width           =   1245
         End
         Begin VB.Label lblPetConf 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   120
            TabIndex        =   101
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
      End
      Begin MSComctlLib.Toolbar tbConformes 
         Height          =   600
         Left            =   60
         TabIndex        =   230
         Top             =   180
         Width           =   10275
         _ExtentX        =   18124
         _ExtentY        =   1058
         ButtonWidth     =   1826
         ButtonHeight    =   1005
         Appearance      =   1
         Style           =   1
         ImageList       =   "imgPeticiones"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   10
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Actualizar"
               Key             =   "btnActualizar"
               Object.ToolTipText     =   "Actualizar los datos de la grilla"
               ImageIndex      =   14
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
               Object.Width           =   1e-4
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Conforme"
               Key             =   "btnConforme"
               Object.ToolTipText     =   "Otorgar un conforme a la petici�n"
               ImageIndex      =   17
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Editar"
               Key             =   "btnEditar"
               Object.ToolTipText     =   "Editar los datos del conforme"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Quitar"
               Key             =   "btnQuitar"
               Object.ToolTipText     =   "Remover de la petici�n el conforme seleccionado"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Confirmar"
               Key             =   "btnConfirmar"
               Object.ToolTipText     =   "Confirmar la operaci�n"
               ImageIndex      =   12
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Cancelar"
               Key             =   "btnCancelar"
               Object.ToolTipText     =   "Cancelar la operaci�n"
               ImageIndex      =   11
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Deshabilitar"
               Key             =   "btnDeshabilitar"
               Object.ToolTipText     =   "Deshabilitar manualmente la validez para pasaje a producci�n de esta petici�n"
               ImageIndex      =   18
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
      Begin MSComctlLib.ListView lvwConformes 
         Height          =   3495
         Left            =   60
         TabIndex        =   231
         Top             =   780
         Width           =   10275
         _ExtentX        =   18124
         _ExtentY        =   6165
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "imgPeticiones"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Tipo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Archivo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Subido por"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Sector"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Grupo"
            Object.Width           =   2540
         EndProperty
      End
   End
End
Attribute VB_Name = "frmPeticionesCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'GMT01 - PET 70487 - Se agrega nueva solapa para DATA Y se agrega para que solo se actualice la validacion en la tabla, cuando realmente cambie de valor
'GMT02 - PET 70487 - Se pone agrega logica, para que solo se valide si esta completa la validacion tecnica cuando es referente de sistema y no es supervisor
'GMT03 - PET 72319 - Se agrega conforme al documento nuevo de ADA
'GMT04 - PET 72319 - Se Saca la validacion en peticiones especiales para agregar un grupo
'GMT05 - PET 70881 - Se inabilita beneficios , KPIs Y VINCULADAS
'GMT06 - PET 72319 - Se discrimina conformes cuando el recurso es de arquitectura
'GMT07 - PET AUDITORIA - Se agrega nuevo documento de componentes

Option Explicit

Private Const STATUS_READY = "Listo."

'{ add -109- a. Constantes para las teclas usadas al homologar
Private Const KEY_S = 83            ' SI / SUPERVISOR
Private Const KEY_N = 78            ' NO
Private Const KEY_A = 65            ' N/A
Private Const KEY_P = 80            ' P950
Private Const KEY_C = 67            ' C100
Private Const KEY_D = 68            ' NO
Private Const KEY_R = 82            ' REFERENTE DE SISTEMA
Private Const KEY_L = 76            ' LIDER
Private Const KEY_U = 85            ' USUARIO
Private Const KEY_OMA = 49
Private Const KEY_OME = 50
Private Const KEY_SOB = 51
Private Const KEY_DEL = 46

Private Const TAB_DETALLES = 2
'Private Const TAB_INDICADORES = 1 'GMT05
'Private Const TAB_KPIS = 0        'GMT05

Private Const BOTONERA_VER = 1
Private Const BOTONERA_ADJUNTAR = 3
Private Const BOTONERA_ACTUALIZAR = 4
Private Const BOTONERA_EDITAR = 5
Private Const BOTONERA_QUITAR = 6
Private Const BOTONERA_CONFIRMAR = 8
Private Const BOTONERA_CANCELAR = 9

Private Const BOTONERACONF_ACTUALIZAR = 1
Private Const BOTONERACONF_CONFORME = 3
Private Const BOTONERACONF_EDITAR = 4
Private Const BOTONERACONF_QUITAR = 5
Private Const BOTONERACONF_CONFIRMAR = 7
Private Const BOTONERACONF_CANCELAR = 8
Private Const BOTONERACONF_DESHABILITAR = 10

' Botonera para homologaci�n
Private Const BOTONERAHOMO_CONFIRMAR = 1
Private Const BOTONERAHOMO_ACTUALIZAR = 3
Private Const BOTONERAHOMO_AGREGAR = 4
Private Const BOTONERAHOMO_EDITAR = 5
Private Const BOTONERAHOMO_QUITAR = 6
Private Const BOTONERAHOMO_INFORME = 8
Private Const BOTONERAHOMO_GUARDAR = 10
Private Const BOTONERAHOMO_CANCELAR = 11

' Botonera para homologaci�n (Items para responsables)
Private Const BOTONERAHOMOITEM_QUITAR = 1
Private Const BOTONERAHOMOITEM_EDITAR = 3
Private Const BOTONERAHOMOITEM_GUARDAR = 5
Private Const BOTONERAHOMOITEM_CANCELAR = 6

Private Const BOTONERA_KPI_ACTUALIZAR = 1
Private Const BOTONERA_KPI_AGREGAR = 3
Private Const BOTONERA_KPI_EDITAR = 4
Private Const BOTONERA_KPI_QUITAR = 5
Private Const BOTONERA_KPI_CONFIRMAR = 7
Private Const BOTONERA_KPI_CANCELAR = 8

Private Const TABHOMO_INFORME = 0
Private Const TABHOMO_OBSERVACIONES = 1
Private Const TABHOMO_RESPONSABLES = 2

' Constantes para Arquitectura / Seguridad Inform�tica
Private Const colVALIDACION_ID = 0
Private Const colVALIDACION_ITEM = 1
Private Const colVALIDACION_PREGUNTA = 2
Private Const colVALIDACION_RESPUESTA = 3
Private Const colVALIDACION_RESPUESTAKEY = 4
Private Const colVALIDACION_ANTERIOR = 5
Private Const colVALIDACION_ITEMKEY = 6
Private Const colVALIDACION_TOTALCOLS = 7
Private Const colVALIDACION_ROWHEIGHT = 600
Private Const VALIDACION_ARQUITECTURA = 1
Private Const VALIDACION_SEGURIDADINFORMATICA = 2
Private Const VALIDACION_DATA = 3
Private Const VALIDACION_ARQUITECTURA_TITULO = "Validaci�n t�cnica de Arquitectura"
Private Const VALIDACION_SEGURIDADINFORMATICA_TITULO = "Validaci�n t�cnica de Seguridad Inform�tica"
Private Const VALIDACION_DATA_TITULO = "Validaci�n t�cnica de Data (Validar respuestas pre-cargadas (SDA), en caso contrario modificarlas)"  'GMT01

' Constantes para homologaci�n
Private Const colHOM_PETNROINTERNO = 0
Private Const colHOM_INFOID = 1
Private Const colHOM_INFOIDVER = 2
Private Const colHOM_INFOTIPO = 3
Private Const colHOM_INFOITEM = 4
Private Const colHOM_ITEMDESC = 5
Private Const colHOM_ITEMREQ = 6
Private Const colHOM_ITEMREQDESC = 7
Private Const colHOM_ITEMESTADO = 8
Private Const colHOM_ITEMESTADONOM = 9
Private Const colHOM_ITEMVALOR = 10
Private Const colHOM_ITEMVALORDESC = 11
Private Const colHOM_ITEMSTATUS = 12
Private Const colHOM_ITEMORDEN = 13
Private Const colHOM_ITEMRESPO = 14
Private Const colHOM_ITEMTIPO = 15
Private Const colHOM_TOTALCOLS = 16

'{ add -113- f. Constantes para la grilla de Beneficios
Private Const colBENE_PETNROINTERNO = 0
Private Const colBENE_INDICADORID = 1
Private Const colBENE_INDICADORNOM = 2
Private Const colBENE_DRIVERID = 3
Private Const colBENE_SEPARADOR1 = 4
Private Const colBENE_DRIVERNOM = 5
Private Const colBENE_DRIVERRESP = 6
Private Const colBENE_DRIVERRESPNOM = 7
Private Const colBENE_DRIVERVALOR = 8
Private Const colBENE_DRIVERVALORDESC = 9
Private Const colBENE_DRIVERAYUDA = 10
Private Const colBENE_VALORREFERENCIA = 11
Private Const colBENE_AUDITFECHA = 12
Private Const colBENE_AUDITUSER = 13
Private Const colBENE_REQUERIDO = 14
Private Const colBENE_TOTALCOLS = 15

Dim valoraciones As New Collection
Dim lastColSelected As Integer

Private Const colKPI_PETNROINTERNO = 0
Private Const colKPI_KPIID = 1
Private Const colKPI_KPINOM = 2
Private Const colKPI_REQ = 3
Private Const colKPI_HAB = 4
Private Const colKPI_AYUDA = 5
Private Const colKPI_UMID = 6
Private Const colKPI_UMDSC = 7
Private Const colKPI_VALORESP = 8
Private Const colKPI_VALORPAR = 9
Private Const colKPI_TIEMPO = 10
Private Const colKPI_TIEMPOUM = 11
Private Const colKPI_TIEMPOUMDESC = 12
Private Const colKPI_DESCRIPCION = 13
Private Const colKPI_TOTALCOLS = 14

Const PETICION_TEXTO_DESCRIPCION = "DESCRIPCIO"
Const PETICION_TEXTO_CARACTERISTICAS = "CARACTERIS"
Const PETICION_TEXTO_MOTIVOS = "MOTIVOS"
Const PETICION_TEXTO_RELACIONES = "RELACIONES"
Const PETICION_TEXTO_OBSERVACIONES = "OBSERVACIO"

Private Const PETICION_TIPO_NORMAL = "NOR"
Private Const PETICION_TIPO_PROPIA = "PRO"
Private Const PETICION_TIPO_ESPECIAL = "ESP"
Private Const PETICION_TIPO_PROYECTO = "PRJ"
Private Const PETICION_TIPO_AUDITORIA_INTERNA = "AUI"
Private Const PETICION_TIPO_AUDITORIA_EXTERNA = "AUX"

Private Const PETICION_TEXTO_TIPONOR = "Normal"
Private Const PETICION_TEXTO_TIPOPRO = "Propia"
Private Const PETICION_TEXTO_TIPOESP = "Especial"
Private Const PETICION_TEXTO_TIPOPRJ = "Proyecto"
Private Const PETICION_TEXTO_TIPOAUI = "Auditor�a Interna"
Private Const PETICION_TEXTO_TIPOAUX = "Auditor�a Externa"

Private Const PETICION_CLASE_SINCLASE = "SINC"
Private Const PETICION_CLASE_NUEVO_DESARROLLO = "NUEV"
Private Const PETICION_CLASE_EVOLUTIVO = "EVOL"
Private Const PETICION_CLASE_OPTIMIZACION = "OPTI"
Private Const PETICION_CLASE_CORRECTIVO = "CORR"
Private Const PETICION_CLASE_ATENCION_USUARIO = "ATEN"
Private Const PETICION_CLASE_SPUFI = "SPUF"

Private Const PETICION_TEXTO_CLASESINC = "--"
Private Const PETICION_TEXTO_CLASENUEV = "Nuevo desarrollo"
Private Const PETICION_TEXTO_CLASEEVOL = "Mantenimiento evolutivo"
Private Const PETICION_TEXTO_CLASEOPTI = "Optimizaci�n"
Private Const PETICION_TEXTO_CLASECORR = "Correctivo"
Private Const PETICION_TEXTO_CLASEATEN = "Atenci�n a usuario"
Private Const PETICION_TEXTO_CLASESPUF = "SPUFI"

Private Const HELP_CLASE_NUEV = "Petici�n de cambio de funcionalidad que se resuelve por la instalaci�n del nuevo aplicativo (siempre tratada como PROYECTO)"
Private Const HELP_CLASE_EVOL = "Nace como consecuencia de la necesidad de hacer evolucionar una aplicaci�n incorporando, eliminando o modificando FUNCIONALIDADES de la misma"
Private Const HELP_CLASE_OPTI = "Tienen como finalidad MEJORAR la calidad y el rendimiento sin tener consideraci�n de desarrollo, ampliaci�n, modificaci�n o eliminaci�n de funcionalidades. Son modificaciones para la adaptaci�n a entornos tecnol�gicos y/o mejoras generales siempre que no impliquen cambios funcionales"
Private Const HELP_CLASE_ATEN = "Reflejo de necesidad planteada como una consulta acerca del funcionamiento de las aplicaciones sobre las que se est� ofreciendo el soporte y que genera la creaci�n y modificaci�n de componentes y/o ejecuci�n de procesos con datos"
Private Const HELP_CLASE_CORR = "Tienen como objetivo LOCALIZAR Y ELIMINAR LOS ERRORES de la aplicaci�n o proceso. Se utiliza para resoluci�n de incidencias, prevenci�n de errores, y monitorizaci�n y/o seguimiento de ejecuciones de procesos con el objeto de verificar en modo on-line el funcionamiento"
Private Const HELP_CLASE_SPUF = "Clase para uso excepcional. Ejecuci�n directa en ambiente de producci�n de actualizaci�n de datos como salida inmediata para permitir la continuidad del servicio."
Private Const HELP_CLASE_SINC = "Debe seleccionar una clase para ver la definici�n de la misma."

Private Const GESTION_SISTEMAS = "DESA"
Private Const GESTION_BPE = "BPE"

Dim bHabilitadoModificarBeneficios As Boolean
Dim bHabilitadoModificarKPI As Boolean
Dim bKPIPendiente As Boolean
Dim sModoEdicionKPI As String
Dim sMensajesValidacionKPI As String
Dim sMensajesValidacionKPITitulo As String

Dim bTieneUnProyecto As Boolean
Dim bHuboCambiosKPI As Boolean
Dim sQuienGestiona As String
'}

Private Const colEDIT_OBLIG = 7
Private Const colEDIT_ESTADO = 9
Private Const colEDIT_OBSERV = 11
Private Const colEDIT_STATUS = 12
Private Const colEDIT_COMENT = 14

' Constantes para el armado de mails a responsables por informes de homologaci�n
Private Const colHOMRES_PETNROINTERNO = 0
Private Const colHOMRES_INFOID = 1
Private Const colHOMRES_INFOIDVER = 2
Private Const colHOMRES_CODRECURSO = 3
Private Const colHOMRES_NOMRECURSO = 4
Private Const colHOMRES_CODPERFIL = 5
Private Const colHOMRES_NOMPERFIL = 6
Private Const colHOMRES_NIVEL = 7
Private Const colHOMRES_AREA = 8
Private Const colHOMRES_INFOITEM = 9
Private Const colHOMRES_MultiSelect = 10
Private Const colHOMRES_CODESTADO = 11
Private Const colHOMRES_NOMESTADO = 12
Private Const colHOMRES_COMENTARIOS = 13
Private Const colHOMRES_ENVIO = 14
Private Const colHOMRES_REENVIO = 15
Private Const colHOMRES_DETALLEITEM = 16
Private Const colHOMRES_RESPUESTAITEM = 17
Private Const colHOMRES_EMAIL = 18
Private Const colHOMRES_TOTALCOLS = 19

' Constantes privadas del formulario
Private Const MODO_ALTA = "ALTA"
Private Const MODO_EDIT = "MODI"
Private Const MODO_VIEW = "VER"
Private Const MODO_DELETE = "BORRAR"

Dim bDebugTrace As Boolean                                  ' Habilita el "traceador" de la pila de llamadas de procesos
Dim cModo As String                                         ' Variable para indicar el modo de trabajo
Dim cModoResponsables As String                             ' Variable para indicar el modo de trabajo para responsables
Dim bEnCarga As Boolean                                     ' Variable auxiliar para no realizar cargas redundantes
Dim bCambiosPendientes As Boolean
Dim bEdicionPermitida As Boolean
Dim informeActivo As Long
Dim tabSELECTION As Integer                                 ' Guarda el TAB seleccionado
Dim lLastRowSelected As Long
'Dim glEsHomologador As Boolean                               ' Se determina al inicio si el recurso actuante es un homologador o un usuario
'Dim bEsTecnologia As Boolean                                ' Indica si el recurso actuante es tecnolog�a
'Dim bEsSeguridadInformatica As Boolean                      ' Indica si el recurso actuante es Seguridad inform�tica (ER&CA)
Dim bIncluirItemsOpcionales As Boolean                       ' Auxiliar para la carga de items del informe
Dim SectorSolicitante As String
'}

Private Const constConfirmMsgAttachConformes = "�Confirma la asignaci�n?"
Private Const constConfirmMsgDeAttachConformes = "�Confirma quitar el conforme seleccionado?"
Private Const constConfirmMsgAttachDocumentos = "�Confirma la asignaci�n?"
Private Const constConfirmMsgDeAttachDocumentos = "�Confirma quitar el documento seleccionado?"
Private Const constConfirmMsgDeAttachVariosDocumentos = "�Confirma quitar los documentos seleccionados?"
Private Const cTAG As String = "Datos Petici�n N� "

Private Const MODELO_CONTROL_ANTERIOR = 0
Private Const MODELO_CONTROL_NUEVO = 1

' Constantes para las pesta�as
Private Const orjDAT = 0
Private Const orjMEM = 1
Private Const orjANX = 2
Private Const orjDOC = 3
Private Const orjADJ = 4
Private Const orjCNF = 5
Private Const orjHOM = 6
Private Const orjEST = 7                                    ' add -064- a.

' Constantes para peticiones anexadas
Private Const colNroAsignado = 0
Private Const colTitulo = 1
Private Const colNroInterno = 2

' Constantes para documentos de metodolog�a vinculados
Private Const colDOC_DUMMY = 0
Private Const colDOC_ADJ_FILE = 1
Private Const colDOC_ADJ_PATH = 2
Private Const colDOC_NOM_AUDIT = 3
Private Const colDOC_AUDIT_DATE = 4
Private Const colDOC_ADJ_TEXTO = 5
Private Const colDOC_FECHAFMT = 6

' Constantes para control de pasajes a producci�n
Private Const colCTRL_NIVEL = 0
Private Const colCTRL_NRO = 1
Private Const colCTRL_CONTROL = 2
Private Const colCTRL_OBS = 3
Private Const colCTRL_ESTADO = 4

' Constantes para documentos adjuntos de una petici�n
Private Const colAdj_AUDIT_USER = 0
Private Const colAdj_ADJ_TIPO = 1
Private Const colAdj_ADJ_FILE = 2
Private Const colAdj_ADJ_SIZE = 3
Private Const colAdj_AREA = 4
Private Const colAdj_NOM_AUDIT = 5
Private Const colAdj_AUDIT_DATE = 6
Private Const colAdj_ADJ_TEXTO = 7
Private Const colAdj_COD_GRUPO = 8
Private Const colAdj_AUDIT_DATE_FMT = 9
Private Const colAdj_NOM_SECTOR_NV = 10
Private Const colAdj_NOM_GRUPO_NV = 11
Private Const colAdj_ADJ_ISCOMPRESS = 12

' Constantes para los conformes de una petici�n
Private Const colPcf_OK_TIPO = 0
Private Const colPcf_OK_MODO = 1
Private Const colPcf_OK_USER = 2
Private Const colPcf_OK_FECHA = 3
Private Const colPcf_OK_COMMENT = 4

' Constantes para cargar documentos
Private Const ADJUNTOS_NORMAL = 0
Private Const ADJUNTOS_HOMO = 1
Private Const ADJUNTOS_ANTERIOR = 2

' Variables para el ordenamiento
Dim bSorting As Boolean                                     ' Variable global que indica que se est� ordenando la grilla
Dim lUltimaColumnaOrdenada As Integer                       ' sss

'Public PerfilInternoActual As String                       ' upd -003- d. - Se pasa del �mbito privado al p�blico
Dim PerfilInternoActual As String                           ' A ver si sirve as�
Dim sEstado As String                                       ' Estado de la petici�n
Dim sTipoPet As String                                      ' Tipo de petici�n
Dim sClasePet As String                                     ' Clase de petici�n
Dim sImpacto As String                                      ' Impacto tecnol�gico

'Dim PetSectorSolicitante As String                         ' add -006- x.
Dim bFlgObservaciones As Boolean, bFlgBeneficios As Boolean, bFlgCaracteristicas As Boolean
Dim bFlgDescripcion As Boolean, bFlgMotivos As Boolean, bFlgRelaciones As Boolean
Dim bFlgExtension As Boolean
Dim sBpar As String, sSoli As String, sRefe As String, sSupe As String, sDire As String
Dim sBPE As String, sBPEFecha As String                     ' add -113- g.
Dim xSec As String, xGer As String, xDir As String          ' Datos de solicitante (direcci�n, gerencia y sector de la petici�n)
Dim sSituacion As String
Dim sUsualta As String
Dim anxNroPeticion
'Dim defTipoPet As String                                    ' Utilizada para guardar el default de Tipo de Petici�n
Dim xPerfNivel As String, xPerfArea As String
Dim xPerfDir As String, xPerfGer As String, xPerfSec As String, xPerfGru As String
Dim flgIngerencia As Boolean
Dim flgEjecutor As Boolean
Dim flgBP As Boolean
Dim flgSolicitor As Boolean
Dim flgSolicitorVinc As Boolean                             ' add -101- a.
Dim flgEjecutorParaModificar As Boolean                     ' add -006- x.
Dim flgEnCarga As Boolean
Dim flgDocumentoAlcance As Boolean                          ' Flag general que indica si la petici�n tiene documento de alcance (P950 o C100)
Dim grupoTipoAlta As String                                 ' add new
Dim xUsrPerfilActual As String, xUsrLoginActual As String
Dim txObservaciones As String
Dim txBeneficios As String
Dim txCaracteristicas As String
Dim txDescripcion As String
Dim txMotivos As String
Dim txRelaciones As String
Dim sRegulatorio As String                                  ' add -036- a.
'{ add -040- b.
Dim cPrioridad As String
Dim cVisibilidad As String
'}
Dim glPathDocMetod As String
Dim docOpcion As String
Dim docIndex As Integer
Dim glPathAdjPet As String
Dim adjOpcion As String
Dim adjIndex As Integer
Dim pcfOpcion As String
Dim antPathAdjPet As String
Dim antPathDocMetod As String
Dim sPcfSecuencia As String
Dim prjNroInterno As String
'Dim cModeloControl As Byte
Dim bExisteHOMA As Boolean
Dim bEstuvoEnEjecucion As Boolean
Dim bOrigenIncidencia As Boolean

' TODO: revisar el nombre de estas variables. Cambiar por uno m�s adecuado
'{ add -022- a. Variables para la parte de Homologaci�n, Seg. Inf. y Tecnolog�a

Dim bHomologacionEnEstadoActivo As Boolean                      ' Si existe homologaci�n para la petici�n actual, indica si est� en estado activo
Dim bExisteHomologacionEnPeticionActual As Boolean
Dim bExistenGruposHomologablesEnPeticionActual As Boolean
Dim bExisteSegInfEnEstadoActivo As Boolean
Dim bExisteSegInfEnPeticionActual As Boolean
Dim bExisteTecnoEnEstadoActivo As Boolean
Dim bExisteTecnoEnPeticionActual As Boolean
Dim bCompletoTodoArquitectura As Boolean
Dim bCompletoTodoSeguridadInformatica As Boolean
Dim bCompletoTodoData As Boolean 'GMT01

'Dim cSectorHomologadorNuevoEstado As String         ' TODO: ver de reemplazar esta variable

' Estado del grupo y sector de homologaci�n para esta petici�n
Dim sHomoGrupoEstado As String
Dim sHomoSectorEstado As String
Dim sHomoGrupoEstadoNuevo As String
Dim sHomoSectorEstadoNuevo As String
' Estado del grupo y sector de Seg. inform�tica para esta petici�n
Dim sSegInfGrupoEstado As String
Dim sSegInfSectorEstado As String
Dim sSegInfGrupoEstadoNuevo As String
Dim sSegInfSectorEstadoNuevo As String
' Estado del grupo y sector de Tecnolog�a/Arquitectura para esta petici�n
Dim sTecnoGrupoEstado As String
Dim sTecnoSectorEstado As String
Dim sTecnoGrupoEstadoNuevo As String
Dim sTecnoSectorEstadoNuevo As String

Dim cMensaje As String
'}
'{ add -031- a.
Dim xImportancia As String
Dim xCorpLocal As String
Dim xOrientacion As String
'}
'{ add -037- a.
Dim cProjNiv1Nom As String
Dim cProjNiv2Nom As String
Dim cProjNiv3Nom As String
'}
Dim cTextoSeleccionado As String                            ' add -048- a.
Dim cMensajeError As String                                 ' add -082- a.
Dim sPathMetodologia As String                              ' Guarda la ubicaci�n de la carpeta p�blica de metodolog�a.
Dim vEmpresaNombre() As String
Dim vIndicadores() As Integer
Dim vIndicadoresSuma() As Double
Dim vIndicadoresCategoria() As String
Dim bIndicadoresInicializados As Boolean
Dim vIndicadorKPI() As Long
Dim vOrientacionDescripcion() As String                     ' Vector para armar los tooltiptool para el combobox del atributo Orientaci�n.
Dim vClasePeticionDescripcion(6) As String                  ' Vector para armar los tooltiptool para el combobox de selecci�n de clase de petici�n.

' Arquitectura / Seguridad Inform�tica
Dim dValidacionTecnicaInicio As Date                        ' Fecha que indica si corresponde hacer este control
Dim bFlagValidacionTecnica As Boolean                       ' Si la petici�n es Nuevo desarrollo o Evolutivo, corresponde la validaci�n t�cnica.
Dim bValidacionTecnicaHabilitadoParaEditar As Boolean
Dim bHayModificacionDatos As Boolean
Dim bValidacionTecnicaCompleta As Boolean                   ' Flag que se activa cuando todos los items de la validaci�n t�cnica fueron completados.
Dim validacionTotalPreguntas As Integer

Dim textActivo As Integer                                   ' Guarda un indice al textbox activo

Dim m_cTT As cTooltip                                       ' Para los tooltips complejos
Dim m_cTT_Clase As cTooltip                                 ' Para los tooltips complejos (Clase petici�n).

'GMT04 -Ini
Dim Hab_Kpi As Boolean                                      ' Para saber si esta habilitados los kpi
Dim Hab_beneficio As Boolean                                ' Para saber si esta habilitados los beneficios
Dim Hab_PesVinculado As Boolean                             ' Para saber si esta habilitada la pesta�a de vinculados
Dim Hab_RGyP As Boolean                                     ' Para saber si esta habilitado el perfil de gestion de la demanda
'GMT04 - FIN


Private Sub Form_Load()
    Dim i As Integer                                        ' Utilizada para iterar vectores dentro de este evento
    
    sPathMetodologia = "\\bbvdfs\dfs\fpven1_e\usr\Metodologia"
     
    Call recupHabilitaciones(glNumeroPeticion) 'GMT05
     
    If bDebuggear Then
        Call Debuggear(Me, "Inicio de formulario")
        Call doLog(String(100, "-"))
        Call doLog("frmPeticionesCarga.Form_Load()")
        Call doLog(vbTab & vbTab & "glUsrPerfilActual : " & glUsrPerfilActual)
        Call doLog(vbTab & vbTab & "glModoPeticion    : " & glModoPeticion)
        Call doLog(vbTab & vbTab & "glNumeroPeticion  : " & glNumeroPeticion)
        Call doLog(vbTab & vbTab & "glNroAsignado     : " & glNroAsignado)
    End If
    

    
  
    Me.KeyPreview = True                                    ' add -002- h.
    flgEnCarga = True                                       ' Variable de control para las cargas iniciales
    cPrioridad = ""                                         ' Inicializaci�n (REVISARRRRR)
    sQuienGestiona = ""                                     ' Indica qui�n gestiona la petici�n: BPAR o BPE
    sTipoPet = ""
    sBpar = ""
    xOrientacion = ""
    bIndicadoresInicializados = False
    
    'TODO: esto es realmente necesario????
    '{ add -003- a.
    orjBtn(orjDAT).BackColor = Me.BackColor
    orjBtn(orjMEM).BackColor = Me.BackColor
    orjBtn(orjANX).BackColor = Me.BackColor
    orjBtn(orjADJ).BackColor = Me.BackColor
    orjBtn(orjDOC).BackColor = Me.BackColor
    orjBtn(orjCNF).BackColor = Me.BackColor
    orjBtn(orjEST).BackColor = Me.BackColor             ' add -064- a.
    orjBtn(orjHOM).BackColor = Me.BackColor             ' add -109- a.
    '}
    '{ add -111- a.
    i = 0
    With cboEmp
        If sp_GetEmpresa(Null, Null) Then
            .Clear
            Do While Not aplRST.EOF
                'If ClearNull(aplRST.Fields!emphab) = "S" Then      ' Con esto no alcanza para deshabilitar las empresas 20 y 21
                    ReDim Preserve vEmpresaNombre(i)
                    vEmpresaNombre(i) = ClearNull(aplRST.Fields!empnom): i = i + 1
                    .AddItem ClearNull(aplRST.Fields!empid) & ": " & ClearNull(aplRST.Fields!empabrev) & ESPACIOS & "||" & ClearNull(aplRST.Fields!empid)
                'End If
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            Call setHabilCtrl(cboEmp, "DIS")
        End If
    End With
    '}
    '{ add -113- g.
    
    'If Hab_RGyP Then 'GMT05
        Call SetInitCombo(cboGestion)
        Call SetValorCombo(cboGestion, "Sistemas", "DESA")
        Call SetValorCombo(cboGestion, "RGyP", "BPE")
        Call SetValorCombo(cboGestion, "Otro", "NULL")
        Call SetCombo(cboGestion, "", True)
   ' Else 'GMT05
   '     Call SetCombo(cboGestion, "Sistemas", True)  '-> GMT05
   ' End If 'GMT05

    If Hab_Kpi Then   ' GMT05
       udKPIValorEsperado.Max = 999999
       udKPIValorPartida.Max = 999999
       udKPITiempoParaEsperado.Max = 999999
    End If 'GMT05
    
    If Hab_Kpi Then   ' GMT05
        With cboKPIUniMed
            .Clear
            If sp_GetUnidadMedida(Null) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!unimedDesc) & ESPACIOS & "||" & ClearNull(aplRST.Fields!unimedId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = -1
            End If
        End With
          
        Call SetInitCombo(cboKPIUniMedTiempo)
        Call SetValorCombo(cboKPIUniMedTiempo, "Meses", "MES")
        Call SetValorCombo(cboKPIUniMedTiempo, "A�os", "ANO")
        Call SetCombo(cboKPIUniMedTiempo, "", True)
    End If        'GMT05
    
    Call SetInitCombo(cboRO)
    Call SetValorCombo(cboRO, "No", "-")
    Call SetValorCombo(cboRO, "ALTA", "A")
    Call SetValorCombo(cboRO, "MEDIA", "M")
    Call SetValorCombo(cboRO, "BAJA", "B")
    Call SetCombo(cboRO, "-", True)
    
'    '{ add -097- a.
'    With cboRO
'        .Clear
'        .AddItem "No" & ESPACIOS & "||-"
'        .AddItem "ALTA " & ESPACIOS & "||A"
'        .AddItem "MEDIA" & ESPACIOS & "||M"
'        .AddItem "BAJA " & ESPACIOS & "||B"
'        '.ListIndex = -1
'        .ListIndex = 0
'    End With
'    '}
    
    '{ add -064- a.
    With cmbValidarEstado
        .Clear
        .AddItem "Validez para pasaje a Producci�n" & ESPACIOS & "||P"
        .AddItem "Validez para finalizaci�n" & ESPACIOS & "||F"
        .ListIndex = 0
        Call setHabilCtrl(cmbValidarEstado, "DIS")
    End With
    '}
    
    Call SetInitCombo(cboCorpLocal)
    Call SetValorCombo(cboCorpLocal, "Local", "L")
    Call SetValorCombo(cboCorpLocal, "Corporativa", "C")
    Call SetCombo(cboCorpLocal, "L", True)
    
    Call SetInitCombo(cboRegulatorio)
    Call SetValorCombo(cboRegulatorio, "--", "-")
    Call SetValorCombo(cboRegulatorio, "No", "N")
    Call SetValorCombo(cboRegulatorio, "Si", "S")
    Call SetCombo(cboRegulatorio, "-", True)
    
    Call SetInitCombo(cboImpacto)
    Call SetValorCombo(cboImpacto, "--", "-")
    Call SetValorCombo(cboImpacto, "No", "N")
    Call SetValorCombo(cboImpacto, "Si", "S")
    Call SetCombo(cboImpacto, "N", True)
   
    '{ add -097- a. Evalua si la petici�n estuvo alguna vez en ejecuci�n
    bEstuvoEnEjecucion = False
    If glModoPeticion <> "ALTA" Then
        If sp_GetHistorial(glNumeroPeticion, Null) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!pet_estado) = "EJECUC" Then
                    bEstuvoEnEjecucion = True
                    Exit Do
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End If
    '}
    
    ' Guardo este perfil por si lo cambio
    PerfilInternoActual = glUsrPerfilActual
    
    Call SetInitCombo(cboTipopet)
    Call SetValorCombo(cboTipopet, PETICION_TEXTO_TIPONOR, PETICION_TIPO_NORMAL)
    Call SetValorCombo(cboTipopet, PETICION_TEXTO_TIPOPRO, PETICION_TIPO_PROPIA)
    Call SetValorCombo(cboTipopet, PETICION_TEXTO_TIPOPRJ, PETICION_TIPO_PROYECTO)
    Call SetValorCombo(cboTipopet, PETICION_TEXTO_TIPOESP, PETICION_TIPO_ESPECIAL)
    Call SetValorCombo(cboTipopet, PETICION_TEXTO_TIPOAUI, PETICION_TIPO_AUDITORIA_INTERNA)
    Call SetValorCombo(cboTipopet, PETICION_TEXTO_TIPOAUX, PETICION_TIPO_AUDITORIA_EXTERNA)
    
    Call SetInitCombo(cboClase)
    Call SetValorCombo(cboClase, PETICION_TEXTO_CLASESINC, PETICION_CLASE_SINCLASE)
    Call SetValorCombo(cboClase, PETICION_TEXTO_CLASECORR, PETICION_CLASE_CORRECTIVO)
    Call SetValorCombo(cboClase, PETICION_TEXTO_CLASEATEN, PETICION_CLASE_ATENCION_USUARIO)
    Call SetValorCombo(cboClase, PETICION_TEXTO_CLASEOPTI, PETICION_CLASE_OPTIMIZACION)
    Call SetValorCombo(cboClase, PETICION_TEXTO_CLASEEVOL, PETICION_CLASE_EVOLUTIVO)
    Call SetValorCombo(cboClase, PETICION_TEXTO_CLASENUEV, PETICION_CLASE_NUEVO_DESARROLLO)
    Call SetValorCombo(cboClase, PETICION_TEXTO_CLASESPUF, PETICION_CLASE_SPUFI)
    
    For i = 0 To orjBtn.Count - 1
        orjFiller(i).Width = orjBtn(i).Width - 48
        orjFiller(i).Left = orjBtn(i).Left + 16
        orjFiller(i).Top = orjBtn(i).Top + orjBtn(i).Height - 24
        orjFiller(i).BackColor = orjBtn(i).BackColor
    Next
    ' Averiguar todo lo que se puede acerca de su perfil
    ' FJS: basado en el perfil con el que actua en este momento, averigua el nivel y �rea que se utilizar�
    '       seg�n definici�n en la tabla RecursoPerfil
    xPerfNivel = getPerfNivel(PerfilInternoActual)
    xPerfArea = getPerfArea(PerfilInternoActual)
    xPerfDir = ""
    xPerfGer = ""
    xPerfSec = ""
    xPerfGru = ""
    Select Case xPerfNivel
        Case "DIRE"
            xPerfDir = xPerfArea
        Case "GERE"
            If sp_GetGerenciaXt(xPerfArea, Null) Then
                If Not aplRST.EOF Then
                    xPerfDir = ClearNull(aplRST!cod_direccion)
                    xPerfGer = xPerfArea
                End If
            End If
        Case "SECT"
            If sp_GetSectorXt(xPerfArea, Null, Null) Then
                If Not aplRST.EOF Then
                    xPerfDir = ClearNull(aplRST!cod_direccion)
                    xPerfGer = ClearNull(aplRST!cod_gerencia)
                    xPerfSec = xPerfArea
                End If
            End If
        Case "GRUP"
            If sp_GetGrupoXt(xPerfArea, Null) Then
                If Not aplRST.EOF Then
                    xPerfDir = ClearNull(aplRST!cod_direccion)
                    xPerfGer = ClearNull(aplRST!cod_gerencia)
                    xPerfSec = ClearNull(aplRST!cod_sector)
                    xPerfGru = xPerfArea
                    grupoTipoAlta = ClearNull(aplRST!grupo_tipopet)
                End If
            End If
    End Select
    bFlgExtension = False
    ' TODO: revisar si no puede realizarse esta carga al inicio del aplicativo
    Call CargarGruposDeControl                     ' Obtengo los datos principales de los grupos de control (homologaci�n, tecnolog�a, seguridad inform�tica, etc.)
    Call InicializarToolTipsControles
    Call InicializarComboAdjuntos(False)
    Call InicializarGrillas(True)
    Call InicializarPantalla(True)
    'Call FormCenterModal(Me, mdiPrincipal)
    'If orjBtn(0).Enabled Then orjBtn(0).SetFocus
    Call Puntero(False)
    Debug.Print "CARGA_FIN: " & TimeToMillisecond()
    Debug.Print "Nro. peticion: " & glNumeroPeticion
End Sub

Private Sub Form_Activate()
    If orjBtn(0).Enabled Then orjBtn(0).SetFocus
    'Call App.LogEvent("Entr� a editar una petici�n.", vbLogEventTypeInformation)
End Sub

'{ add -064- a.
Private Sub InicializarGrillas(bOpcion As Boolean)
    ' IMPORTANTE:
    ' No se utiliza el IniciarScroll para los controles MSHFlexgrid porque la API no lo soporta.
    ' 18.02.2016: se agreg� una funci�n para el scroll del MSHFlexgrid.
    If bOpcion Then
        Call IniciarScroll(grdControl)
        '{ add -113- g.
        Call HookForm(grdHomologacion)
        Call HookForm(grdHomoResponsables)
        
        If Hab_beneficio Then 'GMT05
            Call HookForm(grdBeneficios)
        End If 'GMT05
        
        If Hab_Kpi Then 'GMT05
           Call HookForm(grdKPI)
        End If 'GMT05
        '}
        Call HookForm(grdArquitectura)
        Call HookForm(grdSeguridadInformatica)
        Call HookForm(grdData) 'GMT01
    Else
        Call DetenerScroll(grdControl)
        '{ add -113- g.
        Call UnHookForm(grdHomologacion)
        Call UnHookForm(grdHomoResponsables)
        
        If Hab_beneficio Then 'GMT05
            Call UnHookForm(grdBeneficios)
        End If 'GMT05
        
        If Hab_Kpi Then 'GMT05
           Call UnHookForm(grdKPI)
        End If 'GMT05
        
        Call UnHookForm(grdArquitectura)
        Call UnHookForm(grdSeguridadInformatica)
        Call UnHookForm(grdData) 'GMT01
        '}
    End If
End Sub
'}

'{ add -002- h.
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then       ' Tecla: ESCAPE
        cmdCancel_Click
    End If
End Sub
'}

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
        If KeyCode = vbKeyDelete Then
            Me.ActiveControl.ListIndex = -1
        End If
    End If
    If Not glModoPeticion = "ALTA" Then
        orjBtn_Click KeyCode - 112
    Else
        Select Case KeyCode
            Case 112        ' Tecla de funci�n: F1
                orjBtn_Click 0
            Case 113        ' Tecla de funci�n: F2
                orjBtn_Click 1
        End Select
    End If
End Sub

Private Sub cboImportancia_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    xUsrLoginActual = glLOGIN_ID_REEMPLAZO
    xUsrPerfilActual = PerfilInternoActual
End Sub

Private Sub cboSector_Click()
    If flgEnCarga = True Then Exit Sub
    Call Debuggear(Me, "cboSector_Click()")
    If Not LockProceso(True) Then Exit Sub
    Call Puntero(True)
    Call CargaIntervinientes
    'If cboBpar = "" Then Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))     ' del -116- b.
    '{ add -116- b.
    If glLOGIN_Gerencia <> "DESA" Then
        If sp_GetSector(CodigoCombo(cboSector, True), Null, "S") Then
            If ClearNull(aplRST.Fields!cod_perfil) = "GBPE" Then
                sQuienGestiona = "BPE"
                sBPE = ClearNull(aplRST.Fields!cod_bpar)
                cboGestion.ListIndex = PosicionCombo(cboGestion, sQuienGestiona, True)
                cboReferenteBPE.ListIndex = PosicionCombo(cboReferenteBPE, sBPE)
                cboBpar.ListIndex = -1
            Else
                sQuienGestiona = "DESA"
                cboGestion.ListIndex = PosicionCombo(cboGestion, sQuienGestiona, True)
                sBpar = ClearNull(aplRST.Fields!cod_bpar)
                cboBpar.ListIndex = PosicionCombo(cboBpar, sBpar)
                cboReferenteBPE.ListIndex = -1
            End If
        End If
    Else
        If sp_GetGrupoXt(glLOGIN_Grupo, glLOGIN_Sector) Then
            sQuienGestiona = "DESA"
            cboGestion.ListIndex = PosicionCombo(cboGestion, sQuienGestiona, True)
            sBpar = ClearNull(aplRST.Fields!cod_bpar)
            cboBpar.ListIndex = PosicionCombo(cboBpar, sBpar)
            cboReferenteBPE.ListIndex = -1
        End If
    End If
    '}
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Private Sub cmdSector_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    frmPeticionesSector.Show 1
    DoEvents
    Call LockProceso(False)
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla(True)
    End If
End Sub

Private Sub cmdGrupos_Click()
    If flgEnCarga Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glSector = ""
    frmPeticionesGrupo.Show vbModal
    DoEvents
    Call LockProceso(False)
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla(True)
    End If
End Sub

'{ add -006- g.
Private Sub cboSolicitante_Click()
    DoEvents
    Select Case glModoPeticion
        Case "MODI"
            If (CodigoCombo(cboTipopet, True)) = "ESP" Then
                cboSolicitante_Change
            End If
    End Select
End Sub

Private Sub cboSolicitante_Change()
    If ClearNull(xSec) <> ClearNull(CodigoCombo(cboSector, True)) Then
        Select Case glModoPeticion
            Case "MODI"
                If (CodigoCombo(cboTipopet, True)) = "ESP" Then
                    cboReferente.ListIndex = -1
                    cboSupervisor.ListIndex = -1
                    cboDirector.ListIndex = -1
                End If
        End Select
    Else
        cboReferente.ListIndex = SetCombo(cboReferente, sRefe)
        cboSupervisor.ListIndex = SetCombo(cboSupervisor, sSupe)
        cboDirector.ListIndex = SetCombo(cboDirector, sDire)
        If ClearNull(sSoli) <> ClearNull(CodigoCombo(cboSolicitante)) Then
            Select Case glModoPeticion
                Case "MODI"
                    If (CodigoCombo(cboTipopet, True)) = "ESP" Then
                        cboReferente.ListIndex = -1
                        cboSupervisor.ListIndex = -1
                        cboDirector.ListIndex = -1
                    End If
            End Select
        End If
    End If
End Sub
'}

Private Sub cboTipopet_Click()
    If flgEnCarga Then Exit Sub
    If Not LockProceso(True) Then Exit Sub
    If bDebuggear Then Call doLog("cboTipopet_Click()")
    Call SeleccionaTipoPeticion
    Call Puntero(False)
End Sub

Private Sub SeleccionaTipoPeticion()
    Dim tipoPetSeleccionado As String
    
    Call LockProceso(True)
    tipoPetSeleccionado = CodigoCombo(cboTipopet, True)
    If bDebuggear Then
        Call doLog("SeleccionaTipoPeticion(" & tipoPetSeleccionado & ")")
    End If
    Select Case tipoPetSeleccionado
        Case "PRJ"
            sEstado = "APROBA"
            txtFechaComite.text = Format(date, "dd/mm/yyyy")
        Case "NOR"
            Call CargaCombosConValor
        Case "PRO"
            sEstado = "PLANIF"
            txtFechaComite.text = Format(date, "dd/mm/yyyy")
            Call InicializarCombos
        Case "ESP"
            'Call setHabilCtrl(cboSector, "NOR")
            'Call setHabilCtrl(cboSolicitante, "NOR")
            'Call CargaCombosConTodo
        Case "AUI", "AUX"
    End Select
    Call InicializarClasePeticion
    Call LockProceso(False)
End Sub

Private Sub SeleccionaClasePeticion()
    Dim clasePetSeleccionado As String
    
    If flgEnCarga Then Exit Sub
    Call LockProceso(True)
    
    clasePetSeleccionado = CodigoCombo(cboClase, True)
    
    If bDebuggear Then Call doLog("SeleccionaClasePeticion(" & clasePetSeleccionado & ")")
    Select Case clasePetSeleccionado
        Case "NUEV", "EVOL"
            Call setHabilCtrl(cboImpacto, "NOR")
            If cboImpacto.ListIndex = -1 Then cboImpacto.ListIndex = PosicionCombo(cboImpacto, "N", True)
            Call setHabilCtrl(cboRegulatorio, "NOR")
            If cboRegulatorio.ListIndex = -1 Then cboRegulatorio.ListIndex = 0
            Call setHabilCtrl(cboRO, "NOR")
            If cboRO.ListIndex = -1 Then cboRO.ListIndex = PosicionCombo(cboRO, "-", True)
        Case "ATEN", "CORR", "OPTI", "SPUF"
            Call setHabilCtrl(cboImpacto, "DIS")
            cboImpacto.ListIndex = PosicionCombo(cboImpacto, "N", True)
            Call setHabilCtrl(cboRegulatorio, "NOR")
            If cboRegulatorio.ListIndex = 0 Then cboRegulatorio.ListIndex = 1
            Call setHabilCtrl(cboRO, "DIS")
            cboRO.ListIndex = PosicionCombo(cboRO, "-", True)
        Case "SINC"
            Call setHabilCtrl(cboImpacto, "DIS")
            cboImpacto.ListIndex = PosicionCombo(cboImpacto, "-", True)
            If glUsrPerfilActual = "ANAL" Then
                cboRegulatorio.ListIndex = 0
                Call setHabilCtrl(cboRegulatorio, "DIS")
            End If
    End Select
    Call LockProceso(False)
End Sub

Private Sub cmdAgrupar_Click()
    glSelectAgrup = ""
    frmPeticionAgrup.Show vbModal
    DoEvents
End Sub

Private Sub cmdAsigNro_Click()
    'en una epoca el numero podia asignarse manualmente
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        frmAsignaNumero.Show 1
        DoEvents
        If Me.Tag = "REFRESH" Then
            cmdAsigNro.Enabled = False
            Call InicializarPantalla(False)
        End If
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdCambioEstado_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If bDebuggear Then Call doLog("cmdCambioEstado_Click()")
    If CamposObligatoriosCambioEstado Then
        frmPeticionesCambioEstado.Show 1
        DoEvents
        If Me.Tag = "REFRESH" Then
            Call InicializarPantalla(False)
        End If
        Call LockProceso(False)
    End If
End Sub

Private Function CamposObligatoriosCambioEstado() As Boolean
    CamposObligatoriosCambioEstado = True
    
    If glNumeroPeticion <> "" Then
        ' Control de clase
        If InStr(1, "BPAR|GBPE|", PerfilInternoActual, vbTextCompare) > 0 Then
            If CodigoCombo(cboClase, True) = "SINC" Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Antes de realizar un cambio de estado" & vbCrLf & "debe especificar la clase de la petici�n.", vbExclamation + vbOKOnly, "Clase de petici�n"
                CamposObligatoriosCambioEstado = False
                Call LockProceso(False)
                Exit Function
            End If
        End If
    
        ' Control de especificaci�n de Impacto tecnol�gico
        If InStr(1, "BPAR|GBPE|", PerfilInternoActual, vbTextCompare) > 0 Then
            If InStr(1, "NUEV|OPTI|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                If CodigoCombo(cboImpacto, True) = "-" Then                                     ' add -091- a.
                    Call orjBtn_Click(orjDAT)
                    MsgBox "Antes de realizar un cambio de estado, DEBE especificar IMPACTO TECNOL�GICO", vbOKOnly + vbExclamation, "Impacto tecnol�gico"
                    CamposObligatoriosCambioEstado = False
                    Call LockProceso(False)
                    Exit Function
                End If
            End If
        End If
        
        ' Control de especificaci�n de regulatorio
        If InStr(1, "BPAR|GBPE|", PerfilInternoActual, vbTextCompare) > 0 Then
            If CodigoCombo(cboRegulatorio, True) = "-" Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Antes de realizar un cambio de estado, DEBE especificar si es REGULATORIO", vbOKOnly + vbExclamation, "Regulatorio"
                CamposObligatoriosCambioEstado = False
                Call LockProceso(False)
                Exit Function
            End If
        End If
        
        ' Control de especificaci�n de Referente de Sistema
        If InStr(1, "GBPE|", PerfilInternoActual, vbTextCompare) > 0 Then
            ' 28.04.2016
            ' TODO: habr�a que deshabilitar esto para que siempre controle que sea designado un referente de sistemas
            If cboBpar.ListIndex = -1 Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Antes de realizar un cambio de estado, DEBE especificar el Referente de Sistema", vbOKOnly + vbExclamation, "Referente de Sistema"
                CamposObligatoriosCambioEstado = False
                Call LockProceso(False)
                Exit Function
            End If
        End If
        
        If Hab_beneficio Then 'GMT05
        ' Control antes de cambio de estado para el referente de RGP (que complete la valoraci�n propia).
            If glUsrPerfilActual = "GBPE" And glModoPeticion = "VIEW" Then
                If chkCargarBeneficios.value = 0 Then
                    If Not ValidarValoracion Then
                        MsgBox "Antes de realizar un cambio de estado, DEBE" & vbCrLf & "completar los items de la pesta�a Impactos.", vbOKOnly + vbExclamation
                        CamposObligatoriosCambioEstado = False
                        Call LockProceso(False)
                        Exit Function
                    End If
                    If Hab_Kpi Then 'GMT05
                        If Not ValidarKPI Then
                            MsgBox sMensajesValidacionKPI, vbOKOnly + vbExclamation
                            CamposObligatoriosCambioEstado = False
                            Call LockProceso(False)
                            Exit Function
                        End If
                    End If 'GMT05
                End If
            End If
        End If 'GMT05
    End If
End Function

Private Sub cmdPrint_Click()
    Dim nCopias As Integer

    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion = "" Then
        MsgBox ("No selecciono ninguna Petici�n")
        Call LockProceso(False)
        Exit Sub
    End If
    
    rptFormulario.Show vbModal
    nCopias = rptFormulario.Copias
    Call Puntero(True)
    DoEvents
    While nCopias > 0
        If rptFormulario.chkFormu Then
            Call PrintFormulario(glNumeroPeticion)
        End If
        If rptFormulario.chkHisto Then
            Call PrintHitorial(glNumeroPeticion)
        End If
        nCopias = nCopias - 1
    Wend
    Call Puntero(False)
    
    Call LockProceso(False)
End Sub

Private Sub cmdVerAnexora_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If Val(getStrCodigo(txtNroAnexada.text, True)) = 0 Then
        MsgBox "Esta petici�n no se encuentra anexada!", vbInformation + vbOKOnly
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    glNumeroPeticion = getStrCodigo(txtNroAnexada.text, True)
    touchForms
    Call InicializarPantalla(True)
End Sub

Private Sub cmdVerAnexa_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If anxNroPeticion = "" Then
        MsgBox ("No selecciono ninguna petici�n")
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    glNumeroPeticion = anxNroPeticion
    touchForms
    Call InicializarPantalla(True)
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        Me.Height = 9255        '8960    '8730
        Me.Width = 14800 ' 14685
    End If
End Sub

Private Sub orjBtn_Click(Index As Integer)
    Dim i As Integer
    
    Call Puntero(True)                      ' add -057- a.
    If Index < 8 And Index >= 0 Then
        For i = 0 To orjBtn.Count - 1
            orjFiller(i).visible = False
            orjBtn(i).Font.Bold = False
            orjFrame(i).visible = False
        Next
        orjFrame(Index).visible = True
        orjBtn(Index).Font.Bold = True
        orjBtn(Index).BackColor = orjFrame(Index).BackColor
        orjFiller(Index).visible = True
    End If
    Call Puntero(False)                 ' add -057- a.
End Sub

Private Sub cmdHistView_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        Call Puntero(True)
        frmHistorial.Show 1
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If bDebuggear Then Call doLog("cmdModificar_Click()")
    glModoPeticion = "MODI"
    Call HabilitarBotones
End Sub

Private Sub cmdCancel_Click()
    If Not LockProceso(True) Then Exit Sub
    If bDebuggear Then Call doLog("cmdCancel_Click()")
    lblCustomToolTip.Caption = ""                           ' add -037- a.
    If glModoPeticion = "MODI" Then
        glModoPeticion = "VIEW"
        'CUIDADO
        flgEnCarga = True
        Call StatusLocal(sbMainPeticion, "")
        Call InicializarTipoPeticion
        Call CargarPeticion(True)
        Call HabilitarBotones
        flgEnCarga = False
    Else
        SectorSolicitante = ""
        If bDebuggear Then Call Debuggear(Me, "CERRANDO petici�n con cmdCancel_Click()", True)
        Unload Me
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdOK_Click()
    If bDebuggear Then Call doLog("cmdOk_Click()")
    Call Puntero(True)
    Call GuardarPeticion
    Call Puntero(False)
End Sub

Private Sub GuardarCambiosHistorial()
    Dim sCambioAtributo As String
    Dim lPeriodo_Actual As Long, lPeriodo_Siguiente As Long
    Dim sPeriodo_Actual As String, sPeriodo_Siguiente As String
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE T�TULO
    ' ------------------------------------------------------
    If ClearNull(txtTitulo.Tag) <> ClearNull(txtTitulo.text) Then
        sCambioAtributo = "� Cambio de t�tulo � Antes: " & ClearNull(txtTitulo.Tag) & " / Despu�s: " & ClearNull(txtTitulo.text)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGTIT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE TIPO
    ' ------------------------------------------------------
    If sTipoPet <> "" Then
        If CodigoCombo(cboTipopet, True) <> sTipoPet Then
            sCambioAtributo = "� Cambio de tipolog�a � Antes: " & sTipoPet & " - Despu�s: " & CodigoCombo(cboTipopet, True)
            Call sp_AddHistorial(glNumeroPeticion, "PCHGTYPE", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
            Call sp_DoMensaje("PET", "PCHGTYPE", glNumeroPeticion, "", "NULL", "", "")
        End If
    End If

    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE CLASE
    ' ------------------------------------------------------
    If sClasePet <> "" And sClasePet <> "SINC" Then
        If CodigoCombo(cboClase, True) <> sClasePet Then
            sCambioAtributo = "� Cambio de clase � Antes: " & sClasePet & " - Despu�s: " & CodigoCombo(cboClase, True)
            Call sp_AddHistorial(glNumeroPeticion, "PCHGCLS", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
            Call sp_DoMensaje("PET", "PCHGCLS", glNumeroPeticion, "", "NULL", "", "")
            If bExisteHomologacionEnPeticionActual Then Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 114, sEstado, sCambioAtributo)
        End If
    Else
        If CodigoCombo(cboClase, True) <> "SINC" Then
            sCambioAtributo = "� Asignaci�n inicial de clase � " & TextoCombo(cboClase, CodigoCombo(cboClase, True), True)
            Call sp_AddHistorial(glNumeroPeticion, "PNEWCLS", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
            Call sp_DoMensaje("PET", "PNEWCLS", glNumeroPeticion, "", "NULL", "", "")
            If bExisteHomologacionEnPeticionActual Then Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 116, sEstado, sCambioAtributo)
        End If
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE IMPACTO TECNOL�GICO
    ' ------------------------------------------------------
    If CodigoCombo(cboImpacto, True) <> sImpacto Then
        sCambioAtributo = ""
        Select Case sImpacto
            Case "-"
                Select Case CodigoCombo(cboImpacto, True)
                    Case "N": sCambioAtributo = "� Impacto tecnol�gico � Antes: -- / Despu�s: No."
                    Case "S": sCambioAtributo = "� Impacto tecnol�gico � Antes: -- / Despu�s: Si."
                End Select
            Case "N"
                Select Case CodigoCombo(cboImpacto, True)
                    Case "-": sCambioAtributo = "� Impacto tecnol�gico � Antes: No / Despu�s: --."
                    Case "S": sCambioAtributo = "� Impacto tecnol�gico � Antes: No / Despu�s: Si."
                End Select
            Case "S"
                Select Case CodigoCombo(cboImpacto, True)
                    Case "-": sCambioAtributo = "� Impacto tecnol�gico � Antes: Si / Despu�s: --."
                    Case "N": sCambioAtributo = "� Impacto tecnol�gico � Antes: Si / Despu�s: No."
                End Select
        End Select
        Call sp_AddHistorial(glNumeroPeticion, "PCHGIMP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
        Call sp_DoMensaje("PET", "PCHGIMP", glNumeroPeticion, "", "NULL", "", "")
    End If

    ' ------------------------------------------------------
    ' REGISTRO DE REGULATORIO
    ' ------------------------------------------------------
    If sRegulatorio <> "-" Then
        sCambioAtributo = ""
        If CodigoCombo(cboRegulatorio, True) <> sRegulatorio Then
            Select Case sRegulatorio
                Case "-": sCambioAtributo = "� Regulatorio � Antes: -- / Despu�s: " & IIf(CodigoCombo(cboRegulatorio, True) = "S", "Si", "No") & "."
                Case "N": sCambioAtributo = "� Regulatorio � Antes: No / Despu�s: " & IIf(CodigoCombo(cboRegulatorio, True) = "-", "--.", "Si.")
                Case "S": sCambioAtributo = "� Regulatorio � Antes: Si / Despu�s: " & IIf(CodigoCombo(cboRegulatorio, True) = "-", "--.", "No.")
            End Select
            Call sp_AddHistorial(glNumeroPeticion, "PCHGREGU", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
            Call sp_DoMensaje("PET", "PCHGREGU", glNumeroPeticion, "", "NULL", "", "")
        End If
    End If
    ' ------------------------------------------------------
    ' REGISTRO DE EMPRESA
    ' ------------------------------------------------------
    If cboEmp.Tag <> CodigoCombo(cboEmp, True) Then
        sCambioAtributo = "� Empresa � Antes: " & cboEmp.Tag & ". Despu�s: " & CodigoCombo(cboEmp, True) & "."
        Call sp_AddHistorial(glNumeroPeticion, "PCHGEMP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If

    ' ------------------------------------------------------
    ' REGISTRO DE VISIBILIDAD (IMPORTANCIA)
    ' ------------------------------------------------------
    '{ add -040- b.
    If glModoPeticion = "MODI" Then
        If CodigoCombo(cboImportancia, True) <> xImportancia Then
            If xImportancia = "" Then
                sCambioAtributo = "� Visibilidad � Antes: --. Despu�s: " & TextoCombo(cboImportancia, CodigoCombo(cboImportancia, True), True) & "."
            Else
                sCambioAtributo = "� Visibilidad � Antes: " & TextoCombo(cboImportancia, xImportancia, True) & ". Despu�s " & TextoCombo(cboImportancia, CodigoCombo(cboImportancia, True), True) & "."
            End If
            Call sp_AddHistorial(glNumeroPeticion, "PCHGVISI", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
        End If
    End If
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE RO
    ' ------------------------------------------------------
    If cboRO.Tag <> "" And cboRO.Tag <> CodigoCombo(cboRO, True) Then
        sCambioAtributo = "� Cambio de RO / Criticidad � Antes: " & cboRO.Tag & " / Despu�s: " & CodigoCombo(cboRO, True)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGRO", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If

    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE PROYECTO
    ' ------------------------------------------------------
    If Val(txtPrjId.Tag) + Val(txtPrjSubId.Tag) + Val(txtPrjSubsId.Tag) > 0 Then
        If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) <> Val(txtPrjId.Tag) + Val(txtPrjSubId.Tag) + Val(txtPrjSubsId.Tag) Then
            sCambioAtributo = "� Cambio de proyecto � Antes: " & Val(txtPrjId.Tag) & "." & Val(txtPrjSubId.Tag) & "." & Val(txtPrjSubsId.Tag) & _
                              " / Despu�s: " & Val(txtPrjId) & "." & Val(txtPrjSubId) & "." & Val(txtPrjSubsId)
            Call sp_AddHistorial(glNumeroPeticion, "PCHGPRJ", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
        End If
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE ORIENTACION
    ' ------------------------------------------------------
    If xOrientacion <> "" And xOrientacion <> CodigoCombo(cboOrientacion, True) Then
        sCambioAtributo = "� Cambio de orientaci�n � Antes: " & TextoCombo(cboOrientacion, xOrientacion, True) & " / Despu�s: " & TextoCombo(cboOrientacion, CodigoCombo(cboOrientacion, True), True)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGORI", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE GESTION
    ' ------------------------------------------------------
    If cboGestion.Tag <> CodigoCombo(cboGestion, True) Then
        sCambioAtributo = "� Cambio de gesti�n � Antes: " & cboGestion.Tag & " / Despu�s: " & CodigoCombo(cboGestion, True)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGADM", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE SECTOR SOLICITANTE
    ' ------------------------------------------------------
    If xSec <> CodigoCombo(cboSector, True) Then
        sCambioAtributo = "� Cambio de sector solicitante � Antes: " & xSec & " / Despu�s: " & CodigoCombo(cboSector, True)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGSECT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE SOLICITANTE
    ' ------------------------------------------------------
    If sSoli <> CodigoCombo(cboSolicitante) Then
        sCambioAtributo = "� Cambio de solicitante � Antes: " & sSoli & " / Despu�s: " & CodigoCombo(cboSolicitante)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGSOLI", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE REFERENTE
    ' ------------------------------------------------------
    If sRefe <> CodigoCombo(cboReferente) Then
        sCambioAtributo = "� Cambio de referente � Antes: " & sRefe & " / Despu�s: " & CodigoCombo(cboReferente)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGREFE", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE SUPERVISOR
    ' ------------------------------------------------------
    If sSupe <> CodigoCombo(cboSupervisor) Then
        sCambioAtributo = "� Cambio de supervisor � Antes: " & sSupe & " / Despu�s: " & CodigoCombo(cboSupervisor)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGSUPE", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE AUTORIZANTE
    ' ------------------------------------------------------
    If sDire <> CodigoCombo(cboDirector) Then
        sCambioAtributo = "� Cambio de autorizante � Antes: " & sDire & " / Despu�s: " & CodigoCombo(cboDirector)
        Call sp_AddHistorial(glNumeroPeticion, "PCHGDIRE", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE REFERENTE RGyP
    ' ------------------------------------------------------
    If glModoPeticion = "MODI" Then
        sCambioAtributo = ""
        If ClearNull(CodigoCombo(cboReferenteBPE, False)) <> "" And ClearNull(CodigoCombo(cboReferenteBPE, False)) <> sBPE Then
            sCambioAtributo = "� Cambio de Referente de RGyP � Antes: " & sp_GetRecursoNombre(sBPE) & " (" & sBPE & ") / Despu�s: " & sp_GetRecursoNombre(CodigoCombo(cboReferenteBPE, False)) & " (" & CodigoCombo(cboReferenteBPE, False) & ")"
            Call sp_AddHistorial(glNumeroPeticion, "PCHGBPE", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
        End If
    End If
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE VALORIZACI�N
    ' ------------------------------------------------------
    If Hab_beneficio Then 'GMT05
        If glModoPeticion = "MODI" Then
            If chkCargarBeneficios.Tag <> IIf(chkCargarBeneficios.value = 1, "N", "S") Then
                If IIf(chkCargarBeneficios.value = 1, "N", "S") = "N" Then
                    sCambioAtributo = "� El usuario ha seleccionado no cargar los beneficios. Se quita la valorizaci�n que hubiera sido cargada para la petici�n. �"
                Else
                    sCambioAtributo = "� El usuario ha seleccionado cargar los beneficios. �"
                End If
                Call sp_AddHistorial(glNumeroPeticion, "PCHGVAL2", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
            End If
        End If
    End If 'GMT05
    
    ' ------------------------------------------------------
    ' REGISTRO DE CAMBIO DE REFERENTE DE SISTEMA
    ' ------------------------------------------------------
    If glModoPeticion = "MODI" Then
        sCambioAtributo = ""
        If ClearNull(CodigoCombo(cboBpar, False)) <> "" And ClearNull(CodigoCombo(cboBpar, False)) <> sBpar Then
            If sBpar = "" Then
                sCambioAtributo = "� Asignaci�n inicial de Referente de Sistema: " & sp_GetRecursoNombre(CodigoCombo(cboBpar, False)) & " (" & CodigoCombo(cboBpar, False) & ") �"
            Else
                sCambioAtributo = "� Cambio de Referente de Sistema � Antes: " & sp_GetRecursoNombre(sBpar) & " (" & sBpar & ") / Despu�s: " & sp_GetRecursoNombre(CodigoCombo(cboBpar, False)) & " (" & CodigoCombo(cboBpar, False) & ")"
            End If
            Call sp_AddHistorial(glNumeroPeticion, "PCHGBP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
            If ClearNull(txtFechaComite.DateValue) = "" Then txtFechaComite.DateValue = date
        End If
    End If
End Sub

Private Sub GuardarPeticion()
    Dim sDir, sGer, sSec As String
    Dim xBpar, sSoli, sRefe, sSupe, sAuto As String
    Dim auxNro As String                                            ' Auxiliar para recuperar el nro. interno
    Dim xEstado As String
    Dim nNumeroAsignado As Long
    Dim iUsrPerfilActual As String, iUsrLoginActual As String
    Dim bMsg As Boolean
    Dim sTXTalcance As String
    Dim sPetClase As String
    
    If Not LockProceso(True) Then Exit Sub
    If bDebuggear Then doLog ("GuardarPeticion()")
    
    If CamposObligatorios Then
        Call touchForms
        sSec = CodigoCombo(Me.cboSector, True)
        If sp_GetSectorXt(sSec, Null, Null) Then
            sDir = ClearNull(aplRST!cod_direccion)
            sGer = ClearNull(aplRST!cod_gerencia)
        End If
        
        iUsrPerfilActual = xUsrPerfilActual
        iUsrLoginActual = xUsrLoginActual
        If iUsrPerfilActual = "SADM" Then
            iUsrPerfilActual = "ADMI"
        End If
        If ClearNull(CodigoCombo(cboImportancia, True)) = "" Then
            iUsrPerfilActual = ""
            iUsrLoginActual = ""
        End If
        
        sSoli = CodigoCombo(Me.cboSolicitante)
        sRefe = CodigoCombo(Me.cboReferente)
        sSupe = CodigoCombo(Me.cboSupervisor)
        sAuto = CodigoCombo(Me.cboDirector)
        xBpar = CodigoCombo(cboBpar)
        sTipoPet = CodigoCombo(Me.cboTipopet, True)
        sPetClase = CodigoCombo(cboClase, True)
        
        Select Case glModoPeticion
            Case "ALTA"
                glNumeroPeticion = ""
                If sp_UpdatePeticion(0, txtNroAsignado, txtTitulo, sTipoPet, Null, CodigoCombo(cboCorpLocal, True), CodigoCombo(cboOrientacion, True), CodigoCombo(cboImportancia, True), iUsrPerfilActual, iUsrLoginActual, getStrCodigo(txtNroAnexada.text, True), prjNroInterno, txtFechaPedido.DateValue, Null, txtFechaComite.DateValue, txtFiniplan.DateValue, txtFfinplan.DateValue, txtFinireal.DateValue, txtFfinreal.DateValue, txtHspresup, sDir, sGer, sSec, sUsualta, sSoli, sRefe, sSupe, sAuto, sEstado, txtFechaEstado.DateValue, sSituacion, xBpar, sPetClase, CodigoCombo(cboImpacto, True), 1, CodigoCombo(cboRegulatorio, True), CInt(txtPrjId.text), CInt(txtPrjSubId.text), CInt(txtPrjSubsId.text), CodigoCombo(cboEmp, True), CodigoCombo(cboRO, True), CodigoCombo(cboReferenteBPE), txtFechaRefBPE, CodigoCombo(cboGestion, True), IIf(chkCargarBeneficios.value = 1, "N", "S")) Then       ' add -113- c.
                    If Not aplRST.EOF Then
                        auxNro = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")    ' recupera el ultimo numero (pet_nrointerno) generado
                    End If
                End If
                Select Case CodigoCombo(cboTipopet, True)
                    Case "NOR": Call sp_AddHistorial(auxNro, "PNEW001", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Dada de alta con el nro. interno: " & auxNro)
                    Case "ESP": Call sp_AddHistorial(auxNro, "PNEW002", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Dada de alta con el nro. interno: " & auxNro)
                    Case "PRO": Call sp_AddHistorial(auxNro, "PNEW003", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Dada de alta con el nro. interno: " & auxNro)
                    Case "PRJ": Call sp_AddHistorial(auxNro, "PNEW004", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Dada de alta con el nro. interno: " & auxNro)
                    Case "AUX", "AUI": Call sp_AddHistorial(auxNro, "PNEW000", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Dada de alta con el nro. interno: " & auxNro)
                End Select
            Case Else
                Call GuardarCambiosHistorial
                If sp_UpdatePeticion(txtNrointerno, txtNroAsignado, txtTitulo, sTipoPet, "", CodigoCombo(cboCorpLocal, True), CodigoCombo(cboOrientacion, True), CodigoCombo(cboImportancia, True), iUsrPerfilActual, iUsrLoginActual, getStrCodigo(txtNroAnexada.text, True), prjNroInterno, txtFechaPedido.DateValue, Null, txtFechaComite.DateValue, txtFiniplan.DateValue, txtFfinplan.DateValue, txtFinireal.DateValue, txtFfinreal.DateValue, txtHspresup, sDir, sGer, sSec, sUsualta, sSoli, sRefe, sSupe, sAuto, sEstado, txtFechaEstado.DateValue, sSituacion, xBpar, sPetClase, cboImpacto, 1, cboRegulatorio.List(cboRegulatorio.ListIndex), CInt(txtPrjId.text), CInt(txtPrjSubId.text), CInt(txtPrjSubsId.text), CodigoCombo(cboEmp, True), CodigoCombo(cboRO, True), CodigoCombo(cboReferenteBPE), txtFechaRefBPE, CodigoCombo(cboGestion, True), IIf(chkCargarBeneficios.value = 1, "N", "S")) Then
                    If Not aplRST.EOF Then
                        auxNro = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
                    End If
                End If
        End Select
        
        ' ------------------------------------------------------
        ' REGISTRO DE CAMBIO DE TEXTOS DE LA PETICI�N
        ' ------------------------------------------------------
        sTXTalcance = ""
        bMsg = False
        
        glNumeroPeticion = auxNro
        
        If bFlgDescripcion Then
            Call sp_UpdateMemo(glNumeroPeticion, PETICION_TEXTO_DESCRIPCION, memDescripcion.text)
            If bFlgExtension Then
                sTXTalcance = sTXTalcance & "� DESCRIPCION ANT. �" & vbCrLf & txDescripcion & vbCrLf
                bMsg = True
            End If
        End If
        If bFlgCaracteristicas Then
            Call sp_UpdateMemo(glNumeroPeticion, PETICION_TEXTO_CARACTERISTICAS, memCaracteristicas.text)
            If bFlgExtension Then
                sTXTalcance = sTXTalcance & "� CARACTERISTICAS ANT. �" & vbCrLf & txCaracteristicas & vbCrLf
                bMsg = True
            End If
        End If
        If bFlgMotivos Then
            Call sp_UpdateMemo(glNumeroPeticion, PETICION_TEXTO_MOTIVOS, memMotivos.text)
            If bFlgExtension Then
                sTXTalcance = sTXTalcance & "� MOTIVOS ANT. �" & vbCrLf & txMotivos & vbCrLf
                bMsg = True
            End If
        End If
        If bFlgRelaciones Then
            Call sp_UpdateMemo(glNumeroPeticion, PETICION_TEXTO_RELACIONES, memRelaciones.text)
            If bFlgExtension Then
                sTXTalcance = sTXTalcance & "� RELACIONES ANT. �" & vbCrLf & txRelaciones & vbCrLf
                bMsg = True
            End If
        End If
        If bFlgObservaciones Then
            Call sp_UpdateMemo(glNumeroPeticion, PETICION_TEXTO_OBSERVACIONES, memObservaciones.text)
            If bFlgExtension Then
                sTXTalcance = sTXTalcance & "� OBSERVACIONES ANT. �" & vbCrLf & txObservaciones & vbCrLf
                bMsg = True
            End If
        End If
        
        If bMsg Then
            Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sTXTalcance)
            Call sp_DoMensaje("PET", "PCHGTXT", glNumeroPeticion, "", "NULL", "", "")
        End If
        
        'CASO MUY ESPECIAL alta de peticiones PROPIAS
        ' Porque debe ser agregado de manera autom�tica el sector y grupo del solicitante (FJS)
        If glModoPeticion = "ALTA" And InStr(1, "PRO|ESP|PRJ|", sTipoPet, vbTextCompare) > 0 Then
            nNumeroAsignado = sp_ProximoNumero("ULPETASI")  ' Asigna el numero automaticamente
            If nNumeroAsignado > 0 Then
                If Not sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, nNumeroAsignado) Then
                    MsgBox "Error al asignar n�mero.", vbCritical + vbOKOnly
                End If
            Else
                MsgBox "Error al asignar n�mero.", vbCritical + vbOKOnly
            End If
            xEstado = "PLANIF"
            If sTipoPet = "ESP" And glLOGIN_Direccion <> "MEDIO" Then
                xEstado = IIf(CodigoCombo(cboGestion, True) = "BPE", "REFBPE", "COMITE")
            End If
            Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", xEstado, Null, Null)
            If InStr(1, "CSEC|CGRU|ANAL", PerfilInternoActual, vbTextCompare) > 0 Then
                Call sp_InsertPeticionSector(glNumeroPeticion, xPerfSec, Null, Null, Null, Null, 0, xEstado, date, "", 0, "0")
                Call sp_InsertPeticionGrupo(glNumeroPeticion, xPerfGru, Null, Null, Null, Null, Null, Null, 0, 0, xEstado, date, "", 0, "0")
                ' Agrego al historial el agregado de grupo automatico agarrado del solicitante
                Call sp_AddHistorial(glNumeroPeticion, "GNEW000", xEstado, xPerfSec, xEstado, xPerfGru, xEstado, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Alta de Petici�n �")
                Select Case sTipoPet
                    Case "PRO"
                        Call sp_DoMensaje("SEC", "PNEW003", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                        Call sp_DoMensaje("PET", "PNEW003", glNumeroPeticion, "", "NULL", "", "")
                    Case "ESP"
                        Call sp_DoMensaje("SEC", "PNEW002", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                        Call sp_DoMensaje("PET", "PNEW002", glNumeroPeticion, "", "NULL", "", "")
                    Case "PRJ"
                        Call sp_DoMensaje("SEC", "PNEW004", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                        Call sp_DoMensaje("PET", "PNEW004", glNumeroPeticion, "", "NULL", "", "")
                End Select
            End If
        End If
        
        ' Homologaci�n
        If glModoPeticion = "ALTA" And sPetClase <> "SINC" Then
            If bExisteHomologacionEnPeticionActual Then
                If sp_GetGrupoHomologable(xPerfGru) Then                                ' 2. Determina si el grupo a agregar a la petici�n es Homologable
                    Call AgregarHomologacion(glNumeroPeticion, sPetClase, xEstado, glHomologacionSector, glHomologacionGrupo)
                End If
            End If
        Else
            If InStr(1, "APROBA|SUSPEN|PLANOK|PLANIF|PLANRK|EJECRK|ESTIOK|ESTIRK|ESTIMA|EJECUC|", sEstado, vbTextCompare) > 0 Then
                If sPetClase <> "SINC" Then
                    If Not bExisteHomologacionEnPeticionActual Then
                        If bExistenGruposHomologablesEnPeticionActual Then
                            Call AgregarHomologacion(glNumeroPeticion, sPetClase, sEstado, glHomologacionSector, glHomologacionGrupo)
                        End If
                    End If
                Else    ' Solo si hay cambio de clase y homologaci�n no esta activo
                    If bExistenGruposHomologablesEnPeticionActual Then
                        If InStr(1, "ATEN|SPUF|CORR|", sClasePet, vbTextCompare) > 0 And InStr(1, "NUEV|EVOL|OPTI|", sPetClase, vbTextCompare) > 0 Then
                            If Not bHomologacionEnEstadoActivo Then
                                Call ReactivarHomologacion(glNumeroPeticion, sPetClase, sEstado, glHomologacionSector, glHomologacionGrupo)
                            End If
                        End If
                    End If
                End If
            End If
        End If
        ' Guarda la parte de validaci�n t�cnica (Arquitectura y Seg. Inf.)
        If InStr(1, "MODI|", glModoPeticion, vbTextCompare) > 0 Then
            If InStr(1, "NUEV|EVOL|", sPetClase, vbTextCompare) > 0 Then
                If InStr(1, "SADM|ADMI|BPAR|", glUsrPerfilActual, vbTextCompare) > 0 Then
                    Call GuardarValidacionTecnica(grdArquitectura)
                    Call GuardarValidacionTecnica(grdSeguridadInformatica)
                    Call GuardarValidacionTecnica(grdData) 'GMT01
                End If
            End If
        End If
        
        ' add -113- g. TODO: revisar esto!
        If Hab_beneficio Then 'GMT05
            If InStr(1, "NOR|ESP|PRJ|", CodigoCombo(cboTipopet, True), vbTextCompare) > 0 Then
                If InStr(1, "ALTA|MODI|", glModoPeticion, vbTextCompare) > 0 Then
                    If InStr(1, "SADM|ADMI|SOLI|REFE|AUTO|SUPE|GBPE|", glUsrPerfilActual, vbTextCompare) > 0 Then
                        If chkCargarBeneficios.value = 1 Then
                            Call sp_DeletePeticionBeneficios(glNumeroPeticion, Null, Null)
                            
                            If Hab_Kpi Then 'GMT05    ' VER
                                Call sp_DeletePeticionKPI(glNumeroPeticion, Null)
                            End If 'GMT05
                            
                            Call sp_UpdatePetField(glNumeroPeticion, "VALOR_IMPACTO", Null, Null, 0)
                            Call sp_UpdatePetField(glNumeroPeticion, "VALOR_FACILIDAD", Null, Null, 0)
                            Call sp_UpdatePetField2(glNumeroPeticion, "PUNTAJE", Null, Null, 0)
                        Else
                            Call GuardarBeneficios
                            
                            If Hab_Kpi Then 'GMT05
                               Call GuardarKPI
                            End If
                        End If
                    End If
                End If
            End If
        End If 'GMT05

        Me.Tag = "REFRESH"
        Call touchForms
        If glModoPeticion = "ALTA" Then
            glModoPeticion = "VIEW"
            Call InicializarPantalla(False)
            DoEvents
        Else
            glModoPeticion = "VIEW"
            Call ControlesPasajeProduccion(sPetClase, sEstado, bEstuvoEnEjecucion)
            Call InicializarPantalla(False)
            DoEvents
        End If
    End If
    Call CargarEstado
    Call LockProceso(False)
    lblCustomToolTip.Caption = ""   ' add -037- a.
End Sub

Private Sub InicializarTipoPeticion()
    Dim bTipoSeleccion As Boolean           ' True: selectivo / False: todos los tipos
    
    With cboTipopet
        .Enabled = False
        .Clear
        Select Case glModoPeticion
            Case "ALTA", "MODI"
                bTipoSeleccion = True
                Select Case PerfilInternoActual
                    Case "SADM", "ADMI"
                        .AddItem PETICION_TEXTO_TIPONOR & ESPACIOS & "||" & PETICION_TIPO_NORMAL
                        .AddItem PETICION_TEXTO_TIPOPRO & ESPACIOS & "||" & PETICION_TIPO_PROPIA
                        .AddItem PETICION_TEXTO_TIPOESP & ESPACIOS & "||" & PETICION_TIPO_ESPECIAL
                        .AddItem PETICION_TEXTO_TIPOPRJ & ESPACIOS & "||" & PETICION_TIPO_PROYECTO
                        .AddItem PETICION_TEXTO_TIPOAUI & ESPACIOS & "||" & PETICION_TIPO_AUDITORIA_INTERNA
                        .AddItem PETICION_TEXTO_TIPOAUX & ESPACIOS & "||" & PETICION_TIPO_AUDITORIA_EXTERNA
                    Case "SOLI"
                        .AddItem PETICION_TEXTO_TIPONOR & ESPACIOS & "||" & PETICION_TIPO_NORMAL
                    Case "CGRU", "CSEC"
                        If glLOGIN_Direccion <> "MEDIO" Then
                            .AddItem PETICION_TEXTO_TIPONOR & ESPACIOS & "||" & PETICION_TIPO_NORMAL
                        Else
                            .AddItem PETICION_TEXTO_TIPOPRO & ESPACIOS & "||" & PETICION_TIPO_PROPIA
                            If grupoTipoAlta = "2" Then
                                .AddItem PETICION_TEXTO_TIPOESP & ESPACIOS & "||" & PETICION_TIPO_ESPECIAL
                            End If
                        End If
                    Case "BPAR"
                        .AddItem PETICION_TEXTO_TIPONOR & ESPACIOS & "||" & PETICION_TIPO_NORMAL
                        .AddItem PETICION_TEXTO_TIPOPRJ & ESPACIOS & "||" & PETICION_TIPO_PROYECTO
                        .AddItem PETICION_TEXTO_TIPOESP & ESPACIOS & "||" & PETICION_TIPO_ESPECIAL
                    Case "GBPE"
                        .AddItem PETICION_TEXTO_TIPONOR & ESPACIOS & "||" & PETICION_TIPO_NORMAL
                        .AddItem PETICION_TEXTO_TIPOPRJ & ESPACIOS & "||" & PETICION_TIPO_PROYECTO
                        .AddItem PETICION_TEXTO_TIPOESP & ESPACIOS & "||" & PETICION_TIPO_ESPECIAL
                        .AddItem PETICION_TEXTO_TIPOAUI & ESPACIOS & "||" & PETICION_TIPO_AUDITORIA_INTERNA
                        .AddItem PETICION_TEXTO_TIPOAUX & ESPACIOS & "||" & PETICION_TIPO_AUDITORIA_EXTERNA
                    Case "ANAL"
                        .AddItem PETICION_TEXTO_TIPOPRO & ESPACIOS & "||" & PETICION_TIPO_PROPIA
                        If grupoTipoAlta = "2" Then
                            .AddItem PETICION_TEXTO_TIPOESP & ESPACIOS & "||" & PETICION_TIPO_ESPECIAL
                        End If
                End Select
                If glModoPeticion = "ALTA" Then
                    .ListIndex = 0
                Else
                    .ListIndex = PosicionCombo(cboTipopet, sTipoPet, True)
                    ' Caso que la tipolog�a actual ya no es opci�n para el perfil actual,
                    ' entonces cargo la primera que si tenga habilitada.
                    If .ListIndex = -1 Then
                        If .ListCount > 0 Then .ListIndex = 0
                    End If
                End If
            Case Else           ' entonces es modo VIEW
                bTipoSeleccion = False
                .AddItem PETICION_TEXTO_TIPONOR & ESPACIOS & "||" & PETICION_TIPO_NORMAL
                .AddItem PETICION_TEXTO_TIPOPRO & ESPACIOS & "||" & PETICION_TIPO_PROPIA
                .AddItem PETICION_TEXTO_TIPOPRJ & ESPACIOS & "||" & PETICION_TIPO_PROYECTO
                .AddItem PETICION_TEXTO_TIPOESP & ESPACIOS & "||" & PETICION_TIPO_ESPECIAL
                .AddItem PETICION_TEXTO_TIPOAUI & ESPACIOS & "||" & PETICION_TIPO_AUDITORIA_INTERNA
                .AddItem PETICION_TEXTO_TIPOAUX & ESPACIOS & "||" & PETICION_TIPO_AUDITORIA_EXTERNA
                .ListIndex = PosicionCombo(cboTipopet, sTipoPet, True)
        End Select
    End With
    If bDebuggear Then
        Dim i As Integer
        Dim s As String
        Call doLog("InicializarTipoPeticion(" & IIf(bTipoSeleccion, "SELECTIVO", "TODOS_LOS_TIPOS") & ")")
        With cboTipopet
            For i = 0 To .ListCount - 1
                s = s & IIf(i = 0, "", ", ") & Right(.List(i), 3)
            Next i
            Call doLog(vbTab & vbTab & "Tipo: " & s)
        End With
    End If
    Call InicializarClasePeticion
End Sub

Private Sub InicializarClasePeticion()
    Dim bTipoSeleccion As Boolean           ' True: selectivo / False: todos los tipos
    'If bDebuggear Then Call Debuggear(Me, "InicializarClasePeticion()")
    With cboClase
        .Enabled = False
        .Clear
        If InStr(1, "ALTA|MODI|", glModoPeticion, vbTextCompare) > 0 Then
            bTipoSeleccion = True
            Select Case CodigoCombo(cboTipopet, True)
                Case PETICION_TIPO_NORMAL, PETICION_TIPO_ESPECIAL
                    Call setHabilCtrl(cboSector, DISABLE)
                    Call setHabilCtrl(cboSolicitante, DISABLE)
                    If glUsrPerfilActual = "SOLI" Then
                        .AddItem PETICION_TEXTO_CLASESINC & ESPACIOS & "||" & PETICION_CLASE_SINCLASE, 0
                    Else
                        If InStr(1, "CSEC|CGRU|ANAL|", glUsrPerfilActual, vbTextCompare) > 0 Then
                            .AddItem PETICION_TEXTO_CLASEATEN & ESPACIOS & "||" & PETICION_CLASE_ATENCION_USUARIO
                            vClasePeticionDescripcion(0) = HELP_CLASE_ATEN
                        Else
                            .AddItem PETICION_TEXTO_CLASENUEV & ESPACIOS & "||" & PETICION_CLASE_NUEVO_DESARROLLO: vClasePeticionDescripcion(0) = HELP_CLASE_NUEV
                            .AddItem PETICION_TEXTO_CLASEEVOL & ESPACIOS & "||" & PETICION_CLASE_EVOLUTIVO: vClasePeticionDescripcion(1) = HELP_CLASE_EVOL
                            .AddItem PETICION_TEXTO_CLASEATEN & ESPACIOS & "||" & PETICION_CLASE_ATENCION_USUARIO: vClasePeticionDescripcion(2) = HELP_CLASE_ATEN
                        End If
                    End If
                    If CodigoCombo(cboTipopet, True) = PETICION_TIPO_ESPECIAL And CodigoCombo(cboGestion, True) = "DESA" Then
                        Call setHabilCtrl(cboSector, "NOR")
                        Call setHabilCtrl(cboSolicitante, "NOR")
                        Call CargaCombosConTodo
                    Else
                        Call setHabilCtrl(cboSector, DISABLE)
                        Call setHabilCtrl(cboSolicitante, DISABLE)
                        Call setHabilCtrl(cboReferente, DISABLE)
                        Call setHabilCtrl(cboSupervisor, DISABLE)
                        Call setHabilCtrl(cboDirector, DISABLE)
                    End If
                Case PETICION_TIPO_PROPIA
                    If glUsrPerfilActual = "ANAL" Then
                        .AddItem PETICION_TEXTO_CLASESINC & ESPACIOS & "||" & PETICION_CLASE_SINCLASE, 0
                        vClasePeticionDescripcion(0) = HELP_CLASE_SINC
                    Else
                        .AddItem PETICION_TEXTO_CLASEOPTI & ESPACIOS & "||" & PETICION_CLASE_OPTIMIZACION: vClasePeticionDescripcion(0) = HELP_CLASE_OPTI
                        .AddItem PETICION_TEXTO_CLASECORR & ESPACIOS & "||" & PETICION_CLASE_CORRECTIVO: vClasePeticionDescripcion(1) = HELP_CLASE_CORR
                        .AddItem PETICION_TEXTO_CLASESPUF & ESPACIOS & "||" & PETICION_CLASE_SPUFI: vClasePeticionDescripcion(2) = HELP_CLASE_SPUF
                    End If
                Case PETICION_TIPO_PROYECTO
                    Call setHabilCtrl(cboSector, DISABLE)
                    Call setHabilCtrl(cboSolicitante, DISABLE)
                    .AddItem PETICION_TEXTO_CLASENUEV & ESPACIOS & "||" & PETICION_CLASE_NUEVO_DESARROLLO: vClasePeticionDescripcion(0) = HELP_CLASE_NUEV
                    .AddItem PETICION_TEXTO_CLASEEVOL & ESPACIOS & "||" & PETICION_CLASE_EVOLUTIVO: vClasePeticionDescripcion(1) = HELP_CLASE_EVOL
                Case PETICION_TIPO_AUDITORIA_INTERNA, PETICION_TIPO_AUDITORIA_EXTERNA
                    .AddItem PETICION_TEXTO_CLASEEVOL & ESPACIOS & "||" & PETICION_CLASE_EVOLUTIVO: vClasePeticionDescripcion(0) = HELP_CLASE_EVOL
                    .AddItem PETICION_TEXTO_CLASEATEN & ESPACIOS & "||" & PETICION_CLASE_ATENCION_USUARIO: vClasePeticionDescripcion(1) = HELP_CLASE_ATEN
                    .AddItem PETICION_TEXTO_CLASECORR & ESPACIOS & "||" & PETICION_CLASE_CORRECTIVO: vClasePeticionDescripcion(2) = HELP_CLASE_CORR
            End Select
            ' Si el que da el alta o edita es el administrador, se agrega la opci�n de dejar sin clase a la petici�n.
            If Not .Enabled Then .Enabled = True
            If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then .AddItem PETICION_TEXTO_CLASESINC & ESPACIOS & "||" & PETICION_CLASE_SINCLASE, 0
            .ListIndex = IIf(glModoPeticion = "ALTA", 0, PosicionCombo(cboClase, sClasePet, True))
            If .ListIndex = -1 Then
                If .Enabled Then
                    If .ListCount > 0 Then .ListIndex = 0
                End If
            End If
            Call setHabilCtrl(cboClase, IIf(.ListCount = 1, "DIS", "NOR"))
            ' Seg�n sea la clase que haya quedado seleccionada, inicializo las opciones
            ' de impacto, RO y regulatorio, que dependen de la clase de petici�n
            Call SeleccionaClasePeticion            ' TODO: REVISAR SI ESTO DEBE IR ACA
        Else
            bTipoSeleccion = False
            .AddItem PETICION_TEXTO_CLASESINC & ESPACIOS & "||" & PETICION_CLASE_SINCLASE: vClasePeticionDescripcion(0) = HELP_CLASE_SINC
            .AddItem PETICION_TEXTO_CLASECORR & ESPACIOS & "||" & PETICION_CLASE_CORRECTIVO: vClasePeticionDescripcion(1) = HELP_CLASE_CORR
            .AddItem PETICION_TEXTO_CLASEATEN & ESPACIOS & "||" & PETICION_CLASE_ATENCION_USUARIO: vClasePeticionDescripcion(2) = HELP_CLASE_ATEN
            .AddItem PETICION_TEXTO_CLASEOPTI & ESPACIOS & "||" & PETICION_CLASE_OPTIMIZACION: vClasePeticionDescripcion(3) = HELP_CLASE_OPTI
            .AddItem PETICION_TEXTO_CLASEEVOL & ESPACIOS & "||" & PETICION_CLASE_EVOLUTIVO: vClasePeticionDescripcion(4) = HELP_CLASE_EVOL
            .AddItem PETICION_TEXTO_CLASENUEV & ESPACIOS & "||" & PETICION_CLASE_NUEVO_DESARROLLO: vClasePeticionDescripcion(5) = HELP_CLASE_NUEV
            .AddItem PETICION_TEXTO_CLASESPUF & ESPACIOS & "||" & PETICION_CLASE_SPUFI: vClasePeticionDescripcion(6) = HELP_CLASE_SPUF
            .ListIndex = PosicionCombo(cboClase, sClasePet, True)
        End If
    End With
    If bDebuggear Then
        Dim i As Integer
        Dim s As String
        Call doLog("InicializarClasePeticion(" & IIf(bTipoSeleccion, "SELECTIVO", "TODAS_LAS_CLASES") & ")")
        With cboClase
            For i = 0 To .ListCount - 1
                s = s & IIf(i = 0, "", ", ") & Right(.List(i), 4)
            Next i
            Call doLog(vbTab & vbTab & "Clase: " & s)
        End With
    End If
End Sub

Public Sub InicializarPantalla(Optional bCompleto)
    If bDebuggear Then
        Call Debuggear(Me, "InicializarPantalla(" & bCompleto & ")")
        Call doLog("InicializarPantalla(" & bCompleto & ")")
    End If
    If IsMissing(bCompleto) Then
        bCompleto = True
    End If
    
    sbMainPeticion.Font.Bold = False
    
    Me.Tag = ""
    flgEnCarga = True
    Call LockProceso(True)
    sSoli = ""
    sRefe = ""
    sDire = ""
    sSupe = ""
    sUsualta = glLOGIN_ID_REEMPLAZO
    xUsrLoginActual = glLOGIN_ID_REEMPLAZO
    xUsrPerfilActual = PerfilInternoActual
    
    Call setHabilCtrl(cmdModificar, "DIS")
    Call setHabilCtrl(cmdOk, "DIS")
    Call setHabilCtrl(cmdPrint, "DIS")
    Call setHabilCtrl(cmdHistView, "DIS")
    Call setHabilCtrl(cmdRptHs, "DIS")
    Call setHabilCtrl(cmdAgrupar, "DIS")
    Call setHabilCtrl(cmdCambioEstado, "DIS")
    Call setHabilCtrl(cmdSector, "DIS")
    Call setHabilCtrl(cmdGrupos, "DIS")
    Select Case glModoPeticion
        Case "ALTA"
            orjBtn(orjANX).visible = False
            orjBtn(orjADJ).visible = False
            If Hab_PesVinculado Then        'GMT05
               orjBtn(orjDOC).visible = False
            End If  'GMT05
            orjBtn(orjCNF).visible = False
            orjBtn(orjEST).visible = False  ' add -064- a.
            orjBtn(orjHOM).visible = False  ' add -109- a.
            txtNrointerno.text = ""
            txtFechaPedido.text = Format$(date, "ddmmyyyy")
            txtFechaPedido.ForceFormat
            If PerfilInternoActual = "SADM" Then
                txtFechaComite.text = Format(date, "dd/mm/yyyy")
                txtFechaComite.ForceFormat
            Else
                sSoli = glLOGIN_ID_REEMPLAZO                    ' Ver para que es esto!!!
            End If
            Call InicializarTipoPeticion                ' add -113- i.
            Call InicializarCombos
            Call HabilitarEdicionTextoFuentes(False)
            Call HabilitarEdicionTexto(True)
        Case Else
            orjBtn(orjANX).visible = True
            orjBtn(orjADJ).visible = True
            If Hab_PesVinculado Then  'GMT05
                orjBtn(orjDOC).visible = True
            End If 'GMT05
            orjBtn(orjCNF).visible = True
            orjBtn(orjEST).visible = IIf(InStr(1, "MEDIO|", glLOGIN_Direccion, vbTextCompare) > 0 Or InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) > 0, True, False) ' add -064- c.
            orjBtn(orjHOM).visible = True
            Call CargarPeticion(bCompleto)
            Me.Caption = cTAG & IIf(Val(txtNroAsignado) > 0, Format(txtNroAsignado, "#########"), "S/N")
    End Select
    
    If glModoPeticion <> "ALTA" Then
        Call CargarValidacionTecnica(grdArquitectura, VALIDACION_ARQUITECTURA_TITULO, VALIDACION_ARQUITECTURA)
        Call CargarValidacionTecnica(grdSeguridadInformatica, VALIDACION_SEGURIDADINFORMATICA_TITULO, VALIDACION_SEGURIDADINFORMATICA)
        Call CargarValidacionTecnica(grdData, VALIDACION_DATA_TITULO, VALIDACION_DATA) 'GMT01
    End If
    
    If bCompleto Then
        If Hab_PesVinculado Then  'GMT05
           Call CargarVinculados
        End If  'GMT05
        Call CargarAdjPet
        Call CargarConformes
        Call CargarEstado
        Call CargarHomologacion
    End If
    Call HabilitarBotones
    
    bFlgObservaciones = False
    
    If Hab_beneficio Then 'GMT05
        bFlgBeneficios = False
    End If 'GMT05
    
    bFlgCaracteristicas = False
    bFlgMotivos = False
    bFlgRelaciones = False
    bFlgDescripcion = False
    On Error Resume Next
    Call orjBtn_Click(orjMEM)
    DoEvents
    Call orjBtn_Click(orjDAT)
    DoEvents
    Call Puntero(False)
    txtTitulo.SetFocus
    DoEvents
    Call LockProceso(False)
    flgEnCarga = False
End Sub

Private Sub HabilitarBotones()
    Dim COLOR_ACTIVA As Long
    Dim COLOR_TERMINAL As Long

    COLOR_ACTIVA = RGB(208, 234, 218)
    COLOR_TERMINAL = RGB(252, 209, 216)

    If bDebuggear Then
        Call doLog("HabilitarBotones(" & glModoPeticion & ")")
        Call doLog(vbTab & vbTab & "flgIngerencia       = " & flgIngerencia)
        Call doLog(vbTab & vbTab & "flgSolicitor        = " & flgSolicitor)
        Call doLog(vbTab & vbTab & "flgSolicitorVinc    = " & flgSolicitorVinc)
    End If

    flgEnCarga = True
    bValidacionTecnicaHabilitadoParaEditar = False
    
    Call setHabilCtrl(txtSgiIncidencia, "DIS")
    Call setHabilCtrl(cboGestion, "DIS")
    Call setHabilCtrl(txtValorImpacto, "DIS")
    Call setHabilCtrl(txtValorFacilidad, "DIS")
    Call setHabilCtrl(cboEmp, "DIS")
    Call setHabilCtrl(cboTipopet, "DIS")
    Call setHabilCtrl(cboClase, "DIS")
    Call setHabilCtrl(txtPetPrioridad, DISABLE)
    Call setHabilCtrl(cboImpacto, "DIS")
    Call setHabilCtrl(cboRegulatorio, "DIS")
    Call HabilitarProyectos(False)
    Call setHabilCtrl(cmdModificar, "DIS")
    Call setHabilCtrl(cmdOk, "DIS")
    Call setHabilCtrl(cmdPrint, "DIS")
    Call setHabilCtrl(cmdHistView, "DIS")
    Call setHabilCtrl(cmdRptHs, "DIS")
    Call setHabilCtrl(cmdCambioEstado, "DIS")
    Call setHabilCtrl(cmdSector, "DIS")
    Call setHabilCtrl(cmdGrupos, "DIS")
    Call setHabilCtrl(cmdCancel, "DIS")
    Call setHabilCtrl(cmdAgrupar, "DIS")
    Call setHabilCtrl(txtNrointerno, "DIS")
    Call setHabilCtrl(txtTitulo, "DIS")
    Call setHabilCtrl(txtFechaPedido, "DIS")
    Call setHabilCtrl(txtFechaComite, "DIS")
    Call setHabilCtrl(txtFechaRefBPE, "DIS")
    Call setHabilCtrl(txtFinireal, "DIS")
    Call setHabilCtrl(txtFiniplan, "DIS")
    Call setHabilCtrl(txtFfinreal, "DIS")
    Call setHabilCtrl(txtFfinplan, "DIS")
    Call setHabilCtrl(txtHspresup, "DIS")
    Call setHabilCtrl(txtHsPlanif, "DIS")
    Call setHabilCtrl(txtCategoria, "DIS")
    Call setHabilCtrl(txtPuntuacion, "DIS")
    Call setHabilCtrl(cboCorpLocal, "DIS")
    Call setHabilCtrl(cboOrientacion, "DIS")
    Call setHabilCtrl(cboImportancia, "DIS")
    Call setHabilCtrl(cboBpar, "DIS")
    Call setHabilCtrl(cboReferenteBPE, "DIS")
    Call setHabilCtrl(cboSector, "DIS")
    Call setHabilCtrl(cboSolicitante, "DIS")
    Call setHabilCtrl(cboReferente, "DIS")
    Call setHabilCtrl(cboSupervisor, "DIS")
    Call setHabilCtrl(cboDirector, "DIS")
    Call setHabilCtrl(cboRO, "DIS")
    Call setHabilCtrl(memCaracteristicas, "DIS")
    Call setHabilCtrl(memDescripcion, "DIS")
    Call setHabilCtrl(memMotivos, "DIS")
    Call setHabilCtrl(memObservaciones, "DIS")
    Call setHabilCtrl(memRelaciones, "DIS")
    Call setHabilCtrl(txtEstado, "DIS")
    Call setHabilCtrl(txtSituacion, "DIS")
    Call setHabilCtrl(txtFechaEstado, "DIS")
    Call setHabilCtrl(txtNroAnexada, "DIS")
    Call setHabilCtrl(txtNroAsignado, "DIS")
    Call setHabilCtrl(txtPrjNom, "DIS")
    
    If Hab_beneficio Then 'GMT05
        Call setHabilCtrl(chkCargarBeneficios, "DIS")
        chkPeticionDisclaimer.visible = False
        bHabilitadoModificarBeneficios = False
    End If 'GMT05
       
    If Hab_Kpi Then 'GMT05
        bHabilitadoModificarKPI = False
        Call HabilitarBotonesKPI(False)
        Call setHabilCtrl(cboKPIIndicador, "DIS")
        Call setHabilCtrl(txtKPIDescripcion, "DIS")
        Call setHabilCtrl(cboKPIUniMed, "DIS")
        Call setHabilCtrl(txtKPIValorEsperado, "DIS")
        Call setHabilCtrl(txtKPIValorPartida, "DIS")
        Call setHabilCtrl(txtKPITiempoParaEsperado, "DIS")
        Call setHabilCtrl(cboKPIUniMedTiempo, "DIS")
    End If 'GMT05
    sstDetalles.Tab = TAB_DETALLES
    
    Select Case glModoPeticion
        Case "ALTA"
            Call HabilitarBotonesHomologacion(MODO_VIEW)
            orjBtn(orjANX).visible = False
            orjBtn(orjADJ).visible = False
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).visible = False
            End If 'GMT05
            orjBtn(orjCNF).visible = False
            orjBtn(orjEST).visible = False
            orjBtn(orjHOM).visible = False
            glNroAsignado = ""
            lblModo.Caption = glModoPeticion
            Call setHabilCtrl(cmdOk, NORMAL)
            Call setHabilCtrl(cmdCancel, "NOR")
            cmdCancel.Caption = "Cancelar"
            Select Case PerfilInternoActual
                Case "SOLI"
                    sEstado = "CONFEC"
                    sSituacion = ""
                    Call setHabilCtrl(txtTitulo, NORMAL)
                    Call setHabilCtrl(cboEmp, NORMAL)                                    ' add -095- a.
                    Call setHabilCtrl(cboOrientacion, NORMAL)                            ' add -113- h.
                    If InStr(1, "DIRE|GERE|", getPerfNivel(PerfilInternoActual), vbTextCompare) > 0 Then Call setHabilCtrl(cboSector, "NOR")
                    Call setHabilCtrl(memCaracteristicas, NORMAL)
                    Call setHabilCtrl(memDescripcion, NORMAL)
                    Call setHabilCtrl(memMotivos, NORMAL)
                    Call setHabilCtrl(memObservaciones, NORMAL)
                    Call setHabilCtrl(memRelaciones, NORMAL)
                    SectorSolicitante = glLOGIN_Sector
                    
                    If Hab_beneficio Then 'GMT05
                        Call CargarGrillaBeneficios
                    End If 'GMT05
                    
                    If Hab_Kpi Then 'GMT05
                        Call CargarGrillaKPI
                    End If 'GMT05
                    
                    If sQuienGestiona = "BPE" Then
                        cboGestion.ListIndex = PosicionCombo(cboGestion, "BPE", True)
                        
                        If Hab_beneficio Then 'GMT05
                            Call setHabilCtrl(chkCargarBeneficios, "NOR")
                            chkPeticionDisclaimer.visible = True
                            bHabilitadoModificarBeneficios = True
                        End If 'GMT05
                            
                        If Hab_Kpi Then 'GMT05
                            bHabilitadoModificarKPI = True
                            Call HabilitarBotonesKPI(True)
                        End If 'GMT05
                    Else
                        cboGestion.ListIndex = PosicionCombo(cboGestion, "DESA", True)
          
                        If Hab_beneficio Then 'GMT05
                            chkPeticionDisclaimer.visible = False
                            bHabilitadoModificarBeneficios = False
                        End If 'GMT05
                        
                        If Hab_Kpi Then 'GMT05
                            bHabilitadoModificarKPI = False
                            Call HabilitarBotonesKPI(False)
                        End If 'GMT05
                    End If
                Case "CSEC", "CGRU"
                    sEstado = "PLANIF"
                    sSituacion = ""
                    txtFechaComite.DateValue = Now: txtFechaComite.text = txtFechaComite.DateValue
                    Call setHabilCtrl(txtTitulo, "NOR")
                    If cboTipopet.ListCount > 1 Then Call setHabilCtrl(cboTipopet, "NOR")
                    Call setHabilCtrl(cboClase, "NOR")
                    Call setHabilCtrl(cboEmp, "NOR")
                    Call setHabilCtrl(cboRegulatorio, "NOR")
                    If CodigoCombo(cboTipopet, True) = "PRO" Then
                        cboBpar.ListIndex = -1
                        txtFechaComite = ""
                        txtFechaComite.DateValue = ""
                        Call setHabilCtrl(cboBpar, DISABLE)
                    Else
                        Call setHabilCtrl(cboBpar, NORMAL)
                    End If
                    Call setHabilCtrl(cboOrientacion, "NOR")
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    SectorSolicitante = glLOGIN_Sector
                    
                    If Hab_beneficio Then 'GMT05
                        Call CargarGrillaBeneficios
                    End If 'GMT05
                    
                    If Hab_Kpi Then 'GMT05
                        Call CargarGrillaKPI
                    End If 'GMT05
                    
                    cboGestion.ListIndex = PosicionCombo(cboGestion, "DESA", True)
                    
                    If Hab_beneficio Then 'GMT05
                        chkCargarBeneficios.visible = False: chkCargarBeneficios.value = 1
                        chkPeticionDisclaimer.visible = False: chkPeticionDisclaimer.value = 1
                        bHabilitadoModificarBeneficios = False
                    End If 'GMT05
                    

                    If Hab_Kpi Then 'GMT05
                        bHabilitadoModificarKPI = False
                        Call HabilitarBotonesKPI(False)
                    End If ' GMT05
                Case "ANAL"
                    sEstado = "PLANIF"
                    sSituacion = ""
                    txtFechaComite.DateValue = Now: txtFechaComite.text = txtFechaComite.DateValue
                    If cboTipopet.ListCount > 1 Then Call setHabilCtrl(cboTipopet, "NOR")
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(cboEmp, "NOR")
                    Call setHabilCtrl(cboOrientacion, "NOR")
                    If CodigoCombo(cboTipopet, True) = "PRO" Then
                        cboBpar.ListIndex = -1
                        txtFechaComite = "": txtFechaComite.DateValue = ""
                        Call setHabilCtrl(cboBpar, DISABLE)
                    End If
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    SectorSolicitante = glLOGIN_Sector
                    
                    If Hab_beneficio Then 'GMT05
                        Call CargarGrillaBeneficios
                    End If 'GMT05
                    
                    If Hab_Kpi Then 'GMT05
                        Call CargarGrillaKPI
                    End If 'GMT05
                    
                    cboGestion.ListIndex = PosicionCombo(cboGestion, "DESA", True)
                    
                    If Hab_beneficio Then 'GMT05
                        chkCargarBeneficios.visible = False: chkCargarBeneficios.value = 1
                        chkPeticionDisclaimer.visible = False: chkPeticionDisclaimer.value = 1
                        bHabilitadoModificarBeneficios = False
                    End If 'GMT05
                    
                    If Hab_Kpi Then 'GMT05
                        bHabilitadoModificarKPI = False
                        Call HabilitarBotonesKPI(False)
                    End If 'GMT05
            End Select
        Case "MODI"
            orjBtn(orjANX).visible = IIf(InStr(1, "BPAR|SADM|", PerfilInternoActual, vbTextCompare) > 0, True, False)
            orjBtn(orjADJ).visible = False
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).visible = False
            End If 'GMT05
            orjBtn(orjCNF).visible = False
            orjBtn(orjEST).visible = False
            orjBtn(orjHOM).visible = False
            Call orjBtn_Click(orjDAT)
            lblModo.Caption = "MODIFICA"
            Call setHabilCtrl(cmdOk, "NOR")
            Call setHabilCtrl(cmdCancel, "NOR")
            cmdCancel.Caption = "Cancelar"
            Select Case PerfilInternoActual
                Case "SOLI"
                    If InStr(1, "DIRE|GERE|", getPerfNivel(PerfilInternoActual), vbTextCompare) > 0 Then
                        Call CargaSectoresPosibles
                        Call setHabilCtrl(cboSector, "NOR")
                    End If
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(cboOrientacion, NORMAL)
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    If bFlgExtension = False Then
                        If sEstado = "CONFEC" Then
                            txtTitulo.SetFocus
                        End If
                    Else
                        Call orjBtn_Click(orjMEM)
                        DoEvents
                        memDescripcion.SetFocus
                    End If
                    SectorSolicitante = glLOGIN_Sector
                    
                    If Hab_beneficio Then 'GMT05
                        bHabilitadoModificarBeneficios = False
                    End If 'GMT05
                    
                    If Hab_Kpi Then 'GMT05
                        bHabilitadoModificarKPI = False
                    End If 'GMT05
                    
                    If Hab_beneficio Then 'GMT05
                        If CodigoCombo(cboGestion, True) = "BPE" Then
                            Call setHabilCtrl(chkCargarBeneficios, "NOR")
                            If InStr(1, "CONFEC|DEVSOL|", sEstado, vbTextCompare) > 0 Then
                                bHabilitadoModificarBeneficios = True
                                
                                If Hab_Kpi Then 'GMT05
                                    bHabilitadoModificarKPI = True
                                    Call HabilitarBotonesKPI(True)
                                End If 'GMT05
                                If sEstado = "DEVSOL" Then Call setHabilCtrl(cboEmp, DISABLE)       ' TODO: revisar esto 22.11.16
                            'Else 'GMT05
                             '   Call HabilitarBotonesKPI(False) -> GMT05
                            End If
                        Else
                            Call setHabilCtrl(chkCargarBeneficios, "DIS")
                            If Hab_Kpi Then 'GMT05
                                Call HabilitarBotonesKPI(False)
                            End If ' GMT05
                        End If
                    End If 'GMT05
                    '}
                Case "REFE", "SUPE", "AUTO"
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(cboOrientacion, "NOR")
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    SectorSolicitante = glLOGIN_Sector
                    
                    If Hab_beneficio Then 'GMT05
                        If CodigoCombo(cboGestion, True) = "BPE" Then
                            Call setHabilCtrl(chkCargarBeneficios, "NOR")
                            bHabilitadoModificarBeneficios = True
                            
                            If Hab_Kpi Then 'GMT05
                                bHabilitadoModificarKPI = True
                                Call HabilitarBotonesKPI(True)
                            End If 'GMT05
                        Else
                            bHabilitadoModificarBeneficios = False
                            
                            If Hab_Kpi Then 'GMT05
                                bHabilitadoModificarKPI = False
                                Call HabilitarBotonesKPI(False)
                            End If ' GMT05
                        End If
                    End If 'GMT05
                Case "CSEC", "CGRU"
                    flgEnCarga = False
                    If flgIngerencia Then
                        Call InicializarTipoPeticion
                        Call setHabilCtrl(txtTitulo, "NOR")
                        Call setHabilCtrl(cboEmp, "NOR")
                        If cboTipopet.ListCount > 1 Then Call setHabilCtrl(cboTipopet, "NOR")
                        Call setHabilCtrl(cboBpar, IIf(CodigoCombo(cboTipopet, True) = "PRO", DISABLE, NORMAL))
                        Call setHabilCtrl(cboOrientacion, "NOR")
                        Call setHabilCtrl(memCaracteristicas, "NOR")
                        Call setHabilCtrl(memDescripcion, "NOR")
                        If Not bOrigenIncidencia Then
                            Call setHabilCtrl(memMotivos, "NOR")
                        Else
                            Call setHabilCtrl(txtTitulo, DISABLE)
                            Call setHabilCtrl(cboClase, DISABLE)
                            Call setHabilCtrl(cboRegulatorio, DISABLE)
                            Call setHabilCtrl(memMotivos, DISABLE)
                        End If
                        Call setHabilCtrl(memObservaciones, "NOR")
                        Call setHabilCtrl(memRelaciones, "NOR")
                        
                        If Hab_beneficio Then 'GMT05
                            Call setHabilCtrl(chkCargarBeneficios, "DIS")
                            bHabilitadoModificarBeneficios = False
                        End If 'GMT05
                        
                        If Hab_Kpi Then 'GMT05
                            bHabilitadoModificarKPI = False
                        End If ' GMT05
                        
                        If CodigoCombo(cboClase, True) <> "SINC" Then Call setHabilCtrl(cboEmp, DISABLE)
                        If Not bOrigenIncidencia Then
                            If sEstado = "EJECUC" Then Call setHabilCtrl(cboRegulatorio, "NOR")
                        Else
                            Call setHabilCtrl(cboRegulatorio, DISABLE)
                        End If
                        DoEvents
                    End If
                Case "ANAL"
                    If flgIngerencia Then
                        Call setHabilCtrl(txtTitulo, "NOR")
                        Call setHabilCtrl(cboEmp, "NOR")
                        Call setHabilCtrl(cboOrientacion, "NOR")
                        Call setHabilCtrl(memCaracteristicas, "NOR")
                        Call setHabilCtrl(memDescripcion, "NOR")
                        Call setHabilCtrl(memMotivos, "NOR")
                        Call setHabilCtrl(memObservaciones, "NOR")
                        Call setHabilCtrl(memRelaciones, "NOR")
                        
                        If Hab_beneficio Then 'GMT05
                            Call setHabilCtrl(chkCargarBeneficios, "DIS")
                            bHabilitadoModificarBeneficios = False
                        End If 'GMT05
                        
                        If Hab_Kpi Then 'GMT05
                            bHabilitadoModificarKPI = False
                        End If ' GMT05
                    End If
                Case "CDIR", "CGCI"
                    If CodigoCombo(cboTipopet, True) = "PRJ" Then
                        If modifImportancia(xUsrLoginActual, xUsrPerfilActual, glLOGIN_ID_REEMPLAZO, PerfilInternoActual) Then
                            Call setHabilCtrl(cboImportancia, "NOR")
                        Else
                            Call setHabilCtrl(cboImportancia, "DIS")
                            cboImportancia.ToolTipText = "No puede modificarse, debido a que el perfil que la defini� previamente es superior o igual al actual."
                        End If
                    End If
                Case "BPAR"
                    flgEnCarga = False
                    If CodigoCombo(cboTipopet, True) <> "PRO" Then
                        If CodigoCombo(cboGestion, True) = "DESA" Then
                            Call InicializarTipoPeticion
                            Call setHabilCtrl(cboTipopet, NORMAL)
                            Call setHabilCtrl(cboClase, NORMAL)
                            Call setHabilCtrl(cboRegulatorio, NORMAL)
                            Call setHabilCtrl(cboCorpLocal, NORMAL)
                            Call setHabilCtrl(cboEmp, "DIS")
                            Call setHabilCtrl(cboImportancia, NORMAL)
                            Call setHabilCtrl(cboBpar, NORMAL)
                            Call setHabilCtrl(cboOrientacion, NORMAL)
                            Call setHabilCtrl(memCaracteristicas, "NOR") 'GMT05
                            Call setHabilCtrl(memDescripcion, "NOR") 'GMT05
                            Call setHabilCtrl(memMotivos, "NOR") 'GMT05
                            Call setHabilCtrl(memObservaciones, "NOR") 'GMT05
                            Call setHabilCtrl(memRelaciones, "NOR") 'GMT05
                        Else
                            Call InicializarTipoPeticion
                            ' Truquillo para que el Ref. de RGP pueda dejar sin clasificar la petici�n. Horrible...
                            If sClasePet = "SINC" Then
                                flgEnCarga = True
                                cboClase.AddItem "--" & ESPACIOS & "||SINC", 0
                                cboClase.ListIndex = 0
                                flgEnCarga = False
                            Else
                                Call setHabilCtrl(cboEmp, DISABLE)
                                Call setHabilCtrl(cboTipopet, "DIS")
                                Call setHabilCtrl(cboClase, "DIS")
                                Call setHabilCtrl(cboRegulatorio, "DIS")
                                Call setHabilCtrl(cboImpacto, DISABLE)
                                Call setHabilCtrl(cboCorpLocal, DISABLE)
                                Call setHabilCtrl(cboRO, DISABLE)
                                Call setHabilCtrl(cboBpar, NORMAL)
                            End If
                        End If
                        ' Se deja que los referentes de sistema puedan modificar esto (se contempla para el caso de vinculadas)
                        Call HabilitarProyectos(True)
                        Call HabilitarVisibilidad(bTieneUnProyecto)
                    Else
                        Call setHabilCtrl(cboTipopet, "DIS")
                        Call setHabilCtrl(cboClase, "DIS")
                        Call setHabilCtrl(cboGestion, "DIS")
                        Call setHabilCtrl(cboCorpLocal, "DIS")
                        Call HabilitarProyectos(True)
                        Call HabilitarVisibilidad(bTieneUnProyecto)
                    End If
                Case "GBPE"
                    flgEnCarga = False
                    If CodigoCombo(cboTipopet, True) <> "PRO" Then
                        If InStr(1, "EJECUC|SUSPEN|", sEstado, vbTextCompare) > 0 Or bEstuvoEnEjecucion Then
                            Call setHabilCtrl(cboTipopet, "DIS")
                            Call setHabilCtrl(cboClase, "DIS")
                            Call setHabilCtrl(cboRegulatorio, "DIS")
                            Call setHabilCtrl(cboCorpLocal, "DIS")
                            Call setHabilCtrl(cboEmp, "DIS")
                        Else
                            Call setHabilCtrl(txtTitulo, NORMAL)
                            Call InicializarTipoPeticion
                            Call setHabilCtrl(cboTipopet, NORMAL)
                            Call setHabilCtrl(cboClase, NORMAL)
                            ' Truquillo para que el Ref. de RGP pueda dejar sin clasificar la petici�n. Horrible...
                            If sClasePet = "SINC" Then
                                flgEnCarga = True
                                cboClase.AddItem "--" & ESPACIOS & "||SINC", 0
                                cboClase.ListIndex = 0
                                flgEnCarga = False
                            Else
                                Call setHabilCtrl(cboEmp, DISABLE)
                            End If
                            Call setHabilCtrl(cboCorpLocal, NORMAL)
                            Call setHabilCtrl(memCaracteristicas, NORMAL)
                            Call setHabilCtrl(memDescripcion, NORMAL)
                            Call setHabilCtrl(memMotivos, NORMAL)
                            Call setHabilCtrl(memObservaciones, NORMAL)
                            Call setHabilCtrl(memRelaciones, NORMAL)
                        End If
                        cboCorpLocal.ListIndex = PosicionCombo(cboCorpLocal, "L", True)
                        Call setHabilCtrl(cboGestion, DISABLE)
                        Call HabilitarProyectos(True)
                        Call HabilitarVisibilidad(bTieneUnProyecto)
                    Else
                        Call setHabilCtrl(cboTipopet, "DIS")
                        Call setHabilCtrl(cboClase, "DIS")
                        Call setHabilCtrl(cboGestion, "DIS")
                        Call setHabilCtrl(cboCorpLocal, "DIS")
                        Call HabilitarProyectos(True)
                        Call HabilitarVisibilidad(bTieneUnProyecto)
                    End If
                    If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                        If Not bEstuvoEnEjecucion Then Call setHabilCtrl(cboRO, "NOR")
                    Else    ' No est� clasificada a�n
                        If CodigoCombo(cboClase, True) = "SINC" Then Call setHabilCtrl(cboRO, "NOR")
                    End If
                    Call setHabilCtrl(cboOrientacion, "NOR")
                    If InStr(1, "AUX|AUI|PRO|", CodigoCombo(cboTipopet, True), vbTextCompare) > 0 Then
                        Call setHabilCtrl(cboTipopet, "NOR")
                        Call setHabilCtrl(cboClase, "NOR")
                    End If
                    If sEstado = "EJECUC" Then
                        Call setHabilCtrl(cboReferenteBPE, DISABLE)
                        Call setHabilCtrl(cboBpar, DISABLE)
                    Else
                        Call setHabilCtrl(cboReferenteBPE, NORMAL)
                        Call setHabilCtrl(cboBpar, NORMAL)
                    End If
                    
                    If Hab_beneficio Then 'GMT05
                        ' TODO: revisar esto! que el GBPE pueda modificar este atributo
                        Call setHabilCtrl(chkCargarBeneficios, "NOR")
                        bHabilitadoModificarBeneficios = True
                    End If 'GMT05
                    
                    If Hab_Kpi Then 'GMT05
                        bHabilitadoModificarKPI = True
                        Call HabilitarBotonesKPI(True)
                    End If 'GMT05
                    '}
                Case "SADM"
                    Call InicializarTipoPeticion
                    Call CargaCombosConTodo
                    If xSec <> "" Then Call SetCombo(cboSector, xSec, True)
                    If sSoli <> "" Then Call SetCombo(cboSolicitante, sSoli)
                    If sRefe <> "" Then Call SetCombo(cboReferente, sRefe)
                    If sSupe <> "" Then Call SetCombo(cboSupervisor, sSupe)
                    If sDire <> "" Then Call SetCombo(cboDirector, sDire)
                    Call setHabilCtrl(cboTipopet, "NOR")
                    Call setHabilCtrl(cboImpacto, "NOR")
                    Call setHabilCtrl(cboRegulatorio, "NOR")
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(cboRO, "NOR")
                    Call setHabilCtrl(cboCorpLocal, "NOR")
                    Call setHabilCtrl(cboOrientacion, "NOR")
                    Call setHabilCtrl(cboImportancia, "NOR")
                    Call HabilitarProyectos(True)
                    Call setHabilCtrl(cboGestion, "NOR")
                    
                    If Hab_beneficio Then 'GMT05
                      Call setHabilCtrl(chkCargarBeneficios, "NOR")
                    End If 'GMT05
                    
                    Call setHabilCtrl(txtFechaComite, "NOR")
                    Call setHabilCtrl(txtFinireal, "NOR")
                    Call setHabilCtrl(txtFiniplan, "NOR")
                    Call setHabilCtrl(txtFfinreal, "NOR")
                    Call setHabilCtrl(txtFfinplan, "NOR")
                    Call setHabilCtrl(cboTipopet, "NOR")
                    Call setHabilCtrl(cboSector, "NOR")
                    Call setHabilCtrl(cboBpar, "NOR")
                    Call setHabilCtrl(cboReferenteBPE, "NOR")
                    Call setHabilCtrl(txtFechaRefBPE, "NOR")
                    Call setHabilCtrl(cboSolicitante, "NOR")
                    Call setHabilCtrl(cboReferente, "NOR")
                    Call setHabilCtrl(cboSupervisor, "NOR")
                    Call setHabilCtrl(cboDirector, "NOR")
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    DoEvents
            End Select
        Case "VIEW"
            lblModo.Caption = "VISUALIZAR"
            cmdCancel.Caption = "Cerrar"
            Call setHabilCtrl(cmdCancel, NORMAL)
            Call setHabilCtrl(cmdOk, "DIS")
            Call setHabilCtrl(cmdPrint, "NOR")
            Call setHabilCtrl(cmdHistView, "NOR")
            Call setHabilCtrl(cmdRptHs, "NOR")
            Call setHabilCtrl(cmdAgrupar, "NOR")
            Call setHabilCtrl(cmdSector, "NOR")
            orjBtn(orjANX).visible = True
            orjBtn(orjADJ).visible = True
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).visible = True
            End If 'GMT05
            orjBtn(orjCNF).visible = True
            orjBtn(orjEST).visible = IIf(InStr(1, "MEDIO|", glLOGIN_Direccion, vbTextCompare) > 0 Or InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) > 0, True, False) ' add -064- c.
            orjBtn(orjHOM).visible = True
            Select Case PerfilInternoActual
                Case "SADM"
                    Call setHabilCtrl(cmdCambioEstado, "NOR")
                    Call setHabilCtrl(cmdModificar, "NOR")
                Case "ANAL"
                    If flgIngerencia Then                                   ' TODO: revisar ac� flgIngerencia si tiene sentido
                        If sTipoPet = "PRO" And sClasePet = "SINC" Then
                            If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then
                                Call setHabilCtrl(cmdModificar, "NOR")
                            End If
                            If sp_GetAccionPerfil("SNEW000", PerfilInternoActual, sEstado, Null, Null) Then
                                Call setHabilCtrl(cmdSector, "NOR")
                            End If
                        End If
                    End If
                Case "CGRU", "CSEC"
                    If flgIngerencia Or flgEjecutorParaModificar Then
                        If (CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "ESP") Then
                            If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then
                                Call setHabilCtrl(cmdModificar, "NOR")
                            End If
                        End If
                    End If
                    If glEsHomologador Then Call setHabilCtrl(cmdModificar, "DIS")
                    
                    Call StatusLocal(sbMainPeticion, "")
                    If CodigoCombo(cboClase, True) = "SINC" Then
                        Call StatusLocal(sbMainPeticion, "Falta seleccionar la clase de la petici�n.")
                    End If
                Case "CDIR", "CGCI"
                    If CodigoCombo(cboTipopet, True) = "PRJ" Then
                        If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then
                            Call setHabilCtrl(cmdModificar, "NOR")
                        End If
                    End If
                Case "GBPE"
                    Call StatusLocal(sbMainPeticion, "")
                    ' Mensajes de faltantes
                    If CodigoCombo(cboClase, True) = "SINC" Then
                        Call StatusLocal(sbMainPeticion, "Falta clasificar la petici�n.")
                        GoTo Continuar
                    End If
                    If CodigoCombo(cboImpacto, True) = "-" Then
                        Call StatusLocal(sbMainPeticion, "Falta especificar impacto t�cnol�gico.")
                        GoTo Continuar
                    End If
                    If CodigoCombo(cboBpar, False) = "" Then
                        Call StatusLocal(sbMainPeticion, "Falta especificar al referente de sistemas.")
                        GoTo Continuar
                    End If
                    
                    If Hab_beneficio Then 'GMT05
                    ' Control antes de cambio de estado para el referente de RGP (que complete la valoraci�n propia).
                        If glModoPeticion = "VIEW" Then
                            If chkCargarBeneficios.value = 0 Then
                                If Not ValidarValoracion Then
                                    Call StatusLocal(sbMainPeticion, "Falta completar la parte de valoraci�n en Detalles (Impactos, Facilidad, etc.)")
                                    GoTo Continuar
                                End If
                            End If
                        End If
                    End If 'GMT05
Continuar:
                    If flgIngerencia Then
                        ' Si el PERFIL puede modificar la petici�n (EVENTO: PMOD000) para el ESTADO actual de la misma.
                        If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdModificar, "NOR")
                        ' Ac� es lo mismo:
                        ' Debo chequear que exista un registro para ese estado, no todos.
                        If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdCambioEstado, "NOR")
                    End If
                Case "BPAR"
                    ' Para que el Ref. de sistemas pueda modificar o cambiar el estado de la petici�n, la petici�n
                    ' debe estar clasificada y con los datos de impacto y regulatorio completados.
                    Select Case CodigoCombo(cboTipopet, True)
                        Case "PRO", "ESP"
                            If CodigoCombo(cboClase, True) <> "SINC" Then
                                If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) = 0 Then
                                    If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdModificar, "NOR")
                                    If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdCambioEstado, "NOR")
                                Else
                                    If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdModificar, "NOR")
                                    If Not InPerfil("CSEC") Then 'GMT02
                                        If ValidarCompletitudValidacionTecnica Then
                                            If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdCambioEstado, "NOR")
                                        Else
                                            If bFlagValidacionTecnica Then Call StatusLocal(sbMainPeticion, "Falta completar la pesta�a de validaci�n t�cnica.")
                                        End If
                                    End If 'GMT02
                                End If
                            End If
                        Case Else
                            If CodigoCombo(cboRegulatorio, True) = "-" Then
                                Call StatusLocal(sbMainPeticion, "Falta especificar si es regulatorio.")
                                GoTo Continuar
                            End If
                            If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) = 0 Then
                                If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdModificar, "NOR")
                                If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdCambioEstado, "NOR")
                            Else
                                If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdModificar, "NOR")
                                If ValidarCompletitudValidacionTecnica Then
                                    If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado, Null, Null) Then Call setHabilCtrl(cmdCambioEstado, "NOR")
                                Else
                                    If bFlagValidacionTecnica Then Call StatusLocal(sbMainPeticion, "Falta completar la pesta�a de validaci�n t�cnica.")
                                End If
                            End If
                    End Select
                Case Else
                    If flgIngerencia Then
                        ' *****************************************************************
                        ' HABILITACION DEL BOTON DE MODIFICAR
                        ' *****************************************************************
                        ' Si el PERFIL puede modificar la petici�n (EVENTO: PMOD000) para el ESTADO actual de la misma.
                        If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then
                            Call setHabilCtrl(cmdModificar, "NOR")
                        End If
                        ' Control de cambio de estado (tiene que estar clasificada)
                        If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado, Null, Null) Then
                            Call setHabilCtrl(cmdCambioEstado, "NOR")
                        End If
                    End If
        End Select
        If bDebuggear Then
            Call doLog(vbTab & vbTab & "Puede cambiar de estado: " & cmdCambioEstado.Enabled)
            Call doLog(vbTab & vbTab & "Puede modificar        : " & cmdModificar.Enabled)
            Call doLog(vbTab & vbTab & "Puede agregar sectores : " & cmdSector.Enabled)
            Call doLog(vbTab & vbTab & "Puede agregar grupos   : " & cmdGrupos.Enabled)
        End If
    End Select
    txtEstado.BackColor = IIf(InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|", ClearNull(sEstado), vbTextCompare) > 0, COLOR_TERMINAL, COLOR_ACTIVA)
    flgEnCarga = False
    Call LockProceso(False)
End Sub

Private Sub InicializarComboAdjuntos(SoloHabilitados As Boolean)
    Dim i As Long
    
    If bDebuggear Then Call doLog("InicializarComboAdjuntos()")
    With cboAdjClass
        .Enabled = False
        .Clear
        For i = 0 To UBound(vTipoDocumento)
            If SoloHabilitados Then
                If vTipoDocumento(i).Habilitado Then
                    .AddItem vTipoDocumento(i).Codigo & " : " & vTipoDocumento(i).Nombre
                End If
            Else
                .AddItem vTipoDocumento(i).Codigo & " : " & vTipoDocumento(i).Nombre
            End If
        Next i
        .ListIndex = PosicionCombo(cboAdjClass, cboAdjClass.Tag, False)
        '.Enabled = True
        '.Locked = True
    End With
End Sub

Private Sub cboClase_Click()
    If flgEnCarga Then Exit Sub
    If bDebuggear Then Call doLog("cboClase_Click()")
    If cboClase.ListIndex > -1 Then
        m_cTT_Clase.ToolText(cboClase) = vClasePeticionDescripcion(cboClase.ListIndex)
    End If
    Call SeleccionaClasePeticion
End Sub

Function CamposObligatorios() As Boolean
    Dim i As Long
    Dim bValidacion As Boolean
    
    If bDebuggear Then
        doLog ("CamposObligatorios()")
    End If
    ' Control de t�tulo
    If ClearNull(txtTitulo.text) = "" Then
       Call orjBtn_Click(orjDAT)
       MsgBox "Falta especificar t�tulo", vbOKOnly + vbExclamation
       CamposObligatorios = False
       Exit Function
    End If
    ' Control de tipo
    If Len(cboTipopet.text) = 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Debe especificar el tipo de la petici�n", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    ' Control de clase
    If Not InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) > 0 Then
        If CodigoCombo(cboClase, True) = "SINC" And InStr(1, "SOLI|REFE|SUPE|AUTO|ANAL|GBPE|SADM|", PerfilInternoActual, vbTextCompare) = 0 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Debe indicar la CLASE de la petici�n.", vbExclamation + vbOKOnly, "Clase de petici�n"
            CamposObligatorios = False
            If cboClase.Enabled = True Then cboClase.SetFocus
            Exit Function
        End If
    End If

    If InStr(1, "ALTA|MODI|", glModoPeticion, vbTextCompare) > 0 Then
        If InStr(1, "EVOL|NUEV|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
            If cboRO.ListIndex = -1 Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Debe indicar si la petici�n est� vinculada a la mitigaci�n de factores de Riesgo Operacional (RO).", vbExclamation + vbOKOnly, "Riesgo Operacional"
                CamposObligatorios = False
                If cboRO.Enabled = True Then cboRO.SetFocus
                Exit Function
            End If
        End If
    End If

    ' Control de especificaci�n de Impacto tecnol�gico
    If InStr(1, "NUEV|OPTI|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
        If CodigoCombo(cboImpacto, True) = "-" Then                                     ' add -091- a.
            If InStr(1, "SADM|ADMI|GBPE|", PerfilInternoActual, vbTextCompare) = 0 Then      ' Para perfiles administradores no realizo control ni advertencia, dejo grabar as�
                Call orjBtn_Click(orjDAT)
                MsgBox "Falta especificar IMPACTO TECNOL�GICO", vbOKOnly + vbExclamation, "Impacto tecnol�gico"
                If cboImpacto.Enabled Then cboImpacto.SetFocus
                CamposObligatorios = False
                Exit Function
            End If
        End If
    Else
        cboImpacto.ListIndex = PosicionCombo(cboImpacto, "N", True)
    End If
    ' Control de especificaci�n de Regulatorio
    If InStr(1, "BPAR|CSEC|CGRU|", PerfilInternoActual, vbTextCompare) > 0 Then             ' upd -113- a. Quitamos al Ref. de RGP
        If CodigoCombo(cboRegulatorio, True) = "-" Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Falta especificar si es REGULATORIO", vbOKOnly + vbExclamation, "Regulatorio"
            If cboRegulatorio.Enabled Then cboRegulatorio.SetFocus
            CamposObligatorios = False
            Exit Function
        End If
    End If
    
    '{ add -037- a. En esta funci�n esta contemplado que impl�citamente reconozca que no se desea guardar datos de proyecto IDM.
    If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) > 0 Then
        If Not ValidarProyecto(txtPrjId, txtPrjSubId, txtPrjSubsId) Then
            Call orjBtn_Click(orjDAT)
            CamposObligatorios = False
            Exit Function
        End If
    End If
    '}
    ' *********************************************
    ' Controles para peticiones de tipo Proyecto...
    ' *********************************************
    If CodigoCombo(cboTipopet, True) = "PRJ" Then
        ' Control de Importancia (Visibilidad)
        If cboImportancia.ListIndex < 0 Then
           Call orjBtn_Click(orjDAT)
           MsgBox "Falta especificar VISIBILIDAD", vbOKOnly + vbExclamation
           CamposObligatorios = False
           Exit Function
        End If
        ' Control de Corporativo/Local
        If cboCorpLocal.ListIndex < 0 Then
           Call orjBtn_Click(orjDAT)
           MsgBox "Falta especificar Corp/Local", vbOKOnly + vbExclamation
           CamposObligatorios = False
           Exit Function
        End If
        ' Control de orientaci�n
        If cboOrientacion.ListIndex < 0 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Falta especificar ORIENTACION", vbOKOnly + vbExclamation
            CamposObligatorios = False
            Exit Function
        End If
        '{ add -001- d. (Proyecto) Error si trata de designar la case CORR, ATEN o SPUF
        If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True)) = 0 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Las peticiones de tipo Proyecto solo pueden ser de alguna de las siguientes clases:" & vbCrLf & vbCrLf & "- Mantenimiento evolutivo" & vbCrLf & "- Nuevo desarrollo", vbExclamation + vbOKOnly, "Clase de petici�n"
            CamposObligatorios = False
            Exit Function
        End If
        '}
    End If
    
    If CodigoCombo(cboImpacto, True) = "S" And (Not PerfilInternoActual = "SOLI" And Not PerfilInternoActual = "ANAL") Then
        If InStr(1, CodigoCombo(cboTipopet, True), "PRO") > 0 Then
            Select Case CodigoCombo(cboClase, True)
                Case "CORR"
                    Call orjBtn_Click(orjDAT)
                    MsgBox "Una petici�n de la clase correctivo no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                    CamposObligatorios = False
                    Exit Function
                Case "ATEN"
                    Call orjBtn_Click(orjDAT)
                    MsgBox "Una petici�n de la clase atenci�n a usuario no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                    CamposObligatorios = False
                    Exit Function
                Case "SPUF"
                    Call orjBtn_Click(orjDAT)
                    MsgBox "Una petici�n de la clase SPUFI no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                    CamposObligatorios = False
                    Exit Function
            End Select
        End If
    End If

    '{ add -113- h.
    If InStr(1, "ALTA|MODI|", glModoPeticion, vbTextCompare) > 0 Then
        If InStr(1, "SOLI|BPAR|GBPE|", glUsrPerfilActual, vbTextCompare) > 0 Then
            If cboOrientacion.ListIndex = -1 Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Falta especificar el campo Orientaci�n.", vbOKOnly + vbExclamation
                CamposObligatorios = False
                Exit Function
            End If
        End If
    End If
    '}
    ' Control de sector
    If cboSector.ListIndex < 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Falta especificar el sector solicitante.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    
    If glModoPeticion = "ALTA" Then
        If Not valSectorHabil(CodigoCombo(cboSector, True)) Then
            MsgBox ("El sector solicitante est� marcado como inhabilitado."), vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    End If
    
    ' Control de Referente de Sistema en propias especiales
    If glModoPeticion = "ALTA" Then
        If CodigoCombo(cboTipopet, True) <> "PRO" Then
            If CodigoCombo(cboGestion, True) = "DESA" Then
                If cboBpar.ListIndex = -1 Then
                    Call orjBtn_Click(orjDAT)
                    MsgBox "Falta especificar el Referente de Sistemas.", vbOKOnly + vbExclamation
                    CamposObligatorios = False
                    Exit Function
                End If
            End If
        End If
    End If
    
    '{ add -095- a. Control de empresa
    If cboEmp.ListIndex < 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Falta especificar la empresa.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    '}

    'GMT04 - INI
    '{ add -105- a.
    'If InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) = 0 Then
    '    If CodigoCombo(cboTipopet, True) = "ESP" Then
    '        If Not valSectorHabilParaSolicitar(CodigoCombo(cboSector, True)) Then
    '            MsgBox ("El sector solicitante est� inhabilitado" & vbCrLf & "para ser usado en peticiones especiales."), vbOKOnly + vbExclamation
    '            CamposObligatorios = False
    '            Exit Function
    '        End If
    '    End If
    'End If
    '}
    'GMT04 - FIN

    ' Control de solicitante
    If cboSolicitante.ListIndex < 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Falta especificar al solicitante.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    
    ' Control de la descripci�n
    If ClearNull(memDescripcion.text) = "" Then
        Call orjBtn_Click(orjMEM)
        MsgBox "Falta especificar la descripci�n.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        memDescripcion.SetFocus
        Exit Function
    End If
    
    ' Control de Caracter�sticas, Motivos y Beneficios (para las no propias)
    If CodigoCombo(cboTipopet, True) <> "PRO" Then
        If ClearNull(memCaracteristicas.text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Faltan especificar las caracter�sticas.", vbOKOnly + vbExclamation
           CamposObligatorios = False
           memCaracteristicas.SetFocus
           Exit Function
        End If
        If ClearNull(memMotivos.text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Faltan especificar los motivos.", vbOKOnly + vbExclamation
           CamposObligatorios = False
           memMotivos.SetFocus
           Exit Function
        End If
    End If
    '{ add -037- a.
    If Len(txtPrjId.text) = 0 Then txtPrjId.text = "0"
    If Len(txtPrjSubId.text) = 0 Then txtPrjSubId.text = "0"
    If Len(txtPrjSubsId.text) = 0 Then txtPrjSubsId.text = "0"
    '}
    
    ' Si cambia el valor y ya no quiere cargar beneficios, se advierte que se perder�n
    ' todos los datos de palancas cargadas, y se pregunta si se desea continuar.
    If Hab_beneficio Then 'GMT05
        If chkCargarBeneficios.Tag <> IIf(chkCargarBeneficios.value = 1, "N", "S") Then
            If chkCargarBeneficios.Tag = "S" Then
                If MsgBox("Si se cargaron indicadores de valorizaci�n / KPIs," & vbCrLf & "�stos se perder�n. �Desea continuar de todos modos?", vbExclamation + vbYesNo) = vbNo Then
                    chkCargarBeneficios.value = 0
                    Call orjBtn_Click(orjMEM)
                    CamposObligatorios = False
                    Exit Function
                End If
            End If
        End If
    End If 'GMT05
    
  
    '{ add -113- g.
   'If sQuienGestiona = "BPE" Then               -> GMT05
    If sQuienGestiona = "BPE" And Hab_RGyP Then '-> GMT05
        If glUsrPerfilActual = "SOLI" And glModoPeticion = "ALTA" Then
            If chkPeticionDisclaimer.value <> 1 Then
                Call orjBtn_Click(orjMEM)
                MsgBox "Debe confirmar la declaraci�n para continuar con la petici�n.", vbOKOnly + vbExclamation
                CamposObligatorios = False
                sstDetalles.Tab = 2
                Exit Function
            End If
        End If
        If Hab_beneficio Then 'GMT05
            If InStr(1, "SOLI|REFE|SUPE|AUTO|", glUsrPerfilActual, vbTextCompare) > 0 And InStr(1, "ALTA|MODI|", glModoPeticion, vbTextCompare) > 0 Then
                If chkCargarBeneficios.value = 0 Then
                    If Not ValidarValoracion Then
                        Call orjBtn_Click(orjMEM)
                        MsgBox "Faltan completar los items de la pesta�a Indicadores.", vbOKOnly + vbExclamation
                        CamposObligatorios = False
                        sstDetalles.Tab = 1
                        Exit Function
                    End If
                    If Hab_Kpi Then 'GMT05
                        If Not ValidarKPI Then
                            Call orjBtn_Click(orjMEM)
                            MsgBox sMensajesValidacionKPI, vbOKOnly + vbExclamation
                            CamposObligatorios = False
                            sstDetalles.Tab = 0
                            Exit Function
                        End If
                    End If 'GMT05
                End If
            End If
        End If 'GMT05
    End If
    '}
    If glModoPeticion = "MODI" And glUsrPerfilActual = "GBPE" Then
        If CodigoCombo(cboGestion, True) = "DESA" Then
            If cboBpar.ListIndex = -1 Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Debe especificar el Referente de Sistemas.", vbOKOnly + vbExclamation
                CamposObligatorios = False
                Exit Function
            End If
        End If
    End If
    
    If glModoPeticion = "MODI" And glUsrPerfilActual = "BPAR" And bFlagValidacionTecnica Then
       'If InStr(1, "EVOL|NUEV|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then 'GMT01
        If InStr(1, "EVOL|NUEV|", CodigoCombo(cboClase, True), vbTextCompare) > 0 And Not InPerfil("CSEC") Then
            If Not ValidarCompletitudValidacionTecnica Then
                Call orjBtn_Click(orjANX)
                
              ' If Not bCompletoTodoArquitectura And Not bCompletoTodoSeguridadInformatica Then 'GMT01
                If Not bCompletoTodoArquitectura And Not bCompletoTodoSeguridadInformatica And Not InPerfil("CSEC") Then 'GMT01
                    sstValidacionTecnica.Tab = 0
                    MsgBox "Faltan completar alg�n/algunos items" & vbCrLf & "de validaci�n t�cnica.", vbOKOnly + vbExclamation
                ElseIf Not bCompletoTodoArquitectura Then
                    sstValidacionTecnica.Tab = 1
                    MsgBox "Faltan completar alg�n/algunos items" & vbCrLf & "de validaci�n t�cnica (Arquitectura).", vbOKOnly + vbExclamation
                ElseIf Not bCompletoTodoSeguridadInformatica Then
                    MsgBox "Faltan completar alg�n/algunos items" & vbCrLf & "de validaci�n t�cnica (Seguridad Inform�tica).", vbOKOnly + vbExclamation
                ElseIf Not bCompletoTodoData Then 'GMT01
                    MsgBox "Faltan completar alg�n/algunos items" & vbCrLf & "de validaci�n t�cnica (Data).", vbOKOnly + vbExclamation 'GMT01
                End If
                CamposObligatorios = False
                Exit Function
            End If
        End If
    End If
    CamposObligatorios = True
End Function

Private Sub CargarPeticion(Optional bCompleto)
    Dim bControlFechaIniPlan As Boolean
    Dim bControlHorasEstimadas As Boolean
    Dim sMensajeControlFechas As String
    Dim i As Integer
    
    bControlFechaIniPlan = False
    bControlHorasEstimadas = False
    sMensajeControlFechas = ""
    
    If bDebuggear Then
        Call Debuggear(Me, "CargarPeticion(" & bCompleto & ")")
        Call doLog("CargarPeticion(" & bCompleto & ")")
    End If

    If IsMissing(bCompleto) Then
        bCompleto = True
    End If
    ' VER PARA QUE SE USAN ESTAS DOS!!!!
    'Dim xDescri As String      ' No se estar�a usando en este proceso CargarPeticion
    Dim xAsig As String         ' Auxiliar p/peticiones anexadas
    
    Call Puntero(True)
    
    bFlagValidacionTecnica = False
    bTieneUnProyecto = False
    flgIngerencia = False
    flgSolicitor = False
    flgSolicitorVinc = False
    bOrigenIncidencia = False
    
    Call CargarGruposDeControl
    
    ' 1. Primero cargo todos los controles
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        txtNrointerno.text = IIf(Not IsNull(aplRST!pet_nrointerno), aplRST!pet_nrointerno, "")
        txtNroAsignado.text = IIf(Not IsNull(aplRST!pet_nroasignado), aplRST!pet_nroasignado, "S/N")
        If Val(ClearNull(aplRST!pet_nroanexada)) > 0 Then
            xAsig = IIf(Val(ClearNull(aplRST!anx_nroasignado)) > 0, ClearNull(aplRST!anx_nroasignado), "S/N")
            txtNroAnexada.text = xAsig & Space(10) & "||" & IIf(Not IsNull(aplRST!pet_nroanexada), aplRST!pet_nroanexada, "")
        Else
            txtNroAnexada.text = ""
        End If
        txtTitulo.text = ClearNull(aplRST!Titulo): txtTitulo.ToolTipText = ClearNull(aplRST!Titulo): txtTitulo.Tag = txtTitulo.text
        txtEstado.text = ClearNull(aplRST!nom_estado)
        txtSituacion.text = ClearNull(aplRST!nom_situacion)
        txtFechaPedido.text = IIf(Not IsNull(aplRST!fe_pedido), Format(aplRST!fe_pedido, "dd/mm/yyyy"), "")
        txtFiniplan.text = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
        txtFfinplan.text = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
        txtFinireal.text = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "dd/mm/yyyy"), "")
        txtFfinreal.text = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "dd/mm/yyyy"), "")
        txtFechaEstado.text = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "dd/mm/yyyy"), "")
        txtFechaComite.text = IIf(Not IsNull(aplRST!fe_comite), Format(aplRST!fe_comite, "dd/mm/yyyy"), "")
        txtHspresup.text = ClearNull(aplRST!horaspresup)
        txtCategoria.text = ClearNull(aplRST.Fields!categoria)
        txtPuntuacion.text = IIf(aplRST.Fields!puntuacion <> 0, Format(ClearNull(aplRST.Fields!puntuacion), "###0.000"), "-")
        cboTipopet.ListIndex = PosicionCombo(cboTipopet, ClearNull(aplRST!cod_tipo_peticion), True)
        cboClase.ListIndex = PosicionCombo(cboClase, ClearNull(aplRST!cod_clase), True)
        cboImpacto.ListIndex = PosicionCombo(cboImpacto, ClearNull(aplRST!pet_imptech), True)
        cboRO.ListIndex = PosicionCombo(cboRO, ClearNull(aplRST!pet_ro), True): cboRO.Tag = ClearNull(aplRST!pet_ro)        ' add -097- a.
        cmdVerAnexora.Enabled = IIf(Val(txtNroAnexada.text) = 0, False, True)
        cboRegulatorio.ListIndex = PosicionCombo(cboRegulatorio, ClearNull(aplRST!pet_regulatorio), True)
        txtHsPlanif.text = ClearNull(aplRST!cant_planif)                            ' add -024- a. Ver el sp_UpdatePetSecFechas (quien calcula este dato)
        txtPrjId = IIf(IsNull(aplRST!pet_projid), 0, aplRST!pet_projid): txtPrjId.Tag = txtPrjId
        txtPrjSubId = IIf(IsNull(aplRST!pet_projsubid), 0, aplRST!pet_projsubid): txtPrjSubId.Tag = txtPrjSubId
        txtPrjSubsId = IIf(IsNull(aplRST!pet_projsubsid), 0, aplRST!pet_projsubsid): txtPrjSubsId.Tag = txtPrjSubsId
        txtAgrupamientoSDA = ClearNull(aplRST!sda_agr_nrointerno)
        txtSgiIncidencia = ClearNull(aplRST!sgi_incidencia): If ClearNull(txtSgiIncidencia) <> "" Then bOrigenIncidencia = True
        Me.Tag = ClearNull(aplRST!cod_usualta)
        If ClearNull(aplRST.Fields!pet_prioridad) = "0" Then
            txtPetPrioridad = "-"
        Else
            txtPetPrioridad = ClearNull(aplRST.Fields!pet_prioridad)
        End If
        sImpacto = ClearNull(aplRST!pet_imptech)
        sClasePet = ClearNull(aplRST!cod_clase)
        If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then
            If CDate(aplRST.Fields!fe_pedido) >= dValidacionTecnicaInicio Then
                bFlagValidacionTecnica = True
            End If
        End If
        ' ******************************************************************************
        sSituacion = ClearNull(aplRST!cod_situacion)
        sRegulatorio = ClearNull(aplRST!pet_regulatorio)    ' PARA QUE CORCHOS ESTA VARIABLE!!!
        glNroAsignado = txtNroAsignado.text
        prjNroInterno = IIf(Not IsNull(aplRST!prj_nrointerno), aplRST!prj_nrointerno, "")
        sEstado = ClearNull(aplRST!cod_estado)
        ' Control de consistencia de fechas
        If InStr(1, "PLANOK|EJECUC|", sEstado, vbTextCompare) > 0 Then
            If Not IsDate(txtFiniplan.text) Then    ' Fecha inicio de planificaci�n
                bControlFechaIniPlan = True
            End If
        End If
        ' Control de horas estimadas
        'TODO: no vale la pena hacer este control por el momento.
'        If InStr(1, "ESTIOK|PLANOK|EJECUC|", sEstado, vbTextCompare) > 0 Then
'            If txtHspresup <> "" Then
'                If Val(txtHspresup) = 0 Then
'                    bControlHorasEstimadas = True
'                End If
'            End If
'        End If
        cPrioridad = ClearNull(aplRST!prioridad)            ' add -040- b.
        sTipoPet = ClearNull(aplRST!cod_tipo_peticion)
        xCorpLocal = ClearNull(aplRST!corp_local)
        xImportancia = ClearNull(aplRST!importancia_cod)
        cVisibilidad = xImportancia                         ' add -040- b.
        xOrientacion = ClearNull(aplRST!cod_orientacion)
        xUsrLoginActual = ClearNull(aplRST!importancia_usr)
        xUsrPerfilActual = ClearNull(aplRST!importancia_prf)
        xDir = ClearNull(aplRST!cod_direccion)
        xGer = ClearNull(aplRST!cod_gerencia)
        xSec = ClearNull(aplRST!cod_sector): cboSector.Tag = xSec
        sSoli = ClearNull(aplRST!cod_solicitante)
        sRefe = ClearNull(aplRST!cod_referente)
        sSupe = ClearNull(aplRST!cod_supervisor)
        sDire = ClearNull(aplRST!cod_director)
        sUsualta = ClearNull(aplRST!cod_usualta)
        sBpar = ClearNull(aplRST!cod_bpar)
        cboEmp.ListIndex = PosicionCombo(cboEmp, ClearNull(aplRST!pet_emp), True): cboEmp.Tag = ClearNull(aplRST!pet_emp)               ' add -095- a.
        sBPE = ClearNull(aplRST!cod_bpe)
        sBPEFecha = ClearNull(aplRST!fecha_BPE)
        txtFechaRefBPE.text = IIf(Not IsNull(aplRST!fecha_BPE), Format(aplRST!fecha_BPE, "dd/mm/yyyy"), "")
        cboGestion.ListIndex = PosicionCombo(cboGestion, ClearNull(aplRST!pet_driver), True): cboGestion.Tag = CodigoCombo(cboGestion, True)
        sQuienGestiona = ClearNull(aplRST!pet_driver)
        txtValorImpacto = IIf(ClearNull(aplRST.Fields!valor_impacto) = "", "-", Format(ClearNull(aplRST.Fields!valor_impacto), "####0.00"))
        txtValorFacilidad = IIf(ClearNull(aplRST.Fields!valor_facilidad) = "", "-", Format(ClearNull(aplRST.Fields!valor_facilidad), "####0.00"))
        
        If Hab_beneficio Then 'GMT05
            chkCargarBeneficios.value = IIf(ClearNull(aplRST.Fields!cargaBeneficios) = "N", 1, 0): chkCargarBeneficios.Tag = ClearNull(aplRST.Fields!cargaBeneficios)
        End If 'GMT05
        
        If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) > 0 Then
            bTieneUnProyecto = True                            ' add -113- i.
            txtPrjNom = MostrarNombreProyecto(txtPrjId, txtPrjSubId, txtPrjSubsId)
        End If
        Call HabilitarEdicionTexto(False)
        Call HabilitarEdicionTextoFuentes(False)
        If bCompleto Then
            memDescripcion.text = sp_GetMemo(glNumeroPeticion, PETICION_TEXTO_DESCRIPCION)
            memCaracteristicas.text = sp_GetMemo(glNumeroPeticion, PETICION_TEXTO_CARACTERISTICAS)
            memMotivos.text = sp_GetMemo(glNumeroPeticion, PETICION_TEXTO_MOTIVOS)
            memRelaciones.text = sp_GetMemo(glNumeroPeticion, PETICION_TEXTO_RELACIONES)
            memObservaciones.text = sp_GetMemo(glNumeroPeticion, PETICION_TEXTO_OBSERVACIONES)
            txObservaciones = memObservaciones.text
            txCaracteristicas = memCaracteristicas.text
            txDescripcion = memDescripcion.text
            txMotivos = memMotivos.text
            txRelaciones = memRelaciones.text
        End If
        If Not bIndicadoresInicializados Then Call InicializarIndicadores
        
        If Hab_beneficio Then 'GMT05
            Call CargarGrillaBeneficios
        End If 'GMT05
        
        If Hab_Kpi Then 'GMT05
            Call CargarGrillaKPI
        End If 'GMT05
        
        Call CargarValidacionTecnica(grdArquitectura, VALIDACION_ARQUITECTURA_TITULO, VALIDACION_ARQUITECTURA)
        Call CargarValidacionTecnica(grdSeguridadInformatica, VALIDACION_SEGURIDADINFORMATICA_TITULO, VALIDACION_SEGURIDADINFORMATICA)
        Call CargarValidacionTecnica(grdData, VALIDACION_DATA_TITULO, VALIDACION_DATA) 'GMT01
        Call ValidarPeticion            ' Verifica si la petici�n es v�lida para pasajes a Producci�n (bandeja diaria PeticionChangeMan)
    End If
    ''INGERENCIA
    flgEjecutor = InEjecutor()
    flgBP = esBP()
    flgEjecutorParaModificar = EsEjecutorParaModificar()
    Select Case PerfilInternoActual
        Case "SADM", "CDIR", "CGCI"             'esto esta harcodeado, por pedido de Milstein. Estos dos perfiles pueden Modificar, (solo la VISIBILIDAD)
            flgIngerencia = True
        Case "BPAR", "GBPE"                     ' upd -113- e. Se agrega GBPE
            If flgBP Then
                flgIngerencia = True
            End If
        Case "SOLI", "REFE", "AUTO", "SUPE"
            If xPerfNivel = "BBVA" Or _
                xPerfNivel = "DIRE" And xDir = xPerfDir Or _
                xPerfNivel = "GERE" And xGer = xPerfGer Or _
                xPerfNivel = "SECT" And xSec = xPerfSec Then
                    flgIngerencia = True
                    flgSolicitor = True
            End If
        'Se hacen preguntas especiales para los que estan en la rama ejecutora
        Case "CSEC"
            If InSector() Then
                flgIngerencia = True
            End If
        Case "CGRU", "ANAL"
            If InGrupo() Then
                flgIngerencia = True
            End If
            '{ add -101- a. TODO: revisar esto. Qued� por la parte de Vinculadas (Cayo).
            If InStr(1, "3|4|5|", CodigoCombo(cboEmp, True), vbTextCompare) > 0 Then
                If sp_GetPeticionRecurso(glNumeroPeticion, glLOGIN_Grupo) Then
                    Do While Not aplRST.EOF
                        If ClearNull(aplRST.Fields!cod_recurso) = glLOGIN_ID_REEMPLAZO And ClearNull(aplRST.Fields!grupo_homologacion) = "N" Then
                            flgSolicitorVinc = True
                            Exit Do
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            End If
            '}
        '{ add -101- a.
        Case Else
            If InStr(1, "3|4|5|", CodigoCombo(cboEmp, True), vbTextCompare) > 0 Then
                If sp_GetPeticionRecurso(glNumeroPeticion, glLOGIN_Grupo) Then
                    Do While Not aplRST.EOF
                        If ClearNull(aplRST.Fields!cod_recurso) = glLOGIN_ID_REEMPLAZO And ClearNull(aplRST.Fields!grupo_homologacion) = "N" Then
                            flgSolicitorVinc = True
                            Exit Do
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            End If
        '}
    End Select
    Call InicializarCombos
    If xSec <> "" Then Call SetCombo(cboSector, xSec, True)
    If sSoli <> "" Then Call SetCombo(cboSolicitante, sSoli)
    If sRefe <> "" Then Call SetCombo(cboReferente, sRefe)
    If sSupe <> "" Then Call SetCombo(cboSupervisor, sSupe)
    If sDire <> "" Then Call SetCombo(cboDirector, sDire)
    If sBPE <> "" Then Call SetCombo(cboReferenteBPE, sBPE)     ' add -113- e.
    
    cboCorpLocal.ListIndex = PosicionCombo(cboCorpLocal, xCorpLocal, True)
    cboImportancia.ListIndex = PosicionCombo(cboImportancia, xImportancia, True)
    cboOrientacion.ListIndex = PosicionCombo(cboOrientacion, xOrientacion, True)
    
    ' Control de inconsistencias en fechas de planificaci�n
    If bControlFechaIniPlan Then
        i = 0
        sMensajeControlFechas = ""
        If sp_GetPeticionGrupo(glNumeroPeticion, Null, Null) Then
            Do While Not aplRST.EOF
                If InStr(1, "S|N|", ClearNull(aplRST.Fields!homo), vbTextCompare) > 0 Then
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                        If Not IsDate(aplRST.Fields!fe_ini_plan) Then
                            i = i + 1
                            If ClearNull(aplRST.Fields!nom_responsable) <> "*** SIN RESPONSABLE A CARGO ***" Then
                                sMensajeControlFechas = sMensajeControlFechas & ClearNull(aplRST.Fields!nom_grupo) & " (" & ClearNull(aplRST.Fields!nom_responsable) & "). "
                            Else
                                sMensajeControlFechas = sMensajeControlFechas & ClearNull(aplRST.Fields!nom_grupo) & ". "
                            End If
                        End If
                    End If
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If glModoPeticion = "VIEW" Then
            If ClearNull(sMensajeControlFechas) <> "" Then
                imgFechaAdvertencia(0).ToolTipText = "Faltan definir fechas de inicio/fin de planificaci�n"
                imgFechaAdvertencia(0).visible = True
                lblCustomToolTip.Caption = "Los siguientes grupos (" & i & ") deben especificar las fechas de planificaci�n: " & sMensajeControlFechas
            End If
        End If
    End If
    Call Puntero(False)
End Sub

Private Sub InicializarCombos()
    Dim xDescri As String
    Dim i As Long
    
    Set m_cTT = New cTooltip                                   ' Para los tooltips complejos
    Set m_cTT_Clase = New cTooltip
    
    With m_cTT
        ' Creamos el toolTip pasandole el nombre del Formulario
        Call .Create(Me)
    
        'Establecemos el Ancho del ToolTip
        .MaxTipWidth = 340  '240
        
        ' establece los m�rgenes
        .Margin(ttMarginBottom) = 2
        .Margin(ttMarginTop) = 2
        .Margin(ttMarginLeft) = 2
        .Margin(ttMarginRight) = 2
        
        ' Establecemos el tiempo que se muestra ( 7 segundos )
        .DelayTime(ttDelayShow) = 7000
        
        ' Agregamos un ToolTip al FileListBox
        'Para agregar mas controles solo hay que a�adir uno por uno
        
        'Nota: solo es valido usar controles que posean HWND
        .AddTool cboOrientacion
    End With
    
    With m_cTT_Clase
        Call .Create(Me)            ' Creamos el toolTip pasandole el nombre del Formulario
        .MaxTipWidth = 340  '240    'Establecemos el Ancho del ToolTip
        ' establece los m�rgenes
        .Margin(ttMarginBottom) = 2
        .Margin(ttMarginTop) = 2
        .Margin(ttMarginLeft) = 2
        .Margin(ttMarginRight) = 2
        .DelayTime(ttDelayShow) = 7000  ' Establecemos el tiempo que se muestra ( 7 segundos )
        ' Agregamos un ToolTip al FileListBox
        'Para agregar mas controles solo hay que a�adir uno por uno
        
        'Nota: solo es valido usar controles que posean HWND
        .AddTool cboClase
    End With
    
    If bDebuggear Then Call doLog("InicializarCombos()")
    ' TODO: arreglar para las ESPeciales, que permita las otras opciones adem�s de ATENci�n de usuarios (cuando no es DYD).
    cboImportancia.Clear
    If sp_GetImportancia(Null, Null) Then
        Do While Not aplRST.EOF
            cboImportancia.AddItem ClearNull(aplRST!nom_importancia) & ESPACIOS & "||" & ClearNull(aplRST!cod_importancia)
            aplRST.MoveNext
        Loop
        If ClearNull(xImportancia) <> "" Then cboImportancia.ListIndex = PosicionCombo(cboImportancia, xImportancia, True)
    End If
    
    i = 0
    Erase vOrientacionDescripcion
    cboOrientacion.Clear
    If sp_GetOrientacion(Null, "S") Then
        Do While Not aplRST.EOF
            ReDim Preserve vOrientacionDescripcion(i)
            vOrientacionDescripcion(i) = ClearNull(aplRST!Descripcion)
            cboOrientacion.AddItem ClearNull(aplRST!nom_orientacion) & ESPACIOS & "||" & ClearNull(aplRST!cod_orientacion)
            i = i + 1
            aplRST.MoveNext
        Loop
        If ClearNull(xOrientacion) <> "" Then cboOrientacion.ListIndex = PosicionCombo(cboOrientacion, xOrientacion, True)
    End If
    
    cboBpar.Clear
    If sp_GetRecursoPerfil(Null, "BPAR") Then
        Do While Not aplRST.EOF
            cboBpar.AddItem ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
            aplRST.MoveNext
        Loop
        If ClearNull(sBpar) <> "" Then cboBpar.ListIndex = PosicionCombo(cboBpar, sBpar)
    End If
    
    If sp_GetRecursoPerfil(Null, "GBPE") Then
        cboReferenteBPE.Clear
        Do While Not aplRST.EOF
            cboReferenteBPE.AddItem ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
            aplRST.MoveNext
        Loop
        If ClearNull(sBPE) <> "" Then cboReferenteBPE.ListIndex = PosicionCombo(cboReferenteBPE, sBPE)
    End If
    
    ' TODO: refactorizar por favor!
    ' ESTA ES LA PARTE PARA EL SECTOR SOLICITANTE Y LOS INTERVINIENTES
    Select Case glModoPeticion
        Case "MODI"
            Select Case PerfilInternoActual
                Case "SADM"
                    Call CargaCombosConTodo     ' Carga TODO para que pueda modificar cuelquier cosa
                Case "CSEC", "CGRU", "ANAL"
                    If CodigoCombo(cboTipopet, True) <> "ESP" Then
                        Call CargaSectoresPosibles
                        With cboSolicitante
                            .Clear
                            If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
                                .AddItem ClearNull(aplRST(0)) & " : " & ClearNull(aplRST(1))
                            End If
                            .ListIndex = 0
                        End With
                    Else
                        Call CargaCombosConValor            ' S�lo cargo lo que figura en la peticion
                    End If
                Case Else
                    Call CargaCombosConValor                ' S�lo cargo lo que figura en la peticion
                End Select
        Case "ALTA"
            Select Case PerfilInternoActual
                Case "SADM"
                    Call CargaCombosConTodo
                Case "SOLI"
                    Call CargaSectoresPosibles              ' Cargo lo que depende del perfil
                    If sp_GetRecurso(sSoli, "R") Then
                        cboSolicitante.AddItem ClearNull(aplRST(0)) & " : " & ClearNull(aplRST(1)) ' upd -040- c.
                        cboSolicitante.ListIndex = 0
                    End If
                Case "CSEC", "CGRU", "ANAL"
                    If CodigoCombo(cboTipopet, True) <> "ESP" Then
                        Call CargaSectoresPosibles          ' S�lo sectores solicitantes del perfil actual
                        With cboSolicitante
                            .Clear
                            If sp_GetRecurso(sSoli, "R") Then
                                .AddItem ClearNull(aplRST(0)) & " : " & ClearNull(aplRST(1)) ' upd -040- c.
                            End If
                            .ListIndex = 0
                        End With
                    Else
                        Call CargaCombosConTodo
                    End If
            End Select
            
            If sp_GetSectorXt(CodigoCombo(cboSector, True), Null, Null) Then
                If ClearNull(aplRST.Fields!cod_perfil) = "BPAR" Then
                    sQuienGestiona = GESTION_SISTEMAS
                    Call SetCombo(cboGestion, sQuienGestiona, True)
                    Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))
                    cboReferenteBPE.ListIndex = -1
                Else
                    sQuienGestiona = GESTION_BPE
                    Call SetCombo(cboGestion, sQuienGestiona, True)
                    Call SetCombo(cboReferenteBPE, getBpSector(CodigoCombo(cboSector, True)))
                    Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))
                    cboReferenteBPE.ListIndex = -1
                End If
            End If
        Case "VIEW"
            'solo cargo lo que figura en la peticion
            Call CargaCombosConValor
    End Select
End Sub

Sub CargaSectoresPosibles()
    ' Carga los sectores solicitantes posibles seg�n la parametrizaci�n del perfil
    ' Ej.:
    ' Perfil SOLI a nivel Direcci�n: carga todos los sectores de la direcci�n (de todas las gerencias de la direcci�n)
    ' Perfil SOLI a nivel Gerencia: carga todos los sectores de la gerencia
    ' Perfil SOLI a nivel Sector: carga s�lo el sector asociado del solicitante
    Dim xDescri As String
    
    cboSector.Clear
    If sp_GetSectorXt(xPerfSec, xPerfGer, xPerfDir) Then
        Do While Not aplRST.EOF
            cboSector.AddItem ClearNull(aplRST!nom_direccion) & " � " & ClearNull(aplRST!nom_gerencia) & " � " & ClearNull(aplRST!nom_sector) & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST(0))  ' add -051- d.
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If cboSector.ListCount = 1 Then
        cboSector.ListIndex = 0
        Call setHabilCtrl(cboSector, "DIS")                             ' Si es solo un sector el cargado, entonces inhabilito la posibilidad de selecci�n del control Combo
    Else
        Call SetCombo(cboSector, IIf(glModoPeticion = "ALTA", xPerfSec, xSec), True)
        If cboSector.ListIndex = -1 Then cboSector.ListIndex = 0        ' Si hay m�s de un sector, por defecto elijo el primero (para que pueda traer correctamente los datos de quien gestiona la petici�n).
    End If
    If bDebuggear Then
        Call doLog("CargaSectoresPosibles()")
        Call doLog(vbTab & vbTab & "xPerfSec: " & Trim(xPerfSec) & ", xPerfGer: " & Trim(xPerfGer) & ", xPerfDir: " & Trim(xPerfDir) & vbTab & " -> " & aplRST.RecordCount & " sector(es) cargado(s).")
    End If
End Sub

' TODO: queda para el futuro implementar esto (15.04.2016).
'Private Sub CargaSectoresPosiblesDESA()
'    ' Similar a CargaSectoresPosibles pero cuando es una especial para Sistemas, que debe excluir a los sectores de Sistemas
'
'End Sub

Sub CargaIntervinientes()
    If bDebuggear Then
        Call Debuggear(Me, "CargaIntervinientes()")
        Call doLog("CargaIntervinientes()")
    End If
    
    Dim sSec, sGer, sDir As String
    If cboSector.ListIndex = -1 Then cboSector.ListIndex = 0
    cboSolicitante.Clear
    cboReferente.Clear
    cboDirector.Clear
    cboSupervisor.Clear
    
    sSec = CodigoCombo(Me.cboSector, True)
    If sp_GetSectorXt(sSec, Null, Null) Then
        sDir = ClearNull(aplRST!cod_direccion)
        sGer = ClearNull(aplRST!cod_gerencia)
    End If
    With cboSolicitante
        If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "SOLI") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)   ' upd -040- c.
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = SetCombo(cboSolicitante, sSoli)
        End If
    End With
    ' Cargo el resto de los perfiles: referentes, autorizantes, supervisores
    If InStr(1, "CGRU|CSEC|", glUsrPerfilActual, vbTextCompare) = 0 Then
        If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "REFE") Then
            Do While Not aplRST.EOF
                cboReferente.AddItem ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "SUPE") Then
            Do While Not aplRST.EOF
                cboSupervisor.AddItem ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "AUTO") Then
            Do While Not aplRST.EOF
                cboDirector.AddItem ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If ClearNull(xSec) = ClearNull(CodigoCombo(cboSector, True)) Then
            If ClearNull(sSoli) = ClearNull(CodigoCombo(cboSolicitante)) Then
                If sRefe <> "" Then Call SetCombo(cboReferente, sRefe)
                If sSupe <> "" Then Call SetCombo(cboSupervisor, sSupe)
                If sDire <> "" Then Call SetCombo(cboDirector, sDire)
            End If
            
            If cboReferente.ListCount <> -1 Then Call setHabilCtrl(cboReferente, NORMAL)
            If cboSupervisor.ListCount <> -1 Then Call setHabilCtrl(cboSupervisor, NORMAL)
            If cboDirector.ListCount <> -1 Then Call setHabilCtrl(cboDirector, NORMAL)
        End If
    End If
End Sub

Sub CargaCombosConTodo()
    ' Por ejemplo, cuando la petici�n es ESPecial, se cargan todos los sectores solicitantes y sus intervinientes (solicitantes).
    Dim xDescri As String
    
    If bDebuggear Then
        Call Debuggear(Me, "CargaCombosConTodo()")
        Call doLog("CargaCombosConTodo()")
    End If
    Call setHabilCtrl(cboSector, DISABLE)
    cboSector.Clear
    If sp_GetSectorXt(Null, Null, Null, "S") Then
        Do While Not aplRST.EOF
            cboSector.AddItem ClearNull(aplRST!nom_direccion) & " � " & ClearNull(aplRST!nom_gerencia) & " � " & ClearNull(aplRST!nom_sector) & ESPACIOS & "||" & aplRST(0)    ' add -051- d.
            aplRST.MoveNext
        Loop
    End If
    If xSec <> "" Then Call SetCombo(cboSector, xSec, True)
    If PerfilInternoActual <> "SADM" Then Call CargaIntervinientes
    Call setHabilCtrl(cboSector, NORMAL)
End Sub

Sub CargaCombosConValor()
    If bDebuggear Then
        Call Debuggear(Me, "CargaCombosConValor()")
        Call doLog("CargaCombosConValor()")
    End If
    ' CargaCombosConValor:
    ' Este procedimiento vuelve a cargar con datos los combos de:
    ' - Sector                  (Todo los sectores para el perfil actuante)
    ' - Solicitante
    ' - Referente
    ' - Supervisor
    ' - Autorizante
    ' - Business Partner
    
    ' TODO ESTO HABRIA QUE REVISARLO!!! 21.03.2016
    
    Dim cod_recurso As String           ' Auxiliar
    Dim xDescri As String               ' Auxiliar
    
    cod_recurso = ""
    cboSector.Clear
    
    '{ add -035- a.
    If sp_GetSectorXt(xSec, xGer, xDir) Then
        If Not aplRST.EOF Then
            cboSector.AddItem ClearNull(aplRST!nom_direccion) & " � " & ClearNull(aplRST!nom_gerencia) & " � " & ClearNull(aplRST!nom_sector) & ESPACIOS & "||" & ClearNull(aplRST!cod_sector)     ' add -051- d.
            cboSector.ListIndex = 0
        End If
    End If
    '}
    If sSoli <> "" Then
        With cboSolicitante
            .Clear
            If sp_GetRecurso(sSoli, "R") Then
                xDescri = ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
            Else
                xDescri = ClearNull(sSoli) & " : " & "Error Solicitante"
            End If
            .AddItem xDescri
            .ListIndex = 0
        End With
    End If

    ' Referente
    If sRefe <> "" Then
        cod_recurso = IIf(CodigoCombo(cboTipopet, True) = "PRO", sUsualta, sRefe)
        With cboReferente
            .Clear
            If sp_GetRecurso(cod_recurso, "R") Then
                xDescri = ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
            Else
                xDescri = cod_recurso & " : " & "Error Referente"
            End If
            .AddItem xDescri
            .ListIndex = 0
        End With
    End If
    ' Supervisor
    If sSupe <> "" Then
        cod_recurso = IIf(CodigoCombo(cboTipopet, True) = "PRO", sUsualta, sSupe)
        With cboSupervisor
            .Clear
            If sp_GetRecurso(cod_recurso, "R") Then
                xDescri = ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
            Else
                xDescri = cod_recurso & " : " & "Error Supervisor"
            End If
            .AddItem xDescri
            .ListIndex = 0
        End With
    End If
    ' Autorizante
    If sDire <> "" Then
        cod_recurso = IIf(CodigoCombo(cboTipopet, True) = "PRO", sUsualta, sDire)
        With cboDirector
            .Clear
            If sp_GetRecurso(cod_recurso, "R") Then
                xDescri = ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
            Else
                xDescri = cod_recurso & " : " & "Error autorizante"
            End If
            .AddItem xDescri
            .ListIndex = 0
        End With
    End If
    '{ add -054- a. Quiere decir que no encuentra el elemento en el combo (le quitaron el perfil al recurso)
    If glModoPeticion = "VIEW" Then
        If cboBpar.ListIndex = -1 Then
            If sBpar <> "" Then
                If sp_GetRecurso(sBpar, "R") Then
                    xDescri = ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
                Else
                    xDescri = sBpar & " : " & "Error referente de sistema"
                End If
                cboBpar.AddItem xDescri
                Call SetCombo(cboBpar, sBpar)
            End If
        End If
        If cboReferenteBPE.ListIndex = -1 Then
            If sBPE <> "" Then
                If sp_GetRecurso(sBPE, "R") Then
                    xDescri = ClearNull(aplRST!cod_recurso) & " : " & ClearNull(aplRST!nom_recurso)
                Else
                    xDescri = sBPE & " : " & "Error referente de RGyP"
                End If
                cboReferenteBPE.AddItem xDescri
                Call SetCombo(cboReferenteBPE, sBPE)
            End If
        End If
    End If
    '}
End Sub


Private Sub memCaracteristicas_Change()
    bFlgCaracteristicas = True
End Sub

Private Sub memDescripcion_Change()
    bFlgDescripcion = True
End Sub

Private Sub memMotivos_Change()
    bFlgMotivos = True
End Sub

Private Sub memObservaciones_Change()
    bFlgObservaciones = True
End Sub

Private Sub memRelaciones_Change()
    bFlgRelaciones = True
End Sub

Private Function modifImportancia(oUsualta, oldPerf, nUsualta, newPerf) As Boolean
    modifImportancia = False
    If oldPerf = "" Then
        modifImportancia = True
    End If
    If newPerf = "ADMI" Or newPerf = "SADM" Then
        modifImportancia = True
    End If
    If oUsualta = nUsualta Then
        modifImportancia = True
    End If
    Select Case oldPerf
    Case "ADMI"
        modifImportancia = True
    Case "CGRU"
        modifImportancia = True
    Case "CSEC"
        If InStr("CGCI|CDIR", newPerf) > 0 Then
            modifImportancia = True
        End If
    Case "CGCI"
        If InStr("CDIR", newPerf) > 0 Then
            modifImportancia = True
        End If
    Case "CDIR"
        If newPerf = "CDIR" Then
            modifImportancia = True
        End If
    End Select
End Function

Private Function InGrupo() As Boolean
    ' Esta funci�n determina si seg�n el perfil actual del usuario, esta inclu�do en alg�n
    ' grupo de la petici�n (por ejemplo, el perfil del usuario actual es Resp. de Ejecuci�n:
    ' el xPerfNivel es 'GRUP' y trata de ver si alguno de los grupos coincide con el del recurso.
    InGrupo = False
    If Not sp_GetPeticionGrupo(glNumeroPeticion, Null, Null) Then Exit Function
    Do While Not aplRST.EOF
        If xPerfNivel = "BBVA" Or _
            xPerfNivel = "DIRE" And ClearNull(aplRST!cod_direccion) = xPerfDir Or _
            xPerfNivel = "GERE" And ClearNull(aplRST!cod_gerencia) = xPerfGer Or _
            xPerfNivel = "SECT" And ClearNull(aplRST!cod_sector) = xPerfSec Or _
            xPerfNivel = "GRUP" And ClearNull(aplRST!cod_grupo) = xPerfGru Then
                InGrupo = True
                Exit Do         ' add -023- a. Agregado como mejora: si ya determin� que existe en alguno de los grupos, no hace falta recorrer todos los grupos de la petici�n.
        End If
        aplRST.MoveNext
    Loop
    If bDebuggear Then Call doLog(DbgFmt(InGrupo, "InGrupo()"))
End Function

Private Function InSector() As Boolean
    InSector = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If xPerfNivel = "BBVA" Or _
            xPerfNivel = "DIRE" And ClearNull(aplRST!cod_direccion) = xPerfDir Or _
            xPerfNivel = "GERE" And ClearNull(aplRST!cod_gerencia) = xPerfGer Or _
            xPerfNivel = "SECT" And ClearNull(aplRST!cod_sector) = xPerfSec Then
            InSector = True
        End If
        aplRST.MoveNext
    Loop
    If bDebuggear Then Call doLog(DbgFmt(InSector, "InSector()"))
End Function

' Esta funci�n devuelve True cuando el Sector del usuario actual esta contenido
' en alguno de los sectores ejecutores de la petici�n actual.
Private Function InEjecutor() As Boolean
    InEjecutor = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If ClearNull(aplRST!cod_sector) = glLOGIN_Sector Then
            InEjecutor = True
        End If
        aplRST.MoveNext
    Loop
    If bDebuggear Then Call doLog(DbgFmt(InEjecutor, "InEjecutor()"))
End Function

' El prop�sito de esta funci�n es devuelver un valor Booleano si el grupo o sector
' del usuario actual esta "relacionado" respecto del BP de la petici�n
Private Function EstaRelacionadoAlBP(Optional ByVal Grupo As String, _
                                     Optional ByVal Sector As String) As Boolean
    Dim xBPSect, xBPGrup, xBPGere, xBPDire As String
    
    EstaRelacionadoAlBP = False
    ' Obtengo el �rbol jerarquico donde se encuentra el BP de la petici�n actual
    If sp_GetRecurso(sBpar, "R") Then
        xBPGrup = ClearNull(aplRST!cod_grupo)
        xBPSect = ClearNull(aplRST!cod_sector)
        xBPGere = ClearNull(aplRST!cod_gerencia)
        xBPDire = ClearNull(aplRST!cod_direccion)
    End If
    
    If Not IsMissing(Grupo) And Len(Grupo) > 0 Then
        If xBPGrup = Grupo Then
            EstaRelacionadoAlBP = True
        End If
    Else
        If Not IsMissing(Sector) And Len(Sector) > 0 Then
            If xBPSect = Sector And PerfilInternoActual = "CSEC" Then
                EstaRelacionadoAlBP = True
            End If
        End If
    End If
    If bDebuggear Then Call doLog(DbgFmt(EstaRelacionadoAlBP, "EstaRelacionadoAlBP()"))
End Function
'}

'{ add -006- x.
' Esta funci�n determina lo siguiente:
' El usuario o recurso es "Ejecutor para modificar" si pertenece a alguno de los sectores de la petici�n
' y se corresponde al sector solicitante de la petici�n.
Private Function EsEjecutorParaModificar() As Boolean
    EsEjecutorParaModificar = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If ClearNull(aplRST!cod_sector) = glLOGIN_Sector Then
            If xSec = glLOGIN_Sector Then
                EsEjecutorParaModificar = True
                Exit Do
            End If
        End If
        aplRST.MoveNext
    Loop
    If bDebuggear Then Call doLog(DbgFmt(EsEjecutorParaModificar, "EsEjecutorParaModificar()"))
End Function
'}

Private Function esBP() As Boolean
    esBP = False
    If InStr(1, "GBPE|BPAR|", PerfilInternoActual, vbTextCompare) > 0 Then                          ' add -113- e.
        If sBpar = ClearNull(glLOGIN_ID_REEMPLAZO) Or sBPE = ClearNull(glLOGIN_ID_REEMPLAZO) Then   ' upd -113- e. Se agrega el OR para BPE
            esBP = True
        Else
            PerfilInternoActual = "VIEW"
        End If
    End If
    If bDebuggear Then Call doLog(DbgFmt(esBP, "esBP()"))
End Function

Private Function DbgFmt(Valor As Boolean, Nombre As String) As String
    DbgFmt = IIf(Valor, CStr(Valor) & vbTab, CStr(Valor) & vbTab & vbTab) & " <== " & Nombre
End Function

Private Sub cmdRptHs_Click()
    If glCrystalNewVersion Then Call NuevoReporte
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
        
    'Abrir el reporte
    Call Puntero(True)
    
    sReportName = "\hstrapeta6.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Horas trabajadas por petici�n / tarea"
        .ReportComments = "Petici�n: " & ClearNull(txtNroAsignado.text)
    End With
    
    'MsgBox "1. Por llamar a FuncionesCrystal10.ConectarDB(crReport)..."
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
    
    'MsgBox "2. Por leer y cargar los parametros"
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    
    'MsgBox "3. Cargados los parametros"
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fdesde": crParamDef.AddCurrentValue ("19000101")
            Case "@fhasta": crParamDef.AddCurrentValue ("20790606")
            Case "@tipo": crParamDef.AddCurrentValue ("PET")
            Case "@codigo": crParamDef.AddCurrentValue (CStr(glNumeroPeticion))
            Case "@detalle": crParamDef.AddCurrentValue (CStr("1"))
        End Select
    Next
    'MsgBox "4. Seteados los parametros"
    
    Set rptVisorRpt.crReport = crReport
    
    'MsgBox "5. Seteados el reporte"
    
    rptVisorRpt.CargarReporte
    
    'MsgBox "6. Cargado el reporte"
    
    rptVisorRpt.Show 1
    
    'MsgBox "7. Mostrado el reporte"
    
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub CargarVinculados()
    Const IMAGE_MSEXCEL = 1
    Const IMAGE_ADOBEPDF = 2
    Const IMAGE_MSWORD = 3
    Const IMAGE_EMAIL = 2       '6
    Const IMAGE_OTHER = 5
    Const IMAGE_HOMOLOGACION = 13
    
    Dim subElemento As MSComctlLib.ListItem
    Dim i As Long
    Dim imagen As Integer
    
    With lvwVinculados
        .Enabled = False
        .ListItems.Clear
        .BorderStyle = ccNone
        .GridLines = False
        .HotTracking = False
        .HoverSelection = False
        .LabelWrap = True
        .View = lvwReport
        .Font.name = "Tahoma": .Font.Size = 8
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , "archivo", "Nombre del archivo", 4000, lvwColumnLeft
        .ColumnHeaders.Add , "peticion", "peticion", 0, lvwColumnLeft
        .ColumnHeaders.Add , "ubicacion", "Ubicaci�n", 4000, lvwColumnLeft
        .ColumnHeaders.Add , "propietario", "Subido por", 2000, lvwColumnLeft
        .ColumnHeaders.Add , "fecha", "Fecha", 1800, lvwColumnLeft
        .ColumnHeaders.Add , "legajo", "legajo", 0, lvwColumnLeft
        .ColumnHeaders.Add , "texto", "texto", 0, lvwColumnLeft
    
        If sp_GetAdjunto(glNumeroPeticion, Null) Then
            Do While Not aplRST.EOF
                i = i + 1
                Set subElemento = .ListItems.Add(, , ClearNull(aplRST.Fields!adj_file), , IMAGE_OTHER)
                subElemento.SubItems(1) = ClearNull(aplRST.Fields!pet_nrointerno)
                subElemento.SubItems(2) = ClearNull(aplRST.Fields!adj_path)
                subElemento.ListSubItems(2).ForeColor = vbBlue
                subElemento.SubItems(3) = ClearNull(aplRST.Fields!nom_audit)
                subElemento.SubItems(4) = ClearNull(aplRST.Fields!audit_date)
                subElemento.SubItems(5) = ClearNull(aplRST.Fields!audit_user)
                subElemento.SubItems(6) = ClearNull(aplRST.Fields!adj_texto)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .Enabled = True
    End With
    Call docHabBtn(0, "X")
End Sub

Private Sub AbrirAdjunto(sFileName)
    'Dim lRetCode As Long
    Call EXEC_ShellExecute(Me.hWnd, sFileName)
End Sub

Private Sub docView_Click()
    If ClearNull(glPathDocMetod) = "" Then
        Exit Sub
    End If
    Call Puntero(True)
    Call AbrirAdjunto(glPathDocMetod)
    glPathDocMetod = ""                 ' add -012- a. - Se blanquea la variable una vez que se utiliza
    Call Puntero(False)
End Sub

'{ add -082- b.
Private Sub docAdd_Click()
    Call SeleccionarVincWindows
End Sub

Private Sub SeleccionarVincWindows()
    On Error GoTo Errores
    Dim i As Integer
    'Dim sPathMetodologia As String
    Dim cNombresDeArchivo As String
    
    If sPathMetodologia = "" Then sPathMetodologia = glWRKDIR
    ' Arma el path para los documentos de metodolog�a
    cNombresDeArchivo = ""
    cMensajeError = ""
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = "Vincular a la petici�n..."
        .Filter = "Todos|*.*|*.xls|*.xls|*.doc|*.doc|*.rtf|*.rtf|*.pps|*.pps|*.mpp|*.mpp|*.ppt|*.ppt|*.tif|*.tif|*.bmp|*.bmp|*.jpg|*.jpg|*.htm|*.htm|*.html|*.html|*.txt|*.txt|*.pdf|*.pdf|*.zip|*.zip|*.msg|*.msg|*.rar|*.rar"
        .FilterIndex = 1
        .InitDir = sPathMetodologia
        .Flags = cdlOFNAllowMultiselect + cdlOFNFileMustExist + cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        cNombresDeArchivo = .FileName
        Call Puntero(True)
        Call ArmarVectorDeArchivos(cNombresDeArchivo, vbNullChar)
    End With
    If ValidarArchivosVinculados Then
        Call GuardarArchivosVinculados
        Call CargarVinculados
    Else
        MsgBox cMensajeError, vbExclamation + vbOKOnly, "Problemas encontrados"
    End If
Fin:
    Call Puntero(False)
    Call docHabBtn(0, "X")
    Exit Sub
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
    End Select
End Sub
'}

Private Sub docCan_Click()
    docFile = "": docPath = "": docComent = ""
    Call docHabBtn(0, "X")
End Sub

Private Sub docCon_Click()
    If ClearNull(docFile.text) = "" Or ClearNull(docPath.text) = "" Then
        docFile = "": docPath = "": docComent = ""
        MsgBox ("No se ha seleccionado archivo.")
        Exit Sub
    End If
    Select Case docOpcion
        Case "A"
            If sp_InsertAdjunto(glNumeroPeticion, docFile.text, docPath.text, ClearNull(docComent.text)) Then
                Call sp_AddHistorial(glNumeroPeticion, "VINCADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, docFile.text) ' add -042- a.
                Call docHabBtn(0, "X")
            End If
        Case "M"
            If sp_UpdateAdjunto(glNumeroPeticion, docFile.text, ClearNull(docComent.text)) Then
                lvwVinculados.SelectedItem.SubItems(6) = ClearNull(docComent.text)
                Call sp_AddHistorial(glNumeroPeticion, "VINCUPD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, docFile.text) ' add -042- a.
                Call docHabBtn(0, "X", False)
            End If
        Case "E"
            If sp_DeleteAdjunto(glNumeroPeticion, docFile.text) Then
                Call sp_AddHistorial(glNumeroPeticion, "VINCDEL", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, docFile.text) ' add -042- a.
                docFile = "": docPath = "": docComent = ""
                Call docHabBtn(0, "X")
            End If
        Case Else
            docFile = "": docPath = "": docComent = ""
            Call docHabBtn(0, "X")
    End Select
End Sub

Private Sub docDel_Click()
    Call docHabBtn(1, "E")
End Sub

Private Sub docMod_Click()
    Call docHabBtn(1, "M")
End Sub

Sub docHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).Enabled = True
            End If 'GMT05
            orjBtn(orjCNF).Enabled = True
            orjBtn(orjEST).Enabled = True
            orjBtn(orjHOM).Enabled = True       ' add -109- a.
            lblDocMetod = "": lblDocMetod.visible = False
            
            Call setHabilCtrl(docFile, "DIS")
            Call setHabilCtrl(docPath, "DIS")
            Call setHabilCtrl(docComent, "DIS")
            If flgEjecutor Then
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) = 0 Then
                    tbVinculados.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                End If
            End If
            tbVinculados.Buttons(BOTONERA_ACTUALIZAR).Enabled = True
            tbVinculados.Buttons(BOTONERA_EDITAR).Enabled = False
            tbVinculados.Buttons(BOTONERA_QUITAR).Enabled = False
            tbVinculados.Buttons(BOTONERA_CONFIRMAR).Enabled = False
            tbVinculados.Buttons(BOTONERA_CANCELAR).Enabled = False
            docComent.Locked = True
            If lvwVinculados.ListItems.Count > 1 Then
                Call lvwVinculados_ItemClick(lvwVinculados.SelectedItem)
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).Enabled = False
            End If 'GMT05
            orjBtn(orjCNF).Enabled = False
            orjBtn(orjEST).Enabled = False
            orjBtn(orjHOM).Enabled = False      ' add -109- a.
            fraButtPpal.Enabled = False
            tbVinculados.Buttons(BOTONERA_VER).Enabled = False
            tbVinculados.Buttons(BOTONERA_ACTUALIZAR).Enabled = False
            tbVinculados.Buttons(BOTONERA_ADJUNTAR).Enabled = False
            tbVinculados.Buttons(BOTONERA_EDITAR).Enabled = False
            tbVinculados.Buttons(BOTONERA_QUITAR).Enabled = False
            tbVinculados.Buttons(BOTONERA_CONFIRMAR).Enabled = True
            tbVinculados.Buttons(BOTONERA_CANCELAR).Enabled = True
            
            Select Case sOpcionSeleccionada
                Case "A"
                    lblDocMetod = " AGREGAR ": lblDocMetod.visible = True
                    docFile = "": docPath = "": docComent = ""
                    docComent.Locked = False
                    fraDocMet.Enabled = True
                Case "M"
                    lblDocMetod = " MODIFICAR ": lblDocMetod.visible = True
                    fraDocMet.Enabled = True
                    docComent.Locked = False
                    Call setHabilCtrl(docComent, "NOR")
                    docComent.SetFocus
                Case "E"
                    lblDocMetod = " DESVINCULAR ": lblDocMetod.visible = True
                    fraDocMet.Enabled = False
            End Select
    End Select
    docOpcion = sOpcionSeleccionada
End Sub

'{ add -082- a. Seleccionar archivos para ser adjuntados a la petici�n actual
' TODO: quitar la validaci�n de que este clasificada la petici�n y agregarla al momento de habilitar el boton.
Private Sub adjAdd_Click()
    If sClasePet = "SINC" Then
        MsgBox "Antes de poder adjuntar un documento a la petici�n, �sta debe ser clasificada.", vbInformation + vbOKOnly, "Clasificaci�n requerida"
        Exit Sub
    Else
        Call SeleccionarAdjuntos
    End If
End Sub
'}

Private Sub SeleccionarAdjuntos()
    On Error GoTo Errores
    Dim i As Integer
    Dim sPath As String
    Dim cNombresDeArchivo As String
    
    sPath = GetSetting("GesPet", "Export01", "chkAdjuntoLPO", "C:\")
    If Dir(sPath, vbDirectory) = "" Then
        SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", "P:\"
        sPath = GetSetting("GesPet", "Export01", "chkAdjuntoLPO", "P:\")
    End If
    cMensajeError = ""
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = "Adjuntar a la petici�n..."
        .Filter = "Todos|*.*|*.xls|*.xls|*.doc|*.doc|*.rtf|*.rtf|*.pps|*.pps|*.mpp|*.mpp|*.ppt|*.ppt|*.tif|*.tif|*.bmp|*.bmp|*.jpg|*.jpg|*.htm|*.htm|*.html|*.html|*.txt|*.txt|*.pdf|*.pdf|*.zip|*.zip|*.msg|*.msg|*.rar|*.rar"
        .FilterIndex = 1
        '.InitDir = IIf(Len(Dir(.InitDir)) > 0, .InitDir, glWRKDIR)     ' del -086- a.
        .InitDir = IIf(glLSTDIR = "", glWRKDIR, glLSTDIR)               ' add -086- a.
        .Flags = cdlOFNAllowMultiselect + cdlOFNFileMustExist + cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        Call Puntero(True)
        cNombresDeArchivo = .FileName
        glLSTDIR = ObtenerDirectorio(.FileName) & "\"                   ' add -086- a.
        Call ArmarVectorDeArchivos(cNombresDeArchivo, vbNullChar)
    End With
    If ValidarArchivosAdjuntos Then
        Call GuardarArchivos
        'Call CargarAdjPet
        Call CargarEstado
        Call adjHabBtn(0, "X")
    Else
        MsgBox cMensajeError, vbExclamation + vbOKOnly, "Problemas encontrados"
    End If
Fin:
    Call Puntero(False)
    Exit Sub
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
        Case Else
            MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    End Select
End Sub

Private Sub ArmarVectorDeArchivos(cLista As String, cSeparator As String)
    ' Recibe una cadena con los elementos de una lista para armar el vector general vElemento
    ' discriminando que solos sean archivos reales los que conformen el vector
    Dim i As Long
    Dim J As Long
    Dim Pos As Long
    Dim cPath As String
    
    ' Determina el path para todos los archivos seleccionados
    If InStr(1, cLista, cSeparator, vbTextCompare) > 0 Then
        cPath = Mid(cLista, 1, InStr(1, cLista, cSeparator, vbTextCompare) - 1)
        cPath = IIf(Right(cPath, 1) = "\", cPath & "", cPath & "\")
        cLista = Mid(cLista, InStr(1, cLista, cSeparator, vbTextCompare) + 1)
    Else
        Pos = 1: J = Pos
        Do While Pos > 0
            Pos = InStr(J, cLista, "\", vbTextCompare)
            If Pos > 0 Then
                J = Pos + 1
            Else
                J = J - 1
            End If
        Loop
        cPath = Mid(cLista, 1, J)
        cLista = Mid(cLista, J + 1)
    End If

    Erase cArchivos             ' Inicializa el vector
    Do While Len(cLista) > 0
        ReDim Preserve cArchivos(i)
        If InStr(1, cLista, cSeparator, vbTextCompare) > 0 Then
            cArchivos(i).Nombre = Mid(cLista, 1, InStr(1, cLista, cSeparator, vbTextCompare) - 1)
            cArchivos(i).Tipo = TipoDeAdjunto(cArchivos(i).Nombre)
            cArchivos(i).Extension = ExtensionDelAdjunto(cArchivos(i).Nombre)
            cArchivos(i).tamanio = FileLen(cPath & cArchivos(i).Nombre)
            cArchivos(i).ubicacion = cPath
            cLista = Mid(cLista, InStr(1, cLista, cSeparator, vbTextCompare) + 1)
        Else
            cArchivos(i).Nombre = Mid(cLista, 1, Len(cLista))
            cArchivos(i).Tipo = TipoDeAdjunto(cArchivos(i).Nombre)
            cArchivos(i).Extension = ExtensionDelAdjunto(cArchivos(i).Nombre)
            cArchivos(i).tamanio = FileLen(cPath & cArchivos(i).Nombre)
            cArchivos(i).ubicacion = cPath
            cLista = ""
        End If
        i = i + 1
        DoEvents
    Loop
End Sub

Private Function ExtensionDelAdjunto(cArchivo As String) As String
    Dim i As Long
    Dim lPos As Long
    Dim cAux As String
    
    For i = Len(cArchivo) To 1 Step -1
        cAux = cAux & Mid(cArchivo, i, 1)
    Next i
    If InStr(1, cAux, ".", vbTextCompare) > 0 Then
        lPos = Len(cArchivo) - InStr(1, cAux, ".", vbTextCompare) + 1
        ExtensionDelAdjunto = Mid(cArchivo, lPos + 1)
    Else
        ExtensionDelAdjunto = ""
    End If
End Function

Private Function TipoDeAdjunto(cArchivo As String) As String
    Dim i As Long
    Dim v_cod_doc() As String
    Dim principioArch As String 'GMT03
    Dim Position As Integer  'GMT03
    Dim posAnt As String  'GMT03
    Dim posPos As String  'GMT03
    Dim archNomMay As String 'GMT03
    
    
    TipoDeAdjunto = "OTRO"      ' Por defecto
    For i = 0 To UBound(vTipoDocumento)
        If vTipoDocumento(i).Codigo <> "ADA" Then  'GMT03
            If InStr(1, cArchivo, vTipoDocumento(i).Codigo, vbTextCompare) > 0 Then
                TipoDeAdjunto = vTipoDocumento(i).Codigo
                Exit For
            End If
        End If  'GMT03
    Next i
    
    'GMT03 - INI
    If TipoDeAdjunto = "OTRO" Then
       'verifico si el archivo es un ADA (Se hace de esta manera porque no le quisieron dar un codigo o arreglar que empiece con ada)
       archNomMay = UCase(cArchivo)
       principioArch = Mid(archNomMay, 1, 3)
       If InStr(1, principioArch, "ADA", vbTextCompare) > 0 Then
           TipoDeAdjunto = "ADA"
       Else
           'verifico si el nombre contine alguna palabra con ADA
           If InStr(1, archNomMay, "ADA", vbTextCompare) > 0 Then
           'De ser as�,verifico si el ada que encontro es el codigo o una parte de alguna palabra
                Position = InStr(1, archNomMay, "ADA", vbBinaryCompare)
                posAnt = Mid(archNomMay, Position - 1, 1)
                posPos = Mid(archNomMay, Position + 3, 1)
                
                If posAnt = " " Or posAnt = "-" Or posAnt = "/" Or posAnt = "_" Or posAnt = "." Then
                   If posPos = " " Or posPos = "-" Or posPos = "/" Or posPos = "." Or posPos = "_" Then
                        TipoDeAdjunto = "ADA"
                   End If
                End If
           End If
       End If
    End If
    'GMT03 - FIN
    
    If TipoDeAdjunto = "OTRO" Then
        If InStr(1, cArchivo, "IH", vbTextCompare) > 0 Then
            If InStr(1, UCase(cArchivo), "SOB", vbTextCompare) > 0 Then
                TipoDeAdjunto = "IDH1"
            ElseIf InStr(1, UCase(cArchivo), "OME", vbTextCompare) > 0 Then
                TipoDeAdjunto = "IDH2"
            ElseIf InStr(1, UCase(cArchivo), "OMA", vbTextCompare) > 0 Then
                TipoDeAdjunto = "IDH3"
            End If
        End If
    End If
End Function

Private Sub GuardarArchivos()
    On Error GoTo Errores
    Dim i As Integer
    Dim bRespuestaControl As Byte
    
    Call Puntero(True)
        
    bRespuestaControl = 0           ' add -006- a. - Se inicializa antes de comenzar el ciclo (indeterminado)
    
    ' Antes que nada averiguo si se va a adjuntar un documento de alcance.
    ' Si es as�, pregunto si continuo a pesar de existir un conforme a a la documentaci�n de alcance.
    For i = 0 To UBound(cArchivos)
        If InStr(1, "P950|C100|ALCA|", cArchivos(i).Tipo, vbTextCompare) > 0 Then   ' upd -007- a. Se cambia CG04 por ALCA
            If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
                If MsgBox("La petici�n tiene ingresado un conforme final al documento de alcance." & vbCrLf & _
                          "Si continua, ser� eliminado dicho conforme y deber� gestionar la obtenci�n de uno nuevo." & vbCrLf & _
                          "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Agregado de documentaci�n de Alcance") = vbYes Then
                    ' Elimina el conforme de alcance final de la petici�n
                    Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
                    Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "ALT", "1")    ' add -077- a.
                    bRespuestaControl = 1       ' Acuerdo con el usuario en adjuntar y eliminar el conforme
                Else
                    bRespuestaControl = 2       ' No hay acuerdo. Se cancela el adjunto de documentos y se preserva el conforme
                End If
                Exit For
            End If
        End If
        
        'GMT03 - INI
        If InStr(1, "ADA", cArchivos(i).Tipo, vbTextCompare) > 0 Then
            If txtCategoria.text = "SDA" Then
                If sp_GetPeticionConf(glNumeroPeticion, "ADAF", Null) Then
                       If MsgBox("La petici�n tiene ingresado un conforme final al documento de ADA." & vbCrLf & _
                                 "Si continua, ser� eliminado dicho conforme y deber� gestionar la obtenci�n de uno nuevo." & vbCrLf & _
                                 "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Agregado de documentaci�n de ADA") = vbYes Then
                           ' Elimina el conforme de alcance final de la petici�n
                           Call sp_DeletePeticionConf(glNumeroPeticion, "ADAF", "REQ", "1")
                           bRespuestaControl = 1       ' Acuerdo con el usuario en adjuntar y eliminar el conforme
                           Call CargarConformes
                       Else
                           bRespuestaControl = 2       ' No hay acuerdo. Se cancela el adjunto de documentos y se preserva el conforme
                       End If
                       Exit For
                End If
            Else
               MsgBox "Solo se puede agregar un documento ADA a una peticion catalogada como SDA.", vbOKOnly + vbExclamation
               bRespuestaControl = 2
            End If
        End If
        'GMT03 - FIN
        
        'GMT07 - INI
        If InStr(1, "CCOM", cArchivos(i).Tipo, vbTextCompare) > 0 Then
           If sp_GetPeticionConf(glNumeroPeticion, "CCOM", Null) Then
              If MsgBox("La petici�n tiene ingresado un conforme final al documento de CCOM." & vbCrLf & _
                 "Si continua, ser� eliminado dicho conforme y deber� gestionar la obtenci�n de uno nuevo." & vbCrLf & _
                 "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Agregado de documentaci�n de ADA") = vbYes Then
                 ' Elimina el conforme de alcance final de la petici�n
                 Call sp_DeletePeticionConf(glNumeroPeticion, "CCOM", "REQ", "1")
                 bRespuestaControl = 1       ' Acuerdo con el usuario en adjuntar y eliminar el conforme
                 Call CargarConformes
              Else
                 bRespuestaControl = 2       ' No hay acuerdo. Se cancela el adjunto de documentos y se preserva el conforme
              End If
              Exit For
           End If
           
           ' Evaluo si ya existe al menos un documento de P950
           If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
              MsgBox "Solo puede adjuntar un CCOM cuando la petici�n ya posee un documento P950", vbExclamation + vbOKOnly, "Documento de cambio alcance"
              Exit Sub
           End If
        End If
        'GMT07 - FIN
    
    Next i
    
    If bRespuestaControl <> 2 Then
        For i = 0 To UBound(cArchivos)
           'If InStr(1, "P950|C100|ALCA|", cArchivos(i).Tipo, vbTextCompare) > 0 Then -> GMT03
           'If InStr(1, "P950|C100|ALCA|ADA|", cArchivos(i).Tipo, vbTextCompare) > 0 Then 'GMT07
            If InStr(1, "P950|C100|ALCA|ADA|CCOM|", cArchivos(i).Tipo, vbTextCompare) > 0 Then 'GMT07
                Select Case cArchivos(i).Tipo
                    Case "P950": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 44, "Nombre del documento: " & Trim(cArchivos(i).Nombre))
                    Case "C100": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 45, "Nombre del documento: " & Trim(cArchivos(i).Nombre))
                    Case "ADA": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 152, "Nombre del documento: " & Trim(cArchivos(i).Nombre)) 'GMT03
                    Case "ALCA": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 46, Trim(cArchivos(i).Nombre))
                    Case "CCOM": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 154, "Nombre del documento: " & Trim(cArchivos(i).Nombre)) 'GMT07
               End Select
            End If
            If sp_InsertPeticionAdjunto(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo, cArchivos(i).Tipo, cArchivos(i).ubicacion, cArchivos(i).Nombre, "") Then
                Call sp_AddHistorial(glNumeroPeticion, "ADJADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, cArchivos(i).Tipo & "-->" & cArchivos(i).Nombre)
                If sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then                                               ' 1. Primero determina si ya existe para la petici�n el grupo Homologador
                    If InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(sHomoGrupoEstado)) = 0 Then        ' Determina si el grupo homologador se encuentra en alguno de los estados "Activos"
                        Select Case cArchivos(i).Tipo
                            Case "C100": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 45, sEstado, Null)
                            Case "P950": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 44, sEstado, Null)
                            Case "CG04": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 46, sEstado, Null)
                            Case "C204": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 47, sEstado, Null)
                            Case "EML1": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 48, sEstado, Null)
                            Case "EML2": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 49, sEstado, Null)
                            Case "EML3": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 53, sEstado, Null)
                            Case "T710": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 50, sEstado, Null)
                        End Select
                    End If
                End If
            End If
        Next i
    End If
    Call Puntero(False)
Exit Sub
Errores:
    Resume Next
End Sub

Private Sub GuardarArchivosVinculados()
    Dim i As Integer
    
    Call Puntero(True)
    For i = 0 To UBound(cArchivos)
        If sp_InsertAdjunto(glNumeroPeticion, cArchivos(i).Nombre, cArchivos(i).ubicacion, "") Then     ' Agrega al historial el mensaje de vinculaci�n del nuevo archivo
            Call sp_AddHistorial(glNumeroPeticion, "VINCADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "")
        End If
    Next i
    Call docHabBtn(0, "X")
    Call Puntero(False)
End Sub

Private Function ValidarArchivosAdjuntos() As Boolean
    Dim i As Integer
    Dim SI_cod_sector As String, SI_cod_grupo As String     ' add -093- a.
    
    ValidarArchivosAdjuntos = True
    cMensajeError = ""

    ' Comprobaci�n de tama�o m�ximo
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).tamanio > ARCHIVOS_ADJUNTOS_MAXSIZE Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " supera" & vbCrLf & "el tama�o m�ximo permitido por la aplicaci�n."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " supera" & vbCrLf & "el tama�o m�ximo permitido por la aplicaci�n."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de tama�o m�nimo
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).tamanio < 1 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " est� vacio. No puede ser adjuntado."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " est� vacio. No puede ser adjuntado."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de largo del nombre del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Nombre) > 100 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El nombre del archivo " & cArchivos(i).Nombre & " excede los 100 caracteres. No puede ser adjuntado."
            Else
                cMensajeError = "El nombre del archivo " & cArchivos(i).Nombre & " excede los 100 caracteres. No puede ser adjuntado."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de largo del path del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).ubicacion) > 250 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser adjuntado."
            Else
                cMensajeError = "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser adjuntado."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de or�gen
    For i = 0 To UBound(cArchivos)
        If Dir(cArchivos(i).ubicacion & cArchivos(i).Nombre) = "" Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' NUEVO
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de destino (control de duplicidad)
    For i = 0 To UBound(cArchivos)
        If sp_GetAdjuntosPet(glNumeroPeticion, cArchivos(i).Tipo, Null, Null) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!adj_file) = cArchivos(i).Nombre Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & "El archivo " & cArchivos(i).Nombre & " ya est� adjunto."
                    Else
                        cMensajeError = "El archivo " & cArchivos(i).Nombre & " ya est� adjunto."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit Do
                Else
                    aplRST.MoveNext
                End If
                DoEvents
            Loop
        End If
    Next i
    
    ' Comprobaci�n de la existencia de extensi�n para el archivo
    For i = 0 To UBound(cArchivos)
        If Trim(cArchivos(i).Extension) = "" Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & "El archivo " & Chr(34) & cArchivos(i).Nombre & Chr(34) & " no tiene extensi�n."
            Else
                cMensajeError = "El archivo " & Chr(34) & cArchivos(i).Nombre & Chr(34) & " no tiene extensi�n."
            End If
            ValidarArchivosAdjuntos = False
        End If
    Next i
        
    ' Control de documentos de alcance
    For i = 0 To UBound(cArchivos)
        If InStr(1, "P950|C100|", cArchivos(i).Tipo, vbTextCompare) > 0 Then
            If sTipoPet = "PRJ" Or sImpacto = "S" Then
                If cArchivos(i).Tipo <> "C100" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Por el tipo y/o clase de la petici�n debe" & vbCrLf & "adjuntar documento de alcance tipo C100."
                    Else
                        cMensajeError = "Por el tipo y/o clase de la petici�n debe" & vbCrLf & "adjuntar documento de alcance tipo C100."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit For
                End If
            Else
                If InStr(1, "EVOL|OPTI|NUEV|", sClasePet, vbTextCompare) > 0 Then
                    If cArchivos(i).Tipo <> "P950" Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Por el tipo y/o clase de la petici�n," & vbCrLf & "debe adjuntar documento de alcance tipo P950."
                        Else
                            cMensajeError = "Por el tipo y/o clase de la petici�n," & vbCrLf & "debe adjuntar documento de alcance tipo P950."
                        End If
                        ValidarArchivosAdjuntos = False
                        Exit For
                    End If
                Else
                    ' Si por la clase no corresponde documento de alcance, entonces solo si el
                    ' usuario intenta agregar un C100 lo detengo
                    If cArchivos(i).Tipo = "C100" Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "La petici�n, por su tipo y/o clase, no requiere documento de alcance," & vbCrLf & "pero si desea agregar uno, debe ser de tipo P950."
                        Else
                            cMensajeError = "La petici�n, por su tipo y/o clase, no requiere documento de alcance," & vbCrLf & "pero si desea agregar uno, debe ser de tipo P950."
                        End If
                        ValidarArchivosAdjuntos = False
                        Exit For
                    End If
                End If
            End If
        End If
    Next i
    ' Control de documento CG04: Pruebas del Usuario
    For i = 0 To UBound(cArchivos)
        If InStr(1, "CG04", cArchivos(i).Tipo, vbTextCompare) > 0 Then
            If sEstado <> "EJECUC" Then
                If Len(cMensajeError) > 0 Then
                    cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo puede adjuntar un CG04 cuando la petici�n de encuentra en estado EN EJECUCION."
                Else
                    cMensajeError = "Solo puede adjuntar un CG04 cuando la petici�n de encuentra en estado EN EJECUCION."
                End If
                ValidarArchivosAdjuntos = False
                Exit For
            Else
                ' Evaluo si ya existe al menos un documento de alcance anterior
                If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) And _
                   Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo puede adjuntar un CG04 cuando la petici�n ya posee alg�n documento de alcance."
                        Else
                            cMensajeError = "Solo puede adjuntar un CG04 cuando la petici�n ya posee alg�n documento de alcance."
                        End If
                        ValidarArchivosAdjuntos = False
                        Exit For
                End If
            End If
        End If
    Next i
        
    ' Otro control de documento de alcance
    For i = 0 To UBound(cArchivos)
        If glUsrPerfilActual <> "SADM" And glLOGIN_Direccion <> "MEDIO" Then
            If cArchivos(i).Tipo = "P950" Or _
               cArchivos(i).Tipo = "C100" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "En documento de alcance solo puede" & vbCrLf & "ser adjuntado por usuarios de SyO."
                    Else
                        cMensajeError = "En documento de alcance solo puede" & vbCrLf & "ser adjuntado por usuarios de SyO."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit For
            End If
        End If
    Next i
    
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).Tipo = "T710" Then
            If glUsrPerfilActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                If Len(cMensajeError) > 0 Then
                    cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Un documento T710 solo puede ser" & vbCrLf & "adjuntado por usuarios de sistemas."
                Else
                    cMensajeError = "Un documento T710 solo puede ser" & vbCrLf & "adjuntado por usuarios de sistemas."
                End If
                ValidarArchivosAdjuntos = False
                Exit For
            End If
        End If
    Next i
        
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).Tipo <> "OTRO" And cArchivos(i).Tipo <> "EML1" And cArchivos(i).Tipo <> "EML2" And Left(cArchivos(i).Tipo, 3) <> "IDH" And Left(cArchivos(i).Tipo, 3) <> "IHB" And Left(cArchivos(i).Tipo, 3) <> "IHC" Then
        'if instr(1, "OTRO|EML1|EML2|EML3|",cArchivos(i).Tipo, vbTextCompare) = 0 and
        '    instr(1, "IDH|IHB|IHC", Left(cArchivos(i).Tipo, 3), vbTextCompare) = 0 then
            ' Control de c�digo en el documento a adjuntar
            If InStr(1, Trim(cArchivos(i).Nombre), cArchivos(i).Tipo, vbTextCompare) = 0 Then
                If Len(cMensajeError) > 0 Then
                    cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El documento a adjuntar debe contener la clave " & cArchivos(i).Tipo & " dentro del nombre del archivo."
                Else
                    cMensajeError = "El documento a adjuntar debe contener la clave " & cArchivos(i).Tipo & " dentro del nombre del archivo."
                End If
                ValidarArchivosAdjuntos = False
                Exit For
            End If
        Else
            ' Si se encuentra la clave de alguno de los Informes de Homologaci�n...
            If InStr(1, "IDH|IHB|IHC", Left(cArchivos(i).Tipo, 3), vbTextCompare) > 0 Then
                ' Valido que solo miembros del equipo de homologaci�n puedan adjuntarlos
                If glUsrPerfilActual <> "SADM" Then
                    If glEsHomologador Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo miembros del grupo homologador" & vbCrLf & "pueden adjuntar Informes de Homologaci�n."
                        Else
                            cMensajeError = "Solo miembros del grupo homologador" & vbCrLf & "pueden adjuntar Informes de Homologaci�n."
                        End If
                        ValidarArchivosAdjuntos = False
                        Exit For
                    End If
                End If
            End If
        End If
    Next i
    If Len(cMensajeError) > 0 Then
        ValidarArchivosAdjuntos = False
    End If
End Function

'{ add -082- b.
Private Function ValidarArchivosVinculados() As Boolean
    Dim i As Integer
    
    ValidarArchivosVinculados = True

    ' Comprobaci�n de largo del nombre del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Nombre) > 50 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El nombre del archivo " & cArchivos(i).Nombre & " excede los 50 caracteres. No puede ser vinculado."
            Else
                cMensajeError = "El nombre del archivo " & cArchivos(i).Nombre & " excede los 50 caracteres. No puede ser vinculado."
            End If
            ValidarArchivosVinculados = False
            Exit For
        End If
    Next i
    ' Comprobaci�n de largo del path del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).ubicacion) > 250 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser vinculado."
            Else
                cMensajeError = "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser vinculado."
            End If
            ValidarArchivosVinculados = False
            Exit For
        End If
    Next i
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de or�gen
    For i = 0 To UBound(cArchivos)
        If Dir(cArchivos(i).ubicacion & cArchivos(i).Nombre) = "" Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            End If
            ValidarArchivosVinculados = False
            Exit For
        End If
    Next i
    If Len(cMensajeError) > 0 Then
        ValidarArchivosVinculados = False
    End If
End Function
'}

' Acci�n de modificar un documento adjunto
Private Sub adjMod_Click()
    Call adjHabBtn(1, "M")
End Sub

' Desadjuntar un archivo adjunto
Private Sub adjDel_Click()
    Call adjHabBtn(1, "E")
End Sub

' Cancelar la acci�n de adjuntar un documento
Private Sub adjCan_Click()
    Call adjHabBtn(0, "X", True)
End Sub

Public Function PuedeDesvincularDocumentoAlcance(TipoInforme As String, PuntoControl As Integer, ResultadoInforme As Integer, ByRef MensajeControlDocAlcance As String) As Boolean
    Const SOB = 1
    Const OME = 2
    Const OMA = 3
    Const INFORME_COMPLETO = "C"
    Const INFORME_REDUCIDO = "R"
    Const PUNTOCONTROL_PRIMERO = 1
    Const PUNTOCONTROL_SEGUNDO = 2
    Const PUNTOCONTROL_TERCERO = 3
    
    MensajeControlDocAlcance = "No puede quitar el documento de alcance" & vbCrLf & "porque la petici�n ya tiene un informe SOB/OME de homologaci�n."
    PuedeDesvincularDocumentoAlcance = False
    
    If TipoInforme = INFORME_COMPLETO Then
        Select Case PuntoControl
            Case PUNTOCONTROL_PRIMERO
                PuedeDesvincularDocumentoAlcance = True
            Case PUNTOCONTROL_SEGUNDO
                If ResultadoInforme = OMA Then
                    PuedeDesvincularDocumentoAlcance = True
                End If
            Case PUNTOCONTROL_TERCERO
                MensajeControlDocAlcance = "No puede quitar el documento de alcance" & vbCrLf & "porque la petici�n ya tiene un informe de homologaci�n."
                PuedeDesvincularDocumentoAlcance = False
            Case Else
                PuedeDesvincularDocumentoAlcance = True
        End Select
    End If
End Function

Private Sub AdjuntoConfirmar()
    Dim cAdjuntoTipo As String
    Dim bInformeHomologacion As Boolean
    Dim bExisteALCA As Boolean
    Dim bExisteALCP As Boolean
    Dim bExisteADAF As Boolean 'GMT03
    Dim bExisteCCOM As Boolean 'GMT07
    ' Informes de homologaci�n
    Dim TipoInforme As String
    Dim PuntoControl As Integer
    Dim ResultadoInforme As Integer
    Dim MensajeControlDocAlcance As String

    If cboAdjClass.ListIndex < 0 Then
        MsgBox "Falta especificar tipo de archivo.", vbOKOnly + vbInformation
        Exit Sub
    End If
    cAdjuntoTipo = CodigoCombo(cboAdjClass, False)  ' add -022- a.
    
    Select Case adjOpcion
        Case "A"
            If Not sp_GetRecursoEsHomologacion(glLOGIN_ID_REEMPLAZO) Then
                If InStr(1, "IDH|IHB|IHC", adjFile.text, vbTextCompare) > 0 Then
                    MsgBox "Solo Usuarios pertenecientes al grupo Homologador pueden adjuntar Informes de Homologaci�n.", vbOKOnly + vbExclamation
                    Exit Sub
                End If
            End If
            If InStr(1, "P950|C100", cAdjuntoTipo, vbTextCompare) > 0 Then
                If CodigoCombo(cboTipopet, True) = "PRJ" Or cboImpacto = "Si" Then
                    If cAdjuntoTipo <> "C100" Then
                        MsgBox "Por el tipo/clase de petici�n, debe informar el documento de alcance tipo C100.", vbExclamation + vbOKOnly, "Documento de alcance"
                        Exit Sub
                    End If
                Else
                    If InStr(1, "EVOL|OPTI|NUEV", CodigoCombo(cboClase, True)) > 0 Then
                        If cAdjuntoTipo <> "P950" Then
                            MsgBox "Por el tipo/clase de petici�n, debe informar el documento de alcance tipo P950.", vbExclamation + vbOKOnly, "Documento de alcance"
                            Exit Sub
                        End If
                    Else
                        ' Si por la clase no corresponde documento de alcance, entonces solo si el usuario intenta agregar un C100 lo detengo
                        If cAdjuntoTipo = "C100" Then
                            MsgBox "El tipo/clase de la petici�n no requiere documento de alcance, pero si desea agregar uno, debe ser de tipo P950.", vbExclamation + vbOKOnly, "Documento de alcance"
                            Exit Sub
                        End If
                    End If
                End If
            End If
            ' Control de subida de CG04
            If InStr(1, "CG04", cAdjuntoTipo, vbTextCompare) > 0 Then
                If sEstado <> "EJECUC" Then
                    MsgBox "Solo puede adjuntar un CG04 cuando la petici�n de encuentra EN EJECUCION.", vbExclamation + vbOKOnly, "Documento de cambio alcance"
                    Exit Sub
                Else
                    ' Evaluo si ya existe al menos un documento de alcance anterior
                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) And _
                       Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                            MsgBox "Solo puede adjuntar un CG04 cuando la petici�n ya posee un documento de alcance.", vbExclamation + vbOKOnly, "Documento de cambio alcance"
                            Exit Sub
                    End If
                End If
            End If
            
            If PerfilInternoActual <> "SADM" And glLOGIN_Direccion <> "MEDIO" Then
                If cAdjuntoTipo = "P950" Or _
                   cAdjuntoTipo = "C100" Then
                    MsgBox "Un 'Doc. de alcance desarrollo'," & Chr(13) & "solo puede adjuntarse" & Chr(13) & "por usuarios de la direcci�n de Medios.", vbOKOnly + vbInformation
                    Exit Sub
                End If
            End If
            If cAdjuntoTipo = "T710" Or cAdjuntoTipo = "PLIM" Then
                If PerfilInternoActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                    MsgBox "Un documento Principios de Desarrollo Aplicativo (T710), solo puede ser adjuntado por usuarios de DyD.", vbOKOnly + vbInformation
                    Exit Sub
                End If
            End If
            If cAdjuntoTipo <> "OTRO" And cAdjuntoTipo <> "EML1" And cAdjuntoTipo <> "EML2" And Left(cAdjuntoTipo, 3) <> "IDH" And Left(cAdjuntoTipo, 3) <> "IHB" And Left(cAdjuntoTipo, 3) <> "IHC" Then  ' add -003- g.  ' upd -008- a.  ' upd -031- a. Se agrega como excepci�n los informes de Homologaci�n
                'Verifica que exista el c�digo de documento seleccionado en la cadena del nombre del archivo
                If InStr(1, Trim(adjFile.text), cAdjuntoTipo, vbTextCompare) = 0 Then
                    MsgBox "El documento a adjuntar debe contener la clave " & Trim(UCase(CodigoCombo(cboAdjClass, False))) & " dentro del nombre del archivo.", vbOKOnly + vbExclamation
                    Exit Sub
                End If
            Else
                ' Si se encuentra la clave de alguno de los Informes de Homologaci�n...
                If InStr(1, "IDH|IHB|IHC", Left(cAdjuntoTipo, 3), vbTextCompare) > 0 Then
                    If InStr(1, Trim(adjFile.text), Left(cAdjuntoTipo, 3), vbTextCompare) = 0 Then
                        MsgBox "Advertencia: Es posible que el documento que intenta adjuntar no corresponda con el tipo de Informes de Homologaci�n.", vbOKOnly + vbExclamation
                        Exit Sub
                    End If
                End If
            End If

            If MsgBox(constConfirmMsgAttachDocumentos, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then
                DoEvents
                If sp_InsertPeticionAdjunto(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo, cAdjuntoTipo, adjPath.text, adjFile.text, adjComent.text) Then     ' add -025- a. Se agregan los parametros de grupo y sector
                    'Envio mensaje cuando se adjunta un documento de alcance o cambio en el alcance
                   'If InStr(1, "P950|C100|CG04|", cAdjuntoTipo, vbTextCompare) > 0 Then -> GMT03
                   'If InStr(1, "P950|C100|CG04|ADA|", cAdjuntoTipo, vbTextCompare) > 0 Then -> GMT07
                    If InStr(1, "P950|C100|CG04|ADA|CCOM|", cAdjuntoTipo, vbTextCompare) > 0 Then 'GMT07
                        Select Case cAdjuntoTipo
                            Case "P950": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 44, "Nombre del documento: " & Trim(adjFile.text))
                            Case "C100": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 45, "Nombre del documento: " & Trim(adjFile.text))
                            Case "CG04": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 46, "Nombre del documento: " & Trim(adjFile.text))
                            Case "C104": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 51, "Nombre del documento: " & Trim(adjFile.text))     ' add -093- b.
                            Case "ADA": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 152, "Nombre del documento: " & Trim(adjFile.text))     ' GMT03
                            Case "CCOM": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 154, "Nombre del documento: " & Trim(adjFile.text))     ' GMT07
                        End Select
                    End If
                    
                    Call sp_AddHistorial(glNumeroPeticion, "ADJADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Tipo: " & TextoCombo(cboAdjClass) & " (" & adjFile.text & ")")
                    If InStr(1, "ALCA|P950|C100|", cAdjuntoTipo, vbTextCompare) > 0 Then
                        Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
                        Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "ALT", "1")    ' add -077- a.
                    End If
                    
                    Call adjHabBtn(0, "X")
                    adjPath.text = ""
                    txtAdjSector.text = ""
                    txtAdjGrupo.text = ""
                    txtAdjSize.text = ""
                    txtAdjPropietario.text = ""
                    '{ add -022- a. Envio un mensaje al grupo Homologaci�n para informar el evento ocurrido (adjunto de un documento de metodolog�a)
                    ' 1. Primero determina si ya existe para la petici�n el grupo Homologador
                    If sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then
                        ' 2. Obtengo los datos del grupo homologador
                        ' 3. Obtiene el estado actual del grupo Homologaci�n para la petici�n
                        If sp_GetPeticionGrupo(glNumeroPeticion, glHomologacionSector, glHomologacionGrupo) Then
                            sHomoGrupoEstado = ClearNull(aplRST.Fields!cod_estado)
                        End If
                        ' 4. Determina si el grupo homologador se encuentra en alguno de los estados "Activos"
                        If InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(sHomoGrupoEstado)) = 0 Then
                            Select Case cAdjuntoTipo
                                Case "C100": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 45, sEstado, Null)
                                Case "P950": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 44, sEstado, Null)
                                Case "CG04": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 46, sEstado, Null)
                                Case "C204": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 47, sEstado, Null)
                                Case "EML1": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 48, sEstado, Null)
                                Case "EML2": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 49, sEstado, Null)
                                Case "T710": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 50, sEstado, Null)
                            End Select
                        End If
                    End If
                    '}
                End If
            End If
        Case "M"
            If Not sp_UpdatePeticionAdjunto(glNumeroPeticion, cAdjuntoTipo, adjFile.text, adjComent.text, cAdjuntoTipo, adjFile.text) Then
                MsgBox "Se ha producido un error al intentar grabar los datos. Revise.", vbExclamation + vbOKOnly, "Error al actualizar"
            End If
            adjFile.text = "": adjPath.text = "": adjComent.text = ""
            txtAdjSector.text = ""
            txtAdjGrupo.text = ""
            txtAdjSize.text = ""
            txtAdjPropietario.text = ""
            Call adjHabBtn(0, "X")
        Case "E"
            bInformeHomologacion = False
            bExisteALCA = False
            bExisteALCP = False
            bExisteADAF = False  'GMT03
            bExisteCCOM = False  'GMT07
            
            If lvwAdjuntos.SelectedItem.Selected Then
                If PerfilInternoActual <> "SADM" Then
                    If InStr(1, "ALCA|P950|C100|", cAdjuntoTipo, vbTextCompare) > 0 Then
                        Call Puntero(True)
                        ' Validaci�n contra los informes de homologaci�n "embebidos" en el aplicativo que ya est�n verificados.
                        ' 06.06.17 - Actualizaci�n del control
                        If sp_GetPeticionInfohc(glNumeroPeticion, Null, Null, Null, "VERIFI") Then
                            TipoInforme = ClearNull(aplRST.Fields!info_tipo)
                            PuntoControl = ClearNull(aplRST.Fields!infoprot_punto)
                            ResultadoInforme = ClearNull(aplRST.Fields!evaluacion)
                            bInformeHomologacion = Not PuedeDesvincularDocumentoAlcance(TipoInforme, PuntoControl, ResultadoInforme, MensajeControlDocAlcance)
                        Else
                            If sp_GetAdjuntosPet(glNumeroPeticion, Null, Null, Null) Then
                                Do While Not aplRST.EOF
                                    If InStr(1, "IDH1|IDH2|", ClearNull(aplRST.Fields!adj_tipo), vbTextCompare) > 0 Then
                                        If InStr(1, ClearNull(aplRST.Fields!adj_file), "SEG", vbTextCompare) > 0 Then
                                            bInformeHomologacion = True
                                        ElseIf InStr(1, ClearNull(aplRST.Fields!adj_file), "FIN", vbTextCompare) > 0 Then
                                            bInformeHomologacion = True
                                        ElseIf InStr(1, ClearNull(aplRST.Fields!adj_file), "PAR", vbTextCompare) > 0 Then
                                            bInformeHomologacion = True
                                        End If
                                        If bInformeHomologacion Then Exit Do
                                    End If
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                        End If
                        If bInformeHomologacion Then
                            MsgBox MensajeControlDocAlcance, vbExclamation + vbOKOnly, "Eliminaci�n de documentaci�n de Alcance"
                            Call adjHabBtn(0, "X")
                            Call Puntero(False)
                            Exit Sub
                        End If
                        If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
                            bExisteALCA = True
                        End If
                        If sp_GetPeticionConf(glNumeroPeticion, "ALCP", Null) Then
                            bExisteALCP = True
                        End If
                        'GMT03 - INI
                        If sp_GetPeticionConf(glNumeroPeticion, "ADAF", Null) Then
                            bExisteADAF = True
                        End If
                        'GMT03 - FIN
                        'GMT07 - INI
                        If sp_GetPeticionConf(glNumeroPeticion, "CCOM", Null) Then
                            bExisteCCOM = True
                        End If
                        'GMT07 - FIN
                        
                       'If bExisteALCA Or bExisteALCP Then ->GMT03
                       'If bExisteALCA Or bExisteALCP Or bExisteADAF Then ' GMT07
                        If bExisteALCA Or bExisteALCP Or bExisteADAF Or bExisteCCOM Then ' GMT07
                            If MsgBox("La petici�n ya tiene alg�n conforme a la documentaci�n de alcance." & vbCrLf & _
                                    "Si continua, ser�n eliminados los conformes y deber� gestionarlos nuevamente." & vbCrLf & _
                                    "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Eliminaci�n de documentaci�n de Alcance") = vbNo Then
                                Call adjHabBtn(0, "X")
                                Call Puntero(False)
                                Exit Sub
                            Else
                                If bExisteALCA Then Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", Null, Null)
                                If bExisteALCP Then Call sp_DeletePeticionConf(glNumeroPeticion, "ALCP", Null, Null)
                                If bExisteADAF Then Call sp_DeletePeticionConf(glNumeroPeticion, "ADAF", Null, Null)  'GMT03
                                If bExisteCCOM Then Call sp_DeletePeticionConf(glNumeroPeticion, "CCOM", Null, Null)  'GMT07
                            End If
                        End If
                    End If
                End If
                Call EliminarDocumentoAdjunto(glNumeroPeticion, lvwAdjuntos.SelectedItem.SubItems(1), lvwAdjuntos.SelectedItem.text, sEstado, lvwAdjuntos.SelectedItem.SubItems(8))
                adjFile.text = "": adjPath.text = "": adjComent.text = ""
                txtAdjPropietario.text = ""
                txtAdjSector.text = ""
                txtAdjGrupo.text = ""
                txtAdjSize.text = ""

                Call adjHabBtn(0, "X")
                Call Puntero(False)
                Call CargarEstado
            End If
        Case Else
            adjFile.text = "": adjPath.text = "": adjComent.text = ""
            txtAdjSector.text = ""
            txtAdjGrupo.text = ""
            txtAdjSize.text = ""
            txtAdjPropietario.text = ""
            Call adjHabBtn(0, "X")
    End Select
End Sub

'{ add -039- c.
Private Sub EliminarDocumentoAdjunto(pet_nrointerno, cAdjuntoTipo, adjFile, sEstado, cCampoTexto)
    If sp_DeletePeticionAdjunto(pet_nrointerno, cAdjuntoTipo, adjFile) Then
        Call sp_AddHistorial(pet_nrointerno, "ADJDEL", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, cCampoTexto)
        
       'If InStr(1, "ALCA|C100|P950|", cAdjuntoTipo, vbTextCompare) > 0 Then           ->GMT03
       'If InStr(1, "ALCA|C100|P950|ADA|", cAdjuntoTipo, vbTextCompare) > 0 Then        'GMT07
        If InStr(1, "ALCA|C100|P950|ADA|CCOM|", cAdjuntoTipo, vbTextCompare) > 0 Then   'GMT07
            If Not sp_GetAdjuntosPet(pet_nrointerno, "ALCA", Null, Null) Then       ' Verifica si no existen mas documentos de alcance
                Call sp_DeletePeticionConf(pet_nrointerno, "ALCA", "REQ", "1")
                Call CargarConformes  'GMT03 - Se actualiza la grilla de conformes
            End If
            'GMT03 - INI
            If Not sp_GetAdjuntosPet(pet_nrointerno, "ADA", Null, Null) Then       ' Verifica si no existen mas documentos de alcance
                Call sp_DeletePeticionConf(pet_nrointerno, "ADAF", "REQ", "1")
                Call CargarConformes  'GMT03 - Se actualiza la grilla de conformes
            End If
            'GMT03 - FIN
            'GMT07 - INI
            If Not sp_GetAdjuntosPet(pet_nrointerno, "CCOM", Null, Null) Then       ' Verifica si no existen mas documentos de alcance
                Call sp_DeletePeticionConf(pet_nrointerno, "CCOM", "REQ", "1")
                Call CargarConformes                                                ' Se actualiza la grilla de conformes
            End If
            'GMT07 - FIN
        End If
        
       'If InStr(1, "P950|C100|CG04|", cAdjuntoTipo, vbTextCompare) > 0 Then -> GMT03
       'If InStr(1, "P950|C100|CG04|ADA|", cAdjuntoTipo, vbTextCompare) > 0 Then -> GMT07
        If InStr(1, "P950|C100|CG04|ADA|CCOM|", cAdjuntoTipo, vbTextCompare) > 0 Then 'GMT07
            Select Case cAdjuntoTipo
                Case "P950": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 144, "Nombre del documento: " & adjFile)
                Case "C100": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 145, "Nombre del documento: " & adjFile)
                Case "ALCA": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 146, adjFile)     ' upd -039- d. Se cambia CG04 por ALCA
                Case "ADA": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 151, "Nombre del documento: " & adjFile)     ' GMT03
                Case "CCOM": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 153, "Nombre del documento: " & adjFile)     ' GMT07
            End Select
        End If
    End If
End Sub
'}

Private Sub CargarAdjuntosListado()
    Const IMAGE_MSEXCEL = 1
    Const IMAGE_ADOBEPDF = 2
    Const IMAGE_MSWORD = 3
    Const IMAGE_EMAIL = 2       '6
    Const IMAGE_OTHER = 16      '5
    Const IMAGE_HOMOLOGACION = 13
    
    Dim tamanio As Long
    Dim lPesoAproxDocs As Long
    Dim subElemento As MSComctlLib.ListItem
    Dim imagen As Integer
    
    With lvwAdjuntos
        .Enabled = False
        .ListItems.Clear
        .BorderStyle = ccNone
        .GridLines = False
        .HotTracking = False
        .HoverSelection = False
        .LabelWrap = True
        .View = lvwReport
        .Font.name = "Tahoma": .Font.Size = 8
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , "archivo", "Nombre del archivo", 3000, lvwColumnLeft
        .ColumnHeaders.Add , "tipo", "Tipo", 1000, lvwColumnLeft
        .ColumnHeaders.Add , "propietario", "Subido por", 2000, lvwColumnLeft
        .ColumnHeaders.Add , "fecha", "Fecha", 1800, lvwColumnLeft
        .ColumnHeaders.Add , "sector", "Sector", 2200, lvwColumnLeft
        .ColumnHeaders.Add , "grupo", "Grupo", 2200, lvwColumnLeft
        .ColumnHeaders.Add , "tamanio", "Tama�o (KB)", 1200, lvwColumnRight
        ' Columnas invisibles
        .ColumnHeaders.Add , "legajo", "legajo", 0, lvwColumnLeft
        .ColumnHeaders.Add , "texto", "texto", 0, lvwColumnLeft
        .ColumnHeaders.Add , "UNC", "UNC", 0, lvwColumnLeft                         ' Uniform Naming Convention (UNC). Por ejemplo: \\Servidor\Volumen\Fichero
        .ColumnHeaders.Add , "filename", "filename", 0, lvwColumnLeft               ' Nombre del archivo como baj� a disco
        .ColumnHeaders.Add , "adj_filesystem", "adj_filesystem", 0, lvwColumnLeft   ' Ya est� en el filesystem
        .ColumnHeaders.Add , "adj_CRC32", "adj_CRC32", 0, lvwColumnLeft             ' C�digo CRC32
    End With
    flgDocumentoAlcance = False
    If sp_GetAdjuntosPet(glNumeroPeticion, Null, Null, Null) Then
        Do While Not aplRST.EOF
            Select Case ClearNull(aplRST.Fields!adj_tipo)
                Case "P950", "CG04"
                    imagen = IMAGE_MSWORD
                    flgDocumentoAlcance = True
                Case "C100", "C204", "T710"
                    imagen = IMAGE_MSEXCEL
                    flgDocumentoAlcance = True
                Case "EML1", "EML2"
                    imagen = IMAGE_EMAIL
                Case "IDH1", "IDH2", "IDH3"
                    imagen = IMAGE_HOMOLOGACION
                Case Else
                    If InStr(1, "xls|xlsx|", Right(ClearNull(aplRST.Fields!adj_file), 3), vbTextCompare) > 0 Then
                        imagen = IMAGE_MSEXCEL
                    ElseIf InStr(1, "doc|docx|", Right(ClearNull(aplRST.Fields!adj_file), 3), vbTextCompare) > 0 Then
                        imagen = IMAGE_MSWORD
                    ElseIf InStr(1, "pdf|", Right(ClearNull(aplRST.Fields!adj_file), 3), vbTextCompare) > 0 Then
                        imagen = IMAGE_ADOBEPDF
                    Else
                        imagen = IMAGE_OTHER
                    End If
            End Select
            Set subElemento = lvwAdjuntos.ListItems.Add(, , ClearNull(aplRST.Fields!adj_file), , imagen)
            tamanio = IIf(aplRST.Fields!adj_size / 1024 > 1, aplRST.Fields!adj_size / 1024, 1)
            subElemento.SubItems(1) = ClearNull(aplRST.Fields!adj_tipo)
            subElemento.SubItems(2) = ClearNull(aplRST.Fields!nom_audit)
            subElemento.SubItems(3) = ClearNull(aplRST.Fields!audit_date)
            subElemento.SubItems(4) = ClearNull(aplRST.Fields!nom_sector)
            subElemento.SubItems(5) = ClearNull(aplRST.Fields!nom_grupo)
            subElemento.SubItems(6) = tamanio
            subElemento.SubItems(7) = ClearNull(aplRST.Fields!audit_user)
            subElemento.SubItems(8) = ClearNull(aplRST.Fields!adj_texto)
            lPesoAproxDocs = lPesoAproxDocs + tamanio
            aplRST.MoveNext
            DoEvents
        Loop
        lblPesoEstDoc.Caption = "Peso total de la documentaci�n: " & lPesoAproxDocs & " KB"
    End If
    lblPesoEstDoc.visible = IIf(lPesoAproxDocs > 0, True, False)
    lvwAdjuntos.Enabled = True
End Sub

Private Sub CargarAdjPet()
    Dim lPesoAproxDocs As Long
    Call CargarAdjuntosListado
    If lvwAdjuntos.ListItems.Count > 0 Then
        Call lvwAdjuntos_ItemClick(lvwAdjuntos.ListItems(1))
    End If
    Call adjHabBtn(0, "X", True)
End Sub

Private Sub lvwAdjuntos_DblClick()
    If lvwAdjuntos.ListItems.Count > 0 Then
        If lvwAdjuntos.SelectedItem.Selected Then
            Call VisualizarAdjunto
        Else
            MsgBox "Debe seleccionar un documento", vbExclamation + vbOKOnly
        End If
    End If
End Sub

Private Sub lvwAdjuntos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    ' Si hay listado al menos un documento, se habilita el bot�n de visualizar
    If lvwAdjuntos.ListItems.Count > 0 Then
        tbAdjPet.Buttons(BOTONERA_VER).Enabled = True
    End If
    Call SetCombo(cboAdjClass, Item.ListSubItems(1), False)
    cboAdjClass.Tag = CodigoCombo(cboAdjClass, False)
    adjFile = Item
    txtAdjSector.text = Item.ListSubItems(4).text
    txtAdjGrupo.text = Item.ListSubItems(5).text
    txtAdjPropietario.text = Item.ListSubItems(2).text
    txtAdjSize.text = Item.ListSubItems(6).text
    adjComent = Item.ListSubItems(8).text
    
    If PerfilInternoActual = "SADM" Then
        tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = True
        tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = True
        GoTo Fin
    End If
    
    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) > 0 Then
        tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = False
        tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = False
        tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = False
        GoTo Fin
    End If

    If ClearNull(glLOGIN_ID_REEMPLAZO) = Item.ListSubItems(7) Or ClearNull(glLOGIN_ID_REAL) = Item.ListSubItems(7) Then
        If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
            tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = True
            tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = True
        End If
        GoTo Fin
    Else
        tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = False
        tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = False
        GoTo Fin
    End If
    If sp_GetRecursoEsHomologacion(glLOGIN_ID_REEMPLAZO) Then
        If sp_GetRecursoEsHomologacion(Item.ListSubItems(7)) Then
            tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = True
            tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = True
        End If
    End If
Fin:
    Exit Sub
End Sub

Private Sub VisualizarAdjunto()
    On Error GoTo Errores
    DoEvents
    Call Puntero(True)
    auxMensaje.prpTexto = "ATENCION" & Chr(13) & Chr(13) & Chr(13) & Chr(13) & "Si realiza modificaciones al documento" & Chr(13) & "no ser�n reflejadas en el adjunto"
    auxMensaje.prpSegundos = 2000
    auxMensaje.Disparar
    With lvwAdjuntos
        If .SelectedItem.SubItems(12) = "S" Then
            Call AbrirAdjunto(.SelectedItem.SubItems(10) & "\" & .SelectedItem.SubItems(11))
        Else
            Call PeticionAdjunto2File(glNumeroPeticion, .SelectedItem.SubItems(1), .SelectedItem.text, glWRKDIR)
            Call AbrirAdjunto(glWRKDIR & .SelectedItem.text)
            Call Puntero(False)
        End If
    End With
    Exit Sub
Errores:
    Select Case Err.Number
        Case 70     'Permiso denegado (archivo en uso, por ejemplo)
            If MsgBox("Debe ser borrada la versi�n local de este archivo (es posible que este en uso)." & vbCrLf & _
                    "Cierre el archivo. �Reintentar?", vbExclamation + vbOKCancel, "Archivo local en uso") = vbOK Then
                Resume
            Else
                Call Puntero(False)
                glPathAdjPet = ""
                Exit Sub
            End If
        Case Else
            MsgBox "Nro. error : " & Err.Number & vbCrLf & _
                   "Descripcion: " & Err.DESCRIPTION, vbCritical + vbOKOnly, "Error al visualizar el adjunto"
    End Select
End Sub

Sub adjHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    Select Case nOpcion     ' Opci�n indica una acci�n confirmativa o cancelativa (1 = confirmativa, 0 = cancelativa)
        Case 0              ' Acci�n cancelativa
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjADJ).Enabled = True
            orjBtn(orjCNF).Enabled = True
            orjBtn(orjEST).Enabled = True
            orjBtn(orjHOM).Enabled = True
            
            Call setHabilCtrl(adjFile, "DIS")
            Call setHabilCtrl(adjPath, "DIS")
            Call setHabilCtrl(adjComent, "DIS")
            Call setHabilCtrl(txtAdjSector, "DIS")
            Call setHabilCtrl(txtAdjGrupo, "DIS")
            Call setHabilCtrl(txtAdjPropietario, "DIS")
            Call setHabilCtrl(txtAdjSize, "DIS")
            Call setHabilCtrl(cboAdjClass, "DIS")
            
            tbAdjPet.Buttons(BOTONERA_ACTUALIZAR).Enabled = True
            tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_CONFIRMAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_CANCELAR).Enabled = False
            
            lblAdjPet = "": lblAdjPet.visible = False
            lvwAdjuntos.Enabled = True
            Call InicializarComboAdjuntos(False)
            
            ' 1. El perfil del usuario actual es Super Administrador
            If PerfilInternoActual = "SADM" Then
                tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = True
                tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = True
                GoTo Continuar:
            End If
            
            ' 4. Es un recurso de homologaci�n
            If glEsHomologador Or glEsSeguridadInformatica Or glEsTecnologia Then
                tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                GoTo Continuar:
            End If
            
            If lvwAdjuntos.ListItems.Count > 0 Then
                tbAdjPet.Buttons(BOTONERA_VER).Enabled = True
                If lvwAdjuntos.SelectedItem.Selected Then
                    ' El usuario es el propietario del documento
                    If ClearNull(glLOGIN_ID_REEMPLAZO) = lvwAdjuntos.SelectedItem.ListSubItems(7) Or _
                        ClearNull(glLOGIN_ID_REAL) = lvwAdjuntos.SelectedItem.ListSubItems(7) Then
                            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) = 0 Then
                                tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                                tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = True
                                tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = True
                            End If
                            GoTo Continuar:
                    End If
                End If
            End If
            
            ' 2. El perfil del usuario actual es BP y es el BP de la petici�n
            If (PerfilInternoActual = "BPAR" And ClearNull(sBpar) = ClearNull(glLOGIN_ID_REEMPLAZO)) Then
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) = 0 Then
                    tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                End If
                GoTo Continuar:
            Else
                If EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) = 0 Then
                        tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                    End If
                    GoTo Continuar:
                End If
            End If
            ' 3. El usuario actual es Ejecutor (esta contenido en alguno de los sectores involucrados a la petici�n)
            If InEjecutor Then
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) = 0 Then
                    tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                End If
                GoTo Continuar:
            End If
            If sp_GetAccionPerfil("ADJADD", glUsrPerfilActual, sEstado, Null, Null) Then
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) = 0 Then
                    tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                End If
            End If
Continuar:
            adjComent.Locked = True
            If IsMissing(vCargaGrid) Then
                Call CargarAdjPet
            End If
        Case 1      ' Acci�n confirmativa
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjADJ).Enabled = False
            orjBtn(orjCNF).Enabled = False
            orjBtn(orjEST).Enabled = False
            orjBtn(orjHOM).Enabled = False
            fraButtPpal.Enabled = False
            lvwAdjuntos.Enabled = False
            
            tbAdjPet.Buttons(BOTONERA_VER).Enabled = False
            tbAdjPet.Buttons(BOTONERA_ADJUNTAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_ACTUALIZAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_EDITAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_QUITAR).Enabled = False
            tbAdjPet.Buttons(BOTONERA_CONFIRMAR).Enabled = True
            tbAdjPet.Buttons(BOTONERA_CANCELAR).Enabled = True
            
            Select Case sOpcionSeleccionada
                Case "A"
                Case "M"
                    lblAdjPet = " MODIFICAR ": lblAdjPet.visible = True
                    fraAdjPet.Enabled = True
                    adjComent.Locked = False
                    Call setHabilCtrl(adjComent, "NOR")
                    cboAdjClass.Tag = CodigoCombo(cboAdjClass, False)
                    If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
                        'cboAdjClass.Tag = CodigoCombo(cboAdjClass, False)
                        Call InicializarComboAdjuntos(True)
                        Call setHabilCtrl(cboAdjClass, "NOR")
                    End If
                    adjComent.SetFocus
                Case "E"
                    lblAdjPet = " DESVINCULAR ": lblAdjPet.visible = True
                    fraAdjPet.Enabled = False
            End Select
    End Select
    adjOpcion = sOpcionSeleccionada
End Sub

'{ add -064- a.
Private Sub CargarEstado()
    Dim i As Long
    Dim bPintar As Boolean
    
    bPintar = False
    Call Debuggear(Me, "CargarEstado()")
    
    If InStr(1, "MEDIO|", glLOGIN_Direccion, vbTextCompare) > 0 Or InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) > 0 Then     ' add -064- c.
        Call Puntero(True)
        If sp_GetVarios("CTRLPRD1") Then
            chkControl1.value = IIf(ClearNull(aplRST.Fields!var_numero) = "1", 1, 0)
        End If
        
        With grdControl
            .visible = False
            .Clear
            .BackColorBkg = Me.BackColor
            .BackColorSel = RGB(58, 110, 165)
            .HighLight = flexHighlightWithFocus
            .Rows = 1           '3
            .cols = 5
            .TextMatrix(0, 0) = "Nivel": .ColWidth(0) = 900: .ColAlignment(0) = flexAlignLeftCenter
            .TextMatrix(0, 1) = "N�": .ColWidth(1) = 400: .ColAlignment(1) = flexAlignLeftCenter
            .TextMatrix(0, 2) = "Control": .ColWidth(2) = 4800: .ColAlignment(2) = flexAlignLeftCenter
            .TextMatrix(0, 3) = "Observaciones": .ColWidth(3) = 5550: .ColAlignment(3) = flexAlignLeftCenter
            .TextMatrix(0, 4) = "Estado": .ColWidth(4) = 1000: .ColAlignment(4) = flexAlignCenterCenter
            .RowHeightMin = 270
            .Font.name = "Tahoma": .Font.Size = 8
            Call CambiarEfectoLinea(grdControl, prmGridEffectFontBold)
        End With
        If sp_GetControlPeticion(glNumeroPeticion, glLOGIN_ID_REEMPLAZO, "1") Then
            Do While Not aplRST.EOF
                With grdControl
                    i = i + 1
                    .Rows = .Rows + 1
                    If .Rows Mod 2 > 0 Then Call PintarLinea(grdControl, prmGridFillRowColorLightGrey2)
                    .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST!nivel)
                    .TextMatrix(.Rows - 1, 1) = i
                    .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!Control)
                    .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!observaciones)
                    .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST!Estado)
                    If InStr(1, "Pendiente", .TextMatrix(.Rows - 1, 4), vbTextCompare) > 0 Then
                        Call PintarLinea(grdControl, prmGridFillRowColorRed)
                    ElseIf InStr(1, "-", .TextMatrix(.Rows - 1, 4), vbTextCompare) > 0 Then
                        Call CambiarForeColorLinea(grdControl, prmGridFillRowColorDarkGrey)
                    End If
                    If ClearNull(aplRST!Codigo) = 99 Then
                        Call CambiarEfectoLinea(grdControl, prmGridEffectFontBold)
                        If InStr(1, "V�lida", .TextMatrix(.Rows - 1, 4), vbTextCompare) > 0 Then
                            Call PintarLinea(grdControl, prmGridFillRowColorGreen)
                        End If
                    End If
                    aplRST.MoveNext
                    DoEvents
                End With
            Loop
        End If
        grdControl.visible = True
        Call CambiarEfectoLinea(grdControl, prmGridEffectFontBold)
        Call Puntero(False)
    End If
    Call ValidarPeticion
End Sub
'}

Private Sub pcfSeleccion()
    If InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) > 0 Then
        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
        tbConformes.Buttons(BOTONERACONF_EDITAR).Enabled = True
        tbConformes.Buttons(BOTONERACONF_QUITAR).Enabled = True
        tbConformes.Buttons(BOTONERACONF_DESHABILITAR).Enabled = True
        GoTo Continuar
    Else
        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = False
        tbConformes.Buttons(BOTONERACONF_EDITAR).Enabled = False
        tbConformes.Buttons(BOTONERACONF_QUITAR).Enabled = False
        tbConformes.Buttons(BOTONERACONF_DESHABILITAR).Enabled = False
    End If

    If glEsHomologador Then
        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
        tbConformes.Buttons(BOTONERACONF_DESHABILITAR).Enabled = True
        If lvwConformes.ListItems.Count > 0 Then
            If InStr(1, "SOB|OMA|OME|", lvwConformes.SelectedItem.SubItems(3), vbTextCompare) > 0 Then
                tbConformes.Buttons(BOTONERACONF_EDITAR).Enabled = True
                tbConformes.Buttons(BOTONERACONF_QUITAR).Enabled = True
            End If
        End If
        GoTo Continuar
    End If
    
    If glEsTecnologia Then
        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
        If lvwConformes.ListItems.Count > 0 Then
            If InStr(1, "AROK|AROB|ARRE|", lvwConformes.SelectedItem.SubItems(3), vbTextCompare) > 0 Then
                tbConformes.Buttons(BOTONERACONF_EDITAR).Enabled = True
                tbConformes.Buttons(BOTONERACONF_QUITAR).Enabled = True
            End If
        End If
        GoTo Continuar
    End If
    
    If glEsSeguridadInformatica Then
        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
        If lvwConformes.ListItems.Count > 0 Then
            If InStr(1, "SIOK|SIOB|SIRE|", lvwConformes.SelectedItem.SubItems(3), vbTextCompare) > 0 Then
                tbConformes.Buttons(BOTONERACONF_EDITAR).Enabled = True
                tbConformes.Buttons(BOTONERACONF_QUITAR).Enabled = True
            End If
        End If
        GoTo Continuar
    End If
    
    '{ add -106- a.
    Select Case CodigoCombo(cboTipopet, True)
        Case "PRO"
            Select Case PerfilInternoActual
                Case "CSEC"
                    ' Si el Responsable de Sector pertenece a la gerencia solicitante de la petici�n, que debe ser "Gerencia de Sistemas" y
                    ' el responsable no es ni el solicitante ni el usuario que di� el alta de la petici�n, entonces puede dar el OK
                    If glLOGIN_Gerencia = xGer Then
                        If xGer = "DESA" Then                                                   ' Mientras el usuario NO sea el mismo que di� el alta de la petici�n
                             If glLOGIN_ID_REEMPLAZO <> sUsualta Then                           ' O el solicitante mismo de la petici�n... entonces puede dar conformes
                                If glLOGIN_ID_REEMPLAZO <> sSoli Then
                                    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
                                        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
                                    End If
                                End If
                            End If
                        Else
                            If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
                                tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
                            End If
                        End If
                    End If
                    GoTo Continuar
                Case "CGRU"
                    ' Si el Responsable de Ejecuci�n pertenece a alguno de los grupos ejecutores de la petici�n,
                    ' y la clase es CORR, SPUF o ATEN, entonces puede dar el OK
                    If Not sp_GetPeticionGrupo(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo) Then
                        Exit Sub
                    End If
                    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
                        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
                    End If
                    GoTo Continuar
            End Select
        Case Else
            If (flgSolicitor Or flgSolicitorVinc) Then
                If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
                    tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
                End If
                GoTo Continuar
            End If
            ' El perfil del usuario es BP y es el BP de la petici�n
            If (PerfilInternoActual = "BPAR" And ClearNull(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Then
                If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
                    tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
                End If
                GoTo Continuar
            End If
            ' Si es una "Atenci�n de usuario", el responsable de ejecuci�n puede dar el OK.
            If PerfilInternoActual = "CGRU" Then
                If Not sp_GetPeticionGrupo(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo) Then
                    GoTo Continuar
                End If
                If CodigoCombo(cboClase, True) = "ATEN" Then
                    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
                        tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = True
                    End If
                    GoTo Continuar
                End If
            End If
    End Select
Continuar:
    Exit Sub
End Sub

Private Sub grdPetConf_Click()
    Call pcfSeleccion
    DoEvents
End Sub

Private Sub grdPetConf_DblClick()
    Call pcfSeleccion
    DoEvents
End Sub

Private Sub grdPetConf_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdPetConf_Click
    End If
End Sub

Private Sub pcfCan_Click()
    pcfTexto = ""
    Call pcfHabBtn(0, "X")
End Sub

Private Sub pcfCon_Click()
    Call ConfirmarConformes
End Sub

Private Sub ConfirmarConformes()
    Dim sPcfTipo As String
    Dim sPcfModo As String
    Dim cod_evento As String
    Dim bHomologacion_EstadoActivo As Boolean
    Dim bExistenGruposHomologables As Boolean
    Dim bOKLider_Homologacion As Boolean
    Dim bOK_SI As Boolean
    Dim iCantidad_Conformes As Integer
    Dim iCantidad_Respaldos As Integer
    Dim NroHistorial As Long
    Dim NuevoEstadoPeticion As String
    Dim sufijo As String
    Dim tipoconforme As String 'GMT03
    
    tipoconforme = CodigoCombo(cboPcfTipo, True) 'GMT03
  ' Select Case CodigoCombo(cboPcfTipo, True) ->  GMT03
    Select Case tipoconforme                     'GMT03
        Case "ALC"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = CodigoCombo(cboPcfTipo, True) & IIf(sufijo = "P", "P", "A")
            cod_evento = "OK" & sPcfTipo
        Case "TES"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = CodigoCombo(cboPcfTipo, True) & IIf(sufijo = "P", "P", "T")
            cod_evento = "OK" & sPcfTipo
        Case "DAT"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = CodigoCombo(cboPcfTipo, True) & sufijo
            cod_evento = sPcfTipo
        Case "OKP"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = CodigoCombo(cboPcfTipo, True) & sufijo
            cod_evento = sPcfTipo
        Case "OMA", "OME", "SOB"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = "H" & CodigoCombo(cboPcfTipo, True) & "." & sufijo
            cod_evento = IIf(pcfOpcion = "E", "V.", "") & sPcfTipo
            sPcfSecuencia = 1
        Case "AROK", "AROB", "ARRE"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = CodigoCombo(cboPcfTipo, True) & "." & sufijo
            cod_evento = "OKARQ1"
        Case "SIOK", "SIOB", "SIRE"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = CodigoCombo(cboPcfTipo, True) & "." & sufijo
            cod_evento = "OKSEG1"
    'GMT03 - INI
        Case "ADA"
            sufijo = CodigoCombo(cboPcfClase, True)
            sPcfTipo = CodigoCombo(cboPcfTipo, True) & sufijo
            cod_evento = "OK" & sPcfTipo
    'GMT03 - FIN
    End Select
    sPcfModo = CodigoCombo(cboPcfModo, True)
    
   'If CamposObligatoriosConformes() Then 'GMT03
    If CamposObligatoriosConformes(tipoconforme, sufijo) Then 'GMT03
        ' Inicializaci�n de variables
        bHomologacion_EstadoActivo = False
        bExistenGruposHomologables = False
        
        ' Quienes puede otorgar conformes
        '- Responsables de sector de la direcci�n de MEDIOs de peticiones tipo PROpias
        '- Responsables de sector o grupo de la gerencia de DESA para peticiones de clase CORR, SPUF, ATEN y OPTI
        '- Solicitantes de la petici�n
        '- Superadministradores

        Select Case pcfOpcion
            Case "A"
             '  If InStr(1, "ALCP|ALCA|TESP|TEST|", sPcfTipo, vbTextCompare) > 0 Then  GMT03
                If InStr(1, "ALCP|ALCA|TESP|TEST|ADAF|", sPcfTipo, vbTextCompare) > 0 Then 'GMT03
                    ' No valida si:
                    ' - Es el administrador/superadministrador
                    ' - Es un BP o Resp. de Sector
                    ' - No es el Solicitante de la petici�n o un usuario asignado al grupo ejecutor funcional (vinculadas)
                    ' - No es una petici�n de clase: CORR, SPUF, ATEN o OPTI
                    If InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) = 0 Then      '
                        If InStr(1, "CSEC|BPAR|", PerfilInternoActual, vbTextCompare) = 0 Then  '
                            If (Not flgSolicitor) And (Not flgSolicitorVinc) Then
                                If InStr(1, "CORR|SPUF|ATEN|OPTI|", sClasePet, vbTextCompare) = 0 Then
                                    MsgBox "Solo pueden otorgar conformes los usuarios" & vbCrLf & "solicitantes o Referentes de Sistema", vbOKOnly + vbInformation
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                End If
                
                Select Case Left(sPcfTipo, 4)
                    Case "ALCP", "ALCA"         ' Solo conformes de alcance
                        ' ******************************************************************************************
                        ' x. Lideres de DYD no pueden otorgar conformes a la documentaci�n de alcance
                        ' ******************************************************************************************
                        'If InStr(1, "CORR|SPUF|ATEN|OPTI|", sClasePet, vbTextCompare) > 0 Then
                        If InStr(1, "CORR|SPUF|ATEN|", sClasePet, vbTextCompare) > 0 Then
                            If PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = "DESA" Then
                                MsgBox "No puede otorgar conformes de alcance a esta clase de petici�n, pero si" & vbCrLf & _
                                        "conformes de usuario (con la documentaci�n respaldatoria correspondiente).", vbOKOnly + vbInformation
                                Exit Sub
                            End If
                            'If PerfilInternoActual = "CSEC" And sClasePet = "OPTI" Then
                            If InStr(1, "CSEC|CGRU|", PerfilInternoActual, vbTextCompare) > 0 And sClasePet = "OPTI" Then
                                If Not sp_GetPeticionSector(glNumeroPeticion, glLOGIN_Sector) Then
                                    MsgBox "Solo responsables de sector/ejecuci�n vinculados a la" & vbCrLf & _
                                            "petici�n pueden otorgar conformes a la documentaci�n de alcance.", vbOKOnly + vbInformation
                                    Exit Sub
                                End If
                            End If
                        End If
                        If (sPcfTipo = "ALCA" And sPcfModo = "REQ") Or (sPcfTipo = "ALCP") Then
                            If sTipoPet = "PRJ" Or sImpacto = "S" Then
                                If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                                    MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                                    Exit Sub
                                End If
                            Else
                                ' Chequea que ya este adjunto alg�n documento de alcance
                                If InStr(1, "EVOL|OPTI|NUEV|", sClasePet, vbTextCompare) > 0 Then
                                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
                                        MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                                        Exit Sub
                                    End If
                                End If
                            End If
                            ' Solicita el respaldo antes de permitir el ok por parte de un relacionado directo a la petici�n
                            If InStr(1, "PRJ|NOR|ESP|AUI|AUX|", sTipoPet, vbTextCompare) > 0 Then
                                If Not flgSolicitor Then
                                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML1", Null, Null) Then
                                        MsgBox "Falta adjuntar el mail de respaldo del" & vbCrLf & "usuario para el OK de alcance (EML1).", vbOKOnly + vbInformation
                                        Exit Sub
                                    End If
                                End If
                            End If
                            If sPcfTipo = "ALCP" Then
                                If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
                                    MsgBox "La petici�n ya posee un conforme final a la documentaci�n" & vbCrLf & _
                                           "de alcance. No puede otorgar conformes parciales posteriores.", vbOKOnly + vbExclamation
                                    Exit Sub
                                End If
                            End If
                        End If
                    Case "TESP", "TEST"
                        If PerfilInternoActual <> "SADM" Then
                            If sEstado <> "EJECUC" And (sEstado = "SUSPEN" And Not bEstuvoEnEjecucion) Then
                                MsgBox "Solo puede otorgar un conforme si la petici�n est� en" & vbCrLf & "ejecuci�n o est� suspendida pero estuvo en ejecuci�n."
                                Exit Sub
                            End If
                        End If
                        ' ******************************************************************************************
                        ' x. Solo EVOL,  NUEV y OPTI reciben conformes de pruebas de usuario parcial
                        ' ******************************************************************************************
                        If sPcfTipo = "TESP" Then
                            If InStr(1, "CORR|SPUF|ATEN|", sClasePet, vbTextCompare) > 0 Then
                                MsgBox "Peticiones de clase Correctivo, Atenci�n a Usuarios y Spufis" & vbCrLf & _
                                        "no pueden recibir conformes de pruebas de usuario parciales.", vbOKOnly + vbExclamation
                                Exit Sub
                            End If
                        End If

                        ' ******************************************************************************************
                        ' x. Confronta la cantidad de respaldos a los conformes alternativos
                        ' ******************************************************************************************
                        If InStr(1, "CORR|SPUF|ATEN|OPTI|", sClasePet, vbTextCompare) > 0 Then
                            If (PerfilInternoActual = "CGRU" Or flgSolicitorVinc) And glLOGIN_Gerencia = "DESA" Then
                                iCantidad_Respaldos = 0
                                iCantidad_Conformes = 0
                                If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML2", Null, Null) Then
                                    MsgBox "Debe adjuntar un documento de tipo EML2 para" & vbCrLf & _
                                            "respaldar el conforme a las pruebas de usuario.", vbOKOnly + vbExclamation
                                    Exit Sub
                                Else
                                    ' Cuento la cantidad de respaldos
                                    Do While Not aplRST.EOF
                                        iCantidad_Respaldos = iCantidad_Respaldos + 1
                                        aplRST.MoveNext
                                        DoEvents
                                    Loop
                                    ' Cuento la cantidad de conformes de tipo "Alternativos" (L�deres DYD)
                                    If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
                                        Do While Not aplRST.EOF
                                            If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                                                iCantidad_Conformes = iCantidad_Conformes + 1
                                            End If
                                            aplRST.MoveNext
                                            DoEvents
                                        Loop
                                        ' Comparo las cantidades contadas
                                        If iCantidad_Conformes >= iCantidad_Respaldos Then
                                            ' Los conformes superan a los respaldos
                                            MsgBox "No hay suficientes respaldos (EML2) para la cantidad de conformes a otorgar." & vbCrLf & _
                                                    "Revise en la pesta�a Adjuntos.", vbOKOnly + vbExclamation
                                            Exit Sub
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        ' Si usuario actual tiene perfil de BP de la petici�n actual
                        ' valida que exista el mail de OK a las Pruebas de Usuario
                        ' TODO: esto se puede mejorar habilitando un flag para los EML1 y EML2
                        If InStr(1, "PRJ|NOR|ESP|AUI|AUX|", sTipoPet, vbTextCompare) > 0 Then
                            If Not flgSolicitor Then
                                If sPcfTipo = "TESP" Then
                                    If sp_GetPeticionConf(glNumeroPeticion, "TESP", Null) Then
                                        MsgBox "La petici�n ya posee un conforme final a las pruebas" & vbCrLf & _
                                               "de usuario. No puede otorgar conformes parciales posteriores.", vbOKOnly + vbExclamation
                                        Exit Sub
                                    End If
                                End If
                                If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML2", Null, Null) Then
                                    MsgBox "Debe adjuntar un documento de tipo EML2 para respaldar" & vbCrLf & _
                                            "el otorgamiento del conforme a las pruebas del usuario.", vbOKOnly + vbExclamation
                                    Exit Sub
                                End If
                            End If
                        End If
                    Case "DATP", "DATF"
                        If InStr(1, "CORR|", sClasePet, vbTextCompare) > 0 Then
                            If InStr(1, "SADM|CGRU|CSEC|", PerfilInternoActual, vbTextCompare) > 0 Then
                                If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML3", Null, Null) Then
                                    MsgBox "Debe adjuntar un documento de tipo EML3 para" & vbCrLf & _
                                            "respaldar el conforme a los datos de usuario.", vbOKOnly + vbExclamation
                                    Exit Sub
                                End If
                            End If
                        End If
                    Case "OKPP", "OKPF"
                        ' TODO Revisar todo esta rutina...
                        Dim bResponsableHabilitado As Boolean
                        
                        bResponsableHabilitado = False
                        ' Primero evalua si el usuario actua como Resp. de Sector
                        If Not PerfilInternoActual = "SADM" Then
                            If PerfilInternoActual <> "CSEC" Then
                                MsgBox "Solo responsables de sector de la gerencia de Sistemas" & vbCrLf & _
                                        "involucrados en la petici�n pueden agregar este tipo de conforme.", vbOKOnly + vbExclamation
                                Exit Sub
                            End If
                        End If
                        ' Antes se evalua si existe un conforme de homologaci�n de tipo HOMA
                        If Not bExisteHOMA Then
                            MsgBox "No es necesario este conforme (la petici�n no tiene OMA).", vbOKOnly + vbExclamation
                            Exit Sub
                        End If
                        ' Debe ser y actuar como un responsable de sector
                        If Not PerfilInternoActual = "SADM" Then
                            If PerfilInternoActual = "CSEC" Then
                                ' Si es Responsable de Sector, veo que lo sea de alguno de los sectores de la petici�n
                                If sp_GetPeticionSector(glNumeroPeticion, Null) Then
                                    Do While Not aplRST.EOF
                                        If glLOGIN_Sector = ClearNull(aplRST!cod_sector) Then
                                            bResponsableHabilitado = True
                                            Exit Do
                                        End If
                                        aplRST.MoveNext
                                        DoEvents
                                    Loop
                                    If Not bResponsableHabilitado Then
                                        MsgBox "Solo responsables de sector de las gerencias involucrados" & vbCrLf & _
                                                "en la petici�n pueden agregar este tipo de conforme.", vbOKOnly + vbExclamation
                                        Exit Sub
                                    End If
                                Else
                                    MsgBox "El Responsable actual no pertenece a ninguno de los sectores ejecutantes de la petici�n." & vbCrLf & vbCrLf & _
                                            "No puede agregar este tipo de conforme.", vbOKOnly + vbExclamation
                                    Exit Sub
                                End If
                            Else
                                MsgBox "Solo responsables de sector involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & _
                                        "El usuario actual no actua como Responsable de Sector.", vbOKOnly + vbExclamation
                                Exit Sub
                            End If
                        End If
                    'GMT03 - INI
                    ' OK FINAL ADA
                    Case "ADAF"
                        If sp_GetPeticionConf(glNumeroPeticion, "ADAF", Null) Then
                            MsgBox "La petici�n ya posee un conforme final de ADA", vbOKOnly + vbExclamation
                            Exit Sub
                        End If
                        
                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "ADA", Null, Null) Then
                            MsgBox "La petici�n no posee un documento de ADA(solapa 'Adjuntos')", vbOKOnly + vbInformation
                            Exit Sub
                        End If
                    'GMT03 - FIN
                    
                    ' La parte de homologaci�n
                    Case "HOMA.P", "HOMA.F", "HOME.P", "HOME.F", "HSOB.P", "HSOB.F"
                    
                    ' La parte de Seguridad Inform�tica y Tecnolog�a
                    Case "SIOK", "SIOB", "SIRE", "AROK", "AROB", "ARRE"
                    
                End Select
                
                If MsgBox(constConfirmMsgAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then
                    If sPcfTipo = "TESP" Then
                        sPcfModo = IIf(Not (glLOGIN_Gerencia = "DESA" And PerfilInternoActual = "CGRU"), "EXC", "ALT")
                        sPcfSecuencia = "0"
                    ElseIf sPcfTipo = "ALCP" Then
                        sPcfModo = "EXC"
                        sPcfSecuencia = "0"
                    Else
                        sPcfSecuencia = "1"
                    End If
                    
                    'SE INSERTA CONFORME EN LAS TABLAS
                    Call sp_InsertPeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, ClearNull(pcfTexto.text), sPcfSecuencia, glLOGIN_ID_REEMPLAZO)
                    Call sp_AddHistorial(glNumeroPeticion, cod_evento, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, ClearNull(pcfTexto.text))
                    
                    If InStr(1, "TESP|TEST|OKPP|OKPF|", sPcfTipo, vbTextCompare) > 0 Then
                        If sHomoGrupoEstado <> "EJECUC" Then
                            If Not InStr(1, "RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(sHomoGrupoEstado), vbTextCompare) > 0 Then   ' Si homologaci�n est� activo
                                Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, glHomologacionGrupo, "EJECUC", "", "")                              ' Se envian los mensajes correspondientes a Responsables del Grupo "Homologador"
                                Call sp_UpdatePetSubField(glNumeroPeticion, glHomologacionGrupo, "ESTADO", "EJECUC", Null, Null)                          ' Entonces cambia el estado del grupo homologador a "En ejecuci�n"
                                sHomoSectorEstadoNuevo = getNuevoEstadoSector(glNumeroPeticion, glHomologacionSector)                         ' Actualiza el estado del Sector del grupo Homologador
                                NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", sEstado, glHomologacionSector, sHomoSectorEstadoNuevo, glHomologacionGrupo, "EJECUC", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� EN EJECUCION � Cambio autom�tico de estado del Grupo Homologador.") ' Se registra en el historial el evento autom�tico de cambio de estado del grupo Homologador
                                Call sp_UpdatePeticionGrupo(glNumeroPeticion, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, "EJECUC", date, "", NroHistorial, 0)
                                Call sp_UpdatePeticionSector(glNumeroPeticion, glHomologacionSector, Null, Null, Null, Null, 0, sHomoSectorEstadoNuevo, date, "", NroHistorial, 0)
                                bHomologacion_EstadoActivo = True
                            End If
                        Else
                            If InStr(1, "RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(sHomoGrupoEstado), vbTextCompare) = 0 Then
                                bHomologacion_EstadoActivo = True
                            End If
                        End If
                        ' TODO: REVISAR ESTO!!!
                        ' Agrega un mensaje para el grupo Homologador (independientemente del tipo de conforme)
                        If sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then
                            Select Case sPcfTipo
                                Case "ALCA", "ALCP": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 100, sEstado, Null)
                                Case "TEST", "TESP": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 101, sEstado, Null)
                                Case "OKPP", "OKPF": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 102, sEstado, Null)
                                Case "DATP", "DATF": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 103, sEstado, Null)
                            End Select
                        End If
                    End If
                    Call pcfHabBtn(0, "X")
                    ' Debo evaluar si debo "reactivar" al grupo Homologador
                    ' TODO: 18.10.2016 Ac� hay que ver si por cambio de clase debe ser reactivado el grupo homologador. Ojo!
                    If InStr(1, "CORR|SPUF|ATEN|OPTI|", sClasePet, vbTextCompare) > 0 Then
                        If PerfilInternoActual = "CGRU" Then
                            If glLOGIN_Gerencia = "DESA" Then
                                If InStr(1, "TESP|TEST|", sPcfTipo, vbTextCompare) > 0 Then
                                    If sp_GetPeticionGrupo(glNumeroPeticion, glHomologacionSector, glHomologacionGrupo) Then
                                        ' Si homologaci�n esta en alguno de los estados terminales, lo "reactivo"
                                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                                            Call sp_UpdatePetSubField(glNumeroPeticion, glHomologacionGrupo, "ESTADO", "EJECUC", Null, Null)
                                            ' Actualizo el estado del sector de Homologaci�n (si corresponde)
                                            sHomoSectorEstado = getNuevoEstadoSector(glNumeroPeticion, glHomologacionSector)
                                            Call sp_UpdatePeticionSector(glNumeroPeticion, glHomologacionSector, Null, Null, Null, Null, 0, sHomoSectorEstado, date, "", 0, 0)
                                            NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", "EJECUC", glHomologacionSector, sHomoSectorEstado, glHomologacionGrupo, "EJECUC", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� EN EJECUCION � Cambio autom�tico de estado del Grupo Homologador por conforme de L�der.")
                                            Call sp_UpdatePetSubField(glNumeroPeticion, glHomologacionGrupo, "HSTSOL", Null, Null, NroHistorial)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                    Call ControlesPasajeProduccion(sClasePet, sEstado, bEstuvoEnEjecucion)
                    Call CargarEstado
                    Call ValidarPeticion
                End If
            Case "M"
                Call sp_UpdatePeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, ClearNull(pcfTexto.text), lvwConformes.SelectedItem.SubItems(9))
                Call pcfHabBtn(0, "X")
            Case "E"
                If MsgBox(constConfirmMsgDeAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then
                    sPcfSecuencia = lvwConformes.SelectedItem.SubItems(9)
                    If sp_DeletePeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, sPcfSecuencia) Then
                        cod_evento = "N" + cod_evento
                        Call sp_AddHistorial(glNumeroPeticion, cod_evento, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "Eliminaci�n " & TextoCombo(cboPcfModo, sPcfModo, True) & ": " & pcfTexto.text)
                        ' TODO: revisar esta l�nea...
                        'If InStr(1, "TESP|TEST|ALCP|ALCA|", sPcfTipo, vbTextCompare) > 0 Then Call sp_DeletePeticionEnviadas(glNumeroPeticion)
                    End If
                    ' TODO: agregado nuevo 12.10.2016
                    Call ControlesPasajeProduccion(sClasePet, sEstado, bEstuvoEnEjecucion)
                    Call ValidarPeticion
                    pcfTexto = ""
                    Call pcfHabBtn(0, "X")
                    Call CargarEstado
                End If
            Case Else
                pcfTexto = ""
                Call pcfHabBtn(0, "X")
        End Select
    End If
End Sub

' TODO: quizas renombrar a ValidacionConformesHomologacion
Private Sub ControlesConformesHomologacion(cTipoHomo As String)
    Dim sConformes As String
    Dim bOK_SI As Boolean
    Dim bOKLider As Boolean
    Dim bOKLider_Homologacion As Boolean
    Dim bOK_PasajeProduccion As Boolean

    bOK_SI = False
    bOKLider = False
    bOKLider_Homologacion = False
    bOK_PasajeProduccion = False
    sConformes = ""
    
    ' La l�gica de agregado de conformes de homologaci�n aqu�...
    Select Case pcfOpcion
        Case "A"
            Select Case sClasePet
                Case "EVOL", "NUEV", "OPTI"
                    ' Cargo los conformes que tuviera la petici�n
                    sConformes = ""
                    If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
                        Do While Not aplRST.EOF
                            sConformes = sConformes & ClearNull(aplRST.Fields!ok_tipo) & "|"
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If
                    If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
                        If InStr(1, sConformes, "OK", vbTextCompare) > 0 Then
                            If bOK_SI Then
                                bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                            Else
                                bOK_PasajeProduccion = True
                            End If
                        Else
                            bOK_PasajeProduccion = False
                        End If
                    Else
                        If InStr(1, sConformes, "TES", vbTextCompare) > 0 Then
                            If bOK_SI Then
                                bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                            Else
                                bOK_PasajeProduccion = True
                            End If
                        End If
                    End If
                Case Else
                    If bOKLider_Homologacion Then
                        bOKLider = False
                        If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
                            Do While Not aplRST.EOF
                                If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                                    bOKLider = True
                                    Exit Do
                                End If
                                aplRST.MoveNext
                                DoEvents
                            Loop
                        End If
                        If bOKLider Then
                            If InStr(1, sConformes, "HOME", vbTextCompare) > 0 Or _
                                InStr(1, sConformes, "HSOB", vbTextCompare) > 0 Then
                                If bOK_SI Then
                                    bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                Else
                                    bOK_PasajeProduccion = True
                                End If
                            Else
                                bOK_PasajeProduccion = False
                            End If
                        End If
                    End If
                    If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
                        If InStr(1, sConformes, "OK", vbTextCompare) > 0 Then
                            If bOK_SI Then
                                bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                            Else
                                bOK_PasajeProduccion = True
                            End If
                        Else
                            bOK_PasajeProduccion = False
                        End If
                    Else
                        If bOK_SI Then
                            bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                        Else
                            bOK_PasajeProduccion = True
                        End If
                    End If
            End Select
            If bOK_PasajeProduccion Then
                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, sClasePet, "", "", glLOGIN_ID_REAL, "02")
            End If
            Call ValidarPeticion
        Case "E"
            sConformes = ""
            If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
                Do While Not aplRST.EOF
                    sConformes = sConformes & ClearNull(aplRST.Fields!ok_tipo) & "|"
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
                bOK_PasajeProduccion = False
            Else
                If InStr(1, "CORR|ATEN|SPUF|", sClasePet) > 0 Then
                    If bOKLider_Homologacion Then
                        bOKLider = False
                        If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
                            Do While Not aplRST.EOF
                                If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                                    bOKLider = True
                                    Exit Do
                                End If
                                aplRST.MoveNext
                                DoEvents
                            Loop
                        End If
                        If bOKLider Then
                            If InStr(1, sConformes, "HOME", vbTextCompare) > 0 Or _
                                InStr(1, sConformes, "HSOB", vbTextCompare) > 0 Then
                                If bOK_SI Then
                                    bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                Else
                                    bOK_PasajeProduccion = True
                                End If
                            Else
                                bOK_PasajeProduccion = False
                            End If
                        Else
                            If InStr(1, sConformes, "TES", vbTextCompare) > 0 Then
                                If bOK_SI Then
                                    bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                Else
                                    bOK_PasajeProduccion = True
                                End If
                            Else            ' Si no hay conforme de Usuario, entonces no puede ser v�lida
                                bOK_PasajeProduccion = False
                            End If
                        End If
                    Else
                        If InStr(1, sConformes, "TES", vbTextCompare) > 0 Then
                            If bOK_SI Then
                                bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                            Else
                                bOK_PasajeProduccion = True
                            End If
                        Else            ' Si no hay conforme de Usuario, entonces no puede ser v�lida
                            bOK_PasajeProduccion = False
                        End If
                    End If
                Else
                    If InStr(1, sConformes, "HOME", vbTextCompare) > 0 Or _
                        InStr(1, sConformes, "HSOB", vbTextCompare) > 0 Then
                        If bOK_SI Then
                            bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                        Else
                            bOK_PasajeProduccion = True
                        End If
                    Else
                        bOK_PasajeProduccion = False
                    End If
                End If
            End If
            If bOK_PasajeProduccion Then
                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, sClasePet, "", "", glLOGIN_ID_REAL, "02")
            Else
                Call sp_DeletePeticionChangeMan(glNumeroPeticion)
                Call sp_DeletePeticionEnviadas(glNumeroPeticion)
            End If
            Call ValidarPeticion
    End Select
End Sub

Sub pcfHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    If sClasePet = "SINC" Then
        MsgBox "No puede dar conformes a la petici�n" & vbCrLf & "antes que �sta sea clasificada.", vbInformation + vbOKOnly, "Clasificaci�n requerida"
        Exit Sub
    End If
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjADJ).Enabled = True
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).Enabled = True
            End If 'GMT05
            orjBtn(orjEST).Enabled = True
            orjBtn(orjHOM).Enabled = True
            lblPetConf = "": lblPetConf.visible = False
            lvwConformes.Enabled = True
            Call setHabilCtrl(cboPcfTipo, "DIS")
            Call setHabilCtrl(cboPcfClase, "DIS")
            Call setHabilCtrl(cboPcfModo, "DIS")
            Call setHabilCtrl(pcfTexto, "DIS")
            
            tbConformes.Buttons(BOTONERACONF_ACTUALIZAR).Enabled = True
            tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = False
            tbConformes.Buttons(BOTONERACONF_EDITAR).Enabled = False
            tbConformes.Buttons(BOTONERACONF_QUITAR).Enabled = False
            tbConformes.Buttons(BOTONERACONF_CONFIRMAR).Enabled = False
            tbConformes.Buttons(BOTONERACONF_CANCELAR).Enabled = False
            tbConformes.Buttons(BOTONERACONF_DESHABILITAR).Enabled = False
            pcfOpcion = sOpcionSeleccionada
            
            If IsMissing(vCargaGrid) Then
                Call CargarConformes
            Else
                InicializarConformes ("1")
                Call pcfSeleccion
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjADJ).Enabled = False
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).Enabled = False
            End If ' GMT05
            orjBtn(orjEST).Enabled = False
            orjBtn(orjHOM).Enabled = False      ' add -109- a.
            fraButtPpal.Enabled = False
            lvwConformes.Enabled = False
            
            tbConformes.Buttons(BOTONERACONF_ACTUALIZAR).Enabled = False
            tbConformes.Buttons(BOTONERACONF_CONFORME).Enabled = False
            tbConformes.Buttons(BOTONERACONF_EDITAR).Enabled = False
            tbConformes.Buttons(BOTONERACONF_QUITAR).Enabled = False
            tbConformes.Buttons(BOTONERACONF_CONFIRMAR).Enabled = True
            tbConformes.Buttons(BOTONERACONF_CANCELAR).Enabled = True
            tbConformes.Buttons(BOTONERACONF_DESHABILITAR).Enabled = False
            
            Select Case sOpcionSeleccionada
                Case "A"
                    lblPetConf = " AGREGAR ": lblPetConf.visible = True
                    pcfTexto = ""
                    pcfTexto.Locked = False
                    fraConforme.Enabled = True
                    Call InicializarConformes("2")
                    cboPcfTipo.ListIndex = 0
                    cboPcfClase.ListIndex = 0
                    Call setHabilCtrl(pcfTexto, "NOR")
                    Call setHabilCtrl(cboPcfTipo, "NOR")
                    Call setHabilCtrl(cboPcfClase, "NOR")
                    If glEsHomologador Then
                        cboPcfModo.ListIndex = PosicionCombo(cboPcfModo, "N/A", True)
                    Else
                        If PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = "DESA" Then  ' upd -065- b.
                            Call SetCombo(cboPcfModo, "ALT", True)
                        Else
                            Call SetCombo(cboPcfModo, "REQ", True)
                        End If
                    End If
                    If PerfilInternoActual = "SADM" Then Call setHabilCtrl(cboPcfModo, "NOR")
                    If PerfilInternoActual = "CSEC" Then
                        cboPcfClase.ListIndex = 1
                        If sp_GetPeticionSector(glNumeroPeticion, glLOGIN_Sector) Then
                            Call setHabilCtrl(pcfTexto, "NOR")
                        Else
                            Call setHabilCtrl(pcfTexto, "OBL")
                        End If
                    End If
                    If glEsSeguridadInformatica Or glEsTecnologia Then
                        cboPcfClase.ListIndex = 1
                        Call setHabilCtrl(cboPcfClase, DISABLE)
                    End If
                Case "M"
                    lblPetConf = " MODIFICAR ": lblPetConf.visible = True
                    fraConforme.Enabled = True
                    pcfTexto.Locked = False
                    Call setHabilCtrl(pcfTexto, "NOR")
                    Call setHabilCtrl(cboPcfTipo, "DIS")
                    If PerfilInternoActual = "SADM" Then
                        Call setHabilCtrl(cboPcfModo, "NOR")
                    End If
                    Call InicializarConformes("2")
                    pcfTexto.SetFocus
                Case "E"
                    lblPetConf = " DESVINCULAR ": lblPetConf.visible = True
                    fraConforme.Enabled = False
            End Select
    End Select
    pcfOpcion = sOpcionSeleccionada
    Debug.Print "pcfOpcion: " & pcfOpcion
End Sub


Private Sub txtTitulo_Change()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Caracteres disponibles: " & txtTitulo.MaxLength - Len(txtTitulo)
    End If
End Sub

Private Sub lblPeticionNumero_Click()
    With lblPeticionNumero
        If .Caption = "N� Asignado" Then
            .ForeColor = vbBlue
            .Caption = "N� Interno"
            txtNroAsignado.visible = False
            txtNrointerno.visible = True
        Else
            .ForeColor = vbBlack
            .Caption = "N� Asignado"
            txtNroAsignado.visible = True
            txtNrointerno.visible = False
        End If
    End With
End Sub

Private Sub HabilitarProyectos(bOpcion As Boolean)
    If bDebuggear Then Call doLog("HabilitarProyectos(" & bOpcion & ")")
    txtPrjNom.BackColor = IIf(bOpcion, vbWhite, Me.BackColor)
    cmdSelProyectos.Enabled = bOpcion
    cmdEraseProyectos.Enabled = bOpcion
End Sub

' TODO: revisar bien esta validaci�n con proyectos
Private Function ValidarProyecto(ProjId, ProjSubId, ProjSubSId) As Boolean
    If bDebuggear Then Call doLog("ValidarProyecto()")
    If IsNumeric(ProjId) Then ProjId = IIf(ProjId > 0, ProjId, 0)
    If IsNumeric(ProjSubId) Then ProjSubId = IIf(ProjSubId > 0, ProjSubId, 0)
    If IsNumeric(ProjSubSId) Then ProjSubSId = IIf(ProjSubSId > 0, ProjSubSId, 0)
    ' Si no existe ningun dato para proyectos (Proyecto = 0,0,0), entonces no hay que validar nada.
    ' Al usuario no le interesa grabar datos de proyecto
    If ProjId + ProjSubId + ProjSubSId > 0 Then
        If Not sp_GetProyectoIDM(ProjId, ProjSubId, ProjSubSId, Null, Null, Null, Null, Null) Then
            MsgBox "El proyecto seleccionado no es v�lido. Revise.", vbExclamation + vbOKOnly, "Proyecto IDM inv�lido"
            ValidarProyecto = False
            Call HabilitarVisibilidad(False)
            Exit Function
        Else
            If InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) = 0 Then
                If InStr(1, "DESEST|TERMIN|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                    MsgBox "El proyecto seleccionado no es v�lido por su estado. Revise.", vbExclamation + vbOKOnly, "Proyecto IDM con estado inv�lido"
                    ValidarProyecto = False
                    Call HabilitarVisibilidad(False)
                    Exit Function
                End If
            End If
        End If
        ValidarProyecto = True
        Call HabilitarVisibilidad(True)
    Else
        ValidarProyecto = False
        txtPrjNom.text = ""
        Call HabilitarVisibilidad(False)
    End If
End Function

Private Sub HabilitarVisibilidad(bModo As Boolean)
    If bDebuggear Then Call doLog("HabilitarVisibilidad(" & bModo & ")")
    
    If bModo Then
        cboImportancia.ListIndex = 0                        ' Por definici�n, se fuerza la Visibilidad al m�ximo nivel
        Call setHabilCtrl(cboImportancia, "DIS")            ' No se puede modificar (Ultima definici�n de A. Ocampo)
    Else
        cboImportancia.ListIndex = PosicionCombo(cboImportancia, xImportancia, True)
        Call setHabilCtrl(cboImportancia, "NOR")
    End If
End Sub

Private Function MostrarNombreProyecto(ProjId As Long, ProjSubId As Long, ProjSubSId As Long) As String
    If bDebuggear Then Call doLog("MostrarNombreProyecto()")
    MostrarNombreProyecto = ""
    If ProjId + ProjSubId + ProjSubSId > 0 Then
        If sp_GetProyectoIDM(ProjId, ProjSubId, ProjSubSId, Null, Null, Null, Null, Null) Then
            MostrarNombreProyecto = ProjId & "." & ProjSubId & "." & ProjSubSId & " - " & ClearNull(aplRST.Fields!projnom)
        End If
    End If
End Function

Private Sub cmdSelProyectos_Click()
    Load frmProyectosIDMSeleccion
    frmProyectosIDMSeleccion.Show 1
    With frmProyectosIDMSeleccion
        If .flgSeleccionado Then
            txtPrjId.text = .lProjId
            txtPrjSubId.text = .lProjSubId
            txtPrjSubsId.text = .lProjSubSId
            txtPrjNom.text = .lProjId & "." & .lProjSubId & "." & .lProjSubSId & " - " & ClearNull(.cProjNom)
            Call ValidarProyecto(.lProjId, .lProjSubId, .lProjSubSId)
        End If
    End With
End Sub

Private Sub cmdEraseProyectos_Click()
    If MsgBox("�Confirma desvincular el proyecto de esta petici�n?", vbQuestion + vbYesNo, "Proyecto IDM") = vbYes Then
        txtPrjId.text = ""
        txtPrjSubId.text = ""
        txtPrjSubsId.text = ""
        txtPrjNom.text = ""
        Call ValidarProyecto(0, 0, 0)
    End If
End Sub

Private Sub InicializarToolTipsControles()
    cboSolicitante.ToolTipText = "Solicitante formal de la petici�n actual"
    txtFechaPedido.ToolTipText = "La fecha real en que la petici�n fu� cargada en el sistema"
    cboSector.ToolTipText = "Indica el sector solicitante de la petici�n actual"
    txtFfinreal.ToolTipText = ""
    txtFfinplan.ToolTipText = ""
    txtFinireal.ToolTipText = ""
    txtFiniplan.ToolTipText = ""
    txtHsPlanif.ToolTipText = ""
    txtHspresup.ToolTipText = ""
    txtNroAnexada.ToolTipText = "N� de petici�n a la cual se ha anexado la petici�n actual"
    txtFechaEstado.ToolTipText = "Fecha en que se alcanz� el estado actual de la petici�n"
    txtSituacion.ToolTipText = "Indica la situaci�n particular de la petici�n"
    txtEstado.ToolTipText = "Indica el estado general consolidado de la petici�n"
    cboOrientacion.ToolTipText = ""
    cboCorpLocal.ToolTipText = "Diferencia �mbito Corporativo de �mbito local."
    txtPrjNom.ToolTipText = "Nombre descriptivo del proyecto (en el marco del Informe a Direcci�n de Medios) del que la presente petici�n forma parte."
    cmdSelProyectos.ToolTipText = "Permite la selecci�n de un proyecto para vincular a la petici�n"
    cmdEraseProyectos.ToolTipText = "Desvincula la petici�n del proyecto asociado"
    txtPrjSubsId.ToolTipText = "Subc�digo de subproyecto"
    txtPrjSubId.ToolTipText = "C�digo de subproyecto"
    txtPrjId.ToolTipText = "C�digo principal de proyecto"
    cboImportancia.ToolTipText = "Establece el nivel de visibilidad de acuerdo a su importancia"
    cboRegulatorio.ToolTipText = "Requerimientos de cumplimiento obligatorio, a partir de exigencias de entes reguladores externos"
    cboImpacto.ToolTipText = "Cuando es necesario adaptar o ampliar el entorno tecnol�gico disponible."
    cboClase.ToolTipText = ""
    cboTipopet.ToolTipText = "Indica el tipo de petici�n: Normal, Propia, Especial, Auditor�a, etc."
    txtTitulo.ToolTipText = "T�tulo descriptivo para esta petici�n"
    txtNroAsignado.ToolTipText = "N�mero identificatorio asignado autom�ticamente para peticiones aprobadas"
    txtFechaComite.ToolTipText = "Fecha inicial en estado al Referente de Sistemas."
    cboBpar.ToolTipText = "Indica el Referente de Sistemas asociado a la petici�n actual"
    cboDirector.ToolTipText = "Autorizante del solicitante de la petici�n"
    cboReferente.ToolTipText = "Referente del solicitante de la petici�n"
    cboSupervisor.ToolTipText = "Supervisor del solicitante de la petici�n"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set valoraciones = Nothing
    Call InicializarGrillas(False)
    If bDebuggear Then Call doLog("frmPeticionesCarga.Form_Unload")
End Sub

Private Sub cboClase_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboClase.ListIndex = 0
End Sub

Private Sub cboTipopet_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboTipopet.ListIndex = 0
End Sub

Private Sub cboImpacto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboImpacto.ListIndex = 0
End Sub

Private Sub cboRegulatorio_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboRegulatorio.ListIndex = 0
End Sub

Private Sub cboImportancia_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboImportancia.ListIndex = 0
End Sub

Private Sub cboCorpLocal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboCorpLocal.ListIndex = 0
End Sub

Private Sub cboOrientacion_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboOrientacion.ListIndex = 0
End Sub

Private Sub cboSector_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboSector.ListIndex = 0
End Sub

Private Sub cboSolicitante_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboSolicitante.ListIndex = 0
End Sub

Private Sub cboReferente_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboReferente.ListIndex = 0
End Sub

Private Sub cboSupervisor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboSupervisor.ListIndex = 0
End Sub

Private Sub cboDirector_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboDirector.ListIndex = 0
End Sub

Private Sub ValidarPeticion()
    With imgTrazabilidad
        .visible = False
        If glNumeroPeticion <> "" Then
            If sp_GetPeticionChangeMan(glNumeroPeticion, Null, Null) Then
                If Not aplRST.EOF Then
                    .visible = True
                End If
            End If
        End If
    End With
    If bDebuggear Then Call doLog(DbgFmt(imgTrazabilidad.visible, "ValidarPeticion()"))
End Sub

'{
' ************************************************************************************************************************************
' ORDENAMIENTO
' ************************************************************************************************************************************
Private Sub ORDENAMIENTO(ByRef grdDatos As MSFlexGrid)
    bSorting = True
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
            .visible = False
            .col = .MouseCol
            If ClearNull(.TextMatrix(0, .col)) = "Ord." Or IsNumeric(.TextMatrix(1, .col)) Then
                If Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortNumericDescending
                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortNumericAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortNumericDescending
                End If
            Else
                If Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortStringNoCaseDescending
                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortStringNoCaseAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortStringNoCaseDescending
                End If
            End If
            lUltimaColumnaOrdenada = .col
            .visible = True
        End If
    End With
    bSorting = False
End Sub
'}

Private Function CamposObligatoriosConformes(tipoconforme As String, tipoOk As String) As Boolean
    If Not glEsHomologador Then
        ' Control de tipo
        If cboPcfTipo.ListIndex < 0 Then
            MsgBox "Falta especificar el tipo de conforme es.", vbOKOnly + vbInformation
            CamposObligatoriosConformes = False
            Exit Function
        End If
        
        ' Control de clase de conforme
        If cboPcfClase.ListIndex < 0 Then
            MsgBox "Falta especificar que clase de conforme es.", vbOKOnly + vbInformation
            CamposObligatoriosConformes = False
            Exit Function
        End If
        
        ' Control de modo
        If cboPcfModo.ListIndex < 0 Then
            MsgBox "Falta especificar exenci�n.", vbOKOnly + vbInformation
            CamposObligatoriosConformes = False
            Exit Function
        End If

        If pcfOpcion <> "E" Then
            If sTipoPet = "PRO" Then
                If InStr(1, "OKPP|OKPF|", CodigoCombo(cboPcfTipo, True), vbTextCompare) > 0 Then
                    If Not (PerfilInternoActual = "SADM") Then
                        If Not (PerfilInternoActual = "CSEC" And glLOGIN_Sector = xSec) Then
                            MsgBox "Solo el responsable de Sector de la petici�n" & vbCrLf & "pueden otorgar este tipo de conforme.", vbOKOnly + vbExclamation
                            CamposObligatoriosConformes = False
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If
        
        If sTipoPet = "PRO" Then
            If PerfilInternoActual = "CSEC" And glLOGIN_Gerencia = "DESA" Then
                If InStr(1, "TEST|TESP|", CodigoCombo(cboPcfTipo, True), vbTextCompare) > 0 Then
                    If Not sp_GetPeticionSector(glNumeroPeticion, glLOGIN_Sector) Then
                        If ClearNull(pcfTexto) = "" Then
                            MsgBox "Debe ingresar en comentarios una justificaci�n.", vbOKOnly + vbInformation
                            CamposObligatoriosConformes = False
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If

        ' TODO: esto se puede mejorar habilitando un flag en la carga inicial para detectar si ya existe un OK TEST final.
        If pcfOpcion <> "E" Then
            If CodigoCombo(cboPcfTipo, True) = "TESP" Then
                If sp_GetPeticionConf(glNumeroPeticion, "TEST", Null) Then
                    MsgBox "La petici�n ya tiene un TEST." & vbCrLf & "No puede ingresar conformes parciales.", vbOKOnly + vbExclamation
                    CamposObligatoriosConformes = False
                    Exit Function
                End If
            End If
        End If

        If pcfOpcion <> "E" Then
            If InStr(1, "ATEN|CORR|SPUF|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                If InStr(1, "ALCA|ALCP|", CodigoCombo(cboPcfTipo, True), vbTextCompare) > 0 Then
                    If InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) = 0 Then
                        MsgBox "Solo peticiones de clase Nuevo desarrollo, Evolutivos y " & vbCrLf & "Optimizaciones pueden tener conformes de alcance.", vbOKOnly + vbExclamation
                        CamposObligatoriosConformes = False
                        Exit Function
                    End If
                End If
            End If
        End If

        If pcfOpcion <> "E" Then
            If InStr(1, "DATP|DATF|", CodigoCombo(cboPcfTipo, True), vbTextCompare) > 0 Then
                If InStr(1, "CORR|", CodigoCombo(cboClase, True), vbTextCompare) = 0 Then
                    MsgBox "Solo peticiones de clase Correctivo pueden tener conformes de datos de usuario.", vbOKOnly + vbExclamation
                    CamposObligatoriosConformes = False
                    Exit Function
                End If
                If InStr(1, "SADM|CGRU|CSEC|", PerfilInternoActual, vbTextCompare) = 0 Then
                    MsgBox "Solo responsables de sector o grupo otorgar conformes de datos de usuario.", vbOKOnly + vbExclamation
                    CamposObligatoriosConformes = False
                    Exit Function
                End If
            End If
        End If
        'GMT03 - INI
        If tipoconforme = "ADA" Then
            If tipoOk = "P" Then
                MsgBox "Para documento ADA, solo se puede cargar OK Final.", vbOKOnly + vbExclamation
                CamposObligatoriosConformes = False
                Exit Function
            End If
        End If
        'GMT03 - FIN
    End If
    CamposObligatoriosConformes = True
End Function

Private Sub HomologacionDeshabilitarManual()
    If MsgBox("La petici�n quedar� deshabilitada para pasajes a producci�n." & vbCrLf & "�Desea continuar?", vbExclamation + vbOKCancel, "Deshabilitar validez") = vbOK Then
        Call Puntero(True)
        Call sp_DeletePeticionChangeMan(glNumeroPeticion)
        Call sp_DeletePeticionEnviadas(glNumeroPeticion)
        Call ValidarPeticion
        Call Puntero(False)
        MsgBox "Proceso finalizado", vbInformation + vbOKOnly
    End If
End Sub

Private Sub InicializarInformesHomologacion()
    bEnCarga = True

    ' Fuerzo la primera pesta�a siempre
    sstInformeHomo.Tab = TABHOMO_INFORME
    
    ' Listado de items
    If glEsHomologador Then
        bIncluirItemsOpcionales = False
        chkIncluirItemsOpcionales.visible = True
    Else
        bIncluirItemsOpcionales = False
        chkIncluirItemsOpcionales.visible = False
    End If

    ' Homologaci�n
    chkIncluirItemsOpcionales.value = 0                          ' Por defecto no se incluyen los items opcionales.
    Call setHabilCtrl(cboInforme, DISABLE)
    Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)
    Call setHabilCtrl(dtpHomoFechaInforme, DISABLE)
    Call setHabilCtrl(cboHomoRecursoHomologador, DISABLE)
    Call setHabilCtrl(txtHomoInfoObservaciones, DISABLE)
    Call HabilitarBotonesHomologacion(MODO_VIEW)
    
    With cboHomoRecursoHomologador
        .Clear
        If sp_GetRecursoPerfil(Null, "HOMO") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_recurso) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        Call setHabilCtrl(cboHomoRecursoHomologador, DISABLE)
    End With

    lblHomoModo = ""
    With cboHomoEstadoInforme
        .Clear
        .AddItem "N/A" & ESPACIOS & "||NULL"
        .AddItem "En confecci�n" & ESPACIOS & "||CONFEC"
        .AddItem "Verificado" & ESPACIOS & "||VERIFI"
        .AddItem "Finalizado" & ESPACIOS & "||TERMIN"           ' Finalizado: no puede volver a modificarse.
        .AddItem "Baja" & ESPACIOS & "||BAJA"
        .ListIndex = 0
        Call setHabilCtrl(cboHomoEstadoInforme, "DIS")
    End With
    
    With cboHomoResultado
        .Clear
        .AddItem "Pendiente" & ESPACIOS & "||NULL"
        .AddItem "SOB" & ESPACIOS & "||SOB"
        .AddItem "OME" & ESPACIOS & "||OME"
        .AddItem "OMA" & ESPACIOS & "||OMA"
        .ListIndex = 0
        Call setHabilCtrl(cboHomoResultado, "DIS")
    End With
    
    With cboHomoProceso
        .Clear
        .AddItem "N/A" & ESPACIOS & "||NULL"
        .AddItem "Reducido" & ESPACIOS & "||R"
        .AddItem "Completo" & ESPACIOS & "||C"
        .ListIndex = 0
        Call setHabilCtrl(cboHomoProceso, "DIS")
    End With
    
    With cboPuntoControl
        .Clear
        .AddItem "N/A" & ESPACIOS & "||0"
        .AddItem "1er. punto" & ESPACIOS & "||1"
        .AddItem "2do. punto" & ESPACIOS & "||2"
        .AddItem "3er. punto" & ESPACIOS & "||3"
        .AddItem "4to. punto" & ESPACIOS & "||4"
        .ListIndex = 0
        Call setHabilCtrl(cboPuntoControl, "DIS")
    End With
    
    If glNumeroPeticion <> "" Then Call CargarInformesHomologacion
    If cboInforme.ListIndex > -1 Then
        Call setHabilCtrl(cboInforme, "NOR")
        Call setHabilCtrl(chkIncluirItemsOpcionales, "NOR")
        Call CargarUnInformeDeHomologacion(CodigoCombo(cboInforme))
    End If
End Sub

Private Sub CargarGruposDeControl()
    If bDebuggear Then
        Call Debuggear(Me, "CargargruposDeControl")
        Call doLog("CargargruposDeControl()")
    End If
    
    ' En el procedimento InicializarRecurso ya se obtuvieron los valores correspondientes
    ' a las variables globales que establecen el perfil especial.
    ' glEsHomologador
    ' glEsSeguridadInformatica
    ' glEsTecnologia
    
    sHomoGrupoEstado = ""
    sHomoSectorEstado = ""
    
    ' Obtiene el estado actual del grupo Homologaci�n para la petici�n
    bHomologacionEnEstadoActivo = False
    bExisteHomologacionEnPeticionActual = False
    If sp_GetPeticionGrupo(glNumeroPeticion, glHomologacionSector, glHomologacionGrupo) Then
        bExisteHomologacionEnPeticionActual = True
        sHomoGrupoEstado = ClearNull(aplRST.Fields!cod_estado)
        If InStr(1, "RECHAZ|CANCEL|TERMIN|ANEXAD|ANULAD|RECHTE|", sHomoGrupoEstado, vbTextCompare) = 0 Then bHomologacionEnEstadoActivo = True
    End If
    ' Obtiene el estado actual del sector del grupo Homologaci�n para la petici�n
    sHomoSectorEstado = ""
    If bExisteHomologacionEnPeticionActual Then
        If sp_GetPeticionSector(glNumeroPeticion, glHomologacionSector) Then
            sHomoSectorEstado = ClearNull(aplRST.Fields!cod_estado)
        End If
    End If
    
    ' Determina si existen grupos homologables (t�cnicos) en la petici�n actual
    bExistenGruposHomologablesEnPeticionActual = False
    If sp_GetPeticionGrupo(glNumeroPeticion, Null, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!homo) = "S" Then
                bExistenGruposHomologablesEnPeticionActual = True
                Exit Do
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    sSegInfGrupoEstado = ""
    sSegInfSectorEstado = ""
    bExisteSegInfEnEstadoActivo = False
    bExisteSegInfEnPeticionActual = False
    ' TODO: revisar ac� porque glSegInfGrupo y glSegInfSector est�n vacios
    If sp_GetPeticionGrupo(glNumeroPeticion, glSegInfSector, glSegInfGrupo) Then
        bExisteSegInfEnPeticionActual = True
        sSegInfGrupoEstado = ClearNull(aplRST.Fields!cod_estado)
        If InStr(1, "RECHAZ|CANCEL|TERMIN|ANEXAD|ANULAD|RECHTE|", sSegInfGrupoEstado, vbTextCompare) = 0 Then bExisteSegInfEnEstadoActivo = True
    End If
    ' Obtiene el estado actual del sector del grupo Seg. Inf. para la petici�n
    sSegInfSectorEstado = ""
    If bExisteSegInfEnPeticionActual Then
        If sp_GetPeticionSector(glNumeroPeticion, glSegInfSector) Then
            sSegInfSectorEstado = ClearNull(aplRST.Fields!cod_estado)
        End If
    End If

    sTecnoGrupoEstado = ""
    sTecnoSectorEstado = ""
    bExisteTecnoEnEstadoActivo = False
    bExisteTecnoEnPeticionActual = False
    
    If sp_GetPeticionGrupo(glNumeroPeticion, glTecnoSector, glTecnoGrupo) Then
        bExisteTecnoEnPeticionActual = True
        sTecnoGrupoEstado = ClearNull(aplRST.Fields!cod_estado)
        If InStr(1, "RECHAZ|CANCEL|TERMIN|ANEXAD|ANULAD|RECHTE|", sTecnoGrupoEstado, vbTextCompare) = 0 Then bExisteTecnoEnEstadoActivo = True
    End If
    ' Obtiene el estado actual del sector del grupo Seg. Inf. para la petici�n
    sTecnoSectorEstado = ""
    If bExisteTecnoEnPeticionActual Then
        If sp_GetPeticionSector(glNumeroPeticion, glTecnoSector) Then
            sTecnoSectorEstado = ClearNull(aplRST.Fields!cod_estado)
        End If
    End If
End Sub

Private Sub grdHomologacion_KeyUp(KeyCode As Integer, Shift As Integer)
    Const LISTO = "Listo."
    Const PENDIENTE = "Pendiente."
    
    With grdHomologacion
        If lblHomoModo.Tag = MODO_EDIT Then
            If .rowSel > 1 Then
                If .col = colEDIT_ESTADO Then
                    If InStr(1, "SNDA123RULCP", Chr(KeyCode), vbTextCompare) > 0 Then
                        If .TextMatrix(.row, colHOM_ITEMTIPO) = "A" Then        ' A,S,D y N
                            Select Case Chr(KeyCode)
                                Case "A": Call ActualizarItem(grdHomologacion, .row, "X", "N/A", "", "-", LISTO)
                                Case "S": Call ActualizarItem(grdHomologacion, .row, "S", "Si", "1", "-", LISTO)
                                Case "D", "N": Call ActualizarItem(grdHomologacion, .row, "N", "No", "", "?", PENDIENTE)
                            End Select
                        End If
                        
                        If .TextMatrix(.row, colHOM_ITEMTIPO) = "B" Then        ' P,C, y N
                            Select Case Chr(KeyCode)
                                Case "N", "D": Call ActualizarItem(grdHomologacion, .row, "N", "No", "0", "-", LISTO)
                                Case "P": Call ActualizarItem(grdHomologacion, .row, "P", "P950", "1", "-", LISTO)
                                Case "C": Call ActualizarItem(grdHomologacion, .row, "C", "C100", "1", "-", LISTO)
                            End Select
                        End If
                        
                        If .TextMatrix(.row, colHOM_ITEMTIPO) = "C" Then        ' R,S,L,U
                            Select Case Chr(KeyCode)
                                Case "A": Call ActualizarItem(grdHomologacion, .row, "X", "N/A", "", "-", LISTO)
                                Case "S": Call ActualizarItem(grdHomologacion, .row, "V", "SUPERV.", "", "?", PENDIENTE)
                                Case "R": Call ActualizarItem(grdHomologacion, .row, "R", "REF.SIS.", "", "?", PENDIENTE)
                                Case "L": Call ActualizarItem(grdHomologacion, .row, "L", "LIDER", "", "?", PENDIENTE)
                                Case "U": Call ActualizarItem(grdHomologacion, .row, "U", "USUARIO", "", "?", PENDIENTE)
                            End Select
                        End If
                        
                        If .TextMatrix(.row, colHOM_ITEMESTADO) <> "" Then
                            Select Case Chr(KeyCode)
                                Case "1"    ' OMA
                                    If InStr(1, "N|V|R|L|U|", .TextMatrix(.row, colHOM_ITEMESTADO), vbTextCompare) > 0 Then
                                        If Not (.TextMatrix(.row, colHOM_ITEMESTADO) = "N" And .TextMatrix(.row, colHOM_ITEMTIPO) = "B") Then
                                            Call ActualizarItemOM(grdHomologacion, .row, "3", "OMA", LISTO)
                                        End If
                                    End If
                                Case "2"    ' OME
                                    If InStr(1, "N|V|R|L|U|", .TextMatrix(.row, colHOM_ITEMESTADO), vbTextCompare) > 0 Then
                                        If Not (.TextMatrix(.row, colHOM_ITEMESTADO) = "N" And .TextMatrix(.row, colHOM_ITEMTIPO) = "B") Then
                                            Call ActualizarItemOM(grdHomologacion, .row, "2", "OME", LISTO)
                                        End If
                                    End If
                                Case "3"    ' SOB
                                    If InStr(1, "N|V|R|L|U|", .TextMatrix(.row, colHOM_ITEMESTADO), vbTextCompare) > 0 Then
                                        If Not (.TextMatrix(.row, colHOM_ITEMESTADO) = "N" And .TextMatrix(.row, colHOM_ITEMTIPO) = "B") Then
                                            Call ActualizarItemOM(grdHomologacion, .row, "1", "SOB", LISTO)
                                        End If
                                    End If
                            End Select
                        End If
                    Else
                        If KeyCode = KEY_DEL Then
                            .TextMatrix(.row, colHOM_ITEMVALORDESC) = ""
                            .TextMatrix(.row, colHOM_ITEMESTADO) = ""
                            .TextMatrix(.row, colHOM_ITEMESTADONOM) = ""
                            .TextMatrix(.row, colHOM_ITEMSTATUS) = PENDIENTE
                        End If
                    End If
                End If
            End If
        End If
        bCambiosPendientes = True
    End With
End Sub

Private Sub ActualizarItem(ByRef grdGrilla, Fila, itemEstado, itemEstadoNombre, itemValor, itemValorDescripcion, itemStatus)
    With grdGrilla
        .TextMatrix(Fila, colHOM_ITEMESTADO) = itemEstado
        .TextMatrix(Fila, colHOM_ITEMESTADONOM) = itemEstadoNombre
        .TextMatrix(Fila, colHOM_ITEMVALOR) = itemValor
        .TextMatrix(Fila, colHOM_ITEMVALORDESC) = itemValorDescripcion
        .TextMatrix(Fila, colHOM_ITEMSTATUS) = itemStatus
    End With
End Sub

Private Sub ActualizarItemOM(ByRef grdGrilla, Fila, itemValor, itemValorDescripcion, itemStatus)
    With grdGrilla
        .TextMatrix(Fila, colHOM_ITEMVALOR) = itemValor
        .TextMatrix(Fila, colHOM_ITEMVALORDESC) = itemValorDescripcion
        .TextMatrix(Fila, colHOM_ITEMSTATUS) = itemStatus
    End With
End Sub

Private Sub HomologacionGuardarInforme()
    Select Case lblHomoModo.Tag
        Case MODO_EDIT
            If GuardarInforme Then
                MsgBox "Hecho!", vbInformation + vbOKOnly
                Call HabilitarBotonesHomologacion(MODO_VIEW)
                Call CargarUnInformeDeHomologacion(CodigoCombo(cboInforme, True))
            End If
        Case MODO_DELETE
            Call EliminarInforme
    End Select
End Sub

Private Sub EliminarInforme()
    If MsgBox("�Confirma la eliminaci�n del informe actual?", vbQuestion + vbYesNo) = vbYes Then
        Call Puntero(True)
        Call sp_DeletePeticionInfohc(glNumeroPeticion, CodigoCombo(cboInforme, True), Null)
        Call CargarInformesHomologacion
        Call CargarUnInformeDeHomologacion(CodigoCombo(cboInforme))
        Call Puntero(False)
        Call HabilitarBotonesHomologacion(MODO_VIEW)
    End If
End Sub
    
Private Function GuardarInforme() As Boolean
    Dim i As Long
    
    GuardarInforme = False
    
    Select Case CodigoCombo(cboHomoEstadoInforme, True)
        Case "VERIFI", "TERMIN"
            If ValidarItemsHomologacion Then        ' Items
                Call Puntero(True)
                With grdHomologacion
                    For i = 1 To .Rows - 1
                        If Not .MergeRow(i) Then
                            Call sp_UpdatePeticionInfohd(.TextMatrix(i, colHOM_PETNROINTERNO), _
                                                        .TextMatrix(i, colHOM_INFOID), _
                                                        .TextMatrix(i, colHOM_INFOIDVER), _
                                                        .TextMatrix(i, colHOM_INFOITEM), _
                                                        .TextMatrix(i, colHOM_ITEMESTADO), _
                                                        .TextMatrix(i, colHOM_ITEMVALOR), _
                                                        .TextMatrix(i, colHOM_ITEMORDEN))
                        End If
                    Next i
                End With
            End If
        Case Else
            ' Guardo los items que no se encuentran pendientes
            With grdHomologacion
                For i = 1 To .Rows - 1
                    If Not .MergeRow(i) Then
                        If .TextMatrix(i, colHOM_ITEMSTATUS) <> "Pendiente" Then
                            Call sp_UpdatePeticionInfohd(.TextMatrix(i, colHOM_PETNROINTERNO), _
                                                        .TextMatrix(i, colHOM_INFOID), _
                                                        .TextMatrix(i, colHOM_INFOIDVER), _
                                                        .TextMatrix(i, colHOM_INFOITEM), _
                                                        .TextMatrix(i, colHOM_ITEMESTADO), _
                                                        .TextMatrix(i, colHOM_ITEMVALOR), _
                                                        .TextMatrix(i, colHOM_ITEMORDEN))
                        End If
                    End If
                Next i
            End With
    End Select
    ' Cabecera
    Call sp_UpdatePeticionInfohc(glNumeroPeticion, CodigoCombo(cboInforme, True), 0, CodigoCombo(cboHomoProceso, True), CodigoCombo(cboPuntoControl, True), CodigoCombo(cboHomoRecursoHomologador, True), CodigoCombo(cboHomoEstadoInforme, True))
    ' Guardamos el texto de las observaciones generales a nivel del informe
    Call sp_UpdatePeticionInfohMemo(glNumeroPeticion, CodigoCombo(cboInforme, True), 0, ClearNull(txtHomoInfoObservaciones.text))
    GuardarInforme = True
    Call Puntero(False)
End Function

Private Function ValidarItemsHomologacion() As Boolean
    Dim bFaltaDefinirValor As Boolean
    Dim cMensaje As String, cFaltaEstado As String
    Dim i As Long
    
    bFaltaDefinirValor = False
    cMensaje = "Falta definir el valor '?' para el item "
    cFaltaEstado = "Falta definir el estado del item "
    
    With grdHomologacion
        For i = 1 To .Rows - 1
            If Not .MergeRow(i) Then
                If InStr(1, "N|C|P|V|R|L|U|", .TextMatrix(i, colHOM_ITEMESTADO), vbTextCompare) > 0 Then
                    If .TextMatrix(i, colHOM_ITEMVALORDESC) = "?" Then
                        bFaltaDefinirValor = True
                        MsgBox cMensaje & .TextMatrix(i, colHOM_INFOITEM) & ".", vbExclamation + vbOKOnly
                        Exit For
                    End If
                    If .TextMatrix(i, colHOM_ITEMREQ) = "S" Then        ' Obligatorio
                        If .TextMatrix(i, colHOM_ITEMSTATUS) = "Pendiente" Then
                            bFaltaDefinirValor = True
                            MsgBox cFaltaEstado & .TextMatrix(i, colHOM_INFOITEM) & ".", vbExclamation + vbOKOnly
                            Exit For
                        End If
                    End If
                End If
            End If
        Next i
        ValidarItemsHomologacion = IIf(bFaltaDefinirValor, False, True)
    End With
End Function

Private Sub chkIncluirItemsOpcionales_Click()
    If Not bEnCarga Then
        bIncluirItemsOpcionales = IIf(chkIncluirItemsOpcionales.value = 1, True, False)
        Call CargarUnInformeDeHomologacion(CodigoCombo(cboInforme))
    End If
End Sub

Private Sub cboInforme_Click()
    If Not bEnCarga Then Call CargarUnInformeDeHomologacion(CodigoCombo(cboInforme))
End Sub

Private Sub HomologacionCancelar()
    Call HabilitarBotonesHomologacion(MODO_VIEW)
    Call CargarUnInformeDeHomologacion(CodigoCombo(cboInforme, True))
End Sub

Private Sub HomologacionEditarInforme()
    If cboInforme.ListIndex > -1 Then
        Call HabilitarBotonesHomologacion(MODO_EDIT)
    Else
        MsgBox "No hay ning�n informe seleccionado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub HomologacionQuitarInforme()
    If cboInforme.ListIndex > -1 Then
        Call HabilitarBotonesHomologacion(MODO_DELETE)
    Else
        MsgBox "No hay ning�n informe seleccionado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub HabilitarBotonesHomologacion(cModo)
    Select Case cModo
        Case MODO_VIEW
            ' Pesta�as
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjADJ).Enabled = True
            If Hab_PesVinculado Then  'GMT05
                orjBtn(orjDOC).Enabled = True
            End If 'GMT05
            orjBtn(orjCNF).Enabled = True
            orjBtn(orjEST).Enabled = True
            fraButtPpal.Enabled = True
            tbHomologacion.Buttons(BOTONERAHOMO_ACTUALIZAR).Enabled = True
            ' Botones
            If glEsHomologador Or (PerfilInternoActual = "SADM") Then
                tbHomologacion.Buttons(BOTONERAHOMO_INFORME).Enabled = True
                If CodigoCombo(cboHomoEstadoInforme, True) <> "TERMIN" Or (PerfilInternoActual = "SADM") Then
                    With tbHomologacion
                        .Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                        .Buttons(BOTONERAHOMO_AGREGAR).Enabled = True
                        .Buttons(BOTONERAHOMO_EDITAR).Enabled = True
                        .Buttons(BOTONERAHOMO_QUITAR).Enabled = True
                        .Buttons(BOTONERAHOMO_GUARDAR).Enabled = False
                        .Buttons(BOTONERAHOMO_CANCELAR).Enabled = False
                    End With
                    grdHomoResponsables.Height = IIf(glEsHomologador, 3735, 5175)
                Else    ' Si el informe est� finalizado, no se permite volver a modificar
                    With tbHomologacion
                        .Buttons(BOTONERAHOMO_AGREGAR).Enabled = False
                        .Buttons(BOTONERAHOMO_EDITAR).Enabled = False
                        .Buttons(BOTONERAHOMO_QUITAR).Enabled = False
                        .Buttons(BOTONERAHOMO_INFORME).Enabled = True
                        .Buttons(BOTONERAHOMO_GUARDAR).Enabled = False
                        .Buttons(BOTONERAHOMO_CANCELAR).Enabled = False
                    End With
                    grdHomoResponsables.Height = 5175
                End If
                sstInformeHomo.TabEnabled(TABHOMO_OBSERVACIONES) = True
                sstInformeHomo.TabEnabled(TABHOMO_RESPONSABLES) = True
            Else
                With tbHomologacion
                    .Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                    .Buttons(BOTONERAHOMO_AGREGAR).Enabled = False
                    .Buttons(BOTONERAHOMO_EDITAR).Enabled = False
                    .Buttons(BOTONERAHOMO_QUITAR).Enabled = False
                    .Buttons(BOTONERAHOMO_INFORME).Enabled = False
                    .Buttons(BOTONERAHOMO_GUARDAR).Enabled = False
                    .Buttons(BOTONERAHOMO_CANCELAR).Enabled = False
                End With
            End If

            ' Controles
            If cboInforme.ListIndex > -1 Then
                Call setHabilCtrl(chkIncluirItemsOpcionales, NORMAL)
                Call setHabilCtrl(cboInforme, NORMAL)
            Else
                Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)
                Call setHabilCtrl(cboInforme, DISABLE)
                Call setHabilCtrl(dtpHomoFechaInforme, DISABLE)
                Call setHabilCtrl(cboHomoResultado, DISABLE)
            End If
            Call setHabilCtrl(cboHomoProceso, DISABLE)
            Call setHabilCtrl(cboPuntoControl, DISABLE)
            Call setHabilCtrl(cboHomoEstadoInforme, DISABLE)
            Call setHabilCtrl(txtHomoInfoObservaciones, DISABLE)
            Call setHabilCtrl(txtItemRespuesta, DISABLE)
            Call setHabilCtrl(cboItemResponsable, DISABLE)
            Call setHabilCtrl(cboHomoRecursoHomologador, DISABLE)
            lblHomoModo = ""
            lblHomoModo.Tag = MODO_VIEW
        Case MODO_EDIT
            ' "Pesta�as viejas"
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjADJ).Enabled = False
            If Hab_PesVinculado Then
                orjBtn(orjDOC).Enabled = False
            End If 'GMT05
            orjBtn(orjCNF).Enabled = False
            orjBtn(orjEST).Enabled = False
            sstInformeHomo.TabEnabled(TABHOMO_RESPONSABLES) = False
            With tbHomologacion
                .Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                .Buttons(BOTONERAHOMO_AGREGAR).Enabled = False
                .Buttons(BOTONERAHOMO_EDITAR).Enabled = False
                .Buttons(BOTONERAHOMO_QUITAR).Enabled = False
                .Buttons(BOTONERAHOMO_INFORME).Enabled = False
                .Buttons(BOTONERAHOMO_GUARDAR).Enabled = True
                .Buttons(BOTONERAHOMO_CANCELAR).Enabled = True
            End With
            Call setHabilCtrl(cboHomoProceso, NORMAL)
            Call setHabilCtrl(cboPuntoControl, NORMAL)
            Call setHabilCtrl(cboHomoEstadoInforme, NORMAL)
            Call setHabilCtrl(txtHomoInfoObservaciones, NORMAL)
            Call setHabilCtrl(cboHomoRecursoHomologador, NORMAL)
            ' Controles
            Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)
            Call setHabilCtrl(cboInforme, DISABLE)
            fraButtPpal.Enabled = False
            lblHomoModo = "Editando informe"
            lblHomoModo.Tag = MODO_EDIT
            grdHomologacion.SetFocus
        Case MODO_DELETE
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjADJ).Enabled = False
            If Hab_PesVinculado Then 'GMT05
                orjBtn(orjDOC).Enabled = False
            End If 'GMT05
            orjBtn(orjCNF).Enabled = False
            orjBtn(orjEST).Enabled = False
            
            sstInformeHomo.TabEnabled(TABHOMO_OBSERVACIONES) = False
            sstInformeHomo.TabEnabled(TABHOMO_RESPONSABLES) = False
            Call setHabilCtrl(cboInforme, DISABLE)
            Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)
            ' Botones
            With tbHomologacion
                .Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                .Buttons(BOTONERAHOMO_AGREGAR).Enabled = False
                .Buttons(BOTONERAHOMO_EDITAR).Enabled = False
                .Buttons(BOTONERAHOMO_QUITAR).Enabled = False
                .Buttons(BOTONERAHOMO_INFORME).Enabled = False
                .Buttons(BOTONERAHOMO_GUARDAR).Enabled = True
                .Buttons(BOTONERAHOMO_CANCELAR).Enabled = True
            End With
            fraButtPpal.Enabled = False
            ' Controles
            lblHomoModo = "Eliminar el informe"
            lblHomoModo.Tag = MODO_DELETE
    End Select
End Sub

Private Sub CargarInformesHomologacion(Optional bNoCarga As Boolean)
    Dim sEstado As String
    
    cboInforme.ToolTipText = "Los informes aparecen ordenados de m�s reciente a menos reciente."          ' TODO: No se si es el mejor lugar ac� para poner esto.
    If bDebugTrace Then Call Debuggear(Me, "CargarInformesHomologacion")
    
    ' Si no es el homologador, entonces solo se muestran los informes en estado Finalizado.
    sEstado = "VERIFI|TERMIN|"
    If glEsHomologador Then
        sEstado = ""
    End If
    With grdHomologacion
        .Clear
        .Rows = 0
    End With
    With grdHomoResponsables
        .Clear
        .Rows = 0
    End With
    With cboInforme
        .Clear
        If sp_GetPeticionInfohc(glNumeroPeticion, Null, Null, Null, sEstado) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(glNroAsignado) & " / N� " & ClearNull(aplRST.Fields!info_id) & "." & ClearNull(aplRST.Fields!info_idver) & " - " & ClearNull(aplRST.Fields!infoprot_nom) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST.Fields!info_id)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
            lblHomo(2).Caption = "Informe(s) (" & .ListCount & "): "
            Call setHabilCtrl(cboInforme, "NOR")
        Else
            lblHomo(2).Caption = "Informe(s): "
        End If
    End With
End Sub

Private Sub CargarUnInformeDeHomologacion(info_id)
    Dim cNombreGrupo As String
    
    If bDebugTrace Then Call Debuggear(Me, "CargarUnInformeDeHomologacion")
    
    bCambiosPendientes = False
    bEnCarga = True
    sstInformeHomo.Tab = TABHOMO_INFORME

    If cboInforme.ListIndex = -1 Then Exit Sub
    
    If sp_GetPeticionInfohc(glNumeroPeticion, CodigoCombo(cboInforme, True), Null, Null, Null) Then
        cboHomoProceso.ListIndex = PosicionCombo(cboHomoProceso, ClearNull(aplRST.Fields!info_tipo), True)
        cboPuntoControl.ListIndex = PosicionCombo(cboPuntoControl, ClearNull(aplRST.Fields!info_punto), True)
        cboHomoEstadoInforme.ListIndex = PosicionCombo(cboHomoEstadoInforme, ClearNull(aplRST.Fields!info_estado), True)
        cboHomoRecursoHomologador.ListIndex = PosicionCombo(cboHomoRecursoHomologador, ClearNull(aplRST.Fields!info_recurso), True)
        dtpHomoFechaInforme = ClearNull(aplRST.Fields!info_fecha)
        If ClearNull(aplRST.Fields!pendientes) <> "0" Then
            cboHomoResultado.ListIndex = 0
        Else
            cboHomoResultado.ListIndex = IIf(ClearNull(aplRST.Fields!evaluacion) = "", "0", ClearNull(aplRST.Fields!evaluacion))
        End If
    End If
    
    Call HabilitarBotonesHomologacion(MODO_VIEW)

    With grdHomologacion
        .visible = False
        .Clear
        .cols = colHOM_TOTALCOLS
        .FocusRect = flexFocusLight
        .HighLight = flexHighlightAlways
        .Rows = 1
        ' NO VISIBLES
        .TextMatrix(0, colHOM_PETNROINTERNO) = "colHOM_PETNROINTERNO": .ColWidth(colHOM_PETNROINTERNO) = 0: .ColAlignment(colHOM_PETNROINTERNO) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_INFOID) = "colHOM_INFOID": .ColWidth(colHOM_INFOID) = 0: .ColAlignment(colHOM_INFOID) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_INFOIDVER) = "colHOM_INFOIDVER": .ColWidth(colHOM_INFOIDVER) = 0: .ColAlignment(colHOM_INFOIDVER) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_INFOTIPO) = "colHOM_INFOTIPO": .ColWidth(colHOM_INFOTIPO) = 0: .ColAlignment(colHOM_INFOTIPO) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMREQ) = "colHOM_ITEMREQ": .ColWidth(colHOM_ITEMREQ) = 0: .ColAlignment(colHOM_ITEMREQ) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMESTADO) = "colHOM_ITEMESTADO": .ColWidth(colHOM_ITEMESTADO) = 0: .ColAlignment(colHOM_ITEMESTADO) = flexAlignCenterCenter
        .TextMatrix(0, colHOM_ITEMVALOR) = "colHOM_ITEMVALOR": .ColWidth(colHOM_ITEMVALOR) = 0: .ColAlignment(colHOM_ITEMVALOR) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMORDEN) = "colHOM_ITEMORDEN": .ColWidth(colHOM_ITEMORDEN) = 0: .ColAlignment(colHOM_ITEMORDEN) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMTIPO) = "colHOM_ITEMTIPO": .ColWidth(colHOM_ITEMTIPO) = 0: .ColAlignment(colHOM_ITEMTIPO) = flexAlignLeftCenter
        ' VISIBLES
        .TextMatrix(0, colHOM_INFOITEM) = "Item": .ColWidth(colHOM_INFOITEM) = 800: .ColAlignment(colHOM_INFOITEM) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMDESC) = "Actividad planificada": .ColWidth(colHOM_ITEMDESC) = 7000: .ColAlignment(colHOM_ITEMDESC) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMREQDESC) = "Obl./Rec./Opc.": .ColWidth(colHOM_ITEMREQDESC) = 1400: .ColAlignment(colHOM_ITEMREQDESC) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMESTADONOM) = "Estado": .ColWidth(colHOM_ITEMESTADONOM) = 900: .ColAlignment(colHOM_ITEMESTADONOM) = flexAlignCenterCenter
        .TextMatrix(0, colHOM_ITEMVALORDESC) = "Observ.": .ColWidth(colHOM_ITEMVALORDESC) = 900: .ColAlignment(colHOM_ITEMVALORDESC) = flexAlignCenterCenter
        .TextMatrix(0, colHOM_ITEMSTATUS) = "Status":: .ColWidth(colHOM_ITEMSTATUS) = 1200: .ColAlignment(colHOM_ITEMSTATUS) = flexAlignLeftCenter
        .TextMatrix(0, colHOM_ITEMRESPO) = "Responsables":: .ColWidth(colHOM_ITEMRESPO) = 0: .ColAlignment(colHOM_ITEMRESPO) = flexAlignLeftCenter
        ' MERGE COLUMNS
        .MergeCol(colHOM_PETNROINTERNO) = True
        .MergeCol(colHOM_INFOID) = True
        .MergeCol(colHOM_INFOIDVER) = True
        .MergeCol(colHOM_INFOTIPO) = True
        .MergeCol(colHOM_INFOITEM) = True
        .MergeCol(colHOM_ITEMDESC) = True
        .MergeCol(colHOM_ITEMREQ) = True
        .MergeCol(colHOM_ITEMREQDESC) = True
        .MergeCol(colHOM_ITEMESTADO) = True
        .MergeCol(colHOM_ITEMESTADONOM) = True
        .MergeCol(colHOM_ITEMVALOR) = True
        .MergeCol(colHOM_ITEMVALORDESC) = True
        .MergeCol(colHOM_ITEMSTATUS) = True
        .MergeCol(colHOM_ITEMORDEN) = True
        .MergeCells = flexMergeRestrictRows
        .MergeCol(colHOM_ITEMVALOR) = False
        .Font.name = "Tahoma"
        .Font.Size = 8
        .row = 0
        .WordWrap = True
        .RowHeightMin = 300
        
        Call CambiarEfectoLinea2(grdHomologacion, prmGridEffectFontBold)
        Call PintarLinea2(grdHomologacion, prmGridFillRowColorLightGrey3)
        If sp_GetPeticionInfohd(glNumeroPeticion, CodigoCombo(cboInforme, True), Null, IIf(bIncluirItemsOpcionales, "S", Null)) Then
            Do While Not aplRST.EOF
                If .Rows = 1 Then
                    .Rows = .Rows + 1
                    cNombreGrupo = ClearNull(aplRST!info_grunom)
                    .TextMatrix(.Rows - 1, colHOM_PETNROINTERNO) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_INFOID) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_INFOIDVER) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_INFOTIPO) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_INFOITEM) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMDESC) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMREQ) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMREQDESC) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMESTADO) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMESTADONOM) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMVALOR) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMVALORDESC) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMSTATUS) = cNombreGrupo
                    .TextMatrix(.Rows - 1, colHOM_ITEMORDEN) = cNombreGrupo
                    .MergeRow(.Rows - 1) = True
                    Call CambiarEfectoLinea2(grdHomologacion, prmGridEffectFontBold)
                    Call PintarLinea2(grdHomologacion, prmGridFillRowColorLightBlue)
                Else
                    If ClearNull(aplRST!info_grunom) <> cNombreGrupo Then
                        .Rows = .Rows + 1
                        cNombreGrupo = ClearNull(aplRST!info_grunom)
                        .TextMatrix(.Rows - 1, colHOM_PETNROINTERNO) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_INFOID) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_INFOIDVER) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_INFOTIPO) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_INFOITEM) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMDESC) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMREQ) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMREQDESC) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMESTADO) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMESTADONOM) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMVALOR) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMVALORDESC) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMSTATUS) = cNombreGrupo
                        .TextMatrix(.Rows - 1, colHOM_ITEMORDEN) = cNombreGrupo
                        .MergeRow(.Rows - 1) = True
                        Call CambiarEfectoLinea2(grdHomologacion, prmGridEffectFontBold)
                        Call PintarLinea2(grdHomologacion, prmGridFillRowColorLightBlue)
                    Else
                        .Rows = .Rows + 1
                        .TextMatrix(.Rows - 1, colHOM_PETNROINTERNO) = ClearNull(aplRST!pet_nrointerno)
                        .TextMatrix(.Rows - 1, colHOM_INFOID) = ClearNull(aplRST!info_id)
                        .TextMatrix(.Rows - 1, colHOM_INFOIDVER) = ClearNull(aplRST!info_idver)
                        .TextMatrix(.Rows - 1, colHOM_INFOTIPO) = ClearNull(aplRST!info_tipo)
                        .TextMatrix(.Rows - 1, colHOM_INFOITEM) = ClearNull(aplRST!info_item)
                        .TextMatrix(.Rows - 1, colHOM_ITEMDESC) = ClearNull(aplRST!infoprot_itemdsc)
                        .TextMatrix(.Rows - 1, colHOM_ITEMREQ) = ClearNull(aplRST!infoprot_req)
                        .TextMatrix(.Rows - 1, colHOM_ITEMREQDESC) = ClearNull(aplRST!req_dsc)
                        .TextMatrix(.Rows - 1, colHOM_ITEMESTADO) = ClearNull(aplRST!info_estado)
                        .TextMatrix(.Rows - 1, colHOM_ITEMESTADONOM) = ClearNull(aplRST!nom_estado)
                        .TextMatrix(.Rows - 1, colHOM_ITEMVALOR) = ClearNull(aplRST!info_valor)
                        If InStr(1, "N|C|P|V|R|L|U|", ClearNull(aplRST!info_estado), vbTextCompare) > 0 Then    ' "N|C|P|V|R|L|U|"
                            .TextMatrix(.Rows - 1, colHOM_ITEMVALORDESC) = ClearNull(aplRST!valor_dsc)
                        Else
                            .TextMatrix(.Rows - 1, colHOM_ITEMVALORDESC) = "-"
                        End If
                        If ClearNull(aplRST!infoprot_req) = "S" Then    ' Obligatorio
                            .TextMatrix(.Rows - 1, colHOM_ITEMSTATUS) = IIf(ClearNull(aplRST.Fields!nom_estado) <> "", "Listo.", "Pendiente")
                        Else
                            .TextMatrix(.Rows - 1, colHOM_ITEMSTATUS) = IIf(ClearNull(aplRST.Fields!nom_estado) <> "", "Listo.", "-")
                        End If
                        .TextMatrix(.Rows - 1, colHOM_ITEMORDEN) = ClearNull(aplRST!info_orden)
                        .TextMatrix(.Rows - 1, colHOM_ITEMTIPO) = ClearNull(aplRST!infoprot_itemtpo)
                        If .TextMatrix(.Rows - 1, colHOM_ITEMREQ) = "N" Then
                            Call CambiarForeColorLinea2(grdHomologacion, prmGridFillRowColorDarkGrey)
                        End If
                        ' Si el texto es superior a 95 caracteres, se duplica el alto de la fila
                        If Len(ClearNull(aplRST!infoprot_itemdsc)) > 95 Then
                            .RowHeight(.Rows - 1) = 500
                        End If
                        aplRST.MoveNext
                    End If
                End If
                DoEvents
            Loop
        End If
        .visible = True
    End With
    bEnCarga = False
End Sub

Private Sub sstInformeHomo_Click(PreviousTab As Integer)
    tabSELECTION = sstInformeHomo.Tab
    lblCantidadRestante = ""
    If lblHomoModo = "Editando informe" Then
        sstInformeHomo.TabEnabled(TABHOMO_RESPONSABLES) = False
        Exit Sub
    End If
    Select Case tabSELECTION
        Case TABHOMO_INFORME
            If glEsHomologador Then
                If CodigoCombo(cboHomoEstadoInforme, True) <> "TERMIN" Then
                    With tbHomologacion
                        .Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                        .Buttons(BOTONERAHOMO_AGREGAR).Enabled = True
                        .Buttons(BOTONERAHOMO_EDITAR).Enabled = True
                        .Buttons(BOTONERAHOMO_QUITAR).Enabled = True
                        .Buttons(BOTONERAHOMO_GUARDAR).Enabled = False
                        .Buttons(BOTONERAHOMO_CANCELAR).Enabled = False
                    End With
                End If
            End If
        Case TABHOMO_RESPONSABLES
            lblHomoRespoModo = ""
            If cboInforme.ListIndex > -1 Then
                cboItemResponsable.ListIndex = -1
                txtItemRespuesta.text = ""
                Call CargarResponsablesItem                 ' Cargo el combo con los responsables
                Call CargarResponsablesInformes
            End If
            If glEsHomologador Then
                If CodigoCombo(cboHomoEstadoInforme, True) <> "TERMIN" Then
                    With tbHomologacion
                        .Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = True
                        .Buttons(BOTONERAHOMO_AGREGAR).Enabled = False
                        .Buttons(BOTONERAHOMO_EDITAR).Enabled = False
                        .Buttons(BOTONERAHOMO_QUITAR).Enabled = False
                        .Buttons(BOTONERAHOMO_GUARDAR).Enabled = False
                        .Buttons(BOTONERAHOMO_CANCELAR).Enabled = False
                    End With
                    grdHomoResponsables.Height = 3735
                Else
                    grdHomoResponsables.Height = 5175
                End If
            Else
                If grdHomoResponsables.Height < 5175 Then grdHomoResponsables.Height = 5175
            End If
        Case TABHOMO_OBSERVACIONES
            If cboInforme.ListIndex > -1 Then
                Call CargarInformeObservaciones
            End If
            If glEsHomologador Then
                If CodigoCombo(cboHomoEstadoInforme, True) <> "TERMIN" Then
                    With tbHomologacion
                        .Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                        .Buttons(BOTONERAHOMO_AGREGAR).Enabled = True
                        .Buttons(BOTONERAHOMO_EDITAR).Enabled = True
                        .Buttons(BOTONERAHOMO_QUITAR).Enabled = True
                        .Buttons(BOTONERAHOMO_GUARDAR).Enabled = False
                        .Buttons(BOTONERAHOMO_CANCELAR).Enabled = False
                    End With
                End If
            End If
    End Select
End Sub

Private Sub CargarInformeObservaciones()
    txtHomoInfoObservaciones.text = ""
    txtHomoInfoObservaciones.text = sp_GetPeticionInfohMemo(glNumeroPeticion, CodigoCombo(cboInforme, True), Null)
End Sub

Private Sub CargarResponsablesInformes()
    With grdHomoResponsables
        .visible = False
        .Clear
        .cols = colHOMRES_TOTALCOLS
        .ScrollBars = flexScrollBarBoth
        .FocusRect = flexFocusLight
        .HighLight = flexHighlightAlways
        .Rows = 1
        ' NO VISIBLES
        .TextMatrix(0, colHOMRES_PETNROINTERNO) = "": .ColWidth(colHOMRES_PETNROINTERNO) = 0: .ColAlignment(colHOMRES_PETNROINTERNO) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_INFOID) = "": .ColWidth(colHOMRES_INFOID) = 0: .ColAlignment(colHOMRES_INFOID) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_INFOIDVER) = "": .ColWidth(colHOMRES_INFOIDVER) = 0: .ColAlignment(colHOMRES_INFOIDVER) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_CODRECURSO) = "cod_recurso": .ColWidth(colHOMRES_CODRECURSO) = 0: .ColAlignment(colHOMRES_CODRECURSO) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_CODPERFIL) = "Obl.": .ColWidth(colHOMRES_CODPERFIL) = 0: .ColAlignment(colHOMRES_CODPERFIL) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_DETALLEITEM) = "texto": .ColWidth(colHOMRES_DETALLEITEM) = 0
        .TextMatrix(0, colHOMRES_RESPUESTAITEM) = "respuesta": .ColWidth(colHOMRES_RESPUESTAITEM) = 0
        .TextMatrix(0, colHOMRES_CODESTADO) = "info_valor": .ColWidth(colHOMRES_CODESTADO) = 0: .ColAlignment(colHOMRES_CODESTADO) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_EMAIL) = "email": .ColWidth(colHOMRES_EMAIL) = 0: .ColAlignment(colHOMRES_EMAIL) = flexAlignLeftCenter
        ' VISIBLES
        .ColWidth(colHOMRES_MultiSelect) = 200
        .TextMatrix(0, colHOMRES_NOMRECURSO) = "Responsable": .ColWidth(colHOMRES_NOMRECURSO) = 1900: .ColAlignment(colHOMRES_NOMRECURSO) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_NOMPERFIL) = "Perfil": .ColWidth(colHOMRES_NOMPERFIL) = 1800: .ColAlignment(colHOMRES_NOMPERFIL) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_NIVEL) = "Nivel": .ColWidth(colHOMRES_NIVEL) = 600: .ColAlignment(colHOMRES_NIVEL) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_AREA) = "�rea": .ColWidth(colHOMRES_AREA) = 600: .ColAlignment(colHOMRES_AREA) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_INFOITEM) = "Item": .ColWidth(colHOMRES_INFOITEM) = 550: .ColAlignment(colHOMRES_INFOITEM) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_NOMESTADO) = "Status":: .ColWidth(colHOMRES_NOMESTADO) = 700: .ColAlignment(colHOMRES_NOMESTADO) = flexAlignCenterCenter
        .TextMatrix(0, colHOMRES_COMENTARIOS) = "Comentarios": .ColWidth(colHOMRES_COMENTARIOS) = 3250: .ColAlignment(colHOMRES_COMENTARIOS) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_ENVIO) = "Enviado": .ColWidth(colHOMRES_ENVIO) = 1200: .ColAlignment(colHOMRES_ENVIO) = flexAlignLeftCenter
        .TextMatrix(0, colHOMRES_REENVIO) = "Reenviado": .ColWidth(colHOMRES_REENVIO) = 1200: .ColAlignment(colHOMRES_REENVIO) = flexAlignLeftCenter
        .MergeCol(colHOMRES_PETNROINTERNO) = True
        .MergeCol(colHOMRES_INFOID) = True
        .MergeCol(colHOMRES_INFOIDVER) = True
        .MergeCol(colHOMRES_INFOITEM) = True
        .MergeCol(colHOMRES_CODRECURSO) = True
        .MergeCol(colHOMRES_NOMRECURSO) = True
        .MergeCol(colHOMRES_CODPERFIL) = True
        .MergeCol(colHOMRES_NOMPERFIL) = True
        .MergeCol(colHOMRES_NIVEL) = True
        .MergeCol(colHOMRES_AREA) = True
        
        .MergeCells = flexMergeRestrictRows
        .Font.name = "Tahoma"
        .Font.Size = 8
        .row = 0
        .WordWrap = True
        
        Call CambiarEfectoLinea2(grdHomoResponsables, prmGridEffectFontBold)
        Call PintarLinea2(grdHomoResponsables, prmGridFillRowColorLightGrey3)
        If sp_GetPeticionInfohr(glNumeroPeticion, CodigoCombo(cboInforme, True), Null) Then
            Do While Not aplRST.EOF
                If glEsHomologador Or (glEsHomologador = False And ClearNull(aplRST!info_valor) <> "0") Then
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, colHOMRES_MultiSelect) = ""
                    .TextMatrix(.Rows - 1, colHOMRES_PETNROINTERNO) = ClearNull(aplRST!pet_nrointerno)
                    .TextMatrix(.Rows - 1, colHOMRES_INFOID) = ClearNull(aplRST!info_id)
                    .TextMatrix(.Rows - 1, colHOMRES_INFOIDVER) = ClearNull(aplRST!info_idver)
                    .TextMatrix(.Rows - 1, colHOMRES_INFOITEM) = ClearNull(aplRST!info_item)
                    .TextMatrix(.Rows - 1, colHOMRES_CODRECURSO) = ClearNull(aplRST!cod_recurso)
                    .TextMatrix(.Rows - 1, colHOMRES_NOMRECURSO) = ClearNull(aplRST!nom_recurso)
                    .TextMatrix(.Rows - 1, colHOMRES_CODPERFIL) = ClearNull(aplRST!cod_perfil)
                    .TextMatrix(.Rows - 1, colHOMRES_NOMPERFIL) = ClearNull(aplRST!nom_perfil)
                    .TextMatrix(.Rows - 1, colHOMRES_NIVEL) = ClearNull(aplRST!cod_nivel)
                    .TextMatrix(.Rows - 1, colHOMRES_AREA) = ClearNull(aplRST!cod_area)
                    .TextMatrix(.Rows - 1, colHOMRES_CODESTADO) = ClearNull(aplRST!info_valor)
                    .TextMatrix(.Rows - 1, colHOMRES_NOMESTADO) = ClearNull(aplRST!nom_valor)
                    Call CambiarEfectoCelda2(grdHomoResponsables, colHOMRES_NOMESTADO, .Rows - 1, prmGridEffectFontBold)
                    .TextMatrix(.Rows - 1, colHOMRES_COMENTARIOS) = ClearNull(aplRST!info_comen)
                    .TextMatrix(.Rows - 1, colHOMRES_ENVIO) = ClearNull(aplRST!fecha_envio)
                    .TextMatrix(.Rows - 1, colHOMRES_REENVIO) = ClearNull(aplRST!fecha_reenvio)
                    .TextMatrix(.Rows - 1, colHOMRES_DETALLEITEM) = ClearNull(aplRST!infoprot_itemdsc)
                    .TextMatrix(.Rows - 1, colHOMRES_RESPUESTAITEM) = ClearNull(aplRST!infoprot_itemresp)
                    .TextMatrix(.Rows - 1, colHOMRES_EMAIL) = ClearNull(aplRST!email)
                    .RowHeight(.Rows - 1) = 800
                End If
                aplRST.MoveNext
                DoEvents
            Loop
            Call MostrarSeleccion
        End If
        .visible = True
    End With
    bEnCarga = False
End Sub

Private Sub MostrarSeleccion()
    With grdHomoResponsables
        If .rowSel > 0 Then
            lLastRowSelected = .rowSel
            lblItemTexto = ClearNull(.TextMatrix(.rowSel, colHOMRES_INFOITEM)) & ". " & ClearNull(.TextMatrix(.rowSel, colHOMRES_DETALLEITEM))
            txtItemRespuesta = IIf(ClearNull(.TextMatrix(.rowSel, colHOMRES_COMENTARIOS)) = "", ClearNull(.TextMatrix(.rowSel, colHOMRES_RESPUESTAITEM)), ClearNull(.TextMatrix(.rowSel, colHOMRES_COMENTARIOS)))
            cboItemResponsable.ListIndex = -1
            cboItemResponsable.ListIndex = PosicionCombo(cboItemResponsable, ClearNull(.TextMatrix(.rowSel, colHOMRES_CODRECURSO)), True)
        End If
    End With
End Sub

Private Sub grdHomoResponsables_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    With grdHomoResponsables
        If .rowSel = 0 Then
            Exit Sub
        End If
        If .row <= .rowSel Then
            nRwD = .row
            nRwH = .rowSel
        Else
            nRwH = .row
            nRwD = .rowSel
        End If
        If nRwD <> nRwH Then
            For i = 1 To .Rows - 1
               .TextMatrix(i, colHOMRES_MultiSelect) = ""
            Next
            Do While nRwD <= nRwH
                .TextMatrix(nRwD, colHOMRES_MultiSelect) = "�"
                nRwD = nRwD + 1
            Loop
            Exit Sub
        End If
    
        If Shift = vbCtrlMask Then
            If Button = vbLeftButton Then
                If .rowSel > 0 Then
                     If ClearNull(.TextMatrix(.rowSel, colHOMRES_MultiSelect)) = "" Then
                        .TextMatrix(.rowSel, colHOMRES_MultiSelect) = "�"
                     Else
                        .TextMatrix(.rowSel, colHOMRES_MultiSelect) = ""
                    End If
                End If
            End If
        End If
        If Shift = 0 Then
            If Button = vbLeftButton Then
                For i = 1 To .Rows - 1
                   .TextMatrix(i, colHOMRES_MultiSelect) = ""
                Next
                If .rowSel > 0 Then
                     .TextMatrix(.rowSel, colHOMRES_MultiSelect) = "�"
                End If
            End If
        End If
    End With
End Sub

Private Sub HomologacionConfirmar()
    Call EnviarEmailOMA
End Sub

Private Sub EnviarEmailOMA()
    Dim i As Long, J As Long
    Dim bHayDestinatario As Boolean
    Dim sResponsable As String
    Dim vResponsables() As udtResponsable
    
    bHayDestinatario = False
    If sstInformeHomo.Tab = TABHOMO_RESPONSABLES Then
        Call Puntero(True)
        With grdHomoResponsables        ' Verificar que haya selecci�n de recursos
            For i = 1 To .Rows - 1
                If .TextMatrix(i, colHOMRES_MultiSelect) = "�" Then
                    bHayDestinatario = True
                    Exit For
                End If
            Next i
        End With
        
        With grdHomoResponsables
            For i = 1 To .Rows - 1
                If .TextMatrix(i, colHOMRES_MultiSelect) = "�" Then
                    If sResponsable = "" Then
                        sResponsable = ClearNull(.TextMatrix(i, colHOMRES_CODRECURSO))
                        ReDim Preserve vResponsables(J)
                        vResponsables(J).Legajo = ClearNull(.TextMatrix(i, colHOMRES_CODRECURSO))
                        vResponsables(J).email = ClearNull(.TextMatrix(i, colHOMRES_EMAIL))
                        vResponsables(J).Items.Add ClearNull(.TextMatrix(i, colHOMRES_INFOITEM))
                    Else
                        If sResponsable <> ClearNull(.TextMatrix(i, colHOMRES_CODRECURSO)) Then       ' Cambi� el responsable
                            J = J + 1
                            ReDim Preserve vResponsables(J)
                            vResponsables(J).Legajo = ClearNull(.TextMatrix(i, colHOMRES_CODRECURSO))
                            vResponsables(J).email = ClearNull(.TextMatrix(i, colHOMRES_EMAIL))
                            vResponsables(J).Items.Add ClearNull(.TextMatrix(i, colHOMRES_INFOITEM))
                            sResponsable = ClearNull(.TextMatrix(i, colHOMRES_CODRECURSO))
                        Else        ' Mismo responsable con un nuevo item
                            vResponsables(J).Items.Add ClearNull(.TextMatrix(i, colHOMRES_INFOITEM))
                        End If
                    End If
                End If
            Next i
        End With
        
        If bHayDestinatario Then
            For i = 0 To UBound(vResponsables)
                Call EnviarCorreo_InformeHomologacion(glNumeroPeticion, CodigoCombo(cboInforme, True), Null, vResponsables(i))
            Next i
            Call Puntero(False)
            Call CargarResponsablesInformes
        Else
            MsgBox "No se ha seleccionado ning�n destinatario.", vbExclamation + vbOKOnly
            Call Puntero(False)
        End If
    End If
End Sub

Private Sub HomologacionNuevoInforme()
    Load frmPeticionesCargaInforme
    frmPeticionesCargaInforme.Show 1

    If frmPeticionesCargaInforme.PlantillaId > 0 Then
        Call AgregarNuevoInformeHomologacion(frmPeticionesCargaInforme.PlantillaId)
    End If
End Sub

Private Sub AgregarNuevoInformeHomologacion(Plantilla)
    Call AgregarInformeManual(glNumeroPeticion, glLOGIN_ID_REEMPLAZO, Plantilla)        ' Alta manual
    Call CargarInformesHomologacion                                                     ' Carga el informe reci�n creado
    Call CargarUnInformeDeHomologacion(CodigoCombo(cboInforme, True))
End Sub

Private Sub grdHomoResponsables_Click()
    Call MostrarSeleccion
End Sub

Private Sub CargarResponsablesItem()
    With grdHomoResponsables
        cboItemResponsable.Clear
        If sp_GetPeticionResponsables(glNumeroPeticion) Then
            Do While Not aplRST.EOF
                cboItemResponsable.AddItem ClearNull(aplRST.Fields!nom_recurso) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

Private Sub MostrarMensaje(mensaje)
    MsgBox mensaje, vbOKOnly + vbInformation
End Sub

Private Function MostrarPregunta(mensaje) As Boolean
    If MsgBox(mensaje, vbYesNo + vbQuestion) = vbYes Then
        MostrarPregunta = True
    Else
        MostrarPregunta = False
    End If
End Function

Private Sub cmdItemRespoEditar_Click()
    If cboInforme.ListIndex > -1 Then
        With grdHomoResponsables
            If .rowSel > 0 Then
                lblHomoRespoModo = "Modificando datos de responsable"
                cModoResponsables = MODO_EDIT
                lblCantidadRestante.visible = True
                tbHomologacion.Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                txtItemRespuesta = ClearNull(.TextMatrix(.rowSel, colHOMRES_COMENTARIOS))
                Call setHabilCtrl(cboItemResponsable, NORMAL)
                Call setHabilCtrl(txtItemRespuesta, NORMAL)
                Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)
                ' Pesta�as
                orjBtn(orjDAT).Enabled = False
                orjBtn(orjMEM).Enabled = False
                orjBtn(orjANX).Enabled = False
                orjBtn(orjADJ).Enabled = False
                If Hab_PesVinculado Then 'GMT05
                    orjBtn(orjDOC).Enabled = False
                End If 'GMT05
                orjBtn(orjCNF).Enabled = False
                orjBtn(orjEST).Enabled = False
            Else
                MsgBox "No hay items observados para este informe.", vbExclamation + vbOKOnly
            End If
        End With
    Else
        MsgBox "No hay ning�n informe seleccionado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub cmdItemRespoEliminar_Click()
    If cboInforme.ListIndex > -1 Then
        With grdHomoResponsables
            If .rowSel > 0 Then
                lblHomoRespoModo = "Quitar responsables"
                cModoResponsables = MODO_DELETE
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_EDITAR).Enabled = False
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_QUITAR).Enabled = False
                tbHomologacion.Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_GUARDAR).Enabled = True
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_CANCELAR).Enabled = True
                ' Otros
                Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)            ' add -109- c.
                ' Pesta�as
                orjBtn(orjDAT).Enabled = False
                orjBtn(orjMEM).Enabled = False
                orjBtn(orjANX).Enabled = False
                orjBtn(orjADJ).Enabled = False
                If Hab_PesVinculado Then ' GMT05
                    orjBtn(orjDOC).Enabled = False
                End If 'GMT05
                orjBtn(orjCNF).Enabled = False
                orjBtn(orjEST).Enabled = False
            Else
                MsgBox "No hay responsables seleccionados.", vbExclamation + vbOKOnly
            End If
        End With
    Else
        MsgBox "No hay ning�n informe seleccionado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub cmdItemRespoCancelar_Click()
    lblCantidadRestante.visible = False
    cModoResponsables = MODO_VIEW
    lblHomoRespoModo = ""
    tbHomologacion.Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = True
    Call setHabilCtrl(cboItemResponsable, DISABLE)
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_EDITAR).Enabled = True
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_QUITAR).Enabled = True
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_GUARDAR).Enabled = False
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_CANCELAR).Enabled = False
    Call setHabilCtrl(txtItemRespuesta, DISABLE)
    ' Otros
    Call setHabilCtrl(chkIncluirItemsOpcionales, NORMAL)
    ' Pesta�as
    orjBtn(orjDAT).Enabled = True
    orjBtn(orjMEM).Enabled = True
    orjBtn(orjANX).Enabled = True
    orjBtn(orjADJ).Enabled = True
    If Hab_PesVinculado Then 'GMT05
        orjBtn(orjDOC).Enabled = True
    End If 'GMT05
    orjBtn(orjCNF).Enabled = True
    orjBtn(orjEST).Enabled = True
End Sub

Private Sub cmdItemRespoGuardar_Click()
    Call GuardarDatosDeItemResponsable
End Sub

Private Sub GuardarDatosDeItemResponsable()
    Dim i As Integer
    
    With grdHomoResponsables
        If .rowSel > 0 Then
            Select Case cModoResponsables
                Case MODO_EDIT
                    ' Si lo que cambia es el responsable, hay que deletear el registro y luego agregar uno nuevo con el cambio
                    If ClearNull(.TextMatrix(.rowSel, colHOMRES_CODRECURSO)) <> CodigoCombo(cboItemResponsable, True) Then
                        Call sp_DeletePeticionInfohr(.TextMatrix(.rowSel, colHOMRES_PETNROINTERNO), .TextMatrix(.rowSel, colHOMRES_INFOID), .TextMatrix(.rowSel, colHOMRES_INFOIDVER), .TextMatrix(.rowSel, colHOMRES_INFOITEM), .TextMatrix(.rowSel, colHOMRES_CODRECURSO))
                        Call sp_InsertPeticionInfohr(.TextMatrix(.rowSel, colHOMRES_PETNROINTERNO), .TextMatrix(.rowSel, colHOMRES_INFOID), .TextMatrix(.rowSel, colHOMRES_INFOIDVER), .TextMatrix(.rowSel, colHOMRES_INFOITEM), CodigoCombo(cboItemResponsable, True), Null, Null, Null, .TextMatrix(.rowSel, colHOMRES_CODESTADO), ClearNull(txtItemRespuesta))
                    Else
                        ' Si lo que cambia es simplemente el texto, actualizamos (update) el registro.
                        Call sp_UpdatePeticionInfohrField(.TextMatrix(.rowSel, colHOMRES_PETNROINTERNO), .TextMatrix(.rowSel, colHOMRES_INFOID), .TextMatrix(.rowSel, colHOMRES_INFOIDVER), .TextMatrix(.rowSel, colHOMRES_INFOITEM), .TextMatrix(.rowSel, colHOMRES_CODRECURSO), "INFO_COMEN", ClearNull(txtItemRespuesta), Null, Null)
                    End If
                    '}
                    Call CargarResponsablesInformes             ' Actualizo en pantalla los comentarios que acaba de modificar el homologador
                    lblCantidadRestante.visible = False
                Case MODO_DELETE
                    If MsgBox("�Confirma quitar el/los responsable(s) seleccionado(s)?", vbQuestion + vbYesNo, "Quitar responsables") = vbYes Then
                        For i = 1 To .Rows - 1
                            If .TextMatrix(i, colHOMRES_MultiSelect) = "�" Then
                                Call sp_DeletePeticionInfohr(.TextMatrix(i, colHOMRES_PETNROINTERNO), .TextMatrix(i, colHOMRES_INFOID), .TextMatrix(i, colHOMRES_INFOIDVER), .TextMatrix(i, colHOMRES_INFOITEM), .TextMatrix(i, colHOMRES_CODRECURSO))
                            End If
                            DoEvents
                        Next i
                        Call CargarResponsablesInformes
                    End If
                    GoTo Finalizar
            End Select
        End If
    End With
Finalizar:
    lblHomoRespoModo = ""
    ' Datos
    Call setHabilCtrl(cboItemResponsable, DISABLE)
    Call setHabilCtrl(txtItemRespuesta, DISABLE)
    ' Botonera del item
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_EDITAR).Enabled = True
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_QUITAR).Enabled = True
    tbHomologacion.Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = True
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_GUARDAR).Enabled = False
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_CANCELAR).Enabled = False
    ' Otros
    Call setHabilCtrl(chkIncluirItemsOpcionales, NORMAL)            ' add -109- c.
    ' Pesta�as
    orjBtn(orjDAT).Enabled = True
    orjBtn(orjMEM).Enabled = True
    orjBtn(orjANX).Enabled = True
    orjBtn(orjADJ).Enabled = True
    If Hab_PesVinculado Then 'GMT05
        orjBtn(orjDOC).Enabled = True
    End If 'GMT05
    orjBtn(orjCNF).Enabled = True
    orjBtn(orjEST).Enabled = True
End Sub

Private Sub CargarHomologacion()
    If glModoPeticion = "ALTA" Then Exit Sub
    DoEvents
    Call InicializarInformesHomologacion
End Sub

Private Sub EnviarInformesPorMailAResponsables()
    Dim i As Long
    Dim sTitulo As String
    Dim sMensaje As String
    Dim sDestinatario As String
    
    With grdHomoResponsables
        For i = 1 To .Rows - 1
            If .TextMatrix(i, colHOMRES_MultiSelect) = "�" Then
                Debug.Print .TextMatrix(i, colHOMRES_INFOITEM)
                If ClearNull(sDestinatario) = "" Then
                    sTitulo = ""
                    sDestinatario = .TextMatrix(i, colHOMRES_NOMRECURSO)
                    sMensaje = sMensaje & vbCrLf & .TextMatrix(i, colHOMRES_INFOITEM) & ". " & .TextMatrix(i, colHOMRES_NOMESTADO) & ": " & .TextMatrix(i, colHOMRES_COMENTARIOS)
                Else
                    If ClearNull(sDestinatario) <> .TextMatrix(i, colHOMRES_NOMRECURSO) Then
                        MsgBox "Destinatario: " & sDestinatario & vbCrLf & "Mensaje: " & sMensaje
                        sDestinatario = .TextMatrix(i, colHOMRES_NOMRECURSO)
                        i = i - 1
                        sMensaje = ""
                    Else
                        sMensaje = sMensaje & vbCrLf & .TextMatrix(i, colHOMRES_INFOITEM) & ". " & .TextMatrix(i, colHOMRES_NOMESTADO) & ": " & .TextMatrix(i, colHOMRES_COMENTARIOS)
                    End If
                End If
            End If
        Next i
        MsgBox "Destinatario: " & sDestinatario & vbCrLf & "Mensaje: " & sMensaje
    End With
    MsgBox STATUS_READY
End Sub

Private Sub txtItemRespuesta_Change()
    lblCantidadRestante = "Quedan " & 255 - Len(Trim(txtItemRespuesta))
End Sub
'}

Private Sub cboEmp_Click()
    cboEmp.ToolTipText = ""
    lblCustomToolTip.Caption = ""
    If cboEmp.ListIndex > -1 Then cboEmp.ToolTipText = TextoCombo(cboEmp, CodigoCombo(cboEmp, True))
    If flgEnCarga Then Exit Sub
    bIndicadoresInicializados = False
    Call InicializarIndicadores
    
    If Hab_beneficio Then 'GMT05
        If glModoPeticion = "ALTA" Then
            Call CargarGrillaBeneficios
            
            If Hab_Kpi Then 'GMT05
                Call CargarGrillaKPI
            End If 'GMT05
        End If
    End If 'GMT05
End Sub

Private Sub InicializarIndicadores()
    Dim i As Integer
    ' Busca los indicadores para la empresa
    If Not bIndicadoresInicializados Then
        If sp_GetIndicador(Null, CodigoCombo(cboEmp, True)) Then
            i = 0
            Do While Not aplRST.EOF
                ReDim Preserve vIndicadores(i)
                ReDim Preserve vIndicadoresSuma(i)
                ReDim Preserve vIndicadoresCategoria(i)
                vIndicadores(i) = aplRST.Fields!indicadorId
                vIndicadoresSuma(i) = 0
                vIndicadoresCategoria(i) = ClearNull(aplRST.Fields!indicadorCategoria)
                aplRST.MoveNext
                i = i + 1
            Loop
            bIndicadoresInicializados = True
        End If
    End If
End Sub

' *********************
' A RECICLAR 29.01.2016
' *********************
Private Sub cboTipopet_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica el tipo de petici�n: Normal, Propia, Especial, Auditor�a, etc."
    End If
End Sub

Private Sub cboClase_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Para una descripci�n ampliada sobre esta clase, posicione el puntero del mouse sobre el combobox, y se desplegar� el tooltip asociado con el detalle ampliado de la petici�n."
    End If
End Sub

Private Sub cboImpacto_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Cuando es necesario adaptar o ampliar el entorno tecnol�gico disponible."
    End If
End Sub

Private Sub cboRegulatorio_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Requerimientos de cumplimiento obligatorio, a partir de exigencias de entes reguladores externos."
    End If
End Sub

Private Sub cboImportancia_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Establece el nivel de visibilidad de acuerdo a su importancia (a mayor importancia, mayor nivel jer�rquico de visibilidad)."
    End If
End Sub

Private Sub txtPrjId_GotFocus()
    txtPrjId.SelStart = 0: txtPrjId.SelLength = Len(txtPrjId)
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Puede seleccionar proyectos existentes presionando el bot�n con la lupa."
    End If
End Sub

Private Sub txtPrjSubId_GotFocus()
    txtPrjSubId.SelStart = 0: txtPrjSubId.SelLength = Len(txtPrjSubId)
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Puede seleccionar proyectos existentes presionando el bot�n con la lupa."
    End If
End Sub

Private Sub txtPrjSubsId_GotFocus()
    txtPrjSubsId.SelStart = 0: txtPrjSubsId.SelLength = Len(txtPrjSubsId)
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Puede seleccionar proyectos existentes presionando el bot�n con la lupa."
    End If
End Sub

Private Sub cmdSelProyectos_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Presione este bot�n para seleccionar proyectos preexistentes."
    End If
End Sub

Private Sub txtPrjNom_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Establece el nombre descriptivo del proyecto asociado a esta petici�n"
    End If
End Sub

Private Sub cboCorpLocal_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Determina si es una petici�n a nivel local o a nivel corporativo"
    End If
End Sub

Private Sub cboOrientacion_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica en t�rminos gen�ricos a qu� apunta el desarrollo o el pedido: qu� tipo de mejora se aspira a conseguir."
    End If
End Sub

Private Sub txtEstado_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "El estado de la petici�n es el resultado consolidado de los sectores y grupos involucrados en esta petici�n (en rojo estados terminales; en verde aquellas en estados activos)."
    End If
End Sub

Private Sub txtSituacion_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "La situaci�n es el resultado de ciertas condiciones a un momento particular producto de procesos de escalamiento, vencimientos autom�ticos, etc."
    End If
End Sub

Private Sub txtFechaEstado_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Fecha en que se alcanz� el estado actual de la petici�n."
    End If
End Sub

Private Sub txtNroAnexada_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "N� de petici�n a la cual se ha anexado la petici�n actual. Puede acceder a la petici�n destino a trav�s del bot�n."
    End If
End Sub

Private Sub txtHspresup_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtHsPlanif_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFiniplan_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFinireal_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFfinplan_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFfinreal_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub cboSector_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica el sector solicitante de la petici�n actual."
    End If
End Sub

Private Sub txtFechaPedido_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "La fecha real en que la petici�n fu� cargada en el sistema."
    End If
End Sub

Private Sub cboSolicitante_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Solicitante formal de la petici�n actual"
    End If
End Sub

Private Sub cboReferente_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Referente del solicitante de la petici�n."
    End If
End Sub

Private Sub cboSupervisor_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Supervisor del solicitante de la petici�n."
    End If
End Sub

Private Sub cboDirector_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Autorizante del solicitante de la petici�n"
    End If
End Sub

Private Sub cboBpar_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica el Referente de Sistemas asociado a la petici�n actual"
    End If
End Sub

Private Sub txtFechaComite_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Fecha en la cual fu� entregada al Referente de Sistemas"
    End If
End Sub

Private Sub txtNroAsignado_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "N�mero secuencial identificatorio asignado autom�ticamente para peticiones aprobadas."
    End If
End Sub

Private Sub txtTitulo_DblClick()
    ' Copiar el titulo al portapapeles
    Clipboard.Clear
    
    'Copiar todo el contenido de la caja de texto
    Clipboard.SetText txtTitulo.text, vbCFText
End Sub

Private Sub txtTitulo_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        If txtTitulo.Enabled Then
            lblCustomToolTip.Caption = "T�tulo descriptivo para esta petici�n. Este ser� el utilizado en todos los reportes del sistema." & vbCrLf & "Longitud m�xima permitida: " & txtTitulo.MaxLength & " caracteres."
        End If
    End If
End Sub

Private Sub CargarGrillaKPI()
    If bDebuggear Then
        Call doLog("CargarGrillaKPI()")
    End If
    txtKPI(8).Caption = "para empresa " & vEmpresaNombre(cboEmp.ListIndex)
    Call CargarKPIPorEmpresa
    With grdKPI
        .visible = False
        .Clear
        .cols = colKPI_TOTALCOLS
        .HighLight = flexHighlightWithFocus
        .AllowUserResizing = flexResizeColumns
        .Rows = 1
        ' NO VISIBLES
        .TextMatrix(0, colKPI_PETNROINTERNO) = "colKPI_PETNROINTERNO": .ColWidth(colKPI_PETNROINTERNO) = 0: .ColAlignment(colKPI_PETNROINTERNO) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_KPIID) = "colKPI_KPIID": .ColWidth(colKPI_KPIID) = 0: .ColAlignment(colKPI_KPIID) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_REQ) = "colKPI_REQ": .ColWidth(colKPI_REQ) = 0: .ColAlignment(colKPI_REQ) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_HAB) = "colKPI_HAB": .ColWidth(colKPI_HAB) = 0: .ColAlignment(colKPI_HAB) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_UMID) = "colKPI_UMID": .ColWidth(colKPI_UMID) = 0: .ColAlignment(colKPI_UMID) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_AYUDA) = "colKPI_AYUDA": .ColWidth(colKPI_AYUDA) = 0: .ColAlignment(colKPI_AYUDA) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_DESCRIPCION) = "colKPI_DESCRIPCION": .ColWidth(colKPI_DESCRIPCION) = 0: .ColAlignment(colKPI_DESCRIPCION) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_TIEMPOUM) = "colKPI_TIEMPOUM": .ColWidth(colKPI_TIEMPOUM) = 0: .ColAlignment(colKPI_TIEMPOUM) = flexAlignLeftCenter
        ' VISIBLES
        .TextMatrix(0, colKPI_KPINOM) = "Indicador": .ColWidth(colKPI_KPINOM) = 3600: .ColAlignment(colKPI_KPINOM) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_UMDSC) = "Unidad de medida": .ColWidth(colKPI_UMDSC) = 1800: .ColAlignment(colKPI_UMDSC) = flexAlignLeftCenter
        .TextMatrix(0, colKPI_VALORESP) = "Valor esperado del indicador": .ColWidth(colKPI_VALORESP) = 1700: .ColAlignment(colKPI_VALORESP) = flexAlignRightCenter
        .TextMatrix(0, colKPI_VALORPAR) = "Valor de partida": .ColWidth(colKPI_VALORPAR) = 1700: .ColAlignment(colKPI_VALORPAR) = flexAlignRightCenter
        .TextMatrix(0, colKPI_TIEMPO) = "Tiempo para valor esperado": .ColWidth(colKPI_TIEMPO) = 1200: .ColAlignment(colKPI_TIEMPO) = flexAlignRightCenter
        .TextMatrix(0, colKPI_TIEMPOUMDESC) = "Unidad de medida de tiempo": .ColWidth(colKPI_TIEMPOUMDESC) = 1200: .ColAlignment(colKPI_TIEMPOUMDESC) = flexAlignLeftCenter
        .Font.name = "Tahoma"
        .Font.Size = 8
        .row = 0
        .RowHeightMin = 265
        Call PintarLinea2(grdKPI, prmGridFillRowColorLightGrey4)
        Call CambiarEfectoLinea2(grdKPI, prmGridEffectFontBold)
        If sp_GetPeticionKPI(glNumeroPeticion, CodigoCombo(cboEmp, True)) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colKPI_PETNROINTERNO) = ClearNull(aplRST.Fields!pet_nrointerno)
                .TextMatrix(.Rows - 1, colKPI_KPIID) = ClearNull(aplRST.Fields!pet_kpiid)
                .TextMatrix(.Rows - 1, colKPI_KPINOM) = ClearNull(aplRST.Fields!kpinom)
                .TextMatrix(.Rows - 1, colKPI_REQ) = ClearNull(aplRST.Fields!kpireq)
                .TextMatrix(.Rows - 1, colKPI_HAB) = ClearNull(aplRST.Fields!kpihab)
                .TextMatrix(.Rows - 1, colKPI_DESCRIPCION) = ClearNull(aplRST.Fields!pet_kpidesc)
                .TextMatrix(.Rows - 1, colKPI_UMID) = ClearNull(aplRST.Fields!kpi_unimedId)
                .TextMatrix(.Rows - 1, colKPI_UMDSC) = ClearNull(aplRST.Fields!unimedDesc)
                .TextMatrix(.Rows - 1, colKPI_VALORESP) = Format(ClearNull(aplRST.Fields!valorEsperado), "###,###,###,##0.00")
                .TextMatrix(.Rows - 1, colKPI_VALORPAR) = Format(ClearNull(aplRST.Fields!valorPartida), "###,###,###,##0.00")
                .TextMatrix(.Rows - 1, colKPI_TIEMPO) = Format(ClearNull(aplRST.Fields!tiempoEsperado), "###,###,###,##0.00")
                .TextMatrix(.Rows - 1, colKPI_TIEMPOUM) = ClearNull(aplRST.Fields!unimedTiempo)
                .TextMatrix(.Rows - 1, colKPI_TIEMPOUMDESC) = ClearNull(aplRST.Fields!unimedTiempoDesc)
                If .Rows Mod 2 > 0 Then Call PintarLinea2(grdKPI, prmGridFillRowColorLightGrey2)
                aplRST.MoveNext
                DoEvents
            Loop
            If .Rows > 1 Then .FixedRows = 1
            Call MostrarSeleccionKPI
        End If
        .RowHeight(0) = 750
        .visible = True
    End With
End Sub

Private Sub CargarKPIPorEmpresa()
    Dim i As Long
    With cboKPIIndicador
        i = 0
        .Clear
        If sp_GetIndicadorKPI(Null, CodigoCombo(cboEmp, True)) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!kpinom) & ESPACIOS & "||" & aplRST.Fields!kpiid
                ReDim Preserve vIndicadorKPI(i)
                vIndicadorKPI(i) = aplRST.Fields!kpiid
                i = i + 1
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
End Sub

Private Sub CargarValidacionTecnica(oGrid As MSHFlexGrid, Titulo As String, Tipo As Integer)
    Dim i As Long
    Dim J As Long
    
    If bDebuggear Then Call doLog("CargarValidacionTecnica()")
    With oGrid
        .visible = False
        .Clear
        .Rows = 2
        .cols = colVALIDACION_TOTALCOLS
        .Font.name = "Tahoma"
        .Font.Size = 8
        .FontFixed.Bold = True
        ' VISIBLES
        If Titulo = VALIDACION_DATA_TITULO Then '-> GMT02
           .row = .Rows - 2: .col = colVALIDACION_ITEM: .CellFontBold = True: .CellForeColor = &HFF&: .CellFontName = "Arial Black": .CellFontSize = 10 '-> GMT02
        Else '-> GMT02
           .row = .Rows - 2: .col = colVALIDACION_ITEM: .CellFontBold = True: .CellForeColor = &HC2752E: .CellFontName = "Arial Black": .CellFontSize = 10
        End If '-> GMT02
        
        .TextMatrix(0, colVALIDACION_ITEM) = Titulo: .ColAlignment(colVALIDACION_ITEM) = flexAlignCenterCenter
        .TextMatrix(0, colVALIDACION_PREGUNTA) = Titulo: .ColAlignment(colVALIDACION_PREGUNTA) = flexAlignCenterCenter
        .TextMatrix(0, colVALIDACION_RESPUESTA) = Titulo: .ColAlignment(colVALIDACION_RESPUESTA) = flexAlignCenterCenter
        .TextMatrix(1, colVALIDACION_ITEM) = "Item": .ColWidth(colVALIDACION_ITEM) = 700: .ColAlignment(colVALIDACION_ITEM) = flexAlignCenterCenter
        .TextMatrix(1, colVALIDACION_PREGUNTA) = "Preguntas": .ColWidth(colVALIDACION_PREGUNTA) = 9000: .ColAlignment(colVALIDACION_PREGUNTA) = flexAlignLeftCenter
        .TextMatrix(1, colVALIDACION_RESPUESTA) = "Respuestas": .ColWidth(colVALIDACION_RESPUESTA) = 2000: .ColAlignment(colVALIDACION_RESPUESTA) = flexAlignCenterCenter
        ' INVISIBLES
        .TextMatrix(0, colVALIDACION_ID) = "id": .ColWidth(colVALIDACION_ID) = 0:  .ColAlignment(colVALIDACION_ID) = flexAlignLeftCenter
        .TextMatrix(0, colVALIDACION_ANTERIOR) = "anterior": .ColWidth(colVALIDACION_ANTERIOR) = 0:  .ColAlignment(colVALIDACION_ANTERIOR) = flexAlignLeftCenter
        .TextMatrix(0, colVALIDACION_RESPUESTAKEY) = "key": .ColWidth(colVALIDACION_RESPUESTAKEY) = 0: .ColAlignment(colVALIDACION_RESPUESTAKEY) = flexAlignLeftCenter
        .TextMatrix(0, colVALIDACION_ITEMKEY) = "itemkey": .ColWidth(colVALIDACION_ITEMKEY) = 0: .ColAlignment(colVALIDACION_ITEMKEY) = flexAlignLeftCenter
        .MergeRow(0) = True
        .MergeCells = flexMergeRestrictRows
        .Font.Bold = False
        .WordWrap = True
        
        i = 0: J = 0
        If glModoPeticion <> "ALTA" Then
            bValidacionTecnicaCompleta = True
            If sp_GetPeticionValidacion(glNumeroPeticion, Tipo, Null) Then
                .Tag = aplRST.RecordCount
                Do While Not aplRST.EOF
                    i = i + 1
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, colVALIDACION_ID) = ClearNull(aplRST.Fields!valid)
                    .TextMatrix(.Rows - 1, colVALIDACION_ITEMKEY) = ClearNull(aplRST.Fields!valitem)
                    .TextMatrix(.Rows - 1, colVALIDACION_ITEM) = i
                    .TextMatrix(.Rows - 1, colVALIDACION_PREGUNTA) = ClearNull(aplRST.Fields!valdesc)
                    Select Case ClearNull(aplRST.Fields!valitemvalor)
                        Case "S": .TextMatrix(.Rows - 1, colVALIDACION_RESPUESTA) = "Si": J = J + 1
                        Case "N": .TextMatrix(.Rows - 1, colVALIDACION_RESPUESTA) = "No": J = J + 1
                        Case Else
                            .TextMatrix(.Rows - 1, colVALIDACION_RESPUESTA) = "-"
                            bValidacionTecnicaCompleta = False
                    End Select
                    .TextMatrix(.Rows - 1, colVALIDACION_ANTERIOR) = ClearNull(aplRST.Fields!valitemvalor)
                    .RowHeight(.Rows - 1) = colVALIDACION_ROWHEIGHT - 100
                    .TextMatrix(1, colVALIDACION_RESPUESTA) = "Respuestas (" & J & "/" & .Tag & ")"
                    If .Rows Mod 2 > 0 Then Call PintarLinea2(oGrid, prmGridFillRowColorLightGrey2)
                    If Not bFlagValidacionTecnica Then Call CambiarForeColorLinea2(oGrid, prmGridFillRowColorLightGrey4)
                    aplRST.MoveNext
                    DoEvents
                Loop
            Else
                If sp_GetValidacion(Tipo, Null) Then
                    .Tag = aplRST.RecordCount
                    Do While Not aplRST.EOF
                        i = i + 1
                        .Rows = .Rows + 1
                        .TextMatrix(.Rows - 1, colVALIDACION_ID) = ClearNull(aplRST.Fields!valid)
                        .TextMatrix(.Rows - 1, colVALIDACION_ITEMKEY) = ClearNull(aplRST.Fields!valitem)
                        .TextMatrix(.Rows - 1, colVALIDACION_ITEM) = i
                        .TextMatrix(.Rows - 1, colVALIDACION_PREGUNTA) = ClearNull(aplRST.Fields!valdesc)
                        .TextMatrix(.Rows - 1, colVALIDACION_RESPUESTA) = "-"
                        .TextMatrix(.Rows - 1, colVALIDACION_ANTERIOR) = ""
                        .RowHeight(.Rows - 1) = colVALIDACION_ROWHEIGHT
                        .TextMatrix(1, colVALIDACION_RESPUESTA) = "Respuestas (" & J & "/" & .Tag & ")"
                        If .Rows Mod 2 > 0 Then Call PintarLinea2(oGrid, prmGridFillRowColorLightGrey2)
                        If Not bFlagValidacionTecnica Then
                            Call CambiarForeColorLinea2(oGrid, prmGridFillRowColorDarkGrey)
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            End If
        End If
        .FixedCols = 0
        If .Rows > 2 Then .FixedRows = 2
        .RowHeight(0) = 650
        .RowHeight(1) = 450
        .visible = True
    End With
End Sub

Private Sub CargarGrillaBeneficios()
    If bDebuggear Then Call doLog("CargarGrillaBeneficios()")
    lblEmpresa.Caption = vEmpresaNombre(cboEmp.ListIndex)
    If valoraciones.Count = 0 Then Call CargaMatrizBeneficios
    With grdBeneficios
        .visible = False
        .Clear
        .cols = colBENE_TOTALCOLS
        If .Rows > 1 Then .FixedRows = 1
        .FocusRect = flexFocusLight
        .HighLight = flexHighlightAlways
        .Rows = 1
        ' NO VISIBLES
        .TextMatrix(0, colBENE_PETNROINTERNO) = "colBENE_PETNROINTERNO": .ColWidth(colBENE_PETNROINTERNO) = 0: .ColAlignment(colBENE_PETNROINTERNO) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_INDICADORID) = "colBENE_INDICADORID": .ColWidth(colBENE_INDICADORID) = 0: .ColAlignment(colBENE_INDICADORID) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_DRIVERID) = "colBENE_DRIVERID": .ColWidth(colBENE_DRIVERID) = 0: .ColAlignment(colBENE_DRIVERID) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_DRIVERRESP) = "colBENE_DRIVERRESP": .ColWidth(colBENE_DRIVERRESP) = 0: .ColAlignment(colBENE_DRIVERRESP) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_DRIVERVALOR) = "colBENE_DRIVERVALOR": .ColWidth(colBENE_DRIVERVALOR) = 0: .ColAlignment(colBENE_DRIVERVALOR) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_AUDITFECHA) = "colBENE_AUDITFECHA": .ColWidth(colBENE_AUDITFECHA) = 0: .ColAlignment(colBENE_AUDITFECHA) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_AUDITUSER) = "colBENE_AUDITUSER": .ColWidth(colBENE_AUDITUSER) = 0: .ColAlignment(colBENE_AUDITUSER) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_REQUERIDO) = "requerido": .ColWidth(colBENE_REQUERIDO) = 0
        ' VISIBLES
        .TextMatrix(0, colBENE_INDICADORNOM) = "Indicador": .ColWidth(colBENE_INDICADORNOM) = 1000: .ColAlignment(colBENE_INDICADORNOM) = flexAlignCenterCenter
        .TextMatrix(0, colBENE_SEPARADOR1) = "": .ColWidth(colBENE_SEPARADOR1) = 200: .ColAlignment(colBENE_SEPARADOR1) = flexAlignCenterCenter
        .TextMatrix(0, colBENE_DRIVERNOM) = "Subindicador": .ColWidth(colBENE_DRIVERNOM) = 2500: .ColAlignment(colBENE_DRIVERNOM) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_DRIVERRESPNOM) = "Responsable": .ColWidth(colBENE_DRIVERRESPNOM) = 1200: .ColAlignment(colBENE_DRIVERRESPNOM) = flexAlignCenterCenter
        .TextMatrix(0, colBENE_DRIVERVALORDESC) = "Valor": .ColWidth(colBENE_DRIVERVALORDESC) = 3000: .ColAlignment(colBENE_DRIVERVALORDESC) = flexAlignCenterCenter
        .TextMatrix(0, colBENE_DRIVERAYUDA) = "Ayuda": .ColWidth(colBENE_DRIVERAYUDA) = 4000: .ColAlignment(colBENE_DRIVERAYUDA) = flexAlignLeftCenter
        .TextMatrix(0, colBENE_VALORREFERENCIA) = "Referencia": .ColWidth(colBENE_VALORREFERENCIA) = IIf(glUsrPerfilActual = "SADM", 2000, 0): .ColAlignment(colBENE_VALORREFERENCIA) = flexAlignLeftCenter
        ' MERGE COLUMNS
        .MergeCol(colBENE_PETNROINTERNO) = True
        .MergeCol(colBENE_INDICADORID) = True
        .MergeCol(colBENE_INDICADORNOM) = True
        .MergeCol(colBENE_DRIVERID) = True
        .MergeCells = flexMergeFree
        .Font.name = "Tahoma"
        .Font.Size = 8
        .row = 0
        .WordWrap = True
       
        Call CambiarEfectoLinea2(grdBeneficios, prmGridEffectFontBold)
        Call PintarLinea2(grdBeneficios, prmGridFillRowColorLightGrey4)
        
        If Not sp_GetPeticionBeneficios(glNumeroPeticion, Null, CodigoCombo(cboEmp, True)) Then
            Call sp_GetPeticionBeneficios(0, Null, CodigoCombo(cboEmp, True))
        End If
        
        Do While Not aplRST.EOF
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colBENE_PETNROINTERNO) = ClearNull(aplRST.Fields!pet_nrointerno)
            .TextMatrix(.Rows - 1, colBENE_INDICADORID) = ClearNull(aplRST.Fields!indicadorId)
            .TextMatrix(.Rows - 1, colBENE_DRIVERID) = ClearNull(aplRST.Fields!driverId)
            .TextMatrix(.Rows - 1, colBENE_DRIVERRESP) = ClearNull(aplRST.Fields!driverResp)
            .TextMatrix(.Rows - 1, colBENE_DRIVERVALOR) = ClearNull(aplRST.Fields!Valor)
            .TextMatrix(.Rows - 1, colBENE_AUDITFECHA) = ClearNull(aplRST.Fields!audit_fe)
            .TextMatrix(.Rows - 1, colBENE_AUDITUSER) = ClearNull(aplRST.Fields!audit_usr)
            .TextMatrix(.Rows - 1, colBENE_INDICADORNOM) = ClearNull(aplRST.Fields!indicadorNom): .row = .Rows - 1: .col = colBENE_INDICADORNOM: .CellFontBold = True: .CellForeColor = &HC2752E
            .TextMatrix(.Rows - 1, colBENE_DRIVERNOM) = ClearNull(aplRST.Fields!subindicadorNom): .row = .Rows - 1: .col = colBENE_DRIVERNOM: .CellFontBold = True
            .TextMatrix(.Rows - 1, colBENE_DRIVERRESPNOM) = ClearNull(aplRST.Fields!Responsable)
            .TextMatrix(.Rows - 1, colBENE_DRIVERVALORDESC) = "- " & ClearNull(aplRST.Fields!valor_nom): .row = .Rows - 1: .col = colBENE_DRIVERVALORDESC: .CellFontBold = True
            .TextMatrix(.Rows - 1, colBENE_DRIVERAYUDA) = ClearNull(aplRST.Fields!driverAyuda)
            .TextMatrix(.Rows - 1, colBENE_VALORREFERENCIA) = ClearNull(aplRST.Fields!valor_referencia)
            .TextMatrix(.Rows - 1, colBENE_REQUERIDO) = ClearNull(aplRST.Fields!driverReq)
            .RowHeight(.Rows - 1) = 850
            If .Rows Mod 2 > 0 Then Call PintarLinea2(grdBeneficios, prmGridFillRowColorLightGrey2, 7)
            aplRST.MoveNext
            DoEvents
        Loop
        .FixedCols = 6
        If .Rows > 1 Then .FixedRows = 1
        .RowHeight(0) = 350
        .visible = True
    End With
End Sub

Private Sub CargaMatrizBeneficios()
    Dim i As Long
    Dim driver As Integer
    Dim numerador As Integer
    
    If sp_GetValoracion(Null, Null) Then
        Do While Not aplRST.EOF
            valoraciones.Add "- " & ClearNull(aplRST.Fields!valorTexto), ClearNull(aplRST.Fields!valorId)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
End Sub

Private Function GuardarKPI() As Boolean
    Dim i As Long
    
    GuardarKPI = False
    With grdKPI
        For i = 1 To .Rows - 1
            Call sp_InsertPeticionKPI(glNumeroPeticion, _
                                    .TextMatrix(i, colKPI_KPIID), _
                                    .TextMatrix(i, colKPI_DESCRIPCION), _
                                    .TextMatrix(i, colKPI_UMID), _
                                    .TextMatrix(i, colKPI_VALORESP), _
                                    .TextMatrix(i, colKPI_VALORPAR), _
                                    .TextMatrix(i, colKPI_TIEMPO), _
                                    .TextMatrix(i, colKPI_TIEMPOUM))
        Next i
        GuardarKPI = True
    End With
End Function

Private Sub GuardarBeneficios()
    Dim i As Long
    Dim sumaImpacto As Double
    Dim sumaFacilidad As Double
    Dim bCompleto As Boolean
    
    If bDebuggear Then
        Call doLog("GuardarBeneficios()")
    End If
    
    sumaImpacto = 0
    sumaFacilidad = 0
    bCompleto = True
    
    With grdBeneficios
        For i = 1 To .Rows - 1
            Debug.Print "Indicador: " & .TextMatrix(i, colBENE_INDICADORID) & " / Driver: " & .TextMatrix(i, colBENE_DRIVERID) & " / Valor: " & .TextMatrix(i, colBENE_DRIVERVALOR)
            If ClearNull(.TextMatrix(i, colBENE_DRIVERVALOR)) <> "0" And ClearNull(.TextMatrix(i, colBENE_DRIVERVALOR)) <> "" Then
                Call sp_UpdatePeticionBeneficios(glNumeroPeticion, .TextMatrix(i, colBENE_INDICADORID), .TextMatrix(i, colBENE_DRIVERID), .TextMatrix(i, colBENE_DRIVERVALOR))
            Else
                Call sp_UpdatePeticionBeneficios(glNumeroPeticion, .TextMatrix(i, colBENE_INDICADORID), .TextMatrix(i, colBENE_DRIVERID), Null)
                bCompleto = False
            End If
        Next i
    End With
    
    If bCompleto And bIndicadoresInicializados Then
        ' Inicializa el vector (es una sumarizaci�n por cada indicador)
        For i = 0 To UBound(vIndicadoresSuma)
            vIndicadoresSuma(i) = 0
        Next i
    
        If sp_GetPeticionBeneficios(glNumeroPeticion, Null, CodigoCombo(cboEmp, True)) Then
            Do While Not aplRST.EOF
                If Not IsNull(aplRST.Fields!Valor) Then
                    For i = 0 To UBound(vIndicadores)
                        If aplRST.Fields!indicadorId = vIndicadores(i) Then
                            vIndicadoresSuma(i) = vIndicadoresSuma(i) + (aplRST.Fields!valor_referencia * aplRST.Fields!Porcentaje)
                            Debug.Print vIndicadoresSuma(i)
                        End If
                    Next i
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        For i = 0 To UBound(vIndicadoresSuma)
            Call sp_UpdatePetField2(glNumeroPeticion, vIndicadoresCategoria(i), Null, Null, vIndicadoresSuma(i) * 20)
        Next i
        ' Finalmente, calcula el puntaje para la petici�n
        Call sp_UpdatePetField2(glNumeroPeticion, "PUNTAJE", Null, Null, Null)
    End If
End Sub

Private Sub grdBeneficios_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim seleccion As Long
    Dim sNivel As String, sArea As String
    
    With grdBeneficios
        If bHabilitadoModificarBeneficios Then
            sNivel = getPerfNivel(glUsrPerfilActual)
            sArea = getPerfArea(glUsrPerfilActual)
            If ClearNull(SectorSolicitante) = "" Then SectorSolicitante = CodigoCombo(cboSector, True)
            If .rowSel > 0 Then
                If Button = 2 Then
                    If .MouseRow > 0 Then
                        If .MouseCol = colBENE_DRIVERVALORDESC Then
                            Select Case ClearNull(.TextMatrix(.MouseRow, colBENE_DRIVERRESP))
                                Case "SOLI"     ' Responsable de carga es la parte usuaria
                                    Select Case glUsrPerfilActual
                                        Case "GBPE"
                                            seleccion = MenuMSHGridGet(grdBeneficios, .TextMatrix(.rowSel, colBENE_DRIVERID))
                                            If seleccion > 0 Then
                                                .TextMatrix(.rowSel, colBENE_DRIVERVALOR) = seleccion
                                                .TextMatrix(.rowSel, colBENE_DRIVERVALORDESC) = valoraciones.Item(.TextMatrix(.rowSel, colBENE_DRIVERVALOR))
                                            End If
                                        Case "SOLI", "REFE", "SUPE", "AUTO"
                                            If (sNivel = "DIRE" And sArea = glLOGIN_Direccion) Or _
                                                (sNivel = "GERE" And sArea = glLOGIN_Gerencia) Or _
                                                (sNivel = "SECT" And sArea = glLOGIN_Sector) Then
                                                seleccion = MenuMSHGridGet(grdBeneficios, .TextMatrix(.rowSel, colBENE_DRIVERID))
                                                If seleccion > 0 Then
                                                    .TextMatrix(.rowSel, colBENE_DRIVERVALOR) = seleccion
                                                    .TextMatrix(.rowSel, colBENE_DRIVERVALORDESC) = valoraciones.Item(.TextMatrix(.rowSel, colBENE_DRIVERVALOR))
                                                End If
                                            End If
                                    End Select
                                Case "BPE"
                                    If glUsrPerfilActual = "GBPE" Then
                                        seleccion = MenuMSHGridGet(grdBeneficios, .TextMatrix(.rowSel, colBENE_DRIVERID))
                                        If seleccion > 0 Then
                                            .TextMatrix(.rowSel, colBENE_DRIVERVALOR) = seleccion
                                            .TextMatrix(.rowSel, colBENE_DRIVERVALORDESC) = valoraciones.Item(.TextMatrix(.rowSel, colBENE_DRIVERVALOR))
                                        End If
                                    End If
                            End Select
                        End If
                    End If
                End If
            End If
        End If
    End With
End Sub

Private Sub grdBeneficios_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim sNivel As String, sArea As String
    
    With grdBeneficios
        If bHabilitadoModificarBeneficios Then
            sNivel = getPerfNivel(glUsrPerfilActual)
            sArea = getPerfArea(glUsrPerfilActual)
            If ClearNull(SectorSolicitante) = "" Then SectorSolicitante = CodigoCombo(cboSector, True)
            If .MouseRow > 0 Then
                If .MouseCol = colBENE_DRIVERVALORDESC Then
                    Select Case ClearNull(.TextMatrix(.MouseRow, colBENE_DRIVERRESP))
                        Case "SOLI"
                            Select Case glUsrPerfilActual
                                Case "GBPE"
                                    .MousePointer = flexCustom
                                Case "SOLI", "REFE", "SUPE", "AUTO"
                                    If (sNivel = "DIRE" And sArea = glLOGIN_Direccion) Or _
                                        (sNivel = "GERE" And sArea = glLOGIN_Gerencia) Or _
                                        (sNivel = "SECT" And sArea = glLOGIN_Sector) Then
                                        .MousePointer = flexCustom
                                    End If
                            End Select
                        Case "BPE"
                            .MousePointer = IIf(glUsrPerfilActual = "GBPE", flexCustom, flexDefault)
                        Case Else
                            .MousePointer = flexDefault
                    End Select
                Else
                    .MousePointer = flexDefault
                End If
            End If
        End If
    End With
End Sub

Private Function ValidarValoracion() As Boolean
    Dim bControl As Boolean
    Dim i As Integer
    
    If bDebuggear Then Call doLog("ValidarValoracion()")
    
    bControl = True
    With grdBeneficios
        For i = 1 To .Rows - 1
            If ClearNull(.TextMatrix(i, colBENE_DRIVERRESP)) = "SOLI" Then
                If InStr(1, "SOLI|AUTO|REFE|SUPE|", glUsrPerfilActual, vbTextCompare) > 0 Then
                    If ClearNull(.TextMatrix(i, colBENE_DRIVERVALOR)) = "0" Or ClearNull(.TextMatrix(i, colBENE_DRIVERVALOR)) = "" Then
                        bControl = False
                        Exit For
                    End If
                End If
            ElseIf ClearNull(.TextMatrix(i, colBENE_DRIVERRESP)) = "BPE" Then
                If glUsrPerfilActual = "GBPE" Then
                    If ClearNull(.TextMatrix(i, colBENE_DRIVERVALOR)) = "0" Or ClearNull(.TextMatrix(i, colBENE_DRIVERVALOR)) = "" Then
                        bControl = False
                        Exit For
                    End If
                End If
            End If
        Next i
    End With
    ValidarValoracion = bControl
End Function

Private Function ValidarKPI() As Boolean
    Dim bControl As Boolean
    Dim bRequerido As Boolean
    Dim lValorNum As Integer
    Dim sMensajesValidacion As String
    Dim i As Integer
    
    If bDebuggear Then Call doLog("ValidarKPI()")
    
    bControl = True
    bRequerido = False
    
    With grdBeneficios
        For i = 1 To .Rows - 1
            If ClearNull(.TextMatrix(i, colBENE_REQUERIDO)) = "S" Then
                ' Obtengo el valor num�rico por defecto para el indicador
                ' TODO: esto se podr�a optimizar guardando el valor num�rico, cuando se selecciona en una celda oculta. 04.11.2016
                If sp_GetValoracion(ClearNull(.TextMatrix(i, colBENE_DRIVERID)), ClearNull(.TextMatrix(i, colBENE_DRIVERVALOR))) Then
                    lValorNum = CLng(ClearNull(aplRST.Fields!valorNum))
                End If
                If lValorNum > 0 Then
                    bRequerido = True
                    Exit For
                End If
            End If
        Next i
        
        If bRequerido Then
            With grdKPI
                If .Rows < 2 Then
                    bControl = False
                    sMensajesValidacionKPI = "Falta ingresar al menos un KPI."
                End If
            End With
        End If
    End With
    ValidarKPI = bControl
End Function

Private Function CamposObligatoriosKPI() As Boolean
    Dim bControl As Boolean
    Dim i As Long
    Dim bExiste As Boolean
    
    If bDebuggear Then
        Call doLog("CamposObligatoriosKPI()")
    End If
    
    sMensajesValidacionKPI = ""
    bControl = True
    
    If cboKPIIndicador.ListIndex = -1 Then
        sMensajesValidacionKPITitulo = "Indicador"
        sMensajesValidacionKPI = sMensajesValidacionKPI & "- Indicador: Falta especificar."
        bControl = False
    End If
    
    ' Control de duplicidad: controlo que no cargue > 1 el mismo KPI
    bExiste = False
    With grdKPI
        For i = 1 To .Rows - 1
            If .TextMatrix(i, colKPI_KPIID) = CodigoCombo(cboKPIIndicador, True) Then
                bExiste = True
                Exit For
            End If
        Next i
    End With
    If bExiste Then
        sMensajesValidacionKPITitulo = "Control de duplicidad"
        sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Indicador: el KPI ya se encuentra cargado."
        bControl = False
    End If

    If cboKPIUniMed.ListIndex = -1 Then
        sMensajesValidacionKPITitulo = "Unidad de medida indicador"
        sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Unidad de medida indicador: Falta especificar."
        bControl = False
    End If

    If ClearNull(txtKPIValorEsperado) = "" Then
        sMensajesValidacionKPITitulo = "Valor esperado"
        sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Valor esperado: Falta especificar."
        bControl = False
    Else
        If Not IsNumeric(txtKPIValorEsperado) Then
            sMensajesValidacionKPITitulo = "Valor esperado"
            sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Valor esperado: debe ser un real positivo."
            bControl = False
        Else
            If CDbl(txtKPIValorEsperado) < 0 Then
                sMensajesValidacionKPITitulo = "Valor esperado"
                sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Valor esperado: debe ser positivo."
                bControl = False
            Else
                If CDbl(txtKPIValorEsperado) > 99999999 Then
                    sMensajesValidacionKPITitulo = "Valor esperado"
                    sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Valor esperado: muy grande!"
                    bControl = False
                End If
            End If
        End If
    End If
    
    ' Opcional
    If ClearNull(txtKPIValorPartida) <> "" Then
        If Not IsNumeric(txtKPIValorPartida) Then
            sMensajesValidacionKPITitulo = "Valor de partida"
            sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Valor de partida: debe ser un real positivo."
            bControl = False
        Else
            If CDbl(txtKPIValorPartida) < 0 Then
                sMensajesValidacionKPITitulo = "Valor de partida"
                sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Valor de partida: debe ser positivo."
                bControl = False
            Else
                If CDbl(txtKPIValorPartida) > 99999999 Then
                    sMensajesValidacionKPITitulo = "Valor de partida"
                    sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Valor de partida: muy grande!"
                    bControl = False
                End If
            End If
        End If
    End If

    If ClearNull(txtKPITiempoParaEsperado) = "" Then
        sMensajesValidacionKPITitulo = "Tiempo para valor esperado"
        sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Tiempo para valor esperado: Falta especificar."
        bControl = False
    Else
        If Not IsNumeric(txtKPITiempoParaEsperado) Then
            sMensajesValidacionKPITitulo = "Tiempo para valor esperado"
            sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Tiempo para valor esperado: debe ser un real positivo."
            bControl = False
        Else
            If CDbl(txtKPITiempoParaEsperado) < 0 Then
                sMensajesValidacionKPITitulo = "Tiempo para valor esperado"
                sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Tiempo para valor esperado: debe ser positivo."
                bControl = False
            Else
                If CDbl(txtKPITiempoParaEsperado) > 99999999 Then
                    sMensajesValidacionKPITitulo = "Tiempo para valor esperado"
                    sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Tiempo para valor esperado: muy grande!"
                    bControl = False
                End If
            End If
        End If
    End If

    If cboKPIUniMedTiempo.ListIndex = -1 Then
        sMensajesValidacionKPITitulo = "Unidad de medida p/ Tiempo esperado"
        sMensajesValidacionKPI = sMensajesValidacionKPI & vbCrLf & "- Unidad de medida p/ Tiempo esperado: Falta especificar."
        bControl = False
    End If
    CamposObligatoriosKPI = bControl
End Function

Private Function AgregarKPIItem() As Boolean
    With grdKPI
        Select Case sModoEdicionKPI
            Case "ALTA"
                If CamposObligatoriosKPI Then
                    .AddItem "0" & vbTab & CodigoCombo(cboKPIIndicador, True) & vbTab & TextoCombo(cboKPIIndicador, CodigoCombo(cboKPIIndicador, True), True) & vbTab & "" & vbTab & "" & vbTab & "" & vbTab & CodigoCombo(cboKPIUniMed, True) & vbTab & TextoCombo(cboKPIUniMed, CodigoCombo(cboKPIUniMed, True), True) & vbTab & Format(txtKPIValorEsperado, "###,###,###,##0.00") & vbTab & Format(txtKPIValorPartida, "###,###,###,##0.00") & vbTab & Format(txtKPITiempoParaEsperado, "###,###,###,##0.00") & vbTab & CodigoCombo(cboKPIUniMedTiempo, True) & vbTab & TextoCombo(cboKPIUniMedTiempo, CodigoCombo(cboKPIUniMedTiempo, True), True) & vbTab & txtKPIDescripcion
                    AgregarKPIItem = True
                Else
                    MsgBox sMensajesValidacionKPI, vbExclamation + vbOKOnly, "Errores"
                    AgregarKPIItem = False
                End If
            Case "MODI"
                .TextMatrix(.rowSel, colKPI_PETNROINTERNO) = glNumeroPeticion
                .TextMatrix(.rowSel, colKPI_KPIID) = CodigoCombo(cboKPIIndicador, True)
                .TextMatrix(.rowSel, colKPI_KPINOM) = TextoCombo(cboKPIIndicador, CodigoCombo(cboKPIIndicador, True), True)
                .TextMatrix(.rowSel, colKPI_REQ) = ""
                .TextMatrix(.rowSel, colKPI_HAB) = ""
                .TextMatrix(.rowSel, colKPI_AYUDA) = ""
                .TextMatrix(.rowSel, colKPI_UMID) = CodigoCombo(cboKPIUniMed, True)
                .TextMatrix(.rowSel, colKPI_UMDSC) = TextoCombo(cboKPIUniMed, CodigoCombo(cboKPIUniMed, True), True)
                .TextMatrix(.rowSel, colKPI_VALORESP) = Format(txtKPIValorEsperado, "###,###,###,##0.00")
                .TextMatrix(.rowSel, colKPI_VALORPAR) = Format(txtKPIValorPartida, "###,###,###,##0.00")
                .TextMatrix(.rowSel, colKPI_TIEMPO) = Format(txtKPITiempoParaEsperado, "###,###,###,##0.00")
                .TextMatrix(.rowSel, colKPI_TIEMPOUM) = CodigoCombo(cboKPIUniMedTiempo, True)
                .TextMatrix(.rowSel, colKPI_TIEMPOUMDESC) = TextoCombo(cboKPIUniMedTiempo, CodigoCombo(cboKPIUniMedTiempo, True), True)
                .TextMatrix(.rowSel, colKPI_DESCRIPCION) = ClearNull(txtKPIDescripcion)
                AgregarKPIItem = True
            Case "DELE"
                Call sp_DeletePeticionKPI(glNumeroPeticion, .TextMatrix(.rowSel, colKPI_KPIID))
                AgregarKPIItem = True
                Call CargarGrillaKPI
        End Select
    End With
End Function

Private Sub grdKPI_RowColChange()
    Call MostrarSeleccionKPI
End Sub

Private Sub txtKPIValorEsperado_Validate(Cancel As Boolean)
    If InStr(1, "ALTA|MODI|", sModoEdicionKPI, vbTextCompare) > 0 Then
        If Len(txtKPIValorEsperado) > 0 Then
            Call ValidarNumero(txtKPIValorEsperado)
        End If
    End If
End Sub

Private Sub txtKPIValorPartida_Validate(Cancel As Boolean)
    If InStr(1, "ALTA|MODI|", sModoEdicionKPI, vbTextCompare) > 0 Then
        If Len(txtKPIValorPartida) > 0 Then
            Call ValidarNumero(txtKPIValorPartida)
        End If
    End If
End Sub

Private Sub txtKPITiempoParaEsperado_Validate(Cancel As Boolean)
    If InStr(1, "ALTA|MODI|", sModoEdicionKPI, vbTextCompare) > 0 Then
        If Len(txtKPITiempoParaEsperado) > 0 Then
            Call ValidarNumero(txtKPITiempoParaEsperado)
        End If
    End If
End Sub

Private Sub cboKPIIndicador_Click()
    Dim i As Long, J As Long
    Dim bExiste As Boolean
    
    If flgEnCarga Then Exit Sub
    If InStr(1, "ALTA|MODI|", sModoEdicionKPI, vbTextCompare) > 0 Then
        If cboKPIIndicador.ListIndex = -1 Then Exit Sub
        bExiste = False
        With grdKPI
            For J = 1 To .Rows - 1
                If .TextMatrix(J, colKPI_KPIID) = CodigoCombo(cboKPIIndicador, True) Then
                    bExiste = True
                    Exit For
                End If
            Next J
        End With
        If Not bExiste Then
            Call ActualizarUnidadMedida
        Else
            MsgBox "El KPI seleccionado ya existe. No puede cargarse m�s de una vez.", vbInformation + vbOKOnly
        End If
    End If
End Sub

Private Sub ActualizarUnidadMedida()
    If bDebuggear Then Call doLog("ActualizarUnidadMedida()")
    If InStr(1, "ALTA|MODI|", sModoEdicionKPI, vbTextCompare) > 0 Then
        With cboKPIUniMed
            .Clear
            If sp_GetKPIUniMed(CodigoCombo(cboKPIIndicador, True)) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!unimedDesc) & ESPACIOS & "||" & ClearNull(aplRST.Fields!unimedId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = 0
                If .ListCount < 2 Then
                    Call setHabilCtrl(cboKPIUniMed, "DIS")
                Else
                    Call setHabilCtrl(cboKPIUniMed, "NOR")
                End If
            End If
        End With
    End If
End Sub

Private Sub tbAdjPet_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnVer"
            If lvwAdjuntos.SelectedItem.Selected Then
                Call VisualizarAdjunto
            Else
                MsgBox "Debe seleccionar un documento", vbExclamation + vbOKOnly
            End If
        Case "btnAdjuntar"
            If sClasePet = "SINC" Then
                MsgBox "La petici�n debe estar clasificada.", vbExclamation + vbOKOnly, "Clasificaci�n requerida"
                Exit Sub
            Else
                Call SeleccionarAdjuntos
            End If
        Case "btnActualizar"                ' Actualizar la grilla
            Call CargarAdjPet
        Case "btnEditar"
            Call adjHabBtn(1, "M")
        Case "btnQuitar"
            Call adjHabBtn(1, "E")
        Case "btnConfirmar"
            Call AdjuntoConfirmar
        Case "btnCancelar"
            Call adjCan_Click
    End Select
End Sub

Private Sub tbVinculados_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnVer"
            If lvwVinculados.SelectedItem.Selected Then
                Call VerVinculado
            Else
                MsgBox "Debe seleccionar un documento", vbExclamation + vbOKOnly
            End If
        Case "btnVincular"
            Call SeleccionarVincWindows
        Case "btnActualizar"                ' Actualizar la grilla
            Call CargarVinculados
        Case "btnEditar"
            Call docHabBtn(1, "M")
        Case "btnQuitar"
            Call docHabBtn(1, "E")
        Case "btnConfirmar"
            Call docCon_Click
        Case "btnCancelar"
            docFile = "": docPath = "": docComent = ""
            Call docHabBtn(0, "X")
    End Select
End Sub

Private Sub VerVinculado()
    If ClearNull(glPathDocMetod) = "" Then
        Exit Sub
    End If
    Call Puntero(True)
    Call AbrirAdjunto(glPathDocMetod)
    glPathDocMetod = ""
    Call Puntero(False)
End Sub

Private Sub lvwVinculados_ItemClick(ByVal Item As MSComctlLib.ListItem)
    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
        If Item.Selected Then
            tbVinculados.Buttons(BOTONERA_VER).Enabled = True
            If flgEjecutor Then
                tbVinculados.Buttons(BOTONERA_ADJUNTAR).Enabled = True
                If glLOGIN_ID_REEMPLAZO = lvwVinculados.SelectedItem.SubItems(5) Then
                    tbVinculados.Buttons(BOTONERA_EDITAR).Enabled = True
                    tbVinculados.Buttons(BOTONERA_QUITAR).Enabled = True
                Else
                    tbVinculados.Buttons(BOTONERA_EDITAR).Enabled = False
                    tbVinculados.Buttons(BOTONERA_QUITAR).Enabled = False
                End If
            End If
            glPathDocMetod = Item.SubItems(2) & Item.text
            docFile = Item.text
            docPath = Item.SubItems(2)
            docComent = Item.SubItems(6)
        End If
    Else
        lblMensajeNoVinculados.visible = True
    End If
End Sub

Private Sub CargarConformes()
    Const IMAGE_MSEXCEL = 1
    Const IMAGE_ADOBEPDF = 2
    Const IMAGE_MSWORD = 3
    Const IMAGE_EMAIL = 2
    Const IMAGE_OTHER = 33
    Const IMAGE_TEST = 26
    Const IMAGE_HOMOLOGACION_SOB = 19
    Const IMAGE_HOMOLOGACION_OME = 20
    Const IMAGE_HOMOLOGACION_OMA = 21
    Const IMAGE_VALIDACION_TECNICA = 36
    'Const IMAGE_OK_SUPERVISOR = 26
    Const IMAGE_OK_DATOS = 32
    
    Dim subElemento As MSComctlLib.ListItem
    Dim imagen As Integer
    
    Call setHabilCtrl(cboPcfTipo, "DIS")
    Call setHabilCtrl(cboPcfModo, "DIS")
    Call setHabilCtrl(cboPcfClase, "DIS")
    Call setHabilCtrl(pcfTexto, "DIS")

    With lvwConformes
        .Enabled = False
        .ListItems.Clear
        .BorderStyle = ccNone
        .GridLines = False
        .HotTracking = False
        .HoverSelection = False
        .LabelWrap = True
        .View = lvwReport
        .Font.name = "Tahoma": .Font.Size = 8
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , "oktiponom", "Conforme", 2800, lvwColumnLeft
        .ColumnHeaders.Add , "oktipoclase", "Tipo", 1000, lvwColumnLeft
        .ColumnHeaders.Add , "oktipo", "Tipo", 0, lvwColumnLeft
        .ColumnHeaders.Add , "oktipocbo", "Tipo", 0, lvwColumnLeft
        .ColumnHeaders.Add , "peticion", "peticion", 0, lvwColumnLeft
        .ColumnHeaders.Add , "modo", "Modo", 0, lvwColumnLeft
        .ColumnHeaders.Add , "modonom", "Modo", 1000, lvwColumnLeft
        .ColumnHeaders.Add , "propietario", "Otorgado por", 3000, lvwColumnLeft
        .ColumnHeaders.Add , "fecha", "Fecha", 1800, lvwColumnLeft
        .ColumnHeaders.Add , "secuencia", "Secuencia", 1000, lvwColumnCenter
        .ColumnHeaders.Add , "legajo", "legajo", 0, lvwColumnLeft
        .ColumnHeaders.Add , "texto", "texto", 0, lvwColumnLeft
    
        If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
            Do While Not aplRST.EOF
                Select Case Trim(Left(aplRST.Fields!ok_tipo, 4))
                    Case "HOMA"
                        imagen = IMAGE_HOMOLOGACION_OMA
                    Case "HOME"
                        imagen = IMAGE_HOMOLOGACION_OME
                    Case "HSOB"
                        imagen = IMAGE_HOMOLOGACION_SOB
                    Case "TEST", "TESP", "OKPF", "OKPP"
                        imagen = IMAGE_TEST
                    Case "AROK", "AROB", "ARRE", "SIOK", "SIOB", "SIRE"
                        imagen = IMAGE_VALIDACION_TECNICA
                    Case "DATF", "DATP"
                        imagen = IMAGE_OK_DATOS
                    Case Else
                        imagen = IMAGE_OTHER
                End Select
                Set subElemento = .ListItems.Add(, , ClearNull(aplRST.Fields!ok_tiponom), , imagen)
                subElemento.SubItems(1) = ClearNull(aplRST.Fields!ok_tipoclase)
                subElemento.SubItems(2) = ClearNull(aplRST.Fields!ok_tipo)
                subElemento.SubItems(3) = ClearNull(aplRST.Fields!ok_tipoCombo)
                subElemento.SubItems(4) = ClearNull(aplRST.Fields!pet_nrointerno)
                subElemento.SubItems(5) = ClearNull(aplRST.Fields!ok_modo)
                subElemento.SubItems(6) = ClearNull(aplRST.Fields!ok_modonom)
                If subElemento.SubItems(1) = "Final" Then subElemento.Bold = True
                subElemento.SubItems(7) = ClearNull(aplRST.Fields!nom_audit)
                subElemento.SubItems(8) = ClearNull(aplRST.Fields!audit_date)
                subElemento.SubItems(9) = ClearNull(aplRST.Fields!ok_secuencia)
                subElemento.SubItems(10) = ClearNull(aplRST.Fields!audit_user)
                subElemento.SubItems(11) = ClearNull(aplRST.Fields!ok_texto)
                If Left(subElemento.SubItems(2), 4) = "HOMA" Then
                    bExisteHOMA = True
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .Enabled = True
    End With
    
    lblMensajeNoConformes.visible = IIf(InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) > 0, True, False)
    Call InicializarConformes("1")
    If lvwConformes.ListItems.Count > 0 Then
        Call lvwConformes_ItemClick(lvwConformes.ListItems(1))
    Else
        pcfSeleccion
    End If
End Sub

Private Sub InicializarConformes(modo)
    With cboPcfClase
        .Clear
        .AddItem "Parcial" & ESPACIOS & "||P"
        .AddItem "Final" & ESPACIOS & "||F"
        .ListIndex = -1
    End With
    
    With cboPcfModo
        .Clear
        .AddItem "Requerido" & ESPACIOS & "||REQ"
        .AddItem "Exenci�n" & ESPACIOS & "||EXC"
        .AddItem "Alternativo" & ESPACIOS & "||ALT"
        .AddItem "N/A" & ESPACIOS & "||N/A"
        .ListIndex = -1
    End With
    
    Select Case modo
        Case "1"            ' TODO
            With cboPcfTipo
                .Clear
                .AddItem "OK Documento alcance" & ESPACIOS & "||ALC"
                .AddItem "OK Pruebas usuario" & ESPACIOS & "||TES"
                .AddItem "OK Datos de usuario" & ESPACIOS & "||DAT"
                .AddItem "OK Supervisor" & ESPACIOS & "||OKP"
                .AddItem "OK Documento ADA" & ESPACIOS & "||ADA"  'GMT03
                .AddItem "SOB" & ESPACIOS & "||SOB"
                .AddItem "OME" & ESPACIOS & "||OME"
                .AddItem "OMA" & ESPACIOS & "||OMA"
                .AddItem "Conforme" & ESPACIOS & "||SIOK"
                .AddItem "Conforme c/ observaciones" & ESPACIOS & "||SIOB"
                .AddItem "No conforme" & ESPACIOS & "||SIRE"
                .AddItem "Conforme" & ESPACIOS & "||AROK"
                .AddItem "Conforme c/ observaciones" & ESPACIOS & "||AROB"
                .AddItem "No conforme" & ESPACIOS & "||ARRE"
                .ListIndex = -1
            End With
        Case "2"            ' Para el alta o edici�n
            With cboPcfTipo
                .Clear
                If glEsHomologador Then
                    .AddItem "SOB" & ESPACIOS & "||SOB"
                    .AddItem "OME" & ESPACIOS & "||OME"
                    .AddItem "OMA" & ESPACIOS & "||OMA"
                ElseIf glEsSeguridadInformatica Then
                    .AddItem "Conforme" & ESPACIOS & "||SIOK"
                    .AddItem "Conforme c/ observaciones" & ESPACIOS & "||SIOB"
                    .AddItem "No conforme" & ESPACIOS & "||SIRE"
                ElseIf glEsTecnologia Then
                    .AddItem "Conforme de ARQ" & ESPACIOS & "||AROK"
                    .AddItem "Conforme c/ observaciones" & ESPACIOS & "||AROB"
                    .AddItem "No conforme" & ESPACIOS & "||ARRE"
                    .AddItem "OK Documento alcance" & ESPACIOS & "||ALC"  'GMT06
                    .AddItem "OK Documento ADA" & ESPACIOS & "||ADA"  'GMT06
                Else
                    .AddItem "OK Documento alcance" & ESPACIOS & "||ALC"
                    .AddItem "OK Pruebas usuario" & ESPACIOS & "||TES"
                    .AddItem "OK Datos de usuario" & ESPACIOS & "||DAT"
                    .AddItem "OK Supervisor" & ESPACIOS & "||OKP"
                    .AddItem "OK Documento ADA" & ESPACIOS & "||ADA"  'GMT03
                End If
                .ListIndex = -1
            End With
    End Select
    If lvwConformes.ListItems.Count > 0 Then
        If lvwConformes.SelectedItem.Selected Then
            cboPcfTipo.ListIndex = PosicionCombo(cboPcfTipo, lvwConformes.SelectedItem.SubItems(3), True)
            cboPcfClase.ListIndex = PosicionCombo(cboPcfClase, Left(lvwConformes.SelectedItem.SubItems(1), 1), True)
            cboPcfModo.ListIndex = PosicionCombo(cboPcfModo, lvwConformes.SelectedItem.SubItems(5), True)
        End If
    End If
End Sub

Private Sub tbConformes_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnActualizar"                        ' Actualizar la grilla
            Call CargarConformes
        Case "btnConforme"                          ' Nuevo conforme
            Call pcfHabBtn(1, "A")
        Case "btnEditar"                            ' Editar conforme seleccionado
            Call pcfHabBtn(1, "M")
        Case "btnQuitar"                            ' Quitar el conforme seleccionado
            Call pcfHabBtn(1, "E")
        Case "btnConfirmar"                         ' Confirmar
            Call ConfirmarConformes
        Case "btnCancelar"                          ' Cancelar
            Call pcfHabBtn(0, "X", "")
        Case "btnDeshabilitar"                      ' Deshabilitar manualmente la validez de pasaje a producci�n de una petici�n
            Call HomologacionDeshabilitarManual
    End Select
End Sub

Private Sub lvwConformes_ItemClick(ByVal Item As MSComctlLib.ListItem)
    ' Datos
    If lvwConformes.ListItems.Count > 0 Then
        cboPcfTipo.ListIndex = PosicionCombo(cboPcfTipo, Item.ListSubItems(3).text, True)
        cboPcfClase.ListIndex = PosicionCombo(cboPcfClase, Left(Item.ListSubItems(1).text, 1), True)
        cboPcfModo.ListIndex = PosicionCombo(cboPcfModo, Item.ListSubItems(5).text, True)
        pcfTexto.text = Item.ListSubItems(11).text
        Call pcfSeleccion
    End If
End Sub

Private Sub tbHomologacion_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnConfirmar": Call HomologacionConfirmar
        Case "btnActualizar": Call CargarInformesHomologacion
        Case "btnAgregar": Call HomologacionNuevoInforme
        Case "btnEditar": Call HomologacionEditarInforme
        Case "btnQuitar": Call HomologacionQuitarInforme
        Case "btnInforme": Call HomologacionGenerarInformePDF
        Case "btnGuardar": Call HomologacionGuardarInforme
        Case "btnCancelar": Call HomologacionCancelar
    End Select
End Sub

Private Sub HomologacionGenerarInformePDF()
    ' Generar PDF
    If cboInforme.ListIndex = -1 Then
        MsgBox "No hay ning�n informe seleccionado.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    
    Call Puntero(True)
    sReportName = "\petinfoh16.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crPortrait
        .ReportTitle = "Informe de homologaci�n"
        .ReportComments = "Informe de homologaci�n"        ' "Peticiones en estado terminal desde " & Format(fe_desde, "dd/mm/yyyy") & " y " & Format(fe_hasta, "dd/mm/yyyy")
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    Set crParamDefs = crReport.ParameterFields              ' Parametros del reporte
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@pet_nrointerno": crParamDef.AddCurrentValue (Val(glNumeroPeticion))
            Case "@info_id": crParamDef.AddCurrentValue (Val(CodigoCombo(cboInforme, True)))
            Case "@info_idver": crParamDef.AddCurrentValue (Val(0))
            Case "@info_req": crParamDef.AddCurrentValue ("S")
        End Select
    Next
    ' Subreporte de observaciones
    Set crParamDefs = crReport.OpenSubreport("HomoObservaciones").ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@pet_nrointerno": crParamDef.AddCurrentValue (Val(glNumeroPeticion))
            Case "@info_id": crParamDef.AddCurrentValue (Val(CodigoCombo(cboInforme, True)))
            Case "@info_idver": crParamDef.AddCurrentValue (Val(0))
        End Select
    Next
    
    ' Subreporte de responsables
    Set crParamDefs = crReport.OpenSubreport("HomoResponsables").ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@pet_nrointerno": crParamDef.AddCurrentValue (Val(glNumeroPeticion))
            Case "@info_id": crParamDef.AddCurrentValue (Val(CodigoCombo(cboInforme, True)))
            Case "@info_idver": crParamDef.AddCurrentValue (Val(0))
        End Select
    Next
 
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub tbControl_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnActualizar": Call CargarEstado
    End Select
End Sub

Private Sub tbHomologacionResponsables_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnQuitar": Call HomologacionItemRespoEliminar
        Case "btnEditar": Call HomologacionItemRespoEditar
        Case "btnActualizar"
        Case "btnConfirmar": Call HomologacionItemRespoGuardar
        Case "btnCancelar": Call HomologacionItemRespoCancelar
    End Select
End Sub

Private Sub HomologacionItemRespoEliminar()
    If cboInforme.ListIndex > -1 Then
        With grdHomoResponsables
            If .rowSel > 0 Then
                lblHomoRespoModo = "Quitar responsables"
                cModoResponsables = MODO_DELETE
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_EDITAR).Enabled = False
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_QUITAR).Enabled = False
                tbHomologacion.Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_GUARDAR).Enabled = True
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_CANCELAR).Enabled = True
                ' Otros
                Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)            ' add -109- c.
                ' Pesta�as
                orjBtn(orjDAT).Enabled = False
                orjBtn(orjMEM).Enabled = False
                orjBtn(orjANX).Enabled = False
                orjBtn(orjADJ).Enabled = False
                If Hab_PesVinculado Then 'GMT05
                    orjBtn(orjDOC).Enabled = False
                End If 'GMT05
                orjBtn(orjCNF).Enabled = False
                orjBtn(orjEST).Enabled = False
            Else
                MsgBox "No hay responsables seleccionados.", vbExclamation + vbOKOnly
            End If
        End With
    Else
        MsgBox "No hay ning�n informe seleccionado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub HomologacionItemRespoEditar()
    If cboInforme.ListIndex > -1 Then
        With grdHomoResponsables
            If .rowSel > 0 Then
                lblHomoRespoModo = "Modificando datos de responsable"
                cModoResponsables = MODO_EDIT
                lblCantidadRestante.visible = True
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_EDITAR).Enabled = False
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_QUITAR).Enabled = False
                tbHomologacion.Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = False
                txtItemRespuesta = ClearNull(.TextMatrix(.rowSel, colHOMRES_COMENTARIOS))
                Call setHabilCtrl(cboItemResponsable, NORMAL)
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_GUARDAR).Enabled = True
                tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_CANCELAR).Enabled = True
                Call setHabilCtrl(txtItemRespuesta, NORMAL)
                Call setHabilCtrl(chkIncluirItemsOpcionales, DISABLE)
                ' Pesta�as
                orjBtn(orjDAT).Enabled = False
                orjBtn(orjMEM).Enabled = False
                orjBtn(orjANX).Enabled = False
                orjBtn(orjADJ).Enabled = False
                If Hab_PesVinculado Then 'GMT05
                    orjBtn(orjDOC).Enabled = False
                End If 'GMT05
                orjBtn(orjCNF).Enabled = False
                orjBtn(orjEST).Enabled = False
            Else
                MsgBox "No hay items observados para este informe.", vbExclamation + vbOKOnly
            End If
        End With
    Else
        MsgBox "No hay ning�n informe seleccionado.", vbExclamation + vbOKOnly
    End If
End Sub

Private Sub HomologacionItemRespoGuardar()
    Call GuardarDatosDeItemResponsable
End Sub

Private Sub HomologacionItemRespoCancelar()
    lblCantidadRestante.visible = False
    cModoResponsables = MODO_VIEW
    lblHomoRespoModo = ""
    tbHomologacion.Buttons(BOTONERAHOMO_CONFIRMAR).Enabled = True
    Call setHabilCtrl(cboItemResponsable, DISABLE)
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_EDITAR).Enabled = True
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_QUITAR).Enabled = True
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_GUARDAR).Enabled = False
    tbHomologacionResponsables.Buttons(BOTONERAHOMOITEM_CANCELAR).Enabled = False
    Call setHabilCtrl(txtItemRespuesta, DISABLE)
    ' Otros
    Call setHabilCtrl(chkIncluirItemsOpcionales, NORMAL)
    ' Pesta�as
    orjBtn(orjDAT).Enabled = True
    orjBtn(orjMEM).Enabled = True
    orjBtn(orjANX).Enabled = True
    orjBtn(orjADJ).Enabled = True
    If Hab_PesVinculado Then 'GMT05
        orjBtn(orjDOC).Enabled = True
    End If 'GMT05
    orjBtn(orjCNF).Enabled = True
    orjBtn(orjEST).Enabled = True
End Sub

Private Sub tbKKPIs_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnActualizar": Call CargarGrillaKPI
        Case "btnAgregar": Call KPINuevo
        Case "btnEditar": Call KPIEditar
        Case "btnQuitar": Call KPIQuitar
        Case "btnConfirmar": Call KPIGuardar
        Case "btnCancelar": Call KPICancelar
    End Select
End Sub

Private Sub KPINuevo()
    bHuboCambiosKPI = True
    sModoEdicionKPI = "ALTA"
    lblKPIModo = "Agregar"
    lblKPIModo.visible = True
    Call HabilitarBotonesKPI(True, sModoEdicionKPI)
End Sub

Private Sub KPIEditar()
    bHuboCambiosKPI = True
    With grdKPI
        If .Rows > 1 Then
            sModoEdicionKPI = "MODI"
            lblKPIModo = "Edici�n"
            lblKPIModo.visible = True
            Call HabilitarBotonesKPI(True, sModoEdicionKPI)
        Else
            MsgBox "Debe seleccionar un item de la grilla.", vbInformation + vbOKOnly
        End If
    End With
End Sub

Private Sub KPIQuitar()
    bHuboCambiosKPI = True
    With grdKPI
        If .Rows > 1 Then
            sModoEdicionKPI = "DELE"
            lblKPIModo = "Eliminar"
            lblKPIModo.visible = True
            Call HabilitarBotonesKPI(True, sModoEdicionKPI)
        Else
            MsgBox "Debe seleccionar un item de la grilla.", vbInformation + vbOKOnly
        End If
    End With
End Sub

Private Sub KPIGuardar()
    If AgregarKPIItem() Then
        sModoEdicionKPI = "VIEW"
        lblKPIModo = "": lblKPIModo.visible = False
        Call HabilitarBotonesKPI(False, sModoEdicionKPI)
    End If
End Sub

Private Sub KPICancelar()
    sModoEdicionKPI = "VIEW"
    lblKPIModo = "": lblKPIModo.visible = False
    Call HabilitarBotonesKPI(False, sModoEdicionKPI)
End Sub

Private Sub HabilitarBotonesKPI(bOpcion As Boolean, Optional sModoKPI As Variant)
    If IsMissing(sModoKPI) Then
        If bOpcion Then
            tbKKPIs.Buttons(BOTONERA_KPI_ACTUALIZAR).Enabled = True
            tbKKPIs.Buttons(BOTONERA_KPI_AGREGAR).Enabled = True
            tbKKPIs.Buttons(BOTONERA_KPI_EDITAR).Enabled = True
            tbKKPIs.Buttons(BOTONERA_KPI_QUITAR).Enabled = True
        Else
            tbKKPIs.Buttons(BOTONERA_KPI_ACTUALIZAR).Enabled = False
            tbKKPIs.Buttons(BOTONERA_KPI_AGREGAR).Enabled = False
            tbKKPIs.Buttons(BOTONERA_KPI_EDITAR).Enabled = False
            tbKKPIs.Buttons(BOTONERA_KPI_QUITAR).Enabled = False
            tbKKPIs.Buttons(BOTONERA_KPI_CONFIRMAR).Enabled = False
            tbKKPIs.Buttons(BOTONERA_KPI_CANCELAR).Enabled = False
        End If
        sModoEdicionKPI = "VIEW"
        Call setHabilCtrl(udKPIValorEsperado, DISABLE)
        Call setHabilCtrl(udKPIValorPartida, DISABLE)
        Call setHabilCtrl(udKPITiempoParaEsperado, DISABLE)
    Else
        Select Case sModoKPI
            Case "ALTA"
                grdKPI.Enabled = False
                cboKPIIndicador.ListIndex = -1
                txtKPIDescripcion = ""
                cboKPIUniMed.ListIndex = -1
                txtKPIValorEsperado = ""
                txtKPIValorPartida = ""
                txtKPITiempoParaEsperado = ""
                cboKPIUniMedTiempo.ListIndex = -1
                Call setHabilCtrl(cboKPIIndicador, "NOR")
                Call setHabilCtrl(txtKPIDescripcion, "NOR")
                Call setHabilCtrl(cboKPIUniMed, "NOR")
                Call setHabilCtrl(txtKPIValorEsperado, "NOR")
                Call setHabilCtrl(txtKPIValorPartida, "NOR")
                Call setHabilCtrl(txtKPITiempoParaEsperado, "NOR")
                Call setHabilCtrl(cboKPIUniMedTiempo, "NOR")
                Call setHabilCtrl(udKPIValorEsperado, NORMAL)
                Call setHabilCtrl(udKPIValorPartida, NORMAL)
                Call setHabilCtrl(udKPITiempoParaEsperado, NORMAL)
                ' Botones de comando
                tbKKPIs.Buttons(BOTONERA_KPI_ACTUALIZAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_AGREGAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_EDITAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_QUITAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_CONFIRMAR).Enabled = True
                tbKKPIs.Buttons(BOTONERA_KPI_CANCELAR).Enabled = True
                
                If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
                    Call setHabilCtrl(cmdOk, "DIS")
                    Call setHabilCtrl(cmdCancel, "DIS")
                End If
                cboKPIIndicador.SetFocus
            Case "MODI"
                grdKPI.Enabled = False
                Call ActualizarUnidadMedida
                With grdKPI
                    cboKPIIndicador.ListIndex = PosicionCombo(cboKPIIndicador, .TextMatrix(.rowSel, colKPI_KPIID), True)
                    txtKPIDescripcion = ClearNull(.TextMatrix(.rowSel, colKPI_DESCRIPCION))
                    cboKPIUniMed.ListIndex = PosicionCombo(cboKPIUniMed, .TextMatrix(.rowSel, colKPI_UMID), True)
                    txtKPIValorEsperado = Format(.TextMatrix(.rowSel, colKPI_VALORESP), "#######0.00")
                    txtKPIValorPartida = Format(.TextMatrix(.rowSel, colKPI_VALORPAR), "#######0.00")
                    txtKPITiempoParaEsperado = Format(.TextMatrix(.rowSel, colKPI_TIEMPO), "#######0.00")
                    cboKPIUniMedTiempo.ListIndex = PosicionCombo(cboKPIUniMedTiempo, .TextMatrix(.rowSel, colKPI_TIEMPOUM), True)
                End With
                Call setHabilCtrl(txtKPIDescripcion, "NOR")
                Call setHabilCtrl(txtKPIValorEsperado, "NOR")
                Call setHabilCtrl(txtKPIValorPartida, "NOR")
                Call setHabilCtrl(txtKPITiempoParaEsperado, "NOR")
                Call setHabilCtrl(cboKPIUniMedTiempo, "NOR")
                Call setHabilCtrl(udKPIValorEsperado, NORMAL)
                Call setHabilCtrl(udKPIValorPartida, NORMAL)
                Call setHabilCtrl(udKPITiempoParaEsperado, NORMAL)
                
                ' Botones de comando
                tbKKPIs.Buttons(BOTONERA_KPI_ACTUALIZAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_AGREGAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_EDITAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_QUITAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_CONFIRMAR).Enabled = True
                tbKKPIs.Buttons(BOTONERA_KPI_CANCELAR).Enabled = True
                
                Call setHabilCtrl(cmdOk, "DIS")
                Call setHabilCtrl(cmdCancel, "DIS")
            Case "DELE"
                grdKPI.Enabled = False
                ' Botones de comando
                tbKKPIs.Buttons(BOTONERA_KPI_ACTUALIZAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_AGREGAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_EDITAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_QUITAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_CONFIRMAR).Enabled = True
                tbKKPIs.Buttons(BOTONERA_KPI_CANCELAR).Enabled = True
                
                If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
                    Call setHabilCtrl(cmdOk, "DIS")
                    Call setHabilCtrl(cmdCancel, "DIS")
                End If
            Case Else
                grdKPI.Enabled = True
                With cboKPIUniMed
                    .Clear
                    If sp_GetUnidadMedida(Null) Then
                        Do While Not aplRST.EOF
                            .AddItem ClearNull(aplRST.Fields!unimedDesc) & ESPACIOS & "||" & ClearNull(aplRST.Fields!unimedId)
                            aplRST.MoveNext
                            DoEvents
                        Loop
                        If ClearNull(.Tag) = "" Then
                            .ListIndex = -1
                        Else
                            .ListIndex = PosicionCombo(cboKPIUniMed, .Tag, True)
                        End If
                    End If
                End With
                Call setHabilCtrl(udKPIValorEsperado, DISABLE)
                Call setHabilCtrl(udKPIValorPartida, DISABLE)
                Call setHabilCtrl(udKPITiempoParaEsperado, DISABLE)
                Call setHabilCtrl(cboKPIIndicador, "DIS")
                Call setHabilCtrl(txtKPIDescripcion, "DIS")
                Call setHabilCtrl(cboKPIUniMed, "DIS")
                Call setHabilCtrl(txtKPIValorEsperado, "DIS")
                Call setHabilCtrl(txtKPIValorPartida, "DIS")
                Call setHabilCtrl(txtKPITiempoParaEsperado, "DIS")
                Call setHabilCtrl(cboKPIUniMedTiempo, "DIS")
                ' Botones de comando
                If bHabilitadoModificarKPI Then
                    tbKKPIs.Buttons(BOTONERA_KPI_ACTUALIZAR).Enabled = True
                    tbKKPIs.Buttons(BOTONERA_KPI_AGREGAR).Enabled = True
                    tbKKPIs.Buttons(BOTONERA_KPI_EDITAR).Enabled = True
                    tbKKPIs.Buttons(BOTONERA_KPI_QUITAR).Enabled = True
                Else
                    tbKKPIs.Buttons(BOTONERA_KPI_ACTUALIZAR).Enabled = False
                    tbKKPIs.Buttons(BOTONERA_KPI_AGREGAR).Enabled = False
                    tbKKPIs.Buttons(BOTONERA_KPI_EDITAR).Enabled = False
                    tbKKPIs.Buttons(BOTONERA_KPI_QUITAR).Enabled = False
                End If
                tbKKPIs.Buttons(BOTONERA_KPI_CONFIRMAR).Enabled = False
                tbKKPIs.Buttons(BOTONERA_KPI_CANCELAR).Enabled = False
                If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
                    Call setHabilCtrl(cmdOk, "NOR")
                    Call setHabilCtrl(cmdCancel, "NOR")
                End If
        End Select
    End If
End Sub

Private Sub MostrarSeleccionKPI()
    With grdKPI
        If .rowSel > 0 Then
            cboKPIIndicador.ListIndex = PosicionCombo(cboKPIIndicador, .TextMatrix(.rowSel, colKPI_KPIID), True)
            txtKPIDescripcion = ClearNull(.TextMatrix(.rowSel, colKPI_DESCRIPCION))
            cboKPIUniMed.Tag = .TextMatrix(.rowSel, colKPI_UMID)     ' new
            cboKPIUniMed.ListIndex = PosicionCombo(cboKPIUniMed, .TextMatrix(.rowSel, colKPI_UMID), True)
            ' TODO: por qu� no se formatea correctamente con los separadores de miles?
            txtKPIValorEsperado = Format(.TextMatrix(.rowSel, colKPI_VALORESP), "#######0.00")
            txtKPIValorPartida = Format(.TextMatrix(.rowSel, colKPI_VALORPAR), "#######0.00")
            txtKPITiempoParaEsperado = Format(.TextMatrix(.rowSel, colKPI_TIEMPO), "#######0.00")
            cboKPIUniMedTiempo.ListIndex = PosicionCombo(cboKPIUniMedTiempo, .TextMatrix(.rowSel, colKPI_TIEMPOUM), True)
        Else
            txtKPIDescripcion = ""
            cboKPIUniMed.ListIndex = -1
            txtKPIValorEsperado = ""
            txtKPIValorPartida = ""
            txtKPITiempoParaEsperado = ""
            cboKPIUniMedTiempo.ListIndex = -1
        End If
    End With
End Sub

Private Sub udKPITiempoParaEsperado_Change()
    Call txtKPITiempoParaEsperado_Validate(False)
End Sub

Private Sub udKPIValorEsperado_Change()
    Call txtKPIValorEsperado_Validate(False)
End Sub

Private Sub udKPIValorPartida_Change()
    Call txtKPIValorPartida_Validate(False)
End Sub

Private Sub cboOrientacion_Click()
    If cboOrientacion.ListIndex > -1 Then
        m_cTT.ToolText(cboOrientacion) = vOrientacionDescripcion(cboOrientacion.ListIndex)
    End If
End Sub

Private Sub grdArquitectura_KeyUp(KeyCode As Integer, Shift As Integer)
    Call GrillasValidacionCompletar(grdArquitectura, KeyCode, Shift)
End Sub

Private Sub grdSeguridadInformatica_KeyUp(KeyCode As Integer, Shift As Integer)
    Call GrillasValidacionCompletar(grdSeguridadInformatica, KeyCode, Shift)
End Sub
'GMT01 - INI
Private Sub grdData_KeyUp(KeyCode As Integer, Shift As Integer)
    Call GrillasValidacionCompletar(grdData, KeyCode, Shift)
End Sub
'GMT01 - FIN

Private Sub GrillasValidacionCompletar(oGrilla As MSHFlexGrid, KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    Dim J As Integer
    
    i = 0
    J = 0
    
    With oGrilla
        If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then
            If glModoPeticion = "MODI" And glUsrPerfilActual = "BPAR" Then
                If .rowSel > 0 Then
                    If .col = colVALIDACION_RESPUESTA Then
                        Select Case Chr(KeyCode)
                            Case "S": .TextMatrix(.row, colVALIDACION_RESPUESTA) = "Si"
                            Case "N": .TextMatrix(.row, colVALIDACION_RESPUESTA) = "No"
                        End Select
                    End If
                    For i = 2 To oGrilla.Rows - 1
                        If .TextMatrix(i, colVALIDACION_RESPUESTA) <> "-" Then
                            J = J + 1
                        End If
                    Next i
                    .TextMatrix(1, colVALIDACION_RESPUESTA) = "Respuestas (" & J & "/" & .Tag & ")"
                End If
            End If
        End If
    End With
End Sub

Private Sub InicializarValidacion(pet_nrointerno)
    Dim valid() As Long
    Dim valitem() As Long
    Dim i As Long
    
    If sp_GetValidacion(Null, Null) Then
        Do While Not aplRST.EOF
            ReDim Preserve valid(i)
            ReDim Preserve valitem(i)
            valid(i) = aplRST.Fields!valid
            valitem(i) = aplRST.Fields!valitem
            aplRST.MoveNext
            i = i + 1
            DoEvents
        Loop
        aplRST.Close
        For i = 0 To UBound(valitem)
            If Not sp_GetPeticionValidacion(pet_nrointerno, valid(i), valitem(i)) Then    'GMT01
                Call sp_InsertPeticionValidacion(pet_nrointerno, valid(i), valitem(i), "")
            End If                                                                        'GMT01
        Next i
    End If
    Erase valid
    Erase valitem
End Sub

Private Sub GuardarValidacionTecnica(oGrilla As MSHFlexGrid)
    Dim i As Integer
    Dim sDescripcion As String
    Dim NroHistorial As Long
    Dim bAgregar As Boolean
    Dim sCambioAtributo As String   'GMT01
    Dim cambioRespuesta As Boolean  'GMT01
    
    
    ' Primero se controla la completitud de las respuestas
    
    ' Guarda las respuestas dadas por el referente de sistemas. Si para alguna de las pesta�as la respuesta es afirmativa,
    ' agrega autom�ticamente al grupo correspondiente para que pueda realizar la evaluaci�n y emitir el dictamen correspondiente.
        
    ' Validamos al principio si ya se cargaron los items de validaci�n (peticiones anteriores al cambio)
    'If Not sp_GetPeticionValidacion(glNumeroPeticion, 1, Null) Then
        Call InicializarValidacion(glNumeroPeticion)
    'End If
    
    sDescripcion = ""
    bAgregar = False
    With oGrilla
        If glModoPeticion = "MODI" And InStr(1, "BPAR|GBPE|SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
            
            'graba en tablas
            For i = 2 To oGrilla.Rows - 1
                If .TextMatrix(i, colVALIDACION_RESPUESTA) <> "-" Then
                    'GMT01 - INI
                    If sp_GetPeticionValidacion(glNumeroPeticion, .TextMatrix(i, colVALIDACION_ID), .TextMatrix(i, colVALIDACION_ITEMKEY)) Then
                       
                       If Left(.TextMatrix(i, colVALIDACION_RESPUESTA), 1) = aplRST.Fields!valitemvalor Then
                          cambioRespuesta = False
                       Else
                          cambioRespuesta = True
                       End If
                       
                       If cambioRespuesta Then
                          Call sp_UpdatePeticionValidacion(glNumeroPeticion, .TextMatrix(i, colVALIDACION_ID), .TextMatrix(i, colVALIDACION_ITEMKEY), Left(.TextMatrix(i, colVALIDACION_RESPUESTA), 1))
                       End If
                    Else
                       Call sp_UpdatePeticionValidacion(glNumeroPeticion, .TextMatrix(i, colVALIDACION_ID), .TextMatrix(i, colVALIDACION_ITEMKEY), Left(.TextMatrix(i, colVALIDACION_RESPUESTA), 1))
                    End If
                    'GMT01 - FIN
                    
                    If .TextMatrix(i, colVALIDACION_RESPUESTA) = "Si" Then
                        bAgregar = True
                        sDescripcion = sDescripcion & vbCrLf & ClearNull(.TextMatrix(i, colVALIDACION_PREGUNTA)) & ": " & ClearNull(.TextMatrix(i, colVALIDACION_RESPUESTA))
                    End If
                End If
            Next i
            
            'asigna grupos
            If oGrilla.name = "grdArquitectura" Then
                If Not bExisteTecnoEnPeticionActual Then
                    If bAgregar Then Call AgregarGrupoValidacion(glTecnoGrupo, glTecnoSector, sDescripcion)
                Else
                    If Not bExisteTecnoEnEstadoActivo Then
                        ' Reactivar al grupo de arquitectura
                        If bAgregar Then Call ReactivarGrupoValidacion(glTecnoGrupo, glTecnoSector, sDescripcion)
                    Else
                        ' Cancelar el grupo de arquitectura
                        If Not bAgregar Then
                            Call CancelarGrupoValidacion(glTecnoGrupo, glTecnoSector)
                        End If
                    End If
                End If
            End If
            If oGrilla.name = "grdSeguridadInformatica" Then
                If Not bExisteSegInfEnPeticionActual Then
                    If bAgregar Then Call AgregarGrupoValidacion(glSegInfGrupo, glSegInfSector, sDescripcion)
                Else
                    If Not bExisteSegInfEnEstadoActivo Then
                        ' Reactivar al grupo de arquitectura
                        If bAgregar Then Call ReactivarGrupoValidacion(glSegInfGrupo, glSegInfSector, sDescripcion)
                    Else
                        If Not bAgregar Then
                            ' Cancelar el grupo de arquitectura
                            Call CancelarGrupoValidacion(glSegInfGrupo, glSegInfSector)
                        End If
                    End If
                End If
            End If
            ' GMT01 - INI
            If oGrilla.name = "grdData" Then
                sCambioAtributo = "� Validaci�n Tecnica completada � Antes: Incompleta / Despu�s: Completa"
                Call sp_AddHistorial(glNumeroPeticion, "VALTEC", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sCambioAtributo)
                Call sp_DoMensaje("PET", "VALTEC", glNumeroPeticion, "", "NULL", "", "")
            End If
            'GMT01 - FIN
        End If
    
    End With
End Sub

Private Sub ReactivarGrupoValidacion(Grupo As String, Sector As String, Descripcion As String)
    Const ESTADO_GRUPO = "PLANIF"
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim NroHistorial As Long
    
    Call sp_UpdatePeticionGrupo(glNumeroPeticion, Grupo, Now, Now, Null, Null, Null, Null, Null, 1, ESTADO_GRUPO, Now, 0, 0, 0)
    Call getIntegracionGrupo(glNumeroPeticion, Sector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
    NuevoEstadoSector = getNuevoEstadoSector(glNumeroPeticion, Sector)
    
    If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
        Call sp_UpdatePeticionSector(glNumeroPeticion, Sector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, date, "", 0, 0)
    Else
        Call sp_UpdatePeticionSector(glNumeroPeticion, Sector, Null, Null, Null, Null, hsPresup, NuevoEstadoSector, date, "", 0, 0)
    End If
    Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
    NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
    Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
    Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)

    NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST2", NuevoEstadoPeticion, Sector, NuevoEstadoSector, Grupo, ESTADO_GRUPO, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Reactivado autom�ticamente. �" & Descripcion)
    Call sp_UpdatePetSubField(glNumeroPeticion, Grupo, "HSTSOL", Null, Null, NroHistorial)
    
    If Grupo = glTecnoGrupo Then bExisteTecnoEnEstadoActivo = True
    If Grupo = glSegInfGrupo Then bExisteSegInfEnEstadoActivo = True
End Sub

Private Sub CancelarGrupoValidacion(Grupo As String, Sector As String)
    Const ESTADO_GRUPO = "CANCEL"
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim NroHistorial As Long
    Dim fe_ini_plan As Date
    Dim fe_fin_plan As Date
    Dim fe_ini_real As Date
    Dim fe_fin_real As Date
    Dim horaspresup As Long
    
    If sp_GetPeticionGrupo(glNumeroPeticion, Sector, Grupo) Then
        fe_ini_plan = IIf(IsNull(aplRST.Fields!fe_ini_plan), "00:00:00", aplRST.Fields!fe_ini_plan)
        fe_fin_plan = IIf(IsNull(aplRST.Fields!fe_fin_plan), "00:00:00", aplRST.Fields!fe_fin_plan)
        fe_ini_real = IIf(IsNull(aplRST.Fields!fe_ini_real), "00:00:00", aplRST.Fields!fe_ini_real)
        fe_fin_real = IIf(IsNull(aplRST.Fields!fe_fin_real), "00:00:00", aplRST.Fields!fe_fin_real)
        horaspresup = CLng(aplRST.Fields!horaspresup)
    End If
    Call sp_UpdatePeticionGrupo(glNumeroPeticion, Grupo, fe_ini_plan, fe_fin_plan, fe_ini_real, fe_fin_real, Null, Null, Null, horaspresup, ESTADO_GRUPO, Now, 0, 0, 0)
    
    Call getIntegracionGrupo(glNumeroPeticion, Sector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
    NuevoEstadoSector = getNuevoEstadoSector(glNumeroPeticion, Sector)
    
    If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
        Call sp_UpdatePeticionSector(glNumeroPeticion, Sector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, date, "", 0, 0)
    Else
        Call sp_UpdatePeticionSector(glNumeroPeticion, Sector, Null, Null, Null, Null, hsPresup, NuevoEstadoSector, date, "", 0, 0)
    End If
    Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
    NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
    Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
    Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)
    NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST1", NuevoEstadoPeticion, Sector, NuevoEstadoSector, Grupo, ESTADO_GRUPO, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Cancelado autom�ticamente. �" & vbCrLf & "Por cambio en las respuestas de validaci�n t�cnica, ya no es necesaria la participaci�n de este grupo.")
    Call sp_UpdatePetSubField(glNumeroPeticion, Grupo, "HSTSOL", Null, Null, NroHistorial)
    
    If Grupo = glTecnoGrupo Then bExisteTecnoEnEstadoActivo = False
    If Grupo = glSegInfGrupo Then bExisteSegInfEnEstadoActivo = False
End Sub

Private Function ValidarCompletitudValidacionTecnica() As Boolean
    Dim i As Integer
    
    bCompletoTodoArquitectura = False
    bCompletoTodoSeguridadInformatica = False
    bCompletoTodoData = False  'GMT01
    ValidarCompletitudValidacionTecnica = True
    
    With grdArquitectura
        For i = 2 To .Rows - 1
            If .TextMatrix(i, colVALIDACION_RESPUESTA) = "-" Then
                ValidarCompletitudValidacionTecnica = False
                Exit For
            End If
        Next i
        If ValidarCompletitudValidacionTecnica Then bCompletoTodoArquitectura = True
    End With
    
    ValidarCompletitudValidacionTecnica = True
    With grdSeguridadInformatica
        For i = 2 To .Rows - 1
            If .TextMatrix(i, colVALIDACION_RESPUESTA) = "-" Then
                ValidarCompletitudValidacionTecnica = False
                Exit For
            End If
        Next i
        If ValidarCompletitudValidacionTecnica Then bCompletoTodoSeguridadInformatica = True
    End With
    
    'GMT01 - INI
    ValidarCompletitudValidacionTecnica = True
    With grdData
        For i = 2 To .Rows - 1
            If .TextMatrix(i, colVALIDACION_RESPUESTA) = "-" Then
                ValidarCompletitudValidacionTecnica = False
                Exit For
            End If
        Next i
        If ValidarCompletitudValidacionTecnica Then bCompletoTodoData = True
    End With
    'GMT01 - FIN
    
    ValidarCompletitudValidacionTecnica = False
   ' If bCompletoTodoArquitectura And bCompletoTodoSeguridadInformatica Then 'GMT01
     If bCompletoTodoArquitectura And bCompletoTodoSeguridadInformatica And bCompletoTodoData Then 'GMT01
        ValidarCompletitudValidacionTecnica = True
        sbMainPeticion.SimpleText = ""
    End If
End Function

Private Sub AgregarGrupoValidacion(Grupo As String, Sector As String, Descripcion As String)
    Const ESTADO_GRUPO = "PLANIF"
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim NroHistorial As Long
    
    Call sp_InsertPeticionGrupo(glNumeroPeticion, Grupo, date, date, Null, Null, Null, Null, 0, 1, ESTADO_GRUPO, date, "", 0, 0)
    If Not sp_GetPeticionSector(glNumeroPeticion, Sector) Then
        Call sp_InsertPeticionSector(glNumeroPeticion, Sector, date, date, Null, Null, 1, ESTADO_GRUPO, date, "", 0, 0)
    End If
    
    Call getIntegracionGrupo(glNumeroPeticion, Sector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
    NuevoEstadoSector = getNuevoEstadoSector(glNumeroPeticion, Sector)
    If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
        Call sp_UpdatePeticionSector(glNumeroPeticion, Sector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, date, "", 0, 0)
    Else
        Call sp_UpdatePeticionSector(glNumeroPeticion, Sector, Null, Null, Null, Null, hsPresup, NuevoEstadoSector, date, "", 0, 0)
    End If
    Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
    NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
    Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
    Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)
    NroHistorial = sp_AddHistorial(glNumeroPeticion, "GNEW001", NuevoEstadoPeticion, Sector, NuevoEstadoSector, Grupo, ESTADO_GRUPO, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Agregado autom�ticamente. �" & Descripcion)
    Call sp_UpdatePetSubField(glNumeroPeticion, Grupo, "HSTSOL", Null, Null, NroHistorial)
    
    Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, Grupo, ESTADO_GRUPO, "", "")
    
    If Grupo = glTecnoGrupo Then bExisteTecnoEnEstadoActivo = False
    If Grupo = glSegInfGrupo Then bExisteSegInfEnEstadoActivo = False
End Sub

Private Sub grdArquitectura_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call GrillaMouseDown(grdArquitectura, Button, Shift, x, y)
End Sub

Private Sub grdSeguridadInformatica_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call GrillaMouseDown(grdSeguridadInformatica, Button, Shift, x, y)
End Sub
'GMT01 - INI
Private Sub grdData_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call GrillaMouseDown(grdData, Button, Shift, x, y)
End Sub
'GMT01 - FIN

Private Sub grdArquitectura_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call GrillaMouseMove(grdArquitectura, Button, Shift, x, y)
End Sub

Private Sub grdSeguridadInformatica_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call GrillaMouseMove(grdSeguridadInformatica, Button, Shift, x, y)
End Sub
'GMT01 - INI
Private Sub grdData_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Call GrillaMouseMove(grdData, Button, Shift, x, y)
End Sub
'GMT01 - FIN

' Efecto de cambio de mouseicon
Private Sub GrillaMouseMove(oGrid As MSHFlexGrid, Button As Integer, Shift As Integer, x As Single, y As Single)
    With oGrid
        If .MouseRow > 0 Then
            If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then
                If glModoPeticion = "MODI" And InStr(1, "GBPE|BPAR|SADM|", glUsrPerfilActual, vbTextCompare) > 0 Then
                    If .MouseCol = colVALIDACION_RESPUESTA Then
                        .MousePointer = flexCustom
                    Else
                        .MousePointer = flexDefault
                    End If
                End If
            End If
        End If
    End With
End Sub

' Completa el valor para la celda
Private Sub GrillaMouseDown(oGrid As MSHFlexGrid, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim seleccion As String
    Dim i As Long
    
    If Not bFlagValidacionTecnica Then Exit Sub
    With oGrid
        If .rowSel > 0 Then
            If Button = 2 Then
                If .MouseRow > 0 Then
                    If .MouseCol = colVALIDACION_RESPUESTA Then
                        If glModoPeticion = "MODI" Then
                        Select Case glUsrPerfilActual
                            Case "GBPE", "BPAR", "SADM"
                                seleccion = MenuMSHGridGet2(oGrid)
                                Select Case seleccion
                                    Case 1: .TextMatrix(.rowSel, colVALIDACION_RESPUESTA) = "Si"
                                    Case 2: .TextMatrix(.rowSel, colVALIDACION_RESPUESTA) = "No"
                                    Case 3:
                                        ' TODO: Hacer que seleccione todo Si
                                        .Enabled = False
                                        For i = 2 To .Rows - 1
                                            .TextMatrix(i, colVALIDACION_RESPUESTA) = "Si"
                                        Next i
                                        .Enabled = True
                                    Case 4:
                                        ' TODO: Hacer que seleccione todo No
                                        .Enabled = False
                                        For i = 2 To .Rows - 1
                                            .TextMatrix(i, colVALIDACION_RESPUESTA) = "No"
                                        Next i
                                        .Enabled = True
                                End Select
                        End Select
                        End If
                    End If
                End If
            End If
        End If
    End With
End Sub

Private Sub chkTextoAmpliar_Click(Index As Integer)
    Dim i As Integer

    Const picReestablecer = 37
    Const picAmpliar = 38
    Const picFontSize = 39
    
    Const textBoxTop = 180
    Const textBoxMaxHeight = 6495
    Const textBoxMinHeight = 1215
    
    Const memDescripcionTopDefault = 180
    Const memCaracteristicasTopDefault = 1500
    Const memMotivosTopDefault = 2820
    Const memRelacionesTopDefault = 4140
    Const memObservacionesTopDefault = 5460
    
    textActivo = Index
    
    If chkTextoAmpliar(Index).value = 1 Then
        For i = 0 To 4
            chkTextoAmpliar(i).ToolTipText = IIf(i = Index, "Reestablecer", "Ampliar")
            chkTextoAmpliar(i).Picture = imgPeticiones.ListImages(IIf(i = Index, picReestablecer, picAmpliar)).Picture
            chkTextoAmpliar(i).Enabled = IIf(i = Index, True, False)
            lblTextoDetalle(i).FontBold = IIf(i = Index, True, False)
            lblTextoDetalle(i).ForeColor = IIf(i = Index, vbBlack, RGB(192, 192, 192))
            Call HabilitarEdicionTextoFuentes(True)
        Next i
    Else
        For i = 0 To 4
            chkTextoAmpliar(i).Picture = imgPeticiones.ListImages(picAmpliar).Picture
            chkTextoAmpliar(i).Enabled = True
            lblTextoDetalle(i).FontBold = True
            lblTextoDetalle(i).ForeColor = vbBlack
            Call HabilitarEdicionTextoFuentes(False)
        Next i
    End If
    
    Select Case Index
        Case 0
            memDescripcion.Font.Size = IIf(chkTextoAmpliar(Index).value = 1, CInt(txtFontSize), 8)
            memDescripcion.Top = IIf(chkTextoAmpliar(Index).value = 1, textBoxTop, memDescripcionTopDefault)
            memDescripcion.Height = IIf(chkTextoAmpliar(Index).value = 1, textBoxMaxHeight, textBoxMinHeight)
            memDescripcion.ZOrder (0)
            memDescripcion.SetFocus
        Case 1
            memCaracteristicas.Font.Size = IIf(chkTextoAmpliar(Index).value = 1, CInt(txtFontSize), 8)
            memCaracteristicas.Top = IIf(chkTextoAmpliar(Index).value = 1, textBoxTop, memCaracteristicasTopDefault)
            memCaracteristicas.Height = IIf(chkTextoAmpliar(Index).value = 1, textBoxMaxHeight, textBoxMinHeight)
            memCaracteristicas.ZOrder (0)
            memCaracteristicas.SetFocus
        Case 2
            memMotivos.Font.Size = IIf(chkTextoAmpliar(Index).value = 1, CInt(txtFontSize), 8)
            memMotivos.Top = IIf(chkTextoAmpliar(Index).value = 1, textBoxTop, memMotivosTopDefault)
            memMotivos.Height = IIf(chkTextoAmpliar(Index).value = 1, textBoxMaxHeight, textBoxMinHeight)
            memMotivos.ZOrder (0)
            memMotivos.SetFocus
        Case 3:
            memRelaciones.Font.Size = IIf(chkTextoAmpliar(Index).value = 1, CInt(txtFontSize), 8)
            memRelaciones.Top = IIf(chkTextoAmpliar(Index).value = 1, textBoxTop, memRelacionesTopDefault)
            memRelaciones.Height = IIf(chkTextoAmpliar(Index).value = 1, textBoxMaxHeight, textBoxMinHeight)
            memRelaciones.ZOrder (0)
            memRelaciones.SetFocus
        Case 4:
            memObservaciones.Font.Size = IIf(chkTextoAmpliar(Index).value = 1, CInt(txtFontSize), 8)
            memObservaciones.Top = IIf(chkTextoAmpliar(Index).value = 1, textBoxTop, memObservacionesTopDefault)
            memObservaciones.Height = IIf(chkTextoAmpliar(Index).value = 1, textBoxMaxHeight, textBoxMinHeight)
            memObservaciones.ZOrder (0)
            memObservaciones.SetFocus
    End Select
End Sub

Private Sub HabilitarEdicionTexto(habilitar As Boolean)
    memDescripcion.BorderStyle = IIf(habilitar, 1, 0)
    memCaracteristicas.BorderStyle = IIf(habilitar, 1, 0)
    memMotivos.BorderStyle = IIf(habilitar, 1, 0)
    memRelaciones.BorderStyle = IIf(habilitar, 1, 0)
    memObservaciones.BorderStyle = IIf(habilitar, 1, 0)
End Sub

Private Sub HabilitarEdicionTextoFuentes(habilitar As Boolean)
    imgFontSize.visible = habilitar
    lblFontSize.visible = habilitar
    txtFontSize.visible = habilitar
    updFontSize.visible = habilitar
End Sub

Private Sub updFontSize_Change()
    Select Case textActivo
        Case 0: memDescripcion.Font.Size = updFontSize.value
        Case 1: memCaracteristicas.Font.Size = updFontSize.value
        Case 2: memMotivos.Font.Size = updFontSize.value
        Case 3: memRelaciones.Font.Size = updFontSize.value
        Case 4: memObservaciones.Font.Size = updFontSize.value
    End Select
End Sub

Private Sub cboPcfTipo_Click()
    If pcfOpcion = "A" Or pcfOpcion = "M" Then
        If CodigoCombo(cboPcfTipo, True) = "DAT" Then
            cboPcfClase.ListIndex = 1
            Call setHabilCtrl(cboPcfClase, DISABLE)
        Else
            Call setHabilCtrl(cboPcfClase, NORMAL)
        End If
    Else
        If cboPcfClase.Enabled Then
            Call setHabilCtrl(cboPcfClase, DISABLE)
        End If
    End If
End Sub
'GMT05 - INI
Private Function recupHabilitaciones(Peticion As String) As Boolean
    Dim usuarioRGyP As String
    
    'se recuperan los valores para saber que flujo de trabajo esta habilitado
    recupHabilitaciones = False
    Hab_Kpi = True
    Hab_beneficio = True
    Hab_RGyP = True
    
    ' Indica a partir de qu� fecha se deber� realizar la validaci�n t�cnica sobre las peticiones
    If sp_GetVarios("CTRLPET1") Then
        dValidacionTecnicaInicio = CDate(aplRST.Fields!var_fecha)
    End If
    
    ' GMT05 - Ini
    ' indica si se debe cargar los kpi
    If sp_GetVarios("HAB_KPI") Then
        Hab_Kpi = ClearNull(aplRST.Fields!var_numero)
    End If
    
    'indica si se debe cargar los beneficios
    If sp_GetVarios("HAB_BEN") Then
        Hab_beneficio = ClearNull(aplRST.Fields!var_numero)
    End If
    
    'indica si ver la solapa de vinculados
    If sp_GetVarios("HAB_RGyP") Then
        Hab_RGyP = ClearNull(aplRST.Fields!var_numero)
    End If
    
    'indica si ver la solapa de vinculados
    If sp_GetVarios("HAB_VINC") Then
        Hab_PesVinculado = ClearNull(aplRST.Fields!var_numero)
    End If
    
    'Si es una peticion existente, se verifica que poosee cargados kpi o beneficios, de ser as� habilito las pantallas para visualizarlas
    If IsNumeric(Peticion) Then
        If sp_GetPeticionBeneficios2(Peticion) Then
            Hab_beneficio = True
        End If
    
        If sp_GetPeticionKPI(Peticion, Null) Then
            Hab_Kpi = True
        End If
      
       If sp_GetUnaPeticionSinCategoria(Peticion) Then
            usuarioRGyP = ClearNull(aplRST.Fields!cod_bpe)
            If usuarioRGyP <> Null And usuarioRGyP <> "00000000" And usuarioRGyP <> " " Then
               Hab_RGyP = True
            End If
       End If

    End If
    
    'se hacen o no visibles las pesta�as
    If Hab_Kpi Then
        sstDetalles.TabVisible(0) = True
        lblPeticionPuntuacion.visible = True
        txtPuntuacion.visible = True
        Label2.visible = True
        txtPetPrioridad.visible = True
        Label11(2).visible = True
        txtValorImpacto.visible = True
        Label11(3).visible = True
        txtValorFacilidad.visible = True
    Else
        sstDetalles.TabVisible(0) = False
        lblPeticionPuntuacion.visible = False
        txtPuntuacion.visible = False
        Label2.visible = False
        txtPetPrioridad.visible = False
        Label11(2).visible = False
        txtValorImpacto.visible = False
        Label11(3).visible = False
        txtValorFacilidad.visible = False
        Label1.visible = False
    End If
    
    If Hab_beneficio Then
        sstDetalles.TabVisible(1) = True
        chkPeticionDisclaimer.visible = True
        chkCargarBeneficios.visible = True
    Else
        sstDetalles.TabVisible(1) = False
        chkPeticionDisclaimer.visible = False
        chkCargarBeneficios.visible = False
    End If
    
    If Hab_RGyP Then
        Label1.visible = True
        cboReferenteBPE.visible = True
        lblPeticionBloque3(1).visible = True
        txtFechaRefBPE.visible = True
        cboGestion.visible = True
        Label11(1).visible = True
    Else
        Label1.visible = False
        cboReferenteBPE.visible = False
        lblPeticionBloque3(1).visible = False
        txtFechaRefBPE.visible = False
        cboGestion.visible = False
        Label11(1).visible = False
    End If
      
    If Hab_PesVinculado Then
       orjBtn(orjDOC).visible = True
    Else
       orjBtn(orjDOC).visible = False
    End If
    ' GMT05 - FIN
    
    recupHabilitaciones = True
 End Function
'GMT05 - FIN

