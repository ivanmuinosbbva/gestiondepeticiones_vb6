VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form rptPetiIrregulares 
   AutoRedraw      =   -1  'True
   Caption         =   "Informes especiales de peticiones"
   ClientHeight    =   7050
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10425
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "rptPetiIrregulares.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7050
   ScaleWidth      =   10425
   Begin MSComctlLib.ImageList imgReportes 
      Left            =   9420
      Top             =   1500
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "rptPetiIrregulares.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "rptPetiIrregulares.frx":0B24
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6405
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   10320
      Begin MSComctlLib.ListView lswDatos 
         Height          =   4815
         Left            =   120
         TabIndex        =   10
         Top             =   1320
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   8493
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         SmallIcons      =   "imgReportes"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.ComboBox cboOpcionReporte 
         Height          =   315
         Left            =   900
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   240
         Width           =   8925
      End
      Begin VB.ComboBox cboEstructura 
         Height          =   315
         Left            =   900
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   660
         Width           =   8925
      End
      Begin MSFlexGridLib.MSFlexGrid grdDatos 
         Height          =   4575
         Left            =   120
         TabIndex        =   6
         Top             =   1680
         Visible         =   0   'False
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   8070
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblTitulos 
         AutoSize        =   -1  'True
         Caption         =   "Reporte:"
         Height          =   195
         Index           =   3
         Left            =   180
         TabIndex        =   8
         Top             =   255
         Width           =   645
      End
      Begin VB.Label lblTitulos 
         AutoSize        =   -1  'True
         Caption         =   "Seleccione la opci�n deseada:"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   2145
      End
      Begin VB.Label lblTitulos 
         AutoSize        =   -1  'True
         Caption         =   "Area:"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   2
         Top             =   675
         Width           =   405
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   60
      TabIndex        =   3
      Top             =   6360
      Width           =   10320
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   9300
         TabIndex        =   5
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   8340
         TabIndex        =   4
         Top             =   180
         Width           =   885
      End
   End
End
Attribute VB_Name = "rptPetiIrregulares"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const colRPTITEM = 0
Const colRPTNOM = 1
Const colRPTHABIL = 2
Const colRPTTOTALCOLS = 3

Dim flgEnCarga As Boolean
Dim vRetorno() As String
Dim opcion As Integer
Dim sReporteComentarios As String

Private Sub Form_Load()
    Call InicializarPantalla
    Call InicializarControles
    Call CargarGrid
End Sub

Private Sub InicializarPantalla()
    Me.Top = 0
    Me.Left = 0
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        Me.Height = 7560
        Me.Width = 10545
    End If
End Sub

Private Sub InicializarControles()
    Dim sDireccion As String
    Dim sGerencia As String
    
    flgEnCarga = True
    Call Puntero(True)
    Call Status("Inicializando...")

    opcion = 0
    With cboOpcionReporte
        .Clear
        If sp_GetRptcab(Null) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!rpthab) = "S" Then
                    .AddItem ClearNull(aplRST.Fields!rptid) & ": " & ClearNull(aplRST.Fields!rptnom)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With

    With cboEstructura
        If sp_GetDireccion("", "S") Then
            Do While Not aplRST.EOF
                'If ClearNull(aplRST!es_ejecutor) = "S" Then
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    cboEstructura.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
                    '.AddItem Trim(aplRST!nom_direccion) & ESPACIOS & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
                'End If
                aplRST.MoveNext
            Loop
        End If
        If sp_GetGerenciaXt("", "", "S") Then
            Do While Not aplRST.EOF
                'If ClearNull(aplRST!es_ejecutor) = "S" Then
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboEstructura.AddItem sDireccion & " � " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|NULL|NULL"
                    '.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & Space(150) & "||GERE|" & Trim(aplRST!cod_gerencia)   ' upd -005- a. Se cambia ">> " por "� "
                'End If
                aplRST.MoveNext
            Loop
        End If
        If sp_GetSectorXt(Null, Null, Null, "S") Then
            Do While Not aplRST.EOF
                'If ClearNull(aplRST!es_ejecutor) = "S" Then
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboEstructura.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|" & Trim(aplRST!cod_sector) & "|NULL"
                    '.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & Space(150) & "||SECT|" & Trim(aplRST!cod_sector)    ' upd -005- a. Se cambia ">> " por "� "
                'End If
                aplRST.MoveNext
            Loop
        End If
        If sp_GetGrupoXt("", "", "S") Then
            Do While Not aplRST.EOF
                'If ClearNull(aplRST!es_ejecutor) = "S" Then
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboEstructura.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_direccion) & "|" & Trim(aplRST!cod_gerencia) & "|" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                    '.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & Space(150) & "||GRUP|" & Trim(aplRST!cod_grupo) ' upd -005- a. Se cambia ">> " por "� "
                'End If
                aplRST.MoveNext
            Loop
        End If
        .AddItem "Todo el BBVA" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL|NULL", 0
        .ListIndex = 0
    End With
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub CargarGrid()
    Dim itmX As ListItem
    
    Call Puntero(True)
    With grdDatos
        .Clear
        .FocusRect = flexFocusNone
        .SelectionMode = flexSelectionByRow
        '.Font.name = "Arial"
        .Font.Size = 8
        .RowHeightMin = 250
        .Rows = 1
        .cols = colRPTTOTALCOLS
        .TextMatrix(0, colRPTITEM) = "Nro.": .ColWidth(colRPTITEM) = 600: .ColAlignment(colRPTITEM) = 0
        .TextMatrix(0, colRPTNOM) = "Descripci�n": .ColWidth(colRPTNOM) = 10000: .ColAlignment(colRPTNOM) = 0
        .TextMatrix(0, colRPTHABIL) = "habilitado": .ColWidth(colRPTHABIL) = 0: .ColAlignment(colRPTHABIL) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    
    With lswDatos
        ' Agrega objetos ColumnHeaders
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "Item", 600
        .ColumnHeaders.Add , , "Informe", 8000
        .ColumnHeaders.Add , , "Habilitado", 1000
        ' Asigna la propiedad View el valor Report.
        .View = lvwReport
        '.Arrange = lvwAutoLeft
        .ListItems.Clear
    End With

    If Not sp_GetRptdet(CodigoCombo(cboOpcionReporte, False)) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colRPTITEM) = ClearNull(aplRST.Fields!rptitem)
            .TextMatrix(.Rows - 1, colRPTNOM) = ClearNull(aplRST.Fields!rptnom)
            .TextMatrix(.Rows - 1, colRPTHABIL) = ClearNull(aplRST.Fields!rpthab)
        End With
        With lswDatos
            Set itmX = .ListItems.Add(, , ClearNull(aplRST!rptitem), , 2)
            itmX.SubItems(1) = CStr(aplRST!rptnom)
            itmX.SubItems(2) = CStr(aplRST!rpthab)
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub cmdOK_Click()
    ' Generar reporte
    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        'Call ViejoReporte
    End If
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
        
    Call Puntero(True)
    sReportName = "petirregu6.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    'sTitulo(1) = "�rea ejecutora: " & ClearNull(TextoCombo(cboEstructura))
    'sTitulo(2) = "Entre: " & txtDESDE & " y " & txtHASTA
        
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = TextoCombo(cboOpcionReporte, CodigoCombo(cboOpcionReporte))
        '.ReportComments = ClearNull(grdDatos.TextMatrix(grdDatos.RowSel, colRPTITEM)) & ". " & ClearNull(grdDatos.TextMatrix(grdDatos.RowSel, colRPTNOM))
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
    
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@nivel": crParamDef.AddCurrentValue (CStr(vRetorno(1)))
            Case "@area": crParamDef.AddCurrentValue (CStr(vRetorno(2)))
            'Case "@opcion": crParamDef.AddCurrentValue (Val(ClearNull(grdDatos.TextMatrix(grdDatos.RowSel, colRPTITEM))))
            Case "@opcion": crParamDef.AddCurrentValue (opcion)
        End Select
    Next
'    ' F�rmulas del reporte
'    With crReport
'        .FormulaFields.GetItemByName("@@0_TITULO").Text = Chr(34) & sTitulo(0) & Chr(34)
'        .FormulaFields.GetItemByName("@@1_TITULO").Text = Chr(34) & sTitulo(1) & Chr(34)
'        .FormulaFields.GetItemByName("@@2_TITULO").Text = Chr(34) & sTitulo(2) & Chr(34)
'    End With
    
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    Call Puntero(False)

    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub cboEstructura_Click()
    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
        MsgBox ("Error")
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub lswDatos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    opcion = Val(Item.Text)
    sReporteComentarios = Item.SubItems(1)
End Sub
