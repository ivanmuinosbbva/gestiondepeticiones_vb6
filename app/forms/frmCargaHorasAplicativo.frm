VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmCargaHorasAplicativo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Desglose de horas cargadas por Aplicativo"
   ClientHeight    =   5730
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8475
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   8475
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraBotonera 
      Height          =   5655
      Left            =   7080
      TabIndex        =   2
      Top             =   0
      Width           =   1335
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "CANCELAR"
         Enabled         =   0   'False
         Height          =   500
         Left            =   80
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Cancela todo el proceso de desglose de horas trabajadas por aplicativo"
         Top             =   120
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   80
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Agregar un nuevo registro de horas trabajadas"
         Top             =   3000
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Height          =   500
         Left            =   80
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Modificar el registro seleccionado"
         Top             =   3510
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   80
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el registro seleccionado"
         Top             =   4020
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   80
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   5040
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Enabled         =   0   'False
         Height          =   500
         Left            =   80
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   4530
         Width           =   1170
      End
   End
   Begin VB.Frame fraHorasAplicativoDesc 
      Height          =   975
      Left            =   120
      TabIndex        =   24
      Top             =   0
      Width           =   6855
      Begin VB.Label lblExplicacion 
         Caption         =   "Label2"
         ForeColor       =   &H00C00000&
         Height          =   615
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Width           =   6615
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraHorasAplicativo 
      Height          =   2655
      Left            =   120
      TabIndex        =   1
      Top             =   3000
      Width           =   6855
      Begin VB.TextBox txtObservaciones 
         Height          =   900
         Left            =   960
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   16
         Top             =   1635
         Width           =   5415
      End
      Begin VB.ComboBox cboAplicativo 
         Height          =   300
         Left            =   960
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   870
         Width           =   5445
      End
      Begin VB.CommandButton cmdMasAplicativos 
         Height          =   300
         Left            =   6360
         Picture         =   "frmCargaHorasAplicativo.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar otro Aplicativo que no figura en la lista"
         Top             =   870
         Width           =   350
      End
      Begin AT_MaskText.MaskText txtHoras 
         Height          =   300
         Left            =   960
         TabIndex        =   5
         Top             =   1245
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   529
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   3
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtMinutos 
         Height          =   300
         Left            =   1590
         TabIndex        =   6
         Top             =   1245
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   529
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         EmptyValue      =   0   'False
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblPeticionTarea 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   26
         Top             =   240
         Width           =   6615
      End
      Begin VB.Label lblHorasResto 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "HHH:MM"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   6000
         TabIndex        =   23
         Top             =   480
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHsResto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Resto:"
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   5400
         TabIndex        =   22
         Top             =   480
         Width           =   495
      End
      Begin VB.Label lblHsCargadas 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Hs. cargadas:"
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   2520
         TabIndex        =   21
         Top             =   480
         Width           =   1050
      End
      Begin VB.Label lblHorasAcargar 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "HHH:MM"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   1200
         TabIndex        =   20
         Top             =   480
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblTotHoras 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "HHH:MM"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   3600
         TabIndex        =   18
         Top             =   480
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHsACargar 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Hs. a cargar:"
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   120
         TabIndex        =   19
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   180
         Left            =   120
         TabIndex        =   17
         Top             =   0
         Width           =   45
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Obs."
         Height          =   180
         Left            =   120
         TabIndex        =   15
         Top             =   1635
         Width           =   360
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Horas"
         Height          =   180
         Left            =   120
         TabIndex        =   9
         Top             =   1305
         Width           =   450
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   ":"
         Height          =   180
         Left            =   1500
         TabIndex        =   8
         Top             =   1305
         Width           =   60
      End
      Begin VB.Label lblApp 
         AutoSize        =   -1  'True
         Caption         =   "Aplicativo"
         Height          =   180
         Left            =   120
         TabIndex        =   7
         Top             =   930
         Width           =   765
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   1890
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   6885
      _ExtentX        =   12144
      _ExtentY        =   3334
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmCargaHorasAplicativo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 28.05.2009 - Este control lo dejamos comentado para que por ahora se permita desglosar por menos que la cantidad total de horas cargadas para el item.

Option Explicit

Dim flgEnCarga As Boolean
Dim sOpcionSeleccionada As String
Dim sModo As String
Dim iTotalHoras As Long

Private Sub Form_Load()
    flgEnCarga = True
    'lblHorasAcargar = hrsFmtHora(glHorasCargadas)
    lblHorasResto = hrsFmtHora(0)
    If frmCargaHoras.cboPeticion.ListIndex = -1 Then
        lblPeticionTarea.Caption = "Tarea: " & CodigoCombo(frmCargaHoras.cboTarea) & ": " & TextoCombo(frmCargaHoras.cboTarea, CodigoCombo(frmCargaHoras.cboTarea))
    Else
        lblPeticionTarea.Caption = "Petici�n: " & CodigoCombo(frmCargaHoras.cboPeticion) & " - " & TextoCombo(frmCargaHoras.cboPeticion, CodigoCombo(frmCargaHoras.cboPeticion))
    End If
    'lblExplicacion.Caption = "El desglose de horas trabajadas por aplicativo no es obligatorio. Sin embargo, si decide desglosar sus horas entre distintos aplicativos, debe realizar el desglose total de las horas imputadas a la petici�n o tarea correspondiente." ' del -001- a.
    lblExplicacion.Caption = "El desglose de horas trabajadas por aplicativo no es obligatorio. Si decide desglosar sus horas entre distintos aplicativos, puede realizar el desglose de las horas imputadas a la petici�n o tarea correspondiente hasta la cantidad total de las horas cargadas."
    
    sOpcionSeleccionada = ""
    sModo = ""
    Call setHabilCtrl(txtHoras, "DIS")
    Call setHabilCtrl(txtMinutos, "DIS")
    Call setHabilCtrl(cboAplicativo, "DIS")
    Call setHabilCtrl(txtObservaciones, "DIS")
    cmdMasAplicativos.Enabled = False
    txtHoras = "": txtMinutos = "": txtObservaciones = ""
    cmdCancelar.Enabled = True
    InicializarControles
    CargarGrid
    flgEnCarga = False
    modUser32.IniciarScroll grdDatos
End Sub

Private Sub InicializarControles()
    If sp_GetAplicativo(Null, Null, Null) Then
        With cboAplicativo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!app_id) & " : " & ClearNull(aplRST.Fields!app_nombre) & Space(200) & "||" & ClearNull(aplRST.Fields!app_id)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End With
    End If
End Sub

Private Sub InicializarControlesGrupo()
    If sp_GetGrupoAplicativo(glGrupo, Null) Then
        With cboAplicativo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!app_id) & " : " & ClearNull(aplRST.Fields!app_nombre) & Space(200) & "||" & ClearNull(aplRST.Fields!app_id)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End With
    End If
End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If Len(CodigoCombo(cboAplicativo, True)) > 0 Then
        sOpcionSeleccionada = "M"
        Call HabilitarBotones(1, False)
    Else
        MsgBox "Debe seleccionar un aplicativo para modificar.", vbExclamation + vbOKOnly, "Aplicativo"
        sOpcionSeleccionada = ""
        Call HabilitarBotones(0, False)
        Exit Sub
    End If
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertHsTrabAplicativo(CodigoCombo(frmCargaHoras.cboRecurso), CodigoCombo(frmCargaHoras.cboTarea), CodigoCombo(frmCargaHoras.cboPeticion, True), frmCargaHoras.txtFechaDesde.DateValue, frmCargaHoras.txtFechaHasta.DateValue, Val(frmCargaHoras.txtHoras) * 60 + Val(frmCargaHoras.txtMinutos), CodigoCombo(cboAplicativo, True), Val(txtHoras) * 60 + Val(txtMinutos), txtObservaciones) Then
                    If sp_GetHsTrabAplicativo(CodigoCombo(frmCargaHoras.cboRecurso), CodigoCombo(frmCargaHoras.cboTarea), CodigoCombo(frmCargaHoras.cboPeticion, True), frmCargaHoras.txtFechaDesde.DateValue, frmCargaHoras.txtFechaHasta.DateValue, Val(frmCargaHoras.txtHoras) * 60 + Val(frmCargaHoras.txtMinutos), CodigoCombo(cboAplicativo, True), Null) Then
                        If Not aplRST.EOF Then
                            With grdDatos
                                .Rows = .Rows + 1
                                .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST.Fields.item("cod_recurso"))
                                .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST.Fields.item("cod_tarea"))
                                .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST.Fields.item("pet_nrointerno"))
                                .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST.Fields.item("app_horas"))
                                .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST.Fields.item("app_id"))
                                .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST.Fields.item("app_nombre"))
                                .TextMatrix(.Rows - 1, 6) = hrsFmtHora(Val(ClearNull(aplRST.Fields.item("app_horas"))))
                                .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST.Fields.item("hsapp_texto"))
                                .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST.Fields.item("app_horas"))
                                .TextMatrix(.Rows - 1, 9) = "S"     ' Es nuevo registro (agregado ahora por el usuario) Si cancela el proceso, los nuevos registros son eliminados.
                                .TextMatrix(.Rows - 1, 10) = ClearNull(aplRST.Fields.item("app_horas"))
                                .TopRow = .Rows - 1
                                .Row = .Rows - 1
                                .RowSel = .Rows - 1
                                .Col = 0
                                .ColSel = .cols - 1
                                Call MostrarSeleccion
                            End With
                            DoEvents
                        End If
                    End If
                    Call HabilitarBotones(2, False)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                With grdDatos
                    If sp_UpdateHsTrabAplicativo(CodigoCombo(frmCargaHoras.cboRecurso), CodigoCombo(frmCargaHoras.cboTarea), CodigoCombo(frmCargaHoras.cboPeticion, True), frmCargaHoras.txtFechaDesde.DateValue, frmCargaHoras.txtFechaHasta.DateValue, Val(frmCargaHoras.txtHoras) * 60 + Val(frmCargaHoras.txtMinutos), CodigoCombo(cboAplicativo, True), Val(txtHoras) * 60 + Val(txtMinutos), txtObservaciones) Then
                        If .RowSel > 0 Then
                            .TextMatrix(.RowSel, 6) = hrsFmtHora(Val(txtHoras) * 60 + Val(txtMinutos))
                            .TextMatrix(.RowSel, 7) = txtObservaciones
                            .TextMatrix(.RowSel, 10) = .TextMatrix(.RowSel, 8)      ' Me guardo el valor anterior
                            .TextMatrix(.RowSel, 8) = Val(txtHoras) * 60 + Val(txtMinutos)
                            .TextMatrix(.RowSel, 9) = "M"                           ' Es un registro modificado
                        End If
                        Call HabilitarBotones(0, False)
                    End If
                End With
            End If
        Case "E"
            If sp_DeleteHsTrabAplicativo(CodigoCombo(frmCargaHoras.cboRecurso), CodigoCombo(frmCargaHoras.cboTarea), CodigoCombo(frmCargaHoras.cboPeticion, True), frmCargaHoras.txtFechaDesde.DateValue, frmCargaHoras.txtFechaHasta.DateValue, Val(frmCargaHoras.txtHoras) * 60 + Val(frmCargaHoras.txtMinutos), CodigoCombo(cboAplicativo, True)) Then
                With grdDatos
                    If .Rows > 1 Then
                        If .Rows = 2 Then
                            .Rows = 1
                        Else
                            If .RowSel > 0 Then
                               .RemoveItem (.RowSel)
                            End If
                        End If
                    End If
                End With
                Call HabilitarBotones(0, False)
            End If
    End Select
    'SumaHoras
End Sub

Sub CargarGrid()
    Dim nPos As Integer
    txtHoras.Text = ""
    txtMinutos.Text = ""
    txtObservaciones.Text = ""
    cboAplicativo.ListIndex = -1
    cmdModificar.Enabled = False
    cmdEliminar.Enabled = False
    
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .cols = 11
        .Rows = 1
        .ColWidth(0) = 0: .ColAlignment(0) = 0: .TextMatrix(0, 0) = "cod_recurso"
        .ColWidth(1) = 0: .ColAlignment(1) = 0: .TextMatrix(0, 1) = "cod_tarea"
        .ColWidth(2) = 0: .ColAlignment(2) = 0: .TextMatrix(0, 2) = "pet_nrointerno"
        .ColWidth(3) = 0: .ColAlignment(3) = 0: .TextMatrix(0, 3) = "horas"
        .ColWidth(4) = 1500: .ColAlignment(4) = 0: .TextMatrix(0, 4) = "Aplicativo"
        .ColWidth(5) = 3630: .ColAlignment(5) = 0: .TextMatrix(0, 5) = "Nombre descriptivo"
        .ColWidth(6) = 800: .ColAlignment(6) = 6: .TextMatrix(0, 6) = "Horas"
        .ColWidth(7) = 5000: .ColAlignment(7) = 0: .TextMatrix(0, 7) = "Observaciones"
        .ColWidth(8) = 0: .ColAlignment(8) = 0: .TextMatrix(0, 8) = "hs_brutas"
        .ColWidth(9) = 0: .ColAlignment(9) = 0: .TextMatrix(0, 9) = "nvo_reg"
        .ColWidth(10) = 0: .ColAlignment(10) = 0: .TextMatrix(0, 10) = "toths_orig"
    End With
    
    If Not sp_GetHsTrabAplicativo(CodigoCombo(frmCargaHoras.cboRecurso), CodigoCombo(frmCargaHoras.cboTarea), CodigoCombo(frmCargaHoras.cboPeticion, True), frmCargaHoras.txtFechaDesde.DateValue, frmCargaHoras.txtFechaHasta.DateValue, Val(frmCargaHoras.txtHoras) * 60 + Val(frmCargaHoras.txtMinutos), Null, Null) Then GoTo finx
    flgEnCarga = True
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST.Fields.item("cod_recurso"))
            .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST.Fields.item("cod_tarea"))
            .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST.Fields.item("pet_nrointerno"))
            .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST.Fields.item("horas"))
            .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST.Fields.item("app_id"))
            .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST.Fields.item("app_nombre"))
            .TextMatrix(.Rows - 1, 6) = hrsFmtHora(Val(ClearNull(aplRST.Fields.item("app_horas"))))
            .TextMatrix(.Rows - 1, 7) = ClearNull(aplRST.Fields.item("hsapp_texto"))
            .TextMatrix(.Rows - 1, 8) = ClearNull(aplRST.Fields.item("app_horas"))
            .TextMatrix(.Rows - 1, 9) = "N"     ' No es nuevo registro (ya exist�a)
            .TextMatrix(.Rows - 1, 10) = ClearNull(aplRST.Fields.item("app_horas"))
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    cmdCerrar.Enabled = True
finx:
    Dim i As Long
    With grdDatos
        .FocusRect = flexFocusNone
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        For i = 0 To .cols - 1
            .Col = i
            .CellFontBold = True
        Next i
    End With
    flgEnCarga = False
    DoEvents
    Call MostrarSeleccion
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Call LockProceso(False)
            Unload Me
'            If Not frmCargaHoras.bDesgloseCancelado Then
'                If ValidarHoras Then
'                    Unload Me
'                End If
'            Else
'                Unload Me
'            End If
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    grdDatos.Font = "Tahoma"
    Show
    Call HabilitarBotones(0)
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Call LockProceso(True)
    Call setHabilCtrl(txtHoras, "DIS")
    Call setHabilCtrl(txtMinutos, "DIS")
    Call setHabilCtrl(cboAplicativo, "DIS")
    Call setHabilCtrl(txtObservaciones, "DIS")
    cmdMasAplicativos.Enabled = False
    Select Case nOpcion
        Case 0  ' NADA (VISUALIZACION)
            InicializarControles
            lblSeleccion = "": lblSeleccion.visible = False
            txtHoras.Text = ""
            txtMinutos.Text = ""
            txtObservaciones.Text = ""
            grdDatos.Enabled = True
            cmdCancelar.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            sOpcionSeleccionada = ""
            cmdMasAplicativos.Enabled = False
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 2  ' ALTA / CONTINUA
            InicializarControlesGrupo
            lblSeleccion = "AGREGAR ": lblSeleccion.visible = True
            txtHoras = "": txtMinutos = "": txtObservaciones = ""
            Call setHabilCtrl(txtHoras, "NOR")
            Call setHabilCtrl(txtMinutos, "NOR")
            Call setHabilCtrl(cboAplicativo, "NOR")
            Call setHabilCtrl(txtObservaciones, "NOR")
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdMasAplicativos.Enabled = True
        Case 1  ' ALTA DIRECTA / MODIFICACION / ELIMINACION
            grdDatos.Enabled = False
            cmdCancelar.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    InicializarControlesGrupo
                    lblSeleccion = "AGREGAR ": lblSeleccion.visible = True
                    Call setHabilCtrl(txtHoras, "NOR")
                    Call setHabilCtrl(txtMinutos, "NOR")
                    Call setHabilCtrl(cboAplicativo, "NOR")
                    Call setHabilCtrl(txtObservaciones, "NOR")
                    txtHoras = "": txtMinutos = "": txtObservaciones = ""
                    cmdCerrar.Caption = "FINALIZAR"
                    cmdCerrar.ToolTipText = "Salir del modo de altas consecutivas"
                    cmdMasAplicativos.Enabled = True
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    Call setHabilCtrl(txtHoras, "NOR")
                    Call setHabilCtrl(txtMinutos, "NOR")
                    Call setHabilCtrl(txtObservaciones, "NOR")
                    cmdMasAplicativos.Enabled = True
                    txtHoras.SetFocus
                Case "E"
                    cmdMasAplicativos.Enabled = False
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
            End Select
    End Select
    Call LockProceso(False)
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If cboAplicativo.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un aplicativo")
        CamposObligatorios = False
        Exit Function
    End If
    If Val(txtHoras) * 60 + Val(txtMinutos) = 0 Then
        MsgBox ("Debe asignar horas")
        CamposObligatorios = False
        txtHoras.SetFocus
        Exit Function
    End If
'{ del -001- a.
'    ' Controles
'    Select Case sOpcionSeleccionada
'        Case "A"
'            If Val(glHorasCargadas) < (iTotalHoras + (Val(txtHoras) * 60 + Val(txtMinutos))) Then
'                MsgBox "Vd. esta tratando de ingresar m�s horas de las que puede desglosar por petici�n/tarea. Revise.", vbExclamation + vbOKOnly
'                CamposObligatorios = False
'                Exit Function
'            End If
'        'Case "M"
'        '    If Val(glHorasCargadas) < ((iTotalHoras - Val(ClearNull(grdDatos.TextMatrix(grdDatos.RowSel, 8)))) + Val(txtHoras) * 60 + Val(txtMinutos)) Then
'        '        MsgBox "Vd. esta tratando de ingresar m�s horas de las que puede desglosar por petici�n/tarea. Revise.", vbExclamation + vbOKOnly
'        '        CamposObligatorios = False
'        '        Exit Function
'        '    End If
'    End Select
'}
End Function

Sub MostrarSeleccion()
    Dim nPos As Integer
    cmdModificar.Enabled = False
    cmdEliminar.Enabled = False
    
    With grdDatos
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 4) <> "" Then
                flgEnCarga = True
                cboAplicativo.ListIndex = PosicionCombo(Me.cboAplicativo, .TextMatrix(.RowSel, 4), True)
                txtHoras = hrsGetHoras(Val(ClearNull(.TextMatrix(.RowSel, 8))))
                txtMinutos = hrsGetMinutos(Val(ClearNull(.TextMatrix(.RowSel, 8))))
                txtObservaciones = ClearNull(.TextMatrix(.RowSel, 7))
                cmdModificar.Enabled = True
                cmdEliminar.Enabled = True
            End If
        End If
    End With
    flgEnCarga = False
    DoEvents
    'Call SumaHoras
    lblTotHoras.Caption = hrsFmtHora(iTotalHoras)
    DoEvents
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub

'Sub SumaHoras()
'    Dim i As Integer
'    With grdDatos
'        iTotalHoras = 0
'        If .Rows < 2 Then
'            Exit Sub
'        End If
'        For i = 1 To .Rows - 1
'           iTotalHoras = iTotalHoras + Val(ClearNull(.TextMatrix(i, 8)))
'        Next
'    End With
'    If Val(glHorasCargadas) - iTotalHoras < 0 Then
'        lblHorasResto.ForeColor = vbRed
'    Else
'        lblHorasResto.ForeColor = vbBlack
'    End If
'    If Val(glHorasCargadas) >= iTotalHoras Then
'        lblHorasResto = hrsFmtHora(Val(glHorasCargadas) - iTotalHoras)
'    Else
'        lblHorasResto = hrsFmtHora(iTotalHoras - Val(glHorasCargadas))
'    End If
'End Sub

Private Sub cmdMasAplicativos_Click()
    Dim vRetorno() As String
    Dim nPos As Integer
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    flgEnCarga = True
    glAuxRetorno = ""
    frmSelAplicativo.cAmbiente = "T"
    frmSelAplicativo.Show 1
    DoEvents
    If glAuxRetorno <> "" Then
        If ParseString(vRetorno, glAuxRetorno, ":") > 0 Then
            nPos = PosicionCombo(cboAplicativo, vRetorno(1))
            If nPos = -1 Then
                cboAplicativo.AddItem glAuxRetorno
                nPos = PosicionCombo(cboAplicativo, vRetorno(1))
                If nPos = -1 Then
                    MsgBox ("Error al incorporar aplicativo")
                    cboAplicativo.ListIndex = 0
                Else
                    cboAplicativo.ListIndex = nPos
                End If
            Else
                cboAplicativo.ListIndex = nPos
            End If
            DoEvents
        End If
       cboAplicativo.SetFocus
    End If
    flgEnCarga = False
    Call LockProceso(False)
End Sub

'Private Function ValidarHoras() As Boolean
'    ValidarHoras = True
'    ' Hay diferencia entre las horas a desglosar y las desglosadas
'    If glHorasCargadas <> iTotalHoras Then
'        '{ del -001- a.
'        ' Faltan desglosar horas: esto lo dejamos as� por sugerencia de Susana Dopazo
'        'If glHorasCargadas > iTotalHoras Then
'        '    MsgBox "Falta completar el desglose de horas por aplicativo para esta petici�n/tarea. Revise.", vbExclamation + vbOKOnly, "Faltan desglosar horas"
'        'End If
'        '}
'        ' Hay horas desglosadas de m�s
'        If glHorasCargadas < iTotalHoras Then
'            MsgBox "El desglose de horas por aplicativo ha superado la cantidad de horas para esta petici�n/tarea. Revise.", vbExclamation + vbOKOnly, "Hay horas desglosadas de m�s"
'            ValidarHoras = False
'            Exit Function
'        End If
'        If iTotalHoras = 0 Then
'            frmCargaHoras.bDesgloseRealizado = False
'        Else
'            frmCargaHoras.bDesgloseRealizado = True
'        End If
'    Else
'        frmCargaHoras.bDesgloseRealizado = True
'    End If
'End Function

Private Sub cmdCancelar_Click()
    If MsgBox("Si continua, todo el desglose para esta petici�n/tarea ser� eliminado. " & vbCrLf & "�Desea continuar?", vbQuestion + vbYesNo, "Cancelar el desglose") = vbYes Then
        Call sp_DeleteHsTrabAplicativo(CodigoCombo(frmCargaHoras.cboRecurso), CodigoCombo(frmCargaHoras.cboTarea), CodigoCombo(frmCargaHoras.cboPeticion, True), frmCargaHoras.txtFechaDesde.DateValue, frmCargaHoras.txtFechaHasta.DateValue, Val(frmCargaHoras.txtHoras) * 60 + Val(frmCargaHoras.txtMinutos), Null)
        'frmCargaHoras.bDesgloseCancelado = True
        'frmCargaHoras.bDesgloseRealizado = False
        cmdCerrar_Click
    End If
End Sub
