VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmAplicativosCarga 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Aplicativos"
   ClientHeight    =   4650
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11850
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAplicativosCarga.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4650
   ScaleWidth      =   11850
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdWizard_Atras 
      Caption         =   "� Atr�s"
      Height          =   375
      Left            =   8400
      TabIndex        =   19
      Top             =   4200
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Cancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   7200
      TabIndex        =   18
      Top             =   4200
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   9480
      TabIndex        =   1
      Top             =   4200
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   10680
      TabIndex        =   0
      Top             =   4200
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Adelante 
      Caption         =   "Sguiente �"
      Height          =   375
      Left            =   9480
      TabIndex        =   64
      Top             =   4200
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Terminar 
      Caption         =   "Finalizar"
      Default         =   -1  'True
      Height          =   375
      Left            =   10680
      TabIndex        =   65
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Frame fraEditMode 
      Height          =   3975
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   11655
      Begin VB.ComboBox cmb_Resp_Grupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":014A
         Left            =   6480
         List            =   "frmAplicativosCarga.frx":014C
         Style           =   2  'Dropdown List
         TabIndex        =   31
         Top             =   2640
         Width           =   4815
      End
      Begin VB.ComboBox cmb_Resp_Sector 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":014E
         Left            =   6480
         List            =   "frmAplicativosCarga.frx":0150
         Style           =   2  'Dropdown List
         TabIndex        =   30
         Top             =   2280
         Width           =   4815
      End
      Begin VB.ComboBox cmb_Resp_Gerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":0152
         Left            =   6480
         List            =   "frmAplicativosCarga.frx":0154
         Style           =   2  'Dropdown List
         TabIndex        =   29
         Top             =   1920
         Width           =   4815
      End
      Begin VB.CheckBox chkAppHab 
         Alignment       =   1  'Right Justify
         Caption         =   "Habilitado"
         Height          =   255
         Left            =   10200
         TabIndex        =   27
         Top             =   840
         Width           =   1095
      End
      Begin VB.ComboBox cmbAppAmb 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":0156
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":0158
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   1200
         Width           =   2535
      End
      Begin VB.ComboBox cmbSector 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":015A
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":015C
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   2280
         Width           =   4815
      End
      Begin VB.ComboBox cmbGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":015E
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":0160
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   2640
         Width           =   4815
      End
      Begin VB.ComboBox cmbDireccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":0162
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":0164
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   1560
         Width           =   4815
      End
      Begin VB.ComboBox cmbGerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":0166
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":0168
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   1920
         Width           =   4815
      End
      Begin AT_MaskText.MaskText txt_Resp_Sector 
         Height          =   300
         Left            =   6480
         TabIndex        =   3
         Top             =   2280
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin AT_MaskText.MaskText txt_Resp_Gerencia 
         Height          =   300
         Left            =   6480
         TabIndex        =   6
         Top             =   1920
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   300
         Left            =   1080
         TabIndex        =   32
         Top             =   480
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   529
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
      End
      Begin AT_MaskText.MaskText txtNombre 
         Height          =   300
         Left            =   1080
         TabIndex        =   33
         Top             =   840
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
      End
      Begin VB.ComboBox cmb_Resp_Direccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":016A
         Left            =   6480
         List            =   "frmAplicativosCarga.frx":016C
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   1560
         Width           =   4815
      End
      Begin AT_MaskText.MaskText txt_Resp_Grupo 
         Height          =   300
         Left            =   6480
         TabIndex        =   4
         Top             =   2640
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin AT_MaskText.MaskText txt_Resp_Direccion 
         Height          =   300
         Left            =   6480
         TabIndex        =   5
         Top             =   1560
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   17
         Top             =   480
         Width           =   525
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   180
         Index           =   1
         Left            =   240
         TabIndex        =   16
         Top             =   840
         Width           =   585
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Ambiente"
         Height          =   180
         Index           =   2
         Left            =   240
         TabIndex        =   15
         Top             =   1200
         Width           =   720
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         Height          =   180
         Index           =   6
         Left            =   240
         TabIndex        =   14
         Top             =   2280
         Width           =   495
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         Height          =   180
         Index           =   7
         Left            =   240
         TabIndex        =   13
         Top             =   2640
         Width           =   450
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   10
         Left            =   6000
         TabIndex        =   12
         Top             =   2280
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   11
         Left            =   6000
         TabIndex        =   11
         Top             =   2640
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   8
         Left            =   6000
         TabIndex        =   10
         Top             =   1560
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         Height          =   180
         Index           =   4
         Left            =   240
         TabIndex        =   9
         Top             =   1560
         Width           =   720
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   9
         Left            =   6000
         TabIndex        =   8
         Top             =   1920
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         Height          =   180
         Index           =   5
         Left            =   240
         TabIndex        =   7
         Top             =   1920
         Width           =   675
      End
   End
   Begin VB.Frame fraStep3 
      Caption         =   " Paso 3. Finalizaci�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   120
      TabIndex        =   66
      Top             =   120
      Width           =   11655
   End
   Begin VB.Frame fraStep2 
      Caption         =   " Paso 2. Estableciendo jerarqu�a del aplicativo "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   120
      TabIndex        =   46
      Top             =   120
      Width           =   11655
      Begin VB.CheckBox chkINSResp_Sector 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   11160
         Picture         =   "frmAplicativosCarga.frx":016E
         Style           =   1  'Graphical
         TabIndex        =   71
         Top             =   2280
         Width           =   300
      End
      Begin VB.CheckBox chkINSResp_Gerencia 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   11160
         Picture         =   "frmAplicativosCarga.frx":02B8
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   1800
         Width           =   300
      End
      Begin VB.CheckBox chkINSResp_Direccion 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   11160
         Picture         =   "frmAplicativosCarga.frx":0402
         Style           =   1  'Graphical
         TabIndex        =   69
         Top             =   1320
         Width           =   300
      End
      Begin VB.CheckBox chkINSResp_Grupo 
         Alignment       =   1  'Right Justify
         Height          =   300
         Left            =   11160
         Picture         =   "frmAplicativosCarga.frx":054C
         Style           =   1  'Graphical
         TabIndex        =   68
         Top             =   2760
         Width           =   300
      End
      Begin VB.ComboBox cmbINSSector 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":0696
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":0698
         Style           =   2  'Dropdown List
         TabIndex        =   58
         Top             =   2265
         Width           =   4815
      End
      Begin VB.ComboBox cmbINSGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":069A
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":069C
         Style           =   2  'Dropdown List
         TabIndex        =   57
         Top             =   2745
         Width           =   4815
      End
      Begin VB.ComboBox cmbINSDireccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":069E
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":06A0
         Style           =   2  'Dropdown List
         TabIndex        =   56
         Top             =   1305
         Width           =   4815
      End
      Begin VB.ComboBox cmbINSGerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":06A2
         Left            =   1080
         List            =   "frmAplicativosCarga.frx":06A4
         Style           =   2  'Dropdown List
         TabIndex        =   55
         Top             =   1785
         Width           =   4815
      End
      Begin AT_MaskText.MaskText txtResp_Sector 
         Height          =   300
         Left            =   6600
         TabIndex        =   59
         Top             =   2280
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin AT_MaskText.MaskText txtResp_Grupo 
         Height          =   300
         Left            =   6600
         TabIndex        =   60
         Top             =   2760
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin AT_MaskText.MaskText txtResp_Gerencia 
         Height          =   300
         Left            =   6600
         TabIndex        =   62
         Top             =   1800
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin VB.ComboBox cmbINSResp_Gerencia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":06A6
         Left            =   6600
         List            =   "frmAplicativosCarga.frx":06A8
         Style           =   2  'Dropdown List
         TabIndex        =   73
         Top             =   1800
         Width           =   4575
      End
      Begin VB.ComboBox cmbINSResp_Sector 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":06AA
         Left            =   6600
         List            =   "frmAplicativosCarga.frx":06AC
         Style           =   2  'Dropdown List
         TabIndex        =   74
         Top             =   2280
         Width           =   4575
      End
      Begin VB.ComboBox cmbINSResp_Grupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":06AE
         Left            =   6600
         List            =   "frmAplicativosCarga.frx":06B0
         Style           =   2  'Dropdown List
         TabIndex        =   75
         Top             =   2760
         Width           =   4575
      End
      Begin AT_MaskText.MaskText txtResp_Direccion 
         Height          =   300
         Left            =   6600
         TabIndex        =   61
         Top             =   1320
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   529
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
         Locked          =   -1  'True
      End
      Begin VB.ComboBox cmbINSResp_Direccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmAplicativosCarga.frx":06B2
         Left            =   6600
         List            =   "frmAplicativosCarga.frx":06B4
         Style           =   2  'Dropdown List
         TabIndex        =   72
         Top             =   1320
         Width           =   4575
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmAplicativosCarga.frx":06B6
         Height          =   540
         Index           =   5
         Left            =   120
         TabIndex        =   63
         Top             =   360
         Width           =   11295
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Gerencia"
         Height          =   180
         Index           =   18
         Left            =   240
         TabIndex        =   54
         Top             =   1860
         Width           =   675
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   17
         Left            =   6000
         TabIndex        =   53
         Top             =   1860
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Direcci�n"
         Height          =   180
         Index           =   16
         Left            =   240
         TabIndex        =   52
         Top             =   1380
         Width           =   720
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   15
         Left            =   6000
         TabIndex        =   51
         Top             =   1380
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   14
         Left            =   6000
         TabIndex        =   50
         Top             =   2820
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Resp."
         Height          =   180
         Index           =   13
         Left            =   6000
         TabIndex        =   49
         Top             =   2340
         Width           =   420
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         Height          =   180
         Index           =   12
         Left            =   240
         TabIndex        =   48
         Top             =   2820
         Width           =   450
      End
      Begin VB.Label lblAplicativos 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         Height          =   180
         Index           =   3
         Left            =   240
         TabIndex        =   47
         Top             =   2340
         Width           =   495
      End
   End
   Begin VB.Frame fraStep1 
      Caption         =   " Paso 1. Definici�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   11655
      Begin VB.CommandButton cmdAyuda 
         Height          =   375
         Left            =   11160
         Picture         =   "frmAplicativosCarga.frx":07F4
         Style           =   1  'Graphical
         TabIndex        =   67
         TabStop         =   0   'False
         ToolTipText     =   "Seleccione la ayuda para comprender la manera en que es concebido conceptualmente un proyecto IDM"
         Top             =   240
         Width           =   375
      End
      Begin VB.ComboBox cmbINSAmbiente 
         Height          =   300
         ItemData        =   "frmAplicativosCarga.frx":093E
         Left            =   240
         List            =   "frmAplicativosCarga.frx":094B
         Style           =   2  'Dropdown List
         TabIndex        =   45
         Top             =   3480
         Width           =   2535
      End
      Begin VB.TextBox Text2 
         Height          =   270
         Left            =   240
         MaxLength       =   100
         TabIndex        =   44
         Top             =   2760
         Width           =   1575
      End
      Begin VB.TextBox Text1 
         Height          =   270
         Left            =   240
         MaxLength       =   100
         TabIndex        =   43
         Top             =   2040
         Width           =   5895
      End
      Begin VB.TextBox txtAplicativoNombre 
         Height          =   270
         Left            =   2040
         MaxLength       =   30
         TabIndex        =   42
         Top             =   1320
         Width           =   4095
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "1.4. Establezca el �mbito o plataforma del aplicativo (ambiente HOST o distribu�do)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   180
         Index           =   4
         Left            =   240
         TabIndex        =   41
         Top             =   3240
         Width           =   6255
      End
      Begin VB.Label lblAuxiliares 
         AutoSize        =   -1  'True
         Caption         =   "Hasta un m�ximo de 10 caracteres alfanum�ricos"
         Height          =   180
         Index           =   3
         Left            =   2040
         TabIndex        =   40
         Top             =   2805
         Width           =   3735
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "1.3. Si lo desea, puede establecer una referencia abreviada o alias para referirse al aplicativo (en reportes, pantallas, etc.)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   180
         Index           =   3
         Left            =   240
         TabIndex        =   39
         Top             =   2520
         Width           =   9135
      End
      Begin VB.Label lblAuxiliares 
         AutoSize        =   -1  'True
         Caption         =   "Hasta un m�ximo de 100 caracteres alfanum�ricos"
         Height          =   180
         Index           =   2
         Left            =   6240
         TabIndex        =   38
         Top             =   2085
         Width           =   3840
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "1.2. A continuaci�n, escriba el nombre con el que se conoce el aplicativo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   180
         Index           =   2
         Left            =   240
         TabIndex        =   37
         Top             =   1800
         Width           =   5340
      End
      Begin VB.Label lblAuxiliares 
         AutoSize        =   -1  'True
         Caption         =   "Hasta un m�ximo de 30 caracteres alfanum�ricos"
         Height          =   180
         Index           =   1
         Left            =   6240
         TabIndex        =   36
         Top             =   1365
         Width           =   3735
      End
      Begin VB.Label lblAuxiliares 
         AutoSize        =   -1  'True
         Caption         =   "C�digo alfanum�rico"
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   35
         Top             =   1365
         Width           =   1530
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "1.1. Primero, defina un c�digo alfanum�rico para identificar de manera un�voca al aplicativo que desea registrar en el sistema"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   180
         Index           =   1
         Left            =   240
         TabIndex        =   34
         Top             =   960
         Width           =   9360
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmAplicativosCarga.frx":0969
         Height          =   495
         Index           =   0
         Left            =   240
         TabIndex        =   21
         Top             =   360
         Width           =   10695
      End
   End
End
Attribute VB_Name = "frmAplicativosCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public pModo As String

Private Sub Form_Load()
    Inicializar
End Sub

Private Sub Inicializar()
    ' Esto es independiente del MODO
    Me.Top = 0
    Me.Left = 0
    chkAppHab.BackColor = Me.BackColor
    
    Select Case pModo
        Case "ALTA"
            HabilitarWizard 1
            'fraEditMode.Visible = False
            'fraStep1.Visible = True
            'fraStep1.Visible = False
            'Inicializar_Asistente
            
        Case "MODI"
            Inicializar_Controles_Edicion
        
    End Select
    
'    Call setHabilCtrl(txtCodigo, "DIS")
'    Call setHabilCtrl(txtNombre, "DIS")
'    Call setHabilCtrl(chkAppHab, "DIS")
'    Call setHabilCtrl(cmbAppAmb, "DIS")
'
'    Call setHabilCtrl(cmbDireccion, "DIS")
'    Call setHabilCtrl(cmbGerencia, "DIS")
'    Call setHabilCtrl(cmbSector, "DIS")
'    Call setHabilCtrl(cmbGrupo, "DIS")
'
'    ' Responsables expl�citos
'    Call setHabilCtrl(cmb_Resp_Direccion, "DIS")
'    Call setHabilCtrl(cmb_Resp_Gerencia, "DIS")
'    Call setHabilCtrl(cmb_Resp_Sector, "DIS")
'    Call setHabilCtrl(cmb_Resp_Grupo, "DIS")
End Sub

Private Sub HabilitarWizard(Step As Byte)
    cmdWizard_Cancelar.Visible = True
    cmdWizard_Atras.Visible = True
    cmdWizard_Adelante.Visible = True
    cmdWizard_Terminar.Visible = True
    
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    
    Select Case Step
        Case 0
            ' Paneles
            fraEditMode.Visible = True
            fraStep1.Visible = False
            fraStep2.Visible = False
            fraStep3.Visible = False
            ' Botones
            cmdWizard_Cancelar.Visible = False
            cmdWizard_Atras.Visible = False
            cmdWizard_Adelante.Visible = False
            cmdWizard_Terminar.Visible = False
            
            cmdAceptar.Visible = True
            cmdCancelar.Visible = True
            
            cmdWizard_Cancelar.Enabled = False
            cmdWizard_Atras.Enabled = False
            cmdWizard_Adelante.Enabled = False
            cmdWizard_Terminar.Enabled = False
            
            cmdAceptar.Enabled = True
            cmdCancelar.Enabled = True
        Case 1
            ' Controles
            ' Cargo los ambientes
            With cmbAppAmb
                .Clear
                .AddItem "D: Distribu�do"
                .AddItem "H: No distribu�do"
                .AddItem "B: Ambos"
                .ListIndex = 0
            End With
            
            ' Paneles
            fraEditMode.Visible = False
            fraStep1.Visible = True
            fraStep2.Visible = False
            fraStep3.Visible = False
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = False
            cmdWizard_Adelante.Enabled = True
            cmdWizard_Terminar.Enabled = False
        Case 2
            ' Controles
            Inicializar_Controles_Alta
        
            ' Paneles
            fraEditMode.Visible = False
            fraStep1.Visible = False
            fraStep2.Visible = True
            fraStep3.Visible = False
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = True
            cmdWizard_Adelante.Enabled = True
            cmdWizard_Terminar.Enabled = False
            
            'txtCodigoNumerico.SetFocus
'        Case 3
'            ' Paneles
'            fraEditMode.Visible = False
'            fraStep1.Visible = False
'            fraStep2.Visible = False
'            fraStep3.Visible = True
'            ' Botones
'            cmdWizard_Cancelar.Enabled = True
'            cmdWizard_Atras.Enabled = True
'            cmdWizard_Adelante.Enabled = False
'            cmdWizard_Terminar.Enabled = True
'
'            Carga_Resumen
'
'            cmdWizard_Terminar.SetFocus
        Case 4
            cmdAceptar_Click
    End Select
End Sub

Private Sub Inicializar_Controles_Edicion()
    Call setHabilCtrl(txtCodigo, "OBL")
    Call setHabilCtrl(txtNombre, "NOR")
    Call setHabilCtrl(chkAppHab, "NOR")
    Call setHabilCtrl(cmbAppAmb, "NOR")

    Call setHabilCtrl(cmbDireccion, "NOR")
    Call setHabilCtrl(cmbGerencia, "NOR")
    Call setHabilCtrl(cmbSector, "NOR")
    Call setHabilCtrl(cmbGrupo, "NOR")
    
'    Call setHabilCtrl(txt_Resp_Direccion, "DIS")
'    Call setHabilCtrl(txt_Resp_Gerencia, "DIS")
'    Call setHabilCtrl(txt_Resp_Sector, "DIS")
'    Call setHabilCtrl(txt_Resp_Grupo, "DIS")

    ' Cargo los ambientes
    With cmbAppAmb
        .Clear
        .AddItem "D: Distribu�do"
        .AddItem "H: No distribu�do"
        .AddItem "B: Ambos"
        .ListIndex = 0
    End With
    
    ' Cargo las direcciones
    With cmbDireccion
        .Clear
        If sp_GetDireccion(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_direccion & ": " & ClearNull(aplRST.Fields!nom_direccion) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_direccion)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbDireccion, "DIS")
        End If
        If sp_GetResponsableAreaFinal("DIRE", CodigoCombo(cmbDireccion, True)) Then
            txt_Resp_Direccion = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
    ' Cargo las gerencias
    With cmbGerencia
        .Clear
        If sp_GetGerencia(Null, CodigoCombo(cmbDireccion, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_gerencia & ": " & ClearNull(aplRST.Fields!nom_gerencia) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_gerencia)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbGerencia, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GERE", CodigoCombo(cmbGerencia, True)) Then
            txt_Resp_Gerencia = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
    ' Cargo los sectores
    With cmbSector
        .Clear
        If sp_GetSector(Null, CodigoCombo(cmbGerencia, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_sector) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_sector)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbSector, "DIS")
        End If
        If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbSector, True)) Then
            txt_Resp_Sector = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
    ' Cargo los grupos
    With cmbGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbGrupo, True)) Then
            txt_Resp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
End Sub

Private Sub Inicializar_Controles_Alta()
    Call setHabilCtrl(cmbINSDireccion, "NOR")
    Call setHabilCtrl(cmbINSGerencia, "NOR")
    Call setHabilCtrl(cmbINSSector, "NOR")
    Call setHabilCtrl(cmbINSGrupo, "NOR")
    
    Call setHabilCtrl(txt_Resp_Direccion, "DIS")
    Call setHabilCtrl(txt_Resp_Gerencia, "DIS")
    Call setHabilCtrl(txt_Resp_Sector, "DIS")
    Call setHabilCtrl(txt_Resp_Grupo, "DIS")

    ' Cargo las direcciones
    With cmbINSDireccion
        .Clear
        If sp_GetDireccion(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_direccion & ": " & ClearNull(aplRST.Fields!nom_direccion) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_direccion)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSDireccion, "DIS")
        End If
        If sp_GetResponsableAreaFinal("DIRE", CodigoCombo(cmbINSDireccion, True)) Then
            txt_Resp_Direccion = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
    ' Cargo las gerencias
    With cmbINSGerencia
        .Clear
        If sp_GetGerencia(Null, CodigoCombo(cmbINSDireccion, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_gerencia & ": " & ClearNull(aplRST.Fields!nom_gerencia) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_gerencia)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSGerencia, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GERE", CodigoCombo(cmbINSGerencia, True)) Then
            txt_Resp_Gerencia = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
    ' Cargo los sectores
    With cmbINSSector
        .Clear
        If sp_GetSector(Null, CodigoCombo(cmbINSGerencia, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_sector) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_sector)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSSector, "DIS")
        End If
        If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbINSSector, True)) Then
            txt_Resp_Sector = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
    ' Cargo los grupos
    With cmbINSGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbINSSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbINSGrupo, True)) Then
            txt_Resp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        End If
    End With
End Sub

Private Sub cmbDireccion_Click()
    Actualizar_Direccion
End Sub

Private Sub cmbGerencia_Click()
    Actualizar_Gerencia
End Sub

Private Sub cmbSector_Click()
    Actualizar_Sector
End Sub

Private Sub cmbGrupo_Click()
    Actualizar_Grupo
End Sub

Private Sub cmbINSDireccion_Click()
    Actualizar_INS_Direccion
End Sub

Private Sub cmbINSGerencia_Click()
    Actualizar_INS_Gerencia
End Sub

Private Sub cmbINSSector_Click()
    Actualizar_INS_Sector
End Sub

Private Sub cmbINSGrupo_Click()
    Actualizar_INS_Grupo
End Sub

Private Sub Actualizar_Direccion()
    Call setHabilCtrl(cmbGerencia, "NOR")
    Call setHabilCtrl(cmbSector, "NOR")
    Call setHabilCtrl(cmbGrupo, "NOR")
    ' Actualiza responsable de Direcci�n
    If sp_GetResponsableAreaFinal("DIRE", CodigoCombo(cmbDireccion, True)) Then
        txt_Resp_Direccion = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txt_Resp_Direccion = "***"
    End If
    ' Cargo las gerencias
    With cmbGerencia
        .Clear
        If sp_GetGerencia(Null, CodigoCombo(cmbDireccion, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_gerencia & ": " & ClearNull(aplRST.Fields!nom_gerencia) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_gerencia)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbGerencia, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GERE", CodigoCombo(cmbGerencia, True)) Then
            txt_Resp_Gerencia = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txt_Resp_Gerencia = "***"
        End If
    End With
    ' Cargo los sectores
    With cmbSector
        .Clear
        If sp_GetSector(Null, CodigoCombo(cmbGerencia, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_sector) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_sector)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbSector, "DIS")
        End If
        If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbSector, True)) Then
            txt_Resp_Sector = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txt_Resp_Sector = "***"
        End If
    End With
    ' Cargo los grupos
    With cmbGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbGrupo, True)) Then
            txt_Resp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txt_Resp_Grupo = "***"
        End If
    End With
End Sub

Private Sub Actualizar_Gerencia()
    Call setHabilCtrl(cmbSector, "NOR")
    Call setHabilCtrl(cmbGrupo, "NOR")
    If sp_GetResponsableAreaFinal("GERE", CodigoCombo(cmbGerencia, True)) Then
        txt_Resp_Gerencia = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txt_Resp_Gerencia = "***"
    End If
    ' Cargo los sectores
    With cmbSector
        .Clear
        If sp_GetSector(Null, CodigoCombo(cmbGerencia, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_sector) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_sector)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbSector, "DIS")
        End If
        If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbSector, True)) Then
            txt_Resp_Sector = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txt_Resp_Sector = "***"
        End If
    End With
    ' Cargo los grupos
    With cmbGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbGrupo, True)) Then
            txt_Resp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txt_Resp_Grupo = "***"
        End If
    End With
End Sub

Private Sub Actualizar_Sector()
    Call setHabilCtrl(cmbGrupo, "NOR")
    If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbSector, True)) Then
        txt_Resp_Sector = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txt_Resp_Sector = "***"
    End If
    ' Cargo los grupos
    With cmbGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbGrupo, True)) Then
            txt_Resp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txt_Resp_Grupo = "***"
        End If
    End With
End Sub

Private Sub Actualizar_Grupo()
    If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbGrupo, True)) Then
        txt_Resp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txt_Resp_Grupo = "***"
    End If
End Sub

Private Sub Actualizar_INS_Direccion()
    Call setHabilCtrl(cmbINSGerencia, "NOR")
    Call setHabilCtrl(cmbINSSector, "NOR")
    Call setHabilCtrl(cmbINSGrupo, "NOR")
    ' Actualiza responsable de Direcci�n
    If sp_GetResponsableAreaFinal("DIRE", CodigoCombo(cmbINSDireccion, True)) Then
        txtResp_Direccion = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txtResp_Direccion = "***"
    End If
    ' Cargo las gerencias
    With cmbINSGerencia
        .Clear
        If sp_GetGerencia(Null, CodigoCombo(cmbINSDireccion, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_gerencia & ": " & ClearNull(aplRST.Fields!nom_gerencia) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_gerencia)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSGerencia, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GERE", CodigoCombo(cmbINSGerencia, True)) Then
            txtResp_Gerencia = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txtResp_Gerencia = "***"
        End If
    End With
    ' Cargo los sectores
    With cmbINSSector
        .Clear
        If sp_GetSector(Null, CodigoCombo(cmbINSGerencia, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_sector) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_sector)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSSector, "DIS")
        End If
        If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbINSSector, True)) Then
            txtResp_Sector = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txtResp_Sector = "***"
        End If
    End With
    ' Cargo los grupos
    With cmbINSGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbINSSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbINSGrupo, True)) Then
            txtResp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txtResp_Grupo = "***"
        End If
    End With
End Sub

Private Sub Actualizar_INS_Gerencia()
    Call setHabilCtrl(cmbINSSector, "NOR")
    Call setHabilCtrl(cmbINSGrupo, "NOR")
    If sp_GetResponsableAreaFinal("GERE", CodigoCombo(cmbINSGerencia, True)) Then
        txtResp_Gerencia = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txtResp_Gerencia = "***"
    End If
    ' Cargo los sectores
    With cmbINSSector
        .Clear
        If sp_GetSector(Null, CodigoCombo(cmbINSGerencia, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_sector) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_sector)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSSector, "DIS")
        End If
        If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbINSSector, True)) Then
            txtResp_Sector = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txtResp_Sector = "***"
        End If
    End With
    ' Cargo los grupos
    With cmbINSGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbINSSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbINSGrupo, True)) Then
            txtResp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txtResp_Grupo = "***"
        End If
    End With
End Sub

Private Sub Actualizar_INS_Sector()
    Call setHabilCtrl(cmbINSGrupo, "NOR")
    If sp_GetResponsableAreaFinal("SECT", CodigoCombo(cmbINSSector, True)) Then
        txtResp_Sector = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txtResp_Sector = "***"
    End If
    ' Cargo los grupos
    With cmbINSGrupo
        .Clear
        If sp_GetGrupo(Null, CodigoCombo(cmbINSSector, True), Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST.Fields!cod_grupo & ": " & ClearNull(aplRST.Fields!nom_grupo) & Space(200) & "||" & ClearNull(aplRST.Fields!cod_grupo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If .ListCount > 0 Then
            .ListIndex = 0
        Else
            Call setHabilCtrl(cmbINSGrupo, "DIS")
        End If
        If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbINSGrupo, True)) Then
            txtResp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
        Else
            txtResp_Grupo = "***"
        End If
    End With
End Sub

Private Sub Actualizar_INS_Grupo()
    If sp_GetResponsableAreaFinal("GRUP", CodigoCombo(cmbINSGrupo, True)) Then
        txtResp_Grupo = ClearNull(aplRST.Fields!nom_recurso)
    Else
        txtResp_Grupo = "***"
    End If
End Sub

Private Sub cmdAceptar_Click()
    'If Validar Then
        If MsgBox("�Confirma el agregado del nuevo aplicativo?", vbQuestion + vbYesNo, "Nuevo aplicativo") = vbYes Then
            'If Not sp_InsertAplicativo(txtCodigo, txtNombre, IIf(chkAppHab.Value = 1, "S", "N"), CodigoCombo(cmbDireccion, True), CodigoCombo(cmbGerencia, True), CodigoCombo(cmbSector, True), CodigoCombo(cmbGrupo, True), txt_Resp_Direccion, txt_Resp_Gerencia, txt_Resp_Sector, txt_Resp_Grupo, CodigoCombo(cmbAppAmb, False)) Then
            If Not sp_InsertAplicativo(txtAplicativoNombre, txtNombre, IIf(chkAppHab.Value = 1, "S", "N"), CodigoCombo(cmbDireccion, True), CodigoCombo(cmbGerencia, True), CodigoCombo(cmbSector, True), CodigoCombo(cmbGrupo, True), txt_Resp_Direccion, txt_Resp_Gerencia, txt_Resp_Sector, txt_Resp_Grupo, CodigoCombo(cmbAppAmb, False)) Then
                MsgBox "Error"
            Else
                Unload Me
            End If
        End If
    'End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdWizard_Adelante_Click()
    Select Case StepId
        Case 1
            If ValidarPaso(1) Then
                HabilitarWizard 2
            End If
        Case 2
            If ValidarPaso(2) Then
                HabilitarWizard 3
            End If
        Case 3
            If ValidarPaso(3) Then
                HabilitarWizard 4
            End If
    End Select
End Sub

Private Sub cmdWizard_Atras_Click()
    Select Case StepId
        Case 2
            HabilitarWizard 1
        Case 3
            HabilitarWizard 2
        Case 4
            HabilitarWizard 3
    End Select
End Sub

Private Function StepId() As Byte
    ' Esta funci�n devuelve el nro. de paso en el que se encuentra el usuario
    If fraStep1.Visible Then
        StepId = 1
    Else
        If fraStep2.Visible Then
            StepId = 2
        Else
            StepId = 3
        End If
    End If
End Function

Private Function ValidarPaso(Step As Byte) As Boolean
    ValidarPaso = False
    Select Case Step
        Case 1
            ' Tipo de proyecto seleccionado
'            If CodigoCombo(cmbTipoProyecto, True) <> "NULL" Then
'                ValidarPaso = True
'            Else
'                MsgBox "Debe seleccionar un tipo de elemento v�lido para crear.", vbExclamation + vbOKOnly, "Tipo de elemento"
'            End If
        Case 2
            ' Nombre para el proyecto
'            If IsNumeric(txtCodigoNumerico) Then
'                If Len(txtNombre) > 0 Then
'                    If CodigoCombo(cmbCategoria, True) <> "" Then
'                        If CodigoCombo(cmbClase, True) <> "" Then
'                            ValidarPaso = True
'                        Else
'                            MsgBox "Debe seleccionar una clase v�lida para el elemento.", vbExclamation + vbOKOnly, "Clase"
'                        End If
'                    Else
'                        MsgBox "Debe seleccionar una categor�a v�lida para el elemento.", vbExclamation + vbOKOnly, "Categor�a"
'                    End If
'                Else
'                    MsgBox "Debe indicar un nombre descriptivo para el elemento.", vbExclamation + vbOKOnly, "Nombre"
'                End If
'            Else
'                MsgBox "Debe indicar un c�digo num�rico para el elemento.", vbExclamation + vbOKOnly, "C�digo num�rico"
'            End If
        Case 3
            ValidarPaso = True
    End Select
    ValidarPaso = True
End Function

Private Sub chkINSResp_Direccion_Click()
    If chkINSResp_Direccion.Value = 1 Then
        txtResp_Direccion.Visible = False
        cmbINSResp_Direccion.Visible = True
    Else
        txtResp_Direccion.Visible = True
        cmbINSResp_Direccion.Visible = False
    End If
End Sub

Private Sub chkINSResp_Gerencia_Click()
    If chkINSResp_Gerencia.Value = 1 Then
        txtResp_Gerencia.Visible = False
        cmbINSResp_Gerencia.Visible = True
    Else
        txtResp_Gerencia.Visible = True
        cmbINSResp_Gerencia.Visible = False
    End If
End Sub

Private Sub chkINSResp_Sector_Click()
    If chkINSResp_Sector.Value = 1 Then
        txtResp_Sector.Visible = False
        cmbINSResp_Sector.Visible = True
    Else
        txtResp_Sector.Visible = True
        cmbINSResp_Sector.Visible = False
    End If
End Sub

Private Sub chkINSResp_Grupo_Click()
    If chkINSResp_Grupo.Value = 1 Then
        txtResp_Grupo.Visible = False
        cmbINSResp_Grupo.Visible = True
    Else
        txtResp_Grupo.Visible = True
        cmbINSResp_Grupo.Visible = False
    End If
End Sub

