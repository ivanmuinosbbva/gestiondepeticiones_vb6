VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmMensajesEvento 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "MENSAJES POR EVENTO"
   ClientHeight    =   6195
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6195
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2940
      Left            =   0
      TabIndex        =   6
      Top             =   3240
      Width           =   8205
      Begin VB.TextBox txtExtend 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   930
         Left            =   5400
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   27
         Top             =   1860
         Width           =   2715
      End
      Begin VB.ComboBox cboFecVto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3300
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   2460
         Width           =   645
      End
      Begin VB.ComboBox cboActivo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4680
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   2460
         Width           =   645
      End
      Begin VB.TextBox txtEstCodigo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   930
         Left            =   5400
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   20
         Top             =   540
         Width           =   2715
      End
      Begin VB.ComboBox cboEstRecept 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmMensajesEvento.frx":0000
         Left            =   900
         List            =   "frmMensajesEvento.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   2100
         Width           =   4425
      End
      Begin VB.ComboBox cboTexto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1740
         Width           =   4425
      End
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1020
         Width           =   4425
      End
      Begin VB.ComboBox cboPerfil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1380
         Width           =   4425
      End
      Begin VB.ComboBox cboAccion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   660
         Width           =   4425
      End
      Begin VB.ComboBox cboAlcance 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   900
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   300
         Width           =   4425
      End
      Begin VB.Label Label10 
         Caption         =   "Texto Adicional"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6060
         TabIndex        =   28
         Top             =   1620
         Width           =   1425
      End
      Begin VB.Label Label9 
         Caption         =   "Incluye Fecha Vto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1620
         TabIndex        =   26
         Top             =   2535
         Width           =   1605
      End
      Begin VB.Label Label8 
         Caption         =   "Activo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4020
         TabIndex        =   23
         Top             =   2535
         Width           =   585
      End
      Begin VB.Label Label7 
         Caption         =   "Estados Receptor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6120
         TabIndex        =   21
         Top             =   300
         Width           =   1545
      End
      Begin VB.Label Label6 
         Caption         =   "Receptor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   2168
         Width           =   795
      End
      Begin VB.Label Label5 
         Caption         =   "Texto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   1808
         Width           =   795
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   30
         Visible         =   0   'False
         Width           =   105
      End
      Begin VB.Label Label2 
         Caption         =   "Perfil"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   1448
         Width           =   795
      End
      Begin VB.Label Label1 
         Caption         =   "Acci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   728
         Width           =   795
      End
      Begin VB.Label Label4 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1088
         Width           =   795
      End
      Begin VB.Label Label3 
         Caption         =   "Alcance"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   368
         Width           =   795
      End
   End
   Begin VB.Frame pnlBotones 
      Height          =   6195
      Left            =   8220
      TabIndex        =   1
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Height          =   500
         Left            =   60
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector"
         Top             =   4080
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   5610
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Sector seleccionado"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector"
         Top             =   3570
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3150
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   5556
      _Version        =   393216
      Cols            =   10
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmMensajesEvento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. - FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. - FJS 02.10.2007 - Para todos los combos se agrega la propiedad ToolTipsText con el valor actual para que pueda visualizarse la selecci�n completa.
' -003- b. - FJS 05.10.2007 - Se agregan los eventos para cambiar el estado de los grupos y sectores. Adem�s se agrega en el combobox de Destinatarios al BP.

Option Explicit

Const colCODALCANCE = 0
Const colCODACCION = 1
Const colCODESTADO = 2
Const colCODPERFIL = 3
Const colCODMSG = 4
Const colESTRECEPT = 5
Const colESTCODIGO = 6
Const colFLGACTIVO = 7
Const colFLGFECVTO = 8
Const colTXTEXTEND = 9

Dim sOpcionSeleccionada As String
Dim bCargaInicial As Boolean        ' add -003- b.

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub
Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub
Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub
'Private Sub cmdConfirmar_Click()
'    Select Case sOpcionSeleccionada
'        Case "A"
'            If CamposObligatorios Then
'                If sp_InsMensajesEvento(CodigoCombo(Me.cboAlcance), CodigoCombo(Me.cboAccion), CodigoCombo(Me.cboEstado), CodigoCombo(Me.cboPerfil), CodigoCombo(Me.cboTexto), CodigoCombo(Me.cboEstRecept), txtEstCodigo.Text, txtExtend.Text, CodigoCombo(Me.cboFecVto), CodigoCombo(Me.cboActivo)) Then
'                    Call HabilitarBotones(0)
'                End If
'            End If
'        Case "M"
'            If CamposObligatorios Then
'                If sp_UpdMensajesEvento(CodigoCombo(Me.cboAlcance), CodigoCombo(Me.cboAccion), CodigoCombo(Me.cboEstado), CodigoCombo(Me.cboPerfil), CodigoCombo(Me.cboTexto), CodigoCombo(Me.cboEstRecept), txtEstCodigo.Text, txtExtend.Text, CodigoCombo(Me.cboFecVto), CodigoCombo(Me.cboActivo)) Then
'                    Call HabilitarBotones(0)
'                End If
'            End If
'        Case "E"
'            If sp_DelMensajesEvento(CodigoCombo(Me.cboAlcance), CodigoCombo(Me.cboAccion), CodigoCombo(Me.cboEstado), CodigoCombo(Me.cboPerfil)) Then
'                Call HabilitarBotones(0)
'            End If
'    End Select
'End Sub
Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "E", "M"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub
Private Sub Form_Load()
    Call InicializarCombos
    bCargaInicial = True    ' add -003- b.
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    bCargaInicial = False    ' add -003- b.
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    Call LimpiarForm(Me)
    Call HabilitarBotones(0)
End Sub
Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.Visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            'grdDatos.SetFocus
            cmdAgregar.Enabled = True
            cmdEliminar.Enabled = True
            cmdModificar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                If Not bCargaInicial Then Call MostrarSeleccion
                'Call MostrarSeleccion  ' del -003- b.
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdEliminar.Enabled = False
            cmdModificar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.Visible = True
                    cboAlcance.Enabled = True
                    cboEstado.Enabled = True
                    cboAccion.Enabled = True
                    cboPerfil.Enabled = True
                    cboAlcance.ListIndex = -1
                    cboEstado.ListIndex = -1
                    cboAccion.ListIndex = -1
                    cboPerfil.ListIndex = -1
                    cboTexto.ListIndex = -1
                    cboEstRecept.ListIndex = -1
                    cboActivo.ListIndex = 0
                    cboFecVto.ListIndex = 0
                    txtEstCodigo.Text = ""
                    fraDatos.Enabled = True
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.Visible = True
                    cboAlcance.Enabled = False
                    cboEstado.Enabled = False
                    cboAccion.Enabled = False
                    cboPerfil.Enabled = False
                    fraDatos.Enabled = True
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub
Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If cboAccion.ListIndex < 0 Then
        MsgBox ("Debe seleccionar una Accion")
        CamposObligatorios = False
        Exit Function
    End If
    If Me.cboEstRecept.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un Receptor")
        CamposObligatorios = False
        Exit Function
    End If
    If Me.cboTexto.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un Mensaje")
        CamposObligatorios = False
        Exit Function
    End If
    If cboPerfil.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un Perfil")
        CamposObligatorios = False
        Exit Function
    End If
End Function
Sub InicializarCombos()
    If sp_GetEstado(Null) Then
        Do While Not aplRST.EOF
            cboEstado.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboEstado.AddItem "NULL" & " : Sin controlar estado"
    If sp_GetPerfil(Null, Null) Then
        Do While Not aplRST.EOF
            cboPerfil.AddItem aplRST(0) & " : " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    'If Not sp_GetMensajesTexto(Null) Then Exit Sub     ' Volver a habilitar
    Do While Not aplRST.EOF
        With grdDatos
            cboTexto.AddItem aplRST(0) & " : " & Trim(aplRST(1))
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    
    cboAlcance.AddItem "PET" & " : Petici�n"
    cboAlcance.AddItem "SEC" & " : Sector"
    cboAlcance.AddItem "GRU" & " : Grupo"
    
    cboEstRecept.AddItem "PET" & " : Petici�n"
    cboEstRecept.AddItem "SEC" & " : Sector"
    cboEstRecept.AddItem "GRU" & " : Grupo"
    cboEstRecept.AddItem "BPA" & " : Business Partner"      ' add
    
    cboActivo.AddItem "S" & " : S"
    cboActivo.AddItem "N" & " : N"
   
    cboFecVto.AddItem "S" & " : S"
    cboFecVto.AddItem "N" & " : N"
   
   
    cboAccion.AddItem "ULTMPET" & " : Ultim�tum Petici�n"
    cboAccion.AddItem "ESC1PET" & " : Escalamiento Petici�n 1ra Instancia"
    cboAccion.AddItem "ESC2PET" & " : Escalamiento Petici�n 2da Instancia"
    cboAccion.AddItem "ULTMSEC" & " : Ultim�tum Sector"
    cboAccion.AddItem "ESC1SEC" & " : Escalamiento Sector 1ra Instancia"
    cboAccion.AddItem "ESC2SEC" & " : Escalamiento Sector 2da Instancia"
    cboAccion.AddItem "ULTMGRU" & " : Ultim�tum Grupo"
    cboAccion.AddItem "ESC1GRU" & " : Escalamiento Grupo 1ra Instancia"
    'cboAccion.AddItem "ESC2GRU" & " : xxxEscalamiento Grupo 1ra Instancia"
    '{ add -003- b.
    cboAccion.AddItem "REVIGRU" & " : Cambio el Grupo a revisar"
    cboAccion.AddItem "REVISEC" & " : Cambio el Sector a revisar"
    cboAccion.AddItem "CANCGRU" & " : Se cancela el Grupo"
    cboAccion.AddItem "CANCSEC" & " : Se cancela el Sector"
    '}
    
    cboAccion.AddItem "PCHGEST" & " : Cambio estado Petici�n"
    cboAccion.AddItem "PCHGTXT" & " : Modificaci�n Alcance Petici�n"
    cboAccion.AddItem "PCHGFEC" & " : Cambio Fechas Petici�n"
    cboAccion.AddItem "PNEW003" & " : Alta Petici�n propia"
    cboAccion.AddItem "PNEW004" & " : Alta Proyecto"
    cboAccion.AddItem "PNEW005" & " : Alta Petici�n Especial"
    cboAccion.AddItem "GNEW000" & " : Asignar Grupo"
    cboAccion.AddItem "SNEW000" & " : Asignar Sector"
End Sub

Sub CargarGrid()
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colCODALCANCE) = "ALCANCE"
        .TextMatrix(0, colCODACCION) = "ACCI�N"
        .TextMatrix(0, colCODESTADO) = "ESTADO"
        .TextMatrix(0, colCODPERFIL) = "PERFIL"
        .TextMatrix(0, colCODMSG) = "MENSAJE"
        .TextMatrix(0, colESTRECEPT) = "ESTREC"
        .TextMatrix(0, colESTCODIGO) = "ESTCOD"
        .TextMatrix(0, colFLGACTIVO) = "ACTIVO"
        .TextMatrix(0, colFLGFECVTO) = "F.VTO"
        .TextMatrix(0, colTXTEXTEND) = "TEXTO"
        .ColWidth(colCODALCANCE) = 1000
        .ColWidth(colCODACCION) = 1000
        .ColWidth(colCODESTADO) = 1000
        .ColWidth(colCODPERFIL) = 1000
        .ColWidth(colCODMSG) = 1000
        .ColWidth(colESTRECEPT) = 1000
        .ColWidth(colESTCODIGO) = 5000
        .ColWidth(colFLGACTIVO) = 600
        .ColWidth(colFLGFECVTO) = 600
    End With
    
    'If Not sp_GetMensajesEvento(Null, Null, Null, Null) Then Exit Sub  ' Volver a habilitar
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODALCANCE) = ClearNull(aplRST.Fields.Item("evt_alcance"))
            .TextMatrix(.Rows - 1, colCODACCION) = ClearNull(aplRST.Fields.Item("cod_accion"))
            .TextMatrix(.Rows - 1, colCODESTADO) = ClearNull(aplRST.Fields.Item("cod_estado"))
            .TextMatrix(.Rows - 1, colCODPERFIL) = ClearNull(aplRST.Fields.Item("cod_perfil"))
            .TextMatrix(.Rows - 1, colCODMSG) = ClearNull(aplRST.Fields.Item("cod_txtmsg"))
            .TextMatrix(.Rows - 1, colESTRECEPT) = ClearNull(aplRST.Fields.Item("est_recept"))
            .TextMatrix(.Rows - 1, colESTCODIGO) = ClearNull(aplRST.Fields.Item("est_codigo"))
            .TextMatrix(.Rows - 1, colFLGACTIVO) = ClearNull(aplRST.Fields.Item("flg_activo"))
            .TextMatrix(.Rows - 1, colFLGFECVTO) = IIf(ClearNull(aplRST!txt_pzofin) = "S", "S", "N")
            .TextMatrix(.Rows - 1, colTXTEXTEND) = ClearNull(aplRST.Fields.Item("txt_extend"))
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    aplRST.Close
    '{ add -002- a.
    Dim i As Long
    
    With grdDatos
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .FocusRect = flexFocusNone
    End With
    '}
    Call MostrarSeleccion
End Sub
Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 0) <> "" Then
                cboEstado.ListIndex = PosicionCombo(Me.cboEstado, .TextMatrix(.RowSel, colCODESTADO))
                cboAlcance.ListIndex = PosicionCombo(Me.cboAlcance, .TextMatrix(.RowSel, colCODALCANCE))
                cboAccion.ListIndex = PosicionCombo(Me.cboAccion, .TextMatrix(.RowSel, colCODACCION))
                cboPerfil.ListIndex = PosicionCombo(Me.cboPerfil, .TextMatrix(.RowSel, colCODPERFIL))
                cboTexto.ListIndex = PosicionCombo(Me.cboTexto, .TextMatrix(.RowSel, colCODMSG))
                cboEstRecept.ListIndex = PosicionCombo(Me.cboEstRecept, .TextMatrix(.RowSel, colESTRECEPT))
                txtEstCodigo.Text = ClearNull(.TextMatrix(.RowSel, colESTCODIGO))
                txtExtend.Text = ClearNull(.TextMatrix(.RowSel, colTXTEXTEND))
                cboActivo.ListIndex = PosicionCombo(Me.cboActivo, .TextMatrix(.RowSel, colFLGACTIVO))
                cboFecVto.ListIndex = PosicionCombo(Me.cboFecVto, .TextMatrix(.RowSel, colFLGFECVTO))
            End If
        End If
    End With
End Sub
Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub
Private Sub grdDatos_RowColChange()
    If Not bCargaInicial Then Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

'{ add -003- a.
Private Sub cboAlcance_Click()
    With cboAlcance
        .ToolTipText = .List(.ListIndex)
    End With
End Sub

Private Sub cboAccion_Click()
    With cboAccion
        .ToolTipText = .List(.ListIndex)
    End With
End Sub

Private Sub cboEstado_Click()
    With cboEstado
        .ToolTipText = .List(.ListIndex)
    End With
End Sub

Private Sub cboPerfil_Click()
    With cboPerfil
        .ToolTipText = .List(.ListIndex)
    End With
End Sub

Private Sub cboTexto_Click()
    With cboTexto
        .ToolTipText = .List(.ListIndex)
    End With
End Sub

Private Sub cboEstRecept_Click()
    With cboEstRecept
        .ToolTipText = .List(.ListIndex)
    End With
End Sub
'}
