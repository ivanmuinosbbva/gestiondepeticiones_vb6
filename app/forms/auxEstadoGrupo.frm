VERSION 5.00
Begin VB.Form auxEstadoGrupo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Estado del grupo"
   ClientHeight    =   4260
   ClientLeft      =   3345
   ClientTop       =   3765
   ClientWidth     =   4650
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "auxEstadoGrupo.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4260
   ScaleWidth      =   4650
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraModoSolic 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3105
      Left            =   60
      TabIndex        =   1
      Top             =   540
      Width           =   4545
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "A Planificar"
         Height          =   225
         Index           =   3
         Left            =   120
         TabIndex        =   7
         Top             =   1122
         Width           =   3615
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "A Estimar Esfuerzo"
         Height          =   225
         Index           =   2
         Left            =   120
         TabIndex        =   6
         Top             =   828
         Width           =   3615
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "En Evaluaci�n"
         Height          =   225
         Index           =   1
         Left            =   120
         TabIndex        =   5
         Top             =   534
         Width           =   3615
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "En Opini�n"
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   3615
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Planificado (adopta fechas del sector)"
         Height          =   225
         Index           =   4
         Left            =   120
         TabIndex        =   3
         Top             =   1416
         Width           =   3615
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "En Ejecuci�n  (adopta fechas del sector)"
         Height          =   225
         Index           =   5
         Left            =   120
         TabIndex        =   2
         Top             =   1710
         Width           =   3615
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   615
      Left            =   60
      TabIndex        =   8
      Top             =   3600
      Width           =   4545
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   3600
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   2640
         TabIndex        =   9
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Seleccione el estado para el grupo a vincular"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   3225
   End
End
Attribute VB_Name = "auxEstadoGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. - FJS 26.06.2008 - Se agrega una variable global del form para saber la gerencia de la petici�n
'                             (importa saber si es DyD para inhabilitar el agregado de grupos de DyD en estado
'                             "En ejecuci�n", por la imposibilidad de forzar la herencia del dato de fecha prevista de pasaje a Producci�n).

Option Explicit

Public bAceptar As Boolean
Public pubEstado As String
Public iniEstado As String
Public cPetGerencia As String       ' add -002- a.

Private Sub Form_Load()
    fraModoSolic.Enabled = True
    bAceptar = False
    pubEstado = ""
    setOption
End Sub

Private Sub cmdAceptar_Click()
    pubEstado = getEstadoSolic
    bAceptar = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    bAceptar = False
    Me.Hide
End Sub

Public Sub setOption()
    Me.OptModoSolic(0).Enabled = False
    Me.OptModoSolic(1).Enabled = False
    Me.OptModoSolic(2).Enabled = False
    Me.OptModoSolic(3).Enabled = False
    Me.OptModoSolic(4).Enabled = False
    Me.OptModoSolic(5).Enabled = False
    
    'If Me.iniEstado = "COMITE" Then
    If InStr(1, "COMITE|REFBPE|", Me.iniEstado, vbTextCompare) > 0 Then
        Me.OptModoSolic(0).Enabled = True
        Me.OptModoSolic(1).Enabled = True
        Me.OptModoSolic(0).Value = True
    ElseIf Me.iniEstado = "EJECUC" Then
        Me.OptModoSolic(3).Enabled = True
        Me.OptModoSolic(3).Value = True     ' add -002- a.
        Me.OptModoSolic(4).Enabled = True
        '{ add -002- a.
        If cPetGerencia = "DESA" Then
            Me.OptModoSolic(5).Enabled = False
            Me.OptModoSolic(5).Value = False
        Else
            Me.OptModoSolic(5).Enabled = True
            Me.OptModoSolic(5).Value = True
        End If
        '}
        '{ del -002- a.
        'Me.OptModoSolic(5).Enabled = True
        'Me.OptModoSolic(5).Value = True
        '}
    ElseIf Me.iniEstado = "PLANOK" Then
        Me.OptModoSolic(3).Enabled = True
        Me.OptModoSolic(4).Enabled = True
        Me.OptModoSolic(4).Value = True
    Else
        Me.OptModoSolic(2).Enabled = True
        Me.OptModoSolic(3).Enabled = True
        Me.OptModoSolic(2).Value = True
    End If
End Sub

Function getEstadoSolic()
    getEstadoSolic = ""
    If OptModoSolic(0).Value = True Then
        getEstadoSolic = "OPINIO"
    End If
    If OptModoSolic(1).Value = True Then
        getEstadoSolic = "EVALUA"
    End If
    If OptModoSolic(2).Value = True Then
        getEstadoSolic = "ESTIMA"
    End If
    If OptModoSolic(3).Value = True Then
        getEstadoSolic = "PLANIF"
    End If
    If OptModoSolic(4).Value = True Then
        getEstadoSolic = "PLANOK"
    End If
    If OptModoSolic(5).Value = True Then
        getEstadoSolic = "EJECUC"
    End If
End Function

