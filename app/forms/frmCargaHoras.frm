VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCargaHoras 
   AutoRedraw      =   -1  'True
   Caption         =   "Carga de horas trabajadas por petici�n y/o tarea"
   ClientHeight    =   8250
   ClientLeft      =   2340
   ClientTop       =   3780
   ClientWidth     =   19065
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCargaHoras.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8250
   ScaleWidth      =   19065
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3075
      Left            =   45
      TabIndex        =   13
      Top             =   5130
      Width           =   13305
      Begin VB.ComboBox cboHorasTipo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1155
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   2040
         Width           =   2205
      End
      Begin MSComctlLib.ImageList imgBotonera 
         Left            =   12600
         Top             =   900
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   9
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":058A
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":0B24
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":10BE
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":1658
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":1BF2
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":218C
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":2726
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":2CC0
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCargaHoras.frx":325A
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.ComboBox cboTarea 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2000
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1275
         Width           =   7245
      End
      Begin VB.ComboBox cboPeticion 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCargaHoras.frx":37F4
         Left            =   2000
         List            =   "frmCargaHoras.frx":37F6
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   900
         Width           =   7245
      End
      Begin VB.CommandButton cmdMasTareas 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9255
         Picture         =   "frmCargaHoras.frx":37F8
         Style           =   1  'Graphical
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar otra Tarea que no figura en la lista"
         Top             =   1275
         Width           =   315
      End
      Begin VB.CommandButton cmdMasPeticiones 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9255
         Picture         =   "frmCargaHoras.frx":3D82
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar otra Peticion que no figura en la lista"
         Top             =   900
         Width           =   315
      End
      Begin VB.TextBox txtObservaciones 
         Height          =   525
         Left            =   1150
         Locked          =   -1  'True
         MaxLength       =   250
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   33
         Top             =   2400
         Width           =   8085
      End
      Begin VB.TextBox txtMinutos 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4320
         MaxLength       =   2
         TabIndex        =   31
         Text            =   "99"
         Top             =   2048
         Width           =   375
      End
      Begin VB.TextBox txtHoras 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3675
         MaxLength       =   3
         TabIndex        =   29
         Text            =   "999"
         Top             =   2048
         Width           =   495
      End
      Begin AT_MaskText.MaskText txtCodtarea 
         Height          =   315
         Left            =   1155
         TabIndex        =   18
         Top             =   1275
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPetnroasig 
         Height          =   315
         Left            =   1155
         TabIndex        =   14
         Top             =   900
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaDesde 
         Height          =   315
         Left            =   1155
         TabIndex        =   24
         Top             =   1650
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaHasta 
         Height          =   315
         Left            =   3480
         TabIndex        =   26
         Top             =   1650
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         EmptyValue      =   0   'False
         DataType        =   3
         SpecialFeatures =   -1  'True
         KeepFocusOnError=   -1  'True
         BeepOnError     =   -1  'True
      End
      Begin MSComctlLib.Toolbar tbBotonera 
         Height          =   570
         Left            =   60
         TabIndex        =   45
         Top             =   180
         Width           =   13155
         _ExtentX        =   23204
         _ExtentY        =   1005
         ButtonWidth     =   1640
         ButtonHeight    =   1005
         Appearance      =   1
         Style           =   1
         ImageList       =   "imgBotonera"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   11
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Agregar"
               Key             =   "btnAgregar"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Modificar"
               Key             =   "btnModificar"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Eliminar"
               Key             =   "btnEliminar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Confirmar"
               Key             =   "btnConfirmar"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "Cancelar"
               Key             =   "btnCancelar"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Actualizar"
               Key             =   "btnActualizar"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Imprimir"
               Key             =   "btnImprimir"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Referencia"
               Key             =   "btnReferencia"
               Object.ToolTipText     =   "Ayuda referencial para los criterios de carga de horas"
               ImageIndex      =   9
            EndProperty
         EndProperty
      End
      Begin VB.Label lblPorcHsCargadas 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "lblPorcHsCargadas"
         Height          =   195
         Left            =   9900
         TabIndex        =   41
         Top             =   2340
         Width           =   1335
      End
      Begin VB.Label lblPorcHsRestantes 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "lblPorcHsRestantes"
         Height          =   195
         Left            =   9900
         TabIndex        =   40
         Top             =   2040
         Visible         =   0   'False
         Width           =   1380
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   34
         Top             =   15
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         Height          =   285
         Left            =   60
         TabIndex        =   32
         Top             =   900
         Width           =   1080
      End
      Begin VB.Label Label4 
         Caption         =   "Tarea"
         Height          =   285
         Left            =   60
         TabIndex        =   30
         Top             =   1320
         Width           =   1080
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Left            =   60
         TabIndex        =   28
         Top             =   1710
         Width           =   450
      End
      Begin VB.Label Label6 
         Caption         =   "Observaci�n"
         Height          =   195
         Left            =   60
         TabIndex        =   25
         Top             =   2340
         Width           =   1080
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   2880
         TabIndex        =   23
         Top             =   1710
         Width           =   420
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "o"
         Height          =   195
         Left            =   240
         TabIndex        =   21
         Top             =   1110
         Width           =   90
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   ":"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4200
         TabIndex        =   19
         Top             =   2115
         Width           =   75
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Tipo / Horas"
         Height          =   195
         Left            =   60
         TabIndex        =   17
         Top             =   2040
         Width           =   870
      End
   End
   Begin VB.Frame fraFiltro 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   45
      TabIndex        =   10
      Top             =   0
      Width           =   17670
      Begin VB.ComboBox cboRecurso 
         Enabled         =   0   'False
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   180
         Width           =   6480
      End
      Begin VB.Label Label1 
         Caption         =   "Recurso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   720
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4485
      Left            =   60
      TabIndex        =   8
      Top             =   630
      Width           =   13275
      _ExtentX        =   23416
      _ExtentY        =   7911
      _Version        =   393216
      Cols            =   16
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDropMode     =   1
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8205
      Left            =   17760
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdPrefe 
         Caption         =   "Preferencias"
         Height          =   435
         Left            =   80
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Ver cantidad de Horas Trabajadas d�a por d�a"
         Top             =   1980
         Width           =   1110
      End
      Begin VB.CommandButton cmdHsXDia 
         Caption         =   "Total horas d�a x d�a"
         Height          =   435
         Left            =   80
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Ver cantidad de Horas Trabajadas d�a por d�a"
         Top             =   2490
         Visible         =   0   'False
         Width           =   1110
      End
      Begin VB.CommandButton cmdPeticionesAsignadas 
         Caption         =   "Peticiones asignadas"
         Height          =   435
         Left            =   80
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Ver cantidad de Horas Trabajadas d�a por d�a"
         Top             =   1560
         Width           =   1110
      End
      Begin VB.CommandButton cmdEntreFechas 
         Caption         =   "Filtrar entre fechas"
         Height          =   435
         Left            =   80
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Filtrar entre fechas"
         Top             =   150
         Width           =   1110
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   435
         Left            =   80
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   7710
         Width           =   1110
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "al"
         Height          =   195
         Left            =   540
         TabIndex        =   6
         Top             =   945
         Width           =   135
      End
      Begin VB.Label lblFechaDesde 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "dd/mm/aaaa"
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   750
         Width           =   1065
      End
      Begin VB.Label lblFechaHasta 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "dd/mm/aaaa"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   1140
         Width           =   1065
      End
   End
   Begin VB.Frame fraHorasCargadas 
      Height          =   1365
      Left            =   13380
      TabIndex        =   35
      Top             =   6840
      Width           =   4335
      Begin VB.CheckBox chkFeriados 
         Caption         =   "Visualizar tambi�n d�as no h�biles y feriados"
         Height          =   195
         Left            =   120
         TabIndex        =   39
         Top             =   1080
         Visible         =   0   'False
         Width           =   2775
      End
      Begin VB.Label lblHorasRestantes 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   47
         Top             =   660
         Width           =   4125
      End
      Begin VB.Label lblHorasCargadasPeriodo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   46
         Top             =   660
         Width           =   4125
      End
      Begin VB.Label lblHsNominales 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3480
         TabIndex        =   44
         Top             =   960
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.Label lblHsCargadas 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2640
         TabIndex        =   43
         Top             =   960
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.Label lblTotalesHorasNominales 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   38
         Top             =   660
         Width           =   4125
      End
      Begin VB.Label lblRecurso 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   37
         Top             =   420
         Width           =   4125
      End
      Begin VB.Label lblFecha 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   36
         Top             =   180
         Width           =   4125
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdHorasCargadas 
      Height          =   6210
      Left            =   13380
      TabIndex        =   42
      Top             =   630
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   10954
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OLEDropMode     =   1
   End
End
Attribute VB_Name = "frmCargaHoras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' GMT01 - PET 69312 - La peticion no banco, categoria PRI, no van a requerir priorizar por BPE
' GMT02 - PET 70881 - Se muestra la lista de hora por MES
' GMT03 - PET 70881 - Si la peticion es PRI, se deja cargar horas.
Option Explicit

Private Const colFEDESDE = 0
Private Const colFEHASTA = 1
Private Const colFMTHORAS = 2
Private Const colOBJTIPO = 3
Private Const colOBJCODIGO = 4
Private Const colOBJNOMBRE = 5
Private Const colASIPETIC = 6
Private Const colNOMPETIC = 7
Private Const colCODTAREA = 8
Private Const colNOMTAREA = 9
Private Const colVINCULADO = 10
Private Const colCODRECURSO = 11
Private Const colSINASIGNAR = 12
Private Const colCODPETIC = 13
Private Const colHORAS = 14
Private Const colSRTFEDESDE = 15
Private Const colSRTFEHASTA = 16
Private Const colTipo = 17
Private Const colTIPONOM = 18
Private Const colOBSERV = 19
Private Const colTOT_COLS = 20

' Botonera
Const BOTON_AGREGAR = 1
Const BOTON_EDITAR = 2
Const BOTON_ELIMINAR = 3
Const BOTON_CONFIRMAR = 5
Const BOTON_CANCELAR = 6
Const BOTON_ACTUALIZAR = 8
Const BOTON_IMPRIMIR = 9

Private Const colFECHA = 0
Private Const colDIASEMANA = 1
Private Const colHORASTOT = 2
Private Const colHORASFAULT = 3

Private Const AGREGAR = "A"
Private Const MODIFICAR = "M"
Private Const ELIMINAR = "E"

Dim sOpcionSeleccionada As String
Dim sRecurso As String
Dim sAplicativo As String
Dim sFechaDesde, sFechaHasta
Dim flgEnCarga As Boolean
Dim flgFabrica As Boolean
Dim flgActivo As Boolean
Dim sModo As String
Dim sPrefVisualizar As String
Dim iTotalHoras As Long
Dim iRowIni As Long
Dim iRowFin As Long
Dim lSumaHoras As Long
Dim lSumaMinutos As Long
Dim per_nrointerno As Long

Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long

Private Type T_Info
    REG As String
    SDA As String
    PES As String
    ENG As String
    PRI As String
    CSI As String
    LCA As String
    SPR As String
End Type

Dim vFeriados() As Date
Dim bHayFeriados As Boolean
Dim lHoras_Cargadas As Long
Dim lHoras_Nominales As Long
Dim hsNomi As Long
Dim FechaDesde As Date
Dim FechaHasta As Date
Dim agrupamientoParaPriorizadasBanco As Long
Dim agrupamientoSDA2 As Long
Dim agrupamientoSDA2Nombre As String
Dim agrupSDA2hijos() As Long
Dim bHayAgrupamientosSDAHijos As Boolean
Dim sMensaje As String

Private Declare Function GetProcAddress Lib "kernel32" (ByVal hModule As Long, ByVal lpProcName As String) As Long
Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long



Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    Me.Height = 8790
    Me.Width = 15645
    
    Call Puntero(True)
    lblHorasCargadasPeriodo.Caption = "Horas cargadas del per�odo: "
    lblHorasRestantes.Caption = "Horas restantes del per�odo: "
    
    lblPorcHsCargadas.visible = False
    lblPorcHsRestantes.visible = False
    If GetSetting("GesPet", "Preferencias\Horas", "Porcentajes", "N") = "S" Then
        lblPorcHsCargadas.ToolTipText = "Calculado sobre el total de todas las horas cargadas desde el primero de mes al d�a de hoy."
    End If
    
    ' Obtengo el agrupamiento especial utilizado para exceptuar aquellas peticiones de banco
    ' que est�n priorizadas y se les permite cargar horas (por el mero hecho de pertenecer a
    ' este agrupamiento especial).
    agrupamientoParaPriorizadasBanco = 0
    If sp_GetVarios("AGP_PRIO") Then
        agrupamientoParaPriorizadasBanco = ClearNull(aplRST.Fields!var_numero)
    End If
    
    ' Obtengo el agrupamiento ra�z para la SDA 2.0
    agrupamientoSDA2 = 0
    If sp_GetVarios("AGP_SDA2") Then
        agrupamientoSDA2 = ClearNull(aplRST.Fields!var_numero)
        If sp_GetAgrup(agrupamientoSDA2) Then
            agrupamientoSDA2Nombre = ClearNull(aplRST.Fields!agr_titulo)
        End If
    End If
    
    ' Obtengo todas las dependencias del SDA 2.0 ra�z
    bHayAgrupamientosSDAHijos = False
    If agrupamientoSDA2 <> 0 Then
        Debug.Print "Agrupamientos dependientes de la SDA 2.0"
        Dim i As Long
        If sp_GetAgrupDepen(agrupamientoSDA2) Then
            Do While Not aplRST.EOF
                If aplRST.Fields!agr_nrointerno <> agrupamientoSDA2 Then
                    ReDim Preserve agrupSDA2hijos(i)
                    agrupSDA2hijos(i) = aplRST.Fields!agr_nrointerno
                    bHayAgrupamientosSDAHijos = True
                    Debug.Print agrupSDA2hijos(i)
                    i = i + 1
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End If

    flgEnCarga = True
    sOpcionSeleccionada = ""
    glSector = glLOGIN_Sector
    glGrupo = glLOGIN_Grupo
    sRecurso = glLOGIN_ID_REEMPLAZO
    sPrefVisualizar = GetSetting("GesPet", "CargaHoras", "Visualizar")
    'modo
    'R recurso unitario
    'G sector
    sModo = "R"
    flgActivo = True
    Call InicializarCombos

    If GetSetting("GesPet", "Preferencias\Horas", "Porcentajes", "N") = "S" Then
        lblPorcHsCargadas.visible = True
        lblPorcHsRestantes.visible = True
        Call InicializarDatos
    End If
    

    If GetSetting("GesPet", "CargaHoras", "ListaInicio", "N") = "SEM" Then
        glFechaDesde = DateAdd("d", -7, glFechaDesde)
    ElseIf GetSetting("GesPet", "CargaHoras", "ListaInicio", "N") = "MES" Then
        glFechaDesde = "01/" & Month(glFechaDesde) & "/" & Year(glFechaDesde)
    Else                                                                          'GMT02
        glFechaDesde = "01/" & Month(glFechaDesde) & "/" & Year(glFechaDesde)     'GMT02
    End If

    ' Determina el periodo de planificaci�n vigente
    If sp_GetPeriodo(Null, "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!vigente) = "S" Then
                per_nrointerno = aplRST.Fields!per_nrointerno
                Exit Do
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    Call Puntero(False)
    
    Call CargarFeriados
    Call IniciarScroll(grdDatos)
    Call IniciarScroll(grdHorasCargadas)
    Call InicializarPantalla
End Sub

Sub InicializarPantalla()
    ' Mensaje de ayuda para la carga de horas
    sMensaje = "Categor�as de peticiones a las que no se le podr�n cargar m�s horas:" & vbCrLf & vbCrLf & _
            "1. Categor�a PRI (Priorizadas por impactos y palancas excepto vinculadas y seguros)" & vbCrLf & _
            "2. Categor�a PRI (por proyecto IDM excepto vinculadas y seguros)" & vbCrLf & _
            "3. Categor�a PES (procesos estrat�gicos - scrum - t�cticos)" & vbCrLf & _
            "4. Categor�a SDA que no sean PI actual o siguiente."
    Me.Tag = ""
    lblFechaDesde = Format(glFechaDesde, "dd/mm/yyyy")
    lblFechaHasta = Format(glFechaHasta, "dd/mm/yyyy")
    Call HabilitarBotones(0)
    Show
End Sub

Private Sub InicializarCombos()
    Dim vExtendido() As String
    Dim sValExtendido As String
    Dim sSec As String
    Dim sMod As String
    Dim flgRecurso As Boolean
    Dim xNivel, xArea, xModo As String
    Dim nPos As Integer
    
    Call Status("Cargando recurso...")
    flgRecurso = False
    
    With cboHorasTipo
        .Clear
        .AddItem "Desarrollo" & ESPACIOS & "||0"
        .AddItem "Refinamiento" & ESPACIOS & "||1"
        .ListIndex = 0
    End With
    
    If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
        If sp_GetRecurso(Null, "R") Then
            Do While Not aplRST.EOF
                cboRecurso.AddItem ClearNull(aplRST.Fields.Item("cod_recurso")) & ": " & aplRST.Fields.Item("nom_recurso") & ESPACIOS & ESPACIOS & "||" & aplRST.Fields.Item("cod_sector") & " | " & aplRST.Fields.Item("cod_grupo")
                aplRST.MoveNext
            Loop
            cboRecurso.ListIndex = 0
        End If
    Else
        If InPerfil("CHRS") Then
            xNivel = getPerfNivel("CHRS")
            xArea = getPerfArea("CHRS")
            Select Case xNivel
                Case "DIRE": xModo = "DI"
                Case "GERE": xModo = "GE"
                Case "SECT": xModo = "GR"
                Case "GRUP": xModo = "SU"
            End Select
            If sp_GetRecurso(xArea, xModo, "A") Then
                Do While Not aplRST.EOF
                    cboRecurso.AddItem ClearNull(aplRST.Fields.Item("cod_recurso")) & " : " & aplRST.Fields.Item("nom_recurso") & ESPACIOS & "||" & aplRST.Fields.Item("cod_sector") & " | " & aplRST.Fields.Item("cod_grupo")
                    aplRST.MoveNext
                Loop
                cboRecurso.ListIndex = 0
            End If
        End If
        If InPerfil("CHSF") Then        ' Horas de F�brica
            xNivel = getPerfNivel("CHSF")
            xArea = getPerfArea("CHSF")
            Select Case xNivel
                Case "DIRE": xModo = "DI"
                Case "GERE": xModo = "GE"
                Case "SECT": xModo = "GR"
                Case "GRUP": xModo = "SU"
            End Select
            
            If sp_GetRecursoOtros(xArea, xModo, "A") Then
                Do While Not aplRST.EOF
                    cboRecurso.AddItem ClearNull(aplRST.Fields.Item("cod_fab")) & " : " & aplRST.Fields.Item("nom_fab") & ESPACIOS & ESPACIOS & "||" & aplRST.Fields.Item("cod_sector") & " | " & aplRST.Fields.Item("cod_grupo")
                    DoEvents
                    aplRST.MoveNext
                Loop
                cboRecurso.ListIndex = 0
            End If
        End If
    End If
    
    nPos = PosicionCombo(cboRecurso, sRecurso)
    If nPos > -1 Then
        cboRecurso.ListIndex = nPos
    Else
        If sp_GetRecurso(sRecurso, "R") Then
            cboRecurso.AddItem ClearNull(aplRST.Fields.Item("cod_recurso")) & " : " & aplRST.Fields.Item("nom_recurso") & ESPACIOS & ESPACIOS & ESPACIOS & "||" & aplRST.Fields.Item("cod_sector") & " | " & aplRST.Fields.Item("cod_grupo"), 0
            If hsNomi = 0 Then hsNomi = ClearNull(aplRST.Fields!horasdiarias)
        End If
        cboRecurso.ListIndex = 0
    End If
    sRecurso = CodigoCombo(cboRecurso)
    Call InicializarPetTar
End Sub

Private Sub InicializarDatos()
    Dim dFechaDesde As Date
    Dim dFechaHasta As Date
    Dim iHorasNominales As Long
    Dim iTotalHorasTrabajadas As Long
    Dim iTotalHorasMinimas As Long
    Dim iTotalHorasNominales As Long
    
    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        iHorasNominales = ClearNull(aplRST.Fields.Item("horasdiarias"))
        iHorasNominales = iHorasNominales * 60
    End If
    
    iTotalHorasTrabajadas = 0
    iTotalHorasNominales = 0
    dFechaDesde = Format("1" & "/" & Month(date) & "/" & Year(date), "dd/mm/yyyy")
    dFechaHasta = Format(date, "dd/mm/yyyy")
    
    ' TODO: revisar esto! 24.11.16
    
    ' % HORAS CARGADAS
    If sp_rptHorasTrabajadasRecurso(glLOGIN_ID_REEMPLAZO, dFechaDesde, dFechaHasta) Then
        Do While Not aplRST.EOF
            iTotalHorasTrabajadas = iTotalHorasTrabajadas + aplRST.Fields!horas_trab
            iTotalHorasNominales = iTotalHorasNominales + iHorasNominales
            If aplRST.Fields!horas_trab > iHorasNominales Then
                iTotalHorasMinimas = iTotalHorasMinimas + iHorasNominales
            Else
                iTotalHorasMinimas = iTotalHorasMinimas + aplRST.Fields!horas_trab
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If iTotalHorasTrabajadas > iTotalHorasNominales Then
        lblPorcHsCargadas.Caption = "Horas cargadas en el per�odo al d�a de hoy: + 100,00 %"
    Else
        If iTotalHorasTrabajadas > 0 And iTotalHorasNominales > 0 Then
            lblPorcHsCargadas.Caption = "Horas cargadas en el per�odo al d�a de hoy: " & Format(iTotalHorasTrabajadas / iTotalHorasNominales, "##0.00 %")
            ' % HORAS RESTANTES
            lblPorcHsRestantes.Caption = "Horas restantes a adeudadas en el per�odo al d�a de hoy: " & Format(1 - (iTotalHorasMinimas / iTotalHorasNominales), "##0.00 %")
        Else
            lblPorcHsCargadas.Caption = "Horas cargadas en el per�odo al d�a de hoy: 0,00 %"
            ' % HORAS RESTANTES
            lblPorcHsRestantes.Caption = "Horas restantes a adeudadas en el per�odo al d�a de hoy: 100,00 %"
        End If
    End If
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            Me.Top = 0
            Me.Left = 0
            If Me.Height < 8790 Then Me.Height = 8790
            If Me.Width < 15645 Then Me.Width = 15645
        End If
        If Me.ScaleWidth > 0 Then pnlBotones.Left = Me.ScaleWidth - pnlBotones.Width - 50
        If Me.ScaleHeight > 0 Then pnlBotones.Height = Me.ScaleHeight - 50
        If Me.ScaleWidth > pnlBotones.Width - 150 Then fraFiltro.Width = Me.ScaleWidth - pnlBotones.Width - 150
        
        fraDatos.Top = Me.ScaleHeight - fraDatos.Height - 50
        
        If Me.ScaleWidth > 4335 Then grdDatos.Width = Me.ScaleWidth - IIf(fraHorasCargadas.Width > 4335, fraHorasCargadas.Width + 150, 4335 + 150) - pnlBotones.Width - 50
        fraDatos.Width = grdDatos.Width
        tbBotonera.Width = fraDatos.Width - 150
        
        fraHorasCargadas.Height = fraDatos.Height
        fraHorasCargadas.Top = fraDatos.Top
        
        grdHorasCargadas.Left = grdDatos.Width + 100
        fraHorasCargadas.Left = grdDatos.Width + 100
        fraHorasCargadas.Width = grdHorasCargadas.Width
        
        If Me.ScaleHeight > fraDatos.Height - 50 - fraFiltro.Height Then grdDatos.Height = Me.ScaleHeight - fraDatos.Height - 50 - fraFiltro.Height
        grdHorasCargadas.Height = grdDatos.Height
        chkFeriados.Top = fraHorasCargadas.Height - 300: chkFeriados.Width = fraHorasCargadas.Width - 200
        cmdCerrar.Top = pnlBotones.Height - cmdCerrar.Height - 100
        
        lblRecurso.Top = lblFecha.Top + lblRecurso.Height
        lblTotalesHorasNominales.Top = lblRecurso.Top + lblTotalesHorasNominales.Height
        lblHorasCargadasPeriodo.Top = lblTotalesHorasNominales.Top + lblHorasCargadasPeriodo.Height
        lblHorasRestantes.Top = lblHorasCargadasPeriodo.Top + lblHorasRestantes.Height
    End If
End Sub

Private Sub cmdPrefe_Click()
    If Not LockProceso(True) Then Exit Sub
    auxSetPetHoras.Show vbModal
    If sPrefVisualizar <> GetSetting("GesPet", "CargaHoras", "Visualizar") Then
        sPrefVisualizar = GetSetting("GesPet", "CargaHoras", "Visualizar")
        Call InicializarPetTar
        Call CargarGrid
    End If
    Call LockProceso(False)
End Sub

Private Sub cboRecurso_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then Exit Sub
   
    flgEnCarga = True
    txtCodtarea = ""
    txtPetnroasig = ""
    Call InicializarPetTar
    If sp_GetRecurso(CodigoCombo(cboRecurso, False), "") Then
        flgFabrica = False
        flgActivo = IIf(ClearNull(aplRST.Fields!estado_recurso) = "A", True, False)
    Else
        If sp_GetFabrica(CodigoCombo(cboRecurso, False)) Then
            flgFabrica = True
            flgActivo = IIf(ClearNull(aplRST.Fields!estado_fab) = "A", True, False)
        End If
    End If
    Call HabilitarBotones(0)
    flgEnCarga = False
    Call LockProceso(False)
End Sub

Private Sub cboTarea_Click()
    If flgEnCarga Then Exit Sub
    cboTarea.ToolTipText = TextoCombo(cboTarea)   ' Se agrega el t�tulo completo de la tarea al ToolTipText para una mejor visualizaci�n
    flgEnCarga = True
    txtCodtarea = ""
    txtCodtarea = CodigoCombo(cboTarea)
    txtPetnroasig = ""
    cboPeticion.ListIndex = -1
    flgEnCarga = False
End Sub

Private Sub cboPeticion_Click()
    If flgEnCarga Then Exit Sub
    cboPeticion.ToolTipText = TextoCombo(cboPeticion)   ' Se agrega el t�tulo completo de la petici�n al ToolTipText para una mejor visualizaci�n
    flgEnCarga = True
    txtPetnroasig = ""
    txtPetnroasig = CodigoCombo(cboPeticion)
    txtCodtarea = ""
    cboTarea.ListIndex = -1
    flgEnCarga = False
End Sub



Private Sub txtFechaDesde_LostFocus()
    If IsNull(txtFechaDesde.DateValue) Then
        txtFechaHasta.text = ""
    Else
        txtFechaHasta.text = Format(txtFechaDesde.DateValue, "dd/mm/yyyy")
    End If
End Sub

Private Sub txtPetnroasig_LostFocus()
    Dim nPos As Integer
    
    If flgEnCarga Then Exit Sub
    
    flgEnCarga = True
    If Trim(txtPetnroasig) <> "" Then
        If Val(Trim(txtPetnroasig)) > 0 Then
            nPos = PosicionCombo(cboPeticion, txtPetnroasig)
            If nPos = -1 Then
                txtPetnroasig = ""
            Else
                cboPeticion.ListIndex = nPos
            End If
            txtCodtarea = ""
            cboTarea.ListIndex = -1
            
            If sOpcionSeleccionada <> "" Then
                If Not cboHorasTipo.Enabled Then
                    Call setHabilCtrl(cboHorasTipo, NORMAL)
                End If
            End If
        Else
            txtPetnroasig = "0"
        End If
    Else
        cboPeticion.ListIndex = -1
    End If
    flgEnCarga = False
End Sub

Private Sub txtCodtarea_LostFocus()
    Dim nPos As Integer
    
    If flgEnCarga Then Exit Sub

    flgEnCarga = True
    If Trim(txtCodtarea) <> "" Then
        nPos = PosicionCombo(cboTarea, txtCodtarea)
        If nPos = -1 Then
            txtCodtarea = ""
        Else
            cboTarea.ListIndex = nPos
        End If
        txtPetnroasig = ""
        cboPeticion.ListIndex = -1
        
        ' Cuando se cargan tareas, las horas corresponden a desarrollo siempre
        cboHorasTipo.ListIndex = 0
        Call setHabilCtrl(cboHorasTipo, DISABLE)
    Else
        cboTarea.ListIndex = -1
    End If
    flgEnCarga = False
End Sub

Private Sub cmdMasPeticiones_Click()
    Dim vRetorno() As String
    Dim nPos As Integer
    flgEnCarga = True
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glAuxRetorno = ""
    glAuxEstado = "CONFEC|DEVSOL|REFERE|DEVREF|SUPERV|DEVSUP|AUTORI|DEVAUT|COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|RECHAZ|RECHTE|CANCEL|SUSPEN|REVISA|TERMIN"
    frmSelPet.Show 1
    DoEvents
    ' Arreglar todo esto!!!!!!!!!!!!!
    If glAuxRetorno <> "" Then
        If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
            txtCodtarea = ""
            cboTarea.ListIndex = -1
            nPos = PosicionCombo(cboPeticion, vRetorno(3), True)
            If nPos = -1 Then
                cboPeticion.AddItem glAuxRetorno
                nPos = PosicionCombo(cboPeticion, vRetorno(3), True)
                If nPos = -1 Then
                    nPos = PosicionCombo(cboPeticion, vRetorno(UBound(vRetorno) - 1), True)
                    If nPos = -1 Then
                        cboPeticion.ListIndex = 0
                        MsgBox "Seleccione manualmente la petici�n que acaba de incorporar del combo de peticiones.", vbInformation + vbOKOnly, "Carga de horas: Incorporando peticiones"
                    Else
                        cboPeticion.ListIndex = nPos
                    End If
                Else
                    cboPeticion.ListIndex = nPos
                End If
            Else
                cboPeticion.ListIndex = nPos
            End If
            DoEvents
            txtPetnroasig = ""
            txtPetnroasig = CodigoCombo(cboPeticion)
        End If
    End If
    flgEnCarga = False
    Call LockProceso(False)
End Sub

Private Sub cmdMasTareas_Click()
    Dim vRetorno() As String
    Dim nPos As Integer
    If Not LockProceso(True) Then
        Exit Sub
    End If
    flgEnCarga = True
    glAuxRetorno = ""
    frmSelTar.prpSoloHabil = True
    frmSelTar.Show 1
    DoEvents
    If glAuxRetorno <> "" Then
        If ParseString(vRetorno, glAuxRetorno, ":") > 0 Then
            txtPetnroasig = ""
            cboPeticion.ListIndex = -1
            nPos = PosicionCombo(cboTarea, vRetorno(1))
            If nPos = -1 Then
                cboTarea.AddItem glAuxRetorno
                nPos = PosicionCombo(cboTarea, vRetorno(1))
                If nPos = -1 Then
                    MsgBox ("Error al incorporar tarea")
                    cboTarea.ListIndex = 0
                Else
                    cboTarea.ListIndex = nPos
                End If
            Else
                cboTarea.ListIndex = nPos
            End If
            DoEvents
            txtCodtarea = ""
            txtCodtarea = CodigoCombo(cboTarea)
        End If
       cboTarea.SetFocus
    End If
    flgEnCarga = False
    Call LockProceso(False)
End Sub

Private Function Valida_Horas() As Boolean
    Valida_Horas = False
    
    ' Si varia la cantidad de horas cargadas, entonces debo preguntar qu� hacer, sino
    ' se maneja internamente la actualizaci�n de datos
    If Val(grdDatos.TextMatrix(grdDatos.rowSel, colHORAS)) <> Val(txtHoras) * 60 + Val(txtMinutos) Then
        Valida_Horas = True
    End If
End Function
'}

Private Sub Confirmar()
    txtFechaDesde.ForceFormat
    txtFechaHasta.ForceFormat
    Select Case sOpcionSeleccionada
        Case AGREGAR
            If CamposObligatorios Then
                Call Puntero(True)
                If sp_InsertHorasTrabajadas(CodigoCombo(cboRecurso), "" & CodigoCombo(cboTarea), "" & CodigoCombo(Me.cboPeticion, True), txtFechaDesde.DateValue, txtFechaHasta.DateValue, Val(txtHoras) * 60 + Val(txtMinutos), chkAsignada(), txtObservaciones, CodigoCombo(cboHorasTipo, True)) Then
                    Call ActualizarCarga
                    Call HabilitarBotones(2, False)
                End If
            End If
        Case MODIFICAR
            If CamposObligatorios Then
                With grdDatos
                    Call Puntero(True)
                    If sp_UpdateHorasTrabajadas(CodigoCombo(cboRecurso), "" & .TextMatrix(.rowSel, colCODTAREA), "" & .TextMatrix(.rowSel, colCODPETIC), .TextMatrix(.rowSel, colFEDESDE), .TextMatrix(.rowSel, colFEHASTA), "" & CodigoCombo(cboTarea), "" & CodigoCombo(Me.cboPeticion, True), txtFechaDesde.DateValue, txtFechaHasta.DateValue, Val(txtHoras) * 60 + Val(txtMinutos), chkAsignada(), txtObservaciones, CodigoCombo(cboHorasTipo, True)) Then
                        Call ActualizarCarga
                        Call HabilitarBotones(0, False)
                    End If
                End With
            End If
        Case ELIMINAR
            Call Puntero(True)
            If sp_DeleteHorasTrabajadas(CodigoCombo(cboRecurso), CodigoCombo(cboTarea), CodigoCombo(Me.cboPeticion, True), txtFechaDesde.DateValue, txtFechaHasta.DateValue) Then
                Call GrillaEliminarItem(grdDatos, grdDatos.rowSel)
                Call ActualizarCarga
                Call HabilitarBotones(0, False)
            End If
    End Select
    Call InicializarDatos
    Call Puntero(False)
End Sub

Private Sub ActualizarCarga()
    Dim ActualRow As Integer
    
    With grdDatos
        Select Case sOpcionSeleccionada
            Case AGREGAR
                .Rows = .Rows + 1
                ActualRow = .Rows - 1
                .TextMatrix(ActualRow, colHORAS) = Val(txtHoras) * 60 + Val(txtMinutos)
                .TextMatrix(ActualRow, colFMTHORAS) = hrsFmtHora(Val(txtHoras) * 60 + Val(txtMinutos))
                .TextMatrix(ActualRow, colOBSERV) = txtObservaciones
                .TextMatrix(ActualRow, colCODTAREA) = CodigoCombo(cboTarea)
                .TextMatrix(ActualRow, colNOMTAREA) = TextoCombo(cboTarea)
                .TextMatrix(ActualRow, colCODPETIC) = CodigoCombo(Me.cboPeticion, True)
                .TextMatrix(ActualRow, colASIPETIC) = CodigoCombo(cboPeticion)
                .TextMatrix(ActualRow, colNOMPETIC) = TextoCombo(cboPeticion)
                .TextMatrix(ActualRow, colFEDESDE) = txtFechaDesde.text
                .TextMatrix(ActualRow, colFEHASTA) = txtFechaHasta.text
                .TextMatrix(ActualRow, colSINASIGNAR) = IIf(cboPeticion.ListIndex = -1, Right(cboTarea, 1), Right(cboPeticion, 1))
                .TextMatrix(ActualRow, colVINCULADO) = IIf(.TextMatrix(ActualRow, colSINASIGNAR) = "S", "No", "Si")
                .TextMatrix(ActualRow, colTipo) = CodigoCombo(cboHorasTipo, True)
                .TextMatrix(ActualRow, colTIPONOM) = IIf(.TextMatrix(ActualRow, colTipo) = "0", "Desarrollo", "Refinamiento")
                If Val(.TextMatrix(ActualRow, colCODPETIC)) > 0 Then
                    .TextMatrix(ActualRow, colOBJTIPO) = "PET"
                    .TextMatrix(ActualRow, colOBJCODIGO) = "Petici�n N� " & .TextMatrix(ActualRow, colASIPETIC)
                    .TextMatrix(ActualRow, colOBJNOMBRE) = .TextMatrix(ActualRow, colNOMPETIC)
                Else
                    .TextMatrix(ActualRow, colOBJTIPO) = "TAR"
                    .TextMatrix(ActualRow, colOBJCODIGO) = "Tarea - " & .TextMatrix(ActualRow, colCODTAREA)
                    .TextMatrix(ActualRow, colOBJNOMBRE) = .TextMatrix(ActualRow, colNOMTAREA)
                End If
                If .Rows Mod 2 = 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
                .TopRow = .Rows - 1
                .row = .Rows - 1
                .rowSel = .Rows - 1
                .col = 0
                .ColSel = .cols - 1
                Call MostrarSeleccion
            Case MODIFICAR
                ActualRow = .rowSel
                .TextMatrix(ActualRow, colHORAS) = Val(txtHoras) * 60 + Val(txtMinutos)
                .TextMatrix(ActualRow, colFMTHORAS) = hrsFmtHora(Val(txtHoras) * 60 + Val(txtMinutos))
                .TextMatrix(ActualRow, colOBSERV) = txtObservaciones
                .TextMatrix(ActualRow, colCODTAREA) = CodigoCombo(cboTarea)
                .TextMatrix(ActualRow, colNOMTAREA) = TextoCombo(cboTarea)
                .TextMatrix(ActualRow, colCODPETIC) = CodigoCombo(Me.cboPeticion, True)
                .TextMatrix(ActualRow, colASIPETIC) = CodigoCombo(cboPeticion)
                .TextMatrix(ActualRow, colNOMPETIC) = TextoCombo(cboPeticion)
                .TextMatrix(ActualRow, colFEDESDE) = txtFechaDesde.text
                .TextMatrix(ActualRow, colFEHASTA) = txtFechaHasta.text
                .TextMatrix(ActualRow, colSINASIGNAR) = IIf(cboPeticion.ListIndex = -1, Right(cboTarea, 1), Right(cboPeticion, 1))
                .TextMatrix(ActualRow, colVINCULADO) = IIf(.TextMatrix(ActualRow, colSINASIGNAR) = "S", "No", "Si")
                .TextMatrix(ActualRow, colTipo) = CodigoCombo(cboHorasTipo, True)
                .TextMatrix(ActualRow, colTIPONOM) = IIf(.TextMatrix(ActualRow, colTipo) = "0", "Desarrollo", "Refinamiento")
                If Val(.TextMatrix(ActualRow, colCODPETIC)) > 0 Then
                    .TextMatrix(ActualRow, colOBJTIPO) = "PET"
                    .TextMatrix(ActualRow, colOBJCODIGO) = "Petici�n N� " & .TextMatrix(ActualRow, colASIPETIC)
                    .TextMatrix(ActualRow, colOBJNOMBRE) = .TextMatrix(ActualRow, colNOMPETIC)
                Else
                    .TextMatrix(ActualRow, colOBJTIPO) = "TAR"
                    .TextMatrix(ActualRow, colOBJCODIGO) = "Tarea - " & .TextMatrix(ActualRow, colCODTAREA)
                    .TextMatrix(ActualRow, colOBJNOMBRE) = .TextMatrix(ActualRow, colNOMTAREA)
                End If
        End Select
    End With
    Call CargarGridHoras
    DoEvents
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case AGREGAR, MODIFICAR, ELIMINAR
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Call LockProceso(False)
            Me.Hide
    End Select
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Call LockProceso(True)
    Call setHabilCtrl(cboRecurso, "DIS")
    Call setHabilCtrl(cboHorasTipo, "DIS")
    Call setHabilCtrl(txtObservaciones, "DIS")
    Call setHabilCtrl(txtFechaDesde, "DIS")
    Call setHabilCtrl(txtFechaHasta, "DIS")
    Call setHabilCtrl(cboTarea, "DIS")
    Call setHabilCtrl(cboPeticion, "DIS")
    Call setHabilCtrl(txtCodtarea, "DIS")
    Call setHabilCtrl(txtPetnroasig, "DIS")
    Call setHabilCtrl(txtHoras, "DIS")
    Call setHabilCtrl(txtMinutos, "DIS")
    Call setHabilCtrl(cmdMasPeticiones, DISABLE)
    Call setHabilCtrl(cmdMasTareas, DISABLE)
   
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            If cboRecurso.ListCount > 1 Then Call setHabilCtrl(cboRecurso, "NOR")
            grdDatos.Enabled = True
            cmdEntreFechas.Enabled = True
            cmdHsXDia.Enabled = True
            cmdPeticionesAsignadas.Enabled = True
            cmdPrefe.Enabled = True
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            
            tbBotonera.Buttons(BOTON_AGREGAR).Enabled = True
            tbBotonera.Buttons(BOTON_EDITAR).Enabled = True
            tbBotonera.Buttons(BOTON_ELIMINAR).Enabled = True
            tbBotonera.Buttons(BOTON_CONFIRMAR).Enabled = False
            tbBotonera.Buttons(BOTON_CANCELAR).Enabled = False
            tbBotonera.Buttons(BOTON_ACTUALIZAR).Enabled = True
            tbBotonera.Buttons(BOTON_IMPRIMIR).Enabled = True

            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
                Call CargarGridHoras
            Else
                Call MostrarSeleccion
            End If
        Case 1
            Call setHabilCtrl(cboRecurso, "DIS")
            grdDatos.Enabled = False
            
            tbBotonera.Buttons(BOTON_AGREGAR).Enabled = False
            tbBotonera.Buttons(BOTON_EDITAR).Enabled = False
            tbBotonera.Buttons(BOTON_ELIMINAR).Enabled = False
            tbBotonera.Buttons(BOTON_CONFIRMAR).Enabled = True
            tbBotonera.Buttons(BOTON_CANCELAR).Enabled = True
            tbBotonera.Buttons(BOTON_ACTUALIZAR).Enabled = False
            tbBotonera.Buttons(BOTON_IMPRIMIR).Enabled = False
            
            cmdEntreFechas.Enabled = False
            cmdHsXDia.Enabled = False
            cmdPeticionesAsignadas.Enabled = False
            cmdPrefe.Enabled = False
            Select Case sOpcionSeleccionada
                Case AGREGAR
                    lblSeleccion = " AGREGAR (PETICI�N O TAREA) ": lblSeleccion.visible = True
                    If IsNull(txtFechaDesde.DateValue) Then
                        txtFechaDesde.text = Format(date, "dd/mm/yyyy")
                    End If
                    If IsNull(txtFechaHasta.DateValue) Then
                        txtFechaHasta.text = Format(date, "dd/mm/yyyy")
                    End If
                    Call setHabilCtrl(txtFechaDesde, "NOR")
                    Call setHabilCtrl(txtFechaHasta, "NOR")
                    
                    ' Por un tema de tiempos, se va a implementar una soluci�n horrible:
                    ' Se va a hardcodear que este comportamiento solo sea para Diego Marcet.
                    If Not flgFabrica Or (glLOGIN_ID_REEMPLAZO = "A114901" And glUsrPerfilActual = "CGRU") Then
                        Call setHabilCtrl(txtCodtarea, "NOR")
                        Call setHabilCtrl(cboTarea, "NOR")
                        Call setHabilCtrl(cmdMasTareas, "NOR")
                    End If
                    Call setHabilCtrl(cboPeticion, "NOR")
                    Call setHabilCtrl(txtPetnroasig, "NOR")
                    Call setHabilCtrl(cboHorasTipo, "NOR"): cboHorasTipo.ListIndex = 0
                    Call setHabilCtrl(txtHoras, "NOR")
                    Call setHabilCtrl(txtMinutos, "NOR")
                    Call setHabilCtrl(txtObservaciones, "NOR")
                    txtHoras = "": txtMinutos = "": txtObservaciones = ""
                    cmdMasPeticiones.Enabled = True
                Case MODIFICAR
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    Call setHabilCtrl(txtFechaDesde, "NOR")
                    Call setHabilCtrl(txtFechaHasta, "NOR")
                    ' Por un tema de tiempos, se va a implementar una soluci�n horrible:
                    ' Se va a hardcodear que este comportamiento solo sea para Diego Marcet.
                    If Not flgFabrica Or (glLOGIN_ID_REEMPLAZO = "A114901" And glUsrPerfilActual = "CGRU") Then
                        Call setHabilCtrl(txtCodtarea, "NOR")
                        Call setHabilCtrl(cboTarea, "NOR")
                        Call setHabilCtrl(cmdMasTareas, "NOR")
                    End If
                    Call setHabilCtrl(cboPeticion, "NOR")
                    Call setHabilCtrl(txtPetnroasig, "NOR")
                    Call setHabilCtrl(cboHorasTipo, "NOR")
                    Call setHabilCtrl(txtHoras, "NOR")
                    Call setHabilCtrl(txtMinutos, "NOR")
                    Call setHabilCtrl(txtObservaciones, "NOR")
                    cmdMasPeticiones.Enabled = True
                    txtHoras.SetFocus
                Case ELIMINAR
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
            End Select
        Case 2
            txtHoras = "": txtMinutos = "": txtObservaciones = ""
            Call setHabilCtrl(txtObservaciones, "NOR")
            Call setHabilCtrl(txtFechaDesde, "NOR")
            Call setHabilCtrl(txtFechaHasta, "NOR")
            ' Por un tema de tiempos, se va a implementar una soluci�n horrible:
            ' Se va a hardcodear que este comportamiento solo sea para Diego Marcet.
            If Not flgFabrica Or (glLOGIN_ID_REEMPLAZO = "A114901" And glUsrPerfilActual = "CGRU") Then
                Call setHabilCtrl(cboTarea, "NOR")
                Call setHabilCtrl(txtCodtarea, "NOR")
                Call setHabilCtrl(cmdMasTareas, "NOR")
            End If
            Call setHabilCtrl(cboPeticion, "NOR")
            Call setHabilCtrl(txtPetnroasig, "NOR")
            Call setHabilCtrl(cboHorasTipo, "NOR")
            Call setHabilCtrl(txtHoras, "NOR")
            Call setHabilCtrl(txtMinutos, "NOR")
            cmdEntreFechas.Enabled = False
            cmdHsXDia.Enabled = False
            cmdPeticionesAsignadas.Enabled = False
            cmdPrefe.Enabled = False
            cmdMasPeticiones.Enabled = True
            
            tbBotonera.Buttons(BOTON_AGREGAR).Enabled = False
            tbBotonera.Buttons(BOTON_EDITAR).Enabled = False
            tbBotonera.Buttons(BOTON_ELIMINAR).Enabled = False
            tbBotonera.Buttons(BOTON_CONFIRMAR).Enabled = True
            tbBotonera.Buttons(BOTON_CANCELAR).Enabled = True
            tbBotonera.Buttons(BOTON_ACTUALIZAR).Enabled = False
            tbBotonera.Buttons(BOTON_IMPRIMIR).Enabled = False
    End Select
    Call LockProceso(False)
End Sub

Private Sub cmdEntreFechas_Click()
    Dim sFechaDesde, sFechaHasta
    If flgEnCarga Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If PedirFechaDesdeHasta() Then
        lblFechaDesde = Format(glFechaDesde, "dd/mm/yyyy")
        lblFechaHasta = Format(glFechaHasta, "dd/mm/yyyy")
        If FechaDesde <> glFechaDesde Or FechaHasta <> glFechaHasta Then
            FechaDesde = glFechaDesde
            FechaHasta = glFechaHasta
            Call CargarFeriados
        End If
        Call HabilitarBotones(0)
    End If
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    Dim pet_nrointerno As Long
    Dim agp_nrointerno As Long
    Dim categoria      As String
    Dim Empresa        As Integer
    Dim i              As Long
    Dim flag           As Boolean
    Dim flagEsSDA2     As Boolean
    
    flagEsSDA2 = False
    CamposObligatorios = True
    If cboRecurso.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un recurso")
        CamposObligatorios = False
        Exit Function
    End If
    
    If Not flgActivo Then
        MsgBox "El recurso seleccionado no se encuentra en estado activo", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If Not valTareaHabil(CodigoCombo(cboTarea)) Then
        MsgBox "La tarea est� marcada como inhabilitada.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If cboTarea.ListIndex < 0 And cboPeticion.ListIndex < 0 Then
        MsgBox "Falta seleccionar petici�n o tarea", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If cboHorasTipo.ListIndex = -1 Then
        MsgBox "Falta seleccionar el tipo de hora a cargar.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If cboTarea.ListIndex > -1 And cboHorasTipo.ListIndex = 1 Then
        MsgBox "Horas de refinamiento solo aplican a peticiones de la SDA.", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    ' Controles varios sobre la petici�n
    If Len(CodigoCombo(cboPeticion, True)) > 0 Then
        pet_nrointerno = CodigoCombo(cboPeticion, True)
        If sp_GetUnaPeticion(pet_nrointerno) Then
            categoria = ClearNull(aplRST.Fields!categoria)
            Empresa = ClearNull(aplRST.Fields!pet_emp)
            ' Control sobre estado de la petici�n
            If Not flgFabrica Then
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|", ClearNull(aplRST!cod_estado), vbTextCompare) > 0 Then
                    MsgBox "La petici�n se encuentra en estado terminal." & vbCrLf & _
                           "No pueden imputarse horas a la misma.", vbExclamation + vbOKOnly
                    CamposObligatorios = False
                    Exit Function
                End If
            End If
            ' La petici�n debe estar clasificada para imputarle horas
            If ClearNull(aplRST.Fields!cod_clase) = "SINC" Or ClearNull(aplRST.Fields!cod_clase) = "" Then
                MsgBox "La petici�n no ha sido clasificada." & vbCrLf & _
                       "No pueden imputarse horas a la misma.", vbExclamation + vbOKOnly
                CamposObligatorios = False
                Exit Function
            End If
            ' Control de petici�n perteneciente a la SDA 2.0
            ' Si es una SDA, verifico si pertenece al la ra�z del �rbol primero, o es un nodo luego
            If categoria = "SDA" Then
                If sp_GetAgrupPetic(agrupamientoSDA2, pet_nrointerno) Then
                    flagEsSDA2 = True
                End If
                If Not flagEsSDA2 Then
                    flag = False
                    If bHayAgrupamientosSDAHijos Then
                        For i = 0 To UBound(agrupSDA2hijos)
                            If sp_GetAgrupPetic(agrupSDA2hijos(i), pet_nrointerno) Then
                                flag = True
                                flagEsSDA2 = True
                                Exit For
                            End If
                        Next i
                    End If

                    If Not flag Then
                        MsgBox "La petici�n no pertenece a " & agrupamientoSDA2Nombre & "." & vbCrLf & _
                               "No pueden imputarse horas a la misma.", vbExclamation + vbOKOnly
                        CamposObligatorios = False
                        Exit Function
                    End If
                End If
            Else
                If cboHorasTipo.ListIndex = 1 Then
                    MsgBox "Horas de refinamiento solo aplican a peticiones de la SDA.", vbExclamation + vbOKOnly
                    CamposObligatorios = False
                    Exit Function
                End If
            End If
            
            ' Control de carga de horas basado en empresa:
            '
            ' Banco
            '
            ' Peticiones del banco, no pueden cargar m�s horas a peticiones de la vieja SDA, no priorizadas (tenga prioridad BPE o no) ni tampoco
            ' peticiones categor�a PES. Excepci�n a esto es si la petici�n fu� incluida expl�cita y arbitrariamente en un agrupamiento especial
            ' para exceptuarla de esta restricci�n (hasta que sea desvinculada de dicho agrupamiento).
            '
            ' Resto de las empresas (Seguros, Vinculadas, etc.)
            '
            ' No pueden cargar horas a peticiones de la vieja SDA, pero si priorizadas (PRI) que tengan prioridad BPE. Tampoco pueden cargar horas
            ' a peticiones categor�a PES.
            Select Case Empresa
                Case 1      ' Banco
            '       If ((categoria = "SDA" And Not flagEsSDA2) Or categoria = "PRI" Or categoria = "PES") Then GMT01
                    If ((categoria = "SDA" And Not flagEsSDA2) Or categoria = "PES") Then 'GMT01
                        If Not sp_GetAgrupPetic(agrupamientoParaPriorizadasBanco, pet_nrointerno) Then
                            MsgBox "No pueden imputarse horas a esta petici�n.", vbExclamation + vbOKOnly
                            CamposObligatorios = False
                            Exit Function
                        Else    ' Si est� en el agrupamiento de excepciones, validar que tenga prioridad BPE
                           'GMT03 - INI
                            If categoria <> "PRI" Then
                                MsgBox "No pueden imputarse horas a esta petici�n.", vbExclamation + vbOKOnly
                                CamposObligatorios = False
                                Exit Function
                            End If
                           
                           ' If categoria = "PRI" Then
                           '     If sp_GetPeticionPlancab(0, pet_nrointerno) Then
                           '         If ClearNull(aplRST.Fields!prioridad) = "" Or ClearNull(aplRST.Fields!prioridad) = "0" Then
                           '             MsgBox "La petici�n no ha sido priorizada por BPE. No pueden cargarse" & vbCrLf & _
                           '                     "horas sobre peticiones a planificar que no han sido priorizadas.", vbExclamation + vbOKOnly
                           '             CamposObligatorios = False
                           '             Exit Function
                           '         End If
                           '     Else
                           '         MsgBox "La petici�n no pertenece a la planificaci�n." & vbCrLf & _
                           '                 "Solo puede imputar horas a peticiones planificadas con prioridad BPE.", vbExclamation + vbOKOnly
                           '         CamposObligatorios = False
                           '         Exit Function
                           '     End If
                           ' Else
                           '     MsgBox "No pueden imputarse horas a esta petici�n.", vbExclamation + vbOKOnly
                           '     CamposObligatorios = False
                           '     Exit Function
                           ' End If
                           'GMT03 - FIN
                        End If
                    End If
                Case Else   ' Resto: seguros, vinculadas, etc.
                    If ((categoria = "SDA" And Not flagEsSDA2) Or categoria = "PRI" Or categoria = "PES") Then
                        If sp_GetPeticionPlancab(0, pet_nrointerno) Then
                            If per_nrointerno = aplRST.Fields!per_nrointerno Then
                                If ClearNull(aplRST.Fields!prioridad) = "" Or ClearNull(aplRST.Fields!prioridad) = "0" Then
                                     If ((categoria = "SDA" And Not flagEsSDA2) Or categoria = "PES") Then  'GMT01
                                        MsgBox "La petici�n no ha sido priorizada por BPE. No pueden cargarse" & vbCrLf & _
                                                "horas sobre peticiones a planificar que no han sido priorizadas.", vbExclamation + vbOKOnly
                                        CamposObligatorios = False
                                        Exit Function
                                     End If
                                End If
                            Else
                                If ClearNull(aplRST.Fields!prioridad) = "" Or ClearNull(aplRST.Fields!prioridad) = "0" Then 'GMT01
                                    MsgBox "La petici�n no pertenece al periodo de planificaci�n actual." & vbCrLf & _
                                            "Solo puede imputar horas a peticiones planificadas del per�odo actual.", vbExclamation + vbOKOnly
                                    CamposObligatorios = False
                                    Exit Function
                                End If 'GMT01
                            End If
                        Else
                            If ((categoria = "SDA" And Not flagEsSDA2) Or categoria = "PES") Then  'GMT01
                                 MsgBox "La petici�n no pertenece al periodo de planificaci�n actual." & vbCrLf & _
                                        "Solo puede imputar horas a peticiones planificadas del per�odo actual.", vbExclamation + vbOKOnly
                                CamposObligatorios = False
                                Exit Function
                            End If 'GMT01
                        End If
                    End If
            End Select
        End If
    End If
    
    If cboTarea.ListIndex >= 0 And cboPeticion.ListIndex >= 0 Then
        MsgBox "No puede asignar simult�neamente petici�n y tarea", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If txtFechaDesde = "" Then
        MsgBox "Falta Fecha inicio informe", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If txtFechaHasta = "" Then
        MsgBox "Falta Fecha fin informe", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If txtFechaHasta.DateValue < txtFechaDesde.DateValue Then
        MsgBox ("Fecha Hasta menor que Fecha Desde")
        CamposObligatorios = False
        Exit Function
    End If
    
    If Year(txtFechaDesde.DateValue) <> Year(txtFechaHasta.DateValue) Then
        MsgBox "El per�odo de horas a cargar debe" & vbCrLf & "pertenecer al mismo a�o", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If Month(txtFechaDesde.DateValue) <> Month(txtFechaHasta.DateValue) Then
        MsgBox "El per�odo de horas a cargar debe" & vbCrLf & "pertenecer al mismo mes", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If
    
    If Val(txtHoras) * 60 + Val(txtMinutos) = 0 Then
        MsgBox "Debe asignar la cantidad de horas", vbExclamation + vbOKOnly
        CamposObligatorios = False
        Exit Function
    End If

    If Val(ClearNull(txtHoras.text)) > 545 Then
        MsgBox "La cantidad declarada no puede excederse de las 545 horas (por per�odo).", vbExclamation + vbOKOnly, "Horas declaradas"
        txtHoras.text = ""
        txtHoras.SetFocus
        CamposObligatorios = False
        Exit Function
    End If
    
    If Val(ClearNull(txtMinutos.text)) > 59 Then
        MsgBox "La cantidad declarada no puede excederse de los 59 minutos.", vbExclamation + vbOKOnly, "Minutos declarados"
        txtMinutos.text = ""
        txtMinutos.SetFocus
        CamposObligatorios = False
        Exit Function
    End If

    ' Control para que no puedan imputarse horas trabajadas a peticiones en estado "Anexadas"
    If ClearNull(CodigoCombo(Me.cboPeticion, True)) <> "" Then
        If sp_GetUnaPeticion(CodigoCombo(Me.cboPeticion, True)) Then
            If ClearNull(aplRST!cod_estado) = "ANEXAD" Then
                MsgBox "No puede asignar horas a una Petici�n ANEXADA.", vbExclamation + vbOKOnly
                CamposObligatorios = False
                Exit Function
            End If
        End If
    End If

    If EsFeriado(txtFechaDesde.DateValue) Or EsFeriado(txtFechaHasta.DateValue) Then
        If MsgBox("Alguna de las fechas a cargar o modificar es un d�a feriado o fin de semana. " & _
                  "�Continuar?", vbInformation + vbYesNo, "Atenci�n") = vbNo Then
            CamposObligatorios = False
            Exit Function
        End If
    End If
End Function

Private Function EsFeriado(dFecha As Date) As Boolean
    EsFeriado = False
    If sp_GetFeriado(dFecha, dFecha) Then
         EsFeriado = True
    End If
End Function

Private Function chkAsignada() As String
    Dim vExtendido() As String
    Dim sValExtendido As String
    
    chkAsignada = "N"
        
    ' Comprueba si una tarea esta asignada o no
    If cboTarea.ListIndex > -1 Then
        sValExtendido = DatosCombo(cboTarea)
        If ParseString(vExtendido, sValExtendido, "|") > 0 Then
            chkAsignada = vExtendido(1)
        End If
    End If
    ' Comprueba si una petici�n esta asignada o no
    If cboPeticion.ListIndex > -1 Then
        sValExtendido = DatosCombo(cboPeticion)
        If ParseString(vExtendido, sValExtendido, "|") > 0 Then
            chkAsignada = vExtendido(2)
        End If
    End If
End Function

Sub InicializarPetTar()
    Dim vExtendido() As String
    Dim sValExtendido As String
    Dim sSec As String
    Dim sSub As String
    
    Call Puntero(True)
    sSub = ""
    sSec = ""
    
    ReDim vExtendido(1)
    sValExtendido = DatosCombo(cboRecurso)
    If ParseString(vExtendido, sValExtendido, "|") > 0 Then
        sSub = vExtendido(2)    ' Grupo
        sSec = vExtendido(1)    ' Sector
    End If
   
    Call Status("Cargando peticiones...")
    cboPeticion.Clear
    
    If ClearNull(sPrefVisualizar) = "USR" Then
        If sp_GetPeticionAreaRec(Null, "EVALUA|ESTIMA|PLANIF|PLANOK|EJECUC", CodigoCombo(cboRecurso), 0) Then
            Do While Not aplRST.EOF
                cboPeticion.AddItem padRight(aplRST!pet_nroasignado, 6) & " : " & Trim(aplRST!Titulo) & ESPACIOS & ESPACIOS & " || " & Trim(aplRST!pet_nrointerno) & " |N"
                aplRST.MoveNext
            Loop
        End If
    ElseIf ClearNull(sSub) <> "" Then
        If sp_GetPeticionGrupoXt(0, Null, ClearNull(sSub), "EVALUA|ESTIMA|PLANIF|PLANOK|EJECUC") Then
            Do While Not aplRST.EOF
                cboPeticion.AddItem padRight(aplRST!pet_nroasignado, 6) & " : " & Trim(aplRST!Titulo) & ESPACIOS & ESPACIOS & " || " & Trim(aplRST!pet_nrointerno) & " |N"
                aplRST.MoveNext
            Loop
        End If
    ElseIf ClearNull(sSec) <> "" Then
        If sp_GetPeticionSectorXt(0, ClearNull(sSec), "EVALUA|ESTIMA|PLANIF|PLANOK|EJECUC") Then
            Do While Not aplRST.EOF
                cboPeticion.AddItem padRight(aplRST!pet_nroasignado, 6) & " : " & Trim(aplRST!Titulo) & ESPACIOS & ESPACIOS & " || " & Trim(aplRST!pet_nrointerno) & " |N"
                aplRST.MoveNext
            Loop
        End If
    End If
    cboPeticion.ListIndex = -1
   
    Call Status("Cargando tareas...")
    cboTarea.Clear
    If ClearNull(sSec) <> "" Then
        If sp_GetTareaSectorXt("", ClearNull(sSec)) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST!flg_habil) = "S" Then
                    cboTarea.AddItem padLeft(aplRST!cod_tarea, 8) & " : " & Trim(aplRST!nom_tarea) & ESPACIOS & ESPACIOS & ESPACIOS & " ||N"
                End If
                aplRST.MoveNext
            Loop
        End If
    End If
    cboTarea.ListIndex = -1
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Sub CargarGrid()
    Dim nPos As Integer
    
    txtFechaDesde.text = ""
    txtFechaHasta.text = ""
    txtHoras.text = ""
    txtMinutos.text = ""
    txtObservaciones.text = ""
    txtCodtarea = ""
    txtPetnroasig = ""
    cboPeticion.ListIndex = -1
    cboTarea.ListIndex = -1
    
    Call Puntero(True)
    With grdDatos
        .visible = False
        .Clear
        .Font = "Tahoma"
        .HighLight = flexHighlightAlways
        .Rows = 1
        .cols = colTOT_COLS
        .RowHeightMin = 285
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        ' Visibles
        .TextMatrix(0, colFEDESDE) = "Desde": .ColWidth(colFEDESDE) = 1200
        .TextMatrix(0, colFEHASTA) = "Hasta": .ColWidth(colFEHASTA) = 1200
        .TextMatrix(0, colFMTHORAS) = "Horas": .ColWidth(colFMTHORAS) = 900
        .TextMatrix(0, colOBJTIPO) = "Tipo": .ColWidth(colOBJTIPO) = 0: .ColAlignment(colOBJTIPO) = 0
        .TextMatrix(0, colOBJCODIGO) = "Nro./Tarea ": .ColWidth(colOBJCODIGO) = 1800: .ColAlignment(colOBJCODIGO) = flexAlignLeftCenter
        .TextMatrix(0, colOBJNOMBRE) = "Descripci�n": .ColWidth(colOBJNOMBRE) = 5500: .ColAlignment(colOBJNOMBRE) = flexAlignLeftCenter
        .TextMatrix(0, colOBSERV) = "Observaciones": .ColWidth(colOBSERV) = 4000: .ColAlignment(colOBSERV) = flexAlignLeftCenter
        .TextMatrix(0, colVINCULADO) = "Vinculado": .ColWidth(colVINCULADO) = 900: .ColAlignment(colVINCULADO) = flexAlignCenterCenter
        .TextMatrix(0, colTIPONOM) = "Tipo": .ColWidth(colTIPONOM) = 1200: .ColAlignment(colTIPONOM) = flexAlignLeftCenter
        ' Invisibles
        .TextMatrix(0, colCODRECURSO) = "Recurso": .ColWidth(colCODRECURSO) = 0: .ColAlignment(colCODRECURSO) = 0
        .TextMatrix(0, colCODPETIC) = "NROINT": .ColWidth(colCODPETIC) = 0
        .TextMatrix(0, colASIPETIC) = "PETICI�N": .ColWidth(colASIPETIC) = 0
        .TextMatrix(0, colNOMPETIC) = "T�tulo": .ColWidth(colNOMPETIC) = 0: .ColAlignment(colNOMPETIC) = 0
        .TextMatrix(0, colCODTAREA) = "COD.TAREA": .ColWidth(colCODTAREA) = 0: .ColAlignment(colCODTAREA) = 0
        .TextMatrix(0, colNOMTAREA) = "DESCRIPCI�N": .ColWidth(colNOMTAREA) = 0: .ColAlignment(colNOMTAREA) = 0
        .TextMatrix(0, colHORAS) = "MINTS": .ColWidth(colHORAS) = 0
        .TextMatrix(0, colSINASIGNAR) = "S/ASIG": .ColWidth(colSINASIGNAR) = 0
        .TextMatrix(0, colTipo) = "TIPO": .ColWidth(colTipo) = 0
        
        .ColWidth(colSRTFEDESDE) = 0:
        .ColWidth(colSRTFEHASTA) = 0:
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With

    If Not sp_GetHorasTrabajadasXt(CodigoCombo(Me.cboRecurso), glFechaDesde, glFechaHasta) Then GoTo finx
    flgEnCarga = True
    With grdDatos
        Do While Not aplRST.EOF
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODRECURSO) = ClearNull(aplRST.Fields.Item("cod_recurso"))
            .TextMatrix(.Rows - 1, colCODTAREA) = ClearNull(aplRST.Fields.Item("cod_tarea"))
            .TextMatrix(.Rows - 1, colNOMTAREA) = ClearNull(aplRST.Fields.Item("nom_tarea"))
            .TextMatrix(.Rows - 1, colCODPETIC) = ClearNull(aplRST.Fields.Item("pet_nrointerno"))
            .TextMatrix(.Rows - 1, colASIPETIC) = ClearNull(aplRST.Fields.Item("pet_nroasignado"))
            .TextMatrix(.Rows - 1, colNOMPETIC) = ClearNull(aplRST.Fields.Item("pet_titulo"))
            .TextMatrix(.Rows - 1, colFEDESDE) = IIf(Not IsNull(aplRST.Fields.Item("fe_desde")), Format(aplRST.Fields.Item("fe_desde"), "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colFEHASTA) = IIf(Not IsNull(aplRST.Fields.Item("fe_hasta")), Format(aplRST.Fields.Item("fe_hasta"), "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colHORAS) = ClearNull(aplRST.Fields.Item("horas"))
            .TextMatrix(.Rows - 1, colFMTHORAS) = hrsFmtHora(Val(ClearNull(aplRST.Fields.Item("horas"))))
            .TextMatrix(.Rows - 1, colOBSERV) = ClearNull(aplRST.Fields.Item("observaciones"))
            .TextMatrix(.Rows - 1, colSINASIGNAR) = ClearNull(aplRST.Fields.Item("trabsinasignar"))
            .TextMatrix(.Rows - 1, colSRTFEDESDE) = "X" & IIf(Not IsNull(aplRST.Fields.Item("fe_desde")), Format(aplRST.Fields.Item("fe_desde"), "yyyymmdd"), "") & IIf(Not IsNull(aplRST.Fields.Item("fe_hasta")), Format(aplRST.Fields.Item("fe_hasta"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colSRTFEHASTA) = "X" & IIf(Not IsNull(aplRST.Fields.Item("fe_hasta")), Format(aplRST.Fields.Item("fe_hasta"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colVINCULADO) = IIf(ClearNull(aplRST.Fields!trabsinasignar) = "S", "No", "Si")
            .TextMatrix(.Rows - 1, colTipo) = ClearNull(aplRST.Fields!Tipo)
            .TextMatrix(.Rows - 1, colTIPONOM) = IIf(ClearNull(aplRST.Fields!Tipo) = "0", "Desarrollo", "Refinamiento")
            If Val(.TextMatrix(.Rows - 1, colCODPETIC)) > 0 Then
                .TextMatrix(.Rows - 1, colOBJTIPO) = "PET"
                .TextMatrix(.Rows - 1, colOBJCODIGO) = "Petici�n N� " & .TextMatrix(.Rows - 1, colASIPETIC)
                .TextMatrix(.Rows - 1, colOBJNOMBRE) = .TextMatrix(.Rows - 1, colNOMPETIC)
                nPos = PosicionCombo(cboPeticion, .TextMatrix(.Rows - 1, colCODPETIC), True)
                If nPos = -1 Then
                    cboPeticion.AddItem padRight(.TextMatrix(.Rows - 1, colASIPETIC), 6) & " : " & .TextMatrix(.Rows - 1, colNOMPETIC) & ESPACIOS & ESPACIOS & ESPACIOS & " || " & .TextMatrix(.Rows - 1, colCODPETIC) & " |" & .TextMatrix(.Rows - 1, colSINASIGNAR)
                End If
            End If
            If ClearNull(.TextMatrix(.Rows - 1, colCODTAREA)) <> "" Then
                .TextMatrix(.Rows - 1, colOBJTIPO) = "TAR"
                .TextMatrix(.Rows - 1, colOBJCODIGO) = "Tarea - " & .TextMatrix(.Rows - 1, colCODTAREA)
                .TextMatrix(.Rows - 1, colOBJNOMBRE) = .TextMatrix(.Rows - 1, colNOMTAREA)
                nPos = PosicionCombo(cboTarea, ClearNull(.TextMatrix(.Rows - 1, colCODTAREA)))
                If nPos = -1 Then
                    cboTarea.AddItem padLeft(.TextMatrix(.Rows - 1, colCODTAREA), 8) & " : " & .TextMatrix(.Rows - 1, colNOMTAREA) & ESPACIOS & ESPACIOS & ESPACIOS & " || " & .TextMatrix(.Rows - 1, colSINASIGNAR)
                End If
            End If
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
            aplRST.MoveNext
            DoEvents
        Loop
        .ColSel = 0
        .ColSel = .cols - 1
        grdDatos.visible = True
    End With
    
finx:
    flgEnCarga = False
    Call MostrarSeleccion
    If Not grdDatos.visible Then grdDatos.visible = True
    cboPeticion.visible = True
    cboTarea.visible = True
    flgEnCarga = False
    DoEvents
    Call Puntero(False)
End Sub

Private Sub MostrarSeleccion()
    Dim nPos As Integer
    If flgEnCarga Then Exit Sub
    
    cmdHsXDia.Enabled = False
    cmdPeticionesAsignadas.Enabled = False
    With grdDatos
        If .rowSel > 0 Then
            If .TextMatrix(.rowSel, 0) <> "" Then
                flgEnCarga = True
                cboTarea.ListIndex = PosicionCombo(Me.cboTarea, .TextMatrix(.rowSel, colCODTAREA))
                cboPeticion.ListIndex = PosicionCombo(Me.cboPeticion, .TextMatrix(.rowSel, colCODPETIC), True)
                txtFechaDesde.text = ClearNull(.TextMatrix(.rowSel, colFEDESDE))
                txtFechaHasta.text = ClearNull(.TextMatrix(.rowSel, colFEHASTA))
                txtHoras = hrsGetHoras(Val(ClearNull(.TextMatrix(.rowSel, colHORAS))))
                txtMinutos = hrsGetMinutos(Val(ClearNull(.TextMatrix(.rowSel, colHORAS))))
                txtObservaciones = ClearNull(.TextMatrix(.rowSel, colOBSERV))
                txtCodtarea = ""
                txtCodtarea = CodigoCombo(cboTarea)
                txtPetnroasig = ""
                txtPetnroasig = CodigoCombo(cboPeticion)
                cboHorasTipo.ListIndex = PosicionCombo(Me.cboHorasTipo, .TextMatrix(.rowSel, colTipo), True)
            End If
        Else
            .HighLight = flexHighlightNever
            .row = 0
        End If
    End With
    flgEnCarga = False
    DoEvents
End Sub

Private Sub grdDatos_Click()
    flgEnCarga = True
    bSorting = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    bSorting = False
    flgEnCarga = False
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Private Sub grdDatos_RowColChange()
    If Not bFormateando Then Call MostrarSeleccion
End Sub

Function valTareaHabil(xTarea) As Boolean
    If ClearNull(xTarea) = "" Then
        valTareaHabil = True
        Exit Function
    End If
    valTareaHabil = False
    If sp_GetTarea(xTarea) Then
        If ClearNull(aplRST!flg_habil) = "S" Then
            valTareaHabil = True
        End If
        aplRST.Close
    End If
End Function

Function valPeticionHabil(xTarea, xFecha) As Boolean
    If Val(ClearNull(xTarea)) = 0 Then
        valPeticionHabil = True
        Exit Function
    End If
    valPeticionHabil = False
    If sp_GetUnaPeticion(Val(xTarea)) Then
        If InStr(1, "TERMIN|CANCEL", ClearNull(aplRST!cod_estado)) = 0 Then
            valPeticionHabil = True
        ElseIf xFecha <= aplRST!fe_estado Then
            valPeticionHabil = True
        End If
        aplRST.Close
    End If
End Function

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
    Call DetenerScroll(grdHorasCargadas)
End Sub

Private Sub ReporteNuevo()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    
    Call Puntero(True)
    sReportName = "\hsrecu6.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crPortrait
        .ReportTitle = "Horas trabajadas"
        .ReportComments = "Recurso: " & ClearNull(ReplaceApostrofo(TextoCombo(cboRecurso))) & " / Entre: " & Format(glFechaDesde, "dd/mm/yyyy") & " y " & Format(glFechaHasta, "dd/mm/yyyy")
    End With
    Call FuncionesCrystal10.ConectarDB(crReport)

    On Error GoTo ErrHandler
    
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@cod_recurso": crParamDef.AddCurrentValue (ClearNull(CodigoCombo(cboRecurso, False)))
            Case "@fechadesde": crParamDef.AddCurrentValue (IIf(IsNull(glFechaDesde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(glFechaDesde, "yyyymmdd")))
            Case "@fechahasta": crParamDef.AddCurrentValue (IIf(IsNull(glFechaHasta), Format(date, "yyyymmdd"), Format(glFechaHasta, "yyyymmdd")))
        End Select
    Next
 
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
 
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub cmdPeticionesAsignadas_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    With auxHorasRecCategoria
        sRecurso = CodigoCombo(cboRecurso)
        Load auxHorasRecCategoria
        .CargarGrid
        .Show 1
        Call LockProceso(False)
        If .pet_nrointerno > 0 Then
            cboPeticion.ListIndex = PosicionCombo(cboPeticion, .pet_nrointerno, True)
        End If
        Unload auxHorasRecCategoria
    End With
End Sub

Private Sub CargarFeriados()
    Dim i As Long
    Dim Dias_Habiles As Long
    Dim Total_HorasNominales As Long
    
    Debug.Print "CargarFeriados()"
    
    If hsNomi = 0 Then
        If sp_GetRecurso(CodigoCombo(cboRecurso, False), "R") Then
            hsNomi = Val(ClearNull(aplRST.Fields!horasdiarias))
            lblRecurso.Caption = "Horas nominales diarias del recurso: " & hsNomi
        End If
    Else
        lblRecurso.Caption = "Horas nominales diarias del recurso: " & hsNomi
    End If
    
    Dias_Habiles = DateDiff("d", glFechaDesde, glFechaHasta, vbSunday) + 1
    
    i = 0
    If sp_GetFeriado(glFechaDesde, glFechaHasta) Then
        If Not aplRST.EOF Then
            Do While Not aplRST.EOF
                ReDim Preserve vFeriados(i)
                vFeriados(i) = aplRST.Fields!fecha
                aplRST.MoveNext
                i = i + 1
                DoEvents
            Loop
        End If
    End If
    If i > 0 Then
        ' Aqu� determina el total de horas nominales para el per�odo seleccionado
        Dias_Habiles = Dias_Habiles - i
        Total_HorasNominales = Dias_Habiles * hsNomi
        lblTotalesHorasNominales = "Total horas nominales del recurso: " & Format(Total_HorasNominales, "###,###,##0")
        bHayFeriados = True
    Else
        Dias_Habiles = Dias_Habiles - i
        Total_HorasNominales = Dias_Habiles * hsNomi
        lblTotalesHorasNominales = "Total horas nominales del recurso: " & Format(Total_HorasNominales, "###,###,##0")
    End If
End Sub

Private Sub CargarGridHoras()
    lblFecha.Caption = "Horas diarias entre el: " & Format(glFechaDesde, "dd/mm/yyyy") & " y " & Format(glFechaHasta, "dd/mm/yyyy")
    lHoras_Cargadas = 0
    lHoras_Nominales = 0
        
    With grdHorasCargadas
        .visible = False
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .cols = 5
        .RowHeightMin = 285
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .TextMatrix(0, colFECHA) = "Fecha": .ColWidth(colFECHA) = 1200: .ColAlignment(colFECHA) = flexAlignLeftCenter
        .TextMatrix(0, colDIASEMANA) = "D�a": .ColWidth(colDIASEMANA) = 890: .ColAlignment(colDIASEMANA) = flexAlignLeftCenter
        .TextMatrix(0, colHORASTOT) = "Horas": .ColWidth(colHORASTOT) = 950: .ColAlignment(colHORASTOT) = flexAlignCenterCenter
        .TextMatrix(0, colHORASFAULT) = "Restan": .ColWidth(colHORASFAULT) = 950: .ColAlignment(colHORASFAULT) = flexAlignCenterCenter
        .TextMatrix(0, colHORASFAULT + 1) = "Aux": .ColWidth(colHORASFAULT + 1) = 0:
        Call CambiarEfectoLinea(grdHorasCargadas, prmGridEffectFontBold)
    End With
    If chkFeriados.value = 1 Then
        ' Devuelve los d�as trabajados para el per�odo incluyendo S�bados, Domingos y Feriados
        If Not sp_rptHorasTrabajadasRecurso2(CodigoCombo(cboRecurso, False), glFechaDesde, glFechaHasta) Then
            Exit Sub
        End If
    Else
        ' Devuelve los d�as trabajados para el per�odo sin incluir ni S�bados ni Domingos ni Feriados (est�ndar)
        If Not sp_rptHorasTrabajadasRecurso(CodigoCombo(cboRecurso, False), glFechaDesde, glFechaHasta) Then
            Exit Sub
        End If
    End If
    
    With grdHorasCargadas
        Do While Not aplRST.EOF
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colFECHA) = IIf(Not IsNull(aplRST.Fields.Item("fe_infor")), Format(aplRST.Fields.Item("fe_infor"), "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colDIASEMANA) = WeekDayName(Weekday(aplRST.Fields!fe_infor), True)        ' add -004- a.
            .TextMatrix(.Rows - 1, colHORASTOT) = IIf(Val(ClearNull(aplRST.Fields.Item("horas_trab"))) = 0, "-", hrsFmtHora(Val(ClearNull(aplRST.Fields.Item("horas_trab")))))     ' upd -004- a.
            If hsNomi * 60 > Val(ClearNull(aplRST.Fields.Item("horas_trab"))) Then
                .TextMatrix(.Rows - 1, colHORASFAULT) = hrsFmtHora(hsNomi * 60 - Val(ClearNull(aplRST.Fields.Item("horas_trab"))))
            Else
                .TextMatrix(.Rows - 1, colHORASFAULT) = "-"
            End If
            .TextMatrix(.Rows - 1, colHORASFAULT + 1) = IIf(hsNomi * 60 - Val(ClearNull(aplRST.Fields.Item("horas_trab"))) < 0, 0, hsNomi * 60 - Val(ClearNull(aplRST.Fields.Item("horas_trab"))))  ' upd -004- b.
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdHorasCargadas, prmGridFillRowColorLightGrey2)
            If bHayFeriados Then
                If EsFechaFeriado(aplRST.Fields.Item("fe_infor")) Then
                    .TextMatrix(.Rows - 1, colHORASFAULT) = "-"
                    Call PintarLinea(grdHorasCargadas, prmGridFillRowColorRose)
                Else
                    lHoras_Cargadas = lHoras_Cargadas + Val(ClearNull(aplRST.Fields.Item("horas_trab")))
                    lHoras_Nominales = lHoras_Nominales + .TextMatrix(.Rows - 1, colHORASFAULT + 1)
                End If
            Else
                lHoras_Cargadas = lHoras_Cargadas + Val(ClearNull(aplRST.Fields.Item("horas_trab")))
                lHoras_Nominales = lHoras_Nominales + .TextMatrix(.Rows - 1, colHORASFAULT + 1)
            End If
            aplRST.MoveNext
            DoEvents
        Loop
        .visible = True
    End With
    lblHsCargadas = hrsFmtHora(lHoras_Cargadas): lblHorasCargadasPeriodo.Caption = "Horas cargadas del per�odo: " & lblHsCargadas
    lblHsNominales = hrsFmtHora(lHoras_Nominales): lblHorasRestantes.Caption = "Horas restantes del per�odo: " & lblHsNominales
End Sub

Private Function EsFechaFeriado(dFecha As Date) As Boolean
    Dim i As Long

    EsFechaFeriado = False    ' Inicializo la funci�n

    i = 0

    For i = 0 To UBound(vFeriados)
        If dFecha >= vFeriados(i) Then
            If vFeriados(i) = dFecha Then
                EsFechaFeriado = True
                Exit For
            End If
        Else
            Exit For
        End If
    Next i
End Function

Private Function WeekDayName(Number As Byte, EmpiezaDomigo As Boolean) As String
    If EmpiezaDomigo Then
        Select Case Number
            Case 1: WeekDayName = "Domingo"
            Case 2: WeekDayName = "Lunes"
            Case 3: WeekDayName = "Martes"
            Case 4: WeekDayName = "Mi�rcoles"
            Case 5: WeekDayName = "Jueves"
            Case 6: WeekDayName = "Viernes"
            Case 7: WeekDayName = "S�bado"
        End Select
    Else
        Select Case Number
            Case 1: WeekDayName = "Lunes"
            Case 2: WeekDayName = "Martes"
            Case 3: WeekDayName = "Mi�rcoles"
            Case 4: WeekDayName = "Jueves"
            Case 5: WeekDayName = "Viernes"
            Case 6: WeekDayName = "S�bado"
            Case 7: WeekDayName = "Domingo"
        End Select
    End If
End Function

Private Function hrsGetHoras(minutos As Long) As Long
    hrsGetHoras = minutos \ 60
End Function

Private Function hrsGetMinutos(minutos As Long) As Long
    hrsGetMinutos = minutos Mod 60
End Function

Private Function hrsFmtHora(minutos As Long) As String
    hrsFmtHora = hrsGetHoras(minutos) & ":" & Right("0" & hrsGetMinutos(minutos), 2)
End Function

Private Sub tbBotonera_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnAgregar"
            If Not LockProceso(True) Then Exit Sub
            sOpcionSeleccionada = AGREGAR
            Call HabilitarBotones(1, False)
        Case "btnModificar"
            If validarModificar Then
                sOpcionSeleccionada = MODIFICAR
                Call HabilitarBotones(1, False)
            End If
        Case "btnEliminar"
            If validarEliminar Then
                sOpcionSeleccionada = ELIMINAR
                Call HabilitarBotones(1, False)
            End If
        Case "btnConfirmar"
            Call Confirmar
        Case "btnCancelar"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case "btnActualizar"
            Call CargarGrid
            Call CargarGridHoras
        Case "btnImprimir"
            Call ReporteNuevo
        Case "btnReferencia"
            Call ntvMessageBox("Ayuda", sMensaje, vbInformation + vbOKOnly)
    End Select
End Sub

Private Function validarModificar() As Boolean
   
    If Not LockProceso(True) Then Exit Function
    validarModificar = False
    If Len(CodigoCombo(cboPeticion, True)) > 0 Or Len(CodigoCombo(cboTarea, True)) > 0 Then
        If Len(CodigoCombo(cboPeticion, True)) > 0 Then
            If sp_GetUnaPeticion(CodigoCombo(cboPeticion, True)) Then
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|", ClearNull(aplRST!cod_estado), vbTextCompare) > 0 Then
                    MsgBox "La petici�n se encuentra en estado terminal." & vbCrLf & _
                           "No pueden imputarse horas a la misma.", vbExclamation + vbOKOnly
                    Call LockProceso(False)
                    Exit Function
                End If
                If ClearNull(aplRST.Fields!cod_clase) = "SINC" Or ClearNull(aplRST.Fields!cod_clase) = "" Then
                    MsgBox "La petici�n no ha sido clasificada." & vbCrLf & _
                           "No pueden imputarse horas a la misma.", vbExclamation + vbOKOnly
                    Call LockProceso(False)
                    Exit Function
                End If
            End If
        End If
    Else
        MsgBox "Debe seleccionar una petici�n o una tarea para modificar.", vbExclamation + vbOKOnly, "Petici�n o tarea"
        Call LockProceso(False)
        Exit Function
    End If
    validarModificar = True
    Call LockProceso(False)
End Function

Private Function validarEliminar() As Boolean
    If Not LockProceso(True) Then Exit Function
    validarEliminar = False
    If Not (Len(CodigoCombo(cboPeticion, True)) > 0 Or Len(CodigoCombo(cboTarea, True)) > 0) Then
        MsgBox "Debe seleccionar una petici�n o una tarea para eliminar.", vbExclamation + vbOKOnly, "Petici�n o tarea"
        Call LockProceso(False)
        Exit Function
    End If
    validarEliminar = True
    Call LockProceso(False)
End Function

Private Function Mostrar_Grafico(MsChart As MsChart) As T_Info
    'Dim Avalables As LARGE_INTEGER, Total As LARGE_INTEGER
    'Dim Libres As LARGE_INTEGER, dTotal As Double, dLibre As Double
    'Dim ret As Long
    Dim vCategorias(7) As Long

    'Llamada al api para retornar los valores
    'ret = GetDiskFreeSpaceEx(Drive, Avalables, Total, Libres)
      
    'Conversi�n a bytes del espacio total y el espacio libre
    'dTotal = Entero_a_Double(Total.LowPart, Total.HighPart)
    'dLibre = Entero_a_Double(Libres.LowPart, Libres.HighPart)
    
    vCategorias(0) = 250        ' REG
    vCategorias(1) = 100        ' SDA
    vCategorias(2) = 150        ' PES
    vCategorias(3) = 150        ' ENG
    vCategorias(4) = 150        ' PRI
    vCategorias(5) = 150        ' CSI
    vCategorias(6) = 150        ' LCA
    vCategorias(7) = 150        ' SPR
    
    
    ' retorna a la funci�n los valores convertidos a String
    'Mostrar_Grafico_Drive.Capacidad = Size(dTotal)
    'Mostrar_Grafico_Drive.Libre = Size(dLibre)
    'Mostrar_Grafico_Drive.Usado = Size(dTotal - dLibre)
      
    ' Leyenda o t�tulo del MsChart ( muestra la capacidad de la unidad )
    MsChart.RowLabel = "Distribuci�n de horas objetivo"
  
    'Columna 1 ( para el espacio libre )
    MsChart.Column = 1
    MsChart.Data = vCategorias(0)
    MsChart.ColumnLabel = " REG : " & vCategorias(0)
      
    'Columna 2 ( para el espacio utilizado )
    MsChart.Column = 2
    MsChart.Data = vCategorias(1)
    MsChart.ColumnLabel = " SDA : " & vCategorias(1)
    
    MsChart.Column = 3
    MsChart.Data = vCategorias(2)
    MsChart.ColumnLabel = " PES : " & vCategorias(2)
    
    MsChart.Column = 4
    MsChart.Data = vCategorias(3)
    MsChart.ColumnLabel = " ENG : " & vCategorias(3)
    
    ' ASigna el estilo del gr�fico
    'MsChart.chartType = Estilo
End Function

Public Function ntvMessageBox(msgboxTitle As String, msgboxText As String, Optional msgboxStyle As VbMsgBoxStyle = vbOKOnly) As Long
    Dim retValue As Long
    Dim APIOffset As Long
    Dim ret As Long
    Dim ASMString As String
    
    ret = LoadLibrary("User32.dll")
    ret = GetProcAddress(ret, "MessageBoxW")
    
    Dim EmptyString As String
    Dim MessageString As String
    
    EmptyString = msgboxTitle
    MessageString = msgboxText
    Dim ASMArray() As Byte
    ASMString = "609C3E8B442434FF303E8B442434FF303E8B442434FF306A00E8"
    ReDim ASMArray(0 To 39)
    APIOffset = ret - (VarPtr(ASMArray(0)) + (Len(ASMString) / 2)) - 4
    ASMString = ASMString & MakeDword(APIOffset) & "3E8B4C242889019D61C3"
    Call ExecuteASM(ASMString, ASMArray(), VarPtr(retValue), VarPtr(msgboxText), VarPtr(msgboxTitle), VarPtr(msgboxStyle))
    ntvMessageBox = retValue
End Function

Private Sub ExecuteASM(ASMString As String, ByRef ASMArray() As Byte, ReturnValue As Long, param1 As Long, param2 As Long, Param3 As Long)
    Dim y As Long, x As Long
    
    y = 0
    For x = 0 To Len(ASMString) - 2 Step 2
        ASMArray(y) = Val("&H" & Mid(ASMString, x + 1, 2))
        y = y + 1
    Next x
    Call CallWindowProc(VarPtr(ASMArray(0)), ReturnValue, param1, param2, Param3)
End Sub

Public Function MakeDword(theNum As Long) As String
    Dim Temp As String, Temp1 As String
    
    Temp = Hex(theNum)
    If Len(Temp) < 8 Then
       Temp = String(8 - Len(Temp), "0") & Temp
    End If
    Temp1 = Mid(Temp, 7, 2)
    Temp1 = Temp1 & Mid(Temp, 5, 2)
    Temp1 = Temp1 & Mid(Temp, 3, 2)
    Temp1 = Temp1 & Mid(Temp, 1, 2)
       MakeDword = Temp1
End Function


