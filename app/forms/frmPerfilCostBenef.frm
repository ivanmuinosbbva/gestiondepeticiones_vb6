VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPerfilCostBenef 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Perfil Costo Beneficio"
   ClientHeight    =   6315
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame pnlBotones 
      Height          =   6195
      Left            =   8220
      TabIndex        =   8
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Direcci�n"
         Top             =   3570
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la direcci�n selecionada"
         Top             =   4080
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Direcci�n seleccionada"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   5610
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3825
      Left            =   6210
      TabIndex        =   7
      Top             =   0
      Width           =   1995
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2340
      Left            =   0
      TabIndex        =   3
      Top             =   3870
      Width           =   8205
      Begin VB.ComboBox cboHabil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1020
         Width           =   1900
      End
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   315
         Left            =   1350
         TabIndex        =   1
         Top             =   330
         Width           =   1000
         _ExtentX        =   1773
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   315
         Left            =   1350
         TabIndex        =   2
         Top             =   675
         Width           =   3285
         _ExtentX        =   5794
         _ExtentY        =   556
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
      End
      Begin VB.Label Label10 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   15
         Top             =   1043
         Width           =   1100
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   345
         Width           =   1100
      End
      Begin VB.Label Label2 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   694
         Width           =   1100
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   30
         Visible         =   0   'False
         Width           =   105
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3810
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   6720
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
   End
End
Attribute VB_Name = "frmPerfilCostBenef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. - FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.

Option Explicit

Const colCODPerfilCostBenef = 0
Const colNOMPerfilCostBenef = 1
Const colHabil = 2
Dim sOpcionSeleccionada As String

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
'    Select Case sOpcionSeleccionada
'        Case "A"
'            If CamposObligatorios Then
'                If sp_InsertPerfilCostBenef(txtCodigo, txtDescripcion, CodigoCombo(Me.cboHabil)) Then
'                    Call HabilitarBotones(0)
'                End If
'            End If
'        Case "M"
'            If CamposObligatorios Then
'                If CodigoCombo(Me.cboHabil) <> "S" Then
'                    If MsgBox("Al inhabilitar la el Balance-Rubro dejar� inhabilitado todo su �rbol jer�rquico. �Contin�a?", vbYesNo) = vbNo Then
'                        Exit Sub
'                    End If
'                End If
'                If sp_UpdatePerfilCostBenef(txtCodigo, txtDescripcion, CodigoCombo(Me.cboHabil)) Then
'                    With grdDatos
'                        If .RowSel > 0 Then
'                           .TextMatrix(.RowSel, colNOMPerfilCostBenef) = txtDescripcion
'                           .TextMatrix(.RowSel, colHabil) = CodigoCombo(Me.cboHabil)
'                        End If
'                    End With
'                    Call HabilitarBotones(0, False)
'                End If
'            End If
'        Case "E"
'            If sp_DeletePerfilCostBenef(txtCodigo) Then
'                Call HabilitarBotones(0)
'            End If
'    End Select
End Sub
Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub
Private Sub Form_Load()
    Call doLog("frmPerfilCostBenef_Form_Load_1")
    Call InicializarPantalla
    Call doLog("frmPerfilCostBenef_Form_Load_2")
    Call FormCenter(Me, mdiPrincipal, False)
End Sub
Sub InicializarPantalla()
    Call doLog("frmPerfilCostBenef_InicializarPantalla_1")
    Me.Tag = ""
    Call doLog("frmPerfilCostBenef_InicializarPantalla_2")
    sOpcionSeleccionada = ""
    Call doLog("frmPerfilCostBenef_InicializarPantalla_3")
    cboHabil.AddItem "S : Habilitado"
    Call doLog("frmPerfilCostBenef_InicializarPantalla_4")
    cboHabil.AddItem "N : Inhabilitado"
    Call doLog("frmPerfilCostBenef_InicializarPantalla_5")
    
    Show
    Call doLog("frmPerfilCostBenef_InicializarPantalla_8")
    'Call LimpiarForm(Me)
    Call HabilitarBotones(0)
    Call doLog("frmPerfilCostBenef_InicializarPantalla_9")
End Sub
Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.Visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            'grdDatos.SetFocus
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.Visible = True
                    txtCodigo = "": txtDescripcion = ""
                    cboHabil.ListIndex = 0
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub
Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(txtCodigo.Text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.Text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtDescripcion.Text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtDescripcion.Text, ":") > 0 Then
        MsgBox ("a Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
End Function
Sub CargarGrid()
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colCODPerfilCostBenef) = "CODIGO"
        .TextMatrix(0, colNOMPerfilCostBenef) = "DESCRIPCION"
        .TextMatrix(0, colHabil) = "HABILIT"
        .ColWidth(colHabil) = 750: .ColAlignment(colHabil) = flexAlignCenterCenter
        .ColWidth(colCODPerfilCostBenef) = 1000: .ColAlignment(colCODPerfilCostBenef) = 0
        .ColWidth(colNOMPerfilCostBenef) = 4300: .ColAlignment(colNOMPerfilCostBenef) = 0
    End With
    'If Not sp_GetPerfilCostBenef("") Then Exit Sub ' Volver a habilitar
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODPerfilCostBenef) = IIf(Not IsNull(aplRST.Fields.Item("cod_PerfilCostBenef")), aplRST.Fields.Item("cod_PerfilCostBenef"), "")
            .TextMatrix(.Rows - 1, colNOMPerfilCostBenef) = IIf(Not IsNull(aplRST.Fields.Item("nom_PerfilCostBenef")), aplRST.Fields.Item("nom_PerfilCostBenef"), "")
            .TextMatrix(.Rows - 1, colHabil) = IIf(ClearNull(aplRST.Fields.Item("flg_habil")) = "N", "N", "S")
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    '{ add -002- a.
    Dim i As Long
    
    With grdDatos
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .FocusRect = flexFocusNone
    End With
    '}
    Call MostrarSeleccion
End Sub
Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colCODPerfilCostBenef) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODPerfilCostBenef))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNOMPerfilCostBenef))
                cboHabil.ListIndex = PosicionCombo(Me.cboHabil, .TextMatrix(.RowSel, colHabil))
            End If
        End If
    End With
End Sub
Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub
Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}
