VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRecursosConsulta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consulta de usuarios y recursos"
   ClientHeight    =   7005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9885
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRecursosConsulta.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7005
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.StatusBar sbMain 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Top             =   6750
      Width           =   9885
      _ExtentX        =   17436
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Text            =   "Total de recursos:"
            TextSave        =   "Total de recursos:"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   3254
            Text            =   "Recursos seleccionados:"
            TextSave        =   "Recursos seleccionados:"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtNombreApellido 
      Height          =   290
      Left            =   3000
      TabIndex        =   1
      Top             =   600
      Width           =   5055
   End
   Begin VB.CommandButton cmdCriterios 
      Caption         =   "M�s crit&erios"
      Height          =   310
      Left            =   8520
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   590
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4215
      Left            =   120
      TabIndex        =   2
      Top             =   2520
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   7435
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      HighLight       =   2
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraCriterios 
      Caption         =   "Otros criterios de b�squeda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00D28D55&
      Height          =   1455
      Left            =   120
      TabIndex        =   4
      Top             =   960
      Visible         =   0   'False
      Width           =   9735
      Begin VB.ComboBox cmbArea 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   600
         Width           =   2655
      End
      Begin VB.CheckBox chkSoloActivos 
         Caption         =   "Solo activos"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   8400
         TabIndex        =   9
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox cmbPerfiles 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2880
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   240
         Width           =   2655
      End
      Begin AT_MaskText.MaskText txtArea 
         Height          =   300
         Left            =   2880
         TabIndex        =   12
         Top             =   960
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblTitulos 
         AutoSize        =   -1  'True
         Caption         =   "�rea:"
         Height          =   195
         Index           =   3
         Left            =   2280
         TabIndex        =   11
         Top             =   660
         Width           =   405
      End
      Begin VB.Label lblTitulos 
         AutoSize        =   -1  'True
         Caption         =   "Por perfil:"
         Height          =   195
         Index           =   6
         Left            =   2040
         TabIndex        =   7
         Top             =   300
         Width           =   705
      End
      Begin VB.Label lblTitulos 
         AutoSize        =   -1  'True
         Caption         =   "Por nombre de �rea:"
         Height          =   195
         Index           =   2
         Left            =   1200
         TabIndex        =   5
         Top             =   1013
         Width           =   1485
      End
   End
   Begin VB.Label lblTitulos 
      AutoSize        =   -1  'True
      Caption         =   "Ingrese los criterios para buscar un recurso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   3690
   End
   Begin VB.Label lblTitulos 
      AutoSize        =   -1  'True
      Caption         =   "Por nombre o apellido del recurso:"
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   2445
   End
End
Attribute VB_Name = "frmRecursosConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -002- a. FJS 04.06.2010 - Se agrega un checkbox para visualizar solamente los recursos activos (habilitados) en la grilla.

Option Explicit

Const colCODRECURSO = 0
Const colNOMRECURSO = 1
Const colNOMDIRECCION = 2
Const colNOMGERENCIA = 3
Const colNOMSECTOR = 4
Const colNOMGRUPO = 5
Const colFlg_cargoarea = 6
Const colEstado_recurso = 7
Const colPERFILES = 8
Const colNOMBRE_PLANO = 9
Const cteSIN_ACENTOS = "aaaaAAAAeeeeEEEEiiiiIIIIooooOOOOuuuuUUUUyyY"
Const cteCON_ACENTOS = "�������������������������������������������"

Dim bSorting As Boolean                                 ' Variable global que indica que se est� ordenando la grilla
Dim lUltimaColumnaOrdenada As Long                      ' Variable auxiliar para el ordenamiento de la grilla

Private Sub Form_Load()
    bFormateando = True
    fraCriterios.Width = grdDatos.Width
    grdDatos.Top = 960
    grdDatos.Height = 5655
    chkSoloActivos.value = IIf(GetSetting("GesPet", "Preferencias\General", "PromptActiveUsersOnly") = "S", 1, 0)   ' add -002- a.
    Call InicializarCombos
    Call CargarRecursos
    cmbPerfiles.ListIndex = 0               ' Selecciono el primer criterio
    Call IniciarScroll(grdDatos)            ' add -001- a.
    bFormateando = False
End Sub

Private Sub InicializarCombos()
    With cmbPerfiles
        .Clear
        .AddItem "* Todos" & ESPACIOS & "||NULL"
        .AddItem "* Perfiles de la rama solicitante" & ESPACIOS & "||SOLI|REFE|AUTO|SUPE|"
        .AddItem "* Perfiles de la rama ejecutora" & ESPACIOS & "||CDIR|CGCI|CSEC|CGRU|ANAL|"
        If sp_GetPerfil(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_perfil) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_perfil)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = 0
    End With
    With cmbArea
        .Clear
        .AddItem "* Todos" & ESPACIOS & "||NULL"
        .AddItem "Direccion" & ESPACIOS & "||DIRE"
        .AddItem "Gerencia" & ESPACIOS & "||GERE"
        .AddItem "Sector" & ESPACIOS & "||SECT"
        .AddItem "Grupo" & ESPACIOS & "||GRUP"
        .ListIndex = 0
    End With
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .visible = False
        .Clear
        .cols = 10
        .RowHeightMin = 265
        .Rows = 1
        .TextMatrix(0, colCODRECURSO) = "Codigo": .ColWidth(colCODRECURSO) = 930: .ColAlignment(colCODRECURSO) = flexAlignLeftCenter
        .TextMatrix(0, colNOMRECURSO) = "Nombre": .ColWidth(colNOMRECURSO) = 3000: .ColAlignment(colNOMRECURSO) = flexAlignLeftCenter
        .TextMatrix(0, colNOMDIRECCION) = "Direcci�n": .ColWidth(colNOMDIRECCION) = 3000: .ColAlignment(colNOMDIRECCION) = flexAlignLeftCenter
        .TextMatrix(0, colNOMGERENCIA) = "Gerencia": .ColWidth(colNOMGERENCIA) = 3000: .ColAlignment(colNOMGERENCIA) = flexAlignLeftCenter
        .TextMatrix(0, colNOMSECTOR) = "Sector": .ColWidth(colNOMSECTOR) = 3000: .ColAlignment(colNOMSECTOR) = flexAlignLeftCenter
        .TextMatrix(0, colNOMGRUPO) = "Grupo": .ColWidth(colNOMGRUPO) = 3000: .ColAlignment(colNOMGRUPO) = flexAlignLeftCenter
        .TextMatrix(0, colFlg_cargoarea) = "A cargo": .ColWidth(colFlg_cargoarea) = 500: .ColAlignment(colFlg_cargoarea) = flexAlignLeftCenter
        .TextMatrix(0, colEstado_recurso) = "Estado": .ColWidth(colEstado_recurso) = 800: .ColAlignment(colEstado_recurso) = flexAlignLeftCenter
        .TextMatrix(0, colPERFILES) = "Perfiles": .ColWidth(colPERFILES) = 3000: .ColAlignment(colPERFILES) = flexAlignLeftCenter
        .TextMatrix(0, colNOMBRE_PLANO) = "nombre_plano": .ColWidth(colNOMBRE_PLANO) = 0: .ColAlignment(colNOMBRE_PLANO) = flexAlignLeftCenter
        .BackColorBkg = Me.BackColor
    End With
    'Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
End Sub

Private Sub CargarRecursos()
    Dim i As Long
    
    With grdDatos
        If sp_GetRecurso2 Then
            Call Puntero(True)
            Call InicializarGrilla
            sbMain.Panels(2).text = aplRST.RecordCount      ' Total de recursos
            sbMain.Panels(4).text = aplRST.RecordCount      ' Total de recursos seleccionados
            .Rows = aplRST.RecordCount + 1                  ' El total del recordset + 1 para el encabezado
            Do While Not aplRST.EOF
                'If chkSoloActivos.value = 0 Or (chkSoloActivos.value = 1 And ClearNull(aplRST.Fields.Item("estado_recurso")) = "A") Then
                    '.Rows = .Rows + 1
                    i = i + 1
                    .TextMatrix(i, colCODRECURSO) = ClearNull(aplRST.Fields.Item("cod_recurso"))
                    .TextMatrix(i, colNOMRECURSO) = ClearNull(aplRST.Fields.Item("nom_recurso"))
                    .TextMatrix(i, colNOMDIRECCION) = ClearNull(aplRST.Fields.Item("nom_direccion"))
                    .TextMatrix(i, colNOMGERENCIA) = ClearNull(aplRST.Fields.Item("nom_gerencia"))
                    .TextMatrix(i, colNOMSECTOR) = ClearNull(aplRST.Fields.Item("nom_sector"))
                    .TextMatrix(i, colNOMGRUPO) = ClearNull(aplRST.Fields.Item("nom_grupo"))
                    .TextMatrix(i, colFlg_cargoarea) = ClearNull(aplRST.Fields.Item("flg_cargoarea"))
                    .TextMatrix(i, colEstado_recurso) = ClearNull(aplRST.Fields.Item("estado_recurso"))
                    .TextMatrix(i, colPERFILES) = ClearNull(aplRST.Fields.Item("perfiles"))
                    .TextMatrix(i, colNOMBRE_PLANO) = ClearNull(aplRST.Fields.Item("nom_recurso_plano"))
                    'If i Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
                    'If .TextMatrix(i, colEstado_recurso) <> "A" Then Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorLightGrey)
                'End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        'If chkSoloActivos.Value = 1 Then sbMain.Panels(4).Text = .Rows - 1      ' Actualizo el contador de registros
        If chkSoloActivos.value = 1 Then sbMain.Panels(4).text = i - 1     ' Actualizo el contador de registros
        Call Puntero(False)
        .visible = True
    End With
End Sub

' *** ORIGINAL ***
'Private Sub CargarRecursos()
'    With grdDatos
'        If sp_GetRecurso2 Then
'            Call Puntero(True)
'            Call InicializarGrilla
'            sbMain.Panels(2).Text = aplRST.RecordCount      ' Total de recursos
'            sbMain.Panels(4).Text = aplRST.RecordCount      ' Total de recursos seleccionados
'            Do While Not aplRST.EOF
'                If chkSoloActivos.Value = 0 Or (chkSoloActivos.Value = 1 And ClearNull(aplRST.Fields.item("estado_recurso")) = "A") Then
'                    .Rows = .Rows + 1
'                    .TextMatrix(.Rows - 1, colCODRECURSO) = ClearNull(aplRST.Fields.item("cod_recurso"))
'                    .TextMatrix(.Rows - 1, colNOMRECURSO) = ClearNull(aplRST.Fields.item("nom_recurso"))
'                    .TextMatrix(.Rows - 1, colNOMDIRECCION) = ClearNull(aplRST.Fields.item("nom_direccion"))
'                    .TextMatrix(.Rows - 1, colNOMGERENCIA) = ClearNull(aplRST.Fields.item("nom_gerencia"))
'                    .TextMatrix(.Rows - 1, colNOMSECTOR) = ClearNull(aplRST.Fields.item("nom_sector"))
'                    .TextMatrix(.Rows - 1, colNOMGRUPO) = ClearNull(aplRST.Fields.item("nom_grupo"))
'                    .TextMatrix(.Rows - 1, colFlg_cargoarea) = ClearNull(aplRST.Fields.item("flg_cargoarea"))
'                    .TextMatrix(.Rows - 1, colEstado_recurso) = ClearNull(aplRST.Fields.item("estado_recurso"))
'                    .TextMatrix(.Rows - 1, colPERFILES) = ClearNull(aplRST.Fields.item("perfiles"))
'                    .TextMatrix(.Rows - 1, colNOMBRE_PLANO) = ClearNull(aplRST.Fields.item("nom_recurso_plano"))
'                    If .TextMatrix(.Rows - 1, colEstado_recurso) <> "A" Then
'                        CambiarForeColorLinea grdDatos, prmGridFillRowColorLightGrey
'                    End If
'                End If
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        If chkSoloActivos.Value = 1 Then sbMain.Panels(4).Text = .Rows - 1      ' Actualizo el contador de registros
'        Call Puntero(False)
'        .Visible = True
'    End With
'End Sub

Private Sub txtNombreApellido_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 40 Then
        grdDatos.SetFocus
    End If
    If KeyCode = 27 Then
        Unload Me
    End If
End Sub

Private Sub txtNombreApellido_Change()
    If Len(Trim(txtNombreApellido)) > 0 Then
        If Mid(txtNombreApellido, Len(txtNombreApellido), 1) = "'" Then
            txtNombreApellido = Mid(txtNombreApellido, 1, Len(txtNombreApellido) - 1)
        End If
    End If
    Call Filtrar
End Sub

Private Sub cmbPerfiles_Click()
    If Not bFormateando Then
        If InStr(1, cmbPerfiles.List(cmbPerfiles.ListIndex), "---", vbTextCompare) > 0 Then
            cmbPerfiles.ListIndex = 0
        Else
            Call Filtrar
        End If
    End If
End Sub

Private Sub txtArea_Change()
    If Len(Trim(txtArea)) > 0 Then
        If Mid(txtArea, Len(txtArea), 1) = "'" Then
            txtArea = Mid(txtArea, 1, Len(txtArea) - 1)
        End If
    End If
    Call Filtrar
End Sub

Private Sub chkSoloActivos_Click()
    If Not bFormateando Then Call Filtrar
End Sub

Private Sub Filtrar()
    Dim cFilters As String
    Dim cFiltroPerfil As String
    Dim vPerfiles_SOLI() As String
    Dim vPerfiles_EJEC() As String
    Dim i As Integer
    
    ' Inicializa los filtros para abarcar todos los items
    aplRST.Filter = adFilterNone
    cFilters = ""
    
    If Len(Trim(txtNombreApellido)) > 0 Then
        cFilters = "nom_recurso_plano LIKE '*" & Trim(txtNombreApellido) & "*'"
        'cFilters = "nom_recurso LIKE '*" & Trim(txtNombreApellido) & "*'"
        'cFilters = "nombre_plano LIKE '*" & Trim(strQuitar_Acentos(txtNombreApellido)) & "*'"
    End If
    
    If Len(Trim(txtArea)) > 0 Then
        Select Case CodigoCombo(cmbArea, True)
            Case "DIRE"
                If Len(cFilters) > 0 Then
                    cFilters = Trim(cFilters) & " AND nom_direccion LIKE '*" & Trim(txtArea) & "*'"
                Else
                    cFilters = "nom_direccion LIKE '*" & Trim(txtArea) & "*'"
                End If
            Case "GERE"
                If Len(cFilters) > 0 Then
                    cFilters = Trim(cFilters) & " AND nom_gerencia LIKE '*" & Trim(txtArea) & "*'"
                Else
                    cFilters = "nom_gerencia LIKE '*" & Trim(txtArea) & "*'"
                End If
            Case "SECT"
                If Len(cFilters) > 0 Then
                    cFilters = Trim(cFilters) & " AND nom_sector LIKE '*" & Trim(txtArea) & "*'"
                Else
                    cFilters = "nom_sector LIKE '*" & Trim(txtArea) & "*'"
                End If
            Case "GRUP"
                If Len(cFilters) > 0 Then
                    cFilters = Trim(cFilters) & " AND nom_grupo LIKE '*" & Trim(txtArea) & "*'"
                Else
                    cFilters = "nom_grupo LIKE '*" & Trim(txtArea) & "*'"
                End If
        End Select
    End If
    ' Perfiles
    Select Case cmbPerfiles.ListIndex
        Case 0      ' Todos los perfiles
            cFiltroPerfil = CodigoCombo(cmbPerfiles, True)
        Case 1      ' Solo perfiles de la rama solicitante
            ReDim vPerfiles_SOLI(4)
            vPerfiles_SOLI(0) = "SOLI"
            vPerfiles_SOLI(1) = "REFE"
            vPerfiles_SOLI(2) = "AUTO"
            vPerfiles_SOLI(3) = "SUPE"
        Case 2      ' Solo perfiles del area ejecutora
            ReDim vPerfiles_EJEC(5)
            vPerfiles_EJEC(0) = "CDIR"
            vPerfiles_EJEC(1) = "CGCI"
            vPerfiles_EJEC(2) = "CSEC"
            vPerfiles_EJEC(3) = "CGRU"
            vPerfiles_EJEC(4) = "ANAL"
        Case Else   ' Espec�fico
            cFiltroPerfil = CodigoCombo(cmbPerfiles, True)
    End Select
    
   
    If cmbPerfiles.ListIndex > 0 Then
        cFiltroPerfil = CodigoCombo(cmbPerfiles, True, True)
    Else
        cFiltroPerfil = ""
    End If
    
    If Len(cFiltroPerfil) > 0 Then
        If Len(cFilters) > 0 Then
            Select Case cmbPerfiles.ListIndex
                Case 1
                    cFilters = " AND ("
                    For i = 0 To UBound(vPerfiles_SOLI)
                        cFilters = IIf(cFilters = "", Trim(cFilters) & "perfiles LIKE '*" & vPerfiles_SOLI(i) & "*'", Trim(cFilters) & " OR perfiles LIKE '*" & vPerfiles_SOLI(i) & "*'")
                    Next i
                Case 2
                    cFilters = " AND ("
                    For i = 0 To UBound(vPerfiles_EJEC)
                        cFilters = IIf(cFilters = "", Trim(cFilters) & "perfiles LIKE '*" & vPerfiles_EJEC(i) & "*'", Trim(cFilters) & " OR perfiles LIKE '*" & vPerfiles_EJEC(i) & "*'")
                    Next i
                Case Else
                    cFilters = Trim(cFilters) & " AND perfiles LIKE '*" & "|" & cFiltroPerfil & "*'"
            End Select
        Else
            Select Case cmbPerfiles.ListIndex
                Case 1
                    For i = 0 To UBound(vPerfiles_SOLI) - 1
                        cFilters = IIf(cFilters = "", Trim(cFilters) & "perfiles LIKE '*" & vPerfiles_SOLI(i) & "*'", Trim(cFilters) & " OR perfiles LIKE '*" & vPerfiles_SOLI(i) & "*'")
                    Next i
                Case 2
                    For i = 0 To UBound(vPerfiles_EJEC) - 1
                        cFilters = IIf(cFilters = "", Trim(cFilters) & "perfiles LIKE '*" & vPerfiles_EJEC(i) & "*'", Trim(cFilters) & " OR perfiles LIKE '*" & vPerfiles_EJEC(i) & "*'")
                    Next i
                Case Else
                    cFilters = Trim(cFilters) & "perfiles LIKE '*" & "|" & cFiltroPerfil & "*'"
            End Select
        End If
    End If
    ' Si existe alg�n criterio, realiza el filtro...
    If Len(cFilters) > 0 Then
        aplRST.Filter = cFilters
        'grdDatos.Refresh
    End If
    Call ReCargarRecursos(aplRST)
End Sub

Private Sub ReCargarRecursos(rsAux As ADODB.Recordset)
    With grdDatos
        Call Puntero(True)
        Call InicializarGrilla
        sbMain.Panels(4).text = rsAux.RecordCount      ' Total de recursos seleccionados
        Do While Not rsAux.EOF
            If chkSoloActivos.value = 0 Or (chkSoloActivos.value = 1 And ClearNull(rsAux.Fields.Item("estado_recurso")) = "A") Then
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colCODRECURSO) = ClearNull(rsAux.Fields.Item("cod_recurso"))
                .TextMatrix(.Rows - 1, colNOMRECURSO) = ClearNull(rsAux.Fields.Item("nom_recurso"))
                .TextMatrix(.Rows - 1, colNOMDIRECCION) = ClearNull(rsAux.Fields.Item("nom_direccion"))
                .TextMatrix(.Rows - 1, colNOMGERENCIA) = ClearNull(rsAux.Fields.Item("nom_gerencia"))
                .TextMatrix(.Rows - 1, colNOMSECTOR) = ClearNull(rsAux.Fields.Item("nom_sector"))
                .TextMatrix(.Rows - 1, colNOMGRUPO) = ClearNull(rsAux.Fields.Item("nom_grupo"))
                .TextMatrix(.Rows - 1, colFlg_cargoarea) = ClearNull(rsAux.Fields.Item("flg_cargoarea"))
                .TextMatrix(.Rows - 1, colEstado_recurso) = ClearNull(rsAux.Fields.Item("estado_recurso"))
                .TextMatrix(.Rows - 1, colPERFILES) = ClearNull(rsAux.Fields.Item("perfiles"))
                If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
                If .TextMatrix(.Rows - 1, colEstado_recurso) <> "A" Then Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorLightGrey)
            End If
            rsAux.MoveNext
            DoEvents
        Loop
        If chkSoloActivos.value = 1 Then sbMain.Panels(4).text = .Rows - 1      ' Actualizo el contador de registros
        Call Puntero(False)
        .visible = True
    End With
End Sub

Private Sub cmdCriterios_Click()
    If fraCriterios.visible Then
        fraCriterios.visible = False
        grdDatos.Top = 1000
        grdDatos.Height = 5655
        cmdCriterios.Caption = "M�s crit&erios"
    Else
        grdDatos.visible = False
        grdDatos.Top = 2500
        grdDatos.Height = 4215
        cmdCriterios.Caption = "Ocult&ar"
        grdDatos.visible = True
        fraCriterios.visible = True
    End If
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        frmLogin.USER_ID = ClearNull(grdDatos.TextMatrix(grdDatos.RowSel, colCODRECURSO))
        Unload Me
        'Me.Hide
    End If
End Sub

Private Sub grdDatos_DblClick()
    frmLogin.USER_ID = ClearNull(grdDatos.TextMatrix(grdDatos.RowSel, colCODRECURSO))
    Unload Me
    'Me.Hide
End Sub

Private Sub grdDatos_Click()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
            bSorting = True
            Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
            bSorting = False
        End If
    End With
End Sub

' La funcion Replace() no existe en Visual Basic 5.0 o anteriores y
' tampoco en el VBA de Excel, Word,... Asi que para los que no vayais
' a usar Visual Basic 6 (o el 7 cuando aparezca), tendreis que usar
' esta otra:
Private Function strQuitar_Acentos(ByVal strMemo As String) As String
    Dim intCont As Integer
    Dim intPos As Integer
    '-----------------------------------
    For intCont = 1 To Len(cteCON_ACENTOS)
        If InStr(1, strMemo, Mid(cteCON_ACENTOS, intCont, 1)) Then                  ' Si encuentra uno de los caracteres de cteCON_ACENTOS, empieza a buscar y cambiar
            intPos = InStr(1, strMemo, Mid(cteCON_ACENTOS, intCont, 1))
            Do Until (intPos = 0)                                                   ' strmemo=(los que hay antes)+(car. sin acento)+(los que hay despues)
                strMemo = Left(strMemo, intPos - 1) & Mid(cteSIN_ACENTOS, intCont, 1) & _
                Mid(strMemo, intPos + 1)
                intPos = InStr(intPos, strMemo, Mid(cteCON_ACENTOS, intCont, 1))    ' Busca el siguiente (0 si no lo encuentra)
            Loop
        End If
    Next
    strQuitar_Acentos = strMemo
End Function

'{ add -001- a.
Private Sub Form_Unload(Cancel As Integer)
    SaveSetting "GesPet", "Preferencias\General", "PromptActiveUsersOnly", IIf(chkSoloActivos.value = 1, "S", "N")   ' add -002- a.
    Call DetenerScroll(grdDatos)
End Sub
'}
