VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form auxFechaDesdeHasta 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   2385
   ClientLeft      =   3345
   ClientTop       =   3645
   ClientWidth     =   4920
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "auxFechaDesdeHasta.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2385
   ScaleWidth      =   4920
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3900
      TabIndex        =   6
      Top             =   180
      Width           =   885
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   3900
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   600
      Width           =   885
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ingrese el rango de fechas"
      ForeColor       =   &H00FF0000&
      Height          =   2115
      Left            =   180
      TabIndex        =   0
      Top             =   120
      Width           =   3615
      Begin AT_MaskText.MaskText txtFechaDesde 
         Height          =   315
         Left            =   840
         TabIndex        =   1
         Top             =   660
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaHasta 
         Height          =   315
         Left            =   840
         TabIndex        =   2
         Top             =   1020
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Left            =   240
         TabIndex        =   4
         Top             =   720
         Width           =   450
      End
      Begin VB.Label Label1 
         Caption         =   "Hasta"
         Height          =   195
         Left            =   240
         TabIndex        =   3
         Top             =   1080
         Width           =   510
      End
   End
End
Attribute VB_Name = "auxFechaDesdeHasta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 26.11.2008 - Se fuerza el formateado de los campos de fecha porque genera problemas en tiempo de ejecuci�n.

Option Explicit

Public bAceptar As Boolean

Private Sub Form_Load()
    bAceptar = False
End Sub

Private Sub cmdAceptar_Click()
    '{ add -002- a.
    txtFechaDesde.ForceFormat
    txtFechaHasta.ForceFormat
    '}
    If IsNull(txtFechaDesde.DateValue) Then
        MsgBox "Fecha desde inv�lida.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    If IsNull(txtFechaHasta.DateValue) Then
        MsgBox "Fecha hasta inv�lida.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    If Trim(txtFechaDesde) = "" Or Trim(txtFechaHasta) = "" Then
       MsgBox "Se deben indicar las fechas", vbExclamation
    ElseIf txtFechaDesde.DateValue > txtFechaHasta.DateValue Then
       MsgBox "La fecha Desde no puede ser posterior a la fecha Hasta", vbExclamation
    Else
       bAceptar = True
       Me.Hide
    End If
End Sub

Private Sub cmdCancelar_Click()
    bAceptar = False
    Me.Hide
End Sub
