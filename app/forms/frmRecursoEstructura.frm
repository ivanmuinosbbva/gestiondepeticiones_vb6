VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRecursoEstructura 
   Caption         =   "Form1"
   ClientHeight    =   4500
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5790
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   4500
   ScaleWidth      =   5790
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList imgTreeView 
      Left            =   4080
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.TreeView trwEstructura 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   6588
      _Version        =   393217
      Style           =   7
      ImageList       =   "imgTreeView"
      Appearance      =   1
   End
End
Attribute VB_Name = "frmRecursoEstructura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const DIRE = "DIRE_"
Private Const GERE = "GERE_"
Private Const SECT = "SECT_"
Private Const GRUP = "GRUP_"

Private Sub Form_Load()
    Call Inicializar
End Sub

Private Sub Inicializar()
    Call CargarArbol
End Sub

Private Sub CargarArbol()
    Dim i As Long
    
    Dim sCodigo As String
    
    Dim aplRST_Direcciones As New ADODB.Recordset
    Dim aplRST_Gerencias As New ADODB.Recordset
    Dim aplRST_Sectores As New ADODB.Recordset
    Dim aplRST_Grupos As New ADODB.Recordset
    Dim aplRST_Recursos As New ADODB.Recordset
    
    If sp_GetDireccion(Null, "S") Then
        Set aplRST_Direcciones = aplRST.Clone(adLockReadOnly)
        aplRST_Direcciones.Sort = "cod_direccion"
    End If
    
    If sp_GetGerencia(Null, Null, "S") Then
        Set aplRST_Gerencias = aplRST.Clone(adLockReadOnly)
        aplRST_Gerencias.Sort = "cod_direccion, cod_gerencia"
    End If
    
    If sp_GetSector(Null, Null, "S") Then
        Set aplRST_Sectores = aplRST.Clone(adLockReadOnly)
        aplRST_Sectores.Sort = "cod_gerencia, cod_sector"
    End If
    
    If sp_GetGrupo(Null, Null, "S") Then
        Set aplRST_Grupos = aplRST.Clone(adLockReadOnly)
        aplRST_Grupos.Sort = "cod_sector, cod_grupo"
    End If
    
    If sp_GetRecurso(Null, "R", "A") Then
        Set aplRST_Recursos = aplRST.Clone(adLockReadOnly)
        aplRST_Recursos.Sort = "cod_grupo"
    End If
    
    
    With trwEstructura
        .Nodes.Clear
        .Indentation = 350
        .Nodes.Add , , "BBVA", "BBVA", 11
        .Nodes(.Nodes.Count).Expanded = True
        
        ' DIRECCIONES
        aplRST_Direcciones.MoveFirst
        Do While Not aplRST_Direcciones.EOF
            sCodigo = DIRE & ClearNull(aplRST_Direcciones.Fields!cod_direccion)
            .Nodes.Add "BBVA", TreeRelationshipConstants.tvwChild, sCodigo, ClearNull(aplRST_Direcciones.Fields!nom_direccion), 8
            .Nodes(.Nodes.Count).Expanded = True
            
            ' GERENCIAS
            aplRST_Gerencias.Filter = "cod_direccion = '" & ClearNull(aplRST_Direcciones.Fields!cod_direccion) & "'"
            If aplRST_Gerencias.RecordCount > 0 Then aplRST_Gerencias.MoveFirst
            Do While Not aplRST_Gerencias.EOF
                sCodigo = GERE & ClearNull(aplRST_Gerencias.Fields!cod_gerencia)
                .Nodes.Add DIRE & ClearNull(aplRST_Direcciones.Fields!cod_direccion), TreeRelationshipConstants.tvwChild, sCodigo, ClearNull(aplRST_Gerencias.Fields!nom_gerencia), 5
                .Nodes(.Nodes.Count).Expanded = True
                
                ' SECTORES
                aplRST_Sectores.Filter = "cod_gerencia = '" & ClearNull(aplRST_Gerencias.Fields!cod_gerencia) & "'"
                If aplRST_Sectores.RecordCount > 0 Then aplRST_Sectores.MoveFirst
                Do While Not aplRST_Sectores.EOF
                    sCodigo = SECT & ClearNull(aplRST_Sectores.Fields!cod_sector)
                    .Nodes.Add GERE & ClearNull(aplRST_Gerencias.Fields!cod_gerencia), TreeRelationshipConstants.tvwChild, sCodigo, ClearNull(aplRST_Sectores.Fields!nom_sector), 2
                    .Nodes(.Nodes.Count).Expanded = True
                    
                    ' GRUPOS
                    aplRST_Grupos.Filter = "cod_sector = '" & ClearNull(aplRST_Sectores.Fields!cod_sector) & "'"
                    If aplRST_Grupos.RecordCount > 0 Then aplRST_Grupos.MoveFirst
                    Do While Not aplRST_Grupos.EOF
                        sCodigo = GRUP & ClearNull(aplRST_Grupos.Fields!cod_grupo)
                        .Nodes.Add SECT & ClearNull(aplRST_Sectores.Fields!cod_sector), TreeRelationshipConstants.tvwChild, sCodigo, ClearNull(aplRST_Grupos.Fields!nom_grupo), 3
                        '.Nodes(.Nodes.Count).Expanded = True
                        
                        ' RECURSOS
                        aplRST_Recursos.Filter = "cod_grupo = '" & ClearNull(aplRST_Grupos.Fields!cod_grupo) & "'"
                        If aplRST_Recursos.RecordCount > 0 Then aplRST_Recursos.MoveFirst
                        Do While Not aplRST_Recursos.EOF
                            sCodigo = ClearNull(aplRST_Recursos.Fields!cod_recurso)
                            .Nodes.Add GRUP & ClearNull(aplRST_Grupos.Fields!cod_grupo), TreeRelationshipConstants.tvwChild, sCodigo, ClearNull(aplRST_Recursos.Fields!nom_recurso), 1
                            aplRST_Recursos.MoveNext
                            DoEvents
                        Loop
                        aplRST_Recursos.Filter = adFilterNone
                        aplRST_Grupos.MoveNext
                        DoEvents
                    Loop
                    aplRST_Grupos.Filter = adFilterNone
                    aplRST_Sectores.MoveNext
                    DoEvents
                Loop
                aplRST_Sectores.Filter = adFilterNone
                aplRST_Gerencias.MoveNext
                DoEvents
            Loop
            aplRST_Gerencias.Filter = adFilterNone
            aplRST_Direcciones.MoveNext
            DoEvents
        Loop
    End With
    
    Set aplRST_Direcciones = Nothing
    Set aplRST_Gerencias = Nothing
    Set aplRST_Sectores = Nothing
    Set aplRST_Grupos = Nothing
    Set aplRST_Recursos = Nothing
End Sub
