VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSSDDCatalogoDetalle 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Definición de datos sensibles de la tabla"
   ClientHeight    =   4725
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8985
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4725
   ScaleWidth      =   8985
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraBotonera 
      Height          =   4695
      Left            =   7920
      TabIndex        =   15
      Top             =   0
      Width           =   1035
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Nuevo"
         Height          =   375
         Left            =   60
         TabIndex        =   19
         Top             =   2820
         Width           =   885
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Editar"
         Height          =   375
         Left            =   60
         TabIndex        =   18
         Top             =   3180
         Width           =   885
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Height          =   375
         Left            =   60
         TabIndex        =   17
         Top             =   3540
         Width           =   885
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   375
         Left            =   60
         TabIndex        =   6
         Top             =   3900
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   60
         TabIndex        =   16
         Top             =   4260
         Width           =   885
      End
   End
   Begin VB.Frame fraDatos 
      Caption         =   "lblModo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   60
      TabIndex        =   8
      Top             =   3180
      Width           =   7815
      Begin VB.TextBox txtRutina 
         Height          =   285
         Left            =   4200
         MaxLength       =   8
         TabIndex        =   5
         Top             =   1020
         Width           =   2295
      End
      Begin VB.ComboBox cboSubtipo 
         Height          =   315
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   660
         Width           =   2295
      End
      Begin VB.ComboBox cboTipo 
         Height          =   315
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   300
         Width           =   2295
      End
      Begin VB.TextBox txtLongitud 
         Height          =   285
         Left            =   2640
         TabIndex        =   2
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox txtPosicionDesde 
         Height          =   285
         Left            =   840
         TabIndex        =   1
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   840
         MaxLength       =   50
         TabIndex        =   0
         Top             =   300
         Width           =   2415
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Rutina"
         Height          =   195
         Index           =   6
         Left            =   3480
         TabIndex        =   14
         Top             =   1065
         Width           =   465
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Subtipo"
         Height          =   195
         Index           =   5
         Left            =   3480
         TabIndex        =   13
         Top             =   720
         Width           =   540
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         Height          =   195
         Index           =   4
         Left            =   3480
         TabIndex        =   12
         Top             =   360
         Width           =   300
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Longitud"
         Height          =   195
         Index           =   3
         Left            =   1920
         TabIndex        =   11
         Top             =   645
         Width           =   615
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   10
         Top             =   645
         Width           =   450
      End
      Begin VB.Label lblEntidad 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   9
         Top             =   345
         Width           =   555
      End
   End
   Begin MSComctlLib.ImageList imgDatos 
      Left            =   7200
      Top             =   180
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSSDDCatalogoDetalle.frx":0000
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lswDatos 
      Height          =   3075
      Left            =   60
      TabIndex        =   7
      Top             =   60
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   5424
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "imgDatos"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmSSDDCatalogoDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public id As Long

Dim bLoading As Boolean
Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Call InicializarCombos(0)
    Call HabilitarBotones(0)
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertCatalogoDetalle(id, txtNombre, txtPosicionDesde, txtLongitud, CodigoCombo(cboTipo, True), CodigoCombo(cboSubtipo, True), txtRutina) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                Call sp_UpdateCatalogoDetalle(id, txtNombre, txtPosicionDesde, txtLongitud, CodigoCombo(cboTipo, True), CodigoCombo(cboSubtipo, True), txtRutina)
                Call HabilitarBotones(0)
            End If
        Case "E"
            If sp_DeleteCatalogoDetalle(id, txtNombre) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub InicializarLista()
    With lswDatos
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "Campo", 1600
        .ColumnHeaders.Add , , "Posición desde", 800: .ColumnHeaders(.ColumnHeaders.Count).Alignment = lvwColumnRight
        .ColumnHeaders.Add , , "Longitud", 800: .ColumnHeaders(.ColumnHeaders.Count).Alignment = lvwColumnRight
        .ColumnHeaders.Add , , "tipo_id", 0
        .ColumnHeaders.Add , , "Tipo", 1400
        .ColumnHeaders.Add , , "subtipo_id", 0
        .ColumnHeaders.Add , , "Subtipo", 1800
        .ColumnHeaders.Add , , "Rutina", 1200
        .ColumnHeaders.Add , , "Id.", 0
        .View = lvwReport
        .ListItems.Clear
    End With
End Sub

Private Sub CargarLista()
    Dim itmX As ListItem
    
    Call Puntero(True)
    Call InicializarLista
    With lswDatos
        If sp_GetCatalogoDetalle(id) Then
            Do While Not aplRST.EOF
                Set itmX = .ListItems.Add(, , ClearNull(aplRST!campo), , 1)
                itmX.SubItems(1) = ClearNull(aplRST!pos_desde)
                itmX.SubItems(2) = ClearNull(aplRST!longitud)
                itmX.SubItems(3) = ClearNull(aplRST!tipo_id)
                itmX.SubItems(4) = ClearNull(aplRST!tipo_nom)
                itmX.SubItems(5) = ClearNull(aplRST!subtipo_id)
                itmX.SubItems(6) = ClearNull(aplRST!subtipo_nom)
                itmX.SubItems(7) = ClearNull(aplRST!rutina)
                itmX.SubItems(8) = ClearNull(aplRST!id)
                aplRST.MoveNext
                DoEvents
            Loop
            If .ListItems.Count > 0 Then
                .ListItems(1).Selected = True
                Set itmX = .ListItems(1)
                Call MostrarSeleccion(itmX)
            End If
        End If
    End With
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaLista As Variant)
    Call setHabilCtrl(txtNombre, DISABLE)
    Call setHabilCtrl(txtPosicionDesde, DISABLE)
    Call setHabilCtrl(txtLongitud, DISABLE)
    Call setHabilCtrl(txtRutina, DISABLE)
    Call setHabilCtrl(cboTipo, DISABLE)
    Call setHabilCtrl(cboSubtipo, DISABLE)
                    
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            lswDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaLista) Then
                Call CargarLista
            Else
                'Call MostrarSeleccion
            End If
        Case 1
            lswDatos.Enabled = False
            fraDatos.Enabled = True
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operación"
            Select Case sOpcionSeleccionada
                Case "A"
                    fraDatos.Caption = " AGREGAR "
                    txtNombre = "": txtPosicionDesde = "": txtLongitud = "": txtRutina = ""
                    cboTipo.ListIndex = -1
                    cboSubtipo.ListIndex = -1
                    Call setHabilCtrl(txtNombre, NORMAL)
                    Call setHabilCtrl(txtPosicionDesde, NORMAL)
                    Call setHabilCtrl(txtLongitud, NORMAL)
                    Call setHabilCtrl(cboTipo, NORMAL)
                    Call setHabilCtrl(cboSubtipo, NORMAL)
                    Call InicializarCombos(1)
                    fraDatos.Enabled = True
                    txtNombre.SetFocus
                Case "M"
                    fraDatos.Caption = " MODIFICAR "
                    fraDatos.Enabled = True
                    Call setHabilCtrl(txtNombre, NORMAL)
                    Call setHabilCtrl(txtPosicionDesde, NORMAL)
                    Call setHabilCtrl(txtLongitud, NORMAL)
                    Call setHabilCtrl(cboTipo, NORMAL)
                    Call setHabilCtrl(cboSubtipo, NORMAL)
                    Call InicializarCombos(1)
                    txtNombre.SetFocus
                Case "E"
                    fraDatos.Caption = " ELIMINAR "
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Sub lswDatos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Call MostrarSeleccion(Item)
End Sub

Private Sub MostrarSeleccion(ByVal Item As MSComctlLib.ListItem)
    bLoading = True
    With lswDatos
        If ClearNull(Item.Text) <> "" Then
            txtNombre = Trim(Item.Text)
            txtPosicionDesde = Item.SubItems(1)
            txtLongitud = Item.SubItems(2)
            txtRutina = Item.SubItems(7)
            cboTipo.ListIndex = PosicionCombo(cboTipo, Item.SubItems(3), True)
            cboSubtipo.ListIndex = PosicionCombo(cboSubtipo, Item.SubItems(5), True)
        End If
    End With
    bLoading = False
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub InicializarCombos(nOpcion As Integer)
    Dim codigoTipo As String
    
    If nOpcion <> 1 Then
        If sp_GetHEDT011(Null, Null) Then
            With cboTipo
                .Clear
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!tipo_nom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!tipo_id)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End With
        End If
    End If
    'If sp_GetHEDT012(CodigoCombo(cboTipo, True), Null, Null, Null) Then
    If sp_GetHEDT012(IIf(nOpcion = 0, Null, CodigoCombo(cboTipo, True)), Null, Null, Null) Then
        With cboSubtipo
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!subtipo_nom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!subtipo_id)
                aplRST.MoveNext
                DoEvents
            Loop
            .AddItem "Otro..." & ESPACIOS & "||OTRO"
        End With
    End If
End Sub

Private Sub cboTipo_Click()
    If Not bLoading Then Call InicializarCombos(1)
End Sub

Private Sub cboSubtipo_Click()
    If Not bLoading Then
        If CodigoCombo(cboSubtipo, True) <> "OTRO" Then
            If sp_GetHEDT012(CodigoCombo(cboTipo, True), Right(CodigoCombo(cboSubtipo, True), 1), Null, Null) Then
                txtRutina = ClearNull(aplRST.Fields!rutina)
            End If
        End If
    End If
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    
End Function
