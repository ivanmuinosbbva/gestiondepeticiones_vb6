VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptSoliEME 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   4680
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   7860
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4680
   ScaleWidth      =   7860
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraRpt 
      Height          =   2925
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   7860
      Begin VB.Frame fraNivel 
         Caption         =   " Pedir solicitudes "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1365
         Left            =   4680
         TabIndex        =   13
         Top             =   840
         Width           =   2505
         Begin VB.OptionButton OptNivel 
            Caption         =   "Por supervisor"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   2
            Left            =   240
            TabIndex        =   16
            Top             =   960
            Width           =   1485
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Por l�der"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   1
            Left            =   240
            TabIndex        =   15
            Top             =   720
            Width           =   1005
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sin filtrar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   240
            TabIndex        =   14
            Top             =   480
            Value           =   -1  'True
            Width           =   1005
         End
      End
      Begin VB.CheckBox chkEmergencias 
         Caption         =   "Solo listar las solicitudes por Emergencia"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   2400
         Width           =   3495
      End
      Begin VB.ComboBox cboRecurso 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1500
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   5685
      End
      Begin AT_MaskText.MaskText txtFechaDesde 
         Height          =   315
         Left            =   1500
         TabIndex        =   8
         Top             =   720
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaHasta 
         Height          =   315
         Left            =   1500
         TabIndex        =   9
         Top             =   1080
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   11
         Top             =   787
         Width           =   480
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   10
         Top             =   1147
         Width           =   450
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Seleccione"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   240
         TabIndex        =   3
         Top             =   435
         Width           =   825
      End
      Begin VB.Label lblAguarde 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   2400
         Width           =   7515
      End
   End
   Begin VB.Frame fraOpciones 
      Height          =   945
      Left            =   0
      TabIndex        =   4
      Top             =   3720
      Width           =   7860
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6480
         TabIndex        =   6
         Top             =   360
         Width           =   1095
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   5280
         TabIndex        =   5
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SOLICITUDES GENERADAS POR PERIODO"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   3885
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptSoliEME.frx":0000
      Top             =   0
      Width           =   8700
   End
End
Attribute VB_Name = "rptSoliEME"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Call setHabilCtrl(cboRecurso, "DIS")
    txtFechaDesde = date
    txtFechaHasta = date
End Sub

Private Sub OptNivel_Click(Index As Integer)
    Select Case Index
        Case 0      ' Todas
            cboRecurso.ListIndex = -1
            Call setHabilCtrl(cboRecurso, "DIS")
        Case 1      ' Lider
            With cboRecurso
                .Clear
                If sp_GetRecursosPorAreaDyd("CGRU") Then
                    Do While Not aplRST.EOF
                        .AddItem ClearNull(aplRST.Fields!nom_recurso) & Space(100) & "||" & ClearNull(aplRST.Fields!cod_recurso)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            End With
            Call setHabilCtrl(cboRecurso, "NOR")
            cboRecurso.ListIndex = 0
        Case 2      ' Supervisor
            With cboRecurso
                .Clear
                If sp_GetRecursosPorAreaDyd("CSEC") Then
                    Do While Not aplRST.EOF
                        .AddItem ClearNull(aplRST.Fields!nom_recurso) & Space(100) & "||" & ClearNull(aplRST.Fields!cod_recurso)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            End With
            Call setHabilCtrl(cboRecurso, "NOR")
            cboRecurso.ListIndex = 0
    End Select
End Sub

Private Sub cmdOK_Click()
    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        'Call ViejoReporte
    End If
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    
    sReportName = "\soleme1.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crPortrait
        .ReportTitle = "Solicitudes a Carga de M�quina (TSO)"
        If OptNivel(1).Value Then
            .ReportComments = "Solicitudes por L�der entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
                        IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
        ElseIf OptNivel(2).Value Then
            .ReportComments = "Solicitudes por Supervisor entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
                        IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
        Else
            .ReportComments = "Solicitudes entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
                        IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
        End If
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
    
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fch_desde": crParamDef.AddCurrentValue (Format(txtFechaDesde, "yyyymmdd"))
            Case "@fch_hasta": crParamDef.AddCurrentValue (Format(txtFechaHasta, "yyyymmdd"))
            Case "@emergencias": crParamDef.AddCurrentValue (IIf(chkEmergencias.Value = 1, "S", "NULL"))
            Case "@cod_recurso": crParamDef.AddCurrentValue (IIf(OptNivel(0).Value <> True, CodigoCombo(cboRecurso, True), "NULL"))
        End Select
    Next
   
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

'Private Sub ViejoReporte()
'    Dim cPathFileNameReport As String
'    Dim cTitulo As String
'    Dim dFecha_Desde As Date
'    Dim dFecha_Hasta As Date
'    Dim iCantidad_Dias As Integer
'
'    Call Status("Preparando el informe...")
'    Screen.MousePointer = vbHourglass
'
'    If OptNivel(1).Value Then
'        cTitulo = "Solicitudes por L�der entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
'                    IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
'    ElseIf OptNivel(2).Value Then
'        cTitulo = "Solicitudes por Supervisor entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
'                    IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
'    Else
'        cTitulo = "Solicitudes entre el " & Format(txtFechaDesde, "dd/mm/yyyy") & " y " & Format(txtFechaHasta, "dd/mm/yyyy") & " (" & _
'                    IIf(chkEmergencias.Value = 1, "Solo EMErgencias)", "Todas)")
'    End If
'
'    With mdiPrincipal.CrystalReport1
'        cPathFileNameReport = App.Path & "\" & "rptGetSolicitudes.rpt"
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .RetrieveStoredProcParams
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .StoredProcParam(0) = IIf(IsNull(txtFechaDesde), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtFechaDesde, "yyyymmdd"))
'        .StoredProcParam(1) = IIf(IsNull(txtFechaHasta), Format(date, "yyyymmdd"), Format(txtFechaHasta, "yyyymmdd"))
'        .StoredProcParam(2) = IIf(chkEmergencias.Value = 1, "S", "NULL")
'        .StoredProcParam(3) = IIf(OptNivel(0).Value <> True, CodigoCombo(cboRecurso, True), "NULL")
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & cTitulo & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Screen.MousePointer = vbNormal
'        Call Status("Listo.")
'    End With
'End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
