VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDireccion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DIRECCIONES"
   ClientHeight    =   6240
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame pnlBotones 
      Height          =   6195
      Left            =   8220
      TabIndex        =   8
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Direcci�n"
         Top             =   3570
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la direcci�n selecionada"
         Top             =   4080
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Direcci�n seleccionada"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   5610
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3825
      Left            =   6210
      TabIndex        =   7
      Top             =   0
      Width           =   1995
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2340
      Left            =   0
      TabIndex        =   3
      Top             =   3870
      Width           =   8205
      Begin VB.CheckBox chkIGM 
         Caption         =   "IGM"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4380
         TabIndex        =   20
         Top             =   1418
         Width           =   735
      End
      Begin VB.ComboBox cboEjecutor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1380
         Width           =   2500
      End
      Begin VB.ComboBox cboHabil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1020
         Width           =   1900
      End
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   315
         Left            =   1350
         TabIndex        =   1
         Top             =   330
         Width           =   1000
         _ExtentX        =   1773
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   315
         Left            =   1350
         TabIndex        =   2
         Top             =   675
         Width           =   5685
         _ExtentX        =   10028
         _ExtentY        =   556
         MaxLength       =   80
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   80
      End
      Begin AT_MaskText.MaskText txtAbreviatura 
         Height          =   315
         Left            =   1350
         TabIndex        =   19
         Top             =   1800
         Width           =   2805
         _ExtentX        =   4948
         _ExtentY        =   556
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
      End
      Begin VB.Label Label4 
         Caption         =   "Abrev."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   18
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Ejecutor"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   17
         Top             =   1410
         Width           =   1095
      End
      Begin VB.Label Label10 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   15
         Top             =   1043
         Width           =   1100
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   345
         Width           =   1100
      End
      Begin VB.Label Label2 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   694
         Width           =   1100
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   4
         Top             =   30
         Visible         =   0   'False
         Width           =   45
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3810
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   6720
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDireccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Private Const colCODDIRECCION = 0
Private Const colNOMDIRECCION = 1
Private Const colEjecutor = 2
Private Const colHabil = 3
Private Const colABREVIATURA = 4
Private Const colIGM = 5
Private Const colTOTCOLS = 6

Dim sOpcionSeleccionada As String

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_UpdateDireccion("A", txtCodigo, txtDescripcion, CodigoCombo(Me.cboHabil), CodigoCombo(Me.cboEjecutor), IIf(chkIGM.Value = 1, "S", "N"), txtAbreviatura) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If CodigoCombo(Me.cboHabil) <> "S" Then
                    If MsgBox("Al inhabilitar la Direcci�n dejar� inhabilitado todo su �rbol jer�rquico. �Contin�a?", vbYesNo) = vbNo Then
                        Exit Sub
                    End If
                End If
                If sp_UpdateDireccion("M", txtCodigo, txtDescripcion, CodigoCombo(Me.cboHabil), CodigoCombo(Me.cboEjecutor), IIf(chkIGM.Value = 1, "S", "N"), txtAbreviatura) Then
                    With grdDatos
                        If .RowSel > 0 Then
                           .TextMatrix(.RowSel, colNOMDIRECCION) = txtDescripcion
                           .TextMatrix(.RowSel, colHabil) = CodigoCombo(Me.cboHabil)
                           .TextMatrix(.RowSel, colEjecutor) = CodigoCombo(Me.cboEjecutor)
                           .TextMatrix(.RowSel, colABREVIATURA) = txtAbreviatura
                           .TextMatrix(.RowSel, colIGM) = IIf(chkIGM.Value = 1, "S", "N")
                        End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeleteDireccion(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Private Sub Form_Load()
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)        ' add -003- a.
    Me.Left = 0
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    cboHabil.AddItem "S : Habilitado"
    cboHabil.AddItem "N : Inhabilitado"
    
    cboEjecutor.AddItem "S : Ejecuta pedidos"
    cboEjecutor.AddItem "N : No ejecuta"
   
    Show
    Call HabilitarBotones(0)
End Sub

Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    txtCodigo = "": txtDescripcion = "": txtAbreviatura = ""
                    cboHabil.ListIndex = 0
                    cboEjecutor.ListIndex = 1
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(txtCodigo.Text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.Text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtDescripcion.Text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtDescripcion.Text, ":") > 0 Then
        MsgBox ("a Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Sub CargarGrid()
    With grdDatos
        .Clear
        .Font.name = "Tahoma"
        .HighLight = flexHighlightNever
        .Rows = 1
        .cols = colTOTCOLS
        .TextMatrix(0, colCODDIRECCION) = "CODIGO"
        .TextMatrix(0, colNOMDIRECCION) = "DESCRIPCION"
        .TextMatrix(0, colHabil) = "HABILIT"
        .TextMatrix(0, colEjecutor) = "EJEC"
        .TextMatrix(0, colABREVIATURA) = "Abrev."
        .ColWidth(colEjecutor) = 800: .ColAlignment(colEjecutor) = flexAlignCenterCenter
        .ColWidth(colHabil) = 800: .ColAlignment(colHabil) = flexAlignCenterCenter
        .ColWidth(colCODDIRECCION) = 1000: .ColAlignment(colCODDIRECCION) = 0
        .ColWidth(colNOMDIRECCION) = 5000: .ColAlignment(colNOMDIRECCION) = 0
        .ColWidth(colABREVIATURA) = 2000: .ColAlignment(colABREVIATURA) = 0
        .ColWidth(colIGM) = 0: .ColAlignment(colIGM) = 0
    End With
    Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    If Not sp_GetDireccion("") Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODDIRECCION) = IIf(Not IsNull(aplRST.Fields.Item("cod_direccion")), aplRST.Fields.Item("cod_direccion"), "")
            .TextMatrix(.Rows - 1, colNOMDIRECCION) = IIf(Not IsNull(aplRST.Fields.Item("nom_direccion")), aplRST.Fields.Item("nom_direccion"), "")
            .TextMatrix(.Rows - 1, colHabil) = IIf(ClearNull(aplRST.Fields.Item("flg_habil")) = "N", "N", "S")
            .TextMatrix(.Rows - 1, colEjecutor) = ClearNull(aplRST.Fields.Item("es_ejecutor"))
            .TextMatrix(.Rows - 1, colIGM) = ClearNull(aplRST.Fields.Item("IGM_hab"))
            .TextMatrix(.Rows - 1, colABREVIATURA) = ClearNull(aplRST.Fields.Item("abrev_direccion"))
        End With
        aplRST.MoveNext
    Loop
    Call MostrarSeleccion
End Sub

Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colCODDIRECCION) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODDIRECCION))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNOMDIRECCION))
                cboHabil.ListIndex = PosicionCombo(Me.cboHabil, .TextMatrix(.RowSel, colHabil))
                cboEjecutor.ListIndex = PosicionCombo(Me.cboEjecutor, .TextMatrix(.RowSel, colEjecutor))
                txtAbreviatura = ClearNull(.TextMatrix(.RowSel, colABREVIATURA))
                chkIGM.Value = IIf(ClearNull(.TextMatrix(.RowSel, colIGM)) = "S", 1, 0)
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
