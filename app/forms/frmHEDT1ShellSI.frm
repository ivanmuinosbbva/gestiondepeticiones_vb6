VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmHEDT1ShellSI 
   Caption         =   "Cat�logo de DSN para procesos de enmascaramiento de datos"
   ClientHeight    =   7980
   ClientLeft      =   2760
   ClientTop       =   2640
   ClientWidth     =   11820
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHEDT1ShellSI.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7980
   ScaleWidth      =   11820
   WindowState     =   2  'Maximized
   Begin VB.Frame fraBotonera 
      Height          =   7935
      Left            =   10320
      TabIndex        =   30
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar"
         Height          =   495
         Left            =   120
         TabIndex        =   40
         ToolTipText     =   "Genera una exportaci�n a una planilla Excel"
         Top             =   3960
         Width           =   1215
      End
      Begin VB.CommandButton cmdSolicitudes 
         Caption         =   "Solicitudes relacionadas"
         Height          =   500
         Left            =   80
         TabIndex        =   37
         Top             =   2400
         Width           =   1290
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Informe"
         Enabled         =   0   'False
         Height          =   500
         Left            =   80
         TabIndex        =   16
         Top             =   1610
         Width           =   1290
      End
      Begin VB.CommandButton cmdCopiar 
         Caption         =   "Copiar nombres"
         Height          =   500
         Left            =   80
         TabIndex        =   15
         ToolTipText     =   "Puede copiar en el Portapapeles los nombres de los archivos seleccionados para pegar donde necesite"
         Top             =   1130
         Width           =   1290
      End
      Begin VB.CheckBox chkBuscar 
         Caption         =   "Filtros"
         Height          =   500
         Left            =   80
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Despliega un buscador secuencial de nombres de DSN que filtra la grilla a medida que escribe"
         Top             =   640
         Width           =   1290
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "&Cerrar"
         Height          =   500
         Left            =   80
         TabIndex        =   19
         ToolTipText     =   "Salir de esta pantalla"
         Top             =   7320
         Width           =   1290
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirm&ar"
         Height          =   500
         Left            =   80
         TabIndex        =   18
         ToolTipText     =   "Confirma la operaci�n actual"
         Top             =   6840
         Width           =   1290
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Mo&dificar"
         Height          =   500
         Left            =   80
         TabIndex        =   17
         ToolTipText     =   "Modificar el archivo seleccionado"
         Top             =   6360
         Width           =   1290
      End
      Begin VB.CheckBox chkCampos 
         Caption         =   "Campos del archivo"
         Height          =   500
         Left            =   80
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   160
         Width           =   1290
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4680
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   8255
      _Version        =   393216
      Cols            =   12
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      HighLight       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.Frame fraBusqueda 
      Enabled         =   0   'False
      Height          =   1095
      Left            =   120
      TabIndex        =   28
      Top             =   60
      Width           =   10095
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "Filtrar"
         Height          =   300
         Left            =   9240
         TabIndex        =   47
         Top             =   240
         Width           =   735
      End
      Begin VB.ListBox lstFiltroEstados 
         BackColor       =   &H8000000F&
         Height          =   690
         ItemData        =   "frmHEDT1ShellSI.frx":014A
         Left            =   6480
         List            =   "frmHEDT1ShellSI.frx":015D
         Style           =   1  'Checkbox
         TabIndex        =   3
         Top             =   240
         Width           =   2655
      End
      Begin AT_MaskText.MaskText txtBuscarDSNNombre 
         Height          =   300
         Left            =   1440
         TabIndex        =   0
         Top             =   195
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFiltroFechaDesde 
         Height          =   300
         Left            =   1440
         TabIndex        =   1
         Top             =   600
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DataType        =   3
         SpecialFeatures =   -1  'True
         TabOnEnter      =   -1  'True
         KeepFocusOnError=   -1  'True
         BeepOnError     =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFiltroFechaHasta 
         Height          =   300
         Left            =   3000
         TabIndex        =   2
         Top             =   600
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   529
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DataType        =   3
         SpecialFeatures =   -1  'True
         TabOnEnter      =   -1  'True
         KeepFocusOnError=   -1  'True
         BeepOnError     =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Filtrar por fechas desde y hasta :"
         Height          =   465
         Index           =   12
         Left            =   120
         TabIndex        =   39
         Top             =   600
         Width           =   1275
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Filtrar por estado :"
         Height          =   180
         Index           =   11
         Left            =   5040
         TabIndex        =   38
         Top             =   240
         Width           =   1395
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Filtrar por nombre de DSN :"
         Height          =   360
         Index           =   9
         Left            =   120
         TabIndex        =   29
         Top             =   165
         Width           =   1290
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraDatos 
      Caption         =   " Modo "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Left            =   120
      TabIndex        =   20
      Top             =   4800
      Width           =   10095
      Begin VB.ComboBox cmbDatosSensibles 
         Height          =   300
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   960
         Width           =   1095
      End
      Begin AT_MaskText.MaskText txtNomProd 
         Height          =   300
         Left            =   4080
         TabIndex        =   12
         Top             =   960
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtIdArch 
         Height          =   300
         Left            =   960
         TabIndex        =   5
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAppId 
         Height          =   300
         Left            =   960
         TabIndex        =   10
         Top             =   1680
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNombre 
         Height          =   300
         Left            =   4080
         TabIndex        =   6
         Top             =   240
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         MaxLength       =   50
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtRutina 
         Height          =   300
         Left            =   960
         TabIndex        =   7
         Top             =   600
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   300
         Left            =   4080
         TabIndex        =   8
         Top             =   600
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin AT_MaskText.MaskText txtValidez 
         Height          =   300
         Left            =   960
         TabIndex        =   11
         Top             =   1320
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtCOPYBBL 
         Height          =   300
         Left            =   4080
         TabIndex        =   41
         Top             =   1320
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtCOPYNME 
         Height          =   300
         Left            =   4080
         TabIndex        =   44
         Top             =   1680
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         UpperCase       =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Nombre de COPY"
         Height          =   180
         Index           =   13
         Left            =   2520
         TabIndex        =   43
         Top             =   1740
         Width           =   1320
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Librer�a COPY"
         Height          =   180
         Index           =   6
         Left            =   2520
         TabIndex        =   42
         Top             =   1380
         Width           =   1080
      End
      Begin VB.Label lblInstrucciones 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Para trabajar con archivos, deben ser seleccionados (presione  Enter o la barra espaciadora)"
         ForeColor       =   &H00000080&
         Height          =   180
         Left            =   3000
         TabIndex        =   36
         Top             =   0
         Width           =   6945
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Validez"
         Height          =   180
         Index           =   10
         Left            =   120
         TabIndex        =   35
         Top             =   1380
         Width           =   555
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Datos sensibles"
         Height          =   420
         Index           =   5
         Left            =   120
         TabIndex        =   34
         Top             =   960
         Width           =   735
         WordWrap        =   -1  'True
      End
      Begin VB.Label NomProd 
         AutoSize        =   -1  'True
         Caption         =   "Nombre Productivo"
         Height          =   180
         Left            =   2520
         TabIndex        =   33
         Top             =   1020
         Width           =   1455
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         Height          =   180
         Index           =   8
         Left            =   2520
         TabIndex        =   25
         Top             =   660
         Width           =   900
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Rutina"
         Height          =   180
         Index           =   3
         Left            =   120
         TabIndex        =   24
         Top             =   660
         Width           =   480
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Nombre del DSN"
         Height          =   180
         Index           =   2
         Left            =   2520
         TabIndex        =   23
         Top             =   300
         Width           =   1245
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Aplicativo"
         Height          =   180
         Index           =   1
         Left            =   120
         TabIndex        =   22
         Top             =   1740
         Width           =   765
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   21
         Top             =   300
         Width           =   525
      End
   End
   Begin VB.Frame fraVisado 
      Caption         =   " Seguridad Inform�tica "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   120
      TabIndex        =   26
      Top             =   6960
      Width           =   10095
      Begin VB.TextBox txtFechaVB 
         Height          =   300
         Left            =   1080
         TabIndex        =   49
         Top             =   600
         Width           =   1215
      End
      Begin VB.ComboBox cmbDSNEstado 
         Height          =   300
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox txtMotivoRechazo 
         Height          =   615
         Left            =   3360
         MaxLength       =   255
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   48
         Top             =   240
         Width           =   6495
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         Height          =   180
         Index           =   4
         Left            =   120
         TabIndex        =   32
         Top             =   300
         Width           =   525
      End
      Begin VB.Label lblHEDT1Labels 
         AutoSize        =   -1  'True
         Caption         =   "Fch. visado"
         Height          =   180
         Index           =   7
         Left            =   120
         TabIndex        =   27
         Top             =   660
         Width           =   855
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdCampos 
      Height          =   4680
      Left            =   120
      TabIndex        =   45
      Top             =   120
      Visible         =   0   'False
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   8255
      _Version        =   393216
      Cols            =   12
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      HighLight       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.Label lblBusqueda 
      ForeColor       =   &H000000C0&
      Height          =   375
      Left            =   120
      TabIndex        =   31
      Top             =   3840
      Width           =   8535
   End
End
Attribute VB_Name = "frmHEDT1ShellSI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -009- a. AA  25.11.2010 - Se agrego la posibilidad de ver todas las solicitudes a la que interviene el catalogo elegido.
' -010- a. AA  29.11.2010 - Se agrega la posibilidad de enviar un mail informativo(en los casos de rechazo de solicitudes).
' -011- a. AA  30.11.2010 - Se agrega un campo mas a la tabla (referente a nombre productivo).
' -012- a. AA  21.12.2010 - Se agrega el MaskedBox "txtNomProd" para el ingreso del nombre productivo, se valida como el resto de los campos(dependiendo el usuario que ingrese y la opcion que se elija).
' -013- a. FJS 07.01.2011 - Arreglo: se quita toda la parte de actualizaci�n que corresponder�a a DYD.
' -014- a. FJS 21.01.2011 - Arreglo: se pasa el motivo del rechazo para la confecci�n del email.
' -015- a. FJS 31.01.2011 - Nuevo: proceso de comparaci�n de DSN para el cambio de estado masivo.
' -015- b. FJS 02.02.2011 - Nuevo: se cambia el control checkbox por un combobox.
' -016- a. FJS 22.03.2011 - Agregado: se especifica en el correo electr�nico el c�digo del dsn rechazado.
' -017- a. FJS 22.03.2011 - Nuevo: se agrega men� contextual para b�squedas en la grilla.

Option Explicit

Private Const colMultiSelect = 0
Private Const COL_DSN_ID = 1
Private Const COL_DSN_NOM = 2
Private Const COL_DSN_FEULT = 3
Private Const COL_DSN_ENMAS = 4
Private Const COL_DSN_VB = 5
Private Const COL_NOM_VB_USUARIO = 6
Private Const COL_DSN_VB_FE = 7
Private Const COL_DSN_NOMRUT = 8
Private Const COL_NOM_USUARIO = 9
Private Const COL_APP_ID = 10
Private Const COL_APP_NAME = 11
Private Const COL_DSN_DESC = 12
Private Const COL_NOM_ESTADO = 13
Private Const COL_ESTADO = 14
Private Const COL_VALIDO = 15
Private Const COL_DSN_VB_ID = 16
Private Const COL_DSN_TXT = 17
Private Const COL_DSN_NOMPROD = 18
Private Const COL_DSN_USERID = 19
Private Const COL_DSN_VB1 = 20
Private Const COL_DSN_SOLI = 21
Private Const COL_DSN_CPYBBL = 22
Private Const COL_DSN_CPYNOM = 23
Private Const COL_TOTALCOLS = 24

Private Const COL_CPY_ID = 1
Private Const CPO_NOMBRE = 2
Private Const CPO_DESCRI = 3
Private Const CPO_POSICI = 4
Private Const CPO_LONGIT = 5
Private Const CPO_DECIMA = 6
Private Const CPO_TIPODA = 7
Private Const CPO_TIPO = 8
Private Const CPO_SUBTIP = 9
Private Const CPO_RUTINA = 10
Private Const CPO_COLSTO = 11

Public sDSNId As String

Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long
Dim inCarga As Boolean, bSegInf As Boolean
Dim sOpcionSeleccionada As String, cLastSearchPattern As String
Dim auxRST As New ADODB.Recordset                                   ' Recordset auxiliar para el filtro en pantalla
Dim dsn_list_actualizar() As String
'Dim vFiltroEstado() As String
Dim sFiltroEstado As String
Dim bFiltrado As Boolean
Dim j As Integer
Dim nRwD As Integer, nRwH As Integer                                ' Para el rango de selecci�n de filas

'{ add -015- a.
Dim dsn_ref() As udt_HEDT003
Private Type udt_HEDT003
    cpo_nrobyte As Long
    cpo_canbyte As Long
    cpo_type As String
    cpo_decimals As Integer
    cpo_nomrut As String
End Type
'}

Private Sub Form_Load()
    inCarga = True
    Call Inicializar
    Call HabilitarBotones(2)
    inCarga = False
End Sub

Private Sub Inicializar()
    Call Puntero(True)
    Call Status("Inicializando...")
    Call LockProceso(True)
    DoEvents
    Call setHabilCtrl(txtIdArch, "DIS")
    Call setHabilCtrl(txtValidez, "DIS")
    Call setHabilCtrl(txtCOPYBBL, "DIS")
    Call setHabilCtrl(txtCOPYNME, "DIS")
    Call setHabilCtrl(txtFechaVB, "DIS")
    Call setHabilCtrl(cmbDSNEstado, "DIS")
    Call setHabilCtrl(txtMotivoRechazo, "DIS")
    Call CargarEstados("TODOS")
    With cmbDatosSensibles
        .Clear
        .AddItem "Si" & Space(100) & "||S"
        .AddItem "No" & Space(100) & "||N"
        .ListIndex = 0
    End With
    With lstFiltroEstados
        .Clear
        .AddItem "D: A revisar DyD"
        .AddItem "N: Pendiente revisi�n"
        .AddItem "P: Pendiente pos-revisi�n"
        .AddItem "R: Rechazado"
        .AddItem "S: Visado"
        .AddItem "H: Hist�rico"
    End With
    chkCampos.ToolTipText = "Visualizar los campos definidos para el DSN seleccionado"
    chkBuscar.ToolTipText = "Permite filtrar los datos de la grilla"
    chkBuscar.Value = 1     ' Empieza con los filtros activos
    txtFiltroFechaDesde = DateAdd("m", -3, date)
    lstFiltroEstados.Selected(1) = True
    lstFiltroEstados.Selected(2) = True
    Call IniciarScroll(grdDatos)
    Call LockProceso(False)
End Sub

Private Sub CargarEstados(sOpcion As String)
    Call LockProceso(True)
    Select Case sOpcion
        Case "M"      ' Modificar
            With cmbDSNEstado
                .Clear
                .AddItem "A revisar DyD" & Space(100) & "||D"
                .AddItem "Visado" & Space(100) & "||S"
                .AddItem "Rechazado" & Space(100) & "||R"
                .ListIndex = 0
            End With
        Case "N"      ' Modificar (A revisar por DyD)
            With cmbDSNEstado
                .Clear
                .AddItem "A revisar DyD" & Space(100) & "||D"
                .AddItem "Rechazado" & Space(100) & "||R"
                .ListIndex = 0
            End With
        Case "H", "S", "R"
            With cmbDSNEstado
                .Clear
                '{ add 17.05.2013
                .AddItem "A revisar DyD" & Space(100) & "||D"
                .AddItem "Visado" & Space(100) & "||S"
                .AddItem "Rechazado" & Space(100) & "||R"
                '}
                .AddItem "Hist�rico" & Space(100) & "||H"
                .ListIndex = 0
            End With
        Case Else   ' Todos
            With cmbDSNEstado
                .Clear
                .AddItem "Pendiente revisi�n" & Space(100) & "||N"
                .AddItem "Pendiente pos-revisi�n" & Space(100) & "||P"
                .AddItem "A revisar DyD" & Space(100) & "||D"
                .AddItem "Visado" & Space(100) & "||S"
                .AddItem "Rechazado" & Space(100) & "||R"
                .AddItem "Hist�rico" & Space(100) & "||H"
                .ListIndex = 0
            End With
    End Select
    Call LockProceso(False)
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .Font.name = "Tahoma"
        .Font.Size = 8
        .cols = COL_TOTALCOLS
        .FixedCols = 1
        .Rows = 1
        .FocusRect = flexFocusLight
        .HighLight = flexHighlightAlways
        .TextMatrix(0, colMultiSelect) = "": .ColWidth(colMultiSelect) = 150
        .TextMatrix(0, COL_DSN_ID) = "C�digo": .ColWidth(COL_DSN_ID) = 1000
        .TextMatrix(0, COL_DSN_NOM) = "Nombre del archivo": .ColWidth(COL_DSN_NOM) = 4000
        .TextMatrix(0, COL_DSN_FEULT) = "Ult. modificaci�n": .ColWidth(COL_DSN_FEULT) = 1600
        .TextMatrix(0, COL_DSN_ENMAS) = "DS": .ColWidth(COL_DSN_ENMAS) = 600: .ColAlignment(COL_DSN_ENMAS) = flexAlignCenterCenter
        .TextMatrix(0, COL_DSN_NOMRUT) = "Rutina": .ColWidth(COL_DSN_NOMRUT) = 800
        .TextMatrix(0, COL_NOM_USUARIO) = "Propietario": .ColWidth(COL_NOM_USUARIO) = 1800
        .TextMatrix(0, COL_DSN_VB) = "Visado": .ColWidth(COL_DSN_VB) = 2000 '1000
        .TextMatrix(0, COL_NOM_VB_USUARIO) = "Por": .ColWidth(COL_NOM_VB_USUARIO) = 1400
        .TextMatrix(0, COL_DSN_VB_FE) = "Fch. Visado": .ColWidth(COL_DSN_VB_FE) = 1500
        .TextMatrix(0, COL_APP_ID) = "Aplicativo": .ColWidth(COL_APP_ID) = 1000
        .TextMatrix(0, COL_APP_NAME) = "Nombre del aplicativo": .ColWidth(COL_APP_NAME) = 2000
        .TextMatrix(0, COL_DSN_DESC) = "Descripci�n del archivo": .ColWidth(COL_DSN_DESC) = 4000
        .TextMatrix(0, COL_NOM_ESTADO) = "Estado": .ColWidth(COL_NOM_ESTADO) = 1600
        .TextMatrix(0, COL_ESTADO) = "": .ColWidth(COL_ESTADO) = 0
        .TextMatrix(0, COL_VALIDO) = "Validez": .ColWidth(COL_VALIDO) = 800
        .TextMatrix(0, COL_DSN_VB_ID) = "dsn_vb": .ColWidth(COL_DSN_VB_ID) = 0
        .TextMatrix(0, COL_DSN_TXT) = "Motivo de rechazo": .ColWidth(COL_DSN_TXT) = 2600
        .TextMatrix(0, COL_DSN_NOMPROD) = "Nombre productivo": .ColWidth(COL_DSN_NOMPROD) = 3500
        .TextMatrix(0, COL_DSN_USERID) = "dsn_userid": .ColWidth(COL_DSN_USERID) = 0
        .TextMatrix(0, COL_DSN_VB1) = "dsn_vb1": .ColWidth(COL_DSN_VB1) = 0
        .TextMatrix(0, COL_DSN_SOLI) = "Soli. #": .ColWidth(COL_DSN_SOLI) = 600
        .TextMatrix(0, COL_DSN_CPYBBL) = "Biblioteca de COPY": .ColWidth(COL_DSN_CPYBBL) = 2000
        .TextMatrix(0, COL_DSN_CPYNOM) = "Nombre de COPY": .ColWidth(COL_DSN_CPYNOM) = 2000
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub CargarGrilla()
    Dim i As Long
    
    bFiltrado = False
    Call Puntero(True)
    Call InicializarGrilla
    If ClearNull(txtBuscarDSNNombre) <> "" Or ClearNull(txtFiltroFechaDesde) <> "" Or ClearNull(txtFiltroFechaHasta) <> "" Or ClearNull(sFiltroEstado) <> "" Then
        bFiltrado = True
    End If
    If sp_GetHEDT101(Null, txtBuscarDSNNombre, txtFiltroFechaDesde, txtFiltroFechaHasta, sFiltroEstado) Then
        With grdDatos
            i = 1
            .Redraw = False
            .Rows = aplRST.RecordCount + 1
            Do While Not aplRST.EOF
                .TextMatrix(i, COL_DSN_ID) = ClearNull(aplRST.Fields!dsn_id)
                .TextMatrix(i, COL_DSN_NOM) = ClearNull(aplRST.Fields!dsn_nom)
                .TextMatrix(i, COL_DSN_ENMAS) = ClearNull(aplRST.Fields!dsn_enmas)
                .TextMatrix(i, COL_DSN_NOMRUT) = ClearNull(aplRST.Fields!dsn_nomrut)
                .TextMatrix(i, COL_DSN_FEULT) = Format(ClearNull(aplRST.Fields!dsn_feult), "yyyy/mm/dd hh:MM")
                .TextMatrix(i, COL_NOM_USUARIO) = ClearNull(aplRST.Fields!nom_usuario)
                .TextMatrix(i, COL_DSN_VB) = ClearNull(aplRST.Fields!dsn_vb)
                .TextMatrix(i, COL_NOM_VB_USUARIO) = ClearNull(aplRST.Fields!nom_vb_usuario)
                .TextMatrix(i, COL_DSN_VB_FE) = Format(ClearNull(aplRST.Fields!dsn_vb_fe), "yyyy/mm/dd hh:MM")
                .TextMatrix(i, COL_APP_ID) = ClearNull(aplRST.Fields!app_id)
                .TextMatrix(i, COL_APP_NAME) = ClearNull(aplRST.Fields!app_name)
                .TextMatrix(i, COL_DSN_DESC) = ClearNull(aplRST.Fields!dsn_desc)
                .TextMatrix(i, COL_NOM_ESTADO) = ClearNull(aplRST.Fields!nom_estado)
                .TextMatrix(i, COL_ESTADO) = ClearNull(aplRST.Fields!Estado)
                .TextMatrix(i, COL_VALIDO) = ClearNull(aplRST.Fields!valido)
                .TextMatrix(i, COL_DSN_VB_ID) = ClearNull(aplRST.Fields!dsn_vb_id)
                .TextMatrix(i, COL_DSN_TXT) = ClearNull(aplRST.Fields!dsn_txt)
                .TextMatrix(i, COL_DSN_NOMPROD) = ClearNull(aplRST.Fields!dsn_nomprod)
                .TextMatrix(i, COL_DSN_USERID) = ClearNull(aplRST.Fields!dsn_userid)
                .TextMatrix(i, COL_DSN_VB1) = ClearNull(aplRST.Fields!dsn_vb1)
                .TextMatrix(i, COL_DSN_SOLI) = ClearNull(aplRST.Fields!dsn_cantuso)
                .TextMatrix(i, COL_DSN_CPYBBL) = ClearNull(aplRST.Fields!dsn_cpybbl)
                .TextMatrix(i, COL_DSN_CPYNOM) = ClearNull(aplRST.Fields!dsn_cpynom)
                i = i + 1
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
    'If aplRST.State = 1 Then Call Status(aplRST.RecordCount & " dsn(s).")
    If aplRST.State = 1 Then Call Status(aplRST.RecordCount & " dsn(s)" & IIf(bFiltrado, " (filtrado).", ""))
        'If bFiltrado Then
        '    Call Status(aplRST.RecordCount & " dsn(s) (filtrado).")
        'Else
        '    Call Status(aplRST.RecordCount & " dsn(s).")
        'End If
    'End If
    inCarga = False
    'Set auxRST = aplRST.Clone        ' add -005- d. "Clonamos" el RS original para el filtrado
    'If bFiltrado Then
    '    Call Filtrar
    'Else
    '    Call MostrarSeleccion
    'End If
    Call MostrarSeleccion
    grdDatos.Redraw = True
    Call Puntero(False)
End Sub
 
Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Call HabilitarBotones(0)
End Sub

Private Sub cmdModificar_Click()
    Dim j As Integer
    Dim bSeleccion As Boolean
    
    bSeleccion = False
    j = 0
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    With grdDatos
        If .TextMatrix(.RowSel, colMultiSelect) = "�" Then  ' Si el registro actual est� seleccionado, alcanza
            sOpcionSeleccionada = "M"
            Call HabilitarBotones(1, False)
        Else    ' Reviso que no haya otros registros seleccionados
            For j = 1 To .Rows - 1
                If .TextMatrix(j, colMultiSelect) = "�" Then
                    bSeleccion = True
                    Exit For
                End If
            Next j
            If bSeleccion Then
                sOpcionSeleccionada = "M"
                Call HabilitarBotones(1, False)
            Else
                MsgBox "No ha seleccionado archivos para trabajar." & vbCrLf & "Seleccione presionando Enter o la barra espaciadora.", vbExclamation + vbOKOnly, "No hay selecci�n"
            End If
        End If
    End With
    LockProceso (False)
    ' ************************************************************************************************************
    'If Not LockProceso(True) Then
    '    Exit Sub
    'End If
    'sOpcionSeleccionada = "M"
    'Call HabilitarBotones(1, False)
    'LockProceso (False)
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()
    Call ProcesarSeleccion
End Sub

Private Sub ProcesarSeleccion()
    '****************************************************************************************************
    ' Nombre:       ProcesarSeleccion
    ' Prop�sito:    Para los DSN seleccionados (dsn_list_actualizar),
    ' Fecha:        01.01.1999
    ' Autor:        Fernando J. Spitz (XA00309)
    ' Descripci�n:  Carga en el vector <dsn_list> todos los DSN que tienen la misma composici�n de campos
    '               que el DSN de origen y mismo nombre productivo.
    '****************************************************************************************************
    Dim sFechaProceso As String
    Dim cMensaje As String
    Dim bContinuar As Boolean
    Dim vDestinatario() As String
    Dim x As Integer, i As Integer

    If Not LockProceso(True) Then
        Exit Sub
    End If
        
    j = 0
    sFechaProceso = Format(Now, "yyyymmdd hh:MM")
    Erase vDestinatario
    Erase dsn_list_actualizar                           ' Inicializa el vector (01.08.2012)
    
    If CamposObligatorios Then
        Call Puntero(True)
        Call Status("Procesando...")
        bContinuar = True
        With grdDatos
            If CodigoCombo(cmbDSNEstado, True) = "H" Then   ' Controlo que todos los DSN tengan el mismo estado
                For x = 1 To .Rows - 1
                    If .TextMatrix(x, colMultiSelect) = "�" Then
                        If InStr(1, "R|S|", .TextMatrix(x, COL_DSN_VB1), vbTextCompare) = 0 Then
                            bContinuar = False
                            MsgBox "Los DSN seleccionados para pasar a hist�rico" & vbCrLf & _
                                   "deben estar en estado Visados o Rechazados.", vbExclamation + vbOKOnly
                            Exit For
                        End If
                    End If
                Next x
            End If
            If bContinuar Then
                For x = 1 To .Rows - 1
                    If .TextMatrix(x, colMultiSelect) = "�" Then
                        Call ProcesarDSN(.TextMatrix(x, COL_DSN_ID), .TextMatrix(x, COL_DSN_ENMAS))
                    End If
                Next x
                For x = 0 To UBound(dsn_list_actualizar)
                    Call sp_UpdateHEDT101(dsn_list_actualizar(x), CodigoCombo(cmbDSNEstado, True), ClearNull(txtMotivoRechazo), sFechaProceso, glLOGIN_ID_REEMPLAZO)
                Next x
                ' Para "Rechazo" o "A revisar DyD" se envia correo electr�nico al propietario del DSN
                If InStr(1, "R|D|", CodigoCombo(cmbDSNEstado, True), vbTextCompare) > 0 Then
                    If sp_GetHEDT101b("A", Null, sFechaProceso, CodigoCombo(cmbDSNEstado, True)) Then
                        Do While Not aplRST.EOF
                            ReDim Preserve vDestinatario(i)
                            vDestinatario(i) = ClearNull(aplRST.Fields!dsn_userid)
                            i = i + 1
                            aplRST.MoveNext
                            DoEvents
                        Loop
                    End If
                    For i = 0 To UBound(vDestinatario)
                        Call EnviarCorreo_Rechazo(vDestinatario(i), sFechaProceso, CodigoCombo(cmbDSNEstado, True), True)
                    Next i
                End If
            End If
        End With
        Call Status("Listo.")
        Call Puntero(False)
        Call HabilitarBotones(0)
        Exit Sub
    End If
    LockProceso (False)
End Sub

Private Sub ProcesarDSN(dsn_id, DSN_Ds)
    '****************************************************************************************************
    ' Nombre:       ProcesarDSN
    ' Prop�sito:    Si un DSN tiene declarados "datos sensibles",
    ' Fecha:        01.01.1999
    ' Autor:        Fernando J. Spitz (XA00309)
    ' Descripci�n:  Carga en el vector <dsn_list> todos los DSN que tienen la misma composici�n de campos
    '               que el DSN de origen y mismo nombre productivo.
    '****************************************************************************************************
    Dim i As Integer
    Dim dsn_list() As String
    Dim sComposicion1 As String, sComposicion2 As String
    
    If DSN_Ds = "Si" And CodigoCombo(cmbDSNEstado, True) = "S" Then     ' Tiene declarados datos sensibles y es ser� Visado (y no se pasa a Hist�rico)
        If sp_GetHEDT003(dsn_id, Null, Null) Then                       ' Obtiene la composici�n de campos para el DSN seleccionado
            Do While Not aplRST.EOF
                sComposicion1 = sComposicion1 & IIf(Trim(sComposicion1) = "", "", vbCrLf) & _
                    ClearNull(aplRST.Fields!cpo_type) & "|" & _
                    ClearNull(aplRST.Fields!cpo_canbyte) & "|" & _
                    ClearNull(aplRST.Fields!cpo_nrobyte) & "|" & _
                    ClearNull(aplRST.Fields!cpo_decimals) & "|" & _
                    ClearNull(aplRST.Fields!cpo_nomrut)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If sp_GetHEDT101a(dsn_id) Then                  ' Obtiene todos los DSN que tienen el mismo nombre productivo que el DSN seleccionado
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!valido) = "S" Then
                    ReDim Preserve dsn_list(i)
                    dsn_list(i) = ClearNull(aplRST.Fields!dsn_id)
                    i = i + 1
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        For i = 0 To UBound(dsn_list)
            sComposicion2 = ""
            If sp_GetHEDT003(dsn_list(i), Null, Null) Then
                Do While Not aplRST.EOF
                    sComposicion2 = sComposicion2 & IIf(Trim(sComposicion2) = "", "", vbCrLf) & _
                        ClearNull(aplRST.Fields!cpo_type) & "|" & _
                        ClearNull(aplRST.Fields!cpo_canbyte) & "|" & _
                        ClearNull(aplRST.Fields!cpo_nrobyte) & "|" & _
                        ClearNull(aplRST.Fields!cpo_decimals) & "|" & _
                        ClearNull(aplRST.Fields!cpo_nomrut)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            If sComposicion1 = sComposicion2 Then
                If NoExisteDSN(dsn_list(i)) Then
                    ReDim Preserve dsn_list_actualizar(j)
                    dsn_list_actualizar(j) = dsn_list(i)
                    j = j + 1
                End If
            End If
        Next i
    ElseIf CodigoCombo(cmbDSNEstado, True) = "D" Then     ' Tiene declarados datos sensibles y es sera devuelto a DyD para revisi�n
        If sp_GetHEDT101a(dsn_id) Then                    ' Listo todos los DSN que refieren al mismo nombre productivo. Si no hay otros, el sp devuelve solo uno (a �l mismo).
            Do While Not aplRST.EOF
                If NoExisteDSN(ClearNull(aplRST.Fields!dsn_id)) Then
                    ReDim Preserve dsn_list_actualizar(j)
                    dsn_list_actualizar(j) = ClearNull(aplRST.Fields!dsn_id)
                    j = j + 1
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    Else                                        ' No tiene datos sensibles (??)
        If sp_GetHEDT101a(dsn_id) Then          ' Listo todos los DSN que refieren al mismo nombre productivo. Si no hay otros, el sp devuelve solo uno (a �l mismo).
            Do While Not aplRST.EOF
                If InStr(1, "R|H|", CodigoCombo(cmbDSNEstado, True), vbTextCompare) > 0 Then    ' Si se va a rechazar/pasar a hist�rico, no importa su validez
                    If NoExisteDSN(ClearNull(aplRST.Fields!dsn_id)) Then
                        ReDim Preserve dsn_list_actualizar(j)
                        dsn_list_actualizar(j) = ClearNull(aplRST.Fields!dsn_id)
                        j = j + 1
                    End If
                Else
                    If ClearNull(aplRST.Fields!valido) = "S" Then
                        If NoExisteDSN(ClearNull(aplRST.Fields!dsn_id)) Then
                            ReDim Preserve dsn_list_actualizar(j)
                            dsn_list_actualizar(j) = ClearNull(aplRST.Fields!dsn_id)
                            j = j + 1
                        End If
                    End If
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        Else
            j = j + 1
            ReDim Preserve dsn_list_actualizar(j)
            dsn_list_actualizar(j) = dsn_id
        End If
    End If
End Sub

Private Function NoExisteDSN(dsn_id) As Boolean
    Dim i As Integer
    
    NoExisteDSN = True
    If j > 0 Then
        For i = 0 To UBound(dsn_list_actualizar)
            If dsn_list_actualizar(i) = dsn_id Then
                NoExisteDSN = False
                Exit For
            End If
        Next i
    End If
End Function

Private Function CamposObligatorios() As Boolean
    Dim i As Long
    
    CamposObligatorios = True
    
    With grdDatos
        For i = 1 To .Rows - 1
            If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                '{ del 25.03.2013
                'If ClearNull(.TextMatrix(i, COL_DSN_NOMPROD)) = "" Then     ' Nombre productivo
                '    MsgBox "Falta especificar el nombre productivo del DSN: " & vbCrLf & ClearNull(.TextMatrix(i, COL_DSN_ID)) & " - " & ClearNull(.TextMatrix(i, COL_DSN_NOM)), vbExclamation + vbOKOnly, "Nombre productivo de archivo"
                '    CamposObligatorios = False
                '    Exit Function
                'End If
                '}
                Select Case CodigoCombo(cmbDSNEstado, True)                 ' Motivo de rechazo
                    Case "R"
                        If ClearNull(txtMotivoRechazo) = "" Then
                            MsgBox "Debe ingresar el motivo de rechazo.", vbExclamation + vbOKOnly, "Motivo de rechazo"
                            CamposObligatorios = False
                            Exit Function
                        End If
                End Select
                If CodigoCombo(cmbDSNEstado, True) = "S" Then
                    If .TextMatrix(.RowSel, COL_VALIDO) = "No" Then             ' Control de validez
                        MsgBox "El DSN " & ClearNull(.TextMatrix(i, COL_DSN_NOM)) & " (" & ClearNull(.TextMatrix(i, COL_DSN_ID)) & ") no tiene campos definidos." & vbCrLf & "No puede ser visado.", vbExclamation + vbOKOnly
                        CamposObligatorios = False
                        Exit Function
                    End If
                End If
            End If
        Next
    End With
End Function

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            Call setHabilCtrl(chkBuscar, "NOR")
            Call setHabilCtrl(cmdModificar, "DIS")
            Call setHabilCtrl(cmdCopiar, "NOR")
            Call setHabilCtrl(chkCampos, "NOR")
            Call setHabilCtrl(cmdConfirmar, "DIS")
            Call setHabilCtrl(cmdImprimir, "NOR")
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            ' Controles de datos
            Call setHabilCtrl(txtIdArch, "DIS")
            Call setHabilCtrl(txtNombre, "DIS")
            Call setHabilCtrl(txtNomProd, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtAppId, "DIS")
            Call setHabilCtrl(txtCOPYBBL, "DIS")
            Call setHabilCtrl(txtCOPYNME, "DIS")
            Call setHabilCtrl(cmbDatosSensibles, "DIS")
            Call setHabilCtrl(cmbDSNEstado, "DIS")
            Call setHabilCtrl(txtMotivoRechazo, "DIS")
            sOpcionSeleccionada = ""
            Call CargarEstados(sOpcionSeleccionada)
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdModificar.Enabled = False
            chkBuscar.Enabled = False
            cmdCopiar.Enabled = False
            chkCampos.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "M"        ' Controles de datos
                    Call setHabilCtrl(txtNombre, "DIS")
                    Call setHabilCtrl(txtNomProd, "DIS")
                    Call setHabilCtrl(txtRutina, "DIS")
                    Call setHabilCtrl(txtDescripcion, "DIS")
                    Call setHabilCtrl(txtAppId, "DIS")
                    Call setHabilCtrl(cmbDatosSensibles, "DIS")
                    Call setHabilCtrl(cmbDSNEstado, "NOR")
                    Call setHabilCtrl(txtMotivoRechazo, "NOR")
                    Call setHabilCtrl(cmdImprimir, "DIS")
                    fraDatos.Caption = " MODIFICAR "
                    txtIdArch.Enabled = False
                    fraDatos.Enabled = True
                    If InStr(1, "S|R|", CodigoCombo(cmbDSNEstado, True), vbTextCompare) > 0 Then
                        Call CargarEstados("H")
                    Else
                        If CodigoCombo(cmbDSNEstado, True) = "D" Then
                            Call CargarEstados("N")
                        Else
                            Call CargarEstados("M")
                        End If
                    End If
                    cmbDSNEstado.SetFocus
            End Select
        Case 2
            fraDatos.Caption = " VISAR "
            grdDatos.Enabled = True
            Call setHabilCtrl(chkCampos, "NOR")
            Call setHabilCtrl(cmdCopiar, "NOR")
            Call setHabilCtrl(cmdCerrar, "NOR")
            Call setHabilCtrl(cmdModificar, "DIS")
            Call setHabilCtrl(cmdConfirmar, "DIS")
            ' Controles de datos
            Call setHabilCtrl(txtIdArch, "DIS")
            Call setHabilCtrl(txtNombre, "DIS")
            Call setHabilCtrl(txtNomProd, "DIS")
            Call setHabilCtrl(txtCOPYBBL, "DIS")
            Call setHabilCtrl(txtCOPYNME, "DIS")
            Call setHabilCtrl(txtRutina, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtAppId, "DIS")
            Call setHabilCtrl(cmbDatosSensibles, "DIS")     ' add -015- b.
            Call setHabilCtrl(cmbDSNEstado, "DIS")
            Call setHabilCtrl(txtMotivoRechazo, "DIS")
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrilla
            Else
                Call MostrarSeleccion
            End If
    End Select
    Call LockProceso(False)
    Call Puntero(False)
End Sub

Public Sub MostrarSeleccion(Optional flgSort)
    On Error GoTo Errores
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    If Not inCarga Then
        With grdDatos
            If .RowSel > 0 Then
                If .TextMatrix(.RowSel, COL_DSN_ID) <> "" Then
                    txtIdArch = ClearNull(.TextMatrix(.RowSel, COL_DSN_ID))
                    txtNombre = ClearNull(.TextMatrix(.RowSel, COL_DSN_NOM))
                    txtRutina = ClearNull(.TextMatrix(.RowSel, COL_DSN_NOMRUT))
                    txtDescripcion = ClearNull(.TextMatrix(.RowSel, COL_DSN_DESC))
                    txtAppId = ClearNull(.TextMatrix(.RowSel, COL_APP_ID))
                    txtAppId.ToolTipText = ClearNull(.TextMatrix(.RowSel, COL_APP_NAME))
                    txtNomProd = ClearNull(.TextMatrix(.RowSel, COL_DSN_NOMPROD))
                    txtCOPYBBL = ClearNull(.TextMatrix(.RowSel, COL_DSN_CPYBBL))
                    txtCOPYNME = ClearNull(.TextMatrix(.RowSel, COL_DSN_CPYNOM))
                    txtValidez = ClearNull(.TextMatrix(.RowSel, COL_VALIDO))
                    cmbDatosSensibles.ListIndex = PosicionCombo(cmbDatosSensibles, Left(ClearNull(.TextMatrix(.RowSel, COL_DSN_ENMAS)), 1), True)   ' add -015- b.
                    If chkBuscar.Value <> 1 Then Call setHabilCtrl(cmdModificar, "NOR")
                    cmbDSNEstado.ListIndex = PosicionCombo(cmbDSNEstado, IIf(ClearNull(.TextMatrix(.RowSel, COL_DSN_VB_ID)) = "", "N", .TextMatrix(.RowSel, COL_DSN_VB_ID)), True) ' add -008- b.
                    'txtFechaVB.DateValue = Format(.TextMatrix(.RowSel, COL_DSN_VB_FE), "dd/mm/yyyy"): txtFechaVB.Text = Format(txtFechaVB.DateValue, "dd/mm/yyyy")
                    txtFechaVB = Format(.TextMatrix(.RowSel, COL_DSN_VB_FE), "dd/mm/yyyy")
                    txtMotivoRechazo.Text = ClearNull(.TextMatrix(.RowSel, COL_DSN_TXT))
                Else
                    txtIdArch = ""
                    txtNombre = ""
                    txtRutina = ""
                    txtDescripcion = ""
                    txtAppId = ""
                    txtAppId.ToolTipText = ""
                    txtNomProd = ""
                    txtCOPYBBL = ""
                    txtCOPYNME = ""
                    txtValidez = ""
                    cmbDatosSensibles.ListIndex = -1
                    Call setHabilCtrl(cmdModificar, "DIS")
                    cmbDSNEstado.ListIndex = -1
                    txtFechaVB.Text = ""
                    txtMotivoRechazo.Text = ""
                End If
            Else
                txtIdArch = ""
                txtNombre = ""
                txtRutina = ""
                txtDescripcion = ""
                txtAppId = ""
                txtAppId.ToolTipText = ""
                txtNomProd = ""
                txtCOPYBBL = ""
                txtCOPYNME = ""
                txtValidez = ""
                cmbDatosSensibles.ListIndex = -1
                Call setHabilCtrl(cmdModificar, "DIS")
                cmbDSNEstado.ListIndex = -1
                txtFechaVB.Text = ""
                txtMotivoRechazo.Text = ""
            End If
        End With
    End If
Exit Sub
Errores:
    If Err.Number = 16 Then
        Stop
        Resume Next
    End If
End Sub

'{ add -009- a.
Private Sub grdDatos_DblClick()
    Call grdDatos_KeyDown(13, 0)
End Sub
'}

Private Sub grdDatos_Click()
    bSorting = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    bSorting = False
End Sub

Private Sub grdDatos_RowColChange()
    If Not bSorting Then MostrarSeleccion
End Sub

Private Sub grdDatos_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim nRwDT As Long
    
    bSorting = True
    nRwDT = nRwD
    With grdDatos
        Select Case KeyCode
            Case 13, 32     ' Enter, Espacio
                Do While nRwDT <= nRwH
                    .TextMatrix(nRwDT, colMultiSelect) = IIf(.TextMatrix(nRwDT, colMultiSelect) = "�", "", "�")
                    nRwDT = nRwDT + 1
                Loop
            Case 46         ' Delete
                Do While nRwDT <= nRwH
                    .TextMatrix(nRwDT, colMultiSelect) = ""
                    nRwDT = nRwDT + 1
                Loop
        End Select
    End With
    bSorting = False
End Sub

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    nRwD = 0
    nRwH = 0
    With grdDatos
        nRwD = IIf(.Row <= grdDatos.RowSel, .Row, .RowSel)
        nRwH = IIf(.Row <= grdDatos.RowSel, .RowSel, .Row)
    End With
End Sub

'{ add -004- a.
Private Sub grdDatos_SelChange()
    nRwD = 0
    nRwH = 0
    With grdDatos
        nRwD = IIf(.Row <= grdDatos.RowSel, .Row, .RowSel)
        nRwH = IIf(.Row <= grdDatos.RowSel, .RowSel, .Row)
    End With
End Sub
'}

'Private Sub cmdAppSearch_Click()
'    Dim vRetorno() As String
'    frmSelAplicativo.cAmbiente = "H"
'    frmSelAplicativo.Show 1
'    If ParseString(vRetorno, glAuxRetorno, ":") > 0 Then
'        txtAppId = vRetorno(1)
'        txtAppId.ToolTipText = Mid(vRetorno(2), 1, InStr(1, vRetorno(2), "|", vbTextCompare) - 1)
'    End If
'End Sub
'
'Private Sub txtAppId_LostFocus()
'    If Len(txtAppId) > 0 And Trim(txtAppId) <> "" Then
'        If sp_GetAplicativo(txtAppId, Null, "S") Then
'            txtAppId.ToolTipText = ClearNull(aplRST.Fields!app_nombre)
'        Else
'            MsgBox "El aplicativo ingresado no existe o se encuentra inhabilitado.", vbExclamation + vbOKOnly, "Aplicativo inexistente o inv�lido"
'            txtAppId.SetFocus
'        End If
'    Else
'        txtAppId.ToolTipText = ""
'    End If
'End Sub

Private Sub chkCampos_Click()
    If chkCampos.Value = 1 Then
        If Len(txtIdArch) > 0 Then
            fraBusqueda.Visible = False
            grdDatos.Visible = False
            Call setHabilCtrl(chkBuscar, "DIS")
            Call setHabilCtrl(cmdCopiar, "DIS")
            Call setHabilCtrl(cmdCerrar, "DIS")
            Call setHabilCtrl(cmdModificar, "DIS")
            Call setHabilCtrl(cmdImprimir, "DIS")
            Call setHabilCtrl(cmdExportar, "DIS")
            Call setHabilCtrl(cmdSolicitudes, "DIS")
            Call CargarCampos
            grdCampos.Visible = True
        End If
    Else
        grdCampos.Visible = False
        fraBusqueda.Visible = True
        grdDatos.Visible = True
        Call setHabilCtrl(chkBuscar, "NOR")
        Call setHabilCtrl(cmdCopiar, "DIS")
        Call setHabilCtrl(cmdImprimir, "NOR")
        Call setHabilCtrl(cmdExportar, "NOR")
        Call setHabilCtrl(cmdSolicitudes, "NOR")
        Call setHabilCtrl(cmdCerrar, "NOR")
    End If
End Sub

Private Sub InicializarCampos()
    With grdCampos
        .Font.name = "Courier New"
        .Font.Size = 8
        .cols = CPO_COLSTO
        .FixedCols = 1
        .Rows = 1
        .FocusRect = flexFocusLight
        .HighLight = flexHighlightAlways
        .MergeCells = flexMergeFree
        .TextMatrix(0, COL_DSN_ID - 1) = "DSN": .ColWidth(COL_DSN_ID - 1) = 1500
        .TextMatrix(0, COL_CPY_ID) = "": .ColWidth(COL_CPY_ID) = 0
        .TextMatrix(0, CPO_NOMBRE) = "Campo": .ColWidth(CPO_NOMBRE) = 2500
        .TextMatrix(0, CPO_DESCRI) = "Descripci�n": .ColWidth(CPO_DESCRI) = 3000
        .TextMatrix(0, CPO_POSICI) = "# Posic.": .ColWidth(CPO_POSICI) = 1000
        .TextMatrix(0, CPO_LONGIT) = "Longitud": .ColWidth(CPO_LONGIT) = 1000
        .TextMatrix(0, CPO_DECIMA) = "# Decim.": .ColWidth(CPO_DECIMA) = 1000
        .TextMatrix(0, CPO_TIPODA) = "Tipo de dato": .ColWidth(CPO_TIPODA) = 0
        .TextMatrix(0, CPO_TIPO) = "Tipo": .ColWidth(CPO_TIPO) = 2000
        .TextMatrix(0, CPO_SUBTIP) = "Subtipo": .ColWidth(CPO_SUBTIP) = 2500
        .TextMatrix(0, CPO_RUTINA) = "Rutina": .ColWidth(CPO_RUTINA) = 2500
        Call CambiarEfectoLinea(grdCampos, prmGridEffectFontBold)
        .MergeCol(0) = True
    End With
End Sub

Private Sub CargarCampos()
    Dim i As Long
    Call InicializarCampos
    If sp_GetHEDT003(txtIdArch, Null, Null) Then
        With grdCampos
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, COL_DSN_ID - 1) = ClearNull(aplRST.Fields!dsn_id)
                .TextMatrix(.Rows - 1, COL_CPY_ID) = ClearNull(aplRST.Fields!CPO_ID)
                .TextMatrix(.Rows - 1, CPO_NOMBRE) = ClearNull(aplRST.Fields!CPO_ID)
                .TextMatrix(.Rows - 1, CPO_DESCRI) = ClearNull(aplRST.Fields!CPO_DSC)
                .TextMatrix(.Rows - 1, CPO_POSICI) = ClearNull(aplRST.Fields!cpo_nrobyte)
                .TextMatrix(.Rows - 1, CPO_LONGIT) = ClearNull(aplRST.Fields!cpo_canbyte)
                .TextMatrix(.Rows - 1, CPO_DECIMA) = IIf(Val(ClearNull(aplRST.Fields!cpo_decimals)) = 0 Or ClearNull(aplRST.Fields!cpo_decimals) = "", "0", ClearNull(aplRST.Fields!cpo_decimals))
                .TextMatrix(.Rows - 1, CPO_TIPODA) = ClearNull(aplRST.Fields!cpo_type)
                .TextMatrix(.Rows - 1, CPO_TIPO) = ClearNull(aplRST.Fields!tipo_nom)
                .TextMatrix(.Rows - 1, CPO_SUBTIP) = ClearNull(aplRST.Fields!subtipo_nom)
                .TextMatrix(.Rows - 1, CPO_RUTINA) = ClearNull(aplRST.Fields!cpo_nomrut)
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
End Sub

'Private Sub txtBuscarDSNNombre_Change()
'    Call Filtrar     ' Ejecuto el filtrado de la grilla
'End Sub

Private Sub cmdFiltrar_Click()
    If FiltrosValidos Then
        Call Filtrar
        chkBuscar.Value = 0
    End If
End Sub

Private Function FiltrosValidos() As Boolean
    FiltrosValidos = True
'    If ClearNull(txtFiltroFechaDesde.Text) <> "" Then
'        If IsDate(txtFiltroFechaDesde.DateValue) Then
'            'if
'            If IsDate(txtFiltroFechaHasta.DateValue) Then
'                If txtFiltroFechaDesde.DateValue > txtFiltroFechaHasta.DateValue Then
'                    MsgBox "La fecha desde debe ser menor o igual a la fecha hasta.", vbExclamation + vbOKOnly
'                    FiltrosValidos = False
'                    Exit Function
'                End If
'            Else
'                Call Filtrar
'                chkBuscar.Value = 0
'            End If
'        Else
'            If IsDate(txtFiltroFechaHasta.DateValue) Then
'
'            End If
'        End If
'    End If
End Function

Private Sub lstFiltroEstados_ItemCheck(item As Integer)
    Dim i As Integer
    sFiltroEstado = ""
    i = 0
    For i = 0 To lstFiltroEstados.ListCount - 1
        If lstFiltroEstados.Selected(i) Then
            sFiltroEstado = sFiltroEstado & Mid(lstFiltroEstados.List(i), 1, 1) & "|"
        End If
    Next i
End Sub

Private Sub Filtrar()
    Call CargarGrilla
End Sub

'{ add -005- a.
Private Sub chkBuscar_Click()
    With grdDatos
        If chkBuscar.Value = 1 Then
            lblBusqueda.Caption = ""
            lblBusqueda.Visible = True
            fraBusqueda.Enabled = True
            Call setHabilCtrl(cmdModificar, "DIS")
            Call setHabilCtrl(chkCampos, "DIS")
            Call setHabilCtrl(cmdConfirmar, "DIS")
            Call setHabilCtrl(cmdImprimir, "DIS")
            Call setHabilCtrl(cmdCopiar, "DIS")
            Call setHabilCtrl(cmdSolicitudes, "DIS")
            Call setHabilCtrl(cmdExportar, "DIS")
            .Height = 3600
            .Top = 1200
            Call setHabilCtrl(txtBuscarDSNNombre, "NOR")
            If Me.Visible Then txtBuscarDSNNombre.SetFocus
        Else
            lblBusqueda.Visible = False
            fraBusqueda.Enabled = False
            .Height = 4680
            .Top = 120
            Call setHabilCtrl(chkCampos, "NOR")
            Call setHabilCtrl(cmdImprimir, "NOR")
            Call setHabilCtrl(cmdCopiar, "NOR")
            Call setHabilCtrl(cmdSolicitudes, "NOR")
            Call setHabilCtrl(cmdExportar, "NOR")
            Call setHabilCtrl(txtBuscarDSNNombre, "DIS")
        End If
    End With
    'Call MostrarSeleccion
End Sub
'}

Private Sub cmdCopiar_Click()
    Dim cBuffer As String
    Dim i As Long
    
    Clipboard.Clear
    If grdDatos.Rows > 1 Then
        For i = nRwD To nRwH
            cBuffer = cBuffer & grdDatos.TextMatrix(i, COL_DSN_NOMPROD) & IIf(nRwD = nRwH, "", vbCrLf)
        Next i
    End If
    Clipboard.SetText Trim(cBuffer), vbCFText
End Sub

'Private Sub cmdImprimir_Click()
'    Dim cPathFileNameReport As String
'
'    Call Puntero(True)
'    Call Status("Preparando el informe...")
'
'    With mdiPrincipal.CrystalReport1
'        cPathFileNameReport = App.Path & "\" & "rptdsn2.rpt"
'        .ReportFileName = GetShortName(cPathFileNameReport)
'        .RetrieveStoredProcParams
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .Formulas(0) = "@0_0EMPRESA='" & Trim(glHEADEMPPRESA) & "'"
'        .Formulas(1) = "@0_1APLICACION='" & Trim(glHEADAPLICACION) & "'"
'        .Formulas(2) = "@0_TITULO='" & "DSN ya utilizados" & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        Screen.MousePointer = vbNormal
'        Call Status("Listo.")
'    End With
'End Sub

'{ add -017- a.
Private Sub grdDatos_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        MenuGridFind grdDatos
    End If
End Sub
'}

Private Sub cmdSolicitudes_Click()
    With grdDatos
        frmCLISTSoli.cDSN_Nom = ClearNull(.TextMatrix(.RowSel, COL_DSN_NOM))
        frmCLISTSoli.Show 1
    End With
End Sub

Private Sub cmdExportar_Click()
    On Error GoTo ErrHandler
    Dim cNombreArchivo As String

    cNombreArchivo = Format(Now, "yyyymmdd_hhmm_") & glLOGIN_ID_REEMPLAZO & ".xls"
    With mdiPrincipal.CommonDialog
        .DialogTitle = "Archivo de exportaci�n"
        .CancelError = True
        .Filter = "Microsoft Excel (*.xls)|*.xls"
        .FilterIndex = 1
        .InitDir = glWRKDIR
        .Flags = cdlOFNCreatePrompt + cdlOFNHideReadOnly + cdlOFNLongNames + cdlOFNOverwritePrompt
        .ShowSave
        cNombreArchivo = .FileName
    End With
    
    If Len(cNombreArchivo) > 0 Then
        mdiPrincipal.sbPrincipal.Tag = mdiPrincipal.sbPrincipal.Panels(1).Text
        Call Status("Preparando exportaci�n... aguarde...")
        Call Exportar_Excel(cNombreArchivo, grdDatos)
    Else
        MsgBox "Debe especificar un nombre de archivo v�lido", vbExclamation + vbOKOnly, "Error en el nombre del archivo"
        mdiPrincipal.sbPrincipal.Panels(1).Text = mdiPrincipal.sbPrincipal.Tag
    End If
    Exit Sub
ErrHandler:
    Exit Sub        ' El usuario ha hecho click en el bot�n Cancelar
End Sub

Private Sub Exportar_Excel(sNombreArchivo As String, ByVal FlexGrid As MSFlexGrid)
    On Error GoTo Error_Handler
    Dim o_Excel As Object, o_Libro As Object, o_Hoja As Object
    Dim Fila As Long, Fila2 As Long, Columna As Long
    Dim Limite As Long
    Dim xlsCol As Long
    Dim dNumero As Double
    Dim i As Integer
    
    Call Puntero(True)
    Call Status("Generando planilla... aguarde...")
    Set o_Excel = CreateObject("Excel.Application")     ' Crea el objeto Excel, el objeto workBook y dos objeto sheet
    
    Set o_Libro = o_Excel.Workbooks.Add
    If o_Libro.Sheets.Count > 1 Then
        For i = 1 To o_Libro.Sheets.Count - 1
            o_Libro.WorkSheets(i).Delete
        Next i
    End If
    
    o_Libro.Sheets.Add
    o_Libro.Sheets(1).name = "HEDT001"
    o_Libro.Sheets(2).name = "HEDT003"

    Set o_Hoja = o_Libro.Sheets(1)
    o_Hoja.Rows(1).Font.Bold = True
    With FlexGrid
        For Fila = 0 To .Rows - 1
            Call Status("Generando planilla... " & Fila & " de " & .Rows - 1)
            o_Hoja.Cells(Fila + 1, 1).Value = .TextMatrix(Fila, COL_DSN_ID)
            o_Hoja.Cells(Fila + 1, 2).Value = .TextMatrix(Fila, COL_DSN_NOM)
            o_Hoja.Cells(Fila + 1, 3).Value = .TextMatrix(Fila, COL_DSN_ENMAS)
            o_Hoja.Cells(Fila + 1, 4).Value = .TextMatrix(Fila, COL_DSN_NOMRUT)
            o_Hoja.Cells(Fila + 1, 5).Value = .TextMatrix(Fila, COL_DSN_FEULT)
            o_Hoja.Cells(Fila + 1, 6).Value = .TextMatrix(Fila, COL_NOM_USUARIO)
            o_Hoja.Cells(Fila + 1, 7).Value = .TextMatrix(Fila, COL_DSN_VB)
            o_Hoja.Cells(Fila + 1, 8).Value = .TextMatrix(Fila, COL_NOM_VB_USUARIO)
            o_Hoja.Cells(Fila + 1, 9).Value = .TextMatrix(Fila, COL_DSN_VB_FE)
            o_Hoja.Cells(Fila + 1, 10).Value = .TextMatrix(Fila, COL_APP_ID)
            o_Hoja.Cells(Fila + 1, 11).Value = .TextMatrix(Fila, COL_APP_NAME)
            o_Hoja.Cells(Fila + 1, 12).Value = .TextMatrix(Fila, COL_DSN_DESC)
            o_Hoja.Cells(Fila + 1, 13).Value = .TextMatrix(Fila, COL_ESTADO)
            o_Hoja.Cells(Fila + 1, 14).Value = .TextMatrix(Fila, COL_VALIDO)
            o_Hoja.Cells(Fila + 1, 15).Value = .TextMatrix(Fila, COL_DSN_NOMPROD)
            o_Hoja.Cells(Fila + 1, 16).Value = .TextMatrix(Fila, COL_DSN_SOLI)
            o_Hoja.Cells(Fila + 1, 17).Value = .TextMatrix(Fila, COL_DSN_CPYBBL)
            o_Hoja.Cells(Fila + 1, 18).Value = .TextMatrix(Fila, COL_DSN_CPYNOM)
        Next Fila
    End With
    With o_Hoja                             ' Formato final (las columnas comienzan a numerarse de 1 para la propiedad Columns)
        .Columns(1).ColumnWidth = 10        ' C�digo
        .Columns(2).ColumnWidth = 48        ' Nombre DSN
        .Columns(3).ColumnWidth = 8.5       ' Datos Sensibles
        .Columns(4).ColumnWidth = 7         ' Rutina
        .Columns(5).ColumnWidth = 17        ' �ltima modificaci�n
        .Columns(6).ColumnWidth = 25        ' Propietario
        .Columns(7).ColumnWidth = 11        ' Estado
        .Columns(8).ColumnWidth = 19        ' Por
        .Columns(9).ColumnWidth = 17        ' Fch. visado
        .Columns(10).ColumnWidth = 10       ' Aplicativo
        .Columns(11).ColumnWidth = 35       ' Nombre de aplicativo
        .Columns(12).ColumnWidth = 50       ' Descripci�n del DSN
        .Columns(13).ColumnWidth = 0
        .Columns(14).ColumnWidth = 8        ' Validez
        .Columns(15).ColumnWidth = 48       ' Nombre productivo
        .Columns(16).ColumnWidth = 6.5      ' Sol. #
        .Columns(17).ColumnWidth = 20       ' Biblioteca de COPY
        .Columns(18).ColumnWidth = 20       ' Nombre de COPY
    End With
    
    Set o_Hoja = o_Libro.Sheets(2)
    Fila2 = 1
    o_Hoja.Rows(Fila2).Font.Bold = True
    With FlexGrid
        o_Hoja.Cells(1, 1).Value = "C�digo"
        o_Hoja.Cells(1, 2).Value = "Nombre del archivo"
        o_Hoja.Cells(1, 3).Value = "Campo"
        o_Hoja.Cells(1, 4).Value = "Descripci�n"
        o_Hoja.Cells(1, 5).Value = "Posici�n"
        o_Hoja.Cells(1, 6).Value = "Longitud"
        o_Hoja.Cells(1, 7).Value = "Decimales"
        o_Hoja.Cells(1, 8).Value = "Tipo de dato"
        o_Hoja.Cells(1, 9).Value = "Tipo"
        o_Hoja.Cells(1, 10).Value = "Subtipo"
        o_Hoja.Cells(1, 11).Value = "Rutina"
        For Fila = 0 To .Rows - 1
            Call Status("Generando planilla... " & Fila & " de " & .Rows - 1)
            If sp_GetHEDT003a(.TextMatrix(Fila, COL_DSN_ID)) Then
                Do While Not aplRST.EOF
                    o_Hoja.Cells(Fila2 + 1, 1).Value = .TextMatrix(Fila, COL_DSN_ID)
                    o_Hoja.Cells(Fila2 + 1, 2).Value = .TextMatrix(Fila, COL_DSN_NOM)
                    o_Hoja.Cells(Fila2 + 1, 3).Value = ClearNull(aplRST.Fields!CPO_ID)
                    o_Hoja.Cells(Fila2 + 1, 4).Value = ClearNull(aplRST.Fields!CPO_DSC)
                    o_Hoja.Cells(Fila2 + 1, 5).Value = ClearNull(aplRST.Fields!cpo_nrobyte)
                    o_Hoja.Cells(Fila2 + 1, 6).Value = ClearNull(aplRST.Fields!cpo_canbyte)
                    o_Hoja.Cells(Fila2 + 1, 7).Value = ClearNull(aplRST.Fields!cpo_decimals)
                    o_Hoja.Cells(Fila2 + 1, 8).Value = ClearNull(aplRST.Fields!cpo_type)
                    o_Hoja.Cells(Fila2 + 1, 9).Value = ClearNull(aplRST.Fields!tipo)
                    o_Hoja.Cells(Fila2 + 1, 10).Value = ClearNull(aplRST.Fields!subtipo)
                    o_Hoja.Cells(Fila2 + 1, 11).Value = ClearNull(aplRST.Fields!cpo_nomrut)
                    Fila2 = Fila2 + 1
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Next Fila
    End With
    With o_Hoja                             ' Formato final (las columnas comienzan a numerarse de 1 para la propiedad Columns)
        .Columns(1).ColumnWidth = 10        ' C�digo
        .Columns(2).ColumnWidth = 48        ' Nombre DSN
        .Columns(3).ColumnWidth = 23
        .Columns(4).ColumnWidth = 20
        .Columns(5).ColumnWidth = 8
        .Columns(6).ColumnWidth = 8
        .Columns(7).ColumnWidth = 10
        .Columns(8).ColumnWidth = 15
        .Columns(9).ColumnWidth = 15
        .Columns(10).ColumnWidth = 23
        .Columns(11).ColumnWidth = 23
    End With
    'o_Libro.Save
    o_Libro.Close True, sNombreArchivo
    o_Excel.Quit
    'Call ReleaseObjects(o_Excel, o_Libro, o_Hoja)   ' Terminar instancias
    
    Call Puntero(False)
    Call Status("Listo.")
    MsgBox "Exportaci�n realizada en " & sNombreArchivo, vbInformation + vbOKOnly
    mdiPrincipal.sbPrincipal.Panels(1).Text = mdiPrincipal.sbPrincipal.Tag
Exit Sub
Error_Handler:          ' Cierra la hoja y el la aplicaci�n Excel
    If Not o_Libro Is Nothing Then: o_Libro.Close False
    If Not o_Excel Is Nothing Then: o_Excel.Quit
    'Call ReleaseObjects(o_Excel, o_Libro, o_Hoja)
    Call Puntero(False)
    Call Status("Listo.")
    If Err.Number <> 1004 Then MsgBox Err.DESCRIPTION, vbCritical
End Sub

Private Sub cmbDSNEstado_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case CodigoCombo(cmbDSNEstado, True)
        Case "S"
            txtMotivoRechazo = ""
        Case Else
            txtMotivoRechazo = grdDatos.TextMatrix(grdDatos.RowSel, COL_DSN_TXT)
    End Select
    Call LockProceso(False)
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Select Case sOpcionSeleccionada
        Case "A", "M"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            If glCatalogoENM <> "ABM" Then
                glCatalogoNombre = grdDatos.TextMatrix(grdDatos.RowSel, COL_DSN_NOM)
            End If
            Set auxRST = Nothing
            Call Status("Listo.")
            Unload Me
    End Select
    LockProceso (False)
End Sub
