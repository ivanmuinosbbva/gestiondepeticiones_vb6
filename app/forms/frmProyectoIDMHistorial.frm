VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmProyectoIDMHistorial 
   Caption         =   "Historial - Detalle de eventos del proyecto"
   ClientHeight    =   7515
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11070
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmProyectoIDMHistorial.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   11070
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1995
      Left            =   9720
      TabIndex        =   8
      Top             =   5460
      Width           =   1275
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   11
         Top             =   1380
         Width           =   1170
      End
      Begin VB.CommandButton cmdAddHist 
         Caption         =   "Agregar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   180
         Width           =   1170
      End
      Begin VB.CommandButton cmdDelHist 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   680
         Visible         =   0   'False
         Width           =   1170
      End
   End
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1995
      Left            =   0
      TabIndex        =   5
      Top             =   5460
      Width           =   9585
      Begin VB.TextBox txtMensaje 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1470
         Left            =   90
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   6
         Top             =   420
         Width           =   9375
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   45
         TabIndex        =   7
         Top             =   180
         Width           =   1140
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   11025
      Begin VB.Label Label2 
         Caption         =   "Proyecto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   4
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1300
         TabIndex        =   3
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Proyecto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1305
         TabIndex        =   2
         Top             =   240
         Width           =   9465
      End
      Begin VB.Label Label6 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   1
         Top             =   480
         Width           =   1200
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4590
      Left            =   30
      TabIndex        =   12
      Top             =   870
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   8096
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      FormatString    =   ""
   End
End
Attribute VB_Name = "frmProyectoIDMHistorial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colFECHA = 0
Private Const colEvento = 1
Private Const colESTADO = 2
Private Const colUsrAudit = 3
Private Const colUsrRplc = 5
Private Const colObser = 4
Private Const colFechaSort = 6
Private Const colNroInterno = 7
Private Const colProjId = 8
Private Const colProjSubId = 9
Private Const colProjSubSId = 10

Private Sub Form_Load()
    Dim auxNro As String
    
    Call Puntero(True)
    If sp_GetProyectoIDM(frmProyectosIDMEdit.l_ProjId, frmProyectosIDMEdit.l_ProjSubId, frmProyectosIDMEdit.l_ProjSubsId, Null, Null, Null, Null, Null) Then
        If Not aplRST.EOF Then
            lblTitulo = frmProyectosIDMEdit.l_ProjId & "." & frmProyectosIDMEdit.l_ProjSubId & "." & frmProyectosIDMEdit.l_ProjSubsId & " - " & ClearNull(aplRST!projnom)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
        End If
    End If
    If SoyElAnalista Then
        cmdDelHist.Visible = True: cmdDelHist.Enabled = True
    End If
    Call CargarGrid(DateAdd("d", -900, date), date)
    Call Status("Listo.")
    Call Puntero(False)
    Call IniciarScroll(grdDatos)
End Sub

Private Sub Form_Resize()
    ' Horizontal
    If Me.WindowState <> vbMinimized Then
        If Me.ScaleWidth > 1 Then
            If Me.ScaleWidth > 4080 Then
                grdDatos.Width = Me.ScaleWidth - 80
                Frame2.Width = Me.ScaleWidth - 80
                fraRpt.Width = Me.ScaleWidth - 1400
                txtMensaje.Width = fraRpt.Width - 200
                fraOpciones.Left = fraRpt.Width + 50
            End If
        End If
        ' Vertical
        If Me.ScaleHeight > 4515 Then
            grdDatos.Height = Me.ScaleHeight - 2900
            fraRpt.Top = Frame2.Height + grdDatos.Height + 20
            fraOpciones.Top = Frame2.Height + grdDatos.Height + 20
        End If
    End If
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .Visible = False
        .Clear
        '.HighLight = flexHighlightNever
        '.Font.name = "Tahoma"
        '.Font.Size = 8
        .Cols = 11
        .Rows = 1
        .TextMatrix(0, colFECHA) = "Fecha y hora": .ColWidth(colFECHA) = 1500: .ColAlignment(colFECHA) = 0
        .TextMatrix(0, colEvento) = "Evento": .ColWidth(colEvento) = 3000: .ColAlignment(colEvento) = 0
        .TextMatrix(0, colESTADO) = "Estado proyecto": .ColWidth(colESTADO) = 1500: .ColAlignment(colESTADO) = 0
        .TextMatrix(0, colUsrAudit) = "Usuario": .ColWidth(colUsrAudit) = 2400: .ColAlignment(colUsrAudit) = 0
        .TextMatrix(0, colUsrRplc) = "A nombre de": .ColWidth(colUsrRplc) = 2400: .ColAlignment(colUsrRplc) = 0
        .TextMatrix(0, colObser) = "Observaciones": .ColWidth(colObser) = 3000: .ColAlignment(colObser) = 0
        .TextMatrix(0, colNroInterno) = "N� interno": .ColWidth(colNroInterno) = 0
        .TextMatrix(0, colProjId) = "ProjId": .ColWidth(colProjId) = 0
        .TextMatrix(0, colProjSubId) = "ProjSubId": .ColWidth(colProjSubId) = 0
        .TextMatrix(0, colProjSubSId) = "ProjSubSId": .ColWidth(colProjSubSId) = 0
        .ColWidth(colFechaSort) = 0
        .HighLight = flexHighlightAlways                                                    ' add -010- a.
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
        .Visible = True
    End With
End Sub

Private Sub CargarGrid(fDes As Date, fHas As Date)
    Dim cObservaciones As String
    Dim lPos1 As Long
    Dim lPos2 As Long
    Dim bSystemProcess As Boolean
    
    Call InicializarGrilla
    If Not sp_GetProyectoIDMHisto(frmProyectosIDMEdit.l_ProjId, frmProyectosIDMEdit.l_ProjSubId, frmProyectosIDMEdit.l_ProjSubsId) Then
        grdDatos.Visible = True
        Exit Sub
    End If
    'Call InicializarGrilla
    Do While Not aplRST.EOF
        With grdDatos
            bSystemProcess = False
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colFECHA) = IIf(Not IsNull(aplRST!hst_fecha), Format(aplRST!hst_fecha, "dd/mm/yyyy   hh:mm"), "")    ' upd -004- a. Se cambia el formato para mostrar la hora del evento
            .TextMatrix(.Rows - 1, colEvento) = ClearNull(aplRST!nom_accion)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            If UCase(ClearNull(aplRST!audit_user)) = "JP00" Then
                .TextMatrix(.Rows - 1, colUsrAudit) = "- Proceso de sistema -"
                .TextMatrix(.Rows - 1, colUsrRplc) = ""
                bSystemProcess = True
            Else
                .TextMatrix(.Rows - 1, colUsrAudit) = ClearNull(aplRST!audit_user) & " : " & ClearNull(aplRST!nom_audit)
                .TextMatrix(.Rows - 1, colUsrRplc) = ClearNull(aplRST!replc_user) & " : " & ClearNull(aplRST!nom_replc)
            End If
            If bSystemProcess Then PintarLinea grdDatos, prmGridFillRowColorLightOrange
            .TextMatrix(.Rows - 1, colObser) = ClearNull(aplRST!mem_texto)
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!hst_nrointerno)
            .TextMatrix(.Rows - 1, colProjId) = ClearNull(aplRST!ProjId)
            .TextMatrix(.Rows - 1, colProjSubId) = ClearNull(aplRST!ProjSubId)
            .TextMatrix(.Rows - 1, colProjSubSId) = ClearNull(aplRST!ProjSubSId)
        End With
        aplRST.MoveNext
    Loop
    Call MostrarSeleccion
End Sub

Sub MostrarSeleccion()
    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                txtMensaje = Trim(.TextMatrix(.RowSel, colObser))
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
            If .MouseCol = colFECHA Then
                .Col = colNroInterno
                .Sort = flexSortStringNoCaseDescending
            Else
                .Col = .MouseCol
                .Sort = flexSortStringNoCaseAscending
            End If
        End If
    End With
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    If Not bFormateando Then
        If grdDatos.Col <> colFechaSort Then
            Call MostrarSeleccion
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub cmdDelHist_Click()
    Dim nRow As Integer
    Dim nRwD As Integer
    Dim nRwH As Integer
    
    With grdDatos
        If .Rows > 1 Then
            If .Row <= .RowSel Then
                nRwD = .Row
                nRwH = .RowSel
            Else
                nRwH = .Row
                nRwD = .RowSel
            End If
            If nRwD <> nRwH Then
                ' Eliminaci�n m�ltiple
                If MsgBox("�Confirma la eliminaci�n de estos registros seleccionados del historial?", vbQuestion + vbYesNo) = vbYes Then
                    Do While nRwD <= nRwH
                        'sp_DeleteHistorialPet glNumeroPeticion, grdDatos.TextMatrix(nRwD, colNroInterno)
                        Call sp_DeleteProyectoIDMHisto(.TextMatrix(nRwD, colProjId), .TextMatrix(nRwD, colProjSubId), .TextMatrix(nRwD, colProjSubSId), .TextMatrix(nRwD, colNroInterno))
                        nRwD = nRwD + 1
                        DoEvents
                    Loop
                    Call CargarGrid(DateAdd("d", -900, date), date)
                End If
                Exit Sub
            Else
                ' Eliminaci�n simple
                If MsgBox("�Confirma la eliminaci�n de este registro del historial?", vbQuestion + vbYesNo) = vbYes Then
                    'sp_DeleteHistorialPet glNumeroPeticion, grdDatos.TextMatrix(nRwD, colNroInterno)
                    Call sp_DeleteProyectoIDMHisto(.TextMatrix(nRwD, colProjId), .TextMatrix(nRwD, colProjSubId), .TextMatrix(nRwD, colProjSubSId), .TextMatrix(nRwD, colNroInterno))
                    Call CargarGrid(DateAdd("d", -900, date), date)
                End If
                Exit Sub
            End If
        Else
            MsgBox "No existen datos para eliminar.", vbExclamation + vbOKOnly
        End If
    End With
End Sub

Private Sub cmdOk_Click()
    Unload Me
End Sub
