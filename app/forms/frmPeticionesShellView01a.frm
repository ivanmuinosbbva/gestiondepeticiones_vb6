VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesShellView01a 
   Caption         =   "Form1"
   ClientHeight    =   8520
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12000
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8520
   ScaleWidth      =   12000
   Begin VB.Frame frmGral 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7900
      Left            =   120
      TabIndex        =   15
      Top             =   0
      Width           =   10485
      Begin VB.ComboBox cboEmp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7125
         Style           =   2  'Dropdown List
         TabIndex        =   146
         Top             =   2040
         Width           =   1500
      End
      Begin VB.ComboBox cboBpar 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   70
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   4440
         Width           =   8895
      End
      Begin VB.CommandButton cmdEraseProyectos 
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2640
         Picture         =   "frmPeticionesShellView01a.frx":0000
         TabIndex        =   65
         TabStop         =   0   'False
         ToolTipText     =   "Quitar el filtro actual de proyecto IDM"
         Top             =   2520
         Width           =   350
      End
      Begin VB.Frame frmSolic 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   215
         Left            =   1440
         TabIndex        =   54
         Top             =   3840
         Width           =   4590
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "BBVA"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   0
            TabIndex        =   58
            Top             =   0
            Width           =   765
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   840
            TabIndex        =   57
            Top             =   0
            Width           =   1035
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1920
            TabIndex        =   56
            Top             =   0
            Width           =   1000
         End
         Begin VB.OptionButton OptNivelPet 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   3000
            TabIndex        =   55
            Top             =   0
            Width           =   885
         End
      End
      Begin VB.ComboBox cboTipopet 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   39
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   1320
         Width           =   1695
      End
      Begin VB.ComboBox cboSituacionPet 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   38
         Top             =   3240
         Width           =   4620
      End
      Begin VB.ComboBox cboSituacionSec 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   6600
         Width           =   6975
      End
      Begin VB.ComboBox cboEstadoSec 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   36
         Top             =   6240
         Width           =   6975
      End
      Begin VB.Frame frmEjec 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   215
         Left            =   1440
         TabIndex        =   30
         Top             =   5580
         Width           =   4815
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "BBVA"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   0
            TabIndex        =   35
            Top             =   30
            Width           =   765
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Direcci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   840
            TabIndex        =   34
            Top             =   30
            Width           =   1125
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Gerencia"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1920
            TabIndex        =   33
            Top             =   30
            Width           =   975
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   3000
            TabIndex        =   32
            Top             =   30
            Width           =   885
         End
         Begin VB.OptionButton OptNivelSec 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   3840
            TabIndex        =   31
            Top             =   30
            Width           =   885
         End
      End
      Begin VB.CommandButton cmdClrAgrup 
         Height          =   315
         Left            =   5280
         Picture         =   "frmPeticionesShellView01a.frx":0532
         Style           =   1  'Graphical
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Limpia agrupamiento seleccionado"
         Top             =   2040
         Width           =   345
      End
      Begin VB.ComboBox cmbPetClass 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3840
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   1320
         Width           =   2205
      End
      Begin VB.ComboBox cmbPetImpTech 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmPeticionesShellView01a.frx":0874
         Left            =   7080
         List            =   "frmPeticionesShellView01a.frx":0884
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   1320
         Width           =   2445
      End
      Begin VB.ComboBox cmbRegulatorio 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmPeticionesShellView01a.frx":08B8
         Left            =   1440
         List            =   "frmPeticionesShellView01a.frx":08C8
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   1680
         Width           =   1695
      End
      Begin VB.CheckBox chkHistorico 
         Caption         =   "Incluir hist�ricas"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8640
         TabIndex        =   25
         Top             =   3600
         Width           =   1455
      End
      Begin VB.Frame fraApertura 
         Caption         =   " Apertura "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   8880
         TabIndex        =   21
         Top             =   6360
         Width           =   1455
         Begin VB.OptionButton optApertura 
            Caption         =   "Sin apertura"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   24
            Top             =   360
            Width           =   1215
         End
         Begin VB.OptionButton optApertura 
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   23
            Top             =   600
            Width           =   1215
         End
         Begin VB.OptionButton optApertura 
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   22
            Top             =   840
            Width           =   1215
         End
      End
      Begin VB.ComboBox cboPrioridad 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3840
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1680
         Width           =   2205
      End
      Begin VB.ComboBox cboImportancia 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7080
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1680
         Width           =   2445
      End
      Begin VB.ComboBox cboEstadoPet 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   2880
         Width           =   4620
      End
      Begin VB.CommandButton cmdAgrup 
         Height          =   315
         Left            =   4920
         Picture         =   "frmPeticionesShellView01a.frx":08EC
         Style           =   1  'Graphical
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar agrupamiento para filtrar las peticiones"
         Top             =   2040
         Width           =   345
      End
      Begin VB.CommandButton cmdPrjFilter 
         Height          =   315
         Left            =   3000
         Picture         =   "frmPeticionesShellView01a.frx":0C2E
         Style           =   1  'Graphical
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto IDM"
         Top             =   2520
         Width           =   300
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   1440
         TabIndex        =   40
         Top             =   960
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin AT_MaskText.MaskText txtNroDesde 
         Height          =   315
         Left            =   3120
         TabIndex        =   41
         Top             =   600
         Width           =   945
         _ExtentX        =   1667
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtNroHasta 
         Height          =   315
         Left            =   1440
         TabIndex        =   42
         Top             =   600
         Width           =   945
         _ExtentX        =   1667
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtDESDEiniplan 
         Height          =   315
         Left            =   1440
         TabIndex        =   43
         Top             =   6960
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEfinplan 
         Height          =   315
         Left            =   1440
         TabIndex        =   44
         Top             =   7320
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAiniplan 
         Height          =   315
         Left            =   3120
         TabIndex        =   45
         Top             =   6960
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAfinplan 
         Height          =   315
         Left            =   3120
         TabIndex        =   46
         Top             =   7320
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEestado 
         Height          =   315
         Left            =   6720
         TabIndex        =   47
         Top             =   2880
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAestado 
         Height          =   315
         Left            =   8835
         TabIndex        =   48
         Top             =   2880
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEinireal 
         Height          =   315
         Left            =   5640
         TabIndex        =   49
         Top             =   6960
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAinireal 
         Height          =   315
         Left            =   7320
         TabIndex        =   50
         Top             =   6960
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEfinreal 
         Height          =   315
         Left            =   5640
         TabIndex        =   51
         Top             =   7320
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAfinreal 
         Height          =   315
         Left            =   7320
         TabIndex        =   52
         Top             =   7320
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAgrup 
         Height          =   315
         Left            =   1440
         TabIndex        =   53
         Top             =   2040
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   315
         Left            =   2280
         TabIndex        =   59
         Top             =   2520
         Width           =   345
         _ExtentX        =   609
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   315
         Left            =   1920
         TabIndex        =   60
         Top             =   2520
         Width           =   345
         _ExtentX        =   609
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   315
         Left            =   1440
         TabIndex        =   61
         Top             =   2520
         Width           =   480
         _ExtentX        =   847
         _ExtentY        =   556
         MaxLength       =   11
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   11
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjNom 
         Height          =   315
         Left            =   3960
         TabIndex        =   62
         Top             =   2520
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   556
         MaxLength       =   50
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDESDEalta 
         Height          =   315
         Left            =   1440
         TabIndex        =   63
         Top             =   4800
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTAalta 
         Height          =   315
         Left            =   3720
         TabIndex        =   64
         Top             =   4800
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.ComboBox cboBBVAPet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   69
         Top             =   4080
         Width           =   8895
      End
      Begin VB.ComboBox cboSectorPet 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   68
         Top             =   4080
         Width           =   8895
      End
      Begin VB.ComboBox cboGerenciaPet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   67
         Top             =   4080
         Width           =   8895
      End
      Begin VB.ComboBox cboDireccionPet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   66
         Top             =   4080
         Width           =   8895
      End
      Begin VB.ComboBox cboBBVASec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   75
         Top             =   5880
         Width           =   8895
      End
      Begin VB.ComboBox cboSectorSec 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   74
         Top             =   5880
         Width           =   8895
      End
      Begin VB.ComboBox cboGerenciaSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   73
         Top             =   5880
         Width           =   8895
      End
      Begin VB.ComboBox cboDireccionSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   72
         Top             =   5880
         Width           =   8895
      End
      Begin VB.ComboBox cboGrupoSec 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   71
         Top             =   5880
         Width           =   8895
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "Empresa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6360
         TabIndex        =   145
         Top             =   2115
         Width           =   615
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Filtro petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   112
         Top             =   270
         Width           =   1155
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo (o parte)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   111
         Top             =   900
         Width           =   1080
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   110
         Top             =   1395
         Width           =   300
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n Pet."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   109
         Top             =   3240
         Width           =   990
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Filtro �rea solicitante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   108
         Top             =   3540
         Width           =   1800
      End
      Begin VB.Label LblDatosRec 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   107
         Top             =   3795
         Width           =   345
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   106
         Top             =   4155
         Width           =   345
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Filtro �rea ejecutora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   105
         Top             =   5310
         Width           =   1740
      End
      Begin VB.Label Lbl7 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   104
         Top             =   5595
         Width           =   345
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Situac. Sec/Gru"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   103
         Top             =   6660
         Width           =   1110
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Estado Sec/Gru"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   102
         Top             =   6300
         Width           =   1110
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   101
         Top             =   5940
         Width           =   345
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2640
         TabIndex        =   100
         Top             =   7350
         Width           =   420
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2640
         TabIndex        =   99
         Top             =   7005
         Width           =   420
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin Plan.Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   98
         Top             =   7350
         Width           =   1215
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini.Plan.Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   97
         Top             =   7005
         Width           =   1200
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   8280
         TabIndex        =   96
         Top             =   2880
         Width           =   420
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6120
         TabIndex        =   95
         Top             =   2910
         Width           =   450
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6840
         TabIndex        =   94
         Top             =   7005
         Width           =   420
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6840
         TabIndex        =   93
         Top             =   7350
         Width           =   420
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini.Real Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   92
         Top             =   7005
         Width           =   1200
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin.Real Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   91
         Top             =   7350
         Width           =   1230
      End
      Begin VB.Label Label41 
         AutoSize        =   -1  'True
         Caption         =   "B. Partner"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   90
         Top             =   4515
         Width           =   735
      End
      Begin VB.Label lblPetClass 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3360
         TabIndex        =   89
         Top             =   1365
         Width           =   390
      End
      Begin VB.Label lblPetImpTech 
         AutoSize        =   -1  'True
         Caption         =   "Imp. tec."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6240
         TabIndex        =   88
         Top             =   1365
         Width           =   660
      End
      Begin VB.Label lblAgrup 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Agrupamientos "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   87
         Top             =   2040
         Width           =   1125
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2520
         TabIndex        =   86
         Top             =   600
         Width           =   420
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "N�mero desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   85
         Top             =   570
         Width           =   1035
      End
      Begin VB.Label Label28 
         AutoSize        =   -1  'True
         Caption         =   "Regulatorio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   84
         Top             =   1755
         Width           =   825
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Prior."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3360
         TabIndex        =   83
         Top             =   1725
         Width           =   390
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6240
         TabIndex        =   82
         Top             =   1680
         Width           =   675
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Proyecto IDM"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   81
         Top             =   2580
         Width           =   975
      End
      Begin VB.Label Label47 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3480
         TabIndex        =   80
         Top             =   2580
         Width           =   390
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Estado Pet."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   79
         Top             =   3000
         Width           =   840
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "F.Env.BP Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   78
         Top             =   4860
         Width           =   1155
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3120
         TabIndex        =   77
         Top             =   4860
         Width           =   420
      End
      Begin VB.Label lblMasFiltros1 
         AutoSize        =   -1  'True
         Caption         =   " 1 / 2 "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   180
         Left            =   9840
         TabIndex        =   76
         Top             =   0
         Width           =   450
      End
   End
   Begin VB.Frame pnlBotones 
      Height          =   7900
      Left            =   10620
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "Acceder"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Ver la Petici�n"
         Top             =   5040
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Consultar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Realizar la consulta"
         Top             =   5040
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   7320
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgrupar 
         Caption         =   "Agrupar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Adjunta las peticiones seleccionadas en un agrupamiento"
         Top             =   3420
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Exportar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Genera un archivo Excel con la salida de la consulta"
         Top             =   2100
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdPrintPet 
         Caption         =   "Imprimir formulario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Imporime un formulario por cada Peticion seleccionada."
         Top             =   3930
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdChgBP 
         Caption         =   "Reasignar Business P."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Reasigna el Business Partner a las peticiones seleccionadas."
         Top             =   2910
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.Frame frmCrtPrn 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   60
         TabIndex        =   5
         Top             =   1200
         Visible         =   0   'False
         Width           =   1150
         Begin VB.OptionButton CrtPrn 
            Caption         =   "SCR"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   7
            Top             =   240
            Value           =   -1  'True
            Width           =   660
         End
         Begin VB.OptionButton CrtPrn 
            Caption         =   "PRN"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   6
            Top             =   480
            Width           =   690
         End
      End
      Begin VB.CommandButton cmdMasFiltros 
         Caption         =   "M�s filtros..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   160
         Width           =   1170
      End
      Begin VB.CommandButton cmdAHistorico 
         Caption         =   "A Hist�rico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Realizar la consulta"
         Top             =   5520
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdDeHistorico 
         Caption         =   "De Hist�rico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Realizar la consulta"
         Top             =   6000
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton cmdGuardar 
         Caption         =   "Guardar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Guarda la parametrizaci�n de la consulta actual"
         Top             =   640
         Visible         =   0   'False
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   7065
      Left            =   120
      TabIndex        =   113
      Top             =   810
      Width           =   10395
      _ExtentX        =   18336
      _ExtentY        =   12462
      _Version        =   393216
      Cols            =   35
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraOtrosFiltros 
      Height          =   7905
      Left            =   120
      TabIndex        =   116
      Top             =   0
      Width           =   10485
      Begin VB.ComboBox cboSupervisor 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   125
         ToolTipText     =   "Recurso que actu� como Supervisor"
         Top             =   1680
         Width           =   5520
      End
      Begin VB.ComboBox cboDirector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   124
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   2040
         Width           =   5520
      End
      Begin VB.ComboBox cboSolicitante 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   123
         ToolTipText     =   "Recurso que actu� como Solicitante"
         Top             =   960
         Width           =   5520
      End
      Begin VB.ComboBox cboReferente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   122
         ToolTipText     =   "Recurso que actu� como Referente"
         Top             =   1320
         Width           =   5520
      End
      Begin VB.ComboBox cboUsuario 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   121
         ToolTipText     =   "El usuario real que di� de alta la petici�n"
         Top             =   600
         Width           =   5520
      End
      Begin VB.ComboBox cmbTextos 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   120
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   3000
         Width           =   5520
      End
      Begin VB.ComboBox cmbOpcionesTexto 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesShellView01a.frx":1160
         Left            =   2760
         List            =   "frmPeticionesShellView01a.frx":1162
         Style           =   2  'Dropdown List
         TabIndex        =   119
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   3360
         Width           =   5520
      End
      Begin VB.ComboBox cmbPetDocu 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesShellView01a.frx":1164
         Left            =   2760
         List            =   "frmPeticionesShellView01a.frx":1171
         Style           =   2  'Dropdown List
         TabIndex        =   118
         Top             =   5040
         Width           =   2085
      End
      Begin VB.ComboBox cmbDocumentoAdjunto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2760
         Style           =   2  'Dropdown List
         TabIndex        =   117
         Top             =   5400
         Width           =   5520
      End
      Begin AT_MaskText.MaskText txtMEM_Texto 
         Height          =   315
         Left            =   2760
         TabIndex        =   126
         Top             =   3720
         Width           =   7440
         _ExtentX        =   13123
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         AutoSelect      =   0   'False
      End
      Begin AT_MaskText.MaskText txtFechaPedidoDesde 
         Height          =   315
         Left            =   2760
         TabIndex        =   127
         ToolTipText     =   "Fecha que se carg� la petici�n en el sistema"
         Top             =   4673
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         EmptyValue      =   0   'False
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaPedidoHasta 
         Height          =   315
         Left            =   5040
         TabIndex        =   128
         ToolTipText     =   "Fecha que se carg� la petici�n en el sistema"
         Top             =   4673
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         DecimalPlaces   =   0
         EmptyValue      =   0   'False
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de alta real"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   144
         Top             =   4740
         Width           =   1335
      End
      Begin VB.Label Label33 
         AutoSize        =   -1  'True
         Caption         =   "Usuario que realiz� el alta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   143
         Top             =   600
         Width           =   1950
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         Caption         =   "Solicitante"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   142
         Top             =   960
         Width           =   810
      End
      Begin VB.Label Label35 
         AutoSize        =   -1  'True
         Caption         =   "Supervisor"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   141
         Top             =   1680
         Width           =   810
      End
      Begin VB.Label Label36 
         AutoSize        =   -1  'True
         Caption         =   "Referente"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   140
         Top             =   1320
         Width           =   720
      End
      Begin VB.Label Label37 
         AutoSize        =   -1  'True
         Caption         =   "Autorizante"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   139
         Top             =   2040
         Width           =   870
      End
      Begin VB.Label Label32 
         AutoSize        =   -1  'True
         BackColor       =   &H00808080&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo de texto de la petici�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   120
         TabIndex        =   138
         Top             =   3000
         Width           =   2040
      End
      Begin VB.Label Label38 
         AutoSize        =   -1  'True
         Caption         =   "Texto a buscar en el detalle"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   137
         Top             =   3720
         Width           =   2085
      End
      Begin VB.Line Line4 
         BorderWidth     =   2
         X1              =   4800
         X2              =   10290
         Y1              =   330
         Y2              =   330
      End
      Begin VB.Label Label44 
         AutoSize        =   -1  'True
         Caption         =   "Filtro por recursos involucrados en peticiones "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   136
         Top             =   240
         Width           =   4575
      End
      Begin VB.Line Line5 
         BorderWidth     =   2
         X1              =   3480
         X2              =   10290
         Y1              =   2610
         Y2              =   2610
      End
      Begin VB.Label Label45 
         AutoSize        =   -1  'True
         Caption         =   "Filtro por textos y descripciones"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   135
         Top             =   2520
         Width           =   3180
      End
      Begin VB.Label Label39 
         AutoSize        =   -1  'True
         BackColor       =   &H00808080&
         BackStyle       =   0  'Transparent
         Caption         =   "Criterio a utilizar en la b�squeda"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   120
         TabIndex        =   134
         Top             =   3360
         Width           =   2430
      End
      Begin VB.Label Label40 
         AutoSize        =   -1  'True
         Caption         =   "Otros filtros"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   133
         Top             =   4320
         Width           =   1170
      End
      Begin VB.Line Line6 
         BorderWidth     =   2
         X1              =   1440
         X2              =   10290
         Y1              =   4410
         Y2              =   4410
      End
      Begin VB.Label lblPetDocu 
         AutoSize        =   -1  'True
         Caption         =   "Con documentos adjuntos"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   132
         Top             =   5115
         Width           =   1950
      End
      Begin VB.Label Label46 
         AutoSize        =   -1  'True
         Caption         =   "�Qu� documento buscar?"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   131
         Top             =   5475
         Width           =   1920
      End
      Begin VB.Label Label43 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   4440
         TabIndex        =   130
         Top             =   4740
         Width           =   450
      End
      Begin VB.Label lblMasFiltros2 
         AutoSize        =   -1  'True
         Caption         =   " 2 / 2 "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   180
         Left            =   9840
         TabIndex        =   129
         Top             =   0
         Width           =   450
      End
   End
   Begin VB.Frame frmFiltro 
      Height          =   795
      Left            =   120
      TabIndex        =   114
      Top             =   0
      Width           =   10485
      Begin VB.Label lblOpcionesApertura 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Label47"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   180
         Left            =   9795
         TabIndex        =   115
         Top             =   480
         Width           =   540
      End
   End
End
Attribute VB_Name = "frmPeticionesShellView01a"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 04.07.2007 - Se inicializan los nuevos controles combobox para soportar en la consulta la inclusi�n de filtros (impacto tecnol�gico, clase de la petici�n y documentos adjuntos)
' -001- b. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 18.09.2007 - Se agrega una columna m�s a la grilla para indicar si la petici�n pertenece al esquema nuevo o el anterior (tener en cuenta la propiedad Cols del control MSFlexGrid).
'                           Se agrega adem�s en el combo de clases el de SIN clase (util para realizar consultas).
' -003- b. FJS 18.09.2007 - Se cambia el orden de las columnas para tener la clase e indicador de IT luego del identificador de tipo de la petici�n.
' -004- a. FJS 08.10.2007 - Se agrega un manejador de error porque cuando se ejecuta la carga de la grilla de un conjunto de resultados muy grande, genera un error de runtime. Se agrega c�digo para manejar lo
'                           m�s elegantemente este error.
' -004- b. FJS 08.10.2007 - Se maneja de mejor manera la automatizaci�n de los controles de la parte de especificaci�n de par�metros de consultas.
' -005- a. FJS 21.11.2007 - Se agrega, para el perfil de Superadministrador la posibilidad de realizar una exportaci�n a Excel desde la pantalla inicial de consultas, sin necesidad de pasar por el resultado
'                           de la consulta (que trae aparejado el problema de capacidad (Overflow)).
' -006- a. FJS 18.03.2008 - Se agrega una mejora para el sector de Susana Dopazo.
' -007- a. FJS 21.07.2008 - Se contempla el argumento de devolver peticiones con documentos adjuntos o no.
' -008- a. FJS 10.09.2008 - Se agrega la columna para indicar si es regulatorio.
' -009- a. FJS 17.12.2008 - Se agregan opciones de filtro para marcar registros.
' -010- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -011- a. FJS 13.02.2009 - Se agrega un contador para indicar la cantidad de peticiones.
' -012- a. FJS 13.02.2009 - Nueva consulta para devolver los registros hist�ricos.
' -012- b. FJS 18.03.2009 - Manejo de peticiones hist�ricas.
' -013- a. FJS 17.03.2009 - Se agrega un nuevo filtro para especificar un documento en particular.
' -014- a. FJS 27.03.2009 - Se modifican estos controles porque no son los apropiadoa. Se cambian de CheckBox a OptionButtons.
' -014- b. FJS 27.03.2009 - Se agrega una leyenda para indicar el grado de apertura de la consulta.
' -015- a. FJS 31.03.2009 - Se agregan nuevas opciones de filtro para devolver peticiones.
' -015- b. FJS 03.06.2009 - Se quita la parte de proyectos (no proyectos IDM).
' -015- c. FJS 01.07.2009 - Se agrega el c�digo de legajo.
' -016- a. FJS 13.04.2010 - Se agrega un item para contemplar los estados terminales (Barbarito).
' -017- a. FJS 07.09.2010 - Planificaci�n DYD.

Option Explicit

'{ add -temp-
Private Type udtPet
    pet_nrointerno As Long
    pet_cod_estado As String
    pet_cod_sector As String
    pet_cod_grupo As String
    grp_cod_estado As String
End Type

Private Type udtRec
    cod_recurso As String
    cod_grupo As String
    cod_sector As String
    cod_gere As String
    cod_dire As String
    pet_nrointerno As Long
End Type
'}

Dim flgQuery As String              ' add -015- a.

Private Const DISTANCIA As Integer = 250    ' add -015- b.

Dim flgEnCarga As Boolean
Dim petTipo As String
Dim petPrioridad As String
Dim petNivel As String
Dim petArea As String
Dim petAreaTxt As String
Dim petEstado As String
Dim petSituacion As String
Dim petTitulo As String
Dim petImportancia As String
Dim secNivel As String
Dim secArea As String
Public secDire As String
Public secGere As String
Public secSect As String
Public secGrup As String
Dim secAreaTxt As String
Dim secEstado As String
Dim secSituacion As String
Dim auxFiltro As String
Dim flgNoHijos As Boolean

'para el filtro de proyectos
Public prj_tipo As String
Public prj_titulo As String
Public prj_Orientacion As String
Public prj_bpar As String
Public prj_Importancia As String
Public prj_estado As String
Public prj_nivel As String
Public prj_area As String
Public prj_NroProyecto As String
Public prj_DESDEiniplan As Variant
Public prj_DESDEfinplan As Variant
Public prj_HASTAiniplan As Variant
Public prj_HASTAfinplan As Variant
''Dim prj_DESDEestado As Variant
''Dim prj_HASTAestado As Variant
Public prj_DESDEinireal As Variant
Public prj_HASTAinireal As Variant
Public prj_DESDEfinreal As Variant
Public prj_HASTAfinreal As Variant

'{ add -003- b.
Const colMultiSelect = 0
Const colNroAsignado = 1
Const colTipoPet = 2
Const colPetClass = 3
Const colPetImpTech = 4
Const colRegulatorio = 5    ' add -008- a.
Const colTitulo = 6
Const colPrioridad = 7
Const colESTADO = 8
Const colSituacion = 9
Const colSecSol = 10
Const colEsfuerzo = 11
Const colFinicio = 12
Const colFtermin = 13
Const colImportancia = 14
Const colCorpLocal = 15
Const colOrientacion = 16
Const colSecNomArea = 17
Const colEstHijo = 18
Const colSitHijo = 19
Const colPrioEjecuc = 20
Const colSecEsfuerzo = 21
Const colSecFinicio = 22
Const colSecFtermin = 23
Const colSecCodDir = 24
Const colSecCodGer = 25
Const colSecCodSec = 26
Const colSecCodSub = 27
Const colNroInterno = 28
Const colSRTPETIPLAN = 29
Const colSRTPETFPLAN = 30
Const colSRTSECIPLAN = 31
Const colSRTSECFPLAN = 32
Const colEstadoFec = 33
Const colPetSOx = 34
'Const colPetDocu = 35  ' del -012- b.
Const colPetHist = 35   ' add -012- b.
'}
'{ del -003- b.
'Const colMultiSelect = 0
'Const colNroAsignado = 1
'Const colTipoPet = 2
'Const colTitulo = 3
'Const colPrioridad = 4
'Const colEstado = 5
'Const colSituacion = 6
'Const colSecSol = 7
'Const colEsfuerzo = 8
'Const colFinicio = 9
'Const colFtermin = 10
'Const colImportancia = 11
'Const colCorpLocal = 12
'Const colOrientacion = 13
'Const colSecNomArea = 14
'Const colEstHijo = 15
'Const colSitHijo = 16
'Const colPrioEjecuc = 17
'Const colSecEsfuerzo = 18
'Const colSecFinicio = 19
'Const colSecFtermin = 20
'Const colSecCodDir = 21
'Const colSecCodGer = 22
'Const colSecCodSec = 23
'Const colSecCodSub = 24
'Const colNroInterno = 25
'Const colSRTPETIPLAN = 26
'Const colSRTPETFPLAN = 27
'Const colSRTSECIPLAN = 28
'Const colSRTSECFPLAN = 29
'Const colEstadoFec = 30
''{ add -001- a.
'Const colPetClass = 31
'Const colPetImpTech = 32
'Const colPetSOx = 33        ' add -003- a.
'Const colPetDocu = 34
''}
'}

Private Sub Form_Load()
    Me.Tag = Me.Caption                     ' add -004- b.
    lblAgrup.Visible = True
    txtAgrup.Visible = True
    cmdAgrup.Visible = True
    cmdClrAgrup.Visible = True
    flgQuery = "QRY1"                       ' add -015- a.
    initQuery
    Call InicializarCombos
    OptNivelPet(0).Value = True
    OptNivelSec(0).Value = True
    Call FormCenter(Me, mdiPrincipal, False)
    modUser32.IniciarScroll grdDatos        ' add -010- a.
End Sub

'{ add -015- a.
Private Sub cmbTextos_Click()
    If cmbTextos.ListIndex = 0 Then
        If cmbOpcionesTexto.ListCount > 0 Then
            cmbOpcionesTexto.ListIndex = 0
            txtMEM_Texto = ""
            Call setHabilCtrl(cmbOpcionesTexto, "DIS")
            Call setHabilCtrl(txtMEM_Texto, "DIS")
        End If
    Else
        Call setHabilCtrl(cmbOpcionesTexto, "NOR")
        Call setHabilCtrl(txtMEM_Texto, "NOR")
    End If
End Sub
'}

Sub InicializarCombos()
    Dim i As Integer
    Dim auxCodPet, auxCodSec As String
    Dim xDirePet, xGerePet, xSectPet, xGrupPet As String
    Dim xDireSec, xGereSec, xSectSec, xGrupSec As String
    flgEnCarga = True
    
    Call Puntero(True)
    lblOpcionesApertura.Caption = ""    ' add -014- b.
    cboImportancia.Clear
    If sp_GetImportancia(Null, Null) Then
        Do While Not aplRST.EOF
            cboImportancia.AddItem Trim(aplRST!nom_importancia) & Space(40) & "||" & Trim(aplRST!cod_importancia)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboImportancia.AddItem "Todas                             ||NULL", 0
    cboImportancia.ListIndex = 0
    
    With cboPrioridad
        .Clear
        .AddItem "Todas" & Space(100) & "||NULL"
        .ListIndex = 0
    End With
    Call setHabilCtrl(cboPrioridad, "DIS")
    cboTipopet.Clear
    cboTipopet.AddItem "Todos                            ||NULL"
    cboTipopet.AddItem "Normal                           ||NOR"
    cboTipopet.AddItem "Especial                         ||ESP"
    cboTipopet.AddItem "Propia                           ||PRO"
    cboTipopet.AddItem "Proyecto                         ||PRJ"
    cboTipopet.AddItem "Auditor�a Interna                ||AUI"
    cboTipopet.AddItem "Auditor�a Externa                ||AUX"
    cboTipopet.ListIndex = 0
    '{ add -001- a.
    With cmbPetClass
        .AddItem "Todas" & Space(50) & "||TODO"
        .AddItem "S/C (Sin clase)" & Space(50) & "||SINC"     ' add -003- a.
        .AddItem "Correctivo" & Space(50) & "||CORR"
        .AddItem "Atenci�n a usuario" & Space(50) & "||ATEN"
        .AddItem "Optimizaci�n" & Space(50) & "||OPTI"
        .AddItem "Mantenimiento evolutivo" & Space(50) & "||EVOL"
        .AddItem "Nuevo desarrollo" & Space(50) & "||NUEV"
        .AddItem "Spufi" & Space(50) & "||SPUF"
        .ListIndex = 0
    End With
    cmbPetImpTech.ListIndex = 0     'Por defecto son todos
    cmbPetDocu.ListIndex = 0        'Por defecto son todos
    '}
   
    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        xDirePet = ClearNull(aplRST!cod_direccion)
        xGerePet = ClearNull(aplRST!cod_gerencia)
        xSectPet = ClearNull(aplRST!cod_sector)
        xGrupPet = ClearNull(aplRST!cod_grupo)
        xDireSec = ClearNull(aplRST!cod_direccion)
        xGereSec = ClearNull(aplRST!cod_gerencia)
        xSectSec = ClearNull(aplRST!cod_sector)
        xGrupSec = ClearNull(aplRST!cod_grupo)
        aplRST.Close
    End If
    
    Call Status("Cargando estados...")
    cboEstadoPet.Clear
    cboEstadoSec.Clear
    If sp_GetEstado(Null) Then
        Do While Not aplRST.EOF
            If InStr("PLANRK|EJECRK|ESTIRK", ClearNull(aplRST!cod_estado)) = 0 Then
                If Val(ClearNull(aplRST!flg_petici)) > 0 Then
                    cboEstadoPet.AddItem Trim(aplRST!nom_estado) & Space(DISTANCIA) & "||" & aplRST!cod_estado
                    If Val(ClearNull(aplRST!flg_petici)) > 1 Then
                        auxCodPet = auxCodPet & aplRST!cod_estado & "|"
                    End If
                End If
                If Val(ClearNull(aplRST!flg_secgru)) > 0 Then
                    cboEstadoSec.AddItem Trim(aplRST!nom_estado) & Space(DISTANCIA) & "||" & aplRST!cod_estado
                    If Val(ClearNull(aplRST!flg_secgru)) > 1 Then
                        auxCodSec = auxCodSec & aplRST!cod_estado & "|"
                    End If
                End If
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
        
    cboEstadoPet.AddItem "Peticiones en estado terminal" & Space(90) & "||" & "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", 0    ' add -016- a.
    cboEstadoPet.AddItem "Peticiones en estado activo" & Space(90) & "||" & auxCodPet, 0
    cboEstadoPet.AddItem "Sin filtrar por estado" & Space(90) & "||" & "NULL", 0
    cboEstadoPet.ListIndex = 0
    
    cboEstadoSec.AddItem "Sector/Grupo en estado terminal" & Space(90) & "||" & "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", 0    ' add -016- a.
    cboEstadoSec.AddItem "Sector/Grupo en estado activo" & Space(90) & "||" & auxCodSec, 0
    cboEstadoSec.AddItem "Sector/Grupo en estado pendiente" & Space(90) & "||OPINIO|EVALUA|PLANIF|ESTIMA|", 0
    cboEstadoSec.AddItem "Sin filtrar por estado" & Space(90) & "||" & "NULL", 0
    cboEstadoSec.ListIndex = 0
    cboBBVASec.AddItem "Todas las �res ejecutoras" & Space(150) & "||NULL", 0
    cboBBVASec.ListIndex = 0
    cboBBVAPet.AddItem "Todas las �res solicitantes" & Space(150) & "||NULL", 0
    cboBBVAPet.ListIndex = 0
    
    Call Status("Cargando situaciones...")
    cboSituacionPet.Clear
    cboSituacionSec.Clear
    If sp_GetSituacion(Null) Then
        Do While Not aplRST.EOF
            cboSituacionPet.AddItem Trim(aplRST!nom_situacion) & Space(90) & "||" & aplRST!cod_situacion
            cboSituacionSec.AddItem Trim(aplRST!nom_situacion) & Space(90) & "||" & aplRST!cod_situacion
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboSituacionPet.AddItem "Sin filtrar por situaci�n" & Space(90) & "||" & "NULL", 0
    cboSituacionPet.ListIndex = 0
    cboSituacionSec.AddItem "Sin filtrar por situaci�n" & Space(90) & "||" & "NULL", 0
    cboSituacionSec.ListIndex = 0
    
    Call Status("Cargando direcciones...")
    If sp_GetDireccion("") Then
        Do While Not aplRST.EOF
            cboDireccionPet.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||" & aplRST(0)
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboDireccionSec.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_direccion) & "|NULL|NULL|NULL"
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando gerencias...")
    If sp_GetGerenciaXt("", "") Then
        Do While Not aplRST.EOF
            cboGerenciaPet.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(DISTANCIA) & "||" & aplRST(0)
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboGerenciaSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(DISTANCIA) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|NULL|NULL"
            End If
            'cboGrupoSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(120) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|NULL|NULL"
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando sector...")
    If sp_GetSectorXt(Null, Null, Null) Then
        Do While Not aplRST.EOF
            cboSectorPet.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(DISTANCIA) & "||" & aplRST(0)
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboSectorSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(DISTANCIA) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|NULL"
            End If
           'cboGrupoSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(120) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|NULL"
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Cargando grupo...")
    If sp_GetGrupoXt("", "") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                cboGrupoSec.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(DISTANCIA) & "||" & ClearNull(aplRST!cod_direccion) & ClearNull(aplRST!cod_gerencia) & ClearNull(aplRST!cod_sector) & ClearNull(aplRST!cod_grupo) & "|" & ClearNull(aplRST!cod_direccion) & "|" & ClearNull(aplRST!cod_gerencia) & "|" & ClearNull(aplRST!cod_sector) & "|" & ClearNull(aplRST!cod_grupo)
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboBpar.Clear
    If sp_GetRecursoPerfil(Null, "BPAR") Then
        Do While Not aplRST.EOF
            cboBpar.AddItem Trim(aplRST!nom_recurso) & Space(DISTANCIA) & "||" & Trim(aplRST!cod_recurso)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboBpar.AddItem "Sin filtrar por B.Partner" & Space(DISTANCIA) & "||" & "NULL", 0
    cboBpar.ListIndex = 0
    cboDireccionPet.ListIndex = PosicionCombo(Me.cboDireccionPet, xDirePet, True)
    cboGerenciaPet.ListIndex = PosicionCombo(Me.cboGerenciaPet, xGerePet, True)
    cboSectorPet.ListIndex = PosicionCombo(Me.cboSectorPet, xSectPet, True)
    cboDireccionSec.ListIndex = PosicionCombo(Me.cboDireccionSec, xDireSec, True)
    cboGerenciaSec.ListIndex = PosicionCombo(Me.cboGerenciaSec, xDireSec & xGereSec, True)
    cboSectorSec.ListIndex = PosicionCombo(Me.cboSectorSec, xDireSec & xGereSec & xSectSec, True)
    cboGrupoSec.ListIndex = PosicionCombo(Me.cboGrupoSec, xDireSec & xGereSec & xSectSec & xGrupSec, True)
    '{ add -005- a.
    If glUsrPerfilActual = "SADM" Then
        cmdExportar.Enabled = True
        cmdExportar.Visible = True
    End If
    '}
    cmbRegulatorio.ListIndex = 0        ' add -008- a.
    
    '{ add -013- a.
    With cmbDocumentoAdjunto
        .Clear
        .AddItem "C102 : Dise�o de soluci�n"
        .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
        .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
        .AddItem "C204 : Casos de prueba"
        .AddItem "CG04 : Documento de cambio de alcance"
        .AddItem "T700 : Plan de implantaci�n Desarrollo"
        .AddItem "T710 : Principios de Desarrollo Aplicativo"
        .AddItem "C310 : Definici�n de Base de Datos"
        .AddItem "EML1 : Mail de OK alcance"
        .AddItem "EML2 : Mail de OK prueba de usuario"
        .AddItem "IDH1 : Informe de Homologaci�n HSOB"
        .AddItem "IDH2 : Informe de Homologaci�n HOME"
        .AddItem "IDH3 : Informe de Homologaci�n HOMA"
        .AddItem "OTRO : Otros documentos"
        Call setHabilCtrl(cmbDocumentoAdjunto, "DIS")
    End With
    '}
    With cboUsuario
        .Clear
        .AddItem "* Todos" & Space(100) & "||NULL"
        If sp_GetRecurso(Null, "R", Null) Then
            Do While Not aplRST.EOF
                .AddItem aplRST!cod_recurso & " : " & Trim(aplRST!nom_recurso) & Space(DISTANCIA) & "||" & aplRST!cod_recurso   ' upd -015- c.
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            .Enabled = True
        Else
            .Clear
            .AddItem "No hay usuarios" & Space(100) & "||NULL"
            .ListIndex = 0
            Call setHabilCtrl(cboUsuario, "DIS")
        End If
    End With
    
    With cboSolicitante
        .Clear
        .AddItem "* Todos" & Space(100) & "||NULL"
        If sp_GetRecursoPerfil(Null, "SOLI") Then
            Do While Not aplRST.EOF
                .AddItem aplRST!cod_recurso & " : " & Trim(aplRST!nom_recurso) & Space(DISTANCIA) & "||" & aplRST!cod_recurso    ' upd -015- c.
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            .Enabled = True
        Else
            .Clear
            .AddItem "No hay solicitantes" & Space(DISTANCIA) & "||NULL"
            .ListIndex = 0
            Call setHabilCtrl(cboSolicitante, "DIS")
        End If
    End With
    
    With cboReferente
        .Clear
        .AddItem "* Todos" & Space(100) & "||NULL"
        If sp_GetRecursoPerfil(Null, "REFE") Then
            Do While Not aplRST.EOF
                .AddItem aplRST!cod_recurso & " : " & Trim(aplRST!nom_recurso) & Space(DISTANCIA) & "||" & aplRST!cod_recurso   ' upd -015- c.
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            .Enabled = True
        Else
            .Clear
            .AddItem "No hay referentes" & Space(DISTANCIA) & "||NULL"
            .ListIndex = 0
            Call setHabilCtrl(cboReferente, "DIS")
        End If
    End With
    
    With cboSupervisor
        .Clear
        .AddItem "* Todos" & Space(DISTANCIA) & "||NULL"
        If sp_GetRecursoPerfil(Null, "SUPE") Then
            Do While Not aplRST.EOF
                .AddItem aplRST!cod_recurso & " : " & Trim(aplRST!nom_recurso) & Space(DISTANCIA) & "||" & aplRST!cod_recurso   ' upd -015- c.
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            .Enabled = True
        Else
            .Clear
            .AddItem "No hay supervisores" & Space(DISTANCIA) & "||NULL"
            .ListIndex = 0
            Call setHabilCtrl(cboSupervisor, "DIS")
        End If
    End With
    
    With cboDirector
        .Clear
        .AddItem "* Todos" & Space(100) & "||NULL"
        If sp_GetRecursoPerfil(Null, "DIRE") Then
            Do While Not aplRST.EOF
                .AddItem aplRST!cod_recurso & " : " & Trim(aplRST!nom_recurso) & Space(DISTANCIA) & "||" & aplRST!cod_recurso   ' upd -015- c.
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
            .Enabled = True
        Else
            .Clear
            .AddItem "No hay autorizantes" & Space(DISTANCIA) & "||NULL"
            .ListIndex = 0
            Call setHabilCtrl(cboDirector, "DIS")
        End If
    End With
    
    With cmbTextos
        .Clear
        .AddItem "* Ninguno" & Space(DISTANCIA) & "||NULL"
        .AddItem "a. Descripci�n del pedido" & Space(DISTANCIA) & "||DESCRIPCIO"
        .AddItem "b. Caracter�sticas principales" & Space(DISTANCIA) & "||CARACTERIS"
        .AddItem "c. Motivos" & Space(DISTANCIA) & "||MOTIVOS"
        .AddItem "d. Beneficios esperados" & Space(DISTANCIA) & "||BENEFICIOS"
        .AddItem "e. Relaci�n con otros requerimientos" & Space(DISTANCIA) & "||RELACIONES"
        .AddItem "f. Observaciones" & Space(DISTANCIA) & "||OBSERVACIO"
        .Enabled = True
        .ListIndex = 0
    End With
    
    With cmbOpcionesTexto
        .Clear
        .AddItem "a. En cualquier parte del texto (LIKE)" & Space(100) & "||LIKE"
        .AddItem "b. Solo al comienzo del texto   (LEFT)" & Space(100) & "||LEFT"
        .AddItem "c. Solo igual al texto          (EQUAL)" & Space(100) & "||EQUAL"
        .Enabled = True
        .ListIndex = 0
    End With
    Call setHabilCtrl(cmbOpcionesTexto, "DIS")
    Call setHabilCtrl(txtMEM_Texto, "DIS")
    '}
    With cmbRegulatorio
        .Clear
        .AddItem "Todos" & ESPACIOS & "||NULL"
        .AddItem "Sin especificar" & ESPACIOS & "||-"
        .AddItem "No" & ESPACIOS & "||N"
        .AddItem "Si" & ESPACIOS & "||S"
        .ListIndex = 0
    End With
    Call Puntero(False)
    Call Status("Listo.")
    flgEnCarga = False
End Sub

'{ add -013- a.
Private Sub cmbPetDocu_Click()
    If UCase(Left(cmbPetDocu, 1)) = "C" Then
        Call setHabilCtrl(cmbDocumentoAdjunto, "NOR")
        cmbDocumentoAdjunto.ListIndex = 0
    Else
        cmbDocumentoAdjunto.ListIndex = -1
        Call setHabilCtrl(cmbDocumentoAdjunto, "DIS")
    End If
End Sub
'}

Private Sub cmdEraseProyectos_Click()
    txtPrjId = ""
    txtPrjSubId = ""
    txtPrjSubsId = ""
    txtPrjNom = ""
End Sub

Private Sub cmdPrjFilter_Click()
    Load frmProyectosIDMSeleccion
    frmProyectosIDMSeleccion.Show 1
    With frmProyectosIDMSeleccion
        txtPrjId.Text = .lProjId
        txtPrjSubId.Text = .lProjSubId
        txtPrjSubsId.Text = .lProjSubSId
        txtPrjNom.Text = Trim(.cProjNom)
    End With
End Sub

Private Sub cmdChgBP_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim i As Integer
    Dim xBP As String
    
    flgEnCarga = True
    
    auxSelBpar.Show vbModal
    flgEnCarga = False
    
    If auxSelBpar.CodRecurso = "" Then
        Exit Sub
    End If
    
    DoEvents
    
    xBP = auxSelBpar.CodRecurso
    
    Dim nRwD As Integer, nRwH As Integer
    flgMulti = False
        
    With grdDatos
        If .Rows < 2 Then
            Exit Sub
        End If
        Call Puntero(True)
        Call Status("")
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                If (Not flgMulti) Then
                    If MsgBox("Desea reasignar esta/s Peticion/es a '" & auxSelBpar.NomRecurso & "'?", vbYesNo) = vbNo Then
                        Exit For
                    End If
                End If
                flgMulti = True
                Call Status("Orden:" & i)
                Call Puntero(True)
                Call sp_UpdatePetField(.TextMatrix(i, colNroInterno), "BPARTNE", xBP, Null, Null)
                Call Puntero(False)
                .TextMatrix(i, colMultiSelect) = ""
           End If
        Next
    End With
    Call Puntero(False)
    Call Status("")
End Sub

Private Sub cmdAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "VIEW"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    txtAgrup.Text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdAgrupar_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim i As Integer
    glSelectAgrup = ""
    glModoAgrup = "ALTAX"
    glNumeroAgrup = ""
    glModoSelectAgrup = "MODI"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    DoEvents
    
    If ClearNull(glSelectAgrup) = "" Then
        Exit Sub
    End If
    
    Dim nRwD As Integer, nRwH As Integer
    flgMulti = False
        
    With grdDatos
        If .Rows < 2 Then
            Exit Sub
        End If
        Call Puntero(True)
        Call Status("")
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                If (Not flgMulti) Then
                    If MsgBox("Desea que esta/s Peticion/es integren el Agrupamiento '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbNo Then
                        Exit For
                    End If
                End If
                Call Status("Orden:" & i)
                flgMulti = True
                'si ya existe agrup-petic no hace nada, ni siquiera avisa
                If sp_GetAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                    aplRST.Close
                Else
                    Call Puntero(True)
                    If sp_GetAgrupPeticDup(glSelectAgrup, .TextMatrix(i, colNroInterno)) Then
                        Call Puntero(False)
                        If MsgBox("La Peticion:" & .TextMatrix(i, colTitulo) & Chr(13) & "ya est� declarada en el agrupamiento: " & ClearNull(aplRST!agr_titulo) & Chr(13) & "Desea igualmente que integre '" & Trim(Left(glSelectAgrupStr, 40)) & "'?", vbYesNo) = vbYes Then
                           aplRST.Close
                           Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                        Else
                           aplRST.Close
                        End If
                    Else
                        Call sp_InsAgrupPetic(glSelectAgrup, .TextMatrix(i, colNroInterno))
                    End If
                    Call Puntero(False)
                End If
                .TextMatrix(i, colMultiSelect) = ""
           End If
        Next
    End With
    Call Puntero(False)
    Call Status("")
End Sub

Private Sub cmdClrAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = ""
    txtAgrup.Text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdPrintPet_Click()
    If flgEnCarga = True Then Exit Sub
    Dim flgMulti As Boolean
    Dim i As Integer
    Dim nCopias As Integer
    
    If grdDatos.Rows < 2 Then
        Exit Sub
    End If
    
    rptFormulario.Show vbModal
    If (Not rptFormulario.chkFormu) And (Not rptFormulario.chkHisto) Then
        Exit Sub
    End If
    nCopias = rptFormulario.Copias
    
    Call Puntero(True)
    Call Status("")
    flgMulti = False
    DoEvents
    With grdDatos
        While nCopias > 0
            i = 1
            For i = 1 To .Rows - 1
               If ClearNull(.TextMatrix(i, colMultiSelect)) = "�" Then
                    flgMulti = True
                    Call Status("Orden:" & i)
                    If rptFormulario.chkFormu Then
                        Call PrintFormulario(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If rptFormulario.chkHisto Then
                        Call PrintHitorial(ClearNull(.TextMatrix(i, colNroInterno)))
                    End If
                    If nCopias = 1 Then
                        .TextMatrix(i, colMultiSelect) = ""
                    End If
               End If
            Next
            nCopias = nCopias - 1
        Wend
    End With
    Call Puntero(False)
    If flgMulti Then
        MsgBox ("Impresi�n finalizada.")
    Else
        MsgBox ("Nada seleccionado para imprimir.")
    End If
    Call Status("Listo.")
End Sub

Private Sub cmdExportar_Click()
    If flgEnCarga = True Then Exit Sub
    glEsMinuta = False
    frmPeticionesShellView01Expo.Show vbModal
End Sub

Private Sub cmdSetExport_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    On Error Resume Next
    frmPeticionesShellView01Expo.Show vbModal
    Call LockProceso(False)
End Sub

Private Sub cmdMinuta_Click()
    If flgEnCarga = True Then Exit Sub
    glEsMinuta = True
    frmPeticionesShellView01Expo.Show vbModal
End Sub

Private Sub cmdVisualizar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        If grdDatos.TextMatrix(grdDatos.RowSel, colPetHist) <> "H" Then    ' add -012- b.
            glModoPeticion = "VIEW"
            On Error Resume Next
            frmPeticionesCarga.Show vbModal
            Call LockProceso(False)
        '{ add -012- b.
        Else
            MsgBox "La petici�n se encuentra en los archivos hist�ricos." & vbCrLf & "No puede trabajar desde aqu� con esta petici�n.", vbInformation + vbOKOnly, "Petici�n en hist�ricos"
        End If
        '}
    End If
End Sub

Private Sub cmdCerrar_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If grdDatos.Visible = True Then
        'Call setHabilCtrl(cmdMasFiltros, "NOR")        ' add -015- a.
        Call initQuery
    Else
        Call LockProceso(False)
        Unload Me
    End If
    LockProceso (False)
End Sub

Private Sub cmdConfirmar_Click()        ' Consultar: generar la grilla
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    petTipo = CodigoCombo(cboTipopet, True)
    petPrioridad = CodigoCombo(cboPrioridad, True)
    petNivel = verNivelPet()
    petArea = verAreaPet()
    petEstado = DatosCombo(cboEstadoPet)
    petImportancia = CodigoCombo(cboImportancia, True)
    petSituacion = CodigoCombo(cboSituacionPet, True)
    secNivel = verNivelSec()
    secArea = verAreaSec()
    If secArea = "ERROR" Then
        LockProceso (False)
        Exit Sub
    End If
    secEstado = DatosCombo(cboEstadoSec)
    secSituacion = CodigoCombo(cboSituacionSec, True)
    auxFiltro = ""
    auxFiltro = auxFiltro & "Tipo Petici�n: " & TextoCombo(cboTipopet) & Chr$(13)
    auxFiltro = auxFiltro & "Prioridad: " & TextoCombo(cboPrioridad) & Chr$(13)
    auxFiltro = auxFiltro & "Area Solicitante: " & petAreaTxt & Chr$(13)
    auxFiltro = auxFiltro & "Estado Petici�n: " & TextoCombo(cboEstadoPet) & Chr$(13)
    auxFiltro = auxFiltro & "Situaci�n Petici�n: " & TextoCombo(cboSituacionPet) & Chr$(13)
    'Call setHabilCtrl(cmdMasFiltros, "DIS")        ' add -015- a.
    Call initView
    Call CargarGrid
    LockProceso (False)
End Sub

Private Sub verAreaFiltro()
    If flgEnCarga Then Exit Sub
    If ClearNull(txtDESDEiniplan.Text) <> "" Or _
        ClearNull(txtDESDEfinplan.Text) <> "" Or _
        ClearNull(txtHASTAiniplan.Text) <> "" Or _
        ClearNull(txtDESDEinireal.Text) <> "" Or _
        ClearNull(txtDESDEfinreal.Text) <> "" Or _
        ClearNull(txtHASTAinireal.Text) <> "" Or _
        ClearNull(txtHASTAfinreal.Text) <> "" Or _
        cboEstadoSec.ListIndex > 0 Or _
        cboSituacionSec.ListIndex > 0 Or _
        OptNivelSec(0).Value = False Then
        'If chkAreGru.Value = 0 Then
        '    If OptNivelSec(4).Value = True Then
        '        chkAreGru.Value = 1
        '    Else
        '        chkAreSec.Value = 1
        '    End If
        'End If
    End If
End Sub

Private Sub cboEstadoSec_Click()
    verAreaFiltro
End Sub

Private Sub cboSituacionSec_Click()
    verAreaFiltro
End Sub

'Private Sub grdDatos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'    'If Button <> 0 Then
'    i = i + 1
'    Debug.Print i & ". " & Button
'    'End If
'End Sub

Private Sub txtDESDEiniplan_LostFocus()
    verAreaFiltro
End Sub

Private Sub txtDESDEfinplan_LostFocus()
    verAreaFiltro
End Sub

Private Sub txtHASTAiniplan_LostFocus()
    verAreaFiltro
End Sub

Private Sub txtHASTAfinplan_LostFocus()
    verAreaFiltro
End Sub

Private Sub txtDESDEinireal_LostFocus()
    verAreaFiltro
End Sub

Private Sub txtDESDEfinreal_LostFocus()
    verAreaFiltro
End Sub

Private Sub txtHASTAinireal_LostFocus()
    verAreaFiltro
End Sub

Private Sub txtHASTAfinreal_LostFocus()
    verAreaFiltro
End Sub

Private Sub chkAreSec_Click()
    'If chkAreSec.Value = 1 Then
    '    chkAreGru.Value = 0
    'End If
    verAreaFiltro
End Sub

Private Sub chkAreGru_Click()
    'If chkAreGru.Value = 1 Then
    '    chkAreSec.Value = 0
    'End If
    verAreaFiltro
End Sub

Private Sub Form_Activate()
    Me.WindowState = vbMaximized
    'StatusBarObjectIdentity Me
End Sub

Sub initQuery()
    cmdConfirmar.Visible = True
    cmdConfirmar.Enabled = True
    cmdVisualizar.Visible = False
    cmdChgBP.Visible = False
    cmdAgrupar.Visible = False
    cmdPrintPet.Visible = False
    cmdExportar.Visible = False
    '{ add -00x-
    'cmdImprimir.Visible = False
    'frmCrtPrn.Visible = False
    '}
    grdDatos.Visible = False
    frmFiltro.Visible = False
    'frmGral.Visible = True     ' del -015- a.
    '{ add -015- a.
    If flgQuery = "QRY1" Then
        frmGral.Visible = True
        fraOtrosFiltros.Visible = False
        cmdMasFiltros.Visible = True
    Else
        frmGral.Visible = False
        fraOtrosFiltros.Visible = True
        cmdMasFiltros.Visible = True
        'cmdMasFiltros.Caption = "M�s filtros..."
    End If
    '}
    Me.Caption = Me.Tag     ' add -004- b.
End Sub

Sub initView()
    cmdConfirmar.Enabled = False
    cmdConfirmar.Visible = False
    cmdVisualizar.Visible = True
    cmdVisualizar.Enabled = False
    cmdChgBP.Visible = True
    cmdAgrupar.Visible = True
    cmdPrintPet.Visible = True
    cmdPrintPet.Enabled = False
    cmdChgBP.Enabled = False
    cmdAgrupar.Enabled = False
    frmGral.Visible = False
    '{ add -015- a.
    fraOtrosFiltros.Visible = False
    cmdMasFiltros.Visible = False
    '}
    grdDatos.Visible = True
    frmFiltro.Visible = True
End Sub

Function verNivelPet() As Variant
    verNivelPet = "NULL"
    If OptNivelPet(0).Value = True Then
         verNivelPet = "NULL"
    End If
    If OptNivelPet(1).Value = True Then
         verNivelPet = "DIRE"
    End If
    If OptNivelPet(2).Value = True Then
         verNivelPet = "GERE"
    End If
    If OptNivelPet(3).Value = True Then
         verNivelPet = "SECT"
    End If
End Function

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    If grdDatos.Row <= grdDatos.RowSel Then
        nRwD = grdDatos.Row
        nRwH = grdDatos.RowSel
    Else
        nRwH = grdDatos.Row
        nRwD = grdDatos.RowSel
    End If
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, colMultiSelect) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, colMultiSelect) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        If Button = vbLeftButton Then
        With grdDatos
        If .RowSel > 0 Then
             If ClearNull(.TextMatrix(.RowSel, colMultiSelect)) = "" Then
                .TextMatrix(.RowSel, colMultiSelect) = "�"
             Else
                .TextMatrix(.RowSel, colMultiSelect) = ""
            End If
        End If
        End With
        End If
    End If
    If Shift = 0 Then
        If Button = vbLeftButton Then
            With grdDatos
                For i = 1 To .Rows - 1
                   .TextMatrix(i, colMultiSelect) = ""
                Next
                If .RowSel > 0 Then
                    .TextMatrix(.RowSel, colMultiSelect) = "�"
                End If
            End With
        End If
    End If
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

Private Sub OptNivelPet_Click(Index As Integer)
    cboBBVAPet.Visible = False
    cboBBVAPet.Enabled = False
    cboDireccionPet.Visible = False
    cboGerenciaPet.Visible = False
    cboSectorPet.Visible = False
    cboDireccionPet.Enabled = False
    cboGerenciaPet.Enabled = False
    cboSectorPet.Enabled = False
    Select Case Index
        Case 0
            cboBBVAPet.Visible = True
        Case 1
            cboDireccionPet.Visible = True
            cboDireccionPet.Enabled = True
        Case 2
            cboGerenciaPet.Visible = True
            cboGerenciaPet.Enabled = True
        Case 3
            cboSectorPet.Visible = True
            cboSectorPet.Enabled = True
    End Select
End Sub

Function verAreaPet() As Variant
    verAreaPet = "NULL"
    petAreaTxt = ""
    If OptNivelPet(0).Value = True Then
         verAreaPet = "NULL"
         petAreaTxt = TextoCombo(Me.cboBBVAPet)
    End If
    If OptNivelPet(1).Value = True Then
         verAreaPet = CodigoCombo(Me.cboDireccionPet, True)
         petAreaTxt = TextoCombo(Me.cboDireccionPet)
    End If
    If OptNivelPet(2).Value = True Then
         verAreaPet = CodigoCombo(Me.cboGerenciaPet, True)
         petAreaTxt = TextoCombo(Me.cboGerenciaPet)
    End If
    If OptNivelPet(3).Value = True Then
         verAreaPet = CodigoCombo(Me.cboSectorPet, True)
         petAreaTxt = TextoCombo(Me.cboSectorPet)
    End If
End Function

Private Sub OptNivelSec_Click(Index As Integer)
    cboBBVASec.Visible = False
    cboDireccionSec.Visible = False
    cboGerenciaSec.Visible = False
    cboSectorSec.Visible = False
    cboGrupoSec.Visible = False
    cboBBVASec.Enabled = False
    cboDireccionSec.Enabled = False
    cboGerenciaSec.Enabled = False
    cboSectorSec.Enabled = False
    cboGrupoSec.Enabled = False
    Select Case Index
        Case 0      ' Todo el BBVA
            cboBBVASec.Visible = True
            verAreaFiltro
            '{ add -014- a.
            optApertura(0).Value = True: optApertura(0).Enabled = True
            optApertura(1).Value = False: optApertura(1).Enabled = True
            optApertura(2).Value = False: optApertura(2).Enabled = True
            '}
        Case 1      ' Nivel: Direcci�n
            cboDireccionSec.Visible = True
            cboDireccionSec.Enabled = True
            'If chkAreGru.Value = 0 Then
            '    chkAreSec.Value = 1
            'End If
            '{ add -014- a.
            optApertura(0).Value = False: optApertura(0).Enabled = False
            optApertura(1).Value = False: optApertura(1).Enabled = True
            optApertura(2).Value = True: optApertura(2).Enabled = True
            '}
        Case 2      ' Nivel: Gerencia
            cboGerenciaSec.Visible = True
            cboGerenciaSec.Enabled = True
            'If chkAreGru.Value = 0 Then
            '    chkAreSec.Value = 1
            'End If
            '{ add -014- a.
            optApertura(0).Value = False: optApertura(0).Enabled = False
            optApertura(1).Value = False: optApertura(1).Enabled = True
            optApertura(2).Value = True: optApertura(2).Enabled = True
            '}
        Case 3      ' Nivel: Sector
            cboSectorSec.Visible = True
            cboSectorSec.Enabled = True
            'If chkAreGru.Value = 0 Then
            '    chkAreSec.Value = 1
            'End If
            '{ add -014- a.
            optApertura(0).Value = False: optApertura(0).Enabled = False
            optApertura(1).Value = False: optApertura(1).Enabled = True
            optApertura(2).Value = True: optApertura(2).Enabled = True
            '}
        Case 4      ' Nivel: Grupo
            cboGrupoSec.Visible = True
            cboGrupoSec.Enabled = True
            'chkAreGru.Value = 1
            '{ add -014- a.
            optApertura(0).Value = False: optApertura(0).Enabled = False
            optApertura(1).Value = False: optApertura(1).Enabled = False
            optApertura(2).Value = True: optApertura(2).Enabled = True
            '}
    End Select
End Sub

Function verAreaSec() As Variant
    Dim vRetorno() As String
    Dim auxAgrup As String
       
    verAreaSec = "NULL"
    secDire = "NULL"
    secGere = "NULL"
    secSect = "NULL"
    secGrup = "NULL"
    secAreaTxt = ""
       
    If OptNivelSec(0).Value = True Then
         secAreaTxt = TextoCombo(Me.cboBBVASec)
    End If
    If OptNivelSec(1).Value = True Then
         If cboDireccionSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Direcci�n")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboDireccionSec, True)
         auxAgrup = DatosCombo(Me.cboDireccionSec)
         secAreaTxt = TextoCombo(Me.cboDireccionSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(2).Value = True Then
         If cboGerenciaSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Gerencia")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboGerenciaSec, True)
         auxAgrup = DatosCombo(Me.cboGerenciaSec)
         secAreaTxt = TextoCombo(Me.cboGerenciaSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(3).Value = True Then
         If cboSectorSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Sector")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboSectorSec, True)
         auxAgrup = DatosCombo(Me.cboSectorSec)
         secAreaTxt = TextoCombo(Me.cboSectorSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
    If OptNivelSec(4).Value = True Then
         If cboGrupoSec.ListIndex < 0 Then
            MsgBox ("Debe seleccionar Grupo")
            verAreaSec = "ERROR"
            Exit Function
         End If
         verAreaSec = CodigoCombo(Me.cboGrupoSec, True)
         secAreaTxt = TextoCombo(Me.cboGrupoSec)
         auxAgrup = DatosCombo(Me.cboGrupoSec)
         If ParseString(vRetorno, auxAgrup, "|") > 0 Then
             secDire = vRetorno(2)
             secGere = vRetorno(3)
             secSect = vRetorno(4)
             secGrup = vRetorno(5)
         End If
    End If
End Function

Function verNivelSec() As Variant
       'verNivelSec = "NULL"
        'If chkAreSec.Value = 1 Then
        '    verNivelSec = "SECT"
        'End If
        'If chkAreGru.Value = 1 Then
        '    verNivelSec = "GRUP"
        'End If
        '{ add -014- a.
        If optApertura(0).Value Then
            verNivelSec = "NULL"
        ElseIf optApertura(1).Value Then
            verNivelSec = "SECT"
        ElseIf optApertura(2).Value Then
            verNivelSec = "GRUP"
        End If
        '}
End Function

Sub CargarGrid()
    On Error GoTo ErrHandler    ' add -004- a.
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
    Dim xAux As String
    Dim vRetorno() As String
    Dim auxAgrup As String
    Dim lPeticiones_Listadas As Long        ' add -011- a.
    '{ add -004- a.
    Dim lRecCount As Long
    Dim bGridOverflow As Boolean
    Dim bRegenerate As Boolean
    
    bRegenerate = False
    bGridOverflow = False
    '}
    
    If ClearNull(txtAgrup.Text) <> "" Then
        If ParseString(vRetorno, txtAgrup.Text, "|") > 0 Then
            auxAgrup = vRetorno(2)
        End If
    Else
        auxAgrup = ""
    End If
    
    glNumeroPeticion = ""
    glCodigoRecurso = ""
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    If secNivel = "NULL" And secArea = "NULL" And secEstado = "NULL" And secSituacion = "NULL" And _
        IsNull(txtDESDEiniplan.DateValue) And IsNull(txtHASTAiniplan.DateValue) And IsNull(txtDESDEfinplan.DateValue) And IsNull(txtHASTAfinplan.DateValue) And _
        IsNull(txtDESDEinireal.DateValue) And IsNull(txtHASTAinireal.DateValue) And IsNull(txtDESDEfinreal.DateValue) And IsNull(txtHASTAfinreal.DateValue) Then
        flgNoHijos = True
    Else
        flgNoHijos = False
    End If
    With grdDatos
        .Clear
        .Cols = 36      ' add -012- b.
        DoEvents
        .HighLight = flexHighlightAlways
        .Rows = 1
        .TextMatrix(0, colNroAsignado) = "Pet. N�": .ColWidth(colNroAsignado) = 700: .ColAlignment(colNroAsignado) = flexAlignRightCenter
        .TextMatrix(0, colPrioridad) = "Pri.": .ColWidth(colPrioridad) = 400: .ColAlignment(colPrioridad) = 3
        .TextMatrix(0, colTipoPet) = "Tipo": .ColWidth(colTipoPet) = 800: .ColAlignment(colTipoPet) = flexAlignLeftCenter
        .TextMatrix(0, colRegulatorio) = "Reg.": .ColWidth(colRegulatorio) = 500: .ColAlignment(colRegulatorio) = flexAlignCenterCenter
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 3800: .ColAlignment(colTitulo) = 0
        .TextMatrix(0, colESTADO) = "Estado Pet": .ColWidth(colESTADO) = 2500
        .TextMatrix(0, colEstadoFec) = "F.Estado"
        .TextMatrix(0, colSituacion) = "Situaci�n Pet.": .ColWidth(colSituacion) = 1500
        .TextMatrix(0, colSecSol) = "Sect. Solicitante": .ColWidth(colSecSol) = 3000
        .TextMatrix(0, colEsfuerzo) = "Hs.Pet"
        .TextMatrix(0, colFinicio) = "F.Ini.Planif.Pet."
        .TextMatrix(0, colFtermin) = "F.Fin Planif.Pet."
        .TextMatrix(0, colImportancia) = "Visibilidad": .ColWidth(colImportancia) = 2000
        .TextMatrix(0, colOrientacion) = "Orientaci�n": .ColWidth(colOrientacion) = 2000
        .TextMatrix(0, colCorpLocal) = "Corp/Local": .ColWidth(colCorpLocal) = 1000: .ColAlignment(colCorpLocal) = flexAlignCenterCenter
        .TextMatrix(0, colEstHijo) = "Est. Sec/Grp"
        .TextMatrix(0, colSitHijo) = "Sit. Sec/Grp"
        .TextMatrix(0, colPrioEjecuc) = "Pri.": .ColWidth(colPrioEjecuc) = 0
        .TextMatrix(0, colNroInterno) = "NroInt.": .ColWidth(colNroInterno) = 0
        .TextMatrix(0, colSecCodDir) = "Cod.Dir"
        .TextMatrix(0, colSecCodGer) = "Cod.Ger"
        .TextMatrix(0, colSecCodSec) = "Cod.Sec"
        .TextMatrix(0, colSecCodSub) = "Cod.Sub"
        .TextMatrix(0, colSecNomArea) = ""
        .TextMatrix(0, colPetClass) = "Clase": .ColWidth(colPetClass) = 800
        .TextMatrix(0, colPetImpTech) = "IT": .ColWidth(colPetImpTech) = 500: .ColAlignment(colPetImpTech) = flexAlignCenterCenter
        .TextMatrix(0, colPetSOx) = "MN.": .ColWidth(colPetSOx) = 500: .ColAlignment(colPetSOx) = flexAlignCenterCenter
        .TextMatrix(0, colPetHist) = "His.": .ColWidth(colPetHist) = 500: .ColAlignment(colPetHist) = flexAlignCenterCenter
        .ColWidth(colMultiSelect) = 200
        .ColWidth(colSRTPETIPLAN) = 0
        .ColWidth(colSRTPETFPLAN) = 0
        .ColWidth(colSRTSECIPLAN) = 0
        .ColWidth(colSRTSECFPLAN) = 0
        If flgNoHijos = True Then
            .ColWidth(colEsfuerzo) = 600
            .ColWidth(colFinicio) = 1000
            .ColWidth(colFtermin) = 1000
            .ColWidth(colEstHijo) = 0
            .ColWidth(colSitHijo) = 0
            .ColWidth(colSecCodDir) = 0: .TextMatrix(0, colSecCodDir) = ""
            .ColWidth(colSecCodGer) = 0: .TextMatrix(0, colSecCodGer) = ""
            .ColWidth(colSecCodSec) = 0: .TextMatrix(0, colSecCodSec) = ""
            .ColWidth(colSecCodSub) = 0: .TextMatrix(0, colSecCodSub) = ""
            .ColWidth(colSecNomArea) = 0: .TextMatrix(0, colSecNomArea) = ""
            .ColWidth(colSecEsfuerzo) = 0: .TextMatrix(0, colSecEsfuerzo) = ""
            .ColWidth(colSecFinicio) = 0: .TextMatrix(0, colSecFinicio) = ""
            .ColWidth(colSecFtermin) = 0: .TextMatrix(0, colSecFtermin) = ""
        Else
            .ColWidth(colEsfuerzo) = 0
            .ColWidth(colFinicio) = 0
            .ColWidth(colFtermin) = 0
            .ColWidth(colSecEsfuerzo) = 600
            .ColWidth(colSecFinicio) = 1000
            .ColWidth(colSecFtermin) = 1000
            .ColWidth(colEstHijo) = 1500
            .ColWidth(colSitHijo) = 1200
            .ColWidth(colSecCodDir) = 0
            .ColWidth(colSecCodGer) = 0
            .ColWidth(colSecCodSec) = 0
            .ColWidth(colSecCodSub) = 0
            .ColWidth(colSecNomArea) = 2000
            If secNivel = "GRUP" Then
                .TextMatrix(0, colSecNomArea) = "Grupo Ejec."
                .TextMatrix(0, colEstHijo) = "Est.Grupo"
                .TextMatrix(0, colSitHijo) = "Sit.Grupo"
                .TextMatrix(0, colSecEsfuerzo) = "Hs.Grupo"
                .TextMatrix(0, colSecFinicio) = "F.Ini.Planif.Gru."
                .TextMatrix(0, colSecFtermin) = "F.Fin Planif.Gru."
            Else
                .TextMatrix(0, colSecNomArea) = "Sector Ejec."
                .TextMatrix(0, colEstHijo) = "Est.Sector"
                .TextMatrix(0, colSitHijo) = "Sit.Sector"
                .TextMatrix(0, colSecEsfuerzo) = "Hs.Sect."
                .TextMatrix(0, colSecFinicio) = "F.Ini.Planif.Sec."
                .TextMatrix(0, colSecFtermin) = "F.Fin Planif.Sec"
            End If
            If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                .ColWidth(colPrioEjecuc) = 350
            End If
        End If
    End With
    CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    
    Call Puntero(True)
    Call Status("Cargando peticiones...")
    If ClearNull(txtTitulo.Text) = "" Then
        xAux = "NULL"
    Else
        xAux = ClearNull(txtTitulo.Text)
    End If
    '{ del -007- a.
    'If Not sp_rptPeticion00(txtNroDesde.Text, txtNroHasta.Text, petTipo, petPrioridad, txtDESDEalta.DateValue, txtHASTAalta.DateValue, petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
    '                        CodigoCombo(Me.cboBpar, True), IIf(chkPrjFilter.Value = 1, "S", "N"), prj_NroProyecto, prj_tipo, prj_titulo, prj_bpar, prj_Orientacion, prj_Importancia, prj_estado, prj_nivel, prj_area, prj_DESDEiniplan, prj_HASTAiniplan, prj_DESDEfinplan, prj_HASTAfinplan, prj_DESDEinireal, prj_HASTAinireal, prj_DESDEfinreal, prj_HASTAfinreal, CodigoCombo(cmbPetClass, True), UCase(Left(cmbPetImpTech.Text, 1))) Then    ' upd -001- a. - Se agregan dos par�metros al final del llamado a la funci�n para soportar el filtro por clase de petici�n e implicancia.
    '}
'    '{ add -008- a. Opciones de filtro para Regulatorio
'    Dim cFiltroRegulatorio As Variant
'    Select Case cmbRegulatorio.List(cmbRegulatorio.ListIndex)
'        Case "Todos": cFiltroRegulatorio = Null
'        Case "Sin especificar": cFiltroRegulatorio = "-"
'        Case "No": cFiltroRegulatorio = "N"
'        Case "Si": cFiltroRegulatorio = "S"
'    End Select
    '}
    '{ add -012- a.
    grdDatos.Visible = False
    cmdVisualizar.Visible = False
    If chkHistorico.Value = 1 Then  ' add -012- b.
        '{ del -015- b.
        'If Not sp_rptPeticion00h(txtNroDesde.Text, txtNroHasta.Text, petTipo, petPrioridad, txtDESDEalta.DateValue, txtHASTAalta.DateValue, petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
        '                        CodigoCombo(Me.cboBpar, True), "N", prj_NroProyecto, prj_tipo, prj_titulo, prj_bpar, prj_Orientacion, prj_Importancia, prj_estado, prj_nivel, prj_area, prj_DESDEiniplan, prj_HASTAiniplan, prj_DESDEfinplan, prj_HASTAfinplan, prj_DESDEinireal, prj_HASTAinireal, prj_DESDEfinreal, prj_HASTAfinreal, CodigoCombo(cmbPetClass, True), UCase(Left(cmbPetImpTech.Text, 1)), UCase(Left(cmbPetDocu.Text, 1)), cFiltroRegulatorio, IIf(UCase(Left(cmbPetDocu, 1)) = "C", UCase(Left(cmbDocumentoAdjunto, 4)), Null), txtPrjId, txtPrjSubId, txtPrjSubsId) Then   ' upd -013- a.    ' upd -015- a. Se agregan los tres campos de proyecto IDM.
        'End If
        '}
        '{ add -015- b.
        If Not sp_rptPeticion00h(txtNroDesde.Text, txtNroHasta.Text, petTipo, petPrioridad, txtDESDEalta.DateValue, txtHASTAalta.DateValue, petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
                                CodigoCombo(Me.cboBpar, True), CodigoCombo(cmbPetClass, True), UCase(Left(cmbPetImpTech.Text, 1)), UCase(Left(cmbPetDocu.Text, 1)), CodigoCombo(cmbRegulatorio, True), IIf(UCase(Left(cmbPetDocu, 1)) = "C", UCase(Left(cmbDocumentoAdjunto, 4)), Null), txtPrjId, txtPrjSubId, txtPrjSubsId) Then   ' upd -013- a.    ' upd -015- a. Se agregan los tres campos de proyecto IDM.
        End If
        '}
        
        lPeticiones_Listadas = lPeticiones_Listadas + aplRST.RecordCount   ' add -011- a.
        Do While Not aplRST.EOF
            With grdDatos
                'lRecCount = lRecCount + 1
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
                .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
                .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!titulo)
                .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
                .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
                .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!sec_nom_estado)
                .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!sec_nom_situacion)
                .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
                .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colEstadoFec) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
                .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
                .TextMatrix(.Rows - 1, colImportancia) = ClearNull(aplRST!importancia_nom)
                .TextMatrix(.Rows - 1, colCorpLocal) = ClearNull(aplRST!corp_local)
                .TextMatrix(.Rows - 1, colOrientacion) = ClearNull(aplRST!nom_orientacion)
                .TextMatrix(.Rows - 1, colSecSol) = ClearNull(aplRST!sol_sector)
                If flgNoHijos = True Then
                    .TextMatrix(.Rows - 1, colSecCodDir) = ""
                    .TextMatrix(.Rows - 1, colSecCodGer) = ""
                    .TextMatrix(.Rows - 1, colSecCodSec) = ""
                    .TextMatrix(.Rows - 1, colSecCodSub) = ""
                    .TextMatrix(.Rows - 1, colSecNomArea) = ""
                    .TextMatrix(.Rows - 1, colSecEsfuerzo) = ""
                    .TextMatrix(.Rows - 1, colSecFinicio) = ""
                    .TextMatrix(.Rows - 1, colSecFtermin) = ""
                Else
                    .TextMatrix(.Rows - 1, colSecCodDir) = ClearNull(aplRST!sec_cod_direccion)
                    .TextMatrix(.Rows - 1, colSecCodGer) = ClearNull(aplRST!sec_cod_gerencia)
                    .TextMatrix(.Rows - 1, colSecCodSec) = ClearNull(aplRST!sec_cod_sector)
                    .TextMatrix(.Rows - 1, colSecCodSub) = ClearNull(aplRST!sec_cod_grupo)
                    .TextMatrix(.Rows - 1, colSecEsfuerzo) = ClearNull(aplRST!sec_horaspresup)
                    .TextMatrix(.Rows - 1, colSecFinicio) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
                    .TextMatrix(.Rows - 1, colSecFtermin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
                    If secNivel = "GRUP" Then
                       .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector) & " � " & ClearNull(aplRST!sec_nom_grupo)
                    Else
                       .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector)
                    End If
                    If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                       .TextMatrix(.Rows - 1, colPrioEjecuc) = ClearNull(aplRST!prio_ejecuc)
                    End If
                End If
                .TextMatrix(.Rows - 1, colSRTPETIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_ini_plan")), Format(aplRST.Fields.item("fe_ini_plan"), "yyyymmdd"), "")
                .TextMatrix(.Rows - 1, colSRTPETFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_fin_plan")), Format(aplRST.Fields.item("fe_fin_plan"), "yyyymmdd"), "")
                .TextMatrix(.Rows - 1, colSRTSECIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_ini_plan")), Format(aplRST.Fields.item("sec_fe_ini_plan"), "yyyymmdd"), "")
                .TextMatrix(.Rows - 1, colSRTSECFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_fin_plan")), Format(aplRST.Fields.item("sec_fe_fin_plan"), "yyyymmdd"), "")
                .TextMatrix(.Rows - 1, colPetClass) = Trim(aplRST!cod_clase)
                .TextMatrix(.Rows - 1, colPetImpTech) = Trim(aplRST!pet_imptech)
                .TextMatrix(.Rows - 1, colPetSOx) = IIf(aplRST!pet_sox001 = 1, 1, 0)
                .TextMatrix(.Rows - 1, colRegulatorio) = IIf(IsNull(aplRST!pet_regulatorio), "-", ClearNull(aplRST!pet_regulatorio))
                .TextMatrix(.Rows - 1, colPetHist) = "H"    ' add -012- a.
                'PintarLinea grdDatos, prmGridFillRowColorBlue
                'FuncionesGenerales.CambiarForeColorLinea grdDatos, prmGridFillRowColorDarkGrey
                CambiarForeColorLinea grdDatos, prmGridFillRowColorDarkGreen2
            End With
            aplRST.MoveNext
        Loop
    End If
    '}
    '{ add -015- a.
    Dim cod_recurso(1 To 5) As String
    
    cod_recurso(1) = ""     ' Usuario
    cod_recurso(2) = ""     ' Solicitante
    cod_recurso(3) = ""     ' Referente
    cod_recurso(4) = ""     ' Supervisor
    cod_recurso(5) = ""     ' Director/Autorizante
    If cboUsuario.ListIndex > 0 Then
        cod_recurso(1) = CodigoCombo(cboUsuario, True)
    End If
    If cboSolicitante.ListIndex > 0 Then
        cod_recurso(2) = CodigoCombo(cboSolicitante, True)
    End If
    If cboReferente.ListIndex > 0 Then
        cod_recurso(3) = CodigoCombo(cboReferente, True)
    End If
    If cboSupervisor.ListIndex > 0 Then
        cod_recurso(4) = CodigoCombo(cboSupervisor, True)
    End If
    If cboDirector.ListIndex > 0 Then
        cod_recurso(5) = CodigoCombo(cboDirector, True)
    End If
    '{ del -015- a.
    'If Not sp_rptPeticion00(txtNroDesde.Text, txtNroHasta.Text, petTipo, petPrioridad, txtDESDEalta.DateValue, txtHASTAalta.DateValue, petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
    '                        CodigoCombo(Me.cboBpar, True), "N", prj_NroProyecto, prj_tipo, prj_titulo, prj_bpar, prj_Orientacion, prj_Importancia, prj_estado, prj_nivel, prj_area, prj_DESDEiniplan, prj_HASTAiniplan, prj_DESDEfinplan, prj_HASTAfinplan, prj_DESDEinireal, prj_HASTAinireal, prj_DESDEfinreal, prj_HASTAfinreal, CodigoCombo(cmbPetClass, True), UCase(Left(cmbPetImpTech.Text, 1)), UCase(Left(cmbPetDocu.Text, 1)), cFiltroRegulatorio, IIf(UCase(Left(cmbPetDocu, 1)) = "C", UCase(Left(cmbDocumentoAdjunto, 4)), Null), CodigoCombo(cmbTextos, True), Trim(txtMEM_Texto), CodigoCombo(cmbOpcionesTexto, True), cod_recurso(), txtFechaPedidoDesde, txtFechaPedidoHasta) Then   ' upd -008- a. Se agrega la variable-filtro 'cFiltroRegulatorio'    ' upd -013- a.
    '}
    '{ add -015- a.
    If Not sp_rptPeticion00(txtNroDesde.Text, txtNroHasta.Text, petTipo, petPrioridad, txtDESDEalta.DateValue, txtHASTAalta.DateValue, petNivel, petArea, petEstado, txtDESDEestado.DateValue, txtHASTAestado.DateValue, petSituacion, xAux, secNivel, secDire, secGere, secSect, secGrup, secEstado, secSituacion, txtDESDEiniplan.DateValue, txtHASTAiniplan.DateValue, txtDESDEfinplan.DateValue, txtHASTAfinplan.DateValue, txtDESDEinireal.DateValue, txtHASTAinireal.DateValue, txtDESDEfinreal.DateValue, txtHASTAfinreal.DateValue, auxAgrup, petImportancia, _
                            CodigoCombo(Me.cboBpar, True), CodigoCombo(cmbPetClass, True), UCase(Left(cmbPetImpTech.Text, 1)), UCase(Left(cmbPetDocu.Text, 1)), CodigoCombo(cmbRegulatorio, True), IIf(UCase(Left(cmbPetDocu, 1)) = "C", UCase(Left(cmbDocumentoAdjunto, 4)), Null), CodigoCombo(cmbTextos, True), Trim(txtMEM_Texto), CodigoCombo(cmbOpcionesTexto, True), cod_recurso(), txtFechaPedidoDesde, txtFechaPedidoHasta, txtPrjId, txtPrjSubId, txtPrjSubsId, CodigoCombo(cboEmp, True)) Then   ' upd -008- a. Se agrega la variable-filtro 'cFiltroRegulatorio'    ' upd -013- a.      ' upd -015- a. ' upd -019- a.
    '}
    '}
       GoTo finx
    End If
    lPeticiones_Listadas = lPeticiones_Listadas + aplRST.RecordCount   ' add -011- a.
    'grdDatos.Visible = False   ' del -012- a.
    'cmdVisualizar.Visible = False  ' add -009- a.  ' del -012- a.
    'lRecCount = 0   ' add -004- a.     ' del -012- b.
    'DoEvents       ' del -004- a.
    grdDatos.Visible = False        ' add
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            '{ add -004- a.
            If bGridOverflow Then
                Exit Do
            End If
            'lRecCount = lRecCount + 1
            DoEvents
            ' TODO ESTO BORRARLO
            '        .TextMatrix(.Rows - 1, 1) = padRight(ClearNull(aplRST!pet_nrointerno), 5)
            '        .TextMatrix(.Rows - 1, 2) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
            '        .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!nom_estado)
            '        .TextMatrix(.Rows - 1, 4) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd hh:mm:ss"), "")
            '        .TextMatrix(.Rows - 1, 5) = Trim(aplRST!cod_clase)
            '}
            .TextMatrix(.Rows - 1, colNroAsignado) = padRight(ClearNull(aplRST!pet_nroasignado), 5)
            .TextMatrix(.Rows - 1, colTipoPet) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!titulo)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!sec_nom_estado)
            .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!sec_nom_situacion)
            .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
            .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colEstadoFec) = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "yyyy-mm-dd"), "")
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
            .TextMatrix(.Rows - 1, colImportancia) = ClearNull(aplRST!importancia_nom)
            .TextMatrix(.Rows - 1, colCorpLocal) = ClearNull(aplRST!corp_local)
            .TextMatrix(.Rows - 1, colOrientacion) = ClearNull(aplRST!nom_orientacion)
            .TextMatrix(.Rows - 1, colSecSol) = ClearNull(aplRST!sol_sector)
            If flgNoHijos = True Then
                .TextMatrix(.Rows - 1, colSecCodDir) = ""
                .TextMatrix(.Rows - 1, colSecCodGer) = ""
                .TextMatrix(.Rows - 1, colSecCodSec) = ""
                .TextMatrix(.Rows - 1, colSecCodSub) = ""
                .TextMatrix(.Rows - 1, colSecNomArea) = ""
                .TextMatrix(.Rows - 1, colSecEsfuerzo) = ""
                .TextMatrix(.Rows - 1, colSecFinicio) = ""
                .TextMatrix(.Rows - 1, colSecFtermin) = ""
            Else
                .TextMatrix(.Rows - 1, colSecCodDir) = ClearNull(aplRST!sec_cod_direccion)
                .TextMatrix(.Rows - 1, colSecCodGer) = ClearNull(aplRST!sec_cod_gerencia)
                .TextMatrix(.Rows - 1, colSecCodSec) = ClearNull(aplRST!sec_cod_sector)
                .TextMatrix(.Rows - 1, colSecCodSub) = ClearNull(aplRST!sec_cod_grupo)
                .TextMatrix(.Rows - 1, colSecEsfuerzo) = ClearNull(aplRST!sec_horaspresup)
                .TextMatrix(.Rows - 1, colSecFinicio) = IIf(Not IsNull(aplRST!sec_fe_ini_plan), Format(aplRST!sec_fe_ini_plan, "yyyy-mm-dd"), "")
                .TextMatrix(.Rows - 1, colSecFtermin) = IIf(Not IsNull(aplRST!sec_fe_fin_plan), Format(aplRST!sec_fe_fin_plan, "yyyy-mm-dd"), "")
                If secNivel = "GRUP" Then
                   .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector) & " � " & ClearNull(aplRST!sec_nom_grupo)
                Else
                   .TextMatrix(.Rows - 1, colSecNomArea) = ClearNull(aplRST!sec_nom_sector)
                End If
                If secNivel = "GRUP" And (glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC") Then
                   .TextMatrix(.Rows - 1, colPrioEjecuc) = ClearNull(aplRST!prio_ejecuc)
                End If
            End If
            .TextMatrix(.Rows - 1, colSRTPETIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_ini_plan")), Format(aplRST.Fields.item("fe_ini_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colSRTPETFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("fe_fin_plan")), Format(aplRST.Fields.item("fe_fin_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colSRTSECIPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_ini_plan")), Format(aplRST.Fields.item("sec_fe_ini_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colSRTSECFPLAN) = "X" & IIf(Not IsNull(aplRST.Fields.item("sec_fe_fin_plan")), Format(aplRST.Fields.item("sec_fe_fin_plan"), "yyyymmdd"), "")
            .TextMatrix(.Rows - 1, colPetClass) = ClearNull(aplRST!cod_clase)
            .TextMatrix(.Rows - 1, colPetImpTech) = ClearNull(aplRST!pet_imptech)
            .TextMatrix(.Rows - 1, colPetSOx) = IIf(aplRST!pet_sox001 = 1, 1, 0)    ' add -003- a.
            .TextMatrix(.Rows - 1, colRegulatorio) = IIf(IsNull(aplRST!pet_regulatorio), "-", ClearNull(aplRST!pet_regulatorio))    ' add -008- a.
            .TextMatrix(.Rows - 1, colPetHist) = "L"                                ' add -012- a.
        End With
        aplRST.MoveNext
    Loop
    '{ add -002- a.
    Dim i As Long
    
    With grdDatos
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        For i = 0 To .Cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .Visible = True     ' add
    End With
    '}
    aplRST.Close
    'cmdVisualizar.Visible = True       ' add -009- a.  ' del -012- a.
finx:
    'grdDatos.Visible = True    ' del -012- a.
    Call Puntero(False)
    '{ add -011- a.
    If lPeticiones_Listadas > 0 Then
        Call Status("Listo. " & lPeticiones_Listadas & " peticiones listadas.")
    Else
    '}
        Call Status("Listo.")
    End If      ' add -011- a.
    LockProceso (False)
    cmdExportar.Visible = False
    '{ add -00x-
    'cmdImprimir.Visible = False
    'frmCrtPrn.Visible = False
    '}
    cmdChgBP.Enabled = False
    cmdAgrupar.Enabled = False
    cmdPrintPet.Enabled = False
    If grdDatos.Rows > 1 Then
        cmdExportar.Visible = True
        '{ add -00x-
        'cmdImprimir.Visible = True
        'frmCrtPrn.Visible = True
        '}
    End If
    LockProceso (False)
    '{ add -004- a.
    If bGridOverflow And bRegenerate Then
        cmdCerrar_Click
    End If
    '}
    '{ add -012- a.
    With grdDatos
        .RowSel = 1
        .Col = 1
        .Sort = flexSortStringNoCaseAscending
        'Call MostrarSeleccion
        .Visible = True
    End With
    cmdVisualizar.Visible = True
    '}
    
'{ add -004- a.
ErrHandler:
    Dim sErrMenssage As String
    Select Case Err.Number
        Case 30006      ' No queda memoria para seguir agregando filas al MSFlexGrid
            bGridOverflow = True
            sErrMenssage = "La consulta que intenta generar es demasiado grande (excede la cantidad de " & Trim(Format(lRecCount, "###,###,###")) & " filas)." & vbCrLf
            sErrMenssage = sErrMenssage & "�Desea volver a generar la consulta agregando restricciones para devolver una menor cantidad de filas?"
            If MsgBox(sErrMenssage, vbInformation + vbYesNo, "Consulta demasiado grande") = vbYes Then
                bRegenerate = True
                Resume Next
            Else
                bRegenerate = False
                MsgBox "IMPORTANTE" & vbCrLf & "La informaci�n mostrada en pantalla ser� parcial. No se ha podido cargar todo el conjunto de resultados esperado.", vbExclamation + vbOKOnly, "Resultado parcial"
                Me.Caption = Me.Tag & " (" & Trim(CStr(Format(lRecCount, "###,###,###"))) & " filas)"
                Resume Next
            End If
    End Select
'}
End Sub

Public Sub MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    cmdVisualizar.Enabled = False

    With grdDatos
        If .RowSel > 0 Then
            '.HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
                cmdVisualizar.Enabled = True
                If InPerfil("ADMI") Then
                    cmdChgBP.Enabled = True
                End If
                cmdAgrupar.Enabled = True
                cmdPrintPet.Enabled = True
            End If
        End If
    End With
    Call LockProceso(False)
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Public Sub grdDatos_Click()
    ''Debug.Print "CLICK1"
    'DoEvents
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
    End With
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_DblClick()
    DoEvents
    Call MostrarSeleccion
    If glNumeroPeticion <> "" Then
        If grdDatos.TextMatrix(grdDatos.RowSel, colPetHist) <> "H" Then    ' add -012- b.
            If Not LockProceso(True) Then
                Exit Sub
            End If
            glModoPeticion = "VIEW"
            On Error Resume Next
            frmPeticionesCarga.Show vbModal
        '{ add -012- b.
        Else
            MsgBox "La petici�n se encuentra en los archivos hist�ricos." & vbCrLf & "No puede trabajar desde aqu� con esta petici�n.", vbInformation + vbOKOnly, "Petici�n en hist�ricos"
        End If
        '}
    End If
End Sub

'{ add -006- a.
Private Sub txtNroDesde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(txtNroDesde) > 0 Then
        txtNroHasta.SetFocus
    End If
End Sub

Private Sub txtNroHasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And Len(txtNroHasta) > 0 Then
        cmdConfirmar.SetFocus
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then
        cmdCerrar_Click
        flgEnCarga = Not flgEnCarga
    End If
End Sub
'}

Private Sub txtNroDesde_LostFocus()
    If Val(txtNroDesde) > 100000 Then
        txtNroDesde = ""
        Call Status("N�mero inv�lido")
        Exit Sub
    End If
    txtNroHasta.Text = txtNroDesde.Text
End Sub

Private Sub txtNroHasta_LostFocus()
    If Val(txtNroHasta) > 100000 Then
        txtNroHasta = ""
        Call Status("N�mero inv�lido")
        Exit Sub
    End If
End Sub

Private Sub CrtPrn_Click(Index As Integer)
    Select Case Index
    Case 0
        mdiPrincipal.CrystalReport1.Destination = crptToWindow
    Case 1
        mdiPrincipal.CrystalReport1.Destination = crptToPrinter
    End Select
End Sub

'{ add -010- a.
Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub
'}

'{ add -014- b.
Private Sub optApertura_Click(Index As Integer)
    Select Case Index
        Case 0
            lblOpcionesApertura.Caption = "Nivel de apertura: Peticiones"
        Case 1
            lblOpcionesApertura.Caption = "Nivel de apertura: Sectores"
        Case 2
            lblOpcionesApertura.Caption = "Nivel de apertura: Grupos"
    End Select
    verNivelSec
End Sub
'}

'{ add -015- a.
Private Sub cmdMasFiltros_Click()
    If frmGral.Visible Then
        fraOtrosFiltros.Visible = True
        frmGral.Visible = False
        cmdMasFiltros.Caption = "Menos filtros..."
        flgQuery = "QRY2"
    Else
        fraOtrosFiltros.Visible = False
        frmGral.Visible = True
        cmdMasFiltros.Caption = "M�s filtros..."
        flgQuery = "QRY1"
    End If
End Sub
'}


