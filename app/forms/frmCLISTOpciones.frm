VERSION 5.00
Begin VB.Form frmCLISTOpciones 
   Caption         =   "Form1"
   ClientHeight    =   8370
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13785
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   8370
   ScaleWidth      =   13785
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraArchivosGenerados 
      Caption         =   " Archivos "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2655
      Left            =   9480
      TabIndex        =   39
      Top             =   2880
      Width           =   2055
      Begin VB.CheckBox chkFile 
         Caption         =   "FILE_PROD"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   43
         Top             =   360
         Width           =   1335
      End
      Begin VB.CheckBox chkFile 
         Caption         =   "FILE_DESA"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   42
         Top             =   600
         Width           =   1335
      End
      Begin VB.CheckBox chkFile 
         Caption         =   "FILE_OUT"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   41
         Top             =   840
         Width           =   1335
      End
      Begin VB.CheckBox chkFile 
         Caption         =   "FILE_INTER"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   40
         Top             =   1080
         Width           =   1455
      End
   End
   Begin VB.Frame fraFileOUT 
      Caption         =   " Archivo salida "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2655
      Left            =   7440
      TabIndex        =   33
      Top             =   2880
      Width           =   1935
      Begin VB.OptionButton opcOUTPrefijo 
         Caption         =   "ARBZ.ENM"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   38
         Top             =   360
         Width           =   1455
      End
      Begin VB.OptionButton opcOUTPrefijo 
         Caption         =   "ARBP.DYD"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   37
         Top             =   840
         Width           =   1455
      End
      Begin VB.OptionButton opcOUTPrefijo 
         Caption         =   "ARBZ.SEN"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004000&
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   36
         Top             =   1080
         Width           =   1455
      End
      Begin VB.OptionButton opcOUTPrefijo 
         Caption         =   "ARBP.ENM"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   35
         Top             =   600
         Width           =   1455
      End
      Begin VB.CheckBox chkIntermedio 
         Caption         =   "Intermedio"
         Height          =   195
         Left            =   600
         TabIndex        =   34
         Top             =   2280
         Width           =   1215
      End
   End
   Begin VB.Frame fraMailTexto 
      Caption         =   " Texto del email "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2655
      Left            =   120
      TabIndex        =   24
      Top             =   5640
      Width           =   6255
      Begin VB.TextBox txtMailTexto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   25
         Top             =   360
         Width           =   5895
      End
   End
   Begin VB.CommandButton cmdGenerar 
      Caption         =   "Generar"
      Height          =   495
      Left            =   12480
      TabIndex        =   23
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Frame fraEstadoSolicitud 
      Caption         =   " Estado solicitud "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2655
      Left            =   120
      TabIndex        =   15
      Top             =   2880
      Width           =   3855
      Begin VB.OptionButton opcEstado 
         Caption         =   "En espera (ejecución diferida)"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   22
         Top             =   1800
         Width           =   2535
      End
      Begin VB.OptionButton opcEstado 
         Caption         =   "En espera (Restore)"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   21
         Top             =   1560
         Width           =   2535
      End
      Begin VB.OptionButton opcEstado 
         Caption         =   "Enviado y finalizado"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   20
         Top             =   1320
         Width           =   2535
      End
      Begin VB.OptionButton opcEstado 
         Caption         =   "Aprobado y en proceso de envio"
         ForeColor       =   &H00808080&
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   3495
      End
      Begin VB.OptionButton opcEstado 
         Caption         =   "Aprobado"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   18
         Top             =   840
         Width           =   2535
      End
      Begin VB.OptionButton opcEstado 
         Caption         =   "Pendiente de aprobación N3"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   2535
      End
      Begin VB.OptionButton opcEstado 
         Caption         =   "Pendiente de aprobación N4"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   2535
      End
   End
   Begin VB.Frame fraEspecificacion_Mails 
      Caption         =   " Envia email "
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2655
      Left            =   4080
      TabIndex        =   10
      Top             =   2880
      Width           =   3255
      Begin VB.OptionButton opcMail 
         Caption         =   "Pedir RESTORE"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   56
         Top             =   1080
         Width           =   1575
      End
      Begin VB.CheckBox chkMailEnviaNotificacion 
         Caption         =   "Envia email de notificación"
         Height          =   255
         Left            =   480
         TabIndex        =   14
         Top             =   1560
         Width           =   2295
      End
      Begin VB.OptionButton opcMail 
         Caption         =   "No"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   735
      End
      Begin VB.OptionButton opcMail 
         Caption         =   "Autorización N3"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   840
         Width           =   1575
      End
      Begin VB.OptionButton opcMail 
         Caption         =   "Autorización N4"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Width           =   1815
      End
   End
   Begin VB.Frame fraEspecificacion 
      Caption         =   " 3. Especificación "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   13575
      Begin VB.Frame fraModo 
         Caption         =   " MODO "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   3960
         TabIndex        =   53
         Top             =   360
         Width           =   1815
         Begin VB.OptionButton opcModo 
            Caption         =   "EMErgencia"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   55
            Top             =   600
            Width           =   1215
         End
         Begin VB.OptionButton opcModo 
            Caption         =   "NORMAL"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   54
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame fraPerfil 
         Caption         =   " 1. Perfil "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   120
         TabIndex        =   49
         Top             =   360
         Width           =   1815
         Begin VB.OptionButton opcPerfil 
            Caption         =   "Analista"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   52
            Top             =   360
            Width           =   975
         End
         Begin VB.OptionButton opcPerfil 
            Caption         =   "Lider"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   51
            Top             =   600
            Width           =   975
         End
         Begin VB.OptionButton opcPerfil 
            Caption         =   "Supervisor"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   50
            Top             =   840
            Width           =   1095
         End
      End
      Begin VB.Frame fraTipo 
         Caption         =   " 2. Tipo solicitud "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   2040
         TabIndex        =   44
         Top             =   360
         Width           =   1815
         Begin VB.OptionButton opcTipo 
            Caption         =   "1. XCOM"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   48
            Top             =   360
            Width           =   1335
         End
         Begin VB.OptionButton opcTipo 
            Caption         =   "2. UNLO"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   47
            Top             =   600
            Width           =   1335
         End
         Begin VB.OptionButton opcTipo 
            Caption         =   "3. TCOR"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   46
            Top             =   840
            Width           =   1335
         End
         Begin VB.OptionButton opcTipo 
            Caption         =   "4. SORT"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   45
            Top             =   1080
            Width           =   1335
         End
      End
      Begin VB.Frame fraEspecificacion_DSN 
         Caption         =   " e. DSN con DS "
         Height          =   1455
         Left            =   11280
         TabIndex        =   26
         Top             =   360
         Width           =   1695
         Begin VB.OptionButton opcDS 
            Caption         =   "N/A"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   29
            Top             =   840
            Width           =   615
         End
         Begin VB.OptionButton opcDS 
            Caption         =   "Si"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   28
            Top             =   360
            Width           =   495
         End
         Begin VB.OptionButton opcDS 
            Caption         =   "No"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   27
            Top             =   600
            Width           =   495
         End
      End
      Begin VB.Frame fraEspecificacion_Enmascara 
         Caption         =   " b. ENMASCARAR "
         Height          =   1455
         Left            =   5880
         TabIndex        =   6
         Top             =   360
         Width           =   1695
         Begin VB.OptionButton opcENM 
            Caption         =   "N/A"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   32
            Top             =   840
            Width           =   735
         End
         Begin VB.OptionButton opcENM 
            Caption         =   "No"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   8
            Top             =   600
            Width           =   615
         End
         Begin VB.OptionButton opcENM 
            Caption         =   "Si"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   7
            Top             =   360
            Width           =   615
         End
      End
      Begin VB.Frame fraEspecificacion_Diferida 
         Caption         =   " d. DIFERIDA "
         Height          =   1455
         Left            =   9480
         TabIndex        =   3
         Top             =   360
         Width           =   1695
         Begin VB.OptionButton opcDIFERIDA 
            Caption         =   "N/A"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   30
            Top             =   840
            Width           =   615
         End
         Begin VB.OptionButton opcDIFERIDA 
            Caption         =   "No"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   9
            Top             =   600
            Width           =   495
         End
         Begin VB.OptionButton opcDIFERIDA 
            Caption         =   "Si"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   4
            Top             =   360
            Width           =   495
         End
      End
      Begin VB.Frame fraEspecificacion_Restore 
         Caption         =   " c. RESTORE "
         Height          =   1455
         Left            =   7680
         TabIndex        =   1
         Top             =   360
         Width           =   1695
         Begin VB.OptionButton opcRESTORE 
            Caption         =   "N/A"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   31
            Top             =   840
            Width           =   735
         End
         Begin VB.OptionButton opcRESTORE 
            Caption         =   "No"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   5
            Top             =   600
            Width           =   615
         End
         Begin VB.OptionButton opcRESTORE 
            Caption         =   "Si"
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   2
            Top             =   360
            Width           =   495
         End
      End
   End
End
Attribute VB_Name = "frmCLISTOpciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const SOLTIPO_XCOM = 1
Private Const SOLTIPO_UNLO = 2
Private Const SOLTIPO_TCOR = 3
Private Const SOLTIPO_SORT = 4

Private Const OPC_SI = 0
Private Const OPC_NO = 1
Private Const OPC_NA = 2

' Estados
Private Const ESTADO_PENDIENTE_APROBACION_N4 = 0
Private Const ESTADO_PENDIENTE_APROBACION_N3 = 1
Private Const ESTADO_APROBADO = 2
Private Const ESTADO_APROBADO_ENVIO = 3
Private Const ESTADO_ENVIADO_FINALIZADO = 4
Private Const ESTADO_ESPERA_RESTORE = 5
Private Const ESTADO_ESPERA_DIFERIDA = 6

' Mail
Private Const MAIL_NO = 0
Private Const MAIL_AUTORIZAR_N4 = 1
Private Const MAIL_AUTORIZAR_N3 = 2
Private Const MAIL_RESTORE = 3

' Prefijos
Private Const PREFIJOS_ARBZ_ENM = 0
Private Const PREFIJOS_ARBP_ENM = 1
Private Const PREFIJOS_ARBP_DYD = 2
Private Const PREFIJOS_ARBZ_SEN = 3

' Archivos
Private Const FILE_PROD = 0
Private Const FILE_DESA = 1
Private Const FILE_OUT = 2
Private Const FILE_INTER = 3

Dim SOLACTUAL_TIPO As Integer
Dim SOLACTAUL_PERFIL As String
Dim SOLACTAUL_MODO As String

Dim bEnmascarar As Boolean
Dim bRestore As Boolean
Dim bDiferida As Boolean
Dim bDatosSensibles As Boolean

Private Sub Form_Load()
    Call Inicializar
End Sub

Private Sub Inicializar()
    ' 1. Perfiles
    opcPerfil(0).Value = True
    opcPerfil(1).Value = False
    opcPerfil(2).Value = False
    
    ' 2. Tipo de solicitud
    opcTipo(SOLTIPO_XCOM - 1).Value = True
    opcTipo(SOLTIPO_UNLO - 1).Value = False
    opcTipo(SOLTIPO_TCOR - 1).Value = False
    opcTipo(SOLTIPO_SORT - 1).Value = False
    
    SOLACTUAL_TIPO = 1
    
    ' 3. Especificación
    ' a. Modo
    opcModo(OPC_SI).Value = True
    opcModo(OPC_NO).Value = False
    
    ' b. Enmascarar
    opcENM(OPC_SI).Value = True
    opcENM(OPC_NO).Value = False
    opcENM(OPC_NA).Value = False
    
    ' c. RESTORE
    opcRESTORE(OPC_SI).Value = False
    opcRESTORE(OPC_NO).Value = True
    opcRESTORE(OPC_NA).Value = False
    
    ' d. DIFERIDA
    opcDIFERIDA(OPC_SI).Value = True
    opcDIFERIDA(OPC_NO).Value = False
    opcDIFERIDA(OPC_NA).Value = False
    
    ' e. DS
    opcDS(OPC_SI).Value = True
    opcDS(OPC_NO).Value = False
    opcDS(OPC_NA).Value = False
End Sub

Private Sub opcDIFERIDA_Click(Index As Integer)
    Select Case Index
        Case 0: bDiferida = True
        Case Else
            bDiferida = False
    End Select
End Sub

Private Sub opcDS_Click(Index As Integer)
    Select Case Index
        Case 0: bDatosSensibles = True
        Case Else
            bDatosSensibles = False
    End Select
End Sub

Private Sub opcENM_Click(Index As Integer)
    Select Case Index
        Case 0: bEnmascarar = True
        Case Else
            bEnmascarar = False
    End Select
End Sub

Private Sub opcModo_Click(Index As Integer)
    Select Case Index
        Case 0: SOLACTAUL_MODO = "NOR"
        Case 1: SOLACTAUL_MODO = "EME"
    End Select
            
    Call EstablecerOpciones
End Sub

Private Sub EstablecerOpciones()
    Select Case SOLACTUAL_TIPO
        Case SOLTIPO_XCOM, SOLTIPO_UNLO, SOLTIPO_SORT
            Select Case SOLACTAUL_MODO
                Case "NOR"
                    ' b. Enmascaramiento
                    opcENM(OPC_SI).Value = True: opcENM(OPC_SI).Enabled = True
                    opcENM(OPC_NO).Value = False: opcENM(OPC_NO).Enabled = True
                    opcENM(OPC_NA).Value = False: opcENM(OPC_NA).Enabled = False
                    
                    ' c. Restore
                    opcRESTORE(OPC_SI).Value = False: opcRESTORE(OPC_SI).Enabled = True
                    opcRESTORE(OPC_NO).Value = True: opcRESTORE(OPC_NO).Enabled = True
                    opcRESTORE(OPC_NA).Value = False: opcRESTORE(OPC_NA).Enabled = False
                    
                    ' d. Diferido
                    opcDIFERIDA(OPC_SI).Value = False: opcDIFERIDA(OPC_SI).Enabled = True
                    opcDIFERIDA(OPC_NO).Value = True: opcDIFERIDA(OPC_NO).Enabled = True
                    opcDIFERIDA(OPC_NA).Value = False: opcDIFERIDA(OPC_NA).Enabled = False
                    
                    ' e. DS (datos sensibles)
                    opcDS(OPC_SI).Value = True: opcDS(OPC_SI).Enabled = True
                    opcDS(OPC_NO).Value = False: opcDS(OPC_NO).Enabled = True
                    opcDS(OPC_NA).Value = False: opcDS(OPC_NA).Enabled = False
                Case "EME"
                    ' b. Enmascaramiento
                    opcENM(OPC_SI).Value = False: opcENM(OPC_SI).Enabled = False
                    opcENM(OPC_NO).Value = False: opcENM(OPC_NO).Enabled = False
                    opcENM(OPC_NA).Value = True: opcENM(OPC_NA).Enabled = False
                    
                    ' c. Restore
                    opcRESTORE(OPC_SI).Value = True: opcRESTORE(OPC_SI).Enabled = True
                    opcRESTORE(OPC_NO).Value = False: opcRESTORE(OPC_NO).Enabled = True
                    opcRESTORE(OPC_NA).Value = False: opcRESTORE(OPC_NA).Enabled = False
                    
                    ' d. Diferido
                    opcDIFERIDA(OPC_SI).Value = False: opcDIFERIDA(OPC_SI).Enabled = False
                    opcDIFERIDA(OPC_NO).Value = False: opcDIFERIDA(OPC_NO).Enabled = False
                    opcDIFERIDA(OPC_NA).Value = True: opcDIFERIDA(OPC_NA).Enabled = False
                    
                    ' e. DS (datos sensibles)
                    opcDS(OPC_SI).Value = False: opcDS(OPC_SI).Enabled = False
                    opcDS(OPC_NO).Value = False: opcDS(OPC_NO).Enabled = False
                    opcDS(OPC_NA).Value = True: opcDS(OPC_NA).Enabled = False
            End Select
        Case SOLTIPO_TCOR
            Select Case SOLACTAUL_MODO
                Case "NOR"
                    ' b. Enmascaramiento
                    opcENM(OPC_SI).Value = True: opcENM(OPC_SI).Enabled = True
                    opcENM(OPC_NO).Value = False: opcENM(OPC_NO).Enabled = True
                    opcENM(OPC_NA).Value = False: opcENM(OPC_NA).Enabled = True
                    
                    ' c. Restore
                    opcRESTORE(OPC_SI).Value = False: opcRESTORE(OPC_SI).Enabled = False
                    opcRESTORE(OPC_NO).Value = False: opcRESTORE(OPC_NO).Enabled = False
                    opcRESTORE(OPC_NA).Value = True: opcRESTORE(OPC_NA).Enabled = False
                    
                    ' d. Diferido
                    opcDIFERIDA(OPC_SI).Value = False: opcDIFERIDA(OPC_SI).Enabled = True
                    opcDIFERIDA(OPC_NO).Value = False: opcDIFERIDA(OPC_NO).Enabled = True
                    opcDIFERIDA(OPC_NA).Value = True: opcDIFERIDA(OPC_NA).Enabled = True
                    
                Case "EME"
                    ' b. Enmascaramiento
                    opcENM(OPC_SI).Value = False: opcENM(OPC_SI).Enabled = False
                    opcENM(OPC_NO).Value = False: opcENM(OPC_NO).Enabled = False
                    opcENM(OPC_NA).Value = True: opcENM(OPC_NA).Enabled = False
                    
                    ' c. Restore
                    opcRESTORE(OPC_SI).Value = False: opcRESTORE(OPC_SI).Enabled = False
                    opcRESTORE(OPC_NO).Value = False: opcRESTORE(OPC_NO).Enabled = False
                    opcRESTORE(OPC_NA).Value = True: opcRESTORE(OPC_NA).Enabled = False
                    
                    ' d. Diferido
                    opcDIFERIDA(OPC_SI).Value = False: opcDIFERIDA(OPC_SI).Enabled = False
                    opcDIFERIDA(OPC_NO).Value = False: opcDIFERIDA(OPC_NO).Enabled = False
                    opcDIFERIDA(OPC_NA).Value = True: opcDIFERIDA(OPC_NA).Enabled = False
            End Select
    End Select
End Sub

' Perfiles
Private Sub opcPerfil_Click(Index As Integer)
    Select Case Index
        Case 0: SOLACTAUL_PERFIL = "ANAL"
        Case 1: SOLACTAUL_PERFIL = "CGRU"
        Case 2: SOLACTAUL_PERFIL = "CSEC"
    End Select
End Sub

Private Sub opcRESTORE_Click(Index As Integer)
    Select Case Index
        Case 0: bRestore = True
        Case Else
            bRestore = False
    End Select
End Sub

Private Sub opcTipo_Click(Index As Integer)
    SOLACTUAL_TIPO = Index + 1
End Sub

Private Sub Set_Estado(Estado As Integer)
    opcEstado(Estado).Value = True
End Sub

Private Sub Set_Mail(Mail As Integer)
    opcMail(Mail).Value = True
    ' Texto de mail
    txtMailTexto.Text = "Falta definir!"
End Sub

Private Sub Set_Prefijo(Prefijo)
    opcOUTPrefijo(Prefijo).Value = True
End Sub

Private Sub cmdGenerar_Click()
    Select Case SOLACTAUL_MODO
        Case "NOR"
            Select Case SOLACTUAL_TIPO
                Case SOLTIPO_XCOM, SOLTIPO_UNLO, SOLTIPO_SORT
                    If bEnmascarar Then
                        If bRestore Then
                            If bDiferida Then
                                If bDatosSensibles Then
                                    Call Set_Estado(ESTADO_ESPERA_RESTORE)
                                    Call Set_Mail(MAIL_RESTORE)
                                    Call Set_Prefijo(PREFIJOS_ARBP_ENM)
                                    ' Archivos
                                    chkFile(FILE_PROD).Value = 1
                                    chkFile(FILE_DESA).Value = 1
                                    chkFile(FILE_OUT).Value = 0
                                    chkFile(FILE_INTER).Value = 1
                                Else
                                    Call Set_Estado(ESTADO_ESPERA_DIFERIDA)
                                    Call Set_Mail(MAIL_RESTORE)
                                    Call Set_Prefijo(PREFIJOS_ARBP_DYD)
                                    ' Archivos
                                    chkFile(FILE_PROD).Value = 1
                                    chkFile(FILE_DESA).Value = 1
                                    chkFile(FILE_OUT).Value = 0
                                    chkFile(FILE_INTER).Value = 1
                                End If
                            Else
                                ' dddd
                                If bDatosSensibles Then
                                    Call Set_Estado(ESTADO_ESPERA_RESTORE)
                                    Call Set_Mail(MAIL_RESTORE)
                                    Call Set_Prefijo(PREFIJOS_ARBP_ENM)
                                    ' Archivos
                                    chkFile(FILE_PROD).Value = 1
                                    chkFile(FILE_DESA).Value = 1
                                    chkFile(FILE_OUT).Value = 0
                                    chkFile(FILE_INTER).Value = 0
                                Else
                                    Call Set_Estado(ESTADO_ENVIADO_FINALIZADO)
                                    Call Set_Mail(MAIL_RESTORE)
                                    Call Set_Prefijo(PREFIJOS_ARBZ_ENM)
                                    ' Archivos
                                    chkFile(FILE_PROD).Value = 1
                                    chkFile(FILE_DESA).Value = 1
                                    chkFile(FILE_OUT).Value = 0
                                    chkFile(FILE_INTER).Value = 0
                                End If
                            End If
                        Else
                            If bDiferida Then
                                If bDatosSensibles Then
                                    Call Set_Estado(ESTADO_ESPERA_DIFERIDA)
                                    Call Set_Mail(MAIL_NO)
                                    Call Set_Prefijo(PREFIJOS_ARBZ_ENM)
                                    ' Archivos
                                    chkFile(FILE_PROD).Value = 1
                                    chkFile(FILE_DESA).Value = 1
                                    chkFile(FILE_OUT).Value = 0
                                    chkFile(FILE_INTER).Value = 0
                                Else
                                    Call Set_Estado(ESTADO_APROBADO)
                                    Call Set_Mail(MAIL_NO)
                                    Call Set_Prefijo(PREFIJOS_ARBZ_ENM)
                                    ' Archivos
                                    chkFile(FILE_PROD).Value = 1
                                    chkFile(FILE_DESA).Value = 1
                                    chkFile(FILE_OUT).Value = 0
                                    chkFile(FILE_INTER).Value = 0
                                End If
                            Else
                                If bDatosSensibles Then
                                    opcEstado(2).Value = True
                                Else
                                    opcEstado(2).Value = True
                                End If
                            End If
                        End If
                    Else
                        If bRestore Then
                            If bDiferida Then
                            
                            Else
                            
                            End If
                        Else
                        
                        End If
                    End If
                Case SOLTIPO_TCOR
            End Select
        Case "EME"
            Select Case SOLACTUAL_TIPO
                Case SOLTIPO_XCOM, SOLTIPO_UNLO, SOLTIPO_TCOR
                Case SOLTIPO_SORT
            End Select
    End Select
End Sub
