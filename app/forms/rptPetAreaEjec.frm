VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form rptPetAreaEjec 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   6180
   ClientLeft      =   795
   ClientTop       =   2025
   ClientWidth     =   10335
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6180
   ScaleWidth      =   10335
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4725
      Left            =   0
      TabIndex        =   15
      Top             =   840
      Width           =   10320
      Begin AT_MaskText.MaskText txtPrjNom 
         Height          =   315
         Left            =   1320
         TabIndex        =   32
         Top             =   3300
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         Locked          =   -1  'True
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.Frame fraProyecto 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   1200
         TabIndex        =   34
         Top             =   3600
         Width           =   5415
         Begin VB.OptionButton optProyecto 
            Caption         =   "Proyecto y sus dependencias"
            Enabled         =   0   'False
            Height          =   195
            Index           =   2
            Left            =   1860
            TabIndex        =   36
            Top             =   180
            Width           =   2475
         End
         Begin VB.OptionButton optProyecto 
            Caption         =   "Solo este proyecto"
            Enabled         =   0   'False
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   35
            Top             =   180
            Value           =   -1  'True
            Width           =   1695
         End
      End
      Begin VB.CommandButton cmdEraseProyectos 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7080
         Picture         =   "rptPetAreaEjec.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto para asociar a la petici�n"
         Top             =   3300
         Width           =   375
      End
      Begin VB.CommandButton cmdClrAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7080
         Picture         =   "rptPetAreaEjec.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   28
         TabStop         =   0   'False
         ToolTipText     =   "Limpia agrupamiento"
         Top             =   2940
         Width           =   375
      End
      Begin VB.CommandButton cmdSelProyectos 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6720
         Picture         =   "rptPetAreaEjec.frx":0B14
         Style           =   1  'Graphical
         TabIndex        =   31
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto para asociar a la petici�n"
         Top             =   3300
         Width           =   375
      End
      Begin VB.CommandButton cmdAgrup 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6720
         Picture         =   "rptPetAreaEjec.frx":109E
         Style           =   1  'Graphical
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar agrupamiento para incluir informaci�n de pertenencia"
         Top             =   2940
         Width           =   375
      End
      Begin VB.ComboBox cmbHorasDetalle 
         Height          =   315
         ItemData        =   "rptPetAreaEjec.frx":1628
         Left            =   1320
         List            =   "rptPetAreaEjec.frx":162A
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   2580
         Width           =   3885
      End
      Begin VB.ComboBox cmbDetalle 
         Height          =   315
         ItemData        =   "rptPetAreaEjec.frx":162C
         Left            =   1320
         List            =   "rptPetAreaEjec.frx":1636
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1860
         Width           =   3885
      End
      Begin VB.ComboBox cmbPetClass 
         Height          =   315
         ItemData        =   "rptPetAreaEjec.frx":1682
         Left            =   1320
         List            =   "rptPetAreaEjec.frx":1684
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   2220
         Width           =   3885
      End
      Begin VB.ComboBox cboRecurso 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   780
         Width           =   3885
      End
      Begin VB.Frame fraNivel 
         Caption         =   " Nivel de detalle "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2685
         Left            =   7860
         TabIndex        =   20
         Top             =   900
         Width           =   2265
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sin agrupar"
            Height          =   225
            Index           =   0
            Left            =   240
            TabIndex        =   12
            Top             =   1740
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Recurso"
            Height          =   225
            Index           =   1
            Left            =   240
            TabIndex        =   11
            Top             =   1485
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Direcci�n"
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   8
            Top             =   420
            Width           =   1365
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Gerencia"
            Height          =   225
            Index           =   4
            Left            =   240
            TabIndex        =   9
            Top             =   705
            Width           =   1125
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Sector"
            Height          =   225
            Index           =   3
            Left            =   240
            TabIndex        =   10
            Top             =   960
            Width           =   1245
         End
         Begin VB.OptionButton OptNivel 
            Caption         =   "Grupo"
            Height          =   225
            Index           =   2
            Left            =   240
            TabIndex        =   0
            Top             =   1230
            Width           =   1365
         End
      End
      Begin VB.ComboBox cboEstructura 
         Height          =   315
         ItemData        =   "rptPetAreaEjec.frx":1686
         Left            =   1320
         List            =   "rptPetAreaEjec.frx":1688
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   420
         Width           =   8805
      End
      Begin AT_MaskText.MaskText txtDESDE 
         Height          =   315
         Left            =   1320
         TabIndex        =   4
         Top             =   1140
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHASTA 
         Height          =   315
         Left            =   1320
         TabIndex        =   5
         Top             =   1500
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAgrup 
         Height          =   315
         Left            =   1320
         TabIndex        =   33
         Top             =   2940
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   556
         MaxLength       =   50
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         Locked          =   -1  'True
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Filtrar"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   37
         Top             =   3760
         Width           =   420
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Proyecto"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   27
         Top             =   3360
         Width           =   645
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Agrupamiento"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   26
         Top             =   3000
         Width           =   1005
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Incluir"
         Height          =   195
         Left            =   180
         TabIndex        =   24
         Top             =   2640
         Width           =   435
      End
      Begin VB.Label lblNivDet 
         AutoSize        =   -1  'True
         Caption         =   "Nivel"
         Height          =   195
         Left            =   180
         TabIndex        =   23
         Top             =   1920
         Width           =   345
      End
      Begin VB.Label lblPetClass 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         Height          =   195
         Left            =   180
         TabIndex        =   22
         Top             =   2280
         Width           =   390
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Recurso"
         Height          =   195
         Left            =   180
         TabIndex        =   21
         Top             =   840
         Width           =   585
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   180
         TabIndex        =   19
         Top             =   1560
         Width           =   420
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Left            =   180
         TabIndex        =   18
         Top             =   1200
         Width           =   450
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Area"
         Height          =   195
         Left            =   180
         TabIndex        =   17
         Top             =   480
         Width           =   345
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   16
      Top             =   5520
      Width           =   10320
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   8340
         TabIndex        =   13
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   9300
         TabIndex        =   14
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label lblTitulo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "3. HORAS TRABAJADAS POR �REA EJECUTORA"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4365
   End
   Begin VB.Image imgFrame 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   840
      Left            =   0
      Picture         =   "rptPetAreaEjec.frx":168A
      Stretch         =   -1  'True
      Top             =   0
      Width           =   10320
   End
End
Attribute VB_Name = "rptPetAreaEjec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 06.07.2007 - Se agregan las opciones de resumir el informe por peticiones y filtrar la clase de las peticiones a informar.
' -001- b. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 25.09.2007 - Se agrega la clase SPUFIs al control Combobox.
' -003- a. FJS 15.05.2008 - Se cambia la asignaci�n de la propiedad 'ReportFileName' del control CrystalReport utilizando la funci�n de formato de ruta corto (8.3) porque por ejemplo, cuando el string tiene 127 caracteres da un error 20504 en runtime al momento de mostrar el reporte (da error de archivo de reporte no encontrado).
' -004- a. FJS 16.10.2008 - Se inhabilita la opci�n de seleccionar por clase cuando se pide con detalle.
' -005- a. FJS 15.04.2009 - Se cambian los s�mbolos para optimizar la visualizaci�n de los datos del combo.
' -006- a. FJS 03.09.2009 - Se restringe la carga del combobox solo para �reas habilitadas, recursos habiltados, etc.
' -006- b. FJS 07.09.2009 - Se corrige el �ltimo par�metro (esta mal pasado, los recursos activos son "A", no "S").
' -007- a. FJS 07.09.2009 - Se agrega una nueva opci�n para seleccionar si se desea reportear las horas cargadas a peticiones o a tareas o ambas.
' -008- a. FJS 05.05.2016 - Nuevo: se agregan opciones para filtrar por agrupamiento y por proyecto.

Option Explicit

Dim flgEnCarga As Boolean
Dim xNivel As Long
Dim vRetorno() As String
'Dim ProjId As Long, ProjSubId As Long, ProjSubSId As Long, projnom As String               ' add -008- a.
Dim ProjId As Variant, ProjSubId As Variant, ProjSubSId As Variant, projnom As String       ' add -008- a.
Dim sProyectoOpcion As String
Dim bSelProyecto As Boolean

Private Sub Form_Load()
    sProyectoOpcion = "S"
    bSelProyecto = False
    flgEnCarga = False
    Call InicializarCombos
    txtDESDE.text = Format(DateAdd("m", -1, date), "dd/mm/yyyy")
    txtHASTA.text = Format(date, "dd/mm/yyyy")
    OptNivel(4).value = True
End Sub

Private Sub InicializarCombos()
    Dim sDireccion As String
    Dim sGerencia As String
    
    flgEnCarga = True
    Call Puntero(True)
    Call Status("Inicializando...")
   
    If sp_GetDireccion("", "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                cboEstructura.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGerenciaXt("", "", "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboEstructura.AddItem sDireccion & " � " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||GERE|" & Trim(aplRST!cod_gerencia)
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & ESPACIOS & ESPACIOS & "||GERE|" & Trim(aplRST!cod_gerencia)   ' upd -005- a. Se cambia ">> " por "� "
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetSectorXt(Null, Null, Null, "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||SECT|" & Trim(aplRST!cod_sector)    ' upd -005- a. Se cambia ">> " por "� "
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboEstructura.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||SECT|" & Trim(aplRST!cod_sector)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGrupoXt("", "", "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboEstructura.AddItem Trim(aplRST!nom_direccion) & "� " & Trim(aplRST!nom_gerencia) & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||GRUP|" & Trim(aplRST!cod_grupo) ' upd -005- a. Se cambia ">> " por "� "
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboEstructura.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||GRUP|" & Trim(aplRST!cod_grupo)
            End If
            aplRST.MoveNext
        Loop
    End If
    cboEstructura.AddItem "Todo el BBVA" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL|NULL", 0
    cboEstructura.ListIndex = 0
    
    Call Initrecurso
    
    With cmbPetClass
        .Clear
        .AddItem "Todas" & ESPACIOS & "||NULL"
        .AddItem "NUEV: Nuevo desarrollo" & ESPACIOS & "||NUEV"
        .AddItem "EVOL: Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
        .AddItem "ATEN: Atenci�n a usuario" & ESPACIOS & "||ATEN"
        .AddItem "OPTI: Optimizaci�n" & ESPACIOS & "||OPTI"
        .AddItem "CORR: Correctivo" & ESPACIOS & "||CORR"
        .AddItem "SPUF: Spufies" & ESPACIOS & "||SPUF"
        .AddItem "SINC: Sin clase" & ESPACIOS & "||SINC"
        .AddItem String(200, "-") & "||NOT"
        .AddItem "Otros: NUEV+EVOL+ATEN" & ESPACIOS & "||NUEV/EVOL/ATEN/"
        .AddItem "Otros: OPTI+CORR+SPUF" & ESPACIOS & "||OPTI/CORR/SPUF/"
        .ListIndex = 0
    End With
    Call setHabilCtrl(cmbPetClass, "DIS")       ' Hasta que se arregle la consulta
    
    With cmbDetalle
        .Clear
        .AddItem "Detalle por peticiones / tareas" & ESPACIOS & "||C"
        .AddItem "Agrupado por clase de petici�n" & ESPACIOS & "||S"
        .ListIndex = 0
    End With
    
    With cmbHorasDetalle
        .Clear
        '.AddItem "Tareas y peticiones" & Space(100) & "||ALL"
        .AddItem "Todo" & ESPACIOS & "||ALL"
        .AddItem "Solo peticiones" & ESPACIOS & "||PET"
        .AddItem "Solo tareas" & ESPACIOS & "||TAR"
        .ListIndex = 0
    End With
    
    ' Inicializa el agrupamiento
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = ""
    
    ' Inicializa la selecci�n de proyectos
    ProjId = 0
    ProjSubId = 0
    ProjSubSId = 0
    projnom = ""
  
    flgEnCarga = False
    Call Status("Listo.")
    Call Puntero(False)
End Sub

'{ add -008- a.
Private Sub cmdAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = "VIEW"
    flgEnCarga = True
    frmAgrupSelect.Show vbModal
    flgEnCarga = False
    txtAgrup = ClearNull(Left(glSelectAgrupStr, 50))
    DoEvents
End Sub

Private Sub cmbDetalle_Click()
    If CodigoCombo(cmbDetalle, True) = "S" Then
        cmbHorasDetalle.ListIndex = PosicionCombo(cmbHorasDetalle, "PET", True)
        Call setHabilCtrl(cmbHorasDetalle, DISABLE)
    Else
        cmbHorasDetalle.ListIndex = PosicionCombo(cmbHorasDetalle, "ALL", True)
        Call setHabilCtrl(cmbHorasDetalle, NORMAL)
    End If
End Sub

Private Sub cmdClrAgrup_Click()
    If flgEnCarga = True Then Exit Sub
    glSelectAgrup = ""
    glSelectAgrupStr = ""
    glNumeroAgrup = ""
    glModoSelectAgrup = ""
    txtAgrup.text = glSelectAgrupStr
    DoEvents
End Sub

Private Sub cmdSelProyectos_Click()
    Load frmProyectosIDMSeleccion
    frmProyectosIDMSeleccion.Show 1
    With frmProyectosIDMSeleccion
        If .flgSeleccionado Then
            ProjId = .lProjId
            ProjSubId = .lProjSubId
            ProjSubSId = .lProjSubSId
            projnom = ClearNull(.cProjNom)
            txtPrjNom = .lProjId & "." & .lProjSubId & "." & .lProjSubSId & " - " & ClearNull(.cProjNom)
            bSelProyecto = True
            Call HabilitarFiltrosProyecto
        End If
    End With
End Sub

Private Sub cmdEraseProyectos_Click()
    ProjId = 0
    ProjSubId = 0
    ProjSubSId = 0
    projnom = ""
    txtPrjNom.text = ""
    bSelProyecto = False
    Call HabilitarFiltrosProyecto
End Sub

Private Sub HabilitarFiltrosProyecto()
    optProyecto(1).Enabled = bSelProyecto
    optProyecto(2).Enabled = bSelProyecto
    If Not bSelProyecto Then optProyecto(2).value = True
End Sub

Private Sub optProyecto_Click(Index As Integer)
    If bSelProyecto Then
        Select Case Index
            Case 1  ' Solo el proyecto
                sProyectoOpcion = "S"
            Case 2  ' Todas las dependencias
                sProyectoOpcion = "N"
        End Select
    End If
End Sub
'}

Private Sub cboEstructura_Click()
    'Dim vRetorno() As String       ' 2012.07.06
    Dim xNivel As String
    Dim flgRecurso As Boolean
    
    If flgEnCarga = True Then
        Exit Sub
    End If
        
    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
        MsgBox ("Error")
        Exit Sub
    End If
    Call Puntero(True)
    
    flgEnCarga = True
    flgRecurso = True
    xNivel = vRetorno(1)
    cboEstructura.ToolTipText = TextoCombo(cboEstructura)       ' add
    
    Select Case xNivel
        Case "NULL"
            OptNivel(0).Enabled = True
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = True
            OptNivel(5).Enabled = True
            OptNivel(4).value = True
        Case "DIRE"
            OptNivel(0).Enabled = True
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = True
            OptNivel(5).Enabled = True
            OptNivel(4).value = True
        Case "GERE"
            OptNivel(0).Enabled = True
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = True
            OptNivel(5).Enabled = False
            OptNivel(4).value = True
        Case "SECT"
            OptNivel(0).Enabled = True
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = True
            OptNivel(4).Enabled = False
            OptNivel(5).Enabled = False
            OptNivel(3).value = True
        Case "GRUP"
            OptNivel(0).Enabled = True
            OptNivel(1).Enabled = True
            OptNivel(2).Enabled = True
            OptNivel(3).Enabled = False
            OptNivel(4).Enabled = False
            OptNivel(5).Enabled = False
            OptNivel(2).value = True
    End Select
    Call Initrecurso
    Call Puntero(False)
    flgEnCarga = False
End Sub

Private Sub Initrecurso()
    'Dim vRetorno() As String       ' 2012.07.06
    Dim xNivel As String
    Dim flgRecurso As Boolean

    If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then
        MsgBox ("Error.")
        Exit Sub
    End If
    Call Puntero(True)

    flgRecurso = True
    xNivel = vRetorno(1)
    cboRecurso.Clear
    
    Select Case xNivel
        Case "NULL"
            If Not sp_GetRecurso(Null, "R", "A") Then
                flgRecurso = False
            End If
        Case "DIRE"
            If Not sp_GetRecurso(vRetorno(2), "DI", "A") Then
                flgRecurso = False
            End If
        Case "GERE"
            If Not sp_GetRecurso(vRetorno(2), "GE", "A") Then
                flgRecurso = False
            End If
        Case "SECT"
            If Not sp_GetRecurso(vRetorno(2), "GR", "A") Then
                flgRecurso = False
            End If
        Case "GRUP"
            If Not sp_GetRecurso(vRetorno(2), "SU", "A") Then
                flgRecurso = False
            End If
    End Select
    If flgRecurso Then
        Do While Not aplRST.EOF
            cboRecurso.AddItem StrConv(Trim(aplRST!nom_recurso), vbProperCase) & Space(150) & "||" & Trim(aplRST!cod_recurso)
            aplRST.MoveNext
        Loop
    End If
    cboRecurso.AddItem "Sin filtrar por recurso" & Space(150) & "||NULL", 0
    cboRecurso.ListIndex = 0
    Call Puntero(False)
End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdOK_Click()
    txtDESDE.ForceFormat
    txtHASTA.ForceFormat
    
    If Not IsDate(txtDESDE.DateValue) Then
        MsgBox "La fecha desde no es v�lida", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    If Not IsDate(txtHASTA.DateValue) Then
        MsgBox "La fecha hasta no es v�lida", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox "La fecha hasta no puede ser menor a la fecha desde", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        'Call ViejoReporte
    End If
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    Dim sTitulo(6) As String            ' Antes 3
    Dim sAuxiliar As String
    
    Call Puntero(True)
    If CodigoCombo(cmbDetalle, True) = "C" Then
        sReportName = "\hstraejec6.rpt"
        sTitulo(0) = "Horas trabajadas por �rea ejecutora"
    Else
        sReportName = "\hstraejecclass6.rpt"
        sTitulo(0) = "Horas trabajadas por �rea ejecutora y clase"
    End If
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    sTitulo(1) = "�rea ejecutora: " & ClearNull(TextoCombo(cboEstructura))
    sTitulo(2) = "Entre: " & txtDESDE & " y " & txtHASTA
    If Len(glSelectAgrupStr) > 0 Then
        sTitulo(3) = vbCrLf & "Agrupamiento: " & Trim(Mid(glSelectAgrupStr, 1, InStr(1, glSelectAgrupStr, "|", vbTextCompare) - 1)) & " (" & Trim(glSelectAgrup) & ")"
    Else
        sTitulo(3) = ""
    End If
    If ProjId > 0 Then
        sAuxiliar = " (" & CStr(ProjId) & "." & CStr(ProjSubId) & "." & CStr(ProjSubSId) & IIf(sProyectoOpcion = "N", " y dependencias)", ")")
        sTitulo(4) = IIf(Len(glSelectAgrupStr) > 0, vbTab & vbTab & "Proyecto: " & Trim(projnom), vbCrLf & "Proyecto: " & Trim(projnom)) & sAuxiliar
    Else
        sTitulo(4) = ""
    End If
    sTitulo(5) = "Nivel de detalle: " & OptNivel(xNivel).Caption
    
    With crReport
        .PaperSize = crPaperA4
        .ReportTitle = sTitulo(0)
        .ReportComments = sTitulo(1) & sTitulo(3) & sTitulo(4) & vbCrLf & sTitulo(2) & vbTab & vbTab & sTitulo(5)
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
    Call Puntero(True)
    
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fdesde": crParamDef.AddCurrentValue (Format(txtDESDE, "yyyymmdd"))
            Case "@fhasta": crParamDef.AddCurrentValue (Format(txtHASTA, "yyyymmdd"))
            Case "@nivel": crParamDef.AddCurrentValue (CStr(vRetorno(1)))
            Case "@area": crParamDef.AddCurrentValue (CStr(vRetorno(2)))
            Case "@recurso": crParamDef.AddCurrentValue (CodigoCombo(cboRecurso, True))
            Case "@detalle": crParamDef.AddCurrentValue (CStr(xNivel))
            Case "@cod_clase": crParamDef.AddCurrentValue (CodigoCombo(cmbPetClass, True))
            Case "@incluir": crParamDef.AddCurrentValue (CodigoCombo(cmbHorasDetalle, True))
            Case "@agr_nrointerno": crParamDef.AddCurrentValue (Val(glSelectAgrup))
            Case "@projid": crParamDef.AddCurrentValue (CLng(ProjId))
            Case "@projsubid": crParamDef.AddCurrentValue (CLng(ProjSubId))
            Case "@projsubsid": crParamDef.AddCurrentValue (CLng(ProjSubSId))
            Case "@tipoproj": crParamDef.AddCurrentValue (sProyectoOpcion)
        End Select
    Next
    ' F�rmulas del reporte
    With crReport
        .FormulaFields.GetItemByName("@@0_TITULO").text = Chr(34) & sTitulo(0) & Chr(34)
        .FormulaFields.GetItemByName("@@1_TITULO").text = Chr(34) & sTitulo(1) & Chr(34)
        .FormulaFields.GetItemByName("@@2_TITULO").text = Chr(34) & sTitulo(2) & Chr(34)
    End With
    
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    Call Puntero(False)

    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

'Private Sub ViejoReporte()
'    Dim txtAux As String
'    Dim sTitulo As String
'    Dim xRet As Integer
'    'Dim vRetorno() As String
'    Dim cPathFileNameReport As String   ' add -003- a.
'
'    If Not CamposObligatorios Then
'        Exit Sub
'    End If
'
'    With mdiPrincipal.CrystalReport1
'        Select Case CodigoCombo(cmbDetalle, True)
'            Case "C"
'                'If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then MsgBox ("Error")
'                cPathFileNameReport = App.Path & "\hstraejec.rpt"
'                .ReportFileName = GetShortName(cPathFileNameReport)
'                .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'                .SelectionFormula = ""
'                .RetrieveStoredProcParams
'                .StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
'                .StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
'                .StoredProcParam(2) = vRetorno(1)
'                .StoredProcParam(3) = vRetorno(2)
'                .StoredProcParam(4) = CodigoCombo(cboRecurso, True)
'                .StoredProcParam(5) = xNivel
'                .StoredProcParam(6) = CodigoCombo(cmbHorasDetalle, True)       ' add -007- a.
'                .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'                .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'                .Formulas(2) = "@0_TITULO='" & "Area Ejecutora: " & ClearNull(TextoCombo(cboEstructura)) & "'"
'                If CodigoCombo(cboRecurso, True) = "NULL" Then
'                    .Formulas(3) = "@1_TITULO=''"
'                Else
'                    .Formulas(3) = "@1_TITULO='" & "Para el recurso: " & ClearNull(ReplaceApostrofo(TextoCombo(cboRecurso))) & "'"   ' add -006- a. Se agrega la funci�n ReplaceApostrofo()
'                End If
'                .Formulas(4) = "@2_TITULO='" & "Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy")) & "'"
''                ' Esto es para debuggear en DESARROLLO
''                Debug.Print "sp_rptHsTrabAreaEjec " & _
''                            "'" & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")) & "', " _
''                            ; "'" & IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")) & "', " & _
''                            "'" & vRetorno(1) & "', " & _
''                            "'" & vRetorno(2) & "', " & _
''                            "'" & CodigoCombo(cboRecurso, True) & "', " & _
''                            "'" & xNivel & "', " & _
''                            "'" & CodigoCombo(cmbHorasDetalle, True) & "'"
'                .WindowLeft = 1
'                .WindowTop = 1
'                .WindowState = crptMaximized
'                .Action = 1
'                .Formulas(2) = ""
'                .Formulas(3) = ""
'                .Formulas(4) = ""
'            '{ add -001- a.
'            Case "S"
'                'If ParseString(vRetorno, DatosCombo(cboEstructura), "|") = 0 Then MsgBox ("Error")
'                cPathFileNameReport = App.Path & "\hstraejecclass.rpt"
'                .ReportFileName = GetShortName(cPathFileNameReport)
'                .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'                .SelectionFormula = ""
'                .RetrieveStoredProcParams
'                .StoredProcParam(0) = IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd"))
'                .StoredProcParam(1) = IIf(IsNull(txtHASTA.DateValue), Format(date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd"))
'                .StoredProcParam(2) = vRetorno(1)
'                .StoredProcParam(3) = vRetorno(2)
'                .StoredProcParam(4) = CodigoCombo(cboRecurso, True)
'                .StoredProcParam(5) = xNivel
'                .StoredProcParam(6) = CodigoCombo(cmbPetClass, True)            ' add -001- a.
'                .StoredProcParam(7) = CodigoCombo(cmbHorasDetalle, True)        ' add -007- a.
'                .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'                .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'                .Formulas(2) = "@0_TITULO='" & "Area Ejecutora: " & ClearNull(TextoCombo(cboEstructura)) & "'"
'                If CodigoCombo(cboRecurso, True) = "NULL" Then
'                    .Formulas(3) = "@1_TITULO=''"
'                Else
'                    .Formulas(3) = "@1_TITULO='" & "Para el recurso: " & ClearNull(TextoCombo(cboRecurso)) & "'"
'                End If
'                .Formulas(4) = "@2_TITULO='" & "Entre: " & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, date), "dd/mm/yyyy"), Format(txtDESDE.DateValue, "dd/mm/yyyy")) & "  y " & IIf(IsNull(txtHASTA.DateValue), Format(date, "dd/mm/yyyy"), Format(txtHASTA.DateValue, "dd/mm/yyyy")) & "'"
'                ' Esto es para debuggear en DESARROLLO
'                'Debug.Print "sp_rptHsTrabAreaEjec2 " & _
'                '            "'" & IIf(IsNull(txtDESDE.DateValue), Format(DateAdd("m", -1, Date), "yyyymmdd"), Format(txtDESDE.DateValue, "yyyymmdd")) & "', " _
'                '            ; "'" & IIf(IsNull(txtHASTA.DateValue), Format(Date, "yyyymmdd"), Format(txtHASTA.DateValue, "yyyymmdd")) & "', " & _
'                '            "'" & vRetorno(1) & "', " & _
'                '            "'" & vRetorno(2) & "', " & _
'                '            "'" & CodigoCombo(cboRecurso, True) & "', " & _
'                '            "'" & xNivel & "', " & _
'                '            "'" & CodigoCombo(cmbPetClass, True) & "'"
'                .WindowLeft = 1
'                .WindowTop = 1
'                .WindowState = crptMaximized
'                .Action = 1
'                .Formulas(2) = ""
'                .Formulas(3) = ""
'                .Formulas(4) = ""
'            '}
'        End Select
'    End With
'End Sub

Private Sub OptNivel_Click(Index As Integer)
    xNivel = Index
    Select Case xNivel
        Case 0, 1
            Call setHabilCtrl(cboRecurso, "NOR")
        Case Else
            Call setHabilCtrl(cboRecurso, "DIS")
            If cboRecurso.ListCount > 0 Then
                cboRecurso.ListIndex = 0
            End If
    End Select
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If txtDESDE = "" Then
        MsgBox ("Falta Fecha inicio informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA = "" Then
        MsgBox ("Falta Fecha fin informe")
        CamposObligatorios = False
        Exit Function
    End If
    If txtHASTA.DateValue < txtDESDE.DateValue Then
        MsgBox ("Fecha Hasta menor que Fecha Desde")
        CamposObligatorios = False
        Exit Function
    End If
End Function

''{ add -004- a.
'Private Sub cmbDetalle_Click()
'    If Left(cmbDetalle.Text, 1) = "C" Then
'        cmbPetClass.ListIndex = 0
'        Call setHabilCtrl(cmbPetClass, "DIS")
'        Call setHabilCtrl(cmbHorasDetalle, "NOR")
'    Else
'        Call setHabilCtrl(cmbPetClass, "NOR")
'        cmbHorasDetalle.ListIndex = 0
'        Call setHabilCtrl(cmbHorasDetalle, "DIS")
'    End If
'End Sub
''}
