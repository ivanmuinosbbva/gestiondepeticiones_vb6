VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConfiguracion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraciones"
   ClientHeight    =   7425
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   11280
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   11280
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab SSTab1 
      Height          =   7215
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   12726
      _Version        =   393216
      Style           =   1
      Tabs            =   6
      Tab             =   5
      TabsPerRow      =   10
      TabHeight       =   520
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmConfiguracion.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Command1"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Solicitudes de transmisi�n"
      TabPicture(1)   =   "frmConfiguracion.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraSolicitudes"
      Tab(1).Control(1)=   "fraSolicitudesOtros"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Otros"
      TabPicture(2)   =   "frmConfiguracion.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "Peticiones"
      TabPicture(3)   =   "frmConfiguracion.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "chkPet(0)"
      Tab(3).Control(1)=   "chkPet(1)"
      Tab(3).Control(2)=   "chkPet(2)"
      Tab(3).Control(3)=   "chkPet(3)"
      Tab(3).Control(4)=   "chkPet(4)"
      Tab(3).Control(5)=   "chkPet(5)"
      Tab(3).Control(6)=   "chkPet(6)"
      Tab(3).Control(7)=   "chkPet(7)"
      Tab(3).Control(8)=   "chkPet(8)"
      Tab(3).Control(9)=   "chkPet(9)"
      Tab(3).Control(10)=   "chkPet(10)"
      Tab(3).Control(11)=   "chkPet(11)"
      Tab(3).Control(12)=   "chkPet(12)"
      Tab(3).Control(13)=   "chkPet(13)"
      Tab(3).Control(14)=   "chkPet(14)"
      Tab(3).Control(15)=   "chkPet(15)"
      Tab(3).Control(16)=   "chkPet(16)"
      Tab(3).Control(17)=   "chkPet(17)"
      Tab(3).Control(18)=   "chkPet(18)"
      Tab(3).Control(19)=   "chkPet(19)"
      Tab(3).Control(20)=   "chkPet(20)"
      Tab(3).Control(21)=   "chkPet(21)"
      Tab(3).Control(22)=   "chkPet(22)"
      Tab(3).Control(23)=   "chkPet(23)"
      Tab(3).Control(24)=   "chkPet(24)"
      Tab(3).Control(25)=   "chkPet(25)"
      Tab(3).Control(26)=   "chkPet(26)"
      Tab(3).ControlCount=   27
      TabCaption(4)   =   "Feriados"
      TabPicture(4)   =   "frmConfiguracion.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "MonthView2"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Planificaci�n"
      TabPicture(5)   =   "frmConfiguracion.frx":008C
      Tab(5).ControlEnabled=   -1  'True
      Tab(5).Control(0)=   "lblPlanificacion(0)"
      Tab(5).Control(0).Enabled=   0   'False
      Tab(5).Control(1)=   "lblPlanificacion(1)"
      Tab(5).Control(1).Enabled=   0   'False
      Tab(5).Control(2)=   "lblPlanificacion(2)"
      Tab(5).Control(2).Enabled=   0   'False
      Tab(5).Control(3)=   "DTPicker1"
      Tab(5).Control(3).Enabled=   0   'False
      Tab(5).Control(4)=   "DTPicker2"
      Tab(5).Control(4).Enabled=   0   'False
      Tab(5).Control(5)=   "cboTipoPlanificacion"
      Tab(5).Control(5).Enabled=   0   'False
      Tab(5).ControlCount=   6
      Begin VB.ComboBox cboTipoPlanificacion 
         Height          =   315
         Left            =   2520
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   1080
         Width           =   2595
      End
      Begin MSComCtl2.MonthView MonthView2 
         Height          =   6660
         Left            =   -74880
         TabIndex        =   49
         Top             =   480
         Width           =   7845
         _ExtentX        =   13838
         _ExtentY        =   11748
         _Version        =   393216
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         Appearance      =   1
         MonthColumns    =   3
         MonthRows       =   3
         StartOfWeek     =   62521345
         CurrentDate     =   42367
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Relaciones con otros pedidos"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   26
         Left            =   -71880
         TabIndex        =   48
         Top             =   2520
         Width           =   2895
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   25
         Left            =   -71880
         TabIndex        =   47
         Top             =   2280
         Width           =   2295
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Motivos"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   24
         Left            =   -71880
         TabIndex        =   46
         Top             =   2040
         Width           =   2295
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Beneficios esperados"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   23
         Left            =   -71880
         TabIndex        =   45
         Top             =   1800
         Width           =   2295
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Descripci�n ampliada"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   22
         Left            =   -71880
         TabIndex        =   44
         Top             =   1560
         Width           =   2295
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Caracter�sticas"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   21
         Left            =   -71880
         TabIndex        =   43
         Top             =   1320
         Width           =   2295
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Beneficios esperados"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   20
         Left            =   -71880
         TabIndex        =   42
         Top             =   1080
         Width           =   2295
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Autorizante"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   19
         Left            =   -74280
         TabIndex        =   41
         Top             =   5640
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Supervisor"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   -74280
         TabIndex        =   40
         Top             =   5400
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Referente"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   -74280
         TabIndex        =   39
         Top             =   5160
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Solicitante"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   -74280
         TabIndex        =   38
         Top             =   4920
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Sector solicitante"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   -74280
         TabIndex        =   37
         Top             =   4680
         Width           =   2295
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Fecha BP"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   -74280
         TabIndex        =   36
         Top             =   4440
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Fecha requerida"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   13
         Left            =   -74280
         TabIndex        =   35
         Top             =   4200
         Width           =   1695
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "BP"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   -74280
         TabIndex        =   34
         Top             =   3960
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Riesgo Operacional"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   11
         Left            =   -74280
         TabIndex        =   33
         Top             =   3720
         Width           =   1935
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Orientaci�n"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   -74280
         TabIndex        =   32
         Top             =   3480
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Corp. / Local"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   -74280
         TabIndex        =   31
         Top             =   3240
         Width           =   1575
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Proyecto"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   -74280
         TabIndex        =   30
         Top             =   3000
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Importancia"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   -74280
         TabIndex        =   29
         Top             =   2760
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Empresa"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   -74280
         TabIndex        =   28
         Top             =   2520
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   -74280
         TabIndex        =   27
         Top             =   2280
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Regulatorio"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   -74280
         TabIndex        =   26
         Top             =   2040
         Width           =   1335
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Impacto tecnol�gico"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   -74280
         TabIndex        =   25
         Top             =   1800
         Width           =   2055
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Clase"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   -74280
         TabIndex        =   24
         Top             =   1560
         Width           =   855
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   -74280
         TabIndex        =   23
         Top             =   1320
         Width           =   855
      End
      Begin VB.CheckBox chkPet 
         Caption         =   "T�tulo"
         BeginProperty Font 
            Name            =   "Consolas"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   -74280
         TabIndex        =   22
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   735
         Left            =   -68280
         TabIndex        =   21
         Top             =   3060
         Width           =   1695
      End
      Begin VB.Frame fraSolicitudesOtros 
         Caption         =   " Otros "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00D28D55&
         Height          =   2535
         Left            =   -74880
         TabIndex        =   19
         Top             =   3540
         Width           =   8055
         Begin VB.CheckBox chkControlDuplicidad 
            Caption         =   "Habilitar el control de duplicidad en la generaci�n de solicitudes"
            Height          =   255
            Left            =   240
            TabIndex        =   20
            ToolTipText     =   "Si esta habilitado, controla que no se pida m�s de una vez el mismo archivo (para evitar que se pisen los mismos)"
            Top             =   360
            Width           =   4935
         End
      End
      Begin VB.Frame fraSolicitudes 
         Caption         =   "Solicitudes de Producci�n a Desarrollo"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00D28D55&
         Height          =   2655
         Left            =   -74880
         TabIndex        =   5
         Top             =   780
         Width           =   8055
         Begin VB.CheckBox chkMostrarGuiaSolicitudes 
            Caption         =   "Mostrar la gu�a r�pida para solicitudes al generar una solicitud"
            Height          =   255
            Left            =   120
            TabIndex        =   18
            Top             =   1920
            Width           =   4815
         End
         Begin VB.CheckBox chkMostrarGuiaCatalogo 
            Caption         =   "Mostrar la gu�a r�pida para solicitudes al trabajar con cat�logos"
            Height          =   255
            Left            =   120
            TabIndex        =   17
            Top             =   2160
            Width           =   4935
         End
         Begin VB.TextBox txtDiasFin 
            Alignment       =   2  'Center
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   1
            EndProperty
            Height          =   285
            Left            =   2400
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   10
            Top             =   1080
            Width           =   495
         End
         Begin VB.CheckBox chkSoli_inhabilitadoFin 
            Caption         =   "Inhabilitado los �ltimos"
            Height          =   255
            Left            =   480
            TabIndex        =   9
            Top             =   1080
            Width           =   1935
         End
         Begin VB.CheckBox chkSoli_inhabilitadoIni 
            Caption         =   "Inhabilitado los primeros"
            Height          =   255
            Left            =   480
            TabIndex        =   8
            Top             =   720
            Width           =   2055
         End
         Begin VB.TextBox txtDiasInicio 
            Alignment       =   2  'Center
            BeginProperty DataFormat 
               Type            =   1
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   1
            EndProperty
            Height          =   285
            Left            =   2520
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   7
            Top             =   720
            Width           =   480
         End
         Begin VB.CheckBox chkSoli_Habilitado 
            Caption         =   "Habilitado"
            Height          =   255
            Left            =   240
            TabIndex        =   6
            Top             =   360
            Width           =   1095
         End
         Begin MSComCtl2.UpDown udInicio 
            Height          =   285
            Left            =   3000
            TabIndex        =   11
            Top             =   705
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   503
            _Version        =   393216
            Value           =   1
            BuddyControl    =   "txtDiasInicio"
            BuddyDispid     =   196620
            OrigLeft        =   3000
            OrigTop         =   720
            OrigRight       =   3255
            OrigBottom      =   975
            Max             =   31
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin MSComCtl2.UpDown udFinal 
            Height          =   285
            Left            =   2880
            TabIndex        =   12
            Top             =   1080
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   503
            _Version        =   393216
            Value           =   1
            BuddyControl    =   "txtDiasFin"
            BuddyDispid     =   196617
            OrigLeft        =   2880
            OrigTop         =   1080
            OrigRight       =   3135
            OrigBottom      =   1335
            Max             =   31
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.Label lblConfiguracionActualSolicitudes 
            Caption         =   "lblConfiguracionActualSolicitudes"
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   2040
            TabIndex        =   16
            Top             =   1560
            Width           =   5745
         End
         Begin VB.Label lblSolicitudes 
            AutoSize        =   -1  'True
            Caption         =   "Configuraci�n actual:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   15
            Top             =   1560
            Width           =   1770
         End
         Begin VB.Label lblSolicitudes 
            AutoSize        =   -1  'True
            Caption         =   "d�as h�biles desde fin de mes"
            Height          =   195
            Index           =   0
            Left            =   3240
            TabIndex        =   14
            Top             =   1110
            Width           =   2085
         End
         Begin VB.Label lblSolicitudes 
            AutoSize        =   -1  'True
            Caption         =   "d�as h�biles al inicio de mes"
            Height          =   195
            Index           =   1
            Left            =   3360
            TabIndex        =   13
            Top             =   750
            Width           =   1935
         End
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   315
         Left            =   2520
         TabIndex        =   54
         Top             =   1440
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         Format          =   62521345
         CurrentDate     =   42751
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   2520
         TabIndex        =   55
         Top             =   720
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   556
         _Version        =   393216
         Format          =   62521345
         CurrentDate     =   42751
      End
      Begin VB.Label lblPlanificacion 
         AutoSize        =   -1  'True
         Caption         =   "Pr�ximo comienzo de per�odo"
         Height          =   195
         Index           =   2
         Left            =   300
         TabIndex        =   53
         Top             =   1500
         Width           =   2085
      End
      Begin VB.Label lblPlanificacion 
         AutoSize        =   -1  'True
         Caption         =   "Frecuencia"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   52
         Top             =   1140
         Width           =   780
      End
      Begin VB.Label lblPlanificacion 
         AutoSize        =   -1  'True
         Caption         =   "Inicio general del proceso"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   51
         Top             =   780
         Width           =   1830
      End
   End
   Begin VB.Frame pnlBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7395
      Left            =   9960
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Enabled         =   0   'False
         Height          =   500
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   600
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Height          =   500
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la direcci�n selecionada"
         Top             =   120
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   6840
         Width           =   1170
      End
   End
End
Attribute VB_Name = "frmConfiguracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim FechaInicio As Date         ' Principio de mes
Dim FechaFinal As Date          ' �ltimo d�a del mes
Dim Fe_Ini As Date              ' Principio de mes +/- d�as parametrizados
Dim fe_fin As Date              ' �ltimo d�a del mes +/- d�as parametrizados
Dim bCargando As Boolean
Dim vFecha() As Date            ' Se usa para guardar los feriados del per�odo
Dim bFeriados As Boolean        ' Flag para indicar existencia de feriados en el per�odo

Private Sub Check1_Click()

End Sub

Private Sub Form_Load()
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Me.Left = 0
    Call Evaluar
End Sub

Private Sub InicializarPantalla()
    Dim i As Integer
    
    Me.Tag = ""
    lblConfiguracionActualSolicitudes = ""
    bCargando = True
    
    FechaInicio = CDate("01/" & Month(Now) & "/" & Year(Now))
    FechaFinal = DateAdd("d", -1, DateAdd("M", 1, FechaInicio))
    
    With cboTipoPlanificacion
        .Clear
        .AddItem "Semanal" & ESPACIOS & "||Z"
        .AddItem "Quincenal" & ESPACIOS & "||Q"
        .AddItem "Mensual" & ESPACIOS & "||M"
        .AddItem "Bimestral" & ESPACIOS & "||B"
        .AddItem "Trimestral" & ESPACIOS & "||T"
        .AddItem "Cuatrimestral" & ESPACIOS & "||C"
        .AddItem "Semestral" & ESPACIOS & "||S"
        .AddItem "Anual" & ESPACIOS & "||A"
        .AddItem "Personalizado" & ESPACIOS & "||X"
        .ListIndex = -1
    End With
    
    chkSoli_inhabilitadoIni.Enabled = False
    chkSoli_inhabilitadoFin.Enabled = False
    'txtDiasInicio.Enabled = False
    'txtDiasFin.Enabled = False
    lblSolicitudes(0).Enabled = False
    lblSolicitudes(1).Enabled = False
    'udInicio.Enabled = False
    'udFinal.Enabled = False
    
    Call setHabilCtrl(txtDiasInicio, "DIS")
    Call setHabilCtrl(txtDiasFin, "DIS")
    Call setHabilCtrl(udInicio, "DIS")
    Call setHabilCtrl(udFinal, "DIS")
    

    If sp_GetVarios("CLIST000") Then
        chkSoli_Habilitado.value = ClearNull(aplRST.Fields!var_numero)
    End If
    
    If sp_GetVarios("CLIST001") Then
        chkSoli_inhabilitadoIni.value = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, 1, 0)
        txtDiasInicio = ClearNull(aplRST.Fields!var_numero)
    End If
    
    If sp_GetVarios("CLIST002") Then
        chkSoli_inhabilitadoFin.value = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, 1, 0)
        txtDiasFin = ClearNull(aplRST.Fields!var_numero)
    End If
    
    ' Mostrar la gu�a r�pida para solicitudes al generar una solicitud
    If sp_GetVarios("CLIST010") Then
        chkMostrarGuiaSolicitudes.value = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, 1, 0)
    End If
    ' Mostrar la gu�a r�pida para solicitudes al trabajar con cat�logos
    If sp_GetVarios("CLIST011") Then
        chkMostrarGuiaCatalogo.value = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, 1, 0)
    End If
    
    i = 0
    bFeriados = False
    If sp_GetFeriado(FechaInicio, FechaFinal) Then
        Do While Not aplRST.EOF
            ReDim Preserve vFecha(i)
            bFeriados = True
            vFecha(i) = aplRST.Fields!fecha
            aplRST.MoveNext
            i = i + 1
            DoEvents
        Loop
    End If
    
    ' Otros
    If sp_GetVarios("CLIST012") Then
        chkControlDuplicidad.value = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, 1, 0)
    End If

    fraSolicitudes.Enabled = False
    fraSolicitudesOtros.Enabled = False
    cmdConfirmar.Enabled = False
    If Not bCargando Then Call Evaluar
    bCargando = False
End Sub

Private Sub cmdModificar_Click()
    fraSolicitudes.Enabled = True
    fraSolicitudesOtros.Enabled = True
    
    cmdModificar.Enabled = False
    cmdConfirmar.Enabled = True
    cmdCerrar.Caption = "Cancelar"
End Sub

Private Sub cmdConfirmar_Click()
    Call Puntero(True)
    Call Status("Guardando configuraci�n...")
    Call sp_UpdateVarios("CLIST000", chkSoli_Habilitado.value, "Carga de solicitudes", Format(Now, "dd/MM/yyyy"))
    Call sp_UpdateVarios("CLIST001", Val(ClearNull(txtDiasInicio)), "D�as hacia adelante", Format(Now, "dd/MM/yyyy"))
    Call sp_UpdateVarios("CLIST002", Val(ClearNull(txtDiasFin)), "D�as hacia atr�s", Format(Now, "dd/MM/yyyy"))
    
    Call sp_UpdateVarios("CLIST010", chkMostrarGuiaSolicitudes.value, "Habilita guia rapida Solicitud", Format(Now, "dd/MM/yyyy"))
    Call sp_UpdateVarios("CLIST011", chkMostrarGuiaCatalogo.value, "Habilita guia rapida Cat�logo", Format(Now, "dd/MM/yyyy"))
    
    ' Otros
    Call sp_UpdateVarios("CLIST012", chkControlDuplicidad.value, "Habilita control de duplicidad en los pedidos", Format(Now, "dd/MM/yyyy"))
    
    fraSolicitudes.Enabled = False
    fraSolicitudesOtros.Enabled = False
    cmdModificar.Enabled = True
    cmdConfirmar.Enabled = False
    cmdCerrar.Caption = "Cerrar"
    Call Status("Listo.")
    Call Puntero(False)
End Sub

Private Sub udInicio_Change()
    If Fe_Ini >= fe_fin Then
        txtDiasInicio = Val(txtDiasInicio) - 1
        Call Evaluar
    End If
End Sub

Private Sub udFinal_Change()
    If fe_fin <= Fe_Ini Then
        txtDiasFin = Val(txtDiasFin) - 1
        Call Evaluar
    End If
End Sub

Private Sub Evaluar()
    Dim cMensaje As String
    Dim i As Integer, j As Integer
    
    If Not bCargando Then
        If DatosValidos Then
            If chkSoli_Habilitado.value = 1 Then                                        ' Habilitado
                cMensaje = "Habilitado. "
                
                If chkSoli_inhabilitadoIni.value = 0 And chkSoli_inhabilitadoFin.value = 0 Then
                    cMensaje = cMensaje & "Sin restricciones."
                Else
                    Fe_Ini = FechaInicio
                    If chkSoli_inhabilitadoIni.value = 1 And Val(txtDiasInicio) > 0 Then    ' Pero prohibido N d�as al inicio del mes
                        For i = 1 To Val(txtDiasInicio)
                            Fe_Ini = DateAdd("d", 1, Fe_Ini)
                            If bFeriados Then
                                For j = 0 To UBound(vFecha)
                                    If Fe_Ini = vFecha(j) Then
                                        i = i - 1
                                    End If
                                Next j
                            End If
                        Next i
                    End If
                    cMensaje = cMensaje & IIf(chkSoli_inhabilitadoIni.value = 1, "V�lido desde " & Fe_Ini, "V�lido desde " & FechaInicio)
                    fe_fin = FechaFinal
                    If chkSoli_inhabilitadoFin.value = 1 And Val(txtDiasFin) > 0 Then
                        i = 0
                        Do While i < Val(txtDiasFin)
                            fe_fin = DateAdd("d", -1, fe_fin)
                            If bFeriados Then
                                For j = 0 To UBound(vFecha)
                                    If fe_fin = vFecha(j) Then
                                        i = i - 1
                                    End If
                                Next j
                            End If
                            i = i + 1
                            DoEvents
                        Loop
                        'For i = 0 To Val(txtDiasFin)
                        '    'Fe_Fin = DateAdd("d", -1, Fe_Fin)
                        '    For j = 0 To UBound(vFecha)
                        '        If Fe_Fin = vFecha(j) Then
                        '            i = i - 1
                        '        End If
                        '    Next j
                        '    Fe_Fin = DateAdd("d", -1, Fe_Fin)
                        'Next i
                    End If
                    cMensaje = cMensaje & IIf(chkSoli_inhabilitadoFin.value = 0, " hasta el " & FechaFinal, " hasta el " & fe_fin)
                End If
                lblConfiguracionActualSolicitudes = cMensaje
                lblConfiguracionActualSolicitudes.ForeColor = vbBlue
            Else
                cMensaje = "El sistema de solicitudes se encuentra inhabilitado."
                lblConfiguracionActualSolicitudes = cMensaje
                lblConfiguracionActualSolicitudes.ForeColor = vbRed
            End If
        End If
    End If
End Sub

'Private Sub Evaluar()
'    Dim cMensaje As String
'    Dim i As Integer
'
'    If Not bCargando Then
'        If DatosValidos Then
'            If chkSoli_Habilitado.Value = 1 Then        ' Habilitado
'                cMensaje = "Habilitado. "
'                If chkSoli_inhabilitadoIni.Value = 0 And chkSoli_inhabilitadoFin.Value = 0 Then
'                    cMensaje = cMensaje & "Sin restricciones."
'                Else
'                    If chkSoli_inhabilitadoIni.Value = 1 And Val(txtDiasInicio) > 0 Then    ' Pero prohibido N d�as al inicio del mes
'                        Fe_Ini = FechaInicio
'                        For i = 0 To Val(txtDiasInicio)
'                            Fe_Ini = DateAdd("d", 1, Fe_Ini)
'                            If sp_GetFeriado(Fe_Ini, Fe_Ini) Then
'                                i = i - 1
'                            End If
'                        Next i
'                    End If
'                    cMensaje = cMensaje & IIf(chkSoli_inhabilitadoIni.Value = 1, "V�lido desde " & Fe_Ini, "V�lido desde " & FechaInicio)
'                    If chkSoli_inhabilitadoFin.Value = 1 And Val(txtDiasFin) > 0 Then
'                        Fe_Fin = FechaFinal
'                        For i = 0 To Val(txtDiasFin)
'                            Fe_Fin = DateAdd("d", -1, Fe_Fin)
'                            If sp_GetFeriado(Fe_Fin, Fe_Fin) Then
'                                i = i - 1
'                            End If
'                        Next i
'                    End If
'                    cMensaje = cMensaje & IIf(chkSoli_inhabilitadoFin.Value = 0, " hasta el " & FechaFinal, " hasta el " & Fe_Fin)
'                End If
'                lblConfiguracionActualSolicitudes = cMensaje
'                lblConfiguracionActualSolicitudes.ForeColor = vbBlue
'            Else
'                cMensaje = "El sistema de solicitudes se encuentra inhabilitado."
'                lblConfiguracionActualSolicitudes = cMensaje
'                lblConfiguracionActualSolicitudes.ForeColor = vbRed
'            End If
'        End If
'    End If
'End Sub

Private Function DatosValidos() As Boolean
    DatosValidos = False
    If IsNumeric(txtDiasInicio) Then
        If Val(txtDiasInicio) >= 0 Then
            DatosValidos = True
            Exit Function
        Else
            MsgBox "La cantidad de d�as de principio de mes" & vbCrLf & _
                    "debe ser un entero positivo.", vbExclamation + vbOKOnly
            Exit Function
        End If
    Else
        MsgBox "La cantidad de d�as de principio de mes" & vbCrLf & _
                "debe ser un entero positivo.", vbExclamation + vbOKOnly
        Exit Function
    End If
    
    If IsNumeric(txtDiasFin) Then
        If Val(txtDiasFin) >= 0 Then
            DatosValidos = True
            Exit Function
        Else
            MsgBox "La cantidad de d�as desde fin de mes" & vbCrLf & _
                    "debe ser un entero positivo.", vbExclamation + vbOKOnly
            Exit Function
        End If
    Else
        MsgBox "La cantidad de d�as desde fin de mes" & vbCrLf & _
                "debe ser un entero positivo.", vbExclamation + vbOKOnly
        Exit Function
    End If
End Function

Private Sub chkSoli_Habilitado_Click()
    If chkSoli_Habilitado.value = 1 Then
        chkSoli_inhabilitadoIni.Enabled = True
        chkSoli_inhabilitadoFin.Enabled = True
        txtDiasInicio.Enabled = True
        txtDiasFin.Enabled = True
        lblSolicitudes(0).Enabled = True
        lblSolicitudes(1).Enabled = True
        udInicio.Enabled = True
        udFinal.Enabled = True
    Else
        chkSoli_inhabilitadoIni.Enabled = False
        chkSoli_inhabilitadoFin.Enabled = False
        txtDiasInicio.Enabled = False
        txtDiasFin.Enabled = False
        lblSolicitudes(0).Enabled = False
        lblSolicitudes(1).Enabled = False
        udInicio.Enabled = False
        udFinal.Enabled = False
    End If
    If Not bCargando Then Call Evaluar
End Sub

Private Sub chkSoli_inhabilitadoIni_Click()
    If chkSoli_inhabilitadoIni.value = 0 Then
        txtDiasInicio.text = 0
        Call setHabilCtrl(txtDiasInicio, "DIS")
        Call setHabilCtrl(udInicio, "DIS")
    Else
        txtDiasInicio.text = 1
        Call setHabilCtrl(txtDiasInicio, "NOR")
        Call setHabilCtrl(udInicio, "NOR")
        txtDiasInicio.Locked = True
    End If
    If Not bCargando Then Call Evaluar
End Sub

Private Sub chkSoli_inhabilitadoFin_Click()
    If chkSoli_inhabilitadoFin.value = 0 Then
        txtDiasFin.text = 0
        Call setHabilCtrl(txtDiasFin, "DIS")
        Call setHabilCtrl(udFinal, "DIS")
    Else
        txtDiasFin.text = 1
        Call setHabilCtrl(txtDiasFin, "NOR")
        Call setHabilCtrl(udFinal, "NOR")
        txtDiasFin.Locked = True
    End If
    If Not bCargando Then Call Evaluar
End Sub

Private Sub txtDiasFin_Change()
    If Not bCargando Then Call Evaluar
End Sub

Private Sub txtDiasInicio_Change()
    If Not bCargando Then Call Evaluar
End Sub

Private Sub cmdCerrar_Click()
    If cmdCerrar.Caption = "Cerrar" Then
        Call Status("Listo.")
        Unload Me
    Else
        fraSolicitudes.Enabled = False
        fraSolicitudesOtros.Enabled = False
        cmdModificar.Enabled = True
        cmdConfirmar.Enabled = False
        cmdCerrar.Caption = "Cerrar"
        Call InicializarPantalla
        Call Evaluar
    End If
End Sub

Private Sub Command1_Click()
    Load auxEsquemas
    auxEsquemas.Show
End Sub
