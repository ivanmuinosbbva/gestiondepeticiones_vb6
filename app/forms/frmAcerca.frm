VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAcerca 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About MyApp"
   ClientHeight    =   5130
   ClientLeft      =   2340
   ClientTop       =   2040
   ClientWidth     =   5760
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAcerca.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5130
   ScaleWidth      =   5760
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView lvwPropiedades 
      Height          =   1755
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   3096
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   4620
      TabIndex        =   0
      Top             =   4260
      Width           =   1065
   End
   Begin VB.CommandButton cmdSysInfo 
      Caption         =   "Info. sistema"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4620
      TabIndex        =   1
      Top             =   4680
      Width           =   1065
   End
   Begin VB.TextBox txtInfo 
      BackColor       =   &H8000000F&
      Height          =   1695
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   1680
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label lblFechaExe 
      Caption         =   "ExeDateTime"
      Height          =   225
      Left            =   570
      TabIndex        =   8
      Top             =   900
      Width           =   4965
   End
   Begin VB.Image Image1 
      Height          =   240
      Left            =   180
      Picture         =   "frmAcerca.frx":000C
      Top             =   240
      Width           =   240
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00808080&
      BorderStyle     =   6  'Inside Solid
      Index           =   1
      X1              =   90
      X2              =   5654
      Y1              =   3120
      Y2              =   3120
   End
   Begin VB.Label lblDescription 
      Caption         =   "App Description"
      ForeColor       =   &H00000000&
      Height          =   450
      Left            =   570
      TabIndex        =   2
      Top             =   1200
      Width           =   4965
   End
   Begin VB.Label lblTitle 
      Caption         =   "Application Title"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   570
      TabIndex        =   4
      Top             =   240
      Width           =   4965
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      Index           =   0
      X1              =   105
      X2              =   5654
      Y1              =   3120
      Y2              =   3120
   End
   Begin VB.Label lblVersion 
      Caption         =   "Version"
      Height          =   225
      Left            =   570
      TabIndex        =   5
      Top             =   600
      Width           =   4965
   End
   Begin VB.Label lblDisclaimer 
      Caption         =   "Warning: ..."
      ForeColor       =   &H00000000&
      Height          =   1425
      Left            =   120
      TabIndex        =   3
      Top             =   3600
      Width           =   4395
   End
End
Attribute VB_Name = "frmAcerca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Reg Key Security Options...
Const READ_CONTROL = &H20000
Const KEY_QUERY_VALUE = &H1
Const KEY_SET_VALUE = &H2
Const KEY_CREATE_SUB_KEY = &H4
Const KEY_ENUMERATE_SUB_KEYS = &H8
Const KEY_NOTIFY = &H10
Const KEY_CREATE_LINK = &H20
Const KEY_ALL_ACCESS = KEY_QUERY_VALUE + KEY_SET_VALUE + _
                       KEY_CREATE_SUB_KEY + KEY_ENUMERATE_SUB_KEYS + _
                       KEY_NOTIFY + KEY_CREATE_LINK + READ_CONTROL

' Reg Key ROOT Types...
Const HKEY_LOCAL_MACHINE = &H80000002
Const ERROR_SUCCESS = 0
Const REG_SZ = 1                         ' Unicode null terminated string
Const REG_DWORD = 4                      ' 32-bit number

Const gREGKEYSYSINFOLOC = "SOFTWARE\Microsoft\Shared Tools Location"
Const gREGVALSYSINFOLOC = "MSINFO"
Const gREGKEYSYSINFO = "SOFTWARE\Microsoft\Shared Tools\MSINFO"
Const gREGVALSYSINFO = "PATH"

Private Declare Function RegOpenKeyEx Lib "advapi32" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, ByRef phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByRef lpType As Long, ByVal lpData As String, ByRef lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32" (ByVal hKey As Long) As Long

Private Sub Form_Load()
    Dim cOSVersion As String
    Dim cLetter As String
    Dim subElemento As MSComctlLib.ListItem
    Dim dFechaRuntime As Date
    
    cOSVersion = getOSVersion()
    Me.Caption = "Acerca de..."
    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision & cLetter
    
    If Dir(App.Path & "\" & App.EXEName & ".exe", vbArchive) <> "" Then
        dFechaRuntime = FileDateTime(App.Path & "\" & App.EXEName & ".exe")
        lblFechaExe.Caption = Year(dFechaRuntime) & "." & Format(Month(dFechaRuntime), "00") & Format(Day(dFechaRuntime), "00") & "." & Format(Hour(dFechaRuntime), "00") & Format(Minute(dFechaRuntime), "00")
    Else
        lblFechaExe.Caption = "Runtime enviroment"
    End If
    lblTitle.Caption = App.Title
    lblDescription.Caption = App.FileDescription
    
    With lvwPropiedades
        .ListItems.Clear
        .BorderStyle = ccNone
        .GridLines = False
        .HotTracking = False
        .HoverSelection = False
        .LabelWrap = True
        .View = lvwReport
        .Font.name = "Tahoma": .Font.Size = 8
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , "key", "Propiedad", 2000, lvwColumnLeft
        .ColumnHeaders.Add , "value", "Valor", 4000, lvwColumnLeft
        Set subElemento = .ListItems.Add(, , "Versi�n OS"): subElemento.SubItems(1) = cOSVersion
        Set subElemento = .ListItems.Add(, , "Versi�n DBMS"): subElemento.SubItems(1) = glDRIVER_ODBC & " (" & aplCONN.Properties(12).value & ")"
        Set subElemento = .ListItems.Add(, , "Versi�n MSADO"): subElemento.SubItems(1) = aplCONN.Version
        Set subElemento = .ListItems.Add(, , "Versi�n de Office"): subElemento.SubItems(1) = glMSOfficeExcelVersion
        Set subElemento = .ListItems.Add(, , "Directorio de trabajo"): subElemento.SubItems(1) = glWRKDIR
        Set subElemento = .ListItems.Add(, , "HOMEDRIVE"): subElemento.SubItems(1) = Environ$("HOMEDRIVE")
        Set subElemento = .ListItems.Add(, , "HOMESHARE"): subElemento.SubItems(1) = Environ$("HOMESHARE")
        Set subElemento = .ListItems.Add(, , "USERPROFILE"): subElemento.SubItems(1) = Environ$("USERPROFILE")
        Set subElemento = .ListItems.Add(, , "TMP"): subElemento.SubItems(1) = Environ$("TMP")
        Set subElemento = .ListItems.Add(, , "TEMP"): subElemento.SubItems(1) = Environ$("TEMP")
        Set subElemento = .ListItems.Add(, , "Grupo homologador"): subElemento.SubItems(1) = glHomologacionSectorNombre & " - " & glHomologacionGrupoNombre
        Set subElemento = .ListItems.Add(, , "Grupo Seg. Inform�tica"): subElemento.SubItems(1) = glSegInfSectorNombre & " - " & glSegInfGrupoNombre
        Set subElemento = .ListItems.Add(, , "Grupo Tecnolog�a"): subElemento.SubItems(1) = glTecnoSectorNombre & " - " & glTecnoGrupoNombre
    End With
    lblDisclaimer = "Advertencia: este programa est� protegido por las leyes de derechos de autor y otros tratados internacionales. La reproducci�n o distribuci�n no autorizadas de este programa, o de cualquier parte del mismo, pueden dar lugar a penalizaciones tanto civiles como penales y ser�n objeto de todas las acciones judiciales que correspondan."
End Sub

Private Sub cmdSysInfo_Click()
  Call StartSysInfo
End Sub

Private Sub cmdOK_Click()
  Unload Me
End Sub

Public Sub StartSysInfo()
    On Error GoTo SysInfoErr
  
    Dim rc As Long
    Dim SysInfoPath As String
    
    If GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFO, gREGVALSYSINFO, SysInfoPath) Then            ' Try To Get System Info Program Path\Name From Registry...
    ElseIf GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFOLOC, gREGVALSYSINFOLOC, SysInfoPath) Then  ' Try To Get System Info Program Path Only From Registry...
        If (Dir(SysInfoPath & "\MSINFO32.EXE") <> "") Then      ' Validate Existance Of Known 32 Bit File Version
            SysInfoPath = SysInfoPath & "\MSINFO32.EXE"         ' Error - File Can Not Be Found...
        Else
            GoTo SysInfoErr
        End If
    Else    ' Error - Registry Entry Can Not Be Found...
        GoTo SysInfoErr
    End If
    Call Shell(SysInfoPath, vbNormalFocus)
    
    Exit Sub
SysInfoErr:
    MsgBox "System Information Is Unavailable At This Time", vbOKOnly
End Sub

Public Function GetKeyValue(KeyRoot As Long, KeyName As String, SubKeyRef As String, ByRef KeyVal As String) As Boolean
    Dim i As Long                                           ' Loop Counter
    Dim rc As Long                                          ' Return Code
    Dim hKey As Long                                        ' Handle To An Open Registry Key
    Dim hDepth As Long                                      '
    Dim KeyValType As Long                                  ' Data Type Of A Registry Key
    Dim tmpVal As String                                    ' Tempory Storage For A Registry Key Value
    Dim KeyValSize As Long                                  ' Size Of Registry Key Variable
    '------------------------------------------------------------
    ' Open RegKey Under KeyRoot {HKEY_LOCAL_MACHINE...}
    '------------------------------------------------------------
    rc = RegOpenKeyEx(KeyRoot, KeyName, 0, KEY_ALL_ACCESS, hKey) ' Open Registry Key
    
    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Handle Error...
    
    tmpVal = String$(1024, 0)                             ' Allocate Variable Space
    KeyValSize = 1024                                       ' Mark Variable Size
    
    '------------------------------------------------------------
    ' Retrieve Registry Key Value...
    '------------------------------------------------------------
    rc = RegQueryValueEx(hKey, SubKeyRef, 0, _
                         KeyValType, tmpVal, KeyValSize)    ' Get/Create Key Value
                        
    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Handle Errors
    
    If (Asc(Mid(tmpVal, KeyValSize, 1)) = 0) Then           ' Win95 Adds Null Terminated String...
        tmpVal = Left(tmpVal, KeyValSize - 1)               ' Null Found, Extract From String
    Else                                                    ' WinNT Does NOT Null Terminate String...
        tmpVal = Left(tmpVal, KeyValSize)                   ' Null Not Found, Extract String Only
    End If
    '------------------------------------------------------------
    ' Determine Key Value Type For Conversion...
    '------------------------------------------------------------
    Select Case KeyValType                                  ' Search Data Types...
    Case REG_SZ                                             ' String Registry Key Data Type
        KeyVal = tmpVal                                     ' Copy String Value
    Case REG_DWORD                                          ' Double Word Registry Key Data Type
        For i = Len(tmpVal) To 1 Step -1                    ' Convert Each Bit
            KeyVal = KeyVal + Hex(Asc(Mid(tmpVal, i, 1)))   ' Build Value Char. By Char.
        Next
        KeyVal = Format$("&h" + KeyVal)                     ' Convert Double Word To String
    End Select
    
    GetKeyValue = True                                      ' Return Success
    rc = RegCloseKey(hKey)                                  ' Close Registry Key
    Exit Function                                           ' Exit
    
GetKeyError:      ' Cleanup After An Error Has Occured...
    KeyVal = ""                                             ' Set Return Val To Empty String
    GetKeyValue = False                                     ' Return Failure
    rc = RegCloseKey(hKey)                                  ' Close Registry Key
End Function

' Para ver las propiedades del aplCONN por el debug
' for i = 0 to aplCONN.Properties.Count - 1: ? i & ". " & aplCONN.Properties(i).Name & ": " & aplCONN.Properties(i).Value: next i

'' Historia...
'Private Sub Command1_Click()
'    Dim X
'    X = ShellExecute(Me.hwnd, "Open", "https://sites.google.com/a/bbva.com/cgm/", &O0, &O0, 1)
'End Sub
