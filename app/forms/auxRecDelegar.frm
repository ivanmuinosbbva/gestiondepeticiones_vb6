VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form auxRecDelegar 
   Caption         =   "Delegaci�n de funciones"
   ClientHeight    =   6300
   ClientLeft      =   1155
   ClientTop       =   345
   ClientWidth     =   7665
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "auxRecDelegar.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6300
   ScaleWidth      =   7665
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      TabIndex        =   10
      Top             =   0
      Width           =   5985
      Begin VB.Label lblHasta 
         Caption         =   "delegar"
         Height          =   195
         Left            =   3420
         TabIndex        =   16
         Top             =   900
         Width           =   1965
      End
      Begin VB.Label lblDesde 
         Caption         =   "delegar"
         Height          =   195
         Left            =   180
         TabIndex        =   15
         Top             =   900
         Width           =   1845
      End
      Begin VB.Label lblDelega 
         Caption         =   "delegar"
         Height          =   195
         Left            =   180
         TabIndex        =   14
         Top             =   570
         Width           =   4965
      End
      Begin VB.Label lblRecurso 
         Caption         =   "recurso"
         Height          =   195
         Left            =   180
         TabIndex        =   11
         Top             =   240
         Width           =   4965
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   120
      TabIndex        =   3
      Top             =   5220
      Width           =   5985
      Begin AT_MaskText.MaskText txtFechaDesde 
         Height          =   315
         Left            =   900
         TabIndex        =   5
         Top             =   600
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaHasta 
         Height          =   315
         Left            =   4260
         TabIndex        =   6
         Top             =   600
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNomRecurso 
         Height          =   315
         Left            =   900
         TabIndex        =   9
         Top             =   180
         Width           =   4845
         _ExtentX        =   8546
         _ExtentY        =   556
         MaxLength       =   50
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Delegar a"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   690
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   3720
         TabIndex        =   8
         Top             =   660
         Width           =   420
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Left            =   360
         TabIndex        =   7
         Top             =   660
         Width           =   450
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6255
      Left            =   6120
      TabIndex        =   0
      Top             =   0
      Width           =   1515
      Begin VB.Frame fraReferencias 
         Caption         =   " Referencias "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   1215
         Begin VB.Shape shpActivo 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   1  'Opaque
            Height          =   255
            Left            =   720
            Top             =   360
            Width           =   375
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Activo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   165
            Left            =   120
            TabIndex        =   20
            Top             =   405
            Width           =   405
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Vencido"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   165
            Left            =   120
            TabIndex        =   19
            Top             =   720
            Width           =   495
         End
         Begin VB.Shape shpVencido 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   1  'Opaque
            Height          =   255
            Left            =   720
            Top             =   675
            Width           =   375
         End
      End
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "Limpiar"
         Height          =   375
         Left            =   120
         TabIndex        =   12
         Top             =   4440
         Width           =   1245
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   5760
         Width           =   1245
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   5400
         Width           =   1245
      End
      Begin VB.Label lblVencida 
         Caption         =   "La vigencia de la delegaci�n de funciones ha expirado."
         Height          =   915
         Left            =   120
         TabIndex        =   17
         Top             =   1440
         Visible         =   0   'False
         Width           =   1275
         WordWrap        =   -1  'True
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3810
      Left            =   120
      TabIndex        =   2
      Top             =   1380
      Width           =   5985
      _ExtentX        =   10557
      _ExtentY        =   6720
      _Version        =   393216
      Cols            =   14
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "auxRecDelegar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 09.12.2008 - Se marca de manera visual al recurso delegado por el recurso actuante.
' -004- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Const colCODRECURSO = 0
Const colNOMRECURSO = 1
Const colCODDIRECCION = 2
Const colCODGERENCIA = 3
Const colCODSECTOR = 4
Const colCODGRUPO = 5
Const colVINCULO = 7
Const colACARGO = 6
Const colESTADO = 8
Const colHORAS = 9
Const colDELEGACOD = 10
Const colDELEGADES = 11
Const colDELEGAHAS = 12
Const colOBSERV = 13

Public pubNroPeticion As Long
Public pubCodEstado As String

'{ add -003- a.
Dim cRecursoDelegado As String
Dim bVencido As Boolean
'}

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    Me.Height = 6810
    Me.Width = 7785
    
    '{ add -003- a.
    bVencido = False
    shpActivo.BackColor = RGB(152, 198, 255)
    shpVencido.BackColor = RGB(250, 177, 163)
    '}
    lblDesde.Caption = ""
    lblHasta.Caption = ""
    lblDelega.Caption = ""
    'If sp_GetRecurso(glCodigoRecurso, "R") Then
    If sp_GetRecurso(glLOGIN_ID_REAL, "R") Then
        lblRecurso.Caption = "Recurso: " & ClearNull(aplRST.Fields.item("nom_recurso"))
        lblDesde.Caption = IIf(Not IsNull(aplRST!dlg_desde), "Desde: " & Format(aplRST!dlg_desde, "dd/mm/yyyy"), "")
        lblHasta.Caption = IIf(Not IsNull(aplRST!dlg_hasta), "Hasta: " & Format(aplRST!dlg_hasta, "dd/mm/yyyy"), "")
        If ClearNull(aplRST.Fields.item("dlg_recurso")) <> "" Then
            '{ add -003- a. Si la fecha de hasta de la vigencia es menor a la fecha de hoy, ya expir�.
            If CDate(aplRST!dlg_hasta) < date Then
                lblVencida.visible = True
                bVencido = True
            Else
                lblVencida.visible = False
            End If
            '}
            cRecursoDelegado = ClearNull(aplRST.Fields.item("dlg_recurso"))     ' add -003- a.
            If sp_GetRecurso(ClearNull(aplRST.Fields.item("dlg_recurso")), "R") Then
                lblDelega.Caption = "Delegado a: " & ClearNull(aplRST.Fields.item("nom_recurso"))
            End If
        End If
    Else
        'lblRecurso.Caption = glCodigoRecurso & " : " & "Error Recurso"
        lblRecurso.Caption = glLOGIN_ID_REAL & " : " & "Error Recurso"
    End If
    Call IniciarScroll(grdDatos)      ' add -004- a.
    Call CargarGrid
End Sub

Private Sub cmdLimpiar_Click()
    txtNomRecurso = ""
    txtFechaDesde.Text = ""
    txtFechaHasta.Text = ""
End Sub

Private Sub cmdOk_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If Not ValidaDelegado Then
        LockProceso (False)
        Exit Sub
    End If
    'If sp_DelegaRecurso(glCodigoRecurso, getStrCodigo(txtNomRecurso), txtFechaDesde.DateValue, txtFechaHasta.DateValue) Then
    If sp_DelegaRecurso(glLOGIN_ID_REAL, getStrCodigo(txtNomRecurso), txtFechaDesde.DateValue, txtFechaHasta.DateValue) Then
        MsgBox "La operacion se realiz� correctamente", vbInformation + vbOKOnly
    Else
        LockProceso (False)
        Exit Sub
    End If
    LockProceso (False)
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Sub CargarGrid()
    Call Puntero(True)
    Call Status("Cargando recursos...")
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colCODRECURSO) = "Codigo": .ColWidth(colCODRECURSO) = 930
        .TextMatrix(0, colNOMRECURSO) = "Nombre": .ColWidth(colNOMRECURSO) = 2500
        .TextMatrix(0, colCODDIRECCION) = "Direcc": .ColWidth(colCODDIRECCION) = 900
        .TextMatrix(0, colCODGERENCIA) = "Geren": .ColWidth(colCODGERENCIA) = 900
        .TextMatrix(0, colCODSECTOR) = "Sector": .ColWidth(colCODSECTOR) = 900
        .TextMatrix(0, colCODGRUPO) = "Grup": .ColWidth(colCODGRUPO) = 900
        .TextMatrix(0, colACARGO) = "a Cargo": .ColWidth(colACARGO) = 500
        .TextMatrix(0, colVINCULO) = "Vinc": .ColWidth(colVINCULO) = 500
        .TextMatrix(0, colESTADO) = "Est": .ColWidth(colESTADO) = 500
        .TextMatrix(0, colHORAS) = "Horas": .ColWidth(colHORAS) = 500
        .TextMatrix(0, colOBSERV) = "Observ": .ColWidth(colOBSERV) = 0
        .TextMatrix(0, colDELEGACOD) = "Delega": .ColWidth(colDELEGACOD) = 1000
        .TextMatrix(0, colDELEGADES) = "Desde": .ColWidth(colDELEGADES) = 1000
        .TextMatrix(0, colDELEGAHAS) = "Hasta": .ColWidth(colDELEGAHAS) = 1000
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetRecurso("", "R", "A") Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODRECURSO) = ClearNull(aplRST.Fields.item("cod_recurso"))
            .TextMatrix(.Rows - 1, colNOMRECURSO) = ClearNull(aplRST.Fields.item("nom_recurso"))
            .TextMatrix(.Rows - 1, colCODDIRECCION) = ClearNull(aplRST.Fields.item("cod_direccion"))
            .TextMatrix(.Rows - 1, colCODGERENCIA) = ClearNull(aplRST.Fields.item("cod_gerencia"))
            .TextMatrix(.Rows - 1, colCODSECTOR) = ClearNull(aplRST.Fields.item("cod_sector"))
            .TextMatrix(.Rows - 1, colCODGRUPO) = ClearNull(aplRST.Fields.item("cod_grupo"))
            .TextMatrix(.Rows - 1, colVINCULO) = ClearNull(aplRST.Fields.item("vinculo"))
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST.Fields.item("estado_recurso"))
            .TextMatrix(.Rows - 1, colACARGO) = IIf(ClearNull(aplRST.Fields.item("flg_cargoarea")) = "", "", ClearNull(aplRST.Fields.item("flg_cargoarea")))
            .TextMatrix(.Rows - 1, colHORAS) = ClearNull(aplRST.Fields.item("horasdiarias"))
            .TextMatrix(.Rows - 1, colOBSERV) = ClearNull(aplRST.Fields.item("observaciones"))
            .TextMatrix(.Rows - 1, colDELEGACOD) = ClearNull(aplRST.Fields.item("dlg_recurso"))
            .TextMatrix(.Rows - 1, colDELEGADES) = IIf(Not IsNull(aplRST!dlg_desde), Format(aplRST!dlg_desde, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colDELEGAHAS) = IIf(Not IsNull(aplRST!dlg_hasta), Format(aplRST!dlg_hasta, "dd/mm/yyyy"), "")
            '{ add -003- a.
            If ClearNull(.TextMatrix(.Rows - 1, colCODRECURSO)) = cRecursoDelegado Then
                If bVencido Then
                    Call PintarLinea(grdDatos, prmGridFillRowColorRed)
                Else
                    Call PintarLinea(grdDatos, prmGridFillRowColorLightBlue)
                End If
            End If
            '}
        End With
        aplRST.MoveNext
    Loop
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Public Sub MostrarSeleccion()
    With grdDatos
        If .RowSel > 0 Then
            .HighLight = flexHighlightWithFocus
            If .TextMatrix(.RowSel, 0) <> "" Then
                txtNomRecurso = ClearNull(.TextMatrix(.RowSel, colCODRECURSO)) & " : " & ClearNull(.TextMatrix(.RowSel, colNOMRECURSO))
                If IsNull(txtFechaDesde.DateValue) Then
                    txtFechaDesde.Text = Format(date, "dd/mm/yyyy")
                End If
            End If
        End If
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Function ValidaDelegado() As Boolean
    Dim xVal As Integer
    ValidaDelegado = False
    xVal = 0
    If getStrCodigo(txtNomRecurso) <> "" Then
        xVal = xVal + 1
    End If
    If Not IsNull(txtFechaDesde.DateValue) Then
        xVal = xVal + 1
    End If
    If Not IsNull(txtFechaHasta.DateValue) Then
        xVal = xVal + 1
    End If
    If xVal = 0 Then
        ValidaDelegado = True
        Exit Function
    End If
    If xVal <> 3 Then
        MsgBox ("Faltan datos de delegaci�n")
        ValidaDelegado = False
        Exit Function
    End If
    If txtFechaHasta.DateValue < txtFechaDesde.DateValue Then
        MsgBox ("Error fechas delegeci�n")
        ValidaDelegado = False
        Exit Function
    End If
    ValidaDelegado = True
End Function


Private Sub txtFechaDesde_LostFocus()
    If IsNull(txtFechaHasta.DateValue) Then
        txtFechaHasta.Text = Format(txtFechaDesde.DateValue, "dd/mm/yyyy")
    End If
End Sub

'{ add -004- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
