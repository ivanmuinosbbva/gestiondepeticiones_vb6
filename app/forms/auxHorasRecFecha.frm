VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form auxHorasRecFecha 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Horas trabajadas por d�a"
   ClientHeight    =   7425
   ClientLeft      =   1140
   ClientTop       =   330
   ClientWidth     =   4905
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   4905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkFeriados 
      Caption         =   "Visualizar tambi�n d�as no h�biles y feriados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   60
      TabIndex        =   4
      Top             =   6540
      Width           =   3735
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   60
      TabIndex        =   3
      Top             =   0
      Width           =   4815
      Begin VB.Label lblFecha 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   4545
      End
      Begin VB.Label lblRecurso 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   8
         Top             =   480
         Width           =   4545
      End
      Begin VB.Label lblTotalesHorasNominales 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   270
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   4545
      End
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   60
      TabIndex        =   0
      Top             =   6780
      Width           =   4815
      Begin VB.CommandButton cmdOk 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3840
         TabIndex        =   1
         Top             =   180
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4710
      Left            =   60
      TabIndex        =   2
      Top             =   1140
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   8308
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblHsNominales 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3000
      TabIndex        =   6
      Top             =   5880
      Width           =   825
   End
   Begin VB.Label lblHsCargadas 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2160
      TabIndex        =   5
      Top             =   5880
      Width           =   825
   End
End
Attribute VB_Name = "auxHorasRecFecha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 26.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -004- a. FJS 18.06.2009 - Se agrega la posibilidad de visualizar los d�as feriados y las horas cargadas en esos d�as (si existieran)
' -005- a. FJS 08.10.2010 - Bug: la primera vez que se carga la pantalla no estaba mostrando las horas nominales del recurso seleccionado.
' -006- a. FJS 26.10.2010 - Bug: si un lider tiene perfil de Carga de horas y puede ver las horas cargadas de sus reportes, al ingresar a esta
'                           pantalla no se estan mostrando las horas de los reportes, sino del usuario que ingres� al sistema.
' -006- b. FJS 26.10.2010 - Bug: se arregla, porque si no hay feriados declarados en la tabla de feriados, no muestra nada.

Option Explicit

Const colFECHA = 0
Const colDIASEMANA = 1      ' add -004- a.
Const colHORAS = 2
Const colHORASFAULT = 3

Dim hsNomi As Integer
'{ add -004- a.
Dim vFeriados() As Date
Dim bHayFeriados As Boolean
Dim lHoras_Cargadas As Long
Dim lHoras_Nominales As Long
Dim bLoading As Boolean
'}

Public pubRecurso As String

Private Sub Form_Load()
    bLoading = True
    lblFecha.Caption = "Suma de horas diarias entre el: " & Format(glFechaDesde, "dd/mm/yyyy") & " y " & Format(glFechaHasta, "dd/mm/yyyy")
    Call IniciarScroll(grdDatos)        ' add -003- a.
    '{ add -004- a.
    pubRecurso = CodigoCombo(frmCargaHoras.cboRecurso, False)   ' add -006- a.
    chkFeriados.value = IIf(GetSetting("GesPet", "Preferencias\Horas", "Feriados", "N") = "N", 0, 1)
    bHayFeriados = False
    Call CargarFeriados
    bLoading = False
    '}
End Sub

'{ add -004- a.
Private Sub chkFeriados_Click()
    If Not bLoading Then Call CargarGrid
End Sub
'}

'{ add -004- a.
Private Sub CargarFeriados()
    Dim i As Long
    Dim Dias_Habiles As Long
    Dim Total_HorasNominales As Long
    
    If sp_GetRecurso(pubRecurso, "R") Then
        lblRecurso.Caption = "Horas nominales diarias del recurso: " & ClearNull(aplRST.Fields.Item("horasdiarias"))
        hsNomi = Val(ClearNull(aplRST.Fields.Item("horasdiarias")))
    End If
    
    Dias_Habiles = DateDiff("d", glFechaDesde, glFechaHasta, vbSunday) + 1  ' add -004- a. Determina las horas nominales totales a cargar por el recurso para el per�odo
    
    i = 0
    
    If sp_GetFeriado(glFechaDesde, glFechaHasta) Then
        If Not aplRST.EOF Then
            Do While Not aplRST.EOF
                ReDim Preserve vFeriados(i)
                vFeriados(i) = aplRST.Fields!fecha
                aplRST.MoveNext
                i = i + 1
                DoEvents
            Loop
        End If
    End If
    If i > 0 Then
        ' Aqu� determina el total de horas nominales para el per�odo seleccionado
        Dias_Habiles = Dias_Habiles - i
        Total_HorasNominales = Dias_Habiles * hsNomi
        lblTotalesHorasNominales = "Total horas nominales del recurso: " & Format(Total_HorasNominales, "###,###,##0")
        bHayFeriados = True
    '{ add -006- b.
    Else
        Dias_Habiles = Dias_Habiles - i
        Total_HorasNominales = Dias_Habiles * hsNomi
        lblTotalesHorasNominales = "Total horas nominales del recurso: " & Format(Total_HorasNominales, "###,###,##0")
    '}
    End If
End Sub
'}

Private Sub cmdOK_Click()
    SaveSetting "GesPet", "Preferencias\Horas", "Feriados", IIf(chkFeriados.value = 1, "S", "N")    ' add -004- a.
    Unload Me
End Sub

Sub CargarGrid()
    Call Puntero(True)
    Call Status("Cargando horas...")
    
    '{ add -004- a.
    grdDatos.visible = False
    lHoras_Cargadas = 0
    lHoras_Nominales = 0
    '}
        
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .cols = 5       ' add -004- b.
        .TextMatrix(0, colFECHA) = "Fecha"
        .TextMatrix(0, colDIASEMANA) = "D�a"    ' add -004- a.
        .TextMatrix(0, colHORAS) = "Horas"
        .TextMatrix(0, colHORASFAULT) = "Restan"
        .TextMatrix(0, colHORASFAULT + 1) = "Aux"   ' add -004- b.
        .ColWidth(colFECHA) = 1100
        .ColWidth(colDIASEMANA) = 900      ' add -004- a.
        .ColWidth(colHORAS) = 900
        .ColWidth(colHORASFAULT) = 900
        .ColWidth(colHORASFAULT + 1) = 0    ' add -004- b.
        .ColAlignment(colFECHA) = flexAlignCenterCenter
        .ColAlignment(colDIASEMANA) = flexAlignLeftCenter       ' add -004- a.
        .ColAlignment(colHORAS) = flexAlignCenterCenter         ' upd -004- a. Antes: flexAlignRightCenter
        .ColAlignment(colHORASFAULT) = flexAlignCenterCenter    ' upd -004- a. Antes: flexAlignRightCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    '{ add -004- a.
    If chkFeriados.value = 1 Then
        ' Devuelve los d�as trabajados para el per�odo incluyendo S�bados, Domingos y Feriados
        If Not sp_rptHorasTrabajadasRecurso2(pubRecurso, glFechaDesde, glFechaHasta) Then
            Call Puntero(False)
            Call Status("Listo.")
            Exit Sub
        End If
    Else
    '}
        ' Devuelve los d�as trabajados para el per�odo sin incluir ni S�bados ni Domingos ni Feriados (est�ndar)
        If Not sp_rptHorasTrabajadasRecurso(pubRecurso, glFechaDesde, glFechaHasta) Then
            Call Puntero(False)
            Call Status("Listo.")
            Exit Sub
        End If
    End If  ' add -004- a.
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colFECHA) = IIf(Not IsNull(aplRST.Fields.Item("fe_infor")), Format(aplRST.Fields.Item("fe_infor"), "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colDIASEMANA) = WeekDayName(Weekday(aplRST.Fields!fe_infor), True)        ' add -004- a.
            .TextMatrix(.Rows - 1, colHORAS) = IIf(Val(ClearNull(aplRST.Fields.Item("horas_trab"))) = 0, "-", hrsFmtHora(Val(ClearNull(aplRST.Fields.Item("horas_trab")))))     ' upd -004- a.
            If hsNomi * 60 > Val(ClearNull(aplRST.Fields.Item("horas_trab"))) Then
                .TextMatrix(.Rows - 1, colHORASFAULT) = hrsFmtHora(hsNomi * 60 - Val(ClearNull(aplRST.Fields.Item("horas_trab"))))
            Else
                .TextMatrix(.Rows - 1, colHORASFAULT) = "-"
            End If
            .TextMatrix(.Rows - 1, colHORASFAULT + 1) = IIf(hsNomi * 60 - Val(ClearNull(aplRST.Fields.Item("horas_trab"))) < 0, 0, hsNomi * 60 - Val(ClearNull(aplRST.Fields.Item("horas_trab"))))  ' upd -004- b.
            '{ add -004- a.
            If bHayFeriados Then
                If EsFeriado(aplRST.Fields.Item("fe_infor")) Then
                    .TextMatrix(.Rows - 1, colHORASFAULT) = "-"
                    PintarLinea grdDatos, prmGridFillRowColorRose
                '{ add -004- b.
                Else
                    lHoras_Cargadas = lHoras_Cargadas + Val(ClearNull(aplRST.Fields.Item("horas_trab")))
                    lHoras_Nominales = lHoras_Nominales + .TextMatrix(.Rows - 1, colHORASFAULT + 1)
                '}
                End If
            '{ add -006- b.
            Else
                lHoras_Cargadas = lHoras_Cargadas + Val(ClearNull(aplRST.Fields.Item("horas_trab")))
                lHoras_Nominales = lHoras_Nominales + .TextMatrix(.Rows - 1, colHORASFAULT + 1)
            '}
            End If
            '}
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    'aplRST.Close
    '{ add -004- a.
    grdDatos.visible = True
    lblHsCargadas = hrsFmtHora(lHoras_Cargadas)
    lblHsNominales = hrsFmtHora(lHoras_Nominales)
    '}
    Call Puntero(False)
    Call Status("Listo.")
End Sub

'{ add -004- a.
Private Function EsFeriado(dFecha As Date) As Boolean
    Dim i As Long
    
    EsFeriado = False    ' Inicializo la funci�n
    
    i = 0
    
    For i = 0 To UBound(vFeriados)
        If dFecha >= vFeriados(i) Then
            If vFeriados(i) = dFecha Then
                EsFeriado = True
                Exit For
            End If
        Else
            Exit For
        End If
    Next i
End Function

Private Function WeekDayName(Number As Byte, EmpiezaDomigo As Boolean) As String
    If EmpiezaDomigo Then
        Select Case Number
            Case 1
                WeekDayName = "Domingo"
            Case 2
                WeekDayName = "Lunes"
            Case 3
                WeekDayName = "Martes"
            Case 4
                WeekDayName = "Mi�rcoles"
            Case 5
                WeekDayName = "Jueves"
            Case 6
                WeekDayName = "Viernes"
            Case 7
                WeekDayName = "S�bado"
        End Select
    Else
        Select Case Number
            Case 1
                WeekDayName = "Lunes"
            Case 2
                WeekDayName = "Martes"
            Case 3
                WeekDayName = "Mi�rcoles"
            Case 4
                WeekDayName = "Jueves"
            Case 5
                WeekDayName = "Viernes"
            Case 6
                WeekDayName = "S�bado"
            Case 7
                WeekDayName = "Domingo"
        End Select
    End If
End Function
'}

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}

