VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesValidas 
   Caption         =   "Busqueda y visualizaci�n de peticiones v�lidas que han pasado a Producci�n"
   ClientHeight    =   7320
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10845
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesValidas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7320
   ScaleWidth      =   10845
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5415
      Left            =   120
      TabIndex        =   13
      Top             =   1800
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   9551
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      BackColorBkg    =   -2147483633
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraCargandoDatos 
      Height          =   615
      Left            =   4200
      TabIndex        =   24
      Top             =   3600
      Width           =   4095
      Begin VB.Label lblCargandoDatos 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cargando los datos... aguarde por favor..."
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   26
         Top             =   240
         Width           =   3120
      End
      Begin VB.Label lblCargandoDatos 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Cargando los datos... aguarde por favor..."
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   500
         TabIndex        =   25
         Top             =   260
         Width           =   3120
      End
   End
   Begin VB.Frame fraBuscar 
      Caption         =   " Para realizar b�squedas por filtros "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   10695
      Begin VB.CheckBox chkAutomaticRefresh 
         Alignment       =   1  'Right Justify
         Caption         =   " Actu&alizar autom�ticamente al modificar un filtro"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6360
         TabIndex        =   23
         Top             =   0
         Value           =   1  'Checked
         Width           =   4095
      End
      Begin AT_MaskText.MaskText txtNroDesde 
         Height          =   315
         Left            =   1560
         TabIndex        =   0
         Top             =   360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         KeepFocusOnError=   -1  'True
         BeepOnError     =   -1  'True
      End
      Begin VB.CommandButton cmdSinFiltros 
         Caption         =   "&Quitar filtros"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9360
         Picture         =   "frmPeticionesValidas.frx":014A
         TabIndex        =   11
         ToolTipText     =   "Inicializa todos los filtros"
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdActualizar 
         Caption         =   "A&ctualizar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9360
         Picture         =   "frmPeticionesValidas.frx":0294
         TabIndex        =   12
         ToolTipText     =   "Actualizar la grilla"
         Top             =   1080
         Width           =   1215
      End
      Begin VB.CommandButton cmdEraseNroHasta 
         Caption         =   "B"
         Height          =   255
         Left            =   1200
         TabIndex        =   3
         ToolTipText     =   "Borrar el n�mero hasta"
         Top             =   750
         Width           =   250
      End
      Begin VB.CommandButton cmdEraseNroDesde 
         Caption         =   "B"
         Height          =   255
         Left            =   1200
         TabIndex        =   1
         ToolTipText     =   "Borrar el n�mero desde"
         Top             =   390
         Width           =   250
      End
      Begin VB.CommandButton cmdEraseTitulo 
         Caption         =   "B"
         Height          =   255
         Left            =   1200
         TabIndex        =   5
         ToolTipText     =   "Borrar el texto del filtro T�tulo"
         Top             =   1080
         Width           =   250
      End
      Begin VB.ComboBox cmbPrioridad 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   7560
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1080
         Width           =   1485
      End
      Begin VB.ComboBox cmbClase 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   720
         Width           =   2055
      End
      Begin VB.ComboBox cmbTipo 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   4200
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   360
         Width           =   2055
      End
      Begin VB.TextBox txtTitulo 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   4
         Top             =   1080
         Width           =   4695
      End
      Begin AT_MaskText.MaskText txtFechaDesde 
         Height          =   315
         Left            =   7560
         TabIndex        =   8
         Top             =   360
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaHasta 
         Height          =   315
         Left            =   7560
         TabIndex        =   9
         Top             =   720
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Align           =   1
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNroHasta 
         Height          =   315
         Left            =   1560
         TabIndex        =   2
         Top             =   720
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Fecha hasta:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   7
         Left            =   6480
         TabIndex        =   22
         Top             =   720
         Width           =   975
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Fecha desde:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   6
         Left            =   6480
         TabIndex        =   21
         Top             =   360
         Width           =   1005
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   5
         Left            =   6480
         TabIndex        =   20
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   4
         Left            =   240
         TabIndex        =   19
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Clase:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   3
         Left            =   3480
         TabIndex        =   18
         Top             =   720
         Width           =   495
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "Tipo:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   2
         Left            =   3480
         TabIndex        =   17
         Top             =   360
         Width           =   390
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "N� desde:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Width           =   750
      End
      Begin VB.Label lblFiltros 
         AutoSize        =   -1  'True
         Caption         =   "N� hasta:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   1
         Left            =   240
         TabIndex        =   15
         Top             =   720
         Width           =   720
      End
   End
End
Attribute VB_Name = "frmPeticionesValidas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -002- a. FJS 24.09.2009 - Se quita la columna de fecha de aprobaci�n y se agrega el estado de la petici�n.
' -002- b. FJS 25.09.2009 - Se quita el coloreado din�mico (no tiene mucho uso ni sentido utilizarlo).

Option Explicit

Dim bLoading As Boolean

Private Sub Form_Load()
    bLoading = True
    Inicializar
    InicializarCombos
    CargarPeticiones True
    bLoading = False
    modUser32.IniciarScroll grdDatos      ' add -001- a.
End Sub

Private Sub Form_Resize()
    ' Vuelve invisibles los controles
    grdDatos.visible = False
    fraBuscar.visible = False
    
    If Me.ScaleWidth > 150 Then
        ' Mensaje de carga
        fraCargandoDatos.Top = (Me.ScaleHeight / 2) - (fraCargandoDatos.Height / 2)
        fraCargandoDatos.Left = (Me.ScaleWidth / 2) - (fraCargandoDatos.Width / 2)
        
        fraBuscar.Width = Me.ScaleWidth - 150
        grdDatos.Width = Me.ScaleWidth - 150
        If Me.ScaleWidth > 10665 Then
            cmdSinFiltros.Left = Me.ScaleWidth - (cmdSinFiltros.Width + 300)
            cmdActualizar.Left = Me.ScaleWidth - (cmdActualizar.Width + 300)
        Else
            cmdSinFiltros.Left = 9360
            cmdActualizar.Left = 9360
        End If
        
        If Me.ScaleWidth > 9390 Then
            chkAutomaticRefresh.Left = Me.ScaleWidth - (chkAutomaticRefresh.Width + 300)
        Else
            chkAutomaticRefresh.Left = 4770
        End If
    End If
    'Text1.Text = Me.ScaleWidth
    If Me.ScaleHeight > 2100 Then
        grdDatos.Height = Me.ScaleHeight - 2100
    End If
    ' Vuelve visibles los controles
    fraBuscar.visible = True
    grdDatos.visible = True
End Sub

Private Sub Inicializar()
    chkAutomaticRefresh.ToolTipText = "Tenga en cuenta que activar esta opci�n puede degradar la performance en la carga de los datos"
    With grdDatos
        .visible = False
        .Clear
        .cols = 7
        .Rows = 1
        .TextMatrix(0, 0) = "Petici�n N�"
        .TextMatrix(0, 1) = "Tipo"
        .TextMatrix(0, 2) = "Clase"
        .TextMatrix(0, 3) = "T�tulo"
        .TextMatrix(0, 4) = "Prioridad"
        .TextMatrix(0, 5) = "Sector solicitante"
        '.TextMatrix(0, 6) = "Aprobado el"      ' del -002- a.
        .TextMatrix(0, 6) = "Estado act."       ' add -002- a.
        
        .ColWidth(0) = 1000
        .ColWidth(1) = 1000
        .ColWidth(2) = 1700
        .ColWidth(3) = 5000: .ColAlignment(3) = flexAlignLeftCenter
        .ColWidth(4) = 1400: .ColAlignment(4) = flexAlignLeftCenter
        .ColWidth(5) = 6000
        .ColWidth(6) = 2000: .ColAlignment(6) = flexAlignLeftCenter     ' add -002- a.
        .BackColorBkg = Me.BackColor
    End With
End Sub

Private Sub InicializarCombos()
    With cmbTipo
        .Clear
        .AddItem "Todas" & Space(50) & "||NULL"
        .AddItem "Normal" & Space(50) & "||NOR"
        .AddItem "Propia" & Space(50) & "||PRO"
        .AddItem "Especial" & Space(50) & "||ESP"
        .AddItem "Proyecto" & Space(50) & "||PRJ"
        .ListIndex = 0
    End With
    With cmbClase
        .Clear
        .AddItem "Todas" & Space(50) & "||NULL"
        .AddItem "Nuevo desarrollo" & Space(50) & "||NUEV"
        .AddItem "Mant. Evolutivo" & Space(50) & "||EVOL"
        .AddItem "Optimizaci�n" & Space(50) & "||OPTI"
        .AddItem "Mant. Correctivo" & Space(50) & "||CORR"
        .AddItem "Atenci�n de Usuario" & Space(50) & "||ATEN"
        .AddItem "SPUFIs" & Space(50) & "||SPUF"
        .AddItem "Sin clase" & Space(50) & "||SINC"
        .ListIndex = 0
    End With
    With cmbPrioridad
        .Clear
        .AddItem "Todas" & Space(50) & "||NULL"
        .AddItem "1. Regulatorio" & Space(50) & "||1"
        .AddItem "2. Muy alta" & Space(50) & "||2"
        .AddItem "3. Alta" & Space(50) & "||3"
        .AddItem "4. Media" & Space(50) & "||4"
        .AddItem "5. Baja" & Space(50) & "||5"
        .AddItem "6. Muy baja" & Space(50) & "||6"
        .ListIndex = 0
    End With
End Sub

Private Sub CargarPeticiones(PrimeraVez As Boolean, Optional rsAux As ADODB.Recordset)
    Dim lCont As Long
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim dFechaTest As Date
    
    dFechaTest = CDate(DateAdd("m", -6, date))
    
    lCont = 0
    If PrimeraVez Then
        If sp_GetPeticionesValidas(Null, Null, Null, Null, Null, Null, Null, Null) Then
            'sbValidas.Panels(2) = aplRST.RecordCount
            With grdDatos
                .visible = False
                Do While Not aplRST.EOF
                    .Rows = .Rows + 1
                    original_Row = .Row
                    original_Col = .Col
                    original_RowSel = .RowSel
                    original_ColSel = .ColSel
                    .TextMatrix(.Rows - 1, 0) = Format(ClearNull(aplRST.Fields!pet_nroasignado), "###,###,###,##0")
                    Select Case ClearNull(aplRST.Fields!cod_tipo_peticion)
                        Case "NOR": .TextMatrix(.Rows - 1, 1) = "Normal"
                        Case "PRO": .TextMatrix(.Rows - 1, 1) = "Propia"
                        Case "ESP": .TextMatrix(.Rows - 1, 1) = "Especial"
                        Case "PRJ": .TextMatrix(.Rows - 1, 1) = "Proyecto"
                        Case Else
                            .TextMatrix(.Rows - 1, 1) = "???"
                    End Select
                    Select Case ClearNull(aplRST.Fields!cod_clase)
                        Case "NUEV": .TextMatrix(.Rows - 1, 2) = "Nuevo desarrollo"
                        Case "EVOL": .TextMatrix(.Rows - 1, 2) = "Mant. Evolutivo"
                        Case "OPTI": .TextMatrix(.Rows - 1, 2) = "Optimizaci�n"
                        Case "CORR": .TextMatrix(.Rows - 1, 2) = "Mant. Correctivo"
                        Case "ATEN": .TextMatrix(.Rows - 1, 2) = "Atenci�n de Usuario"
                        Case "SPUF": .TextMatrix(.Rows - 1, 2) = "SPUFI"
                        Case Else
                            .TextMatrix(.Rows - 1, 2) = "Sin clase"
                    End Select
                    .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST.Fields!Titulo)
                    Select Case ClearNull(aplRST.Fields!prioridad)
                        Case "1": .TextMatrix(.Rows - 1, 4) = "1: Regulatorio"
                        Case "2": .TextMatrix(.Rows - 1, 4) = "2: Muy alta"
                        Case "3": .TextMatrix(.Rows - 1, 4) = "3: Alta"
                        Case "4": .TextMatrix(.Rows - 1, 4) = "4: Media"
                        Case "5": .TextMatrix(.Rows - 1, 4) = "5: Baja"
                        Case "6": .TextMatrix(.Rows - 1, 4) = "6: Muy baja"
                    End Select
                    .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST.Fields!nom_sector)
                    .Row = original_Row
                    .Col = original_Col
                    .RowSel = original_RowSel
                    .ColSel = original_ColSel
                    lCont = lCont + 1
                    aplRST.MoveNext
                    DoEvents
                Loop
                'sbValidas.Panels(4) = lCont
            End With
        End If
        grdDatos.visible = True
    Else
        With grdDatos
            .visible = False
            .Clear
            Inicializar
            Do While Not rsAux.EOF
                .Rows = .Rows + 1
                'original_Row = .row
                'original_Col = .Col
                'original_RowSel = .RowSel
                'original_ColSel = .ColSel
                .TextMatrix(.Rows - 1, 0) = Format(ClearNull(rsAux.Fields!pet_nroasignado), "###,###,###,##0")
                Select Case ClearNull(rsAux.Fields!cod_tipo_peticion)
                    Case "NOR"
                        .TextMatrix(.Rows - 1, 1) = "Normal"
                    Case "PRO"
                        .TextMatrix(.Rows - 1, 1) = "Propia"
                    Case "ESP"
                        .TextMatrix(.Rows - 1, 1) = "Especial"
                    Case "PRJ"
                        .TextMatrix(.Rows - 1, 1) = "Proyecto"
                    Case Else
                        .TextMatrix(.Rows - 1, 1) = "???"
                End Select
                Select Case ClearNull(rsAux.Fields!cod_clase)
                    Case "NUEV"
                        .TextMatrix(.Rows - 1, 2) = "Nuevo desarrollo"
                    Case "EVOL"
                        .TextMatrix(.Rows - 1, 2) = "Mant. Evolutivo"
                    Case "OPTI"
                        .TextMatrix(.Rows - 1, 2) = "Optimizaci�n"
                    Case "CORR"
                        .TextMatrix(.Rows - 1, 2) = "Mant. Correctivo"
                        'FuncionesGenerales.PintarLinea grdDatos, prmGridFillRowColorRose   ' del -002- b.
                    Case "ATEN"
                        .TextMatrix(.Rows - 1, 2) = "Atenci�n de Usuario"
                    Case "SPUF"
                        .TextMatrix(.Rows - 1, 2) = "SPUFI"
                        'FuncionesGenerales.PintarLinea grdDatos, prmGridFillRowColorRose   ' del -002- b.
                    Case Else
                        .TextMatrix(.Rows - 1, 2) = "Sin clase"
                End Select
                .TextMatrix(.Rows - 1, 3) = ClearNull(rsAux.Fields!Titulo)
                Select Case ClearNull(rsAux.Fields!prioridad)
                    Case "1"
                        .TextMatrix(.Rows - 1, 4) = "1: Regulatorio"
                        '{ del -002- b.
                        'FuncionesGenerales.CambiarEfectoLinea grdDatos, prmGridEffectFontBold
                        'FuncionesGenerales.CambiarEfectoLinea grdDatos, prmGridEffectFontItalic
                        '}
                    Case "2"
                        .TextMatrix(.Rows - 1, 4) = "2: Muy alta"
                    Case "3"
                        .TextMatrix(.Rows - 1, 4) = "3: Alta"
                    Case "4"
                        .TextMatrix(.Rows - 1, 4) = "4: Media"
                    Case "5"
                        .TextMatrix(.Rows - 1, 4) = "5: Baja"
                    Case "6"
                        .TextMatrix(.Rows - 1, 4) = "6: Muy baja"
                End Select
                .TextMatrix(.Rows - 1, 5) = ClearNull(rsAux.Fields!nom_sector)
                .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST.Fields!nom_estado)      ' add -002- a.
                    '.TextMatrix(.Rows - 1, 6) = Format(ClearNull(aplRST.Fields!fecha), "dd/mm/yy  hh:mm")  ' del -002- a.
                '.row = original_Row
                '.Col = original_Col
                '.RowSel = original_RowSel
                '.ColSel = original_ColSel
                lCont = lCont + 1
                rsAux.MoveNext
                DoEvents
            Loop
            'sbValidas.Panels(4) = lCont
            .visible = True
        End With
    End If
End Sub

Private Sub cmdActualizar_Click()
    Filtrar
End Sub

Private Sub Filtrar()
    On Error GoTo Errores
    Dim cFilters As String
    Dim lPos As Long
    Dim cAuxiliar As String
    Dim cSpecial As Boolean
    Dim cFiltroPerfil As String
    
    ' Inicializa los filtros para abarcar todos los items
    'aplRST.Filter = adFilterNone
    
    ' Filtro por n�mero desde
    If Len(txtNroDesde) > 0 Then
        cFilters = "pet_nroasignado >= " & Trim(txtNroDesde)
    End If
    ' Filtro por n�mero hasta
    If Len(txtNroHasta) > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = cFilters & " AND pet_nroasignado <= " & Trim(txtNroHasta)
        Else
            cFilters = "pet_nroasignado <= " & Trim(txtNroHasta)
        End If
    End If
    ' Filtro por t�tulo
    If Len(txtTitulo) > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = cFilters & " AND titulo LIKE '%" & Trim(txtTitulo) & "%'"
        Else
            cFilters = "titulo LIKE '%" & Trim(txtTitulo) & "%'"
        End If
    End If
    ' Filtro por Tipo
    If cmbTipo.ListIndex > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = cFilters & " AND cod_tipo_peticion = '" & CodigoCombo(cmbTipo, True) & "'"
        Else
            cFilters = "cod_tipo_peticion = '" & CodigoCombo(cmbTipo, True) & "'"
        End If
    End If
    ' Filtro por Clase
    If cmbClase.ListIndex > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = cFilters & " AND cod_clase = '" & CodigoCombo(cmbClase, True) & "'"
        Else
            cFilters = "cod_clase = '" & CodigoCombo(cmbClase, True) & "'"
        End If
    End If
    ' Filtro por Prioridad
    If cmbPrioridad.ListIndex > 0 Then
        If Len(cFilters) > 0 Then
            cFilters = cFilters & " AND prioridad = '" & CodigoCombo(cmbPrioridad, True) & "'"
        Else
            cFilters = "prioridad = '" & CodigoCombo(cmbPrioridad, True) & "'"
        End If
    End If
    ' Filtro por fecha desde de aprobaci�n
    If Len(txtFechaDesde) > 0 Then
        If IsDate(txtFechaDesde.DateValue) Then
            If Len(cFilters) > 0 Then
                cFilters = cFilters & " AND fecha >= #" & txtFechaDesde.DateValue & "#"
            Else
                cFilters = "fecha >= #" & txtFechaDesde.DateValue & "#"
            End If
        Else
            MsgBox "Debe ingresar una fecha v�lida.", vbExclamation + vbOKOnly, "Fecha desde"
        End If
    End If
    ' Filtro por fecha hasta de aprobaci�n
    If Len(txtFechaHasta) > 0 Then
        If IsDate(txtFechaHasta.DateValue) Then
            If Len(cFilters) > 0 Then
                cFilters = cFilters & " AND fecha <= #" & txtFechaHasta.DateValue & " 23:59:59#"
            Else
                cFilters = "fecha <= #" & txtFechaHasta.DateValue & "# 23:59:59"
            End If
        Else
            MsgBox "Debe ingresar una fecha v�lida.", vbExclamation + vbOKOnly, "Fecha hasta"
        End If
    End If

    ' Aplico el filtro al recordset actual y vuelvo a cargar la grilla
    If Len(cFilters) > 0 Then
        If aplRST.Filter <> cFilters Then
            aplRST.Filter = cFilters
            CargarPeticiones False, aplRST
        End If
    Else
        If Not (Len(aplRST.Filter) = adFilterNone) And aplRST.Filter <> cFilters Then
            ' Inicializa los filtros para abarcar todos los items
            aplRST.Filter = adFilterNone
            CargarPeticiones False, aplRST
        End If
    End If
Exit Sub
Errores:
    Select Case Err.Number
        Case 3001
            MsgBox "El �ltimo valor ingresado no es v�lido para realizar la actualizaci�n.", vbExclamation + vbOKOnly, "Valor inv�lido"
            aplRST.Filter = adFilterNone
            Resume Next
    End Select
End Sub

Private Sub cmdSinFiltros_Click()
    txtNroDesde.Text = ""
    txtNroHasta.Text = ""
    txtTitulo.Text = ""
    cmbTipo.ListIndex = 0
    cmbClase.ListIndex = 0
    cmbPrioridad.ListIndex = 0
    txtFechaDesde.Text = ""
    txtFechaHasta.Text = ""
    cmdActualizar_Click
    'txtNroDesde.SetFocus
End Sub

Private Sub cmdEraseNroDesde_Click()
    txtNroDesde.Text = ""
    txtNroDesde.SetFocus
End Sub

Private Sub cmdEraseNroHasta_Click()
    txtNroHasta.Text = ""
    txtNroHasta.SetFocus
End Sub

Private Sub cmdEraseTitulo_Click()
    txtTitulo.Text = ""
    txtTitulo.SetFocus
End Sub

Private Sub grdDatos_Click()
    If grdDatos.MouseRow = 0 And grdDatos.Rows > 1 Then
       grdDatos.RowSel = 1
       
       'If grdDatos.MouseCol = colFinicio Then
       '    grdDatos.Col = colFinicioSRT
       'ElseIf grdDatos.MouseCol = colFtermin Then
       '    grdDatos.Col = colFterminSRT
       'Else
        If grdDatos.MouseCol = 0 Then
            grdDatos.Col = grdDatos.MouseCol
            grdDatos.Sort = flexSortGenericAscending
        Else
            grdDatos.Sort = flexSortStringNoCaseAscending
        End If
       'End If
       'grdDatos.Sort = flexSortStringNoCaseAscending
    End If
    DoEvents
End Sub

Private Function cstm_BOM(dFecha As Date) As Date
    Dim Mes As Byte
    Dim Anio As Integer
    
    Mes = Month(dFecha)
    Anio = Year(dFecha)
    cstm_BOM = "01/" & Mes & "/" & Anio
End Function

Private Function cstm_EOM(dFecha As Date) As Date
    Dim Dia As Byte
    Dim Mes As Byte
    Dim Anio As Integer
    
    Mes = Month(dFecha)
    Anio = Year(dFecha)
    
    Select Case Mes
        Case 1, 3, 5, 7, 8, 10, 12
            Dia = 31
        Case 4, 6, 9, 11
            Dia = 30
        Case 2
            If ((Anio / 400) = 0) Or ((Anio / 4) = 0 And Not (Anio / 100) = 0) Then
                Dia = 29
            Else
                Dia = 28
            End If
    End Select
    cstm_EOM = Dia & "/" & Mes & "/" & Anio
End Function

Private Sub cmbTipo_Click()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            cmdActualizar_Click
        End If
    End If
End Sub

Private Sub cmbClase_Click()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            cmdActualizar_Click
        End If
    End If
End Sub

Private Sub cmbPrioridad_Click()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            cmdActualizar_Click
        End If
    End If
End Sub

Private Sub txtNroDesde_Change()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            Filtrar
        End If
    End If
End Sub

Private Sub txtNroHasta_Change()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            Filtrar
        End If
    End If
End Sub

Private Sub txtTitulo_Change()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            Filtrar
        End If
    End If
End Sub

Private Sub txtFechaDesde_Change()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            If Len(txtFechaDesde) > 0 Then
                If IsDate(txtFechaDesde.DateValue) Then
                    Filtrar
                End If
            End If
        End If
    End If
End Sub

Private Sub txtFechaHasta_Change()
    If Not bLoading Then
        If chkAutomaticRefresh.Value = 1 Then
            If Len(txtFechaHasta) > 0 Then
                If IsDate(txtFechaHasta.DateValue) Then
                    Filtrar
                End If
            End If
        End If
    End If
End Sub

'{ add -001- a.
Private Sub Form_Unload(Cancel As Integer)
    modUser32.DetenerScroll grdDatos
End Sub
'}
