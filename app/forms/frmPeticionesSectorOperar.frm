VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesSectorOperar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cambio Estado Sector Interviniente"
   ClientHeight    =   5505
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   8280
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5505
   ScaleWidth      =   8280
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraSector 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2985
      Left            =   60
      TabIndex        =   23
      Top             =   1875
      Width           =   8190
      Begin VB.TextBox obsResp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   1380
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   7
         Top             =   2160
         Width           =   6705
      End
      Begin VB.TextBox obsSolic 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   1380
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   32
         Top             =   1320
         Width           =   6705
      End
      Begin AT_MaskText.MaskText txtHoras 
         Height          =   315
         Left            =   1365
         TabIndex        =   2
         Top             =   240
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   556
         MaxLength       =   5
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   5
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtFechaInicio 
         Height          =   315
         Left            =   1365
         TabIndex        =   3
         Top             =   600
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaIniReal 
         Height          =   315
         Left            =   4200
         TabIndex        =   5
         Top             =   600
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaTermin 
         Height          =   315
         Left            =   1365
         TabIndex        =   4
         Top             =   960
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaFinReal 
         Height          =   315
         Left            =   4200
         TabIndex        =   6
         Top             =   960
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label lblFechaIniReal 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini Real"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   3270
         TabIndex        =   31
         Top             =   660
         Width           =   780
      End
      Begin VB.Label lblFechaFinReal 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin Real"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3270
         TabIndex        =   30
         Top             =   1020
         Width           =   795
      End
      Begin VB.Label lblSolic 
         Caption         =   "Observaci�n Nuevo Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         TabIndex        =   29
         Top             =   2160
         Width           =   1290
      End
      Begin VB.Label lblFechaTermin 
         AutoSize        =   -1  'True
         Caption         =   "F.Fin Planif"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   28
         Top             =   1020
         Width           =   885
      End
      Begin VB.Label lblFechaInicio 
         AutoSize        =   -1  'True
         Caption         =   "F.Ini Planif"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   27
         Top             =   660
         Width           =   870
      End
      Begin VB.Label lblHoras 
         AutoSize        =   -1  'True
         Caption         =   "Hs. Presup."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   26
         Top             =   300
         Width           =   930
      End
      Begin VB.Label lblResp 
         Caption         =   "Observaci�n Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   120
         TabIndex        =   25
         Top             =   1320
         Width           =   1230
      End
      Begin VB.Label lblAccion 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   8010
         TabIndex        =   24
         Top             =   180
         Width           =   45
      End
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   795
      Left            =   60
      TabIndex        =   11
      Top             =   -60
      Width           =   8190
      Begin VB.Label Label6 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   17
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   7620
         TabIndex        =   16
         Top             =   480
         Width           =   480
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1305
         TabIndex        =   15
         Top             =   240
         Width           =   6705
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1300
         TabIndex        =   14
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6705
         TabIndex        =   13
         Top             =   480
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   12
         Top             =   240
         Width           =   1200
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   495
      Left            =   60
      TabIndex        =   18
      Top             =   660
      Width           =   8190
      Begin VB.Label Label4 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   22
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoSector 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5565
         TabIndex        =   21
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label9 
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   20
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblSector 
         Caption         =   "sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1320
         TabIndex        =   19
         Top             =   180
         Width           =   2745
      End
   End
   Begin VB.ComboBox cboAccion 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1740
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1380
      Width           =   4755
   End
   Begin VB.Frame pnlBtnControl 
      Height          =   645
      Left            =   60
      TabIndex        =   0
      Top             =   4800
      Width           =   8190
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6240
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7170
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Colocar en estado:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   1440
      Width           =   1560
   End
End
Attribute VB_Name = "frmPeticionesSectorOperar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 12.07.2007 - Se agrega la nueva validaci�n de controles SOX dependiendo de la fecha de corte a definir.
' -001- b. FJS 19.07.2007 - Cuando se realice el control SOX para la documentaci�n adjunta y los conformes, no hay que realizar los controles
'                           secuencialmente sino solo al final del ciclo.
' -001- c. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para
'                           poder facilitar el reporte de errores por parte del usuario).
' -001- d. FJS 01.08.2007 - De acuerdo a definci�n de Roberto Guena: al pasar una petici�n a estado finalizado, esta debe tener SIEMPRE todos
'                           los controles de OK de alcance y documentaci�n que ya existen, independientemente del or�gen de la misma o la persona
'                           que cambia el estado (Organizaci�n o DyD), la documentaci�n exigida depender� de si dicha petici�n est� bajo los
'                           controles SOX o no.
' -001- e. FJS 03.08.2007 - Se redefinen o "reajustan" las condiciones de evaluaci�n de documentos para SOX (ver definici�n de Roberto Guena).
' -001- f. FJS 07.08.2007 - Se agrega el control del plan de implantaci�n (T700) para todas las peticiones que pasan a estado finalizado (por
'                           definici�n de Roberto Guena). Vale para aquellas identificadas como SOX como para las anteriores.
' -001- g. FJS 08.08.2007 - Obligatoriedad del documento T700 (Plan de implantaci�n): solo debe exigirse cuando el or�gen de la petici�n es DyD.
'                           Quedan exceptuadas de este control las peticiones generadas por DyD con clase: Spufi, Correctivo y Atenci�n de Usuario.
'                           Este control ser� aplicable a todas las peticiones independientemente de la fecha que tengas (no valida el campo pet_sox001)
' -002- a. FJS 13.08.2007 - Se agrega la llamada a la funci�n de exportaci�n de informaci�n a un archivo para HOST.
' -002- b. FJS 13.08.2007 - Se controla que la petici�n haya sido clasificada antes de que pueda ser pasada de estado.
' -002- c. FJS 15.08.2007 - Se controlar� que cuando una petici�n pase al estado "Finalizada" y la clase sea "Atenci�n de usuario", se requerir�
'                           el conforme del usuario (TEST).
' -003- a.                  *** NO USADO ***
' -003- b. FJS 27.08.2007 - Se agrega una nueva funci�n en el m�dulo FuncionesHOST para saber si finalmente se debe o no generar el archivo de exportaciones de peticiones para HOST.
' -004- a. FJS 03.09.2007 - Se quitan todas las validaciones respecto al campo de Clase de la petici�n quedando solo para aquellas identificadas expl�citamente denominadas como SOx.
' -005- a. FJS 26.09.2007 - Se agrega la variable de indicador de SOx al llamar al SP GetUnaPeticion y se utiliza la validaci�n por este campo en vez de por la funci�n y se elimina
'                           para la funci�n de evaluaci�n por el m�todo anterior a Sox las referencias a la clase (ya que para las anteriores peticiones no existe este atributo).
' -006- a. FJS 14.03.2008 - Se agregan los controles para guardar la fecha prevista para pasaje a producci�n del Sector.
' -007- a. FJS 26.05.2008 - Cambios para estado FINALIZADO.
' -008- a. FJS 06.08.2008 - Se reemplaza el control SOx del producto T700 por el nuevo T710. El viejo T700 queda como un documento m�s (sin controles SOx).
' -009- a. FJS 14.08.2008 - Se agrega el control para que ning�n sector pueda cambiar el estado a "En ejecuci�n" cuando la petici�n a�n no ha sido clasificada.
' -010- a. FJS 23.12.2008 - Si al cambiar el estado del grupo la petici�n alcanza alguno de los estados terminales, entonces esta petici�n deja de ser v�lida y debe ser eliminada del archivo de peticiones v�lidas.
'                           Este evento no debe ser registrado en el historial.
' -011- a. FJS 08.01.2009 - Se cambia la asignaci�n desde otro SP (para optimizar la carga del sp_GetPeticionSector).
' -012- a. FJS 18.03.2009 - Agregado de control de Homologaci�n: si la petici�n posee un conforme tipo OMA (parcial o final) el sector/grupo no puede pasar al estado FINALIZADO (definido por Claudio Lozano).
' -013- a. FJS 05.05.2009 - Cuando la petici�n alcanza el estado "En ejecuci�n" debe ser ingresada a la tabla PeticionesEnviadas2.
' -014- a. FJS 03.01.2012 - Nuevo: cuando un sector pasa al estado "Cancelado" o "Rechazo t�cnico", se elimina autom�ticamente de la planificaci�n actual
'                           (siempre que el per�odo no haya alcanzado el estado "Planificaci�n inicial" o "Seguimiento").

Option Explicit

Dim sOpcionSeleccionada As String
Dim EstadoSolicitud As String
Dim sGrupo As String
Dim sSector As String
Dim sGerencia As String
Dim sDireccion As String
Dim solGerencia As String
Dim sCoorSect As String
Dim EstadoPeticion As String
Dim NuevoEstadoPeticion As String
Dim NuevoEstadoSector As String
Dim sTipoPet As String
Dim sClasePet As String
Dim sImpacto As String
Dim sFechaComite
Dim xPerfNivel, xPerfArea As String
Dim hst_nrointerno_sol
Dim flgAccion As Boolean
'{ add -002- a.
Dim lPetNroAsignado As Long
'}
Dim bPeticionSOx As Byte     ' add -005- a.

Private Sub Form_Load()
    Call InicializarPantalla
    Call InicializarCombos
End Sub

Private Sub Form_Activate()
    Select Case sOpcionSeleccionada
        Case "OPINOK"
            obsResp.SetFocus
        Case "EVALOK"
            obsResp.SetFocus
        Case "ESTIOK", "ESTIRK"
            txtHoras.SetFocus
        Case "PLANOK", "PLANRK", "EJECRK"
            txtHoras.SetFocus
        Case "RECHTE"
            obsResp.SetFocus
        Case "EJECUC"
            txtFechaIniReal.SetFocus
        Case "CANCEL"
            obsResp.SetFocus
        Case "SUSPEN", "REVISA"
            obsResp.SetFocus
        Case "TERMIN"
            txtFechaFinReal.SetFocus
    End Select
    'StatusBarObjectIdentity Me
End Sub

Sub InicializarPantalla()
   Dim auxNro As String
    Me.Tag = ""
    Call setHabilCtrl(txtFechaTermin, "DIS")
    Call setHabilCtrl(txtFechaInicio, "DIS")
    Call setHabilCtrl(txtFechaIniReal, "DIS")
    Call setHabilCtrl(txtFechaFinReal, "DIS")
    Call setHabilCtrl(txtHoras, "DIS")
    Call setHabilCtrl(obsSolic, "DIS")
    Call setHabilCtrl(obsResp, "DIS")
    xPerfNivel = getPerfNivel(glUsrPerfilActual)
    xPerfArea = getPerfArea(glUsrPerfilActual)
    sTipoPet = ""
    cmdConfirmar.Enabled = False
    glForzarEvaluar = False
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            bPeticionSOx = ClearNull(aplRST!pet_sox001)     ' add -005- a.
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNro = "S/N"
            Else
                auxNro = ClearNull(aplRST!pet_nroasignado)
                lPetNroAsignado = CLng(auxNro)  ' add -002- a.
            End If
            lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
            lblPrioridad = ClearNull(aplRST!prioridad)
            sFechaComite = aplRST!fe_comite
            EstadoPeticion = ClearNull(aplRST!cod_estado)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
            sTipoPet = ClearNull(aplRST!cod_tipo_peticion)
            solGerencia = ClearNull(aplRST!cod_gerencia)
            '{ add -011- a.
            'glPETFechaEnvioAlBP = IIf(IsNull(aplRST!fe_comite), Format(0, "dd/mm/yyyy"), ClearNull(aplRST!fe_comite))
            sImpacto = ClearNull(aplRST!pet_imptech)
            sClasePet = ClearNull(aplRST!cod_clase)
            '}
        End If
        aplRST.Close
    End If

    fIniPlan = Null
    fFinPlan = Null
    fIniReal = Null
    fFinReal = Null
    hsPresup = 0

    If sp_GetPeticionSector(glNumeroPeticion, glSector) Then
       If Not aplRST.EOF Then
            fIniPlan = aplRST!fe_ini_plan
            fFinPlan = aplRST!fe_fin_plan
            fIniReal = aplRST!fe_ini_real
            fFinReal = aplRST!fe_fin_real
            hsPresup = Val(ClearNull(aplRST!horaspresup))
            txtFechaTermin.text = IIf(Not IsNull(fFinPlan), Format(fFinPlan, "dd/mm/yyyy"), "")
            txtFechaInicio.text = IIf(Not IsNull(fIniPlan), Format(fIniPlan, "dd/mm/yyyy"), "")
            txtHoras.text = hsPresup
            txtFechaFinReal.text = IIf(Not IsNull(fFinReal), Format(fFinReal, "dd/mm/yyyy"), "")
            txtFechaIniReal.text = IIf(Not IsNull(fIniReal), Format(fIniReal, "dd/mm/yyyy"), "")
            hst_nrointerno_sol = ClearNull(aplRST!hst_nrointerno_sol)
            EstadoSolicitud = ClearNull(aplRST!cod_estado)
            lblSector = ClearNull(aplRST!cod_sector) & " : " & ClearNull(aplRST!nom_sector)
            lblEstadoSector = ClearNull(aplRST!nom_estado)
            sGerencia = ClearNull(aplRST!cod_gerencia)
            sDireccion = ClearNull(aplRST!cod_direccion)
            sSector = ClearNull(aplRST!cod_sector)
        End If
        'aplRST.Close
    End If
    If Val(hst_nrointerno_sol) > 0 Then
        obsSolic.text = sp_GetHistorialMemo(hst_nrointerno_sol)
    Else
        obsSolic.text = ""
    End If
    obsResp.text = ""
End Sub

Sub InicializarCombos()
    Dim flgExtendido As Boolean
    Dim xAccion As String
    Call Status("Cargando acciones...")
    'si tiene ingerencia en el area del sector
    flgExtendido = False
    If xPerfNivel = "BBVA" Or _
        xPerfNivel = "DIRE" And sDireccion = xPerfArea Or _
        xPerfNivel = "GERE" And sGerencia = xPerfArea Or _
        xPerfNivel = "SECT" And sSector = xPerfArea Then
        'If sp_GetAccionPerfil("SCHGEST", glUsrPerfilActual, EstadoSolicitud, Null, Null) Then
        If sp_GetAccionPerfilEstados(1, "SCHGEST", glUsrPerfilActual, EstadoSolicitud, Null, Null) Then
            Do While Not aplRST.EOF
                cboAccion.AddItem aplRST!nom_estnew & Space(60) & "||" & aplRST!cod_estnew
                If InStr(1, "OPINOK|EVALOK", ClearNull(aplRST!cod_estnew)) = 0 Then
                    flgExtendido = True
                End If
                aplRST.MoveNext
            Loop
            aplRST.Close
        End If
    End If
    
    If flgExtendido = False Then
        txtFechaTermin.visible = False
        txtFechaInicio.visible = False
        txtFechaIniReal.visible = False
        txtFechaFinReal.visible = False
        txtHoras.visible = False
        lblFechaTermin.visible = False
        lblFechaInicio.visible = False
        lblFechaIniReal.visible = False
        lblFechaFinReal.visible = False
        lblHoras.visible = False
    End If
    
    Call Status("")
    If cboAccion.ListCount > 0 Then
        Call setHabilCtrl(cboAccion, "OBL")
        If cboAccion.ListCount = 1 Then
            cboAccion.ListIndex = 0
           Call setHabilCtrl(cboAccion, "DIS")
        End If
    Else
        Call setHabilCtrl(cboAccion, "DIS")
    End If
End Sub

Private Sub cboAccion_Click()
    If cboAccion.ListIndex < 0 Then
        MsgBox ("Debe seleccionar una Acci�n")
        Exit Sub
    End If
    txtFechaTermin.text = IIf(Not IsNull(fFinPlan), Format(fFinPlan, "dd/mm/yyyy"), "")
    txtFechaInicio.text = IIf(Not IsNull(fIniPlan), Format(fIniPlan, "dd/mm/yyyy"), "")
    txtHoras.text = hsPresup
    txtFechaFinReal.text = IIf(Not IsNull(fFinReal), Format(fFinReal, "dd/mm/yyyy"), "")
    txtFechaIniReal.text = IIf(Not IsNull(fIniReal), Format(fIniReal, "dd/mm/yyyy"), "")
    
    sOpcionSeleccionada = CodigoCombo(Me.cboAccion, True)
    Habilitar
End Sub

Private Sub cmdConfirmar_Click()
    Dim AuxNroHistorial As Long
    Dim NroHistorial As Long
    Dim xAccion As String
    Dim sMsgPeticion As String
    Dim flgIntegrar As Boolean
    Dim Cantidad As Integer
    Dim bContinue As Boolean        'add -001- a.
    '{ add -014- a.
    Dim per_nrointerno As Long
    Dim per_estado As String
    '}
    
    bContinue = False               'add -001- a.
    
    If sClasePet = "SINC" And _
        (sOpcionSeleccionada = "TERMIN" Or _
         sOpcionSeleccionada = "EJECUC" Or _
         sOpcionSeleccionada = "EJECRK") Then
            MsgBox "Antes de cambiar al estado seleccionado, la petici�n debe ser clasificada.", vbExclamation + vbOKOnly, "Clasificaci�n de la Petici�n"
            Exit Sub
    End If
    
    If cboAccion.ListIndex < 0 Then
        MsgBox ("Debe seleccionar una acci�n.")
        Exit Sub
    End If
    Dim flagPLANRK As Integer: flagPLANRK = 0

    'If CamposObligatorios Then     ' del -001- a.
    '{ add -001- a.
    If bPeticionSOx = 1 Then    ' add -005- a.
        '{ add -007- a.
        If sGerencia = "DESA" Then
            If CamposObligatorios_Standard And _
               CamposObligatorios_SOx And _
               CamposObligatorios_Fechas And _
               CamposObligatorios_Horas And _
               CamposObligatorios_Homologacion Then     ' upd -012- a. Se agrega la funci�n CamposObligatorios_Homologacion
                    bContinue = True
            End If
        Else
            If CamposObligatorios_Standard And _
               CamposObligatorios_Fechas Then
                    bContinue = True
            End If
        End If
        '}
        'bContinue = CamposObligatorios_SOX_Nuevo    ' Entonces se aplican los nuevos controles SOX     ' del -007- a.
    Else
        bContinue = CamposObligatorios              ' Entonces se aplican los controles anteriores a SOX
    End If
    
    If bContinue Then
    '}
        If sOpcionSeleccionada = "PLANOK" And (EstadoSolicitud = "ESTIMA" Or EstadoSolicitud = "ESTIOK") Then
            If MsgBox("De existir otros sectores, esta Planificaci�n los forzar� a planificar. �Confirma?", vbYesNo) = vbNo Then
                Exit Sub
            End If
        End If
        If sOpcionSeleccionada = "RECHTE" Then
            If MsgBox("Si corresponde asignar la Petici�n a otro sector" & Chr(13) & "h�galo antes de informar su Rechazo T�cnico." & Chr(13) & "De lo contrario, la Petici�n puede quedar definitivamente rechazada" & Chr(13) & Chr(13) & " �Confirma el Cambio de Estado?", vbYesNo + vbExclamation + vbDefaultButton2, "Cambio a Rechazo T�cnico") = vbNo Then
                Exit Sub
            End If
        End If
        
        'cuenta todos los grupos
        Cantidad = 0
        Select Case sOpcionSeleccionada
            Case "PLANIF", "PLANOK", "PLANRK", "ESTIMA", "ESTIOK", "ESTIRK", "EJECUC", "EJECRK", "TERMIN"
                Call chkEstadoGrupo(glNumeroPeticion, glSector, Null, "NULL", Cantidad)
                If Cantidad > 1 Then
                    If MsgBox("Esta operaci�n cambiar� los estados y las fechas" & Chr(13) & Chr(9) & "de los grupos intervinientes." & Chr(13) & Chr(13) & Chr(9) & Chr(9) & "�Confirma?", vbYesNo) = vbNo Then
                        Call Status("Listo")
                        Exit Sub
                    End If
                End If
        End Select
        
        cmdConfirmar.Enabled = False
        cmdCerrar.Enabled = False
        sMsgPeticion = ""
        NuevoEstadoSector = sOpcionSeleccionada
        Select Case sOpcionSeleccionada
            Case "OPINOK"
                obsResp.text = "� OPINION � " & vbCrLf & obsResp.text
            Case "EVALOK"
                obsResp.text = "� EVALUACION � " & vbCrLf & obsResp.text
            Case "ESTIOK"
                obsResp.text = "� ESTIMACION � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector pasa a ESTIMADO"
            Case "PLANOK"
                obsResp.text = "� PLANIFICACION Inicio Planif: " & txtFechaInicio.text & " Finalizaci�n Planif: " & txtFechaTermin.text & " Horas Planif: " & txtHoras.text & " � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector pasa a Planificado"
            Case "ESTIRK"
                obsResp.text = "� RE-ESTIMACION � " & vbCrLf & obsResp.text
                'sOpcionSeleccionada = "ESTIOK"
                NuevoEstadoSector = "ESTIOK"
            Case "PLANRK"
                obsResp.text = "� RE-PLANIFICACION Inicio Planif: " & txtFechaInicio.text & " Finalizaci�n Planif: " & txtFechaTermin.text & " Horas Planif: " & txtHoras.text & " � " & vbCrLf & obsResp.text
                'sOpcionSeleccionada = "PLANOK"
                NuevoEstadoSector = "PLANOK"
            Case "EJECUC"
                obsResp.text = "� EJECUCION Inicio Planif: " & txtFechaInicio.text & " Finalizaci�n Planif: " & txtFechaTermin.text & " Horas Planif: " & txtHoras.text & " � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector fu� puesto en EJECUCION"
            Case "EJECRK"
                obsResp.text = "� RE-PLANIFICACION Inicio Planif: " & txtFechaInicio.text & " Finalizaci�n Planif: " & txtFechaTermin.text & " Horas Planif: " & txtHoras.text & " � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector fu� re-planificado"
                NuevoEstadoSector = "EJECUC"
            Case "RECHTE"
                obsResp.text = "� RECHAZO � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector fu� Rechazado Tecnicamente"
            Case "CANCEL"
                obsResp.text = "� CANCELADO � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector fu� Cancelado"
            Case "SUSPEN"
                obsResp.text = "� SUSPENSION � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector fu� Suspendido"
            Case "REVISA"
                obsResp.text = "� A REVISAR � " & vbCrLf & obsResp.text
            Case "TERMIN"
                obsResp.text = "� FINALIZACION Inicio Planif: " & txtFechaInicio.text & " Finalizaci�n Planif: " & txtFechaTermin.text & " Horas Planif: " & txtHoras.text & " � " & vbCrLf & obsResp.text
                sMsgPeticion = "El sector pasa a FINALIZADO"
            Case "PLANIF"
                obsResp.text = "� A PLANIFICAR � " & vbCrLf & obsResp.text
            Case "ESTIMA"
                obsResp.text = "� A ESTIMAR � " & vbCrLf & obsResp.text
        End Select
        Call Puntero(True)
        Call Status("Procesando...")
        NuevoEstadoPeticion = EstadoPeticion
        flgIntegrar = False
        Select Case sOpcionSeleccionada
            Case "CANCEL", "RECHTE", "SUSPEN", "REVISA"
                'solamente  hereda el estado a los grupos
                Call sp_HeredaEstadoGrupo(glNumeroPeticion, glSector, NuevoEstadoSector, Null, Null, Null, Null, 0, "OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA", "E", 0)
                '''    Case "SUSPEN"
                '''        'hereda el estado y fechas null a los grupos
                '''        Call sp_HeredaEstadoGrupo(glNumeroPeticion, glSector, NuevoEstadoSector, Null, Null, Null, Null, 0, "OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN", "A", 0)
            Case "PLANIF", "PLANOK", "PLANRK", "ESTIMA", "ESTIOK", "ESTIRK", "EJECUC", "EJECRK", "TERMIN"
                If Cantidad = 1 Then
                    'entonces debe heredar TODO horas,fecha y estado
                    Call sp_HeredaEstadoGrupo(glNumeroPeticion, glSector, NuevoEstadoSector, txtFechaInicio.DateValue, txtFechaTermin.DateValue, txtFechaIniReal.DateValue, txtFechaFinReal.DateValue, txtHoras, "NULL", "H", 0)
                Else
                    'para todos menos TERMIN
                    Call sp_HeredaEstadoGrupo(glNumeroPeticion, glSector, NuevoEstadoSector, txtFechaInicio.DateValue, txtFechaTermin.DateValue, txtFechaIniReal.DateValue, txtFechaFinReal.DateValue, 0, "OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA", "A", 0)
                    'para TERMIN, una herencia Especial de fechas que deje coherente las inicio/fin ejecuci�n
                    Call sp_HeredaEstadoGrupo(glNumeroPeticion, glSector, NuevoEstadoSector, txtFechaInicio.DateValue, txtFechaTermin.DateValue, txtFechaIniReal.DateValue, txtFechaFinReal.DateValue, 0, "TERMIN", "X", 0)
                End If
        End Select
        'elimina todos los mensajes para ese sector
        Call sp_DelMensajePerfil(glNumeroPeticion, "CSEC", "SECT", glSector)
        
        'elimina todos los mensajes para los grupos de ese sector
        Call sp_DelMensajePerfil(glNumeroPeticion, "HSEC", "SECT", glSector)
        
        If sOpcionSeleccionada = "REVISA" Then
            Call sp_DoMensaje("SEC", "REVIGRU", glNumeroPeticion, glSector, "REVISA", "", "")
        End If
        NroHistorial = sp_AddHistorial(glNumeroPeticion, "SCHGEST", EstadoPeticion, glSector, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, obsResp.text)
        Call sp_UpdatePeticionSector(glNumeroPeticion, glSector, txtFechaInicio.DateValue, txtFechaTermin.DateValue, txtFechaIniReal.DateValue, txtFechaFinReal.DateValue, CInt(Val(txtHoras.text)), NuevoEstadoSector, date, "", NroHistorial, 0)
        Select Case sOpcionSeleccionada
            Case "OPINOK", "EVALOK"
                flgIntegrar = False
                Call chkSectorOpiEva(glNumeroPeticion)
                'ELIMIRA LOS GRUPOS EN CUALQUIER ESTADO !!!!!
                Call sp_DeletePeticionGrupo(glNumeroPeticion, glSector, Null)
                'If glEstadoPeticion = "COMITE" Then
                If InStr(1, "COMITE|REFBPE|", glEstadoPeticion, vbTextCompare) > 0 Then
                    NuevoEstadoPeticion = glEstadoPeticion
                End If
            Case "EJECUC"
                flgIntegrar = True
            Case "EJECRK"
                flgIntegrar = True
            Case "PLANOK", "ESTIOK", "ESTIRK", "PLANRK"
                flgIntegrar = True
                'si fue una planificaci�n forzada, exige a los demas sectores
                'que planifiquen
                If sOpcionSeleccionada = "PLANOK" And (EstadoSolicitud = "ESTIMA" Or EstadoSolicitud = "ESTIOK") Then
                    AuxNroHistorial = sp_AddHistorial(glNumeroPeticion, "SFRZPLN", "PLANIF", Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� La Peticion y los sectores se fuerzan -A Planificar- �")
                    Call sp_FuerzaEstadoPlanok(glNumeroPeticion, AuxNroHistorial)
                End If
            Case "CANCEL", "RECHTE", "TERMIN"
                flgIntegrar = True
            Case "SUSPEN", "REVISA"
                flgIntegrar = True
            Case "PLANIF", "ESTIMA"
                flgIntegrar = True
        End Select
        
        If flgIntegrar Then
            'integra fechas del sector
            Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
            'averigua
            NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
            Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
            'Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)     ' Ver por qu� Cancela!!! 03.04.2012
            If NuevoEstadoPeticion <> "TERMIN" Then
                fFinReal = Null
            End If
            If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoPeticion) > 0 Then
                Call sp_UpdatePetFechas(glNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
            Else
                Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
            End If
        End If
        Call sp_UpdHistorial(NroHistorial, "SCHGEST", NuevoEstadoPeticion, glSector, NuevoEstadoSector, Null, Null, glLOGIN_ID_REEMPLAZO)
        '{ add -010- a. El nuevo estado alcanzado por la petici�n es alguno de los estados terminales
        If InStr(1, "TERMIN|ANEXAD|ANULAD|CANCEL|RECHTE|RECHAZ", NuevoEstadoPeticion, vbTextCompare) > 0 Then
            ' Elimino la petici�n de la tabla de PeticionesEnviadas (deja de ser v�lida para pasajes a Producci�n)
            Call sp_DeletePeticionChangeMan(glNumeroPeticion)
            Call sp_DeletePeticionEnviadas(glNumeroPeticion)
        End If
        '}
        '{ add -013- a.
        If NuevoEstadoPeticion = "EJECUC" Then
            ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
            FuncionesHOST.sp_InsertPeticionEnviadas2 glNumeroPeticion, CLng(lPetNroAsignado), sClasePet, "", ""
        Else
            FuncionesHOST.sp_DeletePeticionEnviadas2 glNumeroPeticion
        End If
        '}
        Call Puntero(False)
        Call Status("Listo.")
        Call touchForms
        Unload Me
    End If
End Sub

'{ add -012- a.
Private Function CamposObligatorios_Homologacion() As Boolean
    If sOpcionSeleccionada = "TERMIN" Then
        If sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Or _
           sp_GetPeticionConf(glNumeroPeticion, "HOMA.F", Null) Then
                MsgBox "El sector no puede cambiar al estado FINALIZADO." & Chr(13) & "La petici�n tiene un NO conforme de homologaci�n.", vbOKOnly + vbInformation
                CamposObligatorios_Homologacion = False
                If Not cmdConfirmar.Enabled Then cmdConfirmar.Enabled = True
                If Not cmdCerrar.Enabled Then cmdCerrar.Enabled = True
                Exit Function
        End If
    End If
    CamposObligatorios_Homologacion = True
End Function
'}

Sub Habilitar()
    cmdConfirmar.Enabled = True
    cmdCerrar.Enabled = True
    fraSector.Enabled = True
    lblAccion = ""
    
    Call setHabilCtrl(txtFechaTermin, "DIS")
    Call setHabilCtrl(txtFechaInicio, "DIS")
    Call setHabilCtrl(txtFechaIniReal, "DIS")
    Call setHabilCtrl(txtFechaFinReal, "DIS")
    Call setHabilCtrl(txtHoras, "DIS")
    Call setHabilCtrl(obsSolic, "DIS")
    Call setHabilCtrl(obsResp, "DIS")
            
    Select Case sOpcionSeleccionada
    Case "RECHTE", "OPINOK", "EVALOK", "CANCEL", "SUSPEN", "REVISA", "PLANRK", "ESTIRK", "EJECRK"
        Call setHabilCtrl(obsResp, "OBL")
    Case Else
        Call setHabilCtrl(obsResp, "NOR")
    End Select
            
    Select Case sOpcionSeleccionada
        Case "OPINOK"
            lblAccion = "OPINAR"
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
        Case "EVALOK"
            lblAccion = "EVALUAR"
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
        Case "ESTIOK"
            lblAccion = "ESTIMAR"
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
        Case "ESTIRK"
            lblAccion = "ESTIMAR"
            Call setHabilCtrl(txtHoras, "OBL")
        Case "PLANIF", "ESTIMA"
            lblAccion = ""
            txtFechaTermin.text = ""
            txtFechaInicio.text = ""
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
            txtHoras.text = ""
        Case "PLANOK"
            lblAccion = "PLANIFICAR"
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
            txtFechaIniReal.text = ""
        Case "PLANRK"
            lblAccion = "PLANIFICAR"
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
        Case "EJECRK"
            lblAccion = "PLANIFICAR"
            Call setHabilCtrl(txtFechaIniReal, "OBL")
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
        Case "RECHTE"
            lblAccion = "RECHAZAR"
        Case "EJECUC"
            lblAccion = "EJECUTAR"
            Call setHabilCtrl(txtFechaIniReal, "OBL")
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
            txtFechaFinReal.text = ""
            If txtFechaIniReal.text = "" And txtFechaInicio.text <> "" Then
                txtFechaIniReal.text = txtFechaInicio.text
            End If
        Case "CANCEL"
            lblAccion = "CANCELAR"
            Call setHabilCtrl(obsResp, "OBL")
        Case "SUSPEN"
            lblAccion = "SUSPENDER"
        Case "REVISA"
            lblAccion = "REVISAR"
        Case "TERMIN"
            lblAccion = "FINALIZAR"
            Call setHabilCtrl(txtFechaFinReal, "OBL")
            Call setHabilCtrl(txtFechaIniReal, "OBL")
            Call setHabilCtrl(txtFechaTermin, "OBL")
            Call setHabilCtrl(txtFechaInicio, "OBL")
            Call setHabilCtrl(txtHoras, "OBL")
            If txtFechaIniReal.text = "" And txtFechaInicio.text <> "" Then
                txtFechaIniReal.text = txtFechaInicio.text
            End If
        Case Else
            lblAccion = sOpcionSeleccionada
    End Select
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = False
    ' mov -001-
    'nuevo requerimiento Susana Dopazo SOX
    If (sTipoPet = "PRJ") And (sOpcionSeleccionada = "PLANOK" Or sOpcionSeleccionada = "TERMIN" Or sOpcionSeleccionada = "EJECUC") Then
        If sGerencia = "DESA" Then
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA", Null, Null) Then
                MsgBox "El sector no puede pasar PLANIFICADO, EJECUCION o FINALIZADO," & Chr(13) & "Falta el documento de alcance.", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            Else
                aplRST.Close
            End If
        End If
    End If
    
    '{ add -001- f.
    If sOpcionSeleccionada = "TERMIN" And sGerencia = "DESA" Then   ' upd -001- f. - Solo exigible a DyD
        If Not sp_GetAdjuntosPet(glNumeroPeticion, "PLIM", Null, Null) Then
            MsgBox "El sector no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el documento Plan de implantaci�n (PLIM).", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        Else
            aplRST.Close
        End If
        'End If ' del -005- a.
    End If
    '}
    
    If sOpcionSeleccionada = "TERMIN" Then
        If sGerencia = "DESA" Then
            If Not sp_VerPeticionHoras(glNumeroPeticion) Then
                MsgBox "El sector no puede pasar a FINALIZADO," & Chr(13) & "No posee horas trabajadas informadas", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            Else
                aplRST.Close
            End If
        End If
    End If
    
'''    esto va, de acuerdo a mail/telefonico/patrignani-efron
    If sOpcionSeleccionada = "TERMIN" And sFechaComite >= CDate("2006/09/30") Then
        If sTipoPet = "NOR" Or sTipoPet = "ESP" Or sTipoPet = "PRJ" Then
            If sGerencia = "DESA" Then
                If Not sp_GetPeticionConf(glNumeroPeticion, "TEST", 1) Then
                    MsgBox "El sector no puede cambiar el estado a FINALIZADO," & Chr(13) & "Falta el Ok de usuario.", vbOKOnly + vbInformation
                    CamposObligatorios = False
                    Exit Function
                Else
                    aplRST.Close
                End If
                '{ del -001- f.
                'If Not sp_GetAdjuntosPet(glNumeroPeticion, "PLIM") Then
                '    MsgBox "El sector no puede pasar a FINALIZADO," & Chr(13) & "Falta el plan de implantaci�n", vbOKOnly + vbInformation
                '    CamposObligatorios = False
                '    Exit Function
                'Else
                '    aplRST.Close
                'End If
                '}
            End If
        End If
        'requerimiento Susana Dopazo 06/03/2007 porque Organizacion genera propias para DESA
        If sTipoPet = "PRO" Then
            If solGerencia <> "DESA" And sGerencia = "DESA" Then
                If Not sp_GetPeticionConf(glNumeroPeticion, "TEST", 1) Then
                    MsgBox "El sector no puede cambiar el estado a FINALIZADO," & Chr(13) & "Falta el Ok de usuario.", vbOKOnly + vbInformation
                    CamposObligatorios = False
                    Exit Function
                Else
                    aplRST.Close
                End If
                '{ del -001- f.
                'If Not sp_GetAdjuntosPet(glNumeroPeticion, "PLIM") Then
                '    MsgBox "El sector no puede pasar a FINALIZADO," & Chr(13) & "Falta el plan de implantaci�n", vbOKOnly + vbInformation
                '    CamposObligatorios = False
                '    Exit Function
                'Else
                '    aplRST.Close
                'End If
                '}
            End If
        End If
    End If
    
    Select Case sOpcionSeleccionada
        Case "RECHTE", "OPINOK", "EVALOK", "CANCEL", "SUSPEN", "REVISA", "PLANRK", "ESTIRK", "EJECRK"
            If Trim(Me.obsResp.text) = "" Then
                MsgBox "Se deben ingresar observaciones", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
    End Select
    
    Select Case sOpcionSeleccionada
        Case "ESTIOK", "ESTIRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
        Case "PLANOK", "PLANRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaInicio.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaTermin.DateValue) Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
        Case "EJECUC", "EJECRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaIniReal.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio real", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaInicio.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaTermin.DateValue) Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
        Case "TERMIN"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaIniReal.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio real", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaInicio.DateValue) Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaFinReal.DateValue) Then
                MsgBox "Se debe informar Fecha de Fin Real", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
            If Not IsDate(txtFechaTermin.DateValue) Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios = False
                Exit Function
            End If
    End Select
    If (Not IsNull(txtFechaInicio.DateValue)) And (Not IsNull(txtFechaTermin.DateValue)) Then
        If txtFechaTermin.DateValue < txtFechaInicio.DateValue Then
            MsgBox "La Fecha Inicio Plan. no debe ser mayor a la Fecha Fin Plan.", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If (Not IsNull(txtFechaIniReal.DateValue)) And (Not IsNull(txtFechaFinReal.DateValue)) Then
        If txtFechaFinReal.DateValue < txtFechaIniReal.DateValue Then
            MsgBox "La Fecha Inicio Real no debe ser mayor a la Fecha Fin Real", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    End If
    If txtFechaIniReal.DateValue > date Then
        MsgBox "La Fecha Inicio Real no debe ser mayor a la Fecha del D�a.", vbOKOnly + vbInformation
        CamposObligatorios = False
        Exit Function
    End If
    If txtFechaFinReal.DateValue > date Then
        MsgBox "La Fecha Fin Real no debe ser mayor a la Fecha del D�a.", vbOKOnly + vbInformation
        CamposObligatorios = False
        Exit Function
    End If
    CamposObligatorios = True
End Function

' mov -002- a.

'{ add -007- a.
Private Function CamposObligatorios_Standard() As Boolean
    CamposObligatorios_Standard = False
    
    ' Controles para observaciones en el cambio de estado
    Select Case sOpcionSeleccionada
        Case "RECHTE", "OPINOK", "EVALOK", "CANCEL", "SUSPEN", "REVISA", "PLANRK", "ESTIRK", "EJECRK"
            If Trim(Me.obsResp.text) = "" Then
                MsgBox "Se deben ingresar observaciones", vbOKOnly + vbInformation
                CamposObligatorios_Standard = False
                Exit Function
            End If
    End Select
    CamposObligatorios_Standard = True
End Function

Private Function CamposObligatorios_SOx() As Boolean
    Dim ExisteDocumentoAlcance As Boolean
    CamposObligatorios_SOx = False
    
    If sOpcionSeleccionada = "TERMIN" Then
        ' Control de Documento de Alcance: C100
        If sTipoPet = "PRJ" Or UCase(sImpacto) = "S" Then
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                MsgBox "El sector no puede pasar al estado FINALIZADO." & Chr(13) & "Falta el documento de alcance de tipo C100.", vbOKOnly + vbInformation
                CamposObligatorios_SOx = False
                Exit Function
            End If
            ExisteDocumentoAlcance = True
        End If
        ' Control de Documento de Alcance: P950
        If sTipoPet <> "PRJ" Then
            If sImpacto = "N" Then
                If InStr(1, "EVOL|OPTI|NUEV|", sClasePet, vbTextCompare) > 0 Then
                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
                        MsgBox "El sector no puede pasar al estado FINALIZADO." & Chr(13) & "Falta el documento de alcance de tipo P950.", vbOKOnly + vbInformation
                        CamposObligatorios_SOx = False
                        Exit Function
                    End If
                End If
            End If
            ExisteDocumentoAlcance = True
        End If
        ' Control de Conforme final de pruebas del usuario: TEST
        If Not sp_GetPeticionConf(glNumeroPeticion, "TEST", 1) Then
            MsgBox "El sector no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el conforme de prueba de usuario (TEST).", vbOKOnly + vbInformation
            CamposObligatorios_SOx = False
            Exit Function
        End If
        ' Control de documento de casos de prueba: C204
        If Not InStr(1, "SPUF|CORR|ATEN", sClasePet, vbTextCompare) > 0 Then
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "C204", Null, Null) Then
                MsgBox "El sector no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el documento de casos de prueba (C204).", vbOKOnly + vbInformation
                CamposObligatorios_SOx = False
                Exit Function
            End If
        End If
        ' Control del Conforme al documento de alcance: ALCA
        If ExisteDocumentoAlcance Then
            If InStr(1, "EVOL|OPTI|NUEV|", sClasePet, vbTextCompare) > 0 Then
                If Not sp_GetPeticionConf(glNumeroPeticion, "ALCA", 1) Then
                    MsgBox "El sector no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el Conforme al documento de alcance (ALCA)", vbOKOnly + vbInformation
                    CamposObligatorios_SOx = False
                    Exit Function
                End If
            End If
        End If
        '{ add -008- a.
        If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then   ' add -008- a.
            If Not sp_GetAdjuntosPet(glNumeroPeticion, "T710", Null, Null) Then
                MsgBox "El sector no puede cambiar al estado FINALIZADO." & Chr(13) & "Falta el documento Principios de Desarrollo Aplicativo (T710).", vbOKOnly + vbInformation
                CamposObligatorios_SOx = False
                Exit Function
            End If
        End If
        '}
    End If
    CamposObligatorios_SOx = True
End Function

Private Function CamposObligatorios_Fechas() As Boolean
    CamposObligatorios_Fechas = False
    
    ' Controles sobre cantidad de horas informadas y fechas de inicio y finalizaci�n
    Select Case sOpcionSeleccionada
        Case "ESTIOK", "ESTIRK"
            If Val(txtHoras.text) = 0 Then
               MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
               CamposObligatorios_Fechas = False
               Exit Function
            End If
        Case "PLANOK", "PLANRK"
            If Val(txtHoras.text) = 0 Then
               MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
               CamposObligatorios_Fechas = False
               Exit Function
            End If
            If txtFechaInicio.text = "" Then
               MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
               CamposObligatorios_Fechas = False
               Exit Function
            End If
            If txtFechaTermin.text = "" Then
               MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
               CamposObligatorios_Fechas = False
               Exit Function
            End If
        Case "EJECUC", "EJECRK"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
            If txtFechaIniReal.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Real", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
            If txtFechaInicio.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
            If txtFechaTermin.text = "" Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
        Case "TERMIN"
            If Val(txtHoras.text) = 0 Then
                MsgBox "Se deben informar las horas", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
            If txtFechaFinReal.text = "" Then
                MsgBox "Se debe informar Fecha de Fin Real", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
            If txtFechaIniReal.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Real", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
            If txtFechaInicio.text = "" Then
                MsgBox "Se debe informar Fecha de Inicio Planificada", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
            If txtFechaTermin.text = "" Then
                MsgBox "Se debe informar Fecha de Fin Planificada", vbOKOnly + vbInformation
                CamposObligatorios_Fechas = False
                Exit Function
            End If
    End Select
    ' Control de fechas de inicio y de finalizaci�n
    If (Not IsNull(txtFechaInicio.DateValue)) And (Not IsNull(txtFechaTermin.DateValue)) Then
        If txtFechaTermin.DateValue < txtFechaInicio.DateValue Then
            MsgBox "La Fecha Inicio Plan. no debe ser mayor a la Fecha Fin Plan.", vbOKOnly + vbInformation
            CamposObligatorios_Fechas = False
            Exit Function
        End If
    End If
    ' Control de fechas de inicio real y de finalizaci�n real
    If (Not IsNull(txtFechaIniReal.DateValue)) And (Not IsNull(txtFechaFinReal.DateValue)) Then
        If txtFechaFinReal.DateValue < txtFechaIniReal.DateValue Then
            MsgBox "La Fecha Inicio Real no debe ser mayor a la Fecha Fin Real", vbOKOnly + vbInformation
            CamposObligatorios_Fechas = False
            Exit Function
        End If
    End If
    ' Control de fecha de inicio real mayor al today
    If txtFechaIniReal.DateValue > date Then
        MsgBox "La Fecha Inicio Real no debe ser mayor a la Fecha del D�a.", vbOKOnly + vbInformation
        CamposObligatorios_Fechas = False
        Exit Function
    End If
    ' Control de fecha de finalizaci�n real mayor al today
    If txtFechaFinReal.DateValue > date Then
        MsgBox "La Fecha Fin Real no debe ser mayor a la Fecha del D�a.", vbOKOnly + vbInformation
        CamposObligatorios_Fechas = False
        Exit Function
    End If
    CamposObligatorios_Fechas = True
End Function

Private Function CamposObligatorios_Horas() As Boolean
    CamposObligatorios_Horas = False
    
    If sOpcionSeleccionada = "TERMIN" Then
        If Not sp_VerPeticionHoras(glNumeroPeticion) Then
            MsgBox "El sector no puede pasar a FINALIZADO," & Chr(13) & "No posee horas trabajadas informadas", vbOKOnly + vbInformation
            CamposObligatorios_Horas = False
            Exit Function
        End If
    End If
    CamposObligatorios_Horas = True
End Function
'}

Private Sub txtFechaIniReal_LostFocus()
    If txtFechaInicio.Enabled = True And txtFechaInicio.text = "" And txtFechaIniReal.text <> "" Then
        txtFechaInicio.text = txtFechaIniReal.text
    End If
End Sub

Private Sub txtFechaFinReal_LostFocus()
    If txtFechaTermin.Enabled = True And txtFechaTermin.text = "" And txtFechaFinReal.text <> "" Then
        txtFechaTermin.text = txtFechaFinReal.text
    End If
End Sub

Private Sub txtFechaInicio_LostFocus()
    If txtFechaIniReal.Enabled = True And txtFechaIniReal.text = "" And txtFechaInicio.text <> "" Then
        txtFechaIniReal.text = txtFechaInicio.text
    End If
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub
