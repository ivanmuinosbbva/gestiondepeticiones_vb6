VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProyectosIDMShell 
   Caption         =   "Trabajar con proyectos"
   ClientHeight    =   7995
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11745
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmProyectosIDMShell.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   7995
   ScaleWidth      =   11745
   WindowState     =   2  'Maximized
   Begin VB.Frame fraOpcionesExportacion 
      Caption         =   " Opciones de generaci�n "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   9360
      TabIndex        =   26
      Top             =   60
      Width           =   2350
      Begin VB.CheckBox chkHitosExposicion 
         Caption         =   "Solo hitos con exposici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   180
         TabIndex        =   30
         Top             =   1080
         Width           =   2115
      End
      Begin VB.CheckBox chkExportarHitos 
         Caption         =   "Incluir detalle de hitos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   180
         TabIndex        =   29
         Top             =   840
         Width           =   2055
      End
      Begin VB.OptionButton optExpo 
         Caption         =   "Exportar proyectos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.OptionButton optExpo 
         Caption         =   "Generar fichas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   27
         Top             =   480
         Width           =   1695
      End
      Begin MSComctlLib.ImageCombo imgFichaFormato 
         Height          =   330
         Left            =   960
         TabIndex        =   31
         Top             =   1500
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "imgFichaFormato"
         ImageList       =   "imgExportacion"
      End
      Begin VB.Label lblProyIDM 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Formato:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   32
         Top             =   1500
         Width           =   660
      End
   End
   Begin VB.Frame fraBotones 
      Height          =   5895
      Left            =   10620
      TabIndex        =   17
      Top             =   2050
      Width           =   1095
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   13
         Top             =   5460
         Width           =   945
      End
      Begin VB.CommandButton cmdImportar 
         Caption         =   "Importar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   25
         Top             =   5100
         Visible         =   0   'False
         Width           =   945
      End
      Begin VB.CommandButton cmdActualizar 
         Caption         =   "Actualizar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   24
         ToolTipText     =   "Actualizar los datos de la grilla"
         Top             =   3720
         Width           =   945
      End
      Begin VB.CommandButton cmdExportar 
         Caption         =   "Generar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   12
         ToolTipText     =   "Generar exportaci�n de proyectos o fichas de proyectos"
         Top             =   2340
         Width           =   945
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   11
         Top             =   1620
         Width           =   945
      End
      Begin VB.CommandButton cmdBaja 
         Caption         =   "Baja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   10
         ToolTipText     =   "Generar la alta/baja l�gica del proyecto seleccionado"
         Top             =   1260
         Width           =   945
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Editar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   9
         Top             =   900
         Width           =   945
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Nuevo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   8
         Top             =   540
         Width           =   945
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "Ver"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   7
         Top             =   180
         Width           =   945
      End
   End
   Begin VB.Frame fraOpciones 
      Caption         =   " Opciones de filtro "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   60
      TabIndex        =   14
      Top             =   60
      Width           =   9255
      Begin VB.ComboBox cboArea 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1020
         Width           =   8175
      End
      Begin VB.OptionButton optFiltroArea 
         Caption         =   "�rea propietaria"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   5880
         TabIndex        =   22
         Top             =   1440
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.OptionButton optFiltroArea 
         Caption         =   "�rea responsable"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   7440
         TabIndex        =   21
         Top             =   1440
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.ComboBox cmbSoloActivos 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8100
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   592
         Width           =   1020
      End
      Begin VB.ComboBox cmbPlanTyO 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8100
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   232
         Width           =   1020
      End
      Begin VB.TextBox txtPrjNom 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         TabIndex        =   3
         Top             =   660
         Width           =   5295
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   285
         Left            =   960
         TabIndex        =   0
         Top             =   300
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   503
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   285
         Left            =   1920
         TabIndex        =   2
         Top             =   300
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   285
         Left            =   1500
         TabIndex        =   1
         Top             =   300
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "�rea propietaria"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   1
         Left            =   120
         TabIndex        =   33
         Top             =   960
         Width           =   810
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblPerfil 
         AutoSize        =   -1  'True
         Caption         =   "lblPerfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   1500
         Width           =   510
      End
      Begin VB.Label lblProyIDM 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Solo activos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   7200
         TabIndex        =   19
         Top             =   645
         Width           =   855
      End
      Begin VB.Label lblProyIDM 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Plan TyO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   7410
         TabIndex        =   18
         Top             =   285
         Width           =   645
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   16
         Top             =   705
         Width           =   555
      End
      Begin VB.Label lblProyecto 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   345
         Width           =   495
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5775
      Left            =   60
      TabIndex        =   6
      Top             =   2160
      Width           =   10515
      _ExtentX        =   18547
      _ExtentY        =   10186
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imgExportacion 
      Left            =   120
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosIDMShell.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosIDMShell.frx":0B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosIDMShell.frx":10BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProyectosIDMShell.frx":1658
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmProyectosIDMShell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 12.05.2014 - La parte de exportaci�n de hitos, se mueve al final de la hoja.
' -002- a. FJS 20.03.2015 - Se agregan datos a la tabla de Hitos: visibilidad y clasificaci�n.
' -003- a. FJS 23.06.2015 - Se adecua la plantilla para las nuevas versiones de MS Office 2013.
' -004- a. FJS 03.08.2015 - Nuevo: se agrega la columna "fe_modif" que indica la �ltima modificaci�n que tuvo el proyecto.
' -005- a. FJS 20.10.2015 - Nuevo: se agrega funcionalidad para permitir la impresi�n en la ficha de m�s de 19 hitos. Y se
'                           expande la cantidad de caracteres de 120 a 175.
' -006- a. FJS 22.10.2015 - Nuevo: se agrega columna para selecci�n m�ltiple de registros. Esto es para que el administrador pueda
'                           realizar una exportaci�n especial de una selecci�n de proyectos para el comit�.
' -006- b. FJS 22.10.2015 - Nuevo: se agrega nuevo proceso de exportaci�n de fichas: cuando la opci�n es Consolidado, se genera un
'                           solo libro con cada proyecto por hoja (Celeste).
' -007- a. FJS 23.08.2016 - Nuevo: se quita la restricci�n de tama�o de la aplicaci�n (se permite la maximizaci�n de la pantalla).
' -007- b. FJS 23.08.2016 - Nuevo: se habilita el coloreo de filas en la grilla para mejorar la visibilidad de los datos.

Option Explicit

Public lProjId As Long
Public lProjSubId As Long
Public lProjSubSId As Long
Public cProjNom As String
Public cod_estado_activo As String

'{ add -006- b.
Dim vlProjId() As Long
Dim vlProjSubId() As Long
Dim vlProjSubSId() As Long
'}

Private Const a = 1
Private Const b = 2
Private Const c = 3
Private Const d = 4
Private Const E = 5
Private Const F = 6
Private Const G = 7
Private Const h = 8

Private Const COLOR_BLANCO = 2
Private Const COLOR_AMARILLO = 36
Private Const COLOR_ROJO = 3
Private Const COLOR_VERDE = 43

Private Const colMultiSelect = 0                ' add -006- a. Se renumeran todas las constantes de columna.
Private Const col_ProjId = 1
Private Const col_ProjSubId = 2
Private Const col_ProjSubsId = 3
Private Const col_Codigo = 4
Private Const col_ProjTipo = 5
Private Const col_ProjNom = 6
Private Const col_ProjDireccion = 7
Private Const col_ProjGerencia = 8
Private Const col_ProjSector = 9
Private Const col_ProjNomAreaSolicitante = 10
Private Const col_ProjEstado = 11
Private Const col_ProjEstadoNom = 12
Private Const col_ProjAvance = 13
Private Const col_ProjSema1 = 14
Private Const col_ProjDireccionP = 15
Private Const col_ProjGerenciaP = 16
Private Const col_ProjSectorP = 17
Private Const col_ProjGrupoP = 18
Private Const col_ProjAreaPropNom = 19
Private Const col_ProjFe_inicio = 20
Private Const col_ProjFe_fin = 21
Private Const col_ProjFe_finreprog = 22
Private Const col_ProjClasId = 23
Private Const col_ProjClasNom = 24
Private Const col_ProjArea = 25
Private Const col_ProjClas3 = 26
Private Const col_ProjFchAlta = 27
Private Const col_ProjFe_modif = 28
Private Const col_ProjPlanTYO = 29
Private Const col_ProjEmpresa = 30
Private Const col_ProjCatId = 31
Private Const col_ProjCatNom = 32
Private Const col_ProjUsrUlt = 33
Private Const col_ProjOrdenamiento = 34
Private Const col_ProjMarca = 35
Private Const col_ProjHsGer = 36
Private Const col_ProjHsSec = 37
Private Const col_ProjCantRepl = 38
Private Const GRILLA_COLUMNAS = 39

Private Const xlLeft = -4131
Private Const xlCenter = -4108
Private Const xlRight = -4152

Dim cont As Long                            ' Contador
Dim bLoading As Boolean
Dim bSorting As Boolean
Dim lUltimaColumnaOrdenada As Long
Dim Coleccion As New Collection
Dim xNivel As String
Dim xArea As String
Dim xNivelFiltro As String
Dim xAreaFiltro As String
Dim xPerfil As String

Private Sub Form_Load()
    Call Puntero(True)
    bLoading = True
    Call Cargar_Opciones
    Call InicializarControles
    Call CargarGrilla
    Call IniciarScroll(grdDatos)
    'Call FormCenter(Me, mdiPrincipal, False)
    bLoading = False
    Call Puntero(False)
End Sub

Private Sub InicializarControles()
    cmbSoloActivos.ToolTipText = "Proyectos activos: En evaluaci�n, Suspendidos, Aprobados, en An�lisis, A planificar, Planificados y En ejecuci�n."
    '.AddItem "Si" & ESPACIOS & "||EVALUA-SUSPEN-APROBA-ANALIS-PLANIF-PLANOK-EJECUC-"
    
    Select Case glUsrPerfilActual
        Case "ADMI", "SADM"     ' "BPAR" Por ahora los BPs no pueden modificar
            Call setHabilCtrl(cmbSoloActivos, "NOR")
            Call setHabilCtrl(cmdVisualizar, "DIS")
            Call setHabilCtrl(cmdAgregar, "NOR")
            Call setHabilCtrl(cmdModificar, "NOR")
            Call setHabilCtrl(cmdBaja, "NOR")
            Call setHabilCtrl(cmdEliminar, "NOR")
            Call setHabilCtrl(chkExportarHitos, "NOR")
            '{ add -006- a.
            'optExpo(0).Enabled = True
            'optExpo(1).Enabled = True
            optExpo(0).value = True
            chkExportarHitos.Enabled = True
            chkHitosExposicion.Enabled = True
            '}
        Case Else
            Call setHabilCtrl(cmbSoloActivos, "NOR")
            Call setHabilCtrl(cmdVisualizar, "NOR")
            Call setHabilCtrl(cmdAgregar, "DIS")
            Call setHabilCtrl(cmdModificar, "DIS")
            Call setHabilCtrl(cmdBaja, "DIS")
            Call setHabilCtrl(cmdEliminar, "DIS")
            Call setHabilCtrl(chkExportarHitos, "DIS")
            '{ add -006- a.
            'optExpo(0).Enabled = False
            'optExpo(1).Enabled = False
            optExpo(0).value = True
            chkExportarHitos.Enabled = True
            chkHitosExposicion.Enabled = False
            '}
    End Select
    Call setHabilCtrl(cmdExportar, "NOR")
    'Call setHabilCtrl(cmdGenerarFicha, "NOR")
End Sub

Private Sub Cargar_Opciones()
    Dim sDireccion As String
    Dim sGerencia As String
    Dim nomArea As String
    
    ' Tomamos el nivel y �rea del usuario, independientemente del perfil actuante (como BP no tiene nivel espec�fico asociado)
    xPerfil = IIf(glUsrPerfilActual = "BPAR" And TieneOtroPerfil(glUsrPerfilActual), GetOtroPerfil(glUsrPerfilActual), glUsrPerfilActual)
    xNivel = getPerfNivel(xPerfil)
    xArea = getPerfArea(xPerfil)
    xNivelFiltro = xNivel
    xAreaFiltro = xArea
    Select Case xNivel
        Case "BBVA": nomArea = "Todo el BBVA"
        Case "DIRE": If sp_GetDireccion(xArea) Then nomArea = ClearNull(aplRST!nom_direccion)
        Case "GERE": If sp_GetGerencia(xArea, Null) Then nomArea = ClearNull(aplRST!nom_gerencia)
        Case "SECT": If sp_GetSector(xArea, Null) Then nomArea = ClearNull(aplRST!nom_sector)
        Case "GRUP": If sp_GetGrupo(xArea, Null) Then nomArea = ClearNull(aplRST!nom_grupo)
    End Select
    lblPerfil.Caption = "Perfil: " & getDescPerfil(xPerfil) & "     Nivel: " & getDescNivel(xNivel) & "     Area: " & nomArea
    
    With cmbPlanTyO
        .Clear
        .AddItem "* Todos" & ESPACIOS & "||T"
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 1
    End With
    
    With cmbSoloActivos
        .Clear
        .AddItem "* Todos" & ESPACIOS & "||NULL"
        .AddItem "Si" & ESPACIOS & "||EVALUA-SUSPEN-APROBA-ANALIS-PLANIF-PLANOK-EJECUC-"
        .ListIndex = 1
    End With
    
    cboArea.Clear
    If sp_GetDireccion("", "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboArea.AddItem ClearNull(aplRST!nom_direccion) & Space(150) & "||DIRE|" & ClearNull(aplRST!cod_direccion)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                cboArea.AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||DIRE|" & Trim(aplRST!cod_direccion)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGerenciaXt("", "", "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboArea.AddItem ClearNull(aplRST!nom_direccion) & "� " & ClearNull(aplRST!nom_gerencia) & Space(150) & "||GERE|" & Trim(aplRST!cod_gerencia)
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboArea.AddItem sDireccion & "� " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||GERE|" & Trim(aplRST!cod_gerencia)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetSectorXt(Null, Null, Null, "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboArea.AddItem ClearNull(aplRST!nom_direccion) & "� " & ClearNull(aplRST!nom_gerencia) & "� " & ClearNull(aplRST!nom_sector) & Space(150) & "||SECT|" & ClearNull(aplRST!cod_sector)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboArea.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||SECT|" & Trim(aplRST!cod_sector)
            End If
            aplRST.MoveNext
        Loop
    End If
    If sp_GetGrupoXt("", "", "S") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboArea.AddItem ClearNull(aplRST!nom_direccion) & "� " & ClearNull(aplRST!nom_gerencia) & "� " & ClearNull(aplRST!nom_sector) & "� " & ClearNull(aplRST!nom_grupo) & Space(150) & "||GRUP|" & ClearNull(aplRST!cod_grupo)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboArea.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||GRUP|" & Trim(aplRST!cod_grupo)
            End If
            aplRST.MoveNext
        Loop
    End If
    cboArea.AddItem "Todo el BBVA" & ESPACIOS & ESPACIOS & ESPACIOS & "||NULL|NULL", 0
    cboArea.ListIndex = 0
    optFiltroArea(1).value = True
    
    '{ add -006- b.
    With imgFichaFormato
        .ComboItems.Clear
        .ComboItems.Add , "XLS", "Excel", 1
        .ComboItems.Add , "PDF", "PDF", 2
        .Locked = True
        .ComboItems(1).Selected = True
        .BackColor = fraOpcionesExportacion.BackColor
    End With
    chkHitosExposicion.value = 0
    chkHitosExposicion.Enabled = False
    '}
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .visible = False
        .Clear
        .cols = GRILLA_COLUMNAS
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Rows = 1
        ' NO VISIBLES
        .TextMatrix(0, col_ProjId) = "Codigo": .ColWidth(col_ProjId) = 0
        .TextMatrix(0, col_ProjSubId) = "Subc�digo": .ColWidth(col_ProjSubId) = 0
        .TextMatrix(0, col_ProjSubsId) = "Subsubc�digo": .ColWidth(col_ProjSubsId) = 0
        .TextMatrix(0, col_ProjDireccion) = "cod_direccion": .ColWidth(col_ProjDireccion) = 0
        .TextMatrix(0, col_ProjGerencia) = "cod_gerencia": .ColWidth(col_ProjGerencia) = 0
        .TextMatrix(0, col_ProjSector) = "cod_sector": .ColWidth(col_ProjSector) = 0
        .TextMatrix(0, col_ProjEstado) = "cod_estado": .ColWidth(col_ProjEstado) = 0
        .TextMatrix(0, col_ProjDireccionP) = "cod_direccion_p": .ColWidth(col_ProjDireccionP) = 0
        .TextMatrix(0, col_ProjGerenciaP) = "cod_gerencia_p": .ColWidth(col_ProjGerenciaP) = 0
        .TextMatrix(0, col_ProjSectorP) = "cod_sector_p": .ColWidth(col_ProjSectorP) = 0
        .TextMatrix(0, col_ProjGrupoP) = "cod_grupo_p": .ColWidth(col_ProjGrupoP) = 0
        .TextMatrix(0, col_ProjClasId) = "clas_id": .ColWidth(col_ProjClasId) = 0
        .TextMatrix(0, col_ProjCatId) = "cat_id": .ColWidth(col_ProjCatId) = 0
        .TextMatrix(0, col_ProjUsrUlt) = "Usuario Ult.": .ColWidth(col_ProjUsrUlt) = 0
        .TextMatrix(0, col_ProjOrdenamiento) = "orden": .ColWidth(col_ProjOrdenamiento) = 0
        .TextMatrix(0, col_ProjHsGer) = "HsGer": .ColWidth(col_ProjHsGer) = 0
        .TextMatrix(0, col_ProjHsSec) = "HsSec": .ColWidth(col_ProjHsSec) = 0
        .TextMatrix(0, col_ProjCantRepl) = "CantRepl": .ColWidth(col_ProjCantRepl) = 0
        ' VISIBLES
        .TextMatrix(0, colMultiSelect) = "": .ColWidth(colMultiSelect) = 200                    ' add -006- a.
        .TextMatrix(0, col_Codigo) = "Proy.": .ColWidth(col_Codigo) = 800
        .TextMatrix(0, col_ProjTipo) = "Tpo": .ColWidth(col_ProjTipo) = 450: .ColAlignment(col_ProjTipo) = flexAlignCenterCenter
        .TextMatrix(0, col_ProjNom) = "Nombre del proyecto": .ColWidth(col_ProjNom) = 3500: .ColAlignment(col_ProjNom) = flexAlignLeftCenter
        .TextMatrix(0, col_ProjNomAreaSolicitante) = "Solicitante": .ColWidth(col_ProjNomAreaSolicitante) = 2000
        .TextMatrix(0, col_ProjEstadoNom) = "Estado": .ColWidth(col_ProjEstadoNom) = 1400
        .TextMatrix(0, col_ProjAvance) = "% Av.": .ColWidth(col_ProjAvance) = 600
        .TextMatrix(0, col_ProjSema1) = "Sm.": .ColWidth(col_ProjSema1) = 500:: .ColAlignment(col_ProjSema1) = flexAlignCenterCenter
        .TextMatrix(0, col_ProjAreaPropNom) = "�rea propietaria": .ColWidth(col_ProjAreaPropNom) = 4000
        .TextMatrix(0, col_ProjFe_inicio) = "F. Inicio": .ColWidth(col_ProjFe_inicio) = 1200
        .TextMatrix(0, col_ProjFe_fin) = "F. Fin": .ColWidth(col_ProjFe_fin) = 1200
        .TextMatrix(0, col_ProjFe_finreprog) = "F. reprog.": .ColWidth(col_ProjFe_finreprog) = 1200
        .TextMatrix(0, col_ProjClasNom) = "Clasificaci�n": .ColWidth(col_ProjClasNom) = 2000: .ColAlignment(col_ProjClasNom) = flexAlignLeftCenter
        .TextMatrix(0, col_ProjArea) = "Area": .ColWidth(col_ProjArea) = 1400
        .TextMatrix(0, col_ProjClas3) = "Clase": .ColWidth(col_ProjClas3) = 1400: .ColAlignment(col_ProjClas3) = flexAlignLeftCenter
        .TextMatrix(0, col_ProjFchAlta) = "F. Alta": .ColWidth(col_ProjFchAlta) = 1200
        .TextMatrix(0, col_ProjFe_modif) = "F. Modif.": .ColWidth(col_ProjFe_modif) = 1200
        .TextMatrix(0, col_ProjPlanTYO) = "Plan TYO": .ColWidth(col_ProjPlanTYO) = 1000
        .TextMatrix(0, col_ProjEmpresa) = "Empr.": .ColWidth(col_ProjEmpresa) = 700
        .TextMatrix(0, col_ProjMarca) = "Marca": .ColWidth(col_ProjMarca) = 500
        .TextMatrix(0, col_ProjCatNom) = "Categor�a": .ColWidth(col_ProjCatNom) = 2500: .ColAlignment(col_ProjCatNom) = flexAlignLeftCenter
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
        .rowSel = .Rows - 1: .ColSel = .cols - 1                                            ' Selecciona la primera fila de todas
    End With
End Sub

Private Sub CargarGrilla(Optional SinFiltro As String)
    Dim lProjId As Variant
    Dim lProjSubId As Variant
    Dim lProjSubSId As Variant
    Dim cProjNom As Variant
    
    bLoading = True
    If IsMissing(SinFiltro) Then SinFiltro = "S"
   
    With grdDatos
        Call Puntero(True)
        If SinFiltro = "S" Then
            lProjId = Null
            lProjSubId = Null
            lProjSubSId = Null
            cProjNom = Null
        Else
            lProjId = IIf(Len(txtPrjId) > 0, txtPrjId, Null)
            lProjSubId = IIf(Len(txtPrjSubId) > 0, txtPrjSubId, Null)
            lProjSubSId = IIf(Len(txtPrjSubsId) > 0, txtPrjSubsId, Null)
            cProjNom = IIf(Len(txtPrjNom) > 0, txtPrjNom, Null)
        End If
        Call InicializarGrilla
        If sp_GetProyectoIDM(lProjId, lProjSubId, lProjSubSId, cProjNom, CodigoCombo(cmbSoloActivos, True), IIf(optFiltroArea(0).value, "RESP", "PROP"), xNivelFiltro, xAreaFiltro) Then
            Do While Not aplRST.EOF
                If (CodigoCombo(cmbPlanTyO, True) <> "T" And CodigoCombo(cmbPlanTyO, True) = ClearNull(aplRST.Fields!plan_tyo)) Or _
                    (CodigoCombo(cmbPlanTyO, True) = "T") Then
                    .Rows = .Rows + 1
                    'Debug.Print aplRST.Fields!ProjId & "." & aplRST.Fields!ProjSubId & "." & aplRST.Fields!ProjSubSId & " - " & aplRST.Fields!ProjNom
                    'If aplRST.Fields!ProjId = 411 Then Stop
                    .TextMatrix(.Rows - 1, col_ProjId) = aplRST.Fields!ProjId
                    .TextMatrix(.Rows - 1, col_ProjSubId) = aplRST.Fields!ProjSubId
                    .TextMatrix(.Rows - 1, col_ProjSubsId) = aplRST.Fields!ProjSubSId
                    .TextMatrix(.Rows - 1, col_ProjNom) = ClearNull(aplRST.Fields!projnom)
                    .TextMatrix(.Rows - 1, col_Codigo) = aplRST.Fields!ProjId & "." & aplRST.Fields!ProjSubId & "." & aplRST.Fields!ProjSubSId
                    .TextMatrix(.Rows - 1, col_ProjTipo) = ClearNull(aplRST.Fields!tipo_proyecto)
                    .TextMatrix(.Rows - 1, col_ProjFchAlta) = Format(aplRST.Fields!ProjFchAlta, "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjCatNom) = ClearNull(aplRST.Fields!ProjCatId) & ". " & ClearNull(aplRST.Fields!ProjCatNom)
                    .TextMatrix(.Rows - 1, col_ProjClasNom) = ClearNull(aplRST.Fields!ProjClaseId) & ". " & ClearNull(aplRST.Fields!ProjClaseNom)
                    .TextMatrix(.Rows - 1, col_ProjEstadoNom) = ClearNull(aplRST.Fields!nom_estado)
                    .TextMatrix(.Rows - 1, col_ProjUsrUlt) = ClearNull(aplRST.Fields!nom_usuario)
                    .TextMatrix(.Rows - 1, col_ProjSema1) = ClearNull(aplRST.Fields!semaphore)
                    .TextMatrix(.Rows - 1, col_ProjPlanTYO) = ClearNull(aplRST.Fields!plan_tyo_nom)
                    .TextMatrix(.Rows - 1, col_ProjEmpresa) = ClearNull(aplRST.Fields!nom_empresa)
                    .TextMatrix(.Rows - 1, col_ProjArea) = ClearNull(aplRST.Fields!area_nom)
                    .TextMatrix(.Rows - 1, col_ProjClas3) = ClearNull(aplRST.Fields!clas3_nom)
                    .TextMatrix(.Rows - 1, col_ProjDireccion) = ClearNull(aplRST.Fields!cod_direccion)
                    .TextMatrix(.Rows - 1, col_ProjGerencia) = ClearNull(aplRST.Fields!cod_gerencia)
                    .TextMatrix(.Rows - 1, col_ProjSector) = ClearNull(aplRST.Fields!cod_sector)
                    .TextMatrix(.Rows - 1, col_ProjNomAreaSolicitante) = ClearNull(aplRST.Fields!nom_areasoli)
                    .TextMatrix(.Rows - 1, col_ProjDireccionP) = ClearNull(aplRST.Fields!cod_direccion_p)
                    .TextMatrix(.Rows - 1, col_ProjGerenciaP) = ClearNull(aplRST.Fields!cod_gerencia_p)
                    .TextMatrix(.Rows - 1, col_ProjSectorP) = ClearNull(aplRST.Fields!cod_sector_p)
                    .TextMatrix(.Rows - 1, col_ProjGrupoP) = ClearNull(aplRST.Fields!cod_grupo_p)
                    .TextMatrix(.Rows - 1, col_ProjAreaPropNom) = ClearNull(aplRST.Fields!nom_areaprop)
                    .TextMatrix(.Rows - 1, col_ProjAvance) = Format(aplRST.Fields!avance, "###,###,##0.0")
                    .TextMatrix(.Rows - 1, col_ProjFe_modif) = Format(ClearNull(aplRST.Fields!fe_modif), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjFe_inicio) = Format(ClearNull(aplRST.Fields!fe_inicio), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjFe_fin) = Format(ClearNull(aplRST.Fields!fe_fin), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjFe_finreprog) = Format(ClearNull(aplRST.Fields!fe_finreprog), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjOrdenamiento) = aplRST.Fields!orden
                    .TextMatrix(.Rows - 1, col_ProjMarca) = aplRST.Fields!marca
                    .row = .Rows - 1
                    If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)                       ' add -007- b.
                    .col = col_ProjSema1
                    Select Case IIf(IsNull(aplRST.Fields!semaphore), aplRST.Fields!semaphore2, aplRST.Fields!semaphore)
                        Case "1": Call PintarCelda(grdDatos, prmGridFillRowColorDarkRed)
                        Case "2": Call PintarCelda(grdDatos, prmGridFillRowColorYellow)
                        Case "3": Call PintarCelda(grdDatos, prmGridFillRowColorDarkGreen)
                    End Select
                    If ClearNull(aplRST.Fields!marca) = "B" Then Call PintarLinea(grdDatos, prmGridFillRowColorRed)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
            Call Status(grdDatos.Rows - 1 & " proyecto(s)")
        End If
        Call Puntero(False)
        bLoading = False
        .visible = True
    End With
End Sub

Private Sub CargarGrilla_Filtrada(ByVal rsAux As ADODB.Recordset)
    With grdDatos
        Call Puntero(True)
        bLoading = True
        Call InicializarGrilla
        Call Status(rsAux.RecordCount & " proyecto(s)")
        Do While Not rsAux.EOF
            If (CodigoCombo(cmbPlanTyO, True) <> "T" And CodigoCombo(cmbPlanTyO, True) = ClearNull(rsAux.Fields!plan_tyo)) Or _
                (CodigoCombo(cmbPlanTyO, True) = "T") Then
                'If (CodigoCombo(cmbSoloActivos, True) <> "T" And CodigoCombo(cmbSoloActivos, True) = ClearNull(rsAux.Fields!marca)) Or _
                '    (CodigoCombo(cmbSoloActivos, True) = "T") Then
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, col_ProjId) = rsAux.Fields!ProjId
                    .TextMatrix(.Rows - 1, col_ProjSubId) = rsAux.Fields!ProjSubId
                    .TextMatrix(.Rows - 1, col_ProjSubsId) = rsAux.Fields!ProjSubSId
                    .TextMatrix(.Rows - 1, col_ProjNom) = ClearNull(rsAux.Fields!projnom)
                    .TextMatrix(.Rows - 1, col_ProjTipo) = ClearNull(rsAux.Fields!tipo_proyecto)
                    .TextMatrix(.Rows - 1, col_Codigo) = rsAux.Fields!ProjId & "." & rsAux.Fields!ProjSubId & "." & rsAux.Fields!ProjSubSId
                    .TextMatrix(.Rows - 1, col_ProjFchAlta) = Format(rsAux.Fields!ProjFchAlta, "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjCatNom) = ClearNull(rsAux.Fields!ProjCatId) & ". " & ClearNull(aplRST.Fields!ProjCatNom)
                    .TextMatrix(.Rows - 1, col_ProjClasNom) = ClearNull(rsAux.Fields!ProjClaseId) & ". " & ClearNull(aplRST.Fields!ProjClaseNom)
                    .TextMatrix(.Rows - 1, col_ProjEstadoNom) = ClearNull(rsAux.Fields!nom_estado)
                    .TextMatrix(.Rows - 1, col_ProjUsrUlt) = ClearNull(rsAux.Fields!nom_usuario)
                    .TextMatrix(.Rows - 1, col_ProjSema1) = ClearNull(rsAux.Fields!semaphore)
                    .TextMatrix(.Rows - 1, col_ProjPlanTYO) = ClearNull(rsAux.Fields!plan_tyo_nom)
                    .TextMatrix(.Rows - 1, col_ProjEmpresa) = ClearNull(rsAux.Fields!nom_empresa)
                    .TextMatrix(.Rows - 1, col_ProjArea) = ClearNull(rsAux.Fields!area_nom)
                    .TextMatrix(.Rows - 1, col_ProjClas3) = ClearNull(rsAux.Fields!clas3_nom)
                    .TextMatrix(.Rows - 1, col_ProjDireccion) = ClearNull(rsAux.Fields!cod_direccion)
                    .TextMatrix(.Rows - 1, col_ProjGerencia) = ClearNull(rsAux.Fields!cod_gerencia)
                    .TextMatrix(.Rows - 1, col_ProjSector) = ClearNull(rsAux.Fields!cod_sector)
                    .TextMatrix(.Rows - 1, col_ProjNomAreaSolicitante) = ClearNull(rsAux.Fields!nom_areasoli)
                    .TextMatrix(.Rows - 1, col_ProjDireccionP) = ClearNull(rsAux.Fields!cod_direccion_p)
                    .TextMatrix(.Rows - 1, col_ProjGerenciaP) = ClearNull(rsAux.Fields!cod_gerencia_p)
                    .TextMatrix(.Rows - 1, col_ProjSectorP) = ClearNull(rsAux.Fields!cod_sector_p)
                    .TextMatrix(.Rows - 1, col_ProjGrupoP) = ClearNull(rsAux.Fields!cod_grupo_p)
                    .TextMatrix(.Rows - 1, col_ProjAreaPropNom) = ClearNull(rsAux.Fields!nom_areaprop)
                    .TextMatrix(.Rows - 1, col_ProjAvance) = Format(rsAux.Fields!avance, "###,###,##0.0")
                    .TextMatrix(.Rows - 1, col_ProjFe_modif) = Format(ClearNull(rsAux.Fields!fe_modif), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjFe_inicio) = Format(ClearNull(rsAux.Fields!fe_inicio), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjFe_fin) = Format(ClearNull(rsAux.Fields!fe_fin), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjFe_finreprog) = Format(ClearNull(rsAux.Fields!fe_finreprog), "Medium Date")
                    .TextMatrix(.Rows - 1, col_ProjOrdenamiento) = ClearNull(rsAux.Fields!orden)
                    .TextMatrix(.Rows - 1, col_ProjMarca) = rsAux.Fields!marca
                    .row = .Rows - 1
                    .col = col_ProjSema1
                    Select Case IIf(IsNull(rsAux.Fields!semaphore), rsAux.Fields!semaphore2, rsAux.Fields!semaphore)
                        Case "1": PintarCelda grdDatos, prmGridFillRowColorDarkRed
                        Case "2": PintarCelda grdDatos, prmGridFillRowColorYellow
                        Case "3": PintarCelda grdDatos, prmGridFillRowColorDarkGreen
                    End Select
                    If ClearNull(rsAux.Fields!marca) = "B" Then PintarLinea grdDatos, prmGridFillRowColorRed
                'End If
            End If
            rsAux.MoveNext
            DoEvents
        Loop
        Call Puntero(False)
        bLoading = False
        .visible = True
    End With
End Sub

Private Sub Form_Resize()
    'Debug.Print "Height: " & Me.Height & " / Width: " & Me.Width
    'Me.WindowState = vbMaximized           ' del -007- a.
    '{ add -007- a.
    If Me.WindowState <> vbMinimized Then
        If Me.WindowState <> vbMaximized Then
            Me.Top = 0
            Me.Left = 0
            If Me.Height < 7170 Then Me.Height = 7170
            If Me.Width < 11850 Then Me.Width = 11850
        End If
        ' Ancho
        If Me.Width > fraOpcionesExportacion.Width Then fraOpciones.Width = Me.ScaleWidth - fraOpcionesExportacion.Width - 150
        fraOpcionesExportacion.Left = fraOpciones.Width + 100
        'grdDatos.Width = Me.ScaleWidth - fraBotones.Width - 150
        If Me.Width > fraBotones.Width Then grdDatos.Width = Me.ScaleWidth - fraBotones.Width - 150
        fraBotones.Left = grdDatos.Width + 100
        ' Alto
        'grdDatos.Height = Me.ScaleHeight - fraOpciones.Height - 200
        If Me.Height > (fraOpciones.Height - 200) Then grdDatos.Height = Me.ScaleHeight - fraOpciones.Height - 200
        'fraBotones.Height = Me.ScaleHeight - fraOpcionesExportacion.Height - 50
        If Me.Height > fraOpcionesExportacion.Height - 50 Then fraBotones.Height = Me.ScaleHeight - fraOpcionesExportacion.Height - 50
        cmdCerrar.Top = fraBotones.Height - cmdCerrar.Height - 100
    End If
    '}
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 13
            lProjId = ClearNull(grdDatos.TextMatrix(grdDatos.rowSel, 0))
            lProjSubId = ClearNull(grdDatos.TextMatrix(grdDatos.rowSel, 1))
            lProjSubSId = ClearNull(grdDatos.TextMatrix(grdDatos.rowSel, 2))
            cProjNom = ClearNull(grdDatos.TextMatrix(grdDatos.rowSel, 3))
            With frmProyectosIDMCarga
                .lProjId = lProjId
                .lProjSubId = lProjSubId
                .lProjSubSId = lProjSubSId
            End With
            cmdModificar_Click
    End Select
End Sub

Private Sub grdDatos_Click()
    bSorting = True
    bLoading = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada, True, col_ProjSema1)
    bLoading = False
    bSorting = False
End Sub

Private Sub grdDatos_RowColChange()
    If Not bLoading Then Call HabilitarModificar
    If Not bLoading Then
        If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
            If grdDatos.TextMatrix(grdDatos.rowSel, col_ProjMarca) <> "A" Then
                cmdBaja.Caption = "Alta"
            Else
                cmdBaja.Caption = "Baja"
            End If
        End If
    End If
End Sub

Private Sub HabilitarModificar()
    If Not bLoading Then
        With grdDatos
            If .rowSel > 0 Then
                Call setHabilCtrl(cmdModificar, "DIS")
                If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
                    Call setHabilCtrl(cmdModificar, "NOR")
                Else
                    If xNivel = "DIRE" And xArea = .TextMatrix(.rowSel, col_ProjDireccionP) Or _
                       xNivel = "GERE" And xArea = .TextMatrix(.rowSel, col_ProjGerenciaP) Or _
                       xNivel = "SECT" And xArea = .TextMatrix(.rowSel, col_ProjSectorP) Or _
                       xNivel = "GRUP" And xArea = .TextMatrix(.rowSel, col_ProjGrupoP) Then
                       '(xNivel = "GRUP" And xArea = .TextMatrix(.RowSel, col_ProjGrupoP) Or xNivel = "GRUP" And glLOGIN_Sector = .TextMatrix(.RowSel, col_ProjSectorP)) Then
                            Call setHabilCtrl(cmdModificar, "NOR")
                    Else
                        If sp_GetProyectoIDMResponsables(.TextMatrix(.rowSel, col_ProjId), .TextMatrix(.rowSel, col_ProjSubId), .TextMatrix(.rowSel, col_ProjSubsId), xNivel, xArea) Then
                            Call setHabilCtrl(cmdModificar, "NOR")
                        End If
                    End If
                End If
            End If
        End With
    End If
End Sub

Private Sub grdDatos_DblClick()
    Select Case glUsrPerfilActual
        Case "ADMI", "SADM":
            cmdModificar_Click
        Case Else
            cmdVisualizar_Click
    End Select
End Sub

Private Sub cmbEstado_Click()
    If Not bLoading Then
        CargarGrilla
    End If
End Sub

Private Sub optExpo_Click(Index As Integer)
    Select Case Index
        Case 0
            chkExportarHitos.Enabled = True
            chkHitosExposicion.Enabled = True
            chkExportarHitos.value = 0
        Case 1
            chkExportarHitos.Enabled = False
            chkHitosExposicion.Enabled = True
            chkExportarHitos.value = 1
    End Select
End Sub

Private Sub txtPrjId_Change()
    If Len(Trim(txtPrjId)) > 0 Then
        If Mid(txtPrjId, Len(txtPrjId), 1) = "'" Then
            txtPrjId = Mid(txtPrjId, 1, Len(txtPrjId) - 1)
        End If
    End If
    Call Filtrar(txtPrjId, txtPrjSubId, txtPrjSubsId, txtPrjNom.Tag)
End Sub

Private Sub txtPrjSubId_Change()
    If Len(Trim(txtPrjSubId)) > 0 Then
        If Mid(txtPrjSubId, Len(txtPrjSubId), 1) = "'" Then
            txtPrjSubId = Mid(txtPrjSubId, 1, Len(txtPrjSubId) - 1)
        End If
    End If
    Call Filtrar(txtPrjId, txtPrjSubId, txtPrjSubsId, txtPrjNom.Tag)
End Sub

Private Sub txtPrjSubsId_Change()
    If Len(Trim(txtPrjSubsId)) > 0 Then
        If Mid(txtPrjSubsId, Len(txtPrjSubsId), 1) = "'" Then
            txtPrjSubsId = Mid(txtPrjSubsId, 1, Len(txtPrjSubsId) - 1)
        End If
    End If
    Call Filtrar(txtPrjId, txtPrjSubId, txtPrjSubsId, txtPrjNom.Tag)
End Sub

Private Sub txtPrjNom_Change()
    If Len(Trim(txtPrjNom)) > 0 Then
        txtPrjNom.Tag = txtPrjNom.text
        If InStr(1, txtPrjNom.Tag, "'", vbTextCompare) > 0 Then
            Do While InStr(1, txtPrjNom.Tag, "'", vbTextCompare) > 0
                txtPrjNom.Tag = Mid(txtPrjNom.Tag, 1, InStr(1, txtPrjNom.Tag, "'", vbTextCompare) - 1) & _
                                Mid(txtPrjNom.Tag, InStr(1, txtPrjNom.Tag, "'", vbTextCompare) + 1)
                DoEvents
            Loop
        End If
        Call Filtrar(txtPrjId, txtPrjSubId, txtPrjSubsId, txtPrjNom.Tag)
    Else
        If Len(Trim(txtPrjNom.Tag)) > 0 Then Call Filtrar(txtPrjId, txtPrjSubId, txtPrjSubsId, "")
        txtPrjNom.Tag = ""
    End If
End Sub

Private Sub txtPrjId_GotFocus()
    With txtPrjId
        .SelStart = 0: .SelLength = Len(.text)
    End With
End Sub

Private Sub txtPrjSubId_GotFocus()
    With txtPrjSubId
        .SelStart = 0: .SelLength = Len(.text)
    End With
End Sub

Private Sub txtPrjSubsId_GotFocus()
    With txtPrjSubsId
        .SelStart = 0: .SelLength = Len(.text)
    End With
End Sub

Private Sub txtPrjNom_GotFocus()
    With txtPrjNom
        .SelStart = 0: .SelLength = Len(.text)
    End With
End Sub

Private Sub Filtrar(prjid, prjsubid, prjsubsid, PrjNom)
    On Error GoTo Errores
    Dim cFilters As String
    
    If ClearNull(prjid) & ClearNull(prjsubid) & ClearNull(prjsubsid) & ClearNull(PrjNom) <> "" Then
        aplRST.Filter = adFilterNone                            ' Inicializa los filtros para abarcar todos los items
        If Len(prjid) > 0 Then cFilters = "ProjId = " & Trim(prjid)                                                                                                                   ' Master Project
        If Len(Trim(prjsubid)) > 0 Then cFilters = IIf(Len(cFilters) > 0, Trim(cFilters) & " AND ProjSubId = " & Trim(prjsubid), "ProjSubId = " & Trim(prjsubid))                 ' Proyecto
        If Len(Trim(prjsubsid)) > 0 Then cFilters = IIf(Len(cFilters) > 0, Trim(cFilters) & " AND ProjSubsId = " & Trim(prjsubsid), "ProjSubSId = " & Trim(prjsubsid))            ' Subproyecto
        If Len(Trim(PrjNom)) > 0 Then cFilters = IIf(Len(cFilters) > 0, Trim(cFilters) & " AND ProjNom LIKE '*" & Trim(PrjNom) & "*'", "ProjNom LIKE '*" & Trim(PrjNom) & "*'")    ' Nombre del proyecto
        If Len(cFilters) > 0 Then
            aplRST.Filter = cFilters      ' Si existe alg�n criterio, aplica el filtro...
        End If
        Call CargarGrilla_Filtrada(aplRST)
    Else
        Call CargarGrilla
    End If
Exit Sub
Errores:
    Select Case Err.Number
        Case 123
            Resume Next
        Case Else
            'MsgBox Err.DESCRIPTION & "(" & Err.Number & ")", vbCritical + vbOKOnly
            Resume Next
    End Select
End Sub

Private Sub cmdVisualizar_Click()
    With grdDatos
        If .rowSel > 0 Then
            With frmProyectosIDMEdit
                .cModo = "VER"
                .l_ProjId = grdDatos.TextMatrix(grdDatos.rowSel, col_ProjId)
                .l_ProjSubId = grdDatos.TextMatrix(grdDatos.rowSel, col_ProjSubId)
                .l_ProjSubsId = grdDatos.TextMatrix(grdDatos.rowSel, col_ProjSubsId)
                .Show 1
            End With
        End If
    End With
End Sub

Private Sub AgregarColeccion(sElemento As String)
    If cont = 0 Then cont = 1
    Coleccion.Add Item:=sElemento, Key:=CStr(cont)
    cont = cont + 1
End Sub

Private Sub cmdAgregar_Click()
    frmProyectosIDMCarga.cModo = "AGREGAR"
    Load frmProyectosIDMCarga
    frmProyectosIDMCarga.Show 1
    If frmProyectosIDMCarga.bActualizo Then
        With frmProyectosIDMEdit
            .cModo = "AGREGAR"
            .l_ProjId = frmProyectosIDMCarga.lProjId
            .l_ProjSubId = frmProyectosIDMCarga.lProjSubId
            .l_ProjSubsId = frmProyectosIDMCarga.lProjSubSId
            .Show 1
        End With
    End If
    
    If frmProyectosIDMCarga.bActualizo Then
        With grdDatos
            If spProyectoIDM.sp_GetProyectoIDM(frmProyectosIDMCarga.lProjId, frmProyectosIDMCarga.lProjSubId, frmProyectosIDMCarga.lProjSubSId, cProjNom, "", IIf(optFiltroArea(0).value, "RESP", "PROP"), xNivelFiltro, xAreaFiltro) Then
                Call AgregarColeccion(aplRST.Fields!ProjId)
                Call AgregarColeccion(aplRST.Fields!ProjSubId)
                Call AgregarColeccion(aplRST.Fields!ProjSubSId)
                Call AgregarColeccion(aplRST.Fields!ProjId & "." & aplRST.Fields!ProjSubId & "." & aplRST.Fields!ProjSubSId)
                Call AgregarColeccion(ClearNull(aplRST.Fields!projnom))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_direccion))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_gerencia))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_sector))
                Call AgregarColeccion(ClearNull(aplRST.Fields!nom_areasoli))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_estado))
                Call AgregarColeccion(ClearNull(aplRST.Fields!nom_estado))
                Call AgregarColeccion(Format(aplRST.Fields!avance, "###,###,##0.0"))
                Call AgregarColeccion(ClearNull(aplRST.Fields!semaphore))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_direccion_p))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_gerencia_p))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_sector_p))
                Call AgregarColeccion(ClearNull(aplRST.Fields!cod_grupo_p))
                Call AgregarColeccion(ClearNull(aplRST.Fields!nom_areaprop))
                Call AgregarColeccion(Format(ClearNull(aplRST.Fields!fe_inicio), "Medium Date"))
                Call AgregarColeccion(Format(ClearNull(aplRST.Fields!fe_fin), "Medium Date"))
                Call AgregarColeccion(Format(ClearNull(aplRST.Fields!fe_finreprog), "Medium Date"))
                Call AgregarColeccion(ClearNull(aplRST.Fields!ProjClaseId))
                Call AgregarColeccion(ClearNull(aplRST.Fields!ProjClaseId) & ". " & ClearNull(aplRST.Fields!ProjClaseNom))
                Call AgregarColeccion(ClearNull(aplRST.Fields!area_nom))
                Call AgregarColeccion(ClearNull(aplRST.Fields!clas3_nom))
                Call AgregarColeccion(Format(ClearNull(aplRST.Fields!ProjFchAlta), "Medium Date"))
                Call AgregarColeccion(Format(ClearNull(aplRST.Fields!fe_modif), "Medium Date"))
                Call AgregarColeccion(ClearNull(aplRST.Fields!plan_tyo_nom))
                Call AgregarColeccion(ClearNull(aplRST.Fields!nom_empresa))
                Call AgregarColeccion(ClearNull(aplRST.Fields!ProjCatId))
                Call AgregarColeccion(ClearNull(aplRST.Fields!ProjCatId) & ". " & ClearNull(aplRST.Fields!ProjCatNom))
                Call AgregarColeccion("")
                Call AgregarColeccion("")
                Call AgregarColeccion(ClearNull(aplRST.Fields!orden))
                Call GrillaAgregarItem(grdDatos, Coleccion)
                .row = .rowSel + 1
                .col = col_ProjSema1
                Select Case IIf(IsNull(aplRST.Fields!semaphore), aplRST.Fields!semaphore2, aplRST.Fields!semaphore)
                    Case "1": PintarCelda grdDatos, prmGridFillRowColorDarkRed
                    Case "2": PintarCelda grdDatos, prmGridFillRowColorYellow
                    Case "3": PintarCelda grdDatos, prmGridFillRowColorDarkGreen
                End Select
                Set Coleccion = Nothing
            End If
        End With
    End If
End Sub

Private Sub cmdModificar_Click()
    Dim lRow As Long
    
    With grdDatos
        If .rowSel >= 1 Then
            With frmProyectosIDMEdit
                lRow = grdDatos.rowSel
                .cModo = "EDICION"
                .l_ProjId = grdDatos.TextMatrix(grdDatos.rowSel, col_ProjId)
                .l_ProjSubId = grdDatos.TextMatrix(grdDatos.rowSel, col_ProjSubId)
                .l_ProjSubsId = grdDatos.TextMatrix(grdDatos.rowSel, col_ProjSubsId)
                .Show 1
            End With
            bLoading = True
            Call CargarRegistro(.row)
            .row = lRow
            .col = 1
            .ColSel = .cols - 1
            bLoading = False
        End If
    End With
End Sub

Private Sub CargarRegistro(ByVal lRow As Long)
    If spProyectoIDM.sp_GetProyectoIDM(grdDatos.TextMatrix(grdDatos.rowSel, col_ProjId), grdDatos.TextMatrix(grdDatos.rowSel, col_ProjSubId), grdDatos.TextMatrix(grdDatos.rowSel, col_ProjSubsId), Null, Null, Null, Null, Null) Then
        With grdDatos
            .TextMatrix(lRow, col_ProjNom) = ClearNull(aplRST.Fields!projnom)
            .TextMatrix(lRow, col_Codigo) = aplRST.Fields!ProjId & "." & aplRST.Fields!ProjSubId & "." & aplRST.Fields!ProjSubSId
            .TextMatrix(lRow, col_ProjFchAlta) = Format(aplRST.Fields!ProjFchAlta, "Medium Date")
            .TextMatrix(lRow, col_ProjCatNom) = ClearNull(aplRST.Fields!ProjCatId) & ". " & ClearNull(aplRST.Fields!ProjCatNom)
            .TextMatrix(lRow, col_ProjClasNom) = ClearNull(aplRST.Fields!ProjClaseId) & ". " & ClearNull(aplRST.Fields!ProjClaseNom)
            .TextMatrix(lRow, col_ProjEstadoNom) = ClearNull(aplRST.Fields!nom_estado)
            .TextMatrix(lRow, col_ProjUsrUlt) = ClearNull(aplRST.Fields!nom_usuario)
            .TextMatrix(lRow, col_ProjSema1) = ClearNull(aplRST.Fields!semaphore)
            .TextMatrix(lRow, col_ProjPlanTYO) = ClearNull(aplRST.Fields!plan_tyo_nom)
            .TextMatrix(lRow, col_ProjEmpresa) = ClearNull(aplRST.Fields!nom_empresa)
            .TextMatrix(lRow, col_ProjArea) = ClearNull(aplRST.Fields!area_nom)
            .TextMatrix(lRow, col_ProjClas3) = ClearNull(aplRST.Fields!clas3_nom)
            .TextMatrix(lRow, col_ProjDireccion) = ClearNull(aplRST.Fields!cod_direccion)
            .TextMatrix(lRow, col_ProjGerencia) = ClearNull(aplRST.Fields!cod_gerencia)
            .TextMatrix(lRow, col_ProjSector) = ClearNull(aplRST.Fields!cod_sector)
            .TextMatrix(lRow, col_ProjNomAreaSolicitante) = ClearNull(aplRST.Fields!nom_areasoli)
            .TextMatrix(lRow, col_ProjDireccionP) = ClearNull(aplRST.Fields!cod_direccion_p)
            .TextMatrix(lRow, col_ProjGerenciaP) = ClearNull(aplRST.Fields!cod_gerencia_p)
            .TextMatrix(lRow, col_ProjSectorP) = ClearNull(aplRST.Fields!cod_sector_p)
            .TextMatrix(lRow, col_ProjGrupoP) = ClearNull(aplRST.Fields!cod_grupo_p)
            .TextMatrix(lRow, col_ProjAreaPropNom) = ClearNull(aplRST.Fields!nom_areaprop)
            .TextMatrix(lRow, col_ProjAvance) = Format(aplRST.Fields!avance, "###,###,##0.0")
            .TextMatrix(lRow, col_ProjFe_modif) = Format(ClearNull(aplRST.Fields!fe_modif), "Medium Date")
            .TextMatrix(lRow, col_ProjFe_inicio) = Format(ClearNull(aplRST.Fields!fe_inicio), "Medium Date")
            .TextMatrix(lRow, col_ProjFe_fin) = Format(ClearNull(aplRST.Fields!fe_fin), "Medium Date")
            .TextMatrix(lRow, col_ProjFe_finreprog) = Format(ClearNull(aplRST.Fields!fe_finreprog), "Medium Date")
            .TextMatrix(lRow, col_ProjOrdenamiento) = ClearNull(aplRST.Fields!orden)
            .TextMatrix(lRow, col_ProjMarca) = aplRST.Fields!marca
            '.Row = .Rows - 1
            .col = col_ProjSema1
            Select Case IIf(IsNull(aplRST.Fields!semaphore), aplRST.Fields!semaphore2, aplRST.Fields!semaphore)
                Case "1": Call PintarCelda(grdDatos, prmGridFillRowColorDarkRed)
                Case "2": Call PintarCelda(grdDatos, prmGridFillRowColorYellow)
                Case "3": Call PintarCelda(grdDatos, prmGridFillRowColorDarkGreen)
            End Select
            If ClearNull(aplRST.Fields!marca) = "B" Then Call PintarLinea(grdDatos, prmGridFillRowColorRed)
        End With
    End If
End Sub

Private Sub cmdBaja_Click()
    With grdDatos
        'If .RowSel > 1 Then
        If .Rows > 1 Then
            If MsgBox("�Confirma el cambio de estado l�gico del proyecto IDM seleccionado?", vbQuestion + vbYesNo, "Estado l�gico") = vbYes Then
                Call Puntero(True)
                With grdDatos
                    Call sp_UpdateProyectoIDMField(.TextMatrix(.rowSel, col_ProjId), .TextMatrix(.rowSel, col_ProjSubId), .TextMatrix(.rowSel, col_ProjSubsId), "MARCA", IIf(.TextMatrix(.rowSel, col_ProjMarca) = "A", "B", "A"), Null, Null)
                    Call CargarGrilla
                End With
                Call Puntero(False)
            End If
        End If
    End With
End Sub

Private Sub cmdEliminar_Click()
    With grdDatos
        If .Rows > 1 Then
            If MsgBox("�Confirma la eliminaci�n definitiva del proyecto IDM seleccionado?", vbQuestion + vbYesNo, "Eliminaci�n") = vbYes Then
                Call Puntero(True)
                Call sp_DeleteProyectoIDM(.TextMatrix(.rowSel, col_ProjId), .TextMatrix(.rowSel, col_ProjSubId), .TextMatrix(.rowSel, col_ProjSubsId))
                .RemoveItem .rowSel
                Call Puntero(False)
            End If
        End If
    End With
End Sub

Private Sub GenerarFicha()
    Dim i As Long, J As Long                                        ' add -006- b.
    Dim ubicacion As String
    
    With grdDatos
        'If .RowSel >= 1 Then Call GenerarFichaProyecto
        'If .RowSel >= 1 Then Call GenerarNUEVAFichaProyecto         ' del -006- b.
        '{ add -006- b.
        If .rowSel >= 1 Then
            With grdDatos
                For i = 1 To .Rows - 1
                    If .TextMatrix(i, colMultiSelect) = "�" Then
                        ReDim Preserve vlProjId(J)
                        ReDim Preserve vlProjSubId(J)
                        ReDim Preserve vlProjSubSId(J)
                        vlProjId(J) = .TextMatrix(i, col_ProjId)
                        vlProjSubId(J) = .TextMatrix(i, col_ProjSubId)
                        vlProjSubSId(J) = .TextMatrix(i, col_ProjSubsId)
                        J = J + 1
                    End If
                Next i
            End With
            Select Case J
                Case Is > 1   ' Multiples
                    ubicacion = Dialogo_EstablecerUbicacion("Establecer ubicaci�n de exportaci�n", glWRKDIR)
                    If ClearNull(ubicacion) = "" Then Exit Sub
                    Call GenerarMultiplesFichas(ubicacion)
                Case 1
                    Call GenerarUnaFicha
                Case Else
                    MsgBox "No se han seleccionado proyectos. Revise.", vbExclamation + vbOKOnly
            End Select
        End If
        '}
    End With
End Sub

Private Sub cmdExportar_Click()
    If optExpo(0).value Then
        Call GenerarExportacion
    Else
        Call GenerarFicha
    End If
End Sub

Private Sub GenerarExportacion()
    Const xlDown = -4121
    
    Dim ExcelApp As Object, ExcelWrk As Object, xlSheet As Object
    Dim sNombreArchivo As String
    Dim lCantidadFilas As Long
    Dim i As Long, J As Long, k As Long
    
    sNombreArchivo = Dialogo_GuardarExportacion("GUARDAR")
    If ClearNull(sNombreArchivo) <> "" Then
        If MsgBox("�Confirma la exportaci�n?", vbQuestion + vbOKCancel, "Exportaci�n") = vbOK Then
            Call Status("Preparando la exportaci�n...")
            Call Puntero(True)
            If sp_GetProyectoIDMExport(txtPrjId, txtPrjSubId, txtPrjSubsId, IIf(chkExportarHitos.value = 1, "S", "N"), IIf(chkHitosExposicion.value = 1, "S", "N"), txtPrjNom, CodigoCombo(cmbPlanTyO, True), CodigoCombo(cmbSoloActivos, True), IIf(optFiltroArea(0).value, "RESP", "PROP"), xNivelFiltro, xAreaFiltro) Then
                lCantidadFilas = aplRST.RecordCount
                If Not aplRST.EOF Then
                    If CrearObjetoExcel(ExcelApp, ExcelWrk, xlSheet, sNombreArchivo) Then           ' Crea la planilla para la exportaci�n
                        With xlSheet                                                                ' Los nombres de las columnas
                            .Cells(1, 1) = "Macro": .Columns(1).HorizontalAlignment = xlRight: .Columns(1).ColumnWidth = 5
                            .Cells(1, 2) = "Proyecto": .Columns(2).HorizontalAlignment = xlRight: .Columns(2).ColumnWidth = 5
                            .Cells(1, 3) = "Subproyecto": .Columns(3).HorizontalAlignment = xlRight: .Columns(3).ColumnWidth = 5
                            .Cells(1, 4) = "Nombre": .Columns(4).HorizontalAlignment = xlLeft: .Columns(4).ColumnWidth = 40
                            .Cells(1, 5) = "F.Alta": .Columns(5).HorizontalAlignment = xlLeft: .Columns(5).ColumnWidth = 9
                            .Cells(1, 6) = "Cat.Id.": .Columns(6).HorizontalAlignment = xlRight: .Columns(6).ColumnWidth = 5
                            .Cells(1, 7) = "Cat.Nom.": .Columns(7).HorizontalAlignment = xlLeft: .Columns(7).ColumnWidth = 20
                            .Cells(1, 8) = "Clase Id.": .Columns(8).HorizontalAlignment = xlRight: .Columns(8).ColumnWidth = 5
                            .Cells(1, 9) = "Clase Nom.": .Columns(9).HorizontalAlignment = xlLeft: .Columns(9).ColumnWidth = 20
                            .Cells(1, 10) = "C�d. Estado": .Columns(10).HorizontalAlignment = xlLeft: .Columns(10).ColumnWidth = 9
                            .Cells(1, 11) = "Nom. Estado": .Columns(11).HorizontalAlignment = xlLeft: .Columns(11).ColumnWidth = 15
                            .Cells(1, 12) = "Fch.Estado": .Columns(12).HorizontalAlignment = xlLeft: .Columns(12).ColumnWidth = 9
                            .Cells(1, 13) = "Plan TYO": .Columns(13).HorizontalAlignment = xlLeft: .Columns(13).ColumnWidth = 5
                            .Cells(1, 14) = "Empresa": .Columns(14).HorizontalAlignment = xlLeft: .Columns(14).ColumnWidth = 15
                            .Cells(1, 15) = "Area": .Columns(15).HorizontalAlignment = xlLeft: .Columns(15).ColumnWidth = 15
                            .Cells(1, 16) = "Clase": .Columns(16).HorizontalAlignment = xlLeft: .Columns(16).ColumnWidth = 15
                            .Cells(1, 17) = "Sem.": .Columns(17).HorizontalAlignment = xlCenter: .Columns(17).ColumnWidth = 5
                            .Cells(1, 18) = "Direcci�n Soli.": .Columns(18).HorizontalAlignment = xlLeft: .Columns(18).ColumnWidth = 9
                            .Cells(1, 19) = "Gerencia Soli.": .Columns(19).HorizontalAlignment = xlLeft: .Columns(19).ColumnWidth = 9
                            .Cells(1, 20) = "Sector Soli.": .Columns(20).HorizontalAlignment = xlLeft: .Columns(20).ColumnWidth = 9
                            .Cells(1, 21) = "Nombre del �rea solicitante": .Columns(21).HorizontalAlignment = xlLeft: .Columns(21).ColumnWidth = 40
                            .Cells(1, 22) = "Direcci�n Resp.": .Columns(22).HorizontalAlignment = xlLeft: .Columns(22).ColumnWidth = 9
                            .Cells(1, 23) = "Gerencia Resp.": .Columns(23).HorizontalAlignment = xlLeft: .Columns(23).ColumnWidth = 9
                            .Cells(1, 24) = "Sector Resp.": .Columns(24).HorizontalAlignment = xlLeft: .Columns(24).ColumnWidth = 9
                            .Cells(1, 25) = "Grupo Resp.": .Columns(25).HorizontalAlignment = xlLeft: .Columns(25).ColumnWidth = 9
                            .Cells(1, 26) = "Nombre del �rea responsable": .Columns(26).HorizontalAlignment = xlLeft: .Columns(26).ColumnWidth = 40
                            .Cells(1, 27) = "% Avance": .Columns(27).HorizontalAlignment = xlCenter: .Columns(27).ColumnWidth = 9
                            .Cells(1, 28) = "F.Inicio": .Columns(28).HorizontalAlignment = xlLeft: .Columns(28).ColumnWidth = 9
                            .Cells(1, 29) = "F.Fin": .Columns(29).HorizontalAlignment = xlLeft: .Columns(29).ColumnWidth = 9
                            .Cells(1, 30) = "F.Reprog.": .Columns(30).HorizontalAlignment = xlRight: .Columns(30).ColumnWidth = 9
                            .Cells(1, 31) = "Hab.": .Columns(31).HorizontalAlignment = xlLeft: .Columns(31).ColumnWidth = 5
                            '{ add -001- a.
                            .Cells(1, 32) = "Tipo": .Columns(32).HorizontalAlignment = xlLeft: .Columns(32).ColumnWidth = 8
                            .Cells(1, 33) = "Hs. Planif. Ger.": .Columns(33).HorizontalAlignment = xlLeft: .Columns(33).ColumnWidth = 15
                            .Cells(1, 34) = "Hs. estim. Sec.": .Columns(34).HorizontalAlignment = xlLeft: .Columns(34).ColumnWidth = 15
                            .Cells(1, 35) = "Cant. Replan.": .Columns(35).HorizontalAlignment = xlLeft: .Columns(35).ColumnWidth = 15
                            '}
                            .Cells(1, 36) = "Fch.Ult.Modif.": .Columns(36).HorizontalAlignment = xlLeft: .Columns(36).ColumnWidth = 15      ' add -004- a.
                            If chkExportarHitos.value = 1 Then
                                ' Se renumeran los n�meros para las columnas
                                .Cells(1, 37) = "Nombre hito": .Columns(37).HorizontalAlignment = xlLeft: .Columns(37).ColumnWidth = 15
                                .Cells(1, 38) = "Descripci�n": .Columns(38).HorizontalAlignment = xlLeft: .Columns(38).ColumnWidth = 15
                                .Cells(1, 39) = "Fecha ini.": .Columns(39).HorizontalAlignment = xlLeft: .Columns(39).ColumnWidth = 9
                                .Cells(1, 40) = "Fecha fin.": .Columns(40).HorizontalAlignment = xlLeft: .Columns(40).ColumnWidth = 9
                                .Cells(1, 41) = "Fecha ult. act.": .Columns(41).HorizontalAlignment = xlLeft: .Columns(41).ColumnWidth = 9
                                .Cells(1, 42) = "Estado": .Columns(42).HorizontalAlignment = xlLeft: .Columns(42).ColumnWidth = 9
                                '{ add -002- a.
                                .Cells(1, 43) = "Orden": .Columns(43).HorizontalAlignment = xlLeft: .Columns(43).ColumnWidth = 9
                                .Cells(1, 44) = "Exposici�n": .Columns(44).HorizontalAlignment = xlLeft: .Columns(44).ColumnWidth = 9
                                .Cells(1, 45) = "Id. clasificaci�n": .Columns(45).HorizontalAlignment = xlLeft: .Columns(45).ColumnWidth = 9
                                .Cells(1, 46) = "Desc. clasificaci�n": .Columns(46).HorizontalAlignment = xlLeft: .Columns(46).ColumnWidth = 12
                                '}
                            End If
                            i = i + 2
                            Do While Not aplRST.EOF
                                Call Status("Generando la exportaci�n... " & i - 1 & " de " & lCantidadFilas)
                                'If i = 51 Then Stop
                                .Cells(i, 1) = ClearNull(aplRST.Fields!ProjId)
                                .Cells(i, 2) = ClearNull(aplRST.Fields!ProjSubId)
                                .Cells(i, 3) = ClearNull(aplRST.Fields!ProjSubSId)
                                .Cells(i, 4) = ClearNull(aplRST.Fields!projnom)
                                .Cells(i, 5) = IIf(IsDate(aplRST.Fields!ProjFchAlta), Format(aplRST.Fields!ProjFchAlta, "Medium Date"), "")
                                .Cells(i, 6) = ClearNull(aplRST.Fields!ProjCatId)
                                .Cells(i, 7) = ClearNull(aplRST.Fields!ProjCatNom)
                                .Cells(i, 8) = ClearNull(aplRST.Fields!ProjClaseId)
                                .Cells(i, 9) = ClearNull(aplRST.Fields!ProjClaseNom)
                                .Cells(i, 10) = ClearNull(aplRST.Fields!cod_estado)
                                .Cells(i, 11) = ClearNull(aplRST.Fields!nom_estado)
                                .Cells(i, 12) = Format(aplRST.Fields!fch_estado, "Medium Date")         ' Antes "dd/mm/yyyy"
                                .Cells(i, 13) = ClearNull(aplRST.Fields!plan_tyo)
                                .Cells(i, 14) = ClearNull(aplRST.Fields!nom_empresa)
                                .Cells(i, 15) = ClearNull(aplRST.Fields!area_nom)
                                .Cells(i, 16) = ClearNull(aplRST.Fields!clas3_nom)
                                .Cells(i, 17) = ClearNull(aplRST.Fields!semaphore)
                                Select Case ClearNull(aplRST.Fields!semaphore)
                                    Case "1": .Cells(i, 17).Interior.ColorIndex = 46    ' Rojo
                                    Case "2": .Cells(i, 17).Interior.ColorIndex = 36    ' Amarillo
                                    Case "3": .Cells(i, 17).Interior.ColorIndex = 43    ' Verde
                                End Select
                                .Cells(i, 18) = ClearNull(aplRST.Fields!cod_direccion)
                                .Cells(i, 19) = ClearNull(aplRST.Fields!cod_gerencia)
                                .Cells(i, 20) = "'" & aplRST.Fields!cod_sector    ' Aqui agrego el ap�strofe
                                .Cells(i, 21) = ClearNull(aplRST.Fields!nom_areasoli)
                                .Cells(i, 22) = ClearNull(aplRST.Fields!cod_direccion_p)
                                .Cells(i, 23) = ClearNull(aplRST.Fields!cod_gerencia_p)
                                .Cells(i, 24) = ClearNull(aplRST.Fields!cod_sector_p)
                                .Cells(i, 25) = ClearNull(aplRST.Fields!cod_grupo_p)
                                .Cells(i, 26) = ClearNull(aplRST.Fields!nom_areaprop)
                                .Cells(i, 27) = ClearNull(aplRST.Fields!avance)
                                .Cells(i, 28) = Format(aplRST.Fields!fe_inicio, "Medium Date")              ' Antes "dd/mm/yyyy"
                                .Cells(i, 29) = Format(aplRST.Fields!fe_fin, "Medium Date")                 ' Antes "dd/mm/yyyy"
                                .Cells(i, 30) = Format(aplRST.Fields!fe_finreprog, "Medium Date")           ' Antes "dd/mm/yyyy"
                                .Cells(i, 31) = ClearNull(aplRST.Fields!marca)
                                '{ add -001- a. Estos son datos del proyecto, no del hito.
                                .Cells(i, 32) = ClearNull(aplRST.Fields!tipo_proyecto)
                                .Cells(i, 33) = ClearNull(aplRST.Fields!totalhs_gerencia)
                                .Cells(i, 34) = ClearNull(aplRST.Fields!totalhs_sector)
                                .Cells(i, 35) = ClearNull(aplRST.Fields!totalreplan)
                                '}
                                .Cells(i, 36) = Format(ClearNull(aplRST.Fields!fe_modif), "Medium Date")    ' add -004- a.
                                If chkExportarHitos.value = 1 Then
                                    .Cells(i, 37) = ClearNull(aplRST.Fields!hito_nombre)
                                    .Cells(i, 38) = ClearNull(aplRST.Fields!hito_descripcion)
                                    .Cells(i, 39) = Format(ClearNull(aplRST.Fields!hito_fe_ini), "Medium Date")
                                    .Cells(i, 40) = Format(ClearNull(aplRST.Fields!hito_fe_fin), "Medium Date")
                                    .Cells(i, 41) = Format(ClearNull(aplRST.Fields!hito_fecha), "Medium Date")      ' Antes "dd/mm/yyyy"
                                    .Cells(i, 42) = ClearNull(aplRST.Fields!hito_estado)
                                    .Cells(i, 43) = ClearNull(aplRST.Fields!hit_orden)
                                    .Cells(i, 44) = ClearNull(aplRST.Fields!hito_expos)
                                    .Cells(i, 45) = ClearNull(aplRST.Fields!hito_id)
                                    .Cells(i, 46) = ClearNull(aplRST.Fields!hitodesc)
                                End If
                                i = i + 1
                                aplRST.MoveNext
                                DoEvents
                            Loop
                        End With
                        Call CerrarObjectoExcel(ExcelApp, ExcelWrk, xlSheet, sNombreArchivo)
                    End If
                End If
            Else
                Call Puntero(False)
                MsgBox "No hay datos para exportar.", vbExclamation + vbOKOnly
                Call Status("Listo.")
            End If
        End If
    End If
End Sub

Private Sub cmbPlanTyO_Click()
    If Not bLoading Then CargarGrilla
End Sub

Private Sub cmbSoloActivos_Click()
    If Not bLoading Then CargarGrilla
End Sub

Private Sub cboArea_Click()
    Dim vRetorno() As String
    
    If ParseString(vRetorno, DatosCombo(cboArea), "|") = 0 Then
        MsgBox ("Error")
        Exit Sub
    End If
    xNivelFiltro = vRetorno(1)
    xAreaFiltro = vRetorno(2)
    If Not bLoading Then CargarGrilla
End Sub

Private Sub cmdActualizar_Click()
    bLoading = True
    Call Filtrar(txtPrjId, txtPrjSubId, txtPrjSubsId, txtPrjNom.Tag)
    bLoading = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

Private Sub cmdCerrar_Click()
    Call Status("Listo.")
    'Unload Me
    Me.Hide
End Sub

' ****** Aparentemente no EXISTE *******
'Private Sub cmdGenerarFichaFicha_Click()
'    If grdDatos.RowSel > 0 Then
'        Call ExportarProyectoIDM
'    End If
'End Sub


'' Original
'Private Sub GenerarFichaProyecto()
'    Dim objExcel As Object
'    Dim xLibro As Object
'    'Dim xWorkSheet As Object
'    Dim sFileName As String
'    Dim sDescripcion As String
'    Dim sBeneficios As String
'    Dim sNovedades As String
'    Dim ProjId As Integer
'    Dim ProjSId As Integer
'    Dim ProjSubSId As Integer
'    Dim projnom As String
'    Dim sTexto As String
'    Dim paginas As Integer
'    Dim i As Integer, j As Integer, k As Integer, x As Integer
'    Const xlPasteFormats = -4122
'    Const xlPasteValues = -4163
'    Const FilasPorPagina = 19           ' Antes: 19
'    Const AlturaDeFila = 25.5           ' Antes: 25.5
'
'    If Not glMSOfficeExcelIsEnabled Then
'        sTexto = "Error al intentar generar la exportaci�n." & vbCrLf & "El producto Microsoft Excel no est� instalado en el equipo."
'        MsgBox sTexto, vbCritical + vbOKOnly, "Generando ficha de proyecto"
'        Exit Sub
'    End If
'
'    '{ add -003- a.
'    sTexto = "No se encuentra la plantilla para realizar la exportaci�n a Excel."
'    Select Case glMSOfficeExcelVersion
'        Case "11.0"         ' Office 2003
'            If Dir(App.Path & "\prjidm.xls", vbArchive) = "" Then
'                MsgBox sTexto, vbCritical + vbOKOnly
'                Exit Sub
'            End If
'        Case Else           ' Versiones posteriores
'            If Dir(App.Path & "\prjidm13.xlsx", vbArchive) = "" Then
'                MsgBox sTexto, vbCritical + vbOKOnly
'                Exit Sub
'            End If
'    End Select
'    '}
'
'    sTexto = ""
'    With grdDatos
'        ProjId = .TextMatrix(.RowSel, col_ProjId)
'        ProjSId = .TextMatrix(.RowSel, col_ProjSubId)
'        ProjSubSId = .TextMatrix(.RowSel, col_ProjSubsId)
'
'        'sFileName = App.Path & "\prjidm_" & ProjId * 100000 + ProjSId * 100 + ProjSubSId & ".xls"
'        sFileName = glWRKDIR & "prjidm_" & ProjId * 100000 + ProjSId * 100 + ProjSubSId
'        sFileName = Dialogo_GuardarExportacion("GUARDAR", sFileName)
'        If ClearNull(sFileName) <> "" Then
'            Call Puntero(True)
'            Call Status("Generando la ficha de proyecto...")
'            Set objExcel = CreateObject("Excel.Application")
'            If sp_GetProyectoIDM(ProjId, ProjSId, ProjSubSId, Null, Null, Null, Null, Null) Then
'                sTexto = mdiPrincipal.sbPrincipal.Panels(1).Text
'                '{ add -003- a.
'                Select Case glMSOfficeExcelVersion
'                    Case "11.0"         ' Office 2003
'                        Set xLibro = objExcel.Workbooks.Open(App.Path & "\prjidm.xls", , , , Desencriptar("8489F59788F4F09780813439"))
'                    Case Else           ' Posteriores: 2007, 2010, etc.
'                        Set xLibro = objExcel.Workbooks.Open(App.Path & "\prjidm13.xlsx", , , , Desencriptar("8489F59788F4F09780813439"))
'                End Select
'                '}
'                'Set xLibro = objExcel.Workbooks.Open(App.Path & "\prjidm.xls", , , , Desencriptar("8489F59788F4F09780813439"))
'
'                'Set xWorkSheet = CreateObject("Excel.Worksheet")
'                'xWorkSheet = xLibro.
'
'                xLibro.SaveAs sFileName, , ""
'                objExcel.Visible = False
'                With xLibro
'                    With .Sheets("ANVERSO")
'                        projnom = ClearNull(aplRST.Fields!projnom)
'                        .Cells(4, c) = ClearNull(aplRST.Fields!projnom)                             ' T�tulo
'                        .Cells(5, c) = ClearNull(aplRST.Fields!nom_areasoli)                        ' Solicitante
'                        .Cells(6, c) = ClearNull(aplRST.Fields!nom_areaprop)                        ' Responsable
'                        .Cells(9, G) = Format(ClearNull(aplRST.Fields!fe_modif), "mm/dd/yyyy")      ' Fecha de �ltima modif.
'                        .Cells(10, G) = Val(ClearNull(aplRST.Fields!avance)) / 100                  ' % de avance
'                        .Cells(11, G) = ClearNull(aplRST.Fields!nom_estado)                         ' Estado actual
'                        .Cells(14, G) = Format(ClearNull(aplRST.Fields!fe_inicio), "mm/dd/yyyy")    ' Inicio
'                        .Cells(15, G) = Format(ClearNull(aplRST.Fields!fe_fin), "mm/dd/yyyy")       ' Fin previsto
'                        .Cells(16, G) = Format(ClearNull(aplRST.Fields!fe_finreprog), "mm/dd/yyyy") ' Fin reprogramado
'                        Select Case ClearNull(aplRST.Fields!semaphore)
'                            Case "1": .Range(.Cells(2, H), .Cells(3, H)).Interior.ColorIndex = COLOR_ROJO       ' rojo
'                            Case "2": .Range(.Cells(2, H), .Cells(3, H)).Interior.ColorIndex = COLOR_AMARILLO   ' amarillo
'                            Case "3": .Range(.Cells(2, H), .Cells(3, H)).Interior.ColorIndex = COLOR_VERDE      ' verde
'                        End Select
'                        sDescripcion = sp_GetProyectoIDMMemo(ProjId, ProjSId, ProjSubSId, "DESCRIPCIO", Null)
'                        .Cells(8, b) = Mid(sDescripcion, 1, 1030)
'                        sBeneficios = sp_GetProyectoIDMMemo(ProjId, ProjSId, ProjSubSId, "BENEFICIOS", Null)
'                        .Cells(19, b) = Mid(sBeneficios, 1, 308)
'                        sNovedades = sp_GetProyectoIDMNovedades3(ProjId, ProjSId, ProjSubSId, Null, "NOVEDADES", Null)
'                        .Cells(21, b) = Mid(sNovedades, 1, 917)
'                        i = 0
'                        If sp_GetProyectoIDMAlertas(ProjId, ProjSId, ProjSubSId) Then
'                            Do While Not aplRST.EOF And i < 3
'                                .Cells(25 + i, b) = ""
'                                Select Case ClearNull(aplRST.Fields!alert_tipo)
'                                    Case "R": .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_ROJO     ' rojo
'                                    Case "A": .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_AMARILLO ' amarillo
'                                    Case Else: .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_BLANCO  ' Blanco
'                                End Select
'                                .Range(.Cells(25 + i, b), .Cells(25 + i, G)).HorizontalAlignment = XLleft           ' Alineaci�n a la izquierda
'                                .Cells(25 + i, c) = Mid(ClearNull(aplRST.Fields!alert_desc), 1, 94)                 ' Descripci�n
'                                .Cells(25 + i, d) = Mid(ClearNull(aplRST.Fields!accion_id), 1, 64)                  ' Acci�n correctiva
'                                .Cells(25 + i, E) = Format(ClearNull(aplRST.Fields!alert_fe_ini), "mm/dd/yyyy")     ' Fecha de inicio
'                                .Cells(25 + i, F) = Format(ClearNull(aplRST.Fields!alert_fe_fin), "mm/dd/yyyy")     ' Fecha de fin
'                                .Cells(25 + i, G) = ClearNull(aplRST.Fields!responsables)                           ' Responsables
'                                aplRST.MoveNext
'                                i = i + 1
'                                DoEvents
'                            Loop
'                        End If
'                    End With
'                    '{ add -005- a.
'                    With .Sheets("REVERSO")
'                        .Activate
'                        If sp_GetProyectoIDMHitos(ProjId, ProjSId, ProjSubSId, Null, Null, IIf(chkHitosExposicion.Value = 1, "S", Null)) Then
'                            paginas = aplRST.RecordCount / FilasPorPagina
'                            i = 0
'                            k = 0       ' Items
'                            .Cells(2, d) = projnom                                                                      ' Nombre del proyecto
'                            For j = 1 To paginas
'                                Do While Not aplRST.EOF
'                                    ' Formato para las filas de hitos
'                                    .Range(.Cells(6 + i, b), .Cells(6 + i, d)).HorizontalAlignment = XLleft             ' Alineaci�n a la izquierda
'                                    .Range(.Cells(6, b), .Cells(6, F)).Copy
'                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
'                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
'                                    .Cells(6 + i, b) = Format(ClearNull(aplRST.Fields!hito_fe_ini), "mm/dd/yyyy")       ' Fecha de inicio
'                                    .Cells(6 + i, c) = Format(ClearNull(aplRST.Fields!hito_fe_fin), "mm/dd/yyyy")       ' Fecha de fin
'                                    .Cells(6 + i, d) = Mid(ClearNull(aplRST.Fields!hito_nombre), 1, 66)                 ' T�tulo del hito
'                                    .Cells(6 + i, E) = Mid(ClearNull(aplRST.Fields!hito_descripcion), 1, 120)           ' Descripci�n del hito
'                                    .Cells(6 + i, F) = ClearNull(aplRST.Fields!nom_estado)                              ' Estado
'                                    k = k + 1
'                                    aplRST.MoveNext
'                                    If k = (FilasPorPagina * j) Then
'                                        ' En el pie de p�gina grabo la p�gina actual
'                                        i = i + 2
'                                        .Cells(6 + i, F) = "P�gina " & j & " de " & paginas
'                                        ' Nuevo encabezado
'                                        i = i + 2
'                                        .Range(.Cells(2, b), .Cells(2, F)).Copy
'                                        .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteValues
'                                        .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
'                                        i = i + 2
'                                        .Range(.Cells(4, b), .Cells(4, F)).Copy
'                                        .Range(.Cells(6 + i, b), .Cells(6 + i, b)).PasteSpecial Paste:=xlPasteValues
'                                        .Range(.Cells(6 + i, b), .Cells(6 + i, b)).PasteSpecial Paste:=xlPasteFormats
'                                        ' Nuevo encabezado
'                                        i = i + 1
'                                        .Range(.Cells(5, b), .Cells(5, F)).Copy
'                                        .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteValues
'                                        .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
'                                        i = i + 1
'                                        Exit Do
'                                    Else
'                                        i = i + 1
'                                    End If
'                                    DoEvents
'                                Loop
'                            Next j
'                            ' Si es la �ltima p�gina, completa en blanco las filas y escribe el pie de p�gina
'                            If (paginas * FilasPorPagina) > k Then
'                                x = (paginas * FilasPorPagina) - k
'                                For j = 1 To x
'                                    ' Formato para las filas de hitos
'                                    .Range(.Cells(6, b), .Cells(6, F)).Copy
'                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
'                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
'                                    .Cells(6 + i, b) = ""
'                                    .Cells(6 + i, c) = ""
'                                    .Cells(6 + i, d) = ""
'                                    .Cells(6 + i, E) = ""
'                                    .Cells(6 + i, F) = ""
'                                    i = i + 1
'                                Next j
'                                ' En el pie de p�gina grabo la p�gina actual
'                                i = i + 1
'                                .Cells(6 + i, F) = "P�gina " & paginas & " de " & paginas
'                            End If
'                        End If
'                        .Range(.Cells(1, a), .Cells(1, a)).Select
'                    End With
'                    With .Sheets("ANVERSO")
'                        .Activate
'                        .Range(.Cells(1, a), .Cells(1, a)).Select
'                    End With
'                    '}
'                    '{ del -005- a.
'                    'With .Sheets("REVERSO")
'                    '    If sp_GetProyectoIDMHitos(ProjId, ProjSId, ProjSubSId, Null, Null) Then
'                    '        i = 0
'                    '        Do While Not aplRST.EOF And i < 19
'                    '            .Range(.Cells(6 + i, B), .Cells(6 + i, D)).HorizontalAlignment = XLleft             ' Alineaci�n a la izquierda
'                    '            .Cells(2, D) = projnom                                                              ' Nombre del proyecto
'                    '            .Cells(6 + i, B) = Format(ClearNull(aplRST.Fields!hito_fe_ini), "mm/dd/yyyy")       ' Fecha de inicio
'                    '            .Cells(6 + i, C) = Format(ClearNull(aplRST.Fields!hito_fe_fin), "mm/dd/yyyy")       ' Fecha de fin
'                    '            .Cells(6 + i, D) = Mid(ClearNull(aplRST.Fields!hito_nombre), 1, 66)                 ' Hito
'                    '            .Cells(6 + i, E) = Mid(ClearNull(aplRST.Fields!hito_descripcion), 1, 120)           ' Hito (descripci�n)
'                    '            .Cells(6 + i, F) = ClearNull(aplRST.Fields!nom_estado)                              ' Estado
'                    '            aplRST.MoveNext
'                    '            i = i + 1
'                    '            DoEvents
'                    '        Loop
'                    '    End If
'                    'End With
'                    '}
'                End With
'
'                With xLibro
'                    .Save
'                    .Close
'                End With
'
'                Set objExcel = Nothing
'                Set xLibro = Nothing
'                Call Status("Listo.")
'                Call Puntero(False)
'                If MsgBox("Generaci�n de ficha finalizada. �Abrir la ficha?", vbInformation + vbYesNo) = vbYes Then
'                    '{ add -003- a.
'                    Select Case glMSOfficeExcelVersion
'                        Case "11.0"         ' Office 2003
'                            Call EXEC_ShellExecute(Me.hwnd, IIf(InStr(1, sFileName, ".xls", vbTextCompare) > 0, sFileName, sFileName & ".xls"))
'                        Case Else           ' Posteriores: 2007, 2010, etc.
'                            Call EXEC_ShellExecute(Me.hwnd, IIf(InStr(1, sFileName, ".xlsx", vbTextCompare) > 0, sFileName, sFileName & ".xlsx"))
'                    End Select
'                    '}
'                    'Call EXEC_ShellExecute(Me.hwnd, IIf(InStr(1, sFileName, ".xls", vbTextCompare) > 0, sFileName, sFileName & ".xls"))    ' del -003- a.
'                End If
'            End If
'        End If
'    End With
'End Sub

Private Sub cmdImportar_Click()
    Call Importar_Cabeceras
    'Call Importar_Descripcion
End Sub

Private Sub Importar_Descripcion()
    Dim objExcel As Object
    Dim xLibro As Object
    Dim sFileName As String, sTexto As String, sResultado As String
    Dim x As Long, Descripcion As Long, Beneficios As Long, Novedades As Long, Alertas As Long, Hitos As Long
    
    Descripcion = 0
    Beneficios = 0
    Novedades = 0
    Alertas = 0
    Hitos = 0
    
    sFileName = ""
    sFileName = Dialogo_GuardarExportacion("ABRIR", sFileName)
    If ClearNull(sFileName) <> "" Then
        sTexto = mdiPrincipal.sbPrincipal.Panels(1).text
        Call Puntero(True)
        Call Status("Importando lista de proyectos...")
        Set objExcel = CreateObject("Excel.Application")
        objExcel.visible = False
        Set xLibro = objExcel.Workbooks.Open(sFileName)
        With xLibro
'            With .Sheets(LCase("DESCRIPCION"))
'                x = 2
'                Do While ClearNull(.Cells(x, A)) <> ""
'                    Call Status("Procesando " & x - 1)
'                    If spProyectoIDM.sp_UpdateProyectoIDMMemo2(IIf(.Cells(x, A) = "", 0, .Cells(x, A)), IIf(.Cells(x, B) = "", 0, .Cells(x, B)), IIf(.Cells(x, C) = "", 0, .Cells(x, C)), "DESCRIPCIO", .Cells(x, D), Limpiar(ClearNull(.Cells(x, E)))) Then
'                        Descripcion = Descripcion + 1
'                        .Cells(x, F) = "OK"
'                    End If
'                    x = x + 1
'                    DoEvents
'                Loop
'                sResultado = sResultado & vbCrLf & "Descripciones: " & Descripcion & " de " & x - 2
'            End With
'            With .Sheets(LCase("BENEFICIOS"))
'                x = 2
'                Do While ClearNull(.Cells(x, A)) <> ""
'                    Call Status("Procesando " & x - 1)
'                    If spProyectoIDM.sp_UpdateProyectoIDMMemo2(IIf(.Cells(x, A) = "", 0, .Cells(x, A)), IIf(.Cells(x, B) = "", 0, .Cells(x, B)), IIf(.Cells(x, C) = "", 0, .Cells(x, C)), "BENEFICIOS", .Cells(x, D), Limpiar(ClearNull(.Cells(x, E)))) Then
'                        Beneficios = Beneficios + 1
'                        .Cells(x, F) = "OK"
'                    End If
'                    x = x + 1
'                    DoEvents
'                Loop
'                sResultado = sResultado & vbCrLf & "Beneficios: " & Beneficios & " de " & x - 2
'            End With
'            With .Sheets(LCase("NOVEDADES"))
'                x = 2
'                Do While ClearNull(.Cells(x, a)) <> ""
'                    Call spProyectoIDM.sp_DeleteProyectoIDMNovedades(IIf(.Cells(x, a) = "", 0, .Cells(x, a)), IIf(.Cells(x, b) = "", 0, .Cells(x, b)), IIf(.Cells(x, c) = "", 0, .Cells(x, c)), Null, Null, Null, "ALL")
'                    x = x + 1
'                    DoEvents
'                Loop
'                x = 2
'                Do While ClearNull(.Cells(x, a)) <> ""
'                    Call Status("Procesando " & x - 1)
'                    If spProyectoIDM.sp_UpdateProyectoIDMNovedades2(IIf(.Cells(x, a) = "", 0, .Cells(x, a)), IIf(.Cells(x, b) = "", 0, .Cells(x, b)), IIf(.Cells(x, c) = "", 0, .Cells(x, c)), date, "NOVEDADES", .Cells(x, d), Limpiar(ClearNull(.Cells(x, E))), "JP00") Then
'                        Novedades = Novedades + 1
'                        .Cells(x, H) = "OK"
'                    End If
'                    x = x + 1
'                    DoEvents
'                Loop
'                sResultado = sResultado & vbCrLf & "Novedades: " & Novedades & " de " & x - 2
'            End With
'            With .Sheets(LCase("ALERTAS"))
'                x = 2
'                Do While ClearNull(.Cells(x, A)) <> ""
'                    Call Status("Procesando " & x - 1)
'                    If spProyectoIDM.sp_InsertProyectoIDMAlertas(IIf(.Cells(x, A) = "", 0, .Cells(x, A)), IIf(.Cells(x, B) = "", 0, .Cells(x, B)), IIf(.Cells(x, C) = "", 0, .Cells(x, C)), .Cells(x, D), .Cells(x, E), .Cells(x, F), .Cells(x, G), .Cells(x, H), .Cells(x, H + 1), .Cells(x, H + 2), .Cells(x, H + 3)) Then
'                        Alertas = Alertas + 1
'                        .Cells(x, 13) = "OK"
'                    End If
'                    x = x + 1
'                    DoEvents
'                Loop
'                sResultado = sResultado & vbCrLf & "Alertas: " & Alertas & " de " & x - 2
'            End With
            
'            ' Aqu� hay que agregar los datos del nro interno del hito
'            With .Sheets(LCase("HITOS"))
'                x = 2
'                Do While ClearNull(.Cells(x, A)) <> ""
'                    Call Status("Procesando " & x - 1)
'                    Call sp_DeleteProyectoIDMHitos(IIf(.Cells(x, A) = "", 0, .Cells(x, A)), IIf(.Cells(x, B) = "", 0, .Cells(x, B)), IIf(.Cells(x, C) = "", 0, .Cells(x, C)), .Cells(x, D), .Cells(x, E), .Cells(x, F))
'                    If sp_InsertProyectoIDMHitos(IIf(.Cells(x, A) = "", 0, .Cells(x, A)), IIf(.Cells(x, B) = "", 0, .Cells(x, B)), IIf(.Cells(x, C) = "", 0, .Cells(x, C)), .Cells(x, D), Limpiar(.Cells(x, G)), .Cells(x, E), .Cells(x, F), .Cells(x, H), Null, Null, Null) Then
'                        Hitos = Hitos + 1
'                        .Cells(x, 9) = "OK"
'                    End If
'                    x = x + 1
'                    DoEvents
'                Loop
'                sResultado = sResultado & vbCrLf & "Hitos: " & Hitos & " de " & x - 2
'            End With
            .Save
            .Close
        End With
        objExcel.Application.Quit
        Set objExcel = Nothing
        Set xLibro = Nothing
        Call Status(sTexto)
        Call Puntero(False)
        Call cmdActualizar_Click
        MsgBox "Proceso de importaci�n finalizado." & sResultado
    End If
End Sub

Private Function Limpiar(sTexto As String) As String
    Dim sINI As String
    Dim sFIN As String
    Dim iPos As Integer
    
    iPos = 0
    If InStr(1, sTexto, vbLf, vbTextCompare) > 0 Then
        Do Until InStr(iPos + 1, sTexto, vbLf, vbTextCompare) = 0
            sINI = Mid(sTexto, 1, InStr(iPos + 1, sTexto, vbLf, vbTextCompare) - 1)
            sFIN = Mid(sTexto, InStr(iPos + 1, sTexto, vbLf, vbTextCompare) + 1)
            sTexto = sINI & vbCrLf & sFIN
            iPos = InStr(iPos + 1, sTexto, vbLf, vbTextCompare)
        Loop
    End If
    Limpiar = sTexto
End Function

Private Sub Importar_Cabeceras()
    Dim objExcel As Object
    Dim xLibro As Object
    Dim sFileName As String
    Dim sTexto As String
    Dim x As Long, Agregados As Long, actualizados As Long
    
    Agregados = 0
    actualizados = 0
    
    sFileName = ""
    sFileName = Dialogo_GuardarExportacion("ABRIR", sFileName)
    If ClearNull(sFileName) <> "" Then
        sTexto = mdiPrincipal.sbPrincipal.Panels(1).text
        Call Puntero(True)
        Call Status("Importando lista de proyectos...")
        Set objExcel = CreateObject("Excel.Application")
        objExcel.visible = False
        Set xLibro = objExcel.Workbooks.Open(sFileName)
        With xLibro
            With .Sheets("PROYECTOS")
                x = 2
                Do While ClearNull(.Cells(x, a)) <> ""
                    Call Status("Procesando " & x - 1)
                    Debug.Print x - 1 & ") " & .Cells(x, a) & "." & .Cells(x, b) & "." & .Cells(x, c)
                    If sp_GetProyectoIDM2(IIf(.Cells(x, a) = "", 0, .Cells(x, a)), IIf(.Cells(x, b) = "", 0, .Cells(x, b)), IIf(.Cells(x, c) = "", 0, .Cells(x, c)), Null, "P", Null) Then
                        actualizados = actualizados + 1
                        Call spProyectoIDM.sp_UpdateProyectoIDM2(IIf(.Cells(x, 1) = "", 0, .Cells(x, 1)), IIf(.Cells(x, 2) = "", 0, .Cells(x, 2)), IIf(.Cells(x, 3) = "", 0, .Cells(x, 3)), _
                            ClearNull(.Cells(x, 4)), _
                            ClearNull(.Cells(x, 5)), _
                            ClearNull(.Cells(x, 6)), _
                            ClearNull(.Cells(x, 7)), _
                            ClearNull(.Cells(x, 8)), _
                            Mid(ClearNull(.Cells(x, 9)), 1, 1), _
                            ClearNull(.Cells(x, 10)), _
                            ClearNull(.Cells(x, 11)), _
                            ClearNull(.Cells(x, 12)), _
                            ClearNull(.Cells(x, 13)), _
                            ClearNull(.Cells(x, 14)), _
                            ClearNull(.Cells(x, 15)), _
                            ClearNull(.Cells(x, 16)), _
                            ClearNull(.Cells(x, 17)), _
                            ClearNull(.Cells(x, 18)), _
                            ClearNull(.Cells(x, 19)), _
                            ClearNull(.Cells(x, 20)), _
                            ClearNull(.Cells(x, 21)), _
                            ClearNull(.Cells(x, 22)), _
                            ClearNull(.Cells(x, 23)), _
                            ClearNull(.Cells(x, 24)), Null)
                        'Call spProyectoIDM.sp_UpdateProyectoIDMField(IIf(.Cells(x, A) = "", 0, .Cells(x, A)), IIf(.Cells(x, B) = "", 0, .Cells(x, B)), IIf(.Cells(x, C) = "", 0, .Cells(x, C)), "COD_ESTADO", .Cells(x, H), Null, Null)
                    Else
                        Agregados = Agregados + 1
                        Call spProyectoIDM.sp_InsertProyectoIDM(IIf(.Cells(x, a) = "", 0, .Cells(x, a)), IIf(.Cells(x, b) = "", 0, .Cells(x, b)), IIf(.Cells(x, c) = "", 0, .Cells(x, c)), "", "", "")
                        Call spProyectoIDM.sp_UpdateProyectoIDM2(IIf(.Cells(x, 1) = "", 0, .Cells(x, 1)), IIf(.Cells(x, 2) = "", 0, .Cells(x, 2)), IIf(.Cells(x, 3) = "", 0, .Cells(x, 3)), _
                            ClearNull(.Cells(x, 4)), _
                            ClearNull(.Cells(x, 5)), _
                            ClearNull(.Cells(x, 6)), _
                            ClearNull(.Cells(x, 7)), _
                            ClearNull(.Cells(x, 8)), _
                            Mid(ClearNull(.Cells(x, 9)), 1, 1), _
                            ClearNull(.Cells(x, 10)), _
                            ClearNull(.Cells(x, 11)), _
                            ClearNull(.Cells(x, 12)), _
                            ClearNull(.Cells(x, 13)), _
                            ClearNull(.Cells(x, 14)), _
                            ClearNull(.Cells(x, 15)), _
                            ClearNull(.Cells(x, 16)), _
                            ClearNull(.Cells(x, 17)), _
                            ClearNull(.Cells(x, 18)), _
                            ClearNull(.Cells(x, 19)), _
                            ClearNull(.Cells(x, 20)), _
                            ClearNull(.Cells(x, 21)), _
                            ClearNull(.Cells(x, 22)), _
                            ClearNull(.Cells(x, 23)), _
                            ClearNull(.Cells(x, 24)), Null)
                        Call spProyectoIDM.sp_UpdateProyectoIDMField(IIf(.Cells(x, a) = "", 0, .Cells(x, a)), IIf(.Cells(x, b) = "", 0, .Cells(x, b)), IIf(.Cells(x, c) = "", 0, .Cells(x, c)), "COD_ESTADO", "PLANIF", Null, Null)
                    End If
                    x = x + 1
                    DoEvents
                Loop
            End With
            .Close
        End With
        objExcel.Application.Quit
        Set objExcel = Nothing
        Set xLibro = Nothing
        Call Status(sTexto)
        Call Puntero(False)
        Call cmdActualizar_Click
        MsgBox "Proceso de importaci�n finalizado." & vbCrLf & _
                "Agregados: " & Agregados & vbCrLf & _
                "Actualizados: " & actualizados, vbInformation
    End If
End Sub

'Private Sub Guardar_Opciones()
'    'SaveSetting "GesPet", "Preferencias\ProyectoIDM", "ImportLastFile", opc_ImportFileName
'    'SaveSetting "GesPet", "Preferencias\ProyectoIDM", "ImportLastPath", opc_ImportFilePath
'    'SaveSetting "GesPet", "Preferencias\ProyectoIDM", "PreAnalizeFile", IIf(chkAnalyzeImport.Value = 1, "S", "N")
'End Sub

'Private Sub Command1_Click()
'    Dim i As Long
'    Dim cEstado As String
'    With grdDatos
'        For i = 1 To .Rows - 1
'            Status ("Procesando: " & i & " de " & .Rows - 1)
'            ' Obtengo el estado que deber�a tener el proyecto IDM
'            cEstado = sp_GetEstadoProyectoIDM(.TextMatrix(i, 0), .TextMatrix(i, 1), .TextMatrix(i, 2))
'            If Len(ClearNull(cEstado)) > 0 Then
'                Call sp_UpdateProyectoIDMField(.TextMatrix(i, 0), .TextMatrix(i, 1), .TextMatrix(i, 2), "COD_ESTADO", cEstado, Null, Null)
'            End If
'        Next i
'    End With
'    MsgBox "Proceso finalizado"
'End Sub

'Private Sub chkMarca_Click()
'    If Not bLoading Then
'        CargarGrilla
'    End If
'End Sub

'Private Sub Filtrar()   ' Original
'    Dim cFilters As String
'
'    aplRST.Filter = adFilterNone        ' Inicializa los filtros para abarcar todos los items
'
'    If Len(txtPrjId) > 0 Then cFilters = "ProjId >= " & Trim(txtPrjId)  ' Master Project
'    ' Proyecto
'    If Len(Trim(txtPrjSubId)) > 0 Then
'        If Len(cFilters) > 0 Then
'            cFilters = Trim(cFilters) & " AND ProjSubId >= " & Trim(txtPrjSubId)
'        Else
'            cFilters = "ProjSubId >= " & Trim(txtPrjSubId)
'        End If
'    End If
'    ' Subproyecto
'    If Len(Trim(txtPrjSubsId)) > 0 Then
'        If Len(cFilters) > 0 Then
'            cFilters = Trim(cFilters) & " AND ProjSubSId >= " & Trim(txtPrjSubsId)
'        Else
'            cFilters = "ProjSubSId >= " & Trim(txtPrjSubsId)
'        End If
'    End If
'    ' Nombre o descripci�n del proyecto
'    If Len(Trim(txtPrjNom)) > 0 Then
'        If Len(cFilters) > 0 Then
'            cFilters = Trim(cFilters) & " AND ProjNom LIKE '*" & Trim(txtPrjNom) & "*'"
'        Else
'            cFilters = "ProjNom LIKE '*" & Trim(txtPrjNom) & "*'"
'        End If
'    End If
'    ' Si existe alg�n criterio, realiza el filtro...
'    If Len(cFilters) > 0 Then
'        aplRST.Filter = cFilters
'    End If
'    'Debug.Print Time & " - Criteria: " & cFilters
'    CargarGrilla_Filtrada aplRST
'End Sub

' ESTE CODIGO ERA PARA ALIMENTAR EL COMBO CON LOS ESTADOS PARA FILTRAR LOS PROYECTOS
'    With cmbEstado
'        .Clear
'        .AddItem "* Todos los proyectos" & Space(100) & "||" & "", 0
'        .ListIndex = 0
'    End With
'    With cmbEstado
'        .Clear
'        cod_estado_activo = ""
'        ' Armo los estados activos
'        If sp_GetEstados(Null, Null) Then
'            Do While Not aplRST.EOF
'                If ClearNull(aplRST.Fields!IGM_hab) = "S" Then
'                    If InStr("PLANRK|EJECRK|ESTIRK|", ClearNull(aplRST!cod_estado)) = 0 Then
'                        If Val(ClearNull(aplRST!flg_petici)) > 0 Then
'                            cmbEstado.AddItem ClearNull(aplRST!nom_estado) & Space(100) & "||" & ClearNull(aplRST!cod_estado)
'                            If Val(ClearNull(aplRST!flg_petici)) > 1 Then
'                                cod_estado_activo = cod_estado_activo & ClearNull(aplRST!cod_estado) & "|"
'                            End If
'                        End If
'                    End If
'                End If
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        .AddItem "* Todos los proyectos" & Space(100) & "||" & "", 0
'        .AddItem "* Proyectos en estado activo" & Space(100) & "||" & cod_estado_activo, 1
'        .AddItem "* Proyectos en estado terminal" & Space(100) & "||" & "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", 2
'        .AddItem "* Proyectos sin asociaci�n a peticiones" & Space(100) & "||" & "NULL|", 3
'        .ListIndex = 0
'    End With

' Orden original (antes del pedido de Palmero)
'Private Const col_ProjId = 0
'Private Const col_ProjSubId = 1
'Private Const col_ProjSubsId = 2
'Private Const col_Codigo = 3
'Private Const col_ProjNom = 4
'Private Const col_ProjFchAlta = 5
'Private Const col_ProjCatId = 6
'Private Const col_ProjCatNom = 7
'Private Const col_ProjClasId = 8
'Private Const col_ProjClasNom = 9
'Private Const col_ProjEstado = 10
'Private Const col_ProjEstadoNom = 11
'Private Const col_ProjFchUlt = 12
'Private Const col_ProjUsrUlt = 13
'Private Const col_ProjPlanTYO = 14
'Private Const col_ProjEmpresa = 15
'Private Const col_ProjArea = 16
'Private Const col_ProjClas3 = 17
'Private Const col_ProjSema1 = 18
'Private Const col_ProjOwner = 19
'Private Const col_ProjBP = 20
'Private Const col_ProjBPNom = 21
'Private Const col_ProjAvance = 22
'Private Const col_ProjFe_modif = 23
'Private Const col_ProjFe_inicio = 24
'Private Const col_ProjFe_fin = 25
'Private Const col_ProjFe_finreprog = 26
'Private Const col_ProjOrdenamiento = 27
'Private Const GRILLA_COLUMNAS = 27

'        .TextMatrix(0, col_ProjId) = "Codigo": .ColWidth(col_ProjId) = 0
'        .TextMatrix(0, col_ProjSubId) = "Subc�digo": .ColWidth(col_ProjSubId) = 0
'        .TextMatrix(0, col_ProjSubsId) = "Subsubc�digo": .ColWidth(col_ProjSubsId) = 0
'        .TextMatrix(0, col_Codigo) = "Proy.": .ColWidth(col_Codigo) = 800
'        .TextMatrix(0, col_ProjNom) = "Nombre del proyecto": .ColWidth(col_ProjNom) = 3500: .ColAlignment(col_ProjNom) = flexAlignLeftCenter
'        .TextMatrix(0, col_ProjFchAlta) = "F. Alta": .ColWidth(col_ProjFchAlta) = 1200
'        .TextMatrix(0, col_ProjCatId) = "cat_id": .ColWidth(col_ProjCatId) = 0
'        .TextMatrix(0, col_ProjCatNom) = "Categor�a": .ColWidth(col_ProjCatNom) = 2000: .ColAlignment(col_ProjCatNom) = flexAlignLeftCenter
'        .TextMatrix(0, col_ProjClasId) = "clas_id": .ColWidth(col_ProjClasId) = 0
'        .TextMatrix(0, col_ProjClasNom) = "Clasificaci�n": .ColWidth(col_ProjClasNom) = 2000: .ColAlignment(col_ProjClasNom) = flexAlignLeftCenter
'        .TextMatrix(0, col_ProjEstado) = "cod_estado": .ColWidth(col_ProjEstado) = 0
'        .TextMatrix(0, col_ProjEstadoNom) = "Estado": .ColWidth(col_ProjEstadoNom) = 1400
'        .TextMatrix(0, col_ProjFchUlt) = "Fch. Ult.": .ColWidth(col_ProjFchUlt) = 0
'        .TextMatrix(0, col_ProjUsrUlt) = "Usuario Ult.": .ColWidth(col_ProjUsrUlt) = 0
'        .TextMatrix(0, col_ProjPlanTYO) = "Plan TYO": .ColWidth(col_ProjPlanTYO) = 1000
'        .TextMatrix(0, col_ProjEmpresa) = "Empresa": .ColWidth(col_ProjEmpresa) = 1400
'        .TextMatrix(0, col_ProjArea) = "Area": .ColWidth(col_ProjArea) = 1400
'        .TextMatrix(0, col_ProjClas3) = "Clase": .ColWidth(col_ProjClas3) = 1400
'        .TextMatrix(0, col_ProjSema1) = "Sm.": .ColWidth(col_ProjSema1) = 500
'        .TextMatrix(0, col_ProjOwner) = "Propietario": .ColWidth(col_ProjOwner) = 1200
'        .TextMatrix(0, col_ProjBP) = "cod_responsable": .ColWidth(col_ProjBP) = 0
'        .TextMatrix(0, col_ProjBPNom) = "Responsable": .ColWidth(col_ProjBPNom) = 1600
'        .TextMatrix(0, col_ProjAvance) = "% Av.": .ColWidth(col_ProjAvance) = 600
'        .TextMatrix(0, col_ProjFe_modif) = "F. Modif.": .ColWidth(col_ProjFe_modif) = 1200
'        .TextMatrix(0, col_ProjFe_inicio) = "F. Inicio": .ColWidth(col_ProjFe_inicio) = 1200
'        .TextMatrix(0, col_ProjFe_fin) = "F. Fin": .ColWidth(col_ProjFe_fin) = 1200
'        .TextMatrix(0, col_ProjFe_finreprog) = "F. reprog.": .ColWidth(col_ProjFe_finreprog) = 1200
'        .TextMatrix(0, col_ProjOrdenamiento) = "orden": .ColWidth(col_ProjOrdenamiento) = 0


'                .AddItem aplRST.Fields!projId & vbTab & aplRST.Fields!projSubId & vbTab & aplRST.Fields!projSubsId & vbTab & _
'                    aplRST.Fields!projId & "." & _
'                    aplRST.Fields!projSubId & "." & _
'                    aplRST.Fields!projSubsId & vbTab & _
'                    ClearNull(aplRST.Fields!ProjNom) & vbTab & Format(aplRST.Fields!ProjFchAlta, "Medium Date") & vbTab & vbTab & _
'                    ClearNull(aplRST.Fields!ProjCatId) & ". " & ClearNull(aplRST.Fields!ProjCatNom) & vbTab & vbTab & _
'                    ClearNull(aplRST.Fields!ProjClaseId) & ". " & ClearNull(aplRST.Fields!ProjClaseNom) & vbTab & vbTab & _
'                    ClearNull(aplRST.Fields!nom_estado) & vbTab & vbTab & vbTab & _
'                    ClearNull(aplRST.Fields!plan_tyo_nom) & vbTab & ClearNull(aplRST.Fields!nom_empresa) & vbTab & _
'                    ClearNull(aplRST.Fields!area_nom) & vbTab & ClearNull(aplRST.Fields!clas3_nom) & vbTab & vbTab & _
'                    ClearNull(aplRST.Fields!nom_areaprop) & vbTab & vbTab & _
'                    ClearNull(aplRST.Fields!nom_bpar) & vbTab & Format(aplRST.Fields!avance, "###,###,##0.0") & vbTab & _
'                    Format(ClearNull(aplRST.Fields!fe_modif), "Medium Date") & vbTab & Format(ClearNull(aplRST.Fields!fe_inicio), "Medium Date") & vbTab & _
'                    Format(ClearNull(aplRST.Fields!fe_fin), "Medium Date") & vbTab & Format(ClearNull(aplRST.Fields!fe_finreprog), "Medium Date"), .RowSel + 1




' Esto era para imprimir por pantalla un proyecto IDM
'    With grdDatos
'        Call PrintFormularioIDM(.TextMatrix(.RowSel, col_ProjId), .TextMatrix(.RowSel, col_ProjSubId), .TextMatrix(.RowSel, col_ProjSubsId))
'        frmPreview.Show
'    End With

'Private Sub Command1_Click()
'    Dim i As Long
'
'    'If sp_GetProyectoIDM(Null, Null, Null, Null, Null, Null, Null, Null) Then
'
'    With grdDatos
'        Call Puntero(True)
'        For i = 1 To .Rows - 1
'            Call Status(CStr(i))
'            If .TextMatrix(i, col_ProjMarca) = "B" Then
'                Call sp_UpdateProyectoIDMField(.TextMatrix(i, col_ProjId), .TextMatrix(i, col_ProjSubId), .TextMatrix(i, col_ProjSubsId), "tipo_proyecto", "I", Null, Null)
'                Call sp_UpdateProyectoIDMField(.TextMatrix(i, col_ProjId), .TextMatrix(i, col_ProjSubId), .TextMatrix(i, col_ProjSubsId), "COD_ESTADO", "DESEST", Null, Null)
'            Else
'                Call sp_UpdateProyectoIDMField(.TextMatrix(i, col_ProjId), .TextMatrix(i, col_ProjSubId), .TextMatrix(i, col_ProjSubsId), "tipo_proyecto", "P", Null, Null)
'            End If
'            Call sp_UpdateProyectoIDMField(.TextMatrix(i, col_ProjId), .TextMatrix(i, col_ProjSubId), .TextMatrix(i, col_ProjSubsId), "totalhs_ger", Null, Null, 0)
'            Call sp_UpdateProyectoIDMField(.TextMatrix(i, col_ProjId), .TextMatrix(i, col_ProjSubId), .TextMatrix(i, col_ProjSubsId), "totalhs_sec", Null, Null, 0)
'            Call sp_UpdateProyectoIDMField(.TextMatrix(i, col_ProjId), .TextMatrix(i, col_ProjSubId), .TextMatrix(i, col_ProjSubsId), "totalreplan", Null, Null, 0)
'            DoEvents
'        Next i
'        Call Status("Listo.")
'        Call Puntero(False)
'    End With
'    MsgBox "Proceso finalizado." & i - 1 & " registros actualizados."
'End Sub




'Private Sub Command1_Click()
'    Dim i As Long
'    Dim bAgregar As Boolean
'    Dim fecha_real As Date
'    Dim p() As oProyectoIDM
'
'    i = 0
'    Erase p
'    If MsgBox("Confirma inicio de proceso?", vbQuestion + vbYesNo) = vbYes Then
'        Call Puntero(True)
'        'If sp_GetProyectoIDM(Null, Null, Null, Null, "EJECUC|", Null, Null, Null) Then
'        If sp_GetProyectoIDM(Null, Null, Null, Null, "PRUEBA|SUSPEN|TERMIN|", Null, Null, Null) Then
'            MsgBox "Cantidad de proyectos: " & aplRST.RecordCount
'            Do While Not aplRST.EOF
'                ReDim Preserve p(i)
'                p(i).ProjId = ClearNull(aplRST.Fields!ProjId)
'                p(i).ProjSubId = ClearNull(aplRST.Fields!ProjSubId)
'                p(i).ProjSubSId = ClearNull(aplRST.Fields!ProjSubSId)
'                p(i).ProjFchAlta = ClearNull(aplRST.Fields!fch_estado)
'                i = i + 1
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        For i = 0 To UBound(p)
'            If sp_GetProyectoIDMHisto(p(i).ProjId, p(i).ProjSubId, p(i).ProjSubSId) Then
'                bAgregar = True
'                fecha_real = vbNull
'                Do While Not aplRST.EOF
'                    If ClearNull(aplRST.Fields!cod_evento) = "CHGEST" And ClearNull(aplRST.Fields!proj_estado) = "EJECUC" Then
'                        bAgregar = False
'                        If fecha_real = vbNull Then
'                            fecha_real = aplRST.Fields!hst_fecha
'                        Else
'                            If aplRST.Fields!hst_fecha < fecha_real Then
'                                fecha_real = aplRST.Fields!hst_fecha
'                                bAgregar = True
'                            End If
'                        End If
'                    End If
'                    aplRST.MoveNext
'                    DoEvents
'                Loop
'                If bAgregar Then        ' Agregar historial porque no existe el evento
'                    Call sp_InsertProyectoIDMHisto(p(i).ProjId, p(i).ProjSubId, p(i).ProjSubSId, p(i).ProjFchAlta, "CHGEST", "EJECUC", "Vuelco inicial", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
'                End If
'            Else    ' No hay historial: hay que agregar
'                Call sp_InsertProyectoIDMHisto(p(i).ProjId, p(i).ProjSubId, p(i).ProjSubSId, p(i).ProjFchAlta, "CHGEST", "EJECUC", "Vuelco inicial", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
'            End If
'        Next i
'        Call Puntero(False)
'        MsgBox "Proceso finalizado."
'    End If
'End Sub
'
'Private Sub Command1_Click()
'    Dim i As Long, j As Long
'    Dim a() As Long, b() As Long, c() As Long, d() As Long
'
'    If sp_GetProyectoIDM(Null, Null, Null, Null, Null, Null, Null, Null) Then
'        Call Puntero(True)
'        Do While Not aplRST.EOF
'            ReDim Preserve a(i)
'            ReDim Preserve b(i)
'            ReDim Preserve c(i)
'            a(i) = aplRST.Fields!ProjId
'            b(i) = aplRST.Fields!ProjSubId
'            c(i) = aplRST.Fields!ProjSubSId
'            d(i) = aplRST.Fields!ProjCatId
'            aplRST.MoveNext
'            DoEvents
'        Loop
'
'        For i = 0 To UBound(a)
'            If Not sp_GetProyIDMCategoria(d(i), Null) Then
'                Call sp_UpdateProyectoIDMField(a(i), b(i), c(i), "PROJCATID", Null, Null, 0)
'                Call sp_UpdateProyectoIDMField(a(i), b(i), c(i), "PROJCLASEID", Null, Null, 0)
'                j = j + 1
'            End If
'        Next i
'        Call Puntero(False)
'        MsgBox "Actualizados: " + j
'    End If
'End Sub

Private Sub GenerarUnaFicha()
    Dim objExcel As Object
    Dim xLibro As Object
    Dim sFileName As String
    Dim sDescripcion As String
    Dim sBeneficios As String
    Dim sNovedades As String
    Dim ProjId As Integer
    Dim ProjSId As Integer
    Dim ProjSubSId As Integer
    Dim projnom As String
    Dim sTexto As String
    Dim paginas As Integer
    Dim paginas_ENTERO As Integer
    Dim i As Integer, J As Integer, k As Integer, x As Integer
    Const xlPasteFormats = -4122
    Const xlPasteValues = -4163
    Const FilasPorPagina = 9           ' Antes: 19
    Const AlturaDeFila = 50           ' Antes: 25.5
    
    'Debug.Print "NUEVO proceso"
    
    If Not glMSOfficeExcelIsEnabled Then
        sTexto = "Error al intentar generar la exportaci�n." & vbCrLf & "El producto Microsoft Excel no est� instalado en el equipo."
        MsgBox sTexto, vbCritical + vbOKOnly, "Generando ficha de proyecto"
        Exit Sub
    End If
    
    '{ add -003- a.
    sTexto = "No se encuentra la plantilla para realizar la exportaci�n a Excel."
    Select Case glMSOfficeExcelVersion
        Case "11.0"         ' Office 2003
            If Dir(App.Path & "\prjidm11.xls", vbArchive) = "" Then
                MsgBox sTexto, vbCritical + vbOKOnly
                Exit Sub
            End If
        Case Else           ' Versiones posteriores
            If Dir(App.Path & "\prjidm13.xlsx", vbArchive) = "" Then
                MsgBox sTexto, vbCritical + vbOKOnly
                Exit Sub
            End If
    End Select
    '}
    
    sTexto = ""
    With grdDatos
        ProjId = .TextMatrix(.rowSel, col_ProjId)
        ProjSId = .TextMatrix(.rowSel, col_ProjSubId)
        ProjSubSId = .TextMatrix(.rowSel, col_ProjSubsId)
        
        sFileName = glWRKDIR & "prjidm_" & ProjId * 100000 + ProjSId * 100 + ProjSubSId
        sFileName = Dialogo_GuardarExportacion("GUARDAR", sFileName)
        If ClearNull(sFileName) <> "" Then
            Call Puntero(True)
            Call Status("Generando la ficha de proyecto...")
            Set objExcel = CreateObject("Excel.Application")
            If sp_GetProyectoIDM(ProjId, ProjSId, ProjSubSId, Null, Null, Null, Null, Null) Then
                sTexto = mdiPrincipal.sbPrincipal.Panels(1).text
                '{ add -003- a.
                Select Case glMSOfficeExcelVersion
                    Case "11.0"         ' Office 2003
                        Set xLibro = objExcel.Workbooks.Open(App.Path & "\prjidm11.xls", , , , Desencriptar("8489F59788F4F09780813439"))
                    Case Else           ' Posteriores: 2007, 2010, etc.
                        Set xLibro = objExcel.Workbooks.Open(App.Path & "\prjidm13.xlsx", , , , Desencriptar("8489F59788F4F09780813439"))
                End Select
                '}
                xLibro.SaveAs sFileName, , ""
                objExcel.visible = False
                With xLibro
                    With .Sheets("ANVERSO")
                        projnom = ClearNull(aplRST.Fields!projnom)
                        .Cells(4, c) = ClearNull(aplRST.Fields!projnom)                             ' T�tulo
                        .Cells(5, c) = ClearNull(aplRST.Fields!nom_areasoli)                        ' Solicitante
                        .Cells(6, c) = ClearNull(aplRST.Fields!nom_areaprop)                        ' Responsable
                        .Cells(9, G) = Format(ClearNull(aplRST.Fields!fe_modif), "mm/dd/yyyy")      ' Fecha de �ltima modif.
                        .Cells(10, G) = Val(ClearNull(aplRST.Fields!avance)) / 100                  ' % de avance
                        .Cells(11, G) = ClearNull(aplRST.Fields!nom_estado)                         ' Estado actual
                        .Cells(14, G) = Format(ClearNull(aplRST.Fields!fe_inicio), "mm/dd/yyyy")    ' Inicio
                        .Cells(15, G) = Format(ClearNull(aplRST.Fields!fe_fin), "mm/dd/yyyy")       ' Fin previsto
                        .Cells(16, G) = Format(ClearNull(aplRST.Fields!fe_finreprog), "mm/dd/yyyy") ' Fin reprogramado
                        Select Case ClearNull(aplRST.Fields!semaphore)
                            Case "1": .Range(.Cells(2, h), .Cells(3, h)).Interior.ColorIndex = COLOR_ROJO       ' rojo
                            Case "2": .Range(.Cells(2, h), .Cells(3, h)).Interior.ColorIndex = COLOR_AMARILLO   ' amarillo
                            Case "3": .Range(.Cells(2, h), .Cells(3, h)).Interior.ColorIndex = COLOR_VERDE      ' verde
                        End Select
                        sDescripcion = sp_GetProyectoIDMMemo(ProjId, ProjSId, ProjSubSId, "DESCRIPCIO", Null)
                        .Cells(8, b) = Mid(sDescripcion, 1, 1030)
                        sBeneficios = sp_GetProyectoIDMMemo(ProjId, ProjSId, ProjSubSId, "BENEFICIOS", Null)
                        .Cells(19, b) = Mid(sBeneficios, 1, 308)
                        sNovedades = sp_GetProyectoIDMNovedades3(ProjId, ProjSId, ProjSubSId, Null, "NOVEDADES", Null)
                        .Cells(21, b) = Mid(sNovedades, 1, 917)
                        i = 0
                        If sp_GetProyectoIDMAlertas(ProjId, ProjSId, ProjSubSId) Then
                            Do While Not aplRST.EOF And i < 3
                                .Cells(25 + i, b) = ""
                                Select Case ClearNull(aplRST.Fields!alert_tipo)
                                    Case "R": .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_ROJO     ' rojo
                                    Case "A": .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_AMARILLO ' amarillo
                                    Case Else: .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_BLANCO  ' Blanco
                                End Select
                                .Range(.Cells(25 + i, b), .Cells(25 + i, G)).HorizontalAlignment = xlLeft           ' Alineaci�n a la izquierda
                                .Cells(25 + i, c) = Mid(ClearNull(aplRST.Fields!alert_desc), 1, 94)                 ' Descripci�n
                                .Cells(25 + i, d) = Mid(ClearNull(aplRST.Fields!accion_id), 1, 64)                  ' Acci�n correctiva
                                .Cells(25 + i, E) = Format(ClearNull(aplRST.Fields!alert_fe_ini), "mm/dd/yyyy")     ' Fecha de inicio
                                .Cells(25 + i, F) = Format(ClearNull(aplRST.Fields!alert_fe_fin), "mm/dd/yyyy")     ' Fecha de fin
                                .Cells(25 + i, G) = ClearNull(aplRST.Fields!responsables)                           ' Responsables
                                aplRST.MoveNext
                                i = i + 1
                                DoEvents
                            Loop
                        End If
                    End With
                    '{ add -005- a.
                    With .Sheets("REVERSO")
                        .Activate
                        If sp_GetProyectoIDMHitos(ProjId, ProjSId, ProjSubSId, Null, Null, IIf(chkHitosExposicion.value = 1, "S", Null)) Then
                            ' Este proceso determina la cantidad de p�ginas a generar para los hitos
                            If aplRST.RecordCount < FilasPorPagina Then
                                paginas = 1
                            Else
                                If (aplRST.RecordCount Mod FilasPorPagina = 0) Then
                                    paginas = aplRST.RecordCount / FilasPorPagina
                                Else
                                    paginas_ENTERO = Fix(aplRST.RecordCount / FilasPorPagina)
                                    paginas = Fix(aplRST.RecordCount / FilasPorPagina)
                                    paginas = IIf(((aplRST.RecordCount / FilasPorPagina) - paginas_ENTERO) > 0, paginas + 1, paginas)
                                End If
                            End If
                            i = 0
                            k = 0       ' Items
                            .Cells(2, d) = projnom                                                                      ' Nombre del proyecto
                            For J = 1 To paginas
                                Do While Not aplRST.EOF
                                    ' Formato para las filas de hitos
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, d)).HorizontalAlignment = xlLeft             ' Alineaci�n a la izquierda
                                    .Range(.Cells(6, b), .Cells(6, F)).Copy
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
                                    .Cells(6 + i, b) = Format(ClearNull(aplRST.Fields!hito_fe_ini), "mm/dd/yyyy")       ' Fecha de inicio
                                    .Cells(6 + i, c) = Format(ClearNull(aplRST.Fields!hito_fe_fin), "mm/dd/yyyy")       ' Fecha de fin
                                    .Cells(6 + i, d) = Mid(ClearNull(aplRST.Fields!hito_nombre), 1, 100)                ' T�tulo del hito
                                    .Cells(6 + i, E) = Mid(ClearNull(aplRST.Fields!hito_descripcion), 1, 255)           ' Descripci�n del hito
                                    .Cells(6 + i, F) = ClearNull(aplRST.Fields!nom_estado)                              ' Estado
                                    k = k + 1
                                    aplRST.MoveNext
                                    If k = (FilasPorPagina * J) Then
                                        ' En el pie de p�gina grabo la p�gina actual
                                        i = i + 2
                                        .Cells(6 + i, F) = "P�gina " & J & " de " & paginas
                                        ' Nuevo encabezado
                                        If J < paginas Then             ' ADD!!!
                                            i = i + 5
                                            .Range(.Cells(2, b), .Cells(2, F)).Copy
                                            .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteValues
                                            .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                            i = i + 2
                                            .Range(.Cells(4, b), .Cells(4, F)).Copy
                                            .Range(.Cells(6 + i, b), .Cells(6 + i, b)).PasteSpecial Paste:=xlPasteValues
                                            .Range(.Cells(6 + i, b), .Cells(6 + i, b)).PasteSpecial Paste:=xlPasteFormats
                                            i = i + 1
                                            .Range(.Cells(5, b), .Cells(5, F)).Copy
                                            .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteValues
                                            .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                            i = i + 1
                                            Exit Do
                                        End If
                                    Else
                                        i = i + 1
                                    End If
                                    DoEvents
                                Loop
                            Next J
                            ' Si es la �ltima p�gina, completa en blanco las filas y escribe el pie de p�gina
                            If (paginas * FilasPorPagina) > k Then
                                x = (paginas * FilasPorPagina) - k
                                For J = 1 To x
                                    ' Formato para las filas de hitos
                                    .Range(.Cells(6, b), .Cells(6, F)).Copy
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
                                    .Cells(6 + i, b) = ""
                                    .Cells(6 + i, c) = ""
                                    .Cells(6 + i, d) = ""
                                    .Cells(6 + i, E) = ""
                                    .Cells(6 + i, F) = ""
                                    i = i + 1
                                Next J
                                ' En el pie de p�gina grabo la p�gina actual
                                i = i + 1
                                .Cells(6 + i, F) = "P�gina " & paginas & " de " & paginas
                            End If
                        Else
                            i = 0
                            For J = 1 To FilasPorPagina
                                ' Formato para las filas de hitos
                                .Range(.Cells(6, b), .Cells(6, F)).Copy
                                .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
                                .Cells(6 + i, b) = ""
                                .Cells(6 + i, c) = ""
                                .Cells(6 + i, d) = ""
                                .Cells(6 + i, E) = ""
                                .Cells(6 + i, F) = ""
                                i = i + 1
                            Next J
                            ' En el pie de p�gina grabo la p�gina actual
                            i = i + 1
                            .Cells(6 + i, F) = "P�gina 1 de 1"
                        End If
                        .Activate
                        .Range("A1").Select
                    End With
                    With .Sheets("ANVERSO")
                        .Activate
                        .Range("A1").Select
                    End With
                    '}
                End With
                
                With xLibro
                    .Save
                    .Close
                End With
                
                Set objExcel = Nothing
                Set xLibro = Nothing
                Call Status("Listo.")
                Call Puntero(False)
                If MsgBox("Generaci�n de ficha finalizada. �Abrir la ficha?", vbInformation + vbYesNo) = vbYes Then
                    Select Case glMSOfficeExcelVersion
                        Case "11.0"         ' Office 2003
                            Call EXEC_ShellExecute(Me.hWnd, IIf(InStr(1, sFileName, ".xls", vbTextCompare) > 0, sFileName, sFileName & ".xls"))
                        Case Else           ' Posteriores: 2007, 2010, etc.
                            Call EXEC_ShellExecute(Me.hWnd, IIf(InStr(1, sFileName, ".xlsx", vbTextCompare) > 0, sFileName, sFileName & ".xlsx"))
                    End Select
                End If
            End If
        End If
    End With
End Sub

'{ add -006- a.
Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    If grdDatos.row <= grdDatos.rowSel Then
        nRwD = grdDatos.row
        nRwH = grdDatos.rowSel
    Else
        nRwH = grdDatos.row
        nRwD = grdDatos.rowSel
    End If
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, colMultiSelect) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, colMultiSelect) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        If Button = vbLeftButton Then
            With grdDatos
                If .rowSel > 0 Then
                     If ClearNull(.TextMatrix(.rowSel, colMultiSelect)) = "" Then
                        .TextMatrix(.rowSel, colMultiSelect) = "�"
                     Else
                        .TextMatrix(.rowSel, colMultiSelect) = ""
                    End If
                End If
            End With
        End If
    End If
    If Shift = 0 Then
        If Button = vbLeftButton Then
            With grdDatos
                For i = 1 To .Rows - 1
                   .TextMatrix(i, colMultiSelect) = ""
                Next
                If .rowSel > 0 Then
                     .TextMatrix(.rowSel, colMultiSelect) = "�"
                End If
            End With
        End If
    End If
End Sub
'}

'{ add -006- a.
Private Sub chkExportarHitos_Click()
    If chkExportarHitos.value = 1 Then
        chkHitosExposicion.Enabled = True
        chkHitosExposicion.value = 0
    Else
        chkHitosExposicion.Enabled = False
        chkHitosExposicion.value = 0
    End If
End Sub
'}

'{ add -006- b.
Private Sub GenerarMultiplesFichas(ubicacion As String)
    Dim objExcel As Object
    Dim xLibro As Object
    Dim sFileName As String
    Dim sDescripcion As String
    Dim sBeneficios As String
    Dim sNovedades As String
    Dim ProjId As Integer
    Dim ProjSId As Integer
    Dim ProjSubSId As Integer
    Dim projnom As String
    Dim sTexto As String
    Dim paginas As Integer
    Dim bPDFEnabled As Boolean
    Dim y As Integer, i As Integer, J As Integer, k As Integer, x As Integer
    
    Const xlPasteFormats = -4122
    Const xlPasteValues = -4163
    Const xlTypePDF = 0
    Const xlQualityStandard = 0
    Const FilasPorPagina = 9            ' Antes: 19
    Const AlturaDeFila = 50             ' Antes: 25.5
    
    If Not glMSOfficeExcelIsEnabled Then
        sTexto = "Error al intentar generar la exportaci�n." & vbCrLf & "El producto Microsoft Excel no est� instalado en el equipo."
        MsgBox sTexto, vbCritical + vbOKOnly, "Generando ficha de proyecto"
        Exit Sub
    End If

    '{ add -003- a.
    sTexto = "No se encuentra la plantilla para realizar la exportaci�n a Excel."
    Select Case glMSOfficeExcelVersion
        Case "11.0"         ' Office 2003
            bPDFEnabled = False
            If Dir(App.Path & "\prjidm11.xls", vbArchive) = "" Then
                MsgBox sTexto, vbCritical + vbOKOnly
                Exit Sub
            End If
        Case Else           ' Versiones posteriores
            bPDFEnabled = True
            If Dir(App.Path & "\prjidm13.xlsx", vbArchive) = "" Then
                MsgBox sTexto, vbCritical + vbOKOnly
                Exit Sub
            End If
    End Select
    '}
    
    If Not bPDFEnabled And imgFichaFormato.ComboItems(2).Selected Then
        MsgBox "La versi�n actual de MS Office no soporta la generaci�n de archivos en PDF.", vbCritical + vbOKOnly
        Exit Sub
    End If
    
    Set objExcel = CreateObject("Excel.Application")
    With grdDatos
        objExcel.visible = False
        
        ' Pedir ubicaci�n de archivos a generar...
        'sFileName = Dialogo_GuardarExportacion("GUARDAR", sFileName)
        'If ClearNull(sFileName) <> "" Then
        
        Call Puntero(True)
        
        For y = 0 To UBound(vlProjId)
            Select Case glMSOfficeExcelVersion
                Case "11.0"         ' Office 2003
                    Set xLibro = objExcel.Workbooks.Open(App.Path & "\prjidm11.xls", , , , Desencriptar("8489F59788F4F09780813439"))
                Case Else           ' Posteriores: 2007, 2010, etc.
                    Set xLibro = objExcel.Workbooks.Open(App.Path & "\prjidm13.xlsx", , , , Desencriptar("8489F59788F4F09780813439"))
            End Select
            
            ProjId = vlProjId(y)
            ProjSId = vlProjSubId(y)
            ProjSubSId = vlProjSubSId(y)
            sTexto = ""
            
            Call Status("Generando la ficha de proyecto " & ProjId & "." & ProjSId & "." & ProjSubSId)
            
            sFileName = ubicacion & "\" & "prjidm_" & ProjId * 100000 + ProjSId * 100 + ProjSubSId & "_" & Format(Now, "yyyyMMdd_hhmmss")
            xLibro.SaveAs sFileName, , ""          ' OJOO!!!!

            If sp_GetProyectoIDM(ProjId, ProjSId, ProjSubSId, Null, Null, Null, Null, Null) Then
                sTexto = mdiPrincipal.sbPrincipal.Panels(1).text
                With xLibro.Sheets("ANVERSO")     ' Cambiar el nombre y agregar nuevas hojas segun cantidad de proyectos
                    projnom = ClearNull(aplRST.Fields!projnom)
                    .Cells(4, c) = ClearNull(aplRST.Fields!projnom)                             ' T�tulo
                    .Cells(5, c) = ClearNull(aplRST.Fields!nom_areasoli)                        ' Solicitante
                    .Cells(6, c) = ClearNull(aplRST.Fields!nom_areaprop)                        ' Responsable
                    .Cells(9, G) = Format(ClearNull(aplRST.Fields!fe_modif), "mm/dd/yyyy")      ' Fecha de �ltima modif.
                    .Cells(10, G) = Val(ClearNull(aplRST.Fields!avance)) / 100                  ' % de avance
                    .Cells(11, G) = ClearNull(aplRST.Fields!nom_estado)                         ' Estado actual
                    .Cells(14, G) = Format(ClearNull(aplRST.Fields!fe_inicio), "mm/dd/yyyy")    ' Inicio
                    .Cells(15, G) = Format(ClearNull(aplRST.Fields!fe_fin), "mm/dd/yyyy")       ' Fin previsto
                    .Cells(16, G) = Format(ClearNull(aplRST.Fields!fe_finreprog), "mm/dd/yyyy") ' Fin reprogramado
                    Select Case ClearNull(aplRST.Fields!semaphore)
                        Case "1": .Range(.Cells(2, h), .Cells(3, h)).Interior.ColorIndex = COLOR_ROJO       ' rojo
                        Case "2": .Range(.Cells(2, h), .Cells(3, h)).Interior.ColorIndex = COLOR_AMARILLO   ' amarillo
                        Case "3": .Range(.Cells(2, h), .Cells(3, h)).Interior.ColorIndex = COLOR_VERDE      ' verde
                    End Select
                    sDescripcion = sp_GetProyectoIDMMemo(ProjId, ProjSId, ProjSubSId, "DESCRIPCIO", Null)
                    .Cells(8, b) = Mid(sDescripcion, 1, 1030)
                    sBeneficios = sp_GetProyectoIDMMemo(ProjId, ProjSId, ProjSubSId, "BENEFICIOS", Null)
                    .Cells(19, b) = Mid(sBeneficios, 1, 308)
                    sNovedades = sp_GetProyectoIDMNovedades3(ProjId, ProjSId, ProjSubSId, Null, "NOVEDADES", Null)
                    .Cells(21, b) = Mid(sNovedades, 1, 917)
                    i = 0
                    If sp_GetProyectoIDMAlertas(ProjId, ProjSId, ProjSubSId) Then
                        Do While Not aplRST.EOF And i < 3
                            .Cells(25 + i, b) = ""
                            Select Case ClearNull(aplRST.Fields!alert_tipo)
                                Case "R": .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_ROJO     ' rojo
                                Case "A": .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_AMARILLO ' amarillo
                                Case Else: .Range(.Cells(25 + i, b), .Cells(25 + i, b)).Interior.ColorIndex = COLOR_BLANCO  ' Blanco
                            End Select
                            .Range(.Cells(25 + i, b), .Cells(25 + i, G)).HorizontalAlignment = xlLeft           ' Alineaci�n a la izquierda
                            .Cells(25 + i, c) = Mid(ClearNull(aplRST.Fields!alert_desc), 1, 94)                 ' Descripci�n
                            .Cells(25 + i, d) = Mid(ClearNull(aplRST.Fields!accion_id), 1, 64)                  ' Acci�n correctiva
                            .Cells(25 + i, E) = Format(ClearNull(aplRST.Fields!alert_fe_ini), "mm/dd/yyyy")     ' Fecha de inicio
                            .Cells(25 + i, F) = Format(ClearNull(aplRST.Fields!alert_fe_fin), "mm/dd/yyyy")     ' Fecha de fin
                            .Cells(25 + i, G) = ClearNull(aplRST.Fields!responsables)                           ' Responsables
                            aplRST.MoveNext
                            i = i + 1
                            DoEvents
                        Loop
                    End If
                End With
                With xLibro.Sheets("REVERSO")
                    .Activate
                    If sp_GetProyectoIDMHitos(ProjId, ProjSId, ProjSubSId, Null, Null, IIf(chkHitosExposicion.value = 1, "S", Null)) Then
                        If aplRST.RecordCount < FilasPorPagina Then
                            paginas = 1
                        Else
                            paginas = aplRST.RecordCount / FilasPorPagina
                        End If
                        i = 0
                        k = 0       ' Items
                        .Cells(2, d) = projnom                                                                      ' Nombre del proyecto
                        For J = 1 To paginas
                            Do While Not aplRST.EOF
                                ' Formato para las filas de hitos
                                .Range(.Cells(6 + i, b), .Cells(6 + i, d)).HorizontalAlignment = xlLeft             ' Alineaci�n a la izquierda
                                .Range(.Cells(6, b), .Cells(6, F)).Copy
                                .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
                                .Cells(6 + i, b) = Format(ClearNull(aplRST.Fields!hito_fe_ini), "mm/dd/yyyy")       ' Fecha de inicio
                                .Cells(6 + i, c) = Format(ClearNull(aplRST.Fields!hito_fe_fin), "mm/dd/yyyy")       ' Fecha de fin
                                .Cells(6 + i, d) = Mid(ClearNull(aplRST.Fields!hito_nombre), 1, 100)                ' T�tulo del hito
                                .Cells(6 + i, E) = Mid(ClearNull(aplRST.Fields!hito_descripcion), 1, 255)           ' Descripci�n del hito
                                .Cells(6 + i, F) = ClearNull(aplRST.Fields!nom_estado)                              ' Estado
                                k = k + 1
                                aplRST.MoveNext
                                If k = (FilasPorPagina * J) Then
                                    ' En el pie de p�gina grabo la p�gina actual
                                    i = i + 2
                                    .Cells(6 + i, F) = "P�gina " & J & " de " & paginas
                                    ' Nuevo encabezado
                                    i = i + 5
                                    .Range(.Cells(2, b), .Cells(2, F)).Copy
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteValues
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                    i = i + 2
                                    .Range(.Cells(4, b), .Cells(4, F)).Copy
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, b)).PasteSpecial Paste:=xlPasteValues
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, b)).PasteSpecial Paste:=xlPasteFormats
                                    ' Nuevo encabezado
                                    i = i + 1
                                    .Range(.Cells(5, b), .Cells(5, F)).Copy
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteValues
                                    .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                    i = i + 1
                                    Exit Do
                                Else
                                    i = i + 1
                                End If
                                DoEvents
                            Loop
                        Next J
                        ' Si es la �ltima p�gina, completa en blanco las filas y escribe el pie de p�gina
                        If (paginas * FilasPorPagina) > k Then
                            x = (paginas * FilasPorPagina) - k
                            For J = 1 To x
                                ' Formato para las filas de hitos
                                .Range(.Cells(6, b), .Cells(6, F)).Copy
                                .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                                .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
                                .Cells(6 + i, b) = ""
                                .Cells(6 + i, c) = ""
                                .Cells(6 + i, d) = ""
                                .Cells(6 + i, E) = ""
                                .Cells(6 + i, F) = ""
                                i = i + 1
                            Next J
                            ' En el pie de p�gina grabo la p�gina actual
                            i = i + 1
                            .Cells(6 + i, F) = "P�gina " & paginas & " de " & paginas
                        End If
                    Else
                        i = 0
                        For J = 1 To FilasPorPagina
                            ' Formato para las filas de hitos
                            .Range(.Cells(6, b), .Cells(6, F)).Copy
                            .Range(.Cells(6 + i, b), .Cells(6 + i, F)).PasteSpecial Paste:=xlPasteFormats
                            .Range(.Cells(6 + i, b), .Cells(6 + i, F)).RowHeight = AlturaDeFila
                            .Cells(6 + i, b) = ""
                            .Cells(6 + i, c) = ""
                            .Cells(6 + i, d) = ""
                            .Cells(6 + i, E) = ""
                            .Cells(6 + i, F) = ""
                            i = i + 1
                        Next J
                        ' En el pie de p�gina grabo la p�gina actual
                        i = i + 1
                        .Cells(6 + i, F) = "P�gina 1 de 1"
                    End If
                    .Activate
                    .Range("A1").Select
                End With
                With xLibro.Sheets("ANVERSO")
                    .Activate
                    .Range("A1").Select
                End With
            End If
            
            With xLibro
                .Save
                If imgFichaFormato.ComboItems(2).Selected Then
                    .ActiveWorkbook.ExportAsFixedFormat Type:=xlTypePDF, FileName:="C:\GesPet\" & Trim(sFileName) & ".pdf", Quality:=xlQualityStandard, IncludeDocProperties:=True, IgnorePrintAreas:=False, OpenAfterPublish:=False
                End If
                .Close
            End With
            Set xLibro = Nothing
        Next y
            
        Set objExcel = Nothing
        Call Status("Listo.")
        Call Puntero(False)
        MsgBox "Proceso finalizado.", vbInformation + vbOKOnly
    End With
End Sub
'}
