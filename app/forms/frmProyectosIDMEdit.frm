VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmProyectosIDMEdit 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edici�n de Proyectos IDM"
   ClientHeight    =   8820
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11820
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmProyectosIDMEdit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8820
   ScaleWidth      =   11820
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab sstProyectosIDM 
      Height          =   4455
      Left            =   180
      TabIndex        =   15
      Top             =   4080
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   7858
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      TabsPerRow      =   8
      TabHeight       =   520
      ForeColor       =   13798741
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmProyectosIDMEdit.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Line1(3)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Line1(2)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Line1(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblProyIDM(10)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblProyIDM(11)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblProyIDM(12)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblTamanioImpresion"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "txtDescripcion"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "txtBeneficios"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "txtNovedades"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).ControlCount=   10
      TabCaption(1)   =   "Alertas && Riesgos"
      TabPicture(1)   =   "frmProyectosIDMEdit.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdAlertas"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "fraAlertas"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "fraABMAlertas"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Hitos detallados"
      TabPicture(2)   =   "frmProyectosIDMEdit.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraABMHitos"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "grdHitos"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "fraHitos"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Areas responsables"
      TabPicture(3)   =   "frmProyectosIDMEdit.frx":0060
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "grdAreasResponsables"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "fraResponsables"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "fraABMResponsables"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).ControlCount=   3
      TabCaption(4)   =   "Sectores y grupos involucrados"
      TabPicture(4)   =   "frmProyectosIDMEdit.frx":007C
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "grdProyectoGrupos"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Documentos adjuntos"
      TabPicture(5)   =   "frmProyectosIDMEdit.frx":0098
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "grdAdjuntos"
      Tab(5).Control(0).Enabled=   0   'False
      Tab(5).Control(1)=   "fraBotoneraAdjuntos"
      Tab(5).Control(1).Enabled=   0   'False
      Tab(5).Control(2)=   "fraAdjuntos"
      Tab(5).Control(2).Enabled=   0   'False
      Tab(5).ControlCount=   3
      TabCaption(6)   =   " Hs. x Ger./Sector"
      TabPicture(6)   =   "frmProyectosIDMEdit.frx":00B4
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "grdHoras"
      Tab(6).Control(0).Enabled=   0   'False
      Tab(6).Control(1)=   "fraHoras"
      Tab(6).Control(1).Enabled=   0   'False
      Tab(6).Control(2)=   "fraABMHoras"
      Tab(6).Control(2).Enabled=   0   'False
      Tab(6).ControlCount=   3
      Begin VB.Frame fraABMHoras 
         Height          =   1335
         Left            =   -74880
         TabIndex        =   154
         Top             =   3000
         Width           =   10455
         Begin VB.OptionButton optHorasDeclaradas 
            Caption         =   "Hs. planificadas por Gerencia"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   120
            TabIndex        =   160
            Top             =   240
            Width           =   1695
         End
         Begin VB.OptionButton optHorasDeclaradas 
            Caption         =   "Hs. estimadas por Sector"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   159
            Top             =   720
            Value           =   -1  'True
            Width           =   1695
         End
         Begin VB.TextBox txtAreaHoras 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2640
            TabIndex        =   158
            Top             =   600
            Width           =   855
         End
         Begin VB.ComboBox cmbHorasArea 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2640
            Style           =   2  'Dropdown List
            TabIndex        =   155
            Top             =   240
            Width           =   7560
         End
         Begin VB.Label lblHorasModoTrabajo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "HorasModoTrabajo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   8715
            TabIndex        =   161
            Top             =   0
            Width           =   1620
         End
         Begin VB.Label lblHoras 
            AutoSize        =   -1  'True
            Caption         =   "Horas:"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   1
            Left            =   2040
            TabIndex        =   157
            Top             =   600
            Width           =   510
         End
         Begin VB.Label lblHoras 
            AutoSize        =   -1  'True
            Caption         =   "�rea:"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   0
            Left            =   2040
            TabIndex        =   156
            Top             =   240
            Width           =   420
         End
      End
      Begin VB.Frame fraHoras 
         Height          =   4030
         Left            =   -64320
         TabIndex        =   148
         Top             =   300
         Width           =   615
         Begin VB.CommandButton cmdHoras_Cancelar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":00D0
            Style           =   1  'Graphical
            TabIndex        =   149
            ToolTipText     =   "Cancelar la operaci�n actual"
            Top             =   1680
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras_Confirmar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":0C0A
            Style           =   1  'Graphical
            TabIndex        =   150
            ToolTipText     =   "Confirmar"
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras_Eliminar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":1744
            Style           =   1  'Graphical
            TabIndex        =   151
            ToolTipText     =   "Quitar un �rea"
            Top             =   960
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras_Modificar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":227E
            Style           =   1  'Graphical
            TabIndex        =   152
            ToolTipText     =   "Modificar un �rea"
            Top             =   600
            Width           =   375
         End
         Begin VB.CommandButton cmdHoras_Agregar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":2DB8
            Style           =   1  'Graphical
            TabIndex        =   153
            ToolTipText     =   "Agregar un �rea"
            Top             =   240
            Width           =   375
         End
      End
      Begin VB.Frame fraAdjuntos 
         ForeColor       =   &H00000000&
         Height          =   1575
         Left            =   -74880
         TabIndex        =   121
         Top             =   2760
         Width           =   10455
         Begin VB.TextBox txtAdjuntoUsuario 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            ForeColor       =   &H00808080&
            Height          =   250
            Left            =   1800
            Locked          =   -1  'True
            TabIndex        =   132
            Text            =   "-"
            Top             =   960
            Width           =   2655
         End
         Begin VB.TextBox txtAdjuntoFecha 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            ForeColor       =   &H00808080&
            Height          =   250
            Left            =   1800
            Locked          =   -1  'True
            TabIndex        =   131
            Text            =   "-"
            Top             =   1200
            Width           =   1455
         End
         Begin VB.TextBox txtAdjuntoArea 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            ForeColor       =   &H00808080&
            Height          =   250
            Left            =   1800
            Locked          =   -1  'True
            TabIndex        =   130
            Text            =   "-"
            Top             =   720
            Width           =   5295
         End
         Begin VB.TextBox txtAdjuntoDescripcion 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            ForeColor       =   &H00808080&
            Height          =   250
            Left            =   1800
            TabIndex        =   129
            Text            =   "-"
            Top             =   480
            Width           =   8415
         End
         Begin VB.TextBox txtAdjuntoFile 
            BackColor       =   &H8000000F&
            BorderStyle     =   0  'None
            ForeColor       =   &H00808080&
            Height          =   250
            Left            =   1800
            Locked          =   -1  'True
            TabIndex        =   128
            Text            =   "-"
            Top             =   240
            Width           =   5295
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Usuario"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   180
            Index           =   10
            Left            =   120
            TabIndex        =   127
            Top             =   960
            Width           =   585
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Fecha"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   180
            Index           =   9
            Left            =   120
            TabIndex        =   126
            Top             =   1200
            Width           =   450
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "�rea que adjunt�"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   180
            Index           =   8
            Left            =   120
            TabIndex        =   125
            Top             =   720
            Width           =   1275
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   180
            Index           =   7
            Left            =   120
            TabIndex        =   124
            Top             =   480
            Width           =   900
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Nombre del archivo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   180
            Index           =   6
            Left            =   120
            TabIndex        =   123
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblAdjuntosModoTrabajo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "AdjuntosModoTrabajo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   8445
            TabIndex        =   122
            Top             =   0
            Width           =   1890
         End
      End
      Begin VB.Frame fraBotoneraAdjuntos 
         Height          =   4030
         Left            =   -64320
         TabIndex        =   115
         Top             =   300
         Width           =   615
         Begin VB.CommandButton cmdAdjuntos_Cancelar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":38F2
            Style           =   1  'Graphical
            TabIndex        =   116
            ToolTipText     =   "Cancelar la operaci�n actual"
            Top             =   2040
            Width           =   375
         End
         Begin VB.CommandButton cmdAdjuntos_Confirmar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":442C
            Style           =   1  'Graphical
            TabIndex        =   117
            ToolTipText     =   "Confirmar"
            Top             =   1680
            Width           =   375
         End
         Begin VB.CommandButton cmdAdjuntos_Eliminar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":4F66
            Style           =   1  'Graphical
            TabIndex        =   118
            ToolTipText     =   "Desadjuntar documento"
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdAdjuntos_Modificar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":5AA0
            Style           =   1  'Graphical
            TabIndex        =   119
            ToolTipText     =   "Modificar documento"
            Top             =   960
            Width           =   375
         End
         Begin VB.CommandButton cmdAdjuntos_Agregar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":65DA
            Style           =   1  'Graphical
            TabIndex        =   120
            ToolTipText     =   "Adjuntar un documento"
            Top             =   600
            Width           =   375
         End
         Begin VB.CommandButton cmdAdjuntos_Visualizar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":7114
            Style           =   1  'Graphical
            TabIndex        =   134
            ToolTipText     =   "Visualizar el documento seleccionado"
            Top             =   240
            Width           =   375
         End
      End
      Begin VB.Frame fraABMResponsables 
         Height          =   1335
         Left            =   -74880
         TabIndex        =   111
         Top             =   3000
         Width           =   10455
         Begin VB.ComboBox cboAreaResponsable 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1680
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   113
            Top             =   240
            Width           =   8160
         End
         Begin VB.Label lblAreaResponsableDatos 
            AutoSize        =   -1  'True
            Caption         =   "Nivel"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   555
            Left            =   240
            TabIndex        =   135
            Top             =   600
            Width           =   9975
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblResponsablesModoTrabajo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "ResponsablesModoTrabajo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   8040
            TabIndex        =   114
            Top             =   0
            Width           =   2295
         End
         Begin VB.Label lblABMResponsable 
            AutoSize        =   -1  'True
            Caption         =   "�rea responsable"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   240
            TabIndex        =   112
            Top             =   300
            Width           =   1320
         End
      End
      Begin VB.Frame fraResponsables 
         Height          =   4030
         Left            =   -64320
         TabIndex        =   105
         Top             =   300
         Width           =   615
         Begin VB.CommandButton cmdResponsables_Cancelar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":7456
            Style           =   1  'Graphical
            TabIndex        =   106
            ToolTipText     =   "Cancelar la operaci�n actual"
            Top             =   1680
            Width           =   375
         End
         Begin VB.CommandButton cmdResponsables_Confirmar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":7F90
            Style           =   1  'Graphical
            TabIndex        =   107
            ToolTipText     =   "Confirmar"
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdResponsables_Eliminar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":8ACA
            Style           =   1  'Graphical
            TabIndex        =   108
            ToolTipText     =   "Quitar un �rea"
            Top             =   960
            Width           =   375
         End
         Begin VB.CommandButton cmdResponsables_Modificar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":9604
            Style           =   1  'Graphical
            TabIndex        =   109
            ToolTipText     =   "Modificar un �rea"
            Top             =   600
            Width           =   375
         End
         Begin VB.CommandButton cmdResponsables_Agregar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":A13E
            Style           =   1  'Graphical
            TabIndex        =   110
            ToolTipText     =   "Agregar un �rea"
            Top             =   240
            Width           =   375
         End
      End
      Begin VB.Frame fraABMAlertas 
         Height          =   2175
         Left            =   -74880
         TabIndex        =   37
         Top             =   2160
         Width           =   10455
         Begin VB.CheckBox chkAlertaResuelta 
            Caption         =   "Resuelta"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   7560
            TabIndex        =   96
            Top             =   270
            Width           =   975
         End
         Begin VB.TextBox txtAlertaItem 
            Height          =   285
            Left            =   3960
            Locked          =   -1  'True
            TabIndex        =   77
            Text            =   "txtAlertaItem"
            Top             =   240
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txtAlertaDescripcion 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1560
            TabIndex        =   80
            Top             =   600
            Width           =   8655
         End
         Begin VB.TextBox txtAccionCorrectiva 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1560
            TabIndex        =   81
            Top             =   960
            Width           =   8655
         End
         Begin VB.TextBox txtAlertasObservaciones 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   4920
            MaxLength       =   255
            TabIndex        =   84
            Top             =   1320
            Width           =   5295
         End
         Begin VB.ComboBox cmbAlertaTipo 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   79
            Top             =   240
            Width           =   2295
         End
         Begin VB.Frame fraAlertasTextos 
            BorderStyle     =   0  'None
            Caption         =   "Frame2"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   3120
            TabIndex        =   76
            Top             =   1320
            Width           =   1695
            Begin VB.OptionButton optAlertasTexto 
               Caption         =   "Responsables"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   83
               Top             =   240
               Width           =   1455
            End
            Begin VB.OptionButton optAlertasTexto 
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "Verdana"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   82
               Top             =   0
               Width           =   1455
            End
         End
         Begin AT_MaskText.MaskText txtFechaResolucion 
            Height          =   315
            Left            =   8730
            TabIndex        =   169
            Top             =   240
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin AT_MaskText.MaskText txtAlertaFechaIni 
            Height          =   315
            Left            =   1560
            TabIndex        =   170
            Top             =   1320
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin AT_MaskText.MaskText txtAlertaFechaFin 
            Height          =   315
            Left            =   1560
            TabIndex        =   171
            Top             =   1680
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin VB.Label lblAlertasModoTrabajo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "AlertasModoTrabajo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   8505
            TabIndex        =   61
            Top             =   0
            Width           =   1740
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   20
            Left            =   150
            TabIndex        =   42
            Top             =   667
            Width           =   900
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de Fin"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   19
            Left            =   150
            TabIndex        =   41
            Top             =   1740
            Width           =   945
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de Inicio"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   18
            Left            =   150
            TabIndex        =   40
            Top             =   1380
            Width           =   1155
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Acci�n correctiva"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   17
            Left            =   150
            TabIndex        =   39
            Top             =   1020
            Width           =   1335
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Alerta"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   1
            Left            =   150
            TabIndex        =   38
            Top             =   307
            Width           =   1065
         End
      End
      Begin VB.Frame fraHitos 
         Height          =   4030
         Left            =   -64320
         TabIndex        =   32
         Top             =   300
         Width           =   615
         Begin VB.CommandButton cmdHitos_Cancelar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":AC78
            Style           =   1  'Graphical
            TabIndex        =   33
            ToolTipText     =   "Cancelar la operaci�n actual"
            Top             =   1680
            Width           =   375
         End
         Begin VB.CommandButton cmdHitos_Confirmar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":B7B2
            Style           =   1  'Graphical
            TabIndex        =   70
            ToolTipText     =   "Confirmar"
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdHitos_Eliminar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":C2EC
            Style           =   1  'Graphical
            TabIndex        =   34
            ToolTipText     =   "Quitar un hito"
            Top             =   960
            Width           =   375
         End
         Begin VB.CommandButton cmdHitos_Modificar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":CE26
            Style           =   1  'Graphical
            TabIndex        =   35
            ToolTipText     =   "Modificar un hito"
            Top             =   600
            Width           =   375
         End
         Begin VB.CommandButton cmdHitos_Agregar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":D960
            Style           =   1  'Graphical
            TabIndex        =   36
            ToolTipText     =   "Agregar un hito"
            Top             =   240
            Width           =   375
         End
      End
      Begin VB.Frame fraAlertas 
         Height          =   4030
         Left            =   -64320
         TabIndex        =   27
         Top             =   300
         Width           =   615
         Begin VB.CommandButton cmdAlertas_Cancelar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":E49A
            Style           =   1  'Graphical
            TabIndex        =   43
            ToolTipText     =   "Cancelar la operaci�n actual"
            Top             =   1680
            Width           =   375
         End
         Begin VB.CommandButton cmdAlertas_Confirmar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":EFD4
            Style           =   1  'Graphical
            TabIndex        =   31
            ToolTipText     =   "Guardar"
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdAlertas_Eliminar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":FB0E
            Style           =   1  'Graphical
            TabIndex        =   30
            ToolTipText     =   "Quitar un alerta"
            Top             =   960
            Width           =   375
         End
         Begin VB.CommandButton cmdAlertas_Modificar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":10648
            Style           =   1  'Graphical
            TabIndex        =   29
            ToolTipText     =   "Modificar un alerta"
            Top             =   600
            Width           =   375
         End
         Begin VB.CommandButton cmdAlertas_Agregar 
            Height          =   375
            Left            =   120
            Picture         =   "frmProyectosIDMEdit.frx":11182
            Style           =   1  'Graphical
            TabIndex        =   28
            ToolTipText     =   "Agregar un alerta"
            Top             =   240
            Width           =   375
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grdAlertas 
         Height          =   1815
         Left            =   -74880
         TabIndex        =   20
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   3201
         _Version        =   393216
         FixedCols       =   0
         BackColorSel    =   12682268
         ForeColorSel    =   16777215
         GridColor       =   14737632
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtNovedades 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   16
         Top             =   2760
         Width           =   11175
      End
      Begin VB.TextBox txtBeneficios 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   12
         Top             =   1800
         Width           =   11175
      End
      Begin VB.TextBox txtDescripcion 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         Top             =   720
         Width           =   11175
      End
      Begin MSFlexGridLib.MSFlexGrid grdHitos 
         Height          =   1695
         Left            =   -74880
         TabIndex        =   21
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   2990
         _Version        =   393216
         FixedCols       =   0
         BackColorSel    =   12682268
         ForeColorSel    =   16777215
         GridColor       =   14737632
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame fraABMHitos 
         Height          =   2295
         Left            =   -74880
         TabIndex        =   64
         Top             =   2040
         Width           =   10500
         Begin VB.TextBox txtHitoDescripcion 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   795
            Left            =   1170
            MaxLength       =   255
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   93
            Top             =   1320
            Width           =   8895
         End
         Begin VB.ComboBox cmbHitoClasificacion 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   3480
            Style           =   2  'Dropdown List
            TabIndex        =   91
            Top             =   960
            Width           =   2895
         End
         Begin VB.ComboBox cmbHitoExposicion 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1170
            Style           =   2  'Dropdown List
            TabIndex        =   90
            Top             =   960
            Width           =   975
         End
         Begin VB.ComboBox cmbHitoEstado 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   3480
            Style           =   2  'Dropdown List
            TabIndex        =   88
            Top             =   600
            Width           =   2895
         End
         Begin AT_MaskText.MaskText txtHitoInicio 
            Height          =   315
            Left            =   1170
            TabIndex        =   85
            Top             =   240
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin AT_MaskText.MaskText txtHitoFinalizacion 
            Height          =   315
            Left            =   1170
            TabIndex        =   87
            Top             =   600
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin AT_MaskText.MaskText txtHitoOrden 
            Height          =   315
            Left            =   7080
            TabIndex        =   89
            Top             =   600
            Width           =   645
            _ExtentX        =   1138
            _ExtentY        =   556
            MaxLength       =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   4
         End
         Begin VB.ComboBox cmbHito 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   3480
            TabIndex        =   86
            Text            =   "cmbHito"
            Top             =   240
            Width           =   6615
         End
         Begin VB.TextBox txtHitoNombre 
            Height          =   300
            Left            =   3480
            MaxLength       =   100
            TabIndex        =   94
            Top             =   240
            Width           =   6615
         End
         Begin VB.ComboBox cboHitosAreaResp 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1170
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   92
            Top             =   1320
            Visible         =   0   'False
            Width           =   8895
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Clasificaci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   35
            Left            =   2340
            TabIndex        =   173
            Top             =   1020
            Width           =   975
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Exposici�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   34
            Left            =   120
            TabIndex        =   172
            Top             =   1020
            Width           =   825
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Orden"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   33
            Left            =   6480
            TabIndex        =   168
            Top             =   660
            Width           =   465
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "�rea resp."
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   27
            Left            =   120
            TabIndex        =   104
            Top             =   1680
            Visible         =   0   'False
            Width           =   780
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   26
            Left            =   120
            TabIndex        =   78
            Top             =   1320
            Width           =   900
         End
         Begin VB.Label lblHitosModoTrabajo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "HitosModoTrabajo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   8700
            TabIndex        =   69
            Top             =   0
            Width           =   1560
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   24
            Left            =   2790
            TabIndex        =   68
            Top             =   660
            Width           =   525
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Hito"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   23
            Left            =   3000
            TabIndex        =   67
            Top             =   300
            Width           =   315
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Inicio"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   22
            Left            =   120
            TabIndex        =   66
            Top             =   300
            Width           =   435
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Finalizaci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   21
            Left            =   120
            TabIndex        =   65
            Top             =   660
            Width           =   885
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grdAreasResponsables 
         Height          =   2655
         Left            =   -74880
         TabIndex        =   102
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   4683
         _Version        =   393216
         FixedCols       =   0
         BackColorSel    =   12682268
         ForeColorSel    =   16777215
         GridColor       =   14737632
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdProyectoGrupos 
         Height          =   3855
         Left            =   -74880
         TabIndex        =   103
         Top             =   360
         Width           =   11175
         _ExtentX        =   19711
         _ExtentY        =   6800
         _Version        =   393216
         FixedCols       =   0
         BackColorSel    =   12682268
         ForeColorSel    =   16777215
         GridColor       =   14737632
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdAdjuntos 
         Height          =   2415
         Left            =   -74880
         TabIndex        =   133
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   4260
         _Version        =   393216
         FixedCols       =   0
         BackColorSel    =   12682268
         ForeColorSel    =   16777215
         GridColor       =   14737632
         FocusRect       =   0
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdHoras 
         Height          =   2655
         Left            =   -74880
         TabIndex        =   145
         Top             =   360
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   4683
         _Version        =   393216
         FixedCols       =   0
         BackColorSel    =   12682268
         ForeColorSel    =   16777215
         GridColor       =   14737632
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblTamanioImpresion 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "lblTamanioImpresion"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   165
         Left            =   10005
         TabIndex        =   162
         Top             =   360
         Visible         =   0   'False
         Width           =   1290
      End
      Begin VB.Label lblProyIDM 
         AutoSize        =   -1  'True
         Caption         =   "Novedades del mes *"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   195
         Index           =   12
         Left            =   120
         TabIndex        =   19
         Top             =   2520
         Width           =   1785
      End
      Begin VB.Label lblProyIDM 
         AutoSize        =   -1  'True
         Caption         =   "Beneficios *"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   18
         Top             =   1560
         Width           =   1005
      End
      Begin VB.Label lblProyIDM 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n *"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   17
         Top             =   480
         Width           =   1125
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         Index           =   1
         X1              =   1080
         X2              =   11280
         Y1              =   1680
         Y2              =   1680
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         Index           =   2
         X1              =   1200
         X2              =   11280
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         Index           =   3
         X1              =   1920
         X2              =   11280
         Y1              =   2640
         Y2              =   2640
      End
   End
   Begin VB.Frame Frame1 
      Height          =   8775
      Left            =   60
      TabIndex        =   14
      Top             =   0
      Width           =   11655
      Begin VB.Frame fraProyectoIDM 
         Height          =   780
         Left            =   120
         TabIndex        =   44
         Top             =   100
         Width           =   11415
         Begin VB.TextBox txtCodigoGPS 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   285
            Left            =   7320
            MaxLength       =   11
            TabIndex        =   164
            Text            =   "GP.90000089"
            Top             =   120
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.TextBox txtNombreProyecto 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   285
            Left            =   960
            TabIndex        =   0
            Text            =   "NombreProyecto"
            Top             =   428
            Visible         =   0   'False
            Width           =   7815
         End
         Begin VB.Label lblCodigoGPS 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblCodigoGPS"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   7440
            TabIndex        =   165
            Top             =   200
            Width           =   1275
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "C�digo GPS"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   180
            Index           =   11
            Left            =   6360
            TabIndex        =   163
            Top             =   195
            Width           =   900
         End
         Begin VB.Label FchUltAct 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "FchUltAct"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   180
            Left            =   10500
            TabIndex        =   99
            Top             =   480
            Width           =   765
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "�lt. act."
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   180
            Index           =   5
            Left            =   9120
            TabIndex        =   98
            Top             =   480
            Width           =   600
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Modo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   180
            Index           =   4
            Left            =   9120
            TabIndex        =   97
            Top             =   240
            Width           =   405
         End
         Begin VB.Label lblModo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblModo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   180
            Left            =   10680
            TabIndex        =   95
            Top             =   240
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.Label lblNombreProyecto 
            Caption         =   "NombreProyecto"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   960
            TabIndex        =   48
            Top             =   480
            Width           =   7755
         End
         Begin VB.Label lblCodigoProyecto 
            AutoSize        =   -1  'True
            Caption         =   "CodigoProyecto"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00D28D55&
            Height          =   195
            Left            =   960
            TabIndex        =   47
            Top             =   200
            Width           =   1530
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   180
            Index           =   0
            Left            =   120
            TabIndex        =   46
            Top             =   200
            Width           =   525
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Nombre *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   180
            Index           =   1
            Left            =   120
            TabIndex        =   45
            Top             =   480
            Width           =   735
         End
      End
      Begin VB.Frame fraDatosCabecera 
         Height          =   3165
         Left            =   120
         TabIndex        =   49
         Top             =   840
         Width           =   10000
         Begin MSComCtl2.UpDown udSemaforo 
            Height          =   255
            Left            =   9360
            TabIndex        =   174
            Top             =   240
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   450
            _Version        =   393216
            Value           =   1
            Max             =   3
            Min             =   1
            Enabled         =   -1  'True
         End
         Begin VB.ComboBox cmbSubestado 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   5880
            Style           =   2  'Dropdown List
            TabIndex        =   144
            Top             =   2760
            Width           =   2400
         End
         Begin AT_MaskText.MaskText txtHsPlanifGer 
            Height          =   315
            Left            =   9120
            TabIndex        =   139
            Top             =   1680
            Width           =   765
            _ExtentX        =   1349
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.ComboBox cmbTipoProyecto 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   138
            Top             =   2760
            Width           =   3135
         End
         Begin VB.ComboBox cmbPropietario 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   101
            Top             =   960
            Width           =   7680
         End
         Begin VB.ComboBox cmbClase 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   6240
            Style           =   2  'Dropdown List
            TabIndex        =   75
            Top             =   240
            Width           =   2775
         End
         Begin VB.ComboBox cmbCategoria 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   74
            Top             =   240
            Width           =   3495
         End
         Begin VB.CheckBox chkRecalcularSemaforo 
            Alignment       =   1  'Right Justify
            Caption         =   "Recalcular"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   8760
            TabIndex        =   63
            ToolTipText     =   "Recalcular el valor del sem�foro"
            Top             =   1320
            Width           =   1095
         End
         Begin VB.ComboBox cmbClas3 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   2400
            Width           =   3135
         End
         Begin VB.ComboBox cmbArea 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   4
            Top             =   2040
            Width           =   3135
         End
         Begin VB.ComboBox cmbEmpresa 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   1680
            Width           =   3135
         End
         Begin VB.ComboBox cmbPlanTyO 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   1320
            Width           =   975
         End
         Begin VB.ComboBox cmbEstado 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   5880
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   2400
            Width           =   2400
         End
         Begin AT_MaskText.MaskText txtInicio 
            Height          =   315
            Left            =   5880
            TabIndex        =   8
            Top             =   1320
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin AT_MaskText.MaskText txtFinPrevisto 
            Height          =   315
            Left            =   5880
            TabIndex        =   9
            Top             =   1680
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin AT_MaskText.MaskText txtFinReprogramado 
            Height          =   315
            Left            =   5880
            TabIndex        =   10
            Top             =   2040
            Width           =   1485
            _ExtentX        =   2619
            _ExtentY        =   556
            MaxLength       =   10
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxLength       =   10
            UpperCase       =   -1  'True
            DataType        =   3
            SpecialFeatures =   -1  'True
            TabOnEnter      =   -1  'True
         End
         Begin AT_MaskText.MaskText txtAvance 
            Height          =   315
            Left            =   3585
            TabIndex        =   6
            Top             =   1320
            Width           =   525
            _ExtentX        =   926
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin AT_MaskText.MaskText txtReplanif 
            Height          =   315
            Left            =   9120
            TabIndex        =   142
            Top             =   2400
            Width           =   765
            _ExtentX        =   1349
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.ComboBox cmbSolicitante 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1320
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   1
            Top             =   600
            Width           =   7680
         End
         Begin AT_MaskText.MaskText txtHsEstimaSec 
            Height          =   315
            Left            =   9120
            TabIndex        =   147
            Top             =   2040
            Width           =   765
            _ExtentX        =   1349
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin AT_MaskText.MaskText txtCambioAlcance 
            Height          =   315
            Left            =   9480
            TabIndex        =   167
            TabStop         =   0   'False
            Top             =   2760
            Width           =   405
            _ExtentX        =   714
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Image imgSemaVerde 
            Height          =   240
            Left            =   9660
            Picture         =   "frmProyectosIDMEdit.frx":11CBC
            Top             =   240
            Width           =   240
         End
         Begin VB.Label lblProyIDM 
            AutoSize        =   -1  'True
            Caption         =   "Cbio. ALCA"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   32
            Left            =   8400
            TabIndex        =   166
            Top             =   2820
            Width           =   885
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Hs. estim. x Sec."
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   31
            Left            =   7680
            TabIndex        =   146
            Top             =   2100
            Width           =   1275
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Sub-estado"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   30
            Left            =   4560
            TabIndex        =   143
            Top             =   2820
            Width           =   870
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tot. replanif."
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   29
            Left            =   8400
            TabIndex        =   141
            Top             =   2377
            Width           =   600
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Hs. planif. x Ger."
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   28
            Left            =   7680
            TabIndex        =   140
            Top             =   1740
            Width           =   1245
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tipo *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   3
            Left            =   735
            TabIndex        =   137
            Top             =   2820
            Width           =   480
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Responsable *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   4
            Left            =   105
            TabIndex        =   100
            Top             =   1020
            Width           =   1110
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Clase *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   180
            Index           =   3
            Left            =   5520
            TabIndex        =   73
            Top             =   307
            Width           =   585
         End
         Begin VB.Label lblEtiquetas 
            AutoSize        =   -1  'True
            Caption         =   "Categor�a *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   180
            Index           =   2
            Left            =   330
            TabIndex        =   72
            Top             =   300
            Width           =   885
         End
         Begin VB.Label lblSemaforoManual 
            AutoSize        =   -1  'True
            Caption         =   "Manual"
            ForeColor       =   &H000000FF&
            Height          =   195
            Left            =   9360
            TabIndex        =   62
            Top             =   540
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Empresa *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   14
            Left            =   405
            TabIndex        =   60
            Top             =   1740
            Width           =   810
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Plan TyO *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   13
            Left            =   360
            TabIndex        =   59
            Top             =   1380
            Width           =   855
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fin reprogramado"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   9
            Left            =   4560
            TabIndex        =   58
            Top             =   2100
            Width           =   1305
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Fin estimado"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   8
            Left            =   4560
            TabIndex        =   57
            Top             =   1740
            Width           =   960
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Inicio estimado"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   7
            Left            =   4560
            TabIndex        =   56
            Top             =   1387
            Width           =   1170
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   6
            Left            =   4560
            TabIndex        =   55
            Top             =   2460
            Width           =   525
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Avance"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   5
            Left            =   2880
            TabIndex        =   54
            Top             =   1380
            Width           =   570
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Solicitante *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   2
            Left            =   255
            TabIndex        =   53
            ToolTipText     =   "Indica el �rea solicitante del proyecto"
            Top             =   660
            Width           =   960
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Area *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   15
            Left            =   705
            TabIndex        =   52
            Top             =   2100
            Width           =   510
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Clasificaci�n *"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   16
            Left            =   90
            TabIndex        =   51
            Top             =   2460
            Width           =   1125
         End
         Begin VB.Label lblProyIDM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   0
            Left            =   4200
            TabIndex        =   50
            Top             =   1380
            Width           =   165
         End
         Begin VB.Image imgSemaAmarillo 
            Height          =   240
            Left            =   9660
            Picture         =   "frmProyectosIDMEdit.frx":12246
            Top             =   240
            Width           =   240
         End
         Begin VB.Image imgSemaRojo 
            Height          =   240
            Left            =   9660
            Picture         =   "frmProyectosIDMEdit.frx":125D0
            Top             =   240
            Width           =   240
         End
      End
      Begin VB.Frame fraGeneral 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   3165
         Left            =   10200
         TabIndex        =   22
         Top             =   840
         Width           =   1335
         Begin VB.CommandButton cmdCerrar 
            Caption         =   "C&errar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   60
            TabIndex        =   24
            Top             =   2640
            Width           =   1215
         End
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Ca&ncelar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   60
            TabIndex        =   25
            Top             =   1560
            Width           =   1215
         End
         Begin VB.CommandButton cmdGuardar 
            Caption         =   "Gu&ardar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   60
            TabIndex        =   13
            Top             =   1200
            Width           =   1215
         End
         Begin VB.CommandButton cmdEditar 
            Caption         =   "Modificar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   60
            TabIndex        =   26
            Top             =   840
            Width           =   1215
         End
         Begin VB.CommandButton cmdAgregarNovedades 
            Caption         =   "Novedades"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   60
            TabIndex        =   23
            ToolTipText     =   "Editar las novedades"
            Top             =   480
            Width           =   1215
         End
         Begin VB.CommandButton cmdHistorial 
            Caption         =   "Historial"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   60
            TabIndex        =   136
            ToolTipText     =   "Editar las novedades"
            Top             =   120
            Width           =   1215
         End
      End
      Begin VB.Label lblProyIDM 
         AutoSize        =   -1  'True
         Caption         =   "* Campos obligatorios"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   150
         Index           =   25
         Left            =   120
         TabIndex        =   71
         Top             =   8560
         Width           =   1350
      End
   End
End
Attribute VB_Name = "frmProyectosIDMEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 06.02.2014 - Solo si el estado de proyecto es no es "Finalizado", se valida que entonces la fecha fin reprogramada sea menor o igual al today.
' -002- a. FJS 10.03.2015 - Agregado: se crean dos nuevos atributos (exposici�n y clasificaci�n) para poder clasificar los hitos cargados seg�n una tabla de referencia.
' -003- a. FJS 15.04.2015 - Agregado: cuando cambia la visibilidad de un hito que ten�a definida una clasificaci�n, se mantiene la misma.
' -004- a. FJS 21.10.2015 - Nuevo: el campo de exposici�n ahora es obligatorio.
' -004- b. FJS 21.10.2015 - Nuevo: al agregar un nuevo hito, se propone el siguiente nro. de orden utilizado + 1.
' -005- a. FJS 09.11.2015 - Nuevo: se agrega el scroll para las grillas.
' -006- a. FJS 11.01.2016 - Nuevo: se agrega nueva empresa y se cambia hardcodeo por tabla.

Option Explicit

Private Const TAB_GENERAL = 0
Private Const TAB_ALERTAS = 1
Private Const TAB_HITOS = 2
Private Const TAB_RESPONSABLES = 3
Private Const TAB_SECTORES = 4
Private Const TAB_ADJUNTOS = 5
Private Const TAB_HORAS = 6

Private Const STANDBY = 0
Private Const AGREGAR = 1
Private Const EDITAR = 2
Private Const ELIMINAR = 3
Private Const DESHABILITADO = 4

Private Const COLOR_SEMAF_ROJO = 1
Private Const COLOR_SEMAF_AMARILLO = 2
Private Const COLOR_SEMAF_VERDE = 3

Private Const SEMAF_BACK_VERDE = &HC000&
Private Const SEMAF_BACK_AMARILLO = &H80FFFF
Private Const SEMAF_BACK_ROJO = &HC0&
Private Const SEMAF_BORD_VERDE = &HC0FFC0
Private Const SEMAF_BORD_AMARILLO = &H80C0FF
Private Const SEMAF_BORD_ROJO = &HFF&

Private Const colALERTA_TIPO = 0
Private Const colALERTA_DESCRIPCION = 1
Private Const colALERTA_ACCIONCORRECTIVA = 2
Private Const colALERTA_INICIO = 3
Private Const colALERTA_FIN = 4
Private Const colALERTA_RESPONSABLES = 5
Private Const colALERTA_OBSERVACIONES = 6
Private Const colALERTA_ITEM = 7
Private Const colALERTA_STATUS = 8
Private Const colALERTA_STATUSDSC = 9
Private Const colALERTA_FECRESOLUCION = 10
Private Const colALERTA_USUARIO = 11
Private Const colALERTA_FECHA = 12

Private Const colHITO_HITORDEN = 0
Private Const colHITO_INICIO = 1
Private Const colHITO_FIN = 2
Private Const colHITO_HITO = 3
Private Const colHITO_HITONOMBRE = 4
Private Const colHITO_DESCRIPCION = 5
Private Const colHITO_ESTADO = 6
Private Const colHITO_ESTADONOMBRE = 7
Private Const colHITO_NIVEL = 8
Private Const colHITO_AREA = 9
Private Const colHITO_AREANOM = 10
Private Const colHITO_USUARIO = 11
Private Const colHITO_FECHA = 12
Private Const colHITO_DEPAREA = 13
Private Const colHITO_HSTNROINTERNO = 14
Private Const colHITO_HITNROINTERNO = 15
Private Const colHITO_ORDENREAL = 16
'{ add -002- a.
Private Const colHITO_EXPOSICION = 17
Private Const colHITO_CLASE = 18
Private Const colHITO_CLASENOMBRE = 19
Private Const colHITO_CLASEEXPOS = 20
'}

Private Const colRESP_AREARESPONSABLE = 0
Private Const colRESP_NOMRESPONSABLE = 1
Private Const colRESP_CODNIVEL = 2
Private Const colRESP_CODAREA = 3
Private Const colRESP_DEPAREA = 4

Private Const colGRUPOS_CODSECTOR = 0
Private Const colGRUPOS_NOMSECTOR = 1
Private Const colGRUPOS_CODGRUPO = 2
Private Const colGRUPOS_NOMGRUPO = 3
Private Const colGRUPOS_NOMESTADOGRUPO = 4
Private Const colGRUPOS_PETNROASIGNADO = 5
Private Const colGRUPOS_PETNROINTERNO = 6

Private Const colADJUNTOS_ProjId = 0
Private Const colADJUNTOS_ProjSubId = 1
Private Const colADJUNTOS_ProjSubSId = 2
Private Const colADJUNTOS_adj_tipo = 3
Private Const colADJUNTOS_adj_file = 4
Private Const colADJUNTOS_adj_texto = 5
Private Const colADJUNTOS_adj_objeto = 6
Private Const colADJUNTOS_audit_user = 7
Private Const colADJUNTOS_nom_user = 8
Private Const colADJUNTOS_audit_date = 9
Private Const colADJUNTOS_cod_nivel = 10
Private Const colADJUNTOS_cod_area = 11
Private Const colADJUNTOS_nom_area = 12

Private Const colHORAS_cod_nivel = 0
Private Const colHORAS_nom_nivel = 1
Private Const colHORAS_cod_area = 2
Private Const colHORAS_nom_area = 3
Private Const colHORAS_cant_horas = 4
Private Const colHORAS_TOTCOLS = 5

Public cModo As String
Public bActualizo As Boolean
Public l_ProjId As Long
Public l_ProjSubId As Long
Public l_ProjSubsId As Long
Public cNuevaNovedad As String

Dim bLoading As Boolean
Dim bSorting As Boolean                     ' Auxiliar para ordenamiento de grillas
'Dim bGuardando As Boolean
Dim cNombreProyecto As String
Dim cCodigoGPS As String
Dim cProyectoCategoria As String
Dim cProyectoClase As String
Dim cSemaforoOriginal As String             ' Auxiliar que guarda el valor del sem�foro al inicio de la edici�n
Dim bSemaforoManual As Boolean              ' Si se establece el semaforo manualmente, se activa el flag
Dim cEstadoOriginal As String               ' Auxiliar que guarda el valor del estado al inicio de la edici�n
Dim bEstadoManual As Boolean                ' Si se establece el estado manualmente, se activa el flag
Dim sOpcionSeleccionada As String           ' Auxiliar para saber en que estado trabajo (alta, modificaci�n, visualizaci�n, etc.)
Dim cAlertasResponsables As String          ' Auxiliar para guardar los responsables en el modo ALTA (Alertas)
Dim cAlertasObservaciones As String         ' Auxiliar para guardar las observaciones en el modo ALTA (Alertas)
Dim lUltimaColumnaOrdenada_Alertas As Long  ' Auxiliar para el ordenamiento de la grilla
Dim lUltimaColumnaOrdenada_Hitos As Long    ' Auxiliar para el ordenamiento de la grilla
Dim lUltimaColumnaOrdenada_Responsables As Long

Dim sModoGeneral As String
Dim cod_direccion_soli As String
Dim cod_gerencia_soli As String
Dim cod_sector_soli As String
Dim cProp_Direccion As String
Dim cProp_Gerencia As String
Dim cProp_Sector As String
Dim cProp_Grupo As String
Dim pNivel As String
Dim pArea As String

Dim bEditandoHito As Boolean                ' add -002- a.

Private Sub Form_Load()
    Call Puntero(True)
    bLoading = True
    Call Inicializar_Controles
    Call Inicializar_Grillas                    ' add -005- a.
    Call Cargar_Proyecto
    Call Cargar_GrillaAlertas
    Call Cargar_GrillaHitos
    Call Cargar_GrillaResponsables
    Call Cargar_GrillaGrupos
    Call Cargar_GrillaAdjuntos
    Call Cargar_GrillaHoras
    Call HabilitarBotonesAlertas(STANDBY, False)
    Call HabilitarBotonesHitos(STANDBY, False)
    'Call HabilitarBotonesResponsables(STANDBY, False)
    Call HabilitarBotonesAdjuntos(STANDBY, False)
    Call HabilitarBotonesHoras(STANDBY, False)
    Select Case cModo
        Case "AGREGAR"
            lblModo = "Agregar"
        Case "EDICION"
            lblModo = "Editar"
            If InStr(1, "ADMI|SADM|", glUsrPerfilActual, vbTextCompare) > 0 Then
                Call HabilitarBotonesResponsables(STANDBY, False)
            Else
                Call ObtenerNivelAreaCombo(cmbPropietario, pNivel, pArea)
                If (pNivel = "DIRE" And pArea = glLOGIN_Direccion) Or _
                   (pNivel = "GERE" And pArea = glLOGIN_Gerencia) Or _
                   (pNivel = "SECT" And pArea = glLOGIN_Sector) Or _
                   (pNivel = "GRUP" And pArea = glLOGIN_Grupo) Then
                        Call HabilitarBotonesResponsables(STANDBY, False)
                Else
                    Call HabilitarBotonesResponsables(DESHABILITADO, False)
                End If
            End If
        Case Else
            lblModo = "Visualizar"
            Call setHabilCtrl(cmdAgregarNovedades, "DIS")
            Call setHabilCtrl(cmdEditar, "DIS")
            Call HabilitarBotonesAlertas(DESHABILITADO, False)
            Call HabilitarBotonesHitos(DESHABILITADO, False)
            Call HabilitarBotonesResponsables(DESHABILITADO, False)
            Call HabilitarBotonesAdjuntos(DESHABILITADO, False)
            Call HabilitarBotonesHoras(DESHABILITADO, False)
    End Select
    Call Puntero(False)
    bLoading = False
    bActualizo = False
End Sub

'{ add -005- a.
Private Sub Inicializar_Grillas()
    Call IniciarScroll(grdHitos)
    Call IniciarScroll(grdAlertas)
    Call IniciarScroll(grdAreasResponsables)
    Call IniciarScroll(grdProyectoGrupos)
    Call IniciarScroll(grdAdjuntos)
    Call IniciarScroll(grdHoras)
End Sub
'}

Private Sub Inicializar_Controles()
    ' Guardo el nivel y �rea del perfil actuante
    'pNivel = getPerfNivel(glUsrPerfilActual)
    'pArea = getPerfArea(glUsrPerfilActual)
    Dim sDireccion As String
    Dim sGerencia As String
    
    txtCodigoGPS.ToolTipText = "C�digo GPS. Ej.: GB.00009782"
    lblAreaResponsableDatos = ""
    sOpcionSeleccionada = "M"
    sstProyectosIDM.Tab = 0     ' Siempre se encuentra el primer tab seleccionado para ver
    grdProyectoGrupos.Appearance = flexFlat
    grdProyectoGrupos.BorderStyle = flexBorderNone
    grdProyectoGrupos.BackColorBkg = sstProyectosIDM.BackColor
    lblModo.visible = True
    Call HabilitarBotones(0, False)
    'shpRecuadroSemaforo.BackColor = fraDatosCabecera.BackColor
    With cmbHito
        .Clear
        .AddItem "Fecha de entrega al usuario"
        .AddItem "Cambio de alcance"
        .ListIndex = -1
    End With
    With cmbTipoProyecto
        .Clear
        .AddItem "Iniciativa" & ESPACIOS & "||I"
        .AddItem "Proyecto" & ESPACIOS & "||P"
        .ListIndex = 0
    End With
    With cmbCategoria
        .Clear
        If sp_GetProyIDMCategoria(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!ProjCatId) & ": " & ClearNull(aplRST.Fields!ProjCatNom)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0      ' Por defecto, selecciono la primera categor�a
        End If
    End With
    With cmbClase
        .Clear
        'If sp_GetProyIDMClase(CodigoCombo(cmbCategoria, False), Null, Null) Then
        'If spProyectoIDM.sp_GetProyIDMClase(Null, Null, Null) Then
        If sp_GetProyIDMClase(Null, Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!ProjClaseId) & ": " & ClearNull(aplRST.Fields!ProjClaseNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!ProjCatId)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0      ' Por defecto, selecciono la primera clase
        End If
    End With
    With cmbSolicitante
        .Clear
        If sp_GetDireccion(Null, "S") Then
            Do While Not aplRST.EOF
                '.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||" & ClearNull(aplRST!cod_direccion)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                .AddItem sDireccion & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If sp_GetGerenciaXt(Null, Null, "S") Then
            Do While Not aplRST.EOF
                '.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                .AddItem sDireccion & " � " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If sp_GetSectorXt(Null, Null, Null, "S") Then
            Do While Not aplRST.EOF
                '.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                .AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    
    'cboHitosAreaResp.Clear
    cmbPropietario.Clear
    cboAreaResponsable.Clear
    If sp_GetDireccion(Null, "S") Then
        Do While Not aplRST.EOF
            'cboHitosAreaResp.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||" & ClearNull(aplRST!cod_direccion)
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            cmbPropietario.AddItem sDireccion & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion)
            'cboAreaResponsable.AddItem Trim(aplRST!nom_direccion) & Space(200) & "||" & ClearNull(aplRST!cod_direccion)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If sp_GetGerenciaXt(Null, Null, "S") Then
        Do While Not aplRST.EOF
            'cboHitosAreaResp.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia)
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cmbPropietario.AddItem sDireccion & " � " & sGerencia & ESPACIOS & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia)
            'cboAreaResponsable.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If sp_GetSectorXt(Null, Null, Null, "S") Then
        Do While Not aplRST.EOF
            'cboHitosAreaResp.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector)
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cmbPropietario.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector)
            'cboAreaResponsable.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If sp_GetGrupoXt(Null, Null, "S") Then
        Do While Not aplRST.EOF
            'cboHitosAreaResp.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector) & "/" & ClearNull(aplRST!cod_grupo)
            sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
            sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
            cmbPropietario.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector) & "/" & ClearNull(aplRST!cod_grupo)
            cboAreaResponsable.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector) & "/" & ClearNull(aplRST!cod_grupo)
            'cmbPropietario.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector) & "/" & ClearNull(aplRST!cod_grupo)
            'cboAreaResponsable.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(200) & "||" & ClearNull(aplRST!cod_direccion) & "/" & ClearNull(aplRST!cod_gerencia) & "/" & ClearNull(aplRST!cod_sector) & "/" & ClearNull(aplRST!cod_grupo)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    With cmbPlanTyO
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = -1
    End With
    '{ del -006- a.
    'With cmbEmpresa
    '    .Clear
    '    .AddItem "Banco" & ESPACIOS & "||B"
    '    .AddItem "BBVA Seguros" & ESPACIOS & "||C"
    '    .AddItem "RCF" & ESPACIOS & "||D"
    '    .AddItem "PSA" & ESPACIOS & "||E"
    '    .AddItem "RCF/PSA" & ESPACIOS & "||F"
    '    .ListIndex = -1
    'End With
    '}
    '{ add -006- a.
    With cmbEmpresa
        If sp_GetEmpresa(Null, Null) Then
            .Clear
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!empid) & ": " & ClearNull(aplRST.Fields!empabrev) & ESPACIOS & "||" & ClearNull(aplRST.Fields!empid)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
    '}
    With cmbArea
        .Clear
        .AddItem "Corporativo" & ESPACIOS & "||C"
        .AddItem "Regional" & ESPACIOS & "||R"
        .AddItem "Local" & ESPACIOS & "||L"
        .ListIndex = -1
    End With
    With cmbClas3
        .Clear
        '.AddItem "Customer Centric" & ESPACIOS & "||C"
        '.AddItem "Modelo de Distribuci�n" & ESPACIOS & "||M"
        '.AddItem "Lean" & ESPACIOS & "||L"
        .AddItem "2013" & ESPACIOS & "||1"
        .AddItem "2014" & ESPACIOS & "||2"
        .AddItem "2015" & ESPACIOS & "||3"
        .AddItem "2013 a 2014" & ESPACIOS & "||4"
        .AddItem "2013 a 2015" & ESPACIOS & "||5"
        .AddItem "2014 a 2015" & ESPACIOS & "||6"
        .ListIndex = -1
    End With
    With cmbEstado
        .Clear
        If sp_GetEstadosIDM(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_estado) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_estado)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1      ' Por defecto el valor es "A planificar"
        End If
    End With
'    With cmbEstado
'        .Clear
'        '.AddItem "A planificar" & ESPACIOS & "||PLANIF"
'        '.AddItem "Desestimado" & ESPACIOS & "||DESEST"
'        '.AddItem "En ejecuci�n" & ESPACIOS & "||EJECUC"
'        '.AddItem "En prueba" & ESPACIOS & "||PRUEBA"
'        '.AddItem "Suspendido" & ESPACIOS & "||SUSPEN"
'        '.AddItem "Finalizado" & ESPACIOS & "||TERMIN"
'        .AddItem "En evaluaci�n" & ESPACIOS & "||EVALUA"
'        .AddItem "Desestimado" & ESPACIOS & "||DESEST"
'        .AddItem "Aprobado" & ESPACIOS & "||APROBA"
'        .AddItem "En an�lisis" & ESPACIOS & "||ANALIS"
'        .AddItem "A planificar" & ESPACIOS & "||PLANIF"
'        .AddItem "Planificado" & ESPACIOS & "||PLANOK"
'        .AddItem "En ejecuci�n" & ESPACIOS & "||EJECUC"
'        .AddItem "Cancelado" & ESPACIOS & "||CANCEL"
'        .AddItem "Finalizado" & ESPACIOS & "||TERMIN"
'        .ListIndex = -1      ' Por defecto el valor es "A planificar"
'    End With
'    With cmbSubestado
'        .Clear
'        .AddItem "En an�lisis" & ESPACIOS & "||ANALIS"
'        .AddItem "Prueba TyO" & ESPACIOS & "||PRUTYO"
'        .AddItem "Prueba de usuario" & ESPACIOS & "||PRUUSR"
'        .AddItem "Implantaci�n" & ESPACIOS & "||IMPLAN"
'        .AddItem "Seguimiento post implantaci�n" & ESPACIOS & "||SEGPOS"
'        .ListIndex = -1
'    End With
    With cmbAlertaTipo
        .Clear
        .AddItem "1. Alerta roja" & ESPACIOS & "||R"
        .AddItem "2. Alerta amarilla" & ESPACIOS & "||A"
        .ListIndex = -1
    End With
    With cmbHitoEstado
        .Clear
        .AddItem "Pendiente" & ESPACIOS & "||PENDIE"
        .AddItem "En curso" & ESPACIOS & "||EJECUC"
        .AddItem "Finalizado" & ESPACIOS & "||TERMIN"
    End With
    '{ add -002- a.
    With cmbHitoExposicion
        .Clear
        '.AddItem "-" & ESPACIOS & "||NULL"
        .AddItem "No" & ESPACIOS & "||N"
        .AddItem "Si" & ESPACIOS & "||S"
        .ListIndex = -1
    End With
    
    With cmbHitoClasificacion
        .Clear
        '.AddItem "-" & ESPACIOS & "||NULL"
        If sp_GetHitoClase(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!hitoid) & ": " & ClearNull(aplRST.Fields!hitodesc) & ESPACIOS & "||" & ClearNull(aplRST.Fields!hitoexpos)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = -1
    End With
    '}
    
    Call CargarAreasHoras
    chkRecalcularSemaforo.Value = 0
End Sub

Private Sub CargarAreasHoras()
    With cmbHorasArea
        .Clear
        If sp_GetSectorXt(Null, Null, "MEDIO", "S") Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!es_ejecutor) = "S" Then
                    .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_gerencia) & " � " & ClearNull(aplRST.Fields!nom_sector) & ESPACIOS & "||SEC"
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = 0
    End With
End Sub

Private Sub Cargar_TodosLosEstados()
    With cmbEstado
        .Clear
        If sp_GetEstadosIDM(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_estado) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_estado)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    With cmbSubestado
        .Clear
        If sp_GetEstadosIDM(Null, "S") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_estado) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_estado)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = -1
    End With
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case STANDBY
            sModoGeneral = ""
            'Call Cargar_TodosLosEstados
            Call setHabilCtrl(cmbTipoProyecto, "DIS")
            Call setHabilCtrl(txtHsPlanifGer, "DIS")
            Call setHabilCtrl(txtHsEstimaSec, "DIS")
            Call setHabilCtrl(txtReplanif, "DIS")
            Call setHabilCtrl(cmbCategoria, "DIS")
            Call setHabilCtrl(cmbClase, "DIS")
            Call setHabilCtrl(cmbSolicitante, "DIS")
            Call setHabilCtrl(cmbPropietario, "DIS")
            Call setHabilCtrl(cmbPlanTyO, "DIS")
            Call setHabilCtrl(cmbEmpresa, "DIS")
            Call setHabilCtrl(cmbArea, "DIS")
            Call setHabilCtrl(cmbClas3, "DIS")
            Call setHabilCtrl(txtAvance, "DIS")
            Call setHabilCtrl(cmbEstado, "DIS")
            Call setHabilCtrl(cmbSubestado, "DIS")
            Call setHabilCtrl(txtInicio, "DIS")
            Call setHabilCtrl(txtFinPrevisto, "DIS")
            Call setHabilCtrl(txtFinReprogramado, "DIS")
            Call setHabilCtrl(txtDescripcion, "DIS")
            Call setHabilCtrl(txtBeneficios, "DIS")
            Call setHabilCtrl(txtNovedades, "DIS")
            Call setHabilCtrl(chkRecalcularSemaforo, "DIS")
            Call setHabilCtrl(txtCambioAlcance, "DIS")
            chkRecalcularSemaforo.Value = 0
            'vscSemaforo.Enabled = False
            udSemaforo.Enabled = False          ' new
            'shpRecuadroSemaforo.BackColor = fraDatosCabecera.BackColor
            Call setVisibleCtrl(txtNombreProyecto, "DIS")
            Call setVisibleCtrl(lblNombreProyecto, "NOR")
            Call setVisibleCtrl(txtCodigoGPS, "DIS")
            Call setVisibleCtrl(lblCodigoGPS, "NOR")
            ' Botonera
            Call setHabilCtrl(cmdHistorial, "NOR")
            Call setHabilCtrl(cmdAgregarNovedades, "NOR")
            Call setHabilCtrl(cmdEditar, "NOR")
            Call setHabilCtrl(cmdCerrar, "NOR")
            Call setHabilCtrl(cmdAgregarNovedades, IIf(cModo = "AGREGAR", "DIS", "NOR"))
            Call setHabilCtrl(cmdGuardar, "DIS")
            Call setHabilCtrl(cmdCancelar, "DIS")
            With sstProyectosIDM
                .TabEnabled(0) = True
                .TabEnabled(1) = True
                .TabEnabled(2) = True
                .TabEnabled(3) = True
                .TabEnabled(4) = True
                .TabEnabled(5) = True
                .TabEnabled(6) = True
            End With
        Case AGREGAR      ' Editar
            sModoGeneral = "Edicion"
            Call setVisibleCtrl(lblNombreProyecto, "DIS")
            Call setVisibleCtrl(txtNombreProyecto, "NOR")
            Call setVisibleCtrl(lblCodigoGPS, "DIS")
            Call setVisibleCtrl(txtCodigoGPS, "NOR")
            txtNombreProyecto.Text = lblNombreProyecto.Caption
            txtCodigoGPS.Text = IIf(cModo = "AGREGAR", "GB.", lblCodigoGPS.Caption)
            Call setHabilCtrl(cmbCategoria, "NOR")
            Call setHabilCtrl(cmbClase, "NOR")
            Select Case glUsrPerfilActual
                Case "ADMI", "SADM"
                    Call setHabilCtrl(cmbTipoProyecto, "NOR")
                    Call setHabilCtrl(txtHsEstimaSec, "NOR")
                    Call setHabilCtrl(txtHsPlanifGer, "NOR")
                    Call setHabilCtrl(cmbPropietario, "NOR")
                    Call setHabilCtrl(cmdAgregarNovedades, "NOR")
                    Call setHabilCtrl(cmbPlanTyO, "NOR")
                    'vscSemaforo.Enabled = True
                    udSemaforo.Enabled = True           ' new
                Case Else
                    Call setHabilCtrl(cmbPlanTyO, "DIS")
                    Call setHabilCtrl(cmbPropietario, "DIS")
            End Select
            Call setHabilCtrl(cmbSolicitante, "NOR")
            Call setHabilCtrl(cmbEmpresa, "NOR")
            Call setHabilCtrl(cmbArea, "NOR")
            Call setHabilCtrl(cmbClas3, "NOR")
            Call setHabilCtrl(txtDescripcion, "NOR")
            Call setHabilCtrl(txtBeneficios, "NOR")
            'If sp_GetAccionesPerfilIDM(IIf(CodigoCombo(cmbTipoProyecto, True) = "I", "INCHGEST", "PJCHGEST"), glUsrPerfilActual, CodigoCombo2(cmbEstado.Tag, True), Null) Then
            If InStr(1, "ADMI|SADM|BPAR|", glUsrPerfilActual, vbTextCompare) > 0 Then
                If sp_GetAccionesPerfilIDM(IIf(CodigoCombo(cmbTipoProyecto, True) = "I", "INCHGEST", "PJCHGEST"), glUsrPerfilActual, CodigoCombo2(cmbEstado.Tag, True), Null) Then
                    bLoading = True
                    With cmbEstado
                        .Clear
                        .AddItem cmbEstado.Tag
                        Do While Not aplRST.EOF
                            .AddItem ClearNull(aplRST.Fields!nom_estadonew) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_estnew)
                            aplRST.MoveNext
                            DoEvents
                        Loop
                        .ListIndex = 0
                    End With
                    Call setHabilCtrl(cmbEstado, "NOR")
                    bLoading = False
                End If
            End If
            If CodigoCombo(cmbEstado, True) = "EJECUC" Then
                Call setHabilCtrl(cmbSubestado, "NOR")
            End If
            Call setHabilCtrl(txtAvance, "NOR")
            Call setHabilCtrl(txtInicio, "NOR")
            Call setHabilCtrl(txtFinReprogramado, "NOR")
            If cmbEstado.ListIndex = -1 Then
                Call SetCombo(cmbEstado, "PLANIF", True)        ' Si no tiene estado, se prefija "A planificar"
            End If
            If bSemaforoManual Then
                'shpRecuadroSemaforo.BackColor = &HFFFFFF
                Call setHabilCtrl(chkRecalcularSemaforo, "NOR")
            Else
                'shpRecuadroSemaforo.BackColor = fraDatosCabecera.BackColor
                Call setHabilCtrl(chkRecalcularSemaforo, "DIS")
            End If
            Call setHabilCtrl(cmdHistorial, "DIS")
            Call setHabilCtrl(cmdAgregarNovedades, "DIS")
            Call setHabilCtrl(cmdEditar, "DIS")
            Call setHabilCtrl(cmdCerrar, "DIS")
            Call setHabilCtrl(cmdGuardar, "NOR")
            Call setHabilCtrl(cmdCancelar, "NOR")
            ' Valores por defecto
            If txtAvance.Text = "" Then txtAvance.Text = "0"
            If InPerfil("SADM") Then
                Call setHabilCtrl(txtFinPrevisto, "NOR")
            Else
                If IsDate(txtFinPrevisto) Then
                    Call setHabilCtrl(txtFinPrevisto, "DIS")
                Else
                    Call setHabilCtrl(txtFinPrevisto, "NOR")
                End If
            End If
            With sstProyectosIDM
                .Tab = 0
                .TabEnabled(0) = True
                .TabEnabled(1) = False
                .TabEnabled(2) = False
                .TabEnabled(3) = False
                .TabEnabled(4) = False
                .TabEnabled(5) = False
                .TabEnabled(6) = False
            End With
        Case 3      ' Inhabilita toda la botonera principal
            sModoGeneral = ""
            Call setHabilCtrl(cmdHistorial, "DIS")
            Call setHabilCtrl(cmdAgregarNovedades, "DIS")
            Call setHabilCtrl(cmdEditar, "DIS")
            Call setHabilCtrl(cmdCerrar, "DIS")
            Call setHabilCtrl(cmdAgregarNovedades, "DIS")
            Call setHabilCtrl(cmdGuardar, "DIS")
            Call setHabilCtrl(cmdCancelar, "DIS")
    End Select
    Call LockProceso(False)
End Sub

Private Sub HabilitarBotonesAlertas(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case STANDBY
            lblAlertasModoTrabajo = ""
            grdAlertas.Enabled = True
            Call setHabilCtrl(cmdAlertas_Agregar, "NOR")
            Call setHabilCtrl(cmdAlertas_Modificar, "NOR")
            Call setHabilCtrl(cmdAlertas_Eliminar, "NOR")
            Call setHabilCtrl(cmdAlertas_Confirmar, "DIS")
            Call setHabilCtrl(cmdAlertas_Cancelar, "DIS")

            Call setHabilCtrl(cmbAlertaTipo, "DIS"): cmbAlertaTipo.ListIndex = -1
            Call setHabilCtrl(txtAlertaDescripcion, "DIS"): txtAlertaDescripcion = ""
            Call setHabilCtrl(txtAccionCorrectiva, "DIS"): txtAccionCorrectiva = ""
            Call setHabilCtrl(txtAlertaFechaIni, "DIS"): txtAlertaFechaIni = ""
            Call setHabilCtrl(txtAlertaFechaFin, "DIS"): txtAlertaFechaFin = ""
            Call setHabilCtrl(txtAlertasObservaciones, "DIS"): txtAlertasObservaciones = ""
            Call setHabilCtrl(chkAlertaResuelta, "DIS"): chkAlertaResuelta.Value = 0
            Call setHabilCtrl(txtFechaResolucion, "DIS"): txtFechaResolucion = ""
            Call setHabilCtrl(fraAlertasTextos, "NOR")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(0)
            Call MostrarSeleccionAlertas
        Case AGREGAR
            lblAlertasModoTrabajo = "Agregar"
            grdAlertas.Enabled = False
            Call setHabilCtrl(cmdAlertas_Agregar, "DIS")
            Call setHabilCtrl(cmdAlertas_Modificar, "DIS")
            Call setHabilCtrl(cmdAlertas_Eliminar, "DIS")
            Call setHabilCtrl(cmdAlertas_Confirmar, "NOR")
            Call setHabilCtrl(cmdAlertas_Cancelar, "NOR")

            Call setHabilCtrl(cmbAlertaTipo, "NOR"): cmbAlertaTipo.ListIndex = -1
            Call setHabilCtrl(txtAlertaDescripcion, "NOR"): txtAlertaDescripcion = ""
            Call setHabilCtrl(txtAccionCorrectiva, "NOR"): txtAccionCorrectiva = ""
            Call setHabilCtrl(txtAlertaFechaIni, "NOR"): txtAlertaFechaIni = ""
            Call setHabilCtrl(txtAlertaFechaFin, "NOR"): txtAlertaFechaFin = ""
            Call setHabilCtrl(txtAlertasObservaciones, "NOR"): txtAlertasObservaciones = ""
            Call setHabilCtrl(chkAlertaResuelta, "NOR"): chkAlertaResuelta.Value = 0
            Call setHabilCtrl(txtFechaResolucion, "NOR"): txtFechaResolucion = ""
            Call setHabilCtrl(fraAlertasTextos, "NOR")
            cAlertasResponsables = ""
            cAlertasObservaciones = ""
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
            cmbAlertaTipo.SetFocus
        Case EDITAR
            lblAlertasModoTrabajo = "Modificar"
            grdAlertas.Enabled = False
            Call setHabilCtrl(cmdAlertas_Agregar, "DIS")
            Call setHabilCtrl(cmdAlertas_Modificar, "DIS")
            Call setHabilCtrl(cmdAlertas_Eliminar, "DIS")
            Call setHabilCtrl(cmdAlertas_Confirmar, "NOR")
            Call setHabilCtrl(cmdAlertas_Cancelar, "NOR")

            Call setHabilCtrl(cmbAlertaTipo, "NOR")
            Call setHabilCtrl(txtAlertaDescripcion, "NOR")
            Call setHabilCtrl(txtAccionCorrectiva, "NOR")
            Call setHabilCtrl(txtAlertaFechaIni, "NOR")
            Call setHabilCtrl(txtAlertaFechaFin, "NOR")
            Call setHabilCtrl(txtAlertasObservaciones, "NOR")
            Call setHabilCtrl(chkAlertaResuelta, "NOR")
            Call setHabilCtrl(txtFechaResolucion, "NOR")
            Call setHabilCtrl(fraAlertasTextos, "NOR")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
        Case ELIMINAR
            lblAlertasModoTrabajo = "Eliminar"
            grdAlertas.Enabled = False
            Call setHabilCtrl(cmdAlertas_Agregar, "DIS")
            Call setHabilCtrl(cmdAlertas_Modificar, "DIS")
            Call setHabilCtrl(cmdAlertas_Eliminar, "DIS")
            Call setHabilCtrl(cmdAlertas_Confirmar, "NOR")
            Call setHabilCtrl(cmdAlertas_Cancelar, "NOR")

            Call setHabilCtrl(cmbAlertaTipo, "DIS")
            Call setHabilCtrl(txtAlertaDescripcion, "DIS")
            Call setHabilCtrl(txtAccionCorrectiva, "DIS")
            Call setHabilCtrl(txtAlertaFechaIni, "DIS")
            Call setHabilCtrl(txtAlertaFechaFin, "DIS")
            Call setHabilCtrl(txtAlertasObservaciones, "DIS")
            Call setHabilCtrl(chkAlertaResuelta, "DIS")
            Call setHabilCtrl(txtFechaResolucion, "DIS")
            Call setHabilCtrl(fraAlertasTextos, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
        Case DESHABILITADO
            lblAlertasModoTrabajo = ""
            grdAlertas.Enabled = True
            Call setHabilCtrl(cmdAlertas_Agregar, "DIS")
            Call setHabilCtrl(cmdAlertas_Modificar, "DIS")
            Call setHabilCtrl(cmdAlertas_Eliminar, "DIS")
            Call setHabilCtrl(cmdAlertas_Confirmar, "DIS")
            Call setHabilCtrl(cmdAlertas_Cancelar, "DIS")

            Call setHabilCtrl(cmbAlertaTipo, "DIS")
            Call setHabilCtrl(txtAlertaDescripcion, "DIS")
            Call setHabilCtrl(txtAccionCorrectiva, "DIS")
            Call setHabilCtrl(txtAlertaFechaIni, "DIS")
            Call setHabilCtrl(txtAlertaFechaFin, "DIS")
            Call setHabilCtrl(txtAlertasObservaciones, "DIS")
            Call setHabilCtrl(chkAlertaResuelta, "DIS")
            Call setHabilCtrl(txtFechaResolucion, "DIS")
            Call setHabilCtrl(fraAlertasTextos, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(0)
    End Select
End Sub

Private Sub HabilitarBotonesHitos(nOpcion As Integer, Optional vCargaGrid As Variant)
    Dim NroOrden As Long
    
    Select Case nOpcion
        Case STANDBY
            lblHitosModoTrabajo = ""
            grdHitos.Enabled = True
            Call setHabilCtrl(cmdHitos_Agregar, "NOR")
            Call setHabilCtrl(cmdHitos_Modificar, "NOR")
            Call setHabilCtrl(cmdHitos_Eliminar, "NOR")
            Call setHabilCtrl(cmdHitos_Confirmar, "DIS")
            Call setHabilCtrl(cmdHitos_Cancelar, "DIS")
            
            Call setHabilCtrl(txtHitoInicio, "DIS"): txtHitoInicio.Text = ""
            Call setHabilCtrl(txtHitoFinalizacion, "DIS"): txtHitoFinalizacion.Text = ""
            Call setHabilCtrl(cmbHito, "DIS")
            Call setHabilCtrl(txtHitoNombre, "DIS"): txtHitoNombre = ""
            Call setHabilCtrl(cmbHitoEstado, "DIS"): cmbHitoEstado.ListIndex = -1
            Call setHabilCtrl(cboHitosAreaResp, "DIS"): cboHitosAreaResp.ListIndex = -1
            Call setHabilCtrl(txtHitoDescripcion, "DIS"): txtHitoDescripcion.Text = ""
            Call setHabilCtrl(txtHitoOrden, "DIS"): txtHitoOrden = ""
            '{ add -002- a.
            Call setHabilCtrl(cmbHitoExposicion, "DIS"): cmbHitoExposicion.ListIndex = -1
            Call setHabilCtrl(cmbHitoClasificacion, "DIS"): cmbHitoClasificacion.ListIndex = -1
            '}
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(0)
            Call MostrarSeleccionHitos
        Case AGREGAR
            lblHitosModoTrabajo = "Agregar"
            grdHitos.Enabled = False
            Call setHabilCtrl(cmdHitos_Agregar, "DIS")
            Call setHabilCtrl(cmdHitos_Modificar, "DIS")
            Call setHabilCtrl(cmdHitos_Eliminar, "DIS")
            Call setHabilCtrl(cmdHitos_Confirmar, "NOR")
            Call setHabilCtrl(cmdHitos_Cancelar, "NOR")
            
            Call setHabilCtrl(txtHitoInicio, "NOR"): txtHitoInicio.Text = ""
            Call setHabilCtrl(txtHitoFinalizacion, "NOR"): txtHitoFinalizacion.Text = ""
            Call setHabilCtrl(cmbHito, "NOR"): cmbHito = "": cmbHito.ListIndex = -1
            Call setHabilCtrl(txtHitoNombre, "NOR"): txtHitoNombre = ""
            Call setHabilCtrl(cboHitosAreaResp, "DIS"): cboHitosAreaResp.ListIndex = -1
            Call setHabilCtrl(cmbHitoEstado, "NOR"): cmbHitoEstado.ListIndex = 0    ' Por defecto: Pendiente -1
            Call setHabilCtrl(txtHitoDescripcion, "NOR"): txtHitoDescripcion.Text = ""
            'Call setHabilCtrl(txtHitoOrden, "NOR"): txtHitoOrden = ""              ' del -004- b.
            '{ add -004- b.
            Call setHabilCtrl(txtHitoOrden, "NOR")
            NroOrden = 0
            If sp_GetProyectoIDMHitosOrden(l_ProjId, l_ProjSubId, l_ProjSubsId) Then
                If IsNull(aplRST.Fields!orden) Then
                    NroOrden = 1
                Else
                    NroOrden = aplRST.Fields!orden
                End If
                'NroOrden = IIf(IsNull(aplRST.Fields!orden), 1, CLng(ClearNull(aplRST.Fields!orden)))
            End If
            txtHitoOrden = IIf(NroOrden > 0, NroOrden, "")
            '}
            '{ add -002- a.
            Call setHabilCtrl(cmbHitoExposicion, "NOR"): cmbHitoExposicion.ListIndex = -1
            Call setHabilCtrl(cmbHitoClasificacion, "NOR"): cmbHitoClasificacion.ListIndex = -1
            Call setHabilCtrl(cmbHitoClasificacion, IIf(CodigoCombo(cmbHitoExposicion, True) = "S", "NOR", "DIS"))
            '}
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
            txtHitoInicio.SetFocus
        Case EDITAR
            lblHitosModoTrabajo = "Modificar"
            grdHitos.Enabled = False
            Call setHabilCtrl(cmdHitos_Agregar, "DIS")
            Call setHabilCtrl(cmdHitos_Modificar, "DIS")
            Call setHabilCtrl(cmdHitos_Eliminar, "DIS")
            Call setHabilCtrl(cmdHitos_Confirmar, "NOR")
            Call setHabilCtrl(cmdHitos_Cancelar, "NOR")
            
            Call setHabilCtrl(txtHitoInicio, "NOR")
            'Call setHabilCtrl(txtHitoNombre, "NOR")            ' del -004- a.
            Call setHabilCtrl(cmbHito, "NOR")                   ' add -004- a.
            Call setHabilCtrl(txtHitoFinalizacion, "NOR")
            Call setHabilCtrl(cmbHitoEstado, "NOR")
            Call setHabilCtrl(cboHitosAreaResp, "DIS")
            Call setHabilCtrl(txtHitoDescripcion, "NOR")
            Call setHabilCtrl(txtHitoOrden, "NOR")
            '{ add -002- a.
            Call setHabilCtrl(cmbHitoExposicion, "NOR")
            Call setHabilCtrl(cmbHitoClasificacion, IIf(CodigoCombo(cmbHitoExposicion, True) = "S", "NOR", "DIS"))
            'Call setHabilCtrl(cmbHitoClasificacion, "NOR")
            '}
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call MostrarSeleccionHitos
            Call HabilitarBotones(3)
        Case ELIMINAR
            lblHitosModoTrabajo = "Eliminar"
            grdHitos.Enabled = False
            Call setHabilCtrl(cmdHitos_Agregar, "DIS")
            Call setHabilCtrl(cmdHitos_Modificar, "DIS")
            Call setHabilCtrl(cmdHitos_Eliminar, "DIS")
            Call setHabilCtrl(cmdHitos_Confirmar, "NOR")
            Call setHabilCtrl(cmdHitos_Cancelar, "NOR")
            
            Call setHabilCtrl(txtHitoInicio, "DIS")
            Call setHabilCtrl(cmbHito, "DIS")
            Call setHabilCtrl(txtHitoNombre, "DIS")
            Call setHabilCtrl(txtHitoFinalizacion, "DIS")
            Call setHabilCtrl(cmbHitoEstado, "DIS")
            Call setHabilCtrl(cboHitosAreaResp, "DIS")
            Call setHabilCtrl(txtHitoDescripcion, "DIS")
            Call setHabilCtrl(txtHitoOrden, "DIS")
            '{ add -002- a.
            Call setHabilCtrl(cmbHitoExposicion, "DIS")
            Call setHabilCtrl(cmbHitoClasificacion, "DIS")
            '}
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
        Case DESHABILITADO
            lblHitosModoTrabajo = ""
            grdHitos.Enabled = True
            Call setHabilCtrl(cmdHitos_Agregar, "DIS")
            Call setHabilCtrl(cmdHitos_Modificar, "DIS")
            Call setHabilCtrl(cmdHitos_Eliminar, "DIS")
            Call setHabilCtrl(cmdHitos_Confirmar, "DIS")
            Call setHabilCtrl(cmdHitos_Cancelar, "DIS")
            
            Call setHabilCtrl(txtHitoInicio, "DIS")
            Call setHabilCtrl(txtHitoNombre, "DIS")
            Call setHabilCtrl(cmbHito, "DIS")
            Call setHabilCtrl(cboHitosAreaResp, "DIS")
            Call setHabilCtrl(txtHitoFinalizacion, "DIS")
            Call setHabilCtrl(cmbHitoEstado, "DIS")
            Call setHabilCtrl(txtHitoDescripcion, "DIS")
            Call setHabilCtrl(txtHitoOrden, "DIS")
            '{ add -002- a.
            Call setHabilCtrl(cmbHitoExposicion, "DIS")
            Call setHabilCtrl(cmbHitoClasificacion, "DIS")
            '}
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(0)
    End Select
End Sub

Private Sub HabilitarBotonesHoras(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case STANDBY
            lblHorasModoTrabajo = ""
            grdHoras.Enabled = True
            Call setHabilCtrl(cmdHoras_Agregar, "NOR")
            Call setHabilCtrl(cmdHoras_Modificar, "NOR")
            Call setHabilCtrl(cmdHoras_Eliminar, "NOR")
            Call setHabilCtrl(cmdHoras_Confirmar, "DIS")
            Call setHabilCtrl(cmdHoras_Cancelar, "DIS")
            Call setHabilCtrl(optHorasDeclaradas(0), "DIS")
            Call setHabilCtrl(cmbHorasArea, "DIS"): cmbHorasArea.ListIndex = -1
            Call setHabilCtrl(txtAreaHoras, "DIS"): txtAreaHoras = ""
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(0)
            Call MostrarSeleccionHoras
        Case AGREGAR
            lblHorasModoTrabajo = "Agregar"
            grdHoras.Enabled = False
            Call setHabilCtrl(cmdHoras_Agregar, "DIS")
            Call setHabilCtrl(cmdHoras_Modificar, "DIS")
            Call setHabilCtrl(cmdHoras_Eliminar, "DIS")
            Call setHabilCtrl(cmdHoras_Confirmar, "NOR")
            Call setHabilCtrl(cmdHoras_Cancelar, "NOR")
            Call setHabilCtrl(optHorasDeclaradas(0), "NOR"): optHorasDeclaradas(0).Value = True
            Call setHabilCtrl(cmbHorasArea, "NOR"): cmbHorasArea.ListIndex = -1
            Call setHabilCtrl(txtAreaHoras, "NOR"): txtAreaHoras = ""
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call CargarAreasHoras
            Call HabilitarBotones(3)
        Case EDITAR
            lblHorasModoTrabajo = "Modificar"
            grdHoras.Enabled = False
            Call setHabilCtrl(cmdHoras_Agregar, "DIS")
            Call setHabilCtrl(cmdHoras_Modificar, "DIS")
            Call setHabilCtrl(cmdHoras_Eliminar, "DIS")
            Call setHabilCtrl(cmdHoras_Confirmar, "NOR")
            Call setHabilCtrl(cmdHoras_Cancelar, "NOR")
            Call setHabilCtrl(optHorasDeclaradas(0), "NOR")
            Call setHabilCtrl(cmbHorasArea, "NOR")
            Call setHabilCtrl(txtAreaHoras, "NOR")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call CargarAreasHoras
            Call MostrarSeleccionHoras
            Call HabilitarBotones(3)
        Case ELIMINAR
            lblHorasModoTrabajo = "Eliminar"
            grdHoras.Enabled = False
            Call setHabilCtrl(cmdHoras_Agregar, "DIS")
            Call setHabilCtrl(cmdHoras_Modificar, "DIS")
            Call setHabilCtrl(cmdHoras_Eliminar, "DIS")
            Call setHabilCtrl(cmdHoras_Confirmar, "NOR")
            Call setHabilCtrl(cmdHoras_Cancelar, "NOR")
            Call setHabilCtrl(optHorasDeclaradas(0), "DIS")
            Call setHabilCtrl(cmbHorasArea, "DIS")
            Call setHabilCtrl(txtAreaHoras, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
        Case DESHABILITADO
            lblHorasModoTrabajo = ""
            grdHoras.Enabled = True
            Call setHabilCtrl(cmdHoras_Agregar, "DIS")
            Call setHabilCtrl(cmdHoras_Modificar, "DIS")
            Call setHabilCtrl(cmdHoras_Eliminar, "DIS")
            Call setHabilCtrl(cmdHoras_Confirmar, "DIS")
            Call setHabilCtrl(cmdHoras_Cancelar, "DIS")
            
            Call setHabilCtrl(optHorasDeclaradas(0), "DIS")
            'Call setHabilCtrl(optHorasDeclaradas(1), "DIS")
            Call setHabilCtrl(cmbHorasArea, "DIS")
            Call setHabilCtrl(txtAreaHoras, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(0)
    End Select
End Sub

Private Sub HabilitarBotonesResponsables(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case STANDBY
            lblResponsablesModoTrabajo = ""
            grdAreasResponsables.Enabled = True
            Call setHabilCtrl(cmdResponsables_Agregar, "NOR")
            Call setHabilCtrl(cmdResponsables_Modificar, "DIS")
            Call setHabilCtrl(cmdResponsables_Eliminar, "NOR")
            Call setHabilCtrl(cmdResponsables_Confirmar, "DIS")
            Call setHabilCtrl(cmdResponsables_Cancelar, "DIS")
            
            Call setHabilCtrl(cboAreaResponsable, "DIS"): cboAreaResponsable.ListIndex = -1
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(0)
            Call MostrarSeleccionResponsables
        Case AGREGAR
            lblResponsablesModoTrabajo = "Agregar"
            grdAreasResponsables.Enabled = False
            Call setHabilCtrl(cmdResponsables_Agregar, "DIS")
            Call setHabilCtrl(cmdResponsables_Modificar, "DIS")
            Call setHabilCtrl(cmdResponsables_Eliminar, "DIS")
            Call setHabilCtrl(cmdResponsables_Confirmar, "NOR")
            Call setHabilCtrl(cmdResponsables_Cancelar, "NOR")
            
            Call setHabilCtrl(cboAreaResponsable, "NOR"): cboAreaResponsable.ListIndex = -1
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
            cboAreaResponsable.SetFocus
        Case EDITAR     ' No hace falta (eliminar)
        Case ELIMINAR
            lblResponsablesModoTrabajo = "Eliminar"
            grdAreasResponsables.Enabled = False
            Call setHabilCtrl(cmdResponsables_Agregar, "DIS")
            Call setHabilCtrl(cmdResponsables_Modificar, "DIS")
            Call setHabilCtrl(cmdResponsables_Eliminar, "DIS")
            Call setHabilCtrl(cmdResponsables_Confirmar, "NOR")
            Call setHabilCtrl(cmdResponsables_Cancelar, "NOR")
            
            Call setHabilCtrl(cboAreaResponsable, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call HabilitarBotones(3)
        Case DESHABILITADO
            lblResponsablesModoTrabajo = ""
            grdAreasResponsables.Enabled = True
            Call setHabilCtrl(cmdResponsables_Agregar, "DIS")
            Call setHabilCtrl(cmdResponsables_Modificar, "DIS")
            Call setHabilCtrl(cmdResponsables_Eliminar, "DIS")
            Call setHabilCtrl(cmdResponsables_Confirmar, "DIS")
            Call setHabilCtrl(cmdResponsables_Cancelar, "DIS")
            Call setHabilCtrl(cboAreaResponsable, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(0)
    End Select
End Sub

Private Sub HabilitarPestanias(p As Integer)
    Select Case p
        Case TAB_ALERTAS
            Call setHabilCtrl(fraHitos, "DIS")
            Call setHabilCtrl(fraResponsables, "DIS")
            Call setHabilCtrl(fraBotoneraAdjuntos, "DIS")
            Call setHabilCtrl(fraHoras, "DIS")
        Case TAB_HITOS
            Call setHabilCtrl(fraAlertas, "DIS")
            Call setHabilCtrl(fraResponsables, "DIS")
            Call setHabilCtrl(fraBotoneraAdjuntos, "DIS")
            Call setHabilCtrl(fraHoras, "DIS")
        Case TAB_RESPONSABLES
            Call setHabilCtrl(fraAlertas, "DIS")
            Call setHabilCtrl(fraHitos, "DIS")
            Call setHabilCtrl(fraBotoneraAdjuntos, "DIS")
            Call setHabilCtrl(fraHoras, "DIS")
        Case TAB_ADJUNTOS
            Call setHabilCtrl(fraAlertas, "DIS")
            Call setHabilCtrl(fraHitos, "DIS")
            Call setHabilCtrl(fraResponsables, "DIS")
            Call setHabilCtrl(fraHoras, "DIS")
        Case TAB_HORAS
            Call setHabilCtrl(fraAlertas, "DIS")
            Call setHabilCtrl(fraHitos, "DIS")
            Call setHabilCtrl(fraResponsables, "DIS")
            Call setHabilCtrl(fraBotoneraAdjuntos, "DIS")
        Case Else
            Call setHabilCtrl(fraAlertas, "NOR")
            Call setHabilCtrl(fraHitos, "NOR")
            Call setHabilCtrl(fraResponsables, "NOR")
            Call setHabilCtrl(fraBotoneraAdjuntos, "NOR")
            Call setHabilCtrl(fraHoras, "NOR")
    End Select
End Sub

Private Sub HabilitarBotonesAdjuntos(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case STANDBY
            lblAdjuntosModoTrabajo = ""
            grdAdjuntos.Enabled = True
            Call setHabilCtrl(cmdAdjuntos_Agregar, "NOR")
            Call setHabilCtrl(cmdAdjuntos_Modificar, "NOR")
            Call setHabilCtrl(cmdAdjuntos_Eliminar, "NOR")
            Call setHabilCtrl(cmdAdjuntos_Confirmar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Cancelar, "DIS")
            Call setHabilCtrl(txtAdjuntoDescripcion, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(0)
            Call HabilitarBotones(0)
            Call Cargar_GrillaAdjuntos
            Call MostrarSeleccionAdjuntos
        Case AGREGAR
            lblAdjuntosModoTrabajo = "Agregar"
            grdAdjuntos.Enabled = False
            Call setHabilCtrl(cmdAdjuntos_Agregar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Modificar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Eliminar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Confirmar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Cancelar, "DIS")
            Call HabilitarBotones(3)
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            Call Adjuntar
            Call Cargar_GrillaAdjuntos
        Case EDITAR
            If grdAdjuntos.RowSel = 0 Then Exit Sub
            lblAdjuntosModoTrabajo = "Editar"
            grdAdjuntos.Enabled = False
            Call setHabilCtrl(cmdAdjuntos_Agregar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Modificar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Eliminar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Confirmar, "NOR")
            Call setHabilCtrl(cmdAdjuntos_Cancelar, "NOR")
            Call setHabilCtrl(txtAdjuntoDescripcion, "NOR")
            Call HabilitarBotones(3)
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
            txtAdjuntoDescripcion.SetFocus
        Case ELIMINAR
            If grdAdjuntos.RowSel = 0 Then Exit Sub
            lblAdjuntosModoTrabajo = "Eliminar"
            grdAdjuntos.Enabled = False
            Call setHabilCtrl(cmdAdjuntos_Agregar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Modificar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Eliminar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Confirmar, "NOR")
            Call setHabilCtrl(cmdAdjuntos_Cancelar, "NOR")
            Call HabilitarBotones(3)
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(sstProyectosIDM.Tab)
        Case DESHABILITADO
            lblAdjuntosModoTrabajo = ""
            grdAdjuntos.Enabled = True
            Call setHabilCtrl(cmdAdjuntos_Agregar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Modificar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Eliminar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Confirmar, "DIS")
            Call setHabilCtrl(cmdAdjuntos_Cancelar, "DIS")
            ' Inhabilita las pesta�as en el alta
            Call HabilitarPestanias(0)
    End Select
End Sub

Private Sub Adjuntar()
    On Error GoTo Errores
    Dim i As Integer
    Dim sPath As String
    
    sPath = GetSetting("GesPet", "Export01", "chkAdjuntoLPO", "C:\")
    If Dir(sPath, vbDirectory) = "" Then
        SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", "P:\"
        sPath = GetSetting("GesPet", "Export01", "chkAdjuntoLPO", "P:\")
    End If
    With mdiPrincipal.CommonDialog
        .CancelError = False
        .DialogTitle = "Adjuntar al proyecto..."
        .Filter = "Todos|*.*|*.xls|*.xls|*.doc|*.doc|*.rtf|*.rtf|*.pps|*.pps|*.mpp|*.mpp|*.ppt|*.ppt|*.tif|*.tif|*.bmp|*.bmp|*.jpg|*.jpg|*.htm|*.htm|*.html|*.html|*.txt|*.txt|*.pdf|*.pdf|*.zip|*.zip|*.msg|*.msg|*.rar|*.rar"
        .FilterIndex = 1
        .InitDir = IIf(glLSTDIR = "", glWRKDIR, glLSTDIR)
        .Flags = cdlOFNAllowMultiselect + cdlOFNFileMustExist + cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        If Len(.FileName) > 0 Then
            glLSTDIR = ObtenerDirectorio(.FileName) & "\"
            If MsgBox("�Confirma adjuntar el archivo " & .FileTitle & "?", vbQuestion + vbYesNo) = vbYes Then
                Call sp_InsertIDMAdjuntos(l_ProjId, l_ProjSubId, l_ProjSubsId, .FileTitle, glLSTDIR, txtAdjuntoDescripcion)
            End If
        Else
            GoTo Fin:
        End If
    End With
Fin:
    Call Puntero(False)
    Call HabilitarBotonesAdjuntos(0, "X")
    Exit Sub
Errores:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Call HabilitarBotonesAdjuntos(0, "X")
End Sub

Private Sub cmdAdjuntos_Visualizar_Click()
    Call VerAdjunto
End Sub

Private Sub VerAdjunto()
    On Error GoTo Errores
    If grdAdjuntos.RowSel = 0 Then
        MsgBox "No hay documento seleccionado para visualizar", vbExclamation + vbOKOnly
        Exit Sub
    End If
    With grdAdjuntos
        DoEvents
        Call Puntero(True)
        auxMensaje.prpTexto = "ATENCION" & Chr(13) & Chr(13) & Chr(13) & Chr(13) & "Si realiza modificaciones al documento" & Chr(13) & "no ser�n reflejadas en el adjunto"
        auxMensaje.prpSegundos = 2000
        auxMensaje.Disparar
        Call ProyectoIDMAdjunto2File(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colADJUNTOS_adj_tipo), .TextMatrix(.RowSel, colADJUNTOS_adj_file), glWRKDIR)
        'Call EXEC_ShellExecute(Me.hwnd, glWRKDIR & .TextMatrix(.RowSel, colADJUNTOS_adj_file))
        Call AbrirAdjunto(glWRKDIR & ClearNull(.TextMatrix(.RowSel, colADJUNTOS_adj_file)))
        Call Status("Listo. Bajado en " & glWRKDIR)
        Call Puntero(False)
    End With
Exit Sub
Errores:
    Select Case Err.Number
        Case 70     'Permiso denegado (archivo en uso, por ejemplo)
            If MsgBox("Debe ser borrada la versi�n local de este archivo (es posible que este en uso)." & vbCrLf & _
                    "Cierre el archivo. �Reintentar?", vbExclamation + vbOKCancel, "Archivo local en uso") = vbOK Then
                Resume
            Else
                Call Puntero(False)
                Call Status("Listo.")
                'glPathAdjPet = ""
                Exit Sub
            End If
        Case Else
            MsgBox "Nro. error : " & Err.Number & vbCrLf & _
                   "Descripcion: " & Err.DESCRIPTION, vbCritical + vbOKOnly, "Error al visualizar el adjunto"
    End Select
End Sub

Private Sub AbrirAdjunto(sFileName)
    Call EXEC_ShellExecute(Me.hwnd, sFileName)
End Sub

Private Sub MostrarSeleccionAlertas()
    If grdAlertas.RowSel > 0 Then       ' Inicializo los controles antes de mostrar
        cmbAlertaTipo.ListIndex = -1
        txtAlertaItem = ""
        txtAlertaDescripcion = ""
        txtAccionCorrectiva = ""
        txtAlertaFechaIni = ""
        txtAlertaFechaFin = ""
        optAlertasTexto(0).Value = True
        txtAlertasObservaciones = ""
        chkAlertaResuelta.Value = 0
        With grdAlertas
            txtAlertaItem = .TextMatrix(.RowSel, colALERTA_ITEM)
            cmbAlertaTipo.ListIndex = PosicionCombo(cmbAlertaTipo, ClearNull(.TextMatrix(.RowSel, colALERTA_TIPO)), True)
            txtAlertaDescripcion = .TextMatrix(.RowSel, colALERTA_DESCRIPCION)
            txtAccionCorrectiva = .TextMatrix(.RowSel, colALERTA_ACCIONCORRECTIVA)
            txtAlertaFechaIni = .TextMatrix(.RowSel, colALERTA_INICIO)
            txtAlertaFechaFin = .TextMatrix(.RowSel, colALERTA_FIN)
            chkAlertaResuelta.Value = IIf(.TextMatrix(.RowSel, colALERTA_STATUS) = "S", 1, 0)
            Select Case IIf(optAlertasTexto(0).Value, 0, 1)
                Case 0: txtAlertasObservaciones = .TextMatrix(.RowSel, colALERTA_OBSERVACIONES)
                Case 1: txtAlertasObservaciones = .TextMatrix(.RowSel, colALERTA_RESPONSABLES)
            End Select
            txtFechaResolucion = .TextMatrix(.RowSel, colALERTA_FECRESOLUCION)
        End With
    End If
End Sub

Private Sub MostrarSeleccionHitos()
    If grdHitos.RowSel > 0 Then         ' Inicializo los controles antes de mostrar
        bLoading = True                 ' add -002- a.
        txtHitoInicio = ""
        txtHitoFinalizacion = ""
        'txtHitoNombre = ""
        cmbHito = ""
        cmbHitoEstado.ListIndex = -1
        txtHitoDescripcion.Text = ""
        '{ add -002- a.
        cmbHitoExposicion.ListIndex = -1
        cmbHitoClasificacion.ListIndex = -1
        '}
        With grdHitos
            txtHitoInicio = ClearNull(.TextMatrix(.RowSel, colHITO_INICIO)): txtHitoInicio.Tag = txtHitoInicio
            txtHitoFinalizacion = ClearNull(.TextMatrix(.RowSel, colHITO_FIN)): txtHitoFinalizacion.Tag = txtHitoFinalizacion
            'txtHitoNombre = .TextMatrix(.RowSel, colHITO_HITONOMBRE): txtHitoNombre.Tag = txtHitoNombre
            cmbHito = .TextMatrix(.RowSel, colHITO_HITONOMBRE): cmbHito.Tag = cmbHito
            cmbHitoEstado.ListIndex = PosicionCombo(cmbHitoEstado, .TextMatrix(.RowSel, colHITO_ESTADO), True)
            cboHitosAreaResp.ListIndex = PosicionCombo(cboHitosAreaResp, .TextMatrix(.RowSel, colHITO_DEPAREA), True)
            txtHitoDescripcion.Text = .TextMatrix(.RowSel, colHITO_DESCRIPCION)
            txtHitoOrden.Text = .TextMatrix(.RowSel, colHITO_HITORDEN)
            '{ add -002- a.
            cmbHitoExposicion.ListIndex = PosicionCombo(cmbHitoExposicion, IIf(.TextMatrix(.RowSel, colHITO_EXPOSICION) = "", "NULL", .TextMatrix(.RowSel, colHITO_EXPOSICION)), True)
            cmbHitoClasificacion.ListIndex = PosicionCombo(cmbHitoClasificacion, IIf(.TextMatrix(.RowSel, colHITO_CLASE) = "", "NULL", .TextMatrix(.RowSel, colHITO_CLASE)), False)
            ' Guardo los datos originales para luego comparar si hubo cambios
            'txtHitoNombre.Tag = txtHitoNombre.Text     ' del -005- a.
            cmbHito.Tag = cmbHito.Text                  ' add -005- a.
            txtHitoDescripcion.Tag = txtHitoDescripcion.Text
            cmbHitoExposicion.Tag = CodigoCombo(cmbHitoExposicion, True)
            cmbHitoClasificacion.Tag = IIf(cmbHitoClasificacion.ListIndex > -1, CodigoCombo(cmbHitoClasificacion, False), "")
            '}
            'If .RowSel > 0 Then txtHitoDescripcion.Text = sp_GetProyectoIDMHitosTxt(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HITO), .TextMatrix(.RowSel, colHITO_INICIO))
        End With
    End If
    bLoading = False                 ' add -002- a.
End Sub

Private Sub grdHoras_RowColChange()
    If Not bLoading Then MostrarSeleccionHoras
End Sub

Private Sub MostrarSeleccionHoras()
    If grdHoras.RowSel > 0 Then         ' Inicializo los controles antes de mostrar
        cmbHorasArea.ListIndex = -1
        txtAreaHoras = ""
        With grdHoras
            'cmbHorasArea.ListIndex = PosicionCombo(cmbHorasArea, ClearNull(.TextMatrix(.RowSel, colHORAS_cod_nivel)), True)
            cmbHorasArea.Clear
            cmbHorasArea.AddItem ClearNull(.TextMatrix(.RowSel, colHORAS_cod_area)) & ": " & ClearNull(.TextMatrix(.RowSel, colHORAS_nom_area)) & Space(100) & "||" & ClearNull(.TextMatrix(.RowSel, colHORAS_cod_nivel))
            cmbHorasArea.ListIndex = 0
            txtAreaHoras = ClearNull(.TextMatrix(.RowSel, colHORAS_cant_horas))
        End With
    End If
End Sub

Private Sub MostrarSeleccionResponsables()
    If grdAreasResponsables.RowSel > 0 Then     ' Inicializo los controles antes de mostrar
        cboAreaResponsable.ListIndex = -1
        With grdAreasResponsables
            cboAreaResponsable.ListIndex = PosicionCombo(cboAreaResponsable, .TextMatrix(.RowSel, colRESP_DEPAREA), True)
        End With
    End If
End Sub

Private Sub MostrarSeleccionAdjuntos()
    ' Inicializo los controles antes de mostrar
    If grdAdjuntos.RowSel > 0 Then
        With grdAdjuntos
            txtAdjuntoFile = .TextMatrix(.RowSel, colADJUNTOS_adj_file)
            txtAdjuntoDescripcion = .TextMatrix(.RowSel, colADJUNTOS_adj_texto)
            txtAdjuntoArea = .TextMatrix(.RowSel, colADJUNTOS_nom_area)
            txtAdjuntoUsuario = .TextMatrix(.RowSel, colADJUNTOS_nom_user)
            txtAdjuntoFecha = Format(.TextMatrix(.RowSel, colADJUNTOS_audit_date), "dd/mm/yyyy hh:MM")
        End With
    End If
End Sub

Private Sub grdAreasResponsables_Click()
    bSorting = True
    Call OrdenarGrilla(grdAreasResponsables, lUltimaColumnaOrdenada_Responsables)
    bSorting = False
End Sub

Private Sub grdAreasResponsables_SelChange()
    Call MostrarSeleccionResponsables
End Sub

Private Sub cboAreaResponsable_Click()
    Dim cod_nivel As String
    Dim cod_area As String
    
    Call ObtenerNivelAreaCombo(cboAreaResponsable, cod_nivel, cod_area)
    Select Case cod_nivel
        Case "DIRE": lblAreaResponsableDatos = "Nivel: Direcci�n" & vbCrLf
        Case "GERE": lblAreaResponsableDatos = "Nivel: Gerencia" & vbCrLf
        Case "SECT": lblAreaResponsableDatos = "Nivel: Sector" & vbCrLf
        Case "GRUP": lblAreaResponsableDatos = "Nivel: Grupo" & vbCrLf
    End Select
    lblAreaResponsableDatos = lblAreaResponsableDatos & "�rea: " & TextoCombo(cboAreaResponsable, CodigoCombo(cboAreaResponsable, True), True)
End Sub

Private Sub optAlertasTexto_Click(Index As Integer)
    If Not bLoading Then
        With grdAlertas
            Select Case lblAlertasModoTrabajo
                Case ""
                    Call setHabilCtrl(txtAlertasObservaciones, "DIS")
                    Select Case IIf(optAlertasTexto(0).Value, 0, 1)
                        Case 0: txtAlertasObservaciones = IIf(.RowSel > 0 And lblAlertasModoTrabajo = "", .TextMatrix(.RowSel, colALERTA_OBSERVACIONES), "")
                        Case 1: txtAlertasObservaciones = IIf(.RowSel > 0 And lblAlertasModoTrabajo = "", .TextMatrix(.RowSel, colALERTA_RESPONSABLES), "")
                        Case Else: txtAlertasObservaciones = ""
                    End Select
                Case "Agregar", "Modificar"
                    Select Case IIf(optAlertasTexto(0).Value, 0, 1)
                        Case 0: txtAlertasObservaciones = ClearNull(cAlertasObservaciones)
                        Case 1: txtAlertasObservaciones = ClearNull(cAlertasResponsables)
                    End Select
            End Select
        End With
    End If
End Sub

' ************************************************************************************************************************
' MENU: HITOS
' ************************************************************************************************************************
Private Sub cmdHitos_Agregar_Click()
    Call HabilitarBotonesHitos(AGREGAR)
End Sub

Private Sub cmdHitos_Modificar_Click()
    Call HabilitarBotonesHitos(EDITAR)
End Sub

Private Sub cmdHitos_Eliminar_Click()
    Call HabilitarBotonesHitos(ELIMINAR)
End Sub

Private Sub cmdHitos_Confirmar_Click()
    If CamposObligatorios_Hitos Then Call Guardar_Hito
End Sub

Private Sub cmdHitos_Cancelar_Click()
    Call HabilitarBotonesHitos(STANDBY)
End Sub

Private Sub Guardar_Hito()
    Dim hst_nrointerno As Long          ' Nro. interno para el historial
    Dim cod_nivel As String
    Dim cod_area As String

    With grdHitos
        Select Case lblHitosModoTrabajo
            Case "Agregar"
                If MsgBox("�Confirma el agregado de este hito?", vbQuestion + vbOKCancel) = vbOK Then
                    Call ObtenerNivelAreaCombo(cboHitosAreaResp, cod_nivel, cod_area)
                    Call sp_InsertProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, ClearNull(cmbHito), ClearNull(txtHitoDescripcion), txtHitoInicio.DateValue, txtHitoFinalizacion.DateValue, CodigoCombo(cmbHitoEstado, True), cod_nivel, cod_area, txtHitoOrden, CodigoCombo(cmbHitoExposicion, True), CodigoCombo(cmbHitoClasificacion, False))      ' upd -002- a.
                    Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "HITOADD", CodigoCombo(cmbEstado, True), "", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
                    If ClearNull(cmbHito) = "Cambio de alcance" Then
                        hst_nrointerno = sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGALCA", CodigoCombo(cmbEstado, True), ClearNull(cmbHito) & "(" & ClearNull(txtHitoDescripcion) & ")", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
                        Call sp_UpdateProyectoIDMHitosField(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HITNROINTERNO), "HST_NROINTERNO", Null, Null, hst_nrointerno)
                    End If
                    Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                    bActualizo = True
                    Call Cargar_GrillaHitos
                    Call HabilitarBotonesHitos(STANDBY)
                End If
            Case "Modificar"
                Call ObtenerNivelAreaCombo(cboHitosAreaResp, cod_nivel, cod_area)
                Call sp_UpdateProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HITNROINTERNO), ClearNull(cmbHito), ClearNull(txtHitoDescripcion), txtHitoInicio.DateValue, txtHitoFinalizacion.DateValue, CodigoCombo(cmbHitoEstado, True), cod_nivel, cod_area, txtHitoOrden, CodigoCombo(cmbHitoExposicion, True), CodigoCombo(cmbHitoClasificacion, False))        ' upd -002- a.
                Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "HITOUPD", CodigoCombo(cmbEstado, True), "", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
                Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                bActualizo = True
                Call Cargar_GrillaHitos
                Call HabilitarBotonesHitos(STANDBY)
            Case "Eliminar"
                If InStr(1, "ADMI|SADM|", glUsrPerfilActual, vbTextCompare) > 0 Then
                    If MsgBox("�Confirma la eliminaci�n de este hito?", vbQuestion + vbOKCancel) = vbOK Then
                        Call sp_DeleteProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HITNROINTERNO))
                        Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                        Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "HITODEL", CodigoCombo(cmbEstado, True), "", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
                        If .TextMatrix(.RowSel, colHITO_HITONOMBRE) = "Cambio de alcance" And IsNumeric(.TextMatrix(.RowSel, colHITO_HSTNROINTERNO)) Then
                            Call sp_DeleteProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HSTNROINTERNO))
                        End If
                        bActualizo = True
                        Call Cargar_GrillaHitos
                        Call HabilitarBotonesHitos(STANDBY)
                    End If
                Else
                    ' Validamos que el hito seleccionado para eliminar no sea "Fecha de entrega al usuario"
                    If sp_GetProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, .TextMatrix(.RowSel, colHITO_HITNROINTERNO), Null) Then
                        If ClearNull(UCase(aplRST.Fields!hito_nombre)) = UCase("Fecha de entrega al usuario") Then
                            MsgBox "El proyecto ya ha sido planificado." & vbCrLf & "No puede eliminar este hito.", vbExclamation + vbOKOnly
                            Call HabilitarBotonesHitos(STANDBY)
                            Exit Sub
                        End If
                    End If
                    If MsgBox("�Confirma la eliminaci�n de este hito?", vbQuestion + vbOKCancel) = vbOK Then
                        Call sp_DeleteProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HITNROINTERNO))
                        Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                        Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "HITODEL", CodigoCombo(cmbEstado, True), "", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
                        If .TextMatrix(.RowSel, colHITO_HITONOMBRE) = "Cambio de alcance" And IsNumeric(.TextMatrix(.RowSel, colHITO_HSTNROINTERNO)) Then
                            Call sp_DeleteProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HSTNROINTERNO))
                        End If
                        bActualizo = True
                        Call Cargar_GrillaHitos
                        Call HabilitarBotonesHitos(STANDBY)
                    End If
'                    If Not sp_GetProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, "Fecha de entrega al usuario") Then
'                        If MsgBox("�Confirma la eliminaci�n de este hito?", vbQuestion + vbOKCancel) = vbOK Then
'                            Call sp_DeleteProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HITNROINTERNO))
'                            Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
'                            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "HITODEL", CodigoCombo(cmbEstado, True), "", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
'                            If .TextMatrix(.RowSel, colHITO_HITONOMBRE) = "Cambio de alcance" And IsNumeric(.TextMatrix(.RowSel, colHITO_HSTNROINTERNO)) Then
'                                Call sp_DeleteProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, .TextMatrix(.RowSel, colHITO_HSTNROINTERNO))
'                            End If
'                            bActualizo = True
'                            Call Cargar_GrillaHitos
'                            Call HabilitarBotonesHitos(STANDBY)
'                        End If
'                    Else
'                        MsgBox "El proyecto ya ha sido planificado." & vbCrLf & "No puede eliminar este hito.", vbExclamation + vbOKOnly
'                        Call HabilitarBotonesHitos(STANDBY)
'                    End If
                End If
        End Select
    End With
End Sub

' ************************************************************************************************************************
' MENU: ALERTAS
' ************************************************************************************************************************
Private Sub cmdAlertas_Agregar_Click()
    HabilitarBotonesAlertas AGREGAR
End Sub

Private Sub cmdAlertas_Modificar_Click()
    HabilitarBotonesAlertas EDITAR
End Sub

Private Sub cmdAlertas_Eliminar_Click()
    HabilitarBotonesAlertas ELIMINAR
End Sub

Private Sub cmdAlertas_Confirmar_Click()
    Guardar_Alerta
End Sub

Private Sub cmdAlertas_Cancelar_Click()
    HabilitarBotonesAlertas STANDBY
End Sub

Private Sub Guardar_Alerta()
    Dim bHecho As Boolean
    
    bHecho = False
    Select Case lblAlertasModoTrabajo
        Case "Agregar"
            If CamposObligatorios_Alertas Then
                Call spProyectoIDM.sp_InsertProyectoIDMAlertas(l_ProjId, l_ProjSubId, l_ProjSubsId, CodigoCombo(cmbAlertaTipo, True), txtAlertaDescripcion, txtAccionCorrectiva, txtAlertaFechaIni, txtAlertaFechaFin, cAlertasResponsables, cAlertasObservaciones, IIf(chkAlertaResuelta.Value = 1, "S", "N"))
                Call spProyectoIDM.sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                bHecho = True
                HabilitarBotonesAlertas STANDBY
                Cargar_GrillaAlertas
                bActualizo = True
            End If
        Case "Modificar"
            If CamposObligatorios_Alertas Then
                Call spProyectoIDM.sp_UpdateProyectoIDMAlertas(l_ProjId, l_ProjSubId, l_ProjSubsId, txtAlertaItem, CodigoCombo(cmbAlertaTipo, True), txtAlertaDescripcion, txtAccionCorrectiva, txtAlertaFechaIni, txtAlertaFechaFin, cAlertasResponsables, cAlertasObservaciones, IIf(chkAlertaResuelta.Value = 1, "S", "N"), txtFechaResolucion)
                Call spProyectoIDM.sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                Cargar_GrillaAlertas
                bHecho = True
                bActualizo = True
                HabilitarBotonesAlertas STANDBY
            End If
        Case "Eliminar"
            If MsgBox("�Confirma la eliminaci�n definitiva del alerta seleccionado?", vbExclamation + vbOKCancel, "Alertas") = vbOK Then
                Call spProyectoIDM.sp_DeleteProyectoIDMAlertas(l_ProjId, l_ProjSubId, l_ProjSubsId, txtAlertaItem)
                Call spProyectoIDM.sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                bHecho = True
                If grdAlertas.RowSel > 1 Then
                    grdAlertas.RemoveItem grdAlertas.RowSel
                Else
                    Call Inicializar_GrillaAlertas
                End If
                bActualizo = True
                Call HabilitarBotonesAlertas(STANDBY)
            End If
    End Select
    If bHecho Then  ' Recalcula el valor del sem�foro en funci�n de las alertas vigentes
        Call sp_GetProyectoIDMAlertaMayor(l_ProjId, l_ProjSubId, l_ProjSubsId)
        If InStr(1, "R|A|-|", ClearNull(aplRST.Fields!alert_tipo), vbTextCompare) > 0 Then
            Select Case ClearNull(aplRST.Fields!alert_tipo)
                Case "R": Call spProyectoIDM.sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "SEMAFORO", COLOR_SEMAF_ROJO, Null, Null)
                Case "A": Call spProyectoIDM.sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "SEMAFORO", COLOR_SEMAF_AMARILLO, Null, Null)
                Case Else: Call spProyectoIDM.sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "SEMAFORO", COLOR_SEMAF_VERDE, Null, Null)
            End Select
            Cargar_Proyecto
        End If
    End If
End Sub

' ************************************************************************************************************************
' MENU: RESPONSABLES
' ************************************************************************************************************************
Private Sub cmdResponsables_Agregar_Click()
    HabilitarBotonesResponsables AGREGAR
End Sub

Private Sub cmdResponsables_Eliminar_Click()
    HabilitarBotonesResponsables ELIMINAR
End Sub

Private Sub cmdResponsables_Confirmar_Click()
    If CamposObligatorios_Responsables Then
        Call Guardar_Responsables
    End If
End Sub

Private Sub cmdResponsables_Cancelar_Click()
    HabilitarBotonesResponsables STANDBY
End Sub

Private Sub Guardar_Responsables()
    Dim cod_nivel As String
    Dim cod_area As String
    Dim bHecho As Boolean
    
    Select Case lblResponsablesModoTrabajo
        Case "Agregar"
            If MsgBox("�Confirma el agregado de este �rea?", vbQuestion + vbOKCancel) = vbOK Then
                Call ObtenerNivelAreaCombo(cboAreaResponsable, cod_nivel, cod_area)
                Call spProyectoIDM.sp_InsertProyectoIDMResponsables(l_ProjId, l_ProjSubId, l_ProjSubsId, cod_nivel, cod_area, "N")
                bHecho = True
                HabilitarBotonesResponsables STANDBY
                Cargar_GrillaResponsables
                bActualizo = True
            End If
        Case "Eliminar"
            If MsgBox("�Confirma la eliminaci�n definitiva del �rea seleccionada?", vbExclamation + vbOKCancel, "�reas responsables") = vbOK Then
                Call ObtenerNivelAreaCombo(cboAreaResponsable, cod_nivel, cod_area)
                Call spProyectoIDM.sp_DeleteProyectoIDMResponsables(l_ProjId, l_ProjSubId, l_ProjSubsId, cod_nivel, cod_area)
                bHecho = True
                If grdAreasResponsables.RowSel > 1 Then
                    grdAreasResponsables.RemoveItem grdAreasResponsables.RowSel
                Else
                    Call Inicializar_GrillaResponsables
                End If
                bActualizo = True
                Call HabilitarBotonesResponsables(STANDBY)
            End If
    End Select
End Sub

Private Sub Guardar_Adjuntos()
    Select Case lblAdjuntosModoTrabajo
        Case "Agregar"
            Call sp_InsertIDMAdjuntos(l_ProjId, l_ProjSubId, l_ProjSubsId, ClearNull(txtAdjuntoFile), "", txtAdjuntoDescripcion)
            HabilitarBotonesAdjuntos STANDBY
            Cargar_GrillaAdjuntos
        Case "Editar"
            Call sp_UpdateIDMAdjuntos(l_ProjId, l_ProjSubId, l_ProjSubsId, "", ClearNull(txtAdjuntoFile), txtAdjuntoDescripcion)
            HabilitarBotonesAdjuntos STANDBY
            Cargar_GrillaAdjuntos
        Case "Eliminar"
            Call sp_DeleteIDMAdjuntos(l_ProjId, l_ProjSubId, l_ProjSubsId, "", ClearNull(txtAdjuntoFile))
            With grdAdjuntos
                txtAdjuntoFile = "-"
                txtAdjuntoDescripcion = "-"
                txtAdjuntoArea = "-"
                txtAdjuntoUsuario = "-"
                txtAdjuntoFecha = "-"
            End With
            HabilitarBotonesAdjuntos STANDBY
            Cargar_GrillaAdjuntos
    End Select
End Sub

' ************************************************************************************************************************
' MENU: ADJUNTOS
' ************************************************************************************************************************
Private Sub cmdAdjuntos_Agregar_Click()
    Call HabilitarBotonesAdjuntos(AGREGAR)
End Sub

Private Sub cmdAdjuntos_Modificar_Click()
    Call HabilitarBotonesAdjuntos(EDITAR)
End Sub

Private Sub cmdAdjuntos_Eliminar_Click()
    Call HabilitarBotonesAdjuntos(ELIMINAR)
End Sub

Private Sub cmdAdjuntos_Confirmar_Click()
    Call Guardar_Adjuntos
End Sub

Private Sub cmdAdjuntos_Cancelar_Click()
    Call HabilitarBotonesAdjuntos(STANDBY)
End Sub

' ************************************************************************************************************************
' MENU: HORAS
' ************************************************************************************************************************
Private Sub cmdHoras_Agregar_Click()
    Call HabilitarBotonesHoras(AGREGAR)
End Sub

Private Sub cmdHoras_Modificar_Click()
    Call HabilitarBotonesHoras(EDITAR)
End Sub

Private Sub cmdHoras_Eliminar_Click()
    Call HabilitarBotonesHoras(ELIMINAR)
End Sub

Private Sub cmdHoras_Confirmar_Click()
    If CamposObligatorios_Horas Then
        Call Guardar_Horas
    End If
End Sub

Private Sub cmdHoras_Cancelar_Click()
    Call HabilitarBotonesHoras(STANDBY)
End Sub

Private Function CamposObligatorios_Horas() As Boolean
    CamposObligatorios_Horas = True

    If cmbHorasArea.ListIndex = -1 Then
        CamposObligatorios_Horas = False
        MsgBox "Debe seleccionar un �rea.", vbExclamation + vbOKOnly, "Error"
        Exit Function
    End If
    If txtAreaHoras = "" Then
        CamposObligatorios_Horas = False
        MsgBox "Debe ingresar las horas.", vbExclamation + vbOKOnly, "Error"
        Exit Function
    Else
        If Not IsNumeric(txtAreaHoras) Then
            CamposObligatorios_Horas = False
            MsgBox "Debe ingresar un entero positivo.", vbExclamation + vbOKOnly, "Error"
            Exit Function
        Else
            If Val(txtAreaHoras) <= 0 Then
                CamposObligatorios_Horas = False
                MsgBox "Debe ingresar un entero positivo.", vbExclamation + vbOKOnly, "Error"
                Exit Function
            End If
        End If
    End If
End Function

Private Sub optHorasDeclaradas_Click(Index As Integer)
    With cmbHorasArea
        .Clear
        Select Case Index
            Case 1      ' Horas planificadas por Gerencia
                If sp_GetGerencia(Null, Null, "S") Then
                    Do While Not aplRST.EOF
                        .AddItem aplRST.Fields!cod_gerencia & ": " & ClearNull(aplRST.Fields!nom_gerencia) & ESPACIOS & "||GER"
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            Case 0      ' Horas estimadas por Sector
                If sp_GetSectorXt(Null, Null, "MEDIO", "S") Then
                    Do While Not aplRST.EOF
                        If aplRST.Fields!es_ejecutor = "S" Then
                            .AddItem aplRST.Fields!cod_sector & ": " & ClearNull(aplRST.Fields!nom_gerencia) & " � " & ClearNull(aplRST.Fields!nom_sector) & ESPACIOS & "||SEC"
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
        End Select
        .ListIndex = 0
    End With
End Sub

Private Sub Guardar_Horas()
    Dim cod_nivel As String
    Dim cod_area As String
    
    Select Case lblHorasModoTrabajo
        Case "Agregar"
            If MsgBox("�Confirma el agregado de horas?", vbQuestion + vbOKCancel) = vbOK Then
                Call sp_InsertProyectoIDMHoras(l_ProjId, l_ProjSubId, l_ProjSubsId, CodigoCombo(cmbHorasArea, True), CodigoCombo(cmbHorasArea, False), Val(txtAreaHoras))
                Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                bActualizo = True
                If sp_GetProyectoIDM(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, Null, Null, Null, Null) Then
                    txtHsPlanifGer = ClearNull(aplRST.Fields!totalhs_gerencia)
                    txtHsEstimaSec = ClearNull(aplRST.Fields!totalhs_sector)
                End If
                Call Cargar_GrillaHoras
                Call HabilitarBotonesHoras(STANDBY)
            End If
        Case "Modificar"
            Call sp_DeleteProyectoIDMHoras(l_ProjId, l_ProjSubId, l_ProjSubsId, CodigoCombo(cmbHorasArea, True), CodigoCombo(cmbHorasArea, False))
            Call sp_InsertProyectoIDMHoras(l_ProjId, l_ProjSubId, l_ProjSubsId, CodigoCombo(cmbHorasArea, True), CodigoCombo(cmbHorasArea, False), txtAreaHoras)
            Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)        ' Actualiza la fecha de �ltima modificaci�n
            If sp_GetProyectoIDM(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, Null, Null, Null, Null) Then
                txtHsPlanifGer = ClearNull(aplRST.Fields!totalhs_gerencia)
                txtHsEstimaSec = ClearNull(aplRST.Fields!totalhs_sector)
            End If
            Call Cargar_GrillaHoras
            Call HabilitarBotonesHoras(STANDBY)
        Case "Eliminar"
            If MsgBox("�Confirma la eliminaci�n de las horas?", vbQuestion + vbOKCancel) = vbOK Then
                Call sp_DeleteProyectoIDMHoras(l_ProjId, l_ProjSubId, l_ProjSubsId, CodigoCombo(cmbHorasArea, True), CodigoCombo(cmbHorasArea, False))
                Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "FE_MODIF", Null, Now, Null)   ' Actualiza la fecha de �ltima modificaci�n
                bActualizo = True
                If grdHoras.RowSel > 1 Then
                    grdHoras.RemoveItem grdHoras.RowSel
                Else
                    Call Inicializar_GrillaHoras
                End If
                If sp_GetProyectoIDM(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, Null, Null, Null, Null) Then
                    txtHsPlanifGer = ClearNull(aplRST.Fields!totalhs_gerencia)
                    txtHsEstimaSec = ClearNull(aplRST.Fields!totalhs_sector)
                End If
                Call HabilitarBotonesHoras(STANDBY)
            End If
    End Select
End Sub

Private Sub Cargar_Proyecto()
    Dim sTexto As String
    
    bLoading = True
    txtDescripcion.Text = ""
    txtBeneficios.Text = ""
    txtNovedades.Text = ""
    bSemaforoManual = False
    
    Call Cargar_TodosLosEstados
    If sp_GetProyectoIDM(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, Null, Null, Null, Null) Then    ' Cargo los datos principales del proyecto
        ' Primero guardo los datos originales para saber si hubo cambios a registrar en el historial
        txtNombreProyecto.Tag = ClearNull(aplRST.Fields!projnom)
        txtCodigoGPS.Tag = ClearNull(aplRST.Fields!codigo_gps)
        cmbCategoria.Tag = ClearNull(aplRST.Fields!ProjCatId)
        cmbClase.Tag = ClearNull(aplRST.Fields!ProjClaseId)
        cmbSolicitante.Tag = ClearNull(aplRST.Fields!cod_direccion) & IIf(IsNull(aplRST.Fields!cod_gerencia), "", "/" & ClearNull(aplRST.Fields!cod_gerencia)) & IIf(IsNull(aplRST.Fields!cod_sector), "", "/" & ClearNull(aplRST.Fields!cod_sector))
        cmbPropietario.Tag = ClearNull(aplRST.Fields!cod_direccion_p) & IIf(IsNull(aplRST.Fields!cod_gerencia_p), "", "/" & ClearNull(aplRST.Fields!cod_gerencia_p)) & IIf(IsNull(aplRST.Fields!cod_sector_p), "", "/" & ClearNull(aplRST.Fields!cod_sector_p)) & IIf(IsNull(aplRST.Fields!cod_grupo_p), "", "/" & ClearNull(aplRST.Fields!cod_grupo_p))
        cmbPlanTyO.Tag = ClearNull(aplRST.Fields!plan_tyo)
        txtInicio.Tag = ClearNull(aplRST.Fields!fe_inicio)
        cmbEmpresa.Tag = ClearNull(aplRST.Fields!empresa)
        txtFinPrevisto.Tag = ClearNull(aplRST.Fields!fe_fin)
        cmbArea.Tag = ClearNull(aplRST.Fields!area)
        cmbClas3.Tag = ClearNull(aplRST.Fields!clas3)
        cmbTipoProyecto.Tag = ClearNull(aplRST.Fields!tipo_proyecto)
        txtFinReprogramado.Tag = ClearNull(aplRST.Fields!fe_finreprog)
        'cmbEstado.Tag = ClearNull(aplRST.Fields!cod_estado)
        cmbEstado.Tag = ClearNull(aplRST.Fields!nom_estado) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_estado)   ' Cargo tambi�n el estado actual
        cmbSubestado.Tag = ClearNull(aplRST.Fields!cod_estado2)
        txtHsPlanifGer.Tag = ClearNull(aplRST.Fields!totalhs_gerencia)
        txtHsEstimaSec.Tag = ClearNull(aplRST.Fields!totalhs_sector)
        txtReplanif.Tag = ClearNull(aplRST.Fields!totalreplan)
        'shpSemaforo.Tag = ""        ' Falta definir!
        cNombreProyecto = ClearNull(aplRST.Fields!projnom)
        cCodigoGPS = ClearNull(aplRST.Fields!codigo_gps)
        cProyectoCategoria = ClearNull(aplRST.Fields!ProjCatId)
        cProyectoClase = ClearNull(aplRST.Fields!ProjClaseId)
        lblCodigoProyecto = aplRST.Fields!ProjId & "." & aplRST.Fields!ProjSubId & "." & aplRST.Fields!ProjSubSId
        lblNombreProyecto = ClearNull(aplRST.Fields!projnom)
        lblCodigoGPS = ClearNull(aplRST.Fields!codigo_gps)
        cmbSolicitante.ListIndex = PosicionCombo(cmbSolicitante, ClearNull(aplRST.Fields!cod_direccion) & IIf(IsNull(aplRST.Fields!cod_gerencia), "", "/" & ClearNull(aplRST.Fields!cod_gerencia)) & IIf(IsNull(aplRST.Fields!cod_sector), "", "/" & ClearNull(aplRST.Fields!cod_sector)), True)
        cmbPropietario.ListIndex = PosicionCombo(cmbPropietario, ClearNull(aplRST.Fields!cod_direccion_p) & IIf(IsNull(aplRST.Fields!cod_gerencia_p), "", "/" & ClearNull(aplRST.Fields!cod_gerencia_p)) & IIf(IsNull(aplRST.Fields!cod_sector_p), "", "/" & ClearNull(aplRST.Fields!cod_sector_p)) & IIf(IsNull(aplRST.Fields!cod_grupo_p), "", "/" & ClearNull(aplRST.Fields!cod_grupo_p)), True)
        cmbPlanTyO.ListIndex = PosicionCombo(cmbPlanTyO, ClearNull(aplRST.Fields!plan_tyo), True)
        cmbEmpresa.ListIndex = PosicionCombo(cmbEmpresa, ClearNull(aplRST.Fields!empresa), True)
        cmbArea.ListIndex = PosicionCombo(cmbArea, ClearNull(aplRST.Fields!area), True)
        cmbClas3.ListIndex = PosicionCombo(cmbClas3, ClearNull(aplRST.Fields!clas3), True)
        FchUltAct = Format(ClearNull(aplRST.Fields!fe_modif), "dd/mm/yyyy hh:MM")
        txtInicio = ClearNull(aplRST.Fields!fe_inicio)
        txtFinPrevisto = ClearNull(aplRST.Fields!fe_fin)
        txtFinReprogramado = ClearNull(aplRST.Fields!fe_finreprog)
        txtAvance = ClearNull(aplRST.Fields!avance)
        txtAvance.Tag = txtAvance       ' Guardo aqu� el valor original para los cambios de estado autom�ticos
        cmbTipoProyecto.ListIndex = PosicionCombo(cmbTipoProyecto, ClearNull(aplRST.Fields!tipo_proyecto), True)
        ' Estado del proyecto
        cEstadoOriginal = ClearNull(aplRST.Fields!cod_estado)       ' Valor original del estado
        cmbEstado.ListIndex = PosicionCombo(cmbEstado, ClearNull(aplRST.Fields!cod_estado), True)
        If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
            cmbSubestado.ListIndex = PosicionCombo(cmbSubestado, ClearNull(aplRST.Fields!cod_estado2), True)
        End If
        ' Semaforo (c�lculo)
        cSemaforoOriginal = ClearNull(aplRST.Fields!semaphore)      ' Valor original del sem�foro
        If IsNull(aplRST.Fields!semaphore2) Then                    ' Autom�tico
            ' ****************************************************************************************
            ' ESTABLECIDO AUTOMATICAMENTE
            ' ****************************************************************************************
            bSemaforoManual = False
            lblSemaforoManual.visible = False
            'vscSemaforo.Value = IIf(IsNull(aplRST.Fields!semaphore), 1, aplRST.Fields!semaphore)
            udSemaforo.Value = IIf(IsNull(aplRST.Fields!semaphore), 1, aplRST.Fields!semaphore)
        Else
            ' ****************************************************************************************
            ' ESTABLECIDO MANUALMENTE
            ' ****************************************************************************************
            bSemaforoManual = True
            lblSemaforoManual.visible = True
            'vscSemaforo.Value = ClearNull(aplRST.Fields!semaphore2)
            udSemaforo.Value = ClearNull(aplRST.Fields!semaphore2)
        End If
        txtHsPlanifGer = ClearNull(aplRST.Fields!totalhs_gerencia)
        txtHsEstimaSec = ClearNull(aplRST.Fields!totalhs_sector)
        txtReplanif = ClearNull(aplRST.Fields!totalreplan)
        txtCambioAlcance = ClearNull(aplRST.Fields!cambio_alcance)
    End If
    Call ActualizarCombos(cProyectoCategoria, cProyectoClase)       ' Categor�a y clase
    sTexto = sp_GetProyectoIDMMemo(l_ProjId, l_ProjSubId, l_ProjSubsId, "DESCRIPCIO", Null)    ' Carga la descripci�n
    txtDescripcion.Text = sTexto
    txtBeneficios.Text = sp_GetProyectoIDMMemo(l_ProjId, l_ProjSubId, l_ProjSubsId, "BENEFICIOS", Null)     ' Carga los beneficios
    Call Cargar_Novedades                                           ' Cargar las novedades
    bLoading = False
End Sub

Private Sub Cargar_Novedades()
    Dim cUltimaNovedad As String
    
    cUltimaNovedad = ""
    txtNovedades.Text = ""
    'If spProyectoIDM.sp_GetProyectoIDMNovedades(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, "NOVEDADES", Null) Then
    If sp_GetProyectoIDMNovedades(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, "NOVEDADES", Null) Then
        Do While Not aplRST.EOF
            Select Case ClearNull(aplRST.Fields!mem_accion)
                Case "ADD"
                    If cUltimaNovedad <> Format(ClearNull(aplRST.Fields!mem_fecha), "yyyymmddhhMM") & ClearNull(aplRST.Fields!cod_usuario) Then
                        txtNovedades.Text = IIf(txtNovedades.Text = "", "", txtNovedades.Text & vbCrLf & vbCrLf)
                        txtNovedades.Text = txtNovedades.Text & "� Agregado el " & _
                                            Format(ClearNull(aplRST.Fields!mem_fecha), "dd/mm/yyyy") & " a las " & _
                                            Format(ClearNull(aplRST.Fields!mem_fecha), "hh:MM") & " por " & _
                                            IIf(ClearNull(aplRST.Fields!nom_usuario) = "", "Sistema", ClearNull(aplRST.Fields!nom_usuario)) & " �" & vbCrLf
                        txtNovedades.Text = txtNovedades.Text & ClearNull(aplRST.Fields!mem_texto)
                    Else
                        txtNovedades.Text = IIf(Right(txtNovedades.Text, 1) <> " ", txtNovedades.Text & " " & ClearNull(aplRST.Fields!mem_texto), txtNovedades.Text & ClearNull(aplRST.Fields!mem_texto))
                    End If
                    cUltimaNovedad = Format(ClearNull(aplRST.Fields!mem_fecha), "yyyymmddhhMM") & ClearNull(aplRST.Fields!cod_usuario)
                Case "UPD"
                    If cUltimaNovedad <> Format(ClearNull(aplRST.Fields!mem_fecha), "yyyymmddhhMM") & ClearNull(aplRST.Fields!cod_usuario) Then
                        txtNovedades.Text = IIf(txtNovedades.Text = "", "", txtNovedades.Text & vbCrLf & vbCrLf)
                        txtNovedades.Text = txtNovedades.Text & "� Modificado el " & _
                                            Format(ClearNull(aplRST.Fields!mem_fecha2), "dd/mm/yyyy") & " a las " & _
                                            Format(ClearNull(aplRST.Fields!mem_fecha2), "hh:MM") & " por " & _
                                            IIf(ClearNull(aplRST.Fields!nom_usuario2) = "", "Sistema", ClearNull(aplRST.Fields!nom_usuario2)) & " �" & vbCrLf
                        txtNovedades.Text = txtNovedades.Text & ClearNull(aplRST.Fields!mem_texto)
                    Else
                        txtNovedades.Text = IIf(Right(txtNovedades.Text, 1) <> " ", txtNovedades.Text & " " & ClearNull(aplRST.Fields!mem_texto), txtNovedades.Text & ClearNull(aplRST.Fields!mem_texto))
                    End If
                    cUltimaNovedad = Format(ClearNull(aplRST.Fields!mem_fecha2), "yyyymmddhhMM") & ClearNull(aplRST.Fields!cod_usuario2)
            End Select
            aplRST.MoveNext
            DoEvents
        Loop
    End If
End Sub

Private Sub Inicializar_GrillaAlertas()
    With grdAlertas
        .visible = False
        .Clear
        .Rows = 1
        .cols = 12 + 1
        .TextMatrix(0, colALERTA_TIPO) = "Tipo": .ColWidth(colALERTA_TIPO) = 600
        .TextMatrix(0, colALERTA_DESCRIPCION) = "Descripci�n": .ColWidth(colALERTA_DESCRIPCION) = 3000
        .TextMatrix(0, colALERTA_ACCIONCORRECTIVA) = "Acci�n correctiva": .ColWidth(colALERTA_ACCIONCORRECTIVA) = 3500
        .TextMatrix(0, colALERTA_INICIO) = "Inicio": .ColWidth(colALERTA_INICIO) = 1200
        .TextMatrix(0, colALERTA_FIN) = "Fin": .ColWidth(colALERTA_FIN) = 1200
        .TextMatrix(0, colALERTA_RESPONSABLES) = "Responsables": .ColWidth(colALERTA_RESPONSABLES) = 3000
        .TextMatrix(0, colALERTA_OBSERVACIONES) = "Observaciones": .ColWidth(colALERTA_OBSERVACIONES) = 3000
        .TextMatrix(0, colALERTA_ITEM) = "Item": .ColWidth(colALERTA_ITEM) = 0
        .TextMatrix(0, colALERTA_STATUS) = "sta": .ColWidth(colALERTA_STATUS) = 0
        .TextMatrix(0, colALERTA_STATUSDSC) = "Resuelta": .ColWidth(colALERTA_STATUSDSC) = 1000
        .TextMatrix(0, colALERTA_FECRESOLUCION) = "F. Resol.": .ColWidth(colALERTA_FECRESOLUCION) = 1200
        .TextMatrix(0, colALERTA_USUARIO) = "Usuario": .ColWidth(colALERTA_USUARIO) = 1000
        .TextMatrix(0, colALERTA_FECHA) = "F. Modif.": .ColWidth(colALERTA_FECHA) = 1200
        Call CambiarEfectoLinea(grdAlertas, prmGridEffectFontBold)
        .RowSel = .Rows - 1: .ColSel = .cols - 1    ' Selecciona la primera fila de todas
        .HighLight = flexHighlightWithFocus
    End With
End Sub

Private Sub Inicializar_GrillaHitos()
    With grdHitos
        .visible = False
        .Clear
        .Rows = 1
        .cols = 20 + 1             ' upd -002- a. - Antes, 16. Ahora 20.
        .TextMatrix(0, colHITO_INICIO) = "Inicio": .ColWidth(colHITO_INICIO) = 1000
        .TextMatrix(0, colHITO_FIN) = "Fin": .ColWidth(colHITO_FIN) = 1000
        .TextMatrix(0, colHITO_HITO) = "Hito": .ColWidth(colHITO_HITO) = 0: .ColAlignment(colHITO_HITO) = vbRightJustify
        .TextMatrix(0, colHITO_HITONOMBRE) = "Nombre": .ColWidth(colHITO_HITONOMBRE) = 3500: .ColAlignment(colHITO_HITONOMBRE) = vbRightJustify
        .TextMatrix(0, colHITO_DESCRIPCION) = "Descripci�n": .ColWidth(colHITO_DESCRIPCION) = 3000: .ColAlignment(colHITO_DESCRIPCION) = vbRightJustify
        .TextMatrix(0, colHITO_ESTADO) = "hito_estado": .ColWidth(colHITO_ESTADO) = 0
        .TextMatrix(0, colHITO_ESTADONOMBRE) = "Estado": .ColWidth(colHITO_ESTADONOMBRE) = 1600
        .TextMatrix(0, colHITO_NIVEL) = "Nivel": .ColWidth(colHITO_NIVEL) = 0
        .TextMatrix(0, colHITO_AREA) = "�rea": .ColWidth(colHITO_AREA) = 0
        .TextMatrix(0, colHITO_AREANOM) = "Nombre de �rea responsable": .ColWidth(colHITO_AREANOM) = 0
        .TextMatrix(0, colHITO_USUARIO) = "Usuario": .ColWidth(colHITO_USUARIO) = 1000
        .TextMatrix(0, colHITO_FECHA) = "F. Modif.": .ColWidth(colHITO_FECHA) = 1200
        .TextMatrix(0, colHITO_DEPAREA) = "dep_area": .ColWidth(colHITO_DEPAREA) = 0
        .TextMatrix(0, colHITO_HSTNROINTERNO) = "hst_nrointerno": .ColWidth(colHITO_HSTNROINTERNO) = 0
        .TextMatrix(0, colHITO_HITNROINTERNO) = "N� interno": .ColWidth(colHITO_HITNROINTERNO) = 1000
        .TextMatrix(0, colHITO_HITORDEN) = "Orden": .ColWidth(colHITO_HITORDEN) = 600: .ColAlignment(colHITO_HITORDEN) = flexAlignRightCenter
        .TextMatrix(0, colHITO_ORDENREAL) = "Ordenreal": .ColWidth(colHITO_ORDENREAL) = 0
        '{ add -002- a.
        .TextMatrix(0, colHITO_EXPOSICION) = "Expos.": .ColWidth(colHITO_EXPOSICION) = 800: .ColAlignment(colHITO_EXPOSICION) = flexAlignLeftCenter
        .TextMatrix(0, colHITO_CLASE) = "Clase": .ColWidth(colHITO_CLASE) = 0: .ColAlignment(colHITO_CLASE) = flexAlignLeftCenter
        .TextMatrix(0, colHITO_CLASENOMBRE) = "Nombre de clase": .ColWidth(colHITO_CLASENOMBRE) = 0: .ColAlignment(colHITO_CLASENOMBRE) = flexAlignLeftCenter
        .TextMatrix(0, colHITO_CLASEEXPOS) = "Expos de clase": .ColWidth(colHITO_CLASEEXPOS) = 0: .ColAlignment(colHITO_CLASEEXPOS) = flexAlignLeftCenter
        '}
        Call CambiarEfectoLinea(grdHitos, prmGridEffectFontBold)
        .RowSel = .Rows - 1: .ColSel = .cols - 1    ' Selecciona la primera fila de todas
        '.HighLight = flexHighlightWithFocus
        .HighLight = flexHighlightAlways
    End With
End Sub

Private Sub Inicializar_GrillaResponsables()
    With grdAreasResponsables
        .visible = False
        .Clear
        .Rows = 1
        .cols = 4 + 1
        .TextMatrix(0, colRESP_AREARESPONSABLE) = "�rea": .ColWidth(colRESP_AREARESPONSABLE) = 9000
        .TextMatrix(0, colRESP_NOMRESPONSABLE) = "Responsable": .ColWidth(colRESP_NOMRESPONSABLE) = 2000
        .TextMatrix(0, colRESP_CODNIVEL) = "cod_nivel": .ColWidth(colRESP_CODNIVEL) = 0
        .TextMatrix(0, colRESP_CODAREA) = "cod_area": .ColWidth(colRESP_CODAREA) = 0
        .TextMatrix(0, colRESP_DEPAREA) = "dep_area": .ColWidth(colRESP_DEPAREA) = 0
        Call CambiarEfectoLinea(grdAreasResponsables, prmGridEffectFontBold)
        .RowSel = .Rows - 1: .ColSel = .cols - 1    ' Selecciona la primera fila de todas
        .HighLight = flexHighlightWithFocus
    End With
End Sub

Private Sub Inicializar_GrillaGrupos()
    With grdProyectoGrupos
        .visible = False
        .Clear
        .Rows = 1
        .cols = 6 + 1
        .TextMatrix(0, colGRUPOS_CODSECTOR) = "cod_sector": .ColWidth(colGRUPOS_CODSECTOR) = 0
        .TextMatrix(0, colGRUPOS_NOMSECTOR) = "Sector": .ColWidth(colGRUPOS_NOMSECTOR) = 3500
        .TextMatrix(0, colGRUPOS_CODGRUPO) = "cod_grupo": .ColWidth(colGRUPOS_CODGRUPO) = 0
        .TextMatrix(0, colGRUPOS_NOMGRUPO) = "Grupo": .ColWidth(colGRUPOS_NOMGRUPO) = 3500
        .TextMatrix(0, colGRUPOS_NOMESTADOGRUPO) = "Estado": .ColWidth(colGRUPOS_NOMESTADOGRUPO) = 2000
        .TextMatrix(0, colGRUPOS_PETNROASIGNADO) = "Pet. N�": .ColWidth(colGRUPOS_PETNROASIGNADO) = 1000
        .TextMatrix(0, colGRUPOS_PETNROINTERNO) = "pet_nrointerno": .ColWidth(colGRUPOS_PETNROINTERNO) = 0
        Call CambiarEfectoLinea(grdProyectoGrupos, prmGridEffectFontBold)
        .RowSel = .Rows - 1: .ColSel = .cols - 1        ' Selecciona la primera fila de todas
        .HighLight = flexHighlightWithFocus
        .MergeCells = flexMergeFree
        .MergeCol(1) = True     ' Nombre sector
        .MergeCol(3) = True     ' Nombre grupo
        .MergeCol(4) = True     ' Nombre estado
    End With
End Sub

Private Sub Inicializar_GrillaAdjuntos()
    With grdAdjuntos
        .visible = False
        .Clear
        .Rows = 1
        .cols = 12 + 1
        .TextMatrix(0, colADJUNTOS_ProjId) = "ProjId": .ColWidth(colADJUNTOS_ProjId) = 0
        .TextMatrix(0, colADJUNTOS_ProjSubId) = "ProjSubId": .ColWidth(colADJUNTOS_ProjSubId) = 0
        .TextMatrix(0, colADJUNTOS_ProjSubSId) = "ProjSubSId": .ColWidth(colADJUNTOS_ProjSubSId) = 0
        .TextMatrix(0, colADJUNTOS_adj_tipo) = "adj_tipo": .ColWidth(colADJUNTOS_adj_tipo) = 0
        .TextMatrix(0, colADJUNTOS_adj_file) = "Nombre del archivo": .ColWidth(colADJUNTOS_adj_file) = 2500: .ColAlignment(colADJUNTOS_adj_file) = flexAlignLeftCenter
        .TextMatrix(0, colADJUNTOS_adj_texto) = "Descripci�n": .ColWidth(colADJUNTOS_adj_texto) = 3000: .ColAlignment(colADJUNTOS_adj_texto) = flexAlignLeftCenter
        .TextMatrix(0, colADJUNTOS_adj_objeto) = "adj_objeto": .ColWidth(colADJUNTOS_adj_objeto) = 0
        .TextMatrix(0, colADJUNTOS_audit_user) = "audit_user": .ColWidth(colADJUNTOS_audit_user) = 0
        .TextMatrix(0, colADJUNTOS_nom_user) = "Adjuntado por": .ColWidth(colADJUNTOS_nom_user) = 2500: .ColAlignment(colADJUNTOS_nom_user) = flexAlignLeftCenter
        .TextMatrix(0, colADJUNTOS_audit_date) = "Fecha": .ColWidth(colADJUNTOS_audit_date) = 0
        .TextMatrix(0, colADJUNTOS_cod_nivel) = "cod_nivel": .ColWidth(colADJUNTOS_cod_nivel) = 0
        .TextMatrix(0, colADJUNTOS_cod_area) = "cod_area": .ColWidth(colADJUNTOS_cod_area) = 0
        .TextMatrix(0, colADJUNTOS_nom_area) = "�rea": .ColWidth(colADJUNTOS_nom_area) = 3500: .ColAlignment(colADJUNTOS_nom_area) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdAdjuntos, prmGridEffectFontBold)
        .RowSel = .Rows - 1: .ColSel = .cols - 1        ' Selecciona la primera fila de todas
        .HighLight = flexHighlightWithFocus
    End With
End Sub

Private Sub Inicializar_GrillaHoras()
    With grdHoras
        .visible = False
        .Clear
        .Rows = 1
        .cols = colHORAS_TOTCOLS
        .TextMatrix(0, colHORAS_cod_nivel) = "cod_nivel": .ColWidth(colHORAS_cod_nivel) = 0: .ColAlignment(colHORAS_cod_nivel) = flexAlignLeftCenter
        .TextMatrix(0, colHORAS_nom_nivel) = "Nivel": .ColWidth(colHORAS_nom_nivel) = 2500: .ColAlignment(colHORAS_nom_nivel) = flexAlignLeftCenter
        .TextMatrix(0, colHORAS_cod_area) = "C�digo": .ColWidth(colHORAS_cod_area) = 1200: .ColAlignment(colHORAS_cod_area) = flexAlignLeftCenter
        .TextMatrix(0, colHORAS_nom_area) = "Nombre de �rea": .ColWidth(colHORAS_nom_area) = 3000: .ColAlignment(colHORAS_nom_area) = flexAlignLeftCenter
        .TextMatrix(0, colHORAS_cant_horas) = "Horas": .ColWidth(colHORAS_cant_horas) = 1600: .ColAlignment(colHORAS_cant_horas) = flexAlignRightCenter
        Call CambiarEfectoLinea(grdHoras, prmGridEffectFontBold)
        .RowSel = .Rows - 1: .ColSel = .cols - 1        ' Selecciona la primera fila de todas
        .HighLight = flexHighlightWithFocus
        '.FocusRect = flexFocusHeavy
        .MergeCells = flexMergeFree
        .MergeCol(1) = True
    End With
End Sub

Private Sub Cargar_GrillaHoras()
    bLoading = True
    With grdHoras
        Call Inicializar_GrillaHoras
        If sp_GetProyectoIDMHoras(l_ProjId, l_ProjSubId, l_ProjSubsId) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colHORAS_cod_nivel) = ClearNull(aplRST.Fields!cod_nivel)
                .TextMatrix(.Rows - 1, colHORAS_nom_nivel) = ClearNull(aplRST.Fields!nom_nivel)
                .TextMatrix(.Rows - 1, colHORAS_cod_area) = ClearNull(aplRST.Fields!cod_area)
                .TextMatrix(.Rows - 1, colHORAS_nom_area) = ClearNull(aplRST.Fields!nom_area)
                .TextMatrix(.Rows - 1, colHORAS_cant_horas) = ClearNull(aplRST.Fields!cant_horas)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
    bLoading = False
End Sub

Private Sub Cargar_GrillaAlertas()
    bLoading = True
    With grdAlertas
        Call Inicializar_GrillaAlertas
        If spProyectoIDM.sp_GetProyectoIDMAlertas(l_ProjId, l_ProjSubId, l_ProjSubsId) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                If ClearNull(aplRST.Fields!alert_status) <> "S" Then
                    Select Case ClearNull(aplRST.Fields!alert_tipo)
                        Case "R": PintarLinea grdAlertas, prmGridFillRowColorRed
                        Case "A": PintarLinea grdAlertas, prmGridFillRowColorYellow
                    End Select
                End If
                .TextMatrix(.Rows - 1, colALERTA_TIPO) = ClearNull(aplRST.Fields!alert_tipo)
                .TextMatrix(.Rows - 1, colALERTA_DESCRIPCION) = ClearNull(aplRST.Fields!alert_desc)
                .TextMatrix(.Rows - 1, colALERTA_ACCIONCORRECTIVA) = ClearNull(aplRST.Fields!accion_id)
                .TextMatrix(.Rows - 1, colALERTA_INICIO) = ClearNull(aplRST.Fields!alert_fe_ini)
                .TextMatrix(.Rows - 1, colALERTA_FIN) = ClearNull(aplRST.Fields!alert_fe_fin)
                .TextMatrix(.Rows - 1, colALERTA_RESPONSABLES) = ClearNull(aplRST.Fields!responsables)
                .TextMatrix(.Rows - 1, colALERTA_OBSERVACIONES) = ClearNull(aplRST.Fields!observaciones)
                .TextMatrix(.Rows - 1, colALERTA_ITEM) = ClearNull(aplRST.Fields!alert_itm)
                .TextMatrix(.Rows - 1, colALERTA_STATUS) = ClearNull(aplRST.Fields!alert_status)
                .TextMatrix(.Rows - 1, colALERTA_STATUSDSC) = ClearNull(aplRST.Fields!alert_stadsc)
                .TextMatrix(.Rows - 1, colALERTA_FECRESOLUCION) = ClearNull(aplRST.Fields!alert_fecres)
                .TextMatrix(.Rows - 1, colALERTA_USUARIO) = ClearNull(aplRST.Fields!alert_usuario)
                .TextMatrix(.Rows - 1, colALERTA_FECHA) = Format(ClearNull(aplRST.Fields!alert_fecha), "dd/mm/yyyy")
                If ClearNull(aplRST.Fields!alert_status) = "S" Then
                    CambiarForeColorLinea grdAlertas, prmGridFillRowColorDarkGrey
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
    bLoading = False
End Sub

Private Sub Cargar_GrillaHitos()
    bLoading = True
    With grdHitos
        Call Inicializar_GrillaHitos
        If sp_GetProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, Null, Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colHITO_HITONOMBRE) = ClearNull(aplRST.Fields!hito_nombre)
                .TextMatrix(.Rows - 1, colHITO_INICIO) = Format(ClearNull(aplRST.Fields!hito_fe_ini), "dd/mm/yyyy")
                .TextMatrix(.Rows - 1, colHITO_FIN) = Format(ClearNull(aplRST.Fields!hito_fe_fin), "dd/mm/yyyy")
                .TextMatrix(.Rows - 1, colHITO_DESCRIPCION) = ClearNull(aplRST.Fields!hito_descripcion)
                .TextMatrix(.Rows - 1, colHITO_ESTADO) = ClearNull(aplRST.Fields!hito_estado)
                .TextMatrix(.Rows - 1, colHITO_ESTADONOMBRE) = ClearNull(aplRST.Fields!nom_estado)
                .TextMatrix(.Rows - 1, colHITO_NIVEL) = ClearNull(aplRST.Fields!cod_nivel)
                .TextMatrix(.Rows - 1, colHITO_AREA) = ClearNull(aplRST.Fields!cod_area)
                .TextMatrix(.Rows - 1, colHITO_AREANOM) = ClearNull(aplRST.Fields!nom_area)
                .TextMatrix(.Rows - 1, colHITO_USUARIO) = ClearNull(aplRST.Fields!hito_usuario)
                .TextMatrix(.Rows - 1, colHITO_FECHA) = Format(ClearNull(aplRST.Fields!hito_fecha), "dd/mm/yyyy")
                .TextMatrix(.Rows - 1, colHITO_DEPAREA) = ClearNull(aplRST.Fields!dep_area)
                .TextMatrix(.Rows - 1, colHITO_HSTNROINTERNO) = ClearNull(aplRST.Fields!hst_nrointerno)
                .TextMatrix(.Rows - 1, colHITO_HITNROINTERNO) = ClearNull(aplRST.Fields!hit_nrointerno)
                .TextMatrix(.Rows - 1, colHITO_HITORDEN) = ClearNull(aplRST.Fields!hit_orden)
                .TextMatrix(.Rows - 1, colHITO_ORDENREAL) = ClearNull(aplRST.Fields!orden)
                '{ add -002- a.
                .TextMatrix(.Rows - 1, colHITO_EXPOSICION) = ClearNull(aplRST.Fields!hito_expos)
                .TextMatrix(.Rows - 1, colHITO_CLASE) = ClearNull(aplRST.Fields!hito_id)
                .TextMatrix(.Rows - 1, colHITO_CLASENOMBRE) = ClearNull(aplRST.Fields!hitodesc)
                .TextMatrix(.Rows - 1, colHITO_CLASEEXPOS) = ClearNull(aplRST.Fields!expos_clase)
                '}
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
    bLoading = False
End Sub

Private Sub Cargar_GrillaResponsables()
    bLoading = True
    With grdAreasResponsables
        Call Inicializar_GrillaResponsables
        If sp_GetProyectoIDMResponsables(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colRESP_AREARESPONSABLE) = ClearNull(aplRST.Fields!nom_area)
                .TextMatrix(.Rows - 1, colRESP_NOMRESPONSABLE) = ClearNull(aplRST.Fields!nom_responsable)
                .TextMatrix(.Rows - 1, colRESP_CODNIVEL) = ClearNull(aplRST.Fields!cod_nivel)
                .TextMatrix(.Rows - 1, colRESP_CODAREA) = ClearNull(aplRST.Fields!cod_area)
                .TextMatrix(.Rows - 1, colRESP_DEPAREA) = ClearNull(aplRST.Fields!dep_area)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
    bLoading = False
End Sub

Private Sub Cargar_GrillaGrupos()
    bLoading = True
    With grdProyectoGrupos
        Call Inicializar_GrillaGrupos
        If sp_GetProyectoIDMGrupos(l_ProjId, l_ProjSubId, l_ProjSubsId) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colGRUPOS_CODSECTOR) = ClearNull(aplRST.Fields!cod_sector)
                .TextMatrix(.Rows - 1, colGRUPOS_NOMSECTOR) = ClearNull(aplRST.Fields!nom_sector)
                .TextMatrix(.Rows - 1, colGRUPOS_CODGRUPO) = ClearNull(aplRST.Fields!cod_grupo)
                .TextMatrix(.Rows - 1, colGRUPOS_NOMGRUPO) = ClearNull(aplRST.Fields!nom_grupo)
                .TextMatrix(.Rows - 1, colGRUPOS_NOMESTADOGRUPO) = ClearNull(aplRST.Fields!nom_estado)
                .TextMatrix(.Rows - 1, colGRUPOS_PETNROASIGNADO) = ClearNull(aplRST.Fields!pet_nroasignado)
                .TextMatrix(.Rows - 1, colGRUPOS_PETNROINTERNO) = ClearNull(aplRST.Fields!pet_nrointerno)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
    bLoading = False
End Sub

Private Sub Cargar_GrillaAdjuntos()
    bLoading = True
    With grdAdjuntos
        Inicializar_GrillaAdjuntos
        If sp_GetProyectoIDMAdjuntos(l_ProjId, l_ProjSubId, l_ProjSubsId, Null, Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colADJUNTOS_ProjId) = ClearNull(aplRST.Fields!ProjId)
                .TextMatrix(.Rows - 1, colADJUNTOS_ProjSubId) = ClearNull(aplRST.Fields!ProjSubId)
                .TextMatrix(.Rows - 1, colADJUNTOS_ProjSubSId) = ClearNull(aplRST.Fields!ProjSubSId)
                .TextMatrix(.Rows - 1, colADJUNTOS_adj_tipo) = ClearNull(aplRST.Fields!adj_tipo)
                .TextMatrix(.Rows - 1, colADJUNTOS_adj_file) = ClearNull(aplRST.Fields!adj_file)
                .TextMatrix(.Rows - 1, colADJUNTOS_adj_texto) = ClearNull(aplRST.Fields!adj_texto)
                .TextMatrix(.Rows - 1, colADJUNTOS_audit_user) = ClearNull(aplRST.Fields!audit_user)
                .TextMatrix(.Rows - 1, colADJUNTOS_nom_user) = ClearNull(aplRST.Fields!nom_user)
                .TextMatrix(.Rows - 1, colADJUNTOS_audit_date) = ClearNull(aplRST.Fields!audit_date)
                .TextMatrix(.Rows - 1, colADJUNTOS_cod_nivel) = ClearNull(aplRST.Fields!cod_nivel)
                .TextMatrix(.Rows - 1, colADJUNTOS_cod_area) = ClearNull(aplRST.Fields!cod_area)
                .TextMatrix(.Rows - 1, colADJUNTOS_nom_area) = ClearNull(aplRST.Fields!nom_area)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
    End With
    bLoading = False
End Sub

Private Sub cmdAgregarNovedades_Click()
    With frmProyectosIDMShellNovedades
        .lProjId = l_ProjId
        .lProjSubId = l_ProjSubId
        .lProjSubSId = l_ProjSubsId
        .Show 1
    End With
    Cargar_Novedades
End Sub

Private Sub cmdEditar_Click()
    Call HabilitarBotones(AGREGAR)
End Sub

Private Sub cmdCancelar_Click()
    Call HabilitarBotones(STANDBY)
    Call Cargar_Proyecto
End Sub

Private Sub cmdGuardar_Click()
    Call Guardar_Proyecto
End Sub

Private Sub Guardar_Proyecto()
    Dim cod_direccion_soli As String
    Dim cod_gerencia_soli As String
    Dim cod_sector_soli As String
    
    Dim cod_direccion_prop As String
    Dim cod_gerencia_prop As String
    Dim cod_sector_prop As String
    Dim cod_grupo_prop As String
    
    Dim cNuevoEstadoProyecto As String
    
    'bGuardando = True
    
    If CamposObligatorios Then
        cNombreProyecto = Trim(txtNombreProyecto.Text)
        cCodigoGPS = Trim(txtCodigoGPS.Text)
        If MsgBox("�Confirma guardar los datos modificados?", vbQuestion + vbYesNo) = vbYes Then
            Call Puntero(True)
            bLoading = True
            Call ObtenerAreasCombo(cmbSolicitante, cod_direccion_soli, cod_gerencia_soli, cod_sector_soli, "")
            Call ObtenerAreasCombo(cmbPropietario, cod_direccion_prop, cod_gerencia_prop, cod_sector_prop, cod_grupo_prop)
            ' Datos principales en ProyectoIDM
            Call sp_UpdateProyectoIDM(l_ProjId, l_ProjSubId, l_ProjSubsId, _
                                    cNombreProyecto, _
                                    CodigoCombo(cmbCategoria, False), _
                                    CodigoCombo(cmbClase, False), _
                                    CodigoCombo(cmbPlanTyO, True), _
                                    CodigoCombo(cmbEmpresa, True), _
                                    CodigoCombo(cmbArea, True), _
                                    CodigoCombo(cmbClas3, True), _
                                    Null, _
                                    IIf(cod_direccion_soli = "", Null, cod_direccion_soli), IIf(cod_gerencia_soli = "", Null, cod_gerencia_soli), IIf(cod_sector_soli = "", Null, cod_sector_soli), _
                                    IIf(cod_direccion_prop = "", Null, cod_direccion_prop), IIf(cod_gerencia_prop = "", Null, cod_gerencia_prop), IIf(cod_sector_prop = "", Null, cod_sector_prop), IIf(cod_grupo_prop = "", Null, cod_grupo_prop), _
                                    txtAvance, _
                                    txtInicio, _
                                    txtFinPrevisto, _
                                    txtFinReprogramado, Null, CodigoCombo(cmbTipoProyecto, True), txtHsPlanifGer, txtHsEstimaSec, txtReplanif, cCodigoGPS)
            ' Estados
            Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "COD_ESTADO", CodigoCombo(cmbEstado, True), Null, Null)
            If CodigoCombo(cmbEstado, True) <> "SUSPEN" Then
                Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "COD_ESTADO2", CodigoCombo(cmbSubestado, True), Null, Null)
            End If
            If CodigoCombo(cmbEstado, True) = "APROBA" And CodigoCombo(cmbTipoProyecto, True) = "I" Then
                Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "TIPO_PROYECTO", "P", Null, Null)
                Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGTIPO", CodigoCombo(cmbEstado, True), "Valor anterior: " & TextoCombo(cmbTipoProyecto, cmbTipoProyecto.Tag, True), glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
            End If
            ' Recalcula el valor del sem�foro
            If chkRecalcularSemaforo.Value = 1 Then
                Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "SEMAFORO", RecalcularSemaforo, Null, Null)
            Else
                'Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, IIf(bSemaforoManual, "SEMAFORO_MANUAL", "SEMAFORO"), CStr(vscSemaforo.Value), Null, Null)
                Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, IIf(bSemaforoManual, "SEMAFORO_MANUAL", "SEMAFORO"), CStr(udSemaforo.Value), Null, Null)        ' new
            End If
            Call sp_UpdateProyectoIDMMemo(l_ProjId, l_ProjSubId, l_ProjSubsId, "DESCRIPCIO", ClearNull(txtDescripcion))   ' General - Descripci�n
            Call sp_UpdateProyectoIDMMemo(l_ProjId, l_ProjSubId, l_ProjSubsId, "BENEFICIOS", ClearNull(txtBeneficios))    ' General - Beneficios
            ' Guardar en el historial los cambios
            Call Guardar_Historial      ' 10.06.2016 - Ac� est� el error Method '~' of Object '~' Failed
            Call Cargar_Proyecto        ' Se actualizan todos los controles luego de la edici�n
            Call Puntero(False)
            Call HabilitarBotones(STANDBY)
            bLoading = False
            bActualizo = True
        End If
    End If
    'bGuardando = False
End Sub

Private Sub Guardar_Historial()
    Dim sTextoHistorial As String
    Dim sEstado1 As String
    Dim sEstado2 As String
    Dim sEstado As String
    Dim sTextoCambioCategoria As String
    Dim sTextoCambioClase As String
    
    
    sEstado = CodigoCombo(cmbEstado, True)
'    sTextoCambioClase = TextoCombo(cmbClase, cmbClase.Tag, True)
'    sTextoCambioCategoria = TextoCombo(cmbCategoria, cmbCategoria.Tag, True)
    
    If txtNombreProyecto.Tag <> txtNombreProyecto Then
        If txtNombreProyecto.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & txtNombreProyecto.Tag
            Call Status("Actualizando 1")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGTIT", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
'    If cmbCategoria.Tag <> CodigoCombo(cmbCategoria, False) Then
'        If cmbCategoria.Tag <> "" And cmbCategoria.Tag <> "0" Then
'            'sTextoHistorial = "Valor anterior: " & TextoCombo(cmbCategoria, cmbCategoria.Tag, True)
'            sTextoHistorial = "Valor anterior: " & sTextoCambioCategoria
'            Call Status("Actualizando 2")
'            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGCAT", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
'        End If
'    End If
'    If cmbClase.Tag <> CodigoCombo(cmbClase, False) Then
'        If cmbClase.Tag <> "" And cmbClase.Tag <> "0" Then
'            'sTextoHistorial = "Valor anterior: " & TextoCombo(cmbClase, cmbClase.Tag, True)
'            sTextoHistorial = "Valor anterior: " & sTextoCambioClase
'            Call Status("Actualizando 3")
'            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGCLASE", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
'        End If
'    End If
    If cmbSolicitante.Tag <> CodigoCombo(cmbSolicitante, True) Then
        If cmbSolicitante.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbSolicitante, cmbSolicitante.Tag, True)
            Call Status("Actualizando 4")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGSOL", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If cmbPropietario.Tag <> CodigoCombo(cmbPropietario, True) Then
        If cmbPropietario.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbPropietario, cmbPropietario.Tag, True)
            Call Status("Actualizando 5")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGPROP", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If cmbPlanTyO.Tag <> CodigoCombo(cmbPlanTyO, True) Then
        If cmbPlanTyO.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbPlanTyO, cmbPlanTyO.Tag, True)
            Call Status("Actualizando 6")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGTYO", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If txtInicio.Tag <> txtInicio Then
        If txtInicio.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & txtInicio.Tag
            Call Status("Actualizando 7")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGFINI", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If txtFinPrevisto.Tag <> txtFinPrevisto Then
        If txtFinPrevisto.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & txtFinPrevisto.Tag
            Call Status("Actualizando 8")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGFFIN", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If txtFinReprogramado.Tag <> txtFinReprogramado Then
        If txtFinReprogramado.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & txtFinReprogramado.Tag
            Call Status("Actualizando 9")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGFREP", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
        If txtFinReprogramado <> "" Then
            Call Status("Actualizando 10")
            Call sp_UpdateProyectoIDMField(l_ProjId, l_ProjSubId, l_ProjSubsId, "totalreplan", Null, Null, Val(txtReplanif) + 1)
        End If
    End If
    If cmbEmpresa.Tag <> CodigoCombo(cmbEmpresa, True) Then
        If cmbEmpresa.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbEmpresa, cmbEmpresa.Tag, True)
            Call Status("Actualizando 11")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, CDate(Now), "CHGEMP", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If cmbArea.Tag <> CodigoCombo(cmbArea, True) Then
        If cmbArea.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbArea, cmbArea.Tag, True)
            Call Status("Actualizando 12")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGAREA", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If cmbClas3.Tag <> CodigoCombo(cmbClas3, True) Then
        If cmbClas3.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbClas3, cmbClas3.Tag, True)
            Call Status("Actualizando 13")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGCLASI", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If cmbTipoProyecto.Tag <> CodigoCombo(cmbTipoProyecto, True) Then
        If cmbTipoProyecto.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbTipoProyecto, cmbTipoProyecto.Tag, True)
            Call Status("Actualizando 14")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGTIPO", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If CodigoCombo2(cmbEstado.Tag, True) <> CodigoCombo(cmbEstado, True) Then
        bLoading = True
        sEstado1 = CodigoCombo2(cmbEstado.Tag, True)        ' Estado anterior
        sEstado2 = CodigoCombo(cmbEstado, True)             ' Estado actual
        If cmbEstado.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & TextoCombo(cmbEstado, sEstado1, True)
            Call Status("Actualizando 15")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGEST", sEstado2, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
        bLoading = False
    End If
    If CodigoCombo(cmbEstado, True) = "EJECUC" Then
        If CodigoCombo(cmbSubestado, True) <> "" And cmbSubestado.Tag <> CodigoCombo(cmbSubestado, True) Then
            If cmbSubestado.Tag <> "" Then
                sTextoHistorial = "Valor anterior: " & TextoCombo(cmbSubestado, cmbSubestado.Tag, True)
                Call Status("Actualizando 16")
                Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGSEST", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
            End If
        End If
    End If
    If txtHsPlanifGer.Tag <> txtHsPlanifGer Then
        If txtHsPlanifGer.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & txtHsPlanifGer.Tag
            Call Status("Actualizando 17")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGHGER", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If txtHsEstimaSec.Tag <> txtHsEstimaSec Then
        If txtHsEstimaSec.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & txtHsEstimaSec.Tag
            Call Status("Actualizando 18")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGHSEC", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
    If txtCodigoGPS.Tag <> txtCodigoGPS Then
        If txtCodigoGPS.Tag <> "" Then
            sTextoHistorial = "Valor anterior: " & txtCodigoGPS.Tag
            Call Status("Actualizando 19")
            Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGCGPS", sEstado, sTextoHistorial, glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
        End If
    End If
'    If txtReplanif.Tag <> txtReplanif Then
'        sTextoHistorial = ""
'        Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "", CodigoCombo(cmbEstado, True), "", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
'    End If
'    If shpSemaforo.Tag <> shpSemaforo Then
'        sTextoHistorial = ""
'        Call sp_InsertProyectoIDMHisto(l_ProjId, l_ProjSubId, l_ProjSubsId, Now, "CHGSEM", CodigoCombo(cmbEstado, True), "", glLOGIN_ID_REEMPLAZO, glLOGIN_ID_REAL)
'    End If
End Sub

'Private Function RecalcularEstado() As String       ' Recalcula el estado del proyecto
'    Dim cEstadosPosibles As String
'    Dim cNuevoEstadoProyecto As String
'
'    cEstadosPosibles = "PLANIF|DESEST|EJECUC|PRUEBA|TERMIN|SUSPEN|"
'    RecalcularEstado = "PLANIF"
'End Function

'Private Function RecalcularEstado() As String       ' Recalcula el estado del proyecto
'    Dim cEstadosPosibles As String
'    Dim cNuevoEstadoProyecto As String
'
'    cEstadosPosibles = "PLANIF|DESEST|EJECUC|PRUEBA|TERMIN|SUSPEN|"
'
'    cNuevoEstadoProyecto = sp_GetEstadoProyectoIDM(l_ProjId, l_ProjSubId, l_ProjSubsId)
'    If ClearNull(cNuevoEstadoProyecto) <> "" Then
'        If InStr(1, cEstadosPosibles, cNuevoEstadoProyecto, vbTextCompare) > 0 Then
'            RecalcularEstado = cNuevoEstadoProyecto
'            'Call SetCombo(cmbEstado, cNuevoEstadoProyecto, True)
'        End If
'    End If
'End Function

Private Function RecalcularSemaforo() As Integer    ' Recalcula el valor del sem�foro
    If sp_GetProyectoIDMAlertaMayor(l_ProjId, l_ProjSubId, l_ProjSubsId) Then
        Select Case ClearNull(aplRST.Fields!alert_tipo)
            Case "R": RecalcularSemaforo = COLOR_SEMAF_ROJO
            Case "A": RecalcularSemaforo = COLOR_SEMAF_AMARILLO
            Case Else
                RecalcularSemaforo = COLOR_SEMAF_VERDE
        End Select
    End If
End Function

Private Function CamposObligatorios(Optional bModoSilencioso As Boolean) As Boolean
    On Error GoTo Errores
    
    CamposObligatorios = True
    
    If Not Trim(txtCodigoGPS.Text) = "" Then
        If Mid(txtCodigoGPS.Text, 1, 3) <> "GB." Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "El c�digo GPS debe comenzar con GB. Ej.: GB.00009742"
                txtCodigoGPS.SetFocus
            End If
            Exit Function
        Else
            If Not IsNumeric(Mid(txtCodigoGPS.Text, 4, 8)) Then
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "Luego de GB. debe agregar un n�mero. Ej.: GB.00009742"
                    txtCodigoGPS.SetFocus
                End If
                Exit Function
            Else
                If Len(Mid(txtCodigoGPS.Text, 4, 8)) < 8 Then       ' Completo con ceros
                    txtCodigoGPS.Text = "GB." & Format(Mid(txtCodigoGPS.Text, 4, 8), "00000000")
                End If
            End If
        End If
    End If
'    If Trim(txtCodigoGPS.Text) = "" Then
'        CamposObligatorios = False
'        If Not bModoSilencioso Then
'            MensajeError "Debe ingresar el c�digo GPS. Ej.: GB.00009742"
'            txtCodigoGPS.SetFocus
'        End If
'        Exit Function
'    Else
    
    If Not InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
        If Trim(txtNombreProyecto.Text) = "" Then           ' Nombre del proyecto
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe ingresar el nombre del proyecto."
                txtNombreProyecto.SetFocus
            End If
            Exit Function
        End If
        If cmbCategoria.ListIndex = -1 Then                 ' Categor�a de proyecto
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar una categor�a para el proyecto."
                cmbCategoria.SetFocus
            End If
            Exit Function
        End If
        If cmbClase.ListIndex = -1 Then                     ' Clase de proyecto
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar una clase para el proyecto."
                cmbClase.SetFocus
            End If
            Exit Function
        End If
        If cmbSolicitante.ListIndex = -1 Then               ' Solicitante: obligatorio
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar el �rea solicitante del proyecto."
                cmbSolicitante.SetFocus
            End If
            Exit Function
        End If
        If cmbPropietario.ListIndex = -1 Then               ' Propietario: obligatorio
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar el �rea propietaria del proyecto."
                cmbPropietario.SetFocus
            End If
            Exit Function
        End If
        If cmbPlanTyO.ListIndex = -1 Then                   ' Plan TYO
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar Plan TyO."
                cmbPlanTyO.SetFocus
            End If
            Exit Function
        End If
        If cmbEmpresa.ListIndex = -1 Then                   ' Clasificaci�n 1 (Empresa)
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar la empresa."
                cmbEmpresa.SetFocus
            End If
            Exit Function
        End If
        If cmbArea.ListIndex = -1 Then                      ' Clasificaci�n 2 (Area)
            CamposObligatorios = False
            MensajeError "Debe seleccionar el �rea."
            cmbArea.SetFocus
            Exit Function
        End If
        If cmbClas3.ListIndex = -1 Then                     ' Clasificaci�n 3
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar la clasificaci�n."
                cmbClas3.SetFocus
            End If
            Exit Function
        End If
        If ClearNull(txtDescripcion.Text) = "" Then         ' Descripci�n: obligatoria
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe ingresar una descripci�n."
                txtDescripcion.SetFocus
            End If
            Exit Function
        End If
        If ClearNull(txtBeneficios.Text) = "" Then          ' Beneficios: obligatorio
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe ingresar los beneficios."
                txtBeneficios.SetFocus
            End If
            Exit Function
        End If
        ' Estado y subestado del proyecto
        If cmbEstado.ListIndex = -1 Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar el estado del proyecto."
                cmbEstado.SetFocus
            End If
            Exit Function
        End If
        Select Case CodigoCombo(cmbEstado, True)
            Case "EJECUC"
                If cmbSubestado.ListIndex = -1 Then
                    CamposObligatorios = False
                    If Not bModoSilencioso Then
                        MensajeError "Debe seleccionar el subestado del proyecto."
                        cmbSubestado.SetFocus
                    End If
                    Exit Function
                End If
            Case "PLANOK"
                If Not sp_GetProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, "Fecha de entrega al usuario", Null, Null) Then
                    CamposObligatorios = False
                    If Not bModoSilencioso Then
                        MensajeError "Debe existir un hito seleccionado que indica -Fecha de entrega al usuario-" & vbCrLf & "para poder cambiar el estado a Planificado."
                    End If
                    Exit Function
                End If
            Case Else
                cmbSubestado.ListIndex = -1
        End Select
        ' Fecha de inicio
        If IsDate(txtInicio.DateValue) Then
            If IsDate(txtFinPrevisto.DateValue) Then
                If txtInicio.DateValue > txtFinPrevisto.DateValue Then
                    CamposObligatorios = False
                    If Not bModoSilencioso Then
                        MensajeError "La fecha de inicio debe ser menor o igual a la fecha de fin."
                    End If
                    Exit Function
                End If
            Else
                If IsDate(txtFinReprogramado.DateValue) Then
                    If txtInicio.DateValue > txtFinReprogramado.DateValue Then
                        CamposObligatorios = False
                        If Not bModoSilencioso Then
                            MensajeError "La fecha de inicio debe ser menor o igual a la fecha de reprogramaci�n."
                        End If
                        Exit Function
                    End If
                End If
            End If
        End If
        ' Fecha de fin estimado
        If IsDate(txtFinPrevisto.DateValue) Then
            If Not IsDate(txtInicio.DateValue) Then     ' Si se carga fecha de fin estimado, debe tener fecha de inicio
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "Debe ingresar la fecha de inicio."
                    txtInicio.SetFocus
                End If
                Exit Function
            End If
            If Not txtFinPrevisto.DateValue >= txtInicio.DateValue Then
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "La fecha fin debe ser mayor o igual a la fecha de inicio."
                    txtFinPrevisto.SetFocus
                End If
                Exit Function
            End If
        End If
        ' Fecha de fin reprogramada
        If IsDate(txtFinReprogramado.DateValue) Then
            If Not txtFinReprogramado.DateValue > txtInicio.DateValue Then
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "La fecha fin reprogramada debe ser mayor a la fecha de inicio."
                    txtFinReprogramado.SetFocus
                End If
                Exit Function
            Else
                If txtFinReprogramado.Tag = "" Then
                    If InStr(1, "DESEST|TERMIN|", CodigoCombo(cmbEstado, True), vbTextCompare) = 0 Then     ' add -001- a.
                        If date > txtFinReprogramado.DateValue Then
                            CamposObligatorios = False
                            If Not bModoSilencioso Then
                                MensajeError "La fecha fin reprogramada no debe ser menor a la fecha de hoy."
                                txtFinReprogramado.SetFocus
                            End If
                            Exit Function
                        End If
                    End If
                End If
            End If
        Else
            If InStr(1, "DESEST|SUSPEN|TERMIN|", CodigoCombo(cmbEstado, True), vbTextCompare) = 0 Then
                If date >= txtFinPrevisto.DateValue Then
                    CamposObligatorios = False
                    If Not bModoSilencioso Then
                        MensajeError "Debe ingresar la fecha fin reprogramada."
                        txtFinPrevisto.SetFocus
                    End If
                    Exit Function
                End If
            End If
        End If
        ' Estado: obligatorio
        If cmbEstado.ListIndex = -1 Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar el estado."
                cmbEstado.SetFocus
            End If
            Exit Function
        End If
        If CodigoCombo(cmbEstado, True) = "EJECUC" Then
            If cmbSubestado.ListIndex = -1 Then
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "Debe seleccionar el subestado del proyecto."
                    cmbSubestado.SetFocus
                End If
                Exit Function
            End If
        End If
        Select Case CodigoCombo(cmbEstado, True)
            Case "EJECUC"
                If Not IsDate(txtInicio.DateValue) Then
                    CamposObligatorios = False
                    If Not bModoSilencioso Then
                        MensajeError "Para estar en estado Ejecuci�n, debe establecer la fecha de inicio."
                        txtInicio.SetFocus
                    End If
                    Exit Function
                End If
            Case "PRUEBA", "TERMIN"
                If Not IsDate(txtInicio.DateValue) Then
                    CamposObligatorios = False
                    If Not bModoSilencioso Then
                        MensajeError "Para cambiar de estado debe establecer la fecha de inicio."
                        txtInicio.SetFocus
                    End If
                    Exit Function
                Else
                    If Not IsDate(txtFinPrevisto.DateValue) And Not IsDate(txtFinReprogramado.DateValue) Then
                        CamposObligatorios = False
                        If Not bModoSilencioso Then
                            MensajeError "Para cambiar de estado debe establecer la fecha de fin previsto o la reprogramaci�n."
                            txtFinPrevisto.SetFocus
                        End If
                        Exit Function
                    End If
                End If
        End Select
        ' % de avance
        If Val(txtAvance) > 100 Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "El porcentaje de avance no puede superar el 100%."
                txtAvance.SetFocus
            End If
            Exit Function
        End If
        If Val(txtAvance) < 0 Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "El porcentaje de avance no puede ser inferior a 0%."
                txtAvance.SetFocus
            End If
            Exit Function
        End If
        If Val(txtAvance) = 100 Then
            If CodigoCombo(cmbEstado, True) <> "TERMIN" Then
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "El proyecto debe ser finalizado para que el avance sea 100%."
                    txtAvance.SetFocus
                End If
                Exit Function
            End If
        End If
        ' Sem�foro
        'If cSemaforoOriginal = CStr(vscSemaforo.Value) Then
        If cSemaforoOriginal = CStr(udSemaforo.Value) Then      ' new
            bSemaforoManual = False
        Else
            bSemaforoManual = True
        End If
    Else
        ' Propietario: obligatorio
        If cmbPropietario.ListIndex = -1 Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar el �rea propietaria del proyecto."
                cmbPropietario.SetFocus
            End If
            Exit Function
        End If
        ' Plan TYO
        If cmbPlanTyO.ListIndex = -1 Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar Plan TyO."
                cmbPlanTyO.SetFocus
            End If
            Exit Function
        End If
        ' Estado y subestado del proyecto
        If cmbEstado.ListIndex = -1 Then
            CamposObligatorios = False
            If Not bModoSilencioso Then
                MensajeError "Debe seleccionar el estado del proyecto."
                cmbEstado.SetFocus
            End If
            Exit Function
        End If
        Select Case CodigoCombo(cmbEstado, True)
            Case "EJECUC"
            If cmbSubestado.ListIndex = -1 Then
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "Debe seleccionar el subestado del proyecto."
                    cmbSubestado.SetFocus
                End If
                Exit Function
            End If
            Case "PLANOK"
            If Not sp_GetProyectoIDMHitos(l_ProjId, l_ProjSubId, l_ProjSubsId, "Fecha de entrega al usuario", Null, Null) Then
                CamposObligatorios = False
                If Not bModoSilencioso Then
                    MensajeError "Debe existir un hito seleccionado que indica -Fecha de entrega al usuario-" & vbCrLf & "para poder cambiar el estado a Planificado."
                End If
                Exit Function
            End If
            Case Else
            cmbSubestado.ListIndex = -1
        End Select
        'If CodigoCombo(cmbEstado, True) = "EJECUC" Then
        '    If cmbSubestado.ListIndex = -1 Then
        '        CamposObligatorios = False
        '        If Not bModoSilencioso Then
        '            MensajeError "Debe seleccionar el subestado del proyecto."
        '            cmbSubestado.SetFocus
        '        End If
        '        Exit Function
        '    End If
        'End If
    End If
Exit Function
Errores:
    Select Case Err.Number
        Case 5  ' Invalid procedure call or argument
            ' Porque est� deshabilitado un control (enabled = false) y se pretende darle foco (setfocus)
            Resume Next
    End Select
End Function

Private Sub MensajeError(cMensaje)
    MsgBox cMensaje, vbExclamation + vbOKOnly, "Validaci�n"
End Sub

Private Function CamposObligatorios_Alertas() As Boolean
    CamposObligatorios_Alertas = True
    ' Tipo Alerta
    If cmbAlertaTipo.ListIndex = -1 Then
        CamposObligatorios_Alertas = False
        MensajeError "Debe seleccionar el tipo de alerta."
        cmbAlertaTipo.SetFocus
        Exit Function
    End If
    ' Descripci�n
    If cmbAlertaTipo.ListIndex > -1 Then
        If ClearNull(txtAlertaDescripcion) = "" Then
            CamposObligatorios_Alertas = False
            MensajeError "Debe ingresar una descripci�n para el alerta."
            txtAlertaDescripcion.SetFocus
            Exit Function
        End If
    End If
    ' Acci�n correctiva (opcional)
    
    ' Fecha inicio
    If ClearNull(txtAccionCorrectiva) <> "" Then
        If Not IsDate(txtAlertaFechaIni) Then
            CamposObligatorios_Alertas = False
            MensajeError "Debe integrar fecha de inicio de acci�n correctiva."
            txtAlertaFechaIni.SetFocus
            Exit Function
        End If
    End If
    ' Fecha fin
    If chkAlertaResuelta.Value = 1 Then
        If Not IsDate(txtAlertaFechaFin) Then
            CamposObligatorios_Alertas = False
            MensajeError "Debe integrar fecha de fin de acci�n correctiva."
            txtAlertaFechaFin.SetFocus
            Exit Function
        End If
    End If
    ' Coherencia en las fechas
    ' Fecha de fin estimado
    If IsDate(txtAlertaFechaFin.DateValue) Then
        If Not txtAlertaFechaFin.DateValue >= txtAlertaFechaIni.DateValue Then
            CamposObligatorios_Alertas = False
            MensajeError "La fecha fin debe ser mayor o igual a la fecha de inicio."
            txtAlertaFechaFin.SetFocus
            Exit Function
        End If
    End If
    ' Fecha de fin reprogramada
    If IsDate(txtAlertaFechaFin.DateValue) Then
        If Not txtAlertaFechaFin.DateValue >= txtAlertaFechaIni.DateValue Then
            CamposObligatorios_Alertas = False
            MensajeError "La fecha fin reprogramada debe ser mayor o igual a la fecha de inicio."
            txtAlertaFechaFin.SetFocus
            Exit Function
        End If
    End If
    ' Responsables
    If ClearNull(cAlertasResponsables) = "" Then
        CamposObligatorios_Alertas = False
        MensajeError "Debe integrar los responsables del alerta."
        optAlertasTexto(1).Value = True
        txtAlertasObservaciones.SetFocus
        Exit Function
    End If
    
    ' Observaciones
    ' Resuelta
    ' Fecha de resoluci�n
    If chkAlertaResuelta.Value = 1 Then
        If txtFechaResolucion.Text = "" Then
            CamposObligatorios_Alertas = False
            MensajeError "Debe establecer la fecha de resoluci�n."
            Exit Function
        End If
        If Not IsDate(txtFechaResolucion.DateValue) Then
            CamposObligatorios_Alertas = False
            MensajeError "Debe establecer una fecha de resoluci�n v�lida."
            Exit Function
        End If
        If txtFechaResolucion.DateValue <= txtAlertaFechaIni.DateValue Then
            CamposObligatorios_Alertas = False
            MensajeError "La fecha de resoluci�n no puede ser anterior o igual a la fecha de inicio."
            Exit Function
        End If
        If txtFechaResolucion.DateValue < txtAlertaFechaFin.DateValue Then
            CamposObligatorios_Alertas = False
            MensajeError "La fecha de resoluci�n no puede ser anterior a la fecha de fin."
            Exit Function
        End If
        
        If txtFechaResolucion.DateValue > date Then
            CamposObligatorios_Alertas = False
            MensajeError "La fecha de resoluci�n no puede ser mayor a la fecha de hoy."
            Exit Function
        End If
    Else
        If txtFechaResolucion.Text <> "" Then
            CamposObligatorios_Alertas = False
            MensajeError "El alerta debe estar resuelta para establecer la fecha de resoluci�n."
            Exit Function
        End If
    End If
End Function

Private Function CamposObligatorios_Hitos() As Boolean
    Dim vMsg As String                  ' add -002- a.
    
    CamposObligatorios_Hitos = True
    
    vMsg = "Cambi� el hito o la descripci�n del mismo." & vbCrLf & _
           "�Los valores de exposici�n y clasificaci�n siguen siendo correctos?" & vbCrLf & vbCrLf & _
           "Exposici�n: " & TextoCombo(cmbHitoExposicion, CodigoCombo(cmbHitoExposicion, True), True) & vbCrLf & _
           "Clasificaci�n: " & IIf(TextoCombo(cmbHitoClasificacion, CodigoCombo(cmbHitoClasificacion, False)) = "", "-", TextoCombo(cmbHitoClasificacion, CodigoCombo(cmbHitoClasificacion, False)))
    
    ' Fecha inicio
    If Not IsDate(txtHitoInicio.DateValue) Then
        CamposObligatorios_Hitos = False
        MensajeError "Debe ingresar o seleccionar la fecha de inicio."
        txtHitoInicio.SetFocus
        Exit Function
    End If
    ' Hito
    If IsDate(txtHitoInicio.DateValue) Then
        If cmbHito.ListIndex = -1 Then
            If Len(Trim(cmbHito)) < 1 Then
                CamposObligatorios_Hitos = False
                MensajeError "Debe ingresar el hito."
                txtHitoNombre.SetFocus
                Exit Function
            End If
        Else
            If Not IsDate(txtHitoFinalizacion) Then
                CamposObligatorios_Hitos = False
                MensajeError "Debe ingresar o seleccionar la fecha de finalizaci�n."
                txtHitoFinalizacion.SetFocus
                Exit Function
            End If
        End If
    End If
    
    ' Fecha finalizaci�n
    If IsDate(txtHitoFinalizacion.DateValue) Then
        If IsDate(txtHitoInicio.DateValue) Then
            If txtHitoInicio.DateValue > txtHitoFinalizacion.DateValue Then
                CamposObligatorios_Hitos = False
                MensajeError "La fecha de finalizaci�n no puede ser menor a la fecha de inicio."
                txtHitoFinalizacion.SetFocus
                Exit Function
            End If
        End If
    End If
    
'    If IsDate(txtHitoInicio.DateValue) Then
'        If Len(Trim(txtHitoNombre)) < 1 Then
'            CamposObligatorios_Hitos = False
'            MensajeError "Debe ingresar el hito."
'            txtHitoNombre.SetFocus
'            Exit Function
'        End If
'    End If
    
    ' Por ahora el �rea responsable es opcional
'    ' �rea responsable del hito (obligatorio)
'    If cboHitosAreaResp.ListIndex = -1 Then
'        CamposObligatorios_Hitos = False
'        MensajeError "Debe seleccionar un �rea responsable para el hito."
'        cboHitosAreaResp.SetFocus
'        Exit Function
'    End If
    ' Descripci�n
    ' Estado
    If IsDate(txtHitoInicio.DateValue) Then
        If cmbHitoEstado.ListIndex = -1 Then
            CamposObligatorios_Hitos = False
            MensajeError "Debe seleccionar el estado del hito."
            cmbHitoEstado.SetFocus
            Exit Function
        End If
    End If
    
    If Not Len(ClearNull(txtHitoOrden)) = 0 Then
        If Not IsNumeric(txtHitoOrden) Then
            CamposObligatorios_Hitos = False
            MensajeError "Debe ingresar un dato v�lido para Orden."
            If cmbHitoEstado.Enabled Then cmbHitoEstado.SetFocus
            Exit Function
        End If
    End If
        
    '{ add -004- a.
    If lblHitosModoTrabajo <> "Eliminar" Then
        If cmbHitoExposicion.ListIndex = -1 Then
            CamposObligatorios_Hitos = False
            MensajeError "Debe seleccionar la exposici�n para este hito."
            If cmbHitoExposicion.Enabled Then cmbHitoExposicion.SetFocus
            Exit Function
        End If
    End If
    '}
    '{ add -002- a. - Control por exposici�n
    If CodigoCombo(cmbHitoExposicion, True) = "S" Then
        If cmbHitoClasificacion.ListIndex = -1 Then
            CamposObligatorios_Hitos = False
            MensajeError "Debe seleccionar la clasificaci�n para este hito."
            If cmbHitoClasificacion.Enabled Then cmbHitoClasificacion.SetFocus
            Exit Function
        End If
    End If
    
    ' Si hubo cambios en el nombre o en la descripci�n del hito en la edici�n, y
    ' el hito estaba marcado como "Expuesto" y con alguna clasificaci�n,
    ' confirmo que los mismos se mantengan.
    If lblHitosModoTrabajo = "Modificar" Then
        If (ClearNull(cmbHito.Tag) <> "" And (ClearNull(cmbHito.Tag) <> ClearNull(cmbHito.Text))) Or _
            (ClearNull(txtHitoDescripcion.Tag) <> "" And ClearNull(txtHitoDescripcion.Tag) <> ClearNull(txtHitoDescripcion.Text)) Then
            If cmbHitoExposicion.Tag = CodigoCombo(cmbHitoExposicion, True) Or _
                cmbHitoClasificacion.Tag = CodigoCombo(cmbHitoClasificacion, False) Then
                If MsgBox(vMsg, vbExclamation + vbYesNo, "Visibilidad de hitos") = vbNo Then
                    cmbHitoExposicion.SetFocus
                    CamposObligatorios_Hitos = False
                    Exit Function
                End If
            End If
        End If
    End If
    '}
End Function

Private Function CamposObligatorios_Responsables() As Boolean
    CamposObligatorios_Responsables = True
    If cboAreaResponsable.ListIndex = -1 Then
        CamposObligatorios_Responsables = False
        MensajeError "Debe seleccionar el �rea responsable a guardar."
        cboAreaResponsable.SetFocus
        Exit Function
    End If
End Function

Private Sub cmdCerrar_Click()
    'If InStr(1, "AGREGAR|EDICION|", cModo, vbTextCompare) > 0 Then  ' Antes de cerrar, controlar ciertas cosas
    If cModo = "AGREGAR" Then  ' Antes de cerrar, controlar ciertas cosas
        If InStr(1, "SADM|ADMI|", glUsrPerfilActual, vbTextCompare) > 0 Then
            Unload Me
        Else
            If CamposObligatorios(True) Then
                Unload Me
            Else
                MsgBox "Antes de cerrar, debe editar y completar los datos requeridos para crear el nuevo proyecto." & vbCrLf & _
                        "Presione el bot�n Modificar y complete los datos solicitados antes de finalizar.", vbExclamation + vbOKOnly, "Alta de nuevo Proyecto"
            End If
        End If
    Else
        Unload Me
    End If
End Sub

Private Sub grdAlertas_SelChange()
    MostrarSeleccionAlertas
End Sub

Private Sub grdAlertas_RowColChange()
    If Not bLoading Then MostrarSeleccionAlertas
End Sub

Private Sub grdHitos_SelChange()
    If Not bLoading Then MostrarSeleccionHitos
End Sub

Private Sub grdHitos_RowColChange()
    If Not bLoading Then MostrarSeleccionHitos
End Sub

Private Sub grdAdjuntos_RowColChange()
    If Not bLoading Then MostrarSeleccionAdjuntos
End Sub

Private Sub grdAdjuntos_SelChange()
    If Not bLoading Then MostrarSeleccionAdjuntos
End Sub

Private Sub txtAlertasObservaciones_LostFocus()
    Select Case IIf(optAlertasTexto(0).Value, 0, 1)
        Case 0: cAlertasObservaciones = ClearNull(txtAlertasObservaciones.Text)
        Case 1: cAlertasResponsables = ClearNull(txtAlertasObservaciones.Text)
    End Select
End Sub

Private Sub cmbEstado_Click()
    If Not bLoading Then
        Select Case CodigoCombo(cmbEstado, True)
            Case "TERMIN"
                txtAvance.Text = "100"
                Call setHabilCtrl(txtAvance, "DIS")
                cmbSubestado.ListIndex = -1
                Call setHabilCtrl(cmbSubestado, "DIS")
            Case "EJECUC"
                Call setHabilCtrl(cmbSubestado, "NOR")
                If cmbSubestado.Tag <> "" Then
                    cmbSubestado.ListIndex = PosicionCombo(cmbSubestado, cmbSubestado.Tag, True)
                Else
                    cmbSubestado.ListIndex = -1
                End If
            Case Else
                txtAvance.Text = txtAvance.Tag
                cmbSubestado.ListIndex = -1
                Call setHabilCtrl(cmbSubestado, "DIS")
                Call setHabilCtrl(txtAvance, "NOR")
        End Select
    End If
End Sub

Private Sub txtAvance_LostFocus()
    If IsNumeric(txtAvance) Then
        If Val(txtAvance) > 0 Then
            If Val(txtAvance) <> Val(txtAvance.Tag) Then
                If Val(txtAvance) = 100 Then
                    cmbEstado.ListIndex = PosicionCombo(cmbEstado, "TERMIN", True)
                Else
                    If InStr(1, "PLANIF|", cEstadoOriginal, vbTextCompare) > 0 Then
                        cmbEstado.ListIndex = PosicionCombo(cmbEstado, "EJECUC", True)
                    End If
                End If
            End If
        End If
    End If
End Sub

'Private Sub vscSemaforo_Change()
'    With shpSemaforo
'        Select Case vscSemaforo.Value
'            Case COLOR_SEMAF_ROJO
'                .BackColor = SEMAF_BACK_ROJO
'                .BorderColor = SEMAF_BORD_ROJO
'            Case COLOR_SEMAF_AMARILLO
'                .BackColor = SEMAF_BACK_AMARILLO
'                .BorderColor = SEMAF_BORD_AMARILLO
'            Case COLOR_SEMAF_VERDE
'                .BackColor = SEMAF_BACK_VERDE
'                .BorderColor = SEMAF_BORD_VERDE
'        End Select
'    End With
'    If Not bLoading Then
'        If cSemaforoOriginal <> CStr(vscSemaforo.Value) Then
'            bSemaforoManual = True
'            lblSemaforoManual.visible = True
'            Call setHabilCtrl(chkRecalcularSemaforo, "NOR")
'            shpRecuadroSemaforo.BackColor = &HFFFFFF
'        Else
'            bSemaforoManual = False
'            lblSemaforoManual.visible = False
'            Call setHabilCtrl(chkRecalcularSemaforo, "DIS")
'            shpRecuadroSemaforo.BackColor = fraDatosCabecera.BackColor
'        End If
'    End If
'End Sub

Private Sub udSemaforo_Change()
    Select Case udSemaforo.Value
        Case 1
            imgSemaVerde.visible = False
            imgSemaRojo.visible = True
            imgSemaAmarillo.visible = False
        Case 2
            imgSemaVerde.visible = False
            imgSemaRojo.visible = False
            imgSemaAmarillo.visible = True
        Case 3
            imgSemaVerde.visible = True
            imgSemaRojo.visible = False
            imgSemaAmarillo.visible = False
    End Select
    If Not bLoading Then
        'If cSemaforoOriginal <> CStr(vscSemaforo.Value) Then
        If cSemaforoOriginal <> CStr(udSemaforo.Value) Then
            bSemaforoManual = True
            lblSemaforoManual.visible = True
            Call setHabilCtrl(chkRecalcularSemaforo, "NOR")
        Else
            bSemaforoManual = False
            lblSemaforoManual.visible = False
            Call setHabilCtrl(chkRecalcularSemaforo, "DIS")
        End If
    End If
End Sub

Private Sub cmbCategoria_Click()
    ' Cada cambio de categor�a debe reflejar las clases correspondientes
    'If bGuardando Then Exit Sub
    If Not bLoading Then
        With cmbClase
            .Clear
            If spProyectoIDM.sp_GetProyIDMClase(CodigoCombo(cmbCategoria, False), Null, Null) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!ProjClaseId) & ": " & ClearNull(aplRST.Fields!ProjClaseNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!ProjCatId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = -1      ' Por defecto, no selecciono ninguna clase para que el usuario elija
            End If
        End With
    End If
End Sub

Private Sub sstProyectosIDM_Click(PreviousTab As Integer)
    With sstProyectosIDM
        Select Case .Tab
            Case 0:
            Case 1: Cargar_GrillaAlertas
            Case 2: Cargar_GrillaHitos
        End Select
    End With
End Sub

' ************************************************************************************************************************************
' ORDENAMIENTO
' ************************************************************************************************************************************
Private Sub grdAlertas_Click()
    bSorting = True
    Call OrdenarGrilla(grdAlertas, lUltimaColumnaOrdenada_Alertas)
    bSorting = False
End Sub

Private Sub grdHitos_Click()
    bSorting = True
    Call OrdenarGrilla(grdHitos, lUltimaColumnaOrdenada_Hitos)
    bSorting = False
End Sub

Private Sub cmbSolicitante_Click()
    If Not bLoading Then
        EstablecerAreaSolicitante
    End If
End Sub

Private Sub cmbPropietario_Click()
    Dim nivel As String
    Dim area As String
    Dim perfil As String
    Dim perfil_nombre As String
    
    If Not bLoading Then
        Call EstablecerArea(nivel, area, perfil)
    End If
End Sub

Private Sub EstablecerArea(ByRef nivel, ByRef area, ByRef perfil)
    Dim aux As String
    
    If Not bLoading Then
        cProp_Direccion = ""
        cProp_Gerencia = ""
        cProp_Sector = ""
        aux = CodigoCombo(cmbSolicitante, True)     ' Inicializaci�n del auxiliar
        If Len(aux) > 0 Then
            If InStr(1, aux, "/", vbTextCompare) > 0 Then
                cProp_Direccion = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
                aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
            Else
                cProp_Direccion = aux
                aux = ""
            End If
        End If
        If Len(aux) > 0 Then
            If InStr(1, aux, "/", vbTextCompare) > 0 Then
                cProp_Gerencia = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
                aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
            Else
                cProp_Gerencia = aux
                aux = ""
            End If
            If Len(aux) > 0 Then
                cProp_Sector = aux
            End If
        End If
    
        If cProp_Sector = "" And cProp_Gerencia = "" Then
            nivel = "DIRE"
            area = cProp_Direccion
            perfil = "CDIR"
        ElseIf cProp_Sector <> "" Then
            nivel = "SECT"
            area = cProp_Sector
            perfil = "CGRU"
        Else
            nivel = "GERE"
            area = cProp_Gerencia
            perfil = "CGER"
        End If
    End If
End Sub

Private Sub EstablecerAreaSolicitante()
    Dim aux As String
    
    If Not bLoading Then
        cod_direccion_soli = ""
        cod_gerencia_soli = ""
        cod_sector_soli = ""
        aux = CodigoCombo(cmbSolicitante, True)     ' Inicializaci�n del auxiliar
        If Len(aux) > 0 Then
            If InStr(1, aux, "/", vbTextCompare) > 0 Then
                cod_direccion_soli = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
                aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
            Else
                cod_direccion_soli = aux
                aux = ""
            End If
        End If
        If Len(aux) > 0 Then
            If InStr(1, aux, "/", vbTextCompare) > 0 Then
                cod_gerencia_soli = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
                aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
            Else
                cod_gerencia_soli = aux
                aux = ""
            End If
            If Len(aux) > 0 Then
                cod_sector_soli = aux
            End If
        End If
    End If
End Sub

Private Sub ActualizarCombos(categoria, clase)
    'If bGuardando Then Exit Sub
    With cmbCategoria
        .Clear
        If spProyectoIDM.sp_GetProyIDMCategoria(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!ProjCatId) & ": " & ClearNull(aplRST.Fields!ProjCatNom)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = PosicionCombo(cmbCategoria, categoria, False)
        End If
    End With
    If categoria <> 0 Then
        With cmbClase
            .Clear
            If sp_GetProyIDMClase(categoria, Null, Null) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!ProjClaseId) & ": " & ClearNull(aplRST.Fields!ProjClaseNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!ProjCatId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = PosicionCombo(cmbClase, clase, False)
            End If
        End With
    End If
End Sub

Private Sub ObtenerNivelAreaCombo(cboControl As ComboBox, ByRef cod_nivel As String, ByRef cod_area As String)
    Dim aux As String
    
    aux = CodigoCombo(cboControl, True)     ' Inicializaci�n del auxiliar
    If Len(aux) > 0 Then
        cod_nivel = "DIRE"
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_area = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_area = aux
            aux = ""
        End If
    End If
    
    If Len(aux) > 0 Then
        cod_nivel = "GERE"
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_area = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_area = aux
            aux = ""
        End If
    End If
    
    If Len(aux) > 0 Then
        cod_nivel = "SECT"
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_area = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_area = aux
            aux = ""
        End If
    End If
    
    If Len(aux) > 0 Then
        cod_nivel = "GRUP"
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_area = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_area = aux
            aux = ""
        End If
    End If
End Sub

Private Sub ObtenerAreasCombo(cboControl As ComboBox, ByRef cod_direccion As String, ByRef cod_gerencia As String, ByRef cod_sector As String, ByRef cod_grupo As String)
    Dim aux As String
    
    aux = CodigoCombo(cboControl, True)     ' Inicializaci�n del auxiliar
    If Len(aux) > 0 Then
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_direccion = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_direccion = aux
            aux = ""
        End If
    End If
    
    If Len(aux) > 0 Then
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_gerencia = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_gerencia = aux
            aux = ""
        End If
    End If
    
    If Len(aux) > 0 Then
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_sector = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_sector = aux
            aux = ""
        End If
    End If
    
    If Len(aux) > 0 Then
        If InStr(1, aux, "/", vbTextCompare) > 0 Then
            cod_grupo = Mid(aux, 1, InStr(1, aux, "/", vbTextCompare) - 1)
            aux = Mid(aux, InStr(1, aux, "/", vbTextCompare) + 1)
        Else
            cod_grupo = aux
            aux = ""
        End If
    End If
End Sub

Private Sub txtBeneficios_GotFocus()
    If sModoGeneral = "Edicion" Then
        txtBeneficios_Change
        lblTamanioImpresion.visible = True
    End If
End Sub

Private Sub txtBeneficios_LostFocus()
    If sModoGeneral = "Edicion" Then lblTamanioImpresion.visible = False
End Sub

Private Sub txtDescripcion_GotFocus()
    If sModoGeneral = "Edicion" Then
        txtDescripcion_Change
        lblTamanioImpresion.visible = True
    End If
End Sub

Private Sub txtDescripcion_LostFocus()
    If sModoGeneral = "Edicion" Then lblTamanioImpresion.visible = False
End Sub

Private Sub txtDescripcion_Change()
    If sModoGeneral = "Edicion" Then lblTamanioImpresion = "Descripci�n: caracteres para impresi�n: " & Len(Trim(txtDescripcion)) & " de 1030"
End Sub

Private Sub txtBeneficios_Change()
    If sModoGeneral = "Edicion" Then lblTamanioImpresion = "Beneficios: caracteres para impresi�n: " & Len(Trim(txtBeneficios)) & " de 380"
End Sub

Private Sub cmdHistorial_Click()
    Load frmProyectoIDMHistorial
    frmProyectoIDMHistorial.Show 1
End Sub

Private Sub cmbTipoProyecto_Click()
    If Not bLoading Then
        'glUsrPerfilActual = "BPAR"
        If sp_GetAccionesPerfilIDM(IIf(CodigoCombo(cmbTipoProyecto, True) = "I", "INCHGEST", "PJCHGEST"), glUsrPerfilActual, CodigoCombo2(cmbEstado.Tag, True), Null) Then
            bLoading = True
            With cmbEstado
                .Clear
                .AddItem cmbEstado.Tag
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!nom_estadonew) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_estnew)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = 0
            End With
            Call setHabilCtrl(cmbEstado, "NOR")
            bLoading = False
        End If
        If CodigoCombo(cmbEstado, True) = "EJECUC" Then
            Call setHabilCtrl(cmbSubestado, "NOR")
        End If
    End If
End Sub

'{ add -002- a.
Private Sub cmbHitoExposicion_Click()
    If Not bLoading Then
        If CodigoCombo(cmbHitoExposicion, True) = "S" Then
            Call setHabilCtrl(cmbHitoClasificacion, "NOR")
        Else
            Call setHabilCtrl(cmbHitoClasificacion, "DIS")
            'cmbHitoClasificacion.ListIndex = -1
        End If
    End If
End Sub
'}


'Private Sub txtNovedades_Change()
'    If sModoGeneral = "Edicion" Then lblTamanioImpresion = "Novedades: caracteres para impresi�n: " & Len(Trim(txtNovedades)) & " de 917"
'End Sub

'Private Sub txtNovedades_GotFocus()
'    If sModoGeneral = "Edicion" Then
'        txtNovedades_Change
'        lblTamanioImpresion.Visible = True
'    End If
'End Sub
'
'Private Sub txtNovedades_LostFocus()
'    If sModoGeneral = "Edicion" Then lblTamanioImpresion.Visible = False
'End Sub

'Private Sub Command1_Click()
'    Dim CadenaNueva As String
'    Dim fClambio As Boolean
'    Dim i As Long
'
'    i = 0
'    If sp_GetProyectoIDMHitos(Null, Null, Null, Null) Then
'        Call Puntero(True)
'        Do While Not aplRST.EOF
'            i = i + 1
'            Call Status("" & i)
'            'If aplRST.Fields!hit_nrointerno = 863 Then
'            CadenaNueva = ClearNull(aplRST.Fields!hito_nombre)
'            fClambio = False
'            ' Campo: nombre
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'                fClambio = True
'            End If
'            If InStr(1, CadenaNueva, "?", vbTextCompare) > 0 Then        ' �
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("?"), "�")
'                fClambio = True
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'                fClambio = True
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'                fClambio = True
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'                fClambio = True
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then         ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then         ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If fClambio Then Call sp_UpdateProyectoIDMHitosField(aplRST.Fields!ProjId, aplRST.Fields!ProjSubId, aplRST.Fields!ProjSubSId, aplRST.Fields!hit_nrointerno, "HITO_NOMBRE", CadenaNueva, Null, Null)
'
'            CadenaNueva = ClearNull(aplRST.Fields!hito_descripcion)
'            fClambio = False
'            ' Campo: descripci�n
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If InStr(1, CadenaNueva, "?", vbTextCompare) > 0 Then        ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("?"), "�")
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'                fClambio = True
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'                fClambio = True
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then        ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then         ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If InStr(1, CadenaNueva, "�", vbTextCompare) > 0 Then         ' �
'                fClambio = True
'                CadenaNueva = ReplaceString(CadenaNueva, Asc("�"), "�")
'            End If
'            If fClambio Then Call sp_UpdateProyectoIDMHitosField(aplRST.Fields!ProjId, aplRST.Fields!ProjSubId, aplRST.Fields!ProjSubSId, aplRST.Fields!hit_nrointerno, "HITO_DESCRIPCION", CadenaNueva, Null, Null)
'            'End If
'            aplRST.MoveNext
'            DoEvents
'        Loop
'        Call Puntero(False)
'        MsgBox ("Proceso finalizado.")
'    End If
'End Sub

'{ add -005- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdHitos)
    Call DetenerScroll(grdAlertas)
    Call DetenerScroll(grdAreasResponsables)
    Call DetenerScroll(grdProyectoGrupos)
    Call DetenerScroll(grdAdjuntos)
    Call DetenerScroll(grdHoras)
End Sub
'}
