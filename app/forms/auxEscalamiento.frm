VERSION 5.00
Begin VB.Form auxEscalamiento 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3075
   ClientLeft      =   795
   ClientTop       =   2025
   ClientWidth     =   7620
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3075
   ScaleWidth      =   7620
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraOpciones 
      Height          =   945
      Left            =   2460
      TabIndex        =   1
      Top             =   1920
      Width           =   2535
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Height          =   555
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   555
         Left            =   1320
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "DISPARO DEL ESCALAMIENTO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   285
      Left            =   840
      TabIndex        =   0
      Top             =   600
      Width           =   6075
   End
End
Attribute VB_Name = "auxEscalamiento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancelar_Click()
   Unload Me
End Sub

Private Sub cmdOK_Click()
    cmdCancelar.Enabled = False
    cmdOk.Enabled = False
    Call puntero(True)
    Call Status("Invocando Proceso")
    Call sp_VerEscalamiento
    Call Status("Listo")
    Call puntero(False)
    cmdCancelar.Enabled = True
    cmdOk.Enabled = False
End Sub

