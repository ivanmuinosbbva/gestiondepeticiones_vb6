VERSION 5.00
Begin VB.Form frmPeticionesTraspaso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Traspaso de cartera de peticiones entre grupos ejecutores"
   ClientHeight    =   5985
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8280
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesTraspaso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   8280
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cboGrupo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   90
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   0
      ToolTipText     =   "Sector Solicitante"
      Top             =   1350
      Width           =   8130
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   60
      TabIndex        =   17
      Top             =   1800
      Width           =   8190
      Begin VB.OptionButton optOpciones 
         Caption         =   "Todas las peticiones en donde participa mi grupo"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   20
         Top             =   480
         Width           =   5055
      End
      Begin VB.OptionButton optOpciones 
         Caption         =   "Solo las que he seleccionado en la bandeja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   5055
      End
      Begin VB.CheckBox chkFinalizarme 
         Caption         =   "Finalizar mi participaci�n al traspasar la cartera de peticiones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   5160
         TabIndex        =   18
         Top             =   240
         Value           =   1  'Checked
         Width           =   2895
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   " Detalle de acciones automatizadas en el proceso "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   60
      TabIndex        =   11
      Top             =   3000
      Width           =   8175
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "� Se actualiza la informaci�n de planificaci�n."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   240
         TabIndex        =   22
         Top             =   1560
         Width           =   3210
      End
      Begin VB.Label lblWarning 
         AutoSize        =   -1  'True
         Caption         =   "Solo ser�n procesadas peticiones en las que su grupo se encuentre en estado activo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   1920
         Width           =   7110
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "� Se transfiere la potestad de los documentos del grupo cedente al grupo que hereda (si �ste finaliza)."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   240
         TabIndex        =   16
         Top             =   1320
         Width           =   7365
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "� Si el grupo heredero existiera en estado activo, se mantienen sus atributos."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   240
         TabIndex        =   15
         Top             =   1080
         Width           =   5580
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "� Si el grupo heredero existiera en estado no activo, se activa con los atributos del grupo cedente."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   14
         Top             =   840
         Width           =   7095
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "� Si el Sector del grupo heredero no existiera, se agrega."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   13
         Top             =   600
         Width           =   4110
      End
      Begin VB.Label lblExplicacion 
         AutoSize        =   -1  'True
         Caption         =   "� Se asigna de manera autom�tica el Sector y Grupo que hereda las peticiones del grupo cedente."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   7035
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   60
      TabIndex        =   1
      Top             =   0
      Width           =   8190
      Begin VB.Label lblSector 
         Caption         =   "sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   840
         TabIndex        =   3
         Top             =   180
         Width           =   7185
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   2
         Top             =   180
         Width           =   555
      End
   End
   Begin VB.Frame Frame3 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   60
      TabIndex        =   7
      Top             =   435
      Width           =   8190
      Begin VB.Label lblGrupo 
         Caption         =   "grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   840
         TabIndex        =   9
         Top             =   180
         Width           =   7185
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   180
         Width           =   510
      End
   End
   Begin VB.Frame pnlBtnControl 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   60
      TabIndex        =   4
      Top             =   5280
      Width           =   8175
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7170
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6180
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Traspasar mi cartera activa al grupo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   90
      TabIndex        =   10
      Top             =   1110
      Width           =   3135
   End
End
Attribute VB_Name = "frmPeticionesTraspaso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim xPerfil As String
Dim xNivel As String
Dim xArea As String

Private Sub Form_Load()
    xPerfil = glUsrPerfilActual
    xNivel = getPerfNivel(xPerfil)
    xArea = getPerfArea(xPerfil)
    Call InicializarPantalla
    Call InicializarCombos
    cmdConfirmar.Enabled = True
End Sub

Sub InicializarPantalla()
    Dim auxNro As String
    Me.Tag = ""
    
    cmdConfirmar.Enabled = False
    If sp_GetSector(glLOGIN_Sector, glLOGIN_Gerencia, "") Then
        lblSector = ClearNull(aplRST.Fields!nom_sector)
    End If
    If sp_GetGrupo(glLOGIN_Grupo, glLOGIN_Sector, "") Then
        lblGrupo = ClearNull(aplRST.Fields!nom_grupo)
    End If
End Sub

Sub InicializarCombos()
    Dim flgExtendido As Boolean
    Dim xAccion As String
    Dim fltSector As String
    Dim fltGrupo As String
    Dim fltNivel As String
    Dim cSector As String, cGrupo As String
    Dim sDireccion As String
    Dim sGerencia As String
    
    ' Cargo el sector del grupo especificado para el perfil del usuario
    If sp_GetGrupo(xArea, Null, Null) Then
        cSector = ClearNull(aplRST.Fields!cod_sector)
        cGrupo = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    If sp_GetVarios("TRANIV") Then
        If aplRST.EOF Then
            fltSector = ""
            fltGrupo = ""
        Else
            fltNivel = ClearNull(aplRST.Fields!var_texto)
            Select Case fltNivel
                Case "BBVA"
                    fltSector = ""
                    fltGrupo = ""
                Case "DIRE"
                    fltSector = ""
                    fltGrupo = ""
                Case "GERE"
                    fltSector = ""
                    fltGrupo = ""
                Case "SECT"
                    fltSector = cSector
                    fltGrupo = cGrupo
                Case "GRUP"
                    fltSector = cSector
                    fltGrupo = cGrupo
            End Select
        End If
    End If
    
    cboGrupo.Clear
    'If sp_GetGrupoXt("", "", "S") Then
    If sp_GetGrupoXt(fltGrupo, fltSector, "S") Then
        Do While Not aplRST.EOF
            If fltNivel = "DIRE" Or fltNivel = "GERE" Then
                If fltNivel = "DIRE" And ClearNull(aplRST.Fields!cod_direccion) = glLOGIN_Direccion Then
                    If ClearNull(aplRST!es_ejecutor) = "S" Then
                        'cboGrupo.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(100) & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                        sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                        sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                        cboGrupo.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                    End If
                End If
                If fltNivel = "GERE" And ClearNull(aplRST.Fields!cod_gerencia) = glLOGIN_Gerencia Then
                    If ClearNull(aplRST!es_ejecutor) = "S" Then
                        'cboGrupo.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(100) & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                        sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                        sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                        cboGrupo.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                    End If
                End If
            Else
                If ClearNull(aplRST!es_ejecutor) = "S" Then
                    'cboGrupo.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & " � " & Trim(aplRST!nom_grupo) & Space(100) & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                    sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                    sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                    cboGrupo.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_sector) & "|" & Trim(aplRST!cod_grupo)
                End If
            End If
            aplRST.MoveNext
        Loop
        aplRST.Close
        cboGrupo.ListIndex = -1
    End If
    
    ' Habilitar luego...
    'If frmPeticionesShell.bSeleccion Then
        optOpciones(0).Caption = optOpciones(0).Caption & " (" & UBound(vPeticiones) & IIf(UBound(vPeticiones) = 1, " petici�n)", " peticiones)")
        optOpciones(0).Value = True
        optOpciones(1).Value = False
        optOpciones(0).Enabled = True
        'optOpciones(1).Enabled = True
    'Else
    '    optOpciones(0).Value = False
    '    optOpciones(1).Value = True
    '    optOpciones(0).Enabled = False
    '    optOpciones(1).Enabled = False
    'End If
End Sub

Private Sub cmdConfirmar_Click()
    Dim vRetorno() As String
    Dim nwSector As String, nwGrupo As String
    Dim cSector As String, cGrupo As String
    Dim X As Long
    Dim cNombreGrupo As String
    Dim cEstado As String
    Dim cMensaje As String
    Dim bErrores As Boolean
    
    cMensaje = "Las siguientes peticiones no pudieron ser tratadas por encontrarse su grupo en estado terminal: " & vbCrLf & vbCrLf
    bErrores = False
    
    If cboGrupo.ListIndex < 0 Then
        MsgBox "Debe seleccionar un grupo.", vbExclamation
        Exit Sub
    Else
        If ParseString(vRetorno, DatosCombo(cboGrupo), "|") > 0 Then
            nwGrupo = vRetorno(2)
            nwSector = vRetorno(1)
            If sp_GetGrupo(nwGrupo, nwSector) Then
                cNombreGrupo = ClearNull(aplRST.Fields!nom_grupo)
            End If
        End If
    End If
    
    ' Cargo el sector del grupo especificado para el perfil del usuario
    If sp_GetGrupo(xArea, Null, Null) Then
        cSector = ClearNull(aplRST.Fields!cod_sector)
        cGrupo = ClearNull(aplRST.Fields!cod_grupo)
    End If
    
    If nwGrupo = cGrupo Then
        MsgBox "Debe seleccionar un grupo distinto al suyo.", vbExclamation
        Exit Sub
    End If
    
    ' Si es una selecci�n
    If optOpciones(0).Value = True Then
        If MsgBox("�Confirma el traspaso de peticiones seleccionadas al grupo " & cNombreGrupo & " ?", vbQuestion + vbYesNo, "Traspaso de cartera") = vbYes Then
            Call Puntero(True)
            Call Status("Procesando...")
            cmdConfirmar.Enabled = False
            cmdCerrar.Enabled = False
            
            For X = 1 To UBound(vPeticiones)
                'If sp_GetPeticionGrupo(vPeticiones(x), glLOGIN_Sector, glLOGIN_Grupo) Then
                If sp_GetPeticionGrupo(vPeticiones(X), cSector, cGrupo) Then
                    cEstado = ClearNull(aplRST.Fields!cod_estado)
                Else
                    ' No se encontr� el grupo cedente en la petici�n al momento de intentar realizar la transferencia
                    Call Puntero(False)
                    Call Status("Listo.")
                    MsgBox "Proceso cancelado. No se encontr� el grupo en la petici�n. Revise.", vbInformation + vbOKOnly, "Traspaso de cartera de peticiones cancelado"
                    Exit Sub
                End If
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", cEstado, vbTextCompare) = 0 Then
                    If Not sp_TraspasarPeticionAGrupo(vPeticiones(X), cSector, cGrupo, nwSector, nwGrupo, IIf(chkFinalizarme.Value = 1, "S", "N")) Then
                        MsgBox "Error al transferir la petici�n", vbExclamation + vbOKOnly
                    End If
                Else
                    If sp_GetUnaPeticion(vPeticiones(X)) Then
                        cMensaje = cMensaje & ClearNull(aplRST.Fields!pet_nroasignado) & vbCrLf
                    End If
                End If
            Next X
            Call Puntero(False)
            Call Status("Listo.")
            MsgBox "Proceso finalizado.", vbInformation + vbOKOnly, "Traspaso de cartera de peticiones"
            If bErrores Then
                MsgBox cMensaje, vbExclamation + vbOKOnly, "Errores al intentar procesar algunas peticiones"
            End If
        End If
    Else    ' Todas las peticiones del grupo que cede
        If MsgBox("�Confirma el traspaso de TODAS las peticiones al grupo " & cNombreGrupo & " ?", vbQuestion + vbYesNo, "Traspaso de cartera COMPLETA") = vbYes Then
            Call Puntero(True)
            Call Status("Procesando...")
            cmdConfirmar.Enabled = False
            cmdCerrar.Enabled = False
            
            'If Not sp_TraspasarPeticionesAGrupo(glLOGIN_Sector, glLOGIN_Grupo, nwSector, nwGrupo, IIf(chkFinalizarme.Value = 1, "S", "N")) Then
            If Not sp_TraspasarPeticionesAGrupo(cSector, cGrupo, nwSector, nwGrupo, IIf(chkFinalizarme.Value = 1, "S", "N")) Then
                MsgBox "Error al transferir peticiones"
            End If
        End If
        Call Puntero(False)
        Call Status("Listo.")
        MsgBox "Proceso finalizado.", vbInformation + vbOKOnly, "Traspaso de cartera COMPLETA"
    End If
 
    Call touchForms
    Unload Me
End Sub

'Private Sub Form_Activate()
'    StatusBarObjectIdentity Me
'End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub
