VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmPeticionesShellView05 
   Caption         =   "Form2"
   ClientHeight    =   7365
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13275
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   7365
   ScaleWidth      =   13275
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdDatos 
      Height          =   5535
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   12315
      _ExtentX        =   21722
      _ExtentY        =   9763
      _Version        =   393216
      FixedCols       =   0
      GridColor       =   14737632
      SelectionMode   =   1
      AllowUserResizing=   3
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
      _Band(0).GridLinesBand=   1
      _Band(0).TextStyleBand=   0
      _Band(0).TextStyleHeader=   0
   End
End
Attribute VB_Name = "frmPeticionesShellView05"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const col_GESTION_CODDIRECCION = 0
Private Const col_GESTION_NOMDIRECCION = 1
Private Const col_GESTION_CODGERENCIA = 2
Private Const col_GESTION_NOMGERENCIA = 3
Private Const col_GESTION_CODSECTOR = 4
Private Const col_GESTION_NOMSECTOR = 5
Private Const col_GESTION_ID = 6
Private Const col_GESTION_CODRESPONSABLE = 7
Private Const col_GESTION_NOMRESPONSABLE = 8
Private Const col_TOTCOLS = 9

Private Sub Form_Load()
    Call CargarGrilla
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .visible = False
        .Clear
        .cols = col_TOTCOLS
        '.Rows = 2
        .HighLight = flexHighlightAlways
        '.BackColorFixed = Me.BackColor
        .FixedRows = 1
        .Rows = 0
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .SelectionMode = flexSelectionByRow
        
        
        
        .ColHeader(0) = flexColHeaderOn
        .ColHeaderCaption(0, 0) = "cod_direccion": .ColWidth(col_GESTION_CODDIRECCION, 0) = 0
        .ColHeaderCaption(0, 1) = "Direcci�n": .ColWidth(col_GESTION_NOMDIRECCION, 0) = 3500
        .ColHeaderCaption(0, 2) = "cod_gerencia": .ColWidth(col_GESTION_CODGERENCIA, 0) = 0
        .ColHeaderCaption(0, 3) = "Gerencia": .ColWidth(col_GESTION_NOMGERENCIA, 0) = 3500
        .ColHeaderCaption(0, 4) = "cod_sector": .ColWidth(col_GESTION_CODSECTOR, 0) = 0
        .ColHeaderCaption(0, 5) = "Sector": .ColWidth(col_GESTION_NOMSECTOR, 0) = 3500
        .ColHeaderCaption(0, 6) = "Admin.": .ColWidth(col_GESTION_ID, 0) = 3500
        .ColHeaderCaption(0, 7) = "cod_responsable": .ColWidth(col_GESTION_CODRESPONSABLE, 0) = 0
        .ColHeaderCaption(0, 8) = "Responsable": .ColWidth(col_GESTION_NOMRESPONSABLE, 0) = 3500
        
'        .TextMatrix(0, col_GESTION_CODDIRECCION) = "cod_direccion": .ColWidth(col_GESTION_CODDIRECCION) = 0: .ColAlignment(col_GESTION_CODDIRECCION) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_NOMDIRECCION) = "Direcci�n": .ColWidth(col_GESTION_NOMDIRECCION) = 3000: .ColAlignment(col_GESTION_NOMDIRECCION) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_CODGERENCIA) = "cod_gerencia": .ColWidth(col_GESTION_CODGERENCIA) = 0: .ColAlignment(col_GESTION_CODGERENCIA) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_NOMGERENCIA) = "Gerencia": .ColWidth(col_GESTION_NOMGERENCIA) = 3000: .ColAlignment(col_GESTION_NOMGERENCIA) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_CODSECTOR) = "cod_sector": .ColWidth(col_GESTION_CODSECTOR) = 0: .ColAlignment(col_GESTION_CODSECTOR) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_NOMSECTOR) = "Sector": .ColWidth(col_GESTION_NOMSECTOR) = 3000: .ColAlignment(col_GESTION_NOMSECTOR) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_ID) = "Admin.": .ColWidth(col_GESTION_ID) = 1000: .ColAlignment(col_GESTION_ID) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_CODRESPONSABLE) = "cod_responsable": .ColWidth(col_GESTION_CODRESPONSABLE) = 0: .ColAlignment(col_GESTION_CODRESPONSABLE) = flexAlignLeftCenter
'        .TextMatrix(0, col_GESTION_NOMRESPONSABLE) = "Responsable": .ColWidth(col_GESTION_NOMRESPONSABLE) = 3000: .ColAlignment(col_GESTION_NOMRESPONSABLE) = flexAlignLeftCenter
    End With
End Sub

Private Sub CargarGrilla()
    Dim i As Long
    
    Call InicializarGrilla
    With grdDatos
        If sp_GetGestionDemanda() Then
            '.row = 1
            .Rows = aplRST.RecordCount + 1
            Do While Not aplRST.EOF
                i = i + 1
                .TextMatrix(i, col_GESTION_CODDIRECCION) = ClearNull(aplRST!cod_direccion)
                .TextMatrix(i, col_GESTION_NOMDIRECCION) = ClearNull(aplRST!nom_direccion)
                .TextMatrix(i, col_GESTION_CODGERENCIA) = ClearNull(aplRST!cod_gerencia)
                .TextMatrix(i, col_GESTION_NOMGERENCIA) = ClearNull(aplRST!nom_gerencia)
                .TextMatrix(i, col_GESTION_CODSECTOR) = ClearNull(aplRST!cod_sector)
                .TextMatrix(i, col_GESTION_NOMSECTOR) = ClearNull(aplRST!nom_sector)
                .TextMatrix(i, col_GESTION_ID) = ClearNull(aplRST!cod_perfil)
                .TextMatrix(i, col_GESTION_CODRESPONSABLE) = ClearNull(aplRST!cod_bpar)
                .TextMatrix(i, col_GESTION_NOMRESPONSABLE) = ClearNull(aplRST!nom_bpar)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
        
    End With
End Sub

Private Sub Form_Resize()
    With grdDatos
        .Width = Me.ScaleWidth - 200
        .Height = Me.ScaleHeight - 200
    End With
End Sub
