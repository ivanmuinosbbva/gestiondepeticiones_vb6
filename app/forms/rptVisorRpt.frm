VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Begin VB.Form rptVisorRpt 
   Caption         =   "rptVisorRpt"
   ClientHeight    =   7095
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11040
   Icon            =   "rptVisorRpt.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7095
   ScaleWidth      =   11040
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer crViewer 
      Height          =   6975
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   10935
      lastProp        =   600
      _cx             =   19288
      _cy             =   12303
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   0   'False
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   0   'False
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
      EnableLogonPrompts=   -1  'True
   End
End
Attribute VB_Name = "rptVisorRpt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public crReport As CRAXDRT.Report

Public Sub CargarReporte()
    crViewer.ReportSource = crReport
    crViewer.DisplayGroupTree = False
    crViewer.ViewReport
    Me.Caption = "Informe: " & crReport.ReportTitle
    Call Puntero(False)
End Sub

Private Sub Form_Resize()
    With crViewer
        If Me.WindowState <> vbMinimized Then
            .Width = Me.ScaleWidth - 200
            .Height = Me.ScaleHeight - 200
        End If
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set crReport = Nothing
End Sub
