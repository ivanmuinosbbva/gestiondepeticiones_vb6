VERSION 5.00
Begin VB.Form auxEstadoSolicitud 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Estado de la petici�n"
   ClientHeight    =   3375
   ClientLeft      =   3345
   ClientTop       =   3765
   ClientWidth     =   4455
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "auxEstadoSolicitud.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3375
   ScaleWidth      =   4455
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraBotonera 
      Height          =   615
      Left            =   60
      TabIndex        =   6
      Top             =   2700
      Width           =   4335
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   3300
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   2340
         TabIndex        =   7
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Frame fraModoSolic 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   60
      TabIndex        =   1
      Top             =   540
      Width           =   4335
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Opini�n"
         Height          =   225
         Index           =   0
         Left            =   90
         TabIndex        =   5
         Top             =   300
         Width           =   1000
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Evaluaci�n"
         Height          =   225
         Index           =   1
         Left            =   90
         TabIndex        =   4
         Top             =   600
         Width           =   1335
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Estimaci�n"
         Height          =   225
         Index           =   2
         Left            =   90
         TabIndex        =   3
         Top             =   900
         Width           =   1335
      End
      Begin VB.OptionButton OptModoSolic 
         Caption         =   "Planificaci�n"
         Height          =   225
         Index           =   3
         Left            =   90
         TabIndex        =   2
         Top             =   1200
         Width           =   1335
      End
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Seleccione que tipo de requerimiento realizar�"
      ForeColor       =   &H00C00000&
      Height          =   195
      Left            =   90
      TabIndex        =   0
      Top             =   120
      Width           =   3315
   End
End
Attribute VB_Name = "auxEstadoSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Public bAceptar As Boolean
Public pubEstado As String
Public iniEstado As String

Private Sub Form_Load()
    fraModoSolic.Enabled = True
    bAceptar = False
    pubEstado = ""
    setOption
End Sub

Private Sub cmdAceptar_Click()
    pubEstado = getEstadoSolic
    bAceptar = True
    Me.Hide
End Sub

Private Sub cmdCancelar_Click()
    bAceptar = False
    Me.Hide
End Sub

Public Sub setOption()
    If Me.iniEstado = "REFBPE" Then
    'If Me.iniEstado = "COMITE" Then
    'If InStr(1, "COMITE|REFBPE|", Me.iniEstado, vbTextCompare) > 0 Then
        Me.OptModoSolic(0).Enabled = True
        Me.OptModoSolic(1).Enabled = True
        Me.OptModoSolic(2).Enabled = False
        Me.OptModoSolic(3).Enabled = False
        Me.OptModoSolic(0).value = True
    Else
        Me.OptModoSolic(0).Enabled = False
        Me.OptModoSolic(1).Enabled = False
        Me.OptModoSolic(2).Enabled = True
        Me.OptModoSolic(3).Enabled = True
        Me.OptModoSolic(2).value = True
    End If
End Sub

Function getEstadoSolic()
    getEstadoSolic = ""
    If OptModoSolic(0).value = True Then
        getEstadoSolic = "OPINIO"
    End If
    If OptModoSolic(1).value = True Then
        getEstadoSolic = "EVALUA"
    End If
    If OptModoSolic(2).value = True Then
        getEstadoSolic = "ESTIMA"
    End If
    If OptModoSolic(3).value = True Then
        getEstadoSolic = "PLANIF"
    End If
End Function
