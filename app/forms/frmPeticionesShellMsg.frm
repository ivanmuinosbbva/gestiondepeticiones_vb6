VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionesShellMsg 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consulta de mensajes"
   ClientHeight    =   7410
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11145
   ClipControls    =   0   'False
   Icon            =   "frmPeticionesShellMsg.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7410
   ScaleWidth      =   11145
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   675
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   9975
      Begin VB.ComboBox cboEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   960
         Visible         =   0   'False
         Width           =   5235
      End
      Begin VB.ComboBox cboPerfil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   240
         Width           =   5475
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Filtro por estado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   180
         Left            =   120
         TabIndex        =   13
         Top             =   1035
         Visible         =   0   'False
         Width           =   1230
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Filtro por mensaje"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   180
         Left            =   120
         TabIndex        =   12
         Top             =   675
         Visible         =   0   'False
         Width           =   1350
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Filtro por Perfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   180
         TabIndex        =   11
         Top             =   315
         Width           =   1050
      End
   End
   Begin VB.Frame fraRpt 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2115
      Left            =   120
      TabIndex        =   5
      Top             =   5280
      Width           =   9915
      Begin VB.TextBox txtMensaje1 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   360
         Width           =   9375
      End
      Begin VB.TextBox txtMensaje2 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   930
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   6
         Top             =   1080
         Width           =   9375
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   " Observaciones "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   7
         Top             =   0
         Width           =   1155
      End
   End
   Begin VB.Frame Frame2 
      Height          =   7395
      Left            =   10080
      TabIndex        =   1
      Top             =   0
      Width           =   1035
      Begin VB.CommandButton cmdFiltro 
         Caption         =   "Filtrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   15
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Elimina el/los Mensajes seleccionados"
         Top             =   2340
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Cierra esta consulta"
         Top             =   6960
         Width           =   885
      End
      Begin VB.CommandButton cmdVisualizar 
         Caption         =   "Acceder"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Acceder a la petici�n involucrada en este mensaje"
         Top             =   1920
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4530
      Left            =   60
      TabIndex        =   4
      Top             =   720
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   7990
      _Version        =   393216
      Cols            =   10
      FixedCols       =   0
      ForeColorSel    =   16777215
      BackColorBkg    =   -2147483633
      GridColor       =   14737632
      GridColorFixed  =   4210752
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   1
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPeticionesShellMsg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 26.05.2008 - Se modifica el ancho de las columnas y otros detalles est�ticos.
' -004- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -005- a. FJS 22.03.2010 - Se agrega un filtro por tipo de mensaje.
' -006- a. FJS 10.11.2010 - Se agregan la funcionalidad de buscar por columna en la grilla.
' -007- a. FJS 10.04.2015 - Nuevo: se agregan dos columnas: tipo y clase de la petici�n. Esto es para que Homologaci�n ubique facilmente las peticiones de las clases que le interesa.

Option Explicit

Dim sOpcionSeleccionada As String
Dim sEstadoQuery As String
Dim flgNumero As Boolean

Dim sNumeroMensaje
Dim sPerfil As String
Dim fPerfil As String

Const colFechaMsg = 0
Const colNomPerfil = 1
Const colNroAsignado = 2
'{ add -007- a.
Const colPetTipo = 3
Const colPetClase = 4
'}
Const colTitulo = 5
Const colTexto1 = 6
Const colTexto2 = 7
Const colNroInterno = 8
Const colNromensaje = 9
Const colFechaSrt = 10
Const colCODPERFIL = 11

Private Sub Form_Load()
    fPerfil = "NULL"
    sNumeroMensaje = ""
    Call InicializarCombos
    Call InicializarPantalla
    'Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)        ' add -004- a.
End Sub

Private Sub InicializarCombos()
    cboPerfil.Clear
    If sp_GetRecursoPerfil(glLOGIN_ID_REEMPLAZO, Null) Then
        Do Until aplRST.EOF
            cboPerfil.AddItem Trim(aplRST!nom_perfil) & Space(60) & "||" & aplRST!cod_perfil
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    cboPerfil.AddItem "Sin filtrar por perfil" & Space(60) & "||NULL", 0
    cboPerfil.ListIndex = 0
'    '{ add -005- a.
'    With cboMensaje
'        .Clear
'        If sp_GetMensajesTexto(Null) Then
'            Do Until aplRST.EOF
'                .AddItem Trim(aplRST!msg_texto) & Space(200) & "||" & aplRST!cod_txtmsg
'                aplRST.MoveNext
'                DoEvents
'            Loop
'            .ListIndex = 0
'        End If
'    End With
'
'    With cboEstado
'        .Clear
'        If sp_GetEstado(Null) Then
'            Do Until aplRST.EOF
'                .AddItem Trim(aplRST!nom_estado) & Space(100) & "||" & aplRST!cod_estado
'                aplRST.MoveNext
'                DoEvents
'            Loop
'            .ListIndex = 0
'        End If
'    End With
'    '}
End Sub

Sub InicializarPantalla()
    'Me.Caption = "Consulta de mensajes enviados por el sistema a " & sp_GetRecursoNombre(glLOGIN_ID_REEMPLAZO)
    Me.Caption = "Consulta de mensajes enviados por el sistema a " & glLOGIN_NAME_REEMPLAZO
    Me.Tag = ""
    Call CargarGrid
    Me.Top = 0
    Me.Left = 0
    'Me.Show
    'Me.visible = True
End Sub

Private Sub cmdEliminar_Click()
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    If grdDatos.row <= grdDatos.RowSel Then
        nRwD = grdDatos.row
        nRwH = grdDatos.RowSel
    Else
        nRwH = grdDatos.row
        nRwD = grdDatos.RowSel
    End If
     
    If nRwD <> nRwH Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        If MsgBox("�Desea eliminar los mensajes seleccionados?", vbYesNo) = vbNo Then
            Call LockProceso(False)
            Exit Sub
        End If
        Do While nRwD <= nRwH
            Call sp_DeleteMensaje(0, ClearNull(grdDatos.TextMatrix(nRwD, colNromensaje)))
            DoEvents
            nRwD = nRwD + 1
        Loop
        Call CargarGrid
        Exit Sub
    End If
    If sNumeroMensaje <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        If MsgBox("�Desea eliminar este mensaje?", vbYesNo) = vbNo Then
            Call LockProceso(False)
            Exit Sub
        End If
        Call sp_DeleteMensaje(0, sNumeroMensaje)
        Call CargarGrid
    End If
End Sub

Private Sub cmdVisualizar_Click()
    If glNumeroPeticion <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoPeticion = "VIEW"
        glUsrPerfilActual = sPerfil
        mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
        On Error Resume Next
        frmPeticionesCarga.Show vbModal
        Call LockProceso(False)
    End If
End Sub

Private Sub cmdFiltro_Click()
    If cboPerfil.ListIndex = -1 Then
        MsgBox ("Debe seleccionar un perfil")
        Exit Sub
    End If
    If Not LockProceso(True) Then
        Exit Sub
    End If
    fPerfil = CodigoCombo(cboPerfil, True)
    FiltrarGrid
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call LockProceso(False)
    'Unload Me
    Me.Hide
End Sub

Private Sub Form_Unload(Cancel As Integer)
    glNumeroPeticion = ""
    Call DetenerScroll(grdDatos)      ' add -004- a.
End Sub

Sub CargarGrid()
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
        
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    cmdVisualizar.Enabled = False
    With grdDatos
        .Clear
        DoEvents
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusNone
        .Rows = 1
        .cols = 12          ' add -007- a.
        .TextMatrix(0, colFechaMsg) = "Fecha": .ColWidth(colFechaMsg) = 1200
        .TextMatrix(0, colNroAsignado) = "Petici�n": .ColWidth(colNroAsignado) = 1000
        .TextMatrix(0, colNomPerfil) = "Perfil": .ColWidth(colNomPerfil) = 1300
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 5000: .ColAlignment(colTitulo) = flexAlignLeftCenter
        .TextMatrix(0, colNroInterno) = "NroInt.": .ColWidth(colNroInterno) = 0
        .TextMatrix(0, colNromensaje) = "NroMsg.": .ColWidth(colNromensaje) = 0
        '{ add -007- a.
        .TextMatrix(0, colPetTipo) = "Tpo.": .ColWidth(colPetTipo) = 600: .ColAlignment(colPetTipo) = flexAlignLeftCenter
        .TextMatrix(0, colPetClase) = "Clase": .ColWidth(colPetClase) = 600: .ColAlignment(colPetClase) = flexAlignLeftCenter
        '}
        .ColWidth(colFechaSrt) = 0
        .ColWidth(colTexto1) = 0
        .ColWidth(colTexto2) = 0
        .ColWidth(colCODPERFIL) = 0
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    Call Puntero(True)
    Call Status("Cargando mensajes...")
    If Not sp_GetMensajeRecurso(glLOGIN_ID_REEMPLAZO, "NULL", "NULL") Then
       GoTo finx
    End If
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colFechaMsg) = IIf(Not IsNull(aplRST!fe_mensaje), Format(aplRST!fe_mensaje, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colNomPerfil) = ClearNull(aplRST!nom_perfil)
            .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            '{ add -007- a.
            .TextMatrix(.Rows - 1, colPetTipo) = ClearNull(aplRST!cod_tipo_peticion)
            .TextMatrix(.Rows - 1, colPetClase) = ClearNull(aplRST!cod_clase)
            '}
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colNromensaje) = ClearNull(aplRST!msg_nrointerno)
            .TextMatrix(.Rows - 1, colFechaSrt) = IIf(Not IsNull(aplRST!fe_mensaje), "X" & Format(aplRST!fe_mensaje, "yyyymmdd") & ClearNull(aplRST!msg_nrointerno), "")
            .TextMatrix(.Rows - 1, colTexto1) = ClearNull(aplRST!msg_texto)
            .TextMatrix(.Rows - 1, colTexto2) = ClearNull(aplRST!txt_txtmsg)
            .TextMatrix(.Rows - 1, colCODPERFIL) = ClearNull(aplRST!cod_perfil)
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
        End With
        aplRST.MoveNext
    Loop
finx:
    Call FiltrarGrid
    Call MostrarSeleccion
    Call LockProceso(False)
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Public Sub MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    
    sNumeroMensaje = ""
    glNumeroPeticion = ""
    sPerfil = ""
    txtMensaje1.text = ""
    txtMensaje2.text = ""
    
    cmdVisualizar.Enabled = False
    cmdEliminar.Enabled = False
    
    'glNumeroPeticion = 0        ' add

    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
                sNumeroMensaje = .TextMatrix(.RowSel, colNromensaje)
                sPerfil = .TextMatrix(.RowSel, colCODPERFIL)
                txtMensaje1.text = .TextMatrix(.RowSel, colTexto1)
                txtMensaje2.text = .TextMatrix(.RowSel, colTexto2)
                cmdVisualizar.Enabled = True
                cmdEliminar.Enabled = True
            End If
        End If
    End With
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Sub FiltrarGrid()
    Dim nRows As Integer
    nRows = 1
    With grdDatos
        If .Rows > 1 Then
            Do While nRows < .Rows
                If fPerfil = "NULL" Then
                    .RowHeight(nRows) = -1
                ElseIf ClearNull(.TextMatrix(nRows, colCODPERFIL)) = fPerfil Then
                    .RowHeight(nRows) = -1
                Else
                    .RowHeight(nRows) = 0
                End If
                nRows = nRows + 1
            Loop
            '.RowSel = 0    ' del -003- a.
            Call MostrarSeleccion
        End If
    End With
    Call LockProceso(False)
    Call Puntero(False)
    Call Status("Listo")
End Sub

Private Sub grdDatos_Click()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           If .MouseCol = colFechaMsg Then
                .col = colFechaSrt
               .Sort = flexSortStringNoCaseDescending
           Else
                .col = .MouseCol
               .Sort = flexSortStringNoCaseAscending
           End If
        End If
    End With
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_DblClick()
    Call MostrarSeleccion
    DoEvents
    If glNumeroPeticion <> "" Then
        If Not LockProceso(True) Then
            Exit Sub
        End If
        glModoPeticion = "VIEW"
        glUsrPerfilActual = sPerfil
        mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
        On Error Resume Next
        frmPeticionesCarga.Show vbModal
        Call LockProceso(False)
    End If
End Sub

Private Sub grdDatos_SelChange()
    Call MostrarSeleccion
End Sub

'{ add -006- a.
Private Sub grdDatos_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        MenuGridFind grdDatos
    End If
End Sub
'}
