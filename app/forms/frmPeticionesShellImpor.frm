VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionesShellImpor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consulta Importaci�n de Peticiones"
   ClientHeight    =   6195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6195
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame2 
      Height          =   6165
      Left            =   8190
      TabIndex        =   3
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   5640
         Width           =   1170
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1155
      Left            =   10
      TabIndex        =   1
      Top             =   0
      Width           =   8145
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   315
         Left            =   4680
         TabIndex        =   2
         Top             =   300
         Width           =   1065
      End
      Begin AT_MaskText.MaskText txtFechaImport 
         Height          =   315
         Left            =   3120
         TabIndex        =   5
         ToolTipText     =   "Fecha en que se inici� la Peticion "
         Top             =   300
         Width           =   1530
         _ExtentX        =   2699
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Locked          =   -1  'True
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Importaci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   1260
         TabIndex        =   6
         Top             =   360
         Width           =   1620
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4950
      Left            =   -30
      TabIndex        =   0
      Top             =   1200
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   8731
      _Version        =   393216
      Cols            =   20
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPeticionesShellImpor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Private Const colNroAsignado = 0
Private Const colTitulo = 1
Private Const colSolicitante = 2
Private Const colPrioridad = 3
Private Const colESTADO = 4
Private Const colSituacion = 5
Private Const colEstHijo = 6
Private Const colSitHijo = 7
Private Const colCantNieto = 8
Private Const colNomHijo = 9
Private Const colEsfuerzo = 10
Private Const colFinicio = 11
Private Const colFtermin = 12
Private Const colNroInterno = 13
Private Const colEstCod = 14
Private Const colSitCod = 15
Private Const colCodigoRecurso = 16
Private Const colNota = 17
Private Const colDerivo = 18
Private Const colCodHijo = 19

Dim sOpcionSeleccionada As String
Dim sEstadoQuery As String
Dim flgNumero As Boolean
Dim xPerfil As String
Dim xNivel As String
Dim xArea As String
Dim sArea As String

Private Sub InicializarPantalla()
    Me.Tag = ""
    'Me.Top = 0
    'Me.Left = 0
    txtFechaImport.Text = Format(date, "dd/mm/yyyy")
    Call HabilitarBotones(0)
    Me.Show
End Sub

Private Sub cmdBuscar_Click()
    If IsNull(txtFechaImport.DateValue) Then
        Call status("Debe especificar fecha")
        Exit Sub
    End If
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glNumeroPeticion = ""
    CargarGrid
End Sub

Private Sub cmdCerrar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
   Unload Me
    Call LockProceso(False)
End Sub

Private Sub Form_Load()
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)        ' add -003- a.
End Sub

Private Sub Form_Unload(Cancel As Integer)
    glNumeroPeticion = ""
    modUser32.DetenerScroll grdDatos        ' add -003- a.
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    If IsMissing(vCargaGrid) Then
        CargarGrid
    Else
        Call MostrarSeleccion(False)
    End If
    Call LockProceso(False)
End Sub

Private Sub CargarGrid()
    Dim auxModoUsuario As Variant
    Dim vestados() As String
    Dim nElements, pElements As Integer
        
    glEstadoPeticion = ""
    auxModoUsuario = Null
    DoEvents
    cmdBuscar.Enabled = False
    With grdDatos
        .Clear
        DoEvents
        .HighLight = flexHighlightNever
        .FocusRect = flexFocusNone
        .Rows = 1
        .TextMatrix(0, colNroAsignado) = "Petici�n": .ColWidth(colNroAsignado) = 600
        .TextMatrix(0, colSolicitante) = "Solicitante": .ColWidth(colSolicitante) = 0
        .TextMatrix(0, colTitulo) = "T�tulo": .ColWidth(colTitulo) = 3800: .ColAlignment(colTitulo) = 0
        .TextMatrix(0, colESTADO) = "Estado Pet": .ColWidth(colESTADO) = 1500
        .TextMatrix(0, colSituacion) = "Situaci�n Pet": .ColWidth(colSituacion) = 1200
        .TextMatrix(0, colPrioridad) = "Pri.":
        .TextMatrix(0, colEsfuerzo) = "Hs Pet":
        .TextMatrix(0, colFinicio) = "F.Ini.Planif Pet":
        .TextMatrix(0, colFtermin) = "F.Fin Planif Pet":
        .TextMatrix(0, colNroInterno) = "NroInt.":
        .TextMatrix(0, colCantNieto) = "": .ColWidth(colCantNieto) = 0
        .ColWidth(colEstHijo) = 0
        .ColWidth(colSitHijo) = 0
        .ColWidth(colNomHijo) = 0
        .ColWidth(colCodHijo) = 0
        
        'If xPerfil = "CSEC" Or xPerfil = "CGCI" Or xPerfil = "CDIR" Then
        If InStr(1, "CSEC|CGCI|CDIR|", xPerfil, vbTextCompare) > 0 Then
            .ColWidth(colESTADO) = 0
            .ColWidth(colSituacion) = 0
            .ColWidth(colEstHijo) = 1500: .TextMatrix(0, colEstHijo) = "Est. Sector"
            .ColWidth(colSitHijo) = 1200: .TextMatrix(0, colSitHijo) = "Sit. Sector"
            .ColWidth(colNomHijo) = 1500: .TextMatrix(0, colNomHijo) = "Sector"
            .ColWidth(colCantNieto) = 400: .TextMatrix(0, colCantNieto) = "Grupos"
            .TextMatrix(0, colEsfuerzo) = "Hs Sec"
            .TextMatrix(0, colFinicio) = "F.Ini.Planif Sec"
            .TextMatrix(0, colFtermin) = "F.Fin Planif Sec"
        End If
        If xPerfil = "CGRU" Then
            .ColWidth(colESTADO) = 0
            .ColWidth(colSituacion) = 0
            .ColWidth(colEstHijo) = 1500: .TextMatrix(0, colEstHijo) = "Est. Grup."
            .ColWidth(colSitHijo) = 1200: .TextMatrix(0, colSitHijo) = "Sit. Grup."
            .ColWidth(colNomHijo) = 1500: .TextMatrix(0, colNomHijo) = "Grupo"
            .TextMatrix(0, colEsfuerzo) = "Hs Gru"
            .TextMatrix(0, colFinicio) = "F.Ini.Planif Gru"
            .TextMatrix(0, colFtermin) = "F.Fin Planif Gru"
        End If
        If xPerfil = "SOLI" Then
            .ColWidth(colSolicitante) = 1000
        End If
        .ColWidth(colPrioridad) = 400: .ColAlignment(colPrioridad) = 3
        .ColWidth(colEsfuerzo) = 600
        .ColWidth(colFinicio) = 1100
        .ColWidth(colFtermin) = 1100
        .ColWidth(colNroInterno) = 700
        .ColWidth(colEstCod) = 0
        .ColWidth(colSitCod) = 0
        .ColWidth(colCodigoRecurso) = 0
        .ColWidth(colDerivo) = 0
        .ColWidth(colNota) = 0
    End With
    CambiarEfectoLinea grdDatos, prmGridEffectFontBold
    
    Call Puntero(True)
    Call status("Cargando peticiones...")
    If Not sp_GetPeticionImport(txtFechaImport.DateValue) Then
       GoTo finx
    End If
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!titulo)
            .TextMatrix(.Rows - 1, colESTADO) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colEstHijo) = ClearNull(aplRST!nom_estado_hij)
            .TextMatrix(.Rows - 1, colSitHijo) = ClearNull(aplRST!nom_situacion_hij)
            .TextMatrix(.Rows - 1, colNomHijo) = ClearNull(aplRST!nom_area_hij)
            .TextMatrix(.Rows - 1, colCodHijo) = ClearNull(aplRST!cod_area_hij)
            .TextMatrix(.Rows - 1, colEsfuerzo) = ClearNull(aplRST!horaspresup)
            .TextMatrix(.Rows - 1, colFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colFtermin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colCodigoRecurso) = ClearNull(aplRST!cod_solicitante)
            .TextMatrix(.Rows - 1, colEstCod) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colSitCod) = ClearNull(aplRST!cod_situacion)
            .TextMatrix(.Rows - 1, colPrioridad) = ClearNull(aplRST!prioridad)
            .TextMatrix(.Rows - 1, colSolicitante) = ClearNull(aplRST!cod_solicitante)
        End With
        aplRST.MoveNext
    Loop
    'aplRST.Close
'    '{ add -002- a.
'    Dim i As Long
'
'    With grdDatos
'        .BackColorFixed = Me.BackColor
'        .BackColorSel = RGB(58, 110, 165)
'        .BackColorBkg = Me.BackColor
'        .Font.name = "Tahoma"
'        .Font.Size = 8
'        .Row = 0
'        For i = 0 To .Cols - 1
'            .Col = i
'            .CellFontBold = True
'        Next i
'        .FocusRect = flexFocusNone
'    End With
'    '}
finx:
    With grdDatos
        If .Rows > 1 Then
            .TopRow = 1
            .Row = 1
            .RowSel = 1
            .Col = 0
            .ColSel = .Cols - 1
            Call MostrarSeleccion(False)
        End If
    End With
    cmdBuscar.Enabled = True
    Call Puntero(False)
    Call status("Listo.")
    Call LockProceso(False)
End Sub

Public Sub MostrarSeleccion(Optional flgSort)
    If IsMissing(flgSort) Then
        flgSort = True
    End If
    sArea = ""
    With grdDatos
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                glNroAsignado = .TextMatrix(.RowSel, colNroAsignado)
                glNumeroPeticion = .TextMatrix(.RowSel, colNroInterno)
                glCodigoRecurso = .TextMatrix(.RowSel, colCodigoRecurso)
                glEstadoPeticion = .TextMatrix(.RowSel, colEstCod)
                sArea = .TextMatrix(.RowSel, colCodHijo)
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    With grdDatos
    If .MouseRow = 0 And .Rows > 1 Then
       .RowSel = 1
       .Col = .MouseCol
       .Sort = flexSortStringNoCaseAscending
    End If
    End With
    Call MostrarSeleccion
    DoEvents
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

'Private Sub grdDatos_DblClick()
'    Call MostrarSeleccion
'    DoEvents
'    If glNumeroPeticion <> "" Then
'        If Not LockProceso(True) Then
'            Exit Sub
'        End If
'        glModoPeticion = "VIEW"
'        On Error Resume Next
'        frmPeticionesCarga.Show vbModal
'        DoEvents
'        If Me.Tag = "REFRESH" Then
'             InicializarPantalla
'        End If
'    End If
'End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub
