VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesCargaModif 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " "
   ClientHeight    =   8220
   ClientLeft      =   915
   ClientTop       =   2835
   ClientWidth     =   11880
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPeticionesCargaModif.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   8220
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   Tag             =   "Datos Petici�n: N�"
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   0
      Left            =   0
      TabIndex        =   110
      Top             =   435
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   6
      Left            =   7800
      TabIndex        =   188
      Top             =   435
      Width           =   1470
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F7 - Control"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   6
      Left            =   7920
      Style           =   1  'Graphical
      TabIndex        =   187
      Top             =   60
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   5
      Left            =   6540
      TabIndex        =   132
      Top             =   435
      Width           =   1470
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   4
      Left            =   5280
      TabIndex        =   115
      Top             =   435
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   3
      Left            =   4020
      TabIndex        =   109
      Top             =   435
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   2
      Left            =   2640
      TabIndex        =   112
      Top             =   435
      Width           =   1335
   End
   Begin VB.Frame orjFiller 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   100
      Index           =   1
      Left            =   1380
      TabIndex        =   111
      Top             =   435
      Width           =   1230
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F6 - Conformes"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   5
      Left            =   6600
      Style           =   1  'Graphical
      TabIndex        =   133
      Top             =   60
      Width           =   1335
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F5 - Adjuntos"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   4
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   116
      Top             =   60
      Width           =   1335
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F4 - Doc.Metod."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   3
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   94
      Top             =   60
      Width           =   1335
   End
   Begin VB.CommandButton orjBtn 
      Appearance      =   0  'Flat
      Caption         =   "F3 - Anexas"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   2
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   80
      Top             =   60
      Width           =   1335
   End
   Begin VB.CommandButton orjBtn 
      Caption         =   "F2 - Detalle"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   1
      Left            =   1320
      MaskColor       =   &H00E0E0E0&
      Style           =   1  'Graphical
      TabIndex        =   74
      Top             =   60
      Width           =   1335
   End
   Begin VB.CommandButton orjBtn 
      Caption         =   "F1 - Datos"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   73
      Top             =   60
      Width           =   1335
   End
   Begin VB.Frame fraButtPpal 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8160
      Left            =   10320
      TabIndex        =   50
      Top             =   0
      Width           =   1515
      Begin VB.CommandButton cmdAsigNro 
         Caption         =   "Asig. N�mero"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   40
         TabIndex        =   198
         TabStop         =   0   'False
         ToolTipText     =   "Invoca el reporte de Horas rabajadas"
         Top             =   4560
         Visible         =   0   'False
         Width           =   1410
      End
      Begin VB.CommandButton cmdPlanificar 
         Caption         =   "Planificaci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         Picture         =   "frmPeticionesCargaModif.frx":000C
         TabIndex        =   155
         TabStop         =   0   'False
         ToolTipText     =   "Permite realizar una validaci�n ad-hoc de integridad para la petici�n actual"
         Top             =   2070
         Visible         =   0   'False
         Width           =   1410
      End
      Begin VB.CommandButton cmdGrupos 
         Caption         =   "Grupos"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         TabIndex        =   93
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   1640
         Width           =   1410
      End
      Begin VB.CommandButton cmdRptHs 
         Caption         =   "Horas trabajadas"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   40
         TabIndex        =   92
         TabStop         =   0   'False
         ToolTipText     =   "Invoca el reporte de Horas rabajadas"
         Top             =   4120
         Width           =   1410
      End
      Begin VB.CommandButton cmdAgrupar 
         Caption         =   "Agrupamientos"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   40
         TabIndex        =   86
         TabStop         =   0   'False
         ToolTipText     =   "Visualiza los agrupamientos que referencian esta Petici�n"
         Top             =   600
         Width           =   1410
      End
      Begin VB.CommandButton cmdSector 
         Caption         =   "Sectores"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         TabIndex        =   57
         TabStop         =   0   'False
         ToolTipText     =   "Muestra Sector/Grupo interviniente"
         Top             =   1200
         Width           =   1410
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         TabIndex        =   56
         TabStop         =   0   'False
         ToolTipText     =   "Permite completar los datos de la Petici�n"
         Top             =   5310
         Width           =   1410
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Imprimir"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         TabIndex        =   55
         TabStop         =   0   'False
         ToolTipText     =   "Imprime un formulario con los datos de la petici�n"
         Top             =   6210
         Width           =   1410
      End
      Begin VB.CommandButton cmdCambioEstado 
         Caption         =   "Cambio de estado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         TabIndex        =   54
         TabStop         =   0   'False
         ToolTipText     =   "Permite cambiar el estado de la Petici�n"
         Top             =   3120
         Width           =   1410
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Guardar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         TabIndex        =   53
         TabStop         =   0   'False
         ToolTipText     =   "Confirma y guarda los cambios realizados"
         Top             =   5760
         Width           =   1410
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   40
         TabIndex        =   52
         TabStop         =   0   'False
         ToolTipText     =   "Terminar la operaci�n"
         Top             =   7640
         Width           =   1410
      End
      Begin VB.CommandButton cmdHistView 
         Caption         =   "Historial"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   40
         TabIndex        =   51
         TabStop         =   0   'False
         ToolTipText     =   "Muestra el historial de la Petici�n"
         Top             =   3720
         Width           =   1410
      End
      Begin VB.Label lblModo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   142
         TabIndex        =   168
         Top             =   330
         Width           =   1200
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "Modo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   60
         TabIndex        =   167
         Top             =   120
         Width           =   1395
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   5
      Left            =   0
      TabIndex        =   134
      Top             =   360
      Width           =   10305
      Begin VB.CommandButton cmdDeshabilitarManual 
         Caption         =   "Deshabilitar manual"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   201
         TabStop         =   0   'False
         ToolTipText     =   "Deshabilitar manualmente la validez de la petici�n para pasajes a Producci�n"
         Top             =   360
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.Frame fraConformeHomo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2160
         Left            =   120
         TabIndex        =   156
         Top             =   5280
         Width           =   8775
         Begin VB.CommandButton cmdHomoCancel 
            Caption         =   "Cancelar"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7680
            TabIndex        =   165
            TabStop         =   0   'False
            Top             =   1560
            Width           =   930
         End
         Begin VB.CommandButton cmdHomoConf 
            Caption         =   "Confirmar"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7680
            TabIndex        =   164
            TabStop         =   0   'False
            Top             =   1200
            Width           =   930
         End
         Begin VB.CommandButton cmdHomoDel 
            Caption         =   "Quitar"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7680
            TabIndex        =   163
            TabStop         =   0   'False
            Top             =   840
            Width           =   930
         End
         Begin VB.CommandButton cmdHomoAdd 
            Caption         =   "Agregar"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   7680
            TabIndex        =   162
            TabStop         =   0   'False
            Top             =   480
            Width           =   930
         End
         Begin VB.TextBox pcfTextoHomo 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   795
            Left            =   120
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   158
            Top             =   1200
            Width           =   7305
         End
         Begin VB.ComboBox cboPcfTipoHomo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmPeticionesCargaModif.frx":0156
            Left            =   120
            List            =   "frmPeticionesCargaModif.frx":0158
            Style           =   2  'Dropdown List
            TabIndex        =   157
            Top             =   480
            Width           =   7305
         End
         Begin VB.Label lblHomologacion 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   " Homologaci�n "
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   -1  'True
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   240
            Left            =   6885
            TabIndex        =   166
            Top             =   0
            Width           =   1665
         End
         Begin VB.Label lblPetConfHomo 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   161
            Top             =   30
            Visible         =   0   'False
            Width           =   105
         End
         Begin VB.Label lblcboPcfTipoHomo 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de conforme"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   160
            Top             =   240
            Width           =   1290
         End
         Begin VB.Label lblpcfTextoHomo 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   159
            Top             =   960
            Width           =   1080
         End
      End
      Begin VB.CommandButton pcfMod 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   145
         TabStop         =   0   'False
         Top             =   4350
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.Frame fraConforme 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1920
         Left            =   90
         TabIndex        =   135
         Top             =   3330
         Width           =   8775
         Begin VB.ComboBox cboPcfTipo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmPeticionesCargaModif.frx":015A
            Left            =   1800
            List            =   "frmPeticionesCargaModif.frx":015C
            Style           =   2  'Dropdown List
            TabIndex        =   152
            Top             =   300
            Width           =   5565
         End
         Begin VB.TextBox pcfTexto 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   1800
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   151
            Top             =   1080
            Width           =   5565
         End
         Begin VB.ComboBox cboPcfModo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmPeticionesCargaModif.frx":015E
            Left            =   1800
            List            =   "frmPeticionesCargaModif.frx":0160
            Style           =   2  'Dropdown List
            TabIndex        =   150
            Top             =   690
            Width           =   5565
         End
         Begin VB.Label Label40 
            AutoSize        =   -1  'True
            Caption         =   "Req."
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   146
            Top             =   720
            Width           =   330
         End
         Begin VB.Label Label36 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   144
            Top             =   1080
            Width           =   930
         End
         Begin VB.Label Label28 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de conforme"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   143
            Top             =   330
            Width           =   1290
         End
         Begin VB.Label lblPetConf 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   136
            Top             =   30
            Visible         =   0   'False
            Width           =   105
         End
      End
      Begin VB.CommandButton pcfCan 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   137
         TabStop         =   0   'False
         Top             =   5880
         Width           =   1170
      End
      Begin VB.CommandButton pcfCon 
         Caption         =   "Confirmar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   138
         TabStop         =   0   'False
         Top             =   5370
         Width           =   1170
      End
      Begin VB.CommandButton pcfDel 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   139
         TabStop         =   0   'False
         Top             =   4860
         Width           =   1170
      End
      Begin VB.CommandButton pcfAdd 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   140
         TabStop         =   0   'False
         Top             =   3840
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdPetConf 
         Height          =   3030
         Left            =   90
         TabIndex        =   141
         Top             =   300
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   5345
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label lblMensajeNoConformes 
         AutoSize        =   -1  'True
         Caption         =   "La petici�n se encuentra en alguno de los estados terminales, por lo cual no es posible modificar conformes."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   177
         Top             =   7560
         Visible         =   0   'False
         Width           =   8100
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   0
      Left            =   0
      TabIndex        =   46
      Top             =   360
      Width           =   10305
      Begin VB.ComboBox cboRO 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6240
         Style           =   2  'Dropdown List
         TabIndex        =   200
         Top             =   3120
         Width           =   1380
      End
      Begin VB.ComboBox cboEmp 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCargaModif.frx":0162
         Left            =   8700
         List            =   "frmPeticionesCargaModif.frx":016F
         Style           =   2  'Dropdown List
         TabIndex        =   197
         Top             =   960
         Width           =   1395
      End
      Begin VB.CommandButton cmdEraseProyectos 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2680
         Picture         =   "frmPeticionesCargaModif.frx":017F
         Style           =   1  'Graphical
         TabIndex        =   185
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto para asociar a la petici�n"
         Top             =   1680
         Width           =   350
      End
      Begin AT_MaskText.MaskText txtPrjSubsId 
         Height          =   315
         Left            =   2280
         TabIndex        =   12
         Top             =   1680
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjSubId 
         Height          =   315
         Left            =   1920
         TabIndex        =   11
         Top             =   1680
         Width           =   360
         _ExtentX        =   635
         _ExtentY        =   556
         MaxLength       =   2
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   2
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtPrjId 
         Height          =   315
         Left            =   1080
         TabIndex        =   10
         Top             =   1680
         Width           =   840
         _ExtentX        =   1482
         _ExtentY        =   556
         MaxLength       =   11
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   11
         Align           =   1
         DecimalPlaces   =   0
         DataType        =   2
         NegativeValues  =   0   'False
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin VB.CommandButton cmdAyudaClase 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4680
         Picture         =   "frmPeticionesCargaModif.frx":04C1
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Abre la Petici�n que agrupa a la actual"
         Top             =   600
         Width           =   345
      End
      Begin VB.CommandButton cmdSelProyectos 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3050
         Picture         =   "frmPeticionesCargaModif.frx":0803
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar un proyecto para asociar a la petici�n"
         Top             =   1680
         Width           =   350
      End
      Begin VB.ComboBox cboSector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   28
         ToolTipText     =   "Sector Solicitante"
         Top             =   4560
         Width           =   9130
      End
      Begin VB.ComboBox cboRegulatorio 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCargaModif.frx":0B45
         Left            =   1080
         List            =   "frmPeticionesCargaModif.frx":0B52
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   960
         Width           =   795
      End
      Begin VB.ComboBox cboImpacto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCargaModif.frx":0B62
         Left            =   9300
         List            =   "frmPeticionesCargaModif.frx":0B6F
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   600
         Width           =   795
      End
      Begin VB.ComboBox cboClase 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5040
         Style           =   2  'Dropdown List
         TabIndex        =   4
         ToolTipText     =   "Indica la clase de la petici�n actual"
         Top             =   600
         Width           =   2775
      End
      Begin VB.ComboBox cboTipopet 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   2
         ToolTipText     =   "Tipo de Petici�n"
         Top             =   600
         Width           =   2340
      End
      Begin VB.ComboBox cboImportancia 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   1312
         Width           =   2340
      End
      Begin VB.ComboBox cboBpar 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   36
         ToolTipText     =   "Recurso que actu� como Business Partner"
         Top             =   6720
         Width           =   6120
      End
      Begin VB.ComboBox cboOrientacion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5040
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   2040
         Width           =   5085
      End
      Begin VB.ComboBox cboCorpLocal 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   2040
         Width           =   2340
      End
      Begin VB.CommandButton cmdVerAnexora 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9850
         Picture         =   "frmPeticionesCargaModif.frx":0B7F
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Abre la Petici�n que agrupa a la actual"
         Top             =   2760
         Width           =   350
      End
      Begin VB.ComboBox cboPrioridad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmPeticionesCargaModif.frx":0EC1
         Left            =   5040
         List            =   "frmPeticionesCargaModif.frx":0EC3
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   960
         Width           =   2775
      End
      Begin VB.ComboBox cboSupervisor 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   34
         ToolTipText     =   "Recurso que actu� como Supervisor"
         Top             =   6000
         Width           =   6120
      End
      Begin VB.ComboBox cboDirector 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   35
         ToolTipText     =   "Recurso que actu� como Autorizante"
         Top             =   6360
         Width           =   6120
      End
      Begin VB.ComboBox cboSolicitante 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   31
         ToolTipText     =   "Recurso que actu� como Solicitante"
         Top             =   5280
         Width           =   6120
      End
      Begin VB.ComboBox cboReferente 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   33
         ToolTipText     =   "Recurso que actu� como Referente"
         Top             =   5640
         Width           =   6120
      End
      Begin AT_MaskText.MaskText txtFechaEstado 
         Height          =   315
         Left            =   6240
         TabIndex        =   18
         ToolTipText     =   "Fecha en la que ingres� al estado actual"
         Top             =   2760
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
      End
      Begin AT_MaskText.MaskText txtTitulo 
         Height          =   315
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   556
         MaxLength       =   120
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   120
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtEstado 
         Height          =   315
         Left            =   1080
         TabIndex        =   17
         ToolTipText     =   "Estado en que se encuentra la Petici�n"
         Top             =   2760
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin AT_MaskText.MaskText txtSituacion 
         Height          =   315
         Left            =   1080
         TabIndex        =   21
         ToolTipText     =   "Situaci�n particular"
         Top             =   3120
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin AT_MaskText.MaskText txtFiniplan 
         Height          =   315
         Left            =   1080
         TabIndex        =   23
         ToolTipText     =   "Fecha de Inicio Planificada"
         Top             =   3480
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFinireal 
         Height          =   315
         Left            =   1080
         TabIndex        =   26
         ToolTipText     =   "Fecha de Inicio Real"
         Top             =   3840
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFfinplan 
         Height          =   315
         Left            =   3600
         TabIndex        =   24
         ToolTipText     =   "Fecha de finalizaci�n Planificada"
         Top             =   3480
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFfinreal 
         Height          =   315
         Left            =   3600
         TabIndex        =   27
         ToolTipText     =   "Fecha de Finalizaci�n Real"
         Top             =   3840
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtHspresup 
         Height          =   315
         Left            =   6240
         TabIndex        =   22
         ToolTipText     =   "Esfuerzo presupuestado"
         Top             =   3480
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtFechaComite 
         Height          =   315
         Left            =   8760
         TabIndex        =   37
         ToolTipText     =   "Fecha en que se remiti� al Business Partner"
         Top             =   6720
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaPedido 
         Height          =   315
         Left            =   8760
         TabIndex        =   30
         ToolTipText     =   "Fecha en que se inici� la Peticion "
         Top             =   4920
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         Locked          =   -1  'True
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNroAsignado 
         Height          =   315
         Left            =   9000
         TabIndex        =   38
         ToolTipText     =   "N�mero asignado por el administrador"
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtNroAnexada 
         Height          =   315
         Left            =   8640
         TabIndex        =   19
         ToolTipText     =   "Petici�n a la cual est� anexada"
         Top             =   2760
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   556
         MaxLength       =   30
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         AutoSelect      =   0   'False
      End
      Begin AT_MaskText.MaskText txtFechaRequerida 
         Height          =   315
         Left            =   8760
         TabIndex        =   32
         Top             =   5280
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         MaxLength       =   10
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtUsualta 
         Height          =   315
         Left            =   1080
         TabIndex        =   29
         Top             =   4920
         Width           =   6120
         _ExtentX        =   10795
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         AutoSelect      =   0   'False
      End
      Begin AT_MaskText.MaskText txtusrImportancia 
         Height          =   315
         Left            =   5040
         TabIndex        =   9
         Top             =   1320
         Width           =   5055
         _ExtentX        =   8916
         _ExtentY        =   556
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Validation      =   0   'False
         DecimalPlaces   =   0
      End
      Begin AT_MaskText.MaskText txtHsPlanif 
         Height          =   315
         Left            =   6240
         TabIndex        =   25
         ToolTipText     =   "Cantidad total de planificaciones"
         Top             =   3840
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin AT_MaskText.MaskText txtPrjNom 
         Height          =   315
         Left            =   5040
         TabIndex        =   14
         Top             =   1680
         Width           =   5055
         _ExtentX        =   8916
         _ExtentY        =   556
         MaxLength       =   50
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         AutoSelect      =   0   'False
         TabOnEnter      =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNrointerno 
         Height          =   315
         Left            =   9000
         TabIndex        =   1
         Top             =   240
         Visible         =   0   'False
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   556
         MaxLength       =   8
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         Locked          =   -1  'True
         Align           =   1
         DecimalPlaces   =   0
         UpperCase       =   -1  'True
         DataType        =   2
      End
      Begin VB.Label lblRO 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "Vinculada a RO / Criticidad"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4680
         TabIndex        =   199
         Top             =   3075
         Width           =   1425
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label54 
         AutoSize        =   -1  'True
         Caption         =   "Empresa"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   7920
         TabIndex        =   196
         Top             =   1035
         Width           =   660
      End
      Begin VB.Image imgControlPasajeAProd 
         Height          =   240
         Left            =   9600
         Picture         =   "frmPeticionesCargaModif.frx":0EC5
         Top             =   7440
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label lblPeticionNumero 
         AutoSize        =   -1  'True
         Caption         =   "N� Asignado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   7920
         TabIndex        =   184
         Top             =   300
         Width           =   945
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00808080&
         X1              =   120
         X2              =   10200
         Y1              =   7200
         Y2              =   7200
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Sector solicitante"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   90
         TabIndex        =   182
         Top             =   4515
         Width           =   855
         WordWrap        =   -1  'True
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00808080&
         X1              =   120
         X2              =   10200
         Y1              =   4320
         Y2              =   4320
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00808080&
         X1              =   120
         X2              =   10200
         Y1              =   2520
         Y2              =   2520
      End
      Begin VB.Label Label50 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo del proyecto"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3550
         TabIndex        =   181
         Top             =   1740
         Width           =   1410
      End
      Begin VB.Label Label49 
         AutoSize        =   -1  'True
         Caption         =   "Proyecto IDM"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   180
         Top             =   1665
         Width           =   690
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label48 
         AutoSize        =   -1  'True
         Caption         =   "Regulatorio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   179
         Top             =   1035
         Width           =   855
      End
      Begin VB.Label Label44 
         AutoSize        =   -1  'True
         Caption         =   "Cant. Planif."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   5160
         TabIndex        =   169
         ToolTipText     =   "Cantidad total de planificaciones"
         Top             =   3900
         Width           =   915
      End
      Begin VB.Image imgTrazabilidad 
         Height          =   240
         Left            =   9960
         Picture         =   "frmPeticionesCargaModif.frx":1207
         Top             =   7450
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label lblPetEquipo 
         AutoSize        =   -1  'True
         Caption         =   "Impacto tecnol�gico"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7920
         TabIndex        =   149
         Top             =   585
         Width           =   1020
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblPetClass 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3550
         TabIndex        =   148
         Top             =   675
         Width           =   435
      End
      Begin VB.Label Label41 
         AutoSize        =   -1  'True
         Caption         =   "B. Partner"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   90
         TabIndex        =   147
         Top             =   6795
         Width           =   750
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         Caption         =   "Orientaci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3550
         TabIndex        =   91
         Top             =   2115
         Width           =   885
      End
      Begin VB.Label Label33 
         AutoSize        =   -1  'True
         Caption         =   "Corp/Local"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   90
         Top             =   2115
         Width           =   825
      End
      Begin VB.Label Label32 
         AutoSize        =   -1  'True
         Caption         =   "Asignada por"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3550
         TabIndex        =   89
         Top             =   1387
         Width           =   990
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         Caption         =   "Visibilidad"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   88
         Top             =   1387
         Width           =   795
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "F. Fin Planif."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   2640
         TabIndex        =   87
         Top             =   3540
         Width           =   915
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Usuario alta"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   90
         TabIndex        =   85
         Top             =   4897
         Width           =   675
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Fecha deseada"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   7320
         TabIndex        =   84
         Top             =   5347
         Width           =   1125
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Anexada a:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   7680
         TabIndex        =   79
         Top             =   2827
         Width           =   855
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   3550
         TabIndex        =   78
         Top             =   1035
         Width           =   675
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de alta real"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   7320
         TabIndex        =   77
         Top             =   4987
         Width           =   1335
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Fecha env. BP."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   7320
         TabIndex        =   76
         Top             =   6795
         Width           =   1110
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   75
         Top             =   675
         Width           =   330
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "F. Ini. Planif."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   72
         Top             =   3540
         Width           =   945
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "F. Fin Real"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   2640
         TabIndex        =   71
         Top             =   3900
         Width           =   780
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "F. Ini. Real"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   70
         Top             =   3900
         Width           =   810
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Hs. Presup."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   5160
         TabIndex        =   69
         ToolTipText     =   "Esfuerzo presupuestado"
         Top             =   3540
         Width           =   870
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Solicitante"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   90
         TabIndex        =   62
         Top             =   5355
         Width           =   810
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Supervisor"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   90
         TabIndex        =   61
         Top             =   6075
         Width           =   810
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Referente"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   90
         TabIndex        =   60
         Top             =   5715
         Width           =   720
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Autorizante"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   90
         TabIndex        =   59
         Top             =   6435
         Width           =   870
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Situaci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   58
         Top             =   3187
         Width           =   705
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "F.Estado Act."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   4680
         TabIndex        =   49
         Top             =   2827
         Width           =   1020
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "T�tulo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   48
         Top             =   307
         Width           =   435
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   47
         Top             =   2827
         Width           =   525
      End
      Begin VB.Label lblCustomToolTip 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   400
         Left            =   120
         TabIndex        =   183
         Top             =   7320
         Width           =   9015
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   1
      Left            =   0
      TabIndex        =   45
      Top             =   360
      Width           =   10305
      Begin VB.TextBox memCaracteristicas 
         Height          =   1155
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   40
         Top             =   1680
         Width           =   7000
      End
      Begin VB.TextBox memRelaciones 
         Height          =   1155
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   43
         Top             =   5280
         Width           =   7000
      End
      Begin VB.TextBox memObservaciones 
         Height          =   1155
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   44
         Top             =   6480
         Width           =   7000
      End
      Begin VB.TextBox memMotivos 
         Height          =   1155
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   41
         Top             =   2880
         Width           =   7000
      End
      Begin VB.TextBox memBeneficios 
         Height          =   1155
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   42
         Top             =   4080
         Width           =   7000
      End
      Begin VB.TextBox memDescripcion 
         Height          =   1155
         Left            =   2200
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   39
         Top             =   480
         Width           =   7000
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00808080&
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n del pedido"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   480
         TabIndex        =   68
         Top             =   480
         Width           =   1650
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Caracter�sticas principales"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   195
         TabIndex        =   67
         Top             =   1680
         Width           =   1860
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         Caption         =   "Motivos"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   195
         TabIndex        =   66
         Top             =   2880
         Width           =   1860
      End
      Begin VB.Label Label21 
         Alignment       =   1  'Right Justify
         Caption         =   "Beneficios esperados"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   240
         TabIndex        =   65
         Top             =   4080
         Width           =   1860
      End
      Begin VB.Label Label22 
         Alignment       =   1  'Right Justify
         Caption         =   "Relaci�n con otros requerimientos"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   195
         TabIndex        =   64
         Top             =   5280
         Width           =   1860
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   195
         TabIndex        =   63
         Top             =   6480
         Width           =   1860
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   3
      Left            =   0
      TabIndex        =   95
      Top             =   360
      Width           =   10305
      Begin VB.Frame fraDocMet 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   60
         TabIndex        =   103
         Top             =   3330
         Width           =   8805
         Begin VB.TextBox docFile 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   114
            Top             =   315
            Width           =   7095
         End
         Begin VB.TextBox docPath 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   113
            Top             =   679
            Width           =   7095
         End
         Begin VB.CommandButton docSel 
            Caption         =   "..."
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   8115
            TabIndex        =   108
            TabStop         =   0   'False
            Top             =   315
            Width           =   330
         End
         Begin VB.TextBox docComent 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1200
            Left            =   1350
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   107
            Top             =   1050
            Width           =   7095
         End
         Begin VB.Label Label42 
            AutoSize        =   -1  'True
            Caption         =   "Archivo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   153
            Top             =   360
            Width           =   570
         End
         Begin VB.Label lblDocMetod 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   106
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label Label39 
            AutoSize        =   -1  'True
            Caption         =   "Ubicaci�n"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   105
            Top             =   690
            Width           =   705
         End
         Begin VB.Label Label38 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   104
            Top             =   1050
            Width           =   930
         End
      End
      Begin VB.CommandButton docCan 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   102
         TabStop         =   0   'False
         Top             =   5880
         Width           =   1170
      End
      Begin VB.CommandButton docCon 
         Caption         =   "Confirmar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   101
         TabStop         =   0   'False
         Top             =   5370
         Width           =   1170
      End
      Begin VB.CommandButton docMod 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   100
         TabStop         =   0   'False
         Top             =   4350
         Width           =   1170
      End
      Begin VB.CommandButton docDel 
         Caption         =   "Desvincular"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   99
         TabStop         =   0   'False
         Top             =   4860
         Width           =   1170
      End
      Begin VB.CommandButton docAdd 
         Caption         =   "Vincular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   98
         TabStop         =   0   'False
         Top             =   3840
         Width           =   1170
      End
      Begin VB.CommandButton docView 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         Picture         =   "frmPeticionesCargaModif.frx":1549
         Style           =   1  'Graphical
         TabIndex        =   96
         TabStop         =   0   'False
         ToolTipText     =   "Ver docum."
         Top             =   360
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdDocMetod 
         Height          =   3030
         Left            =   90
         TabIndex        =   97
         Top             =   300
         Width           =   8805
         _ExtentX        =   15531
         _ExtentY        =   5345
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Label lblMensajeNoVinculados 
         AutoSize        =   -1  'True
         Caption         =   "La petici�n se encuentra en alguno de los estados terminales, por lo cual no es posible agregar o remover vinculos a documentos."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   178
         Top             =   7560
         Visible         =   0   'False
         Width           =   9720
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   4
      Left            =   0
      TabIndex        =   117
      Top             =   360
      Width           =   10305
      Begin VB.CheckBox chkZipFileWhileLoad 
         Caption         =   "Comprimir"
         Height          =   255
         Left            =   9000
         TabIndex        =   195
         Top             =   6480
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdComprimir 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         Picture         =   "frmPeticionesCargaModif.frx":188B
         Style           =   1  'Graphical
         TabIndex        =   186
         Top             =   840
         Visible         =   0   'False
         Width           =   1170
      End
      Begin VB.CommandButton adjCan 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   125
         TabStop         =   0   'False
         Top             =   5880
         Width           =   1170
      End
      Begin VB.CommandButton adjCon 
         Caption         =   "Confirmar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   126
         TabStop         =   0   'False
         Top             =   5370
         Width           =   1170
      End
      Begin VB.CommandButton adjMod 
         Caption         =   "Modificar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   127
         TabStop         =   0   'False
         Top             =   4350
         Width           =   1170
      End
      Begin VB.CommandButton adjDel 
         Caption         =   "Desvincular"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   128
         TabStop         =   0   'False
         Top             =   4860
         Width           =   1170
      End
      Begin VB.CommandButton adjAdd 
         Caption         =   "Adjuntar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   129
         TabStop         =   0   'False
         Top             =   3840
         Width           =   1170
      End
      Begin VB.CommandButton adjView 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         Picture         =   "frmPeticionesCargaModif.frx":1BCD
         Style           =   1  'Graphical
         TabIndex        =   130
         TabStop         =   0   'False
         ToolTipText     =   "Ver adjunto"
         Top             =   360
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdAdjPet 
         Height          =   3030
         Left            =   90
         TabIndex        =   131
         Top             =   300
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   5345
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
      Begin VB.Frame fraAdjPet 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3840
         Left            =   60
         TabIndex        =   118
         Top             =   3330
         Width           =   8775
         Begin VB.TextBox txtAdjSize 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            TabIndex        =   174
            Top             =   1800
            Width           =   1575
         End
         Begin VB.TextBox txtAdjGrupo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            TabIndex        =   173
            Top             =   1440
            Width           =   4095
         End
         Begin VB.TextBox txtAdjSector 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            TabIndex        =   172
            Top             =   1080
            Width           =   4095
         End
         Begin VB.TextBox adjFile 
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   119
            Top             =   315
            Width           =   6975
         End
         Begin VB.TextBox adjComent 
            Height          =   1320
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   121
            Top             =   2160
            Width           =   6975
         End
         Begin VB.ComboBox cboAdjClass 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmPeticionesCargaModif.frx":1F0F
            Left            =   1680
            List            =   "frmPeticionesCargaModif.frx":1F11
            Style           =   2  'Dropdown List
            TabIndex        =   120
            Top             =   638
            Width           =   6975
         End
         Begin VB.TextBox adjPath 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   142
            Top             =   3120
            Visible         =   0   'False
            Width           =   6975
         End
         Begin VB.Label lblPesoEstDoc 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "lblPesoEstDoc"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00808080&
            Height          =   180
            Left            =   7560
            TabIndex        =   194
            Top             =   3600
            Width           =   1110
         End
         Begin VB.Label Label47 
            AutoSize        =   -1  'True
            Caption         =   "Tama�o"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   175
            Top             =   1800
            Width           =   585
         End
         Begin VB.Label Label46 
            AutoSize        =   -1  'True
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   171
            Top             =   1440
            Width           =   435
         End
         Begin VB.Label Label45 
            AutoSize        =   -1  'True
            Caption         =   "Sector"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   170
            Top             =   1080
            Width           =   480
         End
         Begin VB.Label Label43 
            AutoSize        =   -1  'True
            Caption         =   "Archivo"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   154
            Top             =   360
            Width           =   570
         End
         Begin VB.Label lblAdjPet 
            AutoSize        =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   122
            Top             =   30
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label Label37 
            AutoSize        =   -1  'True
            Caption         =   "Tipo documento"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   123
            Top             =   720
            Width           =   1185
         End
         Begin VB.Label Label35 
            AutoSize        =   -1  'True
            Caption         =   "Comentarios"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   120
            TabIndex        =   124
            Top             =   2160
            Width           =   930
         End
      End
      Begin VB.Label Label53 
         AutoSize        =   -1  'True
         Caption         =   "Se sugiere que comprima los documentos antes de adjuntarlos para optimizar el espacio de almacenamiento."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   180
         Left            =   120
         TabIndex        =   193
         Top             =   7200
         Width           =   8160
      End
      Begin VB.Label lblMensajeNoAdjuntos 
         AutoSize        =   -1  'True
         Caption         =   "La petici�n se encuentra en alguno de los estados terminales, por lo cual no es posible agregar o remover adjuntos a la misma."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   176
         Top             =   7560
         Visible         =   0   'False
         Width           =   9495
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   6
      Left            =   0
      TabIndex        =   189
      Top             =   360
      Width           =   10305
      Begin VB.ComboBox cmbValidarEstado 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         ItemData        =   "frmPeticionesCargaModif.frx":1F13
         Left            =   120
         List            =   "frmPeticionesCargaModif.frx":1F15
         Style           =   2  'Dropdown List
         TabIndex        =   192
         Top             =   240
         Width           =   3255
      End
      Begin VB.CheckBox chkControl1 
         Caption         =   "Conformes de Pruebas de Usuario dependen del validaci�n de Homologaci�n"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   191
         Top             =   5880
         Width           =   6135
      End
      Begin MSFlexGridLib.MSFlexGrid grdControl 
         Height          =   5175
         Left            =   120
         TabIndex        =   190
         Top             =   600
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   9128
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
   Begin VB.Frame orjFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Index           =   2
      Left            =   0
      TabIndex        =   81
      Top             =   360
      Width           =   10305
      Begin VB.CommandButton cmdVerAnexa 
         Caption         =   "Ver Peticion"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   8970
         TabIndex        =   83
         TabStop         =   0   'False
         Top             =   480
         Width           =   1170
      End
      Begin MSFlexGridLib.MSFlexGrid grdAnexas 
         Height          =   4920
         Left            =   150
         TabIndex        =   82
         Top             =   480
         Width           =   8715
         _ExtentX        =   15372
         _ExtentY        =   8678
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColorSel    =   16777215
         GridColor       =   14737632
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
End
Attribute VB_Name = "frmPeticionesCargaModif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 25.06.2007 Se agregan dos atributos (Clase de petici�n e indicador de impacto tecnol�gico del
'                         mismo). Los controles son: cboClase y cboImpacto.
' -001- b. FJS 26.06.2007 Se inicializan en el evento Form_Load y se agregan las cargas y controles seg�n el perfil del usuario.
' -001- c. FJS 26.06.2007 Se carga el control con el dato correspondiente al registro solicitado
' -001- d. FJS 27.06.2007 Se agrega el control de valores v�lidos para el control.
' -001- e. FJS 27.06.2007 Se agregan los par�metros necesarios para contemplar los nuevos campos (Clase de petici�n e indicador si corresponde impacto tecnol�gico)
' -001- f. FJS 27.06.2007 Se elimina la posibilidad de realizar altas de peticiones de tipo "Proyecto". En caso de que a una petici�n Normal, Especial o Propia le
'                         corresponda dicho estado, solo el BP asignado a la petici�n podr� transformarla en Proyecto.
'                         Los resp. de Sector y/o Grupo de cualquiera de las gerencias de medios podran realizar altas de peticiones "Especiales". No obstante ello,
'                         las altas de peticiones "Especiales" que no sean realizadas por usuarios de organizaci�n solo podr�n ser de clase "Atenci�n al usuario".
'                         Solo podr�n realizar altas de peticiones Normales los solicitantes de �reas usuarias (no Medios) y los pertenecientes a la gerencia de Organizaci�n.
'                         Es decir que no podr�n hacer altas de peticiones Normales los usuarios de las dem�s gerencias de medios, por m�s que tengan asignado un perfil Solic.
' -001- g. FJS 05.07.2007 Adem�s, se habilitar� la posibilidad de que el BP modifique una petici�n de tipo Normal a Especial. De este modo el BP podr�, por ejemplo, transformar
'                         peticiones dadas de alta por Organizaci�n como Normales (con sector solicitante de Organizaci�n) en Especiales (con la verdadera �rea usuaria como
'                         sector solicitante).
' -001- h. FJS 10.07.2007 Se actualiza el manejo y control de archivos incrustados (adjuntos) y vinculados a una petici�n.
'                         Importante: se modifica la funci�n esBP para la validaci�n entre cadenas, agregandole la funci�n TRIM porque en ciertos casos esta carencia hace que
'                         la expresi�n se evalue incorrectamente, devolviendo False cuando es True.
'                         Se comenta el m�todo SetFocus del campo de comentarios, para focalizar el tipo de clasificaci�n del archivo que se adjunta.
' -001- i. FJS 17.07.2007 Se adaptan las validaciones de los conformes de la petici�n al esquema de los nuevos controles SOX para documentos adjuntos y conformes de usuario.
' -001- j. FJS 18.07.2007 Se inicializa el valor que tomar� por defecto el campo de la tabla Peticion para los nuevos controles SOX para documentos adjuntos. Seg�n la fecha de
'                         corte definida se guardar� con valor 1 (activado) o 0 (desactivado).
' -001- k. FJS 18.07.2007 Se agrega una nueva clase para una petici�n denominada SPUFI que tendr� el mismo tratamiento que la clase Mantenimiento correctivo (es para peticiones
'                         propias cursadas por DyD.
' -001- l. FJS 18.07.2007 Peticiones Especiales: la petici�n, en el alta queda en estado "Al BP". El sector/grupo queda en estado "A confirmar". Cuando el BP pase la petici�n a
'                         "Aprobada", el sector/grupo que estaba "A confirmar" quedar� "A planificar".
' -001- m. FJS 19.07.2007 Se guarda en el log del sistema el cambio en el att. impacto tecnol�gico y la clase de la petici�n (Ver documento anexo).
' -001- n. FJS 20.07.2007 Las peticiones propias de DyD no pueden ser de la clase Atenci�n a usuario. Deber�n ser de tipo Especial a partir de ahora (Anexo I, punto 8).
' -001- o. FJS 24.07.2007 Los OKs pueden ser dados solamente por: Solicitante o Referente de la petici�n, salvo para las peticiones Propias (es as� actualmente).
'                         Si el sector solicitante es de la direcci�n de Medios y la petici�n es propia, tambi�n puede dar el OK el responsable del Sector que tiene injerencia
'                         en el sector solicitante de la petici�n (esta consideraci�n es nueva, ya que actualmente no se requieren OKs para las peticiones propias, pero en el
'                         modelo de gesti�n es necesario que exista un Ok del supervisor en este tipo de peticiones.
' -002- a. FJS 26.07.2007 Por definici�n, y fuera del alcance del proyecto actual, se establece cambiar la restricci�n al tipo de documento que puede estipular un BP tanto en el
'                         alta como en la modificaci�n. Incluido en el documento Anexo.
' -002- b. FJS 27.07.2007 Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por
'                         parte del usuario).
' -002- c. FJS 30.07.2007 Se condiciona la carga del control ComboBox para la clase de la petici�n (cboClase) seg�n los valores del ComboBox de tipo de la petici�n (cboTipoPet).
' -002- d. FJS 31.07.2007 Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -002- e. FJS 03.08.2007 Se agrega una confirmaci�n antes de ejecutar la asignaci�n de documentos y/o conformes a la base ya que no es posible volver atr�s las situaciones debido
'                           a tema de seguridad y perfiles.
' -002- f. FJS 03.08.2007 - Se corrige un defecto inesperado: perdi� el control de la fecha requerida de la petici�n.
' -002- g. FJS 03.08.2007 - Se valida que al momento de adjuntar el archivo adjunto, en el nombre exista el c�digo del documento (Ej.: C100)
' -002- h. FJS 06.08.2007 - Se agregan algunas mejoras a la usabilidad de la pantalla para mejorar la experiencia del usuario.
' -002- i. FJS 07.08.2007 - Se quita la restriccion de no permitir adjuntar un T700 cuando el usuario pertenece a Organizacion (antes solo permitia DyD).
' -002- j. FJS 09.08.2007 - Se agrega la restricci�n de que el responsable de ejecuci�n o sector no pertenezcan a la direcci�n de medios. Es decir, pasa al BP
'                           si es Especial, de alg�n responsable de sector o grupo y no pertenece a la direcci�n de Medios.
' -002- k. FJS 13.08.2007 - Se agrega el control de clase cuando una petici�n esta a punto de recibir un conforme de parte del usuario (esto es porque si bien naturalmente ya esta
'                           clasificada una petici�n en esta instancia, al momento de arrancar los controles SOX debe controlarse que se clasifique la petici�n).
' -002- m. FJS 21.08.2007 - Se corrige el comportamiento del control de impacto tecnol�gico.
' -003- a. FJS 21.08.2007 - Ning�n �rea solicitante podr� establecer la prioridad de una petici�n, tarea que ser� exclusiva responsabilidad del BP. Adem�s se inhibe la posibilidad de
'                           realizar el cambio de estado de una petici�n hasta que no se clasifique la misma (BP). Se agrega un warning gr�fico en pantalla para avisar al BP de la falta
'                           de clasificaci�n de la petici�n.
' -003- b. FJS 23.08.2007 - Se agrega una funci�n oculta que permite exportar todos los datos de una petici�n a un �nico archivo con formato XML.
' -003- c. FJS 23.08.2007 - Se agrega un bot�n para realizar una validaci�n completa de integridad con despliegue de mensajes para el usuario.
' -003- d. FJS 24.08.2007 - Se agrega la llamada al proceso de exportaci�n de las peticiones con conformes parciales.
' -003- e. FJS 27.08.2007 - Se agrega una nueva funci�n en el m�dulo FuncionesHOST para saber si finalmente se debe o no generar el archivo de exportaciones de peticiones para HOST.
' -003- f. FJS 28.08.2007 - Peticiones de tipo "Especial" dadas de alta (solo desde Organizaci�n y DyD) deben nacer siempre con el estado "A planificar" sin depender de la prioridad
'                           asignada (como ahora pasa).
' -003- g. FJS 28.08.2007 - Se agrega la posibilidad de adjuntar otros documentos adem�s de los propios de metodolog�a.
' -003- h. FJS 28.08.2007 - Se agrega la posibilidad de indicar que una petici�n de tipo normal puede tener impacto tecnol�gico (adem�s de una de tipo proyecto).
' -003- i. FJS 29.08.2007 - Cuando faltan los datos de la solapa Detalle de la petici�n, al dar el mensaje de error se invoca al m�todo SetFocus de cada campo. Adem�s, se establece
'                           cual bot�n ser� el que tenga la propiedad Default en verdadero, seg�n el modo de la transacci�n.
' -004- a. FJS 03.09.2007 - Se corrige un problema en la programaci�n: los usuarios no pueden desadjuntar un archivo adjunto anterior a los controles en el nombre. Adem�s se limita
'                           el control solo para las peticiones nuevas (SOx).
' -004- b. FJS 03.09.2007 - Se modifica que la validaci�n entre fechas de alta de la petici�n y fecha requerida sea igual que antes, pero ahora que permita el igual a la fecha del d�a.
' -004- c. FJS 03.09.2007 - Se quitan todas las validaciones respecto al campo de Clase de la petici�n quedando solo para aquellas identificadas expl�citamente denominadas como SOx.
' -004- d. FJS 03.09.2007 - *** LIBRE ***
' -004- e. FJS 03.09.2007 - Se agrega que la restricci�n para adjuntar los conformes a la petici�n se validen si es nueva (SOx).
' -004- f. FJS 03.09.2007 - Se quita la restricci�n que diferencia a ORG y DYD en el alta como solicitantes. Todo es tratado como de organizaci�n (pet. Normales).
' -004- g. FJS 03.09.2007 - Esta correci�n en el c�digo fu� producto de la corrida el lunes de la implementaci�n. Habr�a que documentarlo bien despu�s, pero creo recordar que el tema era
'                           que no me dejaba modificar datos de clase e impacto tecnol�gico para el perfil BP.
' -005- a. FJS 06.09.2007 - Se agrega el c�digo faltante para manejar los nuevos datos de Clase de petici�n e indicador de impacto tecnol�gico para cuando el usuario tiene perfil de
'                           Superadministrador (SADM). Se deshabilita la posibilidad de que un usuario de el alta de una petici�n con perfil de Administrador (ADMI).
' -005- b. FJS 07.09.2007 - Se permite la modificaci�n de la clase y el indicador de impacto tecnol�gico para los ejecutantes y al BP, siempre y cuando la petici�n jam�s haya estado en
'                           estado "En ejecuci�n". El problema qued� para las Propias, ya que para el resto de los tipos no hay problema, pero esto qued� as� me temo por la corrida del
'                           lunes 03.09 que fu� el primer d�a de la versi�n 4.2.0 y habia problemas. Se genera una funci�n que determina si la petici�n alguna vez estuvo en estado de
'                           "En Ejecuci�n".
' -005- c. FJS 07.09.2007 - Se agrega la instrucci�n DoEvents por comportamiento inesperado en los combobox de Clase de petici�n e Impacto tecnol�gico.
' -006- a. FJS 13.09.2007 - Se inhabilita que el perfil BP pueda modificar el tipo de una petici�n a Propia.
' -006- b. FJS 13.09.2007 - Se habilita que al momento de modificar una petici�n Propia se pueda establecer el BP (ahora no lo esta permitiendo y no permite grabar).
' -006- c. FJS 13.09.2007 -
' -006- d. FJS 13.09.2007 - No permitir modificar los datos del formulario cuando esta en estado en Ejecuci�n (para RE y RS).
' -006- e. FJS 13.09.2007 -
' -006- f. FJS 13.09.2007 -
' -006- g. FJS 13.09.2007 -
' -006- h. FJS 13.09.2007 - No validar la clase cuando no este indicada y el perfil es el de Superadministrador.
' -007- a. FJS 18.09.2007 - Se toma el valor del campo pet_sox001 porque esto result� determinante y no fue contemplado por error u omisi�n.
' -008- a. FJS 20.09.2007 - Ver documento que redefine como de maneja la habilitaci�n para declarar conformes.
' -008- b. FJS 24.09.2007 -
' -008- c. FJS 24.09.2007 - Se corrige un bug que impide que una petici�n del nuevo esquema pueda recibir un archivo adjunto.
' -008- d. FJS 27.09.2007 - Se condiciona el pedido de Mail de Ok de Pruebas del usuario para las peticiones del nuevo esquema exclusivamente.
' -009- a. FJS 03.10.2007 - Se muestra al programador el nro. interno de la petici�n.
' -010- a. FJS 26.02.2008 - Homologaci�n
' -011- a. FJS 04.04.2008 - Cambio de metodolog�a de control: solo autorizado para usuarios "selectos" cuando reemplazan usuarios con perfiles de Administrador o SuperAdministrador
' -012- a. SLS 25.04.2008 - Blanqueo de variables para adjuntar Documentos de Metodolog�a y Anexos
' -013- a. FJS 22.04.2008 - Documentos adjuntos: los archivos se guardan ahora con un formato preestablecido.
' -014- a. FJS 13.05.2008 - Se cambia la llamada a otro procedimiento que invoca un SP m�s performante.
' -015- a. FJS 20.05.2008 - Se modifica el par�metro de clase, ya que debe tomar el valor del combo seg�n corresponda.
' -016- a. FJS 23.05.2008 - Se modifica el m�ximo permitido para subir archivos adjuntos (2 Mb. reales) basado en una constante de sistema predefinida
' -017- a. FJS 26.05.2008 - Se inhabilita la posibilidad de selecci�n del combo para Sectores porque solo existe un solo sector para seleccionar.
' -018- a. FJS 26.05.2008 - Cambios para el estado FINALIZADO.
' -019- a. FJS 30.05.2008 - Se quita el mensaje de error que acontece cuando el combo de tipo de petici�n pierde el foco y no tiene selecci�n alguna.
'                           Se agrega en la validaci�n general antes de confirmar y guardar los cambios, que el tipo de la petici�n este cargado correctamente.
' -020- a. FJS 03.06.2008 - Se modifica el control sobre la asignaci�n de "Mail de OK al documento de alcance". Actualmente, esta fallando porque antes de hacer
'                           el control actual, deber�a verificar que el usuario puede estar en el arbol solicitante y entonces no debe solicitar el email. Esto
'                           paso en Organizaci�n.
' -021- a. FJS 04.06.2008 - Se cambia la determinaci�n de si el usuario pertenece al arbol solicitante o no. Lo primero que hago es determinar si el usuario pertenece a la gerencia
'                           de DyD. Si es as�, entonces no pertenece al arbol solicitante. Por definici�n alcanzada con Beto Ocampo.
' -022- a. FJS 09.06.2008 - Se agrega un mensaje personalizado para el grupo Homologador cuando a la petici�n se le asigna un documento de la metodolog�a.
' -023- a. FJS 12.06.2008 - Se agrega una restricci�n para evitar el cambio del tipo y la clase a peticiones de tipo Auditor�a (Internas y Externas)
' -024- a. FJS 11.07.2008 - Se agrega un dato m�s en pantalla: la cantidad de horas totales planificadas.
' -025- a. FJS 21.07.2008 - Al momento de adjuntar un documento a una petici�n, se guarda el sector y grupo del usuario que adjunta el documento.
' -026- a. FJS 23.07.2008 - Se omite el envio del aviso al Resp. de Sector del grupo Homologaci�n (Dopazo).
' -027- a. FJS 28.07.2008 - Se agrega un control m�s para evitar inconsistencias: debe existir ya el conforme de usuario (total o parcial) antes de pasar la cadena a Changeman.
' -028- a. FJS 06.08.2008 - Se reemplaza el control SOx del producto T700 por el nuevo T710. El viejo T700 queda como un documento m�s (sin controles SOx). Adem�s se guarda en el historial el cambio de tipo de una petici�n.
' -029- a. FJS 11.08.2008 - Para que un supervisor pueda agregar conformes de usuario o de alcance, si es de DyD, no debe ser el mismo que di� el alta de la petici�n ni el solicitante de la misma.
' -029- b. FJS 11.08.2008 - Se inhibe la posibilidad de modificar (adjuntando o eliminando) documentos adjuntos cuando la petici�n se encuentre en alguno de los estados terminales.
' -029- c. FJS 11.08.2008 - Se inhibe la posibilidad de modificar (otorgando o removiendo) conformes de cualquier tipo cuando la petici�n se encuentre en alguno de los estados terminales. Lo mismo se hace para documentos vinculados.
' -030- a. FJS 15.08.2008 - Esta porci�n de c�digo no va.
' -031- a. FJS 19.08.2008 - Solo para el grupo Homologador, se permite que cualquier integrante pueda quitar informes de homologaci�n de cualquier otro miembro del grupo.
' -031- b. FJS 19.08.2008 - Se tipifican los informes de homologaci�n en CGM.
' -032- a. FJS 19.08.2008 - Se permite modificar el atributo Visibilidad a los perfiles de BP, ADM y SADM.
' -033- a. FJS 19.08.2008 - Se agrega un nuevo atributo para indicar si es regulatorio o no.
' -034- a. FJS 21.08.2008 - Se agrega al historial el evento de habilitaci�n para pasaje a Producci�n de la petici�n.
' -035- a. FJS 21.08.2008 - Se corrige un viejo BUG que dejaba inconsistente los datos de Solicitante, Referente y Autorizante en peticiones que cambiaban su tipo entre Propias y Especiales.
' -036- a. FJS 10.09.2008 - Impacto tecnol�gico e Indicador de Regulatorio: se cambia la definici�n original, dejando por default un valor impreciso, para que se explicite el Si o el No.
' -036- b. FJS 11.09.2008 - Avisos al adjuntar y/o eliminar documentos de alcance (SOx).
' -036- c. FJS 25.09.2008 - Se modifica la definici�n: se agrega que el perfil de Resp. de Ejecuci�n tambi�n pueda modificar el atributo Regulatorio.
' -037- a. FJS 15.09.2008 - Se modifica todo el front-end. Se agregan los datos de proyecto (IDM).
' -038- a. FJS 17.09.2008 - Se agrega la tipificaci�n de un nuevo documento de la metodolog�a: C310 - Definici�n de Base de Datos.
' -039- a. FJS 02.10.2008 - Se corrige el comportamiento actual al modificar una petici�n con el BP.
' -039- b. FJS 02.10.2008 - Se agrega la funcionalidad necesaria para soportar la m�ltiple selecci�n de documentos al adjuntar.
' -039- c. FJS 24.11.2008 - Se agrega la funcionalidad de eliminar/desadjuntar m�ltiples documentos.
' -039- c. FJS 10.10.2008 - Se agrega una restricci�n para evitar que suba un documento CG04 sin que exista un documento de alcance.
'                           Adem�s para que pueda subir a la base la petici�n debe estar en estado "En Ejecuci�n".
' -039- d. FJS 20.09.2010 - Bug: no debe ejecutarse esto cuando el documento es un CG04 y adem�s, debe registrarse en el historial el evento.
' -040- a. FJS 23.10.2008 - Se permite la modificaci�n de ciertos datos de una petici�n cuando se encuentre en el estado "En ejecuci�n" a los perfiles de: Business Partner, Resp. Grupo y Resp. Sector.
'                           Para lograr esto se agregan las acciones por perfil (tabla AccionesPerfil).
'                           FJS 21.11.2008 - A partir de pruebas del usuario, surgi� el inconveniente de que no debe tenerse en cuenta el control de las variables de flgIngerencia y flgEjecutorParaModificar.
' -040- b. FJS 11.11.2008 - Se agrega el registro de 3 nuevos eventos al historial de una petici�n: Regulatorio, Prioridad y Visibilidad.
' -040- c. FJS 12.11.2008 - Se agrega la funci�n TRIM al dato de recurso para evitar los espacios en blanco que dejan de manera desprolija la visualizaci�n de estos datos en la pantalla.
' -040- d. FJS 12.11.2008 -
' -040- e. FJS 12.11.2008 - Se corrige un BUG para evitar generar los avisos a responsables cuando el estado es alguno de los terminales.
' -040- f. FJS 13.11.2008 - Se corrige un BUG para evitar un mensaje de control que solo debe aplicar en caso que se este intentando agregar un conforme.
' -041- a. FJS 20.11.2008 - Se condiciona la funcionalidad de uso de multiples documentos para adjuntar seg�n preferencias del usuario.
' -042- a. FJS 05.12.2008 - Se agrega el evento en el historial cuando se vincula o desvinculan documentos vinculados.
' -042- b. FJS 18.12.2008 - Se agrega el cambio en el puntero del mouse y la leyenda para avisar al usuario que a�n se esta cargando el historial (por demoras).
' -043- a. FJS 23.12.2008 - Se agrega una restricci�n para que no se puedan otorgar conformes de usuario parciales a peticiones de clase: CORR, ATEN y SPUF.
' -044- a. FJS 29.12.2008 - Se agrega que en el evento de clasificaci�n de una petici�n, si existe ya un grupo de DyD homologable, entonces es agregado el grupo homologador si este a�n no estuviera vinculado.
' -045- a. FJS 08.01.2009 - Se optimiza la carga de adjuntos, documentos vinculados y conformes (problemas de performance).
' -046- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.
' -047- a. FJS 28.01.2009 - Se modifica la ubicaci�n de la rutina de mensajes para el grupo homologador porque no esta enviando los mensajes correspondientes.
' -047- b. FJS 05.06.2009 - Se agrega una funci�n para determinar si existe vinculado el grupo homologador a la petici�n antes de enviar los mensajes porque sino se dan casos donde el grupo homologador recibe avisos
'                           y no se encuentran siquiera vinculados a la petici�n.
' -048- a. FJS 12.03.2009 - Se agrega el manejo detallado de textos de peticiones.
' -049- a. FJS 13.03.2009 - Se agrega rutina de control para detectar si se intenta eliminar un documento de alcance y exista ya un conforme al documento de alcance, generar una advertencia sobre la eliminaci�n
'                           de dicho conforme.
' -050- a. FJS 06.04.2009 - Replanificaciones.
' -051- a. FJS 16.04.2009 - Nuevo perfil de analista ejecutor.
' -051- b. FJS 20.04.2009 - Se controla que no pueda borrarse el dato del combobox porque genera inconsistencias.
' -051- c. FJS 20.04.2009 - Se mejora el log de las modificaciones sensibles en los datos de la petici�n. No se registra el cambio si anteriormente no tenia especificaci�n.
' -051- d. FJS 28.04.2009 - Mejora: cuando es seleccionado un sector que no posee perfiles solicitantes para seleccionar (en el caso de peticiones Especiales - Atenci�n a Usuarios) se inhabilita el combo
'                           para mostrar que no existen solicitantes.
' -051- e. FJS 04.05.2009 - Correcci�n: el Analista Ejecutor, cuando pertenece a alguna de las gerencias de BP o TRYAPO, tambi�n debe poder cargar peticiones de tipo Propias con las
'                           caracter�sticas de un Analista Ejecutor DyD.
' -051- f. FJS 28.05.2009 - Se agrega en la condici�n tener en cuenta al nuevo perfil ANAL para grabar el log en el historial.
' -051- g. FJS 04.06.2009 - Bug: cuando el alta la realiza un Analista Ejecutor, se fuerza, como cuando la daba un Responsable, al today.
' -052- a. FJS 17.06.2009 - Cambio: cuando un perfil analista ejecutor realiza un alta de nueva petici�n, debe permitir la selecci�n del BP, de la misma manera que cuando el alta la realiza un Resp. de Ejecuci�n.
'                           Solicitado por Luciana Galindez el 05.06.2009 v�a email.
' -053- a. FJS 22.06.2009 - Se modifica un horror ortogr�fico.
' -053- b. FJS 23.06.2009 - Se habilita la selecci�n m�ltiple de documentos para el usuario final.
' -054- a. FJS 31.08.2009 - Mejora: para evitar problemas en la visualizaci�n, cuando estoy visualizando la petici�n, muestro los datos de los combos independientemente de las validaciones y/o cargas que realice el mismo,
'                           porque si por ejemplo, el BP ya no tuviera ese perfil, me esta dejando el combo "vacio".
' -055- a. FJS 01.09.2009 - Se corrige un bug: se comprueba que antes de disparar la rutina de autovinculaci�n del grupo homologador, se considere el estado de la petici�n (no ejecutar cuando este en alguno de los estados terminales).
' -055- b. FJS 03.09.2009 - Se restringe la carga de sectores, solicitantes, referentes, autorizantes, supervisores y BP en el modo ALTA de una petici�n para solo permitir la selecci�n de aquellos recursos habilitados.
' -056- a. FJS 16.09.2009 - Se modifica esta comprobaci�n permitiendo que existan solicitantes de la gerencia DYD (deber�an ser casos excepcionales, como el de Susana y Carlos).
' -057- a. FJS 09.10.2009 - Mejoras est�ticas.
' -058- a. FJS 07.01.2010 - Se cambia el hasta fijo de esta fecha al tope permitido por el tipo de dato.
' -059- a. FJS 28.01.2010 - Nuevo: se habilita la posibilidad de que l�deres de DYD puedan dar un conforme a las pruebas de usuario(parcial y/o final), de parte de �ste, adjuntanto un documento adjunto de tipo EML2.
' -059- b. FJS 13.04.2010 - Mejora: cuando un l�der DYD intenta adjuntar un TEST o TESP, deben existir al menos un EML2 adicional (para que se corresponda con el nuevo conforme a otorgar).
' -059- c. FJS 13.04.2010 - Mejora: se agrega al historial, el registro del evento de remoci�n de conformes parciales.
' -060- a. FJS 18.02.2010 - Bug: cuando se intenta adjuntar un documento que ya existe y genera un error, igualmente esta enviando un mensaje al grupo homologaci�n sin tener en cuenta el error (y por ende, que el documento no esta adjuntado).
' -061- a. FJS 08.03.2010 - Mejora: se agrega una etiqueta para evitar la redundancia en las evaluaciones para habilitar las opciones correspondientes.
' -062- a. FJS 10.03.2010 - Bug: se corrige el estado de alta del sector cuando interviene homologaci�n.
' -063- a. FJS 10.03.2010 - Mejora: se permite que el lider, al modificar una petici�n propia que a�n no ha sido clasificada, modifique los textos de la misma.
' -063- b. FJS 19.04.2010 - Si homologaci�n se encuentra en alguno de los estados terminales, y se esta ingresando un conforme de tipo ALT (otorgado por L�deres DYD) entonces debe reactivarse Homologaci�n para
'                           que sean ellos quienes otorguen el conforme final y habiliten la petici�n para pasaje a Producci�n.
' -064- a. FJS 17.03.2010 - Nueva pesta�a para visualizar de manera �gil las validaciones y estados al momento de una petici�n para Pasaje a Producci�n.
' -064- b. FJS 27.04.2010 - Condicionada su visualizaci�n al sector Planeamiento y Metodolog�a de Sistemas por el momento.
' -064- c. FJS 20.10.2010 - Condicionada para que sea visible solo por usuarios de la gerencia de DYD y/o Superadministrador.
' -065- a. FJS 29.03.2010 -
' -065- b. FJS 11.05.2010 - Bug: se acota a la gerencia de DYD.
' -066- a. FJS 30.03.2010 - Se actualizan los datos principales de la petici�n si se pulsa F1.
' -066- b. FJS 09.04.2010 - Se agrega el evento de scroll en la grilla para actualizar los datos seleccionados.
' -066- c. FJS 09.04.2010 - Se corrige para evitar mostrar el texto cuando no hay selecci�n activa en la grilla.
' -067- a. FJS 09.04.2010 - Se agrega validaci�n para que Homologaci�n deshabilite la validez de una petici�n ante un OMA.
' -068- a. FJS 09.04.2010 - Se inhibe la posibilidad de cargar conformes parciales de ALCA cuando ya existe un final.
' -069- a. FJS 12.04.2010 - Se agrega una condici�n para inhibir la modificaci�n del tipo y la clase de una petici�n, por parte del perfil BP, en peticiones propias de DYD.
' -070- a. FJS 21.04.2010 - Si un homologador otorga un OMA, sin importar la clase o que ya exista un SOB u OME, la petici�n deja de estar v�lida para pasajes a Prod.
' -071- a. FJS 23.04.2010 - Se descarta que el BP pueda especificar la clase SPUFI para peticiones fuera de la gerencia de DYD.
' -072- a. FJS 27.04.2010 - Se agregan dos controles Image en la solapa F5 (Conformes) para que el grupo homologador pueda visualizar r�pidamente la validez de una petici�n. Esto queda restringido al sector Planeamiento y Metodolog�a.
' -073- a. FJS 06.05.2010 - Se especifica el tipo de proceso de homologaci�n seg�n la clase de la petici�n.
' -073- b. FJS 19.05.2010 - Si se cambia la clase de la petici�n, debo reactivar a Homologaci�n (en cualquiera de sus estados terminales).
' -073- c. FJS 04.06.2010 - Excepto que Homologaci�n se encuentre en estado "Rechazo t�cnico".
' -074- a. FJS 13.05.2010 - Evento particular de alta de petici�n especial: se agrega un evento (acci�n) particular para cuando el alta de una petici�n es especial para enviar un mensaje del evento al solicitante.
' -074- b. FJS 13.05.2010 - Evento particular de cambio de tipo: se agrega un evento (acci�n) particular para cuando la petici�n cambia a tipo Especial, para enviar un mensaje del evento al solicitante.
' -075- a. FJS 14.05.2010 - Para peticiones Propias de DYD, el BP de la petici�n no puede otorgar conformes de pruebas de usuario (Marcet - Dopazo).
' -076- a. FJS 14.05.2010 - Mejora: se agrega una columna oculta a la grilla para permitir el correcto ordenamiento por fechas.
' -077- a. FJS 06.07.2010 - Bug: se agrega la eliminaci�n tambi�n de los ALCA otorgados por l�deres DyD.
' -078- a. FJS 01.09.2010 - Planificaci�n DYD.
' -079- a. FJS 05.10.2010 - Mejora: se busca el c�digo del grupo homologador una vez al entrar en la pesta�a de adjuntos.
' -079- b. FJS 05.10.2010 - Mejora: se busca mejorar la performance de la carga de esta pesta�a.
' -080- a. FJS 12.10.2010 - Mejora: se quita del c�digo la activaci�n de estos �conos y mensajes. Ya no son necesarios.
' -081- a. FJS 14.10.2010 - Mejora: se indica si un archivo adjunto es alguno de los tipos de archivo comprimido de la lista (arc|arj|gz|gzip|sea|tar|tar.gz|tar.Z|tgz|Z|zip|ace|rar|).
' -082- a. FJS 08.11.2010 - Mejora: se habilita el uso del control CommonControl para adjuntar archivos.
' -082- b. FJS 08.11.2010 - Mejora: se habilita el uso del control CommonControl para vincular documentos de metodolog�a.
' -083- a. FJS 09.11.2010 - Se carga el combo de documentos a partir de la nueva tabla.
' -083- b. FJS 30.11.2010 - Por ahora, se agrega esto hasta que sea completamente reemplazado.
' -084- a. FJS 15.12.2010 - Se agrega la opci�n de comprimir el archivo al adjuntarlo a la petici�n.
' -085- a. FJS 05.01.2011 - Arreglo: no estaba infiriendo correctamente los informes de homologaci�n.
' -086- a. FJS 05.01.2011 - Mejora: se permite que se guarde en memoria la �ltima ubicaci�n usada para adjuntar archivos.
' -087- a. FJS 04.02.2011 - Mejora: legibilidad en el c�digo. Se cambia la variable cModeloControl (byte) por bModeloControl (byte).
' -088- a. FJS 29.03.2011 - Modificaci�n: no se borran los mensajes para el grupo homologador.
' -089- a. FJS 26.04.2011 - Arreglo: con el perfil de BP, al modificar, cuando se modifica el tipo, siempre se vuelve a establecer el que tiene la petici�n.
' -090- a. FJS 03.05.2011 - Arreglo: al eliminar un conforme de TEST/TESP, la petici�n debe quedar inhabilitada para pasajes a Prod.
' -091- a. FJS 05.05.2011 - Arreglo: estaba evaluando mal, al guardar, la especificaci�n de IT (reportado por Ariotti).
' -092- a. FJS 12.05.2011 - Arreglo: si cambia la clase de la petici�n, de NUEV/EVOL a ATEN/SPUF, se elimina de la planificaci�n (todos los Q).
' -093- a. FJS 14.07.2011 - Nuevo: si es un proyecto, o tiene impacto tec. o tiene proyecto IDM, agrega y activa al grupo de SI.
' -093- b. FJS 14.07.2011 - Nuevo: el nuevo tipo de documento C104 solo puede ser adjuntado por miembros del sector y/o grupo de SI.
' -093- c. FJS 17.08.2011 - Nuevo: al otorgar conformes, evaluar si se exige el C104 para habilitar las peticiones para pasajes a Producci�n.
' -094- a. FJS 28.12.2011 - Nuevo: al modificar el BP, se determina si la petici�n se encuentra planificada en el Q actual o posterior, y se modifica...
' -095- a. FJS 12.03.2012 - Fusi�n Bco/Seg.
' -096- a. FJS 04.04.2012 - Mejora: se permite que el analista ejecutor y el lider, puedan modificar sus datos (t�tulo, fecha y descripciones).
' -097- a. FJS 11.07.2012 - Nuevo: Se agrega el campo para indentificar peticiones vinculadas a la mitigaci�n de factores de Riesgo Operacional.
' -098- a. FJS 28.10.2014 - Nuevo: se quita el "hardcodeo" respecto de las gerencias de DyD, Org. y Tryapo.
' -099- a. FJS 07.11.2014 - Modificaci�n: Foggia.
' -100- a. FJS 25.11.2014 - Modificaci�n: al momento de eliminar un adjunto de tipo documento de alcance, se verifica que para realizarse, no
'                           exista un documento de homologaci�n (seguimiento de homologaci�n) adjunto o conformes parciales y/o finales. Adem�s,
'                           se agrega que, adem�s del conforme final al documento de alcance, se caiga tambi�n el conforme parcial.
' -101- a. FJS 05.12.2014 - Nuevo: se parametriza dinamicamente la combinaci�n de tipo/clase.
' -101- b. FJS 05.12.2014 - Nuevo: para mejorar la legibilidad del c�digo, se ordenan las rutinas en nuevas subrutinas.
'
' mov -001-
' mov -005-
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Option Explicit

'{ add -101- a.
Private Type udtPeticionParam
    tipo As String
    TipoNom As String
    clase As String
    ClaseNom As String
End Type
'}

Private Const constConfirmMsgAttachConformes = "�Confirma la asignaci�n del conforme seleccionado?"
Private Const constConfirmMsgDeAttachConformes = "�Confirma la desasignaci�n del conforme seleccionado?"
Private Const constConfirmMsgAttachDocumentos = "�Confirma la asignaci�n del documento seleccionado?"
Private Const constConfirmMsgDeAttachDocumentos = "�Confirma la desasignaci�n del documento seleccionado?"
Private Const constConfirmMsgDeAttachVariosDocumentos = "�Confirma la desasignaci�n de los documentos seleccionados?"
Private Const cTAG As String = "Datos Petici�n N� "

Private Const MODELO_CONTROL_ANTERIOR = 0
Private Const MODELO_CONTROL_NUEVO = 1

' Constantes para las pesta�as
Private Const orjDAT = 0
Private Const orjMEM = 1
Private Const orjANX = 2
Private Const orjDOC = 3
Private Const orjADJ = 4
Private Const orjCNF = 5
Private Const orjEST = 6                                    ' add -064- a.

' Constantes para peticiones anexadas
Private Const colNroAsignado = 0
Private Const colTitulo = 1
Private Const colNroInterno = 2

' Constantes para documentos de metodolog�a vinculados
Private Const colDOC_DUMMY = 0
Private Const colDOC_ADJ_FILE = 1
Private Const colDOC_ADJ_PATH = 2
Private Const colDOC_NOM_AUDIT = 3
Private Const colDOC_AUDIT_DATE = 4
Private Const colDOC_ADJ_TEXTO = 5
Private Const colDOC_FECHAFMT = 6

' Constantes para control de pasajes a producci�n
Private Const colCTRL_NIVEL = 0
Private Const colCTRL_NRO = 1
Private Const colCTRL_CONTROL = 2
Private Const colCTRL_OBS = 3
Private Const colCTRL_ESTADO = 4

' Constantes para documentos adjuntos de una petici�n
Private Const colAdj_AUDIT_USER = 0
Private Const colAdj_ADJ_TIPO = 1
Private Const colAdj_ADJ_FILE = 2
Private Const colAdj_ADJ_SIZE = 3
Private Const colAdj_AREA = 4
Private Const colAdj_NOM_AUDIT = 5
Private Const colAdj_AUDIT_DATE = 6
Private Const colAdj_ADJ_TEXTO = 7
Private Const colAdj_COD_GRUPO = 8
Private Const colAdj_AUDIT_DATE_FMT = 9
Private Const colAdj_NOM_SECTOR_NV = 10
Private Const colAdj_NOM_GRUPO_NV = 11
Private Const colAdj_ADJ_ISCOMPRESS = 12

' Constantes para los conformes de una petici�n
Private Const colPcf_OK_TIPO = 0
Private Const colPcf_OK_MODO = 1
Private Const colPcf_OK_USER = 2
Private Const colPcf_OK_FECHA = 3
Private Const colPcf_OK_COMMENT = 4

' Constantes para cargar documentos
Private Const ADJUNTOS_NORMAL = 0
Private Const ADJUNTOS_HOMO = 1
Private Const ADJUNTOS_ANTERIOR = 2

' Variables para permisos de edici�n de atributos
Dim flgUPDPET_titulo As Boolean
Dim flgUPDPET_tipo As Boolean
Dim flgUPDPET_clase As Boolean
Dim flgUPDPET_impacto As Boolean
Dim flgUPDPET_regulatorio As Boolean
Dim flgUPDPET_prioridad As Boolean
Dim flgUPDPET_empresa As Boolean
Dim flgUPDPET_importancia As Boolean
Dim flgUPDPET_proyecto As Boolean
Dim flgUPDPET_corploc As Boolean
Dim flgUPDPET_orientacion As Boolean
Dim flgUPDPET_ro As Boolean
Dim flgUPDPET_bpar As Boolean
Dim flgUPDPET_fchreq As Boolean
Dim flgUPDPET_fchbp As Boolean
Dim flgUPDPET_sector As Boolean
Dim flgUPDPET_soli As Boolean
Dim flgUPDPET_refe As Boolean
Dim flgUPDPET_supe As Boolean
Dim flgUPDPET_auto As Boolean
Dim flgUPDPET_membenef As Boolean
Dim flgUPDPET_memcarac As Boolean
Dim flgUPDPET_memdescr As Boolean
Dim flgUPDPET_memmotiv As Boolean
Dim flgUPDPET_memobser As Boolean
Dim flgUPDPET_memrelac As Boolean
Dim flgUPDPET_DFLT_clase As String
Dim flgUPDPET_DFLT_estado As String
Dim flgUPDPET_DFLT_situac As String

'Dim flgUPDPET_ As Boolean

' Variables para el ordenamiento
Dim bSorting As Boolean                                     ' Variable global que indica que se est� ordenando la grilla
Dim lUltimaColumnaOrdenada As Integer                       ' sss

'Public PerfilInternoActual As String                       ' upd -003- d. - Se pasa del �mbito privado al p�blico
Dim PerfilInternoActual As String
Dim sEstado As String
Dim sTipoPet As String
Dim bFlgObservaciones As Boolean, bFlgBeneficios As Boolean, bFlgCaracteristicas As Boolean
Dim bFlgDescripcion As Boolean, bFlgMotivos As Boolean, bFlgRelaciones As Boolean
Dim flgExtension As Boolean
Dim sBpar As String, sSoli As String, sRefe As String, sSupe As String, sDire As String
Dim xSec As String, xGer As String, xDir As String          ' Datos de solicitante (direcci�n, gerencia y sector de la petici�n)
Dim sSituacion As String
Dim sUsualta As String
Dim anxNroPeticion
'Dim defTipoPet As String                                    ' Utilizada para guardar el default de Tipo de Petici�n
Dim xPerfNivel As String, xPerfArea As String
Dim xPerfDir As String, xPerfGer As String, xPerfSec As String, xPerfGru As String
Dim flgIngerencia As Boolean
Dim flgEjecutor As Boolean
Dim flgBP As Boolean
Dim flgSolicitor As Boolean
Dim flgEjecutorParaModificar As Boolean                     ' add -006- x.
Dim flgEnCarga As Boolean                                   ' Utilizada para indicar que se encuentra dentro de proceso de carga de datos (para evitar el disparo de eventos).
Dim xUsrPerfilActual As String, xUsrLoginActual As String
Dim txObservaciones As String
Dim txBeneficios As String
Dim txCaracteristicas As String
Dim txDescripcion As String
Dim txMotivos As String
Dim txRelaciones As String
Dim sRegulatorio As String                                  ' add -036- a.
Dim glPathDocMetod As String
Dim docOpcion As String
Dim docIndex As Integer
Dim glPathAdjPet As String
Dim adjOpcion As String
Dim adjIndex As Integer
Dim pcfOpcion As String
Dim antPathAdjPet As String
Dim antPathDocMetod As String
Dim sPcfSecuencia As String
Dim prjNroInterno As String
Dim cModeloControl As Byte                              ' add -087- a.
Dim bExisteHOMA As Boolean                              ' add -020- a.
Dim flgEstuvoEJECUC As Boolean                       ' add -097- a.

'{ add -022- a.
' Variables para la parte de Homologaci�n
Dim cGrupoHomologador As String
Dim cGrupoHomologadorSector As String
Dim cGrupoHomologadorEstado As String
Dim cSectorHomologadorEstado As String                  ' add -062- a.
Dim cSectorHomologadorNuevoEstado As String
Dim cMensaje As String
'}
'{ add -031- a.
Dim xImportancia As String
Dim xCorpLocal As String
Dim xOrientacion As String
'}
'{ add -037- a.
Dim cProjNiv1Nom As String
Dim cProjNiv2Nom As String
Dim cProjNiv3Nom As String
'}
Dim cTextoSeleccionado As String                        ' add -048- a.
Dim cMensajeError As String                             ' add -082- a.

Dim PetParam() As udtPeticionParam                      ' add -101- a.

'Dim vPetTipo() As String            ' Vector para tipo de petici�n
'Dim vPetClase() As String           ' Vector para clase de petici�n

Private Sub Form_Load()
    ' Inicializamos variables de flag generales
    flgEnCarga = True                                   ' Variable de control para las cargas iniciales
    flgExtension = False
    flgEstuvoEJECUC = EstuvoEnEjecucion                 ' Indica si la petici�n alguna vez estuve en estado "En ejecuci�n"
    
    ' Inicializamos otras variables
    PerfilInternoActual = glUsrPerfilActual             ' Guardo este perfil por si lo cambio
    
    'cPrioridad = ""                                     ' Inicializaci�n (REVISARRRRR)
    
'    '{ add -001- h.
'    Select Case glModoPeticion
'        Case "ALTA"
'            Call Cargar_Adjuntos(ADJUNTOS_NORMAL)
'            Call Cargar_Adjuntos(ADJUNTOS_HOMO)
'            cModeloControl = MODELO_CONTROL_NUEVO
'        Case Else
'            If cModeloControl = MODELO_CONTROL_NUEVO Then
'                Call Cargar_Adjuntos(ADJUNTOS_NORMAL)
'                Call Cargar_Adjuntos(ADJUNTOS_HOMO)
'            Else
'                Call Cargar_Adjuntos(ADJUNTOS_ANTERIOR)
'            End If
'    End Select
'    '}
    
    'defTipoPet = "PRO"      ' Por defecto, el tipo de petici�n es Propia (REVISARRRR!!!)
    'cboTipopet.Clear       ' del -036- a. No hace falta, ya inclu�do en el procedimiento InitTipoPet llamado a continuaci�n
    
    
    'Call InitTipoPet                            ' Inicializo el control con los tipos de petici�n (lo hace de acuerdo al perfil actual del usuario)
    
    '{ del -101- b.
'    '{ add -001- a. - Se inicializa el control cboClase (indicador de la clase de la petici�n) con todos
'    '                 los valores definidos. Tambi�n se prefija la opci�n en "No" para el indicador de Impacto tecnol�gico.
'    With cboClase
'        .Clear
'        Select Case glModoPeticion
'            Case "ALTA"
'                Select Case PerfilInternoActual
'                    Case "CGRU", "CSEC", "BPAR"
'                        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                        .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                        .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                        .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                        .AddItem "SPUFI" & ESPACIOS & "||SPUF"    ' add -001- k.
'                        .ListIndex = 0
'                    Case Else
'                        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                        .ListIndex = 0
'                End Select
'            Case Else
'                .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC" ' 47 espacios
'                .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                .AddItem "SPUFI" & ESPACIOS & "||SPUF"    ' add -001- k.
'                .ListIndex = 0
'        End Select
'    End With
'}
    
    'With cboImpacto
    '    .ListIndex = 0              ' Por defecto, el indicador de impacto tecnol�gico es indeterminado: se espera se fuerce    ' add -036- a.
    'End With
    '}
    ''{ add -036- a.
    'cboRegulatorio.ListIndex = 0    ' Por defecto, este indicador se encuentra indeterminado. Para forzar la especificaci�n.
    ''}
    
    
    ' FJS: basado en el perfil con el que actua en este momento, averigua el nivel y �rea que se utilizar� seg�n definici�n en la tabla RecursoPerfil
    
    Call InicializarToolTipsControles                   ' add -037- a.
    Call InicializarControles                           ' Inicializa algunos controles con valores por defecto
    Call CargaCombosConValorFijo                        ' add -101- b.
    
    Call InicializarDatosRecurso                        ' Carga los datos de �rea del recurso actuante
    Call CargarGrupoHomologador                         ' Cargo los datos principales del grupo homologador (c�digo, estado, etc.)
    Call CargarParametria                               ' add -101- a. Solo se debe cargar si se da de ALTA o MODIFica una petici�n
    
    Call InicializarPantalla(True)
    Call FormCenterModal(Me, mdiPrincipal)
    Call InicializarGrillas(True)                   ' add -046- a.
End Sub

Private Sub cboImportancia_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    xUsrLoginActual = glLOGIN_ID_REEMPLAZO
    xUsrPerfilActual = PerfilInternoActual
End Sub

Private Sub cboSector_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    If PerfilInternoActual = "SOLI" Then Exit Sub
    If CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ" Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call Puntero(True)
    Call CargaIntervinientes
    If cboBpar = "" Then Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))  ' add -xxx- 04.02.2011
    'Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))  ' del -xxx- 04.02.2011
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Private Sub cmdSector_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    frmPeticionesSector.Show 1
    DoEvents
    Call LockProceso(False)
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla(False)
    End If
End Sub

Private Sub cmdGrupos_Click()
    If flgEnCarga = True Then Exit Sub
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glSector = ""
    frmPeticionesGrupo.Show vbModal
    DoEvents
    Call LockProceso(False)
    If Me.Tag = "REFRESH" Then
        InicializarPantalla
    End If
End Sub

'{ add -006- g.
Private Sub cboSolicitante_Click()
    DoEvents
    Select Case glModoPeticion
        Case "MODI"
            If (CodigoCombo(cboTipopet, True)) = "ESP" Then
                cboSolicitante_Change
            End If
    End Select
End Sub

Private Sub cboSolicitante_Change()
    If Trim(xSec) <> Trim(CodigoCombo(cboSector, True)) Then
        Select Case glModoPeticion
            Case "MODI"
                If (CodigoCombo(cboTipopet, True)) = "ESP" Then
                    cboReferente.ListIndex = -1
                    cboSupervisor.ListIndex = -1
                    cboDirector.ListIndex = -1
                End If
        End Select
    Else
        cboReferente.ListIndex = SetCombo(cboReferente, sRefe)
        cboSupervisor.ListIndex = SetCombo(cboSupervisor, sSupe)
        cboDirector.ListIndex = SetCombo(cboDirector, sDire)
        If Trim(sSoli) <> Trim(CodigoCombo(cboSolicitante)) Then
            Select Case glModoPeticion
                Case "MODI"
                    If (CodigoCombo(cboTipopet, True)) = "ESP" Then
                        cboReferente.ListIndex = -1
                        cboSupervisor.ListIndex = -1
                        cboDirector.ListIndex = -1
                    End If
            End Select
        End If
    End If
End Sub
'}

Private Sub cboTipopet_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If CodigoCombo(cboTipopet, True) = "PRJ" Then
        If cboImportancia.ListIndex = -1 Then cboImportancia.ListIndex = cboImportancia.ListCount - 1
        If cboCorpLocal.ListIndex = -1 Then cboCorpLocal.ListIndex = 0
    End If
    '{ add -002- c.
    If glModoPeticion = "MODI" Then
        Call HabilitarBotones
        Call LockProceso(False)
        Exit Sub
    End If
    '}
    '{ del -002- c.
    'If glModoPeticion = "MODI" Then
    '    If CodigoCombo(cboTipopet, True) = "PRJ" Then
    '        'estamos en el caso en que se cambio de NOR o PRO a PRJ
    '        Call HabilitarBotones
    '        Call LockProceso(False)
    '        Exit Sub
    '   ElseIf CodigoCombo(cboTipopet, True) = "ESP" Then
    '        'estamos en el caso en que se cambio de PRO a ESP
    '        Call HabilitarBotones
    '        Call LockProceso(False)
    '        Exit Sub
    '    Else
    '        Call setHabilCtrl(cboCorpLocal, "DIS")
    '        Call setHabilCtrl(cboOrientacion, "DIS")
    '        Call setHabilCtrl(cboImportancia, "DIS")
    '        Call LockProceso(False)
    '        Exit Sub
    '    End If
    'End If
    '}
    Call Puntero(True)
    Dim sDir, sGer, sSec As String
    If CodigoCombo(cboTipopet, True) = "ESP" Then
        sEstado = "APROBA"
        txtFechaComite.Text = Format(date, "dd/mm/yyyy")
        '{ del -078- a.
        'If PerfilInternoActual <> "ANAL" Then        ' add -051- e.
        '    Call setHabilCtrl(cboPrioridad, "NOR")
        'End If                                      ' add -051- e.
        '}
        Call setHabilCtrl(cboSector, "NOR")         ' add -017- a.
        Call setHabilCtrl(cboSolicitante, "NOR")
        Call setHabilCtrl(cboBpar, "DIS")
        'Call InicializarCombos
    End If
    If CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ" Then
        sEstado = "APROBA"
        txtFechaComite.Text = Format(date, "dd/mm/yyyy")
        If PerfilInternoActual <> "ANAL" Then           ' add -051- e.
            'Call setHabilCtrl(cboPrioridad, "NOR")     ' del -078- a.
            Call setHabilCtrl(cboBpar, "NOR")               ' add -051- e.
        End If                                          ' add -051- e.
        Call setHabilCtrl(cboSolicitante, "DIS")
        'Call setHabilCtrl(cboBpar, "NOR")               ' del -051- e.
        'Call InicializarCombos
        'cboBpar.ListIndex = -1
    End If
    If CodigoCombo(cboTipopet, True) = "PRJ" Then
        Call setHabilCtrl(cboCorpLocal, "NOR")
        Call setHabilCtrl(cboOrientacion, "NOR")
        'cboPrioridad.ListIndex = 1     ' del -078- a.
        'If glModoPeticion = "ALTA" Then
        '    cboImportancia.ListIndex = cboImportancia.ListCount - 1
        'End If
        If glModoPeticion = "ALTA" Then
            cboImportancia.ListIndex = cboImportancia.ListCount - 1
            If PerfilInternoActual = "CGRU" Then
                Call setHabilCtrl(cboImportancia, "DIS")
            Else
                Call setHabilCtrl(cboImportancia, "NOR")
            End If
        End If
    Else
        Call setHabilCtrl(cboCorpLocal, "DIS")
        Call setHabilCtrl(cboOrientacion, "DIS")
        Call setHabilCtrl(cboImportancia, "DIS")
        cboImportancia.ListIndex = -1
        cboCorpLocal.ListIndex = 0
    End If
    
    Call CargarPermisosEdicion
    Call CargarPetClase
    ' Carga las clases correspondientes seg�n el tipo seleccionado
'    With cboClase
'        .Clear
'        If sp_GetTipoPetClase(CodigoCombo(cboTipopet, True), Null) Then
'            Do While Not aplRST.EOF
'                .AddItem ClearNull(aplRST.Fields!nom_clase) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_clase)
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        .ListIndex = 0
'        If .ListCount > 1 Then .Enabled = True
'    End With
    
    
    '{ add -051- a.
    If cboClase.ListCount = 1 Then
        Call setHabilCtrl(cboClase, "DIS")
    Else
        Call setHabilCtrl(cboClase, "NOR")
    End If
    '}
    Call LockProceso(False)
    'defTipoPet = CodigoCombo(cboTipopet, True)
    DoEvents
    Call Puntero(False)
End Sub

Private Sub CargarPermisosEdicion()
    ' Inicializar las variables
    flgUPDPET_titulo = False
    flgUPDPET_tipo = False
    flgUPDPET_clase = False
    flgUPDPET_impacto = False
    flgUPDPET_regulatorio = False
    flgUPDPET_prioridad = False
    flgUPDPET_empresa = False
    flgUPDPET_importancia = False
    flgUPDPET_proyecto = False
    flgUPDPET_corploc = False
    flgUPDPET_orientacion = False
    flgUPDPET_ro = False
    flgUPDPET_bpar = False
    flgUPDPET_fchreq = False
    flgUPDPET_fchbp = False
    flgUPDPET_sector = False
    flgUPDPET_soli = False
    flgUPDPET_refe = False
    flgUPDPET_supe = False
    flgUPDPET_auto = False
    flgUPDPET_membenef = False
    flgUPDPET_memcarac = False
    flgUPDPET_memdescr = False
    flgUPDPET_memmotiv = False
    flgUPDPET_memobser = False
    flgUPDPET_memrelac = False
    flgUPDPET_DFLT_clase = "SINC"
    flgUPDPET_DFLT_estado = "PLANIF"
    flgUPDPET_DFLT_situac = ""
    
    If sp_GetTipoPerfil(glModoPeticion, xPerfDir, xPerfGer, PerfilInternoActual, CodigoCombo(cboTipopet, True)) Then
        If Not aplRST.EOF Then
            flgUPDPET_titulo = IIf(aplRST.Fields!petTitulo = "S", True, False)
            flgUPDPET_tipo = IIf(aplRST.Fields!petTipo = "S", True, False)
            flgUPDPET_clase = IIf(aplRST.Fields!petClase = "S", True, False)
            flgUPDPET_impacto = IIf(aplRST.Fields!petimpacto = "S", True, False)
            flgUPDPET_regulatorio = IIf(aplRST.Fields!petregulatorio = "S", True, False)
            flgUPDPET_prioridad = IIf(aplRST.Fields!petPrioridad = "S", True, False)
            flgUPDPET_empresa = IIf(aplRST.Fields!petempresa = "S", True, False)
            flgUPDPET_importancia = IIf(aplRST.Fields!petImportancia = "S", True, False)
            flgUPDPET_proyecto = IIf(aplRST.Fields!petproyecto = "S", True, False)
            flgUPDPET_corploc = IIf(aplRST.Fields!petcorploc = "S", True, False)
            flgUPDPET_orientacion = IIf(aplRST.Fields!petorientacion = "S", True, False)
            flgUPDPET_ro = IIf(aplRST.Fields!petro = "S", True, False)
            flgUPDPET_bpar = IIf(aplRST.Fields!petbpar = "S", True, False)
            flgUPDPET_fchreq = IIf(aplRST.Fields!petfchreq = "S", True, False)
            flgUPDPET_fchbp = IIf(aplRST.Fields!petfchbp = "S", True, False)
            flgUPDPET_sector = IIf(aplRST.Fields!PetSector = "S", True, False)
            flgUPDPET_soli = IIf(aplRST.Fields!petsoli = "S", True, False)
            flgUPDPET_refe = IIf(aplRST.Fields!petrefe = "S", True, False)
            flgUPDPET_supe = IIf(aplRST.Fields!petsupe = "S", True, False)
            flgUPDPET_auto = IIf(aplRST.Fields!petauto = "S", True, False)
            flgUPDPET_membenef = IIf(aplRST.Fields!petmembenef = "S", True, False)
            flgUPDPET_memcarac = IIf(aplRST.Fields!petmemcarac = "S", True, False)
            flgUPDPET_memdescr = IIf(aplRST.Fields!petmemdescr = "S", True, False)
            flgUPDPET_memmotiv = IIf(aplRST.Fields!petmemmotiv = "S", True, False)
            flgUPDPET_memobser = IIf(aplRST.Fields!petmemobser = "S", True, False)
            flgUPDPET_memrelac = IIf(aplRST.Fields!petmemrelac = "S", True, False)
            flgUPDPET_DFLT_clase = ClearNull(aplRST.Fields!petdfltclase)
            flgUPDPET_DFLT_estado = ClearNull(aplRST.Fields!petdfltestado)
            flgUPDPET_DFLT_situac = ClearNull(aplRST.Fields!petdfltsituac)
        End If
    Else
        MsgBox "No se encontr� la parametrizaci�n para este caso. Revise.", vbCritical + vbOKOnly
    End If
End Sub

Private Sub cmdAgrupar_Click()
    glSelectAgrup = ""
    frmPeticionAgrup.Show vbModal
    DoEvents
End Sub

Private Sub cmdAsigNro_Click()
    'en una epoca el numero podia asignarse manualmente
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        frmAsignaNumero.Show 1
        DoEvents
        If Me.Tag = "REFRESH" Then
            cmdAsigNro.Enabled = False
            Call InicializarPantalla(False)
        End If
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdCambioEstado_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        frmPeticionesCambioEstado.Show 1
        DoEvents
        If Me.Tag = "REFRESH" Then
            Call InicializarPantalla(False)
        End If
    End If
    Call LockProceso(False)
End Sub

'Private Sub cmdEliminar_Click()
'    If Not LockProceso(True) Then
'        Exit Sub
'    End If
'    If MsgBox("�Confirma la eliminaci�n definitiva de la petici�n?", vbQuestion + vbYesNo, "Eliminar una petici�n") = vbNo Then
'        Call LockProceso(False)
'        Exit Sub
'    End If
'    Call Puntero(True)
'    If Not sp_DeletePeticion(txtNrointerno.Text) Then
'        Call LockProceso(False)
'        Exit Sub
'    End If
'    Me.Hide
'    touchForms
'    Call LockProceso(False)
'    Call Puntero(False)
'    Unload Me
'End Sub

Private Sub cmdPrint_Click()
    Dim nCopias As Integer

    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion = "" Then
        MsgBox ("No selecciono ninguna Petici�n")
        Call LockProceso(False)
        Exit Sub
    End If
    
    rptFormulario.Show vbModal
    nCopias = rptFormulario.Copias
    Call Puntero(True)
    DoEvents
    While nCopias > 0
        If rptFormulario.chkFormu Then
            Call PrintFormulario(glNumeroPeticion)
        End If
        If rptFormulario.chkHisto Then
            Call PrintHitorial(glNumeroPeticion)
        End If
        nCopias = nCopias - 1
    Wend
    Call Puntero(False)
    
    Call LockProceso(False)
End Sub

Private Sub cmdVerAnexora_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If Val(getStrCodigo(txtNroAnexada.Text, True)) = 0 Then
        MsgBox ("Esta Petici�n no se encuentra anexada")
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    glNumeroPeticion = getStrCodigo(txtNroAnexada.Text, True)
    touchForms
    Call InicializarPantalla(True)
End Sub

Private Sub cmdVerAnexa_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If anxNroPeticion = "" Then
        MsgBox ("No selecciono ninguna Petici�n")
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    glNumeroPeticion = anxNroPeticion
    touchForms
    Call InicializarPantalla(True)
End Sub

'{ add -045- a.
Private Sub grdAdjPet_SelChange()
    If Not bFormateando Then
        Call adjSeleccion
    End If
End Sub
'}

'{ add -066- b.
Private Sub grdPetConf_SelChange()
    If Not bFormateando Then
        Call pcfSeleccion
    End If
End Sub
'}

Private Sub orjBtn_Click(Index As Integer)
    Dim i As Integer
    
    Call Puntero(True)                      ' add -057- a.
    If Index < 7 And Index >= 0 Then        ' upd -064- a.
        For i = 0 To orjBtn.Count - 1
            orjFiller(i).Visible = False
            orjBtn(i).Font.Bold = False
            orjBtn(i).BackColor = RGB(208, 208, 208)
            orjFrame(i).Visible = False
        Next
        orjFrame(Index).Visible = True
        orjBtn(Index).Font.Bold = True
        orjBtn(Index).BackColor = orjFrame(Index).BackColor
        orjFiller(Index).Visible = True
        '{ add -064- a.
        Select Case Index
            Case orjDAT: Call ValidarPeticion
            Case orjDOC: Call CargarDocMetod
            Case orjADJ: Call CargarAdjPet
            Case orjCNF: Call CargarPetConf
            Case orjEST: Call CargarEstado
        End Select
        '}
    End If
    Call Puntero(False)                 ' add -057- a.
End Sub

Private Sub cmdHistView_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glNumeroPeticion <> "" Then
        '{ add -042- b.
        Call Status("Cargando el historial...")
        Call Puntero(True)
        '}
        frmHistorial.Show 1
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdModificar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    glModoPeticion = "MODI"
    Call CargarPermisosEdicion
    Call HabilitarBotones
End Sub

Private Sub cmdCancel_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    lblCustomToolTip.Caption = ""       ' add -037- a.
    If glModoPeticion = "MODI" Then
        glModoPeticion = "VIEW"
        'CUIDADO
        Call InitTipoPet
        Call CargaPeticion(True)
        Call HabilitarBotones
    Else
        Unload Me
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdOk_Click()
    Call Puntero(True)
    Call Guardar
    Call Puntero(False)
End Sub

Private Sub Guardar()
    Dim sDir, sGer, sSec As String
    Dim xBpar, sSoli, sRefe, sSupe, sAuto As String
    Dim auxNro As String
    Dim xEstado As String
    Dim nNumeroAsignado As Long
    Dim iUsrPerfilActual As String, iUsrLoginActual As String
    Dim bMsg As Boolean
    Dim sTXTalcance As String
    Dim sTipoPet As String
    '{ add -001- m. Variables para guardar la leyenda de los cambios realizados para el historial
    Dim sCambioDeClase As String
    Dim sCambioImpacto As String
    Dim sCambioAtributo As String
    '}
    '{ add -044- a.
    Dim cGrupos() As String
    Dim bComprobar As Boolean
    Dim lGrupos As Long
    Dim cont As Long
    '}
    Dim SI_cod_sector As String, SI_cod_grupo As String, NroHistorial As Long   ' add -093- a.
    '{ add -094- a.
    Dim lPeriodo_Actual As Long, lPeriodo_Siguiente As Long
    Dim sPeriodo_Actual As String, sPeriodo_Siguiente As String
    '}
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    
    bMsg = False
    
    If CamposObligatorios Then
        If glModoPeticion = "ALTA" Then
            If Not valSectorHabil(CodigoCombo(cboSector, True)) Then
                MsgBox ("El sector solicitante est� marcado como inhabilitado."), vbOKOnly + vbInformation
                Call LockProceso(False)
                Exit Sub
            End If
        End If
        touchForms
        sSec = CodigoCombo(Me.cboSector, True)
        If sp_GetSectorXt(sSec, Null, Null) Then
            sDir = ClearNull(aplRST!cod_direccion)
            sGer = ClearNull(aplRST!cod_gerencia)
        End If
        
        iUsrPerfilActual = xUsrPerfilActual
        iUsrLoginActual = xUsrLoginActual
        If iUsrPerfilActual = "SADM" Then
            iUsrPerfilActual = "ADMI"
        End If
        If ClearNull(CodigoCombo(cboImportancia, True)) = "" Then
            iUsrPerfilActual = ""
            iUsrLoginActual = ""
        End If
        
        sSoli = CodigoCombo(Me.cboSolicitante)
        sRefe = CodigoCombo(Me.cboReferente)
        sSupe = CodigoCombo(Me.cboSupervisor)
        sAuto = CodigoCombo(Me.cboDirector)
        sTipoPet = CodigoCombo(Me.cboTipopet, True)
        
        If glModoPeticion = "ALTA" Then
            cModeloControl = MODELO_CONTROL_NUEVO
            If sTipoPet = "ESP" Then
                sRefe = sSoli
                sSupe = Null
                sAuto = sSoli
            End If
            xBpar = CodigoCombo(cboBpar)
            If InStr(1, "NOR|ESP|", sTipoPet, vbTextCompare) > 0 Then xBpar = getBpSector(CodigoCombo(cboSector, True))
            If sp_UpdatePeticion(txtNrointerno.Text, txtNroAsignado.Text, txtTitulo.Text, sTipoPet, CodigoCombo(Me.cboPrioridad, True), CodigoCombo(cboCorpLocal, True), CodigoCombo(cboOrientacion, True), CodigoCombo(cboImportancia, True), iUsrPerfilActual, iUsrLoginActual, getStrCodigo(txtNroAnexada.Text, True), prjNroInterno, txtFechaPedido.DateValue, txtFechaRequerida.DateValue, txtFechaComite.DateValue, txtFiniplan.DateValue, txtFfinplan.DateValue, txtFinireal.DateValue, txtFfinreal.DateValue, txtHspresup, sDir, sGer, sSec, sUsualta, sSoli, sRefe, sSupe, sAuto, sEstado, txtFechaEstado.DateValue, sSituacion, xBpar, CodigoCombo(cboClase, True), CodigoCombo(cboImpacto, True), cModeloControl, CodigoCombo(cboRegulatorio, True), CInt(txtPrjId.Text), CInt(txtPrjSubId.Text), CInt(txtPrjSubsId.Text), CodigoCombo(cboEmp, True), CodigoCombo(cboRO, True)) Then        ' add -015- a.  ' upd -037- a.    ' upd -095- a.
                If Not aplRST.EOF Then
                    auxNro = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")    ' recupera el ultimo numero generado
                End If
            End If
            '{ add -093- a.
            If sp_GetVarios("CTRLPET1") Then
                If ClearNull(aplRST.Fields!var_numero) = "1" Then
                    If CodigoCombo(cboTipopet, True) = "PRJ" Or _
                        CodigoCombo(cboImpacto, True) = "S" Or _
                        ValidarProyecto(txtPrjId, txtPrjSubId, txtPrjSubsId) Then
                            ' Agrega al sector y grupo de Seguridad Inform�tica (si no estaba asignado ya)
                            If sp_GetVarios("SI_SECT") Then SI_cod_sector = ClearNull(aplRST.Fields!var_texto)
                            If sp_GetVarios("SI_GRUP") Then SI_cod_grupo = ClearNull(aplRST.Fields!var_texto)
                            If Not sp_GetPeticionGrupoXt(glNumeroPeticion, SI_cod_sector, SI_cod_grupo) Then
                                NroHistorial = sp_AddHistorial(glNumeroPeticion, "SNEW000", sEstado, SI_cod_sector, "PLANIF", Null, Null, glLOGIN_ID_REEMPLAZO, "Sector agregado autom�ticamente (C104).")
                                Call sp_InsertPeticionSector(glNumeroPeticion, SI_cod_sector, Null, Null, Null, Null, 0, "PLANIF", date, "", NroHistorial, "0")
                                NroHistorial = sp_AddHistorial(glNumeroPeticion, "GNEW000", sEstado, SI_cod_sector, "PLANIF", SI_cod_grupo, "PLANIF", glLOGIN_ID_REEMPLAZO, "Grupo agregado autom�ticamente (C104).")
                                Call sp_InsertPeticionGrupo(glNumeroPeticion, SI_cod_grupo, Null, Null, Null, Null, Null, Null, 0, 0, "PLANIF", date, "", NroHistorial, "0")
                                Call sp_DoMensaje("SEC", "PNEW000", glNumeroPeticion, SI_cod_sector, sEstado, "", "")
                                Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, SI_cod_grupo, sEstado, "", "")
                                Call sp_DoMensaje("PET", "PCHGTYP2", glNumeroPeticion, SI_cod_grupo, "NULL", "", "")
                            End If
                    End If
                End If
            End If
            '}
        Else
            xBpar = CodigoCombo(cboBpar)
            ' ------------------------------------------------------
            ' REGISTRO DE CAMBIO DE TIPO
            ' ------------------------------------------------------
            '{ add -028- a.
            If glPETTipo <> "" Then         ' add -051- c. Solo registro cambios si ten�a datos antes de la edici�n
                If CodigoCombo(cboTipopet, True) <> glPETTipo Then
                    sCambioAtributo = "� Cambio de tipo � Antes: " & glPETTipo & " - Despu�s: " & CodigoCombo(cboTipopet, True)
                    Call sp_AddHistorial(glNumeroPeticion, "PCHGTYPE", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sCambioAtributo)
                    Call sp_DoMensaje("PET", "PCHGTYPE", glNumeroPeticion, "", "NULL", "", "")
                End If
            End If                          ' add -051- c.
            '}
            ' ------------------------------------------------------
            ' REGISTRO DE CAMBIO DE CLASE
            ' ------------------------------------------------------
            '{ add -001- m.
            If glPETClase <> "" And glPETClase <> "SINC" Then               ' add -051- c. Solo registro cambios si ten�a datos antes de la edici�n
                If CodigoCombo(cboClase, True) <> glPETClase Then
                    sCambioAtributo = "� Cambio de clase � Antes: " & glPETClase & " - Despu�s: " & CodigoCombo(cboClase, True)
                    Call sp_AddHistorial(glNumeroPeticion, "PCHGCLS", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sCambioAtributo)
                    Call sp_DoMensaje("PET", "PCHGCLS", glNumeroPeticion, "", "NULL", "", "")
                    '{ add -022- a. Aviso de cambio de clase para el grupo homologador
                    If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then    ' 1. Primero determina si ya existe para la petici�n el grupo Homologador
                        Call CargarGrupoHomologador                             ' 2. Obtengo los datos del grupo homologador
                        Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 114, sEstado, sCambioAtributo)
                    End If
                    '}
                    '{ add -092- a. Si la petici�n era un NUEV o EVOL, y se cambia la clase a ATEN/CORR/SPUF o SINC, se elimina de la planificaci�n
                    If InStr(1, "EVOL|NUEV|OPTI|", glPETClase, vbTextCompare) > 0 Then
                        If InStr(1, "ATEN|CORR|SPUF|SINC|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                            Call sp_DeletePeticionPlancab(Null, glNumeroPeticion)
                        End If
                    End If
                    '}
                End If
            '{ add -051- c. Entonces es la clasificaci�n inicial: Aviso al grupo Homologador.
            Else
                If CodigoCombo(cboClase, True) <> "SINC" Then
                    sCambioAtributo = "� Asignaci�n inicial de clase � " & TextoCombo(cboClase, CodigoCombo(cboClase, True), True)
                    Call sp_AddHistorial(glNumeroPeticion, "PNEWCLS", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sCambioAtributo)
                    Call sp_DoMensaje("PET", "PNEWCLS", glNumeroPeticion, "", "NULL", "", "")
                    If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then                    ' 1. Primero determina si ya existe para la petici�n el grupo Homologador
                        Call CargarGrupoHomologador                                             ' 2. Obtengo los datos del grupo homologador
                        Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 116, sEstado, sCambioAtributo)
                    End If
                End If
            End If
            ' ------------------------------------------------------
            ' REGISTRO DE IMPACTO TECNOL�GICO
            ' ------------------------------------------------------
            If CodigoCombo(cboImpacto, True) <> glPETImpacto Then
                sCambioAtributo = ""
                Select Case glPETImpacto
                    Case "-"
                        Select Case CodigoCombo(cboImpacto, True)
                            Case "N": sCambioAtributo = "� Imp. Tecnol�gico � Antes: indeterminado / Despu�s: SIN impacto tecnol�gico"
                            Case "S": sCambioAtributo = "� Imp. Tecnol�gico � Antes: indeterminado / Despu�s: CON impacto tecnol�gico"
                        End Select
                    Case "N"
                        Select Case CodigoCombo(cboImpacto, True)
                            Case "-": sCambioAtributo = "� Imp. Tecnol�gico � Antes: SIN impacto tecnol�gico / Despu�s: indeterminado"
                            Case "S": sCambioAtributo = "� Imp. Tecnol�gico � Antes: SIN impacto tecnol�gico / Despu�s: CON impacto tecnol�gico"
                        End Select
                    Case "S"
                        Select Case CodigoCombo(cboImpacto, True)
                            Case "-": sCambioAtributo = "� Imp. Tecnol�gico � Antes: CON impacto tecnol�gico / Despu�s: indeterminado"
                            Case "N": sCambioAtributo = "� Imp. Tecnol�gico � Antes: CON impacto tecnol�gico / Despu�s: SIN impacto tecnol�gico"
                        End Select
                End Select
                Call sp_AddHistorial(glNumeroPeticion, "PCHGIMP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sCambioAtributo)
                Call sp_DoMensaje("PET", "PCHGIMP", glNumeroPeticion, "", "NULL", "", "")
            End If
            '}
            '}
            ' ------------------------------------------------------
            ' REGISTRO DE REGULATORIO
            ' ------------------------------------------------------
            '{ add -036- a.
            If sRegulatorio <> "-" Then     ' add -051- c. Solo registro cambios si ten�a datos antes de la edici�n
                sCambioAtributo = ""
                If CodigoCombo(cboRegulatorio, True) <> sRegulatorio Then   ' Si cambi� el atributo
                    Select Case sRegulatorio
                        Case "-": sCambioAtributo = "� Regulatorio � Antes sin especificar. Luego " & IIf(CodigoCombo(cboRegulatorio, True) = "S", "SI", "NO") & " es regulatorio."
                        Case "N": sCambioAtributo = "� Regulatorio � Antes NO era regulatorio. Luego " & IIf(CodigoCombo(cboRegulatorio, True) = "-", "no se encuentra especificado.", "SI es regulatorio.")
                        Case "S": sCambioAtributo = "� Regulatorio � Antes SI era regulatorio. Luego " & IIf(CodigoCombo(cboRegulatorio, True) = "-", "no se encuentra especificado.", "NO es regulatorio.")
                    End Select
                    Call sp_AddHistorial(glNumeroPeticion, "PCHGREGU", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sCambioAtributo)
                    Call sp_DoMensaje("PET", "PCHGREGU", glNumeroPeticion, "", "NULL", "", "")
                End If
            End If      ' add -051- c.
            '}
            ' ------------------------------------------------------
            ' REGISTRO DE VISIBILIDAD (IMPORTANCIA)
            ' ------------------------------------------------------
            '{ add -040- b.
            If CodigoCombo(cboImportancia, True) <> xImportancia Then
                If xImportancia = "" Then
                    sCambioAtributo = "� Visibilidad � Antes: sin definir. Despu�s: " & TextoCombo(cboImportancia, CodigoCombo(cboImportancia, True), True) & "."
                Else
                    sCambioAtributo = "� Visibilidad � Antes: " & TextoCombo(cboImportancia, xImportancia, True) & ". Despu�s " & TextoCombo(cboImportancia, CodigoCombo(cboImportancia, True), True) & "."
                End If
                Call sp_AddHistorial(glNumeroPeticion, "PCHGVISI", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sCambioAtributo)
            End If
            ' ------------------------------------------------------
            ' REGISTRO DE CAMBIO DE BP
            ' ------------------------------------------------------
            If glModoPeticion = "MODI" Then
                sCambioAtributo = ""
                If ClearNull(CodigoCombo(cboBpar, False)) <> "" And ClearNull(CodigoCombo(cboBpar, False)) <> sBpar Then
                    sCambioAtributo = "� Cambio de Business Partner � Antes: " & sp_GetRecursoNombre(sBpar) & " (" & sBpar & ") / Despu�s: " & sp_GetRecursoNombre(CodigoCombo(cboBpar, False)) & " (" & CodigoCombo(cboBpar, False) & ")"
                    '{ add -094- a.
                    lPeriodo_Actual = 0: lPeriodo_Siguiente = 0
                    If sp_GetPeriodo(Null) Then         ' Obtengo el per�odo de planificaci�n actual y el pr�ximo (en caso que se estuviera planificando para el pr�ximo per�odo)
                        Do While Not aplRST.EOF
                            If ClearNull(aplRST.Fields!vigente) = "S" Then
                                lPeriodo_Actual = ClearNull(aplRST.Fields!per_nrointerno)
                                sPeriodo_Actual = ClearNull(aplRST.Fields!per_abrev)
                                aplRST.MoveNext
                                If Not aplRST.EOF Then
                                    lPeriodo_Siguiente = ClearNull(aplRST.Fields!per_nrointerno)
                                    sPeriodo_Siguiente = ClearNull(aplRST.Fields!per_abrev)
                                End If
                                Exit Do
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                        If sp_GetPeticionPlancab(lPeriodo_Actual, glNumeroPeticion) Then
                            If lPeriodo_Actual <> 0 Then
                                Call sp_UpdatePeticionPlancabField(lPeriodo_Actual, glNumeroPeticion, "COD_BPAR", CodigoCombo(cboBpar, False), Null, Null)
                                sCambioAtributo = sCambioAtributo & vbCrLf & "- Por cambio de BP se actualiz� la petici�n planificada en " & sPeriodo_Actual
                            End If
                        End If
                        If sp_GetPeticionPlancab(lPeriodo_Siguiente, glNumeroPeticion) Then
                            If lPeriodo_Siguiente <> 0 Then
                                Call sp_UpdatePeticionPlancabField(lPeriodo_Siguiente, glNumeroPeticion, "COD_BPAR", CodigoCombo(cboBpar, False), Null, Null)
                                sCambioAtributo = sCambioAtributo & vbCrLf & "- Por cambio de BP se actualiz� la petici�n planificada en " & sPeriodo_Siguiente
                            End If
                        End If
                    End If
                    '}
                    Call sp_AddHistorial(glNumeroPeticion, "PCHGBP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sCambioAtributo)
                End If
            End If
            
            If sp_UpdatePeticion(txtNrointerno.Text, txtNroAsignado.Text, txtTitulo.Text, sTipoPet, CodigoCombo(Me.cboPrioridad, True), CodigoCombo(cboCorpLocal, True), CodigoCombo(cboOrientacion, True), CodigoCombo(cboImportancia, True), iUsrPerfilActual, iUsrLoginActual, getStrCodigo(txtNroAnexada.Text, True), prjNroInterno, txtFechaPedido.DateValue, txtFechaRequerida.DateValue, txtFechaComite.DateValue, txtFiniplan.DateValue, txtFfinplan.DateValue, txtFinireal.DateValue, txtFfinreal.DateValue, txtHspresup, sDir, sGer, sSec, sUsualta, sSoli, sRefe, sSupe, sAuto, sEstado, txtFechaEstado.DateValue, sSituacion, xBpar, CodigoCombo(cboClase, True), cboImpacto, cModeloControl, cboRegulatorio.List(cboRegulatorio.ListIndex), CInt(txtPrjId.Text), CInt(txtPrjSubId.Text), CInt(txtPrjSubsId.Text), CodigoCombo(cboEmp, True), CodigoCombo(cboRO, True)) Then        ' add -015- a.  ' upd -037- a.   ' upd -095- a.
                If Not aplRST.EOF Then
                    auxNro = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
                End If
            End If
            '{ add -093- a.
            If sp_GetVarios("CTRLPET1") Then
                If ClearNull(aplRST.Fields!var_numero) = "1" Then
                    If CodigoCombo(cboTipopet, True) = "PRJ" Or _
                        CodigoCombo(cboImpacto, True) = "S" Or _
                        ValidarProyecto(txtPrjId, txtPrjSubId, txtPrjSubsId) Then
                            ' Agrega al sector y grupo de Seguridad Inform�tica (si no estaba asignado ya)
                            If sp_GetVarios("SI_SECT") Then SI_cod_sector = ClearNull(aplRST.Fields!var_texto)
                            If sp_GetVarios("SI_GRUP") Then SI_cod_grupo = ClearNull(aplRST.Fields!var_texto)
                            If Not sp_GetPeticionGrupoXt(glNumeroPeticion, SI_cod_sector, SI_cod_grupo) Then
                                NroHistorial = sp_AddHistorial(glNumeroPeticion, "SNEW000", sEstado, SI_cod_sector, "PLANIF", Null, Null, glLOGIN_ID_REEMPLAZO, "Sector agregado autom�ticamente (C104).")
                                Call sp_InsertPeticionSector(glNumeroPeticion, SI_cod_sector, Null, Null, Null, Null, 0, "PLANIF", date, "", NroHistorial, "0")
                                NroHistorial = sp_AddHistorial(glNumeroPeticion, "GNEW000", sEstado, SI_cod_sector, "PLANIF", SI_cod_grupo, "PLANIF", glLOGIN_ID_REEMPLAZO, "Grupo agregado autom�ticamente (C104).")
                                Call sp_InsertPeticionGrupo(glNumeroPeticion, SI_cod_grupo, Null, Null, Null, Null, Null, Null, 0, 0, "PLANIF", date, "", NroHistorial, "0")
                                Call sp_DoMensaje("SEC", "SNEW000", glNumeroPeticion, SI_cod_sector, sEstado, "", "")
                                Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, SI_cod_grupo, sEstado, "", "")
                                Call sp_DoMensaje("PET", "PCHGTYP2", glNumeroPeticion, SI_cod_grupo, "NULL", "", "")
                            End If
                    End If
                End If
            End If
            '}
        End If
        
        sTXTalcance = ""
        If Not IsEmpty(auxNro) And auxNro <> "" Then
            glNumeroPeticion = auxNro
            If bFlgDescripcion Then
                Call sp_UpdateMemo(auxNro, "DESCRIPCIO", memDescripcion.Text)
                If flgExtension = True Then
                    sTXTalcance = sTXTalcance & "� DESCRIPCION ANT. �" & vbCrLf & txDescripcion & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgCaracteristicas Then
                Call sp_UpdateMemo(auxNro, "CARACTERIS", memCaracteristicas.Text)
                If flgExtension = True Then
                    sTXTalcance = sTXTalcance & "� CARACTERISTICAS ANT. �" & vbCrLf & txCaracteristicas & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgMotivos Then
                Call sp_UpdateMemo(auxNro, "MOTIVOS", memMotivos.Text)
                If flgExtension = True Then
                    sTXTalcance = sTXTalcance & "� MOTIVOS ANT. �" & vbCrLf & txMotivos & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgBeneficios Then
                Call sp_UpdateMemo(auxNro, "BENEFICIOS", memBeneficios.Text)
                If flgExtension = True Then
                    sTXTalcance = sTXTalcance & "� BENEFICIOS ANT. �" & vbCrLf & txBeneficios & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgRelaciones Then
                Call sp_UpdateMemo(auxNro, "RELACIONES", memRelaciones.Text)
                If flgExtension = True Then
                    sTXTalcance = sTXTalcance & "� RELACIONES ANT. �" & vbCrLf & txRelaciones & vbCrLf
                    bMsg = True
                End If
            End If
            If bFlgObservaciones Then
                Call sp_UpdateMemo(auxNro, "OBSERVACIO", memObservaciones.Text)
                If flgExtension = True Then
                    sTXTalcance = sTXTalcance & "� OBSERVACIONES ANT. �" & vbCrLf & txObservaciones & vbCrLf
                    bMsg = True
                End If
            End If
        End If
        ' ------------------------------------------------------
        ' REGISTRO DE CAMBIO DE TEXTOS DE LA PETICI�N
        ' ------------------------------------------------------
        If bMsg = True Then
            Call sp_AddHistorial(glNumeroPeticion, "PCHGTXT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, sTXTalcance)
            Call sp_DoMensaje("PET", "PCHGTXT", glNumeroPeticion, "", "NULL", "", "")
        End If
        'CASO MUY ESPECIAL alta de peticiones PROPIAS
        ' Porque debe ser agregado de manera autom�tica el sector y grupo del solicitante (FJS)
        If (sTipoPet = "PRO" Or sTipoPet = "ESP" Or sTipoPet = "PRJ") And glModoPeticion = "ALTA" Then
            nNumeroAsignado = sp_ProximoNumero("ULPETASI")  ' Asigna el numero automaticamente
            If nNumeroAsignado > 0 Then
                If Not sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, nNumeroAsignado) Then
                    MsgBox ("Error al asignar n�mero")
                End If
            Else
                MsgBox ("Error al asignar n�mero")
            End If
            xEstado = "PLANIF"      ' add -003- f.
            '{ add -001- l.
            If sTipoPet = "ESP" Then
                If InStr(1, "CSEC|CGRU|ANAL|", PerfilInternoActual, vbTextCompare) > 0 Then     ' upd -051- a.
                    '{ add -002- j.
                    If glLOGIN_Direccion <> "MEDIO" Or PerfilInternoActual = "ANAL" Then        ' upd -051- a.
                        xEstado = "COMITE"
                    End If
                    '}
                End If
            End If
            '}
            Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", xEstado, Null, Null)
            If sTipoPet = "PRO" Or sTipoPet = "ESP" Then
                ' Observaci�n: a pesar de existir el evento o acci�n de alta de petici�n "especial"
                ' siempre se esta usando el evento o acci�n de petici�n "propia". Una "especial" es
                ' siempre una propia pero de otra subclase.
                If sTipoPet = "PRO" Then
                    Call sp_AddHistorial(glNumeroPeticion, "PNEW003", xEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "� Alta de Petici�n �")
                Else
                    Call sp_AddHistorial(glNumeroPeticion, "PNEW002", xEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "� Alta de Petici�n �")
                End If
            Else
                Call sp_AddHistorial(glNumeroPeticion, "PNEW004", xEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "� Alta de Proyecto �")
            End If
            'si es CSEC o CGRU o ANAL genera un sector
            'If PerfilInternoActual = "CSEC" Or PerfilInternoActual = "CGRU" Then       ' del -051- a.
            If InStr(1, "CSEC|CGRU|ANAL", PerfilInternoActual, vbTextCompare) > 0 Then  ' upd -051- a. Se agrega perfil ANAL
                If sp_InsertPeticionSector(glNumeroPeticion, xPerfSec, Null, Null, Null, Null, 0, xEstado, date, "", 0, "0") Then
                    'si es un CGRU o un ANAL a nivel de grupo ademas genera un grupo
                    If (PerfilInternoActual = "CGRU" Or PerfilInternoActual = "ANAL") And xPerfGru <> "" Then   ' upd -051- a. Se agrega perfil ANAL
                        Call sp_InsertPeticionGrupo(glNumeroPeticion, xPerfGru, Null, Null, Null, Null, Null, Null, 0, 0, xEstado, date, "", 0, "0") ' upd -010- a.
                        ' Agrego al historial el agregado de grupo automatico agarrado del solicitante
                        Call sp_AddHistorial(glNumeroPeticion, "GNEW000", xEstado, xPerfSec, xEstado, xPerfGru, xEstado, glLOGIN_ID_REEMPLAZO, "� Alta de Petici�n �")
                    End If
                    If InStr(1, "CGRU|ANAL", PerfilInternoActual, vbTextCompare) > 0 Then   ' upd -051- f.
                        If sTipoPet = "PRO" Or sTipoPet = "ESP" Then
                            If sTipoPet = "PRO" Then
                                Call sp_DoMensaje("SEC", "PNEW003", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                            Else
                                Call sp_DoMensaje("SEC", "PNEW002", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                            End If
                        ElseIf sTipoPet = "PRJ" Then
                            Call sp_DoMensaje("SEC", "PNEW004", glNumeroPeticion, CodigoCombo(Me.cboSector, True), xEstado, "", "")
                        End If
                    End If
                    If sTipoPet = "PRO" Then
                        Call sp_DoMensaje("PET", "PNEW003", glNumeroPeticion, "", "NULL", "", "")
                    ElseIf sTipoPet = "ESP" Then
                        Call sp_DoMensaje("PET", "PNEW002", glNumeroPeticion, "", "NULL", "", "")
                    ElseIf sTipoPet = "PRJ" Then
                        Call sp_DoMensaje("PET", "PNEW004", glNumeroPeticion, "", "NULL", "", "")
                    End If
                End If
            End If
        End If
        '{ add -010- a. - Homologaci�n
        If glModoPeticion = "ALTA" And CodigoCombo(cboClase, True) <> "SINC" Then    ' Si estoy dando de alta una petici�n y ya tiene la clase designada
            If Not sp_GetPetGrupoHomologacion(glNumeroPeticion) Then                    ' 1. Primero determina si ya existe para la petici�n el grupo Homologador
                If sp_GetGrupoHomologable(xPerfGru) Then                                ' 2. Determina si el grupo a agregar a la petici�n es Homologable
                    If sp_GetGrupoHomologacion Then                                 ' 3. Primero obtengo el grupo Homologador
                        Call Status("Anexando grupo homologador...")
                        cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
                        cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
                        aplRST.Close
                        If Not valGrupoHabil(cGrupoHomologador) Then
                            MsgBox ("El grupo Homologador se encuentra en estado inhabilitado.")
                            Call LockProceso(False)
                            Exit Sub
                        End If
                        If cGrupoHomologadorSector <> xPerfSec Then                 ' 4. Agrego el sector del grupo homologador
                            Call sp_InsertPeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
                        End If
                        If sp_InsertPeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", 0, "0") Then   ' 5. Es agregado el grupo Homologador en estado "A estimar Esfuerzo"
                            Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, cGrupoHomologador, "ESTIMA", "", "")                                              ' Se envian los mensajes correspondientes a Responsables de Grupo
                            Call Status("Actualizando el historial...")             ' 6. Detalla el historial
                            Select Case CodigoCombo(cboClase, True)
                                Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", xEstado, cGrupoHomologadorSector, "ESTIMA", cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso REDUCIDO) �")
                                Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", xEstado, cGrupoHomologadorSector, "ESTIMA", cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso COMPLETO) �")
                            End Select
                            Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")   ' upd -010- a.  ' 7. Esta llamada es solo para darle el numero de historial
                        End If
                    End If
                End If
            End If
        Else
            'If Not InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) > 0 Then    ' add -055- a.
            If InStr(1, "APROBA|SUSPEN|PLANOK|PLANIF|PLANRK|EJECRK|ESTIOK|ESTIRK|ESTIMA|EJECUC|", sEstado, vbTextCompare) > 0 Then
                bComprobar = False
                lGrupos = 0
                cont = 0
                If CodigoCombo(cboClase, True) <> "SINC" Then                                   ' 1. Si la petici�n tiene designada una clase
                    If Not sp_GetPetGrupoHomologacion(glNumeroPeticion) Then                ' 2. Determina si ya existe para la petici�n el grupo Homologador
                        If sp_GetPeticionGrupoXt(glNumeroPeticion, Null, Null, Null) Then   ' 3. Si no existe... determina si existe vinculado a la petici�n alg�n grupo de DyD
                            Do While Not aplRST.EOF
                                If Trim(aplRST.Fields!cod_gerencia) = "DESA" Then           ' Si el grupo pertenece a DyD, lo cargo en un vector din�mico
                                    bComprobar = True
                                    lGrupos = lGrupos + 1
                                    ReDim Preserve cGrupos(lGrupos)
                                    cGrupos(lGrupos) = ClearNull(aplRST.Fields!cod_grupo)
                                End If
                                aplRST.MoveNext
                                DoEvents
                            Loop
                            If bComprobar Then                                              ' 4. Si existe al menos uno, comprueba si es Homologable o no
                                bComprobar = False
                                For cont = 1 To lGrupos
                                    If sp_GetGrupoHomologable(cGrupos(cont)) Then
                                        bComprobar = True
                                        Exit For
                                    End If
                                Next cont
                            End If
                            If bComprobar Then                                              ' 5. Si alguno es homologable, entonces agrego al grupo Homologador
                                If sp_GetGrupoHomologacion Then                             ' 6. Si existe definido el grupo Homologador...
                                    Call Status("Anexando grupo homologador...")
                                    cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
                                    cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
                                    'aplRST.Close
                                    If Not valGrupoHabil(cGrupoHomologador) Then
                                        MsgBox ("El grupo Homologador se encuentra en estado inhabilitado.")
                                        Call LockProceso(False)
                                        Exit Sub
                                    End If
                                    If xPerfSec <> cGrupoHomologadorSector Then             ' 7. Agrego el sector del grupo homologador
                                        Call sp_InsertPeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
                                    Else
                                        If sp_GetPeticionSector(glNumeroPeticion, xPerfSec) Then    ' Si ya existe el sector del grupo homologador, averiguo el estado actual para mantenerlo
                                            cSectorHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
                                        End If
                                    End If
                                    Call sp_InsertPeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", 0, "0")      ' 8. Es agregado el grupo Homologador en estado "A estimar Esfuerzo"
                                    Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, cGrupoHomologador, "ESTIMA", "", "")                                                  ' Se envian los mensajes correspondientes a Responsables de Grupo
                                    Call Status("Actualizando el historial...")     ' 6. Detalla el historial
                                    Select Case CodigoCombo(cboClase, True)
                                        Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", sEstado, cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso REDUCIDO) �")
                                        Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", sEstado, cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso COMPLETO) �")
                                    End Select
                                    Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")
                                End If
                            End If
                        End If
                    Else                                                                ' Solo si hay cambio de clase y homologaci�n no esta activo
                        If glPETClase <> CodigoCombo(cboClase, True) Then
                            bComprobar = False
                            If sp_GetPetGrupoHomologable(glNumeroPeticion) Then         ' Compruebo si existen grupos homologables activos
                                Do While Not aplRST.EOF
                                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                                        bComprobar = True
                                        Exit Do
                                    End If
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                            If bComprobar Then      ' Si existe al menos un grupo homologable en estado activo
                                Call CargarGrupoHomologador
                                If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, cGrupoHomologador) Then
                                    Do While Not aplRST.EOF
                                        If ClearNull(aplRST.Fields!homo) = "H" Then
                                            '{ add -073- d. - Si el cambio de clase es de una menor a una mayor, aunque Homologaci�n se encuentre en rechazo t�cnico, debe ser reactivado
                                            If InStr(1, "ATEN|SPUF|CORR|", glPETClase, vbTextCompare) > 0 And InStr(1, "NUEV|EVOL|OPTI|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                                                If InStr(1, "RECHAZ|CANCEL|TERMIN|ANEXAD|ANULAD|RECHTE|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                                                    Select Case CodigoCombo(cboClase, True)
                                                        Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", sEstado, cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Reactivaci�n autom�tica del grupo Homologador por cambio de clase: 1� Punto de Control (Proceso REDUCIDO) �")
                                                        Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", sEstado, cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Reactivaci�n autom�tica del grupo Homologador por cambio de clase: 1� Punto de Control (Proceso COMPLETO) �")
                                                    End Select
                                                    ' Si homologaci�n se encuentra en alguno de los estados terminales, debo reactivarlo
                                                    Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "ESTADO", "ESTIMA", Null, Null)
                                                    Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")
                                                    ' Si no existen otros grupos adem�s del grupo homologador, le fuerzo el estado del sector al mismo que el grupo Homologador
                                                    bComprobar = False
                                                    If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, Null) Then
                                                        Do While Not aplRST.EOF
                                                            If ClearNull(aplRST.Fields!cod_grupo) <> cGrupoHomologador Then
                                                                bComprobar = True
                                                                Exit Do
                                                            End If
                                                            aplRST.MoveNext
                                                            DoEvents
                                                        Loop
                                                        If Not bComprobar Then
                                                            Call sp_UpdatePeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, "ESTIMA", date, "", NroHistorial, "0")
                                                        End If
                                                    End If
                                                    Exit Do
                                                End If
                                            Else
                                                If InStr(1, "RECHAZ|CANCEL|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                                                    Select Case CodigoCombo(cboClase, True)
                                                        Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", sEstado, cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Reactivaci�n autom�tica del grupo Homologador por cambio de clase: 1� Punto de Control (Proceso REDUCIDO) �")
                                                        Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", sEstado, cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Reactivaci�n autom�tica del grupo Homologador por cambio de clase: 1� Punto de Control (Proceso COMPLETO) �")
                                                    End Select
                                                    ' Si homologaci�n se encuentra en alguno de los estados terminales, debo reactivarlo
                                                    Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "ESTADO", "ESTIMA", Null, Null)
                                                    Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")
                                                    ' Si no existen otros grupos adem�s del grupo homologador, le fuerzo el estado del sector al mismo que el grupo Homologador
                                                    bComprobar = False
                                                    If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, Null) Then
                                                        Do While Not aplRST.EOF
                                                            If ClearNull(aplRST.Fields!cod_grupo) <> cGrupoHomologador Then
                                                                bComprobar = True
                                                                Exit Do
                                                            End If
                                                            aplRST.MoveNext
                                                            DoEvents
                                                        Loop
                                                        If Not bComprobar Then
                                                            Call sp_UpdatePeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, "ESTIMA", date, "", NroHistorial, "0")
                                                        End If
                                                    End If
                                                    Exit Do
                                                End If
                                            End If
                                            '}
                                        End If
                                        aplRST.MoveNext
                                        DoEvents
                                    Loop
                                End If
                            End If      ' add -073- d.
                        End If
                    End If
                End If
            End If                      ' add -055- a.
        End If
        Me.Tag = "REFRESH"
        Call touchForms
        If glModoPeticion = "ALTA" Then
            glModoPeticion = "VIEW"
            Call InicializarPantalla(False)
            DoEvents
        Else
            glModoPeticion = "VIEW"
            Call InicializarPantalla(False)
            DoEvents
        End If
    End If
    Call LockProceso(False)
    lblCustomToolTip.Caption = ""   ' add -037- a.
End Sub

Private Sub InitTipoPet()
    Dim i As Integer
    
    Call Debuggear(Me, "InitTipoPet")
    
    If sp_GetTipoPerfil(glModoPeticion, xPerfDir, xPerfGer, PerfilInternoActual, Null) Then
        If Not aplRST.EOF Then
            cboTipopet.Clear
            Do While Not aplRST.EOF
                'ReDim Preserve vPetTipo(i)
                'vPetTipo(i) =  ClearNull(aplRST.Fields!cod_tipopet)
                cboTipopet.AddItem ClearNull(aplRST.Fields!nom_tipopet) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_tipopet)
                i = i + 1
                aplRST.MoveNext
                DoEvents
            Loop
            cboTipopet.ListIndex = PosicionCombo(cboTipopet, "PRO", True)
            If i = 1 Then cboTipopet.Enabled = False
        End If
    End If
    
    Call CargarPetClase
'    With cboClase
'        .Clear
'        If sp_GetTipoPetClase(CodigoCombo(cboTipopet, True), Null) Then
'            Do While Not aplRST.EOF
'                .AddItem ClearNull(aplRST.Fields!nom_clase) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_clase)
'                aplRST.MoveNext
'                DoEvents
'            Loop
'        End If
'        .ListIndex = 0
'        If .ListCount > 1 Then .Enabled = True
'    End With
    
'    '{ add -101- b.
'    With cboTipopet
'        .Clear
'        If glModoPeticion = "ALTA" Then
'            If sp_GetTipoPet(Null) Then
'                Do While Not aplRST.EOF
'                    .AddItem ClearNull(aplRST.Fields!nom_tipopet) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_tipopet)
'                    aplRST.MoveNext
'                    DoEvents
'                Loop
'                .ListIndex = 0
'            End If
'        Else
'
'        End If
'    End With
'    '}

'{ del -101- b.
'    With cboTipopet
'        .Clear
'        Select Case glModoPeticion
'            Case "ALTA"
'                Select Case PerfilInternoActual
'                    Case "ADMI"
'                        .AddItem "Normal" & ESPACIOS & "||NOR"
'                        .AddItem "Propia" & ESPACIOS & "||PRO"
'                        .AddItem "Especial" & ESPACIOS & "||ESP"
'                        .AddItem "Auditor�a Interna" & ESPACIOS & "||AUI"
'                        .AddItem "Auditor�a Externa" & ESPACIOS & "||AUX"
'                    Case "SADM"
'                        .AddItem "Normal" & ESPACIOS & "||NOR"
'                        .AddItem "Propia" & ESPACIOS & "||PRO"
'                        .AddItem "Especial" & ESPACIOS & "||ESP"
'                        .AddItem "Proyecto" & ESPACIOS & "||PRJ"
'                        .AddItem "Auditor�a Interna" & ESPACIOS & "||AUI"
'                        .AddItem "Auditor�a Externa" & ESPACIOS & "||AUX"
'                    Case "SOLI"
'                        .AddItem "Normal" & ESPACIOS & "||NOR"      ' add -004- f.
'                    Case "CGRU", "CSEC"
'                        If glLOGIN_Direccion <> "MEDIO" Then
'                            .AddItem "Normal" & ESPACIOS & "||NOR"
'                        Else
'                            .AddItem "Propia" & ESPACIOS & "||PRO"
'                            .AddItem "Especial" & ESPACIOS & "||ESP"
'                        End If
'                        txtFechaComite.Text = Format(date, "dd/mm/yyyy")
'                    Case "BPAR"
'                        .AddItem "Normal" & ESPACIOS & "||NOR"
'                        .AddItem "Proyecto" & ESPACIOS & "||PRJ"
'                        .AddItem "Especial" & ESPACIOS & "||ESP"
'                    Case "ANAL"
'                        '{ add -098- a.
'                        .AddItem "Propia" & ESPACIOS & "||PRO"
'                        .AddItem "Especial" & ESPACIOS & "||ESP"
'                        '}
'                        '{ del -098- a.
'                        'If InStr(1, "ORG.|TRYAPO", glLOGIN_Gerencia, vbTextCompare) > 0 Then
'                        '    .AddItem "Propia" & ESPACIOS & "||PRO"      ' add -051- e.
'                        '    .AddItem "Especial" & ESPACIOS & "||ESP"
'                        'Else
'                        '    .AddItem "Propia" & ESPACIOS & "||PRO"
'                        'End If
'                        '}
'                        txtFechaComite.Text = Format(date, "dd/mm/yyyy")    ' add -051- g.
'                End Select
'            Case Else
'                .AddItem "Normal" & ESPACIOS & "||NOR"
'                .AddItem "Propia" & ESPACIOS & "||PRO"
'                .AddItem "Proyecto" & ESPACIOS & "||PRJ"
'                .AddItem "Especial" & ESPACIOS & "||ESP"
'                .AddItem "Auditor�a Interna" & ESPACIOS & "||AUI"
'                .AddItem "Auditor�a Externa" & ESPACIOS & "||AUX"
'        End Select
'        .ListIndex = 0
'    End With
'}
    'Call InitPetClass    ' add -005- b.
End Sub

'{ add -005- b.
'Private Sub InitPetClass()
Private Sub InitClasePet()
    Debug.Print Now & ": InitPetClass"

    With cboClase
        If sp_GetClase(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nom_clase) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_clase)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .ListIndex = 0
    End With
'    With cboClase
'        .Clear
'        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'        .AddItem "Correctivo" & ESPACIOS & "||CORR"
'        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'        .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'        .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'        .AddItem "SPUFI" & ESPACIOS & "||SPUF"
'        .ListIndex = 0
'    End With
End Sub
'}

Public Sub InicializarPantalla(Optional bCompleto)
    If IsMissing(bCompleto) Then
        bCompleto = True
    End If
    Me.Tag = ""
    flgEnCarga = True
    Call LockProceso(True)
    sSoli = ""
    sRefe = ""
    sDire = ""
    sSupe = ""
    sUsualta = glLOGIN_ID_REEMPLAZO
    xUsrLoginActual = glLOGIN_ID_REEMPLAZO
    xUsrPerfilActual = PerfilInternoActual
    cmdModificar.Enabled = False
    cmdOk.Enabled = False
    cmdPrint.Enabled = False
    cmdHistView.Enabled = False
    cmdRptHs.Enabled = False
    cmdAgrupar.Enabled = False
    cmdCambioEstado.Enabled = False
    cmdSector.Enabled = False
    cmdGrupos.Enabled = False
'{ del -101- b.
'    '{ add -001- h.
'    Select Case glModoPeticion
'        Case "ALTA"
'            With cboAdjClass
'                .Clear
'                .AddItem "C102 : Dise�o de soluci�n"
'                .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
'                .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
'                .AddItem "C204 : Casos de prueba"
'                .AddItem "CG04 : Documento de cambio de alcance"
'                .AddItem "T700 : Plan de implantaci�n Desarrollo"       ' Este es el T700
'                .AddItem "T710 : Principios de Desarrollo Aplicativo"   ' add -028- a.
'                .AddItem "C310 : Definici�n de Base de Datos"           ' add -038- a.
'                .AddItem "EML1 : Mail de OK alcance"
'                .AddItem "EML2 : Mail de OK prueba de usuario"
'                .AddItem "OTRO : Otros documentos"                      ' add -003- g.
'                .ListIndex = -1
'            End With
'            '{ add -031- a.
'            With cboAdjHomoClass
'                .Clear
'                .AddItem "IDH1 : Informe de Homologaci�n HSOB"
'                .AddItem "IDH2 : Informe de Homologaci�n HOME"
'                .AddItem "IDH3 : Informe de Homologaci�n HOMA"
'                .AddItem "OTRO : Otros documentos"
'                .ListIndex = 0
'            End With
'            '}
'        Case Else
'            If cModeloControl = MODELO_CONTROL_NUEVO Then       ' add -007- a.
'                With cboAdjClass
'                    .Clear
'                    .AddItem "C102 : Dise�o de soluci�n"
'                    .AddItem "C100 : Documento de alcance (c/impacto tecnol�gico y/o proyecto)"
'                    .AddItem "P950 : Documento de alcance (s/impacto tecnol�gico ni proyecto)"
'                    .AddItem "C204 : Casos de prueba"
'                    .AddItem "CG04 : Documento de cambio de alcance"
'                    .AddItem "T700 : Plan de implantaci�n Desarrollo"       ' Este es el T700
'                    .AddItem "T710 : Principios de Desarrollo Aplicativo"   ' add -028- a.
'                    .AddItem "C310 : Definici�n de Base de Datos"           ' add -038- a.
'                    .AddItem "EML1 : Mail de OK alcance"
'                    .AddItem "EML2 : Mail de OK prueba de usuario"
'                    .AddItem "OTRO : Otros documentos"                      ' add -003- g.
'                    .ListIndex = -1
'                End With
'                With cboAdjHomoClass
'                    .Clear
'                    .AddItem "IDH1 : Informe de Homologaci�n HSOB"
'                    .AddItem "IDH2 : Informe de Homologaci�n HOME"
'                    .AddItem "IDH3 : Informe de Homologaci�n HOMA"
'                    .AddItem "OTRO : Otros documentos"
'                    .ListIndex = 0
'                End With
'            Else
'                With cboAdjClass
'                    .Clear
'                    .AddItem "REQU : Requerimiento"
'                    .AddItem "ALCA : Doc. de alcance desarrollo"
'                    .AddItem "COST : Evaluaci�n costo - beneficio"
'                    .AddItem "PLEJ : Plan de ejecuci�n"
'                    .AddItem "PLIM : Plan de implantaci�n Desarrollo"
'                    .AddItem "MAIL : Mail"
'                    .AddItem "OTRO : Otros"
'                    .ListIndex = -1
'                End With
'            End If
'    End Select
'}
    Select Case glModoPeticion
        Case "ALTA"
            Call InicializarControles(glModoPeticion)
            txtNrointerno.Text = ""
            txtFechaPedido.Text = Format$(date, "ddmmyyyy")
            txtFechaPedido.ForceFormat
            If PerfilInternoActual = "SADM" Then
                txtFechaComite.Text = Format(date, "dd/mm/yyyy")
                txtFechaComite.ForceFormat
            End If
            If PerfilInternoActual <> "SADM" Then
                sSoli = glLOGIN_ID_REEMPLAZO
            End If
            Call InicializarCombos
        Case Else
            Call InicializarControles(glModoPeticion)
            Call InicializarCombos
            Call CargaPeticion(bCompleto)
            Me.Caption = cTAG & IIf(Val(txtNroAsignado) > 0, Format(txtNroAsignado, "###,###,###"), "S/N")
    End Select
    
    If bCompleto Then Call CargaAnexas
    
    Call HabilitarBotones
    
    bFlgObservaciones = False
    bFlgBeneficios = False
    bFlgCaracteristicas = False
    bFlgMotivos = False
    bFlgRelaciones = False
    bFlgDescripcion = False
    On Error Resume Next
    Call orjBtn_Click(orjMEM)
    DoEvents
    Call orjBtn_Click(orjDAT)
    DoEvents
    Call Puntero(False)      ' add -065- a.
    txtTitulo.SetFocus
    DoEvents
    Call LockProceso(False)
    flgEnCarga = False
End Sub

Private Sub HabilitarBotones()
    Dim PetClassActual As String
    Dim PetTypeActual As String
    Dim lPeriodoActual As Long                              ' add -078- a.
    
    Call setHabilCtrl(cboEmp, "DIS")                        ' add -095- a.
    Call setHabilCtrl(cboTipopet, "DIS")
    Call setHabilCtrl(cboClase, "DIS")
    Call setHabilCtrl(cmdAyudaClase, "DIS")
    Call setHabilCtrl(cboImpacto, "DIS")
    Call setHabilCtrl(cboRegulatorio, "DIS")
    Call setHabilCtrl(cmdPlanificar, "DIS")
    Call InhabilitarProyectos
    cmdModificar.Enabled = False
    cmdOk.Enabled = False
    cmdPrint.Enabled = False
    cmdHistView.Enabled = False
    cmdRptHs.Enabled = False
    cmdCambioEstado.Enabled = False
    cmdSector.Enabled = False
    cmdGrupos.Enabled = False
    cmdCancel.Enabled = True
    cmdAgrupar.Enabled = False
    cboImportancia.Locked = False
    cboImportancia.ToolTipText = ""
    
    Call setHabilCtrl(txtNrointerno, "DIS")                 ' add -009- a.
    Call setHabilCtrl(txtTitulo, "DIS")
    Call setHabilCtrl(txtFechaRequerida, "DIS")
    Call setHabilCtrl(txtFechaPedido, "DIS")
    Call setHabilCtrl(txtFechaComite, "DIS")
    Call setHabilCtrl(txtFinireal, "DIS")
    Call setHabilCtrl(txtFiniplan, "DIS")
    Call setHabilCtrl(txtFfinreal, "DIS")
    Call setHabilCtrl(txtFfinplan, "DIS")
    Call setHabilCtrl(txtHspresup, "DIS")
    Call setHabilCtrl(txtHsPlanif, "DIS")       ' add -024- a.
    Call setHabilCtrl(txtusrImportancia, "DIS")
    Call setHabilCtrl(cboPrioridad, "DIS")
    Call setHabilCtrl(cboCorpLocal, "DIS")
    Call setHabilCtrl(cboOrientacion, "DIS")
    Call setHabilCtrl(cboImportancia, "DIS")
    Call setHabilCtrl(cboBpar, "DIS")
    Call setHabilCtrl(cboSector, "DIS")
    Call setHabilCtrl(cboSolicitante, "DIS")
    Call setHabilCtrl(cboReferente, "DIS")
    Call setHabilCtrl(cboSupervisor, "DIS")
    Call setHabilCtrl(cboDirector, "DIS")
    Call setHabilCtrl(cboRO, "DIS")             ' add -097- a.
    Call setHabilCtrl(memBeneficios, "DIS")
    Call setHabilCtrl(memCaracteristicas, "DIS")
    Call setHabilCtrl(memDescripcion, "DIS")
    Call setHabilCtrl(memMotivos, "DIS")
    Call setHabilCtrl(memObservaciones, "DIS")
    Call setHabilCtrl(memRelaciones, "DIS")
    Call setHabilCtrl(txtEstado, "DIS")
    Call setHabilCtrl(txtSituacion, "DIS")
    Call setHabilCtrl(txtFechaEstado, "DIS")
    Call setHabilCtrl(txtNroAnexada, "DIS")
    Call setHabilCtrl(txtUsualta, "DIS")
    Call setHabilCtrl(txtNroAsignado, "DIS")
    Call setHabilCtrl(txtPrjNom, "DIS")     ' add -037- a.
    Call setHabilCtrl(cmdVerAnexora, IIf(Val(getStrCodigo(txtNroAnexada.Text, True)) = 0, "DIS", "NOR"))
    
    Select Case glModoPeticion
        Case "ALTA"
            Call InicializarControles("ALTA")
            glNroAsignado = ""
            lblModo.Caption = "ALTA"
            cmdOk.Enabled = True
            cmdCancel.Caption = "Cancelar"
            
            ' Habilita atributos para alta/edici�n seg�n parametr�a
            Call setHabilCtrl(txtTitulo, IIf(flgUPDPET_titulo, "NOR", "DIS"))
            Call setHabilCtrl(cboTipopet, IIf(flgUPDPET_tipo, "NOR", "DIS"))
            Call setHabilCtrl(cboClase, IIf(flgUPDPET_clase, "NOR", "DIS"))
            Call setHabilCtrl(cboImpacto, IIf(flgUPDPET_impacto, "NOR", "DIS"))
            Call setHabilCtrl(cboRegulatorio, IIf(flgUPDPET_regulatorio, "NOR", "DIS"))
            Call setHabilCtrl(cboPrioridad, IIf(flgUPDPET_prioridad, "NOR", "DIS"))
            Call setHabilCtrl(cboEmp, IIf(flgUPDPET_empresa, "NOR", "DIS"))
            Call setHabilCtrl(cboImportancia, IIf(flgUPDPET_importancia, "NOR", "DIS"))
            Call setHabilCtrl(txtPrjId, IIf(flgUPDPET_proyecto, "NOR", "DIS"))
            Call setHabilCtrl(txtPrjSubId, IIf(flgUPDPET_proyecto, "NOR", "DIS"))
            Call setHabilCtrl(txtPrjSubsId, IIf(flgUPDPET_proyecto, "NOR", "DIS"))
            Call setHabilCtrl(cboCorpLocal, IIf(flgUPDPET_corploc, "NOR", "DIS"))
            Call setHabilCtrl(cboOrientacion, IIf(flgUPDPET_orientacion, "NOR", "DIS"))
            Call setHabilCtrl(cboRO, IIf(flgUPDPET_ro, "NOR", "DIS"))
            Call setHabilCtrl(cboSector, IIf(flgUPDPET_sector, "NOR", "DIS"))
            Call setHabilCtrl(cboSolicitante, IIf(flgUPDPET_soli, "NOR", "DIS"))
            Call setHabilCtrl(cboReferente, IIf(flgUPDPET_refe, "NOR", "DIS"))
            Call setHabilCtrl(cboSupervisor, IIf(flgUPDPET_supe, "NOR", "DIS"))
            Call setHabilCtrl(cboDirector, IIf(flgUPDPET_auto, "NOR", "DIS"))
            Call setHabilCtrl(cboBpar, IIf(flgUPDPET_bpar, "NOR", "DIS"))
            Call setHabilCtrl(txtFechaRequerida, IIf(flgUPDPET_fchreq, "NOR", "DIS"))
            Call setHabilCtrl(txtFechaComite, IIf(flgUPDPET_fchbp, "NOR", "DIS"))
            Call setHabilCtrl(memBeneficios, IIf(flgUPDPET_membenef, "NOR", "DIS"))
            Call setHabilCtrl(memCaracteristicas, IIf(flgUPDPET_memcarac, "NOR", "DIS"))
            Call setHabilCtrl(memDescripcion, IIf(flgUPDPET_memdescr, "NOR", "DIS"))
            Call setHabilCtrl(memMotivos, IIf(flgUPDPET_memmotiv, "NOR", "DIS"))
            Call setHabilCtrl(memObservaciones, IIf(flgUPDPET_memobser, "NOR", "DIS"))
            Call setHabilCtrl(memRelaciones, IIf(flgUPDPET_memrelac, "NOR", "DIS"))
            sEstado = flgUPDPET_DFLT_estado
            sSituacion = flgUPDPET_DFLT_situac
            cboTipopet.ListIndex = PosicionCombo(Me.cboTipopet, "NOR", True)
            ' flgUPDPET_DFLT_clase
            
        Case "MODI"
            Call InicializarControles("MODI")
            Call orjBtn_Click(orjDAT)
            lblModo.Caption = "MODIFICA"
            cmdOk.Enabled = True
            cmdCancel.Caption = "Cancelar"
            
            ' Habilita atributos para alta/edici�n seg�n parametr�a
            Call setHabilCtrl(txtTitulo, IIf(flgUPDPET_titulo, "NOR", "DIS"))
            Call setHabilCtrl(cboTipopet, IIf(flgUPDPET_tipo, "NOR", "DIS"))
            Call setHabilCtrl(cboClase, IIf(flgUPDPET_clase, "NOR", "DIS"))
            Call setHabilCtrl(cboImpacto, IIf(flgUPDPET_impacto, "NOR", "DIS"))
            Call setHabilCtrl(cboRegulatorio, IIf(flgUPDPET_regulatorio, "NOR", "DIS"))
            Call setHabilCtrl(cboPrioridad, IIf(flgUPDPET_prioridad, "NOR", "DIS"))
            Call setHabilCtrl(cboEmp, IIf(flgUPDPET_empresa, "NOR", "DIS"))
            Call setHabilCtrl(cboImportancia, IIf(flgUPDPET_importancia, "NOR", "DIS"))
            Call setHabilCtrl(txtPrjId, IIf(flgUPDPET_proyecto, "NOR", "DIS"))
            Call setHabilCtrl(txtPrjSubId, IIf(flgUPDPET_proyecto, "NOR", "DIS"))
            Call setHabilCtrl(txtPrjSubsId, IIf(flgUPDPET_proyecto, "NOR", "DIS"))
            Call setHabilCtrl(cboCorpLocal, IIf(flgUPDPET_corploc, "NOR", "DIS"))
            Call setHabilCtrl(cboOrientacion, IIf(flgUPDPET_orientacion, "NOR", "DIS"))
            Call setHabilCtrl(cboRO, IIf(flgUPDPET_ro, "NOR", "DIS"))
            Call setHabilCtrl(cboSector, IIf(flgUPDPET_sector, "NOR", "DIS"))
            Call setHabilCtrl(cboSolicitante, IIf(flgUPDPET_soli, "NOR", "DIS"))
            Call setHabilCtrl(cboReferente, IIf(flgUPDPET_refe, "NOR", "DIS"))
            Call setHabilCtrl(cboSupervisor, IIf(flgUPDPET_supe, "NOR", "DIS"))
            Call setHabilCtrl(cboDirector, IIf(flgUPDPET_auto, "NOR", "DIS"))
            Call setHabilCtrl(cboBpar, IIf(flgUPDPET_bpar, "NOR", "DIS"))
            Call setHabilCtrl(txtFechaRequerida, IIf(flgUPDPET_fchreq, "NOR", "DIS"))
            Call setHabilCtrl(txtFechaComite, IIf(flgUPDPET_fchbp, "NOR", "DIS"))
            Call setHabilCtrl(memBeneficios, IIf(flgUPDPET_membenef, "NOR", "DIS"))
            Call setHabilCtrl(memCaracteristicas, IIf(flgUPDPET_memcarac, "NOR", "DIS"))
            Call setHabilCtrl(memDescripcion, IIf(flgUPDPET_memdescr, "NOR", "DIS"))
            Call setHabilCtrl(memMotivos, IIf(flgUPDPET_memmotiv, "NOR", "DIS"))
            Call setHabilCtrl(memObservaciones, IIf(flgUPDPET_memobser, "NOR", "DIS"))
            Call setHabilCtrl(memRelaciones, IIf(flgUPDPET_memrelac, "NOR", "DIS"))
            
        If 1 <> 1 Then
            Select Case PerfilInternoActual
                Case "SOLI"
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(cboEmp, "NOR")        ' add -095- a.
                    Call setHabilCtrl(memBeneficios, "NOR")
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    
                    If flgExtension = False Then
                        Call setHabilCtrl(txtFechaRequerida, "NOR")
                        If sEstado = "CONFEC" Then
                            txtTitulo.SetFocus
                        Else
                            txtFechaRequerida.SetFocus
                        End If
                    Else
                        Call orjBtn_Click(orjMEM)
                        DoEvents
                        memDescripcion.SetFocus
                    End If
                Case "REFE"
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(txtFechaRequerida, "NOR")
                    Call setHabilCtrl(memBeneficios, "NOR")
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                Case "CSEC"
                    If flgIngerencia Then
                        '{ add -040- a.
                        If sEstado = "EJECUC" Then
                            Call setHabilCtrl(cboRegulatorio, "NOR")
                        Else
                        '}
                            Call setHabilCtrl(cboBpar, "NOR")           ' add -006- b.
                            Call setHabilCtrl(cboClase, "DIS")          ' add -002- c.

                            '{ add -005- b.
                            PetClassActual = CodigoCombo(cboClase, True)
                            
                            Call setHabilCtrl(cboTipopet, "NOR")
                            Call setHabilCtrl(cboClase, "NOR")
                            Call setHabilCtrl(cmdAyudaClase, "NOR")     ' add -037- a.
                            
                            If CodigoCombo(cboTipopet, True) = "PRJ" Or _
                                InStr(1, "EVOL|NUEV", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                                Call setHabilCtrl(cboImpacto, "NOR")
                            End If
                            '}
                            Call setHabilCtrl(cboRegulatorio, "NOR")    ' add -036- a.
                            If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then Call setHabilCtrl(cboRO, "NOR")           ' add -097- a.
                            DoEvents
                        End If  ' add -040- a.
                    End If
                Case "CGRU"     ' Responsable de grupo
                    If flgIngerencia Then
                        If glPETTipo = "PRO" And glPETClase = "SINC" Then
                            
                            Call setHabilCtrl(txtTitulo, "NOR")     ' add -096- a.
                            Call setHabilCtrl(cboEmp, "NOR")        ' add -095- a.
                            '{ add -097- a.
                            If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                                If Not flgEstuvoEJECUC And InStr(1, "EJECUC|SUSPEN|", sEstado, vbTextCompare) > 0 Then
                                    Call setHabilCtrl(cboRO, "NOR")
                                End If
                            End If
                            '}
                            Call setHabilCtrl(cboClase, "NOR")
                            Call setHabilCtrl(cboRegulatorio, "NOR")
                            Call setHabilCtrl(memBeneficios, "NOR")
                            Call setHabilCtrl(memCaracteristicas, "NOR")
                            Call setHabilCtrl(memDescripcion, "NOR")
                            Call setHabilCtrl(memMotivos, "NOR")
                            Call setHabilCtrl(memObservaciones, "NOR")
                            Call setHabilCtrl(memRelaciones, "NOR")
                        Else
                            If sEstado = "EJECUC" Then
                                Call setHabilCtrl(cboRegulatorio, "NOR")
                            Else
                                Call setHabilCtrl(cboBpar, "NOR")
                                ' Si es Proyecto, o Propia o Especial entonces
                                If (CodigoCombo(cboTipopet, True) = "PRJ" Or CodigoCombo(cboTipopet, True) = "PRO") Or CodigoCombo(cboTipopet, True) = "ESP" Then
                                    Call setHabilCtrl(txtTitulo, "NOR")
                                    Call setHabilCtrl(cboEmp, "NOR")            ' add -095- a.
                                    'Call setHabilCtrl(cboPrioridad, "NOR")     ' del -078- a.
                                    Call setHabilCtrl(txtFechaRequerida, "NOR")
                                    Call setHabilCtrl(memBeneficios, "NOR")
                                    Call setHabilCtrl(memCaracteristicas, "NOR")
                                    Call setHabilCtrl(memDescripcion, "NOR")
                                    Call setHabilCtrl(memMotivos, "NOR")
                                    Call setHabilCtrl(memObservaciones, "NOR")
                                    Call setHabilCtrl(memRelaciones, "NOR")
                                    Call setHabilCtrl(cboClase, "DIS")       ' add -002- c.
                                End If

                                Call setHabilCtrl(cboTipopet, "NOR")
                                Call setHabilCtrl(cboClase, "NOR")
                                Call setHabilCtrl(cmdAyudaClase, "NOR")     ' add -037- a.
                                ' Solo lleva impacto tecnol�gico un proyecto o clases EVOL y NUEV
                                If CodigoCombo(cboTipopet, True) = "PRJ" Or _
                                    (InStr(1, "EVOL|NUEV", CodigoCombo(cboClase, True), vbTextCompare) > 0) Then
                                        Call setHabilCtrl(cboImpacto, "NOR")
                                End If
                                '}
                                Call setHabilCtrl(cboRegulatorio, "NOR")    ' add -036- a.
                            End If  ' add -040- a.
                        End If      ' add -051- a.
                    End If
                '{ add -096- a.
                Case "ANAL"
                    If flgIngerencia Then
                        If glPETTipo = "PRO" And glPETClase = "SINC" Then
                            Call setHabilCtrl(txtTitulo, "NOR")
                            Call setHabilCtrl(cboEmp, "NOR")
                            Call setHabilCtrl(memBeneficios, "NOR")
                            Call setHabilCtrl(memCaracteristicas, "NOR")
                            Call setHabilCtrl(memDescripcion, "NOR")
                            Call setHabilCtrl(memMotivos, "NOR")
                            Call setHabilCtrl(memObservaciones, "NOR")
                            Call setHabilCtrl(memRelaciones, "NOR")
                            Call setHabilCtrl(txtFechaRequerida, "NOR")
                        End If
                    End If
                '}
                Case "CDIR", "CGCI"
                    If CodigoCombo(cboTipopet, True) = "PRJ" Then
                        If modifImportancia(xUsrLoginActual, xUsrPerfilActual, glLOGIN_ID_REEMPLAZO, PerfilInternoActual) Then
                            Call setHabilCtrl(cboImportancia, "NOR")
                            cboImportancia.Locked = False
                        Else
                            Call setHabilCtrl(cboImportancia, "NOR")
                            cboImportancia.Locked = True
                            cboImportancia.ToolTipText = "No puede modificarse, debido a que el perfil que la defini� previamente es superior o igual al actual."
                        End If
                    End If
                Case "BPAR"
                    Call setHabilCtrl(cboEmp, "NOR")                    ' add -095- a.
                    '{ add -097- a.
                    If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                        'If Not flgEstuvoEJECUC And InStr(1, "EJECUC|SUSPEN|", sEstado, vbTextCompare) > 0 Then
                        If Not flgEstuvoEJECUC Then
                            Call setHabilCtrl(cboRO, "NOR")
                        End If
                    Else    ' No est� clasificada a�n
                        If CodigoCombo(cboClase, True) = "SINC" Then
                            Call setHabilCtrl(cboRO, "NOR")
                        End If
                    End If
                    '}
                    PetTypeActual = CodigoCombo(cboTipopet, True)       ' add -089- a.
                    '{ add -040- a. Seg�n el estado...
                    'If sEstado = "EJECUC" Then
                    If InStr(1, "EJECUC|SUSPEN|", sEstado, vbTextCompare) > 0 Then
                        Call setHabilCtrl(cboRegulatorio, "NOR")
                        Call HabilitarProyectos
                        If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) > 0 Then
                            ' Tiene proyecto IDM asociado
                            DoEvents
                            Call HabilitarVisibilidad("Proyectos")
                        Else
                            DoEvents
                            Call HabilitarVisibilidad("Estandar")
                        End If
                        Call setHabilCtrl(cboOrientacion, "NOR")
                        Call setHabilCtrl(cboCorpLocal, "NOR")
                    ElseIf EsTerminal(sEstado) Then
                        Call HabilitarProyectos
                        If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) > 0 Then
                            ' Tiene proyecto IDM asociado
                            DoEvents
                            Call HabilitarVisibilidad("Proyectos")
                        Else
                            DoEvents
                            Call HabilitarVisibilidad("Estandar")
                        End If
                    Else
                    '}
                        Call setHabilCtrl(txtFechaComite, "DIS")
                        '{ add -037- a. Visibilidad
                        If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) > 0 Then
                            ' Tiene proyecto IDM asociado
                            DoEvents
                            Call HabilitarVisibilidad("Proyectos")
                        Else
                            DoEvents
                            Call HabilitarVisibilidad("Estandar")
                        End If
                        '}
                        '{ add -002- a.
                        Call setHabilCtrl(cboBpar, "NOR")
                        Call setHabilCtrl(cboOrientacion, "NOR")    ' Se agrega Orientaci�n
                        '}
                        Call setHabilCtrl(memDescripcion, "NOR")
                        '{ add -040- d. Bug
                        If CodigoCombo(cboTipopet, True) = "PRO" Then
                            Call setHabilCtrl(memCaracteristicas, "NOR")
                            Call setHabilCtrl(memMotivos, "NOR")
                            Call setHabilCtrl(memBeneficios, "NOR")
                        End If
                        '}
'                        If InStr(1, "AUX|AUI|", CodigoCombo(cboTipopet, True), vbTextCompare) = 0 Then
'                            With cboTipopet
'                                .Clear
'                                .AddItem "Normal" & ESPACIOS & "||NOR"
'                                .AddItem "Especial" & ESPACIOS & "||ESP"
'                                .AddItem "Proyecto" & ESPACIOS & "||PRJ"
'                                '.ListIndex = PosicionCombo(cboTipopet, ClearNull(sTipoPet), True)  ' del -089- a.
'                                .ListIndex = PosicionCombo(cboTipopet, PetTypeActual, True)         ' add -089- a.
'                            End With
'                            If cboTipopet.List(cboTipopet.ListIndex) = "" Then cboTipopet.ListIndex = 0 ' add -036- a.
'                        End If
                        '}
                        ' Inicializa las opciones de clase
'                        PetClassActual = CodigoCombo(cboClase, True)
'                        Select Case CodigoCombo(cboTipopet, True)
'                            Case "NOR"
'                                With cboClase
'                                    .Clear
'                                    .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                                    .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                                    .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                                End With
'                                CargaCombosConValor     ' add -006- g.
'                            '{ add -005- b.
'                            Case "PRO"
'                                '{ add -069- a.
'                                If xGer = "DESA" Then
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                        .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                                        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                                        .AddItem "SPUFI" & ESPACIOS & "||SPUF"
'                                    End With
'                                '}
'                                Else
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                                        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                                    End With
'                                End If
'                            '}
'                            Case "ESP"
'                                With cboClase
'                                    .Clear
'                                    .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                                    .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                                    .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                                End With
'                                cmdCancel.Enabled = True
'                                cmdOk.Enabled = True
'                                DoEvents
'                                'Call CargaCombosConValor     ' add -006- g.
'                                Call CargaCombosConTodo
'                                Call setHabilCtrl(cboTipopet, "NOR")
'                                Call setHabilCtrl(cboSector, "NOR")
'                                Call setHabilCtrl(cboBpar, "NOR")
'                                Call setHabilCtrl(cboSolicitante, "NOR")
'                                Call setHabilCtrl(memBeneficios, "NOR")
'                                Call setHabilCtrl(memCaracteristicas, "NOR")
'                                Call setHabilCtrl(memDescripcion, "NOR")
'                                Call setHabilCtrl(memMotivos, "NOR")
'                                Call setHabilCtrl(memObservaciones, "NOR")
'                                Call setHabilCtrl(memRelaciones, "NOR")
'                            Case "PRJ"
'                                With cboClase
'                                    .Clear
'                                    .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                                    .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                                End With
'                                CargaCombosConValor     ' add -006- g.
'                        End Select
'                        If Len(PetClassActual) > 0 Then
'                            cboClase.ListIndex = PosicionCombo(cboClase, ClearNull(PetClassActual), True)
'                            '{ add -005- b.
'                            If cboClase.ListIndex = -1 Then
'                                cboClase.ListIndex = 0
'                            End If
'                            '}
'                        Else
'                            ' Selecciona por defecto el primer item de la lista
'                            cboClase.ListIndex = 0
'                        End If
'                        DoEvents            ' add -005- c.
                        If InStr(1, "AUX|AUI|", sTipoPet, vbTextCompare) = 0 Then
                            Call setHabilCtrl(cboTipopet, "NOR")
                            Call setHabilCtrl(cboClase, "NOR")
                            Call setHabilCtrl(cmdAyudaClase, "NOR")
                        End If
                        '}
                        '{ add -003- h. Si es una petici�n de tipo proyecto o es una petici�n de clase Mantenimiento Evolutivo
                        '               o Nuevo Desarrollo, entonces puede indicar si lleva o no Impacto tecnol�gico.
                        If CodigoCombo(cboTipopet, True) = "PRJ" Or _
                            (InStr(1, "EVOL|NUEV|", CodigoCombo(cboClase, True), vbTextCompare) > 0) Then
                                Call setHabilCtrl(cboImpacto, "NOR")
                        End If
                        '}
                        Call setHabilCtrl(cboRegulatorio, "NOR")    ' add -036- a.
                        HabilitarProyectos      ' add -037- a.
                        '{ mov -001- }
                    End If      ' add -040- a.
'                    '{ add -051- d.
'                    If cboClase.ListCount = 1 Then
'                        Call setHabilCtrl(cboClase, "DIS")
'                    End If
                    '}
                    '{ del -098- a.
                    ''{ add -069- a. Si la petici�n es de origen DYD, el BP no puede modificar
                    ''               el tipo ni la clase de la misma.
                    'If glPETTipo = "PRO" And xGer = "DESA" Then
                    '    Call setHabilCtrl(cboTipopet, "DIS")
                    '    Call setHabilCtrl(cboClase, "DIS")
                    'ElseIf glPETTipo = "PRO" And InStr(1, "TRYAPO|ORG.|", xGer, vbTextCompare) > 0 Then
                    '    Call setHabilCtrl(cboTipopet, "DIS")
                    '    Call setHabilCtrl(cboClase, "DIS")
                    'End If
                    ''}
                    '}
                Case "SADM"
                    'Call setHabilCtrl(txtNroAsignado, "NOR")
                    'txtNrointerno.Visible = False
                    Call CargaCombosConTodo
                    If xSec <> "" Then Call SetCombo(cboSector, xSec, True)
                    If sSoli <> "" Then Call SetCombo(cboSolicitante, sSoli)
                    If sRefe <> "" Then Call SetCombo(cboReferente, sRefe)
                    If sSupe <> "" Then Call SetCombo(cboSupervisor, sSupe)
                    If sDire <> "" Then Call SetCombo(cboDirector, sDire)
                    Call setHabilCtrl(txtTitulo, "NOR")
                    Call setHabilCtrl(cboEmp, "NOR")            ' add -095- a.
                    Call setHabilCtrl(cboRO, "NOR")             ' add -097- a.
                    Call setHabilCtrl(txtFechaRequerida, "NOR")
                    Call setHabilCtrl(txtFechaComite, "NOR")
                    Call setHabilCtrl(txtFinireal, "NOR")
                    Call setHabilCtrl(txtFiniplan, "NOR")
                    Call setHabilCtrl(txtFfinreal, "NOR")
                    Call setHabilCtrl(txtFfinplan, "NOR")
                    Call setHabilCtrl(txtHspresup, "NOR")
                    Call setHabilCtrl(txtHsPlanif, "NOR")
                    Call setHabilCtrl(cboTipopet, "NOR")
                    Call setHabilCtrl(cboPrioridad, "NOR")
                    Call setHabilCtrl(cboSector, "NOR")
                    Call setHabilCtrl(cboBpar, "NOR")
                    Call setHabilCtrl(cboSolicitante, "NOR")
                    Call setHabilCtrl(cboReferente, "NOR")
                    Call setHabilCtrl(cboSupervisor, "NOR")
                    Call setHabilCtrl(cboDirector, "NOR")
                    Call setHabilCtrl(memBeneficios, "NOR")
                    Call setHabilCtrl(memCaracteristicas, "NOR")
                    Call setHabilCtrl(memDescripcion, "NOR")
                    Call setHabilCtrl(memMotivos, "NOR")
                    Call setHabilCtrl(memObservaciones, "NOR")
                    Call setHabilCtrl(memRelaciones, "NOR")
                    Call setHabilCtrl(cboCorpLocal, "NOR")
                    Call setHabilCtrl(cboOrientacion, "NOR")
                    Call setHabilCtrl(cboImportancia, "NOR")
'                    '{ add -005- a. - Inicializa las opciones de tipo
'                    'PetTypeActual = CodigoCombo(cboTipopet, True)
'                    PetClassActual = CodigoCombo(cboClase, True)
'                    Select Case CodigoCombo(cboTipopet, True)
'                        Case "NOR"
'                            With cboClase
'                                .Clear
'                                .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                                .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                                .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                            End With
'                            Call setHabilCtrl(cboClase, "NOR")
'                        Case "PRO"
'                            With cboClase
'                                .Clear
'                                .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                                .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                                .AddItem "SPUFI" & ESPACIOS & "||SPUF"
'                            End With
'                            Call setHabilCtrl(cboClase, "NOR")
'                        Case "ESP"
'                            With cboClase
'                                .Clear
'                                .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                                .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                                .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                            End With
'                            cmdCancel.Enabled = True
'                            cmdOk.Enabled = True
'                            DoEvents
'                            CargaIntervinientes                                 ' add -006- g.
'                            Call setHabilCtrl(cboClase, "NOR")
'                            If sBpar <> "" Then Call SetCombo(cboBpar, sBpar)   ' add -006- c.
'                        Case "PRJ"
'                            With cboClase
'                                .Clear
'                                .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                                .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
'                            End With
'                            Call setHabilCtrl(cboClase, "NOR")
'                        Case "AUI", "AUX"
'                            With cboClase
'                                .Clear
'                                .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
'                            End With
'                            Call setHabilCtrl(cboClase, "DIS")
'                    End Select
'                    If Len(PetClassActual) > 0 Then
'                        cboClase.ListIndex = PosicionCombo(cboClase, ClearNull(PetClassActual), True)
'                        If cboClase.ListIndex = -1 Then
'                            cboClase.ListIndex = 0
'                        End If
'                    Else
'                        cboClase.ListIndex = 0       ' Selecciona por defecto el primer item de la lista
'                    End If
'                    DoEvents
                    Call setHabilCtrl(cboTipopet, "NOR")
                    Call setHabilCtrl(cmdAyudaClase, "NOR")         ' add -037- a.
                    Call setHabilCtrl(cboImpacto, "NOR")
                    Call setHabilCtrl(cboRegulatorio, "NOR")        ' add -036- a.
                    HabilitarProyectos                              ' add -037- a.
            End Select
        End If  ' If 1 <> 1
        Case "VIEW"
            lblModo.Caption = "VISUALIZAR"
            cmdOk.Enabled = False
            cmdCancel.Caption = "Cerrar"
            cmdPrint.Enabled = True
            cmdHistView.Enabled = True
            cmdRptHs.Enabled = True
            cmdAgrupar.Enabled = True
            Call InicializarControles(glModoPeticion)

            ' BOTONES
            Select Case PerfilInternoActual
                Case "SADM"
                    cmdCambioEstado.Enabled = True
                    cmdModificar.Enabled = True
                '{ add -096- a.
                Case "ANAL"
                    If flgIngerencia Then
                        If glPETTipo = "PRO" And glPETClase = "SINC" Then
                            If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado, Null, Null) Then
                                Call setHabilCtrl(cmdModificar, "NOR")
                            End If
                        End If
                    End If
                '}
                Case "CGRU"
                    '{ add -078- a.
                    If sp_GetPeriodo(Null) Then
                        Do While Not aplRST.EOF
                            If ClearNull(aplRST.Fields!vigente) = "S" Then
                                lPeriodoActual = aplRST.Fields!per_nrointerno
                                Exit Do
                            End If
                            aplRST.MoveNext
                            DoEvents
                        Loop
                        If sp_GetPeticionPlandet(lPeriodoActual, glNumeroPeticion, glLOGIN_Grupo) Then
                            Call setHabilCtrl(cmdPlanificar, "NOR")
                        Else
                            Call setHabilCtrl(cmdPlanificar, "DIS")
                        End If
                    End If
                    '}
                    'esto est� sin tabla, horrible!!
                    ' FJS - Aclaraci�n:
                    ' flgIngerencia             ====> (Cuando no es perfil BP) Indica que el recurso pertenece a alguno de los grupos ejecutores de la petici�n
                    ' flgEjecutorParaModificar  ====> El usuario pertenece a alguno de los sectores de la petici�n y se corresponde al sector solicitante de la petici�n
                    '{ add -040- a.1.
                    ' Si el estado de la petici�n es alguno de los terminales...
                    If InStr(1, "EJECUC|RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD", ClearNull(sEstado), vbTextCompare) > 0 Then
                        ' Y si el perfil del recurso actual es alguno de los permitidos para modificar
                        If InStr(1, "BPAR|CGRU|CSEC|", PerfilInternoActual, vbTextCompare) > 0 Then
                            If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado) Then
                                If Not aplRST.EOF Then
                                    Call setHabilCtrl(cmdModificar, "NOR")
                                End If
                            End If
                        End If
                    Else
                    '}
                        If flgIngerencia And flgEjecutorParaModificar Then    ' upd -006- x.
                            'If (CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ") Then       ' del -006- a.
                            If (CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "ESP") Then    ' add -006- a.
                                '{ add -040- a.
                                If glLOGIN_Gerencia = "DESA" Then
                                    If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado) Then
                                        If Not aplRST.EOF Then
                                            cmdModificar.Enabled = True
                                        End If
                                    '{ add -051- a.
                                    Else
                                        If PerfilInternoActual = "CGRU" And glPETTipo = "PRO" And glPETClase = "SINC" Then
                                            cmdModificar.Enabled = True
                                        End If
                                    '}
                                    End If
                                Else
                                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(sEstado), vbTextCompare) = 0 Then
                                        cmdModificar.Enabled = True
                                    End If
                                End If
                                '}
                            End If
                        End If
                    End If          ' add -040- a.1.
                    '{ add -070- a.
                    If EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                        If cmdModificar.Enabled Then
                            cmdModificar.Enabled = False
                        End If
                    End If
                    '}
                Case "CSEC"
                    '{ add -040- a.1.
                    ' Si el estado de la petici�n es alguno de los excepcionales...
                    If InStr(1, "EJECUC|RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD", ClearNull(sEstado), vbTextCompare) > 0 Then
                        ' Y si el perfil del recurso actual es alguno de los permitidos para modificar
                        If InStr(1, "BPAR|CGRU|CSEC", PerfilInternoActual, vbTextCompare) > 0 Then
                            If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado) Then
                                If Not aplRST.EOF Then
                                    cmdModificar.Enabled = True
                                End If
                            End If
                        End If
                    Else
                    '}
                        'esto est� sin tabla, horrible!!
                        If flgIngerencia And flgEjecutorParaModificar Then    ' upd -006- x.
                            'If (CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "PRJ") Then       ' del -006- a.
                            If (CodigoCombo(cboTipopet, True) = "PRO" Or CodigoCombo(cboTipopet, True) = "ESP") Then    ' add -006- a.
                                '{ add -040- a.
                                If glLOGIN_Gerencia = "DESA" Then
                                    If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado) Then
                                        If Not aplRST.EOF Then
                                            cmdModificar.Enabled = True
                                        End If
                                    End If
                                Else
                                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(sEstado), vbTextCompare) = 0 Then
                                        cmdModificar.Enabled = True
                                    End If
                                End If
                                '}
                            End If
                         End If
                    End If      ' add -040- a.1.
                Case "CDIR", "CGCI"
                    If CodigoCombo(cboTipopet, True) = "PRJ" Then
                        '{ add -017- a.
                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(sEstado), vbTextCompare) = 0 Then
                            cmdModificar.Enabled = True
                        End If
                        '}
                    End If
                Case Else
                    If flgIngerencia Then
                        ' *****************************************************************
                        ' HABILITACION DEL BOTON DE MODIFICAR
                        ' *****************************************************************
                        ' Si el PERFIL puede modificar la petici�n (EVENTO: PMOD000) para el
                        ' ESTADO actual de la misma.
                        If sp_GetAccionPerfil("PMOD000", PerfilInternoActual, sEstado) Then
                            If Not aplRST.EOF Then
                                cmdModificar.Enabled = True
                            End If
                        End If
                        If PerfilInternoActual <> "BPAR" Then   ' add -040- a.
                            '{ add -017- a.
                            If cmdModificar.Enabled Then
                                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD|", sEstado, vbTextCompare) > 0 Then
                                    cmdModificar.Enabled = False
                                End If
                            End If
                            '}
                        End If  ' add -040- a.
                        ' Control de cambio de estado
                        If sp_GetAccionPerfil("PCHGEST", PerfilInternoActual, sEstado) Then
                            If Not aplRST.EOF Then
                                '{ add -003- a.
                                If PerfilInternoActual = "BPAR" Then
                                    ' Solo si se encuentra la petici�n CLASIFICADA permito el cambio de estado
                                    If CodigoCombo(cboClase, True) <> "SINC" And Not CodigoCombo(cboClase, True) = "" Then
                                        cmdCambioEstado.Enabled = True
                                    End If
                                Else
                                    cmdCambioEstado.Enabled = True
                                End If
                                '}
                            End If
                        End If
                        '{ add -017- a.
                        If cmdCambioEstado.Enabled Then
                            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(sEstado), vbTextCompare) > 0 Then
                                cmdCambioEstado.Enabled = False
                            End If
                        End If
                        '}
                        ' Puede modificar la petici�n como solicitante si est� en estos estados y el tipo ESP o PRJ o NOR
                        If PerfilInternoActual = "SOLI" And _
                            InStr("PLANIF|PLANOK|ESTIMA|ESTIOK|EJECUC", sEstado) > 0 And _
                            (CodigoCombo(cboTipopet, True) = "NOR" Or CodigoCombo(cboTipopet, True) = "ESP" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                                flgExtension = True
                                cmdModificar.Enabled = True
                        End If
                        ' Si es Solicitante, puede agregar nuevos grupos si...
                        If PerfilInternoActual = "SOLI" And _
                            InStr("CONFEC|DEVSOL", sEstado) > 0 And _
                            (CodigoCombo(cboTipopet, True) = "NOR" Or CodigoCombo(cboTipopet, True) = "ESP" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                                cmdGrupos.Enabled = True
                        End If
                        ' Si es Referente, puede agregar nuevos grupos si...
                        If PerfilInternoActual = "REFE" And _
                            InStr("REFERE|DEVREF", sEstado) > 0 And _
                            (CodigoCombo(cboTipopet, True) = "NOR" Or CodigoCombo(cboTipopet, True) = "ESP" Or CodigoCombo(cboTipopet, True) = "PRJ") Then
                                cmdGrupos.Enabled = True
                        End If
                    End If
            End Select
            ' Siempre se pueden agregar nuevos sectores (en principio, todos los perfiles)
            cmdSector.Enabled = True
    End Select

    If cboSector.ListCount = 1 Then Call setHabilCtrl(cboSector, "DIS")     ' add -017- a.
    '{ add -051- a.
    If cboSolicitante.ListCount = 1 Then
        If cboSector.ListCount = 1 Then
            Call setHabilCtrl(cboSolicitante, "DIS")
        End If
    End If
    '}
    If cboTipopet.ListCount = 1 Then Call setHabilCtrl(cboTipopet, "DIS")   ' add -051- e.
'    '{ add -037- a. Para pintar el campo de estado de acuerdo al mismo (Activos o Terminales)
'    If Not glModoPeticion = "ALTA" Then
'        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(sEstado), vbTextCompare) > 0 Then
'            txtEstado.BackColor = RGB(252, 209, 216)
'            txtEstado.Enabled = False
'        Else
'            If Len(sEstado) > 0 Then
'                txtEstado.BackColor = RGB(208, 234, 218)
'                txtEstado.Enabled = False
'            End If
'        End If
'    End If
'    '}
    Call LockProceso(False)
End Sub

Private Sub cboClase_Click()
    ' Si es una petici�n de tipo proyecto o es una petici�n de clase Mantenimiento Evolutivo
    ' o Nuevo Desarrollo, entonces puede indicar si lleva o no Impacto tecnol�gico.
    If Not flgEnCarga Then
    
    If glUsrPerfilActual <> "SADM" Then
        If CodigoCombo(cboTipopet, True) = "PRJ" Or _
            (InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0) Then
                Call setHabilCtrl(cboImpacto, "NOR")
                Call setHabilCtrl(cboRO, "NOR")                                 ' add -097- a.
                If CodigoCombo(cboClase, True) <> glPETClase Then            ' Si la clase seleccionada es distinta a la clase actual de la petici�n, inicializo el control de impacto tecnol�gico 31.10.2008 - Esta inicializaci�n es siempre al valor NO (antes era a indeterminado: "--" )
                    cboImpacto.ListIndex = PosicionCombo(cboImpacto, glPETImpacto, True)
                Else
                    If glModoPeticion = "ALTA" Then                             ' Por defecto, SIEMPRE el valor es indeterminado para cuando es un alta, pero si estoy modificando y ya tenia un valor, lo respeto
                        cboImpacto.ListIndex = PosicionCombo(cboImpacto, "-", True)
                    Else
                        cboImpacto.ListIndex = PosicionCombo(cboImpacto, glPETImpacto, True)
                    End If
                End If
                If CodigoCombo(cboRO, True) <> cboRO.Tag Then cboRO.ListIndex = PosicionCombo(cboRO, cboRO.Tag, True)   ' add -097- a.
        Else
            Call setHabilCtrl(cboImpacto, "DIS")                              ' Por defecto, mantengo el valor que tenia si no puedo modificar
            Call setHabilCtrl(cboRO, "DIS")                                     ' add -097- a.
            If InStr(1, "ALTA|MODI|", glModoPeticion, vbTextCompare) > 0 Then   ' Si la clase no es NUEV o EVOL, no lleva nunca impacto tecnol�gico. Si estoy modificando la petici�n actual, entonces fuerzo el valor a NO
                If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) = 0 Then
                    cboRO.ListIndex = 0                                         ' add -097- a.
                    cboImpacto.ListIndex = PosicionCombo(cboImpacto, IIf(CodigoCombo(cboClase, True) = "SINC", "-", "N"), True)
                End If
            End If
        End If
    End If
    End If
End Sub

Function CamposObligatorios() As Boolean
    ' Control de t�tulo
    If ClearNull(txtTitulo.Text) = "" Then
       Call orjBtn_Click(orjDAT)
       MsgBox "Falta especificar t�tulo", vbOKOnly + vbExclamation
       CamposObligatorios = False
       Exit Function
    End If
    ' Control de tipo
    If Len(cboTipopet.Text) = 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Debe especificar el tipo de la petici�n", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    ' Control de clase
    If Not InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) > 0 Then
        If cModeloControl = MODELO_CONTROL_NUEVO Then
            If CodigoCombo(cboClase, True) = "SINC" And (Not PerfilInternoActual = "SOLI" And Not PerfilInternoActual = "REFE" And Not PerfilInternoActual = "AUTO" And Not PerfilInternoActual = "SUPE" And Not PerfilInternoActual = "SADM" And Not PerfilInternoActual = "ANAL") Then      ' upd -006- h.      ' upd -051- a.
                Call orjBtn_Click(orjDAT)
                MsgBox "Debe indicar la clase de la petici�n actual para grabar los datos.", vbExclamation + vbOKOnly, "Clase de petici�n"
                CamposObligatorios = False
                If cboClase.Enabled = True Then cboClase.SetFocus
                Exit Function
            End If
        End If
    End If
    '{ add -097- a. Control de RO
    If InStr(1, "ALTA|MODI|", glModoPeticion, vbTextCompare) > 0 Then
        If InStr(1, "EVOL|NUEV|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
            If cboRO.ListIndex = -1 Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Debe indicar si la petici�n est� vinculada a la mitigaci�n de factores de Riesgo Operacional (RO).", vbExclamation + vbOKOnly, "Riesgo Operacional"
                CamposObligatorios = False
                If cboRO.Enabled = True Then cboRO.SetFocus
                Exit Function
            End If
        End If
    End If
    '}
    ' Control de fecha requerida (solo en altas)
    If glModoPeticion = "ALTA" Then
        If Not IsNull(txtFechaRequerida.DateValue) Then
            If txtFechaRequerida.DateValue < txtFechaPedido.DateValue Then
                Call orjBtn_Click(orjDAT)
                MsgBox "La fecha requerida debe ser mayor o igual a la fecha de alta", vbOKOnly + vbExclamation
                CamposObligatorios = False
                Exit Function
            End If
        End If
    End If
    '}
    ' Control de especificaci�n de Impacto tecnol�gico
    If cModeloControl = MODELO_CONTROL_NUEVO Then
        If InStr(1, "NUEV|OPTI|EVOL|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
            If CodigoCombo(cboImpacto, True) = "-" Then       ' add -091- a.
                If InStr(1, "SADM|ADMI|", PerfilInternoActual, vbTextCompare) = 0 Then      ' Para perfiles administradores no realizo control ni advertencia, dejo grabar as�
                'If Not (PerfilInternoActual = "SADM" Or PerfilInternoActual = "ADMI") Then
                    ' Si a�n no se especific� el atributo Regulatorio, lo exijo
                    Call orjBtn_Click(orjDAT)
                    MsgBox "Falta especificar IMPACTO TECNOL�GICO", vbOKOnly + vbExclamation, "Impacto tecnol�gico"
                    cboImpacto.SetFocus
                    CamposObligatorios = False
                    Exit Function
                End If
            End If
        Else
            'cboImpacto.ListIndex = 1      ' Entonces NO lleva impacto tecnol�gico
            cboImpacto.ListIndex = PosicionCombo(cboImpacto, "N", True)
        End If
        'End If
    End If
    ' Control de especificaci�n de Regulatorio
    If InStr(1, "BPAR|CSEC|CGRU|", PerfilInternoActual, vbTextCompare) > 0 Then
        If cboRegulatorio.ListIndex < 1 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Falta especificar si es REGULATORIO", vbOKOnly + vbExclamation
            cboRegulatorio.SetFocus
            CamposObligatorios = False
            Exit Function
        End If
    End If
    '{ add -037- a. En esta funci�n esta contemplado que impl�citamente reconozca que no se
    '               desea guardar datos de proyecto IDM.
    If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) > 0 Then
        If Not ValidarProyecto(txtPrjId, txtPrjSubId, txtPrjSubsId) Then
            Call orjBtn_Click(orjDAT)
            CamposObligatorios = False
            'MsgBox "El proyecto IDM ingresado no es v�lido. Revise.", vbOKOnly + vbExclamation
            Exit Function
        End If
    End If
    '}
    ' *********************************************
    ' Controles para peticiones de tipo Proyecto...
    ' *********************************************
    If CodigoCombo(cboTipopet, True) = "PRJ" Then
        ' Control de Importancia (Visibilidad)
        If cboImportancia.ListIndex < 0 Then
           Call orjBtn_Click(orjDAT)
           MsgBox "Falta especificar VISIBILIDAD", vbOKOnly + vbExclamation
           CamposObligatorios = False
           Exit Function
        End If
        ' Control de Corporativo/Local
        If cboCorpLocal.ListIndex < 0 Then
           Call orjBtn_Click(orjDAT)
           MsgBox "Falta especificar Corp/Local", vbOKOnly + vbExclamation
           CamposObligatorios = False
           Exit Function
        End If
        ' Control de orientaci�n
        If cboOrientacion.ListIndex < 0 Then
            Call orjBtn_Click(orjDAT)
            MsgBox "Falta especificar ORIENTACION", vbOKOnly + vbExclamation
            CamposObligatorios = False
            Exit Function
        End If
        '{ add -001- d. (Proyecto) Error si trata de designar la case CORR, ATEN o SPUF
        'If glPeticion_sox001 = 1 Then   ' add -004- c.     ' del -007- a.
        If cModeloControl = MODELO_CONTROL_NUEVO Then   ' add -007- a.
            If InStr(1, "NUEV|EVOL|", CodigoCombo(cboClase, True)) = 0 Then
                Call orjBtn_Click(orjDAT)
                MsgBox "Las peticiones de tipo Proyecto solo pueden ser de alguna de las siguientes clases:" & vbCrLf & vbCrLf & "- Mantenimiento evolutivo" & vbCrLf & "- Nuevo desarrollo", vbExclamation + vbOKOnly, "Clase de petici�n"
                CamposObligatorios = False
                Exit Function
            End If
        End If      ' add -004- c.
        '}
    End If
    
    '{ add -001- d. Control de clase de petici�n
    ' Control de impacto tecnol�gico
    'If glPeticion_sox001 = 1 Then       ' add -004- c. ' del -007- a.
    If cModeloControl = MODELO_CONTROL_NUEVO Then       ' add -007- c.
        'If cboImpacto.ListIndex = 1 And Not PerfilInternoActual = "SOLI" Then          ' Aqu� el caso que indica que lleva SI impacto tecnol�gico
        'If cboImpacto.List(cboImpacto.ListIndex) = "Si" And (Not PerfilInternoActual = "SOLI" And Not PerfilInternoActual = "ANAL") Then    ' upd -051- a.
        If CodigoCombo(cboImpacto, True) = "S" And (Not PerfilInternoActual = "SOLI" And Not PerfilInternoActual = "ANAL") Then    ' upd -051- a.
            If InStr(1, CodigoCombo(cboTipopet, True), "PRO") > 0 Then
                Select Case CodigoCombo(cboClase, True)       ' Clase de petici�n
                    Case "CORR"  ' Clase: Correctivo
                        Call orjBtn_Click(orjDAT)
                        MsgBox "Una petici�n de la clase correctivo no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                        CamposObligatorios = False
                        Exit Function
                    Case "ATEN"  ' Clase: Atenci�n a Usuario
                        Call orjBtn_Click(orjDAT)
                        MsgBox "Una petici�n de la clase atenci�n a usuario no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                        CamposObligatorios = False
                        Exit Function
                    Case "SPUF" ' Clase: SPUFIs
                        Call orjBtn_Click(orjDAT)
                        MsgBox "Una petici�n de la clase SPUFI no puede implicar impacto tecnol�gico.", vbCritical + vbOKOnly, "Impacto tecnol�gico"
                        CamposObligatorios = False
                        Exit Function
                End Select
            End If
        End If
    End If
    ' Controles varios
    ' Def.: Peticiones activas del nuevo modelo, el actor no es ni SOLI, ni ANAL ni ADMI/SADM, en ALTA y MODI
    If Not InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) > 0 Then
        If cModeloControl = MODELO_CONTROL_NUEVO Then
            'If Not PerfilInternoActual = "SOLI" And Not PerfilInternoActual = "ANAL" And Not PerfilInternoActual = "SADM" Then       ' upd -006- h.  ' upd -051- a.
            If Not InStr(1, "SOLI|ANAL|SADM|ADMI|", PerfilInternoActual, vbTextCompare) > 0 Then
                Select Case glModoPeticion
                    Case "ALTA", "MODI"
                        Select Case CodigoCombo(cboTipopet, True)
                            Case "ESP"
                                'If glLOGIN_Gerencia <> "ORG." And glLOGIN_Gerencia <> "TRYAPO" Then
                                'If Not InStr(1, "ORG.|TRYAPO|", glLOGIN_Gerencia, vbTextCompare) > 0 Then  ' del -098- a.
                                If PerfilInternoActual <> "BPAR" Then                                       ' add -098- a.
                                    If CodigoCombo(cboClase, True) <> "ATEN" Then
                                        Call orjBtn_Click(orjDAT)
                                        MsgBox "Peticiones Especiales para el perfil actual solo pueden ser Atenci�n a Usuario.", vbExclamation + vbOKOnly, "Clase de petici�n"
                                        CamposObligatorios = False
                                        Exit Function
                                    End If
                                End If      ' add -098- a.
                                'End If     ' del -098- a.
                            Case "PRO"
                                If CodigoCombo(cboClase, True) = "ATEN" Then
                                    Call orjBtn_Click(orjDAT)
                                    MsgBox "Para generar una petici�n de clase Atenci�n al usuario, debe especificar que el tipo sea Especial.", vbExclamation + vbOKOnly, "Clase de petici�n"
                                    CamposObligatorios = False
                                    Exit Function
                                End If
                                'If CodigoCombo(cboClase, True) <> "CORR" And CodigoCombo(cboClase, True) <> "OPTI" And CodigoCombo(cboClase, True) <> "SPUF" Then   ' add -001- n.
                                If Not InStr(1, "CORR|OPTI|SPUF|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                                    Call orjBtn_Click(orjDAT)
                                    MsgBox "Peticiones Propias para el perfil actual solo pueden ser: " & vbCrLf & vbCrLf & _
                                    "- Correctivo" & vbCrLf & _
                                    "- SPUFI" & vbCrLf & _
                                    "- Optimizaci�n", vbExclamation + vbOKOnly, "Clase de petici�n"
                                    CamposObligatorios = False
                                    Exit Function
                                End If
                            Case "PRJ"
                                'If CodigoCombo(cboClase, True) <> "OPTI" And CodigoCombo(cboClase, True) <> "EVOL" And CodigoCombo(cboClase, True) <> "NUEV" Then
                                If Not InStr(1, "OPTI|EVOL|NUEV|", CodigoCombo(cboClase, True), vbTextCompare) > 0 Then
                                    Call orjBtn_Click(orjDAT)
                                    MsgBox "Peticiones de Proyecto para el perfil actual solo pueden ser: " & vbCrLf & vbCrLf & _
                                    "- Correctivo" & vbCrLf & _
                                    "- Atenci�n a Usuario", vbExclamation + vbOKOnly, "Clase de petici�n"
                                    CamposObligatorios = False
                                    Exit Function
                                End If
                        End Select
                End Select
            End If
        End If
    End If
    ' Control de sector
    If cboSector.ListIndex < 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Falta especificar el sector solicitante.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    ' Control de solicitante
    If cboSolicitante.ListIndex < 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Falta especificar al solicitante.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    ' Control de la descripci�n
    If ClearNull(memDescripcion.Text) = "" Then
        Call orjBtn_Click(orjMEM)
        MsgBox "Falta especificar la descripci�n.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        memDescripcion.SetFocus
        Exit Function
    End If
    '{ add -095- a. Control de empresa
    If cboEmp.ListIndex < 0 Then
        Call orjBtn_Click(orjDAT)
        MsgBox "Falta especificar la empresa.", vbOKOnly + vbExclamation
        CamposObligatorios = False
        Exit Function
    End If
    '}
    ' Control de Caracter�sticas, Motivos y Beneficios (para las no propias)
    If CodigoCombo(cboTipopet, True) <> "PRO" Then
        If ClearNull(memCaracteristicas.Text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Faltan especificar las caracter�sticas.", vbOKOnly + vbExclamation
           CamposObligatorios = False
           memCaracteristicas.SetFocus
           Exit Function
        End If
        If ClearNull(memMotivos.Text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Faltan especificar los motivos.", vbOKOnly + vbExclamation
           CamposObligatorios = False
           memMotivos.SetFocus
           Exit Function
        End If
        If ClearNull(memBeneficios.Text) = "" Then
           Call orjBtn_Click(orjMEM)
           MsgBox "Faltan especificar los beneficios esperados.", vbOKOnly + vbExclamation
           CamposObligatorios = False
           memBeneficios.SetFocus
           Exit Function
        End If
    End If
    '{ add -037- a.
    If Len(txtPrjId.Text) = 0 Then txtPrjId.Text = "0"
    If Len(txtPrjSubId.Text) = 0 Then txtPrjSubId.Text = "0"
    If Len(txtPrjSubsId.Text) = 0 Then txtPrjSubsId.Text = "0"
    '}
    CamposObligatorios = True
End Function

Private Sub CargaPeticion(Optional bCompleto)
    If IsMissing(bCompleto) Then
        bCompleto = True
    End If
    ' VER PARA QUE SE USAN ESTAS DOS!!!!
    'Dim xDescri As String      ' No se estar�a usando en este proceso CargarPeticion
    Dim xAsig As String         ' Auxiliar p/peticiones anexadas
    
    Call Puntero(True)
    flgIngerencia = False
    flgSolicitor = False
      
    ' 1. Primero cargo todos los controles
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        txtNrointerno.Text = IIf(Not IsNull(aplRST!pet_nrointerno), aplRST!pet_nrointerno, "")
        txtNroAsignado.Text = IIf(Not IsNull(aplRST!pet_nroasignado), aplRST!pet_nroasignado, "S/N")
        If Val(ClearNull(aplRST!pet_nroanexada)) > 0 Then
            xAsig = IIf(Val(ClearNull(aplRST!anx_nroasignado)) > 0, ClearNull(aplRST!anx_nroasignado), "S/N")
            txtNroAnexada.Text = xAsig & Space(10) & "||" & IIf(Not IsNull(aplRST!pet_nroanexada), aplRST!pet_nroanexada, "")
        Else
            txtNroAnexada.Text = ""
        End If
        txtTitulo.Text = ClearNull(aplRST!Titulo)
        txtEstado.Text = ClearNull(aplRST!nom_estado)
        txtSituacion.Text = ClearNull(aplRST!nom_situacion)
        txtFechaPedido.Text = IIf(Not IsNull(aplRST!fe_pedido), Format(aplRST!fe_pedido, "dd/mm/yyyy"), "")
        txtFechaRequerida.Text = IIf(Not IsNull(aplRST!fe_requerida), Format(aplRST!fe_requerida, "dd/mm/yyyy"), "")
        txtFiniplan.Text = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
        txtFfinplan.Text = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
        txtFinireal.Text = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "dd/mm/yyyy"), "")
        txtFfinreal.Text = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "dd/mm/yyyy"), "")
        txtFechaEstado.Text = IIf(Not IsNull(aplRST!fe_estado), Format(aplRST!fe_estado, "dd/mm/yyyy"), "")
        txtFechaComite.Text = IIf(Not IsNull(aplRST!fe_comite), Format(aplRST!fe_comite, "dd/mm/yyyy"), "")
        txtHspresup.Text = ClearNull(aplRST!horaspresup)
        cboPrioridad.ListIndex = PosicionCombo(Me.cboPrioridad, ClearNull(aplRST!prioridad), True)
        cboTipopet.ListIndex = PosicionCombo(cboTipopet, ClearNull(aplRST!cod_tipo_peticion), True)
        cboClase.ListIndex = PosicionCombo(cboClase, ClearNull(aplRST!cod_clase), True)
        cboImpacto.ListIndex = PosicionCombo(cboImpacto, ClearNull(aplRST!pet_imptech), True)
        cboRO.ListIndex = PosicionCombo(cboRO, ClearNull(aplRST!pet_ro), True): cboRO.Tag = ClearNull(aplRST!pet_ro)        ' add -097- a.
        txtusrImportancia.Text = ClearNull(aplRST!importancia_usrname)
        cmdVerAnexora.Enabled = IIf(Val(txtNroAnexada.Text) = 0, True, False)
        cboRegulatorio.ListIndex = PosicionCombo(cboRegulatorio, ClearNull(aplRST!pet_regulatorio), True)
        txtHsPlanif.Text = ClearNull(aplRST!cant_planif)                            ' add -024- a. Ver el sp_UpdatePetSecFechas (quien calcula este dato)
        txtPrjId.Text = IIf(IsNull(aplRST!pet_projid), 0, aplRST!pet_projid)
        txtPrjSubId.Text = IIf(IsNull(aplRST!pet_projsubid), 0, aplRST!pet_projsubid)
        txtPrjSubsId.Text = IIf(IsNull(aplRST!pet_projsubsid), 0, aplRST!pet_projsubsid)
        Me.Tag = ClearNull(aplRST!cod_usualta)
        ' ******************************************************************************
        ' HAY QUE VER QUE HORROR ESTA USAN   DO ESTAS VARIABLES!!!!!
        ' ******************************************************************************
        ' 2. Luego, todas las variables con el estado de la petici�n antes de modificar
        glPETFechaEnvioAlBP = IIf(IsNull(aplRST!fe_comite), Format(0, "dd/mm/yyyy"), ClearNull(aplRST!fe_comite))
        glPETImpacto = ClearNull(aplRST!pet_imptech)
        glPETClase = ClearNull(aplRST!cod_clase)
        glPETTipo = ClearNull(aplRST!cod_tipo_peticion)
        ' ******************************************************************************
        cModeloControl = ClearNull(aplRST!pet_sox001)        ' add -007- a.
        sSituacion = ClearNull(aplRST!cod_situacion)
        glPETModelo = IIf(aplRST!pet_sox001 = 1, True, False)
        sRegulatorio = ClearNull(aplRST!pet_regulatorio)    ' PARA QUE CORCHOS ESTA VARIABLE!!!
        glNroAsignado = txtNroAsignado.Text
        prjNroInterno = IIf(Not IsNull(aplRST!prj_nrointerno), aplRST!prj_nrointerno, "")
        sEstado = ClearNull(aplRST!cod_estado)
        'cPrioridad = ClearNull(aplRST!prioridad)            ' add -040- b.
        sTipoPet = ClearNull(aplRST!cod_tipo_peticion)
        xCorpLocal = ClearNull(aplRST!corp_local)
        xImportancia = ClearNull(aplRST!importancia_cod)
        'cVisibilidad = xImportancia                         ' add -040- b.
        xOrientacion = ClearNull(aplRST!cod_orientacion)
        xUsrLoginActual = ClearNull(aplRST!importancia_usr)
        xUsrPerfilActual = ClearNull(aplRST!importancia_prf)
        xDir = ClearNull(aplRST!cod_direccion)
        xSec = ClearNull(aplRST!cod_sector)
        xGer = ClearNull(aplRST!cod_gerencia)
        sSoli = ClearNull(aplRST!cod_solicitante)
        sRefe = ClearNull(aplRST!cod_referente)
        sSupe = ClearNull(aplRST!cod_supervisor)
        sDire = ClearNull(aplRST!cod_director)
        sUsualta = ClearNull(aplRST!cod_usualta)
        sBpar = ClearNull(aplRST!cod_bpar)
        cboEmp.ListIndex = PosicionCombo(cboEmp, ClearNull(aplRST!pet_emp), True)   ' add -095- a.
        If bCompleto Then
            Call Status("Cargando descripcion...")
            memDescripcion.Text = sp_GetMemo(glNumeroPeticion, "DESCRIPCIO")
            Call Status("Cargando caracteristicas...")
            memCaracteristicas.Text = sp_GetMemo(glNumeroPeticion, "CARACTERIS")
            Call Status("Cargando motivos...")
            memMotivos.Text = sp_GetMemo(glNumeroPeticion, "MOTIVOS")
            Call Status("Cargando beneficios...")
            memBeneficios.Text = sp_GetMemo(glNumeroPeticion, "BENEFICIOS")
            Call Status("Cargando relaciones...")
            memRelaciones.Text = sp_GetMemo(glNumeroPeticion, "RELACIONES")
            Call Status("Cargando observaciones...")
            memObservaciones.Text = sp_GetMemo(glNumeroPeticion, "OBSERVACIO")
            txObservaciones = memObservaciones.Text
            txBeneficios = memBeneficios.Text
            txCaracteristicas = memCaracteristicas.Text
            txDescripcion = memDescripcion.Text
            txMotivos = memMotivos.Text
            txRelaciones = memRelaciones.Text
        End If
        ' Valores que carga luego de usar el recordset de Peticion
        txtPrjNom.Text = NombreProyecto(txtPrjId, txtPrjSubId, txtPrjSubsId)      ' Busco el nombre del proyecto linkeado a la petici�n
        txtUsualta.Tag = sp_GetRecursoNombre(ClearNull(Me.Tag))
        txtUsualta.Text = ClearNull(Me.Tag) & " : " & IIf(txtUsualta.Tag = "", "Error Solicitante", ClearNull(txtUsualta.Tag)) ' add -037- a.
        Call ValidarPeticion    ' Verifica si la petici�n es v�lida para pasajes a Producci�n (bandeja diaria PeticionChangeMan)
    End If
    ''INGERENCIA
    flgEjecutor = InEjecutor()
    flgBP = esBP()
    flgEjecutorParaModificar = EsEjecutorParaModificar()
    Select Case PerfilInternoActual
        Case "SADM"
            flgIngerencia = True
        Case "BPAR"
            If flgBP Then
                flgIngerencia = True
            End If
        Case "SOLI", "REFE", "AUTO", "SUPE"
            If xPerfNivel = "BBVA" Or _
                xPerfNivel = "DIRE" And xDir = xPerfDir Or _
                xPerfNivel = "GERE" And xGer = xPerfGer Or _
                xPerfNivel = "SECT" And xSec = xPerfSec Then
                    flgIngerencia = True
                    flgSolicitor = True
            End If
        'Se hacen preguntas especiales para los que estan en la rama ejecutora
        Case "CSEC"
            If InSector() Then
                flgIngerencia = True
            End If
        Case "CGRU", "ANAL"     ' upd -096- a. Se agrega "ANAL"
            If InGrupo() Then
                flgIngerencia = True
            End If
        Case "CDIR", "CGCI"
            'esto esta harcodeado, por pedido de Milstein
            'estos dos perfiles pueden Modificar, (solo la VISIBILIDAD)
            flgIngerencia = True
    End Select
    'Call InicializarCombos
    
    Call CargaCombosConValor
    
    If xSec <> "" Then Call SetCombo(cboSector, xSec, True)
    If sSoli <> "" Then Call SetCombo(cboSolicitante, sSoli)
    If sRefe <> "" Then Call SetCombo(cboReferente, sRefe)
    If sSupe <> "" Then Call SetCombo(cboSupervisor, sSupe)
    If sDire <> "" Then Call SetCombo(cboDirector, sDire)
    Call SetCombo(cboBpar, sBpar)
    cboCorpLocal.ListIndex = PosicionCombo(cboCorpLocal, xCorpLocal, True)
    cboImportancia.ListIndex = PosicionCombo(cboImportancia, xImportancia, True)
    cboOrientacion.ListIndex = PosicionCombo(cboOrientacion, xOrientacion, True)
    Call Puntero(False)
End Sub

Private Sub InicializarCombos()
    Dim i As Integer                ' add -101- a.
    Dim xDescri As String
    
'    '{ add -101- a.
'    ' Actualizaci�n din�mica de la clase seg�n el tipo seleccionado
'    With cboClase
'        .Clear
'        For i = 0 To UBound(PetParam)
'            If PetParam(i).tipo = defTipoPet Then
'                .AddItem PetParam(i).ClaseNom & ESPACIOS & "||" & PetParam(i).clase
'            End If
'        Next i
'        .ListIndex = 0
'    End With
'    '}
    
'{ del -101- a.
'
'    '{ add -002- c. Carga din�mica de la clase de la petici�n seg�n el tipo de petici�n
'    Select Case glLOGIN_Gerencia
'        '{ del -098- a.
''        Case "ORG.", "TRYAPO"       ' upd -017- a. Se agrega "TRYAPO"
''            ' Organizaci�n y Transformaci�n y Apoyo a Red.
''            Select Case glModoPeticion
''                Case "ALTA"
''                    Select Case PerfilInternoActual
''                        Case "CGRU", "CSEC"
''                            Select Case CodigoCombo(cboTipopet, True)
''                                Case "PRO"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "Correctivo" & ESPACIOS & "||CORR"
''                                        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
''                                        .ListIndex = 0
''                                    End With
''                                Case "ESP"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
''                                        .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
''                                        .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
''                                        .ListIndex = 0
''                                    End With
''                            End Select
''                        Case Else
''                            Select Case CodigoCombo(cboTipopet, True)
''                                Case "NOR"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
''                                        .ListIndex = 0
''                                    End With
''                            End Select
''                    End Select
''                Case "MODI"
''                    Select Case PerfilInternoActual
''                        Case "CGRU", "CSEC"
''                            Select Case CodigoCombo(cboTipopet, True)
''                                Case "PRO"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "Correctivo" & ESPACIOS & "||CORR"
''                                        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
''                                        .ListIndex = 0
''                                    End With
''                                Case "ESP"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
''                                        .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
''                                        .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
''                                        .ListIndex = 0
''                                    End With
''                            End Select
''                        Case "BPAR"
''                            Select Case CodigoCombo(cboTipopet, True)
''                                Case "NOR"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
''                                        .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
''                                        .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
''                                        .ListIndex = 0
''                                    End With
''                                Case "ESP"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
''                                        .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
''                                        .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
''                                        .ListIndex = 0
''                                    End With
''                                Case "PRJ"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "Mantenimiento evolutivo" & ESPACIOS & "||EVOL"
''                                        .AddItem "Nuevo desarrollo" & ESPACIOS & "||NUEV"
''                                        .ListIndex = 0
''                                    End With
''                            End Select
''                        Case Else
''                            Select Case CodigoCombo(cboTipopet, True)
''                                Case "NOR"
''                                    With cboClase
''                                        .Clear
''                                        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
''                                        .ListIndex = 0
''                                    End With
''                            End Select
''                    End Select
''            End Select
'        '}
'        Case "DESA"
'        ' Dise�o y Desarrollo
'            Select Case glModoPeticion
'                Case "ALTA"
'                    Select Case PerfilInternoActual
'                        Case "CGRU", "CSEC"
'                            Select Case CodigoCombo(cboTipopet, True)
'                                Case "PRO"
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                                        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                                        .AddItem "SPUFI" & ESPACIOS & "||SPUF"
'                                        .ListIndex = 0
'                                    End With
'                                Case "ESP"
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                                        .ListIndex = 0
'                                    End With
'                            End Select
'                    End Select
'                Case "MODI"
'                    Select Case PerfilInternoActual
'                        Case "CGRU", "CSEC"
'                            Select Case CodigoCombo(cboTipopet, True)
'                                Case "PRO"
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                                        .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                                        .AddItem "SPUFI" & ESPACIOS & "||SPUF"
'                                        .ListIndex = 0
'                                    End With
'                                Case "ESP"
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                                        .ListIndex = 0
'                                    End With
'                            End Select
'                    End Select
'            End Select
'        Case Else
'        ' Usuarios
'            Select Case glModoPeticion
'                Case "ALTA"
'                    Select Case PerfilInternoActual
'                        Case "SOLI"
'                            Select Case CodigoCombo(cboTipopet, True)
'                                Case "NOR"
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                        .ListIndex = 0
'                                    End With
'                            End Select
'                        '{ add parche
'                        Case Else
'                            If PerfilInternoActual <> "ANAL" Then       ' add -051- a.
'                                Select Case CodigoCombo(cboTipopet, True)
'                                    Case "PRO"
'                                        With cboClase
'                                            .Clear
'                                            .AddItem "Correctivo" & ESPACIOS & "||CORR"
'                                            .AddItem "Optimizaci�n" & ESPACIOS & "||OPTI"
'                                            .AddItem "SPUFI" & ESPACIOS & "||SPUF"
'                                            .ListIndex = 0
'                                        End With
'                                    Case "ESP"
'                                        With cboClase
'                                            .Clear
'                                            .AddItem "Atenci�n a usuario" & ESPACIOS & "||ATEN"
'                                            .ListIndex = 0
'                                        End With
'                                End Select
'                            End If      ' add -051- a.
'                            '}
'                    End Select
'                Case "MODI"
'                    Select Case PerfilInternoActual
'                        Case "SOLI"
'                            Select Case CodigoCombo(cboTipopet, True)
'                                Case "NOR"
'                                    With cboClase
'                                        .Clear
'                                        .AddItem "S/C (Sin clase)" & ESPACIOS & "||SINC"
'                                        .ListIndex = 0
'                                    End With
'                            End Select
'                    End Select
'            End Select
'    End Select
'    '}
'}
    If glModoPeticion = "ALTA" Then
        Call InitTipoPet                    ' Cargar los combos de Tipo y Clase de la petici�n (ALTA)
    Else
        Call CargarPetTipo(True)            ' Carga completa de tipo de petici�n
    End If

    cboImportancia.Clear
    If sp_GetImportancia(Null, Null) Then
        Do While Not aplRST.EOF
            cboImportancia.AddItem Trim(aplRST!nom_importancia) & ESPACIOS & "||" & Trim(aplRST!cod_importancia)
            aplRST.MoveNext
        Loop
    End If
    cboOrientacion.Clear
    If sp_GetOrientacion(Null, Null) Then
        Do While Not aplRST.EOF
            cboOrientacion.AddItem Trim(aplRST!nom_orientacion) & ESPACIOS & "||" & Trim(aplRST!cod_orientacion)
            aplRST.MoveNext
        Loop
    End If
    cboBpar.Clear
    If sp_GetRecursoPerfil(Null, "BPAR") Then
        Do While Not aplRST.EOF
            cboBpar.AddItem ClearNull(aplRST!cod_recurso) & " : " & Trim(aplRST!nom_recurso)
            aplRST.MoveNext
        Loop
    End If
    
    Select Case glModoPeticion
        Case "ALTA"
            Select Case PerfilInternoActual
                Case "SADM"
                    Call CargaCombosConTodo                     ' carga todo para que pueda modificar cuelquier cosa
                Case "SOLI"
                    Call CargaSectoresPosibles                  ' cargo lo que depende del perfil
                    If sp_GetRecurso(sSoli, "R") Then
                        If Not aplRST.EOF Then
                            cboSolicitante.AddItem Trim(aplRST(0)) & " : " & ClearNull(aplRST(1))   ' upd -040- c.
                        End If
                    End If
                    cboSolicitante.ListIndex = 0
                Case "CSEC", "CGRU", "ANAL"                     ' upd -051- a. Se agrega el perfil ANAL
                    If InStr(1, "PRO|PRJ|", CodigoCombo(cboTipopet, True), vbTextCompare) > 0 Then
                        Call CargaSectoresPosibles              ' cargo lo que depende del perfil
                        cboSolicitante.Clear
                        cboReferente.Clear
                        cboDirector.Clear
                        cboSupervisor.Clear
                        If sp_GetRecurso(sSoli, "R") Then
                            If Not aplRST.EOF Then
                                cboSolicitante.AddItem ClearNull(aplRST(0)) & " : " & ClearNull(aplRST(1))
                                cboReferente.AddItem ClearNull(aplRST(0)) & " : " & ClearNull(aplRST(1))
                                cboDirector.AddItem ClearNull(aplRST(0)) & " : " & ClearNull(aplRST(1))
                            End If
                        End If
                        cboSolicitante.ListIndex = 0
                        cboReferente.ListIndex = 0
                        cboDirector.ListIndex = 0
                    Else
                        Call CargaCombosConTodo                 'todo
                    End If
            End Select
            Call SetCombo(cboBpar, getBpSector(CodigoCombo(cboSector, True)))
        Case "MODI"
            Select Case PerfilInternoActual
                Case "SADM"
                    Call CargaCombosConTodo                     'carga todo para que pueda modificar cuelquier cosa
                Case "CSEC", "CGRU"
                    If InStr(1, "PRO|PRJ|", CodigoCombo(cboTipopet, True), vbTextCompare) > 0 Then
                        Call CargaCombosConValor                'primero cargo lo que figura en la peticion
                        Call CargaSectoresPosibles              'cargo lo que depende del perfil
                    Else
                        Call CargaCombosConValor                'solo cargo lo que figura en la peticion
                    End If
                Case Else
                    Call CargaCombosConValor                    'solo cargo lo que figura en la peticion
                End Select
        Case "VIEW"
            Call CargaCombosConValor                            'solo cargo lo que figura en la peticion
    End Select
End Sub

Private Sub CargaSectoresPosibles()
    Dim xDescri As String
    
    'Call Status("Cargando sectores...")
    cboSector.Clear
    If sp_GetSectorXt(xPerfSec, xPerfGer, xPerfDir) Then
        Do While Not aplRST.EOF
            cboSector.AddItem ClearNull(aplRST!nom_direccion) & " � " & ClearNull(aplRST!nom_gerencia) & " � " & ClearNull(aplRST!nom_sector) & ESPACIOS & "||" & aplRST(0)  ' add -051- d.
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If cboSector.ListCount = 1 Then
        cboSector.ListIndex = 0
        '{ add -017- a.
        ' Si es solo un sector el cargado, entonces inhabilito la posibilidad de selecci�n del control Combo
        Call setHabilCtrl(cboSector, "DIS")
        '}
    Else
        Call SetCombo(cboSector, xPerfSec, True)
    End If
End Sub

Private Sub CargaIntervinientes()
    Dim sSec, sGer, sDir As String
    
    '{ add -014- a. ???
    If cboSector.ListIndex = -1 Then
        cboSector.ListIndex = 0
    End If
    '}
    cboSolicitante.Clear
    cboReferente.Clear
    cboDirector.Clear
    cboSupervisor.Clear
    
    sSec = CodigoCombo(Me.cboSector, True)
    If sp_GetSectorXt(sSec, Null, Null) Then
        sDir = ClearNull(aplRST!cod_direccion)
        sGer = ClearNull(aplRST!cod_gerencia)
    End If
    With cboSolicitante
        If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "SOLI") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST!cod_recurso) & " : " & Trim(aplRST!nom_recurso)   ' upd -040- c.
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = SetCombo(cboSolicitante, sSoli)
        End If
        .BackColor = IIf(.ListCount > 0, vbWhite, vbRed)
    End With
    ' Cargo el resto de los perfiles: referentes, autorizantes, supervisores
    If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "REFE") Then
        Do While Not aplRST.EOF
            cboReferente.AddItem ClearNull(aplRST!cod_recurso) & " : " & Trim(aplRST!nom_recurso)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    'cboReferente.BackColor = IIf(cboReferente.ListCount > 0, vbWhite, vbRed)
    If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "SUPE") Then
        Do While Not aplRST.EOF
            cboSupervisor.AddItem ClearNull(aplRST!cod_recurso) & " : " & Trim(aplRST!nom_recurso)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If sp_GetRecursoActuanteArea(sDir, sGer, sSec, "NULL", "AUTO") Then
        Do While Not aplRST.EOF
            cboDirector.AddItem ClearNull(aplRST!cod_recurso) & " : " & Trim(aplRST!nom_recurso)
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If ClearNull(xSec) = ClearNull(CodigoCombo(cboSector, True)) Then
        If ClearNull(sSoli) = ClearNull(CodigoCombo(cboSolicitante)) Then
            If sRefe <> "" Then Call SetCombo(cboReferente, sRefe)
            If sSupe <> "" Then Call SetCombo(cboSupervisor, sSupe)
            If sDire <> "" Then Call SetCombo(cboDirector, sDire)
        End If
    End If
    Call Status("Listo.")
End Sub

Sub CargaCombosConTodo()
    Dim xDescri As String
    
    cboSector.Clear
    If sp_GetSectorXt(Null, Null, Null, "S") Then   ' upd -055- b. Se agrega el par�metro opcional "S" para evitar la carga de sectores inhabilitados.
        Do While Not aplRST.EOF
            cboSector.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & ESPACIOS & "||" & aplRST(0)    ' add -051- d.
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If xSec <> "" Then
        Call SetCombo(cboSector, xSec, True)
    End If
    If PerfilInternoActual <> "SADM" Then
        Call CargaIntervinientes
    End If
End Sub

Private Sub CargaCombosConValor()
    ' CargaCombosConValor:
    ' Este procedimiento vuelve a cargar los datos de:
    ' - Sector solicitante
    ' - Solicitante
    ' - Referente
    ' - Supervisor
    ' - Autorizante
    ' - Business Partner
    
    Dim xDescri As String
    
    cboSector.Clear
    
    '{ add -035- a.
    'If sp_GetSectorXt(xSec, xGer, xDir) Then
    If sp_GetSectorXt(xSec, xGer, xDir) Then
        If Not aplRST.EOF Then
            cboSector.AddItem Trim(aplRST!nom_direccion) & " � " & Trim(aplRST!nom_gerencia) & " � " & Trim(aplRST!nom_sector) & ESPACIOS & "||" & aplRST(0)     ' add -051- d.
            cboSector.ListIndex = 0
        End If
    End If
    '}
    ' Solicitante
    If sSoli <> "" Then
        '{ add -035- a.
        ' Si el usuario que di� el alta de la petici�n no es el mismo que el declarado como
        ' Solicitante, entonces igualo el solicitante al usuario de alta (si la petici�n ahora
        ' es Propia y antes era otra cosa o Especial)
        If glPETTipo = "ESP" And CodigoCombo(cboTipopet, True) = "PRO" Then
            If sUsualta <> sSoli Then
                sSoli = sUsualta
                sRefe = sUsualta
                sDire = sUsualta
            End If
        End If
        '}
        'Call Status("Cargando solicitante...")
        cboSolicitante.Clear
        If sp_GetRecurso(sSoli, "R") Then
            xDescri = Trim(sSoli) & " : " & ClearNull(aplRST(1))
            'aplRST.Close
        Else
            xDescri = Trim(sSoli) & " : " & "Error Solicitante"
        End If
        cboSolicitante.AddItem xDescri
        cboSolicitante.ListIndex = 0
        '{ add -054- a.
        If glModoPeticion = "VIEW" Then
            If cboSolicitante.ListIndex = -1 Then
                If sSoli <> "" Then
                    cboSolicitante.AddItem Trim(sSoli) & " : " & sp_GetRecursoNombre(sSoli)
                    Call SetCombo(cboSolicitante, sSoli)
                End If
            End If
        End If
        '}
    End If
    ' Referente
    If sRefe <> "" Then
        'Call Status("Cargando referente...")
        cboReferente.Clear
        If sp_GetRecurso(sRefe, "R") Then
            xDescri = sRefe & " : " & ClearNull(aplRST(1))
            'aplRST.Close
        Else
            xDescri = sRefe & " : " & "Error Referente"
        End If
        cboReferente.AddItem xDescri
        cboReferente.ListIndex = 0
        '{ add -054- a.
        If glModoPeticion = "VIEW" Then
            If cboReferente.ListIndex = -1 Then
                If sRefe <> "" Then
                    cboReferente.AddItem sRefe & " : " & sp_GetRecursoNombre(sRefe)
                    Call SetCombo(cboReferente, sRefe)
                End If
            End If
        End If
        '}
    End If
    ' Supervisor
    If sSupe <> "" Then
        'Call Status("Cargando supervisor...")
        cboSupervisor.Clear
        If sp_GetRecurso(sSupe, "R") Then
            xDescri = sSupe & " : " & ClearNull(aplRST(1))
            'aplRST.Close
        Else
            xDescri = sSupe & " : " & "Error Supervisor"
        End If
        cboSupervisor.AddItem xDescri
        cboSupervisor.ListIndex = 0
        '{ add -054- a.
        If glModoPeticion = "VIEW" Then
            If cboSupervisor.ListIndex = -1 Then
                If sSupe <> "" Then
                    cboSupervisor.AddItem sSupe & " : " & sp_GetRecursoNombre(sSupe)
                    Call SetCombo(cboSupervisor, sSupe)
                End If
            End If
        End If
        '}
    End If
    ' Autorizante
    If sDire <> "" Then
        'Call Status("Cargando director...")
        cboDirector.Clear
        If sp_GetRecurso(sDire, "R") Then
            xDescri = sDire & " : " & ClearNull(aplRST(1))
            'aplRST.Close
        Else
            xDescri = sDire & " : " & "Error Director"
        End If
        cboDirector.AddItem xDescri
        cboDirector.ListIndex = 0
        '{ add -054- a.
        If glModoPeticion = "VIEW" Then
            If cboDirector.ListIndex = -1 Then
                If sDire <> "" Then
                    cboDirector.AddItem sDire & " : " & sp_GetRecursoNombre(sDire)
                    Call SetCombo(cboDirector, sDire)
                End If
            End If
        End If
        '}
    End If
    '{ add -051- d.
    If sBpar <> "" Then
        'Call Status("Cargando Business Partner...")
        'Call SetCombo(cboBpar, getBpSector(xSec))      ' del -054- a.
        Call SetCombo(cboBpar, sBpar)                   ' add -054- a.
    End If
    '}
    '{ add -054- a. Quiere decir que no encuentra el elemento en el combo (le quitaron el perfil al recurso)
    If glModoPeticion = "VIEW" Then
        If cboBpar.ListIndex = -1 Then
            If sBpar <> "" Then
                cboBpar.AddItem ClearNull(sBpar) & " : " & sp_GetRecursoNombre(sBpar)
                Call SetCombo(cboBpar, sBpar)
            End If
        End If
    End If
    '}
    'Call Status("Listo.")
End Sub

Private Sub CargaAnexas()
    anxNroPeticion = ""
    cmdVerAnexa.Enabled = False
    With grdAnexas
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colNroInterno) = "NroInt."
        .TextMatrix(0, colNroAsignado) = "Petici�n"
        .TextMatrix(0, colTitulo) = "T�tulo"
        .ColWidth(colNroAsignado) = 800
        .ColWidth(colTitulo) = 8000
        .ColWidth(colNroInterno) = 0
        .ColAlignment(colTitulo) = 0
    End With
    'truchada solo para mostrar
    Call Status("Cargando anexas...")
    If glNumeroPeticion <> "" Then
        If Not sp_GetPeticionAnexadas(glNumeroPeticion) Then
            aplRST.Close
            Call Status("Listo.")
            GoTo finx
            Exit Sub
        End If
        Call Status("Listo.")
    Else
        Call Status("Listo.")
        Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdAnexas
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colNroInterno) = ClearNull(aplRST!pet_nrointerno)
            .TextMatrix(.Rows - 1, colNroAsignado) = ClearNull(aplRST!pet_nroasignado)
            .TextMatrix(.Rows - 1, colTitulo) = ClearNull(aplRST!Titulo)
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
finx:
    '{ add -002- d.
    Dim i As Long
    
    With grdAnexas
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Font.Bold = True
        .Row = 0
        For i = 0 To .cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .FocusRect = flexFocusNone
    End With
    '}
    If grdAnexas.Rows > 1 Then
        cmdVerAnexa.Enabled = True
    End If
    Call Status("Listo.")
End Sub

Private Sub anxSeleccion()
    With grdAnexas
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, colNroInterno) <> "" Then
                anxNroPeticion = .TextMatrix(.RowSel, colNroInterno)
            End If
        End If
    End With
End Sub

Private Sub grdAnexas_Click()
    Call anxSeleccion
End Sub

'{ add -002- d.
Private Sub grdAnexas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdAnexas_Click
    End If
End Sub
'}

Private Sub memBeneficios_Change()
    bFlgBeneficios = True
End Sub

Private Sub memCaracteristicas_Change()
    bFlgCaracteristicas = True
End Sub

Private Sub memDescripcion_Change()
    bFlgDescripcion = True
End Sub

Private Sub memMotivos_Change()
    bFlgMotivos = True
End Sub

Private Sub memObservaciones_Change()
    bFlgObservaciones = True
End Sub

Private Sub memRelaciones_Change()
    bFlgRelaciones = True
End Sub

Private Function modifImportancia(oUsualta, oldPerf, nUsualta, newPerf) As Boolean
    modifImportancia = False
    If oldPerf = "" Then
        modifImportancia = True
    End If
    If newPerf = "ADMI" Or newPerf = "SADM" Then
        modifImportancia = True
    End If
    If oUsualta = nUsualta Then
        modifImportancia = True
    End If
    Select Case oldPerf
    Case "ADMI"
        modifImportancia = True
    Case "CGRU"
        modifImportancia = True
    Case "CSEC"
        If InStr("CGCI|CDIR", newPerf) > 0 Then
            modifImportancia = True
        End If
    Case "CGCI"
        If InStr("CDIR", newPerf) > 0 Then
            modifImportancia = True
        End If
    Case "CDIR"
        If newPerf = "CDIR" Then
            modifImportancia = True
        End If
    End Select
End Function

Private Function InGrupo() As Boolean
    ' Esta funci�n determina si seg�n el perfil actual del usuario, esta inclu�do en alg�n
    ' grupo de la petici�n (por ejemplo, el perfil del usuario actual es Resp. de Ejecuci�n:
    ' el xPerfNivel es 'GRUP' y trata de ver si alguno de los grupos coincide con el del recurso.
    InGrupo = False
    If Not sp_GetPeticionGrupo(glNumeroPeticion, Null, Null) Then Exit Function
    Do While Not aplRST.EOF
        If xPerfNivel = "BBVA" Or _
            xPerfNivel = "DIRE" And ClearNull(aplRST!cod_direccion) = xPerfDir Or _
            xPerfNivel = "GERE" And ClearNull(aplRST!cod_gerencia) = xPerfGer Or _
            xPerfNivel = "SECT" And ClearNull(aplRST!cod_sector) = xPerfSec Or _
            xPerfNivel = "GRUP" And ClearNull(aplRST!cod_grupo) = xPerfGru Then
                InGrupo = True
                Exit Do         ' add -023- a. Agregado como mejora: si ya determin� que existe en alguno de los grupos, no hace falta recorrer todos los grupos de la petici�n.
        End If
        aplRST.MoveNext
    Loop
    'aplRST.Close
End Function

Private Function InSector() As Boolean
    InSector = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If xPerfNivel = "BBVA" Or _
            xPerfNivel = "DIRE" And ClearNull(aplRST!cod_direccion) = xPerfDir Or _
            xPerfNivel = "GERE" And ClearNull(aplRST!cod_gerencia) = xPerfGer Or _
            xPerfNivel = "SECT" And ClearNull(aplRST!cod_sector) = xPerfSec Then
            InSector = True
        End If
        aplRST.MoveNext
    Loop
    'aplRST.Close
End Function

' Esta funci�n devuelve True cuando el Sector del usuario actual esta contenido
' en alguno de los sectores de la petici�n actual.
Private Function InEjecutor() As Boolean
    InEjecutor = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If ClearNull(aplRST!cod_sector) = glLOGIN_Sector Then
            InEjecutor = True
        End If
        aplRST.MoveNext
    Loop
    'aplRST.Close
End Function

'{ del -098- a.
''{ add -008- a.
'' Esta funci�n devuelve True cuando el Sector del usuario actual esta contenido
'' en alguno de los sectores de Organizaci�n de la petici�n actual.
'Private Function InEjecutorOrganizacion() As Boolean
'    InEjecutorOrganizacion = False
'    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
'    Do While Not aplRST.EOF
'        If ClearNull(aplRST!cod_sector) = glLOGIN_Sector And _
'            (ClearNull(aplRST!cod_gerencia) = "ORG." Or ClearNull(aplRST!cod_gerencia) = "TRYAPO") Then     ' upd -017- a. Se agrega TRYAPO
'                InEjecutorOrganizacion = True
'                Exit Do
'        End If
'        aplRST.MoveNext
'    Loop
'    'aplRST.Close
'End Function
'}

' El prop�sito de esta funci�n es devuelver un valor Booleano si el grupo o sector
' del usuario actual esta "relacionado" respecto del BP de la petici�n
Private Function EstaRelacionadoAlBP(Optional ByVal Grupo As String, _
                                     Optional ByVal Sector As String) As Boolean
    Dim xBPSect, xBPGrup, xBPGere, xBPDire As String
    
    EstaRelacionadoAlBP = False
    ' Obtengo el �rbol jerarquico donde se encuentra el BP de la petici�n actual
    If sp_GetRecurso(sBpar, "R") Then
        xBPGrup = ClearNull(aplRST!cod_grupo)
        xBPSect = ClearNull(aplRST!cod_sector)
        xBPGere = ClearNull(aplRST!cod_gerencia)
        xBPDire = ClearNull(aplRST!cod_direccion)
        aplRST.Close
    End If
    
    If Not IsMissing(Grupo) And Len(Grupo) > 0 Then
        If xBPGrup = Grupo Then
            EstaRelacionadoAlBP = True
        End If
    Else
        If Not IsMissing(Sector) And Len(Sector) > 0 Then
            ' Es Responsable de Sector
            If xBPSect = Sector And PerfilInternoActual = "CSEC" Then
                EstaRelacionadoAlBP = True
            End If
        End If
    End If
End Function
'}

'{ add -006- x.
' Esta funci�n determina lo siguiente:
' El usuario o recurso es "Ejecutor para modificar" si pertenece a alguno de los sectores de la petici�n
' y se corresponde al sector solicitante de la petici�n.
Private Function EsEjecutorParaModificar() As Boolean
    EsEjecutorParaModificar = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    Do While Not aplRST.EOF
        If ClearNull(aplRST!cod_sector) = glLOGIN_Sector Then
            If xSec = glLOGIN_Sector Then
                EsEjecutorParaModificar = True
                Exit Do     ' add -023- a. Mejora: se sale del loop en cuanto la funci�n es verdadera porque no tiene sentido seguir recorriendo el loop.
            End If
        End If
        aplRST.MoveNext
    Loop
    aplRST.Close
End Function
'}

Private Function esBP() As Boolean
    esBP = False
    ' Si el usuario actual actua como Business Partner...
    If PerfilInternoActual = "BPAR" Then
        ' Y adem�s es el Business Partner de la petici�n...
        If sBpar = Trim(glLOGIN_ID_REEMPLAZO) Then      ' add -001- h. - Se agrega la funci�n Trim a glLOGIN_ID_REEMPLAZO
            esBP = True
        Else
            'MsgBox ("CUIDADO")
            PerfilInternoActual = "VIEW"
        End If
    End If
End Function

Private Sub cmdRptHs_Click()
    If glCrystalNewVersion Then
        Call NuevoReporte
    Else
        Call ViejoReporte
    End If
End Sub

Private Sub NuevoReporte()
    Dim crApp As New CRAXDRT.Application
    Dim crReport As New CRAXDRT.Report
    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
    Dim sReportName As String
    
    sReportName = "\hstrapeta6.rpt"
    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
    With crReport
        .PaperSize = crPaperA4
        .PaperOrientation = crLandscape
        .ReportTitle = "Horas trabajadas por petici�n / tarea"
        .ReportComments = "Petici�n: " & ClearNull(txtNroAsignado.Text)
    End With
    
    Call FuncionesCrystal10.ConectarDB(crReport)
    On Error GoTo ErrHandler
 
    'Abrir el reporte
    Call Puntero(True)
    ' Parametros del reporte
    Set crParamDefs = crReport.ParameterFields
    For Each crParamDef In crParamDefs
        crParamDef.ClearCurrentValueAndRange
        Select Case crParamDef.ParameterFieldName
            Case "@fdesde": crParamDef.AddCurrentValue ("19000101")
            Case "@fhasta": crParamDef.AddCurrentValue ("20790606")
            Case "@tipo": crParamDef.AddCurrentValue ("PET")
            Case "@codigo": crParamDef.AddCurrentValue (CStr(glNumeroPeticion))
            Case "@detalle": crParamDef.AddCurrentValue (CStr("1"))
        End Select
    Next
   
    Set rptVisorRpt.crReport = crReport
    rptVisorRpt.CargarReporte
    rptVisorRpt.Show 1
    
    Call Puntero(False)
    Set crParamDefs = Nothing
    Set crParamDef = Nothing
    Set crApp = Nothing
    Set crReport = Nothing
Exit Sub
ErrHandler:
    If Err.Number = -2147206461 Then
        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
            vbCritical + vbOKOnly
    Else
        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
    End If
    Call Puntero(False)
End Sub

Private Sub ViejoReporte()
    Dim sTitulo As String

    If Not LockProceso(True) Then
        Exit Sub
    End If
    Call Puntero(True)
    With mdiPrincipal.CrystalReport1
        sTitulo = "Petici�n: " & ClearNull(txtNroAsignado.Text)
        .Destination = crptToWindow
        .ReportFileName = GetShortName(App.Path) & "\" & "hstrapeta.rpt"
        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
        .SelectionFormula = ""
        .RetrieveStoredProcParams
        .StoredProcParam(0) = "19000101"    'se fuerza la fecha
        .StoredProcParam(1) = "20790606"    ' add -058- a.
        .StoredProcParam(2) = "PET"
        .StoredProcParam(3) = "" & glNumeroPeticion
        .StoredProcParam(4) = 1             'se fuerza el nivel: recurso
        .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
        .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
        .Formulas(2) = "@0_TITULO='" & sTitulo & "'"
        .WindowLeft = 1
        .WindowTop = 1
        .WindowState = crptMaximized
        .Action = 1
        .Formulas(2) = ""
    End With
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Private Sub CargarDocMetod()
    docView.Enabled = False
    docDel.Enabled = False
    docMod.Enabled = False
    docAdd.Enabled = False

    With grdDocMetod
        .Visible = False
        .Clear
        .cols = 7
        '.HighLight = flexHighlightWithFocus
        .HighLight = flexHighlightAlways
        .Rows = 1
        .TextMatrix(0, colDOC_DUMMY) = "": .ColWidth(colDOC_DUMMY) = 0
        .TextMatrix(0, colDOC_ADJ_FILE) = "Archivo": .ColWidth(colDOC_ADJ_FILE) = 4500: .ColAlignment(colDOC_ADJ_FILE) = 0
        .TextMatrix(0, colDOC_ADJ_PATH) = "Path": .ColWidth(colDOC_ADJ_PATH) = 0
        .TextMatrix(0, colDOC_NOM_AUDIT) = "Usuario": .ColWidth(colDOC_NOM_AUDIT) = 2000
        .TextMatrix(0, colDOC_AUDIT_DATE) = "Fecha": .ColWidth(colDOC_AUDIT_DATE) = 980
        .TextMatrix(0, colDOC_ADJ_TEXTO) = "Comentario": .ColWidth(colDOC_ADJ_TEXTO) = 5000: .ColAlignment(colDOC_ADJ_TEXTO) = 0
        .TextMatrix(0, colDOC_FECHAFMT) = "yyyymmdd": .ColWidth(colDOC_FECHAFMT) = 0
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        CambiarEfectoLinea grdDocMetod, prmGridEffectFontBold
    End With

    Call Status("Cargando documentos vinculados...")
    If glNumeroPeticion <> "" Then
        If Not sp_GetAdjunto(glNumeroPeticion, Null) Then
            grdDocMetod.Visible = True
            GoTo finAdj
            Exit Sub
        End If
    Else
          Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdDocMetod
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, 1) = ClearNull(aplRST!adj_file)
            .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!adj_path)
            .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!nom_audit)
            .TextMatrix(.Rows - 1, 4) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST!adj_texto)
            .TextMatrix(.Rows - 1, 6) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "yyyymmdd"), "")   ' add -076- a.
        End With
        aplRST.MoveNext
        DoEvents
    Loop
finAdj:
    With grdDocMetod
        .FocusRect = flexFocusNone
        .Visible = True
    End With
    Call Status("Listo.")
    Call docSeleccion
End Sub

Private Sub docSeleccion()
    lblMensajeNoVinculados.Visible = False  ' add -029- c.
    Call setHabilCtrl(docFile, "DIS")
    Call setHabilCtrl(docPath, "DIS")
    Call setHabilCtrl(docComent, "DIS")
    
    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then   ' add -029- c.
        If flgEjecutor Then
           docAdd.Enabled = True
        End If
    Else
        lblMensajeNoVinculados.Visible = True
    End If
    
    With grdDocMetod
'        If .MouseRow = 0 And .Rows > 1 Then
'           .RowSel = 1
'           .col = .MouseCol
'           If .MouseCol = 4 Then .col = 6       ' add -076- a.
'           .Sort = flexSortStringNoCaseAscending
'        End If
        'If flgEjecutor Then
        '   docAdd.Enabled = True
        'End If
        If .RowSel > 0 Then
            '.HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 1) <> "" Then
                glPathDocMetod = .TextMatrix(.RowSel, 2) & .TextMatrix(.RowSel, 1)
                docFile = .TextMatrix(.RowSel, 1)
                docPath = .TextMatrix(.RowSel, 2)
                docComent = .TextMatrix(.RowSel, 5)
                docView.Enabled = True
                If flgEjecutor Then
                   docMod.Enabled = True
                   docDel.Enabled = True
                End If
            End If
        End If
    End With
End Sub

Private Sub grdDocMetod_Click()
    'Call docSeleccion
    bSorting = True
    Call Ordenamiento(grdDocMetod)
    bSorting = False
    Call docSeleccion
End Sub

'{ add -002- d.
Private Sub grdDocMetod_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDocMetod_Click
    End If
End Sub
'}

Private Sub grdDocMetod_DblClick()
    Call docSeleccion
    DoEvents
    If ClearNull(glPathDocMetod) = "" Then
        Exit Sub
    End If
    Call Puntero(True)
    AbrirAdjunto glPathDocMetod
    glPathDocMetod = ""                 ' add -012- a. - Se blanquea la variable una vez que se utiliza
    Call Puntero(False)
End Sub

Private Sub AbrirAdjunto(sFileName)
    'Dim lRetCode As Long
    Call EXEC_ShellExecute(Me.hwnd, sFileName)
    'lRetCode = ShellExecute(Me.HWnd, "open", xFilename, "", "C:\", 1)  ' "Unable to open the specified file. Either you do not have sufficient permissions to open the file for reading, or the file may be damaged." ??????
    'lRetCode = ShellExecute(Me.HWnd, "open", xFilename, "", "", 1)
    'If lRetCode <= 31 Then MsgBox "Error al abrir el documento"
End Sub

Private Sub docView_Click()
    If ClearNull(glPathDocMetod) = "" Then
        Exit Sub
    End If
    Call Puntero(True)
    Call AbrirAdjunto(glPathDocMetod)
    glPathDocMetod = ""                 ' add -012- a. - Se blanquea la variable una vez que se utiliza
    Call Puntero(False)
    'grdDocMetod.SetFocus
End Sub

'{ add -082- b.
'Private Sub docSel_Click()
Private Sub docAdd_Click()
    SeleccionarVincWindows
End Sub

Private Sub SeleccionarVincWindows()
    On Error GoTo Errores
    Dim i As Integer
    Dim sPath As String
    Dim cNombresDeArchivo As String
    
    ' Arma el path para los documentos de metodolog�a
    cNombresDeArchivo = ""
    If sp_GetVarios("PTHADJ_1") Then
        sPath = ClearNull(aplRST!var_texto)
    End If
    If sp_GetVarios("PTHADJ_2") Then
        sPath = sPath & ClearNull(aplRST!var_texto)
    End If
    If sp_GetVarios("PTHADJ_3") Then
        sPath = sPath & ClearNull(aplRST!var_texto)
    End If
    
    cMensajeError = ""
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = "Vincular a la petici�n..."
        '.Filter = "*.xls;*.doc;*.pps;*.mpp;*.ppt;*.rtf;*.pdf;*.zip;*.msg"
        '.Filter = "*.xls|*.doc|*.pps|*.mpp|*.ppt|*.rtf|*.pdf|*.zip|*.msg"
        .Filter = "Todos|*.*|*.xls|*.xls|*.doc|*.doc|*.rtf|*.rtf|*.pps|*.pps|*.mpp|*.mpp|*.ppt|*.ppt|*.tif|*.tif|*.bmp|*.bmp|*.jpg|*.jpg|*.htm|*.htm|*.html|*.html|*.txt|*.txt|*.pdf|*.pdf|*.zip|*.zip|*.msg|*.msg|*.rar|*.rar"
        .FilterIndex = 1
        .InitDir = sPath
        .Flags = cdlOFNAllowMultiselect + cdlOFNFileMustExist + cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        Call Puntero(True)
        cNombresDeArchivo = .FileName
        'If Len(cNombresDeArchivo) > 0 Then
            ' Determino cuantos archivos seleccion� y los guardo en el vector
            ArmarVectorDeArchivos cNombresDeArchivo, vbNullChar
        'Else
        '    GoTo Fin
        'End If
    End With
    If ValidarArchivosVinculados Then
        GuardarArchivosVinculados
        CargarDocMetod
    Else
        MsgBox cMensajeError, vbExclamation + vbOKOnly, "Problemas encontrados"
    End If
Fin:
    Call Puntero(False)
    Call docHabBtn(0, "X")
    Exit Sub
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
    End Select
End Sub
'}

Private Sub docCan_Click()
    docFile = "": docPath = "": docComent = ""
    Call docHabBtn(0, "X")
End Sub

Private Sub docCon_Click()
    If ClearNull(docFile.Text) = "" Or ClearNull(docPath.Text) = "" Then
        docFile = "": docPath = "": docComent = ""
        MsgBox ("No se ha seleccionado archivo.")
        Exit Sub
    End If
    Select Case docOpcion
        Case "A"
            If sp_InsertAdjunto(glNumeroPeticion, docFile.Text, docPath.Text, docComent.Text) Then
                Call sp_AddHistorial(glNumeroPeticion, "VINCADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, docFile.Text) ' add -042- a.
                Call docHabBtn(0, "X")
            End If
        Case "M"
            If sp_UpdateAdjunto(glNumeroPeticion, docFile.Text, docComent.Text) Then
                Call sp_AddHistorial(glNumeroPeticion, "VINCUPD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, docFile.Text) ' add -042- a.
                With grdDocMetod
                    If .RowSel > 0 Then
                       .TextMatrix(.RowSel, 5) = docComent.Text
                    End If
                End With
                Call docHabBtn(0, "X", False)
            End If
        Case "E"
            If sp_DeleteAdjunto(glNumeroPeticion, docFile.Text) Then
                Call sp_AddHistorial(glNumeroPeticion, "VINCDEL", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, docFile.Text) ' add -042- a.
                docFile = "": docPath = "": docComent = ""
                Call docHabBtn(0, "X")
            End If
        Case Else
            docFile = "": docPath = "": docComent = ""
            Call docHabBtn(0, "X")
    End Select
End Sub

Private Sub docDel_Click()
    Call docHabBtn(1, "E")
End Sub

Private Sub docMod_Click()
    Call docHabBtn(1, "M")
End Sub

'Private Sub docAdd_Click()
'    Call docHabBtn(1, "A")
'    Call docSel_Click
'End Sub

Sub docHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjADJ).Enabled = True
            orjBtn(orjCNF).Enabled = True
            orjBtn(orjEST).Enabled = True
            lblDocMetod = "": lblDocMetod.Visible = False
            grdDocMetod.Enabled = True
            grdDocMetod.SetFocus
            docView.Enabled = False
            docAdd.Enabled = False
            If flgEjecutor Then
               docAdd.Enabled = True
            End If
            docMod.Enabled = False
            docDel.Enabled = False
            docCon.Enabled = False
            docCan.Enabled = False
            'docSel.Enabled = False     ' del -082- b.
            'sOpcionSeleccionada = ""
            docComent.Locked = True

            If IsMissing(vCargaGrid) Then
                Call CargarDocMetod
            Else
                Call docSeleccion
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjADJ).Enabled = False
            orjBtn(orjCNF).Enabled = False
            orjBtn(orjEST).Enabled = False
            fraButtPpal.Enabled = False

            grdDocMetod.Enabled = False
            docAdd.Enabled = False
            docMod.Enabled = False
            docDel.Enabled = False
            docCon.Enabled = True
            docCan.Enabled = True
            Select Case sOpcionSeleccionada
                Case "A"
                    'docSel.Enabled = True      ' del -082- b.
                    lblDocMetod = " AGREGAR ": lblDocMetod.Visible = True
                    docFile = "": docPath = "": docComent = ""
                    docComent.Locked = False
                    fraDocMet.Enabled = True
                    'Call setHabilCtrl(docComent, "NOR")
                Case "M"
                    lblDocMetod = " MODIFICAR ": lblDocMetod.Visible = True
                    fraDocMet.Enabled = True
                    docComent.Locked = False
                    Call setHabilCtrl(docComent, "NOR")
                    docComent.SetFocus
                Case "E"
                    lblDocMetod = " DESVINCULAR ": lblDocMetod.Visible = True
                    fraDocMet.Enabled = False
            End Select
    End Select
    docOpcion = sOpcionSeleccionada
End Sub

'{ add -082- a. Seleccionar archivos para ser adjuntados a la petici�n actual
'Private Sub adjAdd_Click()
'    If cModeloControl = MODELO_CONTROL_NUEVO Then
'        If glPETClase = "SINC" Then
'            MsgBox "Antes de poder adjuntar un documento a la petici�n, �sta debe ser clasificada.", vbInformation + vbOKOnly, "Clasificaci�n requerida"
'            Exit Sub
'        Else
'            'Call adjHabBtn(1, "A")
'            'Call adjSel_Click
'            SeleccionarAdjuntos
'        End If
'    Else
'        'Call adjHabBtn(1, "A")
'        'Call adjSel_Click
'        SeleccionarAdjuntos
'    End If
'End Sub
'}

Private Sub adjAdd_Click()
    If glPETClase = "SINC" Then
        MsgBox "Antes de poder adjuntar un documento a la petici�n, �sta debe ser clasificada.", vbInformation + vbOKOnly, "Clasificaci�n requerida"
        Exit Sub
    Else
        SeleccionarAdjuntos
    End If
End Sub

Private Sub SeleccionarAdjuntos()
    On Error GoTo Errores
    Dim i As Integer
    Dim sPath As String
    Dim cNombresDeArchivo As String
    
    sPath = GetSetting("GesPet", "Export01", "chkAdjuntoLPO", "C:\")
    If Dir(sPath, vbDirectory) = "" Then
        SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", "P:\"
        sPath = GetSetting("GesPet", "Export01", "chkAdjuntoLPO", "P:\")
    End If
    cMensajeError = ""
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = "Adjuntar a la petici�n..."
        .Filter = "Todos|*.*|*.xls|*.xls|*.doc|*.doc|*.rtf|*.rtf|*.pps|*.pps|*.mpp|*.mpp|*.ppt|*.ppt|*.tif|*.tif|*.bmp|*.bmp|*.jpg|*.jpg|*.htm|*.htm|*.html|*.html|*.txt|*.txt|*.pdf|*.pdf|*.zip|*.zip|*.msg|*.msg|*.rar|*.rar"
        .FilterIndex = 1
        '.InitDir = IIf(Len(Dir(.InitDir)) > 0, .InitDir, glWRKDIR)     ' del -086- a.
        .InitDir = IIf(glLSTDIR = "", glWRKDIR, glLSTDIR)               ' add -086- a.
        .Flags = cdlOFNAllowMultiselect + cdlOFNFileMustExist + cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        Call Puntero(True)
        cNombresDeArchivo = .FileName
        glLSTDIR = ObtenerDirectorio(.FileName) & "\"                   ' add -086- a.
        ArmarVectorDeArchivos cNombresDeArchivo, vbNullChar
    End With
    If ValidarArchivosAdjuntos Then
        GuardarArchivos
        CargarAdjPet
    Else
        MsgBox cMensajeError, vbExclamation + vbOKOnly, "Problemas encontrados"
    End If
Fin:
    Call Puntero(False)
    Call adjHabBtn(0, "X")
    Exit Sub
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
        Case Else
            MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    End Select
End Sub

Private Sub ArmarVectorDeArchivos(cLista As String, cSeparator As String)
    ' Recibe una cadena con los elementos de una lista para armar el vector general vElemento
    ' discriminando que solos sean archivos reales los que conformen el vector
    Dim i As Long
    Dim j As Long
    Dim Pos As Long
    Dim cPath As String
    
    ' Determina el path para todos los archivos seleccionados
    If InStr(1, cLista, cSeparator, vbTextCompare) > 0 Then
        cPath = Mid(cLista, 1, InStr(1, cLista, cSeparator, vbTextCompare) - 1)
        cPath = IIf(Right(cPath, 1) = "\", cPath & "", cPath & "\")
        cLista = Mid(cLista, InStr(1, cLista, cSeparator, vbTextCompare) + 1)
    Else
        Pos = 1: j = Pos
        Do While Pos > 0
            Pos = InStr(j, cLista, "\", vbTextCompare)
            If Pos > 0 Then
                j = Pos + 1
            Else
                j = j - 1
            End If
        Loop
        cPath = Mid(cLista, 1, j)
        cLista = Mid(cLista, j + 1)
    End If

    Erase cArchivos             ' Inicializa el vector
    Do While Len(cLista) > 0
        ReDim Preserve cArchivos(i)
        If InStr(1, cLista, cSeparator, vbTextCompare) > 0 Then
            cArchivos(i).Nombre = Mid(cLista, 1, InStr(1, cLista, cSeparator, vbTextCompare) - 1)
            cArchivos(i).tipo = TipoDeAdjunto(cArchivos(i).Nombre)
            cArchivos(i).Extension = ExtensionDelAdjunto(cArchivos(i).Nombre)
            cArchivos(i).Tamanio = FileLen(cPath & cArchivos(i).Nombre)
            cArchivos(i).Ubicacion = cPath
            cLista = Mid(cLista, InStr(1, cLista, cSeparator, vbTextCompare) + 1)
        Else
            cArchivos(i).Nombre = Mid(cLista, 1, Len(cLista))
            cArchivos(i).tipo = TipoDeAdjunto(cArchivos(i).Nombre)
            cArchivos(i).Extension = ExtensionDelAdjunto(cArchivos(i).Nombre)
            cArchivos(i).Tamanio = FileLen(cPath & cArchivos(i).Nombre)
            cArchivos(i).Ubicacion = cPath
            cLista = ""
        End If
        i = i + 1
        DoEvents
    Loop
End Sub

Private Function ExtensionDelAdjunto(cArchivo As String) As String
    Dim i As Long
    Dim lPos As Long
    Dim cAux As String
    
    For i = Len(cArchivo) To 1 Step -1
        cAux = cAux & Mid(cArchivo, i, 1)
    Next i
    If InStr(1, cAux, ".", vbTextCompare) > 0 Then
        lPos = Len(cArchivo) - InStr(1, cAux, ".", vbTextCompare) + 1
        ExtensionDelAdjunto = Mid(cArchivo, lPos + 1)
    Else
        ExtensionDelAdjunto = ""
    End If
End Function

Private Function TipoDeAdjunto(cArchivo As String) As String
    Dim i As Long
    Dim v_cod_doc() As String
    
    TipoDeAdjunto = "OTRO"      ' Por defecto
    For i = 0 To UBound(vTipoDocumento)
        If InStr(1, cArchivo, vTipoDocumento(i).codigo, vbTextCompare) > 0 Then
            TipoDeAdjunto = vTipoDocumento(i).codigo
            Exit For
        End If
    Next i
    If TipoDeAdjunto = "OTRO" Then
        'If InStr(1, cArchivo, "IDH", vbTextCompare) > 0 Then   ' del -085- a.
        If InStr(1, cArchivo, "IH", vbTextCompare) > 0 Then     ' add -085- a.
            If InStr(1, UCase(cArchivo), "SOB", vbTextCompare) > 0 Then
                TipoDeAdjunto = "IDH1"
            ElseIf InStr(1, UCase(cArchivo), "OME", vbTextCompare) > 0 Then
                TipoDeAdjunto = "IDH2"
            ElseIf InStr(1, UCase(cArchivo), "OMA", vbTextCompare) > 0 Then
                TipoDeAdjunto = "IDH3"
            End If
            'For i = 0 To UBound(vTipoDocumento)
            '    If InStr(1, cArchivo, Mid(vTipoDocumento(i).codigo, 1, 3), vbTextCompare) > 0 Then
            '        TipoDeAdjunto = vTipoDocumento(i).codigo
            '        Exit For
            '    End If
            'Next i
        End If
    End If
End Function

Private Sub GuardarArchivos()
    Dim i As Integer
    Dim bRespuestaControl As Byte
    
    Call Status("Adjuntando... aguarde, por favor...")
    Call Puntero(True)
        
    bRespuestaControl = 0           ' add -006- a. - Se inicializa antes de comenzar el ciclo (indeterminado)
    
    ' Antes que nada averiguo si se va a adjuntar un documento de alcance.
    ' Si es as�, pregunto si continuo a pesar de existir un conforme a a la documentaci�n de alcance.
    For i = 0 To UBound(cArchivos)
        If InStr(1, "P950|C100|ALCA|", cArchivos(i).tipo, vbTextCompare) > 0 Then   ' upd -007- a. Se cambia CG04 por ALCA
            If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
                If MsgBox("La petici�n tiene ingresado un conforme final al documento de alcance." & vbCrLf & _
                          "Si continua, ser� eliminado dicho conforme y deber� gestionar la obtenci�n de uno nuevo." & vbCrLf & _
                          "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Agregado de documentaci�n de Alcance") = vbYes Then
                    ' Elimina el conforme de alcance final de la petici�n
                    Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
                    Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "ALT", "1")    ' add -077- a.
                    bRespuestaControl = 1       ' Acuerdo con el usuario en adjuntar y eliminar el conforme
                Else
                    bRespuestaControl = 2       ' No hay acuerdo. Se cancela el adjunto de documentos y se preserva el conforme
                End If
                Exit For
            End If
        End If
    Next i
    If bRespuestaControl <> 2 Then
        For i = 0 To UBound(cArchivos)
            If InStr(1, "P950|C100|ALCA|", cArchivos(i).tipo, vbTextCompare) > 0 Then
                Select Case cArchivos(i).tipo
                    Case "P950": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 44, "Nombre del documento: " & Trim(cArchivos(i).Nombre))
                    Case "C100": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 45, "Nombre del documento: " & Trim(cArchivos(i).Nombre))
                    Case "ALCA": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 46, Trim(cArchivos(i).Nombre))
                End Select
            End If
            ' Agrega al historial el mensaje de agregado del nuevo documento
            '{ add -084- a. Comprimir antes de subir
            'If chkZipFileWhileLoad.Value = 1 Then
            '    If ComprimirArchivo(cArchivos(i).Ubicacion & cArchivos(i).Nombre) Then
            '        cArchivos(i).Nombre = cArchivos(i).Nombre & ".zip"
            '    End If
            'End If
            '}
            If sp_InsertPeticionAdjunto(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo, cArchivos(i).tipo, cArchivos(i).Ubicacion, cArchivos(i).Nombre, "") Then
                Call sp_AddHistorial(glNumeroPeticion, "ADJADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, cArchivos(i).tipo & "-->" & cArchivos(i).Nombre)
                '{ add -new- 2011.01.28
                If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then    ' 1. Primero determina si ya existe para la petici�n el grupo Homologador
                    Call CargarGrupoHomologador                             ' Obtengo los datos del grupo homologador
                    If InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(cGrupoHomologadorEstado)) = 0 Then     ' Determina si el grupo homologador se encuentra en alguno de los estados "Activos"
                        Select Case cArchivos(i).tipo
                            Case "C100": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 45, sEstado, Null)
                            Case "P950": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 44, sEstado, Null)
                            Case "CG04": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 46, sEstado, Null)
                            Case "C204": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 47, sEstado, Null)
                            Case "EML1": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 48, sEstado, Null)
                            Case "EML2": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 49, sEstado, Null)
                            Case "T710": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 50, sEstado, Null)
                        End Select
                    End If
                End If
            End If
        Next i
    End If
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Sub GuardarArchivosVinculados()
    Dim i As Integer
    
    Call Status("Vinculando... aguarde, por favor...")
    Call Puntero(True)
        
    For i = 0 To UBound(cArchivos)
        If sp_InsertAdjunto(glNumeroPeticion, cArchivos(i).Nombre, cArchivos(i).Ubicacion, "") Then     ' Agrega al historial el mensaje de vinculaci�n del nuevo archivo
            Call sp_AddHistorial(glNumeroPeticion, "VINCADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "")
        End If
    Next i
    Call docHabBtn(0, "X")
    Call Puntero(False)
    Call Status("Listo.")
End Sub

Private Function ValidarArchivosAdjuntos() As Boolean
    Dim i As Integer
    Dim SI_cod_sector As String, SI_cod_grupo As String     ' add -093- a.
    
    ValidarArchivosAdjuntos = True

    ' Comprobaci�n de tama�o m�ximo
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).Tamanio > ARCHIVOS_ADJUNTOS_MAXSIZE Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " supera el tama�o m�ximo permitido por la aplicaci�n."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " supera el tama�o m�ximo permitido por la aplicaci�n."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de tama�o m�nimo
    For i = 0 To UBound(cArchivos)
        If cArchivos(i).Tamanio < 1 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " est� vacio. No puede ser adjuntado."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " est� vacio. No puede ser adjuntado."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de largo del nombre del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Nombre) > 100 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El nombre del archivo " & cArchivos(i).Nombre & " excede los 100 caracteres. No puede ser adjuntado."
            Else
                cMensajeError = "El nombre del archivo " & cArchivos(i).Nombre & " excede los 100 caracteres. No puede ser adjuntado."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de largo del path del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Ubicacion) > 250 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser adjuntado."
            Else
                cMensajeError = "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser adjuntado."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de or�gen
    For i = 0 To UBound(cArchivos)
        If Dir(cArchivos(i).Ubicacion & cArchivos(i).Nombre) = "" Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            End If
            ValidarArchivosAdjuntos = False
            Exit For
        End If
    Next i
    
    ' NUEVO
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de destino (control de duplicidad)
    For i = 0 To UBound(cArchivos)
        If sp_GetAdjuntosPet(glNumeroPeticion, cArchivos(i).tipo, Null, Null) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!adj_file) = cArchivos(i).Nombre Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & "El archivo " & cArchivos(i).Nombre & " ya existe adjuntado a esta petici�n."
                    Else
                        cMensajeError = "El archivo " & cArchivos(i).Nombre & " ya existe adjuntado a esta petici�n."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit Do
                Else
                    aplRST.MoveNext
                End If
                DoEvents
            Loop
        End If
    Next i
    
    ' NUEVO (28.06.2011)
    ' Comprobaci�n de la existencia de extensi�n para el archivo
    For i = 0 To UBound(cArchivos)
        If Trim(cArchivos(i).Extension) = "" Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & "El archivo " & Chr(34) & cArchivos(i).Nombre & Chr(34) & " no tiene extensi�n."
            Else
                cMensajeError = "El archivo " & Chr(34) & cArchivos(i).Nombre & Chr(34) & " no tiene extensi�n."
            End If
            ValidarArchivosAdjuntos = False
        End If
    Next i
        
    'If glPETModelo Then
    ' Control de documentos de alcance
    If cModeloControl = MODELO_CONTROL_NUEVO Then
        For i = 0 To UBound(cArchivos)
            If InStr(1, "P950|C100|", cArchivos(i).tipo, vbTextCompare) > 0 Then
                If glPETTipo = "PRJ" Or glPETImpacto = "S" Then
                    If cArchivos(i).tipo <> "C100" Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Por el tipo y/o clase, la petici�n debe informar el documento de alcance tipo C100."
                        Else
                            cMensajeError = "Por el tipo y/o clase, la petici�n debe informar el documento de alcance tipo C100."
                        End If
                        ValidarArchivosAdjuntos = False
                        Exit For
                    End If
                Else
                    If InStr(1, "EVOL|OPTI|NUEV", glPETClase, vbTextCompare) > 0 Then
                        If cArchivos(i).tipo <> "P950" Then
                            If Len(cMensajeError) > 0 Then
                                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Por el tipo y/o clase de la petici�n, el documento de alcance requerido para la misma es de tipo P950."
                            Else
                                cMensajeError = "Por el tipo y/o clase de la petici�n, el documento de alcance requerido para la misma es de tipo P950."
                            End If
                            ValidarArchivosAdjuntos = False
                            Exit For
                        End If
                    Else
                        ' Si por la clase no corresponde documento de alcance, entonces solo si el
                        ' usuario intenta agregar un C100 lo detengo
                        If cArchivos(i).tipo = "C100" Then
                            If Len(cMensajeError) > 0 Then
                                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "La petici�n, por su tipo y/o clase, no requiere documento de alcance," & vbCrLf & "pero si desea agregar uno, debe ser de tipo P950."
                            Else
                                cMensajeError = "La petici�n, por su tipo y/o clase, no requiere documento de alcance," & vbCrLf & "pero si desea agregar uno, debe ser de tipo P950."
                            End If
                            ValidarArchivosAdjuntos = False
                            Exit For
                        End If
                    End If
                End If
            End If
        Next i
        ' Control de documento CG04: Pruebas del Usuario
        For i = 0 To UBound(cArchivos)
            If InStr(1, "CG04", cArchivos(i).tipo, vbTextCompare) > 0 Then
                If sEstado <> "EJECUC" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo puede adjuntar un CG04 cuando la petici�n de encuentra en estado EN EJECUCION."
                    Else
                        cMensajeError = "Solo puede adjuntar un CG04 cuando la petici�n de encuentra en estado EN EJECUCION."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit For
                Else
                    ' Evaluo si ya existe al menos un documento de alcance anterior
                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) And _
                       Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                            If Len(cMensajeError) > 0 Then
                                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo puede adjuntar un CG04 cuando la petici�n ya posee alg�n documento de alcance."
                            Else
                                cMensajeError = "Solo puede adjuntar un CG04 cuando la petici�n ya posee alg�n documento de alcance."
                            End If
                            ValidarArchivosAdjuntos = False
                            Exit For
                    End If
                End If
            End If
        Next i
        
        ' Otro control de documento de Alcance
        For i = 0 To UBound(cArchivos)
            If glUsrPerfilActual <> "SADM" And glLOGIN_Direccion <> "MEDIO" Then
                If cArchivos(i).tipo = "P950" Or _
                   cArchivos(i).tipo = "C100" Then
                        If Len(cMensajeError) > 0 Then
                            cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Un Documento de Alcance solo puede ser adjuntado por usuarios de la direcci�n de Medios."
                        Else
                            cMensajeError = "Un Documento de Alcance solo puede ser adjuntado por usuarios de la direcci�n de Medios."
                        End If
                        ValidarArchivosAdjuntos = False
                        Exit For
                End If
            End If
        Next i
        
        For i = 0 To UBound(cArchivos)
            If cArchivos(i).tipo = "T710" Then
                If glUsrPerfilActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Un documento Principios de Desarrollo Aplicativo (T710), solo puede ser adjuntado por usuarios de DyD."
                    Else
                        cMensajeError = "Un documento Principios de Desarrollo Aplicativo (T710), solo puede ser adjuntado por usuarios de DyD."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit For
                End If
            End If
        Next i
        
        For i = 0 To UBound(cArchivos)
            If cArchivos(i).tipo <> "OTRO" And cArchivos(i).tipo <> "EML1" And cArchivos(i).tipo <> "EML2" And Left(cArchivos(i).tipo, 3) <> "IDH" And Left(cArchivos(i).tipo, 3) <> "IHB" And Left(cArchivos(i).tipo, 3) <> "IHC" Then
                ' Control de c�digo en el documento a adjuntar
                If InStr(1, Trim(cArchivos(i).Nombre), cArchivos(i).tipo, vbTextCompare) = 0 Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El documento a adjuntar debe contener la clave " & cArchivos(i).tipo & " dentro del nombre del archivo."
                    Else
                        cMensajeError = "El documento a adjuntar debe contener la clave " & cArchivos(i).tipo & " dentro del nombre del archivo."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit For
                End If
            Else
                ' Si se encuentra la clave de alguno de los Informes de Homologaci�n...
                If InStr(1, "IDH|IHB|IHC", Left(cArchivos(i).tipo, 3), vbTextCompare) > 0 Then
                    ' Valido que solo miembros del equipo de homologaci�n puedan adjuntarlos
                    If glUsrPerfilActual <> "SADM" Then
                        If Not EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                            If Len(cMensajeError) > 0 Then
                                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "Solo miembros del grupo Homologador pueden adjuntar Informes de Homologaci�n."
                            Else
                                cMensajeError = "Solo miembros del grupo Homologador pueden adjuntar Informes de Homologaci�n."
                            End If
                            ValidarArchivosAdjuntos = False
                            Exit For
                        End If
                    End If
                End If
            End If
        Next i
'        '{ add -093- b.
'        If sp_GetVarios("SI_SECT") Then SI_cod_sector = ClearNull(aplRST.Fields!var_texto)
'        If sp_GetVarios("SI_GRUP") Then SI_cod_grupo = ClearNull(aplRST.Fields!var_texto)
'        For i = 0 To UBound(cArchivos)
'            If InStr(1, "C104", Left(cArchivos(i).Tipo, 4), vbTextCompare) > 0 Then
'                If Not (glLOGIN_Sector = SI_cod_sector Or glLOGIN_Grupo = SI_cod_grupo) Then
'                    If Len(cMensajeError) > 0 Then
'                        cMensajeError = cMensajeError & vbCrLf & "Un documento C104 - Dictamen de seguridad, solo puede ser adjuntado por" & vbCrLf & "usuarios pertenecientes al sector y/o grupo de Seguridad Inform�tica."
'                    Else
'                        cMensajeError = "Un documento C104 - Dictamen de seguridad, solo puede ser adjuntado por" & vbCrLf & "usuarios pertenecientes al sector y/o grupo de Seguridad Inform�tica."
'                    End If
'                    ValidarArchivosAdjuntos = False
'                    Exit For
'                End If
'            End If
'        Next i
'        '}
    Else    ' Controles NO SOx
        ' Control de documento de Alcance: modelo anterior
        For i = 0 To UBound(cArchivos)
            If glUsrPerfilActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                If cArchivos(i).tipo = "ALCA" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & "Un Documento de Alcance solo puede ser adjuntado por usuarios de DyD."
                    Else
                        cMensajeError = "Un Documento de Alcance solo puede ser adjuntado por usuarios de DyD."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit For
                End If
            End If
        Next i
        ' Controles sobre Plan de Implantaci�n (viejo modelo de control)
        For i = 0 To UBound(cArchivos)
            If cArchivos(i).tipo = "PLIM" Then
                If glUsrPerfilActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                    If Len(cMensajeError) > 0 Then
                        cMensajeError = cMensajeError & vbCrLf & "Un documento Plan de Implantaci�n (PLIM), solo puede ser adjuntado por usuarios de DyD."
                    Else
                        cMensajeError = "Un documento Plan de Implantaci�n (PLIM), solo puede ser adjuntado por usuarios de DyD."
                    End If
                    ValidarArchivosAdjuntos = False
                    Exit For
                End If
            End If
        Next i
    End If
    If Len(cMensajeError) > 0 Then
        ValidarArchivosAdjuntos = False
    End If
End Function
'}

'{ add -082- b.
Private Function ValidarArchivosVinculados() As Boolean
    Dim i As Integer
    
    ValidarArchivosVinculados = True

    ' Comprobaci�n de largo del nombre del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Nombre) > 50 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El nombre del archivo " & cArchivos(i).Nombre & " excede los 50 caracteres. No puede ser vinculado."
            Else
                cMensajeError = "El nombre del archivo " & cArchivos(i).Nombre & " excede los 50 caracteres. No puede ser vinculado."
            End If
            ValidarArchivosVinculados = False
            Exit For
        End If
    Next i
    ' Comprobaci�n de largo del path del archivo
    For i = 0 To UBound(cArchivos)
        If Len(cArchivos(i).Ubicacion) > 250 Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser vinculado."
            Else
                cMensajeError = "El path del archivo " & cArchivos(i).Nombre & " excede los 250 caracteres. No puede ser vinculado."
            End If
            ValidarArchivosVinculados = False
            Exit For
        End If
    Next i
    ' Comprobaci�n de la existencia del archivo en ubicaci�n de or�gen
    For i = 0 To UBound(cArchivos)
        If Dir(cArchivos(i).Ubicacion & cArchivos(i).Nombre) = "" Then
            If Len(cMensajeError) > 0 Then
                cMensajeError = cMensajeError & vbCrLf & vbCrLf & "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            Else
                cMensajeError = "El archivo " & cArchivos(i).Nombre & " no existe en la ubicaci�n seleccionada."
            End If
            ValidarArchivosVinculados = False
            Exit For
        End If
    Next i
    If Len(cMensajeError) > 0 Then
        ValidarArchivosVinculados = False
    End If
End Function
'}

' Acci�n de modificar un documento adjunto
Private Sub adjMod_Click()
    Call adjHabBtn(1, "M")
End Sub

' Desadjuntar un archivo adjunto
Private Sub adjDel_Click()
    Call adjHabBtn(1, "E")
End Sub

' Cancelar la acci�n de adjuntar un documento
Private Sub adjCan_Click()
    adjFile = "": adjPath = "": adjComent = ""
    '{ add -025- a.
    txtAdjSector.Text = ""
    txtAdjGrupo.Text = ""
    txtAdjSize.Text = ""
    '}
    ' ESTO APARENTEMENTE NO VA
    Call adjHabBtn(0, "X")
End Sub

' Confirmar el adjuntado de un documento
Private Sub adjCon_Click()
    '{ add -013- a.
    Dim cIdentificadorDocumento As String
    Dim cExtensionOriginal As String
    'Dim cGrupoHomologador As String     ' add -031- a.
    '}
    Dim cAdjuntoTipo As String      ' add -022- a.
    '{ add -100- a.
    Dim bInformeHomologacion As Boolean
    Dim bExisteALCA As Boolean
    Dim bExisteALCP As Boolean
    '}

    If ClearNull(adjFile.Text) = "" Then
        adjFile = "": adjPath = "": adjComent = ""
        MsgBox "No se ha seleccionado ning�n archivo.", vbExclamation + vbOKOnly
        Exit Sub
    End If
    
    '{ add -031- a.
    If adjOpcion = "A" Or adjOpcion = "M" Then
        If cboAdjClass.ListIndex < 0 Then
            MsgBox "Falta especificar tipo de archivo.", vbOKOnly + vbInformation
            Exit Sub
        End If
        cAdjuntoTipo = CodigoCombo(cboAdjClass, False)  ' add -022- a.
    End If
    
    ' Informes de Homologaci�n: Control para recursos que NO pertenecen al grupo Homologador.
    If adjOpcion = "A" Or adjOpcion = "M" Then
        If Not EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
            If InStr(1, "IDH|IHB|IHC|", adjFile.Text, vbTextCompare) > 0 Or _
                InStr(1, "IDH1|IDH2|IDH3|", cAdjuntoTipo, vbTextCompare) > 0 Then
                    MsgBox "Solo usuarios pertenecientes al grupo Homologador" & vbCrLf & "pueden adjuntar informes de Homologaci�n.", vbOKOnly + vbExclamation
                    Exit Sub
            End If
        End If
    End If
    '}
    ' Control de documento de alcance
    If adjOpcion = "A" Then
        If InStr(1, "P950|C100|", cAdjuntoTipo, vbTextCompare) > 0 Then
            If CodigoCombo(cboTipopet, True) = "PRJ" Or cboImpacto = "Si" Then
                If cAdjuntoTipo <> "C100" Then
                    MsgBox "Por el tipo/clase de petici�n, debe informar el documento de alcance tipo C100.", vbExclamation + vbOKOnly, "Documento de alcance"
                    Exit Sub
                End If
            Else
                If InStr(1, "EVOL|OPTI|NUEV|", CodigoCombo(cboClase, True)) > 0 Then
                    If cAdjuntoTipo <> "P950" Then
                        MsgBox "Por el tipo/clase de petici�n, debe informar el documento de alcance tipo P950.", vbExclamation + vbOKOnly, "Documento de alcance"
                        Exit Sub
                    End If
                Else
                    ' Si por la clase no corresponde documento de alcance, entonces solo si el usuario intenta agregar un C100 lo detengo
                    If cAdjuntoTipo = "C100" Then
                        MsgBox "El tipo/clase de la petici�n no requiere documento de alcance, pero si desea agregar uno, debe ser de tipo P950.", vbExclamation + vbOKOnly, "Documento de alcance"
                        Exit Sub
                    End If
                End If
            End If
        End If
        '{ add -039- c. Control de subida de CG04
        If InStr(1, "CG04", cAdjuntoTipo, vbTextCompare) > 0 Then
            If sEstado <> "EJECUC" Then
                MsgBox "Solo puede adjuntar un CG04 cuando la petici�n de encuentra EN EJECUCION.", vbExclamation + vbOKOnly, "Documento de cambio alcance"
                Exit Sub
            Else
                ' Evaluo si ya existe al menos un documento de alcance anterior
                If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) And _
                   Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                        MsgBox "Solo puede adjuntar un CG04 cuando la petici�n ya posee un documento de alcance.", vbExclamation + vbOKOnly, "Documento de cambio alcance"
                        Exit Sub
                End If
            End If
        End If
        '}
    End If
    '}
    '{ add -008- a.
    If adjOpcion = "A" Or adjOpcion = "M" Then
        If Not cModeloControl = MODELO_CONTROL_NUEVO Then
            If PerfilInternoActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                If cAdjuntoTipo = "ALCA" Then     ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
                    MsgBox "Un 'Doc. de alcance desarrollo'," & Chr(13) & "solo puede adjuntarse" & Chr(13) & "por usuarios de DyD.", vbOKOnly + vbInformation
                    Exit Sub
                End If
            End If
        Else
            If PerfilInternoActual <> "SADM" And glLOGIN_Direccion <> "MEDIO" Then
                If cAdjuntoTipo = "P950" Or _
                   cAdjuntoTipo = "C100" Then
                    MsgBox "Un 'Doc. de alcance desarrollo'," & Chr(13) & "solo puede adjuntarse" & Chr(13) & "por usuarios de la direcci�n de Medios.", vbOKOnly + vbInformation
                    Exit Sub
                End If
            End If
        End If
        '{ add -028- a.
        If cAdjuntoTipo = "T710" Or cAdjuntoTipo = "PLIM" Then
            If PerfilInternoActual <> "SADM" And glLOGIN_Gerencia <> "DESA" Then
                MsgBox "Un documento Principios de Desarrollo Aplicativo (T710)" & vbCrLf & "solo puede ser adjuntado por usuarios de DyD.", vbOKOnly + vbInformation
                Exit Sub
            End If
        End If
        '{ add -004- a. - Viene de [mov -004-]
        If InStr(1, "OTRO|EML1|EML2|", cAdjuntoTipo, vbTextCompare) = 0 And _
            InStr(1, "IDH|IHB|IHC|", Left(cAdjuntoTipo, 3), vbTextCompare) = 0 Then
            '{ add -002- g. - Verifica que exista el c�digo de documento seleccionado en la cadena del nombre del archivo
            If InStr(1, Trim(adjFile.Text), cAdjuntoTipo, vbTextCompare) = 0 Then
                MsgBox "El documento a adjuntar debe contener" & vbCrLf & "la clave " & Trim(UCase(CodigoCombo(cboAdjClass, False))) & " en el nombre del archivo.", vbOKOnly + vbExclamation
                Exit Sub
            End If
            '}
        Else
            ' Si se encuentra la clave de alguno de los Informes de Homologaci�n...
            If InStr(1, "IDH|IHB|IHC|", Left(cAdjuntoTipo, 3), vbTextCompare) > 0 Then
                If InStr(1, Trim(adjFile.Text), Left(cAdjuntoTipo, 3), vbTextCompare) = 0 Then
                    MsgBox "Advertencia: Es posible que el documento que est� adjuntando" & vbCrLf & "no corresponda con el tipo de Informes de Homologaci�n.", vbOKOnly + vbExclamation
                End If
            End If
        End If
        '}
    End If
    '}
    
    DoEvents    ' add -008- d.
    
    Select Case adjOpcion
        Case "A"        ' Adjuntar
            If MsgBox(constConfirmMsgAttachDocumentos, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then     ' add -002- e.
                DoEvents    ' add -008- d.
                Call Status("Adjuntando... aguarde, por favor...")
                If sp_InsertPeticionAdjunto(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo, cAdjuntoTipo, adjPath.Text, adjFile.Text, adjComent.Text) Then     ' add -025- a. Se agregan los parametros de grupo y sector
                    '{ add -036- b. Envio mensaje cuando se adjunta un documento de alcance o cambio en el alcance
                    If InStr(1, "P950|C100|CG04|", cAdjuntoTipo, vbTextCompare) > 0 Then
                        Select Case cAdjuntoTipo
                            Case "P950": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 44, "Nombre del documento: " & Trim(adjFile.Text))
                            Case "C100": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 45, "Nombre del documento: " & Trim(adjFile.Text))
                            Case "CG04": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 46, "Nombre del documento: " & Trim(adjFile.Text))
                            Case "C104": Call spMensajes.sp_DoMensajePersonalizado(glNumeroPeticion, 51, "Nombre del documento: " & Trim(adjFile.Text))     ' add -093- b.
                        End Select
                    End If
                    '}
'                    '{ add -031- a.
'                    If Left(cAdjuntoTipo, 3) = "IDH" Then
'                        Call sp_AddHistorial(glNumeroPeticion, "ADJADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboAdjHomoClass) & "-->" & adjFile.Text)
'                    Else
'                    '}
'                        Call sp_AddHistorial(glNumeroPeticion, "ADJADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboAdjClass) & "-->" & adjFile.Text)
'                    End If  ' add -031- a.
                    Call sp_AddHistorial(glNumeroPeticion, "ADJADD", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboAdjClass) & "-->" & adjFile.Text)     ' add -101- b.
                    If InStr(1, "ALCA|P950|C100|", cAdjuntoTipo, vbTextCompare) > 0 Then
                        Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
                        Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "ALT", "1")    ' add -077- a.
                    End If
                    Call adjHabBtn(0, "X")
                    adjPath.Text = ""
                    txtAdjSector.Text = ""
                    txtAdjGrupo.Text = ""
                    txtAdjSize.Text = ""
                'End If     ' del -060- a.
                    '{ add -022- a. Envio un mensaje al grupo Homologaci�n para informar el evento ocurrido (adjunto de un documento de metodolog�a)
                    ' 1. Primero determina si ya existe para la petici�n el grupo Homologador
                    If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
                        ' 2. Obtengo los datos del grupo homologador
                        Call CargarGrupoHomologador
                        ' 3. Obtiene el estado actual del grupo Homologaci�n para la petici�n
                        If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, cGrupoHomologador) Then
                            cGrupoHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
                        End If
                        ' 4. Determina si el grupo homologador se encuentra en alguno de los estados "Activos"
                        If InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(cGrupoHomologadorEstado)) = 0 Then
                            Select Case cAdjuntoTipo
                                Case "C100": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 45, sEstado, Null)
                                Case "P950": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 44, sEstado, Null)
                                Case "CG04": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 46, sEstado, Null)
                                Case "C204": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 47, sEstado, Null)
                                Case "EML1": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 48, sEstado, Null)
                                Case "EML2": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 49, sEstado, Null)
                                Case "T710": Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 50, sEstado, Null)
                            End Select
                        End If
                    End If
                    '}
                End If      ' add -060- a.
                Call Status("Listo.")
            End If      ' add -002- e.
        Case "M"
            If Not sp_UpdatePeticionAdjunto(glNumeroPeticion, grdAdjPet.TextMatrix(grdAdjPet.RowSel, 1), grdAdjPet.TextMatrix(grdAdjPet.RowSel, 2), adjComent.Text, cAdjuntoTipo, adjFile.Text) Then
                MsgBox "Se ha producido un error al intentar grabar los datos. Revise.", vbExclamation + vbOKOnly, "Error al actualizar"
            End If
            adjFile.Text = "": adjPath.Text = "": adjComent.Text = ""
            txtAdjSector.Text = ""
            txtAdjGrupo.Text = ""
            txtAdjSize.Text = ""
            Call adjHabBtn(0, "X")
        Case "E"
            '{ del -100- a.
'            '{ add -039- c.
'            Dim nRow As Integer
'            Dim nRwD As Integer
'            Dim nRwH As Integer
'            Dim naux_RwD As Integer
'
'            If grdAdjPet.Row <= grdAdjPet.RowSel Then
'                nRwD = grdAdjPet.Row
'                nRwH = grdAdjPet.RowSel
'            Else
'                nRwH = grdAdjPet.Row
'                nRwD = grdAdjPet.RowSel
'            End If
'
'            If nRwD <> nRwH Then
'                ' Se eliminan m�s de un documento (> 1)
'                If MsgBox(constConfirmMsgDeAttachVariosDocumentos, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then
'                    DoEvents
'                    Call Puntero(True)
'                    '{ add -049- a.
'                    Dim bControlAdjuntos As Boolean
'                    naux_RwD = nRwD
'                    Do While naux_RwD <= nRwH
'                        If InStr(1, "ALCA|P950|C100", grdAdjPet.TextMatrix(naux_RwD, 1), vbTextCompare) > 0 Then
'                            bControlAdjuntos = True
'                            Exit Do
'                        End If
'                        DoEvents
'                        naux_RwD = naux_RwD + 1
'                    Loop
'
'                    If bControlAdjuntos Then
'                        If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
'                            ' Existe el conforme al documento de alcance... hay que generar la advertencia
'                            If MsgBox("La petici�n tiene ingresado un conforme final al documento de alcance." & vbCrLf & "Si continua, ser� eliminado dicho conforme y deber� gestionar la obtenci�n de uno nuevo." & vbCrLf & "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Eliminaci�n de documentaci�n de Alcance") = vbNo Then
'                                Call adjHabBtn(0, "X")
'                                Call Puntero(False)
'                                Call Status("Listo.")
'                                Exit Sub
'                            Else
'                                Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
'                            End If
'                        End If
'                    End If
'                    '}
'                    Call Status("Aguarde, por favor...")
'                    Do While nRwD <= nRwH
'                        Call EliminarDocumentoAdjunto(glNumeroPeticion, grdAdjPet.TextMatrix(nRwD, 1), grdAdjPet.TextMatrix(nRwD, 2), sEstado, grdAdjPet.TextMatrix(nRwD, 1) & "-->" & grdAdjPet.TextMatrix(nRwD, 2))
'                        DoEvents
'                        nRwD = nRwD + 1
'                    Loop
'                    adjFile.Text = "": adjPath.Text = "": adjComent.Text = ""
'                    txtAdjSector.Text = ""
'                    txtAdjGrupo.Text = ""
'                    txtAdjSize.Text = ""
'                    Call adjHabBtn(0, "X")
'                    Screen.MousePointer = vbDefault
'                    Call Status("Listo.")
'                End If
'            Else        ' Se elimina un solo documento
'                If MsgBox(constConfirmMsgDeAttachDocumentos, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then
'                    DoEvents
'                    Call Puntero(True)
'                    Call Status("Aguarde, por favor...")
'                    If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
'                        ' Existe el conforme al documento de alcance... si el documento a eliminar es un documento de
'                        ' alcance, entonces hay que generar la advertencia
'                        If InStr(1, "ALCA|P950|C100|", cAdjuntoTipo, vbTextCompare) > 0 Then
'                            If MsgBox("La petici�n ya tiene ingresado un conforme final al documento de alcance." & vbCrLf & "Si continua, ser� eliminado dicho conforme y deber� gestionar la obtenci�n de uno nuevo." & vbCrLf & "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Eliminaci�n de documentaci�n de Alcance") = vbNo Then
'                                Call adjHabBtn(0, "X")
'                                Call Puntero(False)
'                                Call Status("Listo.")
'                                Exit Sub
'                            Else
'                                Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", "REQ", "1")
'                            End If
'                        End If
'                    End If
'                    Call EliminarDocumentoAdjunto(glNumeroPeticion, grdAdjPet.TextMatrix(nRwD, 1), grdAdjPet.TextMatrix(nRwD, 2), sEstado, grdAdjPet.TextMatrix(nRwD, 1) & "-->" & grdAdjPet.TextMatrix(nRwD, 2))
'                    adjFile.Text = "": adjPath.Text = "": adjComent.Text = ""
'                    txtAdjSector.Text = ""
'                    txtAdjGrupo.Text = ""
'                    txtAdjSize.Text = ""
'                    Call adjHabBtn(0, "X")
'                    Call Puntero(False)
'                    Call Status("Listo.")
'                End If
'            End If
'            '}
            '}
            '{ add -100- a.
            bInformeHomologacion = False
            bExisteALCA = False
            bExisteALCP = False
            With grdAdjPet
                If .RowSel > 0 Then
                    If PerfilInternoActual <> "SADM" Then
                        'If InStr(1, "ALCA|P950|C100|", cAdjuntoTipo, vbTextCompare) > 0 Then
                        If InStr(1, "ALCA|P950|C100|", .TextMatrix(.RowSel, 1), vbTextCompare) > 0 Then
                            Call Puntero(True)
                            Call Status("Desvinculando...")
                            If sp_GetAdjuntosPet(glNumeroPeticion, Null, Null, Null) Then
                                Do While Not aplRST.EOF
                                    If InStr(1, "IDH1|IDH2|", ClearNull(aplRST.Fields!adj_tipo), vbTextCompare) > 0 Then
                                        If InStr(1, ClearNull(aplRST.Fields!adj_file), "SEG", vbTextCompare) > 0 Then
                                            bInformeHomologacion = True
                                        ElseIf InStr(1, ClearNull(aplRST.Fields!adj_file), "FIN", vbTextCompare) > 0 Then
                                            bInformeHomologacion = True
                                        ElseIf InStr(1, ClearNull(aplRST.Fields!adj_file), "PAR", vbTextCompare) > 0 Then
                                            bInformeHomologacion = True
                                        End If
                                        If bInformeHomologacion Then Exit Do
                                    End If
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                            If bInformeHomologacion Then
                                MsgBox "No puede quitar el documento de alcance," & vbCrLf & _
                                        "porque la petici�n ya tiene un informe" & vbCrLf & _
                                        "SOB/OME de homologaci�n.", vbInformation + vbOKOnly, "Eliminaci�n de documentaci�n de Alcance"
                                Call adjHabBtn(0, "X")
                                Call Puntero(False)
                                Call Status("Listo.")
                                Exit Sub
                            End If
                            If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
                                bExisteALCA = True
                            End If
                            If sp_GetPeticionConf(glNumeroPeticion, "ALCP", Null) Then
                                bExisteALCP = True
                            End If
                            If bExisteALCA Or bExisteALCP Then
                                If MsgBox("La petici�n ya tiene alg�n conforme a la documentaci�n de alcance." & vbCrLf & _
                                        "Si continua, ser�n eliminados los conformes y deber� gestionarlos nuevamente." & vbCrLf & _
                                        "�Desea continuar de todos modos?", vbQuestion + vbYesNo, "Eliminaci�n de documentaci�n de Alcance") = vbNo Then
                                    Call adjHabBtn(0, "X")
                                    Call Puntero(False)
                                    Call Status("Listo.")
                                    Exit Sub
                                Else
                                    If bExisteALCA Then Call sp_DeletePeticionConf(glNumeroPeticion, "ALCA", Null, Null)
                                    If bExisteALCP Then Call sp_DeletePeticionConf(glNumeroPeticion, "ALCP", Null, Null)
                                End If
                            End If
                        End If
                    End If
                    Call EliminarDocumentoAdjunto(glNumeroPeticion, .TextMatrix(.RowSel, 1), .TextMatrix(.RowSel, 2), sEstado, .TextMatrix(.RowSel, 1) & "-->" & .TextMatrix(.RowSel, 2))
                    adjFile.Text = "": adjPath.Text = "": adjComent.Text = ""
                    txtAdjSector.Text = ""
                    txtAdjGrupo.Text = ""
                    txtAdjSize.Text = ""
                    Call adjHabBtn(0, "X")
                    Call Puntero(False)
                    Call Status("Listo.")
                End If
            End With
            '}
        Case Else
            adjFile.Text = "": adjPath.Text = "": adjComent.Text = ""
            '{ add -025- a.
            txtAdjSector.Text = ""
            txtAdjGrupo.Text = ""
            txtAdjSize.Text = ""
            '}
            Call adjHabBtn(0, "X")
    End Select
End Sub

'{ add -039- c.
Private Sub EliminarDocumentoAdjunto(pet_nrointerno, cAdjuntoTipo, adjFile, sEstado, cCampoTexto)
    If sp_DeletePeticionAdjunto(pet_nrointerno, cAdjuntoTipo, adjFile) Then
        Call sp_AddHistorial(pet_nrointerno, "ADJDEL", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, cCampoTexto)
        If InStr(1, "ALCA|C100|P950|", cAdjuntoTipo, vbTextCompare) > 0 Then
            If Not sp_GetAdjuntosPet(pet_nrointerno, "ALCA", Null, Null) Then       ' Verifica si no existen mas documentos de alcance
                Call sp_DeletePeticionConf(pet_nrointerno, "ALCA", "REQ", "1")
            End If
        End If
        If InStr(1, "P950|C100|CG04|", cAdjuntoTipo, vbTextCompare) > 0 Then
            Select Case cAdjuntoTipo
                Case "P950": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 144, "Nombre del documento: " & adjFile)
                Case "C100": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 145, "Nombre del documento: " & adjFile)
                Case "ALCA": Call spMensajes.sp_DoMensajePersonalizado(pet_nrointerno, 146, adjFile)     ' upd -039- d. Se cambia CG04 por ALCA
            End Select
        End If
    End If
End Sub
'}

Private Sub CargarAdjPet()
    Dim lPesoAproxDocs As Long  ' add -081- a.
    
    bFormateando = True         ' add -079- b.
    adjView.Enabled = False
    adjDel.Enabled = False
    adjMod.Enabled = False
    adjAdd.Enabled = False

    With grdAdjPet
        .Visible = False
        .Clear
        .cols = 13
        .HighLight = flexHighlightAlways
        .Rows = 1
        .TextMatrix(0, colAdj_AUDIT_USER) = "": .ColWidth(colAdj_AUDIT_USER) = 0
        .TextMatrix(0, colAdj_ADJ_TIPO) = "Tipo": .ColWidth(colAdj_ADJ_TIPO) = 900
        .TextMatrix(0, colAdj_ADJ_FILE) = "Archivo": .ColWidth(colAdj_ADJ_FILE) = 3500: .ColAlignment(colAdj_ADJ_FILE) = flexAlignLeftCenter
        .TextMatrix(0, colAdj_ADJ_SIZE) = "Tama�o": .ColWidth(colAdj_ADJ_SIZE) = 1000
        .TextMatrix(0, colAdj_AREA) = "Sector y Grupo": .ColWidth(colAdj_AREA) = 3500: .ColAlignment(colAdj_AREA) = flexAlignLeftCenter    ' add -045- a.
        .TextMatrix(0, colAdj_NOM_AUDIT) = "Usuario": .ColWidth(colAdj_NOM_AUDIT) = 2000: .ColAlignment(colAdj_NOM_AUDIT) = flexAlignLeftCenter
        .TextMatrix(0, colAdj_AUDIT_DATE) = "Fecha": .ColWidth(colAdj_AUDIT_DATE) = 1500        ' upd '*    Antes 980
        .TextMatrix(0, colAdj_ADJ_TEXTO) = "Comentario": .ColWidth(colAdj_ADJ_TEXTO) = 5000: .ColAlignment(colAdj_ADJ_TEXTO) = flexAlignLeftCenter
        .TextMatrix(0, colAdj_COD_GRUPO) = "Grupo": .ColWidth(colAdj_COD_GRUPO) = 0            ' add -031- a.         ' add -031- a.
        .TextMatrix(0, colAdj_AUDIT_DATE_FMT) = "yyyymmdd": .ColWidth(colAdj_AUDIT_DATE_FMT) = 0       ' add -076- a.      ' add -076- a.
        .TextMatrix(0, colAdj_NOM_SECTOR_NV) = "sector": .ColWidth(colAdj_NOM_SECTOR_NV) = 0
        .TextMatrix(0, colAdj_NOM_GRUPO_NV) = "grupo": .ColWidth(colAdj_NOM_GRUPO_NV) = 0
        .TextMatrix(0, colAdj_ADJ_ISCOMPRESS) = "Compr.": .ColWidth(colAdj_ADJ_ISCOMPRESS) = 800: .ColAlignment(colAdj_ADJ_ISCOMPRESS) = 3     ' add -081- a.
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .Row = 0
        Call CambiarEfectoLinea(grdAdjPet, prmGridEffectFontBold)
    End With

    Call Status("Cargando documentos adjuntos...")
    If glNumeroPeticion <> "" Then
        If Not sp_GetAdjuntosPet(glNumeroPeticion, Null, Null, Null) Then
            aplRST.Close
            GoTo finAdjp
            Exit Sub
        End If
    Else
        aplRST.Close
        GoTo finAdjp
        Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdAdjPet
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colAdj_AUDIT_USER) = ClearNull(aplRST!audit_user)
            .TextMatrix(.Rows - 1, colAdj_ADJ_TIPO) = ClearNull(aplRST!adj_tipo)
            .TextMatrix(.Rows - 1, colAdj_ADJ_FILE) = ClearNull(aplRST!adj_file)
            .TextMatrix(.Rows - 1, colAdj_ADJ_SIZE) = IIf(aplRST!adj_size / 1024 < 1, "1 KB", Format(aplRST!adj_size / 1024, "###,###,###,##0") & " KB")
            .TextMatrix(.Rows - 1, colAdj_NOM_SECTOR_NV) = ClearNull(aplRST!nom_sector)
            .TextMatrix(.Rows - 1, colAdj_NOM_GRUPO_NV) = ClearNull(aplRST!nom_grupo)
            .TextMatrix(.Rows - 1, colAdj_AREA) = IIf(IsNull(aplRST!nom_sector), "-", IIf(ClearNull(aplRST!nom_grupo) = "", ClearNull(aplRST!nom_sector), ClearNull(aplRST!nom_sector) & " � " & ClearNull(aplRST!nom_grupo)))
            .TextMatrix(.Rows - 1, colAdj_NOM_AUDIT) = ClearNull(aplRST!nom_audit)
            .TextMatrix(.Rows - 1, colAdj_AUDIT_DATE) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "dd/mm/yyyy hh:MM"), "")   ' upd -*
            .TextMatrix(.Rows - 1, colAdj_ADJ_TEXTO) = ClearNull(aplRST!adj_texto)
            .TextMatrix(.Rows - 1, colAdj_COD_GRUPO) = ClearNull(aplRST!cod_grupo)                  ' add -031- a.
            .TextMatrix(.Rows - 1, colAdj_AUDIT_DATE_FMT) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "yyyymmdd"), "")   ' add -076- a.
            .TextMatrix(.Rows - 1, colAdj_ADJ_ISCOMPRESS) = ClearNull(aplRST!adj_iscompress)        ' add -081- a.
            lPesoAproxDocs = lPesoAproxDocs + Val(aplRST!adj_size / 1024)                           ' add -081- a.
        End With
        aplRST.MoveNext
        DoEvents
    Loop
finAdjp:
    With grdAdjPet
        bFormateando = True                 ' add -045- a.
        '.Row = .Rows - 1: .col = 0          ' Para evitar el pintado de la fila fija (Fixed Row)
        .FocusRect = flexFocusNone
        .Visible = True
        bFormateando = False                ' add -045- a.
    End With
    lblPesoEstDoc = "Peso estimado total de la documentaci�n: " & lPesoAproxDocs & " KB"    ' add -081- a.
    Call Status("Listo.")
    bFormateando = False                    ' add -079- b.
    Call adjSeleccion
End Sub

' Cuando el usuario hace click seleccionando la pesta�a de Adjuntos
Private Sub adjSeleccion()
    bFormateando = True
    Call setHabilCtrl(adjFile, "DIS")
    Call setHabilCtrl(adjPath, "DIS")
    Call setHabilCtrl(adjComent, "DIS")
    Call setHabilCtrl(txtAdjSector, "DIS")
    Call setHabilCtrl(txtAdjGrupo, "DIS")
    Call setHabilCtrl(txtAdjSize, "DIS")
    Call setHabilCtrl(cboAdjClass, "DIS")
    'Call setHabilCtrl(cboAdjHomoClass, "DIS")   ' add -031- a.     ' del -101- b.
    Call setHabilCtrl(adjAdd, "DIS")
    Call setHabilCtrl(adjMod, "DIS")
    Call setHabilCtrl(adjDel, "DIS")
    lblMensajeNoAdjuntos.Visible = False        ' add -029- b.
    
    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then   ' add -029- a.
        ' 1. El perfil del usuario actual es Super Administrador
        If PerfilInternoActual = "SADM" Then
            Call setHabilCtrl(adjAdd, "NOR")
            GoTo Continuar      ' add -061- a.
        End If
        ' 2. El perfil del usuario actual es BP o esta contenido debajo del sector o grupo del BP
        If (PerfilInternoActual = "BPAR" And Trim(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Then
            If glPETClase <> "OPTI" Then
                Call setHabilCtrl(adjAdd, "NOR")
                GoTo Continuar      ' add -061- a.
            End If
        Else
            If EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
                Call setHabilCtrl(adjAdd, "NOR")
                GoTo Continuar      ' add -061- a.
            End If
        End If
        ' 3. El usuario actual es Ejecutor (esta contenido en alguno de los sectores involucrados a la petici�n)
        If InEjecutor Then
            Call setHabilCtrl(adjAdd, "NOR")
        End If
    Else
        ' 1. El perfil del usuario actual es Super Administrador
        If PerfilInternoActual = "SADM" Then
            Call setHabilCtrl(adjAdd, "NOR")
            Call setHabilCtrl(adjMod, "NOR")
            Call setHabilCtrl(adjDel, "NOR")
            GoTo Continuar      ' add -061- a.
        Else
            lblMensajeNoAdjuntos.Visible = True
        End If
    End If
Continuar:                          ' add -061- a.
    With grdAdjPet
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, colAdj_ADJ_TIPO) <> "" Then
                glPathAdjPet = .TextMatrix(.RowSel, colAdj_ADJ_FILE)
                '{ del -101- b.
'                If ClearNull(.TextMatrix(.RowSel, colAdj_COD_GRUPO)) = cGrupoHomologador Then
'                    cboAdjClass.Visible = False
'                    cboAdjHomoClass.Visible = True
'                    Call SetCombo(cboAdjHomoClass, .TextMatrix(.RowSel, colAdj_ADJ_TIPO), False)
'                Else
'                    cboAdjClass.Visible = True
'                    cboAdjHomoClass.Visible = False
'                    Call SetCombo(cboAdjClass, .TextMatrix(.RowSel, colAdj_ADJ_TIPO), False)
'                End If
'}
                Call SetCombo(cboAdjClass, .TextMatrix(.RowSel, colAdj_ADJ_TIPO), False)
                adjFile = .TextMatrix(.RowSel, colAdj_ADJ_FILE)
                txtAdjSector.Text = IIf(.TextMatrix(.RowSel, colAdj_NOM_SECTOR_NV) = "", "-", .TextMatrix(.RowSel, colAdj_NOM_SECTOR_NV))
                txtAdjGrupo.Text = IIf(.TextMatrix(.RowSel, colAdj_NOM_GRUPO_NV) = "", "-", .TextMatrix(.RowSel, colAdj_NOM_GRUPO_NV))
                txtAdjSize.Text = .TextMatrix(.RowSel, colAdj_ADJ_SIZE)
                adjComent = .TextMatrix(.RowSel, colAdj_ADJ_TEXTO)
                
                Call setHabilCtrl(adjView, "NOR")
                
                If PerfilInternoActual = "SADM" Or _
                    .TextMatrix(.RowSel, colAdj_AUDIT_USER) = ClearNull(glLOGIN_ID_REEMPLAZO) Or _
                    .TextMatrix(.RowSel, colAdj_AUDIT_USER) = ClearNull(glLOGIN_ID_REAL) Then    ' add -001- o. - Se agreg� la funci�n CLEARNULL para evitar que no se habiliten las opciones de edici�n sobre los documentos
                    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then
                        Call setHabilCtrl(adjMod, "NOR")
                        Call setHabilCtrl(adjDel, "NOR")
                    End If
                End If
                If Not adjDel.Enabled Then
                    If EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                        If EsRecursoHomologador(.TextMatrix(.RowSel, colAdj_AUDIT_USER)) Then
                            Call setHabilCtrl(adjMod, "NOR")
                            Call setHabilCtrl(adjDel, "NOR")
                        End If
                    End If
                End If
            End If
        End If
    End With
    bFormateando = False
End Sub

Private Sub VisualizarAdjunto()
    On Error GoTo Errores
    
    DoEvents
    If ClearNull(glPathAdjPet) = "" Then
        Exit Sub
    End If
    Call Puntero(True)
    auxMensaje.prpTexto = "ATENCION" & Chr(13) & Chr(13) & Chr(13) & Chr(13) & "Si realiza modificaciones al documento" & Chr(13) & "no ser�n reflejadas en el adjunto"
    auxMensaje.prpSegundos = 2000   ' 3000
    auxMensaje.Disparar
    
    ' Hay un problema con este c�digo: me pasa con Diego Marcet, que reporta problemas con la librer�a Scripting (scrrun.dll)
    ' y por ende, entonces no puede usar la funci�n EstadoDeArchivo... revisar luego. Por ahora se deja as�.
    
'    If cboAdjClass.Visible Then
'        If EstadoDeArchivo(glWRKDIR & ClearNull(adjFile.Text)) Then
'            Kill (glWRKDIR & ClearNull(adjFile.Text))
'        End If
'        Call PeticionAdjunto2File(glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjFile.Text, glWRKDIR)
'    Else
'        If EstadoDeArchivo(glWRKDIR & ClearNull(adjFile.Text)) Then
'            Kill (glWRKDIR & ClearNull(adjFile.Text))
'        End If
'        Call PeticionAdjunto2File(glNumeroPeticion, CodigoCombo(cboAdjHomoClass, False), adjFile.Text, glWRKDIR)
'    End If
    
    '{ del -101- b.
    'If cboAdjClass.Visible Then
    '    PeticionAdjunto2File glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjFile.Text, glWRKDIR       ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
    'Else
    '    PeticionAdjunto2File glNumeroPeticion, CodigoCombo(cboAdjHomoClass, False), adjFile.Text, glWRKDIR
    'End If
    '}
    PeticionAdjunto2File glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjFile.Text, glWRKDIR       ' add -101- b.
    
    grdAdjPet.SetFocus
    Call AbrirAdjunto(glWRKDIR & adjFile.Text)
    Call Puntero(False)
    Call Status("Listo. Bajado en " & glWRKDIR)
    'glPathAdjPet = ""               ' add -012- a. - Se blanquea la variable una vez que se utiliza
    Exit Sub
Errores:
    Select Case Err.Number
        Case 70     'Permiso denegado (archivo en uso, por ejemplo)
            If MsgBox("Debe ser borrada la versi�n local de este archivo (es posible que este en uso)." & vbCrLf & _
                    "Cierre el archivo. �Reintentar?", vbExclamation + vbOKCancel, "Archivo local en uso") = vbOK Then
                Resume
            Else
                Call Puntero(False)
                Call Status("Listo.")
                glPathAdjPet = ""
                Exit Sub
            End If
    End Select
End Sub

Private Sub adjView_Click()
    Call VisualizarAdjunto
End Sub

Private Sub grdAdjPet_Click()
    'If Not bFormateando Then
    '    Call adjSeleccion
    'End If
    bSorting = True
    Call Ordenamiento(grdAdjPet)
    bSorting = False
End Sub

'{ add -002- d.
Private Sub grdAdjPet_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdAdjPet_Click
    End If
End Sub
'}

Private Sub grdAdjPet_DblClick()
    Call VisualizarAdjunto
'    'Call adjSeleccion
'    DoEvents
'    If ClearNull(glPathAdjPet) = "" Then
'        Exit Sub
'    End If
'    Call Puntero(True)
'    auxMensaje.prpTexto = "ATENCION" & Chr(13) & Chr(13) & Chr(13) & "Si realiza modificaciones al documento" & Chr(13) & "no ser�n reflejadas en el adjunto"
'    auxMensaje.prpSegundos = 3000
'    auxMensaje.Disparar
'    If cboAdjClass.Visible Then
'        Call PeticionAdjunto2File(glNumeroPeticion, CodigoCombo(cboAdjClass, False), adjFile.Text, glWRKDIR)       ' upd -001- h. Se pasa el valor en False a la funci�n CodigoCombo
'    Else
'        Call PeticionAdjunto2File(glNumeroPeticion, CodigoCombo(cboAdjHomoClass, False), adjFile.Text, glWRKDIR)
'    End If
'    Call AbrirAdjunto(glWRKDIR & adjFile.Text)
'    Call Puntero(False)
'    glPathAdjPet = "" ' add -012- a. - Se blanquea la variable una vez que se utiliza
End Sub

Sub adjHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    Select Case nOpcion     ' Opci�n indica una acci�n confirmativa o cancelativa (1 = confirmativa, 0 = cancelativa)
        Case 0              ' Acci�n cancelativa
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjDOC).Enabled = True
            orjBtn(orjCNF).Enabled = True
            orjBtn(orjEST).Enabled = True
            lblAdjPet = "": lblAdjPet.Visible = False
            grdAdjPet.Enabled = True
            grdAdjPet.SetFocus
            adjView.Enabled = False
            adjAdd.Enabled = False
            '{ add -083- a.
            If sp_GetDocumento(Null, Null) Then
                With cboAdjClass
                    .Clear
                    Do While Not aplRST.EOF
                        .AddItem ClearNull(aplRST.Fields!cod_doc) & ": " & ClearNull(aplRST.Fields!nom_doc)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    ' Por compatibilidad con peticiones del modelo de control anterior
                    .AddItem "REQU : Requerimiento"
                    .AddItem "ALCA : Doc. de alcance desarrollo"
                    .AddItem "COST : Evaluaci�n costo - beneficio"
                    .AddItem "PLEJ : Plan de ejecuci�n"
                    .AddItem "PLIM : Plan de implantaci�n Desarrollo"
                    .AddItem "MAIL : Mail"
                    .ListIndex = -1
                End With
            End If
            '}
            '{ del -101- b.
'            '{ add -031- a.
'            With cboAdjHomoClass
'                .Clear
'                .AddItem "IDH1 : Informe de Homologaci�n SOB"
'                .AddItem "IDH2 : Informe de Homologaci�n OME"
'                .AddItem "IDH3 : Informe de Homologaci�n OMA"
'                .AddItem "OTRO : Otros documentos"
'                .ListIndex = -1
'            End With
'            '}
            '}
            adjFile.Text = ""
            adjComent.Text = ""
            '}
            '{ add -008- a.
            ' 1. El perfil del usuario actual es Super Administrador
            If PerfilInternoActual = "SADM" And Not adjAdd.Enabled Then
                adjAdd.Enabled = True
            End If
            ' 2. El perfil del usuario actual es BP y es el BP de la petici�n
            If (PerfilInternoActual = "BPAR" And Trim(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) And Not adjAdd.Enabled Then
                adjAdd.Enabled = True
            Else
                If EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) And Not adjAdd.Enabled Then
                    adjAdd.Enabled = True
                End If
            End If
            ' 3. El usuario actual es Ejecutor (esta contenido en alguno de los sectores involucrados a la petici�n)
            If InEjecutor And Not adjAdd.Enabled Then
                adjAdd.Enabled = True
            End If
            '}
            adjMod.Enabled = False
            adjDel.Enabled = False
            adjCon.Enabled = False
            adjCan.Enabled = False
            adjComent.Locked = True
            If IsMissing(vCargaGrid) Then
                Call CargarAdjPet
            Else
                Call adjSeleccion
            End If
        Case 1      ' Acci�n confirmativa
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjDOC).Enabled = False
            orjBtn(orjCNF).Enabled = False
            orjBtn(orjEST).Enabled = False
            fraButtPpal.Enabled = False
            grdAdjPet.Enabled = False
            adjAdd.Enabled = False
            adjMod.Enabled = False
            adjDel.Enabled = False
            adjCon.Enabled = True
            adjCan.Enabled = True
            adjView.Enabled = False
            Select Case sOpcionSeleccionada
                Case "A"
                Case "M"
                    lblAdjPet = " MODIFICAR ": lblAdjPet.Visible = True
                    fraAdjPet.Enabled = True
                    adjComent.Locked = False
                    Call setHabilCtrl(adjComent, "NOR")
                    '{ add -006- i.
                    If cModeloControl = MODELO_CONTROL_NUEVO Then
                        '{ add -083- a.
                        If sp_GetDocumento(Null, "S") Then
                            With cboAdjClass
                                .Clear
                                Do While Not aplRST.EOF
                                    .AddItem ClearNull(aplRST.Fields!cod_doc) & ": " & ClearNull(aplRST.Fields!nom_doc)
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                                .ListIndex = 0
                            End With
                            '{ del -101- b.
'                            With cboAdjHomoClass
'                                .Clear
'                                .AddItem "IDH1 : Informe de Homologaci�n SOB"
'                                .AddItem "IDH2 : Informe de Homologaci�n OME"
'                                .AddItem "IDH3 : Informe de Homologaci�n OMA"
'                                .AddItem "OTRO : Otros documentos"
'                                .ListIndex = 0
'                            End With
                            '}
                        End If
                    Else
                        With cboAdjClass
                            .Clear
                            .AddItem "REQU : Requerimiento"
                            .AddItem "ALCA : Doc. de alcance desarrollo"
                            .AddItem "COST : Evaluaci�n costo - beneficio"
                            .AddItem "PLEJ : Plan de ejecuci�n"
                            .AddItem "PLIM : Plan de implantaci�n Desarrollo"
                            .AddItem "MAIL : Mail"
                            .AddItem "OTRO : Otros"
                            .ListIndex = -1
                        End With
                    End If
                    '{ del -101- b.
'                    If EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
'                        Call setHabilCtrl(cboAdjHomoClass, "NOR")
'                        cboAdjHomoClass.Visible = True
'                        cboAdjClass.Visible = False
'                        cboAdjHomoClass.ListIndex = PosicionCombo(cboAdjHomoClass, grdAdjPet.TextMatrix(grdAdjPet.RowSel, 1), False)
'                    Else
'                        Call setHabilCtrl(cboAdjClass, "NOR")
'                        cboAdjHomoClass.Visible = False
'                        cboAdjClass.Visible = True
'                        cboAdjClass.ListIndex = PosicionCombo(cboAdjClass, grdAdjPet.TextMatrix(grdAdjPet.RowSel, 1), False)
'                    End If
                    '}
                    
                    '{ add -101- b.
                    Call setHabilCtrl(cboAdjClass, "NOR")
                    cboAdjClass.ListIndex = PosicionCombo(cboAdjClass, grdAdjPet.TextMatrix(grdAdjPet.RowSel, 1), False)
                    '}
                    
                    '}
                    adjComent.SetFocus
                Case "E"
                    lblAdjPet = " DESVINCULAR ": lblAdjPet.Visible = True
                    fraAdjPet.Enabled = False
            End Select
    End Select
    adjOpcion = sOpcionSeleccionada
End Sub

'{ add -064- a.
Private Sub CargarEstado()
    Dim i As Long
       
    If glLOGIN_Gerencia = "DESA" Or ExisteEnCadena("SADM|ADMI|", PerfilInternoActual) Then       ' add -064- c.
        Call Status("Cargando controles...")
        Call Puntero(True)
        
        If sp_GetVarios("CTRLPRD1") Then
            If Not aplRST.EOF Then
                If ClearNull(aplRST.Fields!var_numero) = 1 Then
                    chkControl1.Value = 1
                Else
                    chkControl1.Value = 0
                End If
            End If
        End If
        
        With grdControl
            .Visible = False
            .Clear
            .HighLight = flexHighlightWithFocus
            .Rows = 1
            .cols = 5
            .TextMatrix(0, 0) = "Nivel": .ColWidth(0) = 900: .ColAlignment(0) = 0
            .TextMatrix(0, 1) = "N�": .ColWidth(1) = 400: .ColAlignment(1) = 0
            .TextMatrix(0, 2) = "Control": .ColWidth(2) = 4000: .ColAlignment(2) = 0
            .TextMatrix(0, 3) = "Observaciones": .ColWidth(3) = 3500: .ColAlignment(3) = 0
            .TextMatrix(0, 4) = "Estado": .ColWidth(4) = 1000: .ColAlignment(4) = 3
        End With
        If sp_GetControlPeticion(glNumeroPeticion, glLOGIN_ID_REEMPLAZO, "1") Then
            Do While Not aplRST.EOF
                With grdControl
                    i = i + 1
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, 0) = ClearNull(aplRST!nivel)
                    .TextMatrix(.Rows - 1, 1) = i
                    .TextMatrix(.Rows - 1, 2) = ClearNull(aplRST!Control)
                    .TextMatrix(.Rows - 1, 3) = ClearNull(aplRST!observaciones)
                    .TextMatrix(.Rows - 1, 4) = ClearNull(aplRST!Estado)
                    If InStr(1, "Pendiente", ClearNull(aplRST!Estado), vbTextCompare) > 0 Then
                        Call PintarLinea(grdControl, prmGridFillRowColorRed)
                    ElseIf InStr(1, "-", ClearNull(aplRST!Estado), vbTextCompare) > 0 Then
                        Call CambiarForeColorLinea(grdControl, prmGridFillRowColorDarkGrey)
                    End If
                    If ClearNull(aplRST!codigo) = 99 Then
                        .TextMatrix(.Rows - 1, 1) = ""
                        Call CambiarEfectoLinea(grdControl, prmGridEffectFontBold)
                        If InStr(1, "V�lida", ClearNull(aplRST!Estado), vbTextCompare) > 0 Then
                            Call PintarLinea(grdControl, prmGridFillRowColorGreen)
                        End If
                    End If
                    aplRST.MoveNext
                    DoEvents
                End With
            Loop
        End If
        grdControl.Visible = True
        Call CambiarEfectoLinea(grdControl, prmGridEffectFontBold)
        Call Puntero(False)
        Call Status("Listo.")
    End If      ' add -064- b.
End Sub
'}

Private Sub CargarPetConf()
    '{ add -010- a.
    Dim i As Long
    Dim bExisteOK_Pasaje As Boolean
    '}
    
    Call setHabilCtrl(pcfAdd, "DIS")
    Call setHabilCtrl(pcfDel, "DIS")
    Call setHabilCtrl(cmdHomoAdd, "DIS")
    Call setHabilCtrl(cmdHomoDel, "DIS")
  
    bExisteHOMA = False         ' add -020- a.
    
    With grdPetConf
        .Visible = False
        .Clear
        .cols = 7
        .HighLight = flexHighlightAlways
        .Rows = 1
        .TextMatrix(0, colPcf_OK_TIPO) = "Tipo": .ColWidth(colPcf_OK_TIPO) = 800: .ColAlignment(colPcf_OK_TIPO) = 0
        .TextMatrix(0, colPcf_OK_MODO) = "Requerido": .ColWidth(colPcf_OK_MODO) = 1000: .ColAlignment(colPcf_OK_MODO) = 0
        .TextMatrix(0, colPcf_OK_USER) = "Usuario": .ColWidth(colPcf_OK_USER) = 3000
        .TextMatrix(0, colPcf_OK_FECHA) = "Fecha": .ColWidth(colPcf_OK_FECHA) = 1600
        .TextMatrix(0, colPcf_OK_COMMENT) = "Comentario": .ColWidth(colPcf_OK_COMMENT) = 5000: .ColAlignment(colPcf_OK_COMMENT) = 0
        .ColWidth(5) = 0
        .ColWidth(6) = 0
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        Call CambiarEfectoLinea(grdPetConf, prmGridEffectFontBold)
    End With

    Call Status("Cargando conformes...")
    If glNumeroPeticion <> "" Then
        If Not sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
            aplRST.Close
            GoTo finPcf
            Exit Sub
        End If
    Else
        aplRST.Close
        GoTo finPcf
        Exit Sub
    End If
    Do While Not aplRST.EOF
        With grdPetConf
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colPcf_OK_TIPO) = ClearNull(aplRST!ok_tipo)
            .TextMatrix(.Rows - 1, colPcf_OK_MODO) = Switch(ClearNull(aplRST!ok_modo) = "REQ", "Requerido", ClearNull(aplRST!ok_modo) = "EXC", "Exenci�n", ClearNull(aplRST!ok_modo) = "N/A", "N/A", ClearNull(aplRST!ok_modo) = "ALT", "Alternativo")     ' add -010- a.  ' upd -053- a.    ' upd -065- a.
'{ del -099- a.
'            '{ add -010- a.
'            ' Si el conforme es de alguno de los tipos v�lidos para Homologaci�n
'            If InStr(1, "HSOB|HOME|HOMA|", Left(.TextMatrix(.Rows - 1, colPcf_OK_TIPO), 4), vbTextCompare) > 0 Then
'                Call SetCombo(cboPcfTipoHomo, .TextMatrix(.RowSel, colPcf_OK_TIPO), False)
'                pcfTextoHomo = .TextMatrix(.RowSel, colPcf_OK_COMMENT)
'                pcfTexto = ""
'
'                ' Si existe un conforme de homologaci�n de tipo HOMA, agrego la opci�n
'                ' de ingresar un conforme de usuario de tipo OKPP u OKPF solo para el Responsable de Sector de DyD
'                If glLOGIN_Gerencia = "DESA" And Left(.TextMatrix(.RowSel, 0), 4) = "HOMA" And InPerfil("CSEC") And Not bExisteOK_Pasaje Then
'                    ' Primero busco que no exista ya en el control combo
'                    For i = 0 To cboPcfTipo.ListCount - 1
'                        If Right(cboPcfTipo.List(i), 4) = "OKPP" Or _
'                           Right(cboPcfTipo.List(i), 4) = "OKPF" Then
'                            bExisteOK_Pasaje = True
'                            Exit For
'                        End If
'                    Next i
'                    If Not bExisteOK_Pasaje Then
'                        cboPcfTipo.AddItem "Ok Parcial Pasaje a Producci�n s/ conf. Homolog." & ESPACIOS & "||OKPP"
'                        cboPcfTipo.AddItem "Ok Final   Pasaje a Producci�n s/ conf. Homolog." & ESPACIOS & "||OKPF"
'                        bExisteOK_Pasaje = True
'                    End If
'                End If
'{ del -099- a.
'                '{ add -020- a.
'                If Left(.TextMatrix(.Rows - 1, colPcf_OK_TIPO), 4) = "HOMA" Then
'                    bExisteHOMA = True
'                    'If PerfilInternoActual = "CSEC" And InStr(1, "DESA|ORG.|TRYAPO|", glLOGIN_Gerencia, vbTextCompare) > 0 Then    ' del -098- a.
'                    If PerfilInternoActual = "CSEC" And InStr(1, "MEDIO|", glLOGIN_Direccion, vbTextCompare) > 0 Then               ' add -098- a.
'                        Call setHabilCtrl(pcfAdd, "NOR")
'                        Call setHabilCtrl(pcfAdd, "NOR")
'                    End If
'                End If
'                '}
'}
'            Else
'                'pcfTexto = .TextMatrix(.RowSel, 4) ' del -066- c.
'                pcfTexto = ""                       ' add -066- c.
'                pcfTextoHomo = ""
'            End If
'            '}
'}
            .TextMatrix(.Rows - 1, colPcf_OK_USER) = ClearNull(aplRST!nom_audit)
            .TextMatrix(.Rows - 1, colPcf_OK_FECHA) = IIf(Not IsNull(aplRST!audit_date), Format(aplRST!audit_date, "dd/mm/yyyy   hh:mm"), "")    ' upd -017- a. Se modifica el formato para incluir la hora, minutos y segundos
            .TextMatrix(.Rows - 1, colPcf_OK_COMMENT) = ClearNull(aplRST!ok_texto)
            .TextMatrix(.Rows - 1, 5) = ClearNull(aplRST!ok_modo)
            .TextMatrix(.Rows - 1, 6) = ClearNull(aplRST!ok_secuencia)
'            '{ add -020- a. Pinto de color la fila actual seg�n el conforme de Homologaci�n correspondiente
'            If InStr(1, "HSOB|HOME|HOMA", Left(.TextMatrix(.Rows - 1, colPcf_OK_TIPO), 4), vbTextCompare) > 0 Then
'                Select Case Left(.TextMatrix(.Rows - 1, colPcf_OK_TIPO), 4)
'                    Case "HSOB": Call PintarLinea(grdPetConf, prmGridFillRowColorGreen)
'                    Case "HOME": Call PintarLinea(grdPetConf, prmGridFillRowColorYellow)
'                    Case "HOMA": Call PintarLinea(grdPetConf, prmGridFillRowColorRose)
'                End Select
'            End If
'            '}
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    '{ add -010- a.
    cboPcfTipoHomo.ListIndex = -1
    pcfTextoHomo.Text = ""
    '{ add -066- c.
    cboPcfTipo.ListIndex = -1
    cboPcfModo.ListIndex = -1
    '}
    '}
finPcf:
    '{ add -059- a. Si es un l�der o un supervisor de DYD, se permite otorgar conformes (solo si adjunto los respaldos)
    grdPetConf.Visible = True
    If glLOGIN_Gerencia = "DESA" Then
        'If PerfilInternoActual = "CGRU" Then
        Select Case PerfilInternoActual
            Case "CGRU"
                If InStr(1, "CORR|SPUF|ATEN|OPTI|", glPETClase, vbTextCompare) > 0 Then
                    ' Verifico que pertenezca a alguno de los grupos ejecutores de la petici�n
                    If Not sp_GetPeticionGrupo(glNumeroPeticion, glLOGIN_Sector, glLOGIN_Grupo) Then
                        ' Este l�der no pertenece a ninguno de los grupos ejecutores
                        Call Status("Listo.")
                        Call pcfSeleccion
                        Exit Sub
                    End If
                    ' Esta habilitaci�n solo si la petici�n no se encuentra en alguno de los estados terminales
                    If Not InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", sEstado, vbTextCompare) > 0 Then
                        ' Y si no es un Homologador
                        If Not EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                            Call setHabilCtrl(pcfAdd, "NOR")
                        End If
                    End If
                End If
                If EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                    cmdDeshabilitarManual.Visible = True
                End If
            Case "CSEC"
                Call setHabilCtrl(pcfAdd, "NOR")
                'Call setHabilCtrl(pcfDel, "NOR")
        End Select
    End If
    '}
    With grdPetConf
        bFormateando = True                 ' add -045- a.
        .FocusRect = flexFocusNone
        .Visible = True
        bFormateando = False                ' add -045- a.
    End With
    Call Status("Listo.")
    Call pcfSeleccion
End Sub

Private Sub pcfSeleccion()
    '{ add -010- a.
    Dim i As Integer
    'Dim bExisteOK_Pasaje As Boolean
    '}
    Call setHabilCtrl(cboPcfTipo, "DIS")
    Call setHabilCtrl(pcfTexto, "DIS")
    Call setHabilCtrl(cboPcfModo, "DIS")
    '{ add -010- a.
    Call setHabilCtrl(cboPcfTipoHomo, "DIS")
    Call setHabilCtrl(pcfTextoHomo, "DIS")
    '}
    lblMensajeNoConformes.Visible = False   ' add -029- c.
    
    If InStr(1, "ANEXAD|ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|", sEstado, vbTextCompare) = 0 Then   ' add -029- c.
        '{ add -008- a.
        If CodigoCombo(cboTipopet, True) = "PRO" Then
            If (PerfilInternoActual = "CSEC" And glLOGIN_Gerencia = xGer) And Not pcfAdd.Enabled Then
                '{ add -029- a.
                ' Si el sector solicitante pertenece a la gerencia de  DyD
                If xGer = "DESA" Then
                    ' Mientras el usuario NO sea el mismo que di� el alta de la petici�n
                     If glLOGIN_ID_REEMPLAZO <> sUsualta Then
                        ' O el solicitante mismo de la petici�n... entonces puede dar conformes
                        If glLOGIN_ID_REEMPLAZO <> sSoli Then
                            Call setHabilCtrl(pcfAdd, "NOR")
                        End If
                    End If
                Else
                    Call setHabilCtrl(pcfAdd, "NOR")
                End If
                '}
                'pcfAdd.Enabled = True  ' del -029- a.
            End If
'            ' 1.2. El perfil del usuario es Responsable de Ejecuci�n de la gerencia de DyD u Organizaci�n
'            ' y su sector es el mismo que el de la petici�n (suspendida por falta de perfil Analista)
'            If (PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = xGer And xSec = xPerfSec And 1 <> 1) And Not pcfAdd.Enabled Then
'                Call setHabilCtrl(pcfAdd, "NOR")
'            End If
            ' 1.3. El perfil del usuario es BP y es el BP de la petici�n
            If (PerfilInternoActual = "BPAR" And ClearNull(sBpar) = ClearNull(glLOGIN_ID_REEMPLAZO)) And Not pcfAdd.Enabled Then
                If InStr(1, "OPTI|CORR|ATEN|SPUF", glPETClase, vbTextCompare) > 0 And xDir = "MEDIO" Then
                    Call setHabilCtrl(pcfAdd, "DIS")
                Else
                    Call setHabilCtrl(pcfAdd, "NOR")
                End If
'                '{ add -075- a. Si es el BP y la petici�n es CORR, ATEN o SPUF y la gerencia solicitante es de DyD
'                If InStr(1, "CORR|ATEN|SPUF", glPETClase, vbTextCompare) > 0 And xGer = "DESA" Then
'                    pcfAdd.Enabled = False
'                Else
'                    pcfAdd.Enabled = True
'                End If
'                '}
                'pcfAdd.Enabled = True      ' del -075- a.
            End If
        Else
            ' 2.1. El usuario pertenece al �rbol Solicitante asociado a la petici�n
            If flgSolicitor And Not pcfAdd.Enabled Then
                Call setHabilCtrl(pcfAdd, "NOR")
            End If
            ' 2.3. El usuario pertenece a cualquier sector de Organizaci�n del �rbol Ejecutor
            ' de la petici�n o su sector se corresponde con el sector del BP asociado a la petici�n
            '{ add -098- a.
            If (InEjecutor Or EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector)) And Not pcfAdd.Enabled Then
                Call setHabilCtrl(pcfAdd, "NOR")
            End If
            '}
            '{ del -098- a.
            'If (InEjecutorOrganizacion Or EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector)) And Not pcfAdd.Enabled Then
            '    pcfAdd.Enabled = True
            'End If
            '}
            ' 2.2. El perfil del usuario es BP y es el BP de la petici�n
            If (PerfilInternoActual = "BPAR" And ClearNull(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Then  ' upd -075- a. Se quit� "And Not pcfAdd.Enabled"
                '{ add -075- a.
                If glPETTipo = "ESP" And InStr(1, "CORR|ATEN|SPUF", glPETClase, vbTextCompare) > 0 And xGer = "DESA" Then
                    Call setHabilCtrl(pcfAdd, "DIS")
                Else
                    Call setHabilCtrl(pcfAdd, "NOR")
                End If
                '}
                'pcfAdd.Enabled = True  ' del -075- a.
            End If
        End If
        '}
        If PerfilInternoActual = "SADM" Then
            Call setHabilCtrl(pcfAdd, "NOR")
            Call setHabilCtrl(pcfDel, "NOR")
        End If
        '{ add -010- a. - Homologaci�n: si existe el grupo homologador vinculado a la petici�n
        If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
            'If Not aplRST.EOF Then
            ' El perfil del usuario es Resp. de Ejecuci�n y es del grupo Homologador
            If PerfilInternoActual = "CGRU" And EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                Call setHabilCtrl(cmdHomoAdd, "NOR")
            End If
            'End If
        End If
        '}
    '{ add -029- c.
    Else
        ' *** IMPORTANTE ***
        ' revisar esto, porque quizas si la petici�n esta finalizada realmente, ni siquiera HOMOLOGACION
        ' necesita realizar acciones sobre la misma...
        '{ add -010- a. - Homologaci�n: si existe el grupo homologador vinculado a la petici�n
        If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
            'If Not aplRST.EOF Then
            ' El perfil del usuario es Resp. de Ejecuci�n y es del grupo Homologador
            If PerfilInternoActual = "CGRU" And EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) Then
                Call setHabilCtrl(cmdHomoAdd, "NOR")
                Call setHabilCtrl(cmdHomoDel, "NOR")
            End If
            'End If
        End If
        '}
        lblMensajeNoConformes.Visible = True
    End If
    '}
        
    With grdPetConf
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, 0) <> "" Then
                Call SetCombo(cboPcfTipo, .TextMatrix(.RowSel, 0), True)
                Call SetCombo(cboPcfModo, .TextMatrix(.RowSel, 5), True)
                '{ add -010- a.
                ' Si el conforme es de alguno de los tipos v�lidos para homologaci�n
                If InStr(1, "HSOB|HOME|HOMA", Left(.TextMatrix(.RowSel, 0), 4), vbTextCompare) > 0 Then
                    Call SetCombo(cboPcfTipoHomo, .TextMatrix(.RowSel, 0), False)
                    pcfTextoHomo = .TextMatrix(.RowSel, 4)
                    pcfTexto = ""
                    ' Por ahora dejamos que, aunque la pet. este finalizada, el recurso de
                    ' homologacion pueda trabajar...
                    If spRecurso.EsRecursoHomologador(glLOGIN_ID_REEMPLAZO) And PerfilInternoActual = "CGRU" Then
                        Call setHabilCtrl(cmdHomoDel, "NOR")
                    End If
                Else
                    Call setHabilCtrl(cmdHomoDel, "DIS")
                    cboPcfTipoHomo.ListIndex = -1
                    pcfTexto = .TextMatrix(.RowSel, 4)
                    pcfTextoHomo = ""
                End If
                '}
                sPcfSecuencia = .TextMatrix(.RowSel, 6)
                '{ del -099- a.
                'If PerfilInternoActual = "SADM" Or PerfilInternoActual = "ADMI" Then
                '    '{ add -010- a.
                '    If InStr(1, "HSOB|HOME|HOMA", Left(.TextMatrix(.RowSel, 0), 4), vbTextCompare) > 0 Then
                '        pcfDel.Enabled = False
                '        cmdHomoDel.Enabled = True
                '    Else
                '        pcfDel.Enabled = True
                '        cmdHomoDel.Enabled = False
                '    End If
                '    '}
                'End If
                ''}
            '{ add -010- a. Si no hay selecci�n, no se puede borrar nada
            Else
                Call setHabilCtrl(pcfDel, "DIS")
                Call setHabilCtrl(cmdHomoDel, "DIS")
            '}
            End If
        End If
    End With
End Sub

Private Sub grdPetConf_Click()
    Call pcfSeleccion
    DoEvents
End Sub

Private Sub grdPetConf_DblClick()
    Call pcfSeleccion
    DoEvents
End Sub

'{ add -002- d.
Private Sub grdPetConf_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdPetConf_Click
    End If
End Sub
'}

Private Sub pcfCan_Click()
    pcfTexto = ""
    Call pcfHabBtn(0, "X")
End Sub

Private Sub pcfCon_Click()
    Call ConfirmarConformes
End Sub

Private Sub ConfirmarConformes()
    Dim sPcfTipo As String
    Dim sPcfModo As String
    Dim bHomologacion_EstadoActivo As Boolean
    Dim bExistenGruposHomologables As Boolean
    Dim bOKLider_Homologacion As Boolean
    Dim bOK_SI As Boolean               ' add -093- c.
    Dim iCantidad_Conformes As Integer
    Dim iCantidad_Respaldos As Integer
    Dim NroHistorial As Long
    Dim NuevoEstadoPeticion As String
        
    '{ add -093- c.
    bOK_SI = False
    If sp_GetVarios("CTRLPET1") Then
        If ClearNull(aplRST.Fields!var_numero) = "1" Then
            bOK_SI = True
        End If
    End If
    '}
    
    If CamposObligatoriosConf Then
        ' Inicializaci�n de variables
        bHomologacion_EstadoActivo = False
        bExistenGruposHomologables = False
        sPcfTipo = CodigoCombo(cboPcfTipo, True)
        sPcfModo = CodigoCombo(cboPcfModo, True)
        
        ' Quienes puede otorgar conformes
        '- Responsables de sector de la direcci�n de MEDIOs de peticiones tipo PROpias
        '- Responsables de sector o grupo de la gerencia de DESA para peticiones de clase CORR, SPUF, ATEN y OPTI
        '- Solicitantes de la petici�n
        '- Superadministradores

        '{ add -098- a.
        If pcfOpcion = "A" Then
            If InStr(1, "ALCP|ALCA|TESP|TEST|", sPcfTipo, vbTextCompare) > 0 Then
                If Not (PerfilInternoActual = "CSEC" And glLOGIN_Direccion = "MEDIO" And glPETTipo = "PRO") And _
                   Not ((PerfilInternoActual = "CSEC" Or PerfilInternoActual = "CGRU") And glLOGIN_Gerencia = "DESA" And InStr(1, "CORR|SPUF|ATEN|OPTI", glPETClase, vbTextCompare) > 0) Then
                    If (Not flgSolicitor) And PerfilInternoActual <> "SADM" And Not (PerfilInternoActual = "BPAR") Then
                    'If (Not flgSolicitor) And PerfilInternoActual <> "SADM" Then
                        MsgBox "Solo pueden otorgar conformes" & vbCrLf & "los usuarios solicitantes o BPs", vbOKOnly + vbInformation
                        Exit Sub
                    End If
                End If
            End If
        End If
        '}
        '{ del -098- a.
        'If pcfOpcion = "A" Then
        '    If InStr(1, "ALCP|ALCA|TESP|TEST|", sPcfTipo, vbTextCompare) > 0 Then
        '        If Not (PerfilInternoActual = "CSEC" And glLOGIN_Direccion = "MEDIO" And glPETTipo = "PRO") And _
        '           Not ((PerfilInternoActual = "CSEC" Or PerfilInternoActual = "CGRU") And glLOGIN_Gerencia = "DESA" And InStr(1, "CORR|SPUF|ATEN|OPTI", glPETClase, vbTextCompare) > 0) Then
        '            If (Not flgSolicitor) And PerfilInternoActual <> "SADM" And (glLOGIN_Gerencia <> "ORG." And glLOGIN_Gerencia <> "TRYAPO") Then
        '                MsgBox "Solo pueden otorgar conformes" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
        '                Exit Sub
        '            End If
        '        End If
        '    End If
        'End If
        '}

        Select Case pcfOpcion
            Case "A"
                Select Case sPcfTipo
                    Case "ALCP", "ALCA"
                        ' ******************************************************************************************
                        ' x. Lideres de DYD no pueden otorgar conformes a la documentaci�n de alcance
                        ' ******************************************************************************************
                        If InStr(1, "CORR|SPUF|ATEN|OPTI|", glPETClase, vbTextCompare) > 0 Then
                            If PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = "DESA" Then
                            'If Not (InStr(1, "CGRU|CSEC|", PerfilInternoActual, vbTextCompare) > 0) And glLOGIN_Gerencia = "DESA" Then
                                MsgBox "Vd. no puede otorgar conformes a la documentaci�n de alcance, pero si puede otorgar conformes" & vbCrLf & _
                                        "a las pruebas del usuario (con la documentaci�n respaldatoria correspondiente)", vbOKOnly + vbExclamation
                                Exit Sub
                            End If
                            If PerfilInternoActual = "CSEC" And glPETClase = "OPTI" Then
                                If Not sp_GetPeticionSector(glNumeroPeticion, glLOGIN_Sector) Then
                                    MsgBox "Solo responsables de sector vinculados a la petici�n" & vbCrLf & "pueden otorgar conformes a la documentaci�n de alcance.", vbOKOnly + vbExclamation
                                    Exit Sub
                                End If
                            End If
                        End If
                        If (sPcfTipo = "ALCA" And sPcfModo = "REQ") Or (sPcfTipo = "ALCP") Then
                            If cModeloControl = MODELO_CONTROL_NUEVO Then
                                If glPETTipo = "PRJ" Or glPETImpacto = "S" Then
                                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
                                        MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                                        Exit Sub
                                    End If
                                Else
                                    ' Chequea que ya este adjunto alg�n documento de alcance
                                    If InStr(1, "EVOL|OPTI|NUEV|", glPETClase, vbTextCompare) > 0 Then
                                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
                                            MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                                            Exit Sub
                                        End If
                                    End If
                                End If
                                ' Solicita el respaldo antes de permitir el ok por parte de un relacionado directo a la petici�n
                                If InStr(1, "NOR|ESP|AUI|AUX|", glPETTipo, vbTextCompare) > 0 Then
                                    If Not flgSolicitor Then
                                        '{ del -098- a.
                                        'If (PerfilInternoActual = "BPAR" And ClearNull(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or _
                                        '    InEjecutorOrganizacion Or _
                                        '    EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
                                        '}
                                        If (PerfilInternoActual = "BPAR" And ClearNull(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then      ' add -098- a.
                                            If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML1", Null, Null) Then
                                                MsgBox "La petici�n no posee el Mail de OK de Alcance (EML1, solapa 'Adjuntos')", vbOKOnly + vbInformation
                                                Exit Sub
                                            End If
                                        End If
                                    End If
                                End If
                                If sPcfTipo = "ALCP" Then
                                    If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
                                        MsgBox "La petici�n ya posee un conforme final a la documentaci�n de alcance." & vbCrLf & _
                                               "No puede otorgar conformes parciales posteriores.", vbOKOnly + vbInformation
                                        Exit Sub
                                    End If
                                End If
                            Else
                                ' Control anterior: que tenga el documento de alcance tipo ALCA
                                If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA", Null, Null) Then
                                    MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
                                    Exit Sub
                                End If
                            End If
                        End If
                    Case "TESP", "TEST"
                        ' ******************************************************************************************
                        ' x. Solo EVOL,  NUEV y OPTI reciben conformes de pruebas de usuario parcial
                        ' ******************************************************************************************
                        If sPcfTipo = "TESP" Then
                            If InStr(1, "CORR|SPUF|ATEN|", glPETClase, vbTextCompare) > 0 Then
                                MsgBox "Peticiones de clase Correctivo, Atenci�n a Usuarios y Spufis" & vbCrLf & _
                                        "no pueden recibir conformes de pruebas de usuario parciales.", vbOKOnly + vbExclamation
                                Exit Sub
                            End If
                        End If

                        ' ******************************************************************************************
                        ' x. Confronta la cantidad de respaldos a los conformes alternativos
                        ' ******************************************************************************************
                        If InStr(1, "CORR|SPUF|ATEN|OPTI|", glPETClase, vbTextCompare) > 0 Then
                            If PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = "DESA" Then
                                iCantidad_Respaldos = 0
                                iCantidad_Conformes = 0
                                If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML2", Null, Null) Then
                                    MsgBox "Debe adjuntar un documento de tipo EML2 para respaldar" & vbCrLf & "el otorgamiento del conforme a las pruebas del usuario.", vbOKOnly + vbExclamation
                                    Exit Sub
                                Else
                                    ' Cuento la cantidad de respaldos
                                    Do While Not aplRST.EOF
                                        iCantidad_Respaldos = iCantidad_Respaldos + 1
                                        aplRST.MoveNext
                                        DoEvents
                                    Loop
                                    ' Cuento la cantidad de conformes de tipo "Alternativos" (L�deres DYD)
                                    If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
                                        Do While Not aplRST.EOF
                                            If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                                                iCantidad_Conformes = iCantidad_Conformes + 1
                                            End If
                                            aplRST.MoveNext
                                            DoEvents
                                        Loop
                                        ' Comparo las cantidades contadas
                                        If iCantidad_Conformes >= iCantidad_Respaldos Then
                                            ' Los conformes superan a los respaldos
                                            MsgBox "No hay suficientes respaldos (EML2) para la cantidad de conformes a otorgar." & vbCrLf & _
                                                    "Revise en la pesta�a Adjuntos.", vbOKOnly + vbExclamation
                                            Exit Sub
                                        End If
                                    End If
                                End If
                            End If
                        End If

                        If cModeloControl = MODELO_CONTROL_NUEVO Then
                            If PerfilInternoActual <> "SADM" And InStr("EJECUC", sEstado) = 0 Then
                                MsgBox "El estado de la petici�n no permite otorgar un conforme de pruebas (parcial/final)." & vbCrLf & _
                                       "Solo peticiones en estado �En Ejecuci�n� pueden recibir conformes de pruebas de usuario.", vbOKOnly + vbInformation, "Conformes de pruebas de usuario"
                                Exit Sub
                            End If
                            ' Si usuario actual tiene perfil de BP de la petici�n actual
                            ' valida que exista el mail de OK a las Pruebas de Usuario
                            If InStr(1, "NOR|ESP|AUI|AUX|", glPETTipo, vbTextCompare) > 0 Then
                                '{ del -098- a.
                                'If (PerfilInternoActual = "BPAR" And Trim(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or _
                                '    InEjecutorOrganizacion Or _
                                '    EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
                                '}
                                If (PerfilInternoActual = "BPAR" And ClearNull(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then       ' add -098- a.
                                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML2", Null, Null) Then
                                        MsgBox "La petici�n no posee el Mail de OK de Pruebas de Usuario (EML2, solapa 'Adjuntos')", vbOKOnly + vbInformation
                                        Exit Sub
                                    End If
                                End If
                            End If
                        Else
                            If PerfilInternoActual <> "SADM" And InStr("ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA|", sEstado) = 0 Then
                                MsgBox "El estado de la petici�n no permite declarar un conforme de pruebas (parcial/final).", vbOKOnly + vbInformation, "Conformes de pruebas de usuario"
                                Exit Sub
                            End If
                        End If
                    Case "OKPP", "OKPF"
                        Dim bResponsableHabilitado As Boolean
                        
                        bResponsableHabilitado = False
                        ' Primero evalua si el usuario actua como Resp. de Sector
                        If PerfilInternoActual <> "CSEC" Then
                            MsgBox "Solo Responsables de Sector de las gerencias de DyD, Organizaci�n o Transformaci�n y Apoyo a Red" & vbCrLf & "involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & _
                                    "El usuario actual no actua como Responsable de Sector.", vbOKOnly + vbExclamation
                            Exit Sub
                        End If
                        ' Antes se evalua si existe un conforme de homologaci�n de tipo HOMA
                        If Not bExisteHOMA Then
                            MsgBox "No existe un conforme de Homologaci�n de tipo HOMA en la petici�n.", vbOKOnly + vbExclamation
                            Exit Sub
                        End If
                        ' Debe ser y actuar como un responsable de sector
                        If PerfilInternoActual = "CSEC" Then
                            ' Si es Responsable de Sector, veo que lo sea de alguno de los sectores de la petici�n
                            If sp_GetPeticionSector(glNumeroPeticion, Null) Then
                                Do While Not aplRST.EOF
                                    '{ add -098- a.
                                    If glLOGIN_Sector = ClearNull(aplRST!cod_sector) Then
                                        bResponsableHabilitado = True
                                        Exit Do
                                    End If
                                    '}
                                    '{ del -098- a.
                                    '' Si es el Responsable de Sector de uno de los sectores de la petici�n y pertenece a DyD,
                                    '' lo dejo agregar el conforme para Homologaci�n
                                    'If glLOGIN_Sector = Trim(aplRST!cod_sector) And _
                                    '    InStr(1, "DESA|ORG.|TRYAPO|", ClearNull(aplRST!cod_gerencia), vbTextCompare) > 0 Then
                                    '    bResponsableHabilitado = True
                                    '    Exit Do
                                    'End If
                                    '}
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                                If Not bResponsableHabilitado Then
                                    MsgBox "Solo responsables de sector de las gerencias involucrados en la petici�n pueden agregar este tipo de conforme.", vbOKOnly + vbExclamation       ' del -098- a.
                                    '{ del -098- a.
                                    'MsgBox "Solo Responsables de Sector de las gerencias de DyD, Organizaci�n o Transformaci�n y Apoyo a Red involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & _
                                    '        "El Responsable actual no pertenece a DyD.", vbOKOnly + vbExclamation
                                    '}
                                    Exit Sub
                                End If
                            Else
                                MsgBox "El Responsable actual no pertenece a ninguno de los sectores ejecutantes de la petici�n." & vbCrLf & vbCrLf & _
                                        "No puede agregar este tipo de conforme.", vbOKOnly + vbExclamation
                                Exit Sub
                            End If
                        Else
                            '{ add -098- a.
                            MsgBox "Solo responsables de sector involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & _
                                    "El usuario actual no actua como Responsable de Sector.", vbOKOnly + vbExclamation
                            '}
                            '{ del -098- a.
                            'MsgBox "Solo Responsables de Sector de las gerencias de DyD, Organizaci�n o Transformaci�n y Apoyo a Red involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & _
                            '        "El usuario actual no actua como Responsable de Sector.", vbOKOnly + vbExclamation
                            '}
                            Exit Sub
                        End If
                End Select
                
                If MsgBox(constConfirmMsgAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then
                    If sPcfTipo = "TESP" Then
                        sPcfModo = IIf(Not (glLOGIN_Gerencia = "DESA" And PerfilInternoActual = "CGRU"), "EXC", "ALT")
                        sPcfSecuencia = "0"
                    ElseIf sPcfTipo = "ALCP" Then
                        sPcfModo = "EXC"
                        sPcfSecuencia = "0"
                    Else
                        sPcfSecuencia = "1"
                    End If
                    If sp_InsertPeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, pcfTexto.Text, sPcfSecuencia, glLOGIN_ID_REEMPLAZO) Then
                        ' Agrega el mensaje correspondiente en el historial
                        'Call sp_AddHistorial(glNumeroPeticion, IIf(InStr(1, "OKPP|OKPF|", sPcfTipo, vbTextCompare) > 0, sPcfTipo, "OK" & sPcfTipo), sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                        Call sp_AddHistorial(glNumeroPeticion, IIf(InStr(1, "OKPP|OKPF|", sPcfTipo, vbTextCompare) > 0, sPcfTipo, "OK" & sPcfTipo), sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & ": " & pcfTexto.Text)
                        If InStr(1, "TESP|TEST|OKPP|OKPF|", sPcfTipo, vbTextCompare) > 0 Then
                            ' Homologaci�n
                            If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
                                Call CargarGrupoHomologador
                                ' Determina si el grupo homologador se encuentra en alguno de los estados "Activos"
                                If cGrupoHomologadorEstado <> "EJECUC" Then
                                    If Not InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(cGrupoHomologadorEstado)) > 0 Then
                                        ' Se envian los mensajes correspondientes a Responsables del Grupo "Homologador"
                                        Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, cGrupoHomologador, "EJECUC", "", "")
                                        ' Entonces cambia el estado del grupo homologador a "En ejecuci�n"
                                        Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "ESTADO", "EJECUC", Null, Null)
                                        ' Actualiza el estado del Sector del grupo Homologador
                                        cSectorHomologadorNuevoEstado = getNuevoEstadoSector(glNumeroPeticion, cGrupoHomologadorSector)
                                        ' Se registra en el historial el evento autom�tico de cambio de estado del grupo Homologador
                                        NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", sEstado, cGrupoHomologadorSector, cSectorHomologadorNuevoEstado, cGrupoHomologador, "EJECUC", glLOGIN_ID_REEMPLAZO, "� EN EJECUCION � Cambio autom�tico de estado del Grupo Homologador.")
                                        Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "EJECUC", date, "", NroHistorial, 0)                 ' upd -010- a.
                                        Call sp_UpdatePeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, cSectorHomologadorNuevoEstado, date, "", NroHistorial, 0)
                                        ' Se registra en el historial el evento autom�tico de cambio de estado del grupo Homologador
                                        bHomologacion_EstadoActivo = True
                                    End If
                                Else
                                    If InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN|", ClearNull(cGrupoHomologadorEstado)) = 0 Then
                                        bHomologacion_EstadoActivo = True
                                    End If
                                End If
                                ' Determina si existen en la petici�n grupos homologables
                                If sp_GetPetGrupoHomologable(glNumeroPeticion) Then
                                    bExistenGruposHomologables = True
                                End If
                                ' ???
                                If bHomologacion_EstadoActivo And bExistenGruposHomologables Then
                                    If InStr(1, "NUEV|EVOL|OPTI|", glPETClase, vbTextCompare) > 0 Then
                                        ' Control de existencia de conforme de homologaci�n tipo HOMA
                                        If sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Or _
                                           sp_GetPeticionConf(glNumeroPeticion, "HOMA.F", Null) Then
                                            ' Entonces busco si existe el conforme de Responsable de Sector a las observaciones de Homologaci�n
                                            If sp_GetPeticionConf(glNumeroPeticion, "OKPP", Null) Or _
                                               sp_GetPeticionConf(glNumeroPeticion, "OKPF", Null) Then
                                                    ' Si existe el conforme del usuario (parcial o final) entonces paso la cadena a Changeman
                                                    If (sp_GetPeticionConf(glNumeroPeticion, "TEST", Null)) Or _
                                                       (sp_GetPeticionConf(glNumeroPeticion, "TESP", Null)) Then
                                                            '{ add -093- c.
                                                            If bOK_SI Then
                                                                If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                                End If
                                                            Else
                                                            '}
                                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                            End If      ' add -093- c.
                                                    End If
                                            End If
                                        Else
                                            If (sp_GetPeticionConf(glNumeroPeticion, "HSOB.P", Null) Or _
                                                sp_GetPeticionConf(glNumeroPeticion, "HSOB.F", Null)) Or _
                                               (sp_GetPeticionConf(glNumeroPeticion, "HOME.P", Null) Or _
                                                sp_GetPeticionConf(glNumeroPeticion, "HOME.F", Null)) Then
                                                    ' Si existe el conforme del usuario (parcial o final) entonces paso la cadena a Changeman
                                                    If (sp_GetPeticionConf(glNumeroPeticion, "TEST", Null)) Or _
                                                       (sp_GetPeticionConf(glNumeroPeticion, "TESP", Null)) Then
                                                            '{ add -093- c.
                                                            If bOK_SI Then
                                                                If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                                    ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                                    'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                                End If
                                                            Else
                                                            '}
                                                                ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                            End If      ' add -093- c.
                                                    End If
                                            End If
                                        End If
                                    Else
                                        ' Peticiones clase CORR, ATEN o SPUF:
                                        ' -----------------------------------
                                        ' Si existe un HOMA, tengo que tener el OK del supervisor para pasar a Producci�n
                                        If sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Or _
                                           sp_GetPeticionConf(glNumeroPeticion, "HOMA.F", Null) Then
                                            ' Entonces busco si existe el conforme de Responsable de Sector a las observaciones de Homologaci�n
                                            If sp_GetPeticionConf(glNumeroPeticion, "OKPP", Null) Or _
                                               sp_GetPeticionConf(glNumeroPeticion, "OKPF", Null) Then
                                                    ' Si existe el conforme del usuario (parcial o final) entonces paso la cadena a Changeman
                                                    If (sp_GetPeticionConf(glNumeroPeticion, "TEST", Null)) Or _
                                                       (sp_GetPeticionConf(glNumeroPeticion, "TESP", Null)) Then
                                                            '{ add -093- c.
                                                            If bOK_SI Then
                                                                If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                                    ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                                    'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                                End If
                                                            Else
                                                            '}
                                                                ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                            End If      ' add -093- c.
                                                    End If
                                            End If
                                        Else
                                            If PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = "DESA" Then  ' upd -065- b.
                                                bOKLider_Homologacion = False
                                                If sp_GetVarios("CTRLPRD1") Then
                                                    If Not aplRST.EOF Then
                                                        If ClearNull(aplRST.Fields!var_numero) = 1 Then
                                                            bOKLider_Homologacion = True
                                                        End If
                                                    End If
                                                End If
                                                
                                                If bOKLider_Homologacion Then
                                                    If (sp_GetPeticionConf(glNumeroPeticion, "HSOB.P", Null)) Or _
                                                       (sp_GetPeticionConf(glNumeroPeticion, "HSOB.F", Null)) Or _
                                                       (sp_GetPeticionConf(glNumeroPeticion, "HOME.P", Null)) Or _
                                                       (sp_GetPeticionConf(glNumeroPeticion, "HOME.F", Null)) Then
                                                            '{ add -093- c.
                                                            If bOK_SI Then
                                                                If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                                    ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                                    'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                                End If
                                                            Else
                                                            '}
                                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                            End If  ' add -093- c.
                                                    End If
                                                Else
                                                    '{ add -093- c.
                                                    If bOK_SI Then
                                                        If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                            ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                            'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                            Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                        End If
                                                    Else
                                                    '}
                                                        'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                    End If      ' add -093- c.
                                                End If
                                            Else
                                                '{ add -093- c.
                                                If bOK_SI Then
                                                    If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                        ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                        'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                    End If
                                                Else
                                                '}
                                                    'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                End If      ' add -093- c.
                                            End If
                                        End If
                                    End If
                                Else
                                    If Not sPcfModo = "ALT" Then
                                        '{ add -093- c.
                                        If bOK_SI Then
                                            If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                            End If
                                        Else
                                        '}
                                            'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                            Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                        End If  ' add -093- c.
                                    Else
                                        bOKLider_Homologacion = False
                                        If sp_GetVarios("CTRLPRD1") Then
                                            If ClearNull(aplRST.Fields!var_numero) = 1 Then
                                                bOKLider_Homologacion = True
                                            End If
                                        End If
                                        If Not bOKLider_Homologacion Then
                                            '{ add -093- c.
                                            If bOK_SI Then
                                                If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
                                                    ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                    'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"
                                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                                End If
                                            Else
                                            '}
                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02"    ' El grupo homologador no se encuentra ya en un estado activo, por ende agrego los datos de la petici�n en la cadena HOST-ChangeMan
                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REAL, "02")
                                            End If  ' add -093- c.
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        ' Agrega un mensaje para el grupo Homologador (independientemente del tipo de conforme)
                        If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
                            Select Case sPcfTipo
                                Case "ALCA", "ALCP": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 100, sEstado, Null)
                                Case "TEST", "TESP": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 101, sEstado, Null)
                                Case "OKPP", "OKPF": Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 102, sEstado, Null)
                            End Select
                        End If
                    End If
                    Call pcfHabBtn(0, "X")
                    ' Debo evaluar si debo "reactivar" al grupo Homologador
                    If InStr(1, "CORR|SPUF|ATEN|OPTI|", glPETClase, vbTextCompare) > 0 Then
                        If PerfilInternoActual = "CGRU" Then
                            If glLOGIN_Gerencia = "DESA" Then
                                If InStr(1, "TESP|TEST|", sPcfTipo, vbTextCompare) > 0 Then
                                    If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, cGrupoHomologador) Then
                                        ' Si homologaci�n esta en alguno de los estados terminales, lo "reactivo"
                                        If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                                            Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "ESTADO", "EJECUC", Null, Null)
                                            ' Actualizo el estado del sector de Homologaci�n (si corresponde)
                                            cSectorHomologadorEstado = getNuevoEstadoSector(glNumeroPeticion, cGrupoHomologadorSector)
                                            Call sp_UpdatePeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, cSectorHomologadorEstado, date, "", 0, 0)
                                            NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", "EJECUC", cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "EJECUC", glLOGIN_ID_REEMPLAZO, "� EN EJECUCION � Cambio autom�tico de estado del Grupo Homologador por conforme de L�der.")
                                            Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "HSTSOL", Null, Null, NroHistorial)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                    ValidarPeticion
                End If
            Case "E"
                If MsgBox(constConfirmMsgDeAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then
                    If sp_DeletePeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, sPcfSecuencia) Then
                        Call sp_AddHistorial(glNumeroPeticion, "OK" & sPcfTipo, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "Eliminaci�n " & TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
                        If InStr(1, "TESP|TEST|ALCP|ALCA|", sPcfTipo, vbTextCompare) > 0 Then Call sp_DeletePeticionEnviadas(glNumeroPeticion)
                        pcfTexto = ""
                        Call pcfHabBtn(0, "X")
                    End If
                End If
                ValidarPeticion
            Case Else
                pcfTexto = ""
                Call pcfHabBtn(0, "X")
        End Select
    End If
End Sub

Private Sub pcfAdd_Click()
    Call pcfHabBtn(1, "A")
End Sub

Private Sub pcfMod_Click()
    Call pcfHabBtn(1, "M")
End Sub

Private Sub pcfDel_Click()
    Call pcfHabBtn(1, "E")
End Sub

'{ add -010- a.
Private Sub cmdHomoAdd_Click()
    Call pcfHabBtnHomo(1, "A")
End Sub

Private Sub cmdHomoDel_Click()
    Call pcfHabBtnHomo(1, "E")
End Sub

Private Sub cmdHomoCancel_Click()
    pcfTextoHomo = ""
    Call pcfHabBtnHomo(0, "X")
End Sub

Private Sub cmdHomoConf_Click()
    Dim cTipoHomo As String
    Dim sConformes As String
    Dim bOK_SI As Boolean
    Dim bOKLider As Boolean
    Dim bOKLider_Homologacion As Boolean
    Dim bOK_PasajeProduccion As Boolean

    bOK_SI = False
    bOKLider = False
    bOKLider_Homologacion = False
    bOK_PasajeProduccion = False
    sConformes = ""

    If sp_GetVarios("CTRLPET1") Then        ' Control de documento C104: Dictamen de Seg. Inf.
        If ClearNull(aplRST.Fields!var_numero) = "1" Then
            If CodigoCombo(cboImpacto, True) = "S" Or _
                CodigoCombo(cboTipopet, True) = "PRJ" Or _
                (Val(txtPrjId.Text) + Val(txtPrjSubId.Text) + Val(txtPrjSubsId.Text) > 0) Then
                    bOK_SI = True
            End If
        End If
    End If
    If sp_GetVarios("CTRLPRD1") Then        ' Control exigencia de OK de Lider DYD
        If ClearNull(aplRST.Fields!var_numero) = 1 Then
            bOKLider_Homologacion = True
        End If
    End If

    If cboPcfTipoHomo.ListIndex < 0 Then            ' Control de selecci�n de tipo de conforme de Homologaci�n
        MsgBox "Falta especificar el tipo de conforme de Homologaci�n.", vbOKOnly + vbInformation, "Homologaci�n"
        Exit Sub
    End If
    cTipoHomo = CodigoCombo(cboPcfTipoHomo, False)  ' Guardo en una variable la clave de indentificaci�n del conforme de Homologaci�n
    
    Select Case pcfOpcion
        Case "A"
            If InStr(1, sConformes, cTipoHomo, vbTextCompare) > 0 Then      ' Verifica que no exista el tipo de conforme de homologaci�n ya en la base
                MsgBox "Ya existe ese conforme de Homologaci�n para esta petici�n. Revise.", vbExclamation + vbOKOnly, "Homologaci�n"
                Exit Sub
            End If
            If MsgBox("�Confirma el conforme de Homologaci�n seleccionado?", vbQuestion + vbOKCancel, "Agregar conforme de Homologaci�n") = vbOK Then
                Call sp_InsertPeticionConf(glNumeroPeticion, cTipoHomo, "N/A", pcfTextoHomo.Text, "1", glLOGIN_ID_REEMPLAZO)
                Call sp_AddHistorial(glNumeroPeticion, cTipoHomo, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfTipoHomo, cTipoHomo, False) & ": " & pcfTextoHomo.Text)
                Call pcfHabBtnHomo(0, "X")
                sConformes = ""
                If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
                    Do While Not aplRST.EOF
                        sConformes = sConformes & ClearNull(aplRST.Fields!ok_tipo) & "|"
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
                ' Evaluo si la petici�n debe quedar v�lida para pasajes a Producci�n (seg�n la clase)
                Select Case glPETClase
                    Case "EVOL", "NUEV", "OPTI"
                        If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
                            If InStr(1, sConformes, "OK", vbTextCompare) > 0 Then
                                If bOK_SI Then
                                    bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                Else
                                    bOK_PasajeProduccion = True
                                End If
                            Else
                                bOK_PasajeProduccion = False
                            End If
                        Else
                            If InStr(1, sConformes, "TES", vbTextCompare) > 0 Then
                                If bOK_SI Then
                                    bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                Else
                                    bOK_PasajeProduccion = True
                                End If
                            End If
                        End If
                    Case Else
                        If bOKLider_Homologacion Then
                            bOKLider = False
                            If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
                                Do While Not aplRST.EOF
                                    If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                                        bOKLider = True
                                        Exit Do
                                    End If
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                            If bOKLider Then
                                If InStr(1, sConformes, "HOME", vbTextCompare) > 0 Or _
                                    InStr(1, sConformes, "HSOB", vbTextCompare) > 0 Then
                                    If bOK_SI Then
                                        bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                    Else
                                        bOK_PasajeProduccion = True
                                    End If
                                Else
                                    bOK_PasajeProduccion = False
                                End If
                            End If
                        End If
                        If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
                            If InStr(1, sConformes, "OK", vbTextCompare) > 0 Then
                                If bOK_SI Then
                                    bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                Else
                                    bOK_PasajeProduccion = True
                                End If
                            Else
                                bOK_PasajeProduccion = False
                            End If
                        Else
                            If bOK_SI Then
                                bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                            Else
                                bOK_PasajeProduccion = True
                            End If
                        End If
                End Select
            End If
            If bOK_PasajeProduccion Then
                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
            End If
            Call ValidarPeticion
        Case "E"
            If MsgBox("�Confirma quitar el conforme de Homologaci�n seleccionado?", vbQuestion + vbOKCancel, "Quitar conforme de Homologaci�n") = vbOK Then
                Call sp_DeletePeticionConf(glNumeroPeticion, cTipoHomo, grdPetConf.TextMatrix(grdPetConf.RowSel, 1), grdPetConf.TextMatrix(grdPetConf.RowSel, 6))
                Call sp_AddHistorial(glNumeroPeticion, cTipoHomo, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "Eliminaci�n " & TextoCombo(cboPcfTipoHomo, cTipoHomo, False) & "  --->  " & pcfTextoHomo.Text)
                sConformes = ""
                If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
                    Do While Not aplRST.EOF
                        sConformes = sConformes & ClearNull(aplRST.Fields!ok_tipo) & "|"
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
                If InStr(1, sConformes, "HOMA", vbTextCompare) > 0 Then
                    bOK_PasajeProduccion = False
                Else
                    If InStr(1, "CORR|ATEN|SPUF|", glPETClase) > 0 Then
                        If bOKLider_Homologacion Then
                            bOKLider = False
                            If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
                                Do While Not aplRST.EOF
                                    If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                                        bOKLider = True
                                        Exit Do
                                    End If
                                    aplRST.MoveNext
                                    DoEvents
                                Loop
                            End If
                            If bOKLider Then
                                If InStr(1, sConformes, "HOME", vbTextCompare) > 0 Or _
                                    InStr(1, sConformes, "HSOB", vbTextCompare) > 0 Then
                                    If bOK_SI Then
                                        bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                    Else
                                        bOK_PasajeProduccion = True
                                    End If
                                Else
                                    bOK_PasajeProduccion = False
                                End If
                            Else
                                If InStr(1, sConformes, "TES", vbTextCompare) > 0 Then
                                    If bOK_SI Then
                                        bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                    Else
                                        bOK_PasajeProduccion = True
                                    End If
                                Else            ' Si no hay conforme de Usuario, entonces no puede ser v�lida
                                    bOK_PasajeProduccion = False
                                End If
                            End If
                        Else
                            If InStr(1, sConformes, "TES", vbTextCompare) > 0 Then
                                If bOK_SI Then
                                    bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                                Else
                                    bOK_PasajeProduccion = True
                                End If
                            Else            ' Si no hay conforme de Usuario, entonces no puede ser v�lida
                                bOK_PasajeProduccion = False
                            End If
                        End If
                    Else
                        If InStr(1, sConformes, "HOME", vbTextCompare) > 0 Or _
                            InStr(1, sConformes, "HSOB", vbTextCompare) > 0 Then
                            If bOK_SI Then
                                bOK_PasajeProduccion = IIf(InStr(1, sConformes, "C104", vbTextCompare) > 0, True, False)
                            Else
                                bOK_PasajeProduccion = True
                            End If
                        Else
                            bOK_PasajeProduccion = False
                        End If
                    End If
                End If
            End If
            Call pcfHabBtnHomo(0, "X")
            cboPcfTipoHomo.ListIndex = -1
            pcfTextoHomo.Text = ""
            If bOK_PasajeProduccion Then
                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
            '{ add -�ltimo- 26.09.2012
            Else
                Call sp_DeletePeticionChangeMan(glNumeroPeticion)
            '}
            End If
            Call ValidarPeticion
        Case Else
            pcfTextoHomo = ""
            Call pcfHabBtnHomo(0, "X")
    End Select
End Sub

Private Sub pcfHabBtnHomo(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    ' Control de que la petici�n este clasificada antes de intentar guardar un conforme de homologaci�n
    If cModeloControl = MODELO_CONTROL_NUEVO Then
        If glPETClase = "SINC" Then
            MsgBox "Antes de poder informar un conforme de homologaci�n, la petici�n debe ser clasificada.", vbInformation + vbOKOnly, "Clasificaci�n requerida"
            Exit Sub
        End If
    End If
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjDOC).Enabled = True
            orjBtn(orjADJ).Enabled = True
            lblPetConfHomo = "": lblPetConfHomo.Visible = False
            grdPetConf.Enabled = True
            grdPetConf.SetFocus
            cmdHomoAdd.Enabled = False
            cmdHomoDel.Enabled = False
            cmdHomoConf.Enabled = False
            cmdHomoCancel.Enabled = False
            pcfTextoHomo.Locked = True
            If IsMissing(vCargaGrid) Then
                Call CargarPetConf
            Else
                Call pcfSeleccion
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjDOC).Enabled = False
            orjBtn(orjADJ).Enabled = False
            fraButtPpal.Enabled = False
            grdPetConf.Enabled = False
            cmdHomoAdd.Enabled = False
            cmdHomoDel.Enabled = False
            cmdHomoConf.Enabled = True
            cmdHomoCancel.Enabled = True
            Select Case sOpcionSeleccionada
                Case "A"
                    lblPetConfHomo = " AGREGAR ": lblPetConfHomo.Visible = True
                    pcfTextoHomo = ""
                    pcfTextoHomo.Locked = False
                    fraConformeHomo.Enabled = True
                    Call setHabilCtrl(pcfTextoHomo, "NOR")
                    Call setHabilCtrl(cboPcfTipoHomo, "NOR")
                    cboPcfTipoHomo.SetFocus
                Case "M"
                    lblPetConfHomo = " MODIFICAR ": lblPetConfHomo.Visible = True
                    fraConformeHomo.Enabled = True
                    pcfTextoHomo.Locked = False
                    Call setHabilCtrl(pcfTextoHomo, "NOR")
                    Call setHabilCtrl(cboPcfTipoHomo, "DIS")
                    pcfTextoHomo.SetFocus
                Case "E"
                    lblPetConfHomo = " DESVINCULAR ": lblPetConfHomo.Visible = True
                    fraConformeHomo.Enabled = True
                    cmdHomoConf.SetFocus
            End Select
    End Select
    pcfOpcion = sOpcionSeleccionada
End Sub
'}

Sub pcfHabBtn(nOpcion As Integer, sOpcionSeleccionada As String, Optional vCargaGrid As Variant)
    'If glPeticion_sox001 = 1 Then      ' add -004- e. ' del -007- a.
    If cModeloControl = MODELO_CONTROL_NUEVO Then           ' add -007- a.
        '{ add -002- k.
        If glPETClase = "SINC" Then
            MsgBox "Antes de poder informar un conforme a la petici�n, �sta debe ser clasificada.", vbInformation + vbOKOnly, "Clasificaci�n requerida"
            Exit Sub
        End If
        '}
    End If      ' add -004- e.
    Call setHabilCtrl(cboPcfModo, "DIS")
    Select Case nOpcion
        Case 0
            fraButtPpal.Enabled = True
            orjBtn(orjDAT).Enabled = True
            orjBtn(orjMEM).Enabled = True
            orjBtn(orjANX).Enabled = True
            orjBtn(orjDOC).Enabled = True
            orjBtn(orjADJ).Enabled = True
            orjBtn(orjEST).Enabled = True
            lblPetConf = "": lblPetConf.Visible = False
            grdPetConf.Enabled = True
            grdPetConf.SetFocus
            pcfAdd.Enabled = False
            pcfDel.Enabled = False
            pcfCon.Enabled = False
            pcfCan.Enabled = False
            pcfTexto.Locked = True
            If IsMissing(vCargaGrid) Then
                Call CargarPetConf
            Else
                Call pcfSeleccion
            End If
        Case 1
            orjBtn(orjDAT).Enabled = False
            orjBtn(orjMEM).Enabled = False
            orjBtn(orjANX).Enabled = False
            orjBtn(orjDOC).Enabled = False
            orjBtn(orjADJ).Enabled = False
            orjBtn(orjEST).Enabled = False
            
            fraButtPpal.Enabled = False

            grdPetConf.Enabled = False
            pcfAdd.Enabled = False
            pcfDel.Enabled = False
            pcfCon.Enabled = True
            pcfCan.Enabled = True
            Select Case sOpcionSeleccionada
                Case "A"
                    lblPetConf = " AGREGAR ": lblPetConf.Visible = True
                    pcfTexto = ""
                    pcfTexto.Locked = False
                    fraConforme.Enabled = True
                    Call setHabilCtrl(pcfTexto, "NOR")
                    Call setHabilCtrl(cboPcfTipo, "NOR")
                    'Call SetCombo(cboPcfModo, "REQ", True)     ' del -065- a.
                    '{ add -065- a.
                    If PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = "DESA" Then  ' upd -065- b.
                        Call SetCombo(cboPcfModo, "ALT", True)
                    Else
                        Call SetCombo(cboPcfModo, "REQ", True)
                    End If
                    '}
                    If PerfilInternoActual = "SADM" Then
                        Call setHabilCtrl(cboPcfModo, "NOR")
                    End If
                    '{ add -099- a.
                    If PerfilInternoActual = "CSEC" Then
                        If sp_GetPeticionSector(glNumeroPeticion, glLOGIN_Sector) Then
                            Call setHabilCtrl(pcfTexto, "Nor")
                        Else
                            Call setHabilCtrl(pcfTexto, "OBL")
                        End If
                    End If
                    '}
                Case "M"
                    lblPetConf = " MODIFICAR ": lblPetConf.Visible = True
                    fraConforme.Enabled = True
                    pcfTexto.Locked = False
                    Call setHabilCtrl(pcfTexto, "NOR")
                    Call setHabilCtrl(cboPcfTipo, "DIS")
                    If PerfilInternoActual = "SADM" Then
                        Call setHabilCtrl(cboPcfModo, "NOR")
                    End If
                    pcfTexto.SetFocus
                Case "E"
                    lblPetConf = " DESVINCULAR ": lblPetConf.Visible = True
                    fraConforme.Enabled = False
            End Select
    End Select
    pcfOpcion = sOpcionSeleccionada
End Sub

'{ add -037- a.
Private Sub txtTitulo_Change()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Caracteres disponibles: " & txtTitulo.MaxLength - Len(txtTitulo)
    End If
End Sub

Private Sub lblPeticionNumero_Click()
    If lblPeticionNumero.Caption = "N� Asignado" Then
        lblPeticionNumero.ForeColor = vbBlue
        lblPeticionNumero.Caption = "N� Interno"
        txtNroAsignado.Visible = False
        txtNrointerno.Visible = True
    Else
        lblPeticionNumero.ForeColor = vbBlack
        lblPeticionNumero.Caption = "N� Asignado"
        txtNroAsignado.Visible = True
        txtNrointerno.Visible = False
    End If
End Sub

Private Sub HabilitarProyectos()
    Call setHabilCtrl(txtPrjId, "NOR")
    Call setHabilCtrl(txtPrjSubId, "NOR")
    Call setHabilCtrl(txtPrjSubsId, "NOR")
    'Call setHabilCtrl(txtPrjNom, "NOR")
    cmdSelProyectos.Enabled = True
    cmdEraseProyectos.Enabled = True
End Sub

Private Sub InhabilitarProyectos()
    Call setHabilCtrl(txtPrjId, "DIS")
    Call setHabilCtrl(txtPrjSubId, "DIS")
    Call setHabilCtrl(txtPrjSubsId, "DIS")
    Call setHabilCtrl(txtPrjNom, "DIS")
    cmdSelProyectos.Enabled = False
    cmdEraseProyectos.Enabled = False
End Sub

Private Sub txtPrjSubsId_LostFocus()
    If Val(txtPrjId) + Val(txtPrjSubId) + Val(txtPrjSubsId) > 0 Then
        ValidarProyecto txtPrjId, txtPrjSubId, txtPrjSubsId
    End If
End Sub

Private Function ValidarProyecto(ProjId, ProjSubId, ProjSubSId) As Boolean
    ' Si inicializan los valores para evitar errores en runtime por Type Missmatch
    If IsNumeric(ProjId) Then
        ProjId = IIf(ProjId > 0, ProjId, 0)
    Else
        ProjId = 0
    End If
    If IsNumeric(ProjSubId) Then
        ProjSubId = IIf(ProjSubId > 0, ProjSubId, 0)
    Else
        ProjSubId = 0
    End If
    If IsNumeric(ProjSubSId) Then
        ProjSubSId = IIf(ProjSubSId > 0, ProjSubSId, 0)
    Else
        ProjSubSId = 0
    End If
    ' Si no existe ningun dato para proyectos (Proyecto = 0,0,0), entonces no hay que validar nada.
    ' Al usuario no le interesa grabar datos de proyecto
    If ProjId + ProjSubId + ProjSubSId > 0 Then
        If Not sp_GetProyectoIDM(ProjId, ProjSubId, ProjSubSId, Null, Null, Null, Null, Null) Then
            MsgBox "El proyecto ingresado no es v�lido. Revise.", vbExclamation + vbOKOnly, "Proyecto IDM inv�lido"
            ValidarProyecto = False
            txtPrjId.SelStart = 0: txtPrjId.SelLength = Len(txtPrjId)
            txtPrjId.SetFocus
            Call HabilitarVisibilidad("Estandar")
            Exit Function
        Else
            If InStr(1, "DESEST|TERMIN|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                MsgBox "El proyecto ingresado no es v�lido. Revise.", vbExclamation + vbOKOnly, "Proyecto IDM inv�lido"
                ValidarProyecto = False
                txtPrjId.SelStart = 0: txtPrjId.SelLength = Len(txtPrjId)
                txtPrjId.SetFocus
                Call HabilitarVisibilidad("Estandar")
                Exit Function
            End If
        End If
        txtPrjNom.Text = Trim(aplRST.Fields!projnom)
        ValidarProyecto = True
        Call HabilitarVisibilidad("Proyectos")
    Else
        ValidarProyecto = False
        txtPrjNom.Text = ""
        Call HabilitarVisibilidad("Estandar")
    End If
End Function

Private Sub HabilitarVisibilidad(modo As String)
    Dim bImportanciaMaxima As Boolean
    
    'bImportanciaMaxima = True
    
    Select Case modo
        Case "Estandar"
            With cboImportancia
                .Clear
                If sp_GetImportancia(Null, Null) Then
                    Do While Not aplRST.EOF
                        .AddItem Trim(aplRST!nom_importancia) & ESPACIOS & "||" & Trim(aplRST!cod_importancia)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    aplRST.Close
                End If
            End With
            cboImportancia.ListIndex = PosicionCombo(cboImportancia, xImportancia, True)
            Call setHabilCtrl(cboImportancia, "NOR")
        Case "Proyectos"
            ' Por definici�n, se fuerza la Visibilidad al m�ximo nivel
            With cboImportancia
                .Clear
                If sp_GetImportancia(Null, Null) Then
                    If Not aplRST.EOF Then
                        Do While Not aplRST.EOF
                            .AddItem Trim(aplRST!nom_importancia) & ESPACIOS & "||" & Trim(aplRST!cod_importancia)
                            aplRST.MoveNext
                            DoEvents
                        Loop
                        aplRST.Close
                    End If
                End If
                .ListIndex = 0
            End With
            ' NO puede modificar (Ultima definici�n de A. Ocampo)
            Call setHabilCtrl(cboImportancia, "DIS")
    End Select
End Sub

Private Function NombreProyecto(ProjId As Long, ProjSubId As Long, ProjSubSId As Long) As String
    NombreProyecto = ""
    If ProjId + ProjSubId + ProjSubSId > 0 Then
        If spProyectoIDM.sp_GetProyectoIDM(ProjId, ProjSubId, ProjSubSId, Null, Null, Null, Null, Null) Then
            NombreProyecto = Trim(aplRST.Fields!projnom)
        End If
    End If
End Function

Private Sub txtTitulo_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        If txtTitulo.Enabled Then
            lblCustomToolTip.Caption = "T�tulo descriptivo para esta petici�n. Este ser� el utilizado en todos los reportes del sistema." & vbCrLf & "Longitud m�xima permitida: " & txtTitulo.MaxLength & " caracteres."
        End If
    End If
End Sub

Private Sub cboTipopet_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica el tipo de petici�n: Normal, Propia, Especial, Auditor�a, etc."
    End If
End Sub

Private Sub cboClase_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Especifica la clase de la petici�n: Nuevo desarrollo, Mantenimiento Evolutivo, Correctivo, etc. (condicionado por el tipo de la petici�n). Vea la ayuda en l�nea."
    End If
End Sub

Private Sub cmdAyudaClase_Click()
    Select Case CodigoCombo(cboClase, True)
        Case "NUEV"
            frmToolTip.Titulo = "Nuevo Desarrollo"
            frmToolTip.Texto = "Petici�n de cambio de funcionalidad que se resuelve por la instalaci�n del nuevo aplicativo (siempre tratada como PROYECTO)."
        Case "EVOL"
            frmToolTip.Titulo = "Mantenimiento Evolutivo"
            frmToolTip.Texto = "Nace como consecuencia de la necesidad de hacer evolucionar una aplicaci�n incorporando, eliminando o modificando FUNCIONALIDADES de la misma."
        Case "OPTI"
            frmToolTip.Titulo = "Optimizaciones"
            frmToolTip.Texto = "Tienen como finalidad MEJORAR la calidad y el rendimiento sin tener consideraci�n de desarrollo, ampliaci�n, modificaci�n o eliminaci�n de funcionalidades. Son modificaciones para la adaptaci�n a entornos tecnol�gicos y/o mejoras generales siempre que no impliquen cambios funcionales."
        Case "ATEN"
            frmToolTip.Titulo = "Atenci�n de Usuarios"
            frmToolTip.Texto = "Reflejo de necesidad planteada como una consulta acerca del funcionamiento de las aplicaciones sobre las que se est� ofreciendo el soporte y que genera la creaci�n y modificaci�n de componentes y/o ejecuci�n de procesos con datos."
        Case "CORR"
            frmToolTip.Titulo = "Mantenimiento correctivo"
            frmToolTip.Texto = "Tienen como objetivo LOCALIZAR Y ELIMINAR LOS ERRORES de la aplicaci�n o proceso. Se utiliza para resoluci�n de incidencias, prevenci�n de errores, y monitorizaci�n y/o seguimiento de ejecuciones de procesos con el objeto de verificar en modo on-line el funcionamiento."
        Case "SPUF"
            frmToolTip.Titulo = "SPUFIs"
            frmToolTip.Texto = "Clase para uso excepcional. Ejecuci�n directa en ambiente de producci�n de actualizaci�n de datos como salida inmediata para permitir la continuidad del servicio."
        Case Else
            MsgBox "Debe seleccionar una clase para ver la definici�n de la misma.", vbInformation + vbOKOnly, "Seleccionar clase"
    End Select
    If CodigoCombo(cboClase, True) <> "SINC" Then
        frmToolTip.Show 1
        cboClase_GotFocus
    End If
End Sub

Private Sub cboImpacto_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Cuando es necesario adaptar o ampliar el entorno tecnol�gico disponible."
    End If
End Sub

Private Sub cboRegulatorio_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Requerimientos de cumplimiento obligatorio, a partir de exigencias de entes reguladores externos."
    End If
End Sub

Private Sub cboPrioridad_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Establece un nivel de prioridad para la petici�n."
    End If
End Sub

Private Sub cboImportancia_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Establece el nivel de visibilidad de acuerdo a su importancia (a mayor importancia, mayor nivel jer�rquico de visibilidad)."
    End If
End Sub

Private Sub txtusrImportancia_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica el usuario que estableci� por �ltima vez la visibilidad"
    End If
End Sub

Private Sub txtPrjId_GotFocus()
    txtPrjId.SelStart = 0: txtPrjId.SelLength = Len(txtPrjId)
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Puede seleccionar proyectos existentes presionando el bot�n con la lupa."
    End If
End Sub

Private Sub txtPrjSubId_GotFocus()
    txtPrjSubId.SelStart = 0: txtPrjSubId.SelLength = Len(txtPrjSubId)
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Puede seleccionar proyectos existentes presionando el bot�n con la lupa."
    End If
End Sub

Private Sub txtPrjSubsId_GotFocus()
    txtPrjSubsId.SelStart = 0: txtPrjSubsId.SelLength = Len(txtPrjSubsId)
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Puede seleccionar proyectos existentes presionando el bot�n con la lupa."
    End If
End Sub

Private Sub cmdSelProyectos_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Presione este bot�n para seleccionar proyectos preexistentes."
    End If
End Sub

Private Sub cmdSelProyectos_Click()
    Load frmProyectosIDMSeleccion
    frmProyectosIDMSeleccion.Show 1
    With frmProyectosIDMSeleccion
        txtPrjId.Text = .lProjId
        txtPrjSubId.Text = .lProjSubId
        txtPrjSubsId.Text = .lProjSubSId
        txtPrjNom.Text = Trim(.cProjNom)
        Call ValidarProyecto(.lProjId, .lProjSubId, .lProjSubSId)
        txtPrjSubsId.SetFocus
    End With
End Sub

Private Sub cmdEraseProyectos_Click()
    If MsgBox("�Confirma la eliminaci�n del proyecto IDM asociado a esta petici�n?", vbQuestion + vbYesNo, "Proyecto IDM") = vbYes Then
        txtPrjId.Text = ""
        txtPrjSubId.Text = ""
        txtPrjSubsId.Text = ""
        txtPrjNom.Text = ""
        ValidarProyecto 0, 0, 0
    End If
End Sub

Private Sub txtPrjNom_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Establece el nombre descriptivo del proyecto asociado a esta petici�n"
    End If
End Sub

Private Sub cboCorpLocal_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Determina si es una petici�n a nivel local o a nivel corporativo"
    End If
End Sub

Private Sub cboOrientacion_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtEstado_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "El estado de la petici�n es el resultado consolidado de los sectores y grupos involucrados en esta petici�n (en rojo estados terminales; en verde aquellas en estados activos)."
    End If
End Sub

Private Sub txtSituacion_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "La situaci�n es el resultado de ciertas condiciones a un momento particular producto de procesos de escalamiento, vencimientos autom�ticos, etc."
    End If
End Sub

Private Sub txtFechaEstado_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Fecha en que se alcanz� el estado actual de la petici�n."
    End If
End Sub

Private Sub txtNroAnexada_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "N� de petici�n a la cual se ha anexado la petici�n actual. Puede acceder a la petici�n destino a trav�s del bot�n."
    End If
End Sub

Private Sub txtHspresup_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtHsPlanif_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFiniplan_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFinireal_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFfinplan_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub txtFfinreal_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = ""
    End If
End Sub

Private Sub cboSector_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica el sector solicitante de la petici�n actual."
    End If
End Sub

Private Sub txtUsualta_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Legajo y nombre del usuario real que carg� la petici�n actual en el sistema."
    End If
End Sub

Private Sub txtFechaPedido_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "La fecha real en que la petici�n fu� cargada en el sistema."
    End If
End Sub

Private Sub cboSolicitante_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Solicitante formal de la petici�n actual"
    End If
End Sub

Private Sub txtFechaRequerida_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica la fecha deseada por el solicitante para la finalizaci�n de esta petici�n"
    End If
End Sub

Private Sub cboReferente_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Referente del solicitante de la petici�n."
    End If
End Sub

Private Sub cboSupervisor_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Supervisor del solicitante de la petici�n."
    End If
End Sub

Private Sub cboDirector_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Autorizante del solicitante de la petici�n"
    End If
End Sub

Private Sub cboBpar_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Indica el Business Partner asociado a la petici�n actual"
    End If
End Sub

Private Sub txtFechaComite_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "Fecha en la cual fu� entregada al Business Partner"
    End If
End Sub

Private Sub txtNroAsignado_GotFocus()
    If glModoPeticion = "ALTA" Or glModoPeticion = "MODI" Then
        lblCustomToolTip.Caption = "N�mero secuencial identificatorio asignado autom�ticamente para peticiones aprobadas."
    End If
End Sub

'{ add -039- a.
Private Function EsTerminal(pet_estado As String) As Boolean
    EsTerminal = False
    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD|", ClearNull(pet_estado), vbTextCompare) > 0 Then
        EsTerminal = True
    End If
End Function
'}

''{ add -040- a.
'Private Sub cboRegulatorio_Click()
'    If InStr(1, "ALTA|MODI", glModoPeticion, vbTextCompare) > 0 Then
'        'Call setHabilCtrl(cboPrioridad, "NOR")     ' del -078- a.
'        If InStr(1, "BPAR|CSEC|CGRU", PerfilInternoActual, vbTextCompare) > 0 Then
'            If cboRegulatorio.List(cboRegulatorio.ListIndex) = "Si" Then
'                '{ del -078- a.
'                'cboPrioridad.ListIndex = 0
'                'Call setHabilCtrl(cboPrioridad, "DIS")
'                '}
'            End If
'        End If
'    End If
'End Sub
''}

'{ add -046- a.
Private Sub Form_Unload(Cancel As Integer)
    Call InicializarGrillas(False)
End Sub
'}

'{ add -051- b.
Private Sub cboClase_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboClase.ListIndex = 0
    End If
End Sub

Private Sub cboTipopet_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboTipopet.ListIndex = 0
    End If
End Sub

Private Sub cboImpacto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboImpacto.ListIndex = 0
    End If
End Sub

Private Sub cboRegulatorio_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboRegulatorio.ListIndex = 0
    End If
End Sub

Private Sub cboImportancia_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboImportancia.ListIndex = 0
    End If
End Sub

Private Sub cboCorpLocal_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboCorpLocal.ListIndex = 0
    End If
End Sub

Private Sub cboOrientacion_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboOrientacion.ListIndex = 0
    End If
End Sub

Private Sub cboSector_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboSector.ListIndex = 0
    End If
End Sub

Private Sub cboSolicitante_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboSolicitante.ListIndex = 0
    End If
End Sub

Private Sub cboReferente_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboReferente.ListIndex = 0
    End If
End Sub

Private Sub cboSupervisor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboSupervisor.ListIndex = 0
    End If
End Sub

Private Sub cboDirector_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboDirector.ListIndex = 0
    End If
End Sub

Private Sub cboBpar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboBpar.ListIndex = 0
    End If
End Sub
'}

'{ add -065- a.
Private Sub ValidarPeticion()
    With imgTrazabilidad
        .Visible = False
        If glNumeroPeticion <> "" Then
            If sp_GetPeticionChangeMan(glNumeroPeticion, Null, Null) Then
                If Not aplRST.EOF Then
                    .Visible = True
                    .ToolTipText = ""
                    'If glProgramOwner = FuncionesGenerales.cstmGetUserName() Then
                    If SoyElAnalista Then
                        .ToolTipText = "La petici�n es v�lida desde " & Format(aplRST.Fields!pet_date, "dd.mm.yyyy") & " a las " & Format(aplRST.Fields!pet_date, "hh:mm")
                    End If
                End If
            End If
        End If
    End With
End Sub
'}

'{ add -078- a.
Private Sub cmdPlanificar_Click()
    frmPeticionesPlanificarDyd.Show 1
End Sub
'}

' BAJADA DE VARIOS DOCUMENTOS ADJUNTOS, COMPRESION DE LOS MISMOS
' Y SUBIDA NUEVAMENTE A LA PETICION EN LA DDBB.
Private Sub cmdComprimir_Click()
    Dim i, H As Long
    Dim Str As String
    Dim cFileName As String
    Dim cExecString As String
    
    Const WINRAR_PATH = "c:\Archivos de Programa\WinRAR"
    
    With grdAdjPet
        Status ("Iniciando la compresi�n...")
        Call Puntero(True)
        If sp_GetAdjuntosPet(glNumeroPeticion, Null, Null, Null) Then
            Str = Str & glNumeroPeticion & vbCrLf
            If Not aplRST.EOF Then
                Do While Not aplRST.EOF
                    If aplRST.Fields!adj_iscompress = "No" Then
                        i = i + 1
                        cFileName = ClearNull(aplRST.Fields!adj_file)
                        Status ("Copiando... " & i & " de " & aplRST.RecordCount)
                        PeticionAdjunto2File glNumeroPeticion, aplRST.Fields!adj_tipo, aplRST.Fields!adj_file, glWRKDIR
                        ' Armo el log
                        Str = Str & Format(CInt(FileLen(glWRKDIR & cFileName) / 1024), "@@@@") & "|" & cFileName & "|" & ClearNull(aplRST.Fields!audit_user) & vbCrLf
                        ' Armo la cadena para comprimir el archivo
                        cExecString = WINRAR_PATH & "\winrar.exe a -m5 -ep " & Chr(34) & glWRKDIR & cFileName & ".zip" & Chr(34) & " " & Chr(34) & glWRKDIR & cFileName & Chr(34)
                        H = Shell(cExecString, vbHide)
                        ' Una vez "zipeado", lo levanto y le cambio el nombre
                        sp_InsertPeticionAdjunto glNumeroPeticion, Null, Null, aplRST.Fields!adj_tipo, glWRKDIR, cFileName & ".zip", "El zipeado."
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
                doFile "log.txt", Str
                aplRST.Close
            End If
        End If
    End With
    Call Puntero(False)
    Status ("Listo.")
    MsgBox "Proceso finalizado."
End Sub

'{
' ************************************************************************************************************************************
' ORDENAMIENTO
' ************************************************************************************************************************************
Private Sub Ordenamiento(ByRef grdDatos As MSFlexGrid)
    bSorting = True
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
            .Visible = False
            .Col = .MouseCol
            If ClearNull(.TextMatrix(0, .Col)) = "Ord." Or IsNumeric(.TextMatrix(1, .Col)) Then
                If Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortNumericDescending
                ElseIf Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortNumericAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortNumericDescending
                End If
            Else
                If Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortStringNoCaseDescending
                ElseIf Left(.TextMatrix(0, .Col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .Col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortStringNoCaseAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .Col) = "� " & .TextMatrix(0, .Col)
                    .Sort = flexSortStringNoCaseDescending
                End If
            End If
            lUltimaColumnaOrdenada = .Col
            .Visible = True
        End If
    End With
    bSorting = False
End Sub
'}

Private Function CamposObligatoriosConf() As Boolean
    If cboPcfTipo.ListIndex < 0 Then
        MsgBox "Falta especificar el tipo de conforme.", vbOKOnly + vbInformation
        CamposObligatoriosConf = False
        Exit Function
    End If
    If cboPcfModo.ListIndex < 0 Then
        MsgBox "Falta especificar exenci�n.", vbOKOnly + vbInformation
        CamposObligatoriosConf = False
        Exit Function
    End If
    '{ add -099- a.
    If sTipoPet = "PRO" Then
        If InStr(1, "OKPP|OKPF|", CodigoCombo(cboPcfTipo, True), vbTextCompare) > 0 Then
            If Not (PerfilInternoActual = "CSEC" And glLOGIN_Sector = xSec) Then
                MsgBox "Solo responsables de Sector pueden otorgar este tipo de conforme.", vbOKOnly + vbInformation
                CamposObligatoriosConf = False
                Exit Function
            End If
        End If
    End If
    If sTipoPet = "PRO" Then
        If PerfilInternoActual = "CSEC" And glLOGIN_Gerencia = "DESA" Then
            If InStr(1, "TEST|TESP|", CodigoCombo(cboPcfTipo, True), vbTextCompare) > 0 Then
                If Not sp_GetPeticionSector(glNumeroPeticion, glLOGIN_Sector) Then
                    If ClearNull(pcfTexto) = "" Then
                        MsgBox "Debe ingresar en comentarios una justificaci�n.", vbOKOnly + vbInformation
                        CamposObligatoriosConf = False
                        Exit Function
                    End If
                End If
            End If
        End If
    End If
    '}
    
    CamposObligatoriosConf = True
End Function

Private Sub CargarGrupoHomologador()
    ' Obtengo el grupo Homologador
    If sp_GetGrupoHomologacion Then
        cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
        cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
    End If
    ' Obtiene el estado actual del grupo Homologaci�n para la petici�n
    If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, cGrupoHomologador) Then
        cGrupoHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
    End If
    ' Obtiene el estado actual del sector del grupo Homologaci�n para la petici�n
    If sp_GetPeticionSector(glNumeroPeticion, cGrupoHomologadorSector) Then
        cSectorHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
    End If
End Sub

'{ del -101- b.
''{ add -083- a.
'Private Sub Cargar_Adjuntos(iTipoAdjunto As Integer)
'    Dim i As Long
'    Select Case iTipoAdjunto
'        Case ADJUNTOS_NORMAL
'            With cboAdjClass
'                .Clear
'                For i = 0 To UBound(vTipoDocumento)
'                    If vTipoDocumento(i).tipo <> "HOMO" Then
'                        .AddItem vTipoDocumento(i).codigo & " : " & vTipoDocumento(i).Nombre
'                    End If
'                Next i
'                .ListIndex = -1
'            End With
'        Case ADJUNTOS_HOMO
'            With cboAdjHomoClass
'                .Clear
'                For i = 0 To UBound(vTipoDocumento)
'                    If vTipoDocumento(i).tipo = "HOMO" Then
'                        .AddItem vTipoDocumento(i).codigo & " : " & vTipoDocumento(i).Nombre
'                    End If
'                Next i
'                .ListIndex = -1
'            End With
'        Case ADJUNTOS_ANTERIOR
'            With cboAdjClass
'                .Clear
'                .AddItem "REQU : Requerimiento"
'                .AddItem "ALCA : Doc. de alcance desarrollo"
'                .AddItem "COST : Evaluaci�n costo - beneficio"
'                .AddItem "PLEJ : Plan de ejecuci�n"
'                .AddItem "PLIM : Plan de implantaci�n Desarrollo"
'                .AddItem "MAIL : Mail"
'                .AddItem "OTRO : Otros"
'                .ListIndex = -1
'            End With
'            With cboAdjHomoClass
'                .Clear
'                .AddItem "IDH1 : Informe de Homologaci�n HSOB"
'                .AddItem "IDH2 : Informe de Homologaci�n HOME"
'                .AddItem "IDH3 : Informe de Homologaci�n HOMA"
'                .AddItem "OTRO : Otros documentos"
'                .ListIndex = 0
'            End With
'    End Select
'End Sub
'}

Private Sub cmdDeshabilitarManual_Click()
    If MsgBox("Esto fuerza la deshabilitaci�n de la" & vbCrLf & "petici�n para pasajes a Producci�n." & vbCrLf & "(Grupo homologador)" & vbCrLf & vbCrLf & "�Desea continuar?", vbExclamation + vbOKCancel, "Deshabilitar petici�n") = vbOK Then
        Call Puntero(True)
        Call sp_DeletePeticionChangeMan(glNumeroPeticion)
        Call ValidarPeticion
        Call Puntero(False)
        MsgBox "Proceso finalizado", vbInformation + vbOKOnly
    End If
End Sub

Private Sub InicializarToolTipsControles()
    txtFechaRequerida.ToolTipText = "Fecha tentativa deseada por el solicitante para la resoluci�n de esta petici�n."
    cboSolicitante.ToolTipText = "Solicitante formal de la petici�n actual"
    txtFechaPedido.ToolTipText = "La fecha real en que la petici�n fu� cargada en el sistema"
    txtUsualta.ToolTipText = "Indica el usuario real que carg� la petici�n actual en el sistema"
    cboSector.ToolTipText = "Indica el sector solicitante de la petici�n actual"
    txtFfinreal.ToolTipText = ""
    txtFfinplan.ToolTipText = ""
    txtFinireal.ToolTipText = ""
    txtFiniplan.ToolTipText = ""
    txtHsPlanif.ToolTipText = ""
    txtHspresup.ToolTipText = ""
    txtNroAnexada.ToolTipText = "N� de petici�n a la cual se ha anexado la petici�n actual"
    txtFechaEstado.ToolTipText = "Fecha en que se alcanz� el estado actual de la petici�n"
    txtSituacion.ToolTipText = "Indica la situaci�n particular de la petici�n"
    txtEstado.ToolTipText = "Indica el estado general consolidado de la petici�n"
    cboOrientacion.ToolTipText = ""
    cboCorpLocal.ToolTipText = "Diferencia �mbito Corporativo de �mbito local."
    txtPrjNom.ToolTipText = "Nombre descriptivo del proyecto (en el marco del Informe a Direcci�n de Medios) del que la presente petici�n forma parte."
    cmdSelProyectos.ToolTipText = "Selecci�n de proyectos preexistentes"
    cmdEraseProyectos.ToolTipText = "Borrar la informaci�n de proyecto de la petici�n actual"
    txtPrjSubsId.ToolTipText = "Subc�digo de subproyecto"
    txtPrjSubId.ToolTipText = "C�digo de subproyecto"
    txtPrjId.ToolTipText = "C�digo principal de proyecto"
    txtusrImportancia.ToolTipText = "Indica el usuario que estableci� por �ltima vez la visibilidad"
    cboImportancia.ToolTipText = "Establece el nivel de visibilidad de acuerdo a su importancia"
    cboPrioridad.ToolTipText = "Establece un nivel de prioridad para la petici�n"
    cboRegulatorio.ToolTipText = "Requerimientos de cumplimiento obligatorio, a partir de exigencias de entes reguladores externos"
    cboImpacto.ToolTipText = "Cuando es necesario adaptar o ampliar el entorno tecnol�gico disponible."
    cboClase.ToolTipText = "Especifica la clase de la petici�n: Nuevo desarrollo, Mantenimiento Evolutivo, Correctivo, etc."
    cboTipopet.ToolTipText = "Indica el tipo de petici�n: Normal, Propia, Especial, Auditor�a, etc."
    txtTitulo.ToolTipText = "T�tulo descriptivo para esta petici�n"
    txtNroAsignado.ToolTipText = "N�mero identificatorio asignado autom�ticamente para peticiones aprobadas"
    txtFechaComite.ToolTipText = "Fecha inicial en estado Al Business Partner."
    cboBpar.ToolTipText = "Indica el Business Partner asociado a la petici�n actual"
    cboDirector.ToolTipText = "Autorizante del solicitante de la petici�n"
    cboReferente.ToolTipText = "Referente del solicitante de la petici�n"
    cboSupervisor.ToolTipText = "Supervisor del solicitante de la petici�n"
End Sub
'}

'{ add -101- b.
Private Sub CargaCombosConValorFijo()
    Dim i As Integer
    
    '{ add -095- a.
    With cboEmp
        .Clear
        .AddItem "1. BCO" & ESPACIOS & "||1"
        .AddItem "2. CON" & ESPACIOS & "||2"
        .AddItem "3. PSA" & ESPACIOS & "||3"
        .AddItem "4. RCF" & ESPACIOS & "||4"
        .ListIndex = 0
        Call setHabilCtrl(cboEmp, "DIS")
    End With
    '}
    '{ add -097- a.
    With cboRO
        .Clear
        .AddItem "No" & ESPACIOS & "||-"
        .AddItem "ALTA " & ESPACIOS & "||A"
        .AddItem "MEDIA" & ESPACIOS & "||M"
        .AddItem "BAJA " & ESPACIOS & "||B"
        .ListIndex = -1
    End With
    '}
    '{ add -064- a.
    With cmbValidarEstado
        .Clear
        .AddItem "Validez para pasaje a Producci�n" & ESPACIOS & "||P"
        .AddItem "Validez para finalizaci�n" & ESPACIOS & "||F"
        .ListIndex = 0
        Call setHabilCtrl(cmbValidarEstado, "DIS")
    End With
    '}
    '{ add -010- a. Conformes de Homologaci�n
    With cboPcfTipoHomo
        .Clear
        .AddItem "HSOB.P : Conf. parcial Homologaci�n - S/ observ."
        .AddItem "HOME.P : Conf. parcial Homologaci�n - C/ observ. menores"
        .AddItem "HOMA.P : No conf. parcial Homologaci�n - C/ observ. mayores"
        .AddItem "HSOB.F : Conf. final Homologaci�n - S/ observ."
        .AddItem "HOME.F : Conf. final Homologaci�n - C/ observ. menores"
        .AddItem "HOMA.F : No conf. final Homologaci�n - Con observ. mayores"
    End With
    '}
    ' Opciones de: Corporativo o Local
    With cboCorpLocal
        .Clear
        .AddItem "Local" & ESPACIOS & "||L"
        .AddItem "Corporativa" & ESPACIOS & "||C"
        .ListIndex = 0
    End With
    ' Regulatorio
    With cboRegulatorio
        .Clear
        .AddItem "--" & ESPACIOS & "||-"
        .AddItem "No" & ESPACIOS & "||N"
        .AddItem "Si" & ESPACIOS & "||S"
        .ListIndex = 0
    End With
    ' Impacto tecnol�gico
    With cboImpacto
        .Clear
        .AddItem "--" & ESPACIOS & "||-"
        .AddItem "No" & ESPACIOS & "||N"
        .AddItem "Si" & ESPACIOS & "||S"
        .ListIndex = -1
    End With
    '{ add -083- a. Carga el listado de los tipos de documentos para adjuntar (habilitados)
    Erase vTipoDocumento
    If sp_GetDocumento(Null, "S") Then
        Do While Not aplRST.EOF
            ReDim Preserve vTipoDocumento(i)
            vTipoDocumento(i).codigo = ClearNull(aplRST.Fields!cod_doc)
            vTipoDocumento(i).Nombre = ClearNull(aplRST.Fields!nom_doc)
            If InStr(1, ClearNull(aplRST.Fields!cod_doc), "IDH", vbTextCompare) > 0 Then
                vTipoDocumento(i).tipo = "HOMO"
            Else
                vTipoDocumento(i).tipo = "DYD"
            End If
            aplRST.MoveNext
            i = i + 1
            DoEvents
        Loop
    End If
    '}
    ' **************************************************************************************************************
    ' Conformes para peticiones
    ' **************************************************************************************************************
    With cboPcfTipo
        .Clear
        .AddItem "OK Parcial Documento Alcance" & ESPACIOS & "||ALCP"
        .AddItem "OK Final   Documento Alcance" & ESPACIOS & "||ALCA"
        .AddItem "OK Parcial Prueba Usuario" & ESPACIOS & "||TESP"
        .AddItem "OK Final   Prueba Usuario" & ESPACIOS & "||TEST"
        .AddItem "OK Parcial Pasaje a Producci�n s/ conf. Homolog." & ESPACIOS & "||OKPP"
        .AddItem "OK Final   Pasaje a Producci�n s/ conf. Homolog." & ESPACIOS & "||OKPF"
    End With
    
    With cboPcfModo
        .Clear
        .AddItem "Requerido" & ESPACIOS & "||REQ"         ' Ojo aca con la leyenda agregada
        .AddItem "Exenci�n" & ESPACIOS & "||EXC"
        .AddItem "Alternativo" & ESPACIOS & "||ALT"       ' add -065- a.
    End With
    
    ' Carga de adjuntos
    With cboAdjClass
        .Clear
        For i = 0 To UBound(vTipoDocumento)
            .AddItem vTipoDocumento(i).codigo & " : " & vTipoDocumento(i).Nombre
        Next i
        .ListIndex = -1
    End With
    
End Sub
'}

'Private Sub Validar_Peticion()
'    ' Conformes
'    Dim flgALCP As Boolean
'    Dim flgTESP As Boolean
'    Dim flgALCA As Boolean
'    Dim flgTEST As Boolean
'
'    ' Conformes de homologaci�n
'
'    ' Documentaci�n
'    Dim pet_Clase As String
'    Dim pet_tipo As String
'    Dim pet_P950 As String
'    Dim pet_C204 As String
'    Dim pet_T710 As String
'
'    ' Obtener los datos principales de la petici�n
'    If sp_GetUnaPeticion(glNumeroPeticion) Then
'
'    End If
'End Sub

'Private Sub Cargar_AdjuntosHomo()
'    With cboAdjHomoClass
'        .Clear
'        For i = 0 To UBound(vTipoDocumento)
'            If vTipoDocumento(i).Tipo = "HOMO" Then
'                .AddItem vTipoDocumento(i).codigo & " : " & vTipoDocumento(i).Nombre
'            End If
'        Next i
'        .ListIndex = -1
'    End With
'End Sub

'Private Sub Cargar_AdjuntosAnteriores()
'    With cboAdjClass
'        .Clear
'        .AddItem "REQU : Requerimiento"
'        .AddItem "ALCA : Doc. de alcance desarrollo"
'        .AddItem "COST : Evaluaci�n costo - beneficio"
'        .AddItem "PLEJ : Plan de ejecuci�n"
'        .AddItem "PLIM : Plan de implantaci�n Desarrollo"
'        .AddItem "MAIL : Mail"
'        .AddItem "OTRO : Otros"
'        .ListIndex = -1
'    End With
'
'    With cboAdjHomoClass
'        .Clear
'        .AddItem "IDH1 : Informe de Homologaci�n HSOB"
'        .AddItem "IDH2 : Informe de Homologaci�n HOME"
'        .AddItem "IDH3 : Informe de Homologaci�n HOMA"
'        .AddItem "OTRO : Otros documentos"
'        .ListIndex = 0
'    End With
'End Sub
'}

'Private Sub pcfCon_Click()
'    Dim sPcfTipo As String
'    Dim sPcfModo As String
'    Dim bOKLider_Homologacion As Boolean    ' add -065- a.
'    '{ add -059- b.
'    Dim iCantidad_Conformes As Integer
'    Dim iCantidad_Respaldos As Integer
'    '}
'    '{ add -new- !!!
'    Dim bHomologacion_EstadoActivo As Boolean
'    Dim bExistenGruposHomologables As Boolean
'
'    bHomologacion_EstadoActivo = False
'    bExistenGruposHomologables = False
'    '}
'
'    If cboPcfTipo.ListIndex < 0 Then
'        MsgBox "Falta especificar el tipo de conforme.", vbOKOnly + vbInformation
'        Exit Sub
'    End If
'    If cboPcfModo.ListIndex < 0 Then
'        MsgBox "Falta especificar exenci�n.", vbOKOnly + vbInformation
'        Exit Sub
'    End If
'
'    sPcfTipo = CodigoCombo(cboPcfTipo, True)
'    sPcfModo = CodigoCombo(cboPcfModo, True)
'
'    '{ add -020- a.
'    ' Si se pretende agregar un conforme sobre las observaciones de Homologaci�n
'    If pcfOpcion = "A" Then
'        '{ add -043- a.
'        If sPcfTipo = "TESP" Then
'            If InStr(1, "CORR|SPUF|ATEN", glPETClase, vbTextCompare) > 0 Then
'                MsgBox "Peticiones de clase Correctivo, Atenci�n a Usuarios y Spufis" & vbCrLf & "no pueden recibir conformes de pruebas de usuario parciales.", vbOKOnly + vbExclamation
'                Exit Sub
'            End If
'        End If
'        '}
'
'        '{ add -059- a.
'        If InStr(1, "CORR|SPUF|ATEN|OPTI", glPETClase, vbTextCompare) > 0 Then
'            If PerfilInternoActual = "CGRU" Then
'                If glLOGIN_Gerencia = "DESA" Then
'                    ' Si para las clases indicadas, es un Resp. de Ejecuci�n de DYD, y
'                    ' quiere dar un conforme de alcance, lo rechazo
'                    If InStr(1, "ALCP|ALCA", sPcfTipo, vbTextCompare) > 0 Then
'                        MsgBox "Vd. no puede otorgar conformes a la documentaci�n de alcance, pero si puede otorgar conformes" & vbCrLf & "a las pruebas del usuario (con la documentaci�n respaldatoria correspondiente)", vbOKOnly + vbExclamation
'                        Exit Sub
'                    End If
'                    iCantidad_Respaldos = 0
'                    iCantidad_Conformes = 0
'                    If InStr(1, "TESP|TEST", sPcfTipo, vbTextCompare) > 0 Then
'                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML2", Null, Null) Then
'                            MsgBox "Debe adjuntar un documento de tipo EML2 para respaldar" & vbCrLf & "el otorgamiento del conforme a las pruebas del usuario.", vbOKOnly + vbExclamation
'                            Exit Sub
'                        '{ add -059- b.
'                        Else
'                            ' Cuento la cantidad de respaldos
'                            Do While Not aplRST.EOF
'                                iCantidad_Respaldos = iCantidad_Respaldos + 1
'                                aplRST.MoveNext
'                                DoEvents
'                            Loop
'                            ' Cuento la cantidad de conformes de tipo "Alternativos" (L�deres DYD)
'                            If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
'                                Do While Not aplRST.EOF
'                                    If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
'                                        iCantidad_Conformes = iCantidad_Conformes + 1
'                                    End If
'                                    aplRST.MoveNext
'                                    DoEvents
'                                Loop
'                                ' Comparo las cantidades contadas
'                                If iCantidad_Conformes >= iCantidad_Respaldos Then
'                                    ' Los conformes superan a los respaldos
'                                    MsgBox "No hay suficientes respaldos (EML2) para la cantidad de conformes a otorgar." & vbCrLf & "Revise en la solapa Adjuntos.", vbOKOnly + vbExclamation
'                                    Exit Sub
'                                End If
'                            End If
'                        '}
'                        End If
'                    End If
'                End If
'            End If
'        End If
'        '}
'        If InStr(1, "OKPP|OKPF", sPcfTipo, vbTextCompare) > 0 Then
'            Dim bResponsableHabilitado As Boolean
'            bResponsableHabilitado = False
'            ' primero evalua si el usuario actua como Resp. de Sector
'            If PerfilInternoActual <> "CSEC" Then
'                MsgBox "Solo Responsables de Sector de las gerencias de DyD, Organizaci�n o Transformaci�n y Apoyo a Red involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & "El usuario actual no actua como Responsable de Sector.", vbOKOnly + vbExclamation
'                Exit Sub
'            End If
'            ' Antes se evalua si existe un conforme de homologaci�n de tipo HOMA
'            If Not bExisteHOMA Then
'                MsgBox "No existe un conforme de Homologaci�n de tipo HOMA en la petici�n.", vbOKOnly + vbExclamation
'                Exit Sub
'            End If
'            ' Debe ser y actuar como un responsable de sector
'            If PerfilInternoActual = "CSEC" Then
'                ' Si es Responsable de Sector, veo que lo sea de alguno de los sectores de la petici�n
'                If sp_GetPeticionSector(glNumeroPeticion, Null) Then
'                    Do While Not aplRST.EOF
'                        ' Si es el Responsable de Sector de uno de los sectores de la petici�n y pertenece a DyD,
'                        ' lo dejo agregar el conforme para Homologaci�n
'                        If glLOGIN_Sector = Trim(aplRST!cod_sector) And (Trim(aplRST!cod_gerencia) = "DESA" Or Trim(aplRST!cod_gerencia) = "ORG." Or Trim(aplRST!cod_gerencia) = "TRYAPO") Then
'                            bResponsableHabilitado = True
'                            Exit Do
'                        End If
'                        aplRST.MoveNext
'                        DoEvents
'                    Loop
'                    If Not bResponsableHabilitado Then
'                        MsgBox "Solo Responsables de Sector de las gerencias de DyD, Organizaci�n o Transformaci�n y Apoyo a Red involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & "El Responsable actual no pertenece a DyD.", vbOKOnly + vbExclamation
'                        Exit Sub
'                    End If
'                Else
'                    MsgBox "El Responsable actual no pertenece a ninguno de los sectores ejecutantes de la petici�n." & vbCrLf & vbCrLf & "No puede agregar este tipo de conforme.", vbOKOnly + vbExclamation
'                    Exit Sub
'                End If
'            Else
'                MsgBox "Solo Responsables de Sector de las gerencias de DyD, Organizaci�n o Transformaci�n y Apoyo a Red involucrados en la petici�n pueden agregar este tipo de conforme." & vbCrLf & vbCrLf & "El usuario actual no actua como Responsable de Sector.", vbOKOnly + vbExclamation
'                Exit Sub
'            End If
'        End If
'    End If
'    '}
'
'    If pcfOpcion = "A" Then     ' add -040- f.
'        ' Ok final al documento de alcance
'        If sPcfTipo = "ALCA" And sPcfModo = "REQ" Then
'            If Not cModeloControl = MODELO_CONTROL_NUEVO Then     ' add -001- i.
'                If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA", Null, Null) Then
'                    MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                    Exit Sub
'                Else
'                    aplRST.Close
'                End If
'            '{ add -001- i. Else para nuevo esquema
'            Else
'                '{ add -008- a.
'                ' Si usuario actual tiene perfil de BP de la petici�n actual valida
'                ' que exista el mail de OK al documento de alcance
'                If Not CodigoCombo(cboTipopet, True) = "PRO" Then
'                    If Not flgSolicitor Then    ' add -020- a.
'                        If (PerfilInternoActual = "BPAR" And Trim(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or _
'                            InEjecutorOrganizacion Or _
'                            EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
'                            If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML1", Null, Null) Then
'                                MsgBox "La petici�n no posee el Mail de OK de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                                Exit Sub
'                            Else
'                                aplRST.Close
'                            End If
'                        End If
'                    End If      ' add -020- a.
'                End If
'                '}
'                If glPETTipo = "PRJ" Or glPETImpacto = "S" Then      ' Proyecto o tiene impacto tecnol�gico
'                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
'                        MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                        Exit Sub
'                    Else
'                        aplRST.Close
'                    End If
'                Else
'                    If glPETClase = "EVOL" Or glPETClase = "OPTI" Or glPETClase = "NUEV" Then
'                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
'                            MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                            Exit Sub
'                        Else
'                            aplRST.Close
'                        End If
'                    Else
'                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
'                            MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                            Exit Sub
'                        End If
'                    End If
'                End If
'            End If
'            '}
'        End If
'        ' OK parcial al documento de alcance
'        If sPcfTipo = "ALCP" Then
'            If Not cModeloControl = MODELO_CONTROL_NUEVO Then     ' add -001- i.
'                If Not sp_GetAdjuntosPet(glNumeroPeticion, "ALCA", Null, Null) Then
'                    MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                    Exit Sub
'                Else
'                    aplRST.Close
'                End If
'            '{ add -001- i. Else para nuevo esquema
'            Else
'                '{ add -008- a.
'                ' Si usuario actual tiene perfil de BP de la petici�n actual valida que exista el mail de OK al documento de alcance
'                If Not CodigoCombo(cboTipopet, True) = "PRO" Then
'                    If Not flgSolicitor Then    ' add -020- a.
'                        If (PerfilInternoActual = "BPAR" And Trim(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or _
'                            InEjecutorOrganizacion Or _
'                            EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
'                            If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML1", Null, Null) Then
'                                MsgBox "La petici�n no posee el Mail de OK de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                                Exit Sub
'                            Else
'                                aplRST.Close
'                            End If
'                        End If
'                    End If  ' add -020- a.
'                End If
'                '}
'                If glPETTipo = "PRJ" Or glPETImpacto = "S" Then          ' Proyectos con impacto tecnol�gico
'                    If Not sp_GetAdjuntosPet(glNumeroPeticion, "C100", Null, Null) Then
'                        MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                        Exit Sub
'                    Else
'                        aplRST.Close
'                    End If
'                Else
'                    If glPETClase = "EVOL" Or glPETClase = "OPTI" Or glPETClase = "NUEV" Then
'                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
'                            MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                            Exit Sub
'                        Else
'                            aplRST.Close
'                        End If
'                    Else
'                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "P950", Null, Null) Then
'                            MsgBox "La petici�n no posee documentos de Alcance (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                            Exit Sub
'                        End If
'                    End If
'                End If
'            End If
'            '}
'            '{ add -068- a. Si ya existe un ALCA, no puedo seguir agregando ALCP
'            If sp_GetPeticionConf(glNumeroPeticion, "ALCA", Null) Then
'                MsgBox "La petici�n ya posee un conforme final a la documentaci�n de alcance." & vbCrLf & _
'                       "No puede otorgar conformes parciales posteriores.", vbOKOnly + vbInformation
'                Exit Sub
'            End If
'            '}
'        End If
'    End If      ' add -040- f.
'
'    ' CONTROLES SOBRE OTORGACION DE CONFORMES DE USUARIO
'    'If (Not flgSolicitor) And PerfilInternoActual <> "SADM" And glLOGIN_Gerencia <> "ORG." Then            ' del -001- o.
'    If pcfOpcion = "A" Then     ' Solo hago este control si quiero agregar un conforme
'        'If Not (PerfilInternoActual = "CSEC" And glLOGIN_Direccion = "MEDIO" And glPETTipo = "PRO") Then    ' add -001- o. ' del -059- a.
'        '{ add -059- a.
'        If Not (PerfilInternoActual = "CSEC" And glLOGIN_Direccion = "MEDIO" And glPETTipo = "PRO") And _
'           Not ((PerfilInternoActual = "CSEC" Or PerfilInternoActual = "CGRU") And glLOGIN_Gerencia = "DESA" And InStr(1, "CORR|SPUF|ATEN|OPTI", glPETClase, vbTextCompare) > 0) Then
'        '}
'            If (Not flgSolicitor) And PerfilInternoActual <> "SADM" And (glLOGIN_Gerencia <> "ORG." And glLOGIN_Gerencia <> "TRYAPO") Then    ' upd -017- a. Se agrega TRYAPO
'                If sPcfTipo = "TESP" Then
'                    MsgBox "Solo pueden dar ok Parciales de Prueba de Usuario" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
'                    Exit Sub
'                End If
'                If sPcfTipo = "TEST" Then
'                    MsgBox "Solo pueden dar ok de Prueba de Usuario" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
'                    Exit Sub
'                End If
'                If sPcfTipo = "ALCA" Then
'                    MsgBox "Solo pueden dar ok a Documentos de Alcance" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
'                    Exit Sub
'                End If
'                If sPcfTipo = "ALCP" Then
'                    MsgBox "Solo pueden dar ok Parciales a Documentos de Alcance" & Chr(13) & "los usuarios de Organizaci�n y los Solicitantes", vbOKOnly + vbInformation
'                    Exit Sub
'                End If
'            End If
'        End If          ' add -001- o.
'    End If
'
'    If pcfOpcion = "A" Then     ' add -040- f.
'        If sPcfTipo = "TEST" Then
'            If cModeloControl = MODELO_CONTROL_NUEVO Then     ' add -008- d. - Controles nuevos (SOx)
'                '{ add -xxx- 10.01.2008
'                If PerfilInternoActual <> "SADM" And InStr("EJECUC", sEstado) = 0 Then
'                    MsgBox "El estado de la petici�n no permite otorgar un conforme de pruebas final." & vbCrLf & _
'                           "Solo peticiones en estado �En Ejecuci�n� pueden recibir conformes de pruebas de usuario.", vbOKOnly + vbInformation, "Conformes de pruebas de usuario"
'                    Exit Sub
'                End If
'                '}
'                '{ add -008- a.
'                ' Si usuario actual tiene perfil de BP de la petici�n actual valida que exista
'                ' el mail de OK a las Pruebas de Usuario
'                If Not CodigoCombo(cboTipopet, True) = "PRO" Then
'                    If (PerfilInternoActual = "BPAR" And Trim(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or _
'                        InEjecutorOrganizacion Or _
'                        EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
'                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML2", Null, Null) Then
'                            MsgBox "La petici�n no posee el Mail de OK de Pruebas de Usuario (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                            Exit Sub
'                        Else
'                            aplRST.Close
'                        End If
'                    End If
'                End If
'                '}
'            '{ add -xxx- 10.01.2008
'            Else
'                If PerfilInternoActual <> "SADM" And InStr("ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA", sEstado) = 0 Then
'                    MsgBox "El estado de la petici�n no permite declarar un conforme de pruebas final.", vbOKOnly + vbInformation, "Conformes de pruebas de usuario"
'                    Exit Sub
'                End If
'            '}
'            End If  ' add -008- d.
'        End If
'    End If      ' add -040- f.
'
'    If pcfOpcion = "A" Then     ' add -040- f.
'        If sPcfTipo = "TESP" Then
'            If cModeloControl = MODELO_CONTROL_NUEVO Then     ' add -008- d. - Controles nuevos (SOx)
'                If PerfilInternoActual <> "SADM" And InStr("EJECUC", sEstado) = 0 Then
'                    MsgBox "El estado de la petici�n no permite declarar un conforme de prueba parcial." & vbCrLf & _
'                           "Solo peticiones en estado � En Ejecuci�n � pueden recibir conformes de prueba de usuario.", vbOKOnly + vbInformation, "Conformes de usuario"
'                    Exit Sub
'                End If
'                '{ add -008- a.
'                ' Si usuario actual tiene perfil de BP de la petici�n actual valida que exista el mail de OK a las Pruebas de Usuario
'                If Not CodigoCombo(cboTipopet, True) = "PRO" Then
'                    If (PerfilInternoActual = "BPAR" And Trim(sBpar) = Trim(glLOGIN_ID_REEMPLAZO)) Or _
'                        InEjecutorOrganizacion Or _
'                        EstaRelacionadoAlBP(glLOGIN_Grupo, glLOGIN_Sector) Then
'                        If Not sp_GetAdjuntosPet(glNumeroPeticion, "EML2", Null, Null) Then
'                            MsgBox "La petici�n no posee el Mail de OK de Pruebas de Usuario (solapa 'Adjuntos')", vbOKOnly + vbInformation
'                            Exit Sub
'                        Else
'                            aplRST.Close
'                        End If
'                    End If
'                End If
'            Else
'                If PerfilInternoActual <> "SADM" And InStr("ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA", sEstado) = 0 Then
'                    MsgBox "El estado de la petici�n no permite declarar un conforme de prueba de usuario.", vbOKOnly + vbInformation, "Conformes de usuario"
'                    Exit Sub
'                End If
'            '}
'            End If  ' add -008- d.
'        End If
'    End If      ' add -040- f.
'
'    Select Case pcfOpcion
'        Case "A"
'            If MsgBox(constConfirmMsgAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then      ' add -002- e.
'                ' Esto es forzado... vaya a saber dios el oscuro
'                ' motivo que pergue�o el programador original...
'                If sPcfTipo = "TESP" Then       ' Ok parcial de prueba
'                    If Not (glLOGIN_Gerencia = "DESA" And PerfilInternoActual = "CGRU") Then
'                        sPcfModo = "EXC"
'                        sPcfSecuencia = "0"
'                    Else
'                        sPcfModo = "ALT"
'                        sPcfSecuencia = "0"
'                    End If
'                ElseIf sPcfTipo = "ALCP" Then   ' Ok parcial al documento de alcance
'                    sPcfModo = "EXC"
'                    sPcfSecuencia = "0"
'                Else
'                    sPcfSecuencia = "1"
'                End If
'                If sp_InsertPeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, pcfTexto.Text, sPcfSecuencia, glLOGIN_ID_REEMPLAZO) Then     ' upd -010- a. - Se agrega el par�metro glLOGIN_ID_REEMPLAZO
'                    If sPcfTipo = "ALCA" Then
'                        Call sp_AddHistorial(glNumeroPeticion, "OKALCA", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
'                    End If
'                    If sPcfTipo = "ALCP" Then
'                        Call sp_AddHistorial(glNumeroPeticion, "OKALCP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
'                    End If
'                    If sPcfTipo = "TEST" Then
'                        Call sp_AddHistorial(glNumeroPeticion, "OKTEST", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
'                    End If
'                    ' Esto es nuevo: ahora se registra en el historial quien asigno un conforme parcial (28.12.2007)
'                    If sPcfTipo = "TESP" Then
'                        Call sp_AddHistorial(glNumeroPeticion, "OKTESP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
'                    End If
'                    '{ add -010- a.
'                    If sPcfTipo = "OKPP" Then
'                        Call sp_AddHistorial(glNumeroPeticion, "OKPP", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
'                    End If
'                    If sPcfTipo = "OKPF" Then
'                        Call sp_AddHistorial(glNumeroPeticion, "OKPF", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)
'                    End If
'                    '}
'                    '{ add -010- a.
'                    ' Se cambia el estado del grupo Homologador a "En ejecuci�n" cuando la petici�n recibe un conforme (tanto parcial como final) de usuario o de supervisor
'                    If sPcfTipo = "TESP" Or sPcfTipo = "TEST" Or sPcfTipo = "OKPP" Or sPcfTipo = "OKPF" Then
'                        ' Esto es para la cadena HOST-ChangeMan
'                        Dim sSector, sGrupo As String
'                        sSector = "": sSector = ClearNull(glLOGIN_Sector)
'                        sGrupo = "": sGrupo = ClearNull(glLOGIN_Grupo)
'                        ' Determina si ya existe para la petici�n el grupo Homologador
'                        If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
'                            'If Not aplRST.EOF Then
'                            ' Variables para la parte de Homologaci�n
'                            Dim cGrupoHomologador As String
'                            Dim cGrupoHomologadorSector As String
'                            Dim cGrupoHomologadorEstado As String
'                            Dim cSectorHomologadorEstado As String
'                            Dim cSectorHomologadorNuevoEstado As String
'                            Dim NroHistorial As Long
'                            Dim NuevoEstadoPeticion As String
'                            ' Obtengo el grupo Homologador
'                            If sp_GetGrupoHomologacion Then
'                                'If Not aplRST.EOF Then
'                                cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
'                                cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
'                                'End If
'                            End If
'                            ' Obtiene el estado actual del grupo Homologaci�n para la petici�n
'                            If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, cGrupoHomologador) Then
'                                If Not aplRST.EOF Then
'                                    cGrupoHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
'                                    aplRST.Close
'                                End If
'                            End If
'                            ' Obtiene el estado actual del sector del grupo Homologaci�n para la petici�n
'                            If sp_GetPeticionSector(glNumeroPeticion, cGrupoHomologadorSector) Then
'                                If Not aplRST.EOF Then
'                                    cSectorHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
'                                    aplRST.Close
'                                End If
'                            End If
'                            ' Determina si el grupo homologador se encuentra en alguno de los estados "Activos"
'                            If cGrupoHomologadorEstado <> "EJECUC" Then
'                                If Not InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN", ClearNull(cGrupoHomologadorEstado)) > 0 Then
'                                    ' Se envian los mensajes correspondientes a Responsables del Grupo "Homologador"
'                                    'Call sp_DelMensajePerfil(glNumeroPeticion, "CGRU", "GRUP", cGrupoHomologador)  ' del -088- a.
'                                    Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, cGrupoHomologador, "EJECUC", "", "")
'                                    ' Entonces cambia el estado del grupo homologador a "En ejecuci�n"
'                                    Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "ESTADO", "EJECUC", Null, Null)
'                                    'Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, , Date, "", 0, 0)     ' upd -010- a.
'                                    ' Actualiza el estado del Sector del grupo Homologador
'                                    cSectorHomologadorNuevoEstado = getNuevoEstadoSector(glNumeroPeticion, cGrupoHomologadorSector)
'                                    ' Se registra en el historial el evento autom�tico de cambio de estado del grupo Homologador
'                                    NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", sEstado, cGrupoHomologadorSector, cSectorHomologadorNuevoEstado, cGrupoHomologador, "EJECUC", glLOGIN_ID_REEMPLAZO, "� EN EJECUCION � Cambio autom�tico de estado del Grupo Homologador.")
'                                    Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "EJECUC", date, "", NroHistorial, 0)                 ' upd -010- a.
'                                    Call sp_UpdatePeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, cSectorHomologadorNuevoEstado, date, "", NroHistorial, 0)
'                                    ' Se registra en el historial el evento autom�tico de cambio de estado del grupo Homologador
'                                    'NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", sEstado, cGrupoHomologadorSector, cSectorHomologadorNuevoEstado, cGrupoHomologador, "EJECUC", glLOGIN_ID_REEMPLAZO, "� EN EJECUCION � Cambio autom�tico de estado del Grupo Homologador.")
'                                    bHomologacion_EstadoActivo = True
'                                End If
'                            Else
'                                '{ add -new-!!!!
'                                If InStr("RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN", ClearNull(cGrupoHomologadorEstado)) = 0 Then
'                                    bHomologacion_EstadoActivo = True
'                                End If
'                                '}
'                            End If
'                            ' Peticiones clase NUEV, EVOL u OPTI:
'                            ' -----------------------------------
'                            ' Si existe un conforme de tipo HOMA, debe existir tambi�n un conforme de tipo OKPP o
'                            ' si existe un conforme de tipo HSOB o HOME no necesita el OK del supervisor
'
'                            '{ add -new- !!!
'                            ' Antes de hacer m�s controles evaluo si alguno de los grupos vinculados
'                            ' a la petici�n esta activado como Homologable. Si existe al menos 1, entonces
'                            ' se activan los controles de Homologaci�n. Si no hay ninguno, entonces la petici�n
'                            ' no es "Homologable", por lo cual agrego la cadena para el envio a Changeman.
'                            If spGrupo.sp_GetPetGrupoHomologable(glNumeroPeticion) Then
'                                If Not aplRST.EOF Then
'                                    bExistenGruposHomologables = True
'                                End If
'                            End If
'                            '}
'                            If bHomologacion_EstadoActivo And bExistenGruposHomologables Then  ' add -new-!!!
'                                If Not InStr(1, "CORR|ATEN|SPUF", glPETClase) > 0 Then
'                                    ' Control de existencia de conforme de homologaci�n tipo HOMA
'                                    If sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Or _
'                                       sp_GetPeticionConf(glNumeroPeticion, "HOMA.F", Null) Then
'                                        ' Entonces busco si existe el conforme de Responsable de Sector a las observaciones de Homologaci�n
'                                        If sp_GetPeticionConf(glNumeroPeticion, "OKPP", Null) Or _
'                                           sp_GetPeticionConf(glNumeroPeticion, "OKPF", Null) Then
'                                                '{ add -027- a.
'                                                ' Si existe el conforme del usuario (parcial o final) entonces paso la cadena a Changeman
'                                                If (sp_GetPeticionConf(glNumeroPeticion, "TEST", Null)) Or _
'                                                   (sp_GetPeticionConf(glNumeroPeticion, "TESP", Null)) Then
'                                                    ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                                    FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                                    '{ add -034- a.
'                                                    ' Guardo en el historial el evento de la habilitaci�n para pasaje a Producci�n
'                                                    'Call sp_AddHistorial(glNumeroPeticion, "HABPROD", sEstado, glLOGIN_Sector, Null, glLOGIN_Grupo, Null, glLOGIN_ID_REEMPLAZO, "�  Se habilit� la petici�n para pasaje a Producci�n  �")
'                                                    '}
'                                                End If
'                                                '}
'                                                '{ del -027- a.
'                                                '' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                                '}
'                                        End If
'                                    Else
'                                        If (sp_GetPeticionConf(glNumeroPeticion, "HSOB.P", Null) Or _
'                                            sp_GetPeticionConf(glNumeroPeticion, "HSOB.F", Null)) Or _
'                                           (sp_GetPeticionConf(glNumeroPeticion, "HOME.P", Null) Or _
'                                            sp_GetPeticionConf(glNumeroPeticion, "HOME.F", Null)) Then
'                                                '{ add -027- a.
'                                                ' Si existe el conforme del usuario (parcial o final) entonces paso la cadena a Changeman
'                                                If (sp_GetPeticionConf(glNumeroPeticion, "TEST", Null)) Or _
'                                                   (sp_GetPeticionConf(glNumeroPeticion, "TESP", Null)) Then
'                                                    ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                                    FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                                    '{ add -034- a.
'                                                    ' Guardo en el historial el evento de la habilitaci�n para pasaje a Producci�n
'                                                    'Call sp_AddHistorial(glNumeroPeticion, "HABPROD", sEstado, glLOGIN_Sector, Null, glLOGIN_Grupo, Null, glLOGIN_ID_REEMPLAZO, "�  Se habilit� la petici�n para pasaje a Producci�n  �")
'                                                    '}
'                                                End If
'                                                '}
'                                                '{ del -027- a.
'                                                '' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                                '}
'                                        End If
'                                    End If
'                                Else
'                                    ' Peticiones clase CORR, ATEN o SPUF:
'                                    ' -----------------------------------
'                                    ' Si existe un HOMA, tengo que tener el OK del supervisor para pasar a Producci�n
'                                    If sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Or _
'                                       sp_GetPeticionConf(glNumeroPeticion, "HOMA.F", Null) Then
'                                        ' Entonces busco si existe el conforme de Responsable de Sector a las observaciones de Homologaci�n
'                                        If sp_GetPeticionConf(glNumeroPeticion, "OKPP", Null) Or _
'                                           sp_GetPeticionConf(glNumeroPeticion, "OKPF", Null) Then
'                                                '{ add -027- a.
'                                                ' Si existe el conforme del usuario (parcial o final) entonces paso la cadena a Changeman
'                                                If (sp_GetPeticionConf(glNumeroPeticion, "TEST", Null)) Or _
'                                                   (sp_GetPeticionConf(glNumeroPeticion, "TESP", Null)) Then
'                                                        ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                                        FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                                        '{ add -034- a.
'                                                        ' Guardo en el historial el evento de la habilitaci�n para pasaje a Producci�n
'                                                        'Call sp_AddHistorial(glNumeroPeticion, "HABPROD", sEstado, glLOGIN_Sector, Null, glLOGIN_Grupo, Null, glLOGIN_ID_REEMPLAZO, "�  Se habilit� la petici�n para pasaje a Producci�n  �")
'                                                        '}
'                                                End If
'                                                '}
'                                                '{ del -027- a.
'                                                ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                                '}
'                                        End If
'                                    Else
'                                        '{ add -065- a. Requiere Conforme de Homologaci�n para habilitar el pasaje a Prod. de peticiones ATEN, CORR y SPUF cuando el OK
'                                        '               es otorgado por un l�der de DYD.
'                                        If PerfilInternoActual = "CGRU" And glLOGIN_Gerencia = "DESA" Then  ' upd -065- b.
'                                            bOKLider_Homologacion = False
'                                            If sp_GetVarios("CTRLPRD1") Then
'                                                If Not aplRST.EOF Then
'                                                    If ClearNull(aplRST.Fields!var_numero) = 1 Then
'                                                        bOKLider_Homologacion = True
'                                                    End If
'                                                End If
'                                            End If
'
'                                            If bOKLider_Homologacion Then
'                                                If (sp_GetPeticionConf(glNumeroPeticion, "HSOB.P", Null)) Or _
'                                                   (sp_GetPeticionConf(glNumeroPeticion, "HSOB.F", Null)) Or _
'                                                   (sp_GetPeticionConf(glNumeroPeticion, "HOME.P", Null)) Or _
'                                                   (sp_GetPeticionConf(glNumeroPeticion, "HOME.F", Null)) Then
'                                                     FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                                End If
'                                            Else
'                                                FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                            End If
'                                        Else
'                                            FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                        End If
'                                        '}
'                                        '{ del -065- a.
'                                        '' No tiene HOMA: agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                        'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"
'                                        '}
'                                        '{ add -034- a.
'                                        ' Guardo en el historial el evento de la habilitaci�n para pasaje a Producci�n
'                                        'Call sp_AddHistorial(glNumeroPeticion, "HABPROD", sEstado, glLOGIN_Sector, Null, glLOGIN_Grupo, Null, glLOGIN_ID_REEMPLAZO, "�  Se habilit� la petici�n para pasaje a Producci�n  �")
'                                        '}
'                                    End If
'                                End If
'                            '{ add -new-!!! El grupo homologador se encuentra en alguno de los estados terminales
'                            Else
'                                '{ add -063- b.
'                                If Not sPcfModo = "ALT" Then
'                                    FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"    ' El grupo homologador no se encuentra ya en un estado activo, por ende agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                Else
'                                    bOKLider_Homologacion = False
'                                    If sp_GetVarios("CTRLPRD1") Then
'                                        If Not aplRST.EOF Then
'                                            If ClearNull(aplRST.Fields!var_numero) = 1 Then
'                                                bOKLider_Homologacion = True
'                                            End If
'                                        End If
'                                    End If
'                                    If Not bOKLider_Homologacion Then
'                                        FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"    ' El grupo homologador no se encuentra ya en un estado activo, por ende agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                    End If
'                                End If
'                                '}
'                                '{ del -063- b.
'                                'FuncionesHOST.GesPetExportToHOST glNumeroPeticion, glNroAsignado, glPETClase, sSector, sGrupo, glLOGIN_ID_REAL, "02"    ' El grupo homologador no se encuentra ya en un estado activo, por ende agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                '}
'                            End If
'                            '}
'                            'End If
'                        End If
'                    End If
'                    '{ add -047- a. Agrega un mensaje para el grupo Homologador
'                    '{ add -047- b.
'                    If sp_GetPetGrupoHomologacion(glNumeroPeticion) Then
'                        'If Not aplRST.EOF Then
'                    '}
'                        ' Obtengo el grupo Homologador
'                        If sp_GetGrupoHomologacion Then
'                            'If Not aplRST.EOF Then
'                            cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
'                            cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
'                            'End If
'                        End If
'                        Select Case sPcfTipo
'                            Case "ALCA", "ALCP"
'                                Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 100, sEstado, Null)
'                            Case "TEST", "TESP"
'                                Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 101, sEstado, Null)
'                            Case "OKPP", "OKPF"
'                                Call spMensajes.sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", cGrupoHomologador, 102, sEstado, Null)
'                        End Select
'                    '{ add -047- b.
'                        'End If
'                    End If
'                    '}
'                    Call pcfHabBtn(0, "X")
'                End If
'            End If              ' add -002- e.
'            '{ add -059- a.
'            If InStr(1, "CORR|SPUF|ATEN|OPTI|", glPETClase, vbTextCompare) > 0 Then
'                If PerfilInternoActual = "CGRU" Then
'                    If glLOGIN_Gerencia = "DESA" Then
'                        If InStr(1, "TESP|TEST", sPcfTipo, vbTextCompare) > 0 Then
'                            ' Debo evaluar si debo "reactivar" al grupo Homologador
'                            If sp_GetGrupoHomologacion Then
'                                'If Not aplRST.EOF Then
'                                ' Obtiene el c�digo de grupo y sector de Homologaci�n
'                                cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
'                                cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
'                                'End If
'                            End If
'                            If sp_GetPeticionGrupo(glNumeroPeticion, cGrupoHomologadorSector, cGrupoHomologador) Then
'                                ' Si homologaci�n esta en alguno de los estados terminales, lo "reactivo"
'                                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
'                                    Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "ESTADO", "EJECUC", Null, Null)
'                                    ' Actualizo el estado del sector de Homologaci�n (si corresponde)
'                                    cSectorHomologadorEstado = getNuevoEstadoSector(glNumeroPeticion, cGrupoHomologadorSector)
'                                    Call sp_UpdatePeticionSector(glNumeroPeticion, cGrupoHomologadorSector, Null, Null, Null, Null, 0, cSectorHomologadorEstado, date, "", 0, 0)
'                                    NroHistorial = sp_AddHistorial(glNumeroPeticion, "AGCHGEST", "EJECUC", cGrupoHomologadorSector, cSectorHomologadorEstado, cGrupoHomologador, "EJECUC", glLOGIN_ID_REEMPLAZO, "� EN EJECUCION � Cambio autom�tico de estado del Grupo Homologador por conforme de L�der.")
'                                    Call sp_UpdatePetSubField(glNumeroPeticion, cGrupoHomologador, "HSTSOL", Null, Null, NroHistorial)
'                                End If
'                            End If
'                        End If
'                    End If
'                End If
'            End If
'            '}
'            ValidarPeticion     ' add -065- a.
'        Case "E"
'            If MsgBox(constConfirmMsgDeAttachConformes, vbQuestion + vbOKCancel, "Confirmaci�n") = vbOK Then        ' add -002- e.
'                If sp_DeletePeticionConf(glNumeroPeticion, sPcfTipo, sPcfModo, sPcfSecuencia) Then
'                    Call sp_AddHistorial(glNumeroPeticion, "OK" & sPcfTipo, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "Eliminaci�n " & TextoCombo(cboPcfModo, sPcfModo, True) & "  --->  " & pcfTexto.Text)    ' add -059- c.
'                    '{ add -090- a.
'                    If InStr(1, "TESP|TEST|ALCP|ALCA|", sPcfTipo, vbTextCompare) > 0 Then
'                        Call sp_DeletePeticionEnviadas(glNumeroPeticion)
'                    End If
'                    '}
'                    pcfTexto = ""
'                    Call pcfHabBtn(0, "X")
'                End If
'            End If              ' add -002- e.
'            ValidarPeticion     ' add -065- a.
'        Case Else
'            pcfTexto = ""
'            Call pcfHabBtn(0, "X")
'    End Select
'End Sub

'Private Sub cmdHomoConf_Click()
'    Dim cTipoHomo As String
'    Dim cMensaje As String
'    Dim bOKLider_Homologacion As Boolean        ' add -065- a.
'    Dim bOKLider As Boolean                     ' add -065- a.
'    Dim bOK_SI As Boolean                       ' add -093- c.
'
'    '{ add -093- c.
'    bOK_SI = False
'    If sp_GetVarios("CTRLPET1") Then
'        If ClearNull(aplRST.Fields!var_numero) = "1" Then
'            bOK_SI = True
'        End If
'    End If
'    '}
'
'
'    If cboPcfTipoHomo.ListIndex < 0 Then            ' Control de selecci�n de tipo de conforme de Homologaci�n
'        MsgBox "Falta especificar el tipo de conforme de Homologaci�n.", vbOKOnly + vbInformation, "Homologaci�n"
'        Exit Sub
'    End If
'    cTipoHomo = CodigoCombo(cboPcfTipoHomo, False)  ' Guardo en una variable la clave de indentificaci�n del conforme de Homologaci�n
'
'    Select Case pcfOpcion
'        Case "A"
'            ' Verifica que no exista el tipo de conforme de homologaci�n ya en la base
'            If sp_GetPeticionConf(glNumeroPeticion, cTipoHomo, Null) Then
'                MsgBox "Ya existe ese conforme de Homologaci�n para esta petici�n. Revise.", vbExclamation + vbOKOnly, "Homologaci�n"
'                Exit Sub
'            End If
'            ' {mov -005-}
'            ' Agrega el conforme para la petici�n y genera un registro del evento en el historial
'            If MsgBox("�Confirma el conforme de Homologaci�n seleccionado?", vbQuestion + vbOKCancel, "Agregar conforme de Homologaci�n") = vbOK Then
'                If sp_InsertPeticionConf(glNumeroPeticion, cTipoHomo, "N/A", pcfTextoHomo.Text, "1", glLOGIN_ID_REEMPLAZO) Then  ' upd -010- a. - Se agrega el par�metro glLOGIN_ID_REEMPLAZO
'                    Call sp_AddHistorial(glNumeroPeticion, cTipoHomo, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, TextoCombo(cboPcfTipoHomo, cTipoHomo, False) & ": " & pcfTextoHomo.Text)
'                End If
'                Call pcfHabBtnHomo(0, "X")
'                ' Evaluo si la petici�n debe pasar a HOST: esto es para la cadena HOST-ChangeMan
'                'Dim sSector, sGrupo As String
'                'sSector = "": sSector = ClearNull(glLOGIN_Sector)
'                'sGrupo = "": sGrupo = ClearNull(glLOGIN_Grupo)
'                If InStr(1, "EVOL|NUEV|OPTI|", glPETClase) > 0 Then                 ' Si es Mantenimiento Evolutivo, Nuevo desarrollo u Optimizaci�n, busco si existe HOMAs
'                    If sp_GetPeticionConf(glNumeroPeticion, "HOMA.P", Null) Or _
'                       sp_GetPeticionConf(glNumeroPeticion, "HOMA.F", Null) Then    ' Busco si existe un HOMA
'                        If sp_GetPeticionConf(glNumeroPeticion, "OKPP", Null) Or _
'                           sp_GetPeticionConf(glNumeroPeticion, "OKPF", Null) Then  ' Entonces busco si existe el conforme de Responsable de Sector a las observaciones de Homologaci�n
'                                '{ add -093- c.
'                                If bOK_SI Then
'                                    If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")          ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
'                                    End If
'                                Else
'                                '}
'                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                End If  ' add -093- c.
'                        '{ add -067- a.
'                        Else
'                            Call sp_DeletePeticionChangeMan(glNumeroPeticion)
'                        '}
'                        End If
'                    Else        ' Busco que exista el conforme de pruebas del usuario
'                        If sp_GetPeticionConf(glNumeroPeticion, "TEST", Null) Or _
'                            sp_GetPeticionConf(glNumeroPeticion, "TESP", Null) Then
'                                '{ add -093- c.
'                                If bOK_SI Then
'                                    If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                    End If
'                                Else
'                                '}
'                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                End If  ' add -093- c.
'                        End If
'                    End If
'                Else
'                    '{ add -065- a. Busco si existe un conforme a las pruebas de usuario otorgado por un l�der
'                    bOKLider_Homologacion = False
'                    If sp_GetVarios("CTRLPRD1") Then
'                        If Not aplRST.EOF Then
'                            If ClearNull(aplRST.Fields!var_numero) = 1 Then
'                                bOKLider_Homologacion = True
'                            End If
'                        End If
'                    End If
'                    If bOKLider_Homologacion Then
'                        bOKLider = False
'                        If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
'                            Do While Not aplRST.EOF
'                                If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
'                                    bOKLider = True
'                                    Exit Do
'                                End If
'                                aplRST.MoveNext
'                                DoEvents
'                            Loop
'                        End If
'                        If bOKLider Then
'                            If sp_GetPeticionConf(glNumeroPeticion, "HSOB.F", Null) Or _
'                               sp_GetPeticionConf(glNumeroPeticion, "HSOB.P", Null) Or _
'                               sp_GetPeticionConf(glNumeroPeticion, "HOME.F", Null) Or _
'                               sp_GetPeticionConf(glNumeroPeticion, "HOME.P", Null) Then
'                                    '{ add -093- c.
'                                    If bOK_SI Then
'                                        If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                            Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                        End If
'                                    Else
'                                    '}
'                                        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                    End If  ' add -093- c.
'                            Else
'                                sp_DeletePeticionChangeMan glNumeroPeticion
'                            End If
'                        End If
'                    End If
'                    '}
'                    '{ add -070- a. Para ATEN, CORR y SPUF
'                    If sp_GetPeticionConf(glNumeroPeticion, "HOMA%", Null) Then
'                        If sp_GetPeticionConf(glNumeroPeticion, "OKP%", Null) Then
'                            '{ add -093- c.
'                            If bOK_SI Then
'                                If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                End If
'                            Else
'                            '}
'                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                            End If  ' add -093- c.
'                        Else
'                            sp_DeletePeticionChangeMan glNumeroPeticion
'                        End If
'                    Else
'                        '{ add -093- c.
'                        If bOK_SI Then
'                            If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                            End If
'                        Else
'                        '}
'                            Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                        End If  ' add -093- c.
'                    End If
'                    '}
'                End If
'                ValidarPeticion     ' add -065- a.
'            End If
'        Case "E"
'            ' Quita el conforme de homologaci�n para la petici�n y genera un registro del evento en el historial
'            If MsgBox("�Confirma quitar el conforme de Homologaci�n seleccionado?", vbQuestion + vbOKCancel, "Quitar conforme de Homologaci�n") = vbOK Then
'                If sp_DeletePeticionConf(glNumeroPeticion, cTipoHomo, grdPetConf.TextMatrix(grdPetConf.RowSel, 1), grdPetConf.TextMatrix(grdPetConf.RowSel, 6)) Then
'                    Call sp_AddHistorial(glNumeroPeticion, cTipoHomo, sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, "Eliminaci�n " & TextoCombo(cboPcfTipoHomo, cTipoHomo, False) & "  --->  " & pcfTextoHomo.Text)
'                    '{ add -067- a. Si queda alg�n OMA, entonces la petici�n no es v�lida
'                    If sp_GetPeticionConf(glNumeroPeticion, "HOMA%", Null) Then
'                        sp_DeletePeticionChangeMan glNumeroPeticion
'                    '{ add -070- a.
'                    Else
'                        If InStr(1, "CORR|ATEN|SPUF|", glPETClase) > 0 Then
'                            bOKLider_Homologacion = False
'                            If sp_GetVarios("CTRLPRD1") Then
'                                If Not aplRST.EOF Then
'                                    If ClearNull(aplRST.Fields!var_numero) = 1 Then
'                                        bOKLider_Homologacion = True
'                                    End If
'                                End If
'                            End If
'                            If bOKLider_Homologacion Then
'                                bOKLider = False
'                                If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
'                                    Do While Not aplRST.EOF
'                                        If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
'                                            bOKLider = True
'                                            Exit Do
'                                        End If
'                                        aplRST.MoveNext
'                                        DoEvents
'                                    Loop
'                                End If
'                                If bOKLider Then
'                                    If sp_GetPeticionConf(glNumeroPeticion, "HSOB%", Null) Or _
'                                       sp_GetPeticionConf(glNumeroPeticion, "HOME%", Null) Then
'                                            '{ add -093- c.
'                                            If bOK_SI Then
'                                                If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                                    Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                                End If
'                                            Else
'                                            '}
'                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                            End If      ' add -093- c.
'                                    Else
'                                        sp_DeletePeticionChangeMan glNumeroPeticion
'                                    End If
'                                Else
'                                    If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
'                                        '{ add -093- c.
'                                        If bOK_SI Then
'                                            If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                                Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                            End If
'                                        Else
'                                        '}
'                                            Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                        End If      ' add -093- c.
'                                    Else            ' Si no hay conforme de Usuario, entonces no puede ser v�lida
'                                        sp_DeletePeticionChangeMan glNumeroPeticion
'                                    End If
'                                End If
'                            Else
'                                If sp_GetPeticionConf(glNumeroPeticion, "TES%", Null) Then
'                                    '{ add -093- c.
'                                    If bOK_SI Then
'                                        If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                            Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                        End If
'                                    Else
'                                    '}
'                                        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                    End If      ' add -093- c.
'                                Else            ' Si no hay conforme de Usuario, entonces no puede ser v�lida
'                                    sp_DeletePeticionChangeMan glNumeroPeticion
'                                End If
'                            End If
'                        Else
'                            If sp_GetPeticionConf(glNumeroPeticion, "HSOB%", Null) Or _
'                               sp_GetPeticionConf(glNumeroPeticion, "HOME%", Null) Then
'                                    '{ add -093- c.
'                                    If bOK_SI Then
'                                        If sp_GetAdjuntosPet(glNumeroPeticion, "C104", Null, Null) Then
'                                            Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                        End If
'                                    Else
'                                    '}
'                                        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, glPETClase, "", "", glLOGIN_ID_REAL, "02")
'                                    End If  ' add -093- c.
'                            Else
'                                sp_DeletePeticionChangeMan glNumeroPeticion
'                            End If
'                        End If
'                    '}
'                    End If
'                    '}
'                End If
'                Call pcfHabBtnHomo(0, "X")
'                cboPcfTipoHomo.ListIndex = -1
'                pcfTextoHomo.Text = ""
'                ValidarPeticion     ' add -065- a.
'            End If
'        Case Else
'            pcfTextoHomo = ""
'            Call pcfHabBtnHomo(0, "X")
'    End Select
'End Sub

'Private Sub cmdRptHs_Click()
'    Dim crApp As New CRAXDRT.Application
'    Dim crReport As New CRAXDRT.Report
'    Dim crParamDefs As CRAXDRT.ParameterFieldDefinitions
'    Dim crParamDef As CRAXDRT.ParameterFieldDefinition
'    Dim vRetorno() As String
'    Dim sReportName As String
'
'    sReportName = "\hstrapeta6.rpt"
'    Set crReport = crApp.OpenReport(App.Path & "\" & sReportName, 1)
'    With crReport
'        .PaperSize = crPaperA4
'        .PaperOrientation = crLandscape
'        .ReportTitle = "Horas trabajadas por petici�n / tarea"
'        .ReportComments = "Petici�n: " & ClearNull(txtNroAsignado.Text)
'    End With
'
'    Call FuncionesCrystal10.ConectarDB(crReport)
'    On Error GoTo ErrHandler
'    'Abrir el reporte
'    Call Puntero(True)
'    ' Parametros del reporte
'    Set crParamDefs = crReport.ParameterFields
'    For Each crParamDef In crParamDefs
'        crParamDef.ClearCurrentValueAndRange
'        Select Case crParamDef.ParameterFieldName
'            Case "@fdesde": crParamDef.AddCurrentValue ("19000101")
'            Case "@fhasta": crParamDef.AddCurrentValue ("20790606")
'            Case "@tipo": crParamDef.AddCurrentValue ("PET")
'            Case "@codigo": crParamDef.AddCurrentValue (CStr(glNumeroPeticion))
'            Case "@detalle": crParamDef.AddCurrentValue (CStr("1"))
'        End Select
'    Next
'    Set rptVisorRpt.crReport = crReport
'    rptVisorRpt.CargarReporte
'    rptVisorRpt.Show 1
'
'    Call Puntero(False)
'    Set crParamDefs = Nothing
'    Set crParamDef = Nothing
'    Set crApp = Nothing
'    Set crReport = Nothing
'Exit Sub
'ErrHandler:
'    If Err.Number = -2147206461 Then
'        MsgBox "El archivo de reporte no se encuentra, rest�urelo de los discos de instalaci�n", _
'            vbCritical + vbOKOnly
'    Else
'        MsgBox Err.DESCRIPTION, vbCritical + vbOKOnly
'    End If
'    Call Puntero(False)
'End Sub

'Private Sub cmdRptHs_Click()
'    Dim sTitulo As String
'
'    If Not LockProceso(True) Then
'        Exit Sub
'    End If
'    Call Puntero(True)
'    With mdiPrincipal.CrystalReport1
'        sTitulo = "Petici�n: " & ClearNull(txtNroAsignado.Text)
'        .Destination = crptToWindow
'        .ReportFileName = GetShortName(App.Path) & "\" & "hstrapeta.rpt"
'        .Connect = "DSN=" & "CRREP" & ";UID=" & mdiPrincipal.AdoConnection.UsuarioAplicacion & ";PWD=" & mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad & ";DB=" & mdiPrincipal.AdoConnection.BaseAplicacion
'        .SelectionFormula = ""
'        .RetrieveStoredProcParams
'        .StoredProcParam(0) = "19000101"    'se fuerza la fecha
'        .StoredProcParam(1) = "20790606"    ' add -058- a.
'        .StoredProcParam(2) = "PET"
'        .StoredProcParam(3) = "" & glNumeroPeticion
'        .StoredProcParam(4) = 1             'se fuerza el nivel: recurso
'        .Formulas(0) = "@0_0EMPRESA='" & glHEADEMPPRESA & "'"
'        .Formulas(1) = "@0_1APLICACION='" & glHEADAPLICACION & "'"
'        .Formulas(2) = "@0_TITULO='" & sTitulo & "'"
'        .WindowLeft = 1
'        .WindowTop = 1
'        .WindowState = crptMaximized
'        .Action = 1
'        .Formulas(2) = ""
'    End With
'    Call Puntero(False)
'    Call LockProceso(False)
'End Sub

'{ add -097- a. Evalua si la petici�n estuvo alguna vez en ejecuci�n
Private Function EstuvoEnEjecucion() As Boolean
    EstuvoEnEjecucion = False
    If sp_GetHistorial(glNumeroPeticion, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!pet_estado) = "EJECUC" Then
                EstuvoEnEjecucion = True
                Exit Do
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
End Function
'}

Private Sub InicializarDatosRecurso()
    ' Averiguar todo lo que se puede acerca de su perfil
    xPerfNivel = getPerfNivel(PerfilInternoActual)
    xPerfArea = getPerfArea(PerfilInternoActual)
    xPerfDir = ""
    xPerfGer = ""
    xPerfSec = ""
    xPerfGru = ""
    Select Case xPerfNivel
        Case "DIRE"
            xPerfDir = xPerfArea
        Case "GERE"
            If sp_GetGerenciaXt(xPerfArea, Null) Then
                If Not aplRST.EOF Then
                    xPerfDir = ClearNull(aplRST!cod_direccion)
                    xPerfGer = xPerfArea
                End If
            End If
        Case "SECT"
            If sp_GetSectorXt(xPerfArea, Null, Null) Then
                If Not aplRST.EOF Then
                    xPerfDir = ClearNull(aplRST!cod_direccion)
                    xPerfGer = ClearNull(aplRST!cod_gerencia)
                    xPerfSec = xPerfArea
                End If
            End If
        Case "GRUP"
            If sp_GetGrupoXt(xPerfArea, Null) Then
                If Not aplRST.EOF Then
                    xPerfDir = ClearNull(aplRST!cod_direccion)
                    xPerfGer = ClearNull(aplRST!cod_gerencia)
                    xPerfSec = ClearNull(aplRST!cod_sector)
                    xPerfGru = xPerfArea
                End If
            End If
    End Select
End Sub

'{ add -101- a.
Private Sub CargarParametria()
    Dim i As Integer
    
'Dim vPetTipo() As String            ' Vector para tipo de petici�n
'Dim vPetClase() As String           ' Vector para clase de petici�n
    
'    ' Carga los tipos de petici�n que puede agregar el recurso actuante
'    If sp_GetTipoPerfil(glModoPeticion, xPerfDir, xPerfGer, PerfilInternoActual, Null) Then
'        If Not aplRST.EOF Then
'            cboTipopet.Clear
'            Do While Not aplRST.EOF
'                ReDim Preserve vPetTipo(i)
'                'vPetTipo(i) =  ClearNull(aplRST.Fields!cod_tipopet)
'                cboTipopet.AddItem ClearNull(aplRST.Fields!nom_tipopet) & ESPACIOS & ClearNull(aplRST.Fields!cod_tipopet)
'                i = i + 1
'                aplRST.MoveNext
'                DoEvents
'            Loop
'            cboTipopet.ListIndex = 0
'        End If
'    End If
    
    If sp_GetTipoPetClase(Null, Null) Then
        Do While Not aplRST.EOF
            ReDim Preserve PetParam(i)
            PetParam(i).tipo = ClearNull(aplRST.Fields!cod_tipopet)
            PetParam(i).TipoNom = ClearNull(aplRST.Fields!nom_tipopet)
            PetParam(i).clase = ClearNull(aplRST.Fields!cod_clase)
            PetParam(i).ClaseNom = ClearNull(aplRST.Fields!nom_clase)
            aplRST.MoveNext
            i = i + 1
            DoEvents
        Loop
    End If
End Sub
'}

Private Sub CargarPetTipo(Optional bCompleto As Boolean)
    Dim i As Integer
    
    Call Debuggear(Me, "CargarPetTipo")
    
    If IsMissing(bCompleto) Then bCompleto = False
    
    With cboTipopet
        .Clear
        If bCompleto Then
            If sp_GetTipoPet(Null) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!nom_tipopet) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_tipopet)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            Call CargarPetClase(True)
        Else
            If sp_GetTipoPerfil(glModoPeticion, xPerfDir, xPerfGer, PerfilInternoActual, Null) Then
                If Not aplRST.EOF Then
                    cboTipopet.Clear
                    Do While Not aplRST.EOF
                        'ReDim Preserve vPetTipo(i)
                        .AddItem ClearNull(aplRST.Fields!nom_tipopet) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_tipopet)
                        i = i + 1
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    .ListIndex = PosicionCombo(cboTipopet, "PRO", True)
                    If i = 1 Then .Enabled = False
                End If
            End If
        End If
        If .ListCount > 0 Then .ListIndex = 0
        If .ListCount = 1 Then Call setHabilCtrl(cboClase, "DIS")
    End With
End Sub

Private Sub CargarPetClase(Optional bCompleto As Boolean)
    Dim i As Integer
    
    Call Debuggear(Me, "CargarPetClase")
    
    If IsMissing(bCompleto) Then bCompleto = False
    
    With cboClase
        .Clear
        If bCompleto Then
            If sp_GetClase(Null) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!nom_clase) & ESPACIOS & "||" & ClearNull(aplRST.Fields!cod_clase)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        Else
            For i = 0 To UBound(PetParam)
                If PetParam(i).tipo = CodigoCombo(cboTipopet, True) Then
                    .AddItem PetParam(i).ClaseNom & ESPACIOS & "||" & PetParam(i).clase
                End If
            Next i
        End If
        If .ListCount > 0 Then .ListIndex = 0
        If .ListCount = 1 Then Call setHabilCtrl(cboClase, "DIS")
    End With
End Sub

Private Sub InicializarControles(Optional sModo As String)
    Dim i As Integer                                    ' Utilizada para iterar vectores dentro de este evento
    
    If IsMissing(sModo) Then
        Me.KeyPreview = True                                ' add -002- h.
        '{ add -003- a.
        orjBtn(orjDAT).BackColor = Me.BackColor
        orjBtn(orjMEM).BackColor = Me.BackColor
        orjBtn(orjANX).BackColor = Me.BackColor
        orjBtn(orjDOC).BackColor = Me.BackColor
        orjBtn(orjADJ).BackColor = Me.BackColor
        orjBtn(orjCNF).BackColor = Me.BackColor
        orjBtn(orjEST).BackColor = Me.BackColor             ' add -064- a.
        '}
        
        For i = 0 To orjBtn.Count - 1
            orjFiller(i).Width = orjBtn(i).Width - 48
            orjFiller(i).Left = orjBtn(i).Left + 16
            orjFiller(i).Top = orjBtn(i).Top + orjBtn(i).Height - 24
            orjFiller(i).BackColor = orjBtn(i).BackColor                ' add -002- h.
        Next
    Else
        Select Case sModo
            Case "ALTA"
                orjBtn(orjANX).Visible = False
                orjBtn(orjDOC).Visible = False
                orjBtn(orjADJ).Visible = False
                orjBtn(orjCNF).Visible = False
                orjBtn(orjEST).Visible = False                  ' add -064- a.
            Case Else
                orjBtn(orjANX).Visible = True
                orjBtn(orjDOC).Visible = True
                orjBtn(orjADJ).Visible = True
                orjBtn(orjCNF).Visible = True
                orjBtn(orjEST).Visible = IIf(glLOGIN_Gerencia = "DESA" Or ExisteEnCadena("SADM|ADMI|", PerfilInternoActual), True, False)
        End Select
    End If
End Sub

'{ add -064- a.
Private Sub InicializarGrillas(bOpcion As Boolean)
    If bOpcion Then
        Call IniciarScroll(grdPetConf)
        Call IniciarScroll(grdDocMetod)
        Call IniciarScroll(grdAdjPet)
        Call IniciarScroll(grdAnexas)
        Call IniciarScroll(grdControl)
    Else
        Call DetenerScroll(grdPetConf)
        Call DetenerScroll(grdDocMetod)
        Call DetenerScroll(grdAdjPet)
        Call DetenerScroll(grdAnexas)
        Call DetenerScroll(grdControl)
    End If
End Sub
'}

'{ add -002- h.
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then cmdCancel_Click      ' Tecla: ESCAPE
End Sub
'}

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
        If KeyCode = vbKeyDelete Then Me.ActiveControl.ListIndex = -1
    End If
    '{ add -002- h.
    If Not glModoPeticion = "ALTA" Then
        orjBtn_Click KeyCode - 112
    Else
        Select Case KeyCode
            Case 112: orjBtn_Click 0       ' Tecla de funci�n: F1
            Case 113: orjBtn_Click 1       ' Tecla de funci�n: F2
        End Select
    End If
    '}
End Sub
