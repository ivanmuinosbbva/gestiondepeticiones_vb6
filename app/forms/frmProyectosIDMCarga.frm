VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmProyectosIDMCarga 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   5625
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9960
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmProyectosIDMCarga.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   9960
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdWizard_Terminar 
      Caption         =   "Finalizar"
      Default         =   -1  'True
      Height          =   375
      Left            =   8640
      TabIndex        =   24
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   8640
      TabIndex        =   37
      Top             =   4560
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Cancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   5160
      TabIndex        =   27
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Atras 
      Caption         =   "� Atr�s"
      Height          =   375
      Left            =   6360
      TabIndex        =   26
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton cmdWizard_Adelante 
      Caption         =   "Siguiente �"
      Height          =   375
      Left            =   7440
      TabIndex        =   25
      Top             =   4920
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Guardar"
      Height          =   375
      Left            =   7440
      TabIndex        =   3
      Top             =   4560
      Width           =   1095
   End
   Begin VB.Frame fraStep1 
      Caption         =   " Paso 1. Definici�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5415
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   9735
      Begin VB.CommandButton cmdAyuda 
         Height          =   375
         Left            =   9240
         Picture         =   "frmProyectosIDMCarga.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   41
         ToolTipText     =   "Seleccione la ayuda para comprender la manera en que es concebido conceptualmente un proyecto IDM"
         Top             =   240
         Width           =   375
      End
      Begin VB.Frame fraProyectoNiveles 
         Height          =   1815
         Left            =   5880
         TabIndex        =   14
         Top             =   1080
         Width           =   3615
         Begin VB.Line Line5 
            BorderColor     =   &H00808080&
            BorderStyle     =   3  'Dot
            X1              =   720
            X2              =   1095
            Y1              =   1395
            Y2              =   1395
         End
         Begin VB.Line Line4 
            BorderColor     =   &H00808080&
            BorderStyle     =   3  'Dot
            X1              =   720
            X2              =   720
            Y1              =   1080
            Y2              =   1440
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00808080&
            BorderStyle     =   3  'Dot
            X1              =   240
            X2              =   615
            Y1              =   915
            Y2              =   915
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00808080&
            BorderStyle     =   3  'Dot
            X1              =   240
            X2              =   240
            Y1              =   600
            Y2              =   960
         End
         Begin VB.Label lblProyecto_Prj 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "PROYECTO"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00E0E0E0&
            Height          =   180
            Left            =   1695
            TabIndex        =   17
            Top             =   1290
            Width           =   825
         End
         Begin VB.Shape shpPrj 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00808080&
            Height          =   375
            Left            =   1080
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Label lblProyecto_Sub 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "SUB PROYECTO"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00E0E0E0&
            Height          =   180
            Left            =   975
            TabIndex        =   16
            Top             =   810
            Width           =   1170
         End
         Begin VB.Shape shpSub 
            BackColor       =   &H00C0C0C0&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00808080&
            Height          =   375
            Left            =   600
            Top             =   720
            Width           =   2055
         End
         Begin VB.Label lblProyecto_Macro 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "MACRO PROYECTO"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   180
            Left            =   390
            TabIndex        =   15
            Top             =   330
            Width           =   1425
         End
         Begin VB.Shape shpMacro 
            BackColor       =   &H00C00000&
            BackStyle       =   1  'Opaque
            Height          =   375
            Left            =   120
            Top             =   240
            Width           =   2055
         End
      End
      Begin VB.ComboBox cmbTipoProyecto 
         Height          =   300
         ItemData        =   "frmProyectosIDMCarga.frx":0294
         Left            =   1920
         List            =   "frmProyectosIDMCarga.frx":02A1
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1200
         Width           =   3135
      End
      Begin VB.Frame fraDependencias 
         BorderStyle     =   0  'None
         Height          =   1095
         Left            =   120
         TabIndex        =   38
         Top             =   3360
         Width           =   9495
         Begin VB.ComboBox cmbMacro 
            Height          =   300
            ItemData        =   "frmProyectosIDMCarga.frx":02FD
            Left            =   1560
            List            =   "frmProyectosIDMCarga.frx":02FF
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   240
            Width           =   7815
         End
         Begin VB.ComboBox cmbSub 
            Height          =   300
            ItemData        =   "frmProyectosIDMCarga.frx":0301
            Left            =   1560
            List            =   "frmProyectosIDMCarga.frx":0303
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   600
            Width           =   7815
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Macro Proyecto"
            Height          =   180
            Index           =   4
            Left            =   120
            TabIndex        =   40
            Top             =   240
            Width           =   1185
         End
         Begin VB.Label lblWizard 
            AutoSize        =   -1  'True
            Caption         =   "Sub Proyecto"
            Height          =   180
            Index           =   5
            Left            =   120
            TabIndex        =   39
            Top             =   600
            Width           =   1005
         End
      End
      Begin VB.Label lblPaso1 
         AutoSize        =   -1  'True
         Caption         =   "Explicacion"
         ForeColor       =   &H00000000&
         Height          =   180
         Index           =   1
         Left            =   240
         TabIndex        =   36
         Top             =   3000
         Width           =   870
      End
      Begin VB.Label lblPaso1 
         AutoSize        =   -1  'True
         Caption         =   "Explicacion"
         ForeColor       =   &H00800000&
         Height          =   420
         Index           =   0
         Left            =   240
         TabIndex        =   35
         Top             =   1920
         Width           =   5775
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmProyectosIDMCarga.frx":0305
         Height          =   495
         Index           =   0
         Left            =   240
         TabIndex        =   13
         Top             =   480
         Width           =   9255
      End
      Begin VB.Label lblWizard 
         AutoSize        =   -1  'True
         Caption         =   "�Qu� desea crear?"
         Height          =   180
         Index           =   0
         Left            =   240
         TabIndex        =   12
         Top             =   1200
         Width           =   1425
      End
   End
   Begin VB.Frame fraStep2 
      Caption         =   " Paso 2. Datos adicionales "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5415
      Left            =   120
      TabIndex        =   23
      Top             =   120
      Width           =   9735
      Begin VB.CommandButton cmdAuto 
         Caption         =   "Carga autom�tica"
         Height          =   300
         Left            =   2880
         TabIndex        =   44
         Top             =   1200
         Visible         =   0   'False
         Width           =   1815
      End
      Begin AT_MaskText.MaskText txtCodigoNumerico 
         Height          =   300
         Left            =   1560
         TabIndex        =   8
         Top             =   1200
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         MaxLength       =   9
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   9
         Align           =   1
         DecimalPlaces   =   0
         EmptyValue      =   0   'False
         DataType        =   2
         NegativeValues  =   0   'False
      End
      Begin VB.TextBox txtNombre 
         Height          =   300
         Left            =   1560
         TabIndex        =   9
         Top             =   1680
         Width           =   7815
      End
      Begin VB.ComboBox cmbClase 
         Height          =   300
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   2640
         Width           =   7815
      End
      Begin VB.ComboBox cmbCategoria 
         Height          =   300
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   2160
         Width           =   7815
      End
      Begin VB.Label lblAdvertencia 
         AutoSize        =   -1  'True
         Caption         =   "lblAdvertencia"
         ForeColor       =   &H000000FF&
         Height          =   180
         Left            =   240
         TabIndex        =   43
         Top             =   3240
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblWizard 
         Caption         =   "C�digo num�rico"
         Height          =   420
         Index           =   6
         Left            =   240
         TabIndex        =   42
         Top             =   1200
         Width           =   1215
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblWizard 
         Caption         =   "Nombre descriptivo"
         Height          =   420
         Index           =   3
         Left            =   240
         TabIndex        =   31
         Top             =   1680
         Width           =   1215
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblWizard 
         AutoSize        =   -1  'True
         Caption         =   "Clasificaci�n"
         Height          =   180
         Index           =   2
         Left            =   240
         TabIndex        =   30
         Top             =   2640
         Width           =   975
      End
      Begin VB.Label lblWizard 
         AutoSize        =   -1  'True
         Caption         =   "Categor�a"
         Height          =   180
         Index           =   1
         Left            =   240
         TabIndex        =   29
         Top             =   2160
         Width           =   735
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmProyectosIDMCarga.frx":03E7
         Height          =   615
         Index           =   1
         Left            =   240
         TabIndex        =   28
         Top             =   480
         Width           =   9255
      End
   End
   Begin VB.Frame fraStep3 
      Caption         =   " Paso 3. Finalizaci�n "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5415
      Left            =   120
      TabIndex        =   32
      Top             =   120
      Width           =   9735
      Begin VB.TextBox txtResultado 
         Height          =   2535
         Left            =   240
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   34
         Top             =   1560
         Width           =   9135
      End
      Begin VB.Label lblExplicacion 
         Caption         =   $"frmProyectosIDMCarga.frx":04F2
         Height          =   615
         Index           =   2
         Left            =   240
         TabIndex        =   33
         Top             =   480
         Width           =   9255
      End
   End
   Begin VB.Frame fraModificar 
      Height          =   5415
      Left            =   120
      TabIndex        =   18
      Top             =   120
      Visible         =   0   'False
      Width           =   9735
      Begin VB.CheckBox chkMarca 
         Caption         =   "Marca"
         Height          =   255
         Left            =   240
         TabIndex        =   46
         Top             =   2640
         Width           =   855
      End
      Begin VB.ComboBox cmbEditCategoria 
         Height          =   300
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1800
         Width           =   7815
      End
      Begin VB.ComboBox cmbEditClase 
         Height          =   300
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   2160
         Width           =   7815
      End
      Begin VB.TextBox txtTitulo 
         Height          =   270
         Left            =   1200
         TabIndex        =   0
         Top             =   1440
         Width           =   7815
      End
      Begin VB.Line Line1 
         X1              =   120
         X2              =   9600
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Label lblClaveCompleta 
         AutoSize        =   -1  'True
         Caption         =   "Variable"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   45
         Top             =   720
         Width           =   810
      End
      Begin VB.Label lblProyectosIDM 
         AutoSize        =   -1  'True
         Caption         =   "Categor�a"
         Height          =   180
         Index           =   4
         Left            =   120
         TabIndex        =   22
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label lblProyectosIDM 
         AutoSize        =   -1  'True
         Caption         =   "Clase"
         Height          =   180
         Index           =   5
         Left            =   120
         TabIndex        =   21
         Top             =   2160
         Width           =   435
      End
      Begin VB.Label lblClaveCompleta 
         AutoSize        =   -1  'True
         Caption         =   "Variable"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   810
      End
      Begin VB.Label lblProyectosIDM 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   180
         Index           =   2
         Left            =   120
         TabIndex        =   19
         Top             =   1440
         Width           =   585
      End
   End
End
Attribute VB_Name = "frmProyectosIDMCarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 23.11.2010 - Se agregan nuevos datos a los proyectos IDM en el marco de desarrollo del aplicativo IGM (pet. 24493).

Option Explicit

Public cModo As String
Public bActualizo As Boolean
Public lProjId As Long
Public lProjSubId As Long
Public lProjSubSId As Long
Private lProjCatId As Long
Private lProjClaseId As Long

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    bActualizo = False
    CargarCombosProyectos
    If cModo = "AGREGAR" Then
        Inicializar_Asistente
        HabilitarWizard 1
        Me.Caption = "Proyectos IDM - Agregando nuevos proyectos..."
        CargarCombos
    Else
        HabilitarWizard 0
        Me.Caption = "Proyectos IDM - Modificando proyectos existentes..."
        If spProyectoIDM.sp_GetProyectoIDM(lProjId, lProjSubId, lProjSubSId, Null, Null, Null, Null, Null) Then
            ' Determino qu� es: Macro, Sub o Proyecto
            If lProjSubId = 0 And lProjSubSId = 0 Then
                lblClaveCompleta(0) = "MACRO PROYECTO"
            Else
                If lProjSubId <> 0 And lProjSubSId = 0 Then
                    lblClaveCompleta(0) = "SUB PROYECTO"
                Else
                    lblClaveCompleta(0) = "PROYECTO"
                End If
            End If
            lblClaveCompleta(1) = lProjId & "." & lProjSubId & "." & lProjSubSId & " - " & Trim(ClearNull(aplRST.Fields!projnom))
            txtTitulo = ClearNull(aplRST.Fields!projnom)
            lProjCatId = ClearNull(aplRST.Fields!ProjCatId)
            lProjClaseId = ClearNull(aplRST.Fields!ProjClaseId)
            chkMarca = IIf(ClearNull(aplRST.Fields!marca) = "S", 1, 0)
            CargaCombosConValoresExistentes
        End If
        cmdAceptar.Default = True
    End If
    Call Puntero(False)
End Sub

Private Sub CargarCombosProyectos()
    With cmbMacro
        .Clear
        If sp_GetProyectoIDMNivel("1", Null, Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!ProjId) & " - " & Trim(aplRST.Fields!projnom) & Space(300) & "||" & Trim(aplRST.Fields!ProjId)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
    
    With cmbSub
        .Clear
        If sp_GetProyectoIDMNivel("2", CodigoCombo(cmbMacro, True), Null, Null) Then
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!ProjSubId) <> 0 Then
                    .AddItem ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & " - " & Trim(aplRST.Fields!projnom) & Space(300) & "||" & Trim(aplRST.Fields!ProjSubId)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
End Sub

Private Sub cmbMacro_Click()
    If CodigoCombo(cmbTipoProyecto, True) = "PRJ" Then
        With cmbSub
            .Clear
            If spProyectoIDM.sp_GetProyectoIDMNivel("2", IIf(CodigoCombo(cmbMacro, True) = "", Null, CodigoCombo(cmbMacro, True)), Null, Null) Then
                Do While Not aplRST.EOF
                    If ClearNull(aplRST.Fields!ProjSubId) <> 0 Then
                        .AddItem ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & " - " & Trim(aplRST.Fields!projnom) & Space(300) & "||" & Trim(aplRST.Fields!ProjSubId) ' & "|" & Trim(aplRST.Fields!ProjSubId) & "|" & Trim(aplRST.Fields!ProjSubSId)
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
                If .ListCount > 0 Then
                    .ListIndex = 0
                    Call setHabilCtrl(cmbSub, "OBL")
                Else
                    Call setHabilCtrl(cmbSub, "DIS")
                End If
            End If
        End With
    End If
End Sub

Private Sub CargarCombos()
    Dim pCategoriaId As Long
    Dim pClaseId As Long
    
    Call setHabilCtrl(cmbCategoria, "DIS")
    Call setHabilCtrl(cmbClase, "DIS")
    pClaseId = -999
    With cmbCategoria
        .Clear
        If spProyectoIDM.sp_GetProyIDMCategoria(Null, Null) Then
            Do While Not aplRST.EOF
                .AddItem Trim(aplRST.Fields!ProjCatId) & ". " & Trim(aplRST.Fields!ProjCatNom) & Space(300) & "||" & Trim(aplRST.Fields!ProjCatId)
                aplRST.MoveNext
                DoEvents
            Loop
            pCategoriaId = PosicionCombo(cmbCategoria, GetSetting("GesPet", "Preferencias\ProyectoIDM", "Categoria"), True)
            If spProyectoIDM.sp_GetProyIDMCategoria(pCategoriaId, Null) Then
                .ListIndex = pCategoriaId
            Else
                .ListIndex = -1
            End If
            Call setHabilCtrl(cmbCategoria, "OBL")
        End If
    End With
    
    With cmbClase
        .Clear
        If cmbCategoria.ListCount > 0 And cmbCategoria.ListIndex > -1 Then
            If spProyectoIDM.sp_GetProyIDMClase(CodigoCombo(cmbCategoria, True), Null, Null) Then
                Do While Not aplRST.EOF
                    .AddItem Trim(aplRST.Fields!ProjClaseId) & ". " & Trim(aplRST.Fields!ProjClaseNom) & Space(200) & "||" & Trim(aplRST.Fields!ProjClaseId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                If Val(GetSetting("GesPet", "Preferencias\ProyectoIDM", "Clase")) > 0 Then
                    pClaseId = PosicionCombo(cmbClase, GetSetting("GesPet", "Preferencias\ProyectoIDM", "Clase"), True)
                    If pClaseId >= 0 Then
                        'If sp_GetProyIDMClase(CodigoCombo(cmbCategoria, pClaseId), Null, Null) Then
                        If spProyectoIDM.sp_GetProyIDMClase(CodigoCombo(cmbCategoria, True), Null, Null) Then
                            .ListIndex = pClaseId
                        Else
                            .ListIndex = -1
                        End If
                    End If
                End If
                Call setHabilCtrl(cmbClase, "OBL")
            End If
        End If
    End With
End Sub

Private Sub CargaCombosConValoresExistentes()
    If cModo = "AGREGAR" Then
        Call setHabilCtrl(cmbCategoria, "DIS")
        Call setHabilCtrl(cmbClase, "DIS")
    
        With cmbCategoria
            .Clear
            If spProyectoIDM.sp_GetProyIDMCategoria(Null, Null) Then
                Do While Not aplRST.EOF
                    .AddItem Trim(aplRST.Fields!ProjCatNom) & Space(300) & "||" & Trim(aplRST.Fields!ProjCatId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = PosicionCombo(cmbCategoria, lProjCatId, True)
                Call setHabilCtrl(cmbCategoria, "OBL")
            End If
        End With
       
        With cmbClase
            .Clear
            If cmbCategoria.ListCount > 0 Then
                If spProyectoIDM.sp_GetProyIDMClase(lProjCatId, Null, Null) Then
                    Do While Not aplRST.EOF
                        .AddItem Trim(aplRST.Fields!ProjClaseNom) & Space(200) & "||" & Trim(aplRST.Fields!ProjClaseId)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    .ListIndex = PosicionCombo(cmbClase, lProjClaseId, True)
                End If
                Call setHabilCtrl(cmbClase, "OBL")
            End If
        End With
    Else
        Call setHabilCtrl(cmbEditCategoria, "DIS")
        Call setHabilCtrl(cmbEditClase, "DIS")
    
        With cmbEditCategoria
            .Clear
            If spProyectoIDM.sp_GetProyIDMCategoria(Null, Null) Then
                Do While Not aplRST.EOF
                    .AddItem Trim(aplRST.Fields!ProjCatId) & ". " & Trim(aplRST.Fields!ProjCatNom) & Space(300) & "||" & Trim(aplRST.Fields!ProjCatId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = PosicionCombo(cmbEditCategoria, lProjCatId, True)
                Call setHabilCtrl(cmbEditCategoria, "OBL")
            End If
        End With
        
        
        With cmbEditClase
            .Clear
            If cmbEditCategoria.ListCount > 0 Then
                If spProyectoIDM.sp_GetProyIDMClase(lProjCatId, Null, Null) Then
                    Do While Not aplRST.EOF
                        .AddItem Trim(aplRST.Fields!ProjClaseId) & ". " & Trim(aplRST.Fields!ProjClaseNom) & Space(200) & "||" & Trim(aplRST.Fields!ProjClaseId)
                        aplRST.MoveNext
                        DoEvents
                    Loop
                    .ListIndex = PosicionCombo(cmbEditClase, lProjClaseId, True)
                End If
                Call setHabilCtrl(cmbEditClase, "OBL")
            End If
        End With
    End If
End Sub

Private Sub cmbCategoria_Click()
    With cmbClase
        .Clear
        If cmbCategoria.ListCount > 0 Then
            If spProyectoIDM.sp_GetProyIDMClase(CodigoCombo(cmbCategoria, True), Null, Null) Then
                Do While Not aplRST.EOF
                   .AddItem Trim(aplRST.Fields!ProjClaseId) & ". " & Trim(aplRST.Fields!ProjClaseNom) & Space(200) & "||" & Trim(aplRST.Fields!ProjClaseId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = 0
                Call setHabilCtrl(cmbClase, "OBL")
            Else
                Call setHabilCtrl(cmbClase, "DIS")
            End If
        End If
    End With
End Sub

Private Sub cmbEditCategoria_Click()
    With cmbEditClase
        .Clear
        If cmbEditCategoria.ListCount > 0 Then
            If spProyectoIDM.sp_GetProyIDMClase(CodigoCombo(cmbEditCategoria, True), Null, Null) Then
                Do While Not aplRST.EOF
                   .AddItem Trim(aplRST.Fields!ProjClaseId) & ". " & Trim(aplRST.Fields!ProjClaseNom) & Space(200) & "||" & Trim(aplRST.Fields!ProjClaseId)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = 0
                Call setHabilCtrl(cmbEditClase, "OBL")
            Else
                Call setHabilCtrl(cmbEditClase, "DIS")
            End If
        End If
    End With
End Sub

Private Sub Inicializar_Asistente()
    With cmbTipoProyecto
        .Clear
        .AddItem "Seleccione una opci�n..." & Space(100) & "||" & "NULL"
        .AddItem "1. Quiero crear un macro proyecto" & Space(100) & "||" & "MACRO"
        .AddItem "2. Quiero crear un sub proyecto" & Space(100) & "||" & "SUB"
        .AddItem "3. Quiero crear un proyecto" & Space(100) & "||" & "PRJ"
        If GetSetting("GesPet", "Preferencias\ProyectoIDM", "TipoProyecto") = "NULL" Then
            .ListIndex = 0
        Else
            .ListIndex = PosicionCombo(cmbTipoProyecto, GetSetting("GesPet", "Preferencias\ProyectoIDM", "TipoProyecto"), True)
            cmbMacro.ListIndex = PosicionCombo(cmbMacro, GetSetting("GesPet", "Preferencias\ProyectoIDM", "MacroProyecto"), True)
            cmbSub.ListIndex = PosicionCombo(cmbSub, GetSetting("GesPet", "Preferencias\ProyectoIDM", "SubProyecto"), True)
        End If
    End With
End Sub

Private Sub HabilitarWizard(Step As Byte)
    cmdWizard_Cancelar.Visible = True
    cmdWizard_Atras.Visible = True
    cmdWizard_Adelante.Visible = True
    cmdWizard_Terminar.Visible = True
    
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    
    Select Case Step
        Case 0
            ' Paneles
            'Me.Height = 6105   ' Altura original
            
            'Me.Height =
            fraModificar.Visible = True
            fraStep1.Visible = False
            fraStep2.Visible = False
            fraStep3.Visible = False
            ' Botones
            cmdWizard_Cancelar.Visible = False
            cmdWizard_Atras.Visible = False
            cmdWizard_Adelante.Visible = False
            cmdWizard_Terminar.Visible = False
            
            cmdAceptar.Visible = True
            cmdCancelar.Visible = True
            
            cmdWizard_Cancelar.Enabled = False
            cmdWizard_Atras.Enabled = False
            cmdWizard_Adelante.Enabled = False
            cmdWizard_Terminar.Enabled = False
            
            cmdAceptar.Enabled = True
            cmdCancelar.Enabled = True
        Case 1
            ' Paneles
            fraModificar.Visible = False
            fraStep1.Visible = True
            fraStep2.Visible = False
            fraStep3.Visible = False
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = False
            cmdWizard_Adelante.Enabled = True
            cmdWizard_Terminar.Enabled = False
        Case 2
            ' Paneles
            fraModificar.Visible = False
            fraStep1.Visible = False
            fraStep2.Visible = True
            fraStep3.Visible = False
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = True
            cmdWizard_Adelante.Enabled = True
            cmdWizard_Terminar.Enabled = False
            
            txtCodigoNumerico.SetFocus
        Case 3
            ' Paneles
            fraModificar.Visible = False
            fraStep1.Visible = False
            fraStep2.Visible = False
            fraStep3.Visible = True
            ' Botones
            cmdWizard_Cancelar.Enabled = True
            cmdWizard_Atras.Enabled = True
            cmdWizard_Adelante.Enabled = False
            cmdWizard_Terminar.Enabled = True
            
            Carga_Resumen
            
            cmdWizard_Terminar.SetFocus
        Case 4

    End Select
End Sub

Private Sub Carga_Resumen()
    Dim cDummy As String
    
    txtResultado.Text = ""
    cDummy = CodigoCombo(cmbTipoProyecto, True)
    Select Case cDummy
        Case "MACRO"
            txtResultado.Text = " Se crear� el siguiente " & Switch(cDummy = "MACRO", "macro proyecto", cDummy = "SUB", "sub proyecto", cDummy = "PRJ", "proyecto") & ":" & vbCrLf & vbCrLf & vbCrLf & _
                                " NUEVO " & UCase(Switch(cDummy = "MACRO", "macro proyecto", cDummy = "SUB", "sub proyecto", cDummy = "PRJ", "proyecto")) & vbCrLf & vbCrLf & vbCrLf & _
                                " C�digo: " & Trim(txtCodigoNumerico) & ".0.0" & vbCrLf & vbCrLf & _
                                " Nombre: " & Trim(txtNombre) & vbCrLf & vbCrLf & _
                                " Categor�a: " & TextoCombo(cmbCategoria, CodigoCombo(cmbCategoria, True), True) & vbCrLf & vbCrLf & _
                                " Clase: " & TextoCombo(cmbClase, CodigoCombo(cmbClase, True), True)
        Case "SUB"
            txtResultado.Text = " Se crear� el siguiente " & Switch(cDummy = "MACRO", "macro proyecto", cDummy = "SUB", "sub proyecto", cDummy = "PRJ", "proyecto") & ":" & vbCrLf & vbCrLf & vbCrLf & _
                                " NUEVO " & UCase(Switch(cDummy = "MACRO", "macro proyecto", cDummy = "SUB", "sub proyecto", cDummy = "PRJ", "proyecto")) & vbCrLf & vbCrLf & vbCrLf & _
                                " C�digo: " & CodigoCombo(cmbMacro, True) & "." & Trim(txtCodigoNumerico) & ".0" & vbCrLf & vbCrLf & _
                                " Nombre: " & Trim(txtNombre) & vbCrLf & vbCrLf & _
                                " Macro Proyecto del que depende: " & TextoCombo(cmbMacro, CodigoCombo(cmbMacro, True), True) & vbCrLf & vbCrLf & _
                                " Categor�a: " & TextoCombo(cmbCategoria, CodigoCombo(cmbCategoria, True), True) & vbCrLf & vbCrLf & _
                                " Clase: " & TextoCombo(cmbClase, CodigoCombo(cmbClase, True), True)
        Case "PRJ"
            txtResultado.Text = " Se crear� el siguiente " & Switch(cDummy = "MACRO", "macro proyecto", cDummy = "SUB", "sub proyecto", cDummy = "PRJ", "proyecto") & ":" & vbCrLf & vbCrLf & vbCrLf & _
                                " NUEVO " & UCase(Switch(cDummy = "MACRO", "macro proyecto", cDummy = "SUB", "sub proyecto", cDummy = "PRJ", "proyecto")) & vbCrLf & vbCrLf & vbCrLf & _
                                " C�digo: " & CodigoCombo(cmbMacro, True) & "." & CodigoCombo(cmbSub, True) & "." & Trim(txtCodigoNumerico) & vbCrLf & vbCrLf & _
                                " Nombre: " & Trim(txtNombre) & vbCrLf & vbCrLf & _
                                " Macro Proyecto del que depende: " & TextoCombo(cmbMacro, CodigoCombo(cmbMacro, True), True) & vbCrLf & vbCrLf & _
                                " Sub Proyecto: " & TextoCombo(cmbSub, CodigoCombo(cmbSub, True), True) & vbCrLf & vbCrLf & _
                                " Categor�a: " & TextoCombo(cmbCategoria, CodigoCombo(cmbCategoria, True), True) & vbCrLf & vbCrLf & _
                                " Clase: " & TextoCombo(cmbClase, CodigoCombo(cmbClase, True), True)
    End Select
End Sub

Private Function StepId() As Byte
    ' Esta funci�n devuelve el nro. de paso en el que se encuentra el usuario
    If fraStep1.Visible Then
        StepId = 1
    Else
        If fraStep2.Visible Then
            StepId = 2
        Else
            StepId = 3
        End If
    End If
End Function

Private Sub cmdWizard_Adelante_Click()
    Select Case StepId
        Case 1
            If ValidarPaso(1) Then
                HabilitarWizard 2
            End If
        Case 2
            If ValidarPaso(2) Then
                HabilitarWizard 3
            End If
        Case 3
            If ValidarPaso(3) Then
                HabilitarWizard 4
            End If
    End Select
End Sub

Private Sub cmdWizard_Atras_Click()
    Select Case StepId
        Case 2: Call HabilitarWizard(1)
        Case 3: Call HabilitarWizard(2)
        Case 4: Call HabilitarWizard(3)
    End Select
End Sub

Private Sub cmbTipoProyecto_Click()
    Call DibujaEsquema(CodigoCombo(cmbTipoProyecto, True))
End Sub

Private Function ValidarPaso(Step As Byte) As Boolean
    ValidarPaso = False
    Select Case Step
        Case 1
            ' Tipo de proyecto seleccionado
            If CodigoCombo(cmbTipoProyecto, True) <> "NULL" Then
                If cmbMacro.ListIndex > -1 Then
                    If cmbSub.ListIndex > -1 Then
                        cmdWizard_Adelante.Default = True
                        ValidarPaso = True
                    Else
                        MsgBox "Debe seleccionar un sub proyecto v�lido para continuar.", vbExclamation + vbOKOnly, "Sub proyecto"
                        If cmbSub.Enabled Then cmbSub.SetFocus
                        cmdWizard_Adelante.Default = True
                    End If
                Else
                    MsgBox "Debe seleccionar un macro proyecto v�lido para continuar.", vbExclamation + vbOKOnly, "Macro proyecto"
                    If cmbMacro.Enabled Then cmbMacro.SetFocus
                    cmdWizard_Adelante.Default = True
                End If
            Else
                MsgBox "Debe seleccionar un tipo de elemento v�lido para crear.", vbExclamation + vbOKOnly, "Tipo de elemento"
                If cmbTipoProyecto.Enabled Then cmbTipoProyecto.SetFocus
            End If
        Case 2
            ' Nombre para el proyecto
            If IsNumeric(txtCodigoNumerico) Then
                If Len(txtNombre) > 0 Then
                    If CodigoCombo(cmbCategoria, True) <> "" Then
                        If CodigoCombo(cmbClase, True) <> "" Then
                            ValidarPaso = True
                        Else
                            MsgBox "Debe seleccionar una clase v�lida para el elemento.", vbExclamation + vbOKOnly, "Clase"
                        End If
                    Else
                        MsgBox "Debe seleccionar una categor�a v�lida para el elemento.", vbExclamation + vbOKOnly, "Categor�a"
                    End If
                Else
                    MsgBox "Debe indicar un nombre descriptivo para el elemento.", vbExclamation + vbOKOnly, "Nombre"
                End If
            Else
                MsgBox "Debe indicar un c�digo num�rico para el elemento.", vbExclamation + vbOKOnly, "C�digo num�rico"
            End If
        Case 3
            ValidarPaso = True
    End Select
End Function

Private Sub DibujaEsquema(Esquema As String)
    Select Case Esquema
        Case "MACRO"
            shpMacro.BackColor = &HC00000
            shpMacro.BorderColor = &H80000008
            lblProyecto_Macro.ForeColor = &HFFFFFF
            
            shpSub.BackColor = &HC0C0C0
            shpSub.BorderColor = &H808080
            lblProyecto_Sub.ForeColor = &HE0E0E0
            
            shpPrj.BackColor = &HC0C0C0
            shpPrj.BorderColor = &H808080
            lblProyecto_Prj.ForeColor = &HE0E0E0
            
            ' Explicaci�n
            lblPaso1(0) = "Ha seleccionado crear un macro proyecto. Es el primer nivel de los tres existentes." & vbCrLf & vbCrLf & _
                          "Para continuar, presione Siguiente."
            lblPaso1(1) = ""
            Call setHabilCtrl(cmbMacro, "DIS")
            Call setHabilCtrl(cmbSub, "DIS")
        Case "SUB"
            shpMacro.BackColor = &HC0C0C0
            shpMacro.BorderColor = &H808080
            lblProyecto_Macro.ForeColor = &HE0E0E0
            
            shpSub.BackColor = &HC00000
            shpSub.BorderColor = &H80000008
            lblProyecto_Sub.ForeColor = &HFFFFFF
            
            shpPrj.BackColor = &HC0C0C0
            shpPrj.BorderColor = &H808080
            lblProyecto_Prj.ForeColor = &HE0E0E0
            
            ' Explicaci�n
            lblPaso1(0) = "Ha seleccionado crear un sub proyecto. Este elemento pertenece al segundo nivel de los tres existentes. Para identificar correctamente al subproyecto, debe indicar de que macroproyecto depende."
            lblPaso1(1) = "Seleccione de que macro proyecto depender� este nuevo subproyecto."
            Call setHabilCtrl(cmbMacro, "OBL")
            Call setHabilCtrl(cmbSub, "DIS")
        Case "PRJ"
            shpMacro.BackColor = &HC0C0C0
            shpMacro.BorderColor = &H808080
            lblProyecto_Macro.ForeColor = &HE0E0E0
            
            shpSub.BackColor = &HC0C0C0
            shpSub.BorderColor = &H808080
            lblProyecto_Sub.ForeColor = &HE0E0E0
            
            shpPrj.BackColor = &HC00000
            shpPrj.BorderColor = &H80000008
            lblProyecto_Prj.ForeColor = &HFFFFFF
            
            ' Explicaci�n
            lblPaso1(0) = "Ha seleccionado crear un proyecto. Este elemento pertenece al tercer y �ltimo nivel de los tres existentes. Para identificar correctamente al proyecto, debe indicar de que macro y sub proyecto depende."
            lblPaso1(1) = "Seleccione de que macro y sub proyecto depender� este nuevo proyecto"
            Call setHabilCtrl(cmbMacro, "OBL")
            Call setHabilCtrl(cmbSub, "OBL")
        Case Else
            shpMacro.BackColor = &HC0C0C0
            shpMacro.BorderColor = &H808080
            lblProyecto_Macro.ForeColor = &HE0E0E0
            
            shpSub.BackColor = &HC0C0C0
            shpSub.BorderColor = &H808080
            lblProyecto_Sub.ForeColor = &HE0E0E0
            
            shpPrj.BackColor = &HC0C0C0
            shpPrj.BorderColor = &H808080
            lblProyecto_Prj.ForeColor = &HE0E0E0
            ' Explicaci�n
            lblPaso1(0) = ""
            lblPaso1(1) = ""
            Call setHabilCtrl(cmbMacro, "DIS")
            Call setHabilCtrl(cmbSub, "DIS")
    End Select
    Call Mostrar_Dependencias(Esquema)
End Sub

Private Sub Mostrar_Dependencias(Esquema As String)
    Select Case Esquema
        Case "MACRO"
            fraDependencias.Visible = False
        Case "SUB"
            lblWizard(4).Visible = True
            lblWizard(5).Visible = False
            cmbMacro.Visible = True
            cmbSub.Visible = False
            fraDependencias.Visible = True
        Case "PRJ"
            lblWizard(4).Visible = True
            lblWizard(5).Visible = True
            cmbMacro.Visible = True
            cmbSub.Visible = True
            fraDependencias.Visible = True
        Case Else
            fraDependencias.Visible = False
    End Select
End Sub

Private Sub cmdWizard_Terminar_Click()
    If MsgBox("�Confirma el agregado de este nuevo elemento?", vbQuestion + vbYesNo, "Confirmar agregado") = vbYes Then
        Call Guardar_Elemento
        bActualizo = True
        Unload Me
    End If
End Sub

Private Sub Guardar_Elemento()
    Select Case CodigoCombo(cmbTipoProyecto, True)
        Case "MACRO"
            lProjId = txtCodigoNumerico
            lProjSubId = 0
            lProjSubSId = 0
        Case "SUB"
            lProjId = CodigoCombo(cmbMacro, True)
            lProjSubId = txtCodigoNumerico
            lProjSubSId = 0
        Case "PRJ"
            lProjId = CodigoCombo(cmbMacro, True)
            lProjSubId = IIf(CodigoCombo(cmbSub, True) = "", 0, CodigoCombo(cmbSub, True))
            lProjSubSId = txtCodigoNumerico
    End Select
    If Not sp_InsertProyectoIDM(lProjId, lProjSubId, lProjSubSId, txtNombre, CodigoCombo(cmbCategoria, True), CodigoCombo(cmbClase, True)) Then
        MsgBox "Se ha producido un error al intentar grabar el elemento solicitado.", vbCritical + vbOKOnly, "Error al guardar"
    End If
End Sub

Private Sub cmdWizard_Cancelar_Click()
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call Guardar_Pantalla
End Sub

Private Sub Guardar_Pantalla()
    If cModo = "AGREGAR" And bActualizo Then
        SaveSetting "GesPet", "Preferencias\ProyectoIDM", "TipoProyecto", CodigoCombo(cmbTipoProyecto, True)
        SaveSetting "GesPet", "Preferencias\ProyectoIDM", "MacroProyecto", CodigoCombo(cmbMacro, True)
        SaveSetting "GesPet", "Preferencias\ProyectoIDM", "SubProyecto", CodigoCombo(cmbSub, True)
        SaveSetting "GesPet", "Preferencias\ProyectoIDM", "Categoria", CodigoCombo(cmbCategoria, True)
        SaveSetting "GesPet", "Preferencias\ProyectoIDM", "Clase", CodigoCombo(cmbClase, True)
    End If
End Sub

Private Sub txtCodigoNumerico_LostFocus()
    Dim xlProjId As Long
    Dim xlProjSubId As Long
    Dim xlProjSubSId As Long
    Dim cDummy As String
    
    If IsNumeric(txtCodigoNumerico) Then
        cDummy = CodigoCombo(cmbTipoProyecto, True)
        Select Case CodigoCombo(cmbTipoProyecto, True)
            Case "MACRO"
                xlProjId = txtCodigoNumerico
                xlProjSubId = 0
                xlProjSubSId = 0
            Case "SUB"
                xlProjId = CodigoCombo(cmbMacro, True)
                xlProjSubId = txtCodigoNumerico
                xlProjSubSId = 0
            Case "PRJ"
                xlProjId = CodigoCombo(cmbMacro, True)
                xlProjSubId = IIf(CodigoCombo(cmbSub, True) = "", 0, CodigoCombo(cmbSub, True))
                xlProjSubSId = txtCodigoNumerico
        End Select
        If sp_GetProyectoIDM(xlProjId, xlProjSubId, xlProjSubSId, Null, Null, Null, Null, Null) Then
            If Not aplRST.EOF Then
                lblAdvertencia.Caption = "El c�digo num�rico ingresado ya existe para el " & _
                                         Switch(cDummy = "MACRO", "macro proyecto", cDummy = "SUB", "sub proyecto", cDummy = "PRJ", "proyecto") & _
                                         " que desea crear. Revise e ingrese un c�digo num�rico v�lido." & vbCrLf & vbCrLf & _
                                         Switch(cDummy = "MACRO", "Macro proyecto: ", cDummy = "SUB", "Sub proyecto: ", cDummy = "PRJ", "Proyecto: ") & _
                                         ClearNull(aplRST.Fields!ProjId) & "." & ClearNull(aplRST.Fields!ProjSubId) & "." & ClearNull(aplRST.Fields!ProjSubSId) & " - " & ClearNull(aplRST.Fields!projnom)
                lblAdvertencia.Visible = True
            End If
        Else
            lblAdvertencia.Visible = False
        End If
    Else
        lblAdvertencia.Visible = False
    End If
End Sub

Private Sub cmdAuto_Click()
    Dim xlProjId As Variant
    Dim xlProjSubId As Variant
    Dim xlProjSubSId As Variant
    Dim cDummy As String
    
    cDummy = CodigoCombo(cmbTipoProyecto, True)
    Select Case cDummy
        Case "MACRO"
            xlProjId = Null
            xlProjSubId = Null
            xlProjSubSId = Null
        Case "SUB"
            xlProjId = CodigoCombo(cmbMacro, True)
            xlProjSubId = Null
            xlProjSubSId = Null
        Case "PRJ"
            xlProjId = CodigoCombo(cmbMacro, True)
            xlProjSubId = CodigoCombo(cmbSub, True)
            xlProjSubSId = Null
    End Select
    If spProyectoIDM.sp_GetProyectoIDM(xlProjId, xlProjSubId, xlProjSubSId, Null, "D", Null, Null, Null) Then
        If Not aplRST.EOF Then
            Select Case cDummy
                Case "MACRO": txtCodigoNumerico = aplRST.Fields!ProjId + 1
                Case "SUB": txtCodigoNumerico = aplRST.Fields!ProjSubId + 1
                Case "PRJ": txtCodigoNumerico = aplRST.Fields!ProjSubSId + 1
            End Select
            txtNombre.SetFocus
        End If
    End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

'Private Sub cmdAceptar_Click()
'    If MsgBox("�Confirma la actualizaci�n?", vbQuestion + vbYesNo, "Actualizar") = vbYes Then
'        Screen.MousePointer = vbHourglass
'        If Not sp_UpdateProyectoIDM(lProjId, lProjSubId, lProjSubSId, Trim(txtTitulo), CodigoCombo(cmbEditCategoria, True), CodigoCombo(cmbEditClase, True), IIf(chkMarca.Value = 1, "S", Null)) Then
'            MsgBox "Problemas al intentar actualizar.", vbExclamation + vbOKOnly, "Problemas encontrados"
'        Else
'            bActualizo = True
'            Screen.MousePointer = vbNormal
'            Unload Me
'        End If
'    End If
'End Sub
