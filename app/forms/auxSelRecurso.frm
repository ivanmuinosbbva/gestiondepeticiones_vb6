VERSION 5.00
Begin VB.Form auxSelRecurso 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccione recurso"
   ClientHeight    =   1770
   ClientLeft      =   1140
   ClientTop       =   330
   ClientWidth     =   5535
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1770
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cboRecurso 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   60
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   480
      Width           =   5415
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   60
      TabIndex        =   0
      Top             =   1080
      Width           =   5415
      Begin VB.CommandButton cmdBD 
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4440
         TabIndex        =   2
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   1
         Top             =   180
         Width           =   885
      End
   End
End
Attribute VB_Name = "auxSelRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_CodRecurso As String
Dim m_NomRecurso As String

Private Sub Form_Load()
    If sp_GetRecurso("", "R", "A") Then
        Do While Not aplRST.EOF
            cboRecurso.AddItem ClearNull(aplRST(1)) & ESPACIOS & "||" & ClearNull(aplRST(0))
            aplRST.MoveNext
        Loop
    End If
End Sub

Public Property Get NomRecurso() As String
    NomRecurso = m_NomRecurso
End Property

Public Property Get CodRecurso() As String
    CodRecurso = m_CodRecurso
End Property

Public Property Let CodRecurso(ByVal NewCodRecurso As String)
    m_CodRecurso = NewCodRecurso
End Property

Private Sub cboRecurso_Click()
    m_CodRecurso = CodigoCombo(cboRecurso, True)
    m_NomRecurso = TextoCombo(cboRecurso)
End Sub

Private Sub cmdOk_Click()
    Unload Me
End Sub

Private Sub cmdBD_Click()
    m_CodRecurso = ""
    Unload Me
End Sub
