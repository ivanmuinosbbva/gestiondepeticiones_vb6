VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTipo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tipo"
   ClientHeight    =   6195
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6195
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame pnlBotones 
      Height          =   6195
      Left            =   8220
      TabIndex        =   14
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         Height          =   500
         Left            =   60
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   5610
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Tipo seleccionada"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la Tipo selecionada"
         Top             =   4080
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         Height          =   500
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Tipo"
         Top             =   3570
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3060
      Left            =   0
      TabIndex        =   10
      Top             =   3120
      Width           =   8205
      Begin VB.ComboBox cboHabil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1440
         Width           =   1935
      End
      Begin AT_MaskText.MaskText txtCodigo 
         Height          =   315
         Left            =   1440
         TabIndex        =   1
         Top             =   360
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtDescripcion 
         Height          =   315
         Left            =   1440
         TabIndex        =   2
         Top             =   1080
         Width           =   4425
         _ExtentX        =   7805
         _ExtentY        =   556
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   30
      End
      Begin AT_MaskText.MaskText txtSecuencia 
         Height          =   315
         Left            =   1440
         TabIndex        =   4
         Top             =   2520
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   556
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   3
      End
      Begin VB.Label Label6 
         Caption         =   "Secuencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   2520
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   15
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   12
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   30
         Visible         =   0   'False
         Width           =   105
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3000
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   5292
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin AT_MaskText.MaskText MaskText1 
      Height          =   315
      Left            =   1320
      TabIndex        =   16
      Top             =   5640
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   556
      MaxLength       =   30
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   30
   End
End
Attribute VB_Name = "frmTipo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. - FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.

Option Explicit

Const colCODTipo = 0
Const colNomTipo = 1
Const colSECUENCIA = 2
Const colHabil = 3
Dim sOpcionSeleccionada As String
Dim sOldBalanceRubro As String

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub
Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub
Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertTipoPrj(txtCodigo, txtDescripcion, CodigoCombo(Me.cboHabil), txtSecuencia) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
'                If sOldBalanceRubro <> CodigoCombo(Me.cboBalanceRubro) Then
'                    If MsgBox("Est� cambiando la Direcci�n, esto modificar� la estructura de dependencias. �Contin�a?", vbYesNo) = vbNo Then
'                        Exit Sub
'                    End If
'                End If
                If CodigoCombo(Me.cboHabil) <> "S" Then
                    If MsgBox("Al inhabilitar la Tipo dejar� inhabilitado todo su �rbol jer�rquico. �Contin�a?", vbYesNo) = vbNo Then
                        Exit Sub
                    End If
                End If
                If sp_UpdateTipoPrj(txtCodigo, txtDescripcion, CodigoCombo(Me.cboHabil), txtSecuencia) Then
'                    If sOldBalanceRubro <> CodigoCombo(Me.cboBalanceRubro) Then
'                        If Not sp_ChangeTipoBalanceRubro(txtCodigo, CodigoCombo(Me.cboBalanceRubro)) Then
'                            MsgBox ("Hubo errores al cambiar la Tipo de BalanceRubro")
'                        End If
'                    End If
                    With grdDatos
                        If .RowSel > 0 Then
                           .TextMatrix(.RowSel, colNomTipo) = txtDescripcion
                          ' .TextMatrix(.RowSel, colNOMBalanceRubro) = TextoCombo(Me.cboBalanceRubro)
                           
                           
                           .TextMatrix(.RowSel, colSECUENCIA) = txtSecuencia
                            .TextMatrix(.RowSel, colHabil) = CodigoCombo(Me.cboHabil)
                       End If
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeleteTipoPrj(txtCodigo) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub
Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub
Private Sub Form_Load()
    Call InicializarCombos
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    Call LimpiarForm(Me)
    Call HabilitarBotones(0)
End Sub
Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True

            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    txtCodigo = "": txtDescripcion = "":  txtSecuencia = ""
                    'cboBalanceRubro.ListIndex = -1
                    cboHabil.ListIndex = 0
'                    cboEjecutor.ListIndex = 1
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = True
                    txtCodigo.SetFocus
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = True
                    txtCodigo.Enabled = False
'                    cboBalanceRubro.Enabled = False
                    txtDescripcion.SetFocus
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub
Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If Trim(txtCodigo.text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtCodigo.text, ":") > 0 Then
        MsgBox ("El C�digo no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtDescripcion.text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If InStr(txtDescripcion.text, ":") > 0 Then
        MsgBox ("a Descripci�n no pude contener el caracter ':'")
        CamposObligatorios = False
        Exit Function
    End If
''    If cboBalanceRubro.ListIndex < 0 Then
''        MsgBox ("Debe asignarle una BalanceRubro")
''        CamposObligatorios = False
''        Exit Function
''    End If
''    If Not valBalanceRubroHabil(CodigoCombo(cboBalanceRubro)) Then
''        MsgBox ("No puede depender de una Balance-Rubro inhabilitado")
''        CamposObligatorios = False
''        Exit Function
''    End If
''    If Trim(txtNombreLargo.Text) = "" Then
''        MsgBox ("Debe completar el Nombre Largo")
''        CamposObligatorios = False
''        Exit Function
''    End If
''    If Trim(txtSigno.Text) = "" Then
''        MsgBox ("Debe completar signo")
''        CamposObligatorios = False
''        txtSigno.SetFocus
''        Exit Function
''    End If
    If Val(txtSecuencia.text) < 1 Then
        MsgBox ("La Secuencia debe ser mayor a 0")
        CamposObligatorios = False
        txtSecuencia.SetFocus
        Exit Function
    End If

End Function
Sub InicializarCombos()
    cboHabil.AddItem "S : Habilitado"
    cboHabil.AddItem "N : Inhabilitado"

'    cboEjecutor.AddItem "S : Ejecuta pedidos"
'    cboEjecutor.AddItem "N : No ejecuta"

End Sub

Sub CargarGrid()
    With grdDatos
        .Clear
        .HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colCODTipo) = "CODIGO"
        .TextMatrix(0, colNomTipo) = "DESCRIPCION"
        .TextMatrix(0, colHabil) = "HABILIT"
        .TextMatrix(0, colSECUENCIA) = "SECUENCIA"
        
        .ColWidth(colHabil) = 800: .ColAlignment(colHabil) = flexAlignCenterCenter
        .ColWidth(colCODTipo) = 930: .ColAlignment(colCODTipo) = 0
        .ColWidth(colNomTipo) = 3250: .ColAlignment(colNomTipo) = 0
        .ColWidth(colSECUENCIA) = 930: .ColAlignment(colSECUENCIA) = 0
    End With
    If Not sp_GetTipoPrj("", "") Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODTipo) = ClearNull(aplRST.Fields.Item("cod_tipoprj"))
            .TextMatrix(.Rows - 1, colNomTipo) = ClearNull(aplRST.Fields.Item("nom_tipoprj"))
            .TextMatrix(.Rows - 1, colHabil) = IIf(ClearNull(aplRST.Fields.Item("flg_habil")) = "N", "N", "S")
            .TextMatrix(.Rows - 1, colSECUENCIA) = ClearNull(aplRST.Fields.Item("secuencia"))
        End With
        aplRST.MoveNext
    Loop
    aplRST.Close
    '{ add -002- a.
    Dim i As Long
    
    With grdDatos
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .row = 0
        For i = 0 To .cols - 1
            .Col = i
            .CellFontBold = True
        Next i
        .FocusRect = flexFocusNone
    End With
    '}
    DoEvents
    With grdDatos
        If .Rows > 1 Then
           .RowSel = 1
           .Col = colNomTipo
           '.Sort = flexSortStringNoCaseAscending
        End If
    End With
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub
'}

Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        sOldBalanceRubro = ""
        If .RowSel > 0 Then
            .HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 0) <> "" Then
                txtCodigo = ClearNull(.TextMatrix(.RowSel, colCODTipo))
                txtDescripcion = ClearNull(.TextMatrix(.RowSel, colNomTipo))
                cboHabil.ListIndex = PosicionCombo(Me.cboHabil, .TextMatrix(.RowSel, colHabil))
                txtSecuencia = ClearNull(.TextMatrix(.RowSel, colSECUENCIA))
            End If
        End If
    End With
End Sub
Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub
Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub
