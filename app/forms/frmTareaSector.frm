VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmTareaSector 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tareas asociadas a un Sector"
   ClientHeight    =   6195
   ClientLeft      =   1740
   ClientTop       =   1530
   ClientWidth     =   9480
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6195
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   8205
      Begin VB.ComboBox cboSector 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   930
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   180
         Width           =   7065
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   180
         TabIndex        =   12
         Top             =   210
         Width           =   480
      End
   End
   Begin VB.Frame pnlBotones 
      Height          =   6195
      Left            =   8220
      TabIndex        =   7
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5100
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   5610
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar el Sector/Tarea seleccionado"
         Top             =   4590
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Crear un nuevo Sector/Tarea"
         Top             =   3570
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1860
      Left            =   0
      TabIndex        =   3
      Top             =   4320
      Width           =   8205
      Begin VB.ComboBox cboTarea 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   870
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   390
         Width           =   6825
      End
      Begin VB.Label Label1 
         Caption         =   "Tarea"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Top             =   420
         Width           =   795
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   60
         Visible         =   0   'False
         Width           =   825
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3630
      Left            =   0
      TabIndex        =   0
      Top             =   630
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   6403
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmTareaSector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 27.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).
' -002- a. FJS 31.07.2007 - Se da un nuevo formato a la grilla para hacerla m�s atractiva al usuario.
' -003- a. FJS 22.01.2009 - Se habilita el scroll para la grilla.

Option Explicit

Const colCODSECTOR = 0
Const colNOMSECTOR = 1
Const colCODTAREA = 2
Const colNOMTAREA = 3

Dim sOpcionSeleccionada As String
Dim sOldSector As String
Dim flgEnCarga As Boolean

Private Sub Form_Load()
    Call InicializarSector
    Call InicializarPantalla
    'Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdDatos)        ' add -003- a.
End Sub

Private Sub InicializarPantalla()
    Me.Top = 0
    Me.Left = 0
    Me.Tag = ""
    sOpcionSeleccionada = ""
    Show
    'Call LimpiarForm(Me)
    Call HabilitarBotones(0)
End Sub

Private Sub cboSector_Click()
    If flgEnCarga = True Then
        Exit Sub
    End If
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    
    flgEnCarga = True
    glSector = CodigoCombo(cboSector, True)
    Call InicializarTar
    Call HabilitarBotones(0)
    flgEnCarga = False
    Call LockProceso(False)
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If Not valTareaHabil(CodigoCombo(cboTarea, True)) Then
                    MsgBox ("La tarea est� marcada como inhabilitada")
                    Exit Sub
                End If
                If sp_InsertTareaSector(CodigoCombo(Me.cboTarea, True), CodigoCombo(Me.cboSector, True)) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "E"
            If sp_DeleteTareaSector(CodigoCombo(Me.cboTarea, True), CodigoCombo(Me.cboSector, True)) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If TypeOf Me.ActiveControl Is ComboBox Then
       If KeyCode = vbKeyDelete Then
          Me.ActiveControl.ListIndex = -1
       End If
    End If
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.Visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            'grdDatos.SetFocus
            cboSector.Enabled = True
            cmdAgregar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            cmdAgregar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            cboSector.Enabled = False
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.Visible = True
                    cboTarea.ListIndex = -1
                    'cboSector.ListIndex = -1
                    fraDatos.Enabled = True
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.Visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
    If cboTarea.ListIndex < 0 Then
        MsgBox ("Debe seleccionar una tarea")
        CamposObligatorios = False
        Exit Function
    End If
    If cboSector.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un Sector")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub InicializarTar()
    Dim xPerfil, xNivel, xArea As String
    Dim xxGr, xxGe, xxDi
    
    Call Status("Cargando Tareas")
    cboTarea.Clear
    If sp_GetTarea(Null) Then
        Do While Not aplRST.EOF
            'cboTarea.AddItem Trim(aplRST(1)) & Space(60) & "||" & aplRST(0)
            cboTarea.AddItem Trim(aplRST(1)) & ESPACIOS & "||" & aplRST(0)
            'cboTarea.AddItem aplRST(0) & ": " & Trim(aplRST(1))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Call Status("Listo.")
End Sub

Private Sub InicializarSector()
    Dim xPerfil, xNivel, xArea As String
    Dim xxGr, xxGe, xxDi
    
    xNivel = getPerfNivel("CSEC")
    xArea = getPerfArea("CSEC")
    
    xxGr = Null
    xxGe = Null
    xxDi = Null
    
    Call Status("Cargando Sector")
    If InPerfil("ADMI") Then
        If sp_GetSectorXt(Null, Null, Null, "S") Then
            Do While Not aplRST.EOF
                cboSector.AddItem Trim(aplRST!nom_direccion) & ": " & Trim(aplRST!nom_gerencia) & ": " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & aplRST(0)
                aplRST.MoveNext
            Loop
            'aplRST.Close
        End If
    ElseIf InPerfil("CSEC") Then
        Select Case xNivel
            Case "DIRE": xxDi = xArea
            Case "GERE": xxGe = xArea
            Case "SECT": xxGr = xArea
        End Select
        If sp_GetSectorXt(xxGr, xxGe, xxDi) Then
            Do While Not aplRST.EOF
                cboSector.AddItem Trim(aplRST!nom_direccion) & " >> " & Trim(aplRST!nom_gerencia) & " >> " & Trim(aplRST!nom_sector) & Space(100) & "||" & Trim(aplRST!cod_sector)
                aplRST.MoveNext
            Loop
            'aplRST.Close
        End If
    End If
    cboSector.ListIndex = 0
    glSector = CodigoCombo(cboSector, True)
    Call InicializarTar
    Call Status("Listo.")
End Sub

Private Sub CargarGrid()
    With grdDatos
        .Clear
        '.HighLight = flexHighlightNever
        .Rows = 1
        .TextMatrix(0, colCODSECTOR) = "Sector": .ColWidth(colCODSECTOR) = 930: .ColAlignment(colCODSECTOR) = 0
        .TextMatrix(0, colNOMSECTOR) = "Nombre": .ColWidth(colNOMSECTOR) = 3250: .ColAlignment(colNOMSECTOR) = 0
        .TextMatrix(0, colCODTAREA) = "Tarea": .ColWidth(colCODTAREA) = 930: .ColAlignment(colCODTAREA) = 0
        .TextMatrix(0, colNOMTAREA) = "Descripcion": .ColWidth(colNOMTAREA) = 3250: .ColAlignment(colNOMTAREA) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    If Not sp_GetTareaSectorXt("", glSector) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colCODTAREA) = ClearNull(aplRST.Fields.Item("cod_tarea"))
            .TextMatrix(.Rows - 1, colNOMTAREA) = ClearNull(aplRST.Fields.Item("nom_tarea"))
            .TextMatrix(.Rows - 1, colCODSECTOR) = ClearNull(aplRST.Fields.Item("cod_sector"))
            .TextMatrix(.Rows - 1, colNOMSECTOR) = ClearNull(aplRST.Fields.Item("nom_sector"))
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    Call MostrarSeleccion
End Sub

'{ add -002- a.
Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call grdDatos_Click
    End If
End Sub
'}

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            '.HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, 0) <> "" Then
                cboTarea.ListIndex = PosicionCombo(Me.cboTarea, .TextMatrix(.RowSel, colCODTAREA), True)
            End If
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    Call MostrarSeleccion
End Sub

'{ add -003- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
'}
