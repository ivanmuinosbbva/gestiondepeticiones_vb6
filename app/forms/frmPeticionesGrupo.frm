VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionesGrupo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Grupos Intervinientes por Sector"
   ClientHeight    =   7170
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   10410
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7170
   ScaleWidth      =   10410
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   1515
      Left            =   60
      TabIndex        =   5
      Top             =   0
      Width           =   9030
      Begin VB.Label lblPerfil 
         Caption         =   "lblPerfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   7140
         TabIndex        =   32
         Top             =   735
         Width           =   1800
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Perfil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6555
         TabIndex        =   31
         Top             =   735
         Width           =   435
      End
      Begin VB.Label lblResponsableNombre 
         Caption         =   "lblResponsableNombre"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1500
         TabIndex        =   30
         Top             =   735
         Width           =   5100
      End
      Begin VB.Label lblResponsable 
         AutoSize        =   -1  'True
         Caption         =   "Responsable"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   29
         Top             =   735
         Width           =   1080
      End
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   675
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6225
         TabIndex        =   17
         Top             =   480
         Width           =   765
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1500
         TabIndex        =   16
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Titulo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1500
         TabIndex        =   15
         Top             =   240
         Width           =   7365
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   7140
         TabIndex        =   14
         Top             =   480
         Width           =   600
      End
      Begin VB.Label Label6 
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   480
         Width           =   645
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   795
      Left            =   60
      TabIndex        =   19
      Top             =   1440
      Width           =   9030
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Width           =   555
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   480
         Width           =   570
      End
      Begin VB.Label lblSector 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   900
         TabIndex        =   21
         Top             =   240
         Width           =   7125
      End
      Begin VB.Label lblSectorEstado 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   900
         TabIndex        =   20
         Top             =   480
         Width           =   2580
      End
   End
   Begin VB.Frame pnlBtnControl 
      Height          =   2325
      Left            =   9120
      TabIndex        =   8
      Top             =   4800
      Width           =   1275
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "Cerrar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   40
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   1680
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   40
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   660
         Width           =   1170
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   40
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   150
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   40
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1170
         Width           =   1170
      End
   End
   Begin VB.Frame pnlFunc1 
      Height          =   4755
      Left            =   9120
      TabIndex        =   6
      Top             =   0
      Width           =   1275
      Begin VB.CommandButton cmdRecursos 
         Caption         =   "Recursos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   2880
         Width           =   1170
      End
      Begin VB.CommandButton cmdInfoAdic 
         Caption         =   "Info. grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   2370
         Width           =   1170
      End
      Begin VB.CommandButton cmdReasignar 
         Caption         =   "Reasignar grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   120
         Width           =   1170
      End
      Begin VB.CommandButton cmdCambiarEstado 
         Caption         =   "Cambio de estado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Despliega una ventana para cambiar el estado del grupo a uno nuevo"
         Top             =   1600
         Width           =   1170
      End
   End
   Begin VB.Frame fraGrupo 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Left            =   60
      TabIndex        =   2
      Top             =   4800
      Width           =   9060
      Begin VB.TextBox obsSolic 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1260
         Left            =   1020
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   24
         Top             =   840
         Width           =   7605
      End
      Begin VB.ComboBox cboGrupo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1020
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   7635
      End
      Begin VB.Label Label3 
         Caption         =   "Observaci�n al estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   25
         Top             =   600
         Width           =   2610
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   293
         Width           =   510
      End
      Begin VB.Label lblAccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5940
         TabIndex        =   3
         Top             =   300
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdGrupo 
      Height          =   2490
      Left            =   60
      TabIndex        =   1
      Top             =   2280
      Width           =   9060
      _ExtentX        =   15981
      _ExtentY        =   4392
      _Version        =   393216
      Cols            =   17
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      HighLight       =   2
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPeticionesGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colGrCodsector = 0
Private Const colGrSector = 1
Private Const colGrEstado = 2
Private Const colGrFeEstado = 3
Private Const colGrSituacion = 4
Private Const colGrHoras = 5
Private Const colGrFinicio = 6
Private Const colGrFfin = 7
Private Const colGrFiniReal = 8
Private Const colGrFfinReal = 9
Private Const colGrDireccion = 10
Private Const colGrGerencia = 11
Private Const colGrCodestado = 12
Private Const colGrhst_nrointerno_sol = 13
Private Const colGrFe_Produccion = 14
Private Const colGrFe_Suspension = 15
Private Const colGrFe_MotivoSusp = 16
Private Const colGr_NomResponsable = 17
Private Const colGr_Perfil = 18
Private Const colGr_PrioEjej = 19

Private Const col_TOTCOLS = 20

Dim XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, XfPasajeProduccion, XfFechaFinSuspension  ' upd -003- a.
Dim sOpcionSeleccionada As String
Dim sClasePet As String 'GMT01
Dim EstadoSector As String
Dim EstadoPeticion As String
Dim EstadoSolicitud As String
'Dim sGerencia As String
'Dim sDireccion As String
'Dim sCoorSect As String
Dim xPerfNivel, xPerfArea As String
Dim sec_nrointerno_sol
'{ add -003- a.
Dim cPetClase As String
Dim XnCodigoMotivo As Integer
'}

'{ add -005- a.
'Dim cGrupoHomologador As String
'Dim cGrupoHomologadorSector As String
Dim cEstadoHomologacion As String
Dim cMensaje As String
'}
Dim cEstadoOriginal As String
Dim bExisteElGrupoHomologadorEnPeticion As Boolean

Private Sub Form_Load()
    'Buscar_Homologacion                     ' add -016- a.
    cEstadoOriginal = ""                    ' add -007- a.
    Call InicializarCombos
    Call InicializarPantalla
    Call FormCenter(Me, mdiPrincipal, False)
    Call IniciarScroll(grdGrupo)
    Call Puntero(False)
    'lblResponsableNombre = ""               ' add -019- a.
End Sub

Private Sub InicializarCombos(Optional sOpcion As String)     ' upd -023- a.
    Dim sDireccion As String
    Dim sGerencia As String
    
    If sp_GetGrupoXt(Null, glSector, IIf(sOpcion = "", Null, "S")) Then
        cboGrupo.Clear
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboGrupo.AddItem ClearNull(aplRST!nom_sector) & " � " & ClearNull(aplRST!nom_grupo) & Space(150) & "||" & ClearNull(aplRST!cod_grupo) & "|" & Trim(aplRST!cod_sector) ' add -022- a.
                'cboGrupo.AddItem ClearNull(aplRST!nom_sector) & " � " & ClearNull(aplRST!nom_grupo) & Space(150) & "||" & ClearNull(aplRST!cod_grupo)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboGrupo.AddItem sDireccion & "� " & sGerencia & "� " & Trim(aplRST!nom_sector) & "� " & Trim(aplRST!nom_grupo) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_grupo)
            End If
            aplRST.MoveNext
        Loop
    End If
End Sub

Private Sub InicializarPantalla()
    Dim auxNro As String
    
    Me.Tag = ""
    Call LockProceso(True)
    xPerfNivel = getPerfNivel(glUsrPerfilActual)
    xPerfArea = getPerfArea(glUsrPerfilActual)
    
    cmdAgregar.Enabled = False
    cmdEliminar.Enabled = False
    cmdCambiarEstado.Enabled = False
    cmdInfoAdic.Enabled = False
    cmdRecursos.Enabled = False
    cmdReasignar.Enabled = False
    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = True
    cboGrupo.Enabled = False
    
    bExisteElGrupoHomologadorEnPeticion = False
    lblAccion = ""
    EstadoSolicitud = ""
    lblResponsableNombre = "-"
    lblPerfil = "-"
    
    If InStr(1, "ADMI|SADM|", glUsrPerfilActual, vbTextCompare) > 0 Then cmdReasignar.visible = True
    
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
            auxNro = "S/N"
        Else
            auxNro = ClearNull(aplRST!pet_nroasignado)
        End If
        lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
        lblPrioridad = IIf(ClearNull(aplRST!prioridad) = "", "-", ClearNull(aplRST!prioridad))
        lblEstado.Caption = ClearNull(aplRST!nom_estado)
        EstadoPeticion = ClearNull(aplRST!cod_estado)
        cPetClase = ClearNull(aplRST!cod_clase)         ' add -003- a.
    End If
    
    If sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then
        bExisteElGrupoHomologadorEnPeticion = True
    End If

    If glSector <> "" Then
        If sp_GetPeticionSector(glNumeroPeticion, glSector) Then
            EstadoSector = ClearNull(aplRST!cod_estado)
            lblSector.Caption = ClearNull(aplRST!nom_sector)
            lblSectorEstado.Caption = ClearNull(aplRST!nom_estado)
            XfIniPlan = aplRST!fe_ini_plan
            XfFinPlan = aplRST!fe_fin_plan
            XfIniReal = aplRST!fe_ini_real
            XfFinReal = aplRST!fe_fin_real
        End If
        If InStr(1, "OPINIO|EVALUA|ESTIMA|PLANIF|ACONFI|", EstadoSector, vbTextCompare) > 0 Then EstadoSolicitud = EstadoSector
        If EstadoSector = "ESTIOK" Then EstadoSolicitud = "ESTIMA"
        If EstadoSector = "SUSPEN" Then EstadoSolicitud = "SUSPEN"
        Me.Caption = "Grupos intervinientes por Sector"
    End If
    ' TODO: Revisar muy bien esto! 22.11.2016
    '{ add -016- a.
    If sp_GetPeticionGrupo(glNumeroPeticion, glSector, Null) Then
        If aplRST.RecordCount = 1 Then
            'If ClearNull(aplRST.Fields!cod_grupo) = ClearNull(cGrupoHomologador) Then
            If ClearNull(aplRST.Fields!cod_grupo) = ClearNull(glHomologacionGrupo) Then
                EstadoSolicitud = EstadoPeticion
            End If
        End If
    End If
    '}
    Call HabilitarBotones(0)
    Call LockProceso(False)
End Sub

'Private Sub cboGrupo_Click()
'    If Not LockProceso(True) Then
'        Exit Sub
'    End If
'    glGrupo = CodigoCombo(cboGrupo, True)
'    Call LockProceso(False)
'End Sub

Private Sub cmdAgregar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    cEstadoOriginal = EstadoSolicitud   ' add -007- a.
    If EstadoSolicitud = "" Then
        EstadoSolicitud = PedirEstado()
        If EstadoSolicitud = "" Then
            MsgBox "No ha seleccionado un estado. Antes de agregar un nuevo grupo" & vbCrLf & "debe seleccionar el estado del mismo.", vbExclamation + vbOKOnly, "Estado del nuevo grupo"  ' add -007- a.
            Call LockProceso(False)
            Exit Sub
        End If
    End If
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
    Call LockProceso(False)
End Sub

Private Sub cmdCerrar_Click()
    If sOpcionSeleccionada = "" Then
        If Me.Tag = "REFRESH" Then
            Call touchForms
        End If
        Unload Me
    Else
        sOpcionSeleccionada = ""
        cmdCerrar.Caption = "Cerrar"
        Call HabilitarBotones(0, False)
        EstadoSolicitud = cEstadoOriginal    ' add -007- a. Se limpia la variable (con el valor original para cuando tiene Estado) para que vuelva a solicitar el estado de los nuevos grupos a agregar (si corresponde).
    End If
End Sub

Private Sub cmdEliminar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
    Call LockProceso(False)
End Sub

Private Sub cmdConfirmar_Click()
    Dim xSector As String
    Dim auxSector As String
    Dim vRetorno() As String
    Dim NuevoEstadoSector As String
    Dim NuevoEstadoPeticion As String
    Dim bGrupoTecnico As Boolean
    Dim NroHistorial As Long
    Dim per_nrointerno As Long      ' add -028- a.
    
    If Not LockProceso(True) Then
        Exit Sub
    End If
    
    Call Puntero(True)
    Call Status("Procesando...")
    Call touchForms
    
    bGrupoTecnico = False
    glGrupo = CodigoCombo(cboGrupo, True)
    
    Select Case sOpcionSeleccionada
        Case "A"
           If CamposObligatorios Then
                If Not valGrupoHabil(glGrupo) Then
                    MsgBox "El grupo seleccionado est� marcado como inhabilitado.", vbExclamation + vbOKOnly
                    Call LockProceso(False)
                    Call Puntero(False)
                    Exit Sub
                End If
                If EstadoSolicitud = "ACONFI" Then
                    If Not sp_GetRecursoActuanteEstado("GRU", "GRUP", glGrupo, "PLANIF") Then
                        MsgBox "No existen recursos que puedan procesar esta petici�n.", vbExclamation + vbOKOnly
                        Call LockProceso(False)
                        Call Puntero(False)
                        Exit Sub
                    End If
                Else
                    If Not sp_GetRecursoActuanteEstado("GRU", "GRUP", glGrupo, EstadoSolicitud) Then
                        MsgBox "No existen recursos que puedan procesar esta petici�n.", vbExclamation + vbOKOnly
                        Call LockProceso(False)
                        Call Puntero(False)
                        Exit Sub
                    End If
                End If
                
                'Se verifica si el grupo es tecnico
                If sp_GetGrupoHomologable(glGrupo) Then
                    bGrupoTecnico = True
                End If
                
                ' GMT01 - INI
                If bGrupoTecnico Then
                   If InStr(1, "NUEV|EVOL|", cPetClase, vbTextCompare) > 0 Then
                      If Not validacionTecnicaCompleta Then
                         MsgBox "Antes de agregar un grupo t�cnico, el referente" & vbCrLf & "de sistema debe completar la validaci�n t�cnica.", vbExclamation + vbOKOnly, "Validaci�n t�cnica"
                         Call LockProceso(False)
                         Call Puntero(False)
                         Exit Sub
                      End If
                   End If
                End If
                ' GMT01 - FIN
                
                xSector = glSector
                If sp_GetPeticionSector(glNumeroPeticion, xSector) Then
                    If Not aplRST.EOF Then
                        EstadoSector = ClearNull(aplRST!cod_estado)
                        XfIniPlan = aplRST!fe_ini_plan
                        XfFinPlan = aplRST!fe_fin_plan
                        XfIniReal = aplRST!fe_ini_real
                        XfFinReal = aplRST!fe_fin_real
                        XfPasajeProduccion = Null       ' TODO: Esto no deberia estar. No hace falta ya
                        XfFechaFinSuspension = Null
                    End If
                Else
                    Call sp_InsertPeticionSector(glNumeroPeticion, xSector, Null, Null, Null, Null, 0, EstadoSolicitud, date, "", 0, "0")
                    EstadoSector = EstadoSolicitud
                End If
                NuevoEstadoSector = EstadoSector
                NuevoEstadoPeticion = EstadoPeticion
                '{ add -003- a. REVISAR ESTO!!!
                XfPasajeProduccion = Null
                XfFechaFinSuspension = Null
                '}
                If InStr(1, "PLANOK|EJECUC|SUSPEN|REVISA", EstadoSolicitud) = 0 Then
                    XfIniPlan = Null
                    XfFinPlan = Null
                    XfIniReal = Null
                    XfFinReal = Null
                End If
                If InStr(1, "PLANOK", EstadoSolicitud) > 0 Then
                    XfIniReal = Null
                    XfFinReal = Null
                End If
                
                If sp_InsertPeticionGrupo(glNumeroPeticion, glGrupo, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, XfPasajeProduccion, XfFechaFinSuspension, 0, 0, EstadoSolicitud, date, "", 0, "0") Then
                    If EstadoSolicitud <> "ACONFI" Then
                        Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, glGrupo, EstadoSolicitud, "El grupo agregado a la petici�n es: " & TextoCombo(cboGrupo, glGrupo, True), "")
                    End If
                    If sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then
                        If bGrupoTecnico Then
                            Call sp_DoMensajeHomologacion(glNumeroPeticion, "CGRU", glHomologacionGrupo, 110, EstadoSolicitud, "El grupo agregado a la petici�n es: " & TextoCombo(cboGrupo, glGrupo, True))
                        End If
                    End If
                    If EstadoSolicitud <> "OPINIO" And EstadoSolicitud <> "EVALUA" And EstadoSolicitud <> "ACONFI" Then
                        Call Status("Integraci�n de sector...")
                        sec_nrointerno_sol = 0
                        If sp_GetPeticionSector(glNumeroPeticion, xSector) Then
                            sec_nrointerno_sol = ClearNull(aplRST!hst_nrointerno_sol)
                        End If
                        Call Status("Integraci�n de grupo...")
                        Call getIntegracionGrupo(glNumeroPeticion, xSector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
                        NuevoEstadoSector = getNuevoEstadoSector(glNumeroPeticion, xSector)
                        If NuevoEstadoSector <> "TERMIN" Then
                            fFinReal = Null
                        End If
                        If NuevoEstadoSector = "REVISA" Then
                            sec_nrointerno_sol = 0
                        End If
                        If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
                            Call sp_UpdatePeticionSector(glNumeroPeticion, xSector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, date, "", sec_nrointerno_sol, 0)
                        Else
                            Call sp_UpdatePeticionSector(glNumeroPeticion, xSector, Null, Null, Null, Null, hsPresup, NuevoEstadoSector, date, "", sec_nrointerno_sol, 0)
                        End If
                        If InStr(1, "PLANIF|ESTIMA", NuevoEstadoSector) > 0 Then
                            Call sp_DelMensajePerfil(glNumeroPeticion, "CSEC", "SECT", xSector)
                        End If
                        Call Status("Integraci�n de la petici�n...")
                        Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
                        NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
                        Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
                        Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)
                        If NuevoEstadoPeticion <> "TERMIN" Then
                            fFinReal = Null
                        End If
                        If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoPeticion) > 0 Then
                            Call sp_UpdatePetFechas(glNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
                        Else
                            Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
                        End If
                    End If
                    Call Status("Historial...")
                    NroHistorial = sp_AddHistorial(glNumeroPeticion, "GNEW000", NuevoEstadoPeticion, xSector, NuevoEstadoSector, CodigoCombo(cboGrupo, True), EstadoSolicitud, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, IIf(Len(Trim(obsSolic.text)) > 0, "� " & Trim(obsSolic.text) & " �", ""))
                    Call sp_UpdatePeticionGrupo(glNumeroPeticion, glGrupo, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, XfPasajeProduccion, XfFechaFinSuspension, XnCodigoMotivo, 0, EstadoSolicitud, date, "", NroHistorial, "0")     ' upd -003- a.
                    sOpcionSeleccionada = ""
                    '{ add -003- a.
                    ' ***********************************************************************************************************************************************************************************************************************
                    ' HOMOLOGACION
                    ' ***********************************************************************************************************************************************************************************************************************
                    If InStr(1, "ESTIMA|PLANIF|REVISA|APROBA|EJECUC|ESTIOK|PLANOK|ESTIRK|PLANRK|EJECRK|SUSPEN|", EstadoSolicitud, vbTextCompare) > 0 Then    ' add -020- a. El mensaje debe ser enviado solo si la petici�n se encuentra en alguno de los estados post-aprobatorios
                        Call Status("Anexando grupo homologador...")
                        'cGrupoHomologador = ""
                        'cGrupoHomologadorSector = ""
                        cEstadoHomologacion = ""
                        If cPetClase <> "SINC" Then
                            ' 2. Determina si el grupo a agregar a la petici�n es Homologable
                            'If sp_GetGrupoHomologable(glGrupo) Then
                            If bGrupoTecnico Then
'                                ' 3. Primero obtengo el grupo Homologador
'                                If sp_GetGrupoHomologacion Then
'                                    cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
'                                    cGrupoHomologadorSector = ClearNull(aplRST.Fields!cod_sector)
'                                End If
'                                If Not valGrupoHabil(cGrupoHomologador) Then
'                                    MsgBox ("El grupo Homologador se encuentra en estado inhabilitado.")
'                                    Call LockProceso(False)
'                                    Exit Sub
'                                End If
                                ' Prefijo el estado del sector y del grupo Homologador seg�n definici�n del P950
                                ' Nota: Antes de prefijar el estado con que nace el grupo y sector Homologaci�n, evaluo
                                '       si alguno de los grupos homologables de la petici�n se encuentra en estado "En ejecuci�n"
                                '       Si as� fuera, entonces el grupo homologaci�n debe incorporarse a la petici�n con estado
                                '       "A planificar" en lugar de "A estimar esfuerzo".
                                cEstadoHomologacion = "ESTIMA"
                                If sp_GetPetGrupoHomologable(glNumeroPeticion) Then
                                    ' No existe ning�n grupo homologable, el grupo se incorpora en estado "A estimar esfuerzo"
                                    If aplRST.EOF Then
                                        cEstadoHomologacion = "ESTIMA"
                                    Else
                                        ' Existe al menos un grupo homologable...
                                        ' Evaluo si alguno de los grupos se encuentra en estado "En ejecuci�n", entonces el
                                        ' el grupo se incorpora en estado "A planificar"
                                        Do While Not aplRST.EOF
                                            If ClearNull(aplRST.Fields!cod_estado) = "EJECUC" Then
                                                cEstadoHomologacion = "PLANIF"
                                                Exit Do
                                            End If
                                            aplRST.MoveNext
                                            DoEvents
                                        Loop
                                        ' Busco si la petici�n tiene un conforme de pruebas del usuario,
                                        ' y si ya existe un grupo en ejecuci�n, entonces el grupo homologaci�n
                                        ' debe nacer con el estado "En ejecuci�n"
                                        If cEstadoHomologacion = "PLANIF" Then
                                            If sp_GetPeticionConf(glNumeroPeticion, "TESP", Null) Or _
                                               sp_GetPeticionConf(glNumeroPeticion, "TEST", Null) Then
                                                    cEstadoHomologacion = "EJECUC"
                                            End If
                                        End If
                                    End If
                                End If
'                                ' 4. Agrego el sector del grupo homologador (si no existe)
'                                If Not sp_GetPeticionSector(glNumeroPeticion, glHomologacionGrupo) Then
'                                    Call sp_InsertPeticionSector(glNumeroPeticion, glHomologacionSector, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, 0, cEstadoHomologacion, date, "", 0, "0") ' upd -003- a.
'                                End If
                                XfIniPlan = Null
                                XfFinPlan = Null
                                XfIniReal = Null
                                XfFinReal = Null
                                XfPasajeProduccion = Null
                                XfFechaFinSuspension = Null
                                
                                'If Not sp_GetPetGrupoHomologacion(glNumeroPeticion) Then        ' add -011- a. Determina si ya existe para la petici�n el grupo Homologador
                                If Not bExisteElGrupoHomologadorEnPeticion Then
                                    ' 5. Es agregado el grupo Homologador en el estado correspondiente
                                    If sp_InsertPeticionGrupo(glNumeroPeticion, glHomologacionGrupo, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, XfPasajeProduccion, XfFechaFinSuspension, 0, 0, cEstadoHomologacion, date, "", 0, "0") Then
                                        Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, glHomologacionGrupo, cEstadoHomologacion, "", "")
                                        Call Status("Actualizando el historial...")
                                        NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", NuevoEstadoPeticion, glHomologacionSector, cEstadoHomologacion, glHomologacionGrupo, cEstadoHomologacion, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Asignaci�n autom�tica del grupo homologador �")
                                        Call sp_UpdatePeticionGrupo(glNumeroPeticion, glHomologacionGrupo, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, XfPasajeProduccion, XfFechaFinSuspension, XnCodigoMotivo, 0, cEstadoHomologacion, date, "", NroHistorial, "0")
                                    End If
                                    ' 4. Agrego el sector del grupo homologador (si no existe)
                                    If Not sp_GetPeticionSector(glNumeroPeticion, glHomologacionGrupo) Then
                                        Call sp_InsertPeticionSector(glNumeroPeticion, glHomologacionSector, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, 0, cEstadoHomologacion, date, "", 0, "0") ' upd -003- a.
                                    End If
                                Else
                                    Dim bHayOtros As Boolean
                                    bHayOtros = False
                                    ' Si Homologaci�n se encuentra "Cancelado", debe ser reactivado
                                    'If InStr(1, "CANCEL|", Trim(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                                    If InStr(1, "CANCEL|", cEstadoHomologacion, vbTextCompare) > 0 Then
                                        Call sp_UpdatePeticionGrupo(glNumeroPeticion, glHomologacionGrupo, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, XfPasajeProduccion, XfFechaFinSuspension, XnCodigoMotivo, 0, cEstadoHomologacion, date, "", 0, "0")
                                        Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, glHomologacionGrupo, cEstadoHomologacion, "", "")
                                        Call Status("Actualizando el historial...")
                                        NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", NuevoEstadoPeticion, glHomologacionSector, cEstadoHomologacion, glHomologacionGrupo, cEstadoHomologacion, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Reactivaci�n autom�tica del grupo homologador �")
                                        Call sp_UpdatePeticionGrupo(glNumeroPeticion, glHomologacionGrupo, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, XfPasajeProduccion, XfFechaFinSuspension, XnCodigoMotivo, 0, cEstadoHomologacion, date, "", NroHistorial, "0")
                                        If sp_GetPeticionGrupo(glNumeroPeticion, glHomologacionSector, Null) Then        ' Si no existen otros grupos adem�s del grupo homologador, le fuerzo el estado del sector al mismo que el grupo Homologador
                                            Do While Not aplRST.EOF
                                                If ClearNull(aplRST.Fields!cod_grupo) <> glHomologacionGrupo Then
                                                    bHayOtros = True
                                                    Exit Do
                                                End If
                                                aplRST.MoveNext
                                                DoEvents
                                            Loop
                                            If Not bHayOtros Then
                                                Call sp_UpdatePeticionSector(glNumeroPeticion, glHomologacionSector, XfIniPlan, XfFinPlan, XfIniReal, XfFinReal, 0, cEstadoHomologacion, date, "", 0, "0")
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                    Call InicializarPantalla
                End If
           End If
       Case "E"
            Call Puntero(True)
            Call Status("Integrando grupos...")
            Call sp_DeletePeticionGrupo(glNumeroPeticion, glSector, glGrupo)
            Call sp_DoMensaje("GRU", "GDEL000", glNumeroPeticion, glGrupo, EstadoSolicitud, "Se desvincul� el grupo de la petici�n: " & ClearNull(grdGrupo.TextMatrix(grdGrupo.rowSel, colGrSector)), "")
            Call chkGrupoOpiEva(glNumeroPeticion, glSector)
            Call Status("Integrando sectores...")
            Call chkSectorOpiEva(glNumeroPeticion)
            Call Status("Integraci�n de sectores...")
            sec_nrointerno_sol = 0
            If sp_GetPeticionSector(glNumeroPeticion, glSector) Then
                If Not aplRST.EOF Then
                  sec_nrointerno_sol = ClearNull(aplRST!hst_nrointerno_sol)
                End If
            End If
            Call getIntegracionGrupo(glNumeroPeticion, glSector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
            NuevoEstadoSector = getNuevoEstadoSector(glNumeroPeticion, glSector)
            If NuevoEstadoSector <> "TERMIN" Then
                fFinReal = Null
            End If
            If NuevoEstadoSector = "REVISA" Then
                sec_nrointerno_sol = 0
            End If
            If NuevoEstadoSector = "" Then
                NuevoEstadoSector = "CANCEL"
            End If
            If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
                Call sp_UpdatePeticionSector(glNumeroPeticion, glSector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, date, "", sec_nrointerno_sol, 0)
            Else
                Call sp_UpdatePeticionSector(glNumeroPeticion, glSector, Null, Null, Null, Null, 0, NuevoEstadoSector, date, "", sec_nrointerno_sol, 0)
            End If
            If InStr(1, "PLANIF|ESTIMA|", NuevoEstadoSector) > 0 Then
                Call sp_DelMensajePerfil(glNumeroPeticion, "CSEC", "SECT", glSector)
            End If
            Call Status("Integraci�n de la petici�n...")
            Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
            NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
            Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
            'Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)     ' Arreglar la tabla y volver a activar todas las llamadas!
            If NuevoEstadoPeticion <> "TERMIN" Then
                fFinReal = Null
            End If
            If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoPeticion) > 0 Then
                Call sp_UpdatePetFechas(glNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
            Else
                Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
            End If
            Call Status("Historial...")
            NroHistorial = sp_AddHistorial(glNumeroPeticion, "GDEL000", EstadoPeticion, glSector, EstadoSector, glGrupo, "", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, IIf(Len(Trim(obsSolic.text)) > 0, "� " & Trim(obsSolic.text) & " �", ""))
            sOpcionSeleccionada = ""
            Call InicializarPantalla
            Call Status("Listo.")
    End Select
    Call Puntero(False)
    Call LockProceso(False)
End Sub

Private Sub cmdCambiarEstado_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glGrupo = "" Then
        MsgBox ("Debe Seleccionar un Grupo")
        Call LockProceso(False)
        Exit Sub
    End If
    Call LockProceso(False)
    frmPeticionesGrupoOperar.Show 1
    If Me.Tag = "REFRESH" Then
        DoEvents
        Call InicializarPantalla
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdInfoAdic_Click()
    Call LockProceso(False)
    frmPeticionesGrupoAdic.Show 1
    If Me.Tag = "REFRESH" Then
        DoEvents
        Call InicializarPantalla
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdReasignar_Click()
    If Not LockProceso(True) Then
        Exit Sub
    End If
    If glGrupo = "" Then
        Call LockProceso(False)
        MsgBox ("Debe Seleccionar un Grupo")
        Exit Sub
    End If
    Call LockProceso(False)
    frmPeticionesGrupoReasignar.Show vbModal
    DoEvents
    If Me.Tag = "REFRESH" Then
        Call InicializarPantalla
    End If
    Call LockProceso(False)
End Sub

Private Sub cmdRecursos_Click()
    frmPeticionesGrupoRecurso.Show 1
    DoEvents
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    lblAccion = ""
    
    cmdAgregar.Enabled = False
    cmdEliminar.Enabled = False
    cmdCambiarEstado.Enabled = False
    cmdInfoAdic.Enabled = False
    cmdRecursos.Enabled = False
    cmdReasignar.Enabled = False
    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = True
    cboGrupo.Enabled = False
    obsSolic.Locked = True
    Select Case nOpcion
        Case 0
            InicializarCombos       ' add -023- a.
            cboGrupo.ListIndex = -1
            cmdCerrar.Caption = "Cerrar"
            grdGrupo.Enabled = True
            VerAgregar
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            cmdAgregar.Enabled = False
            grdGrupo.Enabled = False
            Select Case sOpcionSeleccionada
                Case "A"  'puede dar altas un administrador y un coord sector
                    Call InicializarCombos(sOpcionSeleccionada)   ' add -023- a.
                    cboGrupo.Enabled = True
                    cmdConfirmar.Enabled = True
                    obsSolic.Locked = False
                    obsSolic.text = ""
                    cmdConfirmar.Enabled = True
                    cmdCerrar.Caption = "Cancelar"
                    lblAccion = "Nuevo Grupo"
                    cboGrupo.SetFocus
                Case "E"
                    cmdConfirmar.Enabled = True
                    cmdCerrar.Caption = "Cancelar"
                    lblAccion = "ELIMINAR GRUPO"
                    obsSolic.text = ""      ' add -020- b.
            End Select
    End Select
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = False
    '{ add -004- a.
    'If glGerencia = "DESA" Then
        If cPetClase = "SINC" Then
            If InStr(1, "ADMI|SADM|BPAR|GBPE|", glUsrPerfilActual, vbTextCompare) = 0 Then
                'cMensaje = "No puede agregar sectores/grupos de la gerencia de DyD a una petici�n sin clase." & vbCrLf & _
                '           "Solo el Business Partner puede agregar sectores/grupos a una petici�n que a�n no ha sido clasificada."
                cMensaje = "No puede agregar grupos a una petici�n sin clase." & vbCrLf & vbCrLf & _
                           "Solo el Ref. de Sistema o el Ref. de RGP puede agregar" & vbCrLf & _
                           "grupos a una petici�n que a�n no ha sido clasificada."
                MsgBox cMensaje, vbExclamation + vbOKOnly, "Agregar grupo"
                Exit Function
            End If
        End If
    'End If
    '}
    If cboGrupo.ListIndex < 0 Then
        MsgBox "Debe especificar un grupo", vbExclamation, vbOKOnly
        Exit Function
    End If
    CamposObligatorios = True
End Function

Private Sub CargarGrid()
    With grdGrupo
        .Clear
        .cols = col_TOTCOLS
        .RowHeightMin = 265
        .BackColorSel = RGB(58, 110, 165)
        .Rows = 1
        .TextMatrix(0, colGrCodsector) = "Cod.Gr.": .ColWidth(colGrCodsector) = 800: .ColAlignment(colGrCodsector) = flexAlignLeftCenter
        .TextMatrix(0, colGrSector) = "Grupo": .ColWidth(colGrSector) = 2800: .ColAlignment(colGrSector) = flexAlignLeftCenter
        .TextMatrix(0, colGrCodestado) = "estadogrupo": .ColWidth(colGrCodestado) = 0: .ColAlignment(colGrCodestado) = flexAlignLeftCenter
        .TextMatrix(0, colGrEstado) = "Estado": .ColWidth(colGrEstado) = 2300: .ColAlignment(colGrEstado) = flexAlignLeftCenter
        .TextMatrix(0, colGrFeEstado) = "F.Estado": .ColWidth(colGrFeEstado) = 950: .ColAlignment(colGrFeEstado) = flexAlignLeftCenter
        .TextMatrix(0, colGrSituacion) = "Situaci�n": .ColWidth(colGrSituacion) = 1300: .ColAlignment(colGrSituacion) = flexAlignLeftCenter
        .TextMatrix(0, colGrHoras) = "Horas": .ColWidth(colGrHoras) = 700: .ColAlignment(colGrHoras) = flexAlignLeftCenter
        .TextMatrix(0, colGrFinicio) = "F.Ini.Planif": .ColWidth(colGrFinicio) = 1200: .ColAlignment(colGrFinicio) = flexAlignLeftCenter
        .TextMatrix(0, colGrFfin) = "F.Fin Planif.": .ColWidth(colGrFfin) = 1200: .ColAlignment(colGrFfin) = flexAlignLeftCenter
        .TextMatrix(0, colGrFiniReal) = "F.Ini.Real": .ColWidth(colGrFiniReal) = 1200: .ColAlignment(colGrFiniReal) = flexAlignLeftCenter
        .TextMatrix(0, colGrFfinReal) = "F.Fin.Real": .ColWidth(colGrFfinReal) = 1200: .ColAlignment(colGrFfinReal) = flexAlignLeftCenter
        .TextMatrix(0, colGrDireccion) = "Dir": .ColWidth(colGrDireccion) = 0: .ColAlignment(colGrDireccion) = flexAlignLeftCenter       '400
        .TextMatrix(0, colGrGerencia) = "Ger": .ColWidth(colGrGerencia) = 0: .ColAlignment(colGrGerencia) = flexAlignLeftCenter         '400
        .TextMatrix(0, colGrhst_nrointerno_sol) = "TxtSol": .ColWidth(colGrhst_nrointerno_sol) = 0: .ColAlignment(colGrhst_nrointerno_sol) = flexAlignLeftCenter      '400
        .TextMatrix(0, colGrFe_Produccion) = "F.Pasaj.Prod.": .ColWidth(colGrFe_Produccion) = 1300: .ColAlignment(colGrFe_Produccion) = flexAlignLeftCenter
        .TextMatrix(0, colGrFe_Suspension) = "F.Fin.Suspen.": .ColWidth(colGrFe_Suspension) = 1300: .ColAlignment(colGrFe_Suspension) = flexAlignLeftCenter
        .TextMatrix(0, colGrFe_MotivoSusp) = "Motivo de Suspensi�n": .ColWidth(colGrFe_MotivoSusp) = 3600: .ColAlignment(colGrFe_MotivoSusp) = flexAlignLeftCenter
        .TextMatrix(0, colGr_NomResponsable) = "Responsable del �rea": .ColWidth(colGr_NomResponsable) = 0: .ColAlignment(colGr_NomResponsable) = flexAlignLeftCenter
        .TextMatrix(0, colGr_Perfil) = "Perfil": .ColWidth(colGr_Perfil) = 0
        .TextMatrix(0, colGr_PrioEjej) = "Prior. Ejec.": .ColWidth(colGr_PrioEjej) = 1000
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusNone
        Call CambiarEfectoLinea(grdGrupo, prmGridEffectFontBold)
    End With
    
    If Not sp_GetPeticionGrupo(glNumeroPeticion, glSector, Null) Then GoTo Fin
    Call Status("Cargando grupos...")
    Do While Not aplRST.EOF
        With grdGrupo
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, colGrSector) = ClearNull(aplRST!nom_grupo)
            .TextMatrix(.Rows - 1, colGrEstado) = ClearNull(aplRST!nom_estado)
            .TextMatrix(.Rows - 1, colGrFeEstado) = ClearNull(aplRST!fe_estado)                     ' add -018- a.
            .TextMatrix(.Rows - 1, colGrSituacion) = ClearNull(aplRST!nom_situacion)
            .TextMatrix(.Rows - 1, colGrFinicio) = IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrFfin) = IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrFiniReal) = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrFfinReal) = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "dd/mm/yyyy"), "")
            .TextMatrix(.Rows - 1, colGrHoras) = IIf(Not IsNull(aplRST!horaspresup), aplRST!horaspresup, "")
            .TextMatrix(.Rows - 1, colGrGerencia) = ClearNull(aplRST!cod_gerencia)
            .TextMatrix(.Rows - 1, colGrCodestado) = ClearNull(aplRST!cod_estado)
            .TextMatrix(.Rows - 1, colGrDireccion) = ClearNull(aplRST!cod_direccion)
            .TextMatrix(.Rows - 1, colGrCodsector) = ClearNull(aplRST!cod_grupo)
            .TextMatrix(.Rows - 1, colGrhst_nrointerno_sol) = ClearNull(aplRST!hst_nrointerno_sol)
            .TextMatrix(.Rows - 1, colGrFe_Produccion) = ClearNull(aplRST!fe_produccion)
            .TextMatrix(.Rows - 1, colGrFe_Suspension) = ClearNull(aplRST!fe_suspension)
            .TextMatrix(.Rows - 1, colGrFe_MotivoSusp) = ClearNull(aplRST!nom_motivo)
            .TextMatrix(.Rows - 1, colGr_NomResponsable) = ClearNull(aplRST!nom_responsable)
            .TextMatrix(.Rows - 1, colGr_Perfil) = ClearNull(aplRST!perfil)
            .TextMatrix(.Rows - 1, colGr_PrioEjej) = ClearNull(aplRST!prio_ejecuc)
            If .Rows Mod 2 > 0 Then Call PintarLinea(grdGrupo, prmGridFillRowColorLightGrey2)
        End With
        aplRST.MoveNext
        DoEvents
    Loop
Fin:
    Call MostrarSeleccion
    Call Status("Listo.")
End Sub

''{ add -016- a.
'Private Sub Buscar_Homologacion()
'    If sp_GetGrupoHomologacion Then
'        cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
'    End If
'End Sub
''}

Private Sub MostrarSeleccion()
    Dim sEstado As String
        
    glGrupo = ""
    grdGrupo.Enabled = False
    'cmdEliminar.Enabled = False
    Call setHabilCtrl(cmdCambiarEstado, DISABLE)
    Call setHabilCtrl(cmdInfoAdic, DISABLE)
    Call setHabilCtrl(cmdRecursos, DISABLE)
    Call setHabilCtrl(cmdReasignar, DISABLE)
    
'    cmdCambiarEstado.Enabled = False
'    cmdInfoAdic.Enabled = False
'    cmdRecursos.Enabled = False
'    cmdReasignar.Enabled = False
    
    With grdGrupo
        If .rowSel > 0 Then
            If .TextMatrix(.rowSel, colGrCodsector) <> "" Then
                '{ add -003- a.
                XfPasajeProduccion = .TextMatrix(.rowSel, colGrFe_Produccion)
                XfFechaFinSuspension = .TextMatrix(.rowSel, colGrFe_Suspension)
                'XnCodigoMotivo = .TextMatrix(.RowSel, colGrFe_MotivoSusp)      ' ESTO HAY QUE ARREGLARLO!!!!
                '}
                glGrupo = .TextMatrix(.rowSel, colGrCodsector)
                sEstado = .TextMatrix(.rowSel, colGrCodestado)
                cboGrupo.ListIndex = PosicionCombo(cboGrupo, .TextMatrix(.rowSel, colGrCodsector), True)
                If Val(.TextMatrix(.rowSel, colGrhst_nrointerno_sol)) > 0 Then
                    Call Puntero(True)          ' add -021- a.
                    obsSolic.text = sp_GetHistorialMemo(.TextMatrix(.rowSel, colGrhst_nrointerno_sol))
                    Call Puntero(False)         ' add -021- a.
                Else
                    obsSolic.text = ""
                End If
                
                lblResponsableNombre = IIf(.TextMatrix(.rowSel, colGr_NomResponsable) = "*** SIN RESPONSABLE A CARGO ***", "-", .TextMatrix(.rowSel, colGr_NomResponsable))
                lblPerfil = .TextMatrix(.rowSel, colGr_Perfil)
                cmdEliminar.Enabled = False
                Select Case glUsrPerfilActual
                    Case "SADM"
                        Call setHabilCtrl(cmdEliminar, NORMAL)
                        Call setHabilCtrl(cmdReasignar, NORMAL)
                    Case "ADMI", "CSEC"
                        If InStr(1, "OPINIO|EVALUA|", sEstado, vbTextCompare) > 0 Then
                            Call setHabilCtrl(cmdEliminar, NORMAL)
                            Call setHabilCtrl(cmdReasignar, NORMAL)
                        End If
                    Case Else
                        If sp_GetAccionPerfil("GDEL000", glUsrPerfilActual, sEstado, Null, Null) Then
                            Call setHabilCtrl(cmdEliminar, NORMAL)
                        End If
                End Select
            End If
            
            'If InStr(1, glHomologacionGrupo & "|" & glTecnoGrupo & "|" & glSegInfGrupo & "|", glGrupo, vbTextCompare) = 0 Then
            
            ' Esto deber�a ser reemplazado: el SP deber�a confirmar que existe UNA l�nea es AccionPerfil con el estado en el que
            ' se permite al Ref. BPE o Ref. Sistemas cambiar el estado del grupo.
            If xPerfNivel = "BBVA" Or _
                xPerfNivel = "DIRE" And glDireccion = xPerfArea Or _
                xPerfNivel = "GERE" And glGerencia = xPerfArea Or _
                xPerfNivel = "GRUP" And glGrupo = xPerfArea Or _
                xPerfNivel = "SECT" And glSector = xPerfArea Then
                'If sp_GetAccionPerfil("GCHGEST", glUsrPerfilActual, sEstado, Null, Null) Then
                If sp_GetAccionPerfilEstados(1, "GCHGEST", glUsrPerfilActual, sEstado, Null, Null) Then
                    Select Case glUsrPerfilActual
                        Case "BPAR", "GBPE"
                            If EsGrupoEspecial(glGrupo) Then
                                Call setHabilCtrl(cmdCambiarEstado, DISABLE)
                                Call setHabilCtrl(cmdInfoAdic, DISABLE)
                                Call setHabilCtrl(cmdRecursos, DISABLE)
                            Else
                                Call setHabilCtrl(cmdCambiarEstado, NORMAL)
                                Call setHabilCtrl(cmdInfoAdic, NORMAL)
                                Call setHabilCtrl(cmdRecursos, NORMAL)
                            End If
                        Case Else
                            Call setHabilCtrl(cmdCambiarEstado, NORMAL)
                            Call setHabilCtrl(cmdInfoAdic, NORMAL)
                            Call setHabilCtrl(cmdRecursos, NORMAL)
                    End Select
                End If
'                '{ add -004- a.
'                ' Se agrega la excepci�n si el grupo seleccionado es el grupo Homologaci�n...
'                Dim cGrupoHomologacion As String
'                ' Obtengo el grupo homologador
'                If sp_GetGrupoHomologacion Then
'                    cGrupoHomologacion = ClearNull(aplRST.Fields!cod_grupo)
'                Else
'                    cGrupoHomologacion = ""
'                End If
'                ' Si no es el grupo homologador, evaluo el estado de la petici�n antes de habilitar los botones.
'                ' Si era el grupo homologador, le dejo las opciones que tenia parametrizadas para modificar los estados.
'                'If cGrupoHomologacion <> CodigoCombo(cboGrupo, True) Then
                If glHomologacionGrupo <> CodigoCombo(cboGrupo, True) Then
                    If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD", ClearNull(EstadoPeticion), vbTextCompare) > 0 Then
                        If glUsrPerfilActual <> "SADM" Then
                            Call setHabilCtrl(cmdCambiarEstado, DISABLE)
                            Call setHabilCtrl(cmdInfoAdic, DISABLE)
                            Call setHabilCtrl(cmdRecursos, DISABLE)
                        Else
                            Call setHabilCtrl(cmdCambiarEstado, NORMAL)
                            Call setHabilCtrl(cmdInfoAdic, NORMAL)
                            Call setHabilCtrl(cmdRecursos, NORMAL)
                        End If
                    End If
                End If
                '}
            End If
        End If
'        '{ add -019- a.
'        'If sp_GetRecursoAcargoArea(glDireccion, glGerencia, glSector, glGrupo) Then
'        If sp_GetRecursoAcargoArea(glDireccion, glGerencia, glSector, .TextMatrix(.RowSel, colGrCodsector)) Then
'            lblResponsableNombre = ClearNull(aplRST.Fields!nom_recurso)
'        Else
'            lblResponsableNombre = "*** SIN RESPONSABLE A CARGO ***"
'        End If
'        '}
        .Enabled = True
    End With
    grdGrupo.Enabled = True
    Call Status("Listo.")
End Sub

Private Sub grdGrupo_Click()
    Call MostrarSeleccion
End Sub

Function PedirEstado() As String
    PedirEstado = ""
    If EstadoSector = "ACONFI" Then
        PedirEstado = "ACONFI"
        Exit Function
    End If
    
    If InStr("CONFEC|DEVSOL|REFERE|DEVREF|", EstadoPeticion) > 0 Then
        PedirEstado = "ACONFI"
        Exit Function
    End If
    Load auxEstadoGrupo
    With auxEstadoGrupo
        .iniEstado = EstadoSector
        .cPetGerencia = glGerencia
        .setOption
        .Show 1
        If .bAceptar = True Then
           PedirEstado = .pubEstado
        End If
    End With
    Unload auxEstadoGrupo
End Function

Private Sub VerAgregar()
    Call setHabilCtrl(cmdAgregar, DISABLE)
    Call setHabilCtrl(cmdEliminar, DISABLE)
    If sp_GetAccionPerfil("GNEW000", glUsrPerfilActual, EstadoSector, Null, Null) Then
        Call setHabilCtrl(cmdAgregar, NORMAL)
    End If
    If sp_GetAccionPerfil("GDEL000", glUsrPerfilActual, EstadoSector, Null, Null) Then
        Call setHabilCtrl(cmdEliminar, NORMAL)
    End If

    '{ add -003- a. Agregado el 17.06.2009: una vez que una petici�n recibe el conforme de pruebas del usuario
    '               de caracter final (TEST) no pueden agregarse nuevos grupos ejecutores a la petici�n.
    If InStr(1, "ADMI|SADM|", glUsrPerfilActual, vbTextCompare) = 0 Then
        If sp_GetPeticionConf(glNumeroPeticion, "TEST", Null) Then
            Call setHabilCtrl(cmdAgregar, DISABLE)
        End If
    End If
    '}
End Sub

'{ add -013- a.
Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdGrupo)
End Sub
'}

'{ add -014- a.
Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 27 Then       ' Tecla: ESCAPE
        cmdCerrar_Click
    End If
End Sub
'}

'{ add -002- a.
Private Sub grdGrupo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdGrupo_Click
    End If
End Sub
'}

'GMT01 - INI
Function validacionTecnicaCompleta() As Boolean
    Dim bSeCompletoValidacionArquitecura As Boolean
    Dim bSeCompletoValidacionSegInf As Boolean
    Dim bSeCompletoValidacionData As Boolean

    bSeCompletoValidacionArquitecura = False
    bSeCompletoValidacionSegInf = False
    bSeCompletoValidacionData = False
    
    If sp_GetPeticionValidacion(glNumeroPeticion, 1, Null) Then
        bSeCompletoValidacionArquitecura = True
        
        Do While Not aplRST.EOF
            If Not (ClearNull(aplRST.Fields!valitemvalor) = "S" Or ClearNull(aplRST.Fields!valitemvalor) = "N") Then
               bSeCompletoValidacionArquitecura = False
            End If
            
            aplRST.MoveNext
            DoEvents
            Loop
    End If
        
    If sp_GetPeticionValidacion(glNumeroPeticion, 2, Null) Then
        bSeCompletoValidacionSegInf = True
        
        Do While Not aplRST.EOF
            If Not (ClearNull(aplRST.Fields!valitemvalor) = "S" Or ClearNull(aplRST.Fields!valitemvalor) = "N") Then
               bSeCompletoValidacionSegInf = False
            End If
           
            aplRST.MoveNext
            DoEvents
            Loop
    End If
        
    If sp_GetPeticionValidacion(glNumeroPeticion, 3, Null) Then
        bSeCompletoValidacionData = True
        Do While Not aplRST.EOF
            If Not (ClearNull(aplRST.Fields!valitemvalor) = "S" Or ClearNull(aplRST.Fields!valitemvalor) = "N") Then
               bSeCompletoValidacionData = False
            End If
            
            aplRST.MoveNext
            DoEvents
            Loop
    End If

    If bSeCompletoValidacionArquitecura And bSeCompletoValidacionSegInf And bSeCompletoValidacionData Then
       validacionTecnicaCompleta = True
    Else
       validacionTecnicaCompleta = False
    End If

End Function

'GMT01 - FIN
