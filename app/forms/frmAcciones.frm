VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAcciones 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Acciones"
   ClientHeight    =   6840
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9675
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6840
   ScaleWidth      =   9675
   ShowInTaskbar   =   0   'False
   Begin VB.Frame pnlBotones 
      Height          =   6735
      Left            =   8340
      TabIndex        =   8
      Top             =   45
      Width           =   1275
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "AGREGAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Crear una nueva Direcci�n"
         Top             =   4050
         Width           =   1170
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "MODIFICAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Modificar la direcci�n selecionada"
         Top             =   4560
         Width           =   1170
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "ELIMINAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Eliminar la Direcci�n seleccionada"
         Top             =   5070
         Width           =   1170
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "CERRAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   6090
         Width           =   1170
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "CONFIRMAR"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Confirma la operaci�n"
         Top             =   5580
         Width           =   1170
      End
   End
   Begin VB.Frame fraDatos 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2940
      Left            =   120
      TabIndex        =   0
      Top             =   3840
      Width           =   8205
      Begin VB.ComboBox cboMultiplesEstados 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6120
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   2400
         Width           =   1065
      End
      Begin VB.ComboBox cboPorEstado 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3360
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   2400
         Width           =   1065
      End
      Begin VB.ComboBox cboAgrupador 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1680
         Width           =   5025
      End
      Begin VB.ComboBox cboTipo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   1320
         Width           =   1545
      End
      Begin VB.ComboBox cboHabIGM 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   960
         Width           =   1065
      End
      Begin AT_MaskText.MaskText txtCodAccion 
         Height          =   315
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin AT_MaskText.MaskText txtNomAccion 
         Height          =   315
         Left            =   1560
         TabIndex        =   3
         Top             =   600
         Width           =   5805
         _ExtentX        =   10239
         _ExtentY        =   556
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
      End
      Begin AT_MaskText.MaskText txtLeyenda 
         Height          =   315
         Left            =   1560
         TabIndex        =   17
         Top             =   2040
         Width           =   5805
         _ExtentX        =   10239
         _ExtentY        =   556
         MaxLength       =   100
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   100
      End
      Begin AT_MaskText.MaskText txtOrden 
         Height          =   315
         Left            =   1560
         TabIndex        =   18
         Top             =   2400
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   556
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   8
         UpperCase       =   -1  'True
      End
      Begin VB.Label lblControl 
         AutoSize        =   -1  'True
         Caption         =   "M�ltiples estados"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   5
         Left            =   4560
         TabIndex        =   26
         Top             =   2467
         Width           =   1305
      End
      Begin VB.Label lblControl 
         AutoSize        =   -1  'True
         Caption         =   "Por estado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   4
         Left            =   2400
         TabIndex        =   25
         Top             =   2467
         Width           =   810
      End
      Begin VB.Label lblControl 
         AutoSize        =   -1  'True
         Caption         =   "Orden"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   3
         Left            =   120
         TabIndex        =   24
         Top             =   2467
         Width           =   450
      End
      Begin VB.Label lblControl 
         AutoSize        =   -1  'True
         Caption         =   "Leyenda"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   2
         Left            =   120
         TabIndex        =   23
         Top             =   2107
         Width           =   630
      End
      Begin VB.Label lblControl 
         AutoSize        =   -1  'True
         Caption         =   "Agrupador"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   1
         Left            =   120
         TabIndex        =   22
         Top             =   1755
         Width           =   780
      End
      Begin VB.Label lblControl 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de acci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   21
         Top             =   1395
         Width           =   1050
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Hab. IGM"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   7
         Top             =   1035
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   6
         Top             =   307
         Width           =   495
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   5
         Top             =   667
         Width           =   840
      End
      Begin VB.Label lblSeleccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   120
         TabIndex        =   4
         Top             =   30
         Visible         =   0   'False
         Width           =   45
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3690
      Left            =   120
      TabIndex        =   14
      Top             =   150
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   6509
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAcciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const col_ACC_CODIGO = 0
Const col_ACC_NOMBRE = 1
Const col_ACC_HABIGM = 2
Const col_ACC_TIPO = 3
Const col_ACC_AGRUP = 4
Const col_ACC_LEYENDA = 5
Const col_ACC_ORDEN = 6
Const col_ACC_PORESTADO = 7
Const col_ACC_ESTADOMULT = 8
Const col_ACC_CANTCOLS = 9

Dim sOpcionSeleccionada As String

Private Sub Form_Load()
    Me.Left = 0
    Me.Top = 0
    Call InicializarPantalla
    Call IniciarScroll(grdDatos)
End Sub

Private Sub InicializarPantalla()
    Me.Tag = ""
    sOpcionSeleccionada = ""
    With cboHabIGM
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = -1
    End With
    
    With cboTipo
        .Clear
        .AddItem "Acci�n" & ESPACIOS & "||ACC"
        .AddItem "Evento" & ESPACIOS & "||EVT"
        .ListIndex = -1
    End With
    
    With cboAgrupador
        .Clear
        If sp_GetAccionesGrupo(Null, Null, "S") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!accionGrupoNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!accionGrupoId)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = -1
        End If
    End With
    
    With cboPorEstado
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 1  ' Por defecto No
    End With
    
    With cboMultiplesEstados
        .Clear
        .AddItem "--" & ESPACIOS & "||-"
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 0  ' Por defecto S/N
    End With
    
    'Show
    Call HabilitarBotones(0)
End Sub

Private Sub CargarGrid()
    With grdDatos
        .Clear
        .cols = col_ACC_CANTCOLS
        .Rows = 1
        .RowHeightMin = 250
        .TextMatrix(0, col_ACC_CODIGO) = "C�digo": .ColWidth(col_ACC_CODIGO) = 1200: .ColAlignment(col_ACC_CODIGO) = 0
        .TextMatrix(0, col_ACC_NOMBRE) = "Nombre": .ColWidth(col_ACC_NOMBRE) = 4000: .ColAlignment(col_ACC_NOMBRE) = 0
        .TextMatrix(0, col_ACC_HABIGM) = "IGM": .ColWidth(col_ACC_HABIGM) = 800: .ColAlignment(col_ACC_HABIGM) = 0
        .TextMatrix(0, col_ACC_TIPO) = "Tipo": .ColWidth(col_ACC_TIPO) = 800: .ColAlignment(col_ACC_TIPO) = 0
        .TextMatrix(0, col_ACC_AGRUP) = "Agrup.": .ColWidth(col_ACC_AGRUP) = 800: .ColAlignment(col_ACC_AGRUP) = 0
        .TextMatrix(0, col_ACC_LEYENDA) = "Leyenda": .ColWidth(col_ACC_LEYENDA) = 2000: .ColAlignment(col_ACC_LEYENDA) = 0
        .TextMatrix(0, col_ACC_ORDEN) = "Orden": .ColWidth(col_ACC_ORDEN) = 800: .ColAlignment(col_ACC_ORDEN) = 0
        .TextMatrix(0, col_ACC_PORESTADO) = "Por estado": .ColWidth(col_ACC_PORESTADO) = 800: .ColAlignment(col_ACC_PORESTADO) = 0
        .TextMatrix(0, col_ACC_ESTADOMULT) = "M�ltipl.": .ColWidth(col_ACC_ESTADOMULT) = 800: .ColAlignment(col_ACC_ESTADOMULT) = 0
        'Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
    'If Not sp_GetPeriodo(Null) Then Exit Sub
    If Not sp_GetAccion(Null) Then Exit Sub
    Do While Not aplRST.EOF
        With grdDatos
            .Rows = .Rows + 1
            .TextMatrix(.Rows - 1, col_ACC_CODIGO) = ClearNull(aplRST.Fields!cod_accion)
            .TextMatrix(.Rows - 1, col_ACC_NOMBRE) = ClearNull(aplRST.Fields!nom_accion)
            .TextMatrix(.Rows - 1, col_ACC_HABIGM) = ClearNull(aplRST.Fields!IGM_hab)
            .TextMatrix(.Rows - 1, col_ACC_TIPO) = ClearNull(aplRST.Fields!tipo_accion)
            .TextMatrix(.Rows - 1, col_ACC_AGRUP) = ClearNull(aplRST.Fields!agrupador)
            .TextMatrix(.Rows - 1, col_ACC_LEYENDA) = ClearNull(aplRST.Fields!leyenda)
            .TextMatrix(.Rows - 1, col_ACC_ORDEN) = ClearNull(aplRST.Fields!orden)
            .TextMatrix(.Rows - 1, col_ACC_PORESTADO) = ClearNull(aplRST.Fields!porEstado)
            .TextMatrix(.Rows - 1, col_ACC_ESTADOMULT) = ClearNull(aplRST.Fields!multiplesEstados)
        End With
        aplRST.MoveNext
        DoEvents
    Loop
    Call MostrarSeleccion
End Sub

Private Sub MostrarSeleccion()
    With grdDatos
        If .MouseRow = 0 And .Rows > 1 Then
           .RowSel = 1
           .RowSel = 1
           .Col = .MouseCol
           .Sort = flexSortStringNoCaseAscending
        End If
        If .RowSel > 0 Then
            '.HighLight = flexHighlightAlways
            If .TextMatrix(.RowSel, col_ACC_CODIGO) <> "" Then
                txtCodAccion = ClearNull(.TextMatrix(.RowSel, col_ACC_CODIGO))
                txtNomAccion = ClearNull(.TextMatrix(.RowSel, col_ACC_NOMBRE))
                cboHabIGM.ListIndex = PosicionCombo(cboHabIGM, ClearNull(.TextMatrix(.RowSel, col_ACC_HABIGM)), True)
                cboTipo.ListIndex = PosicionCombo(cboTipo, ClearNull(.TextMatrix(.RowSel, col_ACC_TIPO)), True)
                cboAgrupador.ListIndex = PosicionCombo(cboAgrupador, ClearNull(.TextMatrix(.RowSel, col_ACC_AGRUP)), True)
                txtLeyenda = ClearNull(.TextMatrix(.RowSel, col_ACC_LEYENDA))
                txtOrden = ClearNull(.TextMatrix(.RowSel, col_ACC_ORDEN))
                cboPorEstado.ListIndex = PosicionCombo(cboPorEstado, ClearNull(.TextMatrix(.RowSel, col_ACC_PORESTADO)), True)
                cboMultiplesEstados.ListIndex = PosicionCombo(cboMultiplesEstados, ClearNull(.TextMatrix(.RowSel, col_ACC_ESTADOMULT)), True)
            End If
        End If
    End With
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertAccion(txtCodAccion, txtNomAccion, CodigoCombo(cboHabIGM, True), CodigoCombo(cboTipo, True), CodigoCombo(cboAgrupador, True), txtLeyenda, txtOrden, CodigoCombo(cboPorEstado, True), CodigoCombo(cboMultiplesEstados, True)) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                If sp_UpdateAccion(txtCodAccion, txtNomAccion, CodigoCombo(cboHabIGM, True), CodigoCombo(cboTipo, True), CodigoCombo(cboAgrupador, True), txtLeyenda, txtOrden, CodigoCombo(cboPorEstado, True), CodigoCombo(cboMultiplesEstados, True)) Then
                    With grdDatos
                        .TextMatrix(.RowSel, col_ACC_NOMBRE) = ClearNull(txtNomAccion)
                        .TextMatrix(.RowSel, col_ACC_HABIGM) = CodigoCombo(cboHabIGM, True)
                        .TextMatrix(.RowSel, col_ACC_TIPO) = CodigoCombo(cboTipo, True)
                        .TextMatrix(.RowSel, col_ACC_AGRUP) = CodigoCombo(cboAgrupador, True)
                        .TextMatrix(.RowSel, col_ACC_LEYENDA) = ClearNull(txtLeyenda)
                        .TextMatrix(.RowSel, col_ACC_ORDEN) = ClearNull(txtOrden)
                        .TextMatrix(.RowSel, col_ACC_PORESTADO) = CodigoCombo(cboPorEstado, True)
                        .TextMatrix(.RowSel, col_ACC_ESTADOMULT) = CodigoCombo(cboMultiplesEstados, True)
                    End With
                    Call HabilitarBotones(0, False)
                End If
            End If
        Case "E"
            If sp_DeleteAccion(txtCodAccion) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaGrid As Variant)
    Select Case nOpcion
        Case 0
            lblSeleccion = "": lblSeleccion.visible = False
            fraDatos.Enabled = False
            grdDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "CERRAR"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            sOpcionSeleccionada = ""
            If IsMissing(vCargaGrid) Then
                Call CargarGrid
            Else
                Call MostrarSeleccion
            End If
        Case 1
            grdDatos.Enabled = False
            fraDatos.Enabled = True
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "CANCELAR"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    lblSeleccion = " AGREGAR ": lblSeleccion.visible = True
                    txtCodAccion = "": txtNomAccion = ""
                    cboHabIGM.ListIndex = 0
                    txtCodAccion.Enabled = True
                    txtCodAccion.SetFocus
                    
                    cboTipo.ListIndex = -1
                    txtLeyenda = ""
                    txtOrden = ""
                    cboAgrupador.ListIndex = -1
                    cboPorEstado.ListIndex = 1
                    cboMultiplesEstados.ListIndex = 0
                Case "M"
                    lblSeleccion = " MODIFICAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = True
                    txtCodAccion.Enabled = False
                    txtNomAccion.SetFocus
                    
                    Call setHabilCtrl(cboTipo, "NOR")
                    'cboTipo.ListIndex = PosicionCombo()
                    'txtLeyenda = ""
                    'txtOrden = ""
                    Call setHabilCtrl(txtLeyenda, "NOR")
                    Call setHabilCtrl(txtOrden, "NOR")
                    Call setHabilCtrl(cboAgrupador, "NOR")
                    Call setHabilCtrl(cboPorEstado, "NOR")
                    Call setHabilCtrl(cboMultiplesEstados, "NOR")
                    
                    'cboAgrupador.ListIndex = -1
                    'cboPorEstado.ListIndex = 1
                    'cboMultiplesEstados.ListIndex = 0
                Case "E"
                    lblSeleccion = " ELIMINAR ": lblSeleccion.visible = True
                    fraDatos.Enabled = False
            End Select
    End Select
    Call LockProceso(False)
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True

    If Trim(txtCodAccion.text) = "" Then
        MsgBox ("Debe completar C�digo")
        CamposObligatorios = False
        Exit Function
    End If
    If Trim(txtNomAccion.text) = "" Then
        MsgBox ("Debe completar Descripci�n")
        CamposObligatorios = False
        Exit Function
    End If
    If cboHabIGM.ListIndex = -1 Then
        MsgBox ("Debe seleccionar habilitaci�n")
        CamposObligatorios = False
        Exit Function
    End If
End Function

Private Sub grdDatos_Click()
    Call MostrarSeleccion
End Sub

Private Sub grdDatos_RowColChange()
    'Call MostrarSeleccion
End Sub

Private Sub grdDatos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        grdDatos_Click
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub

