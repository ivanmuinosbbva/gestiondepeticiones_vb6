VERSION 5.00
Begin VB.Form auxSelBpar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccione Referente de RGyP/Sistema"
   ClientHeight    =   2160
   ClientLeft      =   1140
   ClientTop       =   330
   ClientWidth     =   5535
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2160
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   " Perfil "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   60
      TabIndex        =   4
      Top             =   60
      Width           =   5415
      Begin VB.OptionButton optPerfil 
         Caption         =   "Referente de Sistemas"
         Height          =   255
         Index           =   1
         Left            =   780
         TabIndex        =   6
         Top             =   540
         Width           =   2055
      End
      Begin VB.OptionButton optPerfil 
         Caption         =   "Referente de RGyP"
         Height          =   255
         Index           =   0
         Left            =   780
         TabIndex        =   5
         Top             =   300
         Value           =   -1  'True
         Width           =   2415
      End
   End
   Begin VB.ComboBox cboRecurso 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   60
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1080
      Width           =   5415
   End
   Begin VB.Frame fraOpciones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   60
      TabIndex        =   0
      Top             =   1500
      Width           =   5415
      Begin VB.CommandButton cmdBD 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   4440
         TabIndex        =   2
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   3480
         TabIndex        =   1
         Top             =   180
         Width           =   885
      End
   End
End
Attribute VB_Name = "auxSelBpar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. - FJS 26.07.2007 - Se agrega el llamado a una rutina que deja el nombre del objeto form en pantalla (en el control Status Bar para poder facilitar el reporte de errores por parte del usuario).

Option Explicit

Dim m_CodRecurso As String
Dim m_NomRecurso As String
Dim m_Perfil As String

Public Property Get NomRecurso() As String
    NomRecurso = m_NomRecurso
End Property

Public Property Get CodRecurso() As String
    CodRecurso = m_CodRecurso
End Property

Public Property Get CodPerfil() As String
    CodPerfil = m_Perfil
End Property

Public Property Let CodRecurso(ByVal NewCodRecurso As String)
    m_CodRecurso = NewCodRecurso
End Property

Private Sub cboRecurso_Click()
    m_CodRecurso = CodigoCombo(cboRecurso, True)
    m_NomRecurso = TextoCombo(cboRecurso)
End Sub

Private Sub cmdOK_Click()
    m_CodRecurso = CodigoCombo(cboRecurso, True)
    Unload Me
End Sub

Private Sub cmdBD_Click()
    m_CodRecurso = ""
    Unload Me
End Sub

Private Sub Form_Load()
    m_Perfil = "GBPE"
    Call ActualizarRecursos
End Sub

Private Sub ActualizarRecursos()
    With cboRecurso
        .Clear
        If sp_GetRecursoPerfil(Null, m_Perfil) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST!nom_recurso) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST!cod_recurso)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

Private Sub optPerfil_Click(Index As Integer)
    Select Case Index
        Case 0: m_Perfil = "GBPE"
        Case 1: m_Perfil = "BPAR"
    End Select
    Call ActualizarRecursos
End Sub
