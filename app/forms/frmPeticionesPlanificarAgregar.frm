VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPeticionesPlanificarAgregar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccione las peticiones a incluir en planificar en el per�odo actual"
   ClientHeight    =   7725
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13905
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7725
   ScaleWidth      =   13905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Height          =   1215
      Left            =   60
      TabIndex        =   6
      Top             =   0
      Width           =   12735
      Begin VB.Label lblCriterios 
         AutoSize        =   -1  'True
         Caption         =   "lblCriterios"
         Height          =   735
         Index           =   1
         Left            =   120
         TabIndex        =   8
         Top             =   420
         Width           =   12510
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblCriterios 
         AutoSize        =   -1  'True
         Caption         =   "Las peticiones listadas a continuaci�n cumplen con los siguientes criterios de selecci�n:"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   180
         Width           =   6195
      End
   End
   Begin VB.ComboBox cmbPeriodo 
      Height          =   315
      Left            =   2520
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   7320
      Width           =   4815
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7695
      Left            =   12840
      TabIndex        =   1
      Top             =   0
      Width           =   1020
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   60
         TabIndex        =   5
         Top             =   7260
         Width           =   885
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         Height          =   375
         Left            =   60
         TabIndex        =   2
         Top             =   120
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   5970
      Left            =   120
      TabIndex        =   0
      Top             =   1260
      Width           =   12675
      _ExtentX        =   22357
      _ExtentY        =   10530
      _Version        =   393216
      Cols            =   30
      FixedCols       =   0
      ForeColorSel    =   16777215
      GridColor       =   14737632
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblEncabezadoPrincipal 
      AutoSize        =   -1  'True
      Caption         =   "Agregar en el siguiente per�odo:"
      ForeColor       =   &H00FF0000&
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   7380
      Width           =   2310
   End
End
Attribute VB_Name = "frmPeticionesPlanificarAgregar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bHayCambios As Boolean

Private Const col_SELECTION = 0
Private Const col_ULTPERIODO = 1
Private Const col_NUMEROASIGNADO = 2
Private Const col_TIPO = 3
Private Const col_CLASE = 4
Private Const col_FEPEDIDO = 5
Private Const col_TITULO = 6
Private Const col_PUNTAJE = 7
Private Const COL_ESTADO = 8
Private Const col_REFBPENOMBRE = 9
Private Const col_REFSISNOMBRE = 10
Private Const col_NROINTERNO = 11
Private Const col_REFSISCODIGO = 12
Private Const col_REFBPECODIGO = 13
Private Const col_COD_ESTADO = 14
Private Const col_TOTCOLS = 15

Dim lUltimaColumnaOrdenada As Long
Dim bSorting As Boolean

Private Sub Form_Load()
    bHayCambios = False
    Me.Caption = "Seleccione peticiones para incorporar a la planificaci�n"
    Call InicializarControles
    Call InicializarGrilla
    Call CargarGrilla
    Call IniciarScroll(grdDatos)
End Sub

Private Sub InicializarControles()
    lblCriterios(1) = _
        "- Son peticiones de clase Nuevo desarrollo o Mantenimiento evolutivo, no regulatorias, " & _
        "activas, que tienen referente de sistemas designado, no est�n asignadas a periodos " & _
        "anteriores y pueden tener palancas de valoraci�n o estar asociadas a un proyecto."

    With cmbPeriodo
        .Clear
        If sp_GetPeriodo(Null, "S") Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!per_abrev) & ": " & ClearNull(aplRST.Fields!per_nombre) & ESPACIOS & ESPACIOS & "||" & ClearNull(aplRST.Fields!per_nrointerno)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = PosicionCombo(cmbPeriodo, CodigoCombo(frmPeticionesPlanificar.cboFiltroPeriodo, True), True)
        End If
    End With
    Call setHabilCtrl(cmbPeriodo, DISABLE)
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .Clear
        DoEvents
        .HighLight = flexHighlightAlways
        .Rows = 1
        .cols = col_TOTCOLS
        ' T�tulos de columna
        .TextMatrix(0, col_SELECTION) = "": .ColWidth(col_SELECTION) = 200: .ColAlignment(col_SELECTION) = flexAlignLeftCenter
        .TextMatrix(0, col_ULTPERIODO) = "Ult.Per.": .ColWidth(col_ULTPERIODO) = 0: .ColAlignment(col_ULTPERIODO) = flexAlignLeftCenter
        .TextMatrix(0, col_NUMEROASIGNADO) = "N�": .ColWidth(col_NUMEROASIGNADO) = 900: .ColAlignment(col_NUMEROASIGNADO) = flexAlignRightCenter
        .TextMatrix(0, col_TIPO) = "Tipo": .ColWidth(col_TIPO) = 600
        .TextMatrix(0, col_CLASE) = "Clase": .ColWidth(col_CLASE) = 600
        .TextMatrix(0, col_FEPEDIDO) = "F.Alta Pet.": .ColWidth(col_FEPEDIDO) = 1200: .ColAlignment(col_FEPEDIDO) = flexAlignLeftCenter
        .TextMatrix(0, col_TITULO) = "T�tulo": .ColWidth(col_TITULO) = 3500: .ColAlignment(col_TITULO) = flexAlignLeftCenter
        .TextMatrix(0, col_PUNTAJE) = "Ptos.": .ColWidth(col_PUNTAJE) = 1000: .ColAlignment(col_PUNTAJE) = flexAlignRightCenter
        .TextMatrix(0, COL_ESTADO) = "Estado Pet.": .ColWidth(COL_ESTADO) = 2000
        .TextMatrix(0, col_REFBPENOMBRE) = "Ref. de RGyP": .ColWidth(col_REFBPENOMBRE) = 2000
        .TextMatrix(0, col_REFSISNOMBRE) = "Ref. de sistemas": .ColWidth(col_REFSISNOMBRE) = 2000
        .TextMatrix(0, col_NROINTERNO) = "pet_nrointerno": .ColWidth(col_NROINTERNO) = 0
        .TextMatrix(0, col_REFSISCODIGO) = "cod_bpar": .ColWidth(col_REFSISCODIGO) = 0
        .TextMatrix(0, col_REFBPECODIGO) = "cod_bpe": .ColWidth(col_REFBPECODIGO) = 0
        .TextMatrix(0, col_COD_ESTADO) = "cod_estado": .ColWidth(col_COD_ESTADO) = 0
        .BackColorFixed = Me.BackColor
        .BackColorSel = RGB(58, 110, 165)
        .BackColorBkg = Me.BackColor
        .Font.name = "Tahoma"
        .Font.Size = 8
        .GridColor = &HE0E0E0
        .row = 0
        .FocusRect = flexFocusNone
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub CargarGrilla()
    With grdDatos
        If sp_GetPetPlanificar_Agregar(glLOGIN_ID_REEMPLAZO, "NULL") Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, col_SELECTION) = ""
                '.TextMatrix(.Rows - 1, col_ULTPERIODO) = ClearNull(aplRST!per_abrev)
                .TextMatrix(.Rows - 1, col_NUMEROASIGNADO) = ClearNull(aplRST!pet_nroasignado)
                .TextMatrix(.Rows - 1, col_TIPO) = ClearNull(aplRST!cod_tipo_peticion)
                .TextMatrix(.Rows - 1, col_CLASE) = ClearNull(aplRST!cod_clase)
                .TextMatrix(.Rows - 1, col_FEPEDIDO) = ClearNull(aplRST!fe_pedido)
                .TextMatrix(.Rows - 1, col_TITULO) = ClearNull(aplRST!Titulo)
                .TextMatrix(.Rows - 1, COL_ESTADO) = ClearNull(aplRST!nom_estado)
                .TextMatrix(.Rows - 1, col_PUNTAJE) = Format(ClearNull(aplRST!puntuacion), "###,##0.000")
                .TextMatrix(.Rows - 1, col_REFSISNOMBRE) = ClearNull(aplRST!nom_bpar)
                .TextMatrix(.Rows - 1, col_REFBPENOMBRE) = ClearNull(aplRST!nom_BPE)
                .TextMatrix(.Rows - 1, col_NROINTERNO) = ClearNull(aplRST!pet_nrointerno)
                .TextMatrix(.Rows - 1, col_REFSISCODIGO) = ClearNull(aplRST!cod_bpar)
                .TextMatrix(.Rows - 1, col_REFBPECODIGO) = ClearNull(aplRST!cod_bpe)
                .TextMatrix(.Rows - 1, col_COD_ESTADO) = ClearNull(aplRST!cod_estado)
                If .Rows Mod 2 > 0 Then Call PintarLinea(grdDatos, prmGridFillRowColorLightGrey2)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
End Sub

'{ add -002- a.
Private Sub grdDatos_Click()
    bSorting = True
    Call OrdenarGrilla(grdDatos, lUltimaColumnaOrdenada)
    bSorting = False
End Sub
'}

Private Sub grdDatos_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        MenuGridFind grdDatos
    End If
End Sub

Private Sub grdDatos_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim i As Integer
    Dim nRow As Integer
    Dim nRwD As Integer, nRwH As Integer
    
    If grdDatos.row <= grdDatos.rowSel Then
        nRwD = grdDatos.row
        nRwH = grdDatos.rowSel
    Else
        nRwH = grdDatos.row
        nRwD = grdDatos.rowSel
    End If
    
    If nRwD <> nRwH Then
        For i = 1 To grdDatos.Rows - 1
           grdDatos.TextMatrix(i, col_SELECTION) = ""
        Next
        Do While nRwD <= nRwH
            grdDatos.TextMatrix(nRwD, col_SELECTION) = "�"
            nRwD = nRwD + 1
        Loop
        Exit Sub
    End If
    
    If Shift = vbCtrlMask Then
        With grdDatos
            If Button = vbLeftButton Then
                If .rowSel > 0 Then
                    If ClearNull(.TextMatrix(.rowSel, col_SELECTION)) = "" Then
                        .TextMatrix(.rowSel, col_SELECTION) = "�"
                    Else
                        .TextMatrix(.rowSel, col_SELECTION) = ""
                    End If
                End If
            End If
        End With
    End If
    
    If Shift = 0 Then
        With grdDatos
            If Button = vbLeftButton Then
                For i = 1 To .Rows - 1
                    .TextMatrix(i, col_SELECTION) = ""
                Next
                If .rowSel > 0 Then
                    .TextMatrix(.rowSel, col_SELECTION) = "�"
                End If
            End If
        End With
    End If
End Sub

Private Sub cmdAceptar_Click()
    Dim x As Long
    Dim sMensajeHistorial As String
    
    If MsgBox("�Confirma el agregado de las peticiones" & vbCrLf & "seleccionadas a la planificaci�n actual?", vbQuestion + vbOKCancel, "Planificaci�n") = vbOK Then
        Call Puntero(True)
        Call Status("Procesando...")
        With grdDatos
            For x = 1 To .Rows - 1
                If .TextMatrix(x, col_SELECTION) = "�" Then
                    bHayCambios = True
                    Call sp_InsertPeticionPlancab(CodigoCombo(cmbPeriodo, True), .TextMatrix(x, col_NROINTERNO), Null, Null)
                    
                    'sMensajeHistorial = "� Agregado autom�ticamente. � Asignaci�n inicial de priorizaci�n (BPE)"
                    '"La petici�n 50287 ha sido incluida autom�ticamente en el per�odo de planificaci�n 2017-03."
                    sMensajeHistorial = "La petici�n " & .TextMatrix(x, col_NUMEROASIGNADO) & " ha sido incluida manualmente en el per�odo de planificaci�n " & CodigoCombo(cmbPeriodo, False) & "."
                    Call sp_AddHistorial(.TextMatrix(x, col_NROINTERNO), "PCHGPRI1", .TextMatrix(x, col_COD_ESTADO), Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, sMensajeHistorial)
'                    ' Si la petici�n se encuentra en estado "En ejecuci�n",
'                    ' la prioridad debe ser por defecto 1; caso contrario, se
'                    ' agrega sin prioridad.
'                    If .TextMatrix(x, col_COD_ESTADO) = "EJECUC" Then
'                        Call sp_InsertPeticionPlancab(CodigoCombo(cmbPeriodo, True), .TextMatrix(x, col_NROINTERNO), "1", .TextMatrix(x, col_BPCODIGO))
'                        Call sp_InsertPeticionPlancab2(CodigoCombo(cmbPeriodo, True), .TextMatrix(x, col_NROINTERNO), "1")
'                    Else
'                        Call sp_InsertPeticionPlancab(CodigoCombo(cmbPeriodo, True), .TextMatrix(x, col_NROINTERNO), Null, .TextMatrix(x, col_BPCODIGO))
'                        Call sp_InsertPeticionPlancab2(CodigoCombo(cmbPeriodo, True), .TextMatrix(x, col_NROINTERNO), Null)
'                    End If
                End If
            Next x
        End With
        Call Puntero(False)
        Call Status("Listo.")
        Unload Me
    End If
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call DetenerScroll(grdDatos)
End Sub
