VERSION 5.00
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmPeticionesCambioEstado 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cambio estado Petici�n"
   ClientHeight    =   4545
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   8295
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4545
   ScaleWidth      =   8295
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVerActuantes 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6240
      Picture         =   "frmPeticionesCambioEstado.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   23
      TabStop         =   0   'False
      ToolTipText     =   "Visualizar los posibles recursos actuantes para el estado seleccionado"
      Top             =   840
      Width           =   375
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   60
      TabIndex        =   13
      Top             =   0
      Width           =   8190
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   19
         Top             =   240
         Width           =   1200
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6705
         TabIndex        =   18
         Top             =   480
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         Height          =   195
         Left            =   1300
         TabIndex        =   17
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         Height          =   195
         Left            =   1305
         TabIndex        =   16
         Top             =   240
         Width           =   6705
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         Height          =   195
         Left            =   7620
         TabIndex        =   15
         Top             =   480
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label Label6 
         Caption         =   "Estado actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   14
         Top             =   480
         Width           =   1200
      End
   End
   Begin VB.ComboBox cboAccion 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmPeticionesCambioEstado.frx":058A
      Left            =   1320
      List            =   "frmPeticionesCambioEstado.frx":058C
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   840
      Width           =   4875
   End
   Begin VB.Frame fraSector 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2775
      Left            =   60
      TabIndex        =   0
      Top             =   1170
      Width           =   8190
      Begin VB.ComboBox cboPrioridad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5385
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   262
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdAnexar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7440
         Picture         =   "frmPeticionesCambioEstado.frx":058E
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Seleccionar otra Peticion que no figura en la lista"
         Top             =   1260
         Visible         =   0   'False
         Width           =   390
      End
      Begin VB.TextBox obsResp 
         Height          =   720
         Left            =   105
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   1890
         Width           =   7725
      End
      Begin AT_MaskText.MaskText txtFechaFinReal 
         Height          =   315
         Left            =   2520
         TabIndex        =   6
         Top             =   600
         Visible         =   0   'False
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtFechaIniReal 
         Height          =   315
         Left            =   2520
         TabIndex        =   7
         Top             =   270
         Visible         =   0   'False
         Width           =   1470
         _ExtentX        =   2593
         _ExtentY        =   556
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   10
         UpperCase       =   -1  'True
         DataType        =   3
         SpecialFeatures =   -1  'True
      End
      Begin AT_MaskText.MaskText txtAnexar 
         Height          =   315
         Left            =   1980
         TabIndex        =   22
         Tag             =   " "
         Top             =   1260
         Visible         =   0   'False
         Width           =   5445
         _ExtentX        =   9604
         _ExtentY        =   556
         MaxLength       =   150
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   150
         AutoSelect      =   0   'False
      End
      Begin VB.Label lblXpriori 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4500
         TabIndex        =   25
         Top             =   315
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.Label lblAnexar 
         AutoSize        =   -1  'True
         Caption         =   "Anexar a la petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   21
         Top             =   1320
         Visible         =   0   'False
         Width           =   1680
      End
      Begin VB.Label lblFechaIniReal 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Inicio Real"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   150
         TabIndex        =   5
         Top             =   330
         Visible         =   0   'False
         Width           =   1680
      End
      Begin VB.Label lblFechaFinReal 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Terminaci�n Real"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   4
         Top             =   645
         Visible         =   0   'False
         Width           =   2265
      End
      Begin VB.Label lblResp 
         Caption         =   "Observaciones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   3
         Top             =   1620
         Width           =   1275
      End
      Begin VB.Label lblAccion 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   5910
         TabIndex        =   1
         Top             =   150
         Width           =   75
      End
   End
   Begin VB.Frame pnlBtnControl 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   60
      TabIndex        =   8
      Top             =   3900
      Width           =   8190
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   375
         Left            =   6300
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   7230
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Cambiar a"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   360
      TabIndex        =   12
      Top             =   900
      Width           =   855
   End
End
Attribute VB_Name = "frmPeticionesCambioEstado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'GMT01 - PET 70881 - Se saca RGyp

Option Explicit

Dim sOpcionSeleccionada As String
Dim EstadoNuevo As String
Dim EstadoSolicitud As String
Dim xSec As String, xGer As String, xDir As String
Dim EstadoPeticion As String
Dim hst_nrointerno_sol, hst_nrointerno_rsp
Dim auxNroAsig As String
Dim sImportancia As String
Dim xImportancia As String
Dim xUsrPerfilActual As String, xUsrLoginActual As String
Dim sPetClass As String
Dim cReferenteBPE As String
Dim cReferenteSIS As String
Dim flgBPE As Boolean
Dim sGestion As String
Dim Hab_RGyP As Boolean 'GMT01

Private Sub Form_Load()
    Call InicializarPantalla
    Call InicializarCombos
End Sub

Sub InicializarPantalla()
    Me.Tag = ""
    
    Call setHabilCtrl(cmdConfirmar, "DIS")
    glForzarEvaluar = False
    flgBPE = False
    cReferenteBPE = ""
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNroAsig = "S/N"
            Else
                auxNroAsig = ClearNull(aplRST!pet_nroasignado)
            End If
            xDir = ClearNull(aplRST!cod_direccion)
            xGer = ClearNull(aplRST!cod_gerencia)
            xSec = ClearNull(aplRST!cod_sector)
            sImportancia = ClearNull(aplRST!importancia_cod)
            lblTitulo = auxNroAsig & " - " & ClearNull(aplRST!Titulo)
            EstadoPeticion = ClearNull(aplRST!cod_estado)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
            txtFechaFinReal.text = IIf(Not IsNull(aplRST!fe_fin_real), Format(aplRST!fe_fin_real, "dd/mm/yyyy"), "")
            txtFechaIniReal.text = IIf(Not IsNull(aplRST!fe_ini_real), Format(aplRST!fe_ini_real, "dd/mm/yyyy"), "")
            sPetClass = ClearNull(aplRST!cod_clase)
            sGestion = ClearNull(aplRST!pet_driver)
            cReferenteBPE = ClearNull(aplRST!cod_bpe)
            cReferenteSIS = ClearNull(aplRST!cod_bpar)
        End If
    End If
    If ClearNull(cReferenteBPE) <> "" Then flgBPE = True
    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = True
    lblAccion = ""
    Call setHabilCtrl(txtFechaIniReal, "DIS")
    Call setHabilCtrl(txtFechaFinReal, "DIS")
    Call setHabilCtrl(obsResp, "DIS")
    Call setHabilCtrl(txtAnexar, "DIS")
End Sub

Sub InicializarCombos()
    Dim xAccion As String
    
    sOpcionSeleccionada = ""
    If sp_GetAccionPerfilEstados(1, "PCHGEST", glUsrPerfilActual, EstadoPeticion, Null, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!cod_estnew) <> ClearNull(aplRST!cod_estado) Then
                cboAccion.AddItem aplRST!nom_estnew & ESPACIOS & "||" & ClearNull(aplRST!cod_estnew)
                ' Esto es para los casos en que el sujeto tiene varios perfiles consecutivos en la rama ascendente
                If glUsrPerfilActual = "SOLI" And ClearNull(aplRST!cod_estnew) = "REFERE" Then
                    If InPerfil("REFE") And perfilConcentrico("REFE", xDir, xGer, xSec) Then
                        cboAccion.AddItem "A Autorizante" & ESPACIOS & "||" & "AUTORI"
                        If sGestion = "BPE" Then
                            If Hab_RGyP Then 'GMT01
                                If InPerfil("AUTO") And perfilConcentrico("AUTO", xDir, xGer, xSec) Then
                                    cboAccion.AddItem "Al referente de RGyP" & ESPACIOS & "||" & "REFBPE"
                                End If 'GMT01
                            Else 'GMT01
                                If InPerfil("AUTO") And perfilConcentrico("AUTO", xDir, xGer, xSec) Then 'GMT01
                                    cboAccion.AddItem "Al referente de Sistema" & ESPACIOS & "||" & "COMITE" 'GMT01
                                End If 'GMT01
                            End If
                        Else
                            If InPerfil("AUTO") And perfilConcentrico("AUTO", xDir, xGer, xSec) Then
                                cboAccion.AddItem "Al referente de Sistema" & ESPACIOS & "||" & "COMITE"
                            End If
                        End If
                    End If
                End If
                If glUsrPerfilActual = "REFE" And ClearNull(aplRST!cod_estnew) = "AUTORI" Then
                    If InPerfil("AUTO") And perfilConcentrico("AUTO", xDir, xGer, xSec) Then
                        If sGestion = "BPE" Then
                           If Hab_RGyP Then 'GMT01
                              cboAccion.AddItem "Al referente de RGyP" & ESPACIOS & "||" & "REFBPE"
                           Else   'GMT01
                              cboAccion.AddItem "Al referente de Sistema" & ESPACIOS & "||" & "COMITE" 'GMT01
                           End If 'GMT01
                        Else
                            cboAccion.AddItem "Al referente de Sistema" & ESPACIOS & "||" & "COMITE"
                        End If
                    End If
                End If
                If ClearNull(aplRST!cod_estnew) = "EJECUC" Then
                    lblFechaIniReal.visible = True
                    txtFechaIniReal.visible = True
                End If
                If ClearNull(aplRST!cod_estnew) = "TERMIN" Then
                    lblFechaFinReal.visible = True
                    txtFechaFinReal.visible = True
                End If
                If ClearNull(aplRST!cod_estnew) = "ANEXAD" Then
                    lblAnexar.visible = True
                    txtAnexar.visible = True
                    cmdAnexar.visible = True
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    ''se queda con el cogigo m�s grande, que significa la menor importancia
    xUsrLoginActual = glLOGIN_ID_REEMPLAZO
    xUsrPerfilActual = glUsrPerfilActual
    xImportancia = ""
    If sp_GetImportancia(Null, Null) Then
        Do While Not aplRST.EOF
            xImportancia = Trim(aplRST!cod_importancia)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    If cboAccion.ListCount > 0 Then
        Call setHabilCtrl(cboAccion, "OBL")
        If cboAccion.ListCount = 1 Then
            cboAccion.ListIndex = 0
            Call setHabilCtrl(cboAccion, "DIS")
        End If
    Else
        Call setHabilCtrl(cboAccion, "DIS")
    End If
End Sub

Sub habilitar()
    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = True
    lblAccion = ""
    txtFechaIniReal.Enabled = False
    txtFechaFinReal.Enabled = False
    txtAnexar.Enabled = False
    cmdAnexar.Enabled = False
            
    Call setHabilCtrl(txtFechaIniReal, "DIS")
    Call setHabilCtrl(txtFechaFinReal, "DIS")
    Call setHabilCtrl(obsResp, "NOR")
            
    Select Case sOpcionSeleccionada
        Case "DEVSOL", "DEVREF", "DEVSUP", "DEVAUT", "ANULAD", "RECHAZ", "RECHTE", "OPINOK", "EVALOK", "CANCEL", "SUSPEN"
            Call setHabilCtrl(obsResp, OBLIGATORIO)
        Case "EJECUC"
            Call setHabilCtrl(txtFechaIniReal, OBLIGATORIO)
        Case "TERMIN"
            Call setHabilCtrl(txtFechaFinReal, OBLIGATORIO)
        Case "ANEXAD"
            Call setHabilCtrl(cmdAnexar, NORMAL)
    End Select
End Sub

Private Sub cboAccion_Click()
    sOpcionSeleccionada = ""
    If cboAccion.ListIndex < 0 Then
        MsgBox ("Debe seleccionar una Acci�n")
        Exit Sub
    End If
    sOpcionSeleccionada = CodigoCombo(Me.cboAccion, True)
    Call habilitar
    cmdConfirmar.Enabled = True
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub cmdConfirmar_Click()
    Dim nUltimoAsignado As Long
    Dim nNumeroAsignado As Long
    Dim NroHistorial As Long
    Dim xAccion As String
    Dim flgOk As Boolean
    Dim bContinuar As Boolean
    Dim cNuevoEstadoSectorHomo As String
    Dim per_nrointerno As Long
    Dim per_estado As String
    
    If Not CamposObligatorios Then Exit Sub

    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = False
    
    flgOk = True
    
    Call Puntero(True)
    '**********************************************
    'para cambiar el estado de la peticion habr�a que
    'verificar que no tenga un estado superior al que
    'le voy a poner
    '**********************************************
    'atenci�n
    'hay cambios de estado que no se permitir�n m�s desde esta pantalla
    'porque no piden fechas de estimaci�n, y los hijos quedar�an con datos incongruentes
    'esos cambios deben realizarse desde los sectores, donde se validar� la completitud de las fechas
    'muchas gracias
    '**********************************************
    If Not InStr(1, "ADMI|SADM|", glUsrPerfilActual, vbTextCompare) > 0 Then
        If InStr(1, "OPINOK|EVALOK|ESTIOK|PLANOK|EJECUC|TERMIN|", sOpcionSeleccionada, vbTextCompare) > 0 Then
            MsgBox ("Operaci�n no permitida a nivel de Petici�n" & Chr(13) & "Debe realizarse a nivel de cada Sector")
            GoTo NO_REALIZA
        End If
    End If
    Call sp_DeleteMensaje(glNumeroPeticion, 0)      'SE ELIMINAN SIEMPRE LOS MENSAJES, CUANDO SE CAMBIA DE ESTADO DESDE LA PETICIONS
    If sOpcionSeleccionada <> "ANEXAD" Then
        Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", sOpcionSeleccionada, Null, Null)
        Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)
        Call sp_UpdatePetField(glNumeroPeticion, "NROANEX", "", Null, 0)
    End If
    'esta pregunta se hace aca para saber si hay que generar historial
    If sOpcionSeleccionada = "ANEXAD" Then
        If Not sp_AnexarPeticion(glNumeroPeticion, Val(getStrCodigo(glAuxRetorno, True))) Then
            flgOk = False
        End If
    End If
    'cada vez que se produzca un cambio de estado de peticion
    'se eliminan los sec/gru opinion/evaluacion
    If flgOk Then Call sp_DeletePeticionSectorEstado(glNumeroPeticion, "OPINIO|OPINOK|EVALUA|EVALOK")
    ' TODO: revisar en el sp_AddHistorial que no genere la l�nea en HistorialMemo si no se ingresa una observaci�n (evitar grabar "<< >>" sin sentido).
    If flgOk Then NroHistorial = sp_AddHistorial(glNumeroPeticion, "PCHGEST", sOpcionSeleccionada, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & Trim(obsResp.text) & " �")
  
    Select Case sOpcionSeleccionada
        Case "CONFEC", "DEVSOL", "DEVREF", "DEVSUP", "DEVAUT"
        Case "REFERE"
            If glUsrPerfilActual = "SOLI" Then
                Call sp_UpdatePetField(glNumeroPeticion, "CHKSOLI", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
        Case "SUPERV"
            If glUsrPerfilActual = "REFE" Then
                Call sp_UpdatePetField(glNumeroPeticion, "CHKREFE", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
        Case "AUTORI"
            If glUsrPerfilActual = "SOLI" Then
                'caso del salto multiple
                Call sp_UpdatePetField(glNumeroPeticion, "CHKSOLI", glLOGIN_ID_REEMPLAZO, Null, Null)
                Call sp_UpdatePetField(glNumeroPeticion, "CHKREFE", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
            If glUsrPerfilActual = "REFE" Then
                Call sp_UpdatePetField(glNumeroPeticion, "CHKREFE", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
            If glUsrPerfilActual = "SUPE" Then
                Call sp_UpdatePetField(glNumeroPeticion, "CHKSUPE", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
        Case "COMITE", "REFBPE"
            If glUsrPerfilActual = "SOLI" Then
                'caso del salto multiple
                Call sp_UpdatePetField(glNumeroPeticion, "CHKSOLI", glLOGIN_ID_REEMPLAZO, Null, Null)
                Call sp_UpdatePetField(glNumeroPeticion, "CHKREFE", glLOGIN_ID_REEMPLAZO, Null, Null)
                Call sp_UpdatePetField(glNumeroPeticion, "CHKAUTO", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
            If glUsrPerfilActual = "REFE" Then
                'caso del salto multiple
                Call sp_UpdatePetField(glNumeroPeticion, "CHKREFE", glLOGIN_ID_REEMPLAZO, Null, Null)
                Call sp_UpdatePetField(glNumeroPeticion, "CHKAUTO", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
            If glUsrPerfilActual = "AUTO" Then
                Call sp_UpdatePetField(glNumeroPeticion, "CHKAUTO", glLOGIN_ID_REEMPLAZO, Null, Null)
            End If
            ' ACA!!! REVISAR PARA EL PROCESO DE DEPURACION!!! 26.02.2016
            If auxNroAsig = "S/N" Then
                nNumeroAsignado = sp_ProximoNumero("ULPETASI")
                If nNumeroAsignado > 0 Then
                    If sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, nNumeroAsignado) Then
                       Call sp_AddHistorial(glNumeroPeticion, "PASINRO", sOpcionSeleccionada, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Asignaci�n de N�mero de Petici�n: " & nNumeroAsignado & " �")
                    Else
                        MsgBox ("Error al asignar numero")
                    End If
                Else
                    MsgBox ("Error al asignar numero")
                End If
            End If
            If sOpcionSeleccionada = "COMITE" Then Call sp_UpdatePetField(glNumeroPeticion, "FECHACOM", Null, Format(Now, "dd/mm/yyyy"), Null)
            If sOpcionSeleccionada = "REFBPE" Then Call sp_UpdatePetField(glNumeroPeticion, "FECHABPE", Null, Format(Now, "dd/mm/yyyy"), Null)
            ''ATENCIOAN
            ''BUG CONOCIDO!!
            ''si hay grupo debe manda agrupo, si solo sector solo al sector
            If HaySector() Then
                Call sp_ChgEstadoSector(glNumeroPeticion, Null, "PLANIF")
                Call sp_ChgEstadoGrupo(glNumeroPeticion, Null, Null, "PLANIF")
                Call sp_DoMensaje("PET", "GNEW000", glNumeroPeticion, "", "NULL", "", "")
                Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", sOpcionSeleccionada, Null, Null)
            End If
        Case "OPINIO", "EVALUA"
            Call sp_HeredaEstadoSector(glNumeroPeticion, sOpcionSeleccionada, Null, Null, Null, Null, 0, "OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC", "H", 0)
            Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
        Case "ESTIMA", "PLANIF"
            Call sp_HeredaEstadoSector(glNumeroPeticion, sOpcionSeleccionada, Null, Null, Null, Null, 0, "OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA", "H", 0)
            Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
        Case "SUSPEN"
            Call sp_HeredaEstadoSector(glNumeroPeticion, "SUSPEN", Null, Null, Null, Null, 0, "OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|REVISA", "E", NroHistorial)
        Case "CANCEL"
            Call sp_UpdatePetField(glNumeroPeticion, "FECHAFRL", Null, txtFechaFinReal.DateValue, Null)
            Call sp_ChgEstadoSector(glNumeroPeticion, Null, "CANCEL")
            Call sp_ChgEstadoGrupo(glNumeroPeticion, Null, Null, "CANCEL")
            Call sp_DeleteMensaje(glNumeroPeticion, 0)
            Call sp_DeletePeticionChangeMan(glNumeroPeticion)
        Case "ANULAD"
           'los sectores y grupos se eliminan
            Call sp_UpdatePetField(glNumeroPeticion, "FECHAFRL", Null, txtFechaFinReal.DateValue, Null)
            Call sp_ChgEstadoSector(glNumeroPeticion, Null, "CANCEL")
            Call sp_ChgEstadoGrupo(glNumeroPeticion, Null, Null, "CANCEL")
            Call sp_DeleteMensaje(glNumeroPeticion, 0)
            Call sp_DeletePeticionChangeMan(glNumeroPeticion)
        Case "RECHAZ"
            Call sp_ChgEstadoSector(glNumeroPeticion, Null, "CANCEL")
            Call sp_ChgEstadoGrupo(glNumeroPeticion, Null, Null, "CANCEL")
            Call sp_DeleteMensaje(glNumeroPeticion, 0)
            Call sp_DeletePeticionChangeMan(glNumeroPeticion)
        Case "RECHTE"
            Call sp_ChgEstadoSector(glNumeroPeticion, Null, "CANCEL")
            Call sp_ChgEstadoGrupo(glNumeroPeticion, Null, Null, "CANCEL")
            Call sp_DeleteMensaje(glNumeroPeticion, 0)
            Call sp_DeletePeticionChangeMan(glNumeroPeticion)
    End Select
    If sOpcionSeleccionada = "EJECUC" Then
        ' Agrego los datos de la petici�n en la cadena HOST-ChangeMan
        Call sp_InsertPeticionEnviadas2(glNumeroPeticion, auxNroAsig, sPetClass, "", "")
    Else
        Call sp_DeletePeticionEnviadas2(glNumeroPeticion)
    End If

    ' En principio esto no servir�a, porque los grupos y/o sectores son eliminados al cambiar de estado la petici�n
    bContinuar = False
    If InStr(1, "APROBA|ESTIMA|PLANIF|REVISA|EJECUC|ESTIOK|PLANOK|ESTIRK|PLANRK|EJECRK|SUSPEN|", sOpcionSeleccionada, vbTextCompare) > 0 Then
        If sp_GetPeticionGrupo(glNumeroPeticion, Null, Null) Then                                   ' Busco si existe al menos un grupo homologable en estado activo
            Do While Not aplRST.EOF
                If ClearNull(aplRST.Fields!homo) = "S" Then
                    If InStr(1, "ANEXAD|ANULAD|CANCEL|RECHAZ|RECHTE|TERMIN|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                        bContinuar = True                                                           ' Al menos uno de ellos es homologable en estado activo
                        Exit Do
                    End If
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If bContinuar Then
            If Not sp_GetPetGrupoHomologacion(glNumeroPeticion, "H") Then       ' Si NO existe para la petici�n el grupo Homologador
                If sp_GetGrupoHomologacion Then                             ' Si existe definido el grupo Homologador...
                    cNuevoEstadoSectorHomo = getNuevoEstadoSector(glNumeroPeticion, glHomologacionSector)                               ' Obtengo el nuevo estado del sector de homologaci�n.
                    Call sp_InsertPeticionGrupo(glNumeroPeticion, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", 0, "0")
                    Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, glHomologacionGrupo, "ESTIMA", "", "")                        ' Se envian los mensajes correspondientes a Responsables del Grupo "Homologador"
                    cNuevoEstadoSectorHomo = getNuevoEstadoSector(glNumeroPeticion, glHomologacionSector)
                    Call sp_InsertPeticionSector(glNumeroPeticion, glHomologacionSector, Null, Null, Null, Null, 0, cNuevoEstadoSectorHomo, date, "", 0, "0")
                End If
            Else
                If sp_GetPeticionGrupo(glNumeroPeticion, Null, Null) Then
                    Do While Not aplRST.EOF
                        If ClearNull(aplRST.Fields!homo) = "H" Then
                            ' Si el grupo homologador se encuentra en un estado terminal, se reactiva
                            If InStr(1, "ANEXAD|ANULAD|CANCEL|RECHAZ|RECHTE|TERMIN", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                                ' Obtengo el nuevo estado del sector de homologaci�n.
                                cNuevoEstadoSectorHomo = getNuevoEstadoSector(glNumeroPeticion, glHomologacionSector)
                                ' Si existe el grupo homologador en estado no activo, debo reactivarlo
                                NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", sOpcionSeleccionada, glHomologacionSector, cNuevoEstadoSectorHomo, glHomologacionGrupo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Reactivaci�n autom�tica del grupo homologador �")
                                Call sp_UpdatePeticionGrupo(glNumeroPeticion, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, NroHistorial)
                                ' Se envian los mensajes correspondientes a Responsables del Grupo "Homologador"
                                Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, glHomologacionGrupo, "ESTIMA", "", "")
                                ' Se registra en el historial el evento autom�tico de cambio de estado autom�tico del grupo Homologador
                                Call sp_AddHistorial(glNumeroPeticion, "AGCHGEST", sOpcionSeleccionada, glHomologacionSector, cNuevoEstadoSectorHomo, glHomologacionGrupo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� A ESTIMAR ESFUERZO � Cambio autom�tico de estado del grupo homologador.")
                                ' Actualiza el estado del Sector del grupo Homologador
                                cNuevoEstadoSectorHomo = getNuevoEstadoSector(glNumeroPeticion, glHomologacionSector)
                                Call sp_UpdatePeticionSector(glNumeroPeticion, glHomologacionSector, Null, Null, Null, Null, 0, cNuevoEstadoSectorHomo, date, "", NroHistorial, NroHistorial)
                            End If
                            Exit Do
                        End If
                        aplRST.MoveNext
                        DoEvents
                    Loop
                End If
            End If
        End If
    End If
NO_REALIZA:
    Call Puntero(False)
    Call touchForms
    Unload Me
End Sub

Private Sub cmdAnexar_Click()
    Dim vRetorno() As String
    Dim nPos As Integer
    glAuxRetorno = ""
    glAuxEstado = "COMITE|OPINIO|OPINOK|EVALUA|EVALOK|APROBA|PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC"
    frmSelPet.Show 1
    DoEvents
    If glAuxRetorno <> "" Then
        txtAnexar = glAuxRetorno
        fraSector.Refresh
    End If
End Sub

Private Sub cmdVerActuantes_Click()
    If sOpcionSeleccionada = "" Then
        MsgBox "Debe seleccionar un estado para ver los actuantes", vbExclamation + vbOKOnly, "Seleccionar estado"
        Exit Sub
    End If
    cmdVerActuantes.Enabled = False
    Load auxRecActuantes
    auxRecActuantes.pubCodEstado = sOpcionSeleccionada
    auxRecActuantes.pubNroPeticion = glNumeroPeticion
    auxRecActuantes.CargarGrid
    auxRecActuantes.Show 1
    Unload auxRecActuantes
    cmdVerActuantes.Enabled = True
End Sub

Function CamposObligatorios() As Boolean
    CamposObligatorios = False
    
    If glUsrPerfilActual = "BPAR" Or glUsrPerfilActual = "CGRU" Or glUsrPerfilActual = "CSEC" Then
        If Trim(sPetClass) = "SINC" Then       ' No tiene la clase definida
            MsgBox "Antes de cambiar el estado de una petici�n, debe clasificarla, seleccionando la clase a la que pertenece en la pantalla anterior.", vbCritical + vbOKOnly, "Clasificaci�n de petici�n inexistente"
            CamposObligatorios = False
            Exit Function
        End If
    End If

    If cboAccion.ListIndex < 0 Then
        MsgBox ("Debe seleccionar una Acci�n")
        CamposObligatorios = False
        Exit Function
    End If
    
    Call Puntero(True)
    
    If InStr(1, "RECHAZ|RECHTE|TERMIN|CANCEL|ANEXAD|ANULAD", sOpcionSeleccionada) = 0 Then
        If Not sp_GetRecursoActuantePeticion(glNumeroPeticion, sOpcionSeleccionada) Then
            MsgBox "No hay recursos que puedan continuar el tratamiento" & vbCrLf & "de la petici�n para el estado seleccionado.", vbOKOnly + vbInformation
            CamposObligatorios = False
            Call Puntero(False)
            Exit Function
        End If
        If Not aplRST.EOF Then
            aplRST.Close
        Else
            MsgBox "No hay recursos que puedan continuar el tratamiento" & vbCrLf & "de la petici�n para el estado seleccionado.", vbOKOnly + vbInformation
            CamposObligatorios = False
            Call Puntero(False)
            Exit Function
        End If
    End If
    
    Call Puntero(False)
    
    Select Case sOpcionSeleccionada
    Case "DEVSOL", "DEVREF", "DEVSUP", "DEVAUT", "ANULAD", "RECHAZ", "RECHTE", "OPINOK", "EVALOK", "CANCEL", "SUSPEN"
        If Trim(Me.obsResp.text) = "" Then
            MsgBox "Se deben ingresar observaciones", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    Case "EJECUC"
        If txtFechaIniReal.text = "" Then
              MsgBox "Se debe informar Fecha de Inicio", vbOKOnly + vbInformation
               CamposObligatorios = False
               Exit Function
        End If
    Case "ANEXAD"
        If Val(getStrCodigo(txtAnexar.text, True)) = 0 Then
            MsgBox "Se debe seleccionar petici�n a anexar", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
        If Val(glNumeroPeticion) = Val(getStrCodigo(glAuxRetorno, True)) Then
            MsgBox "Se est� intentando anexar la petici�n a s� misma", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    Case "TERMIN"
        If txtFechaFinReal.text = "" Then
            MsgBox "Se debe informar Fecha de Terminaci�n", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    Case "APROBA"
        If auxNroAsig = "S/N" Then
            MsgBox "Esta Petici�n no posee n�mero asignado", vbOKOnly + vbInformation
            CamposObligatorios = False
            Exit Function
        End If
    End Select
    CamposObligatorios = True
End Function

Function perfilConcentrico(xPrf, sDir, sGer, sSec) As Boolean
    Dim xPerfNivel As String, xPerfArea As String
    perfilConcentrico = False
    
    xPerfNivel = getPerfNivel(xPrf)
    xPerfArea = getPerfArea(xPrf)
                
    If (xPerfNivel = "BBVA" Or _
        xPerfNivel = "DIRE" And sDir = xPerfArea Or _
        xPerfNivel = "GERE" And sGer = xPerfArea Or _
        xPerfNivel = "SECT" And sSec = xPerfArea) Then
        perfilConcentrico = True
    End If
End Function

Private Function HaySector() As Boolean
    HaySector = False
    If Not sp_GetPeticionSector(glNumeroPeticion, Null) Then Exit Function
    If Not aplRST.EOF Then
        HaySector = True
        aplRST.Close
    End If
End Function
Private Function recupHabilitaciones() As Boolean
    'se recuperan los valores para saber que flujo de trabajo esta habilitado
    recupHabilitaciones = False
    Hab_RGyP = True
    
    'indica si ver la solapa de vinculados
    If sp_GetVarios("HAB_RGyP") Then
        Hab_RGyP = ClearNull(aplRST.Fields!var_numero)
    End If

    recupHabilitaciones = True
 End Function
'GMT01 - FIN


