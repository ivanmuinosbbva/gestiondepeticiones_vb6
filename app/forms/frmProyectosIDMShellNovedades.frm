VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmProyectosIDMShellNovedades 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Administrar novedades registradas"
   ClientHeight    =   6315
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11700
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   11700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraPrincipal 
      Height          =   855
      Left            =   120
      TabIndex        =   9
      Top             =   0
      Width           =   10035
      Begin VB.CheckBox chkMostrarBorrados 
         Caption         =   "Mostrar novedades borradas"
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label lblEjemplo 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Novedades borradas"
         ForeColor       =   &H00808080&
         Height          =   180
         Index           =   2
         Left            =   8400
         TabIndex        =   11
         Top             =   360
         Width           =   1545
      End
      Begin VB.Label lblEjemplo 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Novedades agregadas"
         Height          =   180
         Index           =   0
         Left            =   8295
         TabIndex        =   10
         Top             =   120
         Width           =   1665
      End
   End
   Begin VB.TextBox txtNovedadesTexto 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   4800
      Width           =   9975
   End
   Begin VB.Frame fraBotones 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6255
      Left            =   10200
      TabIndex        =   7
      Top             =   0
      Width           =   1455
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Borrar"
         Height          =   375
         Left            =   120
         TabIndex        =   5
         ToolTipText     =   "Borra las novedades, pero no las elimina"
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Editar"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   5760
         Width           =   1215
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   6600
         Width           =   1215
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   3735
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   10035
      _ExtentX        =   17701
      _ExtentY        =   6588
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   13798741
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmProyectosIDMShellNovedades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public lProjId As Long
Public lProjSubId As Long
Public lProjSubSId As Long

Private Const colPROJID = 0
Private Const colPROJSUBID = 1
Private Const colPROJSUBSID = 2
Private Const colMEMFECHA = 3
Private Const colMEMSECUENCIA = 4
Private Const colMEMTEXTO = 5
Private Const colRECURSO = 6
Private Const colNOMBRERECURSO = 7
Private Const colMEM_ACCION = 8
Private Const colNOM_ACCION = 9
Private Const colMEM_FECHA2 = 10
Private Const colCOD_USUARIO2 = 11
Private Const colNOM_USUARIO2 = 12
Private Const colHIDDEN_MEMFECHA = 13
Private Const colTOTALCOLS = 14

Public cNuevaNovedad As String

Dim bLoading As Boolean

Private Sub Form_Load()
    bLoading = True
    Call Inicializar_Controles
    Call CargarGrilla
    bLoading = False
End Sub

Private Sub Inicializar_Controles()
    chkMostrarBorrados.Value = 1
End Sub

Private Sub InicializarGrilla()
    Dim i As Integer
    With grdDatos
        .Visible = False
        .Clear
        .Rows = 1
        .Cols = colTOTALCOLS
        .TextMatrix(0, colPROJID) = "ProjId": .ColWidth(colPROJID) = 0
        .TextMatrix(0, colPROJSUBID) = "ProjSubId": .ColWidth(colPROJSUBID) = 0
        .TextMatrix(0, colPROJSUBSID) = "ProjSubSId": .ColWidth(colPROJSUBSID) = 0
        .TextMatrix(0, colMEMFECHA) = "Fecha": .ColWidth(colMEMFECHA) = 1800
        .TextMatrix(0, colMEMSECUENCIA) = "Rng.": .ColWidth(colMEMSECUENCIA) = 0
        .TextMatrix(0, colMEMTEXTO) = "Texto": .ColWidth(colMEMTEXTO) = 6500: .ColAlignment(colMEMTEXTO) = flexAlignLeftCenter
        .TextMatrix(0, colRECURSO) = "Recurso": .ColWidth(colRECURSO) = 0
        .TextMatrix(0, colNOMBRERECURSO) = "Nombre": .ColWidth(colNOMBRERECURSO) = 2000
        .TextMatrix(0, colMEM_ACCION) = "mem_accion": .ColWidth(colMEM_ACCION) = 0
        .TextMatrix(0, colNOM_ACCION) = "Acci�n": .ColWidth(colNOM_ACCION) = 2000
        .TextMatrix(0, colMEM_FECHA2) = "Fecha": .ColWidth(colMEM_FECHA2) = 2000
        .TextMatrix(0, colCOD_USUARIO2) = "cod_usuario2": .ColWidth(colCOD_USUARIO2) = 0
        .TextMatrix(0, colNOM_USUARIO2) = "Realizado por": .ColWidth(colNOM_USUARIO2) = 2000
        .TextMatrix(0, colHIDDEN_MEMFECHA) = "": .ColWidth(colHIDDEN_MEMFECHA) = 2000
        CambiarEfectoLinea grdDatos, prmGridEffectFontBold
        .RowSel = .Rows - 1: .ColSel = .Cols - 1    ' Selecciona la primera fila de todas
        .HighLight = flexHighlightAlways
        .FocusRect = flexFocusLight
    End With
End Sub

Private Sub CargarGrilla()
    With grdDatos
        Call Puntero(True)
        InicializarGrilla
        If sp_GetProyectoIDMNovedades2(lProjId, lProjSubId, lProjSubSId, Null, "NOVEDADES") Then
            Do While Not aplRST.EOF
                If (chkMostrarBorrados.Value = 0 And ClearNull(aplRST.Fields!mem_accion) = "DEL") Then
                    aplRST.MoveNext
                Else
                    .Rows = .Rows + 1
                    .TextMatrix(.Rows - 1, colPROJID) = ClearNull(aplRST.Fields!ProjId)
                    .TextMatrix(.Rows - 1, colPROJSUBID) = ClearNull(aplRST.Fields!ProjSubId)
                    .TextMatrix(.Rows - 1, colPROJSUBSID) = ClearNull(aplRST.Fields!ProjSubSId)
                    .TextMatrix(.Rows - 1, colMEMFECHA) = Format(ClearNull(aplRST.Fields!mem_fecha), "dd/mm/yyyy hh:MM")
                    .TextMatrix(.Rows - 1, colMEMSECUENCIA) = "0"
                    .TextMatrix(.Rows - 1, colMEMTEXTO) = ReplaceString(ReplaceString(ClearNull(aplRST.Fields!mem_texto), 10, " "), 13, "")
                    .TextMatrix(.Rows - 1, colRECURSO) = ClearNull(aplRST.Fields!cod_usuario)
                    .TextMatrix(.Rows - 1, colNOMBRERECURSO) = ClearNull(aplRST.Fields!nom_usuario)
                    .TextMatrix(.Rows - 1, colMEM_ACCION) = ClearNull(aplRST.Fields!mem_accion)
                    .TextMatrix(.Rows - 1, colNOM_ACCION) = ClearNull(aplRST.Fields!nom_accion)
                    .TextMatrix(.Rows - 1, colMEM_FECHA2) = ClearNull(aplRST.Fields!mem_fecha2)
                    .TextMatrix(.Rows - 1, colCOD_USUARIO2) = ClearNull(aplRST.Fields!cod_usuario2)
                    .TextMatrix(.Rows - 1, colNOM_USUARIO2) = ClearNull(aplRST.Fields!nom_usuario2)
                    .TextMatrix(.Rows - 1, colHIDDEN_MEMFECHA) = ClearNull(aplRST.Fields!mem_fecha)
                    Select Case ClearNull(aplRST.Fields!mem_accion)
                        Case "DEL": CambiarForeColorLinea grdDatos, prmGridFillRowColorDarkGrey
                        Case "UPD": CambiarForeColorLinea grdDatos, prmGridFillRowColorDarkBlue
                    End Select
                    aplRST.MoveNext
                End If
                DoEvents
            Loop
        End If
        Call Puntero(False)
        .Visible = True
    End With
End Sub

Private Sub cmdAgregar_Click()
    Dim dFechaHora As Date
    
    With frmProyectosIDMEditNovedades
        dFechaHora = Now
        .cModo = "ALTA"
        .Show 1
        If ClearNull(cNuevaNovedad) <> "" Then
            If MsgBox("�Confirma el agregado?", vbQuestion + vbOKCancel, "Confirmar novedades") = vbOK Then
                Call sp_InsertProyectoIDMNovedades(lProjId, lProjSubId, lProjSubSId, dFechaHora, "NOVEDADES", ClearNull(cNuevaNovedad), glLOGIN_ID_REEMPLAZO)
                Call sp_UpdateProyectoIDMField(lProjId, lProjSubId, lProjSubSId, "FE_MODIF", Null, date, Null)   ' Actualiza la fecha de �ltima modificaci�n
                CargarGrilla
            End If
        End If
    End With
End Sub

Private Sub cmdModificar_Click()
    If grdDatos.Rows > 1 Then
        With frmProyectosIDMEditNovedades
            .cModo = "EDICION"
            .cTexto = sp_GetProyectoIDMNovedadesMemo(lProjId, lProjSubId, lProjSubSId, grdDatos.TextMatrix(grdDatos.RowSel, colHIDDEN_MEMFECHA), "NOVEDADES")
            .Show 1
            If ClearNull(cNuevaNovedad) <> "" Then
                If MsgBox("�Confirma la actualizaci�n?", vbQuestion + vbOKCancel, "Confirmar novedades") = vbOK Then
                    With grdDatos
                        Call sp_DeleteProyectoIDMNovedades(lProjId, lProjSubId, lProjSubSId, .TextMatrix(.RowSel, colHIDDEN_MEMFECHA), "NOVEDADES", Null, "KILL")
                        Call sp_InsertProyectoIDMNovedades(lProjId, lProjSubId, lProjSubSId, .TextMatrix(.RowSel, colHIDDEN_MEMFECHA), "NOVEDADES", ClearNull(cNuevaNovedad), glLOGIN_ID_REEMPLAZO)
                        'Call sp_UpdateProyectoIDMNovedades(lProjId, lProjSubId, lProjSubSId, .TextMatrix(.RowSel, colMEMFECHA), "NOVEDADES", ClearNull(cNuevaNovedad), glLOGIN_ID_REEMPLAZO)
                        .TextMatrix(.RowSel, colMEMTEXTO) = ClearNull(cNuevaNovedad): txtNovedadesTexto.Text = ClearNull(cNuevaNovedad)
                        .TextMatrix(.RowSel, colMEM_ACCION) = "UPD"
                        .TextMatrix(.RowSel, colNOM_ACCION) = "Actualizado"
                        .TextMatrix(.RowSel, colMEM_FECHA2) = Format(Now, "dd/mm/yyyy hh:MM:ss")
                        .TextMatrix(.RowSel, colCOD_USUARIO2) = glLOGIN_ID_REEMPLAZO
                        .TextMatrix(.RowSel, colNOM_USUARIO2) = glLOGIN_NAME_REEMPLAZO
                    End With
                    Call spProyectoIDM.sp_UpdateProyectoIDMField(lProjId, lProjSubId, lProjSubSId, "FE_MODIF", Null, date, Null)   ' Actualiza la fecha de �ltima modificaci�n
                    'Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorDarkBlue)
                End If
            End If
        End With
    End If
End Sub

Private Sub cmdEliminar_Click()
    With grdDatos
        If .Rows > 1 Then
            If MsgBox("�Confirma la eliminaci�n?", vbQuestion + vbOKCancel, "Eliminar novedades") = vbOK Then
                Call spProyectoIDM.sp_DeleteProyectoIDMNovedades(lProjId, lProjSubId, lProjSubSId, .TextMatrix(.RowSel, colHIDDEN_MEMFECHA), "NOVEDADES", Null, "DEL")
                Call spProyectoIDM.sp_UpdateProyectoIDMField(lProjId, lProjSubId, lProjSubSId, "FE_MODIF", Null, date, Null)   ' Actualiza la fecha de �ltima modificaci�n
                .TextMatrix(.RowSel, colMEM_ACCION) = "DEL"
                .TextMatrix(.RowSel, colNOM_ACCION) = "Borrado"
                .TextMatrix(.RowSel, colMEM_FECHA2) = Format(Now, "dd/mm/yyyy hh:MM:ss")
                .TextMatrix(.RowSel, colCOD_USUARIO2) = glLOGIN_ID_REEMPLAZO
                .TextMatrix(.RowSel, colNOM_USUARIO2) = glLOGIN_NAME_REEMPLAZO
                Call CambiarForeColorLinea(grdDatos, prmGridFillRowColorDarkGrey)
                If chkMostrarBorrados.Value = 0 Then .RemoveItem .RowSel
            End If
        End If
    End With
End Sub

Private Sub MostrarSeleccion()
    Dim sTexto As String
    
    With grdDatos
        If .Rows > 1 Then
            sTexto = sp_GetProyectoIDMNovedadesMemo(lProjId, lProjSubId, lProjSubSId, .TextMatrix(.RowSel, colHIDDEN_MEMFECHA), "NOVEDADES")
            'sTexto = ReplaceString(sTexto, 13, "")
            'sTexto = ReplaceString(sTexto, 10, " ")
            txtNovedadesTexto.Text = sTexto
        End If
    End With
End Sub

Private Sub grdDatos_Click()
    MostrarSeleccion
End Sub

Private Sub grdDatos_SelChange()
    MostrarSeleccion
End Sub

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub chkMostrarBorrados_Click()
    If Not bLoading Then
        CargarGrilla
    End If
End Sub
