VERSION 5.00
Begin VB.Form frmPeticionesSectorReasignar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Reasignaci�n Sector Interviniente"
   ClientHeight    =   3270
   ClientLeft      =   1155
   ClientTop       =   330
   ClientWidth     =   8280
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   8280
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cboSector 
      Height          =   315
      Left            =   60
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   16
      ToolTipText     =   "Sector Solicitante"
      Top             =   1740
      Width           =   7890
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   795
      Left            =   60
      TabIndex        =   4
      Top             =   -60
      Width           =   8190
      Begin VB.Label Label6 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   10
         Top             =   480
         Width           =   1200
      End
      Begin VB.Label lblPrioridad 
         Caption         =   "prior"
         Height          =   195
         Left            =   7620
         TabIndex        =   9
         Top             =   480
         Width           =   480
      End
      Begin VB.Label lblTitulo 
         Caption         =   "Peticion"
         Height          =   195
         Left            =   1305
         TabIndex        =   8
         Top             =   240
         Width           =   6705
      End
      Begin VB.Label lblEstado 
         Caption         =   "Estado"
         Height          =   195
         Left            =   1300
         TabIndex        =   7
         Top             =   480
         Width           =   2580
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Prioridad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6660
         TabIndex        =   6
         Top             =   480
         Width           =   765
      End
      Begin VB.Label Label2 
         Caption         =   "Petici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   5
         Top             =   240
         Width           =   1200
      End
   End
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   60
      TabIndex        =   11
      Top             =   660
      Width           =   8190
      Begin VB.Label Label4 
         Caption         =   "Estado Actual"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4320
         TabIndex        =   15
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblEstadoSector 
         Caption         =   "Estado"
         Height          =   195
         Left            =   5565
         TabIndex        =   14
         Top             =   180
         Width           =   2580
      End
      Begin VB.Label Label9 
         Caption         =   "Sector"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   13
         Top             =   180
         Width           =   1200
      End
      Begin VB.Label lblSector 
         Caption         =   "sector"
         Height          =   195
         Left            =   1320
         TabIndex        =   12
         Top             =   180
         Width           =   2745
      End
   End
   Begin VB.Frame pnlBtnControl 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   60
      TabIndex        =   0
      Top             =   2610
      Width           =   8190
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   375
         Left            =   6210
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   7200
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Width           =   885
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Reasignar al sector:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   1440
      Width           =   1680
   End
End
Attribute VB_Name = "frmPeticionesSectorReasignar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' -001- a. FJS 20.08.2008 - Se modifica el font del combo de sectores para evitar la visualizaci�n de los c�digos.

Option Explicit

Dim sOpcionSeleccionada As String
Dim EstadoSolicitud As String
Dim sGrupo As String
Dim sSector As String
Dim sGerencia As String
Dim sDireccion As String
Dim sCoorSect As String
Dim EstadoPeticion As String
Dim NuevoEstadoPeticion As String
Dim EstadoSector As String
Dim xPerfNivel, xPerfArea As String
Dim hst_nrointerno_sol
Dim flgAccion As Boolean

Sub InicializarCombos()
    Dim flgExtendido As Boolean
    Dim xAccion As String
    Dim sDireccion As String
    Dim sGerencia As String
    
    'si tiene ingerencia en el area del sector
    cboSector.Clear
    If sp_GetSectorXt("", "", "") Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!es_ejecutor) = "S" Then
                'cboSector.AddItem Trim(aplRST!nom_direccion) & "> " & Trim(aplRST!nom_gerencia) & "> " & Trim(aplRST!nom_sector) & Space(40) & "||" & aplRST(0)
                sDireccion = IIf(ClearNull(aplRST.Fields!abrev_direccion) <> "", ClearNull(aplRST.Fields!abrev_direccion), ClearNull(aplRST!nom_direccion))
                sGerencia = IIf(ClearNull(aplRST.Fields!abrev_gerencia) <> "", ClearNull(aplRST.Fields!abrev_gerencia), ClearNull(aplRST!nom_gerencia))
                cboSector.AddItem sDireccion & " � " & sGerencia & " � " & Trim(aplRST!nom_sector) & ESPACIOS & ESPACIOS & "||" & Trim(aplRST!cod_sector)
            End If
            aplRST.MoveNext
        Loop
        cboSector.ListIndex = 0
    End If
End Sub

Private Sub cmdCerrar_Click()
    Unload Me
End Sub

Private Sub cmdConfirmar_Click()
    Dim NroHistorial As Long
    If cboSector.ListIndex < 0 Then
        MsgBox ("Debe seleccionar un Sector")
        Exit Sub
    End If

    cmdConfirmar.Enabled = False
    cmdCerrar.Enabled = False
    Call Puntero(True)
    Call Status("Procesando")
    
    If sp_ChangePeticionSector(glNumeroPeticion, glSector, CodigoCombo(cboSector, True)) Then
        NroHistorial = sp_AddHistorial(glNumeroPeticion, "SREASIG", EstadoPeticion, CodigoCombo(cboSector, True), Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Sector anterior: " & Trim(lblSector) & " �")
        Call getIntegracionSector(glNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
        'averigua
        NuevoEstadoPeticion = getNuevoEstadoPeticion(glNumeroPeticion)
        Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
        'Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "", Null, Null)     ' Ver por qu� cancela 03.04.2012!
        If NuevoEstadoPeticion <> "TERMIN" Then
            fFinReal = Null
        End If
        If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoPeticion) > 0 Then
            Call sp_UpdatePetFechas(glNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
        Else
            Call sp_UpdatePetFechas(glNumeroPeticion, Null, Null, Null, Null, 0)
        End If
        
        Call sp_UpdHistorial(NroHistorial, "SREASIG", NuevoEstadoPeticion, CodigoCombo(cboSector, True), EstadoSector, Null, Null, glLOGIN_ID_REEMPLAZO)
    End If
    Call Puntero(False)
    Call Status("Listo")
    Call touchForms
    Unload Me

End Sub


Private Sub Form_Load()
    Call InicializarPantalla
    Call InicializarCombos
    cmdConfirmar.Enabled = True
End Sub

Sub InicializarPantalla()
   Dim auxNro As String
    Me.Tag = ""
    
    cmdConfirmar.Enabled = False
    If sp_GetUnaPeticion(glNumeroPeticion) Then
        If Not aplRST.EOF Then
            If Val(ClearNull(aplRST!pet_nroasignado)) = 0 Then
                auxNro = "S/N"
            Else
                auxNro = ClearNull(aplRST!pet_nroasignado)
            End If
            lblTitulo = auxNro & " - " & ClearNull(aplRST!Titulo)
            lblPrioridad = ClearNull(aplRST!prioridad)
            lblEstado.Caption = ClearNull(aplRST!nom_estado)
        End If
        'aplRST.Close
    End If


    If sp_GetPeticionSector(glNumeroPeticion, glSector) Then
        If Not aplRST.EOF Then
            fIniPlan = aplRST!fe_ini_plan
            fFinPlan = aplRST!fe_fin_plan
            fIniReal = aplRST!fe_ini_real
            fFinReal = aplRST!fe_fin_real
            hsPresup = Val(ClearNull(aplRST!horaspresup))
            lblSector = ClearNull(aplRST!cod_sector) & " : " & ClearNull(aplRST!nom_sector)
            lblEstadoSector = ClearNull(aplRST!nom_estado)
            EstadoSector = ClearNull(aplRST!cod_estado)
        End If
        'aplRST.Close
    End If
End Sub
