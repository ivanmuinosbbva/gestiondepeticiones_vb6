VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BC53DDCE-4B61-11D3-AAFB-0060973966D5}#1.1#0"; "AT_MASKTEXT.OCX"
Begin VB.Form frmHEDT1ShellDyDSel 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Agregar nuevo DSN al cat�logo"
   ClientHeight    =   6300
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8085
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   6.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmHEDT1ShellDyDSel.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6300
   ScaleWidth      =   8085
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraPrincipal 
      Height          =   5535
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   7935
      Begin VB.OptionButton optSeleccion 
         Caption         =   "No encontr� ning�n archivo para reutilizar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   3
         Top             =   840
         Width           =   3495
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   375
         Left            =   6600
         Picture         =   "frmHEDT1ShellDyDSel.frx":0B3A
         TabIndex        =   1
         ToolTipText     =   "Buscar archivos productivos"
         Top             =   330
         Width           =   855
      End
      Begin VB.OptionButton optSeleccion 
         Caption         =   "Ingrese o seleccione el archivo productivo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   2055
      End
      Begin MSFlexGridLib.MSFlexGrid grdSeleccion 
         Height          =   1695
         Left            =   600
         TabIndex        =   5
         Top             =   3600
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   2990
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         GridColor       =   14737632
         FocusRect       =   0
         HighLight       =   2
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdDatos 
         Height          =   1695
         Left            =   600
         TabIndex        =   10
         Top             =   1440
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   2990
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         GridColor       =   14737632
         FocusRect       =   0
         HighLight       =   2
         GridLinesFixed  =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin AT_MaskText.MaskText txtBuscarNombreProd 
         Height          =   300
         Left            =   2280
         TabIndex        =   0
         Top             =   360
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   529
         MaxLength       =   50
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   50
         UpperCase       =   -1  'True
      End
      Begin VB.Label lblPrincipal 
         AutoSize        =   -1  'True
         Caption         =   "Archivos productivos"
         ForeColor       =   &H00C00000&
         Height          =   180
         Left            =   600
         TabIndex        =   11
         Top             =   1200
         Width           =   1605
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "DSN disponibles para utilizar"
         ForeColor       =   &H00C00000&
         Height          =   180
         Left            =   600
         TabIndex        =   6
         Top             =   3360
         Width           =   2175
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   735
      Left            =   120
      TabIndex        =   7
      Top             =   5520
      Width           =   7935
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   420
         Left            =   5160
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Cancelar el agregado de DSN"
         Top             =   220
         Width           =   1290
      End
      Begin VB.CommandButton cmdContinuar 
         Caption         =   "Continuar"
         Default         =   -1  'True
         Height          =   420
         Left            =   6480
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Continuar el proceso de alta"
         Top             =   220
         Width           =   1290
      End
   End
End
Attribute VB_Name = "frmHEDT1ShellDyDSel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const COL_DSN_NOMPRODUCTIVO = 0
Private Const COL_DSN_NOMBRECOPY = 1
Private Const COL_DSN_BIBLIOCOPY = 2

Private Const SEL_DSN_ID = 0
Private Const SEL_DSN_NOM = 1
Private Const SEL_DSN_FECHAMODIF = 2

Public sDSN_ID As String
Public sModo As String

Private Sub Form_Load()
    Call Inicializar
End Sub

Private Sub Inicializar()
    sModo = "SEL"
    Call setHabilCtrl(cmdContinuar, "DIS")
    Call setHabilCtrl(cmdBuscar, "DIS")
    Call InicializarGrilla
    Call InicializarGrillaSeleccion
    grdDatos.Visible = True
    grdSeleccion.Visible = True
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .Visible = False
        .Clear
        .cols = 3
        .Rows = 1
        .TextMatrix(0, COL_DSN_NOMPRODUCTIVO) = "Archivo Productivo": .ColWidth(COL_DSN_NOMPRODUCTIVO) = 4500: .ColAlignment(COL_DSN_NOMPRODUCTIVO) = flexAlignLeftCenter
        .TextMatrix(0, COL_DSN_NOMBRECOPY) = "Nombre de COPY": .ColWidth(COL_DSN_NOMBRECOPY) = 2000: .ColAlignment(COL_DSN_NOMBRECOPY) = flexAlignLeftCenter
        .TextMatrix(0, COL_DSN_BIBLIOCOPY) = "Biblioteca de COPY": .ColWidth(COL_DSN_BIBLIOCOPY) = 2000: .ColAlignment(COL_DSN_BIBLIOCOPY) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub InicializarGrillaSeleccion()
    With grdSeleccion
        .Visible = False
        .Clear
        .cols = 3
        .Rows = 1
        .TextMatrix(0, SEL_DSN_ID) = "DSN": .ColWidth(SEL_DSN_ID) = 1200: .ColAlignment(SEL_DSN_ID) = flexAlignLeftCenter
        .TextMatrix(0, SEL_DSN_NOM) = "Nombre": .ColWidth(SEL_DSN_NOM) = 5130: .ColAlignment(SEL_DSN_NOM) = flexAlignLeftCenter
        .TextMatrix(0, SEL_DSN_FECHAMODIF) = "F.modif.": .ColWidth(SEL_DSN_FECHAMODIF) = 1200: .ColAlignment(SEL_DSN_FECHAMODIF) = flexAlignLeftCenter
        Call CambiarEfectoLinea(grdSeleccion, prmGridEffectFontBold)
    End With
End Sub

Private Sub cmdBuscar_Click()
    If ClearNull(txtBuscarNombreProd) <> "" Then Call CargarGrilla
End Sub

Private Sub CargarGrilla()
    Call InicializarGrilla
    If sp_GetHEDT101c(ClearNull(txtBuscarNombreProd)) Then
        Call Puntero(True)
        With grdDatos
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, COL_DSN_NOMPRODUCTIVO) = ClearNull(aplRST.Fields!dsn_nomprod)
                .TextMatrix(.Rows - 1, COL_DSN_BIBLIOCOPY) = ClearNull(aplRST.Fields!dsn_cpybbl)
                .TextMatrix(.Rows - 1, COL_DSN_NOMBRECOPY) = ClearNull(aplRST.Fields!dsn_cpynom)
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
    If aplRST.State = 1 Then Call Status(aplRST.RecordCount & " dsn(s).")
    grdDatos.Visible = True
    Call Puntero(False)
End Sub

Private Sub CargarGrillaSeleccion()
    Call InicializarGrillaSeleccion
    If sp_GetHEDT101d(ClearNull(grdDatos.TextMatrix(grdDatos.RowSel, COL_DSN_NOMPRODUCTIVO))) Then
        With grdSeleccion
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, SEL_DSN_ID) = ClearNull(aplRST.Fields!dsn_id)
                .TextMatrix(.Rows - 1, SEL_DSN_NOM) = ClearNull(aplRST.Fields!dsn_nom)
                .TextMatrix(.Rows - 1, SEL_DSN_FECHAMODIF) = Format(ClearNull(aplRST.Fields!dsn_feult), "dd/mm/yyyy")
                aplRST.MoveNext
                DoEvents
            Loop
        End With
    End If
    grdSeleccion.Visible = True
End Sub

Private Sub grdDatos_SelChange()
    Call grdDatos_Click
End Sub

Private Sub grdSeleccion_Click()
    With grdSeleccion
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, SEL_DSN_NOM) <> "" Then
                Call setHabilCtrl(cmdContinuar, "NOR")
            End If
        End If
    End With
End Sub

Private Sub txtBuscarNombreProd_Change()
    If Len(txtBuscarNombreProd) > 0 Then
        Call setHabilCtrl(cmdBuscar, "NOR")
    Else
        Call setHabilCtrl(cmdBuscar, "DIS")
    End If
End Sub

Private Sub grdDatos_Click()
    Call setHabilCtrl(cmdContinuar, "DIS")
    With grdDatos
        If .RowSel > 0 Then
            If .TextMatrix(.RowSel, COL_DSN_NOMPRODUCTIVO) <> "" Then
                Call CargarGrillaSeleccion
            End If
        End If
    End With
End Sub

Private Sub cmdCancelar_Click()
    sModo = "CANCEL"
    Unload Me
End Sub

Private Sub optSeleccion_Click(Index As Integer)
    Select Case Index
        Case 0
            sModo = "SEL"
            Call grdDatos_Click
        Case 1
            sModo = "ADD"
            Call setHabilCtrl(cmdContinuar, "NOR")
    End Select
End Sub

Private Sub cmdContinuar_Click()
    With grdSeleccion
        If .TextMatrix(.RowSel, SEL_DSN_ID) <> "" Then
            sDSN_ID = .TextMatrix(.RowSel, SEL_DSN_ID)
            Unload Me
        Else
            MsgBox "No ha seleccionado ning�n DSN.", vbExclamation + vbOKOnly
        End If
    End With
End Sub
