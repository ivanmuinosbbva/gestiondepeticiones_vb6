VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSSDDSolicitud 
   Caption         =   "Trabajar con solicitudes para SSDD"
   ClientHeight    =   7995
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11865
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7995
   ScaleWidth      =   11865
   Begin VB.Frame fraDatos 
      Caption         =   "fraDatos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3375
      Left            =   60
      TabIndex        =   2
      Top             =   4560
      Width           =   10695
      Begin VB.CommandButton cmdSeleccionPeticiones 
         Height          =   315
         Left            =   8700
         Picture         =   "frmSSDDSolicitud.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   1800
         Width           =   315
      End
      Begin VB.ComboBox cboDataBase 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   29
         Top             =   2160
         Width           =   3375
      End
      Begin MSComCtl2.DTPicker dtpFechaAlta 
         Height          =   315
         Left            =   7680
         TabIndex        =   27
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         Format          =   63176705
         CurrentDate     =   42578
      End
      Begin VB.ComboBox cboEmergencia 
         Height          =   315
         Left            =   7680
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   720
         Width           =   855
      End
      Begin VB.ComboBox cboDiferido 
         Height          =   315
         Left            =   7680
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   1080
         Width           =   855
      End
      Begin VB.ComboBox cboEstado 
         Height          =   315
         Left            =   7680
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   1440
         Width           =   2835
      End
      Begin VB.TextBox txtArchivoDeSalida 
         Height          =   315
         Left            =   1800
         TabIndex        =   21
         Top             =   2880
         Width           =   4395
      End
      Begin VB.ComboBox cboTablaId 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   2520
         Width           =   3375
      End
      Begin VB.ComboBox cboDBMSId 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1800
         Width           =   3375
      End
      Begin VB.TextBox txtPetNroAsignado 
         Height          =   315
         Left            =   7680
         TabIndex        =   18
         Top             =   1800
         Width           =   1035
      End
      Begin VB.TextBox txtAmpliacion 
         Height          =   315
         Left            =   1800
         TabIndex        =   17
         Top             =   1440
         Width           =   4395
      End
      Begin VB.ComboBox cboJustificativo 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1080
         Width           =   4395
      End
      Begin VB.ComboBox cboEnmascaramiento 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   720
         Width           =   855
      End
      Begin VB.TextBox txtSolNroAsignado 
         Height          =   315
         Left            =   1800
         TabIndex        =   4
         Top             =   360
         Width           =   1155
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Base de datos"
         Height          =   195
         Index           =   12
         Left            =   180
         TabIndex        =   28
         Top             =   2220
         Width           =   1020
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Fecha alta"
         Height          =   195
         Index           =   11
         Left            =   6720
         TabIndex        =   26
         Top             =   420
         Width           =   750
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Estado"
         Height          =   195
         Index           =   10
         Left            =   6720
         TabIndex        =   15
         Top             =   1500
         Width           =   495
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Diferido"
         Height          =   195
         Index           =   9
         Left            =   6720
         TabIndex        =   14
         Top             =   1140
         Width           =   555
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Emergencia"
         Height          =   195
         Index           =   8
         Left            =   6720
         TabIndex        =   13
         Top             =   780
         Width           =   825
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Archivo de salida"
         Height          =   195
         Index           =   7
         Left            =   180
         TabIndex        =   12
         ToolTipText     =   "S�lo nombre del archivo de salida"
         Top             =   2940
         Width           =   1215
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Tabla"
         Height          =   195
         Index           =   6
         Left            =   180
         TabIndex        =   11
         Top             =   2580
         Width           =   390
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "DBMS"
         Height          =   195
         Index           =   5
         Left            =   180
         TabIndex        =   10
         Top             =   1860
         Width           =   405
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Petici�n"
         Height          =   195
         Index           =   4
         Left            =   6720
         TabIndex        =   9
         Top             =   1860
         Width           =   555
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Ampliaci�n de detalle"
         Height          =   195
         Index           =   3
         Left            =   180
         TabIndex        =   8
         Top             =   1500
         Width           =   1500
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Justificativo"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   7
         Top             =   1140
         Width           =   855
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Enmascaramiento"
         Height          =   195
         Index           =   1
         Left            =   180
         TabIndex        =   5
         Top             =   780
         Width           =   1260
      End
      Begin VB.Label lblDatos 
         AutoSize        =   -1  'True
         Caption         =   "Nro. solicitud"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   3
         Top             =   420
         Width           =   930
      End
   End
   Begin VB.Frame fraBotonera 
      Height          =   3375
      Left            =   10800
      TabIndex        =   1
      Top             =   4560
      Width           =   1035
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "Cerrar"
         Height          =   375
         Left            =   60
         TabIndex        =   22
         Top             =   2940
         Width           =   885
      End
      Begin VB.CommandButton cmdConfirmar 
         Caption         =   "Confirmar"
         Height          =   375
         Left            =   60
         TabIndex        =   33
         Top             =   2580
         Width           =   885
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Borrar"
         Height          =   375
         Left            =   60
         TabIndex        =   32
         Top             =   900
         Width           =   885
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "Editar"
         Height          =   375
         Left            =   60
         TabIndex        =   31
         Top             =   540
         Width           =   885
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   60
         TabIndex        =   30
         Top             =   180
         Width           =   885
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdDatos 
      Height          =   4485
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   7911
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      BackColor       =   16777215
      BackColorSel    =   12682268
      ForeColorSel    =   16777215
      GridColor       =   14737632
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLinesFixed  =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSSDDSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const colSOLI_NRO = 0
Private Const colSOLI_MSK = 1
Private Const colSOLI_FECHA = 2
Private Const colSOLI_CODRECURSO = 3
Private Const colSOLI_NOMRECURSO = 4
Private Const colSOLI_PETNROINT = 5
Private Const colSOLI_PETNROASI = 6
Private Const colSOLI_DBMS = 7
Private Const colSOLI_DBMSNOM = 8
Private Const colSOLI_DDBBNOM = 9
Private Const colSOLI_TABLAID = 10
Private Const colSOLI_TABLANOM = 11
Private Const colSOLI_ARCHIVO = 12
Private Const colSOLI_EME = 13
Private Const colSOLI_DIF = 14
Private Const colSOLI_ESTADO = 15
Private Const colSOLI_ESTADONOM = 16
Private Const colSOLI_FCHPROC = 17
Private Const colSOLI_USRPROC = 18
Private Const colSOLI_FCHAPROB = 19
Private Const colSOLI_USRAPROB = 20
Private Const colSOLI_JUSTIFCOD = 21
Private Const colSOLI_JUSTIFTXT = 22
Private Const colSOLI_JUSTIFAMP = 23
Private Const colSOLI_TOTCOLS = 24

Dim sOpcionSeleccionada As String
Dim bEnCarga As Boolean

Private Sub Form_Load()
    bEnCarga = True
    Call InicializarCombos
    Call HabilitarBotones(0)
    bEnCarga = False
End Sub

Private Sub InicializarCombos()
    With cboEnmascaramiento
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 0
    End With
    With cboJustificativo
        .Clear
        If sp_GetJustificativos(Null, Null) Then     ' Controles con datos
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!jus_descripcion) & ESPACIOS & "||" & ClearNull(aplRST.Fields!jus_codigo)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End With
    With cboDBMSId
        .Clear
        If sp_GetDBMS(Null) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!dbmsId) & ". " & ClearNull(aplRST.Fields!dbmsNom) & ESPACIOS & "||" & ClearNull(aplRST.Fields!dbmsId)
                aplRST.MoveNext
                DoEvents
            Loop
            .ListIndex = 0
        End If
    End With
    'Call setHabilCtrl(cboDataBase, DISABLE)
    With cboDataBase
        .Clear
        If cboDBMSId.ListIndex > -1 Then
            'If sp_GetCatalogoTabla(Null, Null, Null, CodigoCombo(cboDBMSId, True)) Then
            If sp_GetAllDataBases(CodigoCombo(cboDBMSId, True)) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!baseDeDatos)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
        End If
    End With
    With cboTablaId
        .Clear
        If sp_GetCatalogoTabla(Null, Null, Null, CodigoCombo(cboDBMSId, True)) Then
            Do While Not aplRST.EOF
                .AddItem ClearNull(aplRST.Fields!nombreTabla) & ESPACIOS & "||" & ClearNull(aplRST.Fields!id)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        Call setHabilCtrl(cboTablaId, DISABLE)
    End With
    With cboEmergencia
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 1
    End With
    With cboDiferido
        .Clear
        .AddItem "Si" & ESPACIOS & "||S"
        .AddItem "No" & ESPACIOS & "||N"
        .ListIndex = 1
    End With
    With cboEstado
        .Clear
        .AddItem "Pendiente de proceso" & ESPACIOS & "||P"
        .AddItem "Pendiente de autorizar" & ESPACIOS & "||B"
        .AddItem "Rechazado" & ESPACIOS & "||X"
        .AddItem "En proceso" & ESPACIOS & "||E"
        .AddItem "Finalizado OK" & ESPACIOS & "||K"
        .AddItem "Finalizado con error" & ESPACIOS & "||W"
        .ListIndex = -1
    End With
End Sub

Private Sub InicializarGrilla()
    With grdDatos
        .Clear
        .visible = False
        .cols = colSOLI_TOTCOLS
        .Rows = 1
        .TextMatrix(0, colSOLI_NRO) = "Nro.": .ColWidth(colSOLI_NRO) = 800: .ColAlignment(colSOLI_NRO) = flexAlignRightCenter
        .TextMatrix(0, colSOLI_MSK) = "ENM.": .ColWidth(colSOLI_MSK) = 600: .ColAlignment(colSOLI_MSK) = 0
        .TextMatrix(0, colSOLI_FECHA) = "Fecha": .ColWidth(colSOLI_FECHA) = 1400: .ColAlignment(colSOLI_FECHA) = 0
        .TextMatrix(0, colSOLI_CODRECURSO) = "Solicitante": .ColWidth(colSOLI_CODRECURSO) = 1000: .ColAlignment(colSOLI_CODRECURSO) = 0
        .TextMatrix(0, colSOLI_NOMRECURSO) = "Nombre y apellido": .ColWidth(colSOLI_NOMRECURSO) = 0: .ColAlignment(colSOLI_NOMRECURSO) = 0
        .TextMatrix(0, colSOLI_JUSTIFCOD) = "Cod.": .ColWidth(colSOLI_JUSTIFCOD) = 0: .ColAlignment(colSOLI_JUSTIFCOD) = 0
        .TextMatrix(0, colSOLI_JUSTIFTXT) = "Justificativo": .ColWidth(colSOLI_JUSTIFTXT) = 4000: .ColAlignment(colSOLI_JUSTIFTXT) = 0
        .TextMatrix(0, colSOLI_JUSTIFAMP) = "Ampliaci�n": .ColWidth(colSOLI_JUSTIFAMP) = 0: .ColAlignment(colSOLI_JUSTIFAMP) = 0
        .TextMatrix(0, colSOLI_PETNROINT) = "": .ColWidth(colSOLI_PETNROINT) = 0: .ColAlignment(colSOLI_PETNROINT) = 0
        .TextMatrix(0, colSOLI_PETNROASI) = "Petici�n": .ColWidth(colSOLI_PETNROASI) = 900: .ColAlignment(colSOLI_PETNROASI) = 0
        .TextMatrix(0, colSOLI_DBMS) = "dbms": .ColWidth(colSOLI_DBMS) = 0: .ColAlignment(colSOLI_DBMS) = 0
        .TextMatrix(0, colSOLI_DBMSNOM) = "DBMS": .ColWidth(colSOLI_DBMSNOM) = 1000: .ColAlignment(colSOLI_DBMSNOM) = 0
        .TextMatrix(0, colSOLI_DDBBNOM) = "DDBB": .ColWidth(colSOLI_DDBBNOM) = 1000: .ColAlignment(colSOLI_DDBBNOM) = 0
        .TextMatrix(0, colSOLI_TABLAID) = "": .ColWidth(colSOLI_TABLAID) = 0: .ColAlignment(colSOLI_TABLAID) = 0
        .TextMatrix(0, colSOLI_TABLANOM) = "Tabla": .ColWidth(colSOLI_TABLANOM) = 2000: .ColAlignment(colSOLI_TABLANOM) = 0
        .TextMatrix(0, colSOLI_ARCHIVO) = "Archivo de salida": .ColWidth(colSOLI_ARCHIVO) = 2000: .ColAlignment(colSOLI_ARCHIVO) = 0
        .TextMatrix(0, colSOLI_EME) = "EME.": .ColWidth(colSOLI_EME) = 600: .ColAlignment(colSOLI_EME) = 0
        .TextMatrix(0, colSOLI_DIF) = "Dif.": .ColWidth(colSOLI_DIF) = 600: .ColAlignment(colSOLI_DIF) = 0
        .TextMatrix(0, colSOLI_ESTADO) = "Estado": .ColWidth(colSOLI_ESTADO) = 0: .ColAlignment(colSOLI_ESTADO) = 0
        .TextMatrix(0, colSOLI_ESTADONOM) = "Estado": .ColWidth(colSOLI_ESTADONOM) = 2000: .ColAlignment(colSOLI_ESTADONOM) = 0
        .TextMatrix(0, colSOLI_FCHAPROB) = "Aprobado el": .ColWidth(colSOLI_FCHAPROB) = 0: .ColAlignment(colSOLI_FCHAPROB) = 0
        .TextMatrix(0, colSOLI_USRAPROB) = "Por": .ColWidth(colSOLI_USRAPROB) = 0: .ColAlignment(colSOLI_USRAPROB) = 0
        .TextMatrix(0, colSOLI_FCHPROC) = "Procesado el": .ColWidth(colSOLI_FCHPROC) = 1400: .ColAlignment(colSOLI_FCHPROC) = 0
        .TextMatrix(0, colSOLI_USRPROC) = "Por": .ColWidth(colSOLI_USRPROC) = 2000: .ColAlignment(colSOLI_USRPROC) = 0
        Call CambiarEfectoLinea(grdDatos, prmGridEffectFontBold)
    End With
End Sub

Private Sub CargarGrilla()
    Call InicializarGrilla
    With grdDatos
        If sp_GetSolicitudesSSDD(Null, glLOGIN_ID_REEMPLAZO, Null) Then
            Do While Not aplRST.EOF
                .Rows = .Rows + 1
                .TextMatrix(.Rows - 1, colSOLI_NRO) = ClearNull(aplRST.Fields!sol_nroasignado)
                .TextMatrix(.Rows - 1, colSOLI_MSK) = ClearNull(aplRST.Fields!sol_mask)
                .TextMatrix(.Rows - 1, colSOLI_FECHA) = ClearNull(aplRST.Fields!SOL_FECHA)
                .TextMatrix(.Rows - 1, colSOLI_CODRECURSO) = ClearNull(aplRST.Fields!sol_recurso)
                .TextMatrix(.Rows - 1, colSOLI_NOMRECURSO) = ClearNull(aplRST.Fields!nom_recurso)
                .TextMatrix(.Rows - 1, colSOLI_JUSTIFCOD) = ClearNull(aplRST.Fields!jus_codigo)
                .TextMatrix(.Rows - 1, colSOLI_JUSTIFTXT) = ClearNull(aplRST.Fields!jus_descripcion)
                .TextMatrix(.Rows - 1, colSOLI_JUSTIFAMP) = ClearNull(aplRST.Fields!sol_texto)
                .TextMatrix(.Rows - 1, colSOLI_PETNROINT) = ClearNull(aplRST.Fields!pet_nrointerno)
                .TextMatrix(.Rows - 1, colSOLI_PETNROASI) = ClearNull(aplRST.Fields!pet_nroasignado)
                .TextMatrix(.Rows - 1, colSOLI_DBMS) = ClearNull(aplRST.Fields!DBMS)
                .TextMatrix(.Rows - 1, colSOLI_DBMSNOM) = ClearNull(aplRST.Fields!dbmsNom)
                .TextMatrix(.Rows - 1, colSOLI_DDBBNOM) = ClearNull(aplRST.Fields!baseDeDatos)
                .TextMatrix(.Rows - 1, colSOLI_TABLAID) = ClearNull(aplRST.Fields!tablaId)
                .TextMatrix(.Rows - 1, colSOLI_TABLANOM) = ClearNull(aplRST.Fields!nombreTabla)
                .TextMatrix(.Rows - 1, colSOLI_ARCHIVO) = ClearNull(aplRST.Fields!sol_archivo)
                .TextMatrix(.Rows - 1, colSOLI_EME) = ClearNull(aplRST.Fields!sol_eme)
                .TextMatrix(.Rows - 1, colSOLI_DIF) = ClearNull(aplRST.Fields!sol_diferida)
                .TextMatrix(.Rows - 1, colSOLI_ESTADO) = ClearNull(aplRST.Fields!sol_estado)
                .TextMatrix(.Rows - 1, colSOLI_ESTADONOM) = ClearNull(aplRST.Fields!nom_estado)
                .TextMatrix(.Rows - 1, colSOLI_FCHAPROB) = ClearNull(aplRST.Fields!fechaAprobacion)
                .TextMatrix(.Rows - 1, colSOLI_USRAPROB) = ClearNull(aplRST.Fields!aprobador)
                .TextMatrix(.Rows - 1, colSOLI_FCHPROC) = ClearNull(aplRST.Fields!sol_trnm_fe)
                .TextMatrix(.Rows - 1, colSOLI_USRPROC) = ClearNull(aplRST.Fields!sol_trnm_usr)
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        .visible = True
        .row = 0: .col = .cols - 1
        If .Rows > 1 Then
            .row = 1: .col = .cols - 1
            Call MostrarSeleccion
        End If
    End With
End Sub

'Private Sub cboDBMSId_Click()
'    If bEnCarga Then Exit Sub
'    Call CargarCatalogos(False)
'End Sub

Private Sub cboDataBase_Click()
    If bEnCarga Then Exit Sub
    Call CargarTablas(False)
End Sub

Private Sub CargarCatalogos(todos As Boolean)
    With cboTablaId
        .Clear
        If cboDBMSId.ListIndex > -1 Then
            If sp_GetCatalogoTabla(Null, Null, Null, IIf(todos, CodigoCombo(cboDBMSId, True), Null)) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!nombreTabla) & ESPACIOS & "||" & ClearNull(aplRST.Fields!id)
                    aplRST.MoveNext
                    DoEvents
                Loop
                If InStr(1, "M|A|", sOpcionSeleccionada, vbTextCompare) > 0 Then
                    .ListIndex = PosicionCombo(cboTablaId, grdDatos.TextMatrix(grdDatos.rowSel, colSOLI_TABLAID), True)
                Else
                    .ListIndex = 0
                End If
            End If
        End If
    End With
End Sub

Private Sub CargarTablas(todas As Boolean)
    With cboTablaId
        .Clear
        If todas Then
            If sp_GetCatalogoTabla(Null, Null, Null, CodigoCombo(cboDBMSId, True)) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!nombreTabla) & ESPACIOS & "||" & ClearNull(aplRST.Fields!id)
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            Exit Sub
        End If
        
        If cboDataBase.ListIndex > -1 Then
            If sp_GetCatalogoTabla(Null, CodigoCombo(cboDataBase), Null, IIf(todas, CodigoCombo(cboDBMSId, True), Null)) Then
                Do While Not aplRST.EOF
                    .AddItem ClearNull(aplRST.Fields!nombreTabla) & ESPACIOS & "||" & ClearNull(aplRST.Fields!id)
                    aplRST.MoveNext
                    DoEvents
                Loop
                .ListIndex = PosicionCombo(cboTablaId, grdDatos.TextMatrix(grdDatos.rowSel, colSOLI_TABLAID), True)
                If sOpcionSeleccionada <> "" And InStr(1, "A|M|", sOpcionSeleccionada, vbTextCompare) > 0 Then
                    Call setHabilCtrl(cboTablaId, NORMAL)
                Else
                    Call setHabilCtrl(cboTablaId, DISABLE)
                End If
            End If
        End If
    End With
End Sub

Private Sub MostrarSeleccion()
    Call setHabilCtrl(cmdModificar, NORMAL)
    Call setHabilCtrl(cmdEliminar, NORMAL)
    With grdDatos
        If .rowSel > 0 Then
            txtSolNroAsignado = .TextMatrix(.rowSel, colSOLI_NRO)
            cboEnmascaramiento.ListIndex = PosicionCombo(cboEnmascaramiento, .TextMatrix(.rowSel, colSOLI_MSK), True)
            cboJustificativo.ListIndex = PosicionCombo(cboJustificativo, .TextMatrix(.rowSel, colSOLI_JUSTIFCOD), True)
            dtpFechaAlta.value = .TextMatrix(.rowSel, colSOLI_FECHA)
            txtAmpliacion.text = .TextMatrix(.rowSel, colSOLI_JUSTIFAMP)
            txtPetNroAsignado.text = .TextMatrix(.rowSel, colSOLI_PETNROASI)
            cboDBMSId.ListIndex = PosicionCombo(cboDBMSId, .TextMatrix(.rowSel, colSOLI_DBMS), True)
            cboTablaId.ListIndex = PosicionCombo(cboTablaId, .TextMatrix(.rowSel, colSOLI_TABLAID), True)
            cboDataBase.ListIndex = PosicionCombo(cboDataBase, .TextMatrix(.rowSel, colSOLI_DDBBNOM))
            txtArchivoDeSalida.text = .TextMatrix(.rowSel, colSOLI_ARCHIVO)
            cboEmergencia.ListIndex = PosicionCombo(cboEmergencia, .TextMatrix(.rowSel, colSOLI_EME), True)
            cboDiferido.ListIndex = PosicionCombo(cboDiferido, .TextMatrix(.rowSel, colSOLI_DIF), True)
            cboEstado.ListIndex = PosicionCombo(cboEstado, .TextMatrix(.rowSel, colSOLI_ESTADO), True)
            If InStr(1, "X|E|K|W|", CodigoCombo(cboEstado, True), vbTextCompare) > 0 Then
                Call setHabilCtrl(cmdModificar, DISABLE)
                Call setHabilCtrl(cmdEliminar, DISABLE)
            End If
        End If
    End With
End Sub

Private Sub Form_Resize()
    Me.Top = 0
    Me.Left = 0
    If Me.WindowState <> vbMinimized Then
        Me.Height = 8505
        Me.Width = 11985
    End If
End Sub

Private Sub grdDatos_Click()
    bEnCarga = True
    Call MostrarSeleccion
    bEnCarga = False
End Sub

Private Sub HabilitarBotones(nOpcion As Integer, Optional vCargaLista As Variant)
    Call setHabilCtrl(txtSolNroAsignado, DISABLE)
    Call setHabilCtrl(cboEnmascaramiento, DISABLE)
    Call setHabilCtrl(cboDataBase, DISABLE)
    Call setHabilCtrl(cboJustificativo, DISABLE)
    Call setHabilCtrl(txtAmpliacion, DISABLE)
    Call setHabilCtrl(cboDBMSId, DISABLE)
    Call setHabilCtrl(cboTablaId, DISABLE)
    Call setHabilCtrl(cboDataBase, DISABLE)
    Call setHabilCtrl(txtArchivoDeSalida, DISABLE)
    Call setHabilCtrl(dtpFechaAlta, DISABLE)
    'Call setHabilCtrl(txtFechaVisado, DISABLE)
    Call setHabilCtrl(cboEmergencia, DISABLE)
    Call setHabilCtrl(cboDiferido, DISABLE)
    Call setHabilCtrl(cboEstado, DISABLE)
    Call setHabilCtrl(txtPetNroAsignado, DISABLE): Call setHabilCtrl(cmdSeleccionPeticiones, DISABLE)
    Select Case nOpcion
        Case 0
            fraDatos.Caption = ""
            fraDatos.Enabled = False
            'lswDatos.Enabled = True
            cmdAgregar.Enabled = True
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
            cmdConfirmar.Enabled = False
            cmdCerrar.Caption = "Cerrar"
            cmdCerrar.ToolTipText = "Salir de esta pantalla"
            cmdCerrar.Enabled = True
            'Call setHabilCtrl(cmdCampos, NORMAL)
            sOpcionSeleccionada = ""
            If IsMissing(vCargaLista) Then
                Call CargarGrilla
            Else
                'Call MostrarSeleccion
                'Call CargarCatalogos(True)
                'bEnCarga = True
                Call MostrarSeleccion
                'bEnCarga = False
            End If
        Case 1
            'lswDatos.Enabled = False
            fraDatos.Enabled = True
            cmdAgregar.Enabled = False
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
            cmdConfirmar.Enabled = True
            cmdCerrar.Enabled = True
            cmdCerrar.Caption = "Cancelar"
            cmdCerrar.ToolTipText = "Cancelar esta operaci�n"
            Select Case sOpcionSeleccionada
                Case "A"
                    fraDatos.Caption = " AGREGAR "
                    txtSolNroAsignado = "": txtAmpliacion = "": txtArchivoDeSalida = "": txtPetNroAsignado = "": dtpFechaAlta.value = Now
                    Call setHabilCtrl(cmdSeleccionPeticiones, NORMAL)
                    cboEnmascaramiento.ListIndex = 0
                    cboJustificativo.ListIndex = -1
                    'cboDBMSId.ListIndex = -1
                    cboDBMSId.ListIndex = 0             ' Por ahora, el �nico DBMS disponible es Sybase
                    cboDataBase.ListIndex = -1
                    cboTablaId.ListIndex = -1
                    cboEmergencia.ListIndex = 1
                    cboDiferido.ListIndex = 1
                    cboEstado.ListIndex = -1
                    'dtpFechaAlta.Value = ""
                    Call setHabilCtrl(txtAmpliacion, NORMAL)
                    Call setHabilCtrl(txtArchivoDeSalida, NORMAL)
                    Call setHabilCtrl(txtPetNroAsignado, NORMAL): Call setHabilCtrl(cmdSeleccionPeticiones, NORMAL)
                    Call setHabilCtrl(cboEnmascaramiento, NORMAL)
                    Call setHabilCtrl(cboJustificativo, NORMAL)
                    'Call setHabilCtrl(cboDBMSId, NORMAL)
                    Call setHabilCtrl(cboDBMSId, DISABLE)       ' Por ahora, el �nico DBMS disponible es Sybase
                    'Call setHabilCtrl(cboTablaId, NORMAL)
                    Call setHabilCtrl(cboDataBase, NORMAL)
                    'Call setHabilCtrl(cboEmergencia, NORMAL)
                    Call setHabilCtrl(cboDiferido, DISABLE)
                    'Call setHabilCtrl(cboEstado, DISABLE)
                    'Call setHabilCtrl(dtpFechaAlta, DISABLE)
                    fraDatos.Enabled = True
                    cboEnmascaramiento.SetFocus
                Case "M"
                    fraDatos.Caption = " MODIFICAR "
                    fraDatos.Enabled = True
                    Call setHabilCtrl(txtAmpliacion, NORMAL)
                    Call setHabilCtrl(txtArchivoDeSalida, NORMAL)
                    Call setHabilCtrl(txtPetNroAsignado, NORMAL): Call setHabilCtrl(cmdSeleccionPeticiones, NORMAL)
                    Call setHabilCtrl(cboEnmascaramiento, NORMAL)
                    Call setHabilCtrl(cboJustificativo, NORMAL)
                    'Call setHabilCtrl(cboDBMSId, NORMAL)
                    Call CargarTablas(False)
                    Call SetCombo(cboTablaId, grdDatos.TextMatrix(grdDatos.rowSel, colSOLI_TABLAID), True)
                    Call setHabilCtrl(cboTablaId, NORMAL)
                    'Call setHabilCtrl(cboDataBase, NORMAL)
                    'Call setHabilCtrl(cboEmergencia, NORMAL)
                    'Call setHabilCtrl(cboDiferido, DISABLE)
                    'Call setHabilCtrl(cboEstado, DISABLE)
                    'Call setHabilCtrl(dtpFechaAlta, DISABLE)
                Case "E"
                    fraDatos.Caption = " ELIMINAR "
                    fraDatos.Enabled = False
            End Select
    End Select
    'Call LockProceso(False)
End Sub

Private Sub cmdAgregar_Click()
    sOpcionSeleccionada = "A"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdModificar_Click()
    sOpcionSeleccionada = "M"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdEliminar_Click()
    sOpcionSeleccionada = "E"
    Call HabilitarBotones(1, False)
End Sub

Private Sub cmdCerrar_Click()
    Select Case sOpcionSeleccionada
        Case "A", "M", "E"
            sOpcionSeleccionada = ""
            Call HabilitarBotones(0, False)
        Case Else
            Unload Me
    End Select
End Sub

Private Sub cmdConfirmar_Click()
    Select Case sOpcionSeleccionada
        Case "A"
            If CamposObligatorios Then
                If sp_InsertSolicitudesSSDD(glLOGIN_ID_REEMPLAZO, CodigoCombo(cboEnmascaramiento, True), CodigoCombo(cboJustificativo, True), txtAmpliacion, txtPetNroAsignado, CodigoCombo(cboTablaId, True), txtArchivoDeSalida, CodigoCombo(cboEmergencia, True), CodigoCombo(cboDiferido, True)) Then
                    Call HabilitarBotones(0)
                End If
            End If
        Case "M"
            If CamposObligatorios Then
                Call sp_UpdateSolicitudesSSDD(txtSolNroAsignado, CodigoCombo(cboEnmascaramiento, True), CodigoCombo(cboJustificativo, True), txtAmpliacion, txtPetNroAsignado, Val(txtPetNroAsignado.Tag), CodigoCombo(cboTablaId, True), txtArchivoDeSalida, CodigoCombo(cboEmergencia, True), CodigoCombo(cboDiferido, True))
                Call HabilitarBotones(0)
            End If
        Case "E"
            If sp_DeleteSolicitudesSSDD(txtSolNroAsignado) Then
                Call HabilitarBotones(0)
            End If
    End Select
End Sub

Private Function CamposObligatorios() As Boolean
    CamposObligatorios = True
End Function

Private Sub cmdSeleccionPeticiones_Click()
    ' Busca y selecciona una petici�n v�lida
    Dim vRetorno() As String
    Dim nPos As Integer
    
    glAuxRetorno = ""
    
    Select Case glUsrPerfilActual
        Case "CSEC": frmSelPet.Show 1
        Case Else
            frmSelPetGrp.Show 1
    End Select
    DoEvents
    With txtPetNroAsignado
        If glAuxRetorno <> "" Then
            If ParseString(vRetorno, glAuxRetorno, "|") > 0 Then
                .text = Mid(vRetorno(1), 1, InStr(1, vRetorno(1), ":", vbTextCompare) - 1)
                .Tag = vRetorno(3)
                If sp_GetUnaPeticion(vRetorno(3)) Then
                    .Tag = ClearNull(aplRST.Fields!pet_nrointerno)
                    .ToolTipText = ClearNull(aplRST.Fields!Titulo)
                Else
                    MsgBox "La petici�n ingresada no existe o no es v�lida.", vbExclamation + vbOKOnly
                    .SetFocus
                End If
            End If
        End If
    End With
End Sub
