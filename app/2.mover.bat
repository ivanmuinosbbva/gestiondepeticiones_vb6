@ECHO OFF
ECHO *** Actualizando el repositorio de TEST ***

REM IF EXIST pet.exe GOTO borrar_pet ELSE GOTO preguntar_cfg
REM :borrar_pet
ECHO Actualizando: pet.exe...
REM IF EXIST \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\pet.exe DEL \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\pet.exe
MOVE /Y pet.exe \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\pet.exe

REM :preguntar_cfg
REM IF EXIST petcfg.exe GOTO borrar_cfg ELSE GOTO preguntar_test
REM :borrar_cfg
ECHO Actualizando: petcfg.exe...
REM IF EXIST \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\petcfg.exe DEL \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\petcfg.exe
MOVE /Y petcfg.exe \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\petcfg.exe

REM :preguntar_test
REM IF EXIST petest.exe GOTO borrar_test ELSE GOTO preguntar_versiones
REM :borrar_test
ECHO Actualizando: petest.exe...
REM IF EXIST \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\petest.exe DEL \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\petest.exe
MOVE /Y petest.exe \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\petest.exe

REM :preguntar_versiones
ECHO Actualizando: versiones.rtf...
REM IF EXIST \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\versiones.rtf DEL \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\versiones.rtf
COPY /Y versiones.rtf \\bbvdfs\dfs\fsdes1_e\bin\Aplic_Depart\CGM\Testing\versiones.rtf

REM :fin
ECHO *** Repositorio actualizado. ***
PAUSE
