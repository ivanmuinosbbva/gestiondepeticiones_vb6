#!/bin/sh

dirhome=/prod/jp00/GesPet
PROC=ParcerAWK
FECHA_ARCH=$1$2
log=/prod/jp00/logs/GesPet.$PROC.$1$2
armsg=$dirhome/Mensaje.txt
#variable con el nombre del archivo generado por el BCP OUT
ARCHIVO_BCP=SOLOUT.txt
LOGDOC=$dirhome/LOGAWK
passftp=`grep HOSTPROD /prod/jp00/general/passw|cut -c12-22`
cd $dirhome


if [ ! -f "$ARCHIVO_BCP" ]
then
  echo "El archivo \"$ARCHIVO_BCP\" no existe."
  echo "El archivo \"$ARCHIVO_BCP\" no existe." > $log.VACIO 
  exit 66
fi
#
cp $ARCHIVO_BCP old/$ARCHIVO_BCP.$FECHA_ARCH
#
WCA=`wc $ARCHIVO_BCP|cut -c3-8`
echo "---------------------- " > $log
echo "Comienza proceso $PROC DIA :$1 HORA $2" >> $log
echo "ARCHIVO : $ARCHIVO_BCP $WCA REGISTROS"  >> $log


#genero la fecha y hora para el fin de cada registro
FECHA=`date +%y%m%d`

#genero el nombre para cada archivo de salida
SALIDASORT="SYS6.BC.SORT.PEDIDOS"
SALIDATCOR="SYS6.BC.TCOR.PEDIDOS"
SALIDAUNLO="SYS6.BC.UNLO.PEDIDOS"
SALIDAXCOM="SYS6.BC.XCOM.PEDIDOS"
SXCOM=ARBP.BC.XCOM.PEDIDOS.CGM
STCOR=ARBP.BC.TCOR.PEDIDOS.CGM
SUNLO=ARBP.BC.UNLO.PEDIDOS.CGM
SSORT=ARBP.BC.SORT.PEDIDOS.CGM
#
rm -f SYS6.BC.*PEDIDOS
rm -f $armsg
#
#XCOM
ORIGEN=$SALIDAXCOM
DESTINO=$SXCOM
MARCA1=PRHP-OK-CGMXCOM
MARCA2=PARCERAWK-OK-GP-XCOM
awk 'BEGIN { FS = "\t" } { if ($2=="1") printf "%-s%s\n%-s%s\n%-s%s\n%-s%s\n%-s\n%-s\n","- ARCHIVO IN  = ",$17,"- ARCHIVO OUT = ", $18,"- SOLICITANTE = ",$5,"- SUPERVISOR  = ",$11, $24, "+----------------------------" }' $ARCHIVO_BCP > $ORIGEN

if [ ! -s "$ORIGEN" ]
then
  ls -l $ORIGEN
  echo "El archivo \"$ORIGEN\" no existe. o ESTA VACIO"
  touch old/$ORIGEN.$FECHA_ARCH
  else 
  echo "Se realiza FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" "
  echo "Se realizara  FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" " >> $armsg
  echo "ARCHIVO $ORIGEN :"  >> $armsg
  cat $ORIGEN >> $armsg
  echo "----------------------------------------------------------------------------" >> $armsg
  WCA=`wc $ORIGEN|cut -c3-8`
  echo "ARCHIVO : $ORIGEN $WCA REGISTROS"  >> $log
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
fi

#UNLO
ORIGEN=$SALIDAUNLO
DESTINO=$SUNLO
MARCA1=PRHP-OK-CGMUNLO
MARCA2=PARCERAWK-OK-GP-UNLO
awk 'BEGIN { FS = "\t" } { if ($2=="2") printf "%-s%s\n%-s%s\n%-s%s\n%-s%s\n%-s%s\n%-s%s\n%s\n%s\n","- ARCHIVO OUT = ", $18,"- LIB SYSIN   = ",$20,"- SYSIN       = ",$21,"- JOIN        = ",$22, "- SOLICITANTE = ",$5,"- SUPERVISOR  = ",$11, $24,"+----------------------------" }' $ARCHIVO_BCP > $ORIGEN

if [ ! -s "$ORIGEN" ]
then
  ls -l $ORIGEN
  echo "El archivo \"$ORIGEN\" no existe. o ESTA VACIO"
  touch old/$ORIGEN.$FECHA_ARCH
  else 
  echo "Se realiza FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" "
  echo "Se realizara  FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" " >> $armsg
  echo "ARCHIVO $ORIGEN :"  >> $armsg
  cat $ORIGEN >> $armsg
  echo "----------------------------------------------------------------------------" >> $armsg
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  WCA=`wc $ORIGEN|cut -c3-8`
  echo "ARCHIVO : $ORIGEN $WCA REGISTROS"  >> $log
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
fi


#
#TCOR
ORIGEN=$SALIDATCOR
DESTINO=$STCOR
MARCA1=PRHP-OK-CGMTCOR
MARCA2=PARCERAWK-OK-GP-TCOR
awk 'BEGIN { FS = "\t" } { if ($2=="3") printf "%-s%s\n%-s%s\n%-s%s\n%-s%s\n%-s\n%-s\n","- INPUT SORT  = ",$23,"- ARCHIVO OUT = ", $18,"- SOLICITANTE = ",$5,"- SUPERVISOR  = ",$11, $24, "+----------------------------" }' $ARCHIVO_BCP > $ORIGEN

if [ ! -s "$ORIGEN" ]
then
  ls -l $ORIGEN
  echo "El archivo \"$ORIGEN\" no existe. o ESTA VACIO"
  touch old/$ORIGEN.$FECHA_ARCH
  else 
  echo "Se realiza FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" "
  echo "Se realizara  FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" " >> $armsg
  echo "ARCHIVO $ORIGEN :"  >> $armsg
  cat $ORIGEN >> $armsg
  echo "----------------------------------------------------------------------------" >> $armsg
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  WCA=`wc $ORIGEN|cut -c3-8`
  echo "ARCHIVO : $ORIGEN $WCA REGISTROS"  >> $log
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
fi


#
#SORT
ORIGEN=$SALIDASORT
DESTINO=$SSORT
MARCA1=PRHP-OK-CGMSORT
MARCA2=PARCERAWK-OK-GP-SORT
awk 'BEGIN { FS = "\t" } { if ($2=="4") printf "%-s%s\n%-s%s\n%-s%s\n%-s%s\n%-s%s\n%-s\n%-s\n","- ARCHIVO INP = ",$17,"- ARCHIVO OUT = ", $18,"- SYSIN       = ",$21,"- SOLICITANTE = ",$5,"- SUPERVISOR  = ",$11, $24, "+----------------------------" }' $ARCHIVO_BCP > $ORIGEN
#

if [ ! -s "$ORIGEN" ]
then
  ls -l $ORIGEN
  echo "El archivo \"$ORIGEN\" no existe. o ESTA VACIO"
  touch old/$ORIGEN.$FECHA_ARCH
  else 
  echo "Se realiza FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" "
  echo "Se realizara  FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" " >> $armsg
  echo "ARCHIVO $ORIGEN :"  >> $armsg
  cat $ORIGEN >> $armsg
  echo "----------------------------------------------------------------------------" >> $armsg
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  WCA=`wc $ORIGEN|cut -c3-8`
  echo "ARCHIVO : $ORIGEN $WCA REGISTROS"  >> $log
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
fi

#
cat $log >> $LOGDOC
#
if [ ! -s "$armsg" ]
then
  echo "El archivo $armsg  no existe. o ESTA VACIO"
  exit 0
  else 
  cat $armsg
  exit 0
fi
