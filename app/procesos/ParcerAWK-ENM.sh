#!/bin/sh

dirhome=/prod/jp00/GesPet
PROC=ParcerENM
FECHA_ARCH=$1$2
log=/prod/jp00/logs/GesPet.$PROC.$1$2
armsg=$dirhome/MensajeENM.txt
#variable con el nombre del archivo generado por el BCP OUT
ARCHIVO_BCP=hedthst.txt
LOGDOC=$dirhome/LOGAWK
passftp=`grep HOSTPROD /prod/jp00/general/passw|cut -c12-22`
cd $dirhome


if [ ! -f "$ARCHIVO_BCP" ]
then
  echo "El archivo \"$ARCHIVO_BCP\" no existe."
  echo "El archivo \"$ARCHIVO_BCP\" no existe." > $log.VACIO 
  exit 66
fi
#
cp $ARCHIVO_BCP old/$ARCHIVO_BCP.$FECHA_ARCH
#
WCA=`wc $ARCHIVO_BCP|cut -c3-8`
echo "---------------------- " > $log
echo "Comienza proceso $PROC DIA :$1 HORA $2" >> $log
echo "ARCHIVO : $ARCHIVO_BCP $WCA REGISTROS"  >> $log

#genero la fecha y hora para el fin de cada registro

#genero el nombre para cada archivo de salida
SALIDAHEDT001="SYS6.BC.HEDT001.ALTAS"
SALIDAHEDT002="SYS6.BC.HEDT002.ALTAS"
SALIDAHEDT003="SYS6.BC.HEDT003.ALTAS"

DESTINO=ARBP.HE.FIX.CARGA.HEDT00x 
#
rm -f SYS6.BC.HEDT00*
rm -f $armsg
#
ORIGEN=$SALIDAHEDT001
MARCA1=PRHP-OK-HEDT001
MARCA2=PARCERAWKENM-OK-GP-HEDT001
awk 'BEGIN { FS = "\t" } { if ($1=="HEDT001") printf "%-8s%-2s%-50s%-50s%-1s%-8s%-8s%-8s%-1s%-8s%-8s\n",$2,"",$5,$6,$7,$8,$9,$10,$11,$12,$13}' $ARCHIVO_BCP > $ORIGEN

if [ ! -s "$ORIGEN" ]
then
  ls -l $ORIGEN
  echo "El archivo \"$ORIGEN\" no existe. o ESTA VACIO"
  touch $ORIGEN
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
  else 
  echo "Se realiza FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" "
  echo "Se realizara  FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" " >> $armsg
  echo "ARCHIVO $ORIGEN :"  >> $armsg
  cat $ORIGEN >> $armsg
  WCA=`wc $ORIGEN|cut -c3-8`
  echo "ARCHIVO : $ORIGEN $WCA REGISTROS"  >> $log
  echo "----------------------------------------------------------------------------" >> $armsg
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
fi

#
#
ORIGEN=$SALIDAHEDT002
MARCA1=PRHP-OK-HEDT002
MARCA2=PARCERAWKENM-OK-GP-HEDT002
awk 'BEGIN { FS = "\t" } { if ($1=="HEDT002") printf "%-8s%-8s%-50s%-8s%-8s%-8s\n",$2,$3,$15,$16,$17,$18}' $ARCHIVO_BCP > $ORIGEN
if [ ! -s "$ORIGEN" ]
then
  ls -l $ORIGEN
  echo "El archivo \"$ORIGEN\" no existe. o ESTA VACIO"
  touch $ORIGEN
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
  else 
  echo "Se realiza FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" "
  echo "Se realizara  FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" " >> $armsg
  echo "ARCHIVO $ORIGEN :"  >> $armsg
  cat $ORIGEN >> $armsg
  echo "----------------------------------------------------------------------------" >> $armsg
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  WCA=`wc $ORIGEN|cut -c3-8`
  echo "ARCHIVO : $ORIGEN $WCA REGISTROS"  >> $log
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
fi
#
ORIGEN=$SALIDAHEDT003
MARCA1=PRHP-OK-HEDT003
MARCA2=PARCERAWKENM-OK-GP-HEDT003
awk 'BEGIN { FS = "\t" } { if ($1=="HEDT003") printf "%-8s%-8s%-18s%-4s%-4s%-1s%-4s%-20s%-8s%-8s%-8s\n",$2,$3,$4,$19,$20,$21,$22,$23,$24,$25,$26}' $ARCHIVO_BCP > $ORIGEN
if [ ! -s "$ORIGEN" ]
then
  ls -l $ORIGEN
  echo "El archivo \"$ORIGEN\" no existe. o ESTA VACIO"
  touch $ORIGEN
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
  else 
  echo "Se realiza FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" "
  echo "Se realizara  FTP a HOST de \"$ORIGEN\" a \"$DESTINO\" " >> $armsg
  echo "ARCHIVO $ORIGEN :"  >> $armsg
  cat $ORIGEN >> $armsg
  echo "----------------------------------------------------------------------------" >> $armsg
  WCA=`wc $ORIGEN|cut -c3-8`
  echo "ARCHIVO : $ORIGEN $WCA REGISTROS"  >> $log
  cp $ORIGEN old/$ORIGEN.$FECHA_ARCH
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -DELETE $MARCA1 ODAT
  rsh -l controlm -n bbvas01 /prod/bmc/controlm/ctm/exe_Solaris/ctmcontb -ADD $MARCA2 ODAT
fi
#
cat $log >> $LOGDOC
#
if [ ! -s "$armsg" ]
then
  echo "El archivo $armsg  no existe. o ESTA VACIO"
  exit 0
  else 
  cat $armsg
  exit 0
fi
