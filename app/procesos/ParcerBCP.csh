#! /bin/csh
##############
set proc = ParcerBCP
set dirhome = /prod/jp00/GesPet
set sistema = GesPet
set serv = DSPROD02
set passsyb = `grep $serv /prod/jp00/general/passw|cut -c12-22`
set base = $sistema
set fec = $1
set hhh = $2
set log = /prod/jp00/logs/$sistema.$proc.$fec$hhh
set sp = sp_UpdateSoliEnviadas
set pa = $3
###########
cd $dirhome
rm -f $dirhome/SOLOUT.txt
rm -f $dirhome/SOLOUT.LOG
date > $log
#
bcp $base..tmpSolicitudes out $dirhome/SOLOUT.txt -b20000 -c -UJP00 -S$serv -P$passsyb -o $dirhome/SOLOUT.LOG
#
cat $dirhome/SOLOUT.LOG >> $log
#
set cant = `grep -c Msg $log`
if ($cant != 0) then
 echo '                                                  ' >> $log
 echo "##################################################" >> $log
 echo "## CANCELO BCP de la BASE  $sistema            ##" >> $log
 echo "##################################################" >> $log
 echo '                                                  ' >> $log
 cat $log
 exit 1
endif
#
set cant = `grep -c DB-LIBRARY $log`
if ($cant != 0 ) then
 echo '                                                  ' >> $log
 echo "##################################################" >> $log
 echo "## CANCELO BCP de la BASE  $sistema               ##" >> $log
 echo "##################################################" >> $log
 echo '                                                  ' >> $log
 cat $log
 exit 1
endif
#
