#! /bin/csh
##############
set proc = ParcerBCP-ENM
set dirhome = /prod/jp00/GesPet
set sistema = GesPet
set serv = DSPROD02
set passsyb = `grep $serv /prod/jp00/general/passw|cut -c12-22`
set base = $sistema
set fec = $1
set hhh = $2
set log = /prod/jp00/logs/$sistema.$proc.$fec$hhh
###########
cd $dirhome
rm -f $dirhome/hedthst.txt
rm -f $dirhome/hedthst.log
date > $log
#
bcp $base..tmpHEDTHST out $dirhome/hedthst.txt -b20000 -c -UJP00 -S$serv -P$passsyb -o $dirhome/hedthst.log
#
cat $dirhome/hedthst.log >> $log
#
set cant = `grep -c Msg $log`
if ($cant != 0) then
 echo '                                                  ' >> $log
 echo "##################################################" >> $log
 echo "## CANCELO BCP de la BASE  $sistema            ##" >> $log
 echo "##################################################" >> $log
 echo '                                                  ' >> $log
 cat $log
 exit 1
endif
#
set cant = `grep -c DB-LIBRARY $log`
if ($cant != 0 ) then
 echo '                                                  ' >> $log
 echo "##################################################" >> $log
 echo "## CANCELO BCP de la BASE  $sistema               ##" >> $log
 echo "##################################################" >> $log
 echo '                                                  ' >> $log
 cat $log
 exit 1
endif
#
