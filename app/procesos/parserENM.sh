#!/bin/sh

##-001- a. FJS 23.10.2009 - Se crea el parseador para armar los archivos para altas en el catalogo en HOST de archivos de enmascaramiento.

ARGS=1
E_BADARGS=65
E_NOFILE=66
#variable con el nombre del archivo generado por el BCP OUT
ARCHIVO_BCP=$1

if [ $# -ne "$ARGS" ]
then
  echo "Faltan argumentos en la llamada al script." 
  echo "Use: `basename $0` archivoBCP_OUT"
  exit $E_BADARGS
fi

if [ ! -f "$ARCHIVO_BCP" ]
then
  echo "El archivo \"$ARCHIVO_BCP\" no existe."
  exit $E_NOFILE
fi

#genero la hora para el nombre del archivo.
FECHA_ARCH=`date +%H%M%S`

#genero la fecha y hora para el fin de cada registro
FECHA=`date +%d" / "%m" / "%y" - "%H":"%M":"%S"`

#genero el nombre para cada archivo de salida
SALIDAHEDT001="SYS6.BC.HEDT001.ALTAS.H$FECHA_ARCH"
SALIDAHEDT002="SYS6.BC.HEDT002.ALTAS.H$FECHA_ARCH"
SALIDAHEDT003="SYS6.BC.HEDT003.ALTAS.H$FECHA_ARCH"

#HEDT001
awk 'BEGIN { FS = "\t" } { if ($1=="HEDT001") printf "%-8s%-2s%-50s%-50s%-1s%-8s%-8s%-8s%-1s%-8s%-8s",$2,"",$5,$6,$7,$8,$9,$10,$11,$12,$13}' $ARCHIVO_BCP > $SALIDAHEDT001
#HEDT002
awk 'BEGIN { FS = "\t" } { if ($1=="HEDT002") printf "%-8s%-8s%-50s%-8s%-8s%-8s",$2,$3,$15,$16,$17,$18}' $ARCHIVO_BCP > $SALIDAHEDT002
#HEDT003
awk 'BEGIN { FS = "\t" } { if ($1=="HEDT003") printf "%-8s%-8s%-18s%-4s%-4s%-1s%-4s%-20s%-8s%-8s%-8s",$2,$3,$4,$19,$20,$21,$22,$23,$24,$25,$26}' $ARCHIVO_BCP > $SALIDAHEDT003
