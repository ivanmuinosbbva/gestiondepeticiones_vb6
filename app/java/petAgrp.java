import java.io.*;
import java.util.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class petAgrp {
	public static String mailSend = "fernando.spitz@correo1.arg.igrupobbva";
	public static String mailSMTP = "gatewaymail.domain.tcpip";
	public static String mailSubj = "Exportaci�n por agrupamientos para Walter Salvi";
	public static String dbUsr = "OdeGesPet";
	public static String dbPwd = "OdeGesPet";
	public static String dbNme = "GesPet";
	public static String dbDrv = "com.sybase.jdbc2.jdbc.SybDriver";
	public static String dbSvr = "jdbc:sybase:Tds:BBVADESA02:9002";
	public static String pathLog = "";
	public static String pathFile = "";
	public static String fileLog = "petAgrp.log";
	public static String fileFile = "petAgrp.txt";
	public static String doMail ="";
	public static String debugMode = "NO";			// SI
	public static String textoAdjunto ="";
	public static String textoRegistro ="";
	public static int agr_nrointerno = 0;
	public static Connection conn;
	public static ResultSet rs;
	public static ResultSet rsTxts;
	public static ResultSet rsAgrp;
	public static CallableStatement cstmt0;
	public static CallableStatement cstmt1;
	public static CallableStatement cstmt2;

	public static String sDescripcion = "";
	public static String sCaracteristicas = "";
	public static String sMotivos = "";
	public static String sBeneficios = "";
	public static String sRelaciones = "";

	private static final int COD_ERROR_CONFIGURACION = 1;
	private static final int COD_ERROR_PROCESO_BASE = 2;
	private static final int COD_ERROR_ARCHIVO = 3;
	private static final int COD_ERROR_PARAMETROS = 4;
	private static final int COD_ERROR_GRAL = 5;
	private static final char TABULATOR = '\t';

	public static void main(String[] args) {
		try {
			leerConfiguracion(args);
			petAgrp objTest = new petAgrp();
			petAgrp.inicializarLog();
			petAgrp.MakeFile();
			petAgrp.cerrar();
		}
		catch (Exception e){
			grabarLog("EXC: main: " + e.getMessage());
			System.exit(COD_ERROR_GRAL);
		}
	}

	// Constructor
	public petAgrp() throws Exception {
		conectarDB();
	}

	public static void inicializarLog() {
		//SimpleDateFormat formatterInicio;
		DateFormat dfDDMMYYYY;
		TimeZone arst;

		arst = new SimpleTimeZone(-3 * 60 * 60 * 1000, "ARST");
		dfDDMMYYYY = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dfDDMMYYYY.setTimeZone(arst);
		String inicioProceso = dfDDMMYYYY.format(new java.util.Date());
		grabarLog("-------------------------------------------------------------------------");
		grabarLog("Inicio de proceso: " + inicioProceso + " / Archivo: " + fileFile);
	}

	public static void leerConfiguracion(String[] args) {
		try {
			//grabarLog("leerConfiguracion : ");
			for (int i=0; i < args.length; i++) {
				if (args[i].length() > 7) {
					String token=args[i].substring(0,6);
					String valor=args[i].substring(6);
					if (token.equalsIgnoreCase("-DBNME")) {
						dbNme = valor;
					}
					if (token.equalsIgnoreCase("-DBSVR")) {
						dbSvr = valor;
					}
					if (token.equalsIgnoreCase("-DBDRV")) {
						dbDrv = valor;
					}
					if (token.equalsIgnoreCase("-DBUSR")) {
						dbUsr = valor;
					}
					if (token.equalsIgnoreCase("-DBPWD")) {
						dbPwd = valor;
					}
					if (token.equalsIgnoreCase("-LOGFL")) {
						fileLog = valor;
					}
					if (token.equalsIgnoreCase("-LOGPT")) {
						pathLog = valor;
					}
					if (token.equalsIgnoreCase("-FILNM")) {
						fileFile = valor;
					}
					if (token.equalsIgnoreCase("-FILPT")) {
						pathFile = valor;
					}
					if (token.equalsIgnoreCase("-MAILN")) {
						doMail = valor;
					}
					if (token.equalsIgnoreCase("-MLSND")) {
						mailSend = valor;
					}
					if (token.equalsIgnoreCase("-MLMTP")) {
						mailSMTP = valor;
					}
					if (token.equalsIgnoreCase("-MLSBJ")) {
						mailSubj = valor;
					}
					if (token.equalsIgnoreCase("-DEBUG")) {
						debugMode = valor;
					}
					if (token.equalsIgnoreCase("-AGRNO")) {
						agr_nrointerno = Integer.parseInt(valor);
					}
				}
			}
		}
		catch (Exception e) {
			grabarLog("EXC: leerConfiguracion: " + e.getMessage());
		}
	}

	static void verificarParametros() {
		if (debugMode.trim().equals("SI")) {
			System.out.println(textoAdjunto);
			System.out.println(dbSvr.trim());
			System.out.println(dbUsr.trim());
			System.out.println(dbNme.trim());
			System.out.println(fileLog.trim());
			System.out.println(pathLog.trim());
			System.out.println(doMail.trim());
		}
		if (fileLog.trim().equals("")) {
			grabarLog("No se ha indicado archivo de Log");
		}
	}

	public static void cerrar() throws Exception {
		conn.close();
		grabarLog("OK: cerrar: ");
	}

	public static void MakeFile() {
		File file = new File(pathFile + fileFile);
		String sTexto = "";
		String sFecha = "";
		int pet_nrointerno = 0;
		String agr_titulo = "";
			
		// Eliminar del disco el archivo anterior (mismo nombre) si existe
		if (file.exists()) {
			if (file.delete() != true) {
				grabarLog("ERR: al borrar el archivo anterior.");
			}
		}

		grabarLog("OK: MakeFile...");

		// Primero grabo el encabezado
		grabarFile("Petici�n"+TABULATOR+"Tipo"+TABULATOR+"T�tulo"+TABULATOR+"Descripci�n"+TABULATOR+"Caracter�sticas"+TABULATOR+"Motivos"+TABULATOR+"Beneficios"+TABULATOR+"Relac. c/Otros Req."+TABULATOR+"Estado Petici�n"+TABULATOR+"F.Ini Planif. Petici�n"+TABULATOR+"F.Fin Planif. Petici�n"+TABULATOR+"F.Ini Real. Petici�n"+TABULATOR+"F.Fin Real. Petici�n"+TABULATOR+"Anexada a"+TABULATOR+"Agrupamiento");
		try {
			cstmt0.setInt(1, agr_nrointerno);
			rs = cstmt0.executeQuery();
			while (rs.next() ) {
				pet_nrointerno = Integer.parseInt(rs.getString("pet_nrointerno"));
				sDescripcion = "";
				sCaracteristicas = "";
				sMotivos = "";
				sBeneficios = "";
				sRelaciones = "";
				obtenerDescripciones(pet_nrointerno);
				sTexto = rs.getString("pet_nroasignado").trim() + TABULATOR;
				sTexto = sTexto + rs.getString("pet_tipo").trim() + TABULATOR;
				sTexto = sTexto + rs.getString("pet_titulo").trim() + TABULATOR;
				sTexto = sTexto + sDescripcion.trim() + TABULATOR;
				sTexto = sTexto + sCaracteristicas.trim() + TABULATOR;
				sTexto = sTexto + sMotivos.trim() + TABULATOR;
				sTexto = sTexto + sBeneficios.trim() + TABULATOR;
				sTexto = sTexto + sRelaciones.trim() + TABULATOR;
				sTexto = sTexto + rs.getString("pet_nom_estado").trim() + TABULATOR;
				sFecha = rs.getString("pet_fe_ini_plan");
				if (sFecha != null)	{
					sTexto = sTexto + sFecha.trim() + TABULATOR;
				} else {
					sTexto = sTexto + "" + TABULATOR;
				}
				sFecha = rs.getString("pet_fe_fin_plan");
				if (sFecha != null)	{
					sTexto = sTexto + sFecha.trim() + TABULATOR;
				} else {
					sTexto = sTexto + "" + TABULATOR;
				}
				sFecha = rs.getString("pet_fe_ini_real");
				if (sFecha != null)	{
					sTexto = sTexto + sFecha.trim() + TABULATOR;
				} else {
					sTexto = sTexto + "" + TABULATOR;
				}
				sFecha = rs.getString("pet_fe_fin_real");
				if (sFecha != null)	{
					sTexto = sTexto + sFecha.trim() + TABULATOR;
				} else {
					sTexto = sTexto + "" + TABULATOR;
				}
				if (rs.getString("pet_nroanexada") != null)	{
					sTexto = sTexto + rs.getString("pet_nroanexada").trim() + TABULATOR;
				} else {
					sTexto = sTexto + "" + TABULATOR;
				}
				agr_titulo = obtenerNombresAgrupamiento(agr_nrointerno, pet_nrointerno);
				if (agr_titulo != null)	{
					if (agr_titulo.equalsIgnoreCase("")){
						sTexto = sTexto + rs.getString("agr_titulo").trim();
					} else {
						sTexto = sTexto + agr_titulo.trim();
					}
				} else {
					sTexto = sTexto + rs.getString("agr_titulo").trim();
				}
				grabarFile(sTexto);
				sTexto = "";
			}
			rs.close();
			grabarLog("EXC: Finalizado MakeFile.");
		}
		catch (Exception e) {
			grabarLog("EXC: MakeFile: " + e.getMessage());
			e.printStackTrace();
		}
	}

	// Obtengo los nombres de los agrupamientos
	public static String obtenerNombresAgrupamiento(int agr_nrointerno, int pet_nrointerno) {
		String valor = "";
		try {
			cstmt2.setInt(1, agr_nrointerno);
			cstmt2.setInt(2, pet_nrointerno);
			rsAgrp = cstmt2.executeQuery();
			while (rsAgrp.next() ) {
				if (rsAgrp.getString("agr_titulo") != null) {
					if (valor == "") {
						valor = rsAgrp.getString("agr_titulo");
					} else {
						valor = valor + " / " + rsAgrp.getString("agr_titulo");
					}
				} else {
					valor = valor + "";
				}
			}
			rsAgrp.close();
		}
		catch (Exception e) {
			grabarLog("EXC: obtenerNombresAgrupamiento: " + e.getMessage());
			e.printStackTrace();
		}
		return valor;
	}

	// Obtengo las descripciones de los campos para una petici�n
	public static void obtenerDescripciones(int pet_nrointerno) {
		String valor = null;
		try {
			cstmt1.setInt(1, pet_nrointerno);
			cstmt1.setString(2, "DESCRIPCIO");
			rsTxts = cstmt1.executeQuery();
			while (rsTxts.next() ) {
				valor = rsTxts.getString("mem_texto");
				if (valor != null) {
					sDescripcion = sDescripcion + rsTxts.getString("mem_texto");
				} else {
					sDescripcion = sDescripcion + "";
				}
			}
			rsTxts.close();
			
			cstmt1.setInt(1, pet_nrointerno);
			cstmt1.setString(2, "CARACTERIS");
			rsTxts = cstmt1.executeQuery();
			while (rsTxts.next() ) {
				valor = rsTxts.getString("mem_texto");
				if (valor != null) {
					sCaracteristicas = sCaracteristicas + rsTxts.getString("mem_texto");
				} else {
					sCaracteristicas = sCaracteristicas + "";
				}
			}
			rsTxts.close();
			
			cstmt1.setInt(1, pet_nrointerno);
			cstmt1.setString(2, "MOTIVOS");
			rsTxts = cstmt1.executeQuery();
			while (rsTxts.next() ) {
				valor = rsTxts.getString("mem_texto");
				if (valor != null) {
					sMotivos = sMotivos + rsTxts.getString("mem_texto");
				} else {
					sMotivos = sMotivos + "";
				}
			}
			rsTxts.close();
			
			cstmt1.setInt(1, pet_nrointerno);
			cstmt1.setString(2, "BENEFICIOS");
			rsTxts = cstmt1.executeQuery();
			while (rsTxts.next() ) {
				valor = rsTxts.getString("mem_texto");
				if (valor != null) {
					sBeneficios = sBeneficios + rsTxts.getString("mem_texto");
				} else {
					sBeneficios = sBeneficios + "";
				}
			}
			rsTxts.close();
			
			cstmt1.setInt(1, pet_nrointerno);
			cstmt1.setString(2, "RELACIONES");
			rsTxts = cstmt1.executeQuery();
			while (rsTxts.next() ) {
				valor = rsTxts.getString("mem_texto");
				if (valor != null) {
					sRelaciones = sRelaciones + rsTxts.getString("mem_texto");
				} else {
					sRelaciones = sRelaciones + "";
				}
			}
			rsTxts.close();
		}
		catch (Exception e) {
			grabarLog("EXC: obtenerDescripciones: " + e.getMessage());
			e.printStackTrace();
		}
	}

	// Graba un archivo plano con los registros le�dos
	public static void grabarFile(String lineaFile) {
		if (debugMode.trim().equals("SI")) {
			System.out.println(lineaFile);
		}
		try {
			PrintWriter out = new PrintWriter(new FileWriter(pathFile + fileFile, true));
			out.println(lineaFile);
			out.flush();
			out.close();
		}
		catch (IOException ioe) {
			System.out.println("Error al grabar linea: " + ioe.getMessage());
		}
	}

	// Graba un log con el String especificado
	public static void grabarLog(String lineaLog) {
		if (debugMode.trim().equals("SI")) {
			System.out.println(lineaLog);
		}
		try {
			PrintWriter out = new PrintWriter(new FileWriter(pathLog + fileLog, true));
			out.println(lineaLog);
			out.flush();
			out.close();
		}
		catch (IOException ioe) {
			System.out.println("Error al grabar el archivo de log.");
		}
	}

	public void conectarDB() throws Exception {
		String driver = dbDrv;
		String url = dbSvr;
		String database = dbNme;

		if (driver == null)
			throw new Exception("No se puede determinar driver.");
		if (url == null)
			throw new Exception("No se puede determinar url.");
		if (database == null)
			throw new Exception("No se puede determinar base de datos.");

		Class.forName(driver).newInstance();
		
		DriverManager.setLoginTimeout(60);
		
		conn = DriverManager.getConnection(url, dbUsr, dbPwd);
		conn.setCatalog(database);

		cstmt0 = conn.prepareCall("{call sp_GetPeticionesPorAgrup(?)}");
		cstmt1 = conn.prepareCall("{call sp_GetPeticionMemo2(?,?)}");
		cstmt2 = conn.prepareCall("{call sp_GetAgrupPeticHijo(?,?)}");
	}
}
