import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.sql.*;
import java.text.*;
import java.math.*;
import javax.mail.*;
import javax.mail.internet.*;

public class petZip {
	private static final int BUFFER = 2048;
	private static final int COD_ERROR_CONFIGURACION = 1;
	private static final int COD_ERROR_PROCESO_BASE = 2;
	private static final int COD_ERROR_ARCHIVO = 3;
	private static final int COD_ERROR_PARAMETROS = 4;
	private static final int COD_ERROR_GRAL = 5;
	private static final char TABULATOR_2 = '\t';
	private static final char TABULATOR = '|';

	public static String mailSend = "fernando.spitz@correo1.arg.igrupobbva";
	//public static String mailSMTP = "gatewaymail.domain.tcpip";
	public static String mailSMTP = "76.253.40.25";
	public static String mailSubj = "Log de archivos adjuntos zipeados en peticiones";
	public static String email = "fernando.spitz@correo1.arg.igrupobbva";
	public static String dbUsr = "OdeGesPet";
	public static String dbPwd = "";
	public static String dbNme = "GesPet";
	public static String dbDrv = "com.sybase.jdbc2.jdbc.SybDriver";		// Para usar jConnect 5.2
	public static String dbSvr = "jdbc:sybase:Tds:BBVADESA02:9002";
	public static String pathLog = "";
	public static String pathFile = "";
	public static String fileLog = "petZip.log";
	public static String fileFile = "";
	public static String doMail = "";
	public static String debugMode = "NO";			// SI
	public static String nrointerno = "";			// Si quiero procesar solo una petici�n espec�fica
	public static String fechaProceso = "";
	public static Connection conn;
	public static ResultSet rs;
	public static ResultSet rs0;
	public static CallableStatement cstmt0;			// Todos los documentos a zipear
	public static CallableStatement cstmt1;			// Todos los documentos a zipear

	public static void main(String[] args) {
		try {
			leerConfiguracion(args);
			petZip objTest = new petZip();
			petZip.inicializarLog();
			petZip.MakeFile();
			petZip.cerrar();
			petZip.finalizarLog();
		}
		catch (Exception e){
			grabarLog("EXC: main: " + e.getMessage());
			e.printStackTrace();
			System.exit(COD_ERROR_GRAL);
		}
	}

	// Constructor
	public petZip() throws Exception {
		conectarDB();
	}

	public static void inicializarLog() {
		DateFormat dfDDMMYYYY;
		TimeZone arst;

		arst = new SimpleTimeZone(-3 * 60 * 60 * 1000, "ARST");
		dfDDMMYYYY = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dfDDMMYYYY.setTimeZone(arst);
		String inicioProceso = dfDDMMYYYY.format(new java.util.Date());
		
		fechaProceso = inicioProceso;
		if (fileFile == "")	{
			dfDDMMYYYY = new SimpleDateFormat("yyyyMMdd_HHmmss");
			fileFile =  "petZip_" + dfDDMMYYYY.format(new java.util.Date()) + ".txt";
		}
		grabarLog("-------------------------------------------------------------------------");
		grabarLog(" >> Inicio de proceso: " + inicioProceso + " / Archivo: " + fileFile);
	}

	public static void finalizarLog() {
		DateFormat dfDDMMYYYY;
		TimeZone arst;

		arst = new SimpleTimeZone(-3 * 60 * 60 * 1000, "ARST");
		dfDDMMYYYY = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dfDDMMYYYY.setTimeZone(arst);
		String inicioProceso = dfDDMMYYYY.format(new java.util.Date());
		
		fechaProceso = inicioProceso;
		if (fileFile == "")	{
			dfDDMMYYYY = new SimpleDateFormat("yyyyMMdd_HHmmss");
			fileFile =  dfDDMMYYYY.format(new java.util.Date()) + ".txt";
		}
		grabarLog(" >> Fin de proceso:    " + inicioProceso);
		grabarLog("-------------------------------------------------------------------------");
	}

	public static void MakeMail() {
		String sTexto = "";

		grabarLog("EXC: MakeMail");
		sTexto = "";
		try {
			enviarMail(email,"");
			rs.close();
		}
		catch (Exception e) {
			grabarLog("EXC: MakeMail: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void enviarMail(String receptor,String mensaje) throws Exception {
        Properties prop = new Properties();
        prop.put("mail.smtp.host",mailSMTP);
		try {
			grabarLog(receptor);
			mensaje = " " + String.valueOf((char)13) + String.valueOf((char)10) + mensaje;
			mensaje = mensaje + " " + String.valueOf((char)13) + String.valueOf((char)10);
			mensaje = mensaje + "GesPet: Se ha generado el proceso de compresi�n de adjuntos.";
			Session sesion = Session.getDefaultInstance(prop, null);
			MimeMessage message = new MimeMessage(sesion);
			message.setFrom(new InternetAddress(mailSend));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(receptor));
			message.setSubject("GesPet: petZip.java");
			message.setText(mensaje,"iso-8859-1");
			Transport.send(message);

		}
		catch (Exception e) {
			grabarLog("EXC: enviarMail: " + e.getMessage());
			e.printStackTrace();
		}
    }

	public static void leerConfiguracion(String[] args) {
		try {
			for (int i=0; i < args.length; i++) {
				if (args[i].length() > 7) {
					String token=args[i].substring(0,6);
					String valor=args[i].substring(6);
					if (token.equalsIgnoreCase("-DBNME")) {
						dbNme = valor;
					}
					if (token.equalsIgnoreCase("-DBSVR")) {
						dbSvr = valor;
					}
					if (token.equalsIgnoreCase("-DBDRV")) {
						dbDrv = valor;
					}
					if (token.equalsIgnoreCase("-DBUSR")) {
						dbUsr = valor;
					}
					if (token.equalsIgnoreCase("-DBPWD")) {
						dbPwd = valor;
					}
					if (token.equalsIgnoreCase("-LOGFL")) {
						fileLog = valor;
					}
					if (token.equalsIgnoreCase("-LOGPT")) {
						pathLog = valor;
					}
					if (token.equalsIgnoreCase("-FILNM")) {
						fileFile = valor;
					}
					if (token.equalsIgnoreCase("-FILPT")) {
						pathFile = valor;
					}
					if (token.equalsIgnoreCase("-MAILN")) {
						doMail = valor;
					}
					if (token.equalsIgnoreCase("-MLSND")) {
						mailSend = valor;
					}
					if (token.equalsIgnoreCase("-MLMTP")) {
						mailSMTP = valor;
					}
					if (token.equalsIgnoreCase("-MLSBJ")) {
						mailSubj = valor;
					}
					if (token.equalsIgnoreCase("-DEBUG")) {
						debugMode = valor;
					}
					if (token.equalsIgnoreCase("-PETNR")) {
						nrointerno = valor;
					}
				}
			}
		}
		catch (Exception e) {
			grabarLog("EXC: leerConfiguracion: " + e.getMessage());
		}
	}

	private static void verificarParametros() {
		if (debugMode.trim().equals("SI")) {
			System.out.println(dbSvr.trim());
			System.out.println(dbUsr.trim());
			System.out.println(dbNme.trim());
			System.out.println(fileLog.trim());
			System.out.println(pathLog.trim());
			System.out.println(doMail.trim());
		}
		if (fileLog.trim().equals("")) {
			grabarLog("No se ha indicado archivo de Log");
		}
	}

	public static void cerrar() throws Exception {
		conn.close();
		//grabarLog("OK: cerrar: ");
	}
	
	private static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}

	public static void MakeFile() {
		File file = new File(pathFile + fileFile);
		String sTexto = "";
		String sFecha = "";
		int resultado = 0;
		int pet_nrointerno = 0;
		
		// Eliminar del disco el archivo anterior (mismo nombre) si existe
		if (file.exists()) {
			if (file.delete() != true) {
				grabarLog("ERROR: al borrar el archivo anterior.");
			}
		}

		grabarLog("OK: MakeFile...");
		grabarFile("-- " + fechaProceso);		// Primero grabo el encabezado
		try {
			if (isNumeric(nrointerno) == true) {
				pet_nrointerno = Integer.parseInt(nrointerno);
				cstmt0.setInt(1, pet_nrointerno);
			} else {
				cstmt0.setNull(1, java.sql.Types.INTEGER);
			}

			rs = cstmt0.executeQuery();
			while (rs.next() ) {
				cstmt1.setInt(1, rs.getInt("pet_nrointerno"));
				cstmt1.setString(2, rs.getString("adj_tipo").trim());
				cstmt1.setString(3, rs.getString("adj_file").trim());
				rs0 = cstmt1.executeQuery();		// Obtengo los bytes del objeto
				rs0.next();
				sTexto = sTexto + rs.getString("pet_nrointerno").trim() + TABULATOR;
				sTexto = sTexto + rs.getString("pet_nroasignado").trim() + TABULATOR;
				sTexto = sTexto + rs.getString("adj_file").trim() + TABULATOR;

				// Streams de salida
		    	FileOutputStream streamObj = new FileOutputStream(pathFile + rs0.getString("adj_file").trim());
		    	streamObj.write(rs0.getBytes("adj_objeto"));	// Escribo en streams y cierro
		    	streamObj.close();

		    	// Compruebo que existen los archivos
		    	File fObj = new File(pathFile + rs.getString("adj_file").trim());
		    	if (fObj.exists()) {
						BufferedInputStream origin = null;
						FileOutputStream dest = new FileOutputStream(pathFile + rs.getString("adj_file").trim() + ".zip");
						CheckedOutputStream checksum = new CheckedOutputStream(dest, new CRC32());
						ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(checksum));
						// Seteo las propiedades del zip
						out.setMethod(ZipOutputStream.DEFLATED);
						out.setComment("Archivo comprimido automaticamente por proceso batch (CGM).");
						out.setLevel(9);															// M�ximo nivel de compresi�n (Rango: 0-9)
						byte data[] = new byte[BUFFER];

						File f = new File(pathFile + rs.getString("adj_file").trim());
						sTexto = sTexto + "(" + f.length() + " bytes)" + TABULATOR;					// Tama�o original
						System.out.println("Adding: " + f);
						FileInputStream fi = new FileInputStream(f);
						origin = new BufferedInputStream(fi, BUFFER);
						ZipEntry entry = new ZipEntry(rs.getString("adj_file").trim());
						out.putNextEntry(entry);
						int count;
						while((count = origin.read(data, 0,BUFFER))!= -1){
								out.write(data, 0, count);
						}
					origin.close();
					out.close();
					// Elimino el archivo original una vez que ha sido comprimido
					if (f.delete() != true) {
						grabarLog("ERROR: al borrar el archivo (NOR) " + pathFile + rs.getString("adj_file").trim());
					}
					sTexto = sTexto + "CHKSUM(CRC32): " + Long.toHexString(checksum.getChecksum().getValue());		// Guardo el Checksum del archivo

					conn.setAutoCommit(false);
					rs0.close();

					// Reemplazar el archivo normal por el comprimido en la base
					File archivo = new File(pathFile + rs.getString("adj_file").trim() + ".zip");
					try	{
						if ( archivo.canRead() ) {
							FileInputStream streamEntrada = new FileInputStream(archivo);
							PreparedStatement pstmt = conn.prepareStatement("update GesPet..PeticionAdjunto set adj_objeto = ?, adj_file = ? where pet_nrointerno = " + rs.getString("pet_nrointerno").trim() + " and adj_tipo ='" + rs.getString("adj_tipo").trim() +"' and adj_file='" + rs.getString("adj_file").trim() + "'");
							pstmt.setBinaryStream(1, streamEntrada, (int)archivo.length());
							pstmt.setString(2, rs.getString("adj_file").trim() + ".zip");
							resultado = pstmt.executeUpdate();
							conn.commit();
							conn.setAutoCommit(true);
							streamEntrada.close();
							pstmt.close();
						}
						// Elimino el archivo zipeado una vez que ha sido subido a la DDBB
						if (archivo.delete() != true) {
							grabarLog("ERROR: al borrar el archivo (ZIP) " + pathFile + rs.getString("adj_file").trim());
						}
						sTexto = sTexto + TABULATOR + "[OK]";
						grabarFile(sTexto);
						sTexto = "";
					}
					catch (SQLException ex) {
						grabarLog("ERROR: al intentar actualizar el archivo " + rs.getString("adj_file").trim() + " / ErrorCode: " + ex.getErrorCode() + " / SQLState: " + ex.getSQLState() + " / Mensaje: " + ex.getMessage());
						// Borro el zip del disco
						if (archivo.delete() != true) {
							grabarLog("ERROR: al borrar el archivo (ZIP) " + pathFile + rs.getString("adj_file").trim() + ".zip");
						}
						//ex.printStackTrace();		// no hace falta aqu� (Produccion)
						conn.rollback();
						conn.setAutoCommit(true);
						sTexto = sTexto + TABULATOR + "[ERROR]";
						grabarFile(sTexto);
						sTexto = "";
					}
				}
			}
			rs.close();
			grabarLog("OK: Finalizado MakeFile.");
		}
		catch (Exception e) {
			grabarLog("EXC: MakeFile: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	// Graba un archivo plano con los registros le�dos
	public static void grabarFile(String lineaFile) {
		if (debugMode.trim().equals("SI")) {
			System.out.println(lineaFile);
		}
		try {
			PrintWriter out = new PrintWriter(new FileWriter(pathFile + fileFile, true));
			out.println(lineaFile);
			out.flush();
			out.close();
		}
		catch (IOException ioe) {
			System.out.println("Error al grabar linea: " + ioe.getMessage());
		}
	}

	// Graba un log con el String especificado
	public static void grabarLog(String lineaLog) {
		if (debugMode.trim().equals("SI")) {
			System.out.println(lineaLog);
		}
		try {
			PrintWriter out = new PrintWriter(new FileWriter(pathLog + fileLog, true));
			out.println(lineaLog);
			out.flush();
			out.close();
		}
		catch (IOException ioe) {
			System.out.println("Error al grabar el archivo de log.");
		}
	}

	public void conectarDB() throws Exception {
		String driver = dbDrv;
		String url = dbSvr;
		String database = dbNme;

		if (driver == null)
			throw new Exception("No se puede determinar driver.");
		if (url == null)
			throw new Exception("No se puede determinar url.");
		if (database == null)
			throw new Exception("No se puede determinar base de datos.");

		Class.forName(driver).newInstance();
		
		DriverManager.setLoginTimeout(60);
		
		conn = DriverManager.getConnection(url, dbUsr, dbPwd);
		conn.setCatalog(database);
		conn.setAutoCommit(false);
		cstmt0 = conn.prepareCall("{call sp_GetPeticionesToZip(?)}");
		cstmt1 = conn.prepareCall("{call sp_GetPeticionesToZipDet(?,?,?)}");
	}

	// Estos dos m�todos (Zip & UnZip) pueden ser utilizados libremente por otras clases. Son independientes, y no se utilizan en esta clase en particular.
	public void Zip(String pFile, String pZipFile) throws Exception {
		// objetos en memoria
		FileInputStream fis = null;
		FileOutputStream fos = null;
		ZipOutputStream zipos = null;
 
		// buffer
		byte[] buffer = new byte[BUFFER];
		try {
			// fichero a comprimir
			fis = new FileInputStream(pFile);
			// fichero contenedor del zip
			fos = new FileOutputStream(pZipFile);
			// fichero comprimido
			zipos = new ZipOutputStream(fos);
			ZipEntry zipEntry = new ZipEntry(pFile);
			zipos.putNextEntry(zipEntry);
			int len = 0;
			// zippear
			while ((len = fis.read(buffer, 0, BUFFER)) != -1)
				zipos.write(buffer, 0, len);
			// volcar la memoria al disco
			zipos.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			// cerramos los files
			zipos.close();
			fis.close();
			fos.close();
		}
	}
 
	public void UnZip(String pZipFile, String pFile) throws Exception {
		BufferedOutputStream bos = null;
		FileInputStream fis = null;
		ZipInputStream zipis = null;
		FileOutputStream fos = null;
 
		try {
			fis = new FileInputStream(pZipFile);
			zipis = new ZipInputStream(new BufferedInputStream(fis));
			if (zipis.getNextEntry() != null) {
				int len = 0;
				byte[] buffer = new byte[BUFFER];
				fos = new FileOutputStream(pFile);
				bos = new BufferedOutputStream(fos, BUFFER);
 
				while  ((len = zipis.read(buffer, 0, BUFFER)) != -1)
					bos.write(buffer, 0, len);
				bos.flush();
			} else {
				throw new Exception("El zip no contenia fichero alguno");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			bos.close();
			zipis.close();
			fos.close();
			fis.close();
		}
	}
}