import java.io.*;
import java.util.*;
import java.sql.*;
import java.text.*;
import java.math.*;
import javax.mail.*;
import javax.mail.internet.*;

public class petMail {
	public static String mailSend = "";
	public static String mailSMTP = "";
	public static String mailSubj = "";
	
	public static String dbDrv = "";
	public static String dbSvr = "";
	public static String dbNme = "";
	public static String dbUsr = "";
	public static String dbPwd = "";
	public static String fileLog = "mailerr.log";
	public static String doMail = "";
	public static String doEscal = "";
	public static String pathLog = "";
	public static String fechaDesde ="";
	public static String fechaHasta ="";
	public static String textoAdjunto ="";
	public static int diasBack = 0;
	public static String debugMode = "NO";
	public static String fecha;
	public static SimpleDateFormat formatterFecha;
	public Connection conn;
	public ResultSet rs;
	public CallableStatement cstmt0;
	public CallableStatement cstmt1;
	public CallableStatement cstmt2;


	public static String cod_recurso = "";
	public static String ant_recurso = "";
	public static String nom_recurso = "";
	public static String email = "";
	public static String sTexto = "";
	public static String titulo = "";
	public static String fe_mensaje = "";
	public static String cod_perfil = "";
	public static String nom_perfil = "";
	public static String txt_txtmsg = "";
	public static String msg_texto = "";

	private static final int COD_ERROR_CONFIGURACION = 1;
	private static final int COD_ERROR_PROCESO_BASE = 2;
	private static final int COD_ERROR_ARCHIVO = 3;
	private static final int COD_ERROR_PARAMETROS = 4;
	private static final int COD_ERROR_GRAL = 5;

	public petMail() throws Exception {
		conectarBase();
	}

	public static void main (String[] args) {
		try {


			leerConfiguracion(args);
			verificarParametros();
			inicializarAplicacion();

			petMail objMail = new petMail();


			if (doEscal.equalsIgnoreCase("SI")) {
				objMail.MakeEscal();
			}
			if (doMail.equalsIgnoreCase("SI")) {
				objMail.MakeMail();
			}

			objMail.procesoCerrado();
			objMail.cerrar();
		}
		catch (Exception e){
			grabarLog("EXC: main: " + e.getMessage());
			procesoCancelado();
			System.exit(COD_ERROR_GRAL);
		}
	}

	static void leerConfiguracion(String[] args) {
			Calendar calendar = Calendar.getInstance();
			String yymmdd = "";
			String mes = "";
			String dia = "";
			String anio = "";


		SimpleDateFormat formatterInicio;
		formatterInicio = new SimpleDateFormat ("yyyyMMdd", Locale.getDefault());
		try {
			FileReader fInput = new FileReader("petMail.cfg");
			BufferedReader fbInput = new BufferedReader(fInput);
			int posicion;
			String cfgCadena;
			while (fbInput.ready()) {
				cfgCadena = fbInput.readLine();
				if (cfgCadena != null) {
					if (!("" + cfgCadena).trim().equals("")) {
						if (!cfgCadena.trim().substring(0,1).equals("#")) {
							posicion = cfgCadena.trim().indexOf("=");
							if (posicion > -1) {
								String token = cfgCadena.trim().substring(0,posicion).trim();
								String valor = cfgCadena.trim().substring(posicion + 1).trim();
								if (token.equalsIgnoreCase("DBDRV")) {
									dbDrv = valor;
								}
								if (token.equalsIgnoreCase("DBSVR")) {
									dbSvr = valor;
								}
								if (token.equalsIgnoreCase("DBNME")) {
									dbNme = valor;
								}
								if (token.equalsIgnoreCase("DBUSR")) {
									dbUsr = valor;
								}
								if (token.equalsIgnoreCase("DBPWD")) {
									dbPwd = valor;
								}
								if (token.equalsIgnoreCase("LOGFL")) {
									fileLog = valor;
								}
								if (token.equalsIgnoreCase("LOGPT")) {
									pathLog = valor;
								}
								if (token.equalsIgnoreCase("ESCAL")) {
									doEscal = valor;
								}
								if (token.equalsIgnoreCase("DIASX")) {
									diasBack = Integer.valueOf(valor).intValue();
								}
								if (token.equalsIgnoreCase("MAILN")) {
									doMail = valor;
								}
								if (token.equalsIgnoreCase("MLSND")) {
									mailSend = valor;
								}
								if (token.equalsIgnoreCase("MLMTP")) {
									mailSMTP = valor;
								}
								if (token.equalsIgnoreCase("MLSBJ")) {
									mailSubj = valor;
								}
								if (token.equalsIgnoreCase("DEBUG")) {
									debugMode = valor;
								}
								if (token.equalsIgnoreCase("TXTAD")) {
									textoAdjunto = textoAdjunto + valor + String.valueOf((char)13) + String.valueOf((char)10);
								}
							}
						}
					}
				}
			}
			//los parametros por linea vencen a los de ini
			for (int i=0; i < args.length; i++) {
				if (args[i].length() > 7) {
					String token=args[i].substring(0,6);
					String valor=args[i].substring(7);
					if (token.equalsIgnoreCase("-DBDRV")) {
						dbDrv = valor;
					}
					if (token.equalsIgnoreCase("-DBSVR")) {
						dbSvr = valor;
					}
					if (token.equalsIgnoreCase("-DBNME")) {
						dbNme = valor;
					}
					if (token.equalsIgnoreCase("-DBUSR")) {
						dbUsr = valor;
					}
					if (token.equalsIgnoreCase("-DBPWD")) {
						dbPwd = valor;
					}
					if (token.equalsIgnoreCase("-LOGFL")) {
						fileLog = valor;
					}
					if (token.equalsIgnoreCase("-LOGPT")) {
						pathLog = valor;
					}
					if (token.equalsIgnoreCase("-ESCAL")) {
						doEscal = valor;
					}
					if (token.equalsIgnoreCase("-MAILN")) {
						doMail = valor;
					}
					if (token.equalsIgnoreCase("-MLSND")) {
						mailSend = valor;
					}
					if (token.equalsIgnoreCase("-MLMTP")) {
						mailSMTP = valor;
					}
					if (token.equalsIgnoreCase("-MLSBJ")) {
						mailSubj = valor;
					}
					if (token.equalsIgnoreCase("-DIASX")) {
						diasBack = Integer.valueOf(valor).intValue();
					}
					if (token.equalsIgnoreCase("-DEBUG")) {
						debugMode = valor;
					}

				}
			}

		fechaHasta = formatterInicio.format(new java.util.Date());

		calendar.add(Calendar.DATE,(-1 * diasBack));
		mes = "" + new java.lang.Integer(calendar.get(Calendar.MONTH)+1).toString();
		mes =  "00".substring(mes.length()) + mes;
		dia = "" +  new java.lang.Integer(calendar.get(Calendar.DAY_OF_MONTH)).toString();
		dia = "00".substring(dia.length()) + dia;
		anio = new java.lang.Integer(calendar.get(Calendar.YEAR)).toString();
		fechaDesde  = anio + mes + dia;

		}
		catch (IOException e) {
			grabarLog("EXC: leerConfiguracion: " + e.getMessage());
			procesoCancelado();
			System.exit(COD_ERROR_CONFIGURACION);
		}
	}

	static void verificarParametros() {

		if (debugMode.trim().equals("SI")) {
			System.out.println(dbSvr.trim());
			System.out.println(dbUsr.trim());
			System.out.println(dbPwd.trim());
			System.out.println(fileLog.trim());
			System.out.println(pathLog.trim());
			System.out.println(doEscal.trim());
			System.out.println(doMail.trim());
			System.out.println(fechaDesde.trim());
			System.out.println(fechaHasta.trim());
			System.out.println(diasBack);
			System.out.println(textoAdjunto);
		}

		if (fileLog.trim().equals("")) {
			grabarLog("No se ha indicado archivo de Log");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (dbSvr.trim().equals("")) {
			grabarLog("No se ha indicado el servidor");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (dbUsr.trim().equals("")) {
			grabarLog("No se ha indicado usuario");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (dbPwd.trim().equals("")) {
			System.out.println("No se ha indicado password");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (mailSubj.trim().equals("")) {
			grabarLog("No se especifico mailSubject");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (mailSMTP.trim().equals("")) {
			grabarLog("No se especifico mailSMTP");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (mailSend.trim().equals("")) {
			grabarLog("No se especifico mailsender");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
	}

	static void inicializarAplicacion() {
		SimpleDateFormat formatterInicio;
		formatterInicio = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
		String inicioProceso = formatterInicio.format(new java.util.Date());
		grabarLog("");
		grabarLog("===================================================");
		grabarLog("Inicio del Proceso --> " + inicioProceso);
		grabarLog("---------------------------------------------------");
		formatterFecha = new SimpleDateFormat ("yyyy-MM-dd", Locale.getDefault());
		fecha = formatterFecha.format(new java.util.Date());
	}

	public void conectarBase() throws Exception {
		//String driver = "com.sybase.jdbc2.jdbc.SybDriver";
		//String url = "jdbc:sybase:Tds:" + dbSvr;
		//String database = "GesPet";
		String driver = dbDrv;
		String url = dbSvr;
		String database = dbNme;

		if (driver == null)
			throw new Exception("No se puede determinar driver");
		if (url == null)
			throw new Exception("No se puede determinar url");
		if (database == null)
			throw new Exception("No se puede determinar base de datos");

		Class.forName(driver).newInstance();
		conn = DriverManager.getConnection(url, dbUsr, dbPwd);
		conn.setCatalog(database);
		cstmt0 = conn.prepareCall("{call GesPet..sp_GetEstado(?)}");
		cstmt1 = conn.prepareCall("{call GesPet..sp_VerEscalamiento()}");
		cstmt2 = conn.prepareCall("{call GesPet..sp_GetMensajesMail(?,?)}");
	}

	public void MakeEscal() {

	}

	public void MakeMail() {
			grabarLog("EXC: MakeMail");
			sTexto = "";
			try {
			cstmt2.setString(1,fechaDesde);
			cstmt2.setString(2,fechaHasta);
			rs = cstmt2.executeQuery();
			while (rs.next()){
				cod_recurso = rs.getString("cod_recurso");
				if (!ant_recurso.trim().equals("") && !ant_recurso.trim().equals(cod_recurso.trim())) {
					enviarMail(email,sTexto);
					ant_recurso = "";
				}
				if (ant_recurso.trim().equals("")) {
					ant_recurso = rs.getString("cod_recurso");
					email = rs.getString("email");
					sTexto = "";
				}
				sTexto = sTexto + "Petici�n: " + rs.getString("pet_nroasignado") + " - " + rs.getString("titulo") + String.valueOf((char)13) + String.valueOf((char)10);
				sTexto = sTexto + rs.getString("nom_recurso") + " - " + rs.getString("nom_perfil") + String.valueOf((char)13) + String.valueOf((char)10);
				sTexto = sTexto + "   " + rs.getString("msg_texto") + String.valueOf((char)13) + String.valueOf((char)10);
				sTexto = sTexto + "   " + rs.getString("txt_txtmsg") + String.valueOf((char)13) + String.valueOf((char)10);
				sTexto = sTexto + "_________________________________________________________" + String.valueOf((char)13) + String.valueOf((char)10);
				sTexto = sTexto + " " + String.valueOf((char)13) + String.valueOf((char)10);
			}
			if (!ant_recurso.trim().equals("")) {
				enviarMail(email,sTexto);				//enviar el mail
			}
			rs.close();
			}
			catch (Exception e) {
				grabarLog("EXC: MakeMail: " + e.getMessage());
				e.printStackTrace();
			}
	}

    public static void enviarMail(String receptor,String mensaje) throws Exception {
        Properties prop = new Properties();
        prop.put("mail.smtp.host",mailSMTP);
		try {
			grabarLog(receptor);
			mensaje = " " + String.valueOf((char)13) + String.valueOf((char)10) + mensaje;
			mensaje = mensaje + " " + String.valueOf((char)13) + String.valueOf((char)10);
			mensaje = mensaje + textoAdjunto;
			if (debugMode.trim().equals("SI")) {
				grabarLog(mensaje);
			}

			Session sesion = Session.getDefaultInstance(prop, null);
			//sesion.setDebug(true);

			MimeMessage message = new MimeMessage(sesion);
			message.setFrom(new InternetAddress(mailSend));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(receptor));
			message.setSubject(mailSubj);
			message.setText(mensaje,"iso-8859-1");
			Transport.send(message);

		}
		catch (Exception e) {
			grabarLog("EXC: enviarMail: " + e.getMessage());
			e.printStackTrace();
		}
    }

	public static void procesoCancelado() {
		SimpleDateFormat formatterFinal;
		formatterFinal = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
		String finalProceso = formatterFinal.format(new java.util.Date());
		grabarLog("---------------------------------------------------");
		grabarLog("Proceso Cancelado  --> " + finalProceso);
		grabarLog("===================================================");
	}
	public static void procesoCerrado() {
		SimpleDateFormat formatterFinal;
		formatterFinal = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
		String finalProceso = formatterFinal.format(new java.util.Date());
		grabarLog("---------------------------------------------------");
		grabarLog("Proceso Cerrado  --> " + finalProceso);
		grabarLog("===================================================");
	}

	public static void grabarLog(String lineaLog) {
		if (debugMode.trim().equals("SI")) {
			System.out.println(lineaLog);
		}
		try {
			PrintWriter out = new PrintWriter(new FileWriter(pathLog + fileLog, true));
			out.println(lineaLog);
			out.flush();
			out.close();
		}
		catch (java.io.IOException ioe) {
			System.out.println("Error al grabar el archivo de log");
		}
	}

	public void cerrar() throws Exception {
		conn.close();
	}
}
