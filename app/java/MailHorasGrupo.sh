#! /bin/ksh
tabla=gespet
sist="Gespet             "
grupo="Semanal              "
job=petHoras
proc=MailHorasGrupo
script=$proc.sh
frecu="Semanal             "
dirproc=/prod/jp00/procesos/$tabla
dirlog=/prod/jp00/logs/$tabla
log=$dirlog/$job.$1.$2.log
ini="`date '+%d/%m/%Y %T'`"
#
# Correos
conso="correo1.consolidar.arg.igrupobbva"
banco="correo1.arg.igrupobbva"
Mail01="ccomputos@$conso"
Mail02="tareasux@$conso"
Mail03="OcampoA@$conso fernando.spitz@$conso"
MAILOK="$Mail01 $Mail02 $Mail03"
MAILCAN="$Mail01 $Mail02 $Mail03"
#
echo "*************************************************************************" >$log
echo " Job: $job                                             " >>$log
echo " Sistema: $sist                 Tabla: $tabla          " >>$log
echo " Grupo: $grupo                  Script: $script        " >>$log
echo " Frecuencia: $frecu             Fecha Proceso: $1      " >>$log
echo "*************************************************************************" >>$log
echo " Comienza proceso:  $ini                               " >>$log
echo "*************************************************************************\n" >>$log
#
##############
# -DBUSR:Usuario Sybase
# -DBPWD:Password Sybase
# -NIVEL:a que nivel se refiere el AREAS (DIRE/GERE/SECT/GRUP)
# -AREAS:Codigo de area
# -INFOR:a que nivel se dispara el informe (DIRE/GERE/SECT/GRUP)
# -PORCM:Porcentaje minimo de carga a partir del cual se informa
# -LOGPT:carpeta en la que deja los detalles de ejecucion
# -LOGFL:nombre de archivo de log
# -DEBUG:SI/NO
# -MAILN:dispara mails SI/NO
# -MLSBJ:Subject del mail pej:GESTION DE PETICIONES
# -MLSND:sender pej:PABLO.GROSSO@MEMO.DOMAIN.TCPIP
# -MLMTP:servidor SMTP pej:gatewaymail.domain.tcpip
# si no se especifican parametros por linea de comando, asume los del .cfg
##############
#
usersyb=JP00
servsyb=S14185031 
passsyb=`grep $servsyb /prod/jp00/general/passw|cut -f2`
echo `date '+%Y%m%d.%T'` > /prod/jp00/procesos/gespet/fecha.proc
#
cd $dirproc
fecha=`cat fecha.proc`
#
java -classpath "/prod/jp00/procesos/gespet/j2ee.jar:/prod/jp00/procesos/gespet/jconn2.jar:/prod/jp00/procesos/gespet/Sprinta2000.jar:/prod/jp00/procesos/gespet" petHoras  -NIVEL:GERE -AREAS:DESA -INFOR:GRUP -PORCM:100 -LOGFL:GesPet.petHoras.log -DBUSR:$usersyb  -DBPWD:$passsyb 
if [ $? -ne 0 ] 
then
    cat /prod/jp00/logs/gespet/GesPet.petHoras.log >> $log
    rm /prod/jp00/logs/gespet/GesPet.petHoras.log
    fin="`date '+%d/%m/%Y %T'`"
    echo "********************************************************************" >>$log
    echo " Hay Errores en la Corrida de $proc                                 " >>$log
    echo " Termina proceso:  $fin                                             " >>$log
    echo "********************************************************************" >>$log
    cat $log | mailx -s "Cancelo $tabla-$job" "$MAILCAN"
    exit 12
fi
#
cat /prod/jp00/logs/gespet/GesPet.petHoras.log  >> $log
rm /prod/jp00/logs/gespet/GesPet.petHoras.log
fin="`date '+%d/%m/%Y %T'`"
echo "*************************************************************************" >>$log
echo " Termina proceso:  $fin                                                  " >>$log
echo "*************************************************************************" >>$log
cat $log | mailx -s "Log de $tabla-$job" "$MAILOK"
