import java.io.*;
import java.util.*;
import java.sql.*;
import java.text.*;
import java.math.*;
import javax.mail.*;
import javax.mail.internet.*;

public class petHoras {
	public static String mailSend= (String)"";
	public static String mailSMTP= (String)"";
	public static String mailSubj= (String)"";
	
	public static String dbDrv = "com.sybase.jdbc2.jdbc.SybDriver";
	public static String dbSvr = "jdbc:sybase:Tds:bbvadesa02:9002";
	public static String dbNme = "GesPet";
	public static String dbUsr= (String)"";
	public static String dbPwd= (String)"";
	public static String fileLog = "mailerr.log";
	public static String Infor= (String)"";
	public static String Porc= (String)"";
	public static String Area= (String)"";
	public static String Nivel= (String)"";
	public static String doMail= (String)"";
	public static String pathLog= (String)"";
	public static String fechaDesde = (String)"";
	public static String fechaHasta = (String)"";
	public static String textoAdjunto = (String)"";
	public static String textoCabezal = (String)"";
	public static int ajusteGMT = 0;
	public static String debugMode = "NO";
	public static SimpleDateFormat formatterFecha;
	public Connection conn;
	public ResultSet rs;
	public ResultSet rsHoras;
	public ResultSet rsRecurso;
	public CallableStatement cstmt0;
	public CallableStatement cstmt1;
	public CallableStatement cstmt2;

	public static String ant_direccion= (String)"";
	public static String ant_gerencia= (String)"";
	public static String ant_sector= (String)"";
	public static String ant_grupo= (String)"";

	public static String cod_recurso= (String)"";
	public static String ant_recurso= (String)"";
	public static String nom_recurso= (String)"";
	public static String email= (String)"";
	public static String sTexto= (String)"";
	public static String titulo= (String)"";
	public static String fe_mensaje= (String)"";
	public static String cod_perfil= (String)"";
	public static String nom_perfil= (String)"";
	public static String txt_txtmsg= (String)"";
	public static String msg_texto= (String)"";

	private static final int COD_ERROR_CONFIGURACION = 1;
	private static final int COD_ERROR_PROCESO_BASE = 2;
	private static final int COD_ERROR_ARCHIVO = 3;
	private static final int COD_ERROR_PARAMETROS = 4;
	private static final int COD_ERROR_GRAL = 5;

	public static void main (String[] args) {
		try {
			leerConfiguracion(args);
			verificarParametros();
			inicializarAplicacion();

			petHoras objMail = new petHoras();
			objMail.MakeMail();
			objMail.procesoCerrado();
			objMail.cerrar();
		}
		catch (Exception e){
			grabarLog("EXC: main: " + e.getMessage());
			procesoCancelado();
			System.exit(COD_ERROR_GRAL);
		}
	}

	public petHoras() throws Exception {
		conectarBase();
	}

	static void leerConfiguracion(String[] args) {
		Calendar calendar = Calendar.getInstance();
		String auxFechaD= (String)"";
		String auxFechaH= (String)"";

		SimpleDateFormat formatterInicio;
		formatterInicio = new SimpleDateFormat ("yyyyMMdd", Locale.getDefault());

		SimpleDateFormat formatterCabezal;
		formatterCabezal = new SimpleDateFormat ("dd/MM/yyyy", Locale.getDefault());

		try {
			FileReader fInput = new FileReader("petMail.cfg");
			BufferedReader fbInput = new BufferedReader(fInput);
			int posicion;
			String cfgCadena;
			while (fbInput.ready()) {
				cfgCadena = fbInput.readLine();
				if (cfgCadena != null) {
					if (!("" + cfgCadena).trim().equals("")) {
						if (!cfgCadena.trim().substring(0,1).equals("#")) {
							posicion = cfgCadena.trim().indexOf("=");
							if (posicion > -1) {
								String token = cfgCadena.trim().substring(0,posicion).trim();
								String valor = cfgCadena.trim().substring(posicion + 1).trim();
								if (token.equalsIgnoreCase("DBDRV")) {
									dbDrv = valor;
								}
								if (token.equalsIgnoreCase("DBSVR")) {
									dbSvr = valor;
								}
								if (token.equalsIgnoreCase("DBNME")) {
									dbNme = valor;
								}
								if (token.equalsIgnoreCase("DBUSR")) {
									dbUsr = valor;
								}
								if (token.equalsIgnoreCase("DBPWD")) {
									dbPwd = valor;
								}
								if (token.equalsIgnoreCase("LOGFL")) {
									fileLog = valor;
								}
								if (token.equalsIgnoreCase("LOGPT")) {
									pathLog = valor;
								}
								if (token.equalsIgnoreCase("AJGMT")) {
									ajusteGMT = Integer.valueOf(valor).intValue();
								}
								if (token.equalsIgnoreCase("MAILN")) {
									doMail = valor;
								}
								if (token.equalsIgnoreCase("PORCM")) {
									Porc = valor;
								}
								if (token.equalsIgnoreCase("INFOR")) {
									Infor = valor;
								}
								if (token.equalsIgnoreCase("AREAS")) {
									Area = valor;
								}
								if (token.equalsIgnoreCase("NIVEL")) {
									Nivel = valor;
								}
								if (token.equalsIgnoreCase("MLSND")) {
									mailSend = valor;
								}
								if (token.equalsIgnoreCase("MLMTP")) {
									mailSMTP = valor;
								}
								if (token.equalsIgnoreCase("MLSBJ")) {
									mailSubj = valor;
								}
								if (token.equalsIgnoreCase("DEBUG")) {
									debugMode = valor;
								}
								if (token.equalsIgnoreCase("TXTAD")) {
									textoAdjunto = textoAdjunto + valor + String.valueOf((char)13) + String.valueOf((char)10);
								}
							}
						}
					}
				}
			}
			//los parametros por linea vencen a los de ini
			for (int i=0; i < args.length; i++) {
				if (args[i].length() > 7) {
					String token=args[i].substring(0,6);
					String valor=args[i].substring(7);

					if (token.equalsIgnoreCase("-DBDRV")) {
						dbDrv = valor;
					}
					if (token.equalsIgnoreCase("-DBSVR")) {
						dbSvr = valor;
					}
					if (token.equalsIgnoreCase("-DBNME")) {
						dbNme = valor;
					}
					if (token.equalsIgnoreCase("-DBUSR")) {
						dbUsr = valor;
					}
					if (token.equalsIgnoreCase("-DBPWD")) {
						dbPwd = valor;
					}
					if (token.equalsIgnoreCase("-LOGFL")) {
						fileLog = valor;
					}
					if (token.equalsIgnoreCase("-LOGPT")) {
						pathLog = valor;
					}
					if (token.equalsIgnoreCase("-MAILN")) {
						doMail = valor;
					}
					if (token.equalsIgnoreCase("-PORCM")) {
						Porc = valor;
					}
					if (token.equalsIgnoreCase("-INFOR")) {
						Infor = valor;
					}
					if (token.equalsIgnoreCase("-AREAS")) {
						Area = valor;
					}
					if (token.equalsIgnoreCase("-NIVEL")) {
						Nivel = valor;
					}
					if (token.equalsIgnoreCase("-MLSND")) {
						mailSend = valor;
					}
					if (token.equalsIgnoreCase("-MLMTP")) {
						mailSMTP = valor;
					}
					if (token.equalsIgnoreCase("-MLSBJ")) {
						mailSubj = valor;
					}
					if (token.equalsIgnoreCase("-AJGMT")) {
						ajusteGMT = Integer.valueOf(valor).intValue();
					}
					if (token.equalsIgnoreCase("-DEBUG")) {
						debugMode = valor;
					}
				}
			}
//			calendar.add(Calendar.DATE,(-1 * calendar.get(Calendar.DAY_OF_WEEK)));
//			mes= (String)"" + new java.lang.Integer(calendar.get(Calendar.MONTH)+1).toString();
//			mes =  "00".substring(mes.length()) + mes;
//			dia= (String)"" +  new java.lang.Integer(calendar.get(Calendar.DAY_OF_MONTH)).toString();
//			dia = "00".substring(dia.length()) + dia;
//			anio = new java.lang.Integer(calendar.get(Calendar.YEAR)).toString();
//			fechaHasta  = anio + mes + dia;
//			fechaDesde  = anio + mes + dia;

			calendar.setTime(realDate(0));
			calendar.add(Calendar.DATE,(-1 * (calendar.get(Calendar.DAY_OF_WEEK) - 1)));
			fechaHasta = formatterInicio.format(calendar.getTime());
			auxFechaH = formatterCabezal.format(calendar.getTime());
			
			calendar.add(Calendar.DATE,(-1 * 6));
			fechaDesde = formatterInicio.format(calendar.getTime());
			auxFechaD = formatterCabezal.format(calendar.getTime());

			textoCabezal= (String)"";
			textoCabezal=textoCabezal + "Se informan los recursos cuyo porcentaje de registro de horas fue inferior al " + Porc +  "%, respecto de su carga de horas nominal, ";
			textoCabezal=textoCabezal + "durante el per�odo comprendido entre el " + auxFechaD + " y el " + auxFechaH + ".";
			textoCabezal=textoCabezal + " " + String.valueOf((char)13) + String.valueOf((char)10);
			textoCabezal=textoCabezal + " " + String.valueOf((char)13) + String.valueOf((char)10);
			textoAdjunto= (String)"";
			textoAdjunto=textoAdjunto + "* Este mail fue enviado en forma autom�tica por la aplicaci�n. Por favor, no env�e respuesta a su emisor.";
			textoAdjunto=textoAdjunto + " " + String.valueOf((char)13) + String.valueOf((char)10);
			textoAdjunto=textoAdjunto + " " + String.valueOf((char)13) + String.valueOf((char)10);

		
		}
		catch (IOException e) {
			grabarLog("EXC: leerConfiguracion: " + e.getMessage());
			procesoCancelado();
			System.exit(COD_ERROR_CONFIGURACION);
		}
	}

	static void verificarParametros() {
		if (debugMode.trim().equals("SI")) {
			System.out.println("DRIVER:      " + dbDrv.trim());
			System.out.println("SERVER:      " + dbSvr.trim());
			System.out.println("DBNAME:      " + dbNme.trim());
			System.out.println("USRNAME:     " + dbUsr.trim());
			System.out.println("PATHLOG:     " + pathLog.trim() + "/" + fileLog.trim());
			//System.out.println(Infor.trim());
			//System.out.println(Area.trim());
			//System.out.println(Nivel.trim());
			System.out.println("DOMAIL:      " + doMail.trim());
			System.out.println("DESDE-HASTA: " + ajusteGMT);
		}
		if (fileLog.trim().equals("")) {
			grabarLog("No se ha indicado archivo de Log");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (dbSvr.trim().equals("")) {
			grabarLog("No se ha indicado el servidor");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (dbUsr.trim().equals("")) {
			grabarLog("No se ha indicado usuario");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (dbPwd.trim().equals("")) {
			System.out.println("No se ha indicado password");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (dbNme.trim().equals("")) {
			System.out.println("No se ha indicado database");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (mailSubj.trim().equals("")) {
			grabarLog("No se especifico mailSubject");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (mailSMTP.trim().equals("")) {
			grabarLog("No se especifico mailSMTP");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
		if (mailSend.trim().equals("")) {
			grabarLog("No se especifico mailsender");
			procesoCancelado();
			System.exit(COD_ERROR_PARAMETROS);
		}
	}

	static void inicializarAplicacion() {
		SimpleDateFormat formatterInicio;
		formatterInicio = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
		String inicioProceso = formatterInicio.format(realDate(0));
		grabarLog("");
		grabarLog("===================================================");
		grabarLog("Inicio del Proceso --> " + inicioProceso);
		grabarLog("---------------------------------------------------");
		grabarLog(fechaDesde.trim() + "-" + fechaHasta.trim());
	}

	static String clearNull(String str) {
		if (str == null){
			return (String)"";
		}
		return str.trim();
	}

	public void conectarBase() throws Exception {
		String driver = dbDrv;
		String url = dbSvr;
		String database = dbNme;

		if (driver == null)
			throw new Exception("No se puede determinar driver");
		if (url == null)
			throw new Exception("No se puede determinar url");
		if (database == null)
			throw new Exception("No se puede determinar base de datos");
		
		/*
		Class.forName(driver).newInstance();
		grabarLog("OK: conectarBase.forName: ");

		conn = DriverManager.getConnection(url, dbUsr, dbPwd);
		grabarLog("OK: conectarBase.getConnection: ");

		conn.setCatalog(database);
		grabarLog("OK: conectarBase.setCatalog: ");
		*/
		cstmt0 = conn.prepareCall("{call sp_GetMensajesTexto(?)}");
		cstmt1 = conn.prepareCall("{call sp_GetRecursoAcargoArea(?,?,?,?)}");
		cstmt2 = conn.prepareCall("{call sp_rptHsTrabRecuEjec(?,?,?,?,?,?)}");
	}

	public void MakeMail() {
			boolean bFirst=true;
			boolean bCorte=false;
			//grabarLog("EXC: MakeMail");

			sTexto = (String)"";
			ant_direccion = (String)"";
			ant_gerencia  = (String)"";
			ant_sector    = (String)"";
			ant_grupo     = (String)"";

			try {
				cstmt2.setString(1,fechaDesde);
				cstmt2.setString(2,fechaHasta);
				cstmt2.setString(3,Nivel);
				cstmt2.setString(4,Area);
				cstmt2.setString(5,"NULL");
				cstmt2.setString(6,Porc);
				rsHoras = cstmt2.executeQuery();
				while (rsHoras.next()){
					if (bFirst) {
						bFirst=false;
						ant_direccion = clearNull(rsHoras.getString("cod_direccion"));
						ant_gerencia  = clearNull(rsHoras.getString("cod_gerencia"));
						ant_sector    = clearNull(rsHoras.getString("cod_sector"));
						ant_grupo     = clearNull(rsHoras.getString("cod_grupo"));
						bCorte=true;
					}

					if (!ant_direccion.trim().equals(clearNull(rsHoras.getString("cod_direccion"))) || !ant_gerencia.trim().equals(clearNull(rsHoras.getString("cod_gerencia"))) || !ant_sector.trim().equals(clearNull(rsHoras.getString("cod_sector"))) || !ant_grupo.trim().equals(clearNull(rsHoras.getString("cod_grupo")))) {
						bCorte=true;
					}

					if (Infor.trim().equals("GRUP")) {
						if (!ant_grupo.trim().equals(clearNull(rsHoras.getString("cod_grupo")))) {
							enviarMail(ant_direccion,ant_gerencia,ant_sector,ant_grupo,sTexto);
							bCorte=true;
							sTexto = (String)"";
						}
					}else if (Infor.trim().equals("SECT")) {
						if (!ant_sector.trim().equals(clearNull(rsHoras.getString("cod_sector")))) {
							enviarMail(ant_direccion,ant_gerencia,ant_sector,"",sTexto);
							bCorte=true;
							sTexto = (String)"";
						}
					}else if (Infor.trim().equals("GERE")) {
						if (!ant_gerencia.trim().equals(clearNull(rsHoras.getString("cod_gerencia")))) {
							enviarMail(ant_direccion,ant_gerencia,"","",sTexto);
							bCorte=true;
							sTexto = (String)"";
						}
					}else if (Infor.trim().equals("DIRE")) {
						if (!ant_direccion.trim().equals(clearNull(rsHoras.getString("cod_direccion")))) {
							enviarMail(ant_direccion,"","","",sTexto);
							bCorte=true;
							sTexto = (String)"";
						}
					}
				
					if (bCorte==true) {
						bCorte=false;
						ant_direccion = clearNull(rsHoras.getString("cod_direccion"));
						ant_gerencia  = clearNull(rsHoras.getString("cod_gerencia"));
						ant_sector    = clearNull(rsHoras.getString("cod_sector"));
						ant_grupo     = clearNull(rsHoras.getString("cod_grupo"));
						sTexto = sTexto + "" + String.valueOf((char)13) + String.valueOf((char)10);
						sTexto = sTexto + "Sector: " + clearNull(rsHoras.getString("nom_sector")) + String.valueOf((char)13) + String.valueOf((char)10);
						sTexto = sTexto + "Grupo: " + clearNull(rsHoras.getString("nom_grupo")) + String.valueOf((char)13) + String.valueOf((char)10);
						sTexto = sTexto + "Carga" + "  " + String.valueOf((char)9) + "Legajo" + "  " + String.valueOf((char)9) + "Nombre" +  "  " + String.valueOf((char)13) + String.valueOf((char)10);
					}

					sTexto = sTexto  + clearNull(rsHoras.getString("horas_por"))+ "%  " + String.valueOf((char)9) +  clearNull(rsHoras.getString("cod_recurso"))+ "  " + String.valueOf((char)9) + clearNull(rsHoras.getString("nom_recurso"));
					sTexto = sTexto + String.valueOf((char)13) + String.valueOf((char)10);
				}
				rsHoras.close();

				if (Infor.trim().equals("GRUP")) {
					if (!sTexto.trim().equals("")) {
						enviarMail(ant_direccion,ant_gerencia,ant_sector,ant_grupo,sTexto);
					}
				}else if (Infor.trim().equals("SECT")) {
					if (!sTexto.trim().equals("")) {
						enviarMail(ant_direccion,ant_gerencia,ant_sector,"",sTexto);
					}
				}else if (Infor.trim().equals("GERE")) {
					if (!sTexto.trim().equals("")) {
						enviarMail(ant_direccion,ant_gerencia,"","",sTexto);
					}
				}else if (Infor.trim().equals("DIRE")) {
					if (!sTexto.trim().equals("")) {
						enviarMail(ant_direccion,"","","",sTexto);
					}
				}

			}	catch (Exception e) {
				grabarLog("EXC: MakeMail: " + e.getMessage());
				e.printStackTrace();
			}
	}


    public void enviarMail(String cod_dire,String cod_gere,String cod_sect,String cod_grup,String mensaje) throws Exception {
        Properties prop = new Properties();
		String Email= (String)"";
		String Nombre= (String)"";
		String Euser= (String)"";
		cstmt1.setString(1,cod_dire);
		cstmt1.setString(2,cod_gere);
		cstmt1.setString(3,cod_sect);
		cstmt1.setString(4,cod_grup);
		try {
			rsRecurso = cstmt1.executeQuery();
		} catch (Exception e) {
			grabarLog("EXC: enviarMail: " + e.getMessage());
			grabarLog("");
			e.printStackTrace();
		}

		if (rsRecurso.next()){
			Euser=clearNull(rsRecurso.getString("euser"));
			Email=clearNull(rsRecurso.getString("email"));
			Nombre=clearNull(rsRecurso.getString("nom_recurso"));
			rsRecurso.close();
		}

		grabarLog(cod_dire + " : " + cod_gere + " : " + cod_sect + " : " + cod_grup + " : " +  Nombre + " : " +   Email);

		if (Email.trim().equals("")) {
			grabarLog("El recurso: " + Nombre + ", no posee email.");
			grabarLog(mensaje);
			grabarLog("");
			return;
		}
		
		prop.put("mail.smtp.host",mailSMTP);
		try {
			mensaje = textoCabezal + mensaje;
			mensaje = mensaje + "_________________________________________________________";
			mensaje = mensaje + " " + String.valueOf((char)13) + String.valueOf((char)10);
			mensaje = mensaje + " " + String.valueOf((char)13) + String.valueOf((char)10);
			mensaje = mensaje + textoAdjunto;

			if (debugMode.trim().equals("SI")) {
				grabarLog(mensaje);
			}

			if (!doMail.equalsIgnoreCase("SI")) {
				return;
			}

			Session sesion = Session.getDefaultInstance(prop, null);
			//sesion.setDebug(true);

			MimeMessage message = new MimeMessage(sesion);
			message.setFrom(new InternetAddress(mailSend));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(Email));
			message.setSubject(mailSubj);
			message.setText(mensaje,"iso-8859-1");
			Transport.send(message);

		}
		catch (Exception e) {
			grabarLog("EXC: enviarMail: " + e.getMessage());
			e.printStackTrace();
		}
    }

	public void cerrar() throws Exception {
		conn.close();
	}

	public static void procesoCancelado() {
		SimpleDateFormat formatterFinal;
		formatterFinal = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
		String finalProceso = formatterFinal.format(realDate(0));
		grabarLog("---------------------------------------------------");
		grabarLog("Proceso Cancelado  --> " + finalProceso);
		grabarLog("===================================================");
	}

	public static void procesoCerrado() {
		SimpleDateFormat formatterFinal;
		formatterFinal = new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
		String finalProceso = formatterFinal.format(realDate(0));
		grabarLog("---------------------------------------------------");
		grabarLog("Proceso Cerrado  --> " + finalProceso);
		grabarLog("===================================================");
	}

	public static void grabarLog(String lineaLog) {
		if (debugMode.trim().equals("SI")) {
			System.out.println(lineaLog);
		}

		try {
			PrintWriter out = new PrintWriter(new FileWriter(pathLog + fileLog, true));
			out.println(lineaLog);
			out.flush();
			out.close();
		}
		catch (java.io.IOException ioe) {
			System.out.println("Error al grabar el archivo de log");
		}
	}

	public void prueba() {
		String xxx;
		try {
			rs = cstmt0.executeQuery();
			while (rs.next()) {
				xxx = rs.getString("msg_texto");
				System.out.println(xxx);
				}
			rs.close();
		}
		catch (Exception e) {
			grabarLog("EXC: prueba: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static java.util.Date realDate(int offsetDay){
		return new java.util.Date(System.currentTimeMillis() + (offsetDay * 86400000) + (3600000 * ajusteGMT));
	}

}
