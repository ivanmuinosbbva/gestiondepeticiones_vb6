#! /bin/ksh
##############
cd /prod/jp00/GesPet
#
proc=$3
log=/prod/jp00/logs/GesPet.$3.$1.$2
usersyb=JP00
servsyb=DSPROD02
passsyb=`grep $servsyb /prod/jp00/general/passw|cut -c12-22`
echo `date '+%Y%m%d.%T'` > /prod/jp00/GesPet/fecha.proc
fecha=`cat fecha.proc`
# Correos
#conso="correo1.consolidar.arg.igrupobbva"
#banco="correo1.arg.igrupobbva"
banco="bbva.com"
#Mail01="tareasux@$banco"
#Mail02="fernando.spitz@$banco"
Mail01="tareasunix-arg@$banco"
Mail02="fernando.spitz.contractor@$banco"
MAILOK="$Mail02"
MAILCAN="$Mail01"
#
# -DBUSR:Usuario Sybase
# -DBPWD:Password Sybase
# -NIVEL:a que nivel se refiere el AREAS (DIRE/GERE/SECT/GRUP)
# -AREAS:Codigo de area
# -INFOR:a que nivel se dispara el informe (DIRE/GERE/SECT/GRUP)
# -PORCM:Porcentaje minimo de carga a partir del cual se informa
# -LOGPT:carpeta en la que deja los detalles de ejecucion
# -LOGFL:nombre de archivo de log
# -DEBUG:SI/NO
# -MAILN:dispara mails SI/NO
# -MLSBJ:Subject del mail pej:GESTION DE PETICIONES
# -MLSND:sender pej:PABLO.GROSSO@MEMO.DOMAIN.TCPIP
# -MLMTP:servidor SMTP pej:gatewaymail.domain.tcpip
# si no se especifican parametros por linea de comando, asume los del .cfg
##############
#
cd /prod/jp00/GesPet
date > $log
#
java -classpath "/prod/jp00/GesPet/j2ee.jar:/prod/jp00/GesPet/jconn2.jar:/prod/jp00/GesPet" petHoras -MAILN:SI -NIVEL:GERE -AREAS:DESA -INFOR:GRUP -PORCM:100 -LOGFL:GesPet.petHoras.log -DBUSR:$usersyb  -DBPWD:$passsyb
if [ $? -ne 0 ]
then
    cat /prod/jp00/logs/GesPet.petHoras.log >> $log
    rm  /prod/jp00/logs/GesPet.petHoras.log
    fin="`date '+%d/%m/%Y %T'`"
    echo "********************************************************************" >>$log
    echo " Hay Errores en la Corrida de $proc                                 " >>$log
    echo " Termina proceso:  $fin                                             " >>$log
    echo "********************************************************************" >>$log
    cat $log | mailx -s "Cancelo proceso $proc - GesPet Banco " "$MAILCAN"
    exit 1
fi
#
cat /prod/jp00/logs/GesPet.petHoras.log  >> $log
rm  /prod/jp00/logs/GesPet.petHoras.log
fin="`date '+%d/%m/%Y %T'`"
echo "*************************************************************************" >>$log
echo " Termina proceso:  $fin                                                  " >>$log
echo "*************************************************************************" >>$log
cat $log | mailx -s "Log de proceso $proc - GesPet Banco " "$MAILOK"
#
date >> $log
cat $log
