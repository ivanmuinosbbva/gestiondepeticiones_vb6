Attribute VB_Name = "boPeticion"
' BOPeticion: objeto de reglas de negocio para peticiones.
'GMT01 - 06/06/2019 - PET 70487 se agrega validacion tecnica DATA

Option Explicit

Public Sub ControlesPasajeProduccion(ByVal sClasePet As String, ByVal EstadoSolicitud As String, bPeticionEstuvoEnEjecucion As Boolean)
    Const VALIDACION_ARQUITECTURA = 1
    Const VALIDACION_SEGURIDADINFORMATICA = 2
    Const VALIDACION_DATA = 3  'GMT01

    Dim docAlcance As Boolean                               ' P950/C100
    Dim okAlcance As Boolean                                ' ALCA/ALCP
    Dim cambioAlcance As Boolean                            ' CG04
    Dim okCambioAlcance As Boolean                          '
    Dim okMailAlcance As Boolean                            ' EML1
    Dim okMailUsuario As Boolean                            ' EML2
    Dim C204 As Boolean                                     ' Pruebas unitarias/sistema
    Dim T710 As Boolean                                     ' Aplicaci�n de principios de desarrollo
    Dim okUsuario As Boolean                                ' TEST/TESP
    Dim okSupervisor As Boolean                             ' OKPP/OKPF
    Dim bValidacionTecnica As Boolean                       ' Indica si est� habilitada la configuraci�n para requerir la validaci�n t�cnica en el sistema.
    Dim bValidacionTecnicaArquitectura As Boolean           ' La validaci�n t�cnica del �rea de Arquitectura fu� completada por el referente
    Dim bValidacionTecnicaSegInf As Boolean                 ' La validaci�n t�cnica del �rea de Seg. Inform�tica fu� completada por el referente
    Dim bValidacionTecnicaData As Boolean           'GMT01  ' La validaci�n t�cnica del �rea de DATA. fu� completada por el referente
    
    Dim bValidacionArquitecturaRequiereConforme As Boolean  ' Por las respuestas indicadas, se requiere el conforme de Arquitectura
    Dim bValidacionSegInfRequiereConforme As Boolean        ' Por las respuestas indicadas, se requiere el conforme de Seg. Inform�tica.
    Dim bValidacionDataRequiereConforme As Boolean  'GMT01  ' Por las respuestas indicadas, se requiere el conforme de Seg. Inform�tica.
 
    Dim okArquitectura As Boolean                           ' OK de arquitectura (Conforme, Conforme c/ observaciones y no conforme)
    Dim okSegInf As Boolean                                 ' OK de Seg. Inf. (Conforme, Conforme c/ observaciones y no conforme)
    Dim OMA As Boolean
    Dim OME As Boolean
    Dim SOB As Boolean
    Dim bOKLider As Boolean
    Dim bOKPasajeProduccion As Boolean

    bOKLider = False
    bValidacionTecnica = False
    
    If sp_GetVarios("CTRLPET1") Then
        If ClearNull(aplRST.Fields!var_numero) = "1" Then
            bValidacionTecnica = True
        End If
    End If

'    ' TODO: 14.10.16: Este flag habr�a que obtenerlo una sola vez al principio.
'    If sp_GetVarios("CTRLPRD1") Then
'        If Not aplRST.EOF Then
'            If ClearNull(aplRST.Fields!var_numero) = 1 Then
'                bOKLider_Homologacion = True
'            End If
'        End If
'    End If
    
'NO HARIA FALTA
'    ' 1. Cargo todos los conformes que tiene la petici�n
'    If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
'        Do While Not aplRST.EOF
'            sConformes = sConformes & ClearNull(aplRST.Fields!ok_tipo) & "|"
'            aplRST.MoveNext
'            DoEvents
'        Loop
'    End If

    bOKPasajeProduccion = False                 ' Flag principal que habilita el pasaje a producci�n
    
    If sp_GetAdjuntosPet(glNumeroPeticion, Null, Null, Null) Then
        Do While Not aplRST.EOF
            Select Case Mid(ClearNull(aplRST.Fields!adj_tipo), 1, 4)
                Case "C100", "P950": docAlcance = True
                Case "C204": C204 = True
                Case "CG04": cambioAlcance = True
                Case "EML1": okMailAlcance = True
                Case "EML2": okMailUsuario = True
                Case "T710": T710 = True
            End Select
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    If sp_GetPeticionConf(glNumeroPeticion, Null, Null) Then
        Do While Not aplRST.EOF
            Select Case Mid(ClearNull(aplRST.Fields!ok_tipo), 1, 4)
                Case "ALCA", "ALCP": okAlcance = True
                Case "TESP", "TEST"
                    If ClearNull(aplRST.Fields!ok_modo) = "ALT" Then
                        bOKLider = True
                    Else
                        okUsuario = True
                    End If
                Case "OKPF", "OKPP": okSupervisor = True
                Case "HOMA": OMA = True
                Case "HOME": OME = True
                Case "HSOB": SOB = True
                Case "SIOK", "SIOB", "SIRE": okSegInf = True
                Case "AROK", "AROB", "ARRE": okArquitectura = True
            End Select
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    If bValidacionTecnica Then
        If InStr(1, "NUEV|EVOL|", sClasePet, vbTextCompare) > 0 Then        ' Requiere validaci�n t�cnica por la case
            bValidacionTecnicaArquitectura = True
            bValidacionArquitecturaRequiereConforme = False
            
            ' Completitud validaci�n t�cnica Arquitectura
            If sp_GetPeticionValidacion(glNumeroPeticion, VALIDACION_ARQUITECTURA, Null) Then
                Do While Not aplRST.EOF
                    If ClearNull(aplRST.Fields!valitemvalor) = "" Then
                        bValidacionTecnicaArquitectura = False
                        Exit Do
                    Else
                        If ClearNull(aplRST.Fields!valitemvalor) = "S" Then
                            bValidacionArquitecturaRequiereConforme = True
                        End If
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            
            bValidacionTecnicaSegInf = True
            bValidacionSegInfRequiereConforme = False
            If sp_GetPeticionValidacion(glNumeroPeticion, VALIDACION_SEGURIDADINFORMATICA, Null) Then
                Do While Not aplRST.EOF
                    If ClearNull(aplRST.Fields!valitemvalor) = "" Then
                        bValidacionTecnicaSegInf = False
                        Exit Do
                    Else
                        If ClearNull(aplRST.Fields!valitemvalor) = "S" Then
                            bValidacionSegInfRequiereConforme = True
                        End If
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            
            'GMT01 - INI
            bValidacionTecnicaData = True
            bValidacionDataRequiereConforme = False
            If sp_GetPeticionValidacion(glNumeroPeticion, VALIDACION_DATA, Null) Then
                Do While Not aplRST.EOF
                    If ClearNull(aplRST.Fields!valitemvalor) = "" Then
                        bValidacionTecnicaData = False
                        Exit Do
                    Else
                        If ClearNull(aplRST.Fields!valitemvalor) = "S" Then
                            bValidacionDataRequiereConforme = True
                        End If
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            'GMT01 - FIN
        End If
    End If
    
    ' NUEVA VALIDACION
    Select Case sClasePet
        Case "NUEV", "EVOL"
            ' Tiene documento de alcance y ok al alcance
            If docAlcance And okAlcance Then
                ' Tiene cambio de alcance y ok al cambio de alcance
                If cambioAlcance Then
                    If okCambioAlcance Then
                        
                    End If
                End If
                If bPeticionEstuvoEnEjecucion Then
                    If C204 Then
                        If T710 Then
                            If SOB Or OME Then
                                If Not OMA Or (OMA And okSupervisor) Then
                                    bOKPasajeProduccion = True
                                    ' Si est� habilitado el control de habilitaci�n para pasaje a producci�n
                                    ' desde la configuraci�n, entonces se hace la validaci�n
                                    If bValidacionTecnica Then
                                        If bValidacionArquitecturaRequiereConforme Then
                                            bOKPasajeProduccion = False
                                            If bValidacionTecnicaArquitectura And okArquitectura Then
                                                bOKPasajeProduccion = True
                                            End If
                                        End If
                                        If bValidacionSegInfRequiereConforme Then
                                            bOKPasajeProduccion = False
                                            If bValidacionTecnicaSegInf And okSegInf Then
                                                bOKPasajeProduccion = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Case "OPTI"
            If docAlcance Then          ' Opcional
                If okAlcance Then
                    
                End If
                If cambioAlcance And okCambioAlcance Then
                    
                End If
            End If
            If bPeticionEstuvoEnEjecucion Then
                If C204 Then
                    If (okUsuario Or (bOKLider And okMailUsuario)) And (SOB Or OME) Then
                        If Not OMA Or (OMA And okSupervisor) Then
                            bOKPasajeProduccion = True
                        End If
                    End If
                End If
            End If
        Case "ATEN"
            If bPeticionEstuvoEnEjecucion Then
                If okUsuario Or (bOKLider And okMailUsuario) Then
                    If Not OMA Or (OMA And okSupervisor) Then
                        bOKPasajeProduccion = True
                    End If
                End If
            End If
        Case Else
            ' Correctivos, spufis
            If bPeticionEstuvoEnEjecucion Then
                If okUsuario Or (bOKLider And okMailUsuario And (SOB Or OME)) Then
                    If Not OMA Or (OMA And okSupervisor) Then
                        bOKPasajeProduccion = True
                    End If
                End If
            End If
    End Select
    If bOKPasajeProduccion Then
        Call Habilitar_PasajeProduccion(glNumeroPeticion, glNroAsignado, sClasePet, "", "", glLOGIN_ID_REAL, "02")
    Else
        Call sp_DeletePeticionChangeMan(glNumeroPeticion)
        Call sp_DeletePeticionEnviadas(glNumeroPeticion)
    End If
End Sub

Public Sub AgregarHomologacion(glNumeroPeticion, petClase, sEstado, SectorHomo, GrupoHomo)
    Dim cSectorHomologadorEstado As String
    Dim NroHistorial As Long
    ' Precondiciones:
    ' 1. No existe el grupo homologador en la petici�n (no sabemos si existe el sector del grupo homologador)
    If Not sp_GetPeticionSector(glNumeroPeticion, SectorHomo) Then
        ' Si no existe el sector del grupo homologador, se agrega en estado incial "A estimar esfuerzo"
        Call sp_InsertPeticionSector(glNumeroPeticion, SectorHomo, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
        cSectorHomologadorEstado = "ESTIMA"
    Else
        ' Si ya existe el sector del grupo homologador, averiguo el estado actual para mantenerlo
        cSectorHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
    End If
    Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, GrupoHomo, "ESTIMA", "", "")                                                  ' Se envian los mensajes correspondientes a Responsables de Grupo
    'Call Status("Actualizando el historial...")
    Select Case petClase
        Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", sEstado, SectorHomo, cSectorHomologadorEstado, GrupoHomo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso REDUCIDO) �")
        Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(glNumeroPeticion, "PAUTHOM", sEstado, SectorHomo, cSectorHomologadorEstado, GrupoHomo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso COMPLETO) �")
    End Select
    Call sp_InsertPeticionGrupo(glNumeroPeticion, GrupoHomo, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")
    'Call sp_UpdatePeticionGrupo(glNumeroPeticion, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")
End Sub

Public Sub ReactivarHomologacion(glNumeroPeticion, petClase, sEstado, SectorHomo, GrupoHomo)
    Dim cSectorHomologadorEstado As String
    Dim NroHistorial As Long
    Dim bComprobar As Boolean
    
    ' Precondiciones:
    ' 1. Ya existe el grupo homologador en la petici�n
    If sp_GetPeticionSector(glNumeroPeticion, SectorHomo) Then
        cSectorHomologadorEstado = ClearNull(aplRST.Fields!cod_estado)
    End If
    Select Case petClase
        Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", sEstado, SectorHomo, cSectorHomologadorEstado, GrupoHomo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & "Reactivaci�n autom�tica del grupo Homologador por cambio de clase: 1� Punto de Control (Proceso REDUCIDO) �")
        Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(glNumeroPeticion, "GCHGEST", sEstado, SectorHomo, cSectorHomologadorEstado, GrupoHomo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & "Reactivaci�n autom�tica del grupo Homologador por cambio de clase: 1� Punto de Control (Proceso COMPLETO) �")
    End Select
    ' Si homologaci�n se encuentra en alguno de los estados terminales, debo reactivarlo
    Call sp_UpdatePetSubField(glNumeroPeticion, GrupoHomo, "ESTADO", "ESTIMA", Null, Null)
    Call sp_UpdatePeticionGrupo(glNumeroPeticion, GrupoHomo, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")
        
    ' Si no existen otros grupos adem�s del grupo homologador, le fuerzo el estado del sector al mismo que el grupo Homologador
    bComprobar = False
    If sp_GetPeticionGrupo(glNumeroPeticion, SectorHomo, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!cod_grupo) <> GrupoHomo Then
                bComprobar = True
                Exit Do
            End If
            aplRST.MoveNext
            DoEvents
        Loop
        If Not bComprobar Then
            Call sp_UpdatePeticionSector(glNumeroPeticion, SectorHomo, Null, Null, Null, Null, 0, "ESTIMA", date, "", NroHistorial, "0")
        End If
    End If
End Sub
