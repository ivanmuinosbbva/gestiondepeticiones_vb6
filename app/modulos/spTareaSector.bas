Attribute VB_Name = "spTareaSector"
Option Explicit

Function sp_GetTareaSector(cod_tarea, cod_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetTareaSector"
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetTareaSector = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTareaSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetTareaSectorXt(cod_tarea, cod_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetTareaSectorXt"
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetTareaSectorXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTareaSectorXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function


Function sp_InsertTareaSector(cod_tarea, cod_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertTareaSector"
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      Set aplRST = .Execute
    End With
    sp_InsertTareaSector = True
'    If Not IsEmpty(aplRST) And (Not aplRST.EOF) And aplRST(0).Name = "ErrCode" Then
'        MsgBox (AnalizarErrorSQL(aplRST, 30000))
'        sp_UpdateTareaSector = False
'    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertTareaSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteTareaSector(cod_tarea, cod_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteTareaSector"
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      Set aplRST = .Execute
    End With
    sp_DeleteTareaSector = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteTareaSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

