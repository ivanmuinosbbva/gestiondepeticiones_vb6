Attribute VB_Name = "spPlanMotivosReplan"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertMotivosReplan(cod_motivo_replan, nom_motivo_replan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertMotivosReplan"
                .Parameters.Append .CreateParameter("@cod_motivo_replan", adInteger, adParamInput, , cod_motivo_replan)
                .Parameters.Append .CreateParameter("@nom_motivo_replan", adChar, adParamInput, , nom_motivo_replan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_InsertMotivosReplan = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertMotivosReplan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateMotivosReplan(cod_motivo_replan, nom_motivo_replan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateMotivosReplan"
                .Parameters.Append .CreateParameter("@cod_motivo_replan", adInteger, adParamInput, , cod_motivo_replan)
                .Parameters.Append .CreateParameter("@nom_motivo_replan", adChar, adParamInput, , nom_motivo_replan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_UpdateMotivosReplan = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateMotivosReplan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateMotivosReplanField(x, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateMotivosReplanField"
                .Parameters.Append .CreateParameter("@x", adInteger, adParamInput, , CLng(Val(x)))
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateMotivosReplanField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateMotivosReplanField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteMotivosReplan(cod_motivo_replan) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteMotivosReplan"
                .Parameters.Append .CreateParameter("@cod_motivo_replan", adInteger, adParamInput, , cod_motivo_replan)
                Set aplRST = .Execute
        End With
        sp_DeleteMotivosReplan = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteMotivosReplan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetMotivosReplan(cod_motivo_replan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
            .ActiveConnection = aplCONN
            .CommandType = adCmdStoredProc
            .CommandText = "sp_GetMotivosReplan"
            .Parameters.Append .CreateParameter("@cod_motivo_replan", adInteger, adParamInput, , cod_motivo_replan)
            .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, 1, estado_hab)
            Set aplRST = .Execute
        End With
        If Not aplRST.EOF Then
            sp_GetMotivosReplan = True
        End If
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_GetMotivosReplan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

