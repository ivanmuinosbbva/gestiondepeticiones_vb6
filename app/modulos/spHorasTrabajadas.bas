Attribute VB_Name = "spHorasTrabajadas"
Option Explicit

Function sp_GetUnaHorasTrabajadas(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnaHorasTrabajadas"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, ClearNull(cod_tarea))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      Set aplRST = .Execute
    End With
    sp_GetUnaHorasTrabajadas = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaHorasTrabajadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHorasTrabajadas(cod_recurso, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHorasTrabajadas"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetHorasTrabajadas = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHorasTrabajadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHorasTrabajadasXt(cod_recurso, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .CommandTimeout = 120
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHorasTrabajadasXt"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetHorasTrabajadasXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHorasTrabajadasXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHorasTrabajadas(cod_recurso, cod_tarea_a, pet_nrointerno_a, fe_desde_a, fe_hasta_a, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, trabsinasignar, observaciones, tipo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateHorasTrabajadas"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_tarea_a", adChar, adParamInput, 8, ClearNull(cod_tarea_a))
      .Parameters.Append .CreateParameter("@pet_nrointerno_a", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno_a))))
      .Parameters.Append .CreateParameter("@fe_desde_a", SQLDdateType, adParamInput, , fe_desde_a)
      .Parameters.Append .CreateParameter("@fe_hasta_a", SQLDdateType, adParamInput, , fe_hasta_a)
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, ClearNull(cod_tarea))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      '.Parameters.Append .CreateParameter("@horas", adSmallInt, adParamInput, , CInt(horas))
      .Parameters.Append .CreateParameter("@horas", adInteger, adParamInput, , CInt(horas))
      .Parameters.Append .CreateParameter("@trabsinasignar", adChar, adParamInput, 1, trabsinasignar)
      .Parameters.Append .CreateParameter("@observaciones", adChar, adParamInput, 255, observaciones)
      .Parameters.Append .CreateParameter("@tipo", adInteger, adParamInput, , CInt(tipo))
      Set aplRST = .Execute
    End With
    sp_UpdateHorasTrabajadas = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHorasTrabajadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertHorasTrabajadas(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, trabsinasignar, observaciones, tipo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertHorasTrabajadas"
      '.CommandTimeout
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, ClearNull(cod_tarea))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      '.Parameters.Append .CreateParameter("@horas", adSmallInt, adParamInput, , CInt(horas))
      .Parameters.Append .CreateParameter("@tipo", adInteger, adParamInput, , CInt(horas))
      .Parameters.Append .CreateParameter("@trabsinasignar", adChar, adParamInput, 1, trabsinasignar)
      .Parameters.Append .CreateParameter("@observaciones", adChar, adParamInput, 255, observaciones)
      .Parameters.Append .CreateParameter("@tipo", adInteger, adParamInput, , CInt(tipo))
      Set aplRST = .Execute
    End With
    sp_InsertHorasTrabajadas = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN)), vbExclamation
    sp_InsertHorasTrabajadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteHorasTrabajadas(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteHorasTrabajadas"
      .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      Set aplRST = .Execute
    End With
    sp_DeleteHorasTrabajadas = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteHorasTrabajadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_rptHorasTrabajadasRecurso(cod_recurso, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_rptHorasTrabajadasRecurso"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_rptHorasTrabajadasRecurso = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_rptHorasTrabajadasRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_rptHorasTrabajadasRecurso2(cod_recurso, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_rptHorasTrabajadasRecurso2"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_rptHorasTrabajadasRecurso2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_rptHorasTrabajadasRecurso2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_VerPeticionHoras(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
'      .CommandType = adCmdText
      .CommandText = "sp_VerPeticionHoras"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
      Set aplRST = .Execute
    End With
    sp_VerPeticionHoras = False
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    If Not (aplRST.EOF) Then
        sp_VerPeticionHoras = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_VerPeticionHoras = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHorasTrabajadasXtC(opcion, cod_recurso, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .CommandTimeout = 120
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHorasTrabajadasXtC"
        .Parameters.Append .CreateParameter("@opcion", adChar, adParamInput, 1, opcion)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetHorasTrabajadasXtC = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHorasTrabajadasXtC = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

