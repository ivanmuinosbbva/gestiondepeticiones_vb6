Attribute VB_Name = "spRecursoPerfilAudit"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertRecursoPerfilAudit(cod_recurso, cod_perfil, cod_ope, cod_nivel, cod_area, auditfch, auditusr) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
            .ActiveConnection = aplCONN
            .CommandType = adCmdStoredProc
            .CommandText = "sp_InsertRecursoPerfilAudit"
            .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
            .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
            .Parameters.Append .CreateParameter("@cod_ope", adInteger, adParamInput, , cod_ope)
            .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
            .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
            .Parameters.Append .CreateParameter("@auditfch", SQLDdateType, adParamInput, , auditfch)
            .Parameters.Append .CreateParameter("@auditusr", adChar, adParamInput, 10, auditusr)
            Set aplRST = .Execute
    End With
    sp_InsertRecursoPerfilAudit = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertRecursoPerfilAudit = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRecursoPerfilAudit(cod_recurso, cod_perfil, cod_ope, cod_nivel, cod_area, auditfch, auditusr) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
            .ActiveConnection = aplCONN
            .CommandType = adCmdStoredProc
            .CommandText = "sp_UpdateRecursoPerfilAudit"
            .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
            .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
            .Parameters.Append .CreateParameter("@cod_ope", adInteger, adParamInput, , cod_ope)
            .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
            .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
            .Parameters.Append .CreateParameter("@auditfch", SQLDdateType, adParamInput, , auditfch)
            .Parameters.Append .CreateParameter("@auditusr", adChar, adParamInput, 10, auditusr)
            Set aplRST = .Execute
    End With
    sp_UpdateRecursoPerfilAudit = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateRecursoPerfilAudit = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteRecursoPerfilAudit(cod_recurso, cod_perfil, cod_ope) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
            .ActiveConnection = aplCONN
            .CommandType = adCmdStoredProc
            .CommandText = "sp_DeleteRecursoPerfilAudit"
            .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
            .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
            .Parameters.Append .CreateParameter("@cod_ope", adInteger, adParamInput, , cod_ope)
            Set aplRST = .Execute
    End With
    sp_DeleteRecursoPerfilAudit = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteRecursoPerfilAudit = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRecursoPerfilAudit(cod_recurso, cod_perfil, cod_ope) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetRecursoPerfilAudit"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
            sp_GetRecursoPerfilAudit = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoPerfilAudit = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

