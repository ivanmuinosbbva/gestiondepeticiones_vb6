Attribute VB_Name = "spEmpresa"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertEmpresa(empid, empnom, emphab, empabrev) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertEmpresa"
        .Parameters.Append .CreateParameter("@empid", adInteger, adParamInput, , empid)
        .Parameters.Append .CreateParameter("@empnom", adChar, adParamInput, 50, empnom)
        .Parameters.Append .CreateParameter("@emphab", adChar, adParamInput, 1, emphab)
        .Parameters.Append .CreateParameter("@empabrev", adChar, adParamInput, 30, empabrev)
        Set aplRST = .Execute
    End With
    sp_InsertEmpresa = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertEmpresa = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateEmpresa(empid, empnom, emphab, empabrev) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateEmpresa"
        .Parameters.Append .CreateParameter("@empid", adInteger, adParamInput, , empid)
        .Parameters.Append .CreateParameter("@empnom", adChar, adParamInput, 50, empnom)
        .Parameters.Append .CreateParameter("@emphab", adChar, adParamInput, 1, emphab)
        .Parameters.Append .CreateParameter("@empabrev", adChar, adParamInput, 30, empabrev)
        Set aplRST = .Execute
    End With
    sp_UpdateEmpresa = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateEmpresa = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteEmpresa(empid) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteEmpresa"
        .Parameters.Append .CreateParameter("@empid", adInteger, adParamInput, , empid)
        Set aplRST = .Execute
    End With
    sp_DeleteEmpresa = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteEmpresa = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetEmpresa(empid, empnom) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetEmpresa"
        .Parameters.Append .CreateParameter("@empid", adInteger, adParamInput, , empid)
        .Parameters.Append .CreateParameter("@empnom", adChar, adParamInput, 50, empnom)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetEmpresa = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetEmpresa = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
