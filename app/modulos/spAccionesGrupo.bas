Attribute VB_Name = "spAccionesGrupo"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertAccionesGrupo(accionGrupoId, accionGrupoNom, accionGrupoHab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertAccionesGrupo"
                .Parameters.Append .CreateParameter("@accionGrupoId", adInteger, adParamInput, , accionGrupoId)
                .Parameters.Append .CreateParameter("@accionGrupoNom", adChar, adParamInput, 50, accionGrupoNom)
                .Parameters.Append .CreateParameter("@accionGrupoHab", adChar, adParamInput, 1, accionGrupoHab)
                Set aplRST = .Execute
        End With
        sp_InsertAccionesGrupo = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_InsertAccionesGrupo = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateAccionesGrupo(accionGrupoId, accionGrupoNom, accionGrupoHab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateAccionesGrupo"
                .Parameters.Append .CreateParameter("@accionGrupoId", adInteger, adParamInput, , accionGrupoId)
                .Parameters.Append .CreateParameter("@accionGrupoNom", adChar, adParamInput, 50, accionGrupoNom)
                .Parameters.Append .CreateParameter("@accionGrupoHab", adChar, adParamInput, 1, accionGrupoHab)
                Set aplRST = .Execute
        End With
        sp_UpdateAccionesGrupo = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateAccionesGrupo = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteAccionesGrupo(accionGrupoId) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteAccionesGrupo"
                .Parameters.Append .CreateParameter("@accionGrupoId", adInteger, adParamInput, , accionGrupoId)
                Set aplRST = .Execute
        End With
        sp_DeleteAccionesGrupo = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteAccionesGrupo = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetAccionesGrupo(accionGrupoId, accionGrupoNom, accionGrupoHab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetAccionesGrupo"
        .Parameters.Append .CreateParameter("@accionGrupoId", adInteger, adParamInput, , IIf(IsNull(accionGrupoId), Null, accionGrupoId))
        .Parameters.Append .CreateParameter("@accionGrupoNom", adChar, adParamInput, 50, IIf(IsNull(accionGrupoNom), Null, accionGrupoNom))
        .Parameters.Append .CreateParameter("@accionGrupoHab", adChar, adParamInput, 1, IIf(IsNull(accionGrupoHab), Null, accionGrupoHab))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetAccionesGrupo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAccionesGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

