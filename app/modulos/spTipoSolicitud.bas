Attribute VB_Name = "spTipoSolicitud"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertTipoSolicitud(cod_tipo, nom_tipo, dsc_tipo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertTipoSolicitud"
        .Parameters.Append .CreateParameter("@cod_tipo", adChar, adParamInput, 1, cod_tipo)
        .Parameters.Append .CreateParameter("@nom_tipo", adChar, adParamInput, 20, nom_tipo)
        .Parameters.Append .CreateParameter("@dsc_tipo", adChar, adParamInput, 50, dsc_tipo)
        Set aplRST = .Execute
    End With
    sp_InsertTipoSolicitud = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertTipoSolicitud = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateTipoSolicitud(cod_tipo, nom_tipo, dsc_tipo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateTipoSolicitud"
        .Parameters.Append .CreateParameter("@cod_tipo", adChar, adParamInput, 1, cod_tipo)
        .Parameters.Append .CreateParameter("@nom_tipo", adChar, adParamInput, 20, nom_tipo)
        .Parameters.Append .CreateParameter("@dsc_tipo", adChar, adParamInput, 50, dsc_tipo)
        Set aplRST = .Execute
    End With
    sp_UpdateTipoSolicitud = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateTipoSolicitud = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteTipoSolicitud(cod_tipo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteTipoSolicitud"
        .Parameters.Append .CreateParameter("@cod_tipo", adChar, adParamInput, 1, cod_tipo)
        Set aplRST = .Execute
    End With
    sp_DeleteTipoSolicitud = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteTipoSolicitud = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetTipoSolicitud(cod_tipo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetTipoSolicitud"
        .Parameters.Append .CreateParameter("@cod_tipo", adChar, adParamInput, 1, IIf(ClearNull(cod_tipo) = "", Null, cod_tipo))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetTipoSolicitud = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTipoSolicitud = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

