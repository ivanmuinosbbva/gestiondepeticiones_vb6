Attribute VB_Name = "spPlanMotivosSuspen"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertMotivosSuspen(cod_motivo_suspen, nom_motivo_suspen, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertMotivosSuspen"
                .Parameters.Append .CreateParameter("@cod_motivo_suspen", adInteger, adParamInput, , cod_motivo_suspen)
                .Parameters.Append .CreateParameter("@nom_motivo_suspen", adChar, adParamInput, , nom_motivo_suspen)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_InsertMotivosSuspen = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertMotivosSuspen = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateMotivosSuspen(cod_motivo_suspen, nom_motivo_suspen, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateMotivosSuspen"
                .Parameters.Append .CreateParameter("@cod_motivo_suspen", adInteger, adParamInput, , cod_motivo_suspen)
                .Parameters.Append .CreateParameter("@nom_motivo_suspen", adChar, adParamInput, , nom_motivo_suspen)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_UpdateMotivosSuspen = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateMotivosSuspen = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateMotivosSuspenField(x, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateMotivosSuspenField"
                .Parameters.Append .CreateParameter("@x", adInteger, adParamInput, , CLng(Val(x)))
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateMotivosSuspenField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateMotivosSuspenField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteMotivosSuspen(cod_motivo_suspen) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteMotivosSuspen"
                .Parameters.Append .CreateParameter("@cod_motivo_suspen", adInteger, adParamInput, , cod_motivo_suspen)
                Set aplRST = .Execute
        End With
        sp_DeleteMotivosSuspen = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteMotivosSuspen = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetMotivosSuspen(cod_motivo_suspen, estado_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetMotivosSuspen"
        .Parameters.Append .CreateParameter("@cod_motivo_suspen", adInteger, adParamInput, , cod_motivo_suspen)
        .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, 1, estado_hab)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetMotivosSuspen = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetMotivosSuspen = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

