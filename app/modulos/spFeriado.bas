Attribute VB_Name = "spFeriado"
Option Explicit

Function sp_GetFeriado2(FechaDesde, FechaHasta) As Boolean
    On Error Resume Next
    Dim oRstAux As ADODB.Recordset
    Set oRstAux = New ADODB.Recordset
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetFeriado"
      .Parameters.Append .CreateParameter("@fechadesde", SQLDdateType, adParamInput, , FechaDesde)
      .Parameters.Append .CreateParameter("@fechahasta", SQLDdateType, adParamInput, , FechaHasta)
      Set oRstAux = .Execute
    End With
    If Not (oRstAux.EOF) Then
        sp_GetFeriado2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    Set oRstAux = Nothing       '
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(oRstAux, Err, aplCONN))
    sp_GetFeriado2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetFeriado(FechaDesde, FechaHasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetFeriado"
      .Parameters.Append .CreateParameter("@fechadesde", SQLDdateType, adParamInput, , FechaDesde)
      .Parameters.Append .CreateParameter("@fechahasta", SQLDdateType, adParamInput, , FechaHasta)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetFeriado = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetFeriado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertFeriado(fecha) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertFeriado"
      .Parameters.Append .CreateParameter("@fecha", SQLDdateType, adParamInput, , fecha)
      Set aplRST = .Execute
    End With
    sp_InsertFeriado = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertFeriado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteFeriado(aaaamm) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteFeriado"
      .Parameters.Append .CreateParameter("aaaamm", adChar, adParamInput, 6, aaaamm)
      Set aplRST = .Execute
    End With
    sp_DeleteFeriado = True
    'If Not IsEmpty(aplRST) And (Not aplRST.EOF) And aplRST(0).Name = "ErrCode" Then
    '    MsgBox (AnalizarErrorSQL(aplRST, 30000))
    '    sp_DeleteFeriado = False
    'End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteFeriado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

