Attribute VB_Name = "spPeticionPlantxt"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

'Function sp_InsertPeticionPlantxt(per_nrointerno, pet_nrointerno, cod_grupo, mem_campo, mem_secuencia, mem_texto) As Boolean
'        On Error Resume Next
'        aplRST.Close
'        Set aplRST = Nothing
'        On Error GoTo Err_SP
'        Dim objCommand As ADODB.Command
'        Set objCommand = New ADODB.Command
'        With objCommand
'                .ActiveConnection = aplCONN
'                .CommandType = adCmdStoredProc
'                .CommandText = "sp_InsertPeticionPlantxt"
'                .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , per_nrointerno)
'                .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'                .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'                .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
'                .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
'                .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255, mem_texto)
'                Set aplRST = .Execute
'        End With
'        sp_InsertPeticionPlantxt = True
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'        On Error GoTo 0
'Exit Function
'Err_SP:
'        MsgBox (AnalizarErrorSQL(aplRST, Err))
'        sp_InsertPeticionPlantxt = False
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'End Function
'
'Function sp_UpdatePeticionPlantxt(per_nrointerno, pet_nrointerno, cod_grupo, mem_campo, mem_secuencia, mem_texto) As Boolean
'        On Error Resume Next
'        aplRST.Close
'        Set aplRST = Nothing
'        On Error GoTo Err_SP
'        Dim objCommand As ADODB.Command
'        Set objCommand = New ADODB.Command
'        With objCommand
'                .ActiveConnection = aplCONN
'                .CommandType = adCmdStoredProc
'                .CommandText = "sp_UpdatePeticionPlantxt"
'                .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , per_nrointerno)
'                .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'                .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'                .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
'                .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
'                .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255, mem_texto)
'                Set aplRST = .Execute
'        End With
'        sp_UpdatePeticionPlantxt = True
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'        On Error GoTo 0
'Exit Function
'Err_SP:
'        MsgBox (AnalizarErrorSQL(aplRST, Err))
'        sp_UpdatePeticionPlantxt = False
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'End Function
'
'Function sp_UpdatePlanMemo(per_nrointerno, pet_nrointerno, cod_grupo, NombreCampo, CampoTexto) As Boolean
'    Dim bFlg As Boolean
'    Dim Secuencia As Integer
'    Dim TmpTexto As String
'    Dim Resto As Integer
'    Dim rsAux As ADODB.Recordset
'    Dim iVersion As Integer
'    Dim dFechaHora As Date
'
'    Set rsAux = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_UpdatePeticionPlantxt"
'      .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1)
'      .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput)
'      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput)
'      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8)
'      .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10)
'      .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput)
'      .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255)
'    End With
'
'    sp_UpdatePlanMemo = True
'    bFlg = True
'    Secuencia = 0
'    Do While bFlg = True
'        TmpTexto = Left(CampoTexto, 255)
'        With objCommand
'            If Len(CampoTexto) > 255 Then
'                .Parameters(0).value = "N"
'            Else
'                bFlg = False
'                .Parameters(0).value = "S"
'            End If
'            .Parameters(1).value = CLng(Val(per_nrointerno))
'            .Parameters(2).value = CLng(Val(pet_nrointerno))
'            .Parameters(3).value = cod_grupo
'            .Parameters(4).value = NombreCampo
'            .Parameters(5).value = CInt(Secuencia)
'            .Parameters(6).value = TmpTexto & ""
'            Set rsAux = .Execute
'        End With
'        Secuencia = Secuencia + 1
'        CampoTexto = Mid(CampoTexto, 256)
'    Loop
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(rsAux, Err, aplCONN))
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    sp_UpdatePlanMemo = False
'End Function
'
'Function sp_DeletePeticionPlantxt(per_nrointerno, pet_nrointerno, cod_grupo, mem_campo, mem_secuencia, mem_texto) As Boolean
'        On Error Resume Next
'        aplRST.Close
'        Set aplRST = Nothing
'        On Error GoTo Err_SP
'        Dim objCommand As ADODB.Command
'        Set objCommand = New ADODB.Command
'        With objCommand
'                .ActiveConnection = aplCONN
'                .CommandType = adCmdStoredProc
'                .CommandText = "sp_DeletePeticionPlantxt"
'                .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , per_nrointerno)
'                .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'                .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 2, cod_grupo)
'                .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
'                .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
'                Set aplRST = .Execute
'        End With
'        sp_DeletePeticionPlantxt = True
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'        On Error GoTo 0
'Exit Function
'Err_SP:
'        MsgBox (AnalizarErrorSQL(aplRST, Err))
'        sp_DeletePeticionPlantxt = False
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'End Function
'
'Function sp_GetPeticionPlantxt(per_nrointerno, pet_nrointerno, cod_grupo, mem_campo, mem_secuencia) As Boolean
'        On Error Resume Next
'        aplRST.Close
'        Set aplRST = Nothing
'        On Error GoTo Err_SP
'        Dim objCommand As ADODB.Command
'        Set objCommand = New ADODB.Command
'        With objCommand
'                .ActiveConnection = aplCONN
'                .CommandType = adCmdStoredProc
'                .CommandText = "sp_GetPeticionPlantxt"
'                .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , per_nrointerno)
'                .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'                .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'                .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
'                .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
'                Set aplRST = .Execute
'        End With
'        sp_GetPeticionPlantxt = True
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'        On Error GoTo 0
'Exit Function
'Err_SP:
'        MsgBox (AnalizarErrorSQL(aplRST, Err))
'        sp_GetPeticionPlantxt = False
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'End Function
'
'Function sp_GetPlanMemo(per_nrointerno, pet_nrointerno, cod_grupo, NombreCampo) As String
'    Dim bGetMemo As Boolean
'    Dim sGetMemo As String
'    sGetMemo = ""
'    On Error Resume Next
'
'    aplRST.Close
'    Set aplRST = Nothing
'
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPeticionPlantxt"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = "-", 0, per_nrointerno))
'        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'        .Parameters.Append .CreateParameter("@cod_grupo", adVarChar, adParamInput, 8, cod_grupo)
'        .Parameters.Append .CreateParameter("@mem_campo", adVarChar, adParamInput, 10, NombreCampo)
'        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , Null)
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        bGetMemo = True
'    End If
'    If bGetMemo Then
'        Do While Not aplRST.EOF
'            sGetMemo = sGetMemo & ClearNull(aplRST.Fields!mem_texto)
'            aplRST.MoveNext
'        Loop
'        aplRST.Close
'    End If
'
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'    sp_GetPlanMemo = sGetMemo
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    sp_GetPlanMemo = ""
'End Function
'
'Function sp_GetPlanMemo2(per_nrointerno, pet_nrointerno, cod_grupo, NombreCampo) As String
'    Dim bGetMemo As Boolean
'    Dim sGetMemo As String
'    sGetMemo = ""
'    On Error Resume Next
'
'    aplRST1.Close
'    Set aplRST1 = Nothing
'
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPeticionPlantxt"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , per_nrointerno)
'        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'        .Parameters.Append .CreateParameter("@cod_grupo", adVarChar, adParamInput, 8, cod_grupo)
'        .Parameters.Append .CreateParameter("@mem_campo", adVarChar, adParamInput, 10, NombreCampo)
'        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , Null)
'        Set aplRST1 = .Execute
'    End With
'    If Not (aplRST1.EOF) Then
'        bGetMemo = True
'    End If
'    If bGetMemo Then
'        Do While Not aplRST1.EOF
'            sGetMemo = sGetMemo & ClearNull(aplRST1.Fields!mem_texto)
'            aplRST1.MoveNext
'        Loop
'        aplRST1.Close
'    End If
'
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'    sp_GetPlanMemo2 = sGetMemo
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST1, Err, aplCONN))
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    sp_GetPlanMemo2 = ""
'End Function
