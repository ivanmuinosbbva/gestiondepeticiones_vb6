Attribute VB_Name = "spAdjuntoPet"
' -001- a. FJS 13.08.2007 - Se agrega al manejador de errores de esta rutina que contemple el mensaje para los archivos sin contenido (0 kb. de size)
' -002- a. FJS 23.05.2008 - Se cambia el tama�o m�ximo permitido para adjuntar documentos en la DB.
' -003- a. FJS 21.07.2008 - Se agregan los par�metros de sector y grupo para grabar documentos adjuntos en la base.
' -004- a. FJS 13.03.2009 - Se agrega una peque�a rutina de control para evitar error de SQL por el uso de ap�strofos en las cadenas de nombre de archivo.
' -005- a. FJS 09.04.2010 - Se cambia la funci�n Date por Now para obtener la hora tambi�n.
' -006- a. FJS 14.04.2010 - Se modifica el timeout por defecto (se deja indefinido).
' -007- a. FJS 30.11.2015 - Modificaci�n: se agrega el cambio en el motor de cursos porque en Windows Server 2012 con ADO 6.3 se genera el siguiente error
'                           y no adjunta los datos: Multiple step operation generated errors. Check each status value.

Option Explicit

Function sp_GetAdjuntosPet(pet_nrointerno, adj_tipo, cod_sector, cod_grupo) As Boolean  ' upd -003- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandTimeout = 0           ' add -006- a.
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAdjuntosPet"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)
      '{ add -003- a.
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      '}
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAdjuntosPet = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAdjuntosPet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

' REVISAR ACA SE ESTA MODIFICANDO TODO!!!!!
Function sp_InsertPeticionAdjunto(pet_nrointerno, cod_sector, cod_grupo, adj_tipo, adj_path, adj_file, adj_texto) As Boolean    ' upd -003- a. Se agreg� luego de 'pet_nrointerno'
    On Error GoTo Err_SP
    On Error Resume Next
    Dim objFileSystemObject As Scripting.FileSystemObject
    Dim objFile As Scripting.file
    Dim strSQL As String
    Dim strPathToFile As String, strFileName As String
    
    Call Puntero(True)
    aplRST.Close
    Set aplRST = Nothing
    
    Set objFileSystemObject = New Scripting.FileSystemObject
    Set objFile = objFileSystemObject.GetFile(adj_path & adj_file)
    If objFile.Attributes And ReadOnly Then
        MsgBox "The file is read-only." & vbCrLf & _
               "This sample requires a file that's not read-only", vbExclamation + vbOKOnly
    End If
    DoEvents
    
    aplCONN.CursorLocation = adUseServer        ' add -007- a.
    With aplRST
        '.Open "select pet_nrointerno,adj_tipo,adj_file,adj_texto,adj_objeto,audit_user,audit_date from PeticionAdjunto where pet_nrointerno=1", aplCONN.ConnectionString, adOpenStatic, adLockOptimistic, adCmdText
        .Open "select pet_nrointerno, cod_sector, cod_grupo, adj_tipo, adj_file, adj_texto, adj_objeto, audit_user, audit_date from PeticionAdjunto where pet_nrointerno=1", aplCONN, adOpenStatic, adLockOptimistic        ' upd -007- a.
        .AddNew
        aplRST!pet_nrointerno = pet_nrointerno
        aplRST!adj_tipo = ClearNull(adj_tipo)
        aplRST!adj_file = ClearNull(adj_file)
        aplRST!adj_texto = ClearNull(adj_texto)
        Call FileToBlob(adj_path & adj_file, aplRST!adj_objeto)
        aplRST!audit_user = ClearNull(glLOGIN_ID_REAL)
        aplRST!audit_date = Now
        aplRST!cod_sector = ClearNull(cod_sector)
        aplRST!cod_grupo = ClearNull(cod_grupo)
        
        'aplRST!audit_user = "XA51043" 'ClearNull(glLOGIN_ID_REAL)
        'aplRST!audit_date = CDate("03/02/2015 16:10:12")   'Now
        'aplRST!cod_sector = "147"  'ClearNull(cod_sector)
        'aplRST!cod_grupo = "24-03"   'ClearNull(cod_grupo)
        
        'MsgBox "Va a hacer el update...", vbExclamation     ' add -xxx-
        .Update
        'MsgBox "Hizo el update...", vbExclamation           ' add -xxx-
        If .State = adStateOpen Then .Close
    End With
    aplCONN.CursorLocation = adUseClient        ' add -007- a.
    
    Call Puntero(False)
    sp_InsertPeticionAdjunto = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    On Error GoTo 0
    If Not objFile Is Nothing Then Set objFile = Nothing
    If Not objFileSystemObject Is Nothing Then Set objFileSystemObject = Nothing
    Call Puntero(False)
    'Call Status("Listo.")
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionAdjunto = False
    'MsgBox "error handled" & vbCrLf & Err.Number & ": " & Err.DESCRIPTION
End Function

' TODO: Esta es la funci�n original -11.06.2015-
'' REVISAR ACA SE ESTA MODIFICANDO TODO!!!!!
'Function sp_InsertPeticionAdjunto(pet_nrointerno, cod_sector, cod_grupo, adj_tipo, adj_path, adj_file, adj_texto) As Boolean    ' upd -003- a. Se agreg� luego de 'pet_nrointerno'
''    Dim objFileSystemObject As Scripting.FileSystemObject
''    Dim objFile As Scripting.File
''    Dim rsFileData As ADODB.Recordset
''    Dim strmFile As ADODB.Stream
''    Dim strSQL As String
''    Dim strPathToFile As String, strFileName As String
''    Static intID As Integer
''    Dim lFileSize As Long
''
''    Set objFileSystemObject = New Scripting.FileSystemObject
''    Set objFile = objFileSystemObject.GetFile(adj_path & adj_file)
''    If objFile.Attributes And ReadOnly Then
''        MsgBox "The file is read-only." & vbCrLf & _
''               "This sample requires a file that's not read-only"
''        'Exit Sub
''    End If
''
''    DoEvents
''
''    Set strmFile = New ADODB.Stream
''    strmFile.Type = adTypeBinary
''    strmFile.Open
''    strmFile.LoadFromFile adj_path & adj_file
''
''    docRST.CursorLocation = adUseClient
''    docRST.Open "select pet_nrointerno,adj_tipo,adj_file,adj_texto,adj_objeto,audit_user,audit_date from PeticionAdjunto where pet_nrointerno=1", aplCONN.ConnectionString, adOpenStatic, adLockOptimistic, adCmdText
''    docRST.AddNew
''    docRST!pet_nrointerno = pet_nrointerno
''    docRST!adj_tipo = ClearNull(adj_tipo)
''    docRST!adj_file = ClearNull(adj_file)
''    docRST!adj_texto = ClearNull(adj_texto)
''    docRST!adj_objeto = strmFile.Read(adReadAll)
''    docRST!audit_user = ClearNull(glLOGIN_ID_REAL)
''    docRST!audit_date = Date
''    docRST.Update
''    docRST.Close
''
''    strmFile.Close
''
''    Screen.MousePointer = vbNormal
''    sp_InsertPeticionAdjunto = True
''    If aplCONN.Errors.Count > 0 Then
''        GoTo Err_SP
''    End If
''    On Error GoTo 0
'
''{ Esto es el original
'    Dim txtCommand
'    On Error Resume Next
'    docRST.Close
'    Set docRST = Nothing
'    On Error GoTo Err_SP
'
'    Dim lFileSize As Long
'
'    adj_file = ClearNombreArchivo(CStr(adj_file))   ' add -004- a.
'
'    txtCommand = "SELECT pet_nrointerno, cod_sector, cod_grupo, adj_tipo, adj_file, adj_texto, adj_objeto, audit_user, audit_date from PeticionAdjunto where pet_nrointerno=1"    ' add -003- a.
'    Call Puntero(True)
'    'docRST.Open txtCommand, aplCONN.ConnectionString, adOpenKeyset, adLockOptimistic, adCmdText
'    docRST.Open txtCommand, aplCONN, adOpenKeyset, adLockOptimistic, adCmdText
'    docRST.AddNew
'    docRST!pet_nrointerno = pet_nrointerno
'    docRST!cod_sector = cod_sector
'    docRST!cod_grupo = cod_grupo
'    docRST!adj_tipo = ClearNull(adj_tipo)
'    'Compressed = HuffmanEncode(SourceText, [Force])
'    docRST!adj_file = ClearNull(adj_file)
'    docRST!adj_texto = ClearNull(adj_texto)
'    adj_file = RestoreNombreArchivo(CStr(adj_file))   ' add -004- a.
'    Call FileToBlob(adj_path & adj_file, docRST!adj_objeto)
'    docRST!audit_user = ClearNull(glLOGIN_ID_REAL)
'    docRST!audit_date = Now       ' upd -005- a. Se cambia Date por Now
'    docRST.Update
'    docRST.Close
'
'    'Screen.MousePointer = vbNormal
'    Call Puntero(False)
'    sp_InsertPeticionAdjunto = True
'    If aplCONN.Errors.Count > 0 Then
'        GoTo Err_SP
'    End If
'    On Error GoTo 0
''}
'Exit Function
'Err_SP:
'    lFileSize = FileLen(adj_path & adj_file)
'    'Screen.MousePointer = vbNormal
'    Call Puntero(False)
'    '{ add -001- a.
'    Select Case Err.Number
'        Case -2147467259            ' Este Nro. de Error es gen�rico de varios otros. Una macana del equipo de desarrollo de ADO
'            If lFileSize = 0 Then
'                MsgBox "Solo puede adjuntar archivos a la petici�n con un tama�o mayor o igual a 1 byte." & vbCrLf & _
'                       "Revise el archivo que intent� adjuntar porque no es v�lido o esta vacio (0 Kb.)", vbExclamation + vbOKOnly, "Archivo vacio o archivo inv�lido"
'            End If
'            If lFileSize > 0 Then
'                If InStr(1, Err.DESCRIPTION, "Attempt to insert duplicate key row in object 'PeticionAdjunto'", vbTextCompare) > 0 Then
'                    MsgBox "El archivo " & Trim(adj_file) & " (" & Trim(adj_tipo) & ") que intenta adjuntar ya existe en la base." & vbCrLf & "Desvincule el archivo antes de volver a adjuntarlo.", vbExclamation + vbOKOnly, "Archivo duplicado"
'                Else
'                    If lFileSize > ARCHIVOS_ADJUNTOS_MAXSIZE Then
'                        MsgBox "Solo puede adjuntar archivos a la petici�n con un tama�o menor o igual a 2 Mb." & vbCrLf & _
'                               "Revise el archivo que intent� adjuntar porque no es v�lido o su tama�o actual (" & Trim(CStr(Format((lFileSize / 1024) / 1024, "#0.00"))) & " Mb.) es mayor al permitido", vbExclamation + vbOKOnly, "Archivo muy grande"
'                    Else
'                        MsgBox Err.DESCRIPTION
'                    End If
'                End If
'            End If
'            sp_InsertPeticionAdjunto = False
'        Case Else
'            MsgBox Err.Number & ": " & Err.DESCRIPTION, vbCritical + vbOKOnly, "Error al adjuntar archivo."
'            sp_InsertPeticionAdjunto = False
'    End Select
'    'MsgBox (AnalizarErrorSQL(docRST, Err, aplCONN))
'    '}
'    '{ del -001- a.
'    'MsgBox (Err.DESCRIPTION)
'    'MsgBox (AnalizarErrorSQL(docRST, Err, aplCONN))
'    'sp_InsertPeticionAdjunto = False
'    '}
'End Function


'{ add -004- a.
Private Function ClearNombreArchivo(ByVal cNombreArchivo As String) As String
    Do While InStr(1, cNombreArchivo, "'", vbTextCompare) > 0
        If InStr(1, cNombreArchivo, "'", vbTextCompare) > 0 Then
            cNombreArchivo = Mid(cNombreArchivo, 1, InStr(1, cNombreArchivo, "'", vbTextCompare) - 1) & "�" & _
                             Mid(cNombreArchivo, InStr(1, cNombreArchivo, "'", vbTextCompare) + 1)
        End If
        DoEvents
    Loop
    ClearNombreArchivo = cNombreArchivo
End Function

Private Function RestoreNombreArchivo(cNombreArchivo As String) As String
    'Dim cAux As String
    
    Do While InStr(1, cNombreArchivo, "�", vbTextCompare) > 0
        If InStr(1, cNombreArchivo, "�", vbTextCompare) > 0 Then
            cNombreArchivo = Mid(cNombreArchivo, 1, InStr(1, cNombreArchivo, "�", vbTextCompare) - 1) & "'" & _
                             Mid(cNombreArchivo, InStr(1, cNombreArchivo, "�", vbTextCompare) + 1)
        End If
        DoEvents
    Loop
    RestoreNombreArchivo = cNombreArchivo
End Function
'}

Function sp_DeletePeticionAdjunto(pet_nrointerno, adj_tipo, adj_file) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeletePeticionAdjunto"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)
      .Parameters.Append .CreateParameter("@adj_file", adChar, adParamInput, 100, adj_file)
      Set aplRST = .Execute
    End With
    sp_DeletePeticionAdjunto = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionAdjunto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionAdjunto(pet_nrointerno, adj_tipo, adj_file, adj_texto, newadj_tipo, newadj_file) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePeticionAdjunto"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)
      .Parameters.Append .CreateParameter("@adj_file", adChar, adParamInput, 100, adj_file)
      .Parameters.Append .CreateParameter("@adj_texto", adChar, adParamInput, 250, adj_texto)
      .Parameters.Append .CreateParameter("@newadj_tipo", adChar, adParamInput, 8, newadj_tipo)
      .Parameters.Append .CreateParameter("@newadj_file", adChar, adParamInput, 100, newadj_file)
      Set aplRST = .Execute
    End With
    sp_UpdatePeticionAdjunto = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionAdjunto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
