Attribute VB_Name = "spMensajesEvento"
Option Explicit

Function sp_GetMensajesEvento(evt_alcance, cod_accion, cod_estado, cod_perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetMensajesEvento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetMensajesEvento = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetMensajesEvento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdMensajesEvento(evt_alcance, cod_accion, cod_estado, cod_perfil, cod_txtmsg, est_recept, est_codigo, txt_extend, txt_pzofin, flg_activo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
'      .CommandType = adCmdText
      .CommandText = "sp_UpdMensajesEvento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_txtmsg", adSmallInt, adParamInput, , CInt(cod_txtmsg))
      .Parameters.Append .CreateParameter("@est_recept", adChar, adParamInput, 3, est_recept)
      .Parameters.Append .CreateParameter("@est_codigo", adVarChar, adParamInput, Len(est_codigo) + 1, est_codigo & " ")
      .Parameters.Append .CreateParameter("@txt_extend", adVarChar, adParamInput, Len(txt_extend) + 1, txt_extend & " ")
      .Parameters.Append .CreateParameter("@txt_pzofin", adChar, adParamInput, 1, txt_pzofin)
      .Parameters.Append .CreateParameter("@flg_activo", adChar, adParamInput, 1, flg_activo)
      Set aplRST = .Execute
    End With
    sp_UpdMensajesEvento = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdMensajesEvento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsMensajesEvento(evt_alcance, cod_accion, cod_estado, cod_perfil, cod_txtmsg, est_recept, est_codigo, txt_extend, txt_pzofin, flg_activo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsMensajesEvento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_txtmsg", adSmallInt, adParamInput, , CInt(cod_txtmsg))
      .Parameters.Append .CreateParameter("@est_recept", adChar, adParamInput, 3, est_recept)
      .Parameters.Append .CreateParameter("@est_codigo", adChar, adParamInput, 255, est_codigo)
      .Parameters.Append .CreateParameter("@txt_extend", adVarChar, adParamInput, Len(txt_extend) + 1, txt_extend & " ")
      .Parameters.Append .CreateParameter("@txt_pzofin", adChar, adParamInput, 1, txt_pzofin)
      .Parameters.Append .CreateParameter("@flg_activo", adChar, adParamInput, 1, flg_activo)
      Set aplRST = .Execute
    End With
    sp_InsMensajesEvento = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsMensajesEvento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function


Function sp_DelMensajesEvento(evt_alcance, cod_accion, cod_estado, cod_perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelMensajesEvento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      Set aplRST = .Execute
    End With
    sp_DelMensajesEvento = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelMensajesEvento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DoMensaje(evt_alcance, cod_accion, pet_nrointerno, cod_hijo, dsc_estado, txt_txtmsg, txt_txtprx) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DoMensaje"
        .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
        .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_hijo", adChar, adParamInput, 8, cod_hijo)
        .Parameters.Append .CreateParameter("@dsc_estado", adChar, adParamInput, 8, dsc_estado)
        .Parameters.Append .CreateParameter("@txt_txtmsg", adChar, adParamInput, 250, txt_txtmsg)
        .Parameters.Append .CreateParameter("@txt_txtprx", adChar, adParamInput, 250, txt_txtprx)
        Set aplRST = .Execute
    End With
    sp_DoMensaje = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DoMensaje = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

