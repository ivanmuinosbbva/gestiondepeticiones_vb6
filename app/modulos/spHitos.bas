Attribute VB_Name = "spHitos"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertHitos(hito_id, hito_desc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertHitos"
        .Parameters.Append .CreateParameter("@hito_id", adInteger, adParamInput, , hito_id)
        .Parameters.Append .CreateParameter("@hito_desc", adChar, adParamInput, , hito_desc)
        Set aplRST = .Execute
    End With
    sp_InsertHitos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHitos(hito_id, hito_desc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateHitos"
        .Parameters.Append .CreateParameter("@hito_id", adInteger, adParamInput, , hito_id)
        .Parameters.Append .CreateParameter("@hito_desc", adChar, adParamInput, , hito_desc)
        Set aplRST = .Execute
    End With
    sp_UpdateHitos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteHitos(hito_id, hito_desc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteHitos"
        .Parameters.Append .CreateParameter("@hito_id", adInteger, adParamInput, , hito_id)
        .Parameters.Append .CreateParameter("@hito_desc", adChar, adParamInput, , hito_desc)
        Set aplRST = .Execute
    End With
    sp_DeleteHitos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHitos(hito_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHitos"
        .Parameters.Append .CreateParameter("@hito_id", adInteger, adParamInput, , hito_id)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHitos = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
