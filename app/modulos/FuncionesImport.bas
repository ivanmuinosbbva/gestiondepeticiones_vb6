Attribute VB_Name = "FuncionesImport"
' -001- a. FJS 27.06.2007 - Se agrega el soporte para el manejo de los nuevos atributos (Clase de petici�n e implicancia de impacto tecnol�gico).
' -001- b. FJS 18.07.2007 - Se agrega el soporte para inicializar y guardar el valor para el campo pet_sox001 de la tabla Peticion al momento de realizar una importaci�n de datos.
' -001- c. FJS 07.08.2007 - Por definicion con la gente de Organizacion el dia de hoy (Testing) se fuerzan los datos de impacto tecnologico y la clase de la peticion.
' -001- d. FJS 08.08.2007 - Importaci�n por auditor�a: 3. Para las importaciones con fecha igual o posterior a la de vigencia de los controles SOX, no se debe permitir la importaci�n
'                           de peticiones cuyo estado sea "Terminado"; para las importaciones previas a la fecha de vigencia no realizar este control.
' -002- a. FJS 21.08.2007 - Se vuelven a establecer los valores por defecto para el atributo de clase e impacto tecnol�gico. Adem�s, el estado deber� preservarse seg�n lo declarado en la
'                           planilla Excel. Adem�s, deben excluirse de la importaci�n aquellas peticiones que tengan el estado "Finalizado" posteriores a la fecha de corte. Las anteriores
'                           deben ser importadas como otrora. Adem�s se revisa el c�digo que depende de la prioridad porque esta dejando afuera peticiones que deben ser importadas a la base.
' -003- a. FJS 17.03.2008 - Homologaciones:
' -004- a. FJS 22.05.2008 - Referencias: punto 1.d y 2.a: se agrega la condici�n para restringir el agregado de los registros a la base en funci�n de las evaluaciones previas. No se importan
'                           peticiones en estados "terminales".
' -005- a. FJS 18.09.2008 - Se agregan los datos sin valor para la parte de proyecto IDM.
' -006- a. FJS 21.04.2010 - Se regularizan el dato de regulatorio.
' -007- a. FJS 20.12.2010 - Se cambia el m�todo de especificaci�n del archivo de importaci�n.
' -008- a. FJS 12.03.2012 - Fusi�n Banco/Consolidar.
' -009- a. FJS 23.04.2012 - Nuevo: Empresa y Grupo Ejecutor.
' -010- a. FJS 01.06.2012 - Contemplar la inclusi�n autom�tica del grupo homologador.
' -010- b. FJS 09.10.2012 - Correcci�n: se agrega autom�ticamente el grupo homologador solo si se agrega un grupo de DYD.
' -011-  GMT01 16.05.2019 - Se saca la validaci�n del codigo de orientaci�n.
' -012-  GMT02 27.05.2019 - Se agregan dos columnas m�s al excel para las preguntas de data
' -013-  GMT03 28.08.2019 - Se agregan 1 columna m�s al excel las agrupaciones

Option Explicit

Sub Importar()
    On Error GoTo Errores
    Const colMARK = 1           ' A
    Const colEMPRESA = 2        ' B
    Const colTIPO = 3           ' C
    Const colTitulo = 4         ' D
    Const colPRIORI = 5         ' E
    Const colORIENT = 6         ' F
    Const colCORLOC = 7         ' G
    Const colIMPORT = 8         ' H
    Const colFECALT = 9         ' I
    Const colFECREQ = 10        ' J
    Const colFECCOM = 11        ' K
    Const colSOLICI = 12        ' L
    Const colREFERE = 13        ' M
    Const colAUTORI = 14        ' N
    Const colSecSol = 15        ' O
    Const colBPAR = 16          ' P
    Const colSecEjc = 17        ' Q
    Const colGrpEjc = 18        ' R
    Const colESTADO = 19        ' S
    Const colFECIPL = 20        ' T
    Const colFECFPL = 21        ' U
    Const colFECIRE = 22        ' V
    Const colFECFRE = 23        ' W
    Const colDESCRI = 24        ' X
    Const colCARACT = 25        ' Y
    Const colMOTIVO = 26        ' Z
    Const colBENEFI = 27        ' AA
    Const colRELACI = 28        ' AB
    Const colOBSERV = 29        ' AC
    Const colDATA1 = 30         ' AD GMT02
    Const colDATA2 = 31         ' AE GMT02
    Const colCATEGORIA = 32     ' AF GMT03
    Const colAGRUPA = 33        ' AG GMT03
    Const colERROR = 34         ' AH GMT03
    
    Const cvalTecData = 3       'validacion tecnica data GMT02
    
    Dim sSoli, sRefe, sDire, sBpar As String
    Dim sDirSol, sGerSol, sSecSol As String
    Dim sDirejc, sGerEjc, sSecEjc As String
    Dim dFecalt, dFecreq, dFeccom As Variant
    Dim dIniPlan, dFinPlan, dIniReal, dFinReal, dPasajeProduccion As Variant    ' upd -003- a. Se agrega la variable 'dPasajeProduccion'
    Dim sTitulo, sTipo, sPrioridad, sEstado As String
    Dim sImportancia As String
    Dim sOrientacion As String
    Dim sCorLoc As String
    Dim sPetClass As String * 4
    Dim sPetImpTech As String * 1
    Dim sPet_sox001 As Byte         ' add -001- b.
    Dim xError As String
    Dim bError As Boolean
    Dim bPerfil As Boolean
    Dim bAsigna As Boolean
    Dim sRegulatorio As String      ' add -006- a.
    Dim sDESCRI, sCARACT, sMOTIVO, sBENEFI, sOBSERV, sRELACI As String
    Dim ExcelApp, ExcelWrk, xlSheet As Object
    Dim xRow, iRow As Long
    Dim pOK As Long
    Dim pBD As Long
    Dim pSKIP As Long
    Dim nUltimoAsignado As Long
    Dim nNumeroAsignado As Long
    Dim bImport As Boolean          ' add -001- d.
    Dim lEmpresa As String          ' add -008- a.
    Dim sGrpEjc As String           ' add -008- a.
    Dim txtFile As String
    Dim sMensajeResultado As String
    '{ add -010- a.
    Dim sGrupoHomologador As String
    Dim sSectorHomologador As String
    '}
    'GMT02 - INI
    Dim sPregData1 As String
    Dim sPregData2 As String
    'GMT02 - FIN
    Dim sCategoria As String  'GMT03
    Dim sAgrupa As String  'GMT03
    Dim sAgrupaNum As Integer 'GMT03
    Dim exiteAgrup As Boolean 'GMT03
    Dim NroAgrupAsignado As Integer 'GMT03
    Dim sNropadre As Integer 'GMT03
    
    '{ add -007- a.
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = "Importar archivo desde Microsoft Excel"
        .Filter = "Microsoft Excel (*.xls)|*.xls"
        .FilterIndex = 1
        .InitDir = glWRKDIR
        .Flags = cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        txtFile = .FileName
        If Len(Trim(txtFile)) = 0 Then
            GoTo finImpo
        End If
    End With
    '}
    If MsgBox("�Confirma inicio de importaci�n?", vbQuestion + vbOKCancel, "ATENCION") = vbCancel Then
        GoTo finImpo
    End If
    
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.visible = False
    On Error Resume Next
    Set ExcelWrk = ExcelApp.Workbooks.Open(txtFile)    ' upd -007- a.
    If ExcelWrk Is Nothing Then
        MsgBox ("Error al abrir planilla")
        GoTo finImpo
    End If
    Set xlSheet = ExcelWrk.Sheets("PETICIONES")
    If xlSheet Is Nothing Then
        ExcelWrk.Close
        ExcelApp.Application.Quit
        MsgBox ("Error, hoja PETICIONES inexistente")
        GoTo finImpo
    End If
    xRow = 1
   
    On Error GoTo 0

    xRow = xRow + 1
    iRow = 0
    
    bError = False
    
    Call Puntero(True)
    While ClearNull(xlSheet.Cells(xRow, colTIPO)) <> "" 'And Asc(ClearNull(xlSheet.Cells(xRow, colTIPO))) <> 10  ' upd -008- a.
        iRow = iRow + 1
        Call Status("Proc: " & iRow)
        DoEvents
        xError = ""
        If ClearNull(xlSheet.Cells(xRow, colMARK)) <> "" And ClearNull(xlSheet.Cells(xRow, colMARK)) <> "ERROR" Then
            pSKIP = pSKIP + 1
            GoTo skipRow
        End If
        lEmpresa = CLng((ClearNull(xlSheet.Cells(xRow, colEMPRESA))))
        sSoli = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSOLICI))), 10)
        sRefe = Left(UCase(ClearNull(xlSheet.Cells(xRow, colREFERE))), 10)
        'sSupe = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSUPERVISOR))), 10)
        sDire = Left(UCase(ClearNull(xlSheet.Cells(xRow, colAUTORI))), 10)
        sBpar = Left(UCase(ClearNull(xlSheet.Cells(xRow, colBPAR))), 10)
        sDirSol = ""
        sGerSol = ""
        sSecSol = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSecSol))), 8)
        sTitulo = Left(ClearNull(xlSheet.Cells(xRow, colTitulo)), 50)
        sTipo = Left(UCase(ClearNull(xlSheet.Cells(xRow, colTIPO))), 3)
        sPrioridad = Left(ClearNull(xlSheet.Cells(xRow, colPRIORI)), 1)
        sImportancia = ClearNull(xlSheet.Cells(xRow, colIMPORT))
        sEstado = Left(UCase(ClearNull(xlSheet.Cells(xRow, colESTADO))), 6)
        sOrientacion = ClearNull(xlSheet.Cells(xRow, colORIENT))
        sPregData1 = Left(UCase(ClearNull(xlSheet.Cells(xRow, colDATA1))), 1) 'GMT02
        sPregData2 = Left(UCase(ClearNull(xlSheet.Cells(xRow, colDATA2))), 1) 'GMT02
        sCategoria = Left(UCase(ClearNull(xlSheet.Cells(xRow, colCATEGORIA))), 3) 'GMT03
        sAgrupa = Left(UCase(ClearNull(xlSheet.Cells(xRow, colAGRUPA))), 50) 'GMT03
        sCorLoc = ClearNull(xlSheet.Cells(xRow, colCORLOC))
        sPetClass = "EVOL"      ' add -002- a.
        sPetImpTech = "N"
        sDESCRI = ClearNull(xlSheet.Cells(xRow, colDESCRI))
        sCARACT = ClearNull(xlSheet.Cells(xRow, colCARACT))
        sMOTIVO = ClearNull(xlSheet.Cells(xRow, colMOTIVO))
        sBENEFI = ClearNull(xlSheet.Cells(xRow, colBENEFI))
        sOBSERV = ClearNull(xlSheet.Cells(xRow, colOBSERV))
        sRELACI = ClearNull(xlSheet.Cells(xRow, colRELACI))
        
        sDESCRI = ReplaceString(sDESCRI, 10, vbNewLine)
        sCARACT = ReplaceString(sCARACT, 10, vbNewLine)
        sMOTIVO = ReplaceString(sMOTIVO, 10, vbNewLine)
        sBENEFI = ReplaceString(sBENEFI, 10, vbNewLine)
        sOBSERV = ReplaceString(sOBSERV, 10, vbNewLine)
        sRELACI = ReplaceString(sRELACI, 10, vbNewLine)
        
        sDirejc = ""
        sGerEjc = ""
        sSecEjc = ""
        sGrpEjc = ""
        '{ del -008- a.
        'sSecEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSecEjc))), 8)
        'sGrpEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colGrpEjc))), 8)
        '}
        '{ add -008- a.
        If InStr(1, ClearNull(xlSheet.Cells(xRow, colSecEjc)), ":", vbTextCompare) > 0 Then
            sSecEjc = Mid(ClearNull(xlSheet.Cells(xRow, colSecEjc)), 1, InStr(1, ClearNull(xlSheet.Cells(xRow, colSecEjc)), ":", vbTextCompare) - 1)
        Else
            sSecEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSecEjc))), 8)
        End If
        If InStr(1, ClearNull(xlSheet.Cells(xRow, colGrpEjc)), ":", vbTextCompare) > 0 Then
            sGrpEjc = Mid(ClearNull(xlSheet.Cells(xRow, colGrpEjc)), 1, InStr(1, ClearNull(xlSheet.Cells(xRow, colGrpEjc)), ":", vbTextCompare) - 1)
        Else
            sGrpEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colGrpEjc))), 8)
        End If
        '}
        sRegulatorio = IIf(InStr(1, sTitulo, "BCRA", vbTextCompare) > 0, "S", "N")  ' add -006- a.
        '**************************************************************************************************
        ' CONTROLES REALIZADOS ANTES DE IMPORTAR LOS DATOS
        '**************************************************************************************************
        '    1. Control del tipo de petici�n
        '    2. Control del t�tulo de la petici�n
        '    3. Control de visibilidad de la petici�n (solo Proyectos)
        '    4. Control de orientaci�n (solo Proyectos)
        '    5. Control de Corporativo o Local (solo Protectos)
        '    6. Control de Estado de la petici�n
        '    7. Control de prioridad
        '    8. Control de Sector solicitante
        '    9. Control de Sector ejecutor
        '   10. Control de Grupo ejecutor
        '   11. Controla el solicitante de la petici�n
        '   12. Controla el referente de la petici�n
        '   13. Controla el autorizante de la petici�n
        '   14. Controla el Business Partner de la petici�n
        '   15. Control de fechas de la petici�n
        '       15.a. Validaci�n de fecha de alta
        '       15.b. Validaci�n de fecha requerida
        '       15.c. Validaci�n de fecha comite
        '       15.d. Validaci�n de fecha IniPlan
        '       15.e. Validaci�n de fecha FinPlan
        '       15.f. Validaci�n de fecha IniReal
        '       15.g. Validaci�n de fecha FinReal
        '   16. Control de textos varios de la petici�n (descripci�n, caracter�sticas, etc.)
        '   17. Control de campo regulatorio (SI, NO, Indeterminado)
        '   18. Empresa (obligatorio)
        '   19. validacion tecnica de data
        '
        '**************************************************************************************************
        ' 1. Control del tipo de petici�n
        If sTipo = "" Then
            xError = xError & Chr(10) & "Falta Tipo de Peticion : "
        ElseIf InStr("PRO|NOR|ESP|AUI|AUX|PRJ", sTipo) = 0 Then
            xError = xError & Chr(10) & "Tipo de Peticion incorrecta o no definido : "
        End If
        ' 2. Control del t�tulo de la petici�n
        If sTitulo = "" Then
            xError = xError & Chr(10) & "Falta T�tulo : "
        End If
        ' 3. Control de visibilidad de la petici�n (solo Proyectos)
        If sTipo = "PRJ" Then
            If sImportancia = "" Then
                xError = xError & Chr(10) & "VISIBILIDAD no informada : "
            ElseIf InStr("10|20|30|40", sImportancia) = 0 Then
                xError = xError & Chr(10) & "VISIBILIDAD incorrecta : "
            End If
        Else
            If sImportancia <> "" Then
                xError = xError & Chr(10) & "No debe informar VISIBILIDAD : "
            End If
        End If
        ' 4. Control de orientaci�n (solo Proyectos)
        If sOrientacion = "" Then      'GMT01
           sOrientacion = "D"          'GMT01
        End If                         'GMT01
       ' If sTipo = "PRJ" Then
            If sOrientacion = "" Then
                xError = xError & Chr(10) & "Orientaci�n no informada : "
            ElseIf Not sp_GetOrientacion(sOrientacion, Null) Then
                xError = xError & Chr(10) & "Orientaci�n incorrecta : "
            End If
       ' Else
       '     If sOrientacion <> "" Then
       '         xError = xError & Chr(10) & "No debe informar orientaci�n : "
       '     End If
       ' End If
        ' 5. Control de Corporativo o Local (solo Protectos)
        If sTipo = "PRJ" Then
            If sCorLoc = "" Then
                xError = xError & Chr(10) & "C/L no informado : "
            ElseIf InStr("C|L", sCorLoc) = 0 Then
                xError = xError & Chr(10) & "C/L incorrecta : "
            End If
        Else
            sCorLoc = "L"
        End If
        ' 6. Control de Estado de la petici�n
        If sEstado = "" Then
            xError = xError & Chr(10) & "Estado no informado : "
        ElseIf InStr("COMITE|APROBA|PLANIF|PLANOK|EJECUC|TERMIN|CANCEL|SUSPEN|", sEstado) = 0 Then
            xError = xError & Chr(10) & "Estado incorrecto : "
        End If
        '{ add -004- a.
        'If InStr("RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|", sEstado) > 0 Then
        If InStr("RECHAZ|RECHTE|ANEXAD|ANULAD|", sEstado) > 0 Then
            xError = xError & Chr(10) & "Estado terminal : "
        End If
        '}
        ' 7. Control de prioridad
        If InStr("PRO|ESP|PRJ", sTipo) > 0 Or InStr("COMITE|CANCEL|SUSPEN|REVISA", sEstado) = 0 Then
            If sPrioridad = "" Then
                xError = xError & Chr(10) & "Prioridad no informada : "
            'ElseIf InStr("1|2|3", sPrioridad) = 0 Then             ' del -002- a.
            ElseIf InStr("1|2|3|4|5|6", sPrioridad) = 0 Then        ' add -002- a.
                xError = xError & Chr(10) & "Prioridad incorrecta : "
            End If
        End If
        ' 8. Control de Sector solicitante
        If sSecSol = "" Then
            xError = xError & Chr(10) & "Falta Sector Solic. : "
        ElseIf Not sp_GetSectorXt(sSecSol, Null, Null) Then
            sSecSol = ""
            xError = xError & Chr(10) & "Sector Solic. inexistente : "
        ElseIf aplRST.EOF Then
            sSecSol = ""
            xError = xError & Chr(10) & "Sector Solic. inexistente : "
        Else
            sDirSol = ClearNull(aplRST!cod_direccion)
            sGerSol = ClearNull(aplRST!cod_gerencia)
            aplRST.Close
            If Not valSectorHabil(sSecSol) Then
                xError = xError & Chr(10) & "Sector Solic. no habilitado : "
            Else
                If sGerSol = "" Then
                    xError = xError & Chr(10) & "Error en Gerencia Solic. : "
                End If
                If sDirSol = "" Then
                    xError = xError & Chr(10) & "Error en Direccion Solic. : "
                End If
            End If
        End If
        ' 9. Control de Sector ejecutor:
        ' ---------------------------
        ' Si la petici�n esta en alguno de los siguientes estados:
        ' - A planificar
        ' - Planificado
        ' - En ejecuci�n
        ' - Finalizada
        ' O:
        ' - Cancelado definitivamente
        ' - Suspendido temporalmente
        ' - A revisar
        If InStr("PLANIF|PLANOK|EJECUC|TERMIN", sEstado) > 0 Then
            If sSecEjc = "" Then
                xError = xError & Chr(10) & "Falta Sector ejecuc. : "
            ElseIf Not sp_GetSectorXt(sSecEjc, Null, Null) Then
                sSecEjc = ""
                xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
            ElseIf aplRST.EOF Then
                sSecEjc = ""
                xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
            Else
                sDirejc = ClearNull(aplRST!cod_direccion)
                sGerEjc = ClearNull(aplRST!cod_gerencia)
                aplRST.Close
                If Not valSectorHabil(sSecEjc) Then
                    xError = xError & Chr(10) & "Sector ejecuc. no habilitado : "
                Else
                    If sGerEjc = "" Then
                        xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    End If
                    If sDirejc = "" Then
                        xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                    End If
                End If
            End If
        ElseIf InStr("CANCEL|SUSPEN|REVISA", sEstado) > 0 Then
            If sSecEjc <> "" Then
                If Not sp_GetSectorXt(sSecEjc, Null, Null) Then
                    sSecEjc = ""
                    xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
                ElseIf aplRST.EOF Then
                    sSecEjc = ""
                    xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
                Else
                    sDirejc = ClearNull(aplRST!cod_direccion)
                    sGerEjc = ClearNull(aplRST!cod_gerencia)
                    aplRST.Close
                    If sGerEjc = "" Then
                        xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    End If
                    If sDirejc = "" Then
                        xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                    End If
                End If
            End If
        Else
            If sSecEjc <> "" Then
                 xError = xError & Chr(10) & "No debe informar Sector ejecuc. : "
            End If
        End If
        ' 10. Control de Grupo ejecutor:
        If InStr("PLANIF|PLANOK|EJECUC|TERMIN|", sEstado) > 0 Then
            If sGrpEjc = "" Then
                xError = xError & Chr(10) & "Falta Grupo ejecuc. : "
            ElseIf Not sp_GetGrupoXt(sGrpEjc, Null, Null) Then
                sGrpEjc = ""
                xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
            ElseIf aplRST.EOF Then
                sGrpEjc = ""
                xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
            Else
                If Not valGrupoHabil(sGrpEjc) Then
                    xError = xError & Chr(10) & "Grupo ejecuc. no habilitado : "
                Else
                    If sSecEjc = "" Then xError = xError & Chr(10) & "Error en Sector ejecuc. : "
                    If sGerEjc = "" Then xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    If sDirejc = "" Then xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                End If
            End If
            '{ add -009- a.
            If Not sp_GetRecursoActuanteEstado("GRU", "GRUP", sGrpEjc, sEstado) Then
                xError = xError & Chr(10) & "No existen recursos que puedan procesar esta solicitud en el grupo ejecuc. : "
            End If
            '}
        ElseIf InStr("CANCEL|SUSPEN|REVISA|", sEstado) > 0 Then
            If sGrpEjc <> "" Then
                If Not sp_GetGrupoXt(sGrpEjc, Null, Null) Then
                    sGrpEjc = ""
                    xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
                ElseIf aplRST.EOF Then
                    sGrpEjc = ""
                    xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
                Else
                    If sSecEjc = "" Then xError = xError & Chr(10) & "Error en Sector ejecuc. : "
                    If sGerEjc = "" Then xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    If sDirejc = "" Then xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                End If
            End If
        Else
            If sGrpEjc <> "" Then
                 xError = xError & Chr(10) & "No debe informar Grupo ejecuc. : "
            End If
        End If
        
        ' 11. Controla el solicitante de la petici�n
        bPerfil = False
        If sSoli <> "" Then
            If sp_GetRecurso(sSoli, "R") Then
                aplRST.Close
                If sSecSol <> "" Then
                    If sp_GetRecursoActuanteEstado("PET", "SECT", sSecSol, "CONFEC") Then
                        While Not aplRST.EOF
                            If sSoli = ClearNull(aplRST!cod_recurso) Then
                                bPerfil = True
                            End If
                            aplRST.MoveNext
                        Wend
                        aplRST.Close
                        If Not bPerfil Then
                            xError = xError & Chr(10) & "El SOLICITANTE no puede procesar esta peticion : "
                        End If
                    Else
                        xError = xError & Chr(10) & "No existen SOLICITANTES que puedan procesar esta peticion : "
                    End If
                End If
            Else
                xError = xError & Chr(10) & "SOLICITANTE inexistente : "
            End If
        Else
            xError = xError & Chr(10) & "Falta SOLICITANTE : "
        End If
        ' 12. Controla el referente de la petici�n
        bPerfil = False
        If sRefe <> "" Then
            If sp_GetRecurso(sRefe, "R") Then
                aplRST.Close
                If sSecSol <> "" Then
                    If sp_GetRecursoActuanteEstado("PET", "SECT", sSecSol, "REFERE") Then
                        While Not aplRST.EOF
                            If sRefe = ClearNull(aplRST!cod_recurso) Then
                                bPerfil = True
                            End If
                            aplRST.MoveNext
                        Wend
                        aplRST.Close
                        If Not bPerfil Then
                            xError = xError & Chr(10) & "El REFERENTE no puede procesar esta peticion : "
                        End If
                    Else
                        xError = xError & Chr(10) & "No existen REFERENTES que puedan procesar esta peticion : "
                    End If
                End If
            Else
                xError = xError & Chr(10) & "REFERENTE inexistente : "
            End If
        Else
            xError = xError & Chr(10) & "Falta REFERENTE : "
        End If
        ' 13. Controla el autorizante de la petici�n
        bPerfil = False
        If sDire <> "" Then
            If sp_GetRecurso(sDire, "R") Then
                aplRST.Close
                If sSecSol <> "" Then
                    If sp_GetRecursoActuanteEstado("PET", "SECT", sSecSol, "AUTORI") Then
                        While Not aplRST.EOF
                            If sDire = ClearNull(aplRST!cod_recurso) Then
                                bPerfil = True
                            End If
                            aplRST.MoveNext
                        Wend
                        aplRST.Close
                        If Not bPerfil Then
                            xError = xError & Chr(10) & "El AUTORIZANTE no puede procesar esta peticion : "
                        End If
                    Else
                        xError = xError & Chr(10) & "No existen AUTORIZANTES que puedan procesar esta peticion : "
                    End If
                End If
            Else
                xError = xError & Chr(10) & "AUTORIZANTE inexistente : "
            End If
        Else
            xError = xError & Chr(10) & "Falta AUTORIZANTE : "
        End If
        ' 14. Controla el Business Partner de la petici�n
        If sBpar <> "" Then
            If sp_GetRecurso(sBpar, "R") Then
                aplRST.Close
                If Not sp_GetRecursoPerfil(sBpar, "BPAR") Then
                    xError = xError & Chr(10) & "El recurso no es B.P.: "
                Else
                   aplRST.Close
                End If
            Else
                xError = xError & Chr(10) & "B.PARTNER inexistente : "
            End If
        Else
            xError = xError & Chr(10) & "Falta B.PARTNER : "
        End If
        ' 15. Control de fechas de la petici�n
        dFecalt = Null
        dFecreq = Null
        dFeccom = Null
        dIniPlan = Null
        dFinPlan = Null
        dIniReal = Null
        dFinReal = Null
        dPasajeProduccion = Null    ' add -003- a.
        ' 15.a. Validaci�n de fecha de alta
        If IsEmpty(xlSheet.Cells(xRow, colFECALT)) Then
            dFecalt = date
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECALT)) Then
            xError = xError & Chr(10) & "Fecha alta inv�lida : "
            dFecalt = date
        Else
            dFecalt = xlSheet.Cells(xRow, colFECALT)
        End If
        If dFecalt > date Then
            xError = xError & Chr(10) & "Fecha alta fuera de rango : "
        End If
        ' 15.b. Validaci�n de fecha requerida
        If IsEmpty(xlSheet.Cells(xRow, colFECREQ)) Then
            dFecreq = Null
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECREQ)) Then
            xError = xError & Chr(10) & "Fecha requerida inv�lida : "
            dFecreq = Null
        Else
            dFecreq = xlSheet.Cells(xRow, colFECREQ)
        End If
        If Not IsNull(dFecreq) Then
            If dFecreq <= dFecalt Then
                xError = xError & Chr(10) & "Fecha requerida fuera de rango : "
            End If
        End If
        ' 15.c. Validaci�n de fecha comite
        If IsEmpty(xlSheet.Cells(xRow, colFECCOM)) Then
            dFeccom = date
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECCOM)) Then
            xError = xError & Chr(10) & "Fecha comit� inv�lida : "
            dFeccom = Null
        Else
            dFeccom = xlSheet.Cells(xRow, colFECCOM)
        End If
        If Not IsNull(dFeccom) Then
            If dFeccom < dFecalt Or dFeccom > date Then
                xError = xError & Chr(10) & "Fecha comite fuera de rango : "
            End If
        End If
        ' CANCEL REVISA y SUSPEN no validan nada mas
        If InStr("CANCEL|SUSPEN|REVISA", sEstado) > 0 Then
            GoTo doInsert
        End If
        ' 15.d. Validaci�n de fecha IniPlan
        If InStr("PLANOK|EJECUC|TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECIPL)) Then
                xError = xError & Chr(10) & "Fecha Ini.Plan. no informada : "
                dIniPlan = date
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECIPL)) Then
                xError = xError & Chr(10) & "Fecha Ini.Plan. inv�lida : "
                dIniPlan = date
            Else
                dIniPlan = xlSheet.Cells(xRow, colFECIPL)
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECIPL)) Then
                xError = xError & Chr(10) & "No debe informar F.Ini.Plan. : "
            End If
        End If
        ' 15.e. Validaci�n de fecha FinPlan
        If InStr("PLANOK|EJECUC|TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECFPL)) Then
                'xError = xError & chr(10) & "Fecha Fin.Plan. no informada : "
                dFinPlan = dIniPlan
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECFPL)) Then
                xError = xError & Chr(10) & "Fecha Fin.Plan. inv�lida : "
            Else
                dFinPlan = xlSheet.Cells(xRow, colFECFPL)
            End If
            If dFinPlan < dIniPlan Then
                xError = xError & Chr(10) & "Fecha Fin.Plan. fuera de rango : "
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECFPL)) Then
                 xError = xError & Chr(10) & "No debe informar F.Fin.Plan. : "
            End If
        End If
        ' 15.f. Validaci�n de fecha IniReal
        If InStr("EJECUC|TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECIRE)) Then
                dIniReal = dIniPlan
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECIRE)) Then
                xError = xError & Chr(10) & "Fecha Ini.Real inv�lida : "
                dIniReal = dIniPlan
            Else
                dIniReal = xlSheet.Cells(xRow, colFECIRE)
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECIRE)) Then
                xError = xError & Chr(10) & "No debe informar F.Ini.Real : "
            End If
        End If
        ' 15.g. Validaci�n de fecha FinReal
        bImport = True      ' add -001- d. - Se inicializa en verdadero para que por defecto haga las inserciones.
        If InStr("TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECFRE)) Then
                dFinReal = dIniReal
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECFRE)) Then
                xError = xError & Chr(10) & "Fecha Fin.Real. inv�lida : "
                dFinReal = dIniReal
            Else
                dFinReal = xlSheet.Cells(xRow, colFECFRE)
            End If
            If dFinReal < dIniReal Then
                xError = xError & Chr(10) & "Fecha Fin.Real fuera de rango : "
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECFRE)) Then
                xError = xError & Chr(10) & "No debe informar F.Fin Real : "
            End If
        End If
        ' 16. Control de textos varios de la petici�n (descripci�n, caracter�sticas, etc.
        If sDESCRI = "" Then
            xError = xError & Chr(10) & "Falta Descripci�n : "
        End If
        If sTipo <> "PRO" Then
            If sCARACT = "" Then
                xError = xError & Chr(10) & "Falta Caracter�sticas : "
            End If
            If sMOTIVO = "" Then
                xError = xError & Chr(10) & "Falta Motivo : "
            End If
            If sBENEFI = "" Then
                xError = xError & Chr(10) & "Falta Beneficios : "
            End If
        End If
        ' 18. Control obligatorio de empresa
        If lEmpresa = "" Then
            xError = xError & Chr(10) & "Falta Empresa : "
        Else
            If InStr(1, "1|2|3|4|", lEmpresa, vbTextCompare) = 0 Then
                xError = xError & Chr(10) & "Empresa debe ser 1, 2, 3 o 4 : "
            End If
        End If
        
        'GMT02 - INI
         ' 19. Se validan las preguntas de data
        If sPregData1 <> "S" And sPregData1 <> "N" And sPregData1 <> "" Then
           xError = xError & Chr(10) & "Valor v�lido para DATA1 es S; N o espacios"
        End If
        
        If sPregData2 <> "S" And sPregData2 <> "N" And sPregData2 <> "" Then
           xError = xError & Chr(10) & "Valor v�lido para DATA2 es S; N o espacios"
        End If
        'GMT02 - FIN
        
        'GMT03 - INI
        If sCategoria <> "SDA" And sCategoria <> "" Then
            xError = xError & Chr(10) & "Valores validos para CATEGORIA son: SDA o espacios"
        End If
        If sCategoria = "SDA" And sAgrupa = "" Then
            xError = xError & Chr(10) & "Por favor ingrese un agrupamiento para el SDA"
        End If
        
        If sAgrupa <> "" Then
            If IsNumeric(sAgrupa) Then
              sAgrupaNum = CInt(sAgrupa)
              If Not sp_GetAgrup(sAgrupaNum) Then
                  xError = xError & Chr(10) & "El n�mero ingresado de agrupamiento no existe"
              Else
                NroAgrupAsignado = sAgrupaNum
              End If
            Else
            ' Verifico si la descripcion ingresado del agrupamiento ya existe
              If sp_GetAgrupTitulo(sAgrupa) Then
                exiteAgrup = True
                NroAgrupAsignado = ClearNull(aplRST.Fields!agr_nrointerno)
              Else
                exiteAgrup = False
              End If
            End If
        End If
        'GMT03 - FIN
        
doInsert:
        Debug.Print xError
        'ALTA de la PETICION
        If xError <> "" Then
            GoTo errRow
        End If
        sPet_sox001 = 1
        glNumeroPeticion = 0
        If sp_UpdatePeticion(Null, 0, sTitulo, sTipo, sPrioridad, sCorLoc, sOrientacion, sImportancia, "", "", 0, 0, dFecalt, dFecreq, dFeccom, dIniPlan, dFinPlan, dIniReal, dFinReal, 0, sDirSol, sGerSol, sSecSol, glLOGIN_ID_REEMPLAZO, sSoli, sRefe, "", sDire, sEstado, date, "", sBpar, sPetClass, sPetImpTech, sPet_sox001, sRegulatorio, 0, 0, 0, lEmpresa, Null, Null, Null, Null, Null) Then     ' upd -008- a.
            If Not aplRST.EOF Then  ' Recupera el ultimo numero generado
                glNumeroPeticion = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
            End If
        End If
        If Val(glNumeroPeticion) = 0 Then
            xError = xError & Chr(10) & "Error al generar Peticion : "
            GoTo errRow
        End If
        
        'GMT02 - INI
        If sPregData1 <> "" Or sPregData2 <> "" Then
          'Se da de alta la respuesta de la pregunta 1 de data en validacion tecnica
           Call sp_InsertPeticionValidacion(glNumeroPeticion, cvalTecData, 1, sPregData1)
          'Se da de alta la respuesta de la pregunta 2 de data en validacion tecnica
           Call sp_InsertPeticionValidacion(glNumeroPeticion, cvalTecData, 2, sPregData2)
        End If
        'GMT02 - FIN
        
        'GMT03 - INI
        If sAgrupa <> "" Then
            If exiteAgrup Then
        ' como ya existe el agrupamiento solo se asocia la peticion
                Call sp_InsAgrupPetic(NroAgrupAsignado, glNumeroPeticion)
            Else
        ' Si no existe el agrupamiento de crea y se asocia la peticion
                If sCategoria = "SDA" Then
                    sNropadre = 1819
                Else
                    sNropadre = 0
                End If
                
                NroAgrupAsignado = sp_InsAgrup(sNropadre, sAgrupa, "S", glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo, glLOGIN_ID_REEMPLAZO, "PUB", "GER", "S")
                If Not aplRST.EOF Then
                    NroAgrupAsignado = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
                End If
                If Val(NroAgrupAsignado) > 1 Then
                    Call sp_InsAgrupPetic(NroAgrupAsignado, glNumeroPeticion)
                Else
                    bAsigna = False
                    xError = xError & Chr(10) & "Error al asignar n�mero : "
                End If
            End If
        End If
        'GMT03 - FIN
        
        If sBENEFI <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "BENEFICIOS", sBENEFI)
        If sCARACT <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "CARACTERIS", sCARACT)
        If sDESCRI <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "DESCRIPCIO", sDESCRI)
        If sMOTIVO <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "MOTIVOS", sMOTIVO)
        If sOBSERV <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "OBSERVACIO", sOBSERV)
        If sRELACI <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "RELACIONES", sRELACI)
        bAsigna = True
        nNumeroAsignado = 0
        nNumeroAsignado = sp_ProximoNumero("ULPETASI")
        If nNumeroAsignado > 0 Then
            If Not sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, nNumeroAsignado) Then
                bAsigna = False
                xError = xError & Chr(10) & "Error al asignar n�mero : "
            Else
                xlSheet.Cells(xRow, colMARK) = nNumeroAsignado
            End If
        Else
            xError = xError & Chr(10) & "Error al asignar n�mero : "
            bAsigna = False
        End If
        If bAsigna Then
            Call sp_AddHistorial(glNumeroPeticion, "PIMPORT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Importaci�n de Petici�n: " & nNumeroAsignado & " �")
            If ClearNull(sSecEjc) <> "" Then
                Call sp_InsertPeticionSector(glNumeroPeticion, sSecEjc, dIniPlan, dFinPlan, dIniReal, dFinReal, 0, sEstado, date, "", 0, "0")
            End If
            '{ add -008- a.
            If ClearNull(sGrpEjc) <> "" Then
                Call sp_InsertPeticionGrupo(glNumeroPeticion, sGrpEjc, dIniPlan, dFinPlan, dIniReal, dFinReal, Null, Null, Null, 0, sEstado, date, "", 0, "0")
                ' { add -010- a. Alta autom�tica del sector/grupo homologador
                If InStr(1, "COMITE|APROBA|PLANOK|PLANIF|EJECUC|SUSPEN|", sEstado, vbTextCompare) > 0 Then
                    If sGerEjc = "DESA" Then    ' Si el grupo ejecutor pertenece a DyD
                        If sp_GetGrupoHomologacion Then
                            sSectorHomologador = ClearNull(aplRST.Fields!cod_sector)
                            sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
                            Call sp_InsertPeticionSector(glNumeroPeticion, sSectorHomologador, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
                            Call sp_InsertPeticionGrupo(glNumeroPeticion, sGrupoHomologador, Null, Null, Null, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
                            Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, sGrupoHomologador, "ESTIMA", "", "")
                        End If
                    End If
                End If
                '}
            End If
            '}
        Else
            Call sp_DeletePeticion(glNumeroPeticion)
        End If
errRow:
        If xError <> "" Then
            xlSheet.Cells(xRow, colMARK) = "ERROR"
            bError = True
            pBD = pBD + 1
        Else
            xlSheet.Cells(xRow, colMARK) = nNumeroAsignado
            pOK = pOK + 1
        End If
skipRow:
        xlSheet.Cells(xRow, colERROR) = xError
        xRow = xRow + 1
    Wend
    If bError Then
        With xlSheet.Columns(colERROR)
            .WrapText = True
            .Orientation = 0
            .IndentLevel = 0
            .ShrinkToFit = False
            .MergeCells = False
            .ColumnWidth = 40
        End With
    End If
    Call Puntero(False)
    Call Status("Listo")
    ExcelWrk.Save
    ExcelWrk.Close
    ExcelApp.Application.Quit
    
    sMensajeResultado = "Procesadas: " & iRow & Chr$(13) & _
                        "Salteadas: " & pSKIP & Chr$(13) & _
                        "Correctas: " & pOK & Chr$(13) & _
                        "Con error: " & pBD & Chr$(13) & Chr$(13) & _
                        "La descripci�n de los errores se encuentra en la planilla"
    If pBD > 0 Then
        MsgBox sMensajeResultado, vbCritical + vbOKOnly, "Resultado"
    Else
        MsgBox sMensajeResultado, vbInformation + vbOKOnly, "Resultado"
    End If
    Exit Sub
errCopy:
    Exit Sub
finImpo:
'{ add -007- a.
    Call Puntero(False)
    Exit Sub
Fin:
    Call Puntero(False)
    Exit Sub
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
    End Select
'}
End Sub

Sub ImportarNuevo()
    On Error GoTo Errores
    Const colMARK = 1               ' A
    Const colEMPRESA = 2            ' B
    Const colTIPO = 3               ' C
    Const colCLASE = 4              ' D
    Const colImpacto = 5            ' E
    Const colRegulatorio = 6        ' F
    Const colVisibilidad = 7        ' G
    Const colRO = 8                 ' H
    Const colTitulo = 9             ' I
    Const colPRIORI = 10            ' J
    Const colORIENT = 11            ' K
    Const colCORLOC = 12            ' L
    Const colIMPORT = 13            ' M
    Const colFECALT = 14            ' N
    Const colFECREQ = 15            ' O
    Const colFECCOM = 16            ' P
    Const colSOLICI = 17            ' Q
    Const colREFERE = 18            ' R
    Const colSUPERVISOR = 19        ' S
    Const colAUTORI = 20            ' T
    Const colSecSol = 21            ' U
    Const colREFBPE = 22            ' V
    Const colBPAR = 23              ' W
    Const colSecEjc = 24            ' X
    Const colGrpEjc = 25            ' Y
    Const colESTADO = 26            ' Z
    Const colFECIPL = 27            ' AA
    Const colFECFPL = 28            ' AB
    Const colFECIRE = 29            ' AC
    Const colFECFRE = 30            ' AD
    Const colDESCRI = 31            ' AE
    Const colCARACT = 32            ' AF
    Const colMOTIVO = 33            ' AG
    Const colBENEFI = 34            ' AH
    Const colRELACI = 35            ' AI
    Const colOBSERV = 36            ' AJ
    Const colGestion = 37           ' AK
    Const colValoracion = 38        ' AL
    Const colERROR = 39             ' AM
    
    Dim vIndicadores() As Long
    Dim vIndicadoresSuma() As Long
    Dim vIndicador() As Long
    Dim vDrivers() As Long
    'Dim vDriverValor() As Double
    Dim vOpcionSelecccionada() As Long
    'Dim vValorReferencia() As Double
    
    Dim sSoli, sRefe, sSupe, sSuper, sAuto, sBpar, sBPE As String
    Dim sDirSol, sGerSol, sSecSol As String
    Dim sAutojc, sGerEjc, sSecEjc As String
    Dim dFecalt, dFecreq, dFeccom As Variant
    Dim dIniPlan, dFinPlan, dIniReal, dFinReal, dPasajeProduccion As Variant    ' upd -003- a. Se agrega la variable 'dPasajeProduccion'
    Dim sTitulo, sTipo, sClase, sImpacto, sRegulatorio, sPrioridad, sRO, sEstado, sGestion As String
    Dim sImportancia As String
    Dim sOrientacion As String
    Dim sCorLoc As String
    Dim sPetClass As String * 4
    Dim sPetImpTech As String * 1
    Dim sPet_sox001 As Byte         ' add -001- b.
    Dim xError As String
    Dim bError As Boolean
    Dim bPerfil As Boolean
    Dim bAsigna As Boolean
    Dim bCargaBeneficios As Boolean
    Dim sDESCRI, sCARACT, sMOTIVO, sBENEFI, sOBSERV, sRELACI As String
    Dim ExcelApp, ExcelWrk, xlSheet As Object
    Dim xRow, iRow As Long
    Dim pOK As Long
    Dim pBD As Long
    Dim pSKIP As Long
    Dim nUltimoAsignado As Long
    Dim nNumeroAsignado As Long
    Dim bImport As Boolean          ' add -001- d.
    'Dim lEmpresa As String          ' add -008- a.
    Dim lEmpresa As Long
    Dim sGrpEjc As String           ' add -008- a.
    Dim txtFile As String
    Dim sMensajeResultado As String
    '{ add -010- a.
    Dim sGrupoHomologador As String
    Dim sSectorHomologador As String
    '}
    
    '{ add -007- a.
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = "Importar archivo desde Microsoft Excel"
        .Filter = "Microsoft Excel (*.xls, *.xlsx)|*.xls*"
        .FilterIndex = 1
        .InitDir = glWRKDIR
        .Flags = cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        txtFile = .FileName
        If Len(Trim(txtFile)) = 0 Then
            GoTo finImpo
        End If
    End With
    '}
    If MsgBox("�Confirma inicio de importaci�n?", vbQuestion + vbOKCancel, "ATENCION") = vbCancel Then
        GoTo finImpo
    End If
    
    Call Puntero(True)
    Call Status("Iniciando proceso de importaci�n...")
    
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.visible = False
    On Error Resume Next
    Set ExcelWrk = ExcelApp.Workbooks.Open(txtFile)    ' upd -007- a.
    If ExcelWrk Is Nothing Then
        MsgBox "Error al abrir planilla", vbCritical
        GoTo finImpo
    End If
    Set xlSheet = ExcelWrk.Sheets("PETICIONES")
    If xlSheet Is Nothing Then
        ExcelWrk.Close
        ExcelApp.Application.Quit
        MsgBox "Error, hoja PETICIONES inexistente", vbCritical
        GoTo finImpo
    End If
    xRow = 2
   
    On Error GoTo 0

    xRow = xRow + 1
    iRow = 0
    
    bError = False
    
    While ClearNull(xlSheet.Cells(xRow, colTIPO)) <> "" 'And Asc(ClearNull(xlSheet.Cells(xRow, colTIPO))) <> 10  ' upd -008- a.
        iRow = iRow + 1
        Call Status("Proc: " & iRow)
        DoEvents
        xError = ""
        If ClearNull(xlSheet.Cells(xRow, colMARK)) <> "" And ClearNull(xlSheet.Cells(xRow, colMARK)) <> "ERROR" Then
            pSKIP = pSKIP + 1
            GoTo skipRow
        End If
        lEmpresa = CLng(ClearNull(xlSheet.Cells(xRow, colEMPRESA)))
        sSoli = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSOLICI))), 10)
        sRefe = Left(UCase(ClearNull(xlSheet.Cells(xRow, colREFERE))), 10)
        sSupe = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSUPERVISOR))), 10)
        sAuto = Left(UCase(ClearNull(xlSheet.Cells(xRow, colAUTORI))), 10)
        sBpar = Left(UCase(ClearNull(xlSheet.Cells(xRow, colBPAR))), 10)
        sBPE = Left(UCase(ClearNull(xlSheet.Cells(xRow, colREFBPE))), 10)
        sDirSol = ""
        sGerSol = ""
        sSecSol = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSecSol))), 8)
        sTitulo = Left(ClearNull(xlSheet.Cells(xRow, colTitulo)), 50)
        sTipo = Left(UCase(ClearNull(xlSheet.Cells(xRow, colTIPO))), 3)
        'sClase = Left(UCase(ClearNull(xlSheet.Cells(xRow, colClase))), 4)
        'sImpacto = Left(UCase(ClearNull(xlSheet.Cells(xRow, colImpacto))), 1)
        sRegulatorio = Left(UCase(ClearNull(xlSheet.Cells(xRow, colRegulatorio))), 1)
        'sVisibilidad = Left(UCase(ClearNull(xlSheet.Cells(xRow, colVisibilidad))), 1)
        sPrioridad = Left(ClearNull(xlSheet.Cells(xRow, colPRIORI)), 1)
        sRO = Left(ClearNull(xlSheet.Cells(xRow, colRO)), 1)
        sImportancia = ClearNull(xlSheet.Cells(xRow, colIMPORT))
        sEstado = Left(UCase(ClearNull(xlSheet.Cells(xRow, colESTADO))), 6)
        sOrientacion = ClearNull(xlSheet.Cells(xRow, colORIENT))
        sCorLoc = ClearNull(xlSheet.Cells(xRow, colCORLOC))
        sPetClass = Left(UCase(ClearNull(xlSheet.Cells(xRow, colCLASE))), 4)
        sPetImpTech = Left(UCase(ClearNull(xlSheet.Cells(xRow, colImpacto))), 1)
        sGestion = Left(UCase(ClearNull(xlSheet.Cells(xRow, colGestion))), 4)
        bCargaBeneficios = IIf(Left(UCase(ClearNull(xlSheet.Cells(xRow, colValoracion))), 1) = "S", True, False)
        
        sDESCRI = ClearNull(xlSheet.Cells(xRow, colDESCRI))
        sCARACT = ClearNull(xlSheet.Cells(xRow, colCARACT))
        sMOTIVO = ClearNull(xlSheet.Cells(xRow, colMOTIVO))
        sBENEFI = ClearNull(xlSheet.Cells(xRow, colBENEFI))
        sOBSERV = ClearNull(xlSheet.Cells(xRow, colOBSERV))
        sRELACI = ClearNull(xlSheet.Cells(xRow, colRELACI))
        
        sDESCRI = ReplaceString(sDESCRI, 10, vbNewLine)
        sCARACT = ReplaceString(sCARACT, 10, vbNewLine)
        sMOTIVO = ReplaceString(sMOTIVO, 10, vbNewLine)
        sBENEFI = ReplaceString(sBENEFI, 10, vbNewLine)
        sOBSERV = ReplaceString(sOBSERV, 10, vbNewLine)
        sRELACI = ReplaceString(sRELACI, 10, vbNewLine)
        
        sAutojc = ""
        sGerEjc = ""
        sSecEjc = ""
        sGrpEjc = ""
        '{ del -008- a.
        'sSecEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSecEjc))), 8)
        'sGrpEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colGrpEjc))), 8)
        '}
        '{ add -008- a.
        If InStr(1, ClearNull(xlSheet.Cells(xRow, colSecEjc)), ":", vbTextCompare) > 0 Then
            sSecEjc = Mid(ClearNull(xlSheet.Cells(xRow, colSecEjc)), 1, InStr(1, ClearNull(xlSheet.Cells(xRow, colSecEjc)), ":", vbTextCompare) - 1)
        Else
            sSecEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSecEjc))), 8)
        End If
        If InStr(1, ClearNull(xlSheet.Cells(xRow, colGrpEjc)), ":", vbTextCompare) > 0 Then
            sGrpEjc = Mid(ClearNull(xlSheet.Cells(xRow, colGrpEjc)), 1, InStr(1, ClearNull(xlSheet.Cells(xRow, colGrpEjc)), ":", vbTextCompare) - 1)
        Else
            sGrpEjc = Left(UCase(ClearNull(xlSheet.Cells(xRow, colGrpEjc))), 8)
        End If
        '}
        'sRegulatorio = IIf(InStr(1, sTitulo, "BCRA", vbTextCompare) > 0, "S", "N")  ' add -006- a.
        '**************************************************************************************************
        ' CONTROLES REALIZADOS ANTES DE IMPORTAR LOS DATOS
        '**************************************************************************************************
        '    1. Control del tipo de petici�n
        '    2. Control del t�tulo de la petici�n
        '    3. Control de visibilidad de la petici�n (solo Proyectos)
        '    4. Control de orientaci�n (solo Proyectos)
        '    5. Control de Corporativo o Local (solo Protectos)
        '    6. Control de Estado de la petici�n
        '    7. Control de prioridad
        '    8. Control de Sector solicitante
        '    9. Control de Sector ejecutor
        '   10. Control de Grupo ejecutor
        '   11. Controla el solicitante de la petici�n
        '   12. Controla el referente de la petici�n
        '   13. Controla el autorizante de la petici�n
        '   14. Controla el Business Partner de la petici�n
        '   15. Control de fechas de la petici�n
        '       15.a. Validaci�n de fecha de alta
        '       15.b. Validaci�n de fecha requerida
        '       15.c. Validaci�n de fecha comite
        '       15.d. Validaci�n de fecha IniPlan
        '       15.e. Validaci�n de fecha FinPlan
        '       15.f. Validaci�n de fecha IniReal
        '       15.g. Validaci�n de fecha FinReal
        '   16. Control de textos varios de la petici�n (descripci�n, caracter�sticas, etc.)
        '   17. Control de campo regulatorio (SI, NO, Indeterminado)
        '   18. Empresa (obligatorio)
        '
        '**************************************************************************************************
        ' 1. Control del tipo de petici�n
        If sTipo = "" Then
            xError = xError & Chr(10) & "Falta Tipo de Peticion : "
        ElseIf InStr("PRO|NOR|ESP|AUI|AUX|PRJ", sTipo) = 0 Then
            xError = xError & Chr(10) & "Tipo de Peticion incorrecta o no definido : "
        End If
        ' 2. Control del t�tulo de la petici�n
        If sTitulo = "" Then
            xError = xError & Chr(10) & "Falta T�tulo : "
        End If
        ' 3. Control de visibilidad de la petici�n (solo Proyectos)
        If sTipo = "PRJ" Then
            If sImportancia = "" Then
                xError = xError & Chr(10) & "VISIBILIDAD no informada : "
            ElseIf InStr("10|20|30|40", sImportancia) = 0 Then
                xError = xError & Chr(10) & "VISIBILIDAD incorrecta : "
            End If
'        Else
'            If sImportancia <> "" Then
'                xError = xError & Chr(10) & "No debe informar VISIBILIDAD : "
'            End If
        End If
        ' 4. Control de orientaci�n (solo Proyectos)
        sOrientacion = "D"  'GMT01
        'If sTipo = "PRJ" Then
            If sOrientacion = "" Then
                xError = xError & Chr(10) & "Orientaci�n no informada : "
            ElseIf Not sp_GetOrientacion(sOrientacion, Null) Then
                xError = xError & Chr(10) & "Orientaci�n incorrecta : "
            End If
'        Else
'            If sOrientacion <> "" Then
'                xError = xError & Chr(10) & "No debe informar orientaci�n : "
'            End If
'        End If
        ' 5. Control de Corporativo o Local (solo Protectos)
        If sTipo = "PRJ" Then
            If sCorLoc = "" Then
                xError = xError & Chr(10) & "C/L no informado : "
            ElseIf InStr("C|L", sCorLoc) = 0 Then
                xError = xError & Chr(10) & "C/L incorrecta : "
            End If
        Else
            sCorLoc = "L"
        End If
        ' 6. Control de Estado de la petici�n
        If sEstado = "" Then
            xError = xError & Chr(10) & "Estado no informado : "
        ElseIf InStr("CONFEC|REFBPE|COMITE|APROBA|PLANIF|PLANOK|EJECUC|TERMIN|CANCEL|SUSPEN|", sEstado) = 0 Then
            xError = xError & Chr(10) & "Estado incorrecto : "
        End If
        '{ add -004- a.
        'If InStr("RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|", sEstado) > 0 Then
        If InStr("RECHAZ|RECHTE|ANEXAD|ANULAD|TERMIN|", sEstado) > 0 Then
            xError = xError & Chr(10) & "Estado terminal : "
        End If
        '}
'        ' 7. Control de prioridad
'        If InStr("PRO|ESP|PRJ", sTipo) > 0 Or InStr("COMITE|CANCEL|SUSPEN|REVISA", sEstado) = 0 Then
'            If sPrioridad = "" Then
'                xError = xError & Chr(10) & "Prioridad no informada : "
'            ElseIf InStr("1|2|3|4|5|6", sPrioridad) = 0 Then
'                xError = xError & Chr(10) & "Prioridad incorrecta : "
'            End If
'        End If
        ' 8. Control de Sector solicitante
        If sSecSol = "" Then
            xError = xError & Chr(10) & "Falta Sector Solic. : "
        ElseIf Not sp_GetSectorXt(sSecSol, Null, Null) Then
            sSecSol = ""
            xError = xError & Chr(10) & "Sector Solic. inexistente : "
        ElseIf aplRST.EOF Then
            sSecSol = ""
            xError = xError & Chr(10) & "Sector Solic. inexistente : "
        Else
            sDirSol = ClearNull(aplRST!cod_direccion)
            sGerSol = ClearNull(aplRST!cod_gerencia)
            aplRST.Close
            If Not valSectorHabil(sSecSol) Then
                xError = xError & Chr(10) & "Sector Solic. no habilitado : "
            Else
                If sGerSol = "" Then
                    xError = xError & Chr(10) & "Error en Gerencia Solic. : "
                End If
                If sDirSol = "" Then
                    xError = xError & Chr(10) & "Error en Direccion Solic. : "
                End If
            End If
        End If
        ' 9. Control de Sector ejecutor:
        ' ---------------------------
        ' Si la petici�n esta en alguno de los siguientes estados:
        ' - A planificar
        ' - Planificado
        ' - En ejecuci�n
        ' - Finalizada
        ' O:
        ' - Cancelado definitivamente
        ' - Suspendido temporalmente
        ' - A revisar
        If InStr("PLANIF|PLANOK|EJECUC|TERMIN", sEstado) > 0 Then
            If sSecEjc = "" Then
                xError = xError & Chr(10) & "Falta Sector ejecuc. : "
            ElseIf Not sp_GetSectorXt(sSecEjc, Null, Null) Then
                sSecEjc = ""
                xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
            ElseIf aplRST.EOF Then
                sSecEjc = ""
                xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
            Else
                sAutojc = ClearNull(aplRST!cod_direccion)
                sGerEjc = ClearNull(aplRST!cod_gerencia)
                aplRST.Close
                If Not valSectorHabil(sSecEjc) Then
                    xError = xError & Chr(10) & "Sector ejecuc. no habilitado : "
                Else
                    If sGerEjc = "" Then
                        xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    End If
                    If sAutojc = "" Then
                        xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                    End If
                End If
            End If
        ElseIf InStr("CANCEL|SUSPEN|REVISA", sEstado) > 0 Then
            If sSecEjc <> "" Then
                If Not sp_GetSectorXt(sSecEjc, Null, Null) Then
                    sSecEjc = ""
                    xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
                ElseIf aplRST.EOF Then
                    sSecEjc = ""
                    xError = xError & Chr(10) & "Sector ejecuc. inexistente : "
                Else
                    sAutojc = ClearNull(aplRST!cod_direccion)
                    sGerEjc = ClearNull(aplRST!cod_gerencia)
                    aplRST.Close
                    If sGerEjc = "" Then
                        xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    End If
                    If sAutojc = "" Then
                        xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                    End If
                End If
            End If
        Else
            If sSecEjc <> "" Then
                 xError = xError & Chr(10) & "No debe informar Sector ejecuc. : "
            End If
        End If
        ' 10. Control de Grupo ejecutor:
        If InStr("PLANIF|PLANOK|EJECUC|TERMIN|", sEstado) > 0 Then
            If sGrpEjc = "" Then
                xError = xError & Chr(10) & "Falta Grupo ejecuc. : "
            ElseIf Not sp_GetGrupoXt(sGrpEjc, Null, Null) Then
                sGrpEjc = ""
                xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
            ElseIf aplRST.EOF Then
                sGrpEjc = ""
                xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
            Else
                If Not valGrupoHabil(sGrpEjc) Then
                    xError = xError & Chr(10) & "Grupo ejecuc. no habilitado : "
                Else
                    If sSecEjc = "" Then xError = xError & Chr(10) & "Error en Sector ejecuc. : "
                    If sGerEjc = "" Then xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    If sAutojc = "" Then xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                End If
            End If
            '{ add -009- a.
            If Not sp_GetRecursoActuanteEstado("GRU", "GRUP", sGrpEjc, sEstado) Then
                xError = xError & Chr(10) & "No existen recursos que puedan procesar esta solicitud en el grupo ejecuc. : "
            End If
            '}
        ElseIf InStr("CANCEL|SUSPEN|REVISA|", sEstado) > 0 Then
            If sGrpEjc <> "" Then
                If Not sp_GetGrupoXt(sGrpEjc, Null, Null) Then
                    sGrpEjc = ""
                    xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
                ElseIf aplRST.EOF Then
                    sGrpEjc = ""
                    xError = xError & Chr(10) & "Grupo ejecuc. inexistente : "
                Else
                    If sSecEjc = "" Then xError = xError & Chr(10) & "Error en Sector ejecuc. : "
                    If sGerEjc = "" Then xError = xError & Chr(10) & "Error en Gerencia ejecuc. : "
                    If sAutojc = "" Then xError = xError & Chr(10) & "Error en Direccion ejecuc. : "
                End If
            End If
        Else
            If sGrpEjc <> "" Then
                 xError = xError & Chr(10) & "No debe informar Grupo ejecuc. : "
            End If
        End If
        
        ' 11. Controla el solicitante de la petici�n
        bPerfil = False
        If sSoli <> "" Then
            If sp_GetRecurso(sSoli, "R") Then
                aplRST.Close
                If sSecSol <> "" Then
                    If sp_GetRecursoActuanteEstado("PET", "SECT", sSecSol, "CONFEC") Then
                        While Not aplRST.EOF
                            If sSoli = ClearNull(aplRST!cod_recurso) Then
                                bPerfil = True
                            End If
                            aplRST.MoveNext
                        Wend
                        aplRST.Close
                        If Not bPerfil Then
                            xError = xError & Chr(10) & "El SOLICITANTE no puede procesar esta peticion : "
                        End If
                    Else
                        xError = xError & Chr(10) & "No existen SOLICITANTES que puedan procesar esta peticion : "
                    End If
                End If
            Else
                xError = xError & Chr(10) & "SOLICITANTE inexistente : "
            End If
        Else
            xError = xError & Chr(10) & "Falta SOLICITANTE : "
        End If
        ' 12. Controla el referente de la petici�n
        bPerfil = False
        If sEstado <> "CONFEC" Then
            If sRefe <> "" Then
                If sp_GetRecurso(sRefe, "R") Then
                    aplRST.Close
                    If sSecSol <> "" Then
                        If sp_GetRecursoActuanteEstado("PET", "SECT", sSecSol, "REFERE") Then
                            While Not aplRST.EOF
                                If sRefe = ClearNull(aplRST!cod_recurso) Then
                                    bPerfil = True
                                End If
                                aplRST.MoveNext
                            Wend
                            aplRST.Close
                            If Not bPerfil Then
                                xError = xError & Chr(10) & "El REFERENTE no puede procesar esta peticion : "
                            End If
                        Else
                            xError = xError & Chr(10) & "No existen REFERENTES que puedan procesar esta peticion : "
                        End If
                    End If
                Else
                    xError = xError & Chr(10) & "REFERENTE inexistente : "
                End If
            Else
                xError = xError & Chr(10) & "Falta REFERENTE : "
            End If
        End If
        ' 13. Controla el autorizante de la petici�n
        bPerfil = False
        If sEstado <> "CONFEC" Then
            If sAuto <> "" Then
                If sp_GetRecurso(sAuto, "R") Then
                    aplRST.Close
                    If sSecSol <> "" Then
                        If sp_GetRecursoActuanteEstado("PET", "SECT", sSecSol, "AUTORI") Then
                            While Not aplRST.EOF
                                If sAuto = ClearNull(aplRST!cod_recurso) Then
                                    bPerfil = True
                                End If
                                aplRST.MoveNext
                            Wend
                            aplRST.Close
                            If Not bPerfil Then
                                xError = xError & Chr(10) & "El AUTORIZANTE no puede procesar esta peticion : "
                            End If
                        Else
                            xError = xError & Chr(10) & "No existen AUTORIZANTES que puedan procesar esta peticion : "
                        End If
                    End If
                Else
                    xError = xError & Chr(10) & "AUTORIZANTE inexistente : "
                End If
            Else
                xError = xError & Chr(10) & "Falta AUTORIZANTE : "
            End If
        End If
        ' 14. Controla el Business Partner de la petici�n
        If sEstado <> "CONFEC" Then
            If sBpar <> "" And sEstado <> "CONFEC" Then
                If sp_GetRecurso(sBpar, "R") Then
                    aplRST.Close
                    If Not sp_GetRecursoPerfil(sBpar, "BPAR") Then
                        xError = xError & Chr(10) & "El recurso no es B.P.: "
                    Else
                       aplRST.Close
                    End If
                Else
                    xError = xError & Chr(10) & "B.PARTNER inexistente : "
                End If
            Else
                xError = xError & Chr(10) & "Falta B.PARTNER : "
            End If
        End If
        ' 15. Control de fechas de la petici�n
        dFecalt = Null
        dFecreq = Null
        dFeccom = Null
        dIniPlan = Null
        dFinPlan = Null
        dIniReal = Null
        dFinReal = Null
        dPasajeProduccion = Null    ' add -003- a.
        ' 15.a. Validaci�n de fecha de alta
        If IsEmpty(xlSheet.Cells(xRow, colFECALT)) Then
            dFecalt = date
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECALT)) Then
            xError = xError & Chr(10) & "Fecha alta inv�lida : "
            dFecalt = date
        Else
            dFecalt = xlSheet.Cells(xRow, colFECALT)
        End If
        If dFecalt > date Then
            xError = xError & Chr(10) & "Fecha alta fuera de rango : "
        End If
        ' 15.b. Validaci�n de fecha requerida
        If IsEmpty(xlSheet.Cells(xRow, colFECREQ)) Then
            dFecreq = Null
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECREQ)) Then
            xError = xError & Chr(10) & "Fecha requerida inv�lida : "
            dFecreq = Null
        Else
            dFecreq = xlSheet.Cells(xRow, colFECREQ)
        End If
        If Not IsNull(dFecreq) Then
            If dFecreq <= dFecalt Then
                xError = xError & Chr(10) & "Fecha requerida fuera de rango : "
            End If
        End If
        ' 15.c. Validaci�n de fecha comite
        If IsEmpty(xlSheet.Cells(xRow, colFECCOM)) Then
            dFeccom = date
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECCOM)) Then
            xError = xError & Chr(10) & "Fecha comit� inv�lida : "
            dFeccom = Null
        Else
            dFeccom = xlSheet.Cells(xRow, colFECCOM)
        End If
        If Not IsNull(dFeccom) Then
            If dFeccom < dFecalt Or dFeccom > date Then
                xError = xError & Chr(10) & "Fecha comite fuera de rango : "
            End If
        End If
        ' CANCEL REVISA y SUSPEN no validan nada mas
        If InStr("CANCEL|SUSPEN|REVISA", sEstado) > 0 Then
            GoTo doInsert
        End If
        ' 15.d. Validaci�n de fecha IniPlan
        If InStr("PLANOK|EJECUC|TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECIPL)) Then
                xError = xError & Chr(10) & "Fecha Ini.Plan. no informada : "
                dIniPlan = date
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECIPL)) Then
                xError = xError & Chr(10) & "Fecha Ini.Plan. inv�lida : "
                dIniPlan = date
            Else
                dIniPlan = xlSheet.Cells(xRow, colFECIPL)
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECIPL)) Then
                xError = xError & Chr(10) & "No debe informar F.Ini.Plan. : "
            End If
        End If
        ' 15.e. Validaci�n de fecha FinPlan
        If InStr("PLANOK|EJECUC|TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECFPL)) Then
                'xError = xError & chr(10) & "Fecha Fin.Plan. no informada : "
                dFinPlan = dIniPlan
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECFPL)) Then
                xError = xError & Chr(10) & "Fecha Fin.Plan. inv�lida : "
            Else
                dFinPlan = xlSheet.Cells(xRow, colFECFPL)
            End If
            If dFinPlan < dIniPlan Then
                xError = xError & Chr(10) & "Fecha Fin.Plan. fuera de rango : "
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECFPL)) Then
                 xError = xError & Chr(10) & "No debe informar F.Fin.Plan. : "
            End If
        End If
        ' 15.f. Validaci�n de fecha IniReal
        If InStr("EJECUC|TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECIRE)) Then
                dIniReal = dIniPlan
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECIRE)) Then
                xError = xError & Chr(10) & "Fecha Ini.Real inv�lida : "
                dIniReal = dIniPlan
            Else
                dIniReal = xlSheet.Cells(xRow, colFECIRE)
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECIRE)) Then
                xError = xError & Chr(10) & "No debe informar F.Ini.Real : "
            End If
        End If
        ' 15.g. Validaci�n de fecha FinReal
        bImport = True      ' add -001- d. - Se inicializa en verdadero para que por defecto haga las inserciones.
        If InStr("TERMIN", sEstado) > 0 Then
            If IsEmpty(xlSheet.Cells(xRow, colFECFRE)) Then
                dFinReal = dIniReal
            ElseIf Not IsDate(xlSheet.Cells(xRow, colFECFRE)) Then
                xError = xError & Chr(10) & "Fecha Fin.Real. inv�lida : "
                dFinReal = dIniReal
            Else
                dFinReal = xlSheet.Cells(xRow, colFECFRE)
            End If
            If dFinReal < dIniReal Then
                xError = xError & Chr(10) & "Fecha Fin.Real fuera de rango : "
            End If
        Else
            If Not IsEmpty(xlSheet.Cells(xRow, colFECFRE)) Then
                xError = xError & Chr(10) & "No debe informar F.Fin Real : "
            End If
        End If
        ' 16. Control de textos varios de la petici�n (descripci�n, caracter�sticas, etc.
        If sDESCRI = "" Then
            xError = xError & Chr(10) & "Falta Descripci�n : "
        End If
        If sTipo <> "PRO" Then
            If sCARACT = "" Then
                xError = xError & Chr(10) & "Falta Caracter�sticas : "
            End If
            If sMOTIVO = "" Then
                xError = xError & Chr(10) & "Falta Motivo : "
            End If
'            If sBENEFI = "" Then
'                xError = xError & Chr(10) & "Falta Beneficios : "
'            End If
        End If
        ' 18. Control obligatorio de empresa
        If lEmpresa = 0 Then
            xError = xError & Chr(10) & "Falta Empresa : "
        Else
            If InStr(1, "1|2|3|4|", lEmpresa, vbTextCompare) = 0 Then
                xError = xError & Chr(10) & "Empresa debe ser 1, 2, 3 o 4 : "
            End If
        End If
doInsert:
        Debug.Print xError
        'ALTA de la PETICION
        If xError <> "" Then
            GoTo errRow
        End If
        sPet_sox001 = 1
        glNumeroPeticion = 0
        If sp_UpdatePeticion(Null, 0, sTitulo, sTipo, sPrioridad, sCorLoc, sOrientacion, sImportancia, "", "", 0, 0, dFecalt, dFecreq, dFeccom, dIniPlan, dFinPlan, dIniReal, dFinReal, 0, sDirSol, sGerSol, sSecSol, glLOGIN_ID_REEMPLAZO, sSoli, sRefe, "", sAuto, sEstado, date, "", sBpar, sPetClass, sPetImpTech, sPet_sox001, sRegulatorio, 0, 0, 0, lEmpresa, sRO, sBPE, date, sGestion, IIf(bCargaBeneficios, "S", "N")) Then     ' upd -008- a.
            If Not aplRST.EOF Then  ' Recupera el ultimo numero generado
                glNumeroPeticion = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
            End If
        End If
        If Val(glNumeroPeticion) = 0 Then
            xError = xError & Chr(10) & "Error al generar Peticion : "
            GoTo errRow
        End If
        If sBENEFI <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "BENEFICIOS", sBENEFI)
        If sCARACT <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "CARACTERIS", sCARACT)
        If sDESCRI <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "DESCRIPCIO", sDESCRI)
        If sMOTIVO <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "MOTIVOS", sMOTIVO)
        If sOBSERV <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "OBSERVACIO", sOBSERV)
        If sRELACI <> "" Then Call sp_UpdateMemo(glNumeroPeticion, "RELACIONES", sRELACI)
        bAsigna = True
        nNumeroAsignado = 0
        nNumeroAsignado = sp_ProximoNumero("ULPETASI")
        If nNumeroAsignado > 0 Then
            If Not sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, nNumeroAsignado) Then
                bAsigna = False
                xError = xError & Chr(10) & "Error al asignar n�mero : "
            Else
                xlSheet.Cells(xRow, colMARK) = nNumeroAsignado
            End If
        Else
            xError = xError & Chr(10) & "Error al asignar n�mero : "
            bAsigna = False
        End If
        
        ' Beneficios
        If bCargaBeneficios Then
            Dim i As Long, J As Long
            i = 0
            If sp_GetIndicador(Null, lEmpresa) Then
                Do While Not aplRST.EOF
                    ReDim Preserve vIndicadores(i)
                    ReDim Preserve vIndicadoresSuma(i)
                    vIndicadores(i) = ClearNull(aplRST.Fields!indicadorId)
                    vIndicadoresSuma(i) = 0
                    aplRST.MoveNext
                    i = i + 1
                    DoEvents
                Loop
            End If
            
            Erase vDrivers
            
            J = 0
            For i = 0 To UBound(vIndicadores)
                If sp_GetDrivers(Null, "S", vIndicadores(i)) Then
                    Do While Not aplRST.EOF
                        ReDim Preserve vIndicador(J)
                        ReDim Preserve vDrivers(J)
                        vIndicador(J) = ClearNull(aplRST.Fields!indicadorId)
                        vDrivers(J) = ClearNull(aplRST.Fields!driverId)
                        aplRST.MoveNext
                        J = J + 1
                        DoEvents
                    Loop
                End If
            Next i
            ' Generamos un n�mero aleatorio para seleccionar alguna de las opciones para el driver
            Dim n As Integer
            Dim menor As Integer
            Dim mayor As Integer
            Dim totalOpciones As Long
            For i = 0 To UBound(vDrivers)
                If sp_GetValoracion(vDrivers(i), Null) Then
                    aplRST.MoveFirst: menor = aplRST.Fields!valorId
                    aplRST.MoveLast: mayor = aplRST.Fields!valorId
                    ReDim Preserve vOpcionSelecccionada(i)
                    n = Int((mayor - menor + 1) * Rnd + menor)
                    vOpcionSelecccionada(i) = n
                End If
            Next i
            ' Ac� grabo
            For i = 0 To UBound(vDrivers)
                Call sp_UpdatePeticionBeneficios(glNumeroPeticion, vIndicador(i), vDrivers(i), vOpcionSelecccionada(i))
            Next i
            
            ' Ac� grabo el puntaje calculado
            For i = 0 To UBound(vIndicadoresSuma)
                Call sp_UpdatePetField(glNumeroPeticion, "PUNTAJE", Null, Null, Null)
            Next i
        End If
        
        If bAsigna Then
            Call sp_AddHistorial(glNumeroPeticion, "PIMPORT", sEstado, Null, Null, Null, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Importaci�n de Petici�n: " & nNumeroAsignado & " �")
            If ClearNull(sSecEjc) <> "" Then
                Call sp_InsertPeticionSector(glNumeroPeticion, sSecEjc, dIniPlan, dFinPlan, dIniReal, dFinReal, 0, sEstado, date, "", 0, "0")
            End If
            '{ add -008- a.
            If ClearNull(sGrpEjc) <> "" Then
                Call sp_InsertPeticionGrupo(glNumeroPeticion, sGrpEjc, dIniPlan, dFinPlan, dIniReal, dFinReal, Null, Null, Null, 0, sEstado, date, "", 0, "0")
                ' { add -010- a. Alta autom�tica del sector/grupo homologador
                If InStr(1, "COMITE|APROBA|PLANOK|PLANIF|EJECUC|SUSPEN|", sEstado, vbTextCompare) > 0 Then
                    If sGerEjc = "DESA" Then    ' Si el grupo ejecutor pertenece a DyD
                        If sp_GetGrupoHomologacion Then
                            sSectorHomologador = ClearNull(aplRST.Fields!cod_sector)
                            sGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
                            Call sp_InsertPeticionSector(glNumeroPeticion, sSectorHomologador, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
                            Call sp_InsertPeticionGrupo(glNumeroPeticion, sGrupoHomologador, Null, Null, Null, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
                            Call sp_DoMensaje("GRU", "GNEW000", glNumeroPeticion, sGrupoHomologador, "ESTIMA", "", "")
                        End If
                    End If
                End If
                '}
            End If
            '}
        Else
            Call sp_DeletePeticion(glNumeroPeticion)
        End If
errRow:
        If xError <> "" Then
            xlSheet.Cells(xRow, colMARK) = "ERROR"
            bError = True
            pBD = pBD + 1
        Else
            xlSheet.Cells(xRow, colMARK) = nNumeroAsignado
            pOK = pOK + 1
        End If
skipRow:
        xlSheet.Cells(xRow, colERROR) = xError
        xRow = xRow + 1
    Wend
    If bError Then
        With xlSheet.Columns(colERROR)
            .WrapText = True
            .Orientation = 0
            .IndentLevel = 0
            .ShrinkToFit = False
            .MergeCells = False
            .ColumnWidth = 40
        End With
    End If
    Call Puntero(False)
    Call Status("Listo.")
    ExcelWrk.Save
    ExcelWrk.Close
    ExcelApp.Application.Quit
    
    sMensajeResultado = "Procesadas: " & iRow & Chr$(13) & _
                        "Salteadas: " & pSKIP & Chr$(13) & _
                        "Correctas: " & pOK & Chr$(13) & _
                        "Con error: " & pBD & Chr$(13) & Chr$(13) & _
                        "La descripci�n de los errores se encuentra en la planilla"
    If pBD > 0 Then
        MsgBox sMensajeResultado, vbCritical + vbOKOnly, "Resultado"
    Else
        MsgBox sMensajeResultado, vbInformation + vbOKOnly, "Resultado"
    End If
    Exit Sub
errCopy:
    Exit Sub
finImpo:
'{ add -007- a.
    Call Puntero(False)
    Exit Sub
Fin:
    Call Puntero(False)
    Exit Sub
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
    End Select
'}
End Sub
