Attribute VB_Name = "spSolicitudes"
' -001- a. FJS 28.12.2009 - Se modifica el SP para que devuelva el nro. de solicitud asignado.
' -002- a. FJS 08.01.2010 - Se adecua para que no genere error por valor inv�lido cuando no implica fecha backup.
' -003- a. FJS 15.01.2010 - Se agrega la opci�n de solicitud para ejecutar en diferido (el fin de semana).
' -004- a. FJS 04.03.2010 - Nuevo SP para validar que no se este solicitando el mismo archivo de salida entre solicitudes en la misma tanda (antes de su proceso en CMAQ).
' -005- a. AA  25.11.2010 - Se agregan parametros de entrada para el stored GetSolicitud, referente a las modificaciones del requerimiento CGM-111111-1 hechas por S.A. (revisado/corregido)
' -006- a. FJS 02.07.2014 - Se agrega par�metro para solicitudes de EMErgencia.
' -007- a. FJS 22.08.2014 - Nuevo: se agregan 3 nuevos par�metros.

Option Explicit

Public Function Evaluar(ByVal fecha As Date, ByRef cMensaje As String) As Boolean
    Dim FechaInicio As Date         ' Principio de mes
    Dim FechaFinal As Date          ' �ltimo d�a del mes
    Dim Fe_Ini As Date              ' Principio de mes +/- d�as parametrizados
    Dim fe_fin As Date              ' �ltimo d�a del mes +/- d�as parametrizados
    Dim vFecha() As Date
    Dim bFeriados As Boolean
    
    Dim bHabilitado As Boolean
    Dim DiasInicio As Integer
    Dim DiasFinal As Integer
    Dim i As Integer, j As Integer
    
    Evaluar = False
    FechaInicio = CDate("01/" & Month(Now) & "/" & Year(Now))
    FechaFinal = DateAdd("d", -1, DateAdd("M", 1, FechaInicio))
    
    If sp_GetVarios("CLIST000") Then
        bHabilitado = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, True, False)
    End If
    
    If sp_GetVarios("CLIST001") Then
        DiasInicio = Val(ClearNull(aplRST.Fields!var_numero))
    End If
    
    If sp_GetVarios("CLIST002") Then
        DiasFinal = Val(ClearNull(aplRST.Fields!var_numero))
    End If

    If bHabilitado Then     ' Habilitado
        ' Cargo el vector con todas las fechas inh�biles para descontar
        bFeriados = False
        i = 0
        If sp_GetFeriado(FechaInicio, FechaFinal) Then
            Do While Not aplRST.EOF
                ReDim Preserve vFecha(i)
                bFeriados = True
                vFecha(i) = aplRST.Fields!fecha
                aplRST.MoveNext
                i = i + 1
                DoEvents
            Loop
        End If
        
        ' Ahora evaluo si hay que descontar d�as
        If DiasInicio > 0 Then
            Fe_Ini = FechaInicio
            'For i = 0 To Val(DiasInicio)
            For i = 1 To Val(DiasInicio)
                Fe_Ini = DateAdd("d", 1, Fe_Ini)
                If bFeriados Then
                    For j = 0 To UBound(vFecha)
                        If Fe_Ini = vFecha(j) Then
                            i = i - 1
                        End If
                    Next j
                End If
            Next i
        Else
            Fe_Ini = FechaInicio
        End If
        If DiasFinal > 0 Then
            fe_fin = FechaFinal
            i = 0
            Do While i < DiasFinal
                fe_fin = DateAdd("d", -1, fe_fin)
                If bFeriados Then
                    For j = 0 To UBound(vFecha)
                        If fe_fin = vFecha(j) Then
                            i = i - 1
                        End If
                    Next j
                End If
                i = i + 1
                DoEvents
            Loop
        Else
            fe_fin = FechaFinal
        End If
        
        ' Ac� se valida que las fechas desde y hasta comprendan la fecha de evaluaci�n
        If Format(fecha, "dd/MM/yyyy") >= Fe_Ini And Format(fecha, "dd/MM/yyyy") <= fe_fin Then
            Evaluar = True
            Exit Function
        Else
            cMensaje = "Podr�n generarse solicitudes los d�as h�biles" & vbCrLf & "a partir del " & Format(Fe_Ini, "dd/MM/yyyy") & " y hasta el " & Format(fe_fin, "dd/MM/yyyy")
        End If
    Else
        Evaluar = False
        cMensaje = "No se pueden generar solicitudes." & vbCrLf & vbCrLf & _
                    "Podr�n volver a generarse solicitudes a partir del d�a " & Format(Fe_Ini, "dd/MM/yyyy") & " hasta el " & Format(fe_fin, "dd/MM/yyyy")
    End If
End Function

'Public Function Evaluar(ByVal fecha As Date, ByRef cMensaje As String) As Boolean
'    Dim FechaInicio As Date         ' Principio de mes
'    Dim FechaFinal As Date          ' �ltimo d�a del mes
'    Dim Fe_Ini As Date              ' Principio de mes +/- d�as parametrizados
'    Dim fe_fin As Date              ' �ltimo d�a del mes +/- d�as parametrizados
'
'    Dim bHabilitado As Boolean
'    Dim DiasInicio As Integer
'    Dim DiasFinal As Integer
'    Dim i As Integer
'
'    Evaluar = False
'    FechaInicio = CDate("01/" & Month(Now) & "/" & Year(Now))
'    FechaFinal = DateAdd("d", -1, DateAdd("M", 1, FechaInicio))
'
'    If sp_GetVarios("CLIST000") Then
'        bHabilitado = IIf(Val(ClearNull(aplRST.Fields!var_numero)) > 0, True, False)
'    End If
'
'    If sp_GetVarios("CLIST001") Then
'        DiasInicio = Val(ClearNull(aplRST.Fields!var_numero))
'    End If
'
'    If sp_GetVarios("CLIST002") Then
'        DiasFinal = Val(ClearNull(aplRST.Fields!var_numero))
'    End If
'
'    If bHabilitado Then     ' Habilitado
'        If DiasInicio > 0 Then
'            Fe_Ini = FechaInicio
'            For i = 0 To Val(DiasInicio)
'                Fe_Ini = DateAdd("d", 1, Fe_Ini)
'                If sp_GetFeriado(Fe_Ini, Fe_Ini) Then
'                    i = i - 1
'                End If
'            Next i
'        End If
'        If DiasFinal > 0 Then
'            fe_fin = FechaFinal
'            For i = 0 To Val(DiasFinal)
'                fe_fin = DateAdd("d", -1, fe_fin)
'                If sp_GetFeriado(fe_fin, fe_fin) Then
'                    i = i - 1
'                End If
'            Next i
'        End If
'
'        If fecha >= Fe_Ini And fecha <= fe_fin Then
'            Evaluar = True
'            Exit Function
'        Else
'            cMensaje = "Podr�n generarse solicitudes los d�as h�biles" & vbCrLf & "a partir del " & Fe_Ini & " y hasta el " & fe_fin
'        End If
'    Else
'        Evaluar = False
'        cMensaje = "No se pueden generar solicitudes." & vbCrLf & vbCrLf & _
'                    "Podr�n volver a generarse solicitudes a partir del d�a " & Fe_Ini & " hasta el " & fe_fin
'    End If
'End Function

Function sp_InsertSolicitud(SOL_DIFERIDA, sol_tipo, sol_mask, sol_recurso, pet_nrointerno, jus_codigo, sol_texto, sol_fe_auto1, sol_fe_auto2, sol_fe_auto3, sol_file_prod, sol_file_desa, sol_file_out, sol_LIB_SYSIN, SOL_MEM_SYSIN, SOL_JOIN, SOL_NROTABLA, copy_biblio, copy_nombre, dsn_id, sol_eme, sol_file_inter, sol_bckuptabla, sol_bckupjob, Optional ByRef sol_nroasignado) As Boolean     ' upd -001- a.   ' upd -003- a.   ' upd -006- a.    ' upd -007- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertSolicitud"
        '.Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, cModo)    ' add -003- a.
        .Parameters.Append .CreateParameter("@sol_diferida", adChar, adParamInput, 1, SOL_DIFERIDA)
        .Parameters.Append .CreateParameter("@sol_tipo", adChar, adParamInput, 1, sol_tipo)
        .Parameters.Append .CreateParameter("@sol_mask", adChar, adParamInput, 1, sol_mask)
        .Parameters.Append .CreateParameter("@sol_recurso", adChar, adParamInput, 10, sol_recurso)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        '.Parameters.Append .CreateParameter("@sol_resp_ejec", adChar, adParamInput, 10, SOL_RESP_EJEC)
        '.Parameters.Append .CreateParameter("@sol_resp_sect", adChar, adParamInput, 10, SOL_RESP_SECT)
        '.Parameters.Append .CreateParameter("@sol_seguridad", adChar, adParamInput, 10, sol_seguridad)
        .Parameters.Append .CreateParameter("@jus_codigo", adInteger, adParamInput, , jus_codigo)
        .Parameters.Append .CreateParameter("@sol_texto", adVarChar, adParamInput, 255, sol_texto)
        .Parameters.Append .CreateParameter("@sol_fe_auto1", SQLDdateType, adParamInput, , IIf(Format(sol_fe_auto1, "dd/mm/yyyy hh:MM:ss") = "31/12/1899 00:00:00" Or Format(sol_fe_auto1, "dd/mm/yyyy") = "01/01/1900", Null, sol_fe_auto1))       ' upd -002- a.
        .Parameters.Append .CreateParameter("@sol_fe_auto2", SQLDdateType, adParamInput, , IIf(Format(sol_fe_auto2, "dd/mm/yyyy hh:MM:ss") = "31/12/1899 00:00:00" Or Format(sol_fe_auto2, "dd/mm/yyyy") = "01/01/1900", Null, sol_fe_auto2))       ' upd -002- a.
        .Parameters.Append .CreateParameter("@sol_fe_auto3", SQLDdateType, adParamInput, , IIf(Format(sol_fe_auto3, "dd/mm/yyyy hh:MM:ss") = "31/12/1899 00:00:00" Or Format(sol_fe_auto3, "dd/mm/yyyy") = "01/01/1900", Null, sol_fe_auto3))       ' upd -002- a.
        .Parameters.Append .CreateParameter("@sol_file_prod", adVarChar, adParamInput, 255, sol_file_prod)
        .Parameters.Append .CreateParameter("@sol_file_desa", adVarChar, adParamInput, 255, sol_file_desa)
        .Parameters.Append .CreateParameter("@sol_file_out", adVarChar, adParamInput, 255, sol_file_out)
        '{ add -007- a.
        .Parameters.Append .CreateParameter("@sol_file_inter", adChar, adParamInput, 50, sol_file_inter)
        .Parameters.Append .CreateParameter("@sol_bckuptabla", adChar, adParamInput, 50, sol_bckuptabla)
        .Parameters.Append .CreateParameter("@sol_bckupjob", adChar, adParamInput, 50, sol_bckupjob)
        '}
        .Parameters.Append .CreateParameter("@sol_lib_Sysin", adVarChar, adParamInput, 255, sol_LIB_SYSIN)
        .Parameters.Append .CreateParameter("@sol_mem_Sysin", adVarChar, adParamInput, 255, SOL_MEM_SYSIN)
        .Parameters.Append .CreateParameter("@sol_join", adChar, adParamInput, 1, SOL_JOIN)
        .Parameters.Append .CreateParameter("@sol_nrotabla", adChar, adParamInput, 4, Mid(CStr(SOL_NROTABLA), 1, 4))
        .Parameters.Append .CreateParameter("@copy_biblio", adChar, adParamInput, 50, Mid(copy_biblio, 1, 50))
        .Parameters.Append .CreateParameter("@copy_nombre", adChar, adParamInput, 50, Mid(copy_nombre, 1, 50))
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@sol_eme", adChar, adParamInput, 1, IIf(sol_eme, "S", "N"))    ' add -006- a.
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamOutput)                   ' add -001- a.
        Set aplRST = .Execute
        'sol_nroasignado = .Parameters(27).Value     ' add -001- a.  ' upd -003- a.2 Pasar al final a 24     ' upd -006- a. Antes �ndice en 23   ' upd -007- a. Antes 24
        sol_nroasignado = .Parameters(.Parameters.Count - 1).Value        ' add -001- a.  ' upd -003- a.2 Pasar al final a 24     ' upd -006- a. Antes �ndice en 23   ' upd -007- a. Antes 24
    End With
    sp_InsertSolicitud = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertSolicitud = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteSolicitudes(sol_nroasignado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteSolicitudes"
        .Parameters.Append .CreateParameter("sol_nroasignado", adInteger, adParamInput, , CLng(Val(sol_nroasignado)))
        Set aplRST = .Execute
    End With
    sp_DeleteSolicitudes = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteSolicitudes = False
End Function

Function sp_UpdateSolicitudAuto(sol_nroasignado, cod_recurso, cod_perfil, fecha) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateSolicitudAuto"
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamInput, , CLng(Val(sol_nroasignado)))
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
        .Parameters.Append .CreateParameter("@fecha", SQLDdateType, adParamInput, , fecha)
        Set aplRST = .Execute
    End With
    sp_UpdateSolicitudAuto = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateSolicitudAuto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSolicitud(cod_recurso, sol_tipo, sol_nroasignado, SOL_ESTADO, sol_mask, sol_fechaini, sol_fechafin) As Boolean ' upd -005- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetSolicitud"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@sol_tipo", adChar, adParamInput, 1, sol_tipo)
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamInput, , sol_nroasignado)
        .Parameters.Append .CreateParameter("@sol_estado", adChar, adParamInput, 1, IIf(SOL_ESTADO = "N", Null, SOL_ESTADO))
        '{ add -005- a.
        .Parameters.Append .CreateParameter("@sol_mask", adChar, adParamInput, 1, IIf(sol_mask = "T", Null, sol_mask))
        .Parameters.Append .CreateParameter("@sol_fechaini", SQLDdateType, adParamInput, , sol_fechaini)
        .Parameters.Append .CreateParameter("@sol_fechafin", SQLDdateType, adParamInput, , sol_fechafin)
        '}
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSolicitud = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSolicitud = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSolicitudListado(sModo, sol_nroasignado, cod_recurso, cod_perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetSolicitudListado"
        .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, sModo)
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamInput, , sol_nroasignado)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 4, cod_perfil)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSolicitudListado = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSolicitudListado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSolicitudNro(sol_nroasignado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetSolicitudNro"
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamInput, , sol_nroasignado)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSolicitudNro = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSolicitudNro = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'Function sp_GetSolicitudAuto(cod_recurso, cod_perfil, fe_desde, pendientes, propias) As Boolean
Function sp_GetSolicitudAuto(cod_recurso, cod_perfil, fe_desde, propias) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetSolicitudAuto"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        '.Parameters.Append .CreateParameter("@pendientes", adChar, adParamInput, 1, IIf(pendientes = 1, "S", "N"))
        '.Parameters.Append .CreateParameter("@propias", adChar, adParamInput, 1, IIf(propias = 1, "S", "N"))
        .Parameters.Append .CreateParameter("@propias", adChar, adParamInput, 1, propias)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSolicitudAuto = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSolicitudAuto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSolicitudRecurso(cod_recurso, sol_tipo, campo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetSolicitudRecurso"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@sol_tipo", adChar, adParamInput, 1, sol_tipo)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSolicitudRecurso = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSolicitudRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetJustificativos(jus_codigo, flag_habil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetJustificativos"
        .Parameters.Append .CreateParameter("@jus_codigo", adInteger, adParamInput, , jus_codigo)
        .Parameters.Append .CreateParameter("@flag_habil", adChar, adParamInput, 1, flag_habil)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetJustificativos = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetJustificativos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -004- a.
Function sp_GetSolicitudValida(cArchivoSalida) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
'    If InStr(1, "DESA|TEST|", glENTORNO, vbTextCompare) > 0 Then
'        sp_GetSolicitudValida = False       ' Para que no haga el control
'        Exit Function
'    End If
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetSolicitudValida"
        .Parameters.Append .CreateParameter("@archivo_salida", adChar, adParamInput, 255, cArchivoSalida)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSolicitudValida = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSolicitudValida = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
