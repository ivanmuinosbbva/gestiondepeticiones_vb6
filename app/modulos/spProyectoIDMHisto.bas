Attribute VB_Name = "spProyectoIDMHisto"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertProyectoIDMHisto(ProjId, ProjSubId, ProjSubSId, hst_fecha, cod_evento, proj_estado, CampoTexto, replc_user, audit_user) As Long
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim hst_nrointerno As Long
    Dim rsAux As ADODB.Recordset
    hst_nrointerno = 0
    
    Set rsAux = Nothing
    
    bFlg = False
    
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN.ConnectionString
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoIDMHisto"
        .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamOutput)
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        '.Parameters.Append .CreateParameter("@hst_fecha", SQLDdateType, adParamInput, , IIf(hst_fecha = "", Null, hst_fecha))
        .Parameters.Append .CreateParameter("@hst_fecha", adChar, adParamInput, 20, IIf(hst_fecha = "", Null, Format(hst_fecha, "mm/dd/yyyy hh:MM:ss")))
        .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 8, cod_evento)
        .Parameters.Append .CreateParameter("@proj_estado", adChar, adParamInput, 6, proj_estado)
        .Parameters.Append .CreateParameter("@replc_user", adChar, adParamInput, 10, replc_user)
        .Parameters.Append .CreateParameter("@audit_user", adChar, adParamInput, 10, audit_user)
        .Execute
        hst_nrointerno = .Parameters(0).Value
    End With
    
    If hst_nrointerno = 0 Then
        sp_InsertProyectoIDMHisto = 0
        Exit Function
    End If
    sp_InsertProyectoIDMHisto = hst_nrointerno
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN.ConnectionString
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsProyIDMHisMemo"
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput)
      .Parameters.Append .CreateParameter("@mem_campo", adVarChar, adParamInput, 10)
      .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput)
      .Parameters.Append .CreateParameter("@mem_texto", adVarChar, adParamInput, 255)
    End With
    
    If Len(Trim(CampoTexto)) > 0 Then
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(CampoTexto, 255)
        With objCommand
            .Parameters(0).Value = hst_nrointerno
            .Parameters(1).Value = "HISTO"
            .Parameters(2).Value = CInt(Secuencia)
            .Parameters(3).Value = TmpTexto & ""
            Set rsAux = .Execute
        End With
        Secuencia = Secuencia + 1
        CampoTexto = Mid(CampoTexto, 256)
        If Len(CampoTexto) = 0 Then
            bFlg = False
        End If
    Loop
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(rsAux, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_InsertProyectoIDMHisto = 0
End Function

'Function sp_InsertProyectoIDMHisto(ProjId, ProjSubId, ProjSubSId, hst_nrointerno, hst_fecha, cod_evento, proj_estado, replc_user, audit_user) As Boolean
'        On Error Resume Next
'        aplRST.Close
'        Set aplRST = Nothing
'        On Error GoTo Err_SP
'        Dim objCommand As ADODB.Command
'        Set objCommand = New ADODB.Command
'        With objCommand
'                .ActiveConnection = aplCONN
'                .CommandType = adCmdStoredProc
'                .CommandText = "sp_InsertProyectoIDMHisto"
'                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
'                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
'                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
'                .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
'                .Parameters.Append .CreateParameter("@hst_fecha", SQLDdateType, adParamInput, , hst_fecha)
'                .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 8, cod_evento)
'                .Parameters.Append .CreateParameter("@proj_estado", adChar, adParamInput, 6, proj_estado)
'                .Parameters.Append .CreateParameter("@replc_user", adChar, adParamInput, 10, replc_user)
'                .Parameters.Append .CreateParameter("@audit_user", adChar, adParamInput, 10, audit_user)
'                Set aplRST = .Execute
'        End With
'        sp_InsertProyectoIDMHisto = True
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'        On Error GoTo 0
'Exit Function
'Err_SP:
'        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'        sp_InsertProyectoIDMHisto = False
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'End Function

Function sp_UpdateProyectoIDMHisto(ProjId, ProjSubId, ProjSubSId, hst_nrointerno, hst_fecha, cod_evento, proj_estado, replc_user, audit_user) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateProyectoIDMHisto"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
                .Parameters.Append .CreateParameter("@hst_fecha", SQLDdateType, adParamInput, , hst_fecha)
                .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 8, cod_evento)
                .Parameters.Append .CreateParameter("@proj_estado", adChar, adParamInput, 6, proj_estado)
                .Parameters.Append .CreateParameter("@replc_user", adChar, adParamInput, 10, replc_user)
                .Parameters.Append .CreateParameter("@audit_user", adChar, adParamInput, 10, audit_user)
                Set aplRST = .Execute
        End With
        sp_UpdateProyectoIDMHisto = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateProyectoIDMHisto = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMHistoField(ProjId, ProjSubId, ProjSubSId, hst_nrointerno, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateProyectoIDMHistoField"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateProyectoIDMHistoField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateProyectoIDMHistoField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteProyectoIDMHisto(ProjId, ProjSubId, ProjSubSId, hst_nrointerno) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteProyectoIDMHisto"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
                Set aplRST = .Execute
        End With
        sp_DeleteProyectoIDMHisto = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteProyectoIDMHisto = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMHisto(ProjId, ProjSubId, ProjSubSId) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_GetProyectoIDMHisto"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                Set aplRST = .Execute
        End With
        If Not aplRST.EOF Then
                sp_GetProyectoIDMHisto = True
        End If
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_GetProyectoIDMHisto = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

