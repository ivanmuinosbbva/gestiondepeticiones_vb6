Attribute VB_Name = "spHEDT011"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertHEDT011(tipo_id, tipo_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertHEDT011"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, , tipo_id)
                .Parameters.Append .CreateParameter("@tipo_nom", adChar, adParamInput, , tipo_nom)
                Set aplRST = .Execute
        End With
        sp_InsertHEDT011 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertHEDT011 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteHEDT011(tipo_id, tipo_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteHEDT011"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, , tipo_id)
                .Parameters.Append .CreateParameter("@tipo_nom", adChar, adParamInput, , tipo_nom)
                Set aplRST = .Execute
        End With
        sp_DeleteHEDT011 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteHEDT011 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetHEDT011(tipo_id, tipo_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_GetHEDT011"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, 1, tipo_id)
                .Parameters.Append .CreateParameter("@tipo_nom", adChar, adParamInput, 50, tipo_nom)
                Set aplRST = .Execute
        End With
        sp_GetHEDT011 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_GetHEDT011 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function
