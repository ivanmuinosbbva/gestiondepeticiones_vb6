Attribute VB_Name = "spEstadisticas"
Option Explicit

Function sp_GetEstadisticasHoras(FechaDesde, FechaHasta, Recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetEstadisticasHoras"
      .Parameters.Append .CreateParameter("@fechadesde", SQLDdateType, adParamInput, , FechaDesde)
      .Parameters.Append .CreateParameter("@fechahasta", SQLDdateType, adParamInput, , FechaHasta)
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, Recurso)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetEstadisticasHoras = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetEstadisticasHoras = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
