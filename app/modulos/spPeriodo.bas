Attribute VB_Name = "spPeriodo"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertPeriodo(per_nrointerno, per_nombre, per_tipo, fe_desde, fe_hasta, per_estado, per_abrev) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeriodo"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
        .Parameters.Append .CreateParameter("@per_nombre", adChar, adParamInput, 50, per_nombre)
        .Parameters.Append .CreateParameter("@per_tipo", adChar, adParamInput, 1, per_tipo)
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        .Parameters.Append .CreateParameter("@per_estado", adChar, adParamInput, 6, per_estado)
        .Parameters.Append .CreateParameter("@per_abrev", adChar, adParamInput, 15, per_abrev)
        Set aplRST = .Execute
    End With
    sp_InsertPeriodo = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeriodo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeriodo(per_nrointerno, per_nombre, per_tipo, fe_desde, fe_hasta, per_estado, per_abrev) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeriodo"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
        .Parameters.Append .CreateParameter("@per_nombre", adChar, adParamInput, 50, per_nombre)
        .Parameters.Append .CreateParameter("@per_tipo", adChar, adParamInput, 1, per_tipo)
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        .Parameters.Append .CreateParameter("@per_estado", adChar, adParamInput, 6, per_estado)
        '.Parameters.Append .CreateParameter("@per_ultfchest", SQLDdateType, adParamInput, , per_ultfchest)
        '.Parameters.Append .CreateParameter("@per_ultusrid", adChar, adParamInput, 10, per_ultusrid)
        .Parameters.Append .CreateParameter("@per_abrev", adChar, adParamInput, 15, per_abrev)
        Set aplRST = .Execute
    End With
    sp_UpdatePeriodo = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdatePeriodo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeriodoField(per_nrointerno, campo, valortxt, valordate, valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeriodoField"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum), 0, CLng(Val("" & valorNum))))
        Set aplRST = .Execute
    End With
    sp_UpdatePeriodoField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdatePeriodoField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeriodo(per_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeriodo"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , per_nrointerno)
        Set aplRST = .Execute
    End With
    sp_DeletePeriodo = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeriodo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeriodo(per_nrointerno, flg_habil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeriodo"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(IsNull(per_nrointerno), Null, per_nrointerno))
        .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, IIf(IsNull(flg_habil), Null, flg_habil))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeriodo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeriodo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function


