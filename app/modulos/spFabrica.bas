Attribute VB_Name = "spFabrica"
' C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

' -001- a. FJS 24.06.2015 - Nuevo: se mejora el mantenimiento de las f�bricas de software.

Option Explicit

Function sp_InsertFabrica(cod_fab, nom_fab, cod_gerencia, cod_direccion, cod_sector, cod_grupo, estado_fab, tipo_fab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertFabrica"
        .Parameters.Append .CreateParameter("@cod_fab", adChar, adParamInput, 10, cod_fab)
        .Parameters.Append .CreateParameter("@nom_fab", adChar, adParamInput, 50, nom_fab)
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@estado_fab", adChar, adParamInput, 1, estado_fab)
        .Parameters.Append .CreateParameter("@tipo_fab", adInteger, adParamInput, , IIf(tipo_fab = Null, Null, tipo_fab))
        Set aplRST = .Execute
    End With
    sp_InsertFabrica = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertFabrica = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateFabrica(cod_fab, nom_fab, cod_gerencia, cod_direccion, cod_sector, cod_grupo, estado_fab, tipo_fab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateFabrica"
        .Parameters.Append .CreateParameter("@cod_fab", adChar, adParamInput, 10, cod_fab)
        .Parameters.Append .CreateParameter("@nom_fab", adChar, adParamInput, 50, nom_fab)
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@estado_fab", adChar, adParamInput, 1, estado_fab)
        .Parameters.Append .CreateParameter("@tipo_fab", adInteger, adParamInput, , IIf(tipo_fab = Null, Null, tipo_fab))
        Set aplRST = .Execute
    End With
    sp_UpdateFabrica = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateFabrica = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteFabrica(cod_fab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteFabrica"
        .Parameters.Append .CreateParameter("@cod_fab", adChar, adParamInput, 10, cod_fab)
        Set aplRST = .Execute
    End With
    sp_DeleteFabrica = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteFabrica = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetFabrica(cod_fab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetFabrica"
        .Parameters.Append .CreateParameter("@cod_fab", adChar, adParamInput, 10, IIf(IsNull(cod_fab) Or cod_fab = "", Null, cod_fab))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetFabrica = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetFabrica = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -001- a.
Function sp_GetTipoFabrica(TipoFabId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetTipoFabrica"
        .Parameters.Append .CreateParameter("@TipoFabId", adInteger, adParamInput, , IIf(TipoFabId = Null, Null, TipoFabId))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetTipoFabrica = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTipoFabrica = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
