Attribute VB_Name = "spPerfil"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertPerfil(cod_perfil, nom_perfil) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertPerfil"
                .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
                .Parameters.Append .CreateParameter("@nom_perfil", adChar, adParamInput, 30, nom_perfil)
                Set aplRST = .Execute
        End With
        sp_InsertPerfil = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertPerfil = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdatePerfil(cod_perfil, nom_perfil) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdatePerfil"
                .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
                .Parameters.Append .CreateParameter("@nom_perfil", adChar, adParamInput, 30, nom_perfil)
                Set aplRST = .Execute
        End With
        sp_UpdatePerfil = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdatePerfil = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeletePerfil(cod_perfil) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeletePerfil"
                .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
                Set aplRST = .Execute
        End With
        sp_DeletePerfil = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeletePerfil = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function
' se pone en otro modulo
'Function sp_GetPerfil(cod_perfil) As Boolean
'        On Error Resume Next
'        aplRST.Close
'        Set aplRST = Nothing
'        On Error GoTo Err_SP
'        Dim objCommand As ADODB.Command
'        Set objCommand = New ADODB.Command
'        With objCommand
'                .ActiveConnection = aplCONN
'                .CommandType = adCmdStoredProc
'                .CommandText = "sp_GetPerfil"
'                .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 30, cod_perfil)
'                Set aplRST = .Execute
'        End With
'        If Not aplRST.EOF Then
'                sp_GetPerfil = True
'        End If
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'        On Error GoTo 0
'Exit Function
'Err_SP:
'        MsgBox (AnalizarErrorSQL(aplRST, Err))
'        sp_GetPerfil = False
'        Set objCommand.ActiveConnection = Nothing
'        Set objCommand = Nothing
'End Function
