Attribute VB_Name = "spAccionesPerfilIDM"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertAccionesPerfilIDM(cod_accion, cod_perfil, cod_estado, cod_estnew, flg_hereda) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertAccionesPerfilIDM"
                .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
                .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
                .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
                .Parameters.Append .CreateParameter("@cod_estnew", adChar, adParamInput, 6, cod_estnew)
                .Parameters.Append .CreateParameter("@flg_hereda", adChar, adParamInput, 1, flg_hereda)
                Set aplRST = .Execute
        End With
        sp_InsertAccionesPerfilIDM = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_InsertAccionesPerfilIDM = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateAccionesPerfilIDM(cod_accion, cod_perfil, cod_estado, cod_estnew, flg_hereda) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateAccionesPerfilIDM"
                .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
                .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
                .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
                .Parameters.Append .CreateParameter("@cod_estnew", adChar, adParamInput, 6, cod_estnew)
                .Parameters.Append .CreateParameter("@flg_hereda", adChar, adParamInput, 1, flg_hereda)
                Set aplRST = .Execute
        End With
        sp_UpdateAccionesPerfilIDM = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateAccionesPerfilIDM = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteAccionesPerfilIDM(cod_accion, cod_perfil, cod_estado, cod_estnew) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteAccionesPerfilIDM"
                .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
                .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
                .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
                .Parameters.Append .CreateParameter("@cod_estnew", adChar, adParamInput, 6, cod_estnew)
                Set aplRST = .Execute
        End With
        sp_DeleteAccionesPerfilIDM = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteAccionesPerfilIDM = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetAccionesPerfilIDM(cod_accion, cod_perfil, cod_estado, cod_estnew) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetAccionesPerfilIDM"
        .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
        .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@cod_estnew", adChar, adParamInput, 6, cod_estnew)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetAccionesPerfilIDM = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAccionesPerfilIDM = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

