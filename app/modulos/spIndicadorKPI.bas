Attribute VB_Name = "spIndicadorKPI"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertIndicadorKPI(kpiid, kpinom, kpireq, kpihab, unimedId, kpiayuda) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertIndicadorKPI"
                .Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , kpiid)
                .Parameters.Append .CreateParameter("@kpinom", adChar, adParamInput, 60, kpinom)
                .Parameters.Append .CreateParameter("@kpireq", adChar, adParamInput, 1, kpireq)
                .Parameters.Append .CreateParameter("@kpihab", adChar, adParamInput, 1, kpihab)
                .Parameters.Append .CreateParameter("@unimedId", adChar, adParamInput, 10, unimedId)
                .Parameters.Append .CreateParameter("@kpiayuda", adChar, adParamInput, 255, kpiayuda)
                Set aplRST = .Execute
        End With
        sp_InsertIndicadorKPI = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_InsertIndicadorKPI = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateIndicadorKPI(kpiid, kpinom, kpireq, kpihab, unimedId, kpiayuda) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateIndicadorKPI"
                .Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , kpiid)
                .Parameters.Append .CreateParameter("@kpinom", adChar, adParamInput, 60, kpinom)
                .Parameters.Append .CreateParameter("@kpireq", adChar, adParamInput, 1, kpireq)
                .Parameters.Append .CreateParameter("@kpihab", adChar, adParamInput, 1, kpihab)
                .Parameters.Append .CreateParameter("@unimedId", adChar, adParamInput, 10, unimedId)
                .Parameters.Append .CreateParameter("@kpiayuda", adChar, adParamInput, 255, kpiayuda)
                Set aplRST = .Execute
        End With
        sp_UpdateIndicadorKPI = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateIndicadorKPI = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteIndicadorKPI(kpiid) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteIndicadorKPI"
                .Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , kpiid)
                Set aplRST = .Execute
        End With
        sp_DeleteIndicadorKPI = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteIndicadorKPI = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetIndicadorKPI(kpiid, empresa) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetIndicadorKPI"
      .Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , IIf(IsNull(kpiid), Null, kpiid))
      .Parameters.Append .CreateParameter("@empresa", adInteger, adParamInput, , IIf(IsNull(empresa), Null, empresa))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetIndicadorKPI = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetIndicadorKPI = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

