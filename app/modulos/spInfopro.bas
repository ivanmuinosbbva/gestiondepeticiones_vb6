Attribute VB_Name = "spInfopro"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertInfoproc(infoprot_id, infoprot_nom, infoprot_estado) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_InsertInfoproc"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@infoprot_nom", adChar, adParamInput, 50, infoprot_nom)
		.Parameters.Append .CreateParameter("@infoprot_estado", adChar, adParamInput, 1, infoprot_estado)
		Set aplRST = .Execute
	End With
	sp_InsertInfoproc = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_InsertInfoproc = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateInfoproc(infoprot_id, infoprot_nom, infoprot_estado) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateInfoproc"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@infoprot_nom", adChar, adParamInput, 50, infoprot_nom)
		.Parameters.Append .CreateParameter("@infoprot_estado", adChar, adParamInput, 1, infoprot_estado)
		Set aplRST = .Execute
	End With
	sp_UpdateInfoproc = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateInfoproc = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateInfoprocField(infoprot_id, campo, valortxt, valordate, valornum) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateInfoprocField"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
		.Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
		.Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
		.Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
		Set aplRST = .Execute
	End With
	sp_UpdateInfoprocField = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateInfoprocField = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_DeleteInfoproc(infoprot_id) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_DeleteInfoproc"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		Set aplRST = .Execute
	End With
	sp_DeleteInfoproc = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_DeleteInfoproc = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_GetInfoproc(infoprot_id) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_GetInfoproc"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		Set aplRST = .Execute
	End With
	If Not aplRST.EOF then
		sp_GetInfoproc = True
	End If
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_GetInfoproc = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_InsertInfoprod(infoprot_id, infoprot_item, infoprot_itemest, infoprot_gru, infoprot_orden) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_InsertInfoprod"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		.Parameters.Append .CreateParameter("@infoprot_itemest", adChar, adParamInput, 1, infoprot_itemest)
		.Parameters.Append .CreateParameter("@infoprot_gru", adInteger, adParamInput, , infoprot_gru)
		.Parameters.Append .CreateParameter("@infoprot_orden", adInteger, adParamInput, , infoprot_orden)
		Set aplRST = .Execute
	End With
	sp_InsertInfoprod = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_InsertInfoprod = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateInfoprod(infoprot_id, infoprot_item, infoprot_itemest, infoprot_gru, infoprot_orden) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateInfoprod"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		.Parameters.Append .CreateParameter("@infoprot_itemest", adChar, adParamInput, 1, infoprot_itemest)
		.Parameters.Append .CreateParameter("@infoprot_gru", adInteger, adParamInput, , infoprot_gru)
		.Parameters.Append .CreateParameter("@infoprot_orden", adInteger, adParamInput, , infoprot_orden)
		Set aplRST = .Execute
	End With
	sp_UpdateInfoprod = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateInfoprod = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateInfoprodField(infoprot_id, infoprot_item, campo, valortxt, valordate, valornum) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateInfoprodField"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		.Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
		.Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
		.Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
		.Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
		Set aplRST = .Execute
	End With
	sp_UpdateInfoprodField = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateInfoprodField = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_DeleteInfoprod(infoprot_id, infoprot_item) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_DeleteInfoprod"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		Set aplRST = .Execute
	End With
	sp_DeleteInfoprod = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_DeleteInfoprod = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_GetInfoprod(infoprot_id, infoprot_item) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_GetInfoprod"
		.Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		Set aplRST = .Execute
	End With
	If Not aplRST.EOF then
		sp_GetInfoprod = True
	End If
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_GetInfoprod = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_InsertInfoproi(infoprot_item, infoprot_itemdsc, infoprot_req, infoprot_itemest) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_InsertInfoproi"
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		.Parameters.Append .CreateParameter("@infoprot_itemdsc", adChar, adParamInput, 100, infoprot_itemdsc)
		.Parameters.Append .CreateParameter("@infoprot_req", adChar, adParamInput, 1, infoprot_req)
		.Parameters.Append .CreateParameter("@infoprot_itemest", adChar, adParamInput, 1, infoprot_itemest)
		Set aplRST = .Execute
	End With
	sp_InsertInfoproi = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_InsertInfoproi = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateInfoproi(infoprot_item, infoprot_itemdsc, infoprot_req, infoprot_itemest) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateInfoproi"
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		.Parameters.Append .CreateParameter("@infoprot_itemdsc", adChar, adParamInput, 100, infoprot_itemdsc)
		.Parameters.Append .CreateParameter("@infoprot_req", adChar, adParamInput, 1, infoprot_req)
		.Parameters.Append .CreateParameter("@infoprot_itemest", adChar, adParamInput, 1, infoprot_itemest)
		Set aplRST = .Execute
	End With
	sp_UpdateInfoproi = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateInfoproi = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateInfoproiField(infoprot_item, campo, valortxt, valordate, valornum) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateInfoproiField"
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		.Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
		.Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
		.Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
		.Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
		Set aplRST = .Execute
	End With
	sp_UpdateInfoproiField = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateInfoproiField = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_DeleteInfoproi(infoprot_item) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_DeleteInfoproi"
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		Set aplRST = .Execute
	End With
	sp_DeleteInfoproi = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_DeleteInfoproi = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_GetInfoproi(infoprot_item) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_GetInfoproi"
		.Parameters.Append .CreateParameter("@infoprot_item", adInteger, adParamInput, , infoprot_item)
		Set aplRST = .Execute
	End With
	If Not aplRST.EOF then
		sp_GetInfoproi = True
	End If
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_GetInfoproi = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function
