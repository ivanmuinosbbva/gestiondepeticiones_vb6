Attribute VB_Name = "spHEDT013"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertHEDT013(tipo_id, subtipo_id, item, item_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertHEDT013"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, , tipo_id)
                .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, , subtipo_id)
                .Parameters.Append .CreateParameter("@item", adInteger, adParamInput, , item)
                .Parameters.Append .CreateParameter("@item_nom", adChar, adParamInput, , item_nom)
                Set aplRST = .Execute
        End With
        sp_InsertHEDT013 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertHEDT013 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateHEDT013(tipo_id, subtipo_id, item, item_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateHEDT013"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, , tipo_id)
                .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, , subtipo_id)
                .Parameters.Append .CreateParameter("@item", adInteger, adParamInput, , item)
                .Parameters.Append .CreateParameter("@item_nom", adChar, adParamInput, , item_nom)
                Set aplRST = .Execute
        End With
        sp_UpdateHEDT013 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateHEDT013 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteHEDT013(tipo_id, subtipo_id, item, item_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteHEDT013"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, , tipo_id)
                .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, , subtipo_id)
                .Parameters.Append .CreateParameter("@item", adInteger, adParamInput, , item)
                .Parameters.Append .CreateParameter("@item_nom", adChar, adParamInput, , item_nom)
                Set aplRST = .Execute
        End With
        sp_DeleteHEDT013 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteHEDT013 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetHEDT013(tipo_id, subtipo_id, item) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_GetHEDT013"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, 1, tipo_id)
                .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, 1, subtipo_id)
                .Parameters.Append .CreateParameter("@item", adInteger, adParamInput, , item)
                Set aplRST = .Execute
        End With
        sp_GetHEDT013 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_GetHEDT013 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

