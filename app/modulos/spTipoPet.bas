Attribute VB_Name = "spTipoPet"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertTipoPet(cod_tipopet, nom_tipopet) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertTipoPet"
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, cod_tipopet)
        .Parameters.Append .CreateParameter("@nom_tipopet", adChar, adParamInput, 50, nom_tipopet)
        Set aplRST = .Execute
    End With
    sp_InsertTipoPet = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertTipoPet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateTipoPet(cod_tipopet, nom_tipopet) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateTipoPet"
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, cod_tipopet)
        .Parameters.Append .CreateParameter("@nom_tipopet", adChar, adParamInput, 50, nom_tipopet)
        Set aplRST = .Execute
    End With
    sp_UpdateTipoPet = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateTipoPet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteTipoPet(cod_tipopet) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteTipoPet"
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, cod_tipopet)
        Set aplRST = .Execute
    End With
    sp_DeleteTipoPet = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteTipoPet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetTipoPet(cod_tipopet) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetTipoPet"
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, cod_tipopet)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetTipoPet = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTipoPet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetTipoPerfil(modo, cod_direccion, cod_gerencia, cod_perfil, cod_tipopet) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetTipoPerfil"
        .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 4, modo)
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, IIf(IsNull(cod_direccion) Or cod_direccion = "", Null, cod_direccion))
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, IIf(IsNull(cod_gerencia) Or cod_gerencia = "", Null, cod_gerencia))
        .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, cod_tipopet)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetTipoPerfil = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTipoPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertTipoPetClase(cod_tipopet, cod_clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertTipoPetClase"
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, cod_tipopet)
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, cod_clase)
        Set aplRST = .Execute
    End With
    sp_InsertTipoPetClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertTipoPetClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteTipoPetClase(cod_tipopet, cod_clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteTipoPetClase"
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, cod_tipopet)
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, cod_clase)
        Set aplRST = .Execute
    End With
    sp_DeleteTipoPetClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteTipoPetClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetTipoPetClase(cod_tipopet, cod_clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetTipoPetClase"
        .Parameters.Append .CreateParameter("@cod_tipopet", adChar, adParamInput, 3, IIf(IsNull(cod_tipopet), Null, cod_tipopet))
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, IIf(IsNull(cod_clase), Null, cod_clase))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetTipoPetClase = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTipoPetClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
