Attribute VB_Name = "spDBMS"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertDBMS(dbmsId, dbmsNom) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_InsertDBMS"
		.Parameters.Append .CreateParameter("@dbmsId", adInteger, adParamInput, , dbmsId)
		.Parameters.Append .CreateParameter("@dbmsNom", adChar, adParamInput, 50, dbmsNom)
		Set aplRST = .Execute
	End With
	sp_InsertDBMS = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_InsertDBMS = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateDBMS(dbmsId, dbmsNom) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateDBMS"
		.Parameters.Append .CreateParameter("@dbmsId", adInteger, adParamInput, , dbmsId)
		.Parameters.Append .CreateParameter("@dbmsNom", adChar, adParamInput, 50, dbmsNom)
		Set aplRST = .Execute
	End With
	sp_UpdateDBMS = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateDBMS = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_DeleteDBMS(dbmsId) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_DeleteDBMS"
		.Parameters.Append .CreateParameter("@dbmsId", adInteger, adParamInput, , dbmsId)
		Set aplRST = .Execute
	End With
	sp_DeleteDBMS = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_DeleteDBMS = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_GetDBMS(dbmsId) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_GetDBMS"
		.Parameters.Append .CreateParameter("@dbmsId", adInteger, adParamInput, , dbmsId)
		Set aplRST = .Execute
	End With
	If Not aplRST.EOF then
		sp_GetDBMS = True
	End If
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_GetDBMS = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

