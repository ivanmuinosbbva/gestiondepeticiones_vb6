Attribute VB_Name = "spPeticionSector"
Option Explicit

Function sp_GetPeticionSector(pet_nrointerno, cod_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionSector"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionSector = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionSectorXt(pet_nrointerno, cod_sector, Optional cod_estado) As Boolean
    If IsMissing(cod_estado) Then
        cod_estado = ""
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionSectorXt"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, cod_estado)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionSectorXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionSectorXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionSector(pet_nrointerno, _
                    cod_sector, _
                    fe_ini_plan, _
                    fe_fin_plan, _
                    fe_ini_real, _
                    fe_fin_real, _
                    horaspresup, _
                    cod_estado, _
                    fe_estado, _
                    cod_situacion, _
                    hst_nrointerno_sol, _
                    hst_nrointerno_rsp) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionSector"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , fe_ini_plan)
        .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , fe_fin_plan)
        .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , fe_ini_real)
        .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , fe_fin_real)
        .Parameters.Append .CreateParameter("@horaspresup", adInteger, adParamInput, , horaspresup)
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@fe_estado", SQLDdateType, adParamInput, , fe_estado)
        .Parameters.Append .CreateParameter("@cod_situacion", adChar, adParamInput, 6, cod_situacion)
        .Parameters.Append .CreateParameter("@hst_nrointerno_sol", adInteger, adParamInput, , CLng(hst_nrointerno_sol))
        .Parameters.Append .CreateParameter("@hst_nrointerno_rsp", adInteger, adParamInput, , CLng(hst_nrointerno_rsp))
      Set aplRST = .Execute
    End With
    sp_UpdatePeticionSector = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionSector = False
End Function

Function sp_InsertPeticionSector(pet_nrointerno, _
                    cod_sector, _
                    fe_ini_plan, _
                    fe_fin_plan, _
                    fe_ini_real, _
                    fe_fin_real, _
                    horaspresup, _
                    cod_estado, _
                    fe_estado, _
                    cod_situacion, _
                    hst_nrointerno_sol, _
                    hst_nrointerno_rsp) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionSector"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , fe_ini_plan)
        .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , fe_fin_plan)
        .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , fe_ini_real)
        .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , fe_fin_real)
        .Parameters.Append .CreateParameter("@horaspresup", adInteger, adParamInput, , horaspresup)
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@fe_estado", SQLDdateType, adParamInput, , fe_estado)
        .Parameters.Append .CreateParameter("@cod_situacion", adChar, adParamInput, 6, cod_situacion)
        .Parameters.Append .CreateParameter("@hst_nrointerno_sol", adInteger, adParamInput, , CLng(hst_nrointerno_sol))
        .Parameters.Append .CreateParameter("@hst_nrointerno_rsp", adInteger, adParamInput, , CLng(hst_nrointerno_rsp))
      Set aplRST = .Execute
    End With
    sp_InsertPeticionSector = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
End Function

Function sp_DeletePeticionSector(pet_nrointerno, cod_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionSector"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      Set aplRST = .Execute
    End With
    sp_DeletePeticionSector = True
'    If Not IsEmpty(rsges) And (Not rsges.EOF) And rsges(0).Name = "ErrCode" Then
'        MsgBox (AnalizarErrorSQL(rsges, 30000))
'        sp_UpdateSectorGES = False
'    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionSector = False
End Function

Function sp_DeletePeticionSectorEstado(pet_nrointerno, cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionSectorEstado"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_estado", adVarChar, adParamInput, 50, cod_estado)
      Set aplRST = .Execute
    End With
    sp_DeletePeticionSectorEstado = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionSectorEstado = False
End Function


Function sp_UpdatePetSecField(pet_nrointerno, cod_sector, campo, valortxt, valordate, valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePetSecField"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
      .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
      .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum), 0, CLng(Val("" & valorNum))))
      Set aplRST = .Execute
    End With
    sp_UpdatePetSecField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePetSecField = False
End Function

Function sp_ChangePeticionSector(pet_nrointerno, _
                    old_sector, _
                    new_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_ChangePeticionSector"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@old_sector", adChar, adParamInput, 8, old_sector)
        .Parameters.Append .CreateParameter("@new_sector", adChar, adParamInput, 8, new_sector)
      Set aplRST = .Execute
    End With
    sp_ChangePeticionSector = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ChangePeticionSector = False
End Function

'{ add -001- a.
Public Function sp_GetNewEstPeticion(pet_nrointerno) As Boolean
    On Error Resume Next
    
    Dim cNew_Estado As String
    
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN.ConnectionString
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetNewEstPeticion"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@new_estado", adChar, adParamOutput, 6, Null)
        .Execute
        cNew_Estado = ClearNull(.Parameters(1).value)
    End With
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    Set objCommand = New ADODB.Command
    
    With objCommand
        .ActiveConnection = aplCONN.ConnectionString
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePetField"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, "ESTADO")
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, cNew_Estado)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , Null)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , Null)
        Set aplRST = .Execute
    End With

    sp_GetNewEstPeticion = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
