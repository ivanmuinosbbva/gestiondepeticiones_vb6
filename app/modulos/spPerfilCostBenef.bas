Attribute VB_Name = "spPerfilCostBenef"
Option Explicit

Function sp_GetPerfilCostBenef(cod_PerfilCostBenef, Optional flg_habil) As Boolean
    If IsMissing(flg_habil) Then
        flg_habil = Null
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPerfilCostBenef"
      .Parameters.Append .CreateParameter("@cod_PerfilCostBenef", adChar, adParamInput, 8, cod_PerfilCostBenef)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPerfilCostBenef = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPerfilCostBenef = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePerfilCostBenef(cod_PerfilCostBenef, nom_PerfilCostBenef, flg_habil) As Boolean
'Function sp_UpdatePerfilCostBenef(modo, cod_PerfilCostBenef, nom_PerfilCostBenef, flg_habil, es_ejecutor) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePerfilCostBenef"
      .Parameters.Append .CreateParameter("@cod_PerfilCostBenef", adChar, adParamInput, 8, cod_PerfilCostBenef)
      .Parameters.Append .CreateParameter("@nom_PerfilCostBenef", adChar, adParamInput, 30, nom_PerfilCostBenef)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      '.Parameters.Append .CreateParameter("@es_ejecutor", adChar, adParamInput, 1, es_ejecutor)
      Set aplRST = .Execute
    End With
    sp_UpdatePerfilCostBenef = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePerfilCostBenef = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePerfilCostBenef(cod_PerfilCostBenef) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeletePerfilCostBenef"
      .Parameters.Append .CreateParameter("@cod_PerfilCostBenef", adChar, adParamInput, 8, cod_PerfilCostBenef)
      Set aplRST = .Execute
    End With
    sp_DeletePerfilCostBenef = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePerfilCostBenef = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'
''Function sp_UpdatePerfilCostBenef(modo, cod_PerfilCostBenef, nom_PerfilCostBenef, flg_habil, es_ejecutor) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_UpdatePerfilCostBenef"
'      .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, modo)
'      .Parameters.Append .CreateParameter("@cod_PerfilCostBenef", adChar, adParamInput, 8, cod_PerfilCostBenef)
'      .Parameters.Append .CreateParameter("@nom_PerfilCostBenef", adChar, adParamInput, 30, nom_PerfilCostBenef)
'      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
'      '.Parameters.Append .CreateParameter("@es_ejecutor", adChar, adParamInput, 1, es_ejecutor)
'      Set aplRST = .Execute
'    End With
'    sp_UpdatePerfilCostBenef = True
'    If aplCONN.Errors.Count > 0 Then
'            GoTo Err_SP
'    End If
'    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
'        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
'            GoTo Err_SP
'        End If
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_UpdatePerfilCostBenef = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function

Function sp_InsertPerfilCostBenef(cod_PerfilCostBenef, nom_PerfilCostBenef, flg_habil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertPerfilCostBenef"
      .Parameters.Append .CreateParameter("@cod_PerfilCostBenef", adChar, adParamInput, 8, cod_PerfilCostBenef)
      .Parameters.Append .CreateParameter("@nom_PerfilCostBenef", adChar, adParamInput, 30, nom_PerfilCostBenef)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    sp_InsertPerfilCostBenef = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPerfilCostBenef = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

