Attribute VB_Name = "spPeticionPlandet"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

' -001- a. FJS 11.03.2011 - Modificación: se agrega un nuevo parámetro para reflejar la primera fecha de planificación cuando una petición nunca ha sido planificada.

Option Explicit

'Function sp_InsertPeticionPlandet(per_nrointerno, cod_grupo, pet_nrointerno, plan_orden) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_InsertPeticionPlandet"
'      .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'      .Parameters.Append .CreateParameter("@plan_orden", adInteger, adParamInput, , IIf(IsNull(plan_orden) Or plan_orden = "", Null, plan_orden))
'      Set aplRST = .Execute
'    End With
'    sp_InsertPeticionPlandet = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_InsertPeticionPlandet = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_UpdatePeticionPlandet(cModoTrabajo, per_nrointerno, pet_nrointerno, cod_grupo, plan_estadoini, plan_motnoplan, plan_fealcadef, plan_fefunctst, plan_horasper, plan_desaestado, plan_motivsusp, plan_motivrepl, plan_feplanori, plan_feplannue, plan_horasreplan, plan_fchestadoini) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        Select Case cModoTrabajo
'            Case "INICIAL"
'                .CommandText = "sp_UpdatePeticionPlandet1"
'                .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'                .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'                .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'                .Parameters.Append .CreateParameter("@plan_estadoini", adInteger, adParamInput, , IIf(plan_estadoini = "", Null, plan_estadoini))
'                .Parameters.Append .CreateParameter("@plan_motnoplan", adInteger, adParamInput, , IIf(plan_motnoplan = "", Null, plan_motnoplan))
'                .Parameters.Append .CreateParameter("@plan_fealcadef", SQLDdateType, adParamInput, , IIf(plan_fealcadef = "", Null, plan_fealcadef))
'                .Parameters.Append .CreateParameter("@plan_fefunctst", SQLDdateType, adParamInput, , IIf(plan_fefunctst = "", Null, plan_fefunctst))
'                .Parameters.Append .CreateParameter("@plan_horasper", adInteger, adParamInput, , IIf(plan_horasper = "", Null, plan_horasper))
'                .Parameters.Append .CreateParameter("@plan_fchestadoini", SQLDdateType, adParamInput, , IIf(plan_fchestadoini = "", Null, plan_fchestadoini))   ' add -001- a.
'            Case "SEGUIMIENTO"
'                .CommandText = "sp_UpdatePeticionPlandet2"
'                .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'                .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'                .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'                .Parameters.Append .CreateParameter("@plan_desaestado", adInteger, adParamInput, , IIf(plan_desaestado = "", Null, plan_desaestado))
'                .Parameters.Append .CreateParameter("@plan_motivsusp", adInteger, adParamInput, , IIf(plan_motivsusp = "", Null, plan_motivsusp))
'            Case "REPLANIFICACION"
'                .CommandText = "sp_UpdatePeticionPlandet3"
'                .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'                .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'                .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'                .Parameters.Append .CreateParameter("@plan_motivrepl", adInteger, adParamInput, , CLng(Val(plan_motivrepl)))
'                .Parameters.Append .CreateParameter("@plan_feplanori", SQLDdateType, adParamInput, , plan_feplanori)
'                .Parameters.Append .CreateParameter("@plan_feplannue", SQLDdateType, adParamInput, , plan_feplannue)
'                .Parameters.Append .CreateParameter("@plan_horasreplan", adInteger, adParamInput, , CLng(Val(plan_horasreplan)))
'        End Select
'        Set aplRST = .Execute
'    End With
'    sp_UpdatePeticionPlandet = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_UpdatePeticionPlandet = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_UpdatePeticionPlandetField(per_nrointerno, pet_nrointerno, cod_grupo, campo, valortxt, valordate, valorNum) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_UpdatePeticionPlandetField"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 80, campo)
'        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
'        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
'        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum) Or valorNum = "", Null, CLng(Val("" & valorNum))))
'        Set aplRST = .Execute
'    End With
'    sp_UpdatePeticionPlandetField = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_UpdatePeticionPlandetField = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_DeletePeticionPlandet(per_nrointerno, pet_nrointerno, cod_grupo) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_DeletePeticionPlandet"
'      .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'      Set aplRST = .Execute
'    End With
'    sp_DeletePeticionPlandet = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_DeletePeticionPlandet = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_GetPeticionPlandet(per_nrointerno, pet_nrointerno, cod_grupo) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPeticionPlandet"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(IsNull(per_nrointerno), Null, per_nrointerno))
'        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , IIf(IsNull(pet_nrointerno), Null, pet_nrointerno))
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPeticionPlandet = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPeticionPlandet = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
