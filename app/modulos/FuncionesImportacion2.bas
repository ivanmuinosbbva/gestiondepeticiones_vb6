Attribute VB_Name = "FuncionesImportacion2"
Option Explicit

Function sp_GetUnaPeticion2(pet_nrointerno, Optional altCONN) As Boolean
    On Error Resume Next
    aplRST.Close
    aplRST1.Close
    Set aplRST = Nothing
    Set aplRST1 = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        If IsMissing(altCONN) Then
            .ActiveConnection = aplCONN
        Else
            .ActiveConnection = altCONN
        End If
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetUnaPeticion"
        If IsNull(pet_nrointerno) Then
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , Null)
        Else
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        End If
        
        If IsMissing(altCONN) Then
            Set aplRST = .Execute
        Else
            Set aplRST1 = .Execute
        End If
    End With
    
    If IsMissing(altCONN) Then
        If Not (aplRST.EOF) Then
            sp_GetUnaPeticion2 = True
        End If
    Else
        If Not (aplRST1.EOF) Then
            sp_GetUnaPeticion2 = True
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    'MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    MsgBox (AnalizarErrorSQL(aplRST, Err, IIf(IsMissing(altCONN), aplCONN, altCONN)))
    sp_GetUnaPeticion2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionSector2(pet_nrointerno, cod_sector, Optional altCONN) As Boolean
    On Error Resume Next
    aplRST.Close
    aplRST1.Close
    Set aplRST = Nothing
    Set aplRST1 = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = IIf(IsMissing(altCONN), aplCONN, altCONN)
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionSector"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
    If IsMissing(altCONN) Then
        Set aplRST = .Execute
    Else
        Set aplRST1 = .Execute
    End If
    End With
    If IsMissing(altCONN) Then
        If Not (aplRST.EOF) Then
            sp_GetPeticionSector2 = True
        End If
    Else
        If Not (aplRST1.EOF) Then
            sp_GetPeticionSector2 = True
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    'MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    MsgBox (AnalizarErrorSQL(aplRST, Err, IIf(IsMissing(altCONN), aplCONN, altCONN)))
    sp_GetPeticionSector2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
