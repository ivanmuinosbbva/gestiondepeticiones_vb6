Attribute VB_Name = "FuncionesHOST"
' -001- a. FJS 20.12.2007 - Trazabilidad
' -002- a. FJS 07.08.2008 - Se agrega un nuevo SP para devolver peticiones que se hayan pasado
'                           al archivo que viaja al Changeman.
' -003- a. FJS 05.05.2009 - Nuevos SP para grabar la nueva tabla PeticionEnviadas2 (para enviar a HOST las peticiones que se encuentran en estado "En ejecuci�n")
' -004- a. FJS 22.02.2010 - Bug: se controla que para quedar como v�lida para pasaje a Producci�n (cadena ChangeMan) debe haber al menos un grupo DYD en estado activo.
' -005- a. FJS 24.02.2010 - Se agrega la actualizaci�n de la bandeja diaria desde el SP que genera primero el hist�rico.
' -006- a. FJS 21.04.2010 - Se agrega soporte para null en los datos de sector y grupo.

Option Explicit

'Public Function GesPetExportToHOST(ByVal PetNroInt As Variant, _
'                                   ByVal PetNroAsg As Variant, _
'                                   ByVal PETClass As Variant, _
'                                   ByVal PetSector As Variant, _
'                                   ByVal PetGrupo As Variant, _
'                                   ByVal PetUsr As Variant, _
'                                   ByVal PetTypeEnt As Variant) As Long

Public Function Habilitar_PasajeProduccion(ByVal PetNroInt As Variant, _
                                   ByVal PetNroAsg As Variant, _
                                   ByVal PETClass As Variant, _
                                   ByVal PetSector As Variant, _
                                   ByVal PetGrupo As Variant, _
                                   ByVal PetUsr As Variant, _
                                   ByVal PetTypeEnt As Variant) As Long
    On Error GoTo Errores
    
    Dim cPetNroAsg As String * 7
    Dim cPetClass As String * 8
    Dim cPetSector As String * 8
    Dim cPetGrupo As String * 8
    Dim cPetUsr As String * 8
    Dim cPetTypeEnt As String * 2
    
    cPetNroAsg = Trim(PetNroAsg)
    cPetClass = Trim(PETClass)
    cPetSector = IIf(IsNull(PetSector), "", Trim(PetSector))
    cPetGrupo = IIf(IsNull(PetGrupo), "", Trim(PetGrupo))
    cPetUsr = Trim(PetUsr)
    cPetTypeEnt = Trim(PetTypeEnt)
    
    Dim PetRecord As String
    Dim bContinue As Boolean    ' add -004- a.
    
    Habilitar_PasajeProduccion = 0      ' Inicializa el valor devuelto por la funci�n
    
    '{ add -004- a.
    bContinue = False
    If sp_GetPeticionGrupoXt(PetNroInt, Null, Null, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!cod_gerencia) = "DESA" Then      ' Si es un grupo de DYD, evalua si esta "activo"
                If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                    bContinue = True
                    Exit Do
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    
    If bContinue Then
    '}
        PetRecord = EncodeString(cPetNroAsg, cPetClass, cPetSector, cPetGrupo, cPetUsr, cPetTypeEnt)    ' 1. Genera a partir de los datos de entrada el string de 80 posiciones a transmitir
        If Not sp_InsertPeticionHost(PetNroInt, cPetSector, cPetGrupo, PetRecord, PetTypeEnt) Then      ' 2. SP que agrega el registro en PeticionEnviadas (Hist�rico)
            Habilitar_PasajeProduccion = 1          ' Error
        End If
        '{ del -005- a.
        '' 3. SP que agrega el registro en PeticionChangeMan (Diario)
        'If Not sp_InsertPeticionChangeMan(PetNroInt, PetSector, PetGrupo) Then
        '    GesPetExportToHOST = 1      ' Hubo un error...
        'End If
        '}
    End If      ' add -004- a.
Exit Function
Errores:
    Habilitar_PasajeProduccion = 1
    If Len(Err.source) > 0 Then
        MsgBox "[Or�gen del error: " & Trim(Err.source) & "] Error n� " & Trim(CStr(Err.Number)) & ": " & Trim(Err.DESCRIPTION), vbCritical + vbOKOnly, "Error al generar la exportaci�n de la petici�n para Changeman"
    Else
        MsgBox "[Or�gen del error: desconocido] " & "Error n� " & Trim(CStr(Err.Number)) & ": " & Trim(Err.DESCRIPTION), vbCritical + vbOKOnly, "Error al generar la exportaci�n de la petici�n para Changeman"
    End If
End Function

Private Function sp_InsertPeticionHost(PetNroInt, PetSector, PetGrupo, PetRecord, PetTypeEnt) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionHost"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        .Parameters.Append .CreateParameter("@pet_record", adChar, adParamInput, 80, PetRecord)
        Set aplRST = .Execute
    End With
    sp_InsertPeticionHost = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionHost = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -003- a.
Public Function sp_InsertPeticionEnviadas2(PetNroInt, _
                                           PetNroAsg, _
                                           PETClass, _
                                           PetSector, _
                                           PetGrupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    
    'PetRecord = EncodeString(PetNroAsg, PETClass, PetSector, PetGrupo, cstmGetUserName, "")
    
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionEnviadas2"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        '.Parameters.Append .CreateParameter("@pet_record", adChar, adParamInput, 80, PetRecord)
        Set aplRST = .Execute
    End With
    sp_InsertPeticionEnviadas2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionEnviadas2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_DeletePeticionEnviadas2(NroInterno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionEnviadas2"
        .Parameters.Append .CreateParameter("@NroInterno", adInteger, adParamInput, , NroInterno)
        Set aplRST = .Execute
    End With
    sp_DeletePeticionEnviadas2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionEnviadas2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Private Function sp_InsertPeticionChangeMan(PetNroInt, _
                                            PetSector, _
                                            PetGrupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPetChangeMan"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        Set aplRST = .Execute
    End With
    sp_InsertPeticionChangeMan = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionChangeMan = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -001- a.
Public Function sp_GetPeticionEnviadas(PetNroInt, _
                                        PetSector, _
                                        PetGrupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionEnviadas"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        Set aplRST = .Execute
    End With
    sp_GetPeticionEnviadas = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionEnviadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_GetPeticionEnviadas2(PetNroInt, _
                                        PetSector, _
                                        PetGrupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionEnviadas2"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        Set aplRST = .Execute
    End With
    sp_GetPeticionEnviadas2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionEnviadas2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_GetPeticionEnviadas2a(PetNroInt, _
                                         PetSector, _
                                         PetGrupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionEnviadas2a"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        Set aplRST = .Execute
    End With
    sp_GetPeticionEnviadas2a = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionEnviadas2a = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_GetPeticionChangeMan(PetNroInt, _
                                        PetSector, _
                                        PetGrupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionChangeMan"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        Set aplRST = .Execute
    End With
    sp_GetPeticionChangeMan = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionChangeMan = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_GetPeticionChangeMan2(Modo, PetNroInt, PetSector, PetGrupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionChangeMan2"
        .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, Modo)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , PetNroInt)
        .Parameters.Append .CreateParameter("@pet_sector", adChar, adParamInput, 8, PetSector)
        .Parameters.Append .CreateParameter("@pet_grupo", adChar, adParamInput, 8, PetGrupo)
        Set aplRST = .Execute
    End With
    sp_GetPeticionChangeMan2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionChangeMan2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_GetPeticionAprobadas(NroDesde, _
                                        NroHasta, _
                                        FechaDesde, _
                                        FechaHasta, _
                                        Estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionAprobadas"
        .Parameters.Append .CreateParameter("@NroDsd", adInteger, adParamInput, , NroDesde)
        .Parameters.Append .CreateParameter("@NroHst", adInteger, adParamInput, , NroHasta)
        .Parameters.Append .CreateParameter("@FchDsd", adChar, adParamInput, 8, FechaDesde)
        .Parameters.Append .CreateParameter("@FchHst", adChar, adParamInput, 8, FechaHasta)
        .Parameters.Append .CreateParameter("@Estado", adChar, adParamInput, 6, Estado)
        Set aplRST = .Execute
    End With
    sp_GetPeticionAprobadas = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionAprobadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_DeletePeticionEnviadas(NroInterno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionEnviadas"
        .Parameters.Append .CreateParameter("@NroInterno", adInteger, adParamInput, , NroInterno)
        Set aplRST = .Execute
    End With
    sp_DeletePeticionEnviadas = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionEnviadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_DeletePeticionEnviada(NroInterno, Sector, Grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionEnviada"
        .Parameters.Append .CreateParameter("@NroInterno", adInteger, adParamInput, , NroInterno)
        .Parameters.Append .CreateParameter("@Sector", adChar, adParamInput, 8, Sector)
        .Parameters.Append .CreateParameter("@Grupo", adChar, adParamInput, 8, Grupo)
        Set aplRST = .Execute
    End With
    sp_DeletePeticionEnviada = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionEnviada = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_DeletePeticionChangeMan(NroInterno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionChangeMan"
        .Parameters.Append .CreateParameter("@NroInterno", adInteger, adParamInput, , NroInterno)
        Set aplRST = .Execute
    End With
    sp_DeletePeticionChangeMan = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionChangeMan = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function sp_InsertPetChangeMan2(NroInterno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPetChangeMan2"
        .Parameters.Append .CreateParameter("@NroInterno", adInteger, adParamInput, , NroInterno)
        Set aplRST = .Execute
    End With
    sp_InsertPetChangeMan2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPetChangeMan2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Public Function sp_UpdatePeticionEnviadas(NroInterno, Cadena) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionEnviadas"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , NroInterno)
        .Parameters.Append .CreateParameter("@cadenaFormateada", adChar, adParamInput, 80, Cadena)
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionEnviadas = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionEnviadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function EncodeString(PetNroAsg, PETClass, PetSector, PetGrupo, PetUsr, PetTypeEnt As Variant) As String
    EncodeString = Space(3)
    EncodeString = EncodeString & Format(PetNroAsg, "0000000")
    EncodeString = EncodeString & Space(1)
    EncodeString = EncodeString & Format(UCase(PETClass), "!@")
    EncodeString = EncodeString & Format(UCase(PetSector), "!@")
    EncodeString = EncodeString & Format(UCase(PetGrupo), "!@")
    EncodeString = EncodeString & Format(UCase(PetUsr), "!@")
    EncodeString = EncodeString & Format(date, "YYYYMMDD")
    EncodeString = EncodeString & Format(Time, "HHMMSS")
    EncodeString = EncodeString & Space(21)
    EncodeString = EncodeString & Space(2)
    If Len(EncodeString) > 80 Or Len(EncodeString) > 80 Then
        MsgBox "ERROR"
    End If
End Function

'{ add -002- a.
Public Function sp_GetPeticionesValidas(NroDesde, NroHasta, Tipo, clase, titulo, prioridad, FechaDesde, FechaHasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionesValidas"
        .Parameters.Append .CreateParameter("@NroDesde", adInteger, adParamInput, , NroDesde)
        .Parameters.Append .CreateParameter("@NroHasta", adInteger, adParamInput, , NroHasta)
        .Parameters.Append .CreateParameter("@Tipo", adChar, adParamInput, 3, Tipo)
        .Parameters.Append .CreateParameter("@Clase", adChar, adParamInput, 4, clase)
        .Parameters.Append .CreateParameter("@Titulo", adChar, adParamInput, 50, titulo)
        .Parameters.Append .CreateParameter("@Prioridad", adChar, adParamInput, 1, prioridad)
        .Parameters.Append .CreateParameter("@FechaDesde", SQLDdateType, adParamInput, , FechaDesde)
        .Parameters.Append .CreateParameter("@FechaHasta", SQLDdateType, adParamInput, , FechaHasta)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionesValidas = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionesValidas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
