Attribute VB_Name = "FuncionesExcel"
' -001- a. FJS 16.06.2015 - Nuevo: se adecua el proceso para que soporte todas las versiones de Microsoft Office 2003 y posteriores.

Option Explicit

Global Const xlContinuous = 1
Global Const xlDot = -4118
Global Const xlDouble = -4119
Global Const xlLineStyleNone = -4142

Global Const xlHairline = 1
Global Const xlMedium = -4138
Global Const xlThick = 4
Global Const xlThin = 2

Global Const xlDiagonalDown = 5
Global Const xlDiagonalUp = 6
Global Const xlEdgeBottom = 9
Global Const xlEdgeLeft = 7
Global Const xlEdgeRight = 10
Global Const xlEdgeTop = 8
Global Const xlInsideHorizontal = 12
Global Const xlInsideVertical = 11

Private Const DIALOGO_ABRIR As String = "Nombre de planilla Microsoft Excel"
Private Const DIALOGO_GUARDAR As String = "Exportar consulta a Microsoft Excel"

Public Function Dialogo_EstablecerUbicacion(Optional Titulo As String, _
                        Optional Path_Inicial As Variant) As String
On Local Error GoTo errFunction
    Dim objShell As Object
    Dim objFolder As Object
    Dim o_Carpeta As Object
      
    ' Nuevo objeto Shell.Application
    Set objShell = CreateObject("Shell.Application")
      
    On Error Resume Next
    'Abre el cuadro de diálogo para seleccionar
    Set objFolder = objShell.BrowseForFolder(0, Titulo, 0, Path_Inicial)       ' 17
    'Set objFolder = objShell.BrowseForFolder(0, Titulo, 0, "")
    ' Devuelve solo el nombre de carpeta
    Set o_Carpeta = objFolder.Self
      
    ' Devuelve la ruta completa seleccionada en el diálogo
    Dialogo_EstablecerUbicacion = o_Carpeta.Path
Exit Function
'Error
errFunction:
    MsgBox Err.DESCRIPTION, vbCritical
    Dialogo_EstablecerUbicacion = vbNullString
End Function

Public Function Dialogo_GuardarExportacion(Accion As String, Optional NombrePorDefecto As String) As String
    On Error GoTo Errores
    Dim sFileName As String
    
    With mdiPrincipal.CommonDialog
        .FileName = NombrePorDefecto
        '.DefaultExt = ""
        .CancelError = True
        .DialogTitle = "Guardar como Microsoft Excel..."
        .Filter = "Microsoft Excel (*.xlsx; *.xls)|*.xlsx;*.xls"
        .FilterIndex = IIf(glMSOfficeExcelDefaultExtension = "xlsx", 1, 2)
        '.DefaultExt = IIf(glMSOfficeExcelDefaultExtension <> "", glMSOfficeExcelDefaultExtension, "xls")
        .InitDir = glWRKDIR
        '.InitDir = Environ$("HOMESHARE")
        .Flags = cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames + cdlOFNOverwritePrompt
        Select Case Accion
            Case "ABRIR": .ShowOpen
            Case "GUARDAR": .ShowSave
        End Select
        sFileName = .FileName
        
        ' Extensión del archivo
        If glMSOfficeExcelDefaultExtension <> "" Then
            If InStr(1, sFileName, ".xlsx", vbTextCompare) > 0 Then
                If "xlsx" <> glMSOfficeExcelDefaultExtension Then
                    'Debug.Print "1. Cambia xlsx por xls"
                    sFileName = Replace(sFileName, "xlsx", glMSOfficeExcelDefaultExtension)
                End If
            ElseIf InStr(1, sFileName, ".xls", vbTextCompare) > 0 Then
                If "xls" <> glMSOfficeExcelDefaultExtension Then
                    'Debug.Print "2. Cambia xls por xlsx"
                    sFileName = Replace(sFileName, "xls", glMSOfficeExcelDefaultExtension)
                End If
            Else
                'Debug.Print "3. Agrega el default para el archivo"
                sFileName = sFileName & "." & glMSOfficeExcelDefaultExtension
            End If
        End If
        Dialogo_GuardarExportacion = sFileName
    End With
Fin:
    Call Puntero(False)
    Exit Function
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario canceló sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandió el buffer automáticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
    End Select
End Function

Public Function CrearObjetoExcel(ByRef ExcelApp As Object, _
                                ByRef ExcelWrk As Object, _
                                ByRef xlSheet As Object, _
                                ByRef sFileName As String) As Boolean
    On Error Resume Next
    Dim sMensaje As String
    'Dim xlsDefaultExtension As String
    'Dim TempVer As String
    
    If Not glMSOfficeExcelIsEnabled Then
        sMensaje = "Error al intentar generar la exportación." & vbCrLf & "El producto Microsoft Excel no está instalado en el equipo."
        MsgBox sMensaje, vbCritical + vbOKOnly, "Generando exportación"
        GoTo Fin
    End If
    
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.visible = False
    
    Set ExcelWrk = ExcelApp.Workbooks.Add
    If ExcelWrk Is Nothing Then
        sMensaje = "Error al intentar generar la exportación." & vbCrLf
        If Not glMSOfficeExcelIsEnabled Then
            sMensaje = sMensaje & "El producto Microsoft Excel no está instalado en el equipo."
        Else
            sMensaje = sMensaje & "Existe un problema con el producto Microsoft Excel."
        End If
        'MsgBox "Error al generar planilla.", vbCritical + vbOKOnly, "Generando exportación"
        MsgBox sMensaje, vbCritical + vbOKOnly, "Generando exportación"
        GoTo Fin
    End If
    'ExcelWrk.SaveAs FileName:=sFileName, FileFormat:="xlWorkbookDefault"       ' upd 08.11.2013
   
    Set xlSheet = ExcelWrk.Sheets(1)
    ExcelApp.ActiveWindow.Zoom = 75
    xlSheet.Cells.VerticalAlignment = -4160
    xlSheet.Rows(1).Font.Bold = True
 
    With xlSheet.Rows(1)
        .HorizontalAlignment = 4131
        .WrapText = True
        .Orientation = 0
        .IndentLevel = 0
        .ShrinkToFit = False
        .MergeCells = False
        .RowHeight = 30
    End With
    CrearObjetoExcel = True
    Exit Function
Fin:
    CrearObjetoExcel = False
    Set xlSheet = Nothing
    Set ExcelWrk = Nothing
    Set ExcelApp = Nothing
    Call Status("Listo.")
    Call Puntero(False)
    'Call CerrarObjectoExcel(ExcelApp, ExcelWrk, xlSheet, sFileName)
End Function

Public Sub CerrarObjectoExcel(ByRef ExcelApp As Object, _
                              ByRef ExcelWrk As Object, _
                              ByRef ExcelSht As Object, _
                              ByVal sNombreArchivo As String)
    'ExcelWrk.Save
    ExcelWrk.Close True, sNombreArchivo
    
    'ExcelWrk.Close True
    ExcelApp.Application.Quit
    
    'Call ReleaseObjects(ExcelApp, ExcelWrk, ExcelSht)
    Set ExcelSht = Nothing
    Set ExcelWrk = Nothing
    Set ExcelApp = Nothing
    'MsgBox "Se ha generado exitosamente el archivo: " & Trim(sNombreArchivo) & vbCrLf & "Proceso de exportación finalizado.", vbInformation + vbOKOnly, "Exportación finalizada"
    Call Puntero(False)
    'Call Status("Listo.")
End Sub

Public Sub setMSOfficeVersion()
    On Error GoTo Errores
    Dim ExcelApp As Object
    
    Set ExcelApp = CreateObject("Excel.Application")
    
    'getMSOfficeVersion = ExcelApp.Version
    glMSOfficeExcelVersion = ExcelApp.Version
    glMSOfficeExcelDefaultExtension = ""
    glMSOfficeExcelIsEnabled = True
    
    Select Case glMSOfficeExcelVersion
        Case "11.0"         ' Office 2003
            glMSOfficeExcelDefaultExtension = "xls"
        Case Else           ' Posteriores: 2007, 2010, etc.
            glMSOfficeExcelDefaultExtension = "xlsx"
    End Select
    Set ExcelApp = Nothing
Exit Sub
Errores:
    'getMSOfficeVersion = "-No instalado-"
    glMSOfficeExcelVersion = "-No instalado-"
    glMSOfficeExcelDefaultExtension = ""
    glMSOfficeExcelIsEnabled = False
    Select Case Err.Number
        Case 429        ' El componente ActiveX no puede crear el objeto (Microsoft Office Excel)
            Exit Sub
        Case Else
            MsgBox Err.Number & ": " & Err.DESCRIPTION & vbCrLf & "(" & Err.Source & ")", vbCritical + vbOKOnly
    End Select
End Sub

'Private Sub ReleaseObjects(o_Excel As Object, o_Libro As Object, o_Hoja As Object)
'    If Not o_Excel Is Nothing Then Set o_Excel = Nothing
'    If Not o_Libro Is Nothing Then Set o_Libro = Nothing
'    If Not o_Hoja Is Nothing Then Set o_Hoja = Nothing
'End Sub


'Public Function Dialogo_GuardarExportacion(Optional NombrePorDefecto As String) As String
'    On Error GoTo Errores
'    With mdiPrincipal.CommonDialog
'        .CancelError = True
'        .DialogTitle = "Exportar consulta a Microsoft Excel"
'        .Filter = "Microsoft Excel (*.xls)|*.xls*"
'        .FilterIndex = 1
'        .InitDir = glWRKDIR
'        .Flags = cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
'        If IsMissing(NombrePorDefecto) Then
'            .FileName = ""
'        Else
'            .FileName = NombrePorDefecto
'        End If
'        .ShowSave
'        Dialogo_GuardarExportacion = .FileName
'    End With
'Fin:
'    Exit Function
'Errores:
'    Select Case Err.Number
'        Case 32755      ' El usuario canceló sin seleccionar archivo(s)
'            GoTo Fin
'        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
'            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
'            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
'                   "El sistema expandió el buffer automáticamente para poder continuar." & vbCrLf & _
'                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
'            Resume
'    End Select
'End Function

