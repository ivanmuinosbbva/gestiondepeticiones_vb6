Attribute VB_Name = "csmDDBBFnc"
'******************************************************************************************************************************************
' Autor:        Fernando J. Spitz
' Fecha:        10.11.2009
' Finalidad:    Inicializa y ejecuta la funci�n DB_ResultSet que efectua la llamada a un Stored Procedure, instancia e
'               inicializa los par�metros del mismo y devuelve un objeto Recordset
' Entradas:     sSQLStoreProcedureName: nombre del SP a ejecutar
'               vArray:                 arreglo con los par�metros que debe instanciar el SP
' Resultados:   Un objecto Recordset con el resultado de ejecuci�n del SP
' Componentes:  csmDDBBFnc.DynSP_Initialize
'               csmDDBBFnc.DynSP_DefParms
'               csmDDBBFnc.DynSP_ExecDynSP
'******************************************************************************************************************************************
Option Explicit

Global glParameterCounter As Long       ' Usado como contador en la rutina DefinirParametros para llevar la cuenta de la cantidad de par�metros creados
Global glvArray() As Variant            ' Arreglo global para la creaci�n de par�metros din�micos
Global gloRcdSet As ADODB.Recordset     ' Recordset p�blico de exclusivo uso de las funciones de este m�dulo (deber�a ser as�).

Public Sub DynSP_Initialize()
    ' Este procedimiento inicializa en cero el valor de la variable global glParameterCounter,
    ' utilizada para llevar la cuenta de la cantidad de par�metros creados de manera din�mica y
    ' borra el vector global de los par�metros din�micos.
    glParameterCounter = 0
    Erase glvArray
End Sub

Public Sub DynSP_DefParms(ByRef vMatrixParms As Variant, _
                          ByVal sParameterName As String, _
                          ByVal nParameterDataType As DataTypeEnum, _
                          ByVal nParameterDirection As ParameterDirectionEnum, _
                          ByVal nParameterSize As Long, _
                          ByVal vParameterValue As Variant)
    ' Este procedimiento se utiliza para crear de manera din�mica en una variable de tipo
    ' Variant (vMatrixParms pasada por referencia) N par�metros a utilizar por la funci�n
    ' DB_ResultSet que basada en este Variant realizar� la creaci�n de los par�metros del
    ' SP. Cada vez que invoque DefinirParametros desde otro m�dulo deber� llamar al
    ' procedimiento InicializarParametros para inicializar el contador (glParameterCounter).

    ReDim Preserve vMatrixParms(glParameterCounter)
    vMatrixParms(glParameterCounter) = Array(sParameterName, _
                                             nParameterDataType, _
                                             nParameterDirection, _
                                             nParameterSize, _
                                             vParameterValue)
    glParameterCounter = glParameterCounter + 1
End Sub

Public Function DynSP_ExecDynSP(ByVal sSQLStoreProcedureName As String, _
                                Optional ByVal vArray As Variant) As ADODB.Recordset
    On Error GoTo Err
    Dim rsResultSet As ADODB.Recordset
    Dim objCommand As ADODB.Command
    Dim i As Long
    
    Set objCommand = New ADODB.Command
    Set rsResultSet = New ADODB.Recordset
    
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = sSQLStoreProcedureName
        ' Aqu� se crean los par�metros de manera din�mica
        If Not IsMissing(vArray) Then
            For i = 0 To UBound(vArray)
                .Parameters.Append .CreateParameter(vArray(i)(0), vArray(i)(1), vArray(i)(2), vArray(i)(3), vArray(i)(4))
            Next i
        End If
        Set rsResultSet = .Execute
    End With
    If Not (rsResultSet.EOF) Then
        Set DynSP_ExecDynSP = rsResultSet.DataSource
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
Exit Function

Err:
    'MsgBox (AnalizarErrorSQL(rsResultSet, Err, aplCONN))
    'DynSP_ExecDynSP = False
    'Set objCommand.ActiveConnection = Nothing
    'Set objCommand = Nothing
End Function
