Attribute VB_Name = "spSector"
' -001- a. FJS 20.04.2015 - Nuevo: se agrega un nuevo atributo para sectores, que indica si el mismo es v�lido para usar como sector solicitante en peticiones especiales.
' -002- a. FJS 29.01.2016 - Nuevo: se agrega funcionalidad para la nueva gerencia de BPE.

Option Explicit

Function sp_GetSector(cod_sector, cod_gerencia, Optional flg_habil) As Boolean
    If IsMissing(flg_habil) Then
        flg_habil = Null
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetSector"
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSector = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSectorXt(cod_sector, cod_gerencia, cod_direccion, Optional flg_habil) As Boolean
    If IsMissing(flg_habil) Then
        flg_habil = Null
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetSectorXt"
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSectorXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSectorXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSectorAbrev(cod_abrev) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetSectorAbrev"
      .Parameters.Append .CreateParameter("@cod_abrev", adChar, adParamInput, 3, cod_abrev)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSectorAbrev = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSectorAbrev = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateSector(modo, cod_sector, nom_sector, cod_gerencia, cod_abrev, flg_habil, es_ejecutor, cod_bpar, cod_perfil, habil_soli) As Boolean      ' upd -001- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateSector"
      .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, modo)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@nom_sector", adChar, adParamInput, 80, nom_sector)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_abrev", adChar, adParamInput, 3, cod_abrev)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      .Parameters.Append .CreateParameter("@es_ejecutor", adChar, adParamInput, 1, es_ejecutor)
      .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
      .Parameters.Append .CreateParameter("@habil_soli", adChar, adParamInput, 1, habil_soli)       ' add -001- a.
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)       ' add -002- a.
      Set aplRST = .Execute
    End With
    sp_UpdateSector = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteSector(cod_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteSector"
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      Set aplRST = .Execute
    End With
    sp_DeleteSector = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_ChangeSectorGerencia(cod_sector, new_gerencia) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_ChangeSectorGerencia"
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@new_gerencia", adChar, adParamInput, 8, new_gerencia)
      Set aplRST = objCommand.Execute
    End With
    sp_ChangeSectorGerencia = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ChangeSectorGerencia = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetGestionDemanda() As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetGestionDemanda"
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetGestionDemanda = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetGestionDemanda = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
