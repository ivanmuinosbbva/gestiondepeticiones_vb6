Attribute VB_Name = "spTips"
'GMT01 - SE AGREGA FILTRO POR MENSAJE HABILITADO

Option Explicit

'Public Function sp_GetTips(tips_version, tips_tipo, tips_nro, tips_rng) As Boolean   ->GMT01
Public Function sp_GetTips(tips_version, tips_tipo, tips_nro, tips_rng, tips_hab) As Boolean  '->GMT01
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetTips"
        .Parameters.Append .CreateParameter("@tips_version", adChar, adParamInput, 10, tips_version)
        .Parameters.Append .CreateParameter("@tips_tipo", adChar, adParamInput, 6, tips_tipo)
        .Parameters.Append .CreateParameter("@tips_nro", adInteger, adParamInput, , tips_nro)
        .Parameters.Append .CreateParameter("@tips_rng", adInteger, adParamInput, , tips_rng)
        .Parameters.Append .CreateParameter("@tips_hab", adChar, adParamInput, 1, tips_hab) 'GMT01
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetTips = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTips = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'GMT01 -INI
Function sp_UpdateTips(tips_version, tips_tipo, tips_nro, tips_rng, tips_txt, tips_hab, tips_date) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateTips"
        .Parameters.Append .CreateParameter("@tips_version", adChar, adParamInput, 10, tips_version)
        .Parameters.Append .CreateParameter("@tips_tipo", adChar, adParamInput, 6, tips_tipo)
        .Parameters.Append .CreateParameter("@tips_nro", adInteger, adParamInput, , tips_nro)
        .Parameters.Append .CreateParameter("@tips_rng", adInteger, adParamInput, , tips_rng)
        .Parameters.Append .CreateParameter("@tips_txt", adChar, adParamInput, 500, tips_txt)
        .Parameters.Append .CreateParameter("@tips_date", adDBTimeStamp, adParamInput, , tips_date)
        .Parameters.Append .CreateParameter("@tips_hab", adChar, adParamInput, 1, tips_hab)
        Set aplRST = .Execute
    End With
    sp_UpdateTips = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateTips = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteTips(tips_version, tips_tipo, tips_nro, tips_rng) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteTips"
        .Parameters.Append .CreateParameter("@tips_version", adChar, adParamInput, 10, tips_version)
        .Parameters.Append .CreateParameter("@tips_tipo", adChar, adParamInput, 6, tips_tipo)
        .Parameters.Append .CreateParameter("@tips_nro", adInteger, adParamInput, , tips_nro)
        .Parameters.Append .CreateParameter("@tips_rng", adInteger, adParamInput, , tips_rng)
         Set aplRST = .Execute
    End With
    sp_DeleteTips = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteTips = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function


Function sp_InsertTips(tips_version, tips_tipo, tips_nro, tips_rng, tips_txt, tips_hab, tips_date) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertTips"
        .Parameters.Append .CreateParameter("@tips_version", adChar, adParamInput, 10, tips_version)
        .Parameters.Append .CreateParameter("@tips_tipo", adChar, adParamInput, 6, tips_tipo)
        .Parameters.Append .CreateParameter("@tips_nro", adInteger, adParamInput, , tips_nro)
        .Parameters.Append .CreateParameter("@tips_rng", adInteger, adParamInput, , tips_rng)
        .Parameters.Append .CreateParameter("@tips_txt", adChar, adParamInput, 500, tips_txt)
        .Parameters.Append .CreateParameter("@tips_date", adDBTimeStamp, adParamInput, , tips_date)
        .Parameters.Append .CreateParameter("@tips_hab", adChar, adParamInput, 1, tips_hab)
        Set aplRST = .Execute
    End With
    sp_InsertTips = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertTips = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'GMT01 - FIN
