Attribute VB_Name = "spExportPeticion"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertExport_Peticion(exp_id, exp_grupo, exp_desc, exp_codigo, exp_orden, exp_seleccion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertExport_Peticion"
        .Parameters.Append .CreateParameter("@exp_id", adInteger, adParamInput, , exp_id)
        .Parameters.Append .CreateParameter("@exp_grupo", adChar, adParamInput, 30, exp_grupo)
        .Parameters.Append .CreateParameter("@exp_desc", adChar, adParamInput, 100, exp_desc)
        .Parameters.Append .CreateParameter("@exp_codigo", adChar, adParamInput, 30, exp_codigo)
        .Parameters.Append .CreateParameter("@exp_orden", adInteger, adParamInput, , exp_orden)
        .Parameters.Append .CreateParameter("@exp_seleccion", adChar, adParamInput, 1, exp_seleccion)
        Set aplRST = .Execute
    End With
    sp_InsertExport_Peticion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertExport_Peticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateExport_Peticion(exp_id, exp_grupo, exp_desc, exp_codigo, exp_orden, exp_seleccion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateExport_Peticion"
        .Parameters.Append .CreateParameter("@exp_id", adInteger, adParamInput, , exp_id)
        .Parameters.Append .CreateParameter("@exp_grupo", adChar, adParamInput, 30, exp_grupo)
        .Parameters.Append .CreateParameter("@exp_desc", adChar, adParamInput, 100, exp_desc)
        .Parameters.Append .CreateParameter("@exp_codigo", adChar, adParamInput, 30, exp_codigo)
        .Parameters.Append .CreateParameter("@exp_orden", adInteger, adParamInput, , exp_orden)
        .Parameters.Append .CreateParameter("@exp_seleccion", adChar, adParamInput, 1, exp_seleccion)
        Set aplRST = .Execute
    End With
    sp_UpdateExport_Peticion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateExport_Peticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteExport_Peticion(exp_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteExport_Peticion"
        .Parameters.Append .CreateParameter("@exp_id", adInteger, adParamInput, , exp_id)
        Set aplRST = .Execute
    End With
    sp_DeleteExport_Peticion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteExport_Peticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetExport_Peticion(exp_id, exp_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetExport_Peticion"
        .Parameters.Append .CreateParameter("@exp_id", adInteger, adParamInput, , exp_id)
        .Parameters.Append .CreateParameter("@exp_grupo", adChar, adParamInput, 30, exp_grupo)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetExport_Peticion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetExport_Peticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateExport_PeticionField(x, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateExport_PeticionField"
        .Parameters.Append .CreateParameter("@x", adInteger, adParamInput, , CLng(Val(x)))
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdateExport_PeticionField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateExport_PeticionField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
