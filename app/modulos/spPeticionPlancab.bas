Attribute VB_Name = "spPeticionPlancab"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertPeticionPlancab(per_nrointerno, pet_nrointerno, prioridad, cod_bpar) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertPeticionPlancab"
      .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_InsertPeticionPlancab = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionPlancab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertPeticionPlancab2(per_nrointerno, pet_nrointerno, prioridad) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertPeticionPlancab2"
      .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@prioridad", adChar, adParamInput, 1, prioridad)
      Set aplRST = .Execute
    End With
    sp_InsertPeticionPlancab2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionPlancab2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionPlancab(per_nrointerno, pet_nrointerno, prioridad, cod_bpe) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionPlancab"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@prioridad", adInteger, adParamInput, , IIf(CLng(Val(prioridad)) = 0, Null, CLng(Val(prioridad))))
        .Parameters.Append .CreateParameter("@cod_bpe", adChar, adParamInput, 10, cod_bpe)
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionPlancab = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionPlancab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionPlancabField(per_nrointerno, pet_nrointerno, campo, valortxt, valordate, valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionPlancabField"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 30, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum), 0, CLng(Val("" & valorNum))))
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionPlancabField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionPlancabField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionPlancab(per_nrointerno, pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeletePeticionPlancab"
      .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(IsNull(per_nrointerno), Null, per_nrointerno))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , IIf(IsNull(pet_nrointerno), Null, pet_nrointerno))
      Set aplRST = .Execute
    End With
    sp_DeletePeticionPlancab = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionPlancab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionPlancab(per_nrointerno, pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionPlancab"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = 0, Null, CLng(Val(per_nrointerno))))
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionPlancab = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionPlancab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPetPlanificar(per_nrointerno, cod_bpe, cod_bpar, Situacion, Estado, cod_grupo, priorizadas) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPetPlanificar"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(IsNull(per_nrointerno), Null, per_nrointerno))
        .Parameters.Append .CreateParameter("@cod_bpe", adChar, adParamInput, 10, IIf(cod_bpe = "NULL", Null, cod_bpe))
        .Parameters.Append .CreateParameter("@cod_bpa", adChar, adParamInput, 10, IIf(cod_bpar = "NULL", Null, cod_bpar))
        .Parameters.Append .CreateParameter("@cod_bpa", adChar, adParamInput, 4, IIf(Situacion = "NULL", Null, Situacion))
        .Parameters.Append .CreateParameter("@estado", adChar, adParamInput, 4, IIf(Estado = "NULL", Null, Estado))
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, IIf(cod_grupo = "NULL", Null, cod_grupo))
        .Parameters.Append .CreateParameter("@priorizadas", adChar, adParamInput, 1, IIf(priorizadas = "NULL", Null, priorizadas))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPetPlanificar = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPetPlanificar = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPetPlanificarPrioridad(per_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPetPlanificarPrioridad"
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(IsNull(per_nrointerno), Null, per_nrointerno))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPetPlanificarPrioridad = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPetPlanificarPrioridad = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPetPlanificar_Agregar(cod_bpar, per_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPetPlanificarBP0"
        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = "NULL", Null, CLng(Val(per_nrointerno))))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPetPlanificar_Agregar = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPetPlanificar_Agregar = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
