Attribute VB_Name = "FuncionesGenerales"
' -001- a. FJS 29.06.2007 - Se agrega un nuevo proc. para mostrar mensajes en los distintos panels del control StatusBar del formulario MDI.
' -001- b. FJS 10.07.2007 - Se define una constante de tipo Date de acceso p�blico para indicar cual ser� la fecha de corte para la realizaci�n
'                           de los nuevos controles sobre la documentaci�n de las peticiones. Se define una funci�n booleana que en base a esa
'                           constante evaluar� que controles se realizar�n sobre los documentos de la petici�n en cuesti�n.
' -002- a. FJS 18.08.2007 - Dos nuevas funciones para obtener el usuario y el puesto de trabajo. Adem�s se agrega una funci�n para analizar los
'                           argumentos de la l�nea de comandos.
' -003- a. FJS 04.10.2007 - Se generaliza la funci�n para identificar objectos en el status bar del mdi.
' -004- a. FJS 22.04.2008 - Se agrega una funci�n personalizada para emular el InStrRev del Visual Basic 6.0 pero devolviendo la posici�n buscada
'                           comenzando desde la izquierda, como la funci�n InStr.
' -005- a. FJS 09.05.2008 - Se agrega que al deshabilitar un control de manera gen�rica, se desactiva tambi�n el TabStop.
' -006- a. FJS 08.09.2008 - Se agregan funciones de encriptado y desencriptado (creadas por Harvey T.)
' -007- a. FJS 21.11.2008 - Funci�n para ADAH (creada por Matias).
' -008- a. FJS 25.03.2009 - Nueva funci�n para consolidar recordsets.
' -009- a. FJS 30.03.2009 - Se agrega nueva funci�n para agregar a la cadena actual de parametros para invocar al ADAH el legajo de recurso de reemplazo
'                           (en caso que no haya reemplazo, se repite el legajo de recurso actuante).
' -010- a. FJS 16.06.2009 - Nueva funci�n booleana para establecer existencia de archivo en ubicaci�n.
' -011- a. FJS 28.01.2010 - Se elimina del men� principal el acceso al link del aplicativo ADAH (se descontinu� el mismo).
' -012- a. FJS 08.11.2010 - Nuevas funciones de VB6: Split y Join (emula las funciones nativas del VB6.0)
' -013- a. FJS 10.11.2010 - Se eliminan las referencias a APIs de Windows que no se usan.
' -014- a. FJS 29.04.2011 - Nuevas funciones auxiliares.
' -015- a. FJS 02.02.2011 - Nuevo: se agrega nueva funci�n para evaluar si una direcci�n de email es v�lida.

Option Explicit

Public Const ENCRYPT = 1
Public Const DECRYPT = 2
Public Const xForReading = 1
Public Const BLOCK_SIZE = 16384

'Public Enum CONTROL_INPUT
'    NORMAL = 0
'    DISABLE = 1
'    OBLIGATORIO = 2
'End Enum

'Public Function EstadoDeArchivo(ByVal Archivo As String) As Boolean
'    Dim fso
'
'    Set fso = CreateObject("Scripting.FileSystemObject")
'
'    If (fso.FileExists(Archivo)) Then
'        EstadoDeArchivo = True
'    Else
'        EstadoDeArchivo = False
'    End If
'End Function

Function OutTab(ByRef sTexto As Variant)
    Dim i As Integer
    For i = 1 To Len(sTexto)
        If Mid(sTexto, i, 1) = vbTab Then
            Mid(sTexto, i, 1) = " "
        End If
    Next
End Function

Function padLeft(sTexto As Variant, iLong As Integer) As String
    Dim auxString As String
    auxString = ClearNull(sTexto) & Space(iLong)
    padLeft = Left$(auxString, iLong)
End Function

Function padRight(sTexto As Variant, iLong As Integer) As String
    Dim auxString As String
    auxString = Space(iLong) & ClearNull(sTexto)
    padRight = Right$(auxString, iLong)
End Function

Function ClearNull(sTexto As Variant)
    If IsNull(sTexto) Then
        ClearNull = ""
    Else
        ClearNull = Trim(sTexto)
    End If
End Function

'Function ReplaceString(vCadena As Variant, nBuscar As Integer, Optional vReemplazar As Variant) As Variant
Function ReplaceString(vCadena As Variant, nBuscar As Variant, Optional vReemplazar As Variant) As Variant
    Dim nIndice As Integer
    Dim nCaracter As Integer
    For nIndice = 1 To Len(vCadena)
        nCaracter = Asc(Mid(vCadena, nIndice, 1))
        If nCaracter = nBuscar Then
            If Not IsMissing(vReemplazar) Then
                 ReplaceString = ReplaceString & vReemplazar
            End If
        Else
            ReplaceString = ReplaceString & Chr(nCaracter)
        End If
    Next nIndice
End Function

Function cutString(vCadena As Variant, nBuscar As Integer) As Variant
    Dim nIndice As Integer
    Dim nCaracter As Integer
    For nIndice = Len(vCadena) To 1 Step -1
        nCaracter = Asc(Mid(vCadena, nIndice, 1))
        If nCaracter <> nBuscar Then
            Exit For
        End If
    Next nIndice
    cutString = Mid(vCadena, 1, nIndice)
End Function

Function ReplaceComma(cValue As String)
    'Cambia la coma decimal por punto en un string
    Dim nPos%
    If InStr(cValue, ",") <> 0 Then
       nPos = InStr(cValue, ",")
       cValue = Left$(cValue, nPos - 1) & "." & Mid$(cValue, nPos + 1)
    End If
    ReplaceComma = cValue
End Function

Function ParseString(vRetorno() As String, ByVal sString As String, sSeparador As String) As Integer
    If Len(sString) = 0 Then
        ParseString = 0
        Exit Function
    End If
    
    Dim nLenSep As Integer
    nLenSep = Len(sSeparador)
    Dim nInicio As Integer
    Dim nPosicion As Integer
    Dim nElements As Integer
    'esto es para forzar por lo menos un elemente a derecha
    sString = " " & sString & " "
    nElements = 0
    nInicio = 1
    nPosicion = InStr(1, sString, sSeparador)
    
    Do While nPosicion <> 0
        nElements = nElements + 1
        ReDim Preserve vRetorno(nElements)
        vRetorno(nElements) = Trim(Mid(sString, 1, nPosicion - 1))
        sString = Mid(sString, nPosicion + 1)
        nPosicion = InStr(1, sString, sSeparador)
    Loop
    If Len(sString) > 0 Then
        nElements = nElements + 1
        ReDim Preserve vRetorno(nElements)
        vRetorno(nElements) = Trim(sString)
    End If
    ParseString = nElements
End Function

Function getStrCodigo(ByVal sStrInput As String, Optional bRight) As Variant
    'el string despues del || puede contener varios codigos separados con |
    'se toma como clave el primero

    If IsMissing(bRight) Then
        bRight = False
    End If

    If ClearNull(sStrInput) = "" Then
    getStrCodigo = ""
    Exit Function
    End If
    If bRight Then
            sStrInput = Trim(Mid(sStrInput, InStr(1, sStrInput, "||") + 2))
            If InStr(1, sStrInput, "|") = 0 Then
                getStrCodigo = Trim(sStrInput)
            Else
                getStrCodigo = Trim(Left(sStrInput, InStr(1, sStrInput, "|") - 1))
            End If
    Else
           If InStr(1, sStrInput, ":") = 0 Then
              getStrCodigo = Trim(sStrInput)
           Else
              getStrCodigo = Trim(Left(sStrInput, InStr(1, sStrInput, ":") - 1))
           End If
    End If
End Function

'**************************************************************
Function SetCombo(cControl As Control, vValor As Variant, Optional bRight) As Integer
    Dim nIndice As Integer
    Dim sAux As String
    If IsMissing(bRight) Then
        bRight = False
    End If
    SetCombo = -1
    If Trim(vValor) <> "" Then
        For nIndice = 0 To cControl.ListCount - 1
            If bRight Then
               sAux = Trim(Mid(cControl.List(nIndice), InStr(1, cControl.List(nIndice), "||") + 2))
               If InStr(1, sAux, "|") > 0 Then
                   sAux = Trim(Left(sAux, InStr(1, sAux, "|") - 1))
               End If
               If Trim(vValor) = Trim(sAux) Then
                   SetCombo = nIndice
                   Exit For
                End If
            Else
                If InStr(1, cControl.List(nIndice), ":") = 0 Then
                    If Trim(cControl.List(nIndice)) = Trim(vValor) Then
                        SetCombo = nIndice
                        Exit For
                    End If
                Else
                    If Trim(Left(cControl.List(nIndice), InStr(1, cControl.List(nIndice), ":") - 1)) = Trim(vValor) Then
                        SetCombo = nIndice
                        Exit For
                    End If
                End If
            End If
        Next nIndice
    End If
    cControl.ListIndex = SetCombo
End Function

Function PosicionCombo(cControl As Control, vValor As Variant, Optional bRight) As Integer
    Dim nIndice As Integer
    Dim sAux As String
    If IsMissing(bRight) Then
        bRight = False
    End If
    'el string despues del || puede contener varios codigos separados con |
    'se toma como clave el primero
    PosicionCombo = -1
    If Trim(vValor) <> "" Then
        For nIndice = 0 To cControl.ListCount - 1
            If bRight Then
               sAux = Trim(Mid(cControl.List(nIndice), InStr(1, cControl.List(nIndice), "||") + 2))
               If InStr(1, sAux, "|") > 0 Then
                   sAux = Trim(Left(sAux, InStr(1, sAux, "|") - 1))
               End If
               If Trim(vValor) = Trim(sAux) Then
                   PosicionCombo = nIndice
                   Exit For
                End If
            Else
                If InStr(1, cControl.List(nIndice), ":") = 0 Then
                    If Trim(cControl.List(nIndice)) = Trim(vValor) Then
                        PosicionCombo = nIndice
                        Exit For
                    End If
                Else
                    If Trim(Left(cControl.List(nIndice), InStr(1, cControl.List(nIndice), ":") - 1)) = Trim(vValor) Then
                        PosicionCombo = nIndice
                        Exit For
                    End If
                End If
            End If
        Next nIndice
    End If
End Function

Function CodigoCombo(cControl As Control, Optional bRight, Optional bComplete As Boolean) As Variant
    Dim auxStr As String
    Dim sAux As String
    
    If IsMissing(bRight) Then
        bRight = False
    End If
    
    If bRight Then
        If cControl.ListIndex >= 0 Then
            auxStr = "" & cControl.List(cControl.ListIndex)
            auxStr = Trim(Mid(auxStr, InStr(1, auxStr, "||") + 2))
            If InStr(1, auxStr, "|") = 0 Or bComplete Then
                CodigoCombo = Trim(auxStr)
            Else
                CodigoCombo = Trim(Left(auxStr, InStr(1, auxStr, "|") - 1))
            End If
        Else
           CodigoCombo = ""
        End If
    Else
        If cControl.ListIndex >= 0 Then
           auxStr = "" & cControl.List(cControl.ListIndex)
           If InStr(1, auxStr, ":") = 0 Then
              CodigoCombo = Trim(auxStr)
           Else
              CodigoCombo = Trim(Left(auxStr, InStr(1, auxStr, ":") - 1))
           End If
        Else
           CodigoCombo = ""
        End If
    End If
End Function

Function CodigoCombo2(ByVal cControl As Variant, Optional bRight, Optional bComplete As Boolean) As Variant
    Dim auxStr As String
    Dim sAux As String
    
    If IsMissing(bRight) Then
        bRight = False
    End If
    
    If bRight Then
        auxStr = "" & cControl
        auxStr = Trim(Mid(auxStr, InStr(1, auxStr, "||") + 2))
        If InStr(1, auxStr, "|") = 0 Or bComplete Then
            CodigoCombo2 = Trim(auxStr)
        Else
            CodigoCombo2 = Trim(Left(auxStr, InStr(1, auxStr, "|") - 1))
        End If
    Else
        auxStr = "" & cControl
        If InStr(1, auxStr, ":") = 0 Then
           CodigoCombo2 = Trim(auxStr)
        Else
           CodigoCombo2 = Trim(Left(auxStr, InStr(1, auxStr, ":") - 1))
        End If
    End If
End Function

Function DatosCombo(cControl As Control) As Variant
    'trae todo el token despues de ||
    Dim auxStr As String
    If cControl.ListIndex >= 0 Then
       auxStr = "" & cControl.List(cControl.ListIndex)
       DatosCombo = Trim(Mid(auxStr, InStr(1, auxStr, "||") + 2))
    Else
       DatosCombo = ""
    End If
End Function

Function TextoCombo(cControl As Control, Optional sCodigo As Variant, Optional bRight) As Variant
    Dim auxStr As String
    Dim i As Integer
    
    If IsMissing(bRight) Then
        bRight = False
    End If
    
    If Not IsMissing(sCodigo) Then
        i = PosicionCombo(cControl, Trim(sCodigo), bRight)
        cControl.ListIndex = i
    End If
    
    If cControl.ListIndex >= 0 Then
       auxStr = "" & ClearNull(cControl.List(cControl.ListIndex))
       'saca lo posterior a ||
       If InStr(1, auxStr, "||") > 0 Then
          auxStr = Trim(Left$(auxStr, InStr(1, auxStr, "||") - 1))
       End If
       'saca lo anterior a :
       If InStr(1, auxStr, ":") > 0 Then
          auxStr = Trim(Right(auxStr, Len(auxStr) - InStr(1, auxStr, ":")))
       End If
    Else
       auxStr = ""
    End If
    TextoCombo = Trim(auxStr)
End Function

Function CodigoText(cControl As Control, Optional bRight, Optional bComplete As Boolean) As Variant
    Dim auxStr As String
    
    If IsMissing(bRight) Then bRight = False
    
    If bRight Then
        If Len(Trim(cControl.ListIndex)) > 0 Then
            auxStr = "" & cControl.text
            auxStr = Trim(Mid(auxStr, InStr(1, auxStr, "||") + 2))
            If InStr(1, auxStr, "|") = 0 Or bComplete Then
                CodigoText = Trim(auxStr)
            Else
                CodigoText = Trim(Left(auxStr, InStr(1, auxStr, "|") - 1))
            End If
        Else
           CodigoText = ""
        End If
    Else
        If Len(Trim(cControl.ListIndex)) > 0 Then
           auxStr = "" & cControl.text
           If InStr(1, auxStr, ":") = 0 Then
              CodigoText = Trim(auxStr)
           Else
              CodigoText = Trim(Left(auxStr, InStr(1, auxStr, ":") - 1))
           End If
        Else
           CodigoText = ""
        End If
    End If
End Function

Sub touchForms()
    Dim i As Integer
    For i = 0 To Forms.Count - 1
        Forms(i).Tag = "REFRESH"
    Next i
End Sub

Sub titForms(iTop As Integer)
    Dim strTitulo As String
    Dim i As Integer
    Exit Sub
    mdiPrincipal.Caption = "Administracion de Peticiones"
    For i = iTop To Forms.Count - 1
        strTitulo = strTitulo & Forms(i).Caption & " - "
    Next i
    mdiPrincipal.Caption = strTitulo
End Sub

Sub FormCenter(frmToCenter As Form, frmFromCenter As Form, Optional flgMaxi)
    Dim nLeft%, nTop%
    
    If IsMissing(flgMaxi) Then
        flgMaxi = False
    End If
    frmToCenter.WindowState = vbNormal
    If flgMaxi = True Then
        If frmFromCenter.Width / 15 <= 650 Then
            frmToCenter.WindowState = vbMaximized
            Exit Sub
        End If
    End If
    nLeft = ((frmFromCenter.ScaleWidth - frmToCenter.Width) / 2)
    If nLeft < 1 Then '
        nLeft = 1
    End If
    frmToCenter.Move nLeft, 1
    DoEvents
End Sub

Sub FormCenterModal(frmToCenter As Form, frmFromCenter As Form)
   Dim nLeft%, nTop%
   
   nLeft = ((frmFromCenter.Width - frmToCenter.Width) / 2)
   nLeft = nLeft + frmFromCenter.Left
   
   If nLeft < 1 Then '
      nLeft = 1
   End If
   frmToCenter.Move nLeft, frmFromCenter.Top
   DoEvents
End Sub

Sub SeleccionarTexto(cControl As Control)
    With cControl
        .SelStart = 0
        .SelLength = Len(cControl)
    End With
End Sub

Sub setColorCtrl(cControl As Control, xLevel As String)
    Select Case xLevel
        Case "DIS": cControl.BackColor = vb3DLight
        Case "NOR": cControl.BackColor = vbWindowBackground     ' cControl.BackColor = RGB(255, 255, 255)
        Case "OBL": cControl.BackColor = vbInfoBackground       ' cControl.BackColor = RGB(255, 255, 0)
    End Select
End Sub

Sub setHabilCtrl(cControl As Control, xLevel As String)
'Sub setHabilCtrl(cControl As Control, xLevel As CONTROL_INPUT)
    On Error GoTo Errores
    Select Case xLevel
        Case "DIS"      ' Disabled mode
        'Case CONTROL_INPUT.DISABLE
            If TypeOf cControl Is TextBox Then
                'cControl.BackColor = &H8000000F
                'cControl.BackColor = vb3DLight
                If cControl.MultiLine Then
                    cControl.BackColor = &H8000000F
                Else
                    cControl.BackColor = vb3DLight
                End If
                cControl.Locked = True
            ElseIf TypeOf cControl Is CheckBox Then
                cControl.Enabled = False
            ElseIf TypeOf cControl Is CommandButton Then
                cControl.BackColor = &H8000000F
                cControl.Enabled = False
            ElseIf TypeOf cControl Is Frame Then
                cControl.Enabled = False
            ElseIf TypeOf cControl Is OptionButton Then
                cControl.Enabled = False
            ElseIf TypeOf cControl Is MSComCtl2.DTPicker Then
                cControl.Enabled = False
            ElseIf TypeOf cControl Is MSComCtl2.UpDown Then
                cControl.Enabled = False
            Else
                cControl.BackColor = vb3DLight
                cControl.Enabled = False
            End If
            cControl.TabStop = False    ' add -005- a.
        Case "NOR"      ' Normal mode
        'Case CONTROL_INPUT.NORMAL
            If TypeOf cControl Is TextBox Then
                cControl.BackColor = vbWindowBackground
                cControl.ForeColor = vbBlack
                cControl.Locked = False
            ElseIf TypeOf cControl Is CheckBox Then
                cControl.Enabled = True
            ElseIf TypeOf cControl Is CommandButton Then
                cControl.BackColor = &H8000000F
                cControl.Enabled = True
            ElseIf TypeOf cControl Is Frame Then
                cControl.Enabled = True
            ElseIf TypeOf cControl Is OptionButton Then
                cControl.Enabled = True
            Else
                cControl.BackColor = vbWindowBackground
                cControl.Enabled = True
            End If
            cControl.TabStop = True    ' add -005- a.
        Case "OBL"      ' Entrada obligatoria
        'Case CONTROL_INPUT.OBLIGATORIO
            'cControl.BackColor = RGB(255, 255, 128)
            cControl.BackColor = &HC0FFFF
            If TypeOf cControl Is TextBox Then
                cControl.Locked = False
            Else
                cControl.Enabled = True
            End If
            cControl.TabStop = True    ' add -005- a.
    End Select
Exit Sub
Errores:
    Select Case Err.Number
        Case 438    ' Object doesn't support this property or method
            Resume Next
    End Select
End Sub

Sub setVisibleCtrl(cControl As Control, xLevel As String)
    Select Case xLevel
        Case "DIS"      ' Invisible mode
            cControl.visible = False
'            If TypeOf cControl Is TextBox Then
'                cControl.BackColor = vb3DLight
'                cControl.Locked = True
'            ElseIf TypeOf cControl Is CheckBox Then
'                cControl.Enabled = False
'            ElseIf TypeOf cControl Is CommandButton Then
'                cControl.BackColor = &H8000000F     ' Ojo!
'                cControl.Enabled = False
'            Else
'                cControl.BackColor = vb3DLight
'                cControl.Enabled = False
'            End If
        Case "NOR"      ' Normal mode
            cControl.visible = True
'            If TypeOf cControl Is TextBox Then
'                cControl.BackColor = vbWindowBackground
'                cControl.Locked = False
'            ElseIf TypeOf cControl Is CheckBox Then
'                cControl.Enabled = True
'            ElseIf TypeOf cControl Is CommandButton Then
'                cControl.BackColor = &H8000000F     ' Ojo!
'                cControl.Enabled = True
'            Else
'                cControl.BackColor = vbWindowBackground
'                cControl.Enabled = True
'            End If
    End Select
End Sub

Function hrsGetHoras(minutos As Long) As Long
    hrsGetHoras = minutos \ 60
End Function

Function hrsGetMinutos(minutos As Long) As Long
    hrsGetMinutos = minutos Mod 60
End Function

Function hrsFmtHora(minutos As Long) As String
    hrsFmtHora = hrsGetHoras(minutos) & ":" & Right("0" & hrsGetMinutos(minutos), 2)
End Function

Function LockProceso(flgModo As Boolean) As Boolean
    If flgModo = True Then
        If glLockProceso = False Then
            glLockProceso = True
            LockProceso = True
            Exit Function
        Else
            LockProceso = False
            Exit Function
        End If
    Else
        glLockProceso = False
        LockProceso = True
        Exit Function
    End If
End Function

Public Sub Debuggear(ByVal F As Form, sProceso As String, Optional separador As Boolean)
    If separador Then
        Debug.Print String(80, "-")
        Debug.Print Now & ": " & F.name & "_" & sProceso
        Debug.Print String(80, "-")
    Else
        Debug.Print Now & ": " & F.name & "_" & sProceso
    End If
End Sub

Public Sub Status(sMensaje As String)
    With mdiPrincipal
        .sbPrincipal.Panels(1) = Trim(sMensaje)
        If Len(sMensaje) > 30 Then
            .sbPrincipal.Panels(1).ToolTipText = Trim(sMensaje)
        Else
            .sbPrincipal.Panels(1).ToolTipText = ""
        End If
    End With
    DoEvents
End Sub

Public Sub StatusLocal(ByRef barra As StatusBar, sMensaje As String)
    With barra
        .SimpleText = Trim(sMensaje)
    End With
    'DoEvents
End Sub

Public Sub Puntero(bOpcion As Boolean)
    Screen.MousePointer = IIf(bOpcion, vbHourglass, vbNormal)
End Sub

Function PedirFechaDesdeHasta() As Boolean
    Load auxFechaDesdeHasta
    With auxFechaDesdeHasta
        .txtFechaDesde = Format(glFechaDesde, "ddmmyyyy")
        .txtFechaDesde.ForceFormat
        .txtFechaHasta = Format(glFechaHasta, "ddmmyyyy")
        .txtFechaHasta.ForceFormat
        .Show 1
        DoEvents
        PedirFechaDesdeHasta = False
        On Error GoTo err_fecha
        If auxFechaDesdeHasta.bAceptar = True Then
           glFechaDesde = .txtFechaDesde.DateValue
           glFechaHasta = .txtFechaHasta.DateValue
           PedirFechaDesdeHasta = True
        End If
    End With
    Unload auxFechaDesdeHasta
    Exit Function
err_fecha:
    MsgBox ("Fecha inv�lida")
    Unload auxFechaDesdeHasta
End Function

'**************************************************************
Function LeerINI(sApp As String, sKey As String, sDefault As String, sINI As String) As String
    '**************************************************************************
    'Lee valores de un archivo INI
    'Parametros:    sApp (aplicacion)
    '               sKey (item)
    '               sDefault (valor default)
    '               sINI (archivo INI a leer)
    '**************************************************************************
    Dim lRetCode As Long
    LeerINI = String(254, Chr(0))
    lRetCode = GetPrivateProfileString(sApp, sKey, sDefault, LeerINI, 254, sINI)
    LeerINI = Left(LeerINI, InStr(1, LeerINI, Chr(0)) - 1)
End Function

'**************************************************************
Sub AnalizarLineaComando()
    Dim sComando As String
    Dim nInicio As Integer
    Dim nPosicion As Integer
    Dim sValorComando As String
    glArchivoINI = App.Path & "\" & ArchivoINI
    sComando = Trim(UCase(Command))
    nInicio = 1
    nPosicion = InStr(nInicio, sComando, "-")
    Do While nPosicion <> 0
        sValorComando = Mid(sComando, nPosicion + 1, 3)
        Select Case sValorComando
            Case "INI"
                glArchivoINI = UCase(Mid(sComando, nPosicion + 6, InStr(nPosicion + 6, sComando, Chr(34)) - nPosicion - 6))
        End Select
        nInicio = nPosicion + 1
        nPosicion = InStr(nInicio, sComando, "-")
    Loop
End Sub

'{ add -002- a.
Public Sub CaptureCommandLineArguments()
    Dim sCommandLineArguments As String         ' Cadena completa de argumentos y valores
    Dim sSimpleCommandLineArgument As String    ' Nombre del argumento
    Dim nInicio As Integer                      ' Para indicar la posici�n inicial de cada vuelta
    Dim nPosicionInicialValor As Integer
    Dim nPosicionFinalValor As Integer
    Dim pUsuario, pPassword As String
    Dim bAutomaticLogOutlogIn As Boolean
    
    sCommandLineArguments = Trim(Command)       ' Obtiene los argumentos en bruto
    
    nInicio = 1: nPosicionInicialValor = 1
    Do While nPosicionInicialValor <> 0 And nInicio < Len(sCommandLineArguments)
        nPosicionInicialValor = InStr(nInicio, sCommandLineArguments, "-")
        sSimpleCommandLineArgument = Mid(sCommandLineArguments, nPosicionInicialValor + 1, 3)
        Select Case Trim(UCase(sSimpleCommandLineArgument))
            Case "USR"  ' Indica el nombre del usuario de Windows
                nPosicionFinalValor = InStr(nPosicionInicialValor + 4, sCommandLineArguments, "-", vbTextCompare) - 1
                If nPosicionFinalValor = -1 Then nPosicionFinalValor = Len(sCommandLineArguments)
                pUsuario = Trim(Mid(sCommandLineArguments, nPosicionInicialValor + 5, (nPosicionFinalValor - nPosicionInicialValor) - 4))
            Case "PWD"  ' Contrase�a del usuario
                nPosicionFinalValor = InStr(nPosicionInicialValor + 4, sCommandLineArguments, "-", vbTextCompare) - 1
                If nPosicionFinalValor = -1 Then nPosicionFinalValor = Len(sCommandLineArguments)
                pPassword = Trim(Mid(sCommandLineArguments, nPosicionInicialValor + 5, (nPosicionFinalValor - nPosicionInicialValor) - 4))
            Case "OFF"  ' Habilita el logout autom�tico
                nPosicionFinalValor = nPosicionFinalValor + 4
                bAutomaticLogOutlogIn = True
            Case Else
                ' Se pas� un argumento inexistente (unhandled), es omitido y se pasa a evaluar el siguiente
                nPosicionFinalValor = nPosicionFinalValor + 4
        End Select
        nInicio = nPosicionFinalValor + 1
    Loop
    If Len(pUsuario) > 0 And Len(pPassword) > 0 Then
        ' Se han pasado como argumentos expl�citamente el usuario y la contrase�a: se realiza el login autom�tico
        frmLogin.InvisibleLogin UCase(pUsuario), FuncionesEncriptacion.Desencriptar(pPassword)
    End If
End Sub
'}

Function GrabarINI(sApp As String, sKey As String, sTexto As String, sINI As String) As String
    '**************************************************************************
    'Graba en un archivo INI
    'Parametros:    sApp (aplicacion)
    '               sKey (item)
    '               sTexto (valor a grabar)
    '               sINI (archivo INI a leer)
    '**************************************************************************
    Dim lRetCode As Long
    lRetCode = WritePrivateProfileString(sApp, sKey, sTexto, sINI)
End Function

Function calColFecha(fBase As Date, fComp As Date) As Integer
    Dim xOffSem As Integer
    Dim xdComp As Integer
    Dim xdBase As Integer
    Select Case Day(fBase)
        Case 1 To 7
            xdBase = 0
        Case 8 To 15
            xdBase = 1
        Case 16 To 23
            xdBase = 2
        Case Else
            xdBase = 3
    End Select
    Select Case Day(fComp)
        Case 1 To 7
            xdComp = 0
        Case 8 To 15
            xdComp = 1
        Case 16 To 23
            xdComp = 2
        Case Else
            xdComp = 3
    End Select
    xOffSem = (Year(fComp) * 48 + Month(fComp) * 4 + xdComp) - (Year(fBase) * 48 + Month(fBase) * 4 + xdBase)
    If xOffSem < 0 Then
        xOffSem = 0
    End If
    calColFecha = xOffSem
End Function

Function SizeFile(sFileName As String) As Long
    Dim F As Long, FileSize As Long
    On Error GoTo Err_Fs
    F = FreeFile
    Open sFileName For Binary As #F
    FileSize = LOF(F)
    Close #F
    SizeFile = FileSize
    On Error GoTo 0
Exit Function
Err_Fs:
    SizeFile = 0
End Function

'{ add -002- a.
Public Function cstmGetUserName() As String
    Dim lpBuffer As String * 255
    Dim nSize As Long
    Dim sLongSize As String
    
    lpBuffer = Space$(255)
    nSize = 16
    sLongSize = GetUserName(lpBuffer, nSize)
    If Len(sLongSize) > 0 Then
        cstmGetUserName = UCase(Mid(lpBuffer, 1, nSize - 1))
    Else
        cstmGetUserName = ""
    End If
End Function

Public Function cstmGetComputerName() As String
    Dim lpBuffer As String * 255
    Dim nSize As Long
    Dim sLongSize As String
    
    lpBuffer = Space$(255)
    nSize = 16
    sLongSize = GetComputerName(lpBuffer, nSize)
    If Len(sLongSize) > 0 Then
        'cstmGetComputerName = Mid(lpBuffer, 1, nSize - 1)
        cstmGetComputerName = Mid(lpBuffer, 1, nSize)
    Else
        cstmGetComputerName = ""
    End If
End Function
'}

'{ add -004- a.
Public Function cstmInStrRev(ByVal cString As String, ByVal cMatchString As String) As Long
    Dim cAux As String
    Dim lPos As Long
    Dim i As Long
    
    For i = Len(cString) To 1 Step -1
        cAux = cAux & Mid(cString, i, 1)
    Next i
    lPos = InStr(1, cAux, cMatchString, vbTextCompare)
    lPos = Len(cString) - (lPos - 1)
    cstmInStrRev = lPos
End Function
'}

'{ add -006- a.
Public Function EncryptString(UserKey As String, text As String, Action As Single) As String
    Dim UserKeyX As String
    Dim Temp     As Integer
    Dim Times    As Integer
    Dim i        As Integer
    Dim J        As Integer
    Dim n        As Integer
    Dim rtn      As String
      
    '//Get UserKey characters
    n = Len(UserKey)
    ReDim UserKeyASCIIS(1 To n)
    For i = 1 To n
        UserKeyASCIIS(i) = Asc(Mid$(UserKey, i, 1))
    Next
          
    '//Get Text characters
    ReDim TextASCIIS(Len(text)) As Integer
    For i = 1 To Len(text)
        TextASCIIS(i) = Asc(Mid$(text, i, 1))
    Next

    '//Encryption/Decryption
    If Action = ENCRYPT Then
       For i = 1 To Len(text)
           J = IIf(J + 1 >= n, 1, J + 1)
           Temp = TextASCIIS(i) + UserKeyASCIIS(J)
           If Temp > 255 Then
              Temp = Temp - 255
           End If
           rtn = rtn + Chr$(Temp)
       Next
    ElseIf Action = DECRYPT Then
       For i = 1 To Len(text)
           J = IIf(J + 1 >= n, 1, J + 1)
           Temp = TextASCIIS(i) - UserKeyASCIIS(J)
           If Temp < 0 Then
              Temp = Temp + 255
           End If
           rtn = rtn + Chr$(Temp)
       Next
    End If
    EncryptString = rtn
End Function
'}

'{ add -xxx-
Public Function SoyElAnalista() As Boolean
    SoyElAnalista = False
    If cstmGetUserName = "A125958" Then
        SoyElAnalista = True
    End If
End Function

Public Function NombreDeMes(bytMes As Byte, Optional blnEnMayusculas As Boolean) As String
    If IsMissing(blnEnMayusculas) Then
        blnEnMayusculas = False
    End If
    Select Case bytMes
        Case 1: NombreDeMes = IIf(blnEnMayusculas, "ENERO", "Enero")
        Case 2: NombreDeMes = IIf(blnEnMayusculas, "FEBRERO", "Febrero")
        Case 3: NombreDeMes = IIf(blnEnMayusculas, "MARZO", "Marzo")
        Case 4: NombreDeMes = IIf(blnEnMayusculas, "ABRIL", "Abril")
        Case 5: NombreDeMes = IIf(blnEnMayusculas, "MAYO", "Mayo")
        Case 6: NombreDeMes = IIf(blnEnMayusculas, "JUNIO", "Junio")
        Case 7: NombreDeMes = IIf(blnEnMayusculas, "JULIO", "Julio")
        Case 8: NombreDeMes = IIf(blnEnMayusculas, "AGOSTO", "Agosto")
        Case 9: NombreDeMes = IIf(blnEnMayusculas, "SEPTIEMBRE", "Septiembre")
        Case 10: NombreDeMes = IIf(blnEnMayusculas, "OCTUBRE", "Octubre")
        Case 11: NombreDeMes = IIf(blnEnMayusculas, "NOVIEMBRE", "Noviembre")
        Case 12: NombreDeMes = IIf(blnEnMayusculas, "DICIEMBRE", "Diciembre")
    End Select
End Function
'}

Function ReplaceApostrofo(cValue As String) As String       ' Cambia la coma decimal por punto en un string
    Dim nPos%
    Do While InStr(cValue, "'") > 0
        If InStr(cValue, "'") <> 0 Then
            nPos = InStr(cValue, "'")
            cValue = Left$(cValue, nPos - 1) & "�" & Mid$(cValue, nPos + 1)
        End If
    Loop
    ReplaceApostrofo = cValue
End Function

'{ add -010- a.
Public Function ExisteArchivo(cSourcePathFile As String) As Boolean
    Dim cAuxStr As String
    
    ExisteArchivo = False
    cAuxStr = Dir(cSourcePathFile)
    If Len(cAuxStr) > 0 Then
        ExisteArchivo = True
    End If
End Function
'}

Public Function ObtenerNombreDeArchivo(cInputFile) As String
    Dim Pos As Long
    
    Pos = cstmInStrRev(cInputFile, "\")
    ObtenerNombreDeArchivo = Mid(cInputFile, Pos + 1)
End Function

Public Function ObtenerDirectorio(cInputFile) As String
    Dim Pos As Long
    
    Pos = cstmInStrRev(cInputFile, "\")
    ObtenerDirectorio = Mid(cInputFile, 1, Pos - 1)
End Function

'{ add -012- a.
Public Sub ArmarVectorDeArchivos(cLista As String, cSeparator As String)
    ' Recibe una cadena con los elementos de una lista para armar el vector general vElemento
    ' discriminando que solos sean archivos reales los que conformen el vector
    Dim i As Long
    Dim cPath As String
    
    cPath = Mid(cLista, 1, InStr(1, cLista, cSeparator, vbTextCompare) - 1)
    cPath = IIf(Right(cPath, 1) = "\", cPath & "", cPath & "\")
    If (GetAttr(cPath) And vbArchive) <> vbArchive Then
        cLista = Mid(cLista, InStr(1, cLista, cSeparator, vbTextCompare) + 1)
    End If

    Erase vElemento             ' Inicializa el vector
    Do While Len(cLista) > 0
        ReDim Preserve vElemento(i)
        If InStr(1, cLista, cSeparator, vbTextCompare) > 0 Then
            vElemento(i) = cPath & Mid(cLista, 1, InStr(1, cLista, cSeparator, vbTextCompare) - 1)
            cLista = Mid(cLista, InStr(1, cLista, cSeparator, vbTextCompare) + 1)
        Else
            vElemento(i) = cPath & Mid(cLista, 1, Len(cLista)): cLista = ""
        End If
        i = i + 1
    Loop
End Sub

'Public Sub Split(cLista As String, cSeparator As String)
'    ' Recibe una cadena con los elementos de una lista para armar el vector general vElemento
'    Dim i As Long
'
'    Erase vElemento             ' Inicializa el vector
'    Do While Len(cLista) > 0
'        ReDim Preserve vElemento(i)
'        If InStr(1, cLista, cSeparator, vbTextCompare) > 0 Then
'            vElemento(i) = Mid(cLista, 1, InStr(1, cLista, cSeparator, vbTextCompare) - 1)
'            cLista = Mid(cLista, InStr(1, cLista, cSeparator, vbTextCompare) + 1)
'        Else
'            vElemento(i) = Mid(cLista, 1, Len(cLista)): cLista = ""
'        End If
'        DoEvents
'        i = i + 1
'    Loop
'End Sub
'
'Public Function Join() As String
'    ' Falta definir esta funci�n
'End Function
'}

'Public Function getVersionNew() As String
'    Dim osinfo As OSVERSIONINFO
'    Dim retvalue As Integer
'
'    osinfo.dwOSVersionInfoSize = 148
'    osinfo.szCSDVersion = Space$(128)
'    retvalue = GetVersionExA(osinfo)
'    With osinfo
'        Select Case .dwPlatformId
'            Case 1
'                Select Case .dwMinorVersion
'                    Case 0
'                        getVersionNew = "Windows 95"
'                    Case 10
'                        getVersionNew = "Windows 98"
'                    Case 90
'                        getVersionNew = "Windows Mellinnium"
'                End Select
'            Case 2
'                Select Case .dwMajorVersion
'                    Case 3
'                        getVersionNew = "Windows NT 3.51"
'                    Case 4
'                        getVersionNew = "Windows NT 4.0"
'                    Case 5
'                        If .dwMinorVersion = 0 Then
'                            getVersionNew = "Windows 2000"
'                        Else
'                            getVersionNew = "Windows XP"
'                        End If
'                End Select
'        Case Else
'            getVersionNew = "Failed"
'        End Select
'    End With
'    If Len(Trim(osinfo.szCSDVersion)) > 0 Then
'        getVersionNew = getVersionNew & " (" & Mid(osinfo.szCSDVersion, 1, Len(Trim(osinfo.szCSDVersion)) - 1) & ")"
'    End If
'End Function
''}

'{ add -005- a.
Public Function GetShortName(ByVal sLongFileName As String) As String
    Dim lRetVal As Long, sShortPathName As String, iLen As Integer
    'Set up buffer area for API function call return
    sShortPathName = Space(255)
    iLen = Len(sShortPathName)

    'Call the function
    lRetVal = GetShortPathName(sLongFileName, sShortPathName, iLen)
    'Strip away unwanted characters.
    GetShortName = Left(sShortPathName, lRetVal)
End Function
'}

'{ add -013- a.
Public Function IDE_Version() As Boolean
    Dim strFileName As String
    Dim lngCount As Long
    
    strFileName = String(255, 0)
    lngCount = GetModuleFileName(App.hInstance, strFileName, 255)
    strFileName = Left(strFileName, lngCount)
    
    If UCase(Right(strFileName, 7)) <> "VB5.EXE" Then
        IDE_Version = False        ' Compiled Version
    Else
        IDE_Version = True         ' IDE Version
    End If
End Function
'}

'{ add -014- a.
Public Function DeterminarHabilDelMes(pFecha As Date, OrdenHabil As Long) As Date
    Dim dFechaIni As Date
    Dim dFechaFin As Date
    Dim dFechaAux As Date
    Dim contador As Long
    
    ' pFecha solo se necesita para determinar el mes y el a�o, no me importa el d�a
    dFechaIni = CDate("01/" & Month(pFecha) & "/" & Year(pFecha))
    dFechaFin = DateAdd("d", -1, DateAdd("m", 1, dFechaIni))
    dFechaAux = dFechaIni
    contador = 1
    
    Do While dFechaAux < dFechaFin
        If Weekday(dFechaAux, vbMonday) < 6 Then
            If contador = OrdenHabil Then
                If sp_GetFeriado(dFechaAux, dFechaAux) Then
                    dFechaAux = DateAdd("d", 1, dFechaAux)
                Else
                    Exit Do
                End If
            Else
                contador = contador + 1
                dFechaAux = DateAdd("d", 1, dFechaAux)
            End If
        Else
            dFechaAux = DateAdd("d", 1, dFechaAux)
        End If
        DoEvents
    Loop
    DeterminarHabilDelMes = dFechaAux
End Function

Public Function DeterminarUltimoHabilDelMes(pFecha As Date) As Date
    Dim dFechaIni As Date
    Dim dFechaFin As Date
    Dim dFechaAux As Date
    Dim contador As Long
    
    dFechaIni = CDate("01/" & Month(pFecha) & "/" & Year(pFecha))
    dFechaFin = DateAdd("d", -1, DateAdd("m", 1, dFechaIni))
    dFechaAux = dFechaFin
    
    Do While sp_GetFeriado(dFechaAux, dFechaAux)
        dFechaAux = DateAdd("d", -1, dFechaAux)
        DoEvents
    Loop
    DeterminarUltimoHabilDelMes = dFechaAux
End Function

Public Function DateAddHabil(ByVal interval As String, ByVal Number As Long, ByVal fecha As Date) As Date
    Dim i As Long
    
    i = 0
    Select Case interval
        Case "d"
            If Number > 0 Then
                Do While i < Number
                    If sp_GetFeriado(fecha, fecha) Then
                        fecha = DateAdd("d", 1, fecha)
                    Else
                        fecha = DateAdd("d", 1, fecha)
                        i = i + 1
                    End If
                Loop
            ElseIf Number < 0 Then
                Number = Number * -1
                Do While i < Number
                    If sp_GetFeriado(fecha, fecha) Then
                        fecha = DateAdd("d", -1, fecha)
                    Else
                        fecha = DateAdd("d", -1, fecha)
                        i = i + 1
                    End If
                Loop
            End If
    End Select
    DateAddHabil = fecha
End Function
'}

'Public Function CheckedAccess() As Boolean
'    Dim sKey As String
'    Dim cAppVersion As String
'    Dim lVersionNumber As Long
'
'    CheckedAccess = True
'
'    sKey = IIf(UCase(App.EXEName) = "PETCFG", "CFGVER", "CGMVER")
'
'    If sp_GetVarios(sKey) Then
'        If Not aplRST.EOF Then
'            cAppVersion = "v" & App.Major & "." & App.Minor & "." & App.Revision
'            If Trim(cAppVersion) <> Trim(aplRST.Fields!var_texto) Then
'                ' Esto queda para terminar de desarrollar!!!
'                MsgBox "Esta intentando ejecutar una versi�n incorrecta de la aplicaci�n." & vbCrLf & vbCrLf & _
'                       "Versi�n del sistema: " & Trim(aplRST.Fields("var_texto")) & vbCrLf & _
'                       "Versi�n del usuario: " & Trim(cAppVersion) & vbCrLf & vbCrLf & _
'                       "A continuaci�n, la aplicaci�n se cerrar�." & vbCrLf & vbCrLf & vbCrLf & vbCrLf & _
'                       "Importante:" & vbCrLf & vbCrLf & _
'                       "� REINICIE SU EQUIPO �" & vbCrLf & vbCrLf & _
'                       "Esto puede deberse a que existe una nueva versi�n de la aplicaci�n. Reinicie el equipo" & vbCrLf & _
'                       "para que pueda actualizarse automaticamente a la nueva versi�n del aplicativo. Si a�n" & vbCrLf & _
'                       "continua con el mismo problema, luego de reiniciar, comun�quese directamente al GAC.", vbExclamation + vbOKOnly, "Versi�n incorrecta de CGM"
'                CheckedAccess = False
'            Else
'                If CLng(aplRST.Fields!var_numero) = 0 Then
'                    MsgBox "Por cuestiones de desarrollo e implementaci�n de versiones, se ha deshabilitado el acceso al sistema." & vbCrLf & vbCrLf & _
'                           "Vuelva a intentar el ingreso al sistema m�s tarde.", vbExclamation + vbOKOnly, "CGM offline"
'                    CheckedAccess = False
'                End If
'            End If
'        End If
'    End If
'End Function

Public Function ValidarVersionDelAplicativo() As Boolean
    Dim sKey As String, sModo As String, sKeyVer As String
    Dim cAppVersion As String
    Dim cDBMSVersion As String
    Dim lDBMS_VersionNumber As Long, lAPP_VersionNumber As Long
    Dim GACNumero As String
    
    ValidarVersionDelAplicativo = True
    
    ' ********************************************************************************
    ' * Valores de sModo
    ' *
    ' * 1: control hasta versi�n Mayor
    ' * 2: control hasta versi�n Menor
    ' * 3: control total (versi�n Mayor + versi�n Menor y el n� de revisi�n)
    ' ********************************************************************************
    
    GACNumero = "22422"                                     ' Por defecto
    sModo = "1"                                             ' En caso que no exista la clave
    sKey = IIf(UCase(App.EXEName) = "PETCFG", "CFGVER", "CGMVER")
    sKeyVer = IIf(sKey = "CGMVER", "MODVER_1", "MODVER_2")  ' 1: pet.exe / 2: petcfg.exe
            
    ' Primero obtengo el nro. del GAC por las dudas
    If sp_GetVarios("GAC") Then
        GACNumero = ClearNull(aplRST.Fields!var_texto)
    End If
        
    If sp_GetVarios(sKeyVer) Then
        If Not aplRST.EOF Then
            sModo = ClearNull(aplRST.Fields!var_numero)
        End If
    End If
    
    If sp_GetVarios(sKey) Then
        If Not aplRST.EOF Then
            cDBMSVersion = ClearNull(aplRST.Fields!var_texto)
            cAppVersion = "v" & App.Major & "." & App.Minor & "." & App.Revision
            lDBMS_VersionNumber = Versionado(aplRST.Fields!var_texto, sModo)
            Select Case sModo
                Case "1": lAPP_VersionNumber = (App.Major * 100000)
                Case "2": lAPP_VersionNumber = (App.Major * 100000) + (App.Minor * 100)
                Case Else
                    lAPP_VersionNumber = (App.Major * 100000) + (App.Minor * 100) + App.Revision
            End Select
        End If
    End If
    
    If lAPP_VersionNumber < lDBMS_VersionNumber Then
        MsgBox "Esta intentando ejecutar una versi�n incorrecta de la aplicaci�n (" & Choose(sModo, "MAJ", "MIN", "REV") & ")." & vbCrLf & vbCrLf & _
               "Versi�n del sistema: " & Trim(aplRST.Fields("var_texto")) & vbCrLf & _
               "Versi�n del usuario: " & Trim(cAppVersion) & vbCrLf & vbCrLf & _
               "A continuaci�n, la aplicaci�n se cerrar�." & vbCrLf & vbCrLf & vbCrLf & _
                "Importante:" & vbCrLf & vbCrLf & _
               "Si ejecuta el aplicativo desde Citrix o desde su equipo de manera" & vbCrLf & _
               "local, comun�quese con el GAC para actualizar el acceso al mismo." & vbCrLf & vbCrLf & _
               "(GAC: " & Trim(GACNumero) & ")", vbExclamation + vbOKOnly, "Versi�n incorrecta de CGM"
'               "Importante:" & vbCrLf & vbCrLf & _
'               "� REINICIE SU EQUIPO �" & vbCrLf & vbCrLf & _
'               "Esto puede deberse a que existe una nueva versi�n de la aplicaci�n. Reinicie el equipo" & vbCrLf & _
'               "para que pueda actualizarse automaticamente a la nueva versi�n del aplicativo. Si a�n" & vbCrLf & _
'               "continua con el mismo problema, luego de reiniciar, comun�quese directamente al GAC.", vbExclamation + vbOKOnly, "Versi�n incorrecta de CGM"
        ValidarVersionDelAplicativo = False
    Else
        'If CLng(aplRST.Fields!var_numero) = 0 Then
        If sModo = "0" Then
            MsgBox "Por cuestiones de desarrollo e implementaci�n de versiones, se ha deshabilitado el acceso al sistema." & vbCrLf & vbCrLf & _
                   "Vuelva a intentar el ingreso al sistema m�s tarde.", vbExclamation + vbOKOnly, "CGM offline"
            ValidarVersionDelAplicativo = False
        End If
    End If
End Function

Private Function Versionado(sVersion, Optional sModo) As Long
    Dim vElemento() As Integer
    Dim sAux As String
    Dim i As Integer
    Dim y As Long
    
    sAux = Mid(sVersion, 2)
    While Len(sAux) > 0
        If InStr(1, sAux, ".", vbTextCompare) > 0 Then
            ReDim Preserve vElemento(i): vElemento(i) = Mid(sAux, 1, InStr(1, sAux, ".", vbTextCompare) - 1)
            sAux = Mid(sAux, InStr(1, sAux, ".", vbTextCompare) + 1)
        Else
            ReDim Preserve vElemento(i): vElemento(i) = Mid(sAux, 1)
            sAux = ""
        End If
        i = i + 1
    Wend
    
    If Not IsMissing(sModo) Then
        Select Case sModo
            Case "1": y = vElemento(0) * 100000
            Case "2": y = vElemento(0) * 100000 + vElemento(1) * 100
            Case Else
                y = vElemento(0) * 100000 + vElemento(1) * 100 + vElemento(2)
        End Select
    Else
        y = vElemento(0) * 100000 + vElemento(1) * 100 + vElemento(2)
    End If
    Versionado = y
End Function

' ----------------------------------------------------------------------
' -- Funci�n de ajuste para usar con PathFindExtension
' ----------------------------------------------------------------------
Function GetExtension(ByVal sPath As String) As String
    Dim sTemp As String
    sTemp = pvGetStrFromPtrA(PathFindExtension(sPath))      'Given a path and filename, return only the filename extension.
    If Len(sTemp) Then GetExtension = ReplaceString(sTemp, Asc("."), vbNullString)
End Function
  
' ----------------------------------------------------------------------------------
' \\ -- Devuelve un string desde un puntero
' ----------------------------------------------------------------------------------
Private Function pvGetStrFromPtrA(ByVal lpszA As Long) As String
   pvGetStrFromPtrA = String$(lstrlenA(ByVal lpszA), 0)     'Given a pointer to a string, return the string
   Call lstrcpyA(ByVal pvGetStrFromPtrA, ByVal lpszA)
End Function

Public Function AbrirArchivo(Optional Titulo As String) As String
    On Error GoTo Errores
    With mdiPrincipal.CommonDialog
        .CancelError = True
        .DialogTitle = IIf(IsMissing(Titulo), "Abrir un archivo para lectura", Trim(Titulo))
        '.Filter = "Microsoft Excel (*.xls)|*.xls"
        .Filter = "Archivos de texto (*.txt)|*.txt"
        .FilterIndex = 1
        .InitDir = glWRKDIR
        .Flags = cdlOFNHideReadOnly + cdlOFNExplorer + cdlOFNNoLongNames
        .FileName = ""
        .ShowOpen
        AbrirArchivo = .FileName
    End With
Fin:
    Exit Function
Errores:
    Select Case Err.Number
        Case 32755      ' El usuario cancel� sin seleccionar archivo(s)
            GoTo Fin
        Case 20476      ' The FileName buffer is too small to store the selected file name(s). (Increase MaxFileSize)
            mdiPrincipal.CommonDialog.MaxFileSize = mdiPrincipal.CommonDialog.MaxFileSize + 256
            MsgBox "La cantidad de archivos seleccionados supera la capacidad actual del buffer." & vbCrLf & _
                   "El sistema expandi� el buffer autom�ticamente para poder continuar." & vbCrLf & _
                   "Por favor, seleccione nuevamente los archivos.", vbInformation + vbOKOnly, "Aviso para continuar"
            Resume
    End Select
End Function

'Sub FileToBlob(ByVal FName As String, fld As ADODB.Field, Optional Threshold As Long = 1048576)    ' del -002- a.
Sub FileToBlob(ByVal FName As String, fld As ADODB.Field, Optional Threshold As Long = ARCHIVOS_ADJUNTOS_MAXSIZE)   ' add -002- a.
' Assumes file exists
' Assumes calling routine does the UPDATE
' File cannot exceed approx. 2 Mb. in size
    Dim F As Long, Data() As Byte, FileSize As Long
    
    F = FreeFile
    Open FName For Binary As #F
        FileSize = LOF(F)
        Select Case fld.Type
            Case adLongVarBinary
                If FileSize > Threshold Then
                    ReadToBinary F, fld, FileSize
                Else
                    Data = InputB(FileSize, F)
                    fld.value = Data
                End If
            Case adLongVarChar, adLongVarWChar
                If FileSize > Threshold Then
                    ReadToText F, fld, FileSize
                Else
                    fld.value = Input(FileSize, F)
                End If
        End Select
    Close #F
End Sub

Sub BlobToFile(fld As ADODB.Field, ByVal FName As String, _
               Optional FieldSize As Long = -1, _
               Optional Threshold As Long = ARCHIVOS_ADJUNTOS_MAXSIZE)  ' upd -002- a. Se paso de 1 Mb. a 2 Mb.
' Assumes file does not exist. Data cannot exceed approx. 2 Mb. in size
Dim F As Long, bData() As Byte, sData As String
    F = FreeFile
    Open FName For Binary As #F
    Select Case fld.Type
        Case adLongVarBinary
            If FieldSize = -1 Then   ' BLOB field is of unknown size
                WriteFromUnsizedBinary F, fld
            Else                     ' BLOB field is of known size
                If FieldSize >= Threshold Then   ' very large actual data
                    WriteFromBinary F, fld, FieldSize
                Else                            ' smallish actual data
                    bData = fld.value
                    Put #F, , bData   ' PUT tacks on overhead if use fld.Value
                End If
            End If
        Case adLongVarChar, adLongVarWChar
            If FieldSize = -1 Then
                WriteFromUnsizedText F, fld
            Else
                If FieldSize > Threshold Then
                    WriteFromText F, fld, FieldSize
                Else
                    sData = fld.value
                    Put #F, , sData  ' PUT tacks on overhead if use fld.Value
                End If
            End If
        End Select
    Close #F
End Sub

Sub WriteFromBinary(ByVal F As Long, fld As ADODB.Field, ByVal FieldSize As Long)
    Dim Data() As Byte, BytesRead As Long
    Do While FieldSize <> BytesRead
        If FieldSize - BytesRead < BLOCK_SIZE Then
            Data = fld.GetChunk(FieldSize - BLOCK_SIZE)
            BytesRead = FieldSize
        Else
            Data = fld.GetChunk(BLOCK_SIZE)
            BytesRead = BytesRead + BLOCK_SIZE
        End If
        Put #F, , Data
    Loop
End Sub

Sub WriteFromUnsizedBinary(ByVal F As Long, fld As ADODB.Field)
    Dim Data() As Byte, Temp As Variant
    Do
        Temp = fld.GetChunk(BLOCK_SIZE)
        If IsNull(Temp) Then Exit Do
            Data = Temp
            Put #F, , Data
    Loop While LenB(Temp) = BLOCK_SIZE
End Sub

Sub WriteFromText(ByVal F As Long, fld As ADODB.Field, ByVal FieldSize As Long)
    Dim Data As String, CharsRead As Long
    Do While FieldSize <> CharsRead
        If FieldSize - CharsRead < BLOCK_SIZE Then
            Data = fld.GetChunk(FieldSize - BLOCK_SIZE)
            CharsRead = FieldSize
        Else
            Data = fld.GetChunk(BLOCK_SIZE)
            CharsRead = CharsRead + BLOCK_SIZE
        End If
        Put #F, , Data
    Loop
End Sub

Sub WriteFromUnsizedText(ByVal F As Long, fld As ADODB.Field)
    Dim Data As String, Temp As Variant
    Do
        Temp = fld.GetChunk(BLOCK_SIZE)
        If IsNull(Temp) Then Exit Do
            Data = Temp
            Put #F, , Data
    Loop While Len(Temp) = BLOCK_SIZE
End Sub

Sub ReadToBinary(ByVal F As Long, fld As ADODB.Field, ByVal FileSize As Long)
    Dim Data() As Byte, BytesRead As Long
    
    Do While FileSize <> BytesRead
        If FileSize - BytesRead < BLOCK_SIZE Then
            Data = InputB(FileSize - BytesRead, F)
            BytesRead = FileSize
        Else
            Data = InputB(BLOCK_SIZE, F)
            BytesRead = BytesRead + BLOCK_SIZE
        End If
        fld.AppendChunk Data
    Loop
End Sub

Sub ReadToText(ByVal F As Long, fld As ADODB.Field, ByVal FileSize As Long)
    Dim Data As String, CharsRead As Long
    
    Do While FileSize <> CharsRead
        If FileSize - CharsRead < BLOCK_SIZE Then
            Data = Input(FileSize - CharsRead, F)
            CharsRead = FileSize
        Else
            Data = Input(BLOCK_SIZE, F)
            CharsRead = CharsRead + BLOCK_SIZE
        End If
        fld.AppendChunk Data
    Loop
End Sub

Function PeticionAdjunto2File(pet_nrointerno, adj_tipo, adj_file, savePath) As Boolean
'    Dim objFileSystemObject As Scripting.FileSystemObject
'    Dim objFile As Scripting.File
'    Dim rsFileData As ADODB.Recordset
'    Dim strmFile As ADODB.Stream
'    Dim strSQL As String
'    Dim strPathToFile As String, strFileName As String
'    Static intID As Integer
'
'    strSQL = ""
'    strSQL = strSQL & "SELECT * FROM PeticionAdjunto WHERE "
'    strSQL = strSQL & "pet_nrointerno=" & ClearNull(pet_nrointerno) & " AND "
'    strSQL = strSQL & "adj_tipo='" & ClearNull(adj_tipo) & "' AND "
'    strSQL = strSQL & "adj_file='" & ClearNull(adj_file) & "' "
'
'    docRST.Open strSQL, aplCONN, adOpenForwardOnly, adLockReadOnly
'    If Not (docRST.EOF) Then
'        Set strmFile = New ADODB.Stream
'        With strmFile
'            .Type = adTypeBinary
'            .Open
'            .Write docRST!adj_objeto
'            .SaveToFile savePath & adj_file, adSaveCreateOverWrite
'            .Close
'        End With
'        docRST.Close
'        PeticionAdjunto2File = True
'    End If

''{ ESTO ES EL ORIGINAL
'    Dim txtCommand
'    Dim cn As ADODB.Connection
'    Set cn = New ADODB.Connection
'    cn.CursorLocation = adUseClient
'    On Error Resume Next
'    docRST.Close
'    Set docRST = Nothing
'    On Error GoTo Err_SP
'    cn.Open aplCONN.ConnectionString
'
'    txtCommand = ""
'    txtCommand = txtCommand & "SELECT * FROM PeticionAdjunto WHERE "
'    txtCommand = txtCommand & "pet_nrointerno=" & ClearNull(pet_nrointerno) & " AND "
'    txtCommand = txtCommand & "adj_tipo='" & ClearNull(adj_tipo) & "' AND "
'    txtCommand = txtCommand & "adj_file='" & ClearNull(adj_file) & "' "
'
'    docRST.Open txtCommand, cn, adOpenKeyset, adLockReadOnly
'    If Not (docRST.EOF) Then
'        Call BlobToFile(docRST!adj_objeto, savePath & adj_file, docRST!adj_objeto.ActualSize)
'        docRST.Close
'        PeticionAdjunto2File = True
'    End If
'    On Error GoTo 0
''}

'{ ESTE ES NUEVO: 08.06.2015
    Dim txtCommand
    Dim strmFile As ADODB.Stream
    On Error Resume Next
    docRST.Close
    Set docRST = Nothing
    On Error GoTo Err_SP

    txtCommand = ""
    txtCommand = txtCommand & "SELECT * FROM PeticionAdjunto WHERE "
    txtCommand = txtCommand & "pet_nrointerno=" & ClearNull(pet_nrointerno) & " AND "
    txtCommand = txtCommand & "adj_tipo='" & ClearNull(adj_tipo) & "' AND "
    txtCommand = txtCommand & "adj_file='" & ClearNull(adj_file) & "'"

    docRST.Open txtCommand, aplCONN, adOpenForwardOnly, adLockReadOnly
    If Not (docRST.EOF) Then
        Set strmFile = New ADODB.Stream
        With strmFile
            .Type = adTypeBinary
            .Open
            .Write docRST!adj_objeto
            .SaveToFile savePath & adj_file, adSaveCreateOverWrite
            .Close
        End With
        docRST.Close
        PeticionAdjunto2File = True
    End If
    On Error GoTo 0
'}
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    PeticionAdjunto2File = False
End Function

Function PeticionAdjunto2FileNew(pet_nrointerno, adj_tipo, ByRef adj_file, savePath) As Boolean
    Dim txtCommand
    Dim sFileName As String
    Dim strmFile As ADODB.Stream
    On Error Resume Next
    docRST.Close
    Set docRST = Nothing
    On Error GoTo Err_SP

    txtCommand = ""
    'txtCommand = txtCommand & "SELECT adj_objeto, pet_nroasignado FROM PeticionAdjunto INNER JOIN Peticion ON (PeticionAdjunto.pet_nrointerno = Peticion.pet_nrointerno) WHERE "
    txtCommand = txtCommand & "SELECT pet_nrointerno, adj_objeto FROM PeticionAdjunto WHERE "
    txtCommand = txtCommand & "pet_nrointerno=" & ClearNull(pet_nrointerno) & " AND "
    txtCommand = txtCommand & "adj_tipo='" & ClearNull(adj_tipo) & "' AND "
    txtCommand = txtCommand & "adj_file='" & ClearNull(adj_file) & "'"

    docRST.Open txtCommand, aplCONN, adOpenForwardOnly, adLockReadOnly
    If Not (docRST.EOF) Then
        Set strmFile = New ADODB.Stream
        With strmFile
            .Type = adTypeBinary
            .Open
            .Write docRST!adj_objeto
            'sFilename = docRST!pet_nrointerno & "_" & adj_tipo & "_" & adj_file
            'adj_file = docRST!pet_nroasignado & "_" & adj_tipo & "_" & adj_file    ' anterior
            adj_file = docRST!pet_nrointerno & "_" & adj_tipo & "_" & adj_file
            .SaveToFile savePath & adj_file, adSaveCreateOverWrite
            .Close
        End With
        docRST.Close
        PeticionAdjunto2FileNew = True
    End If
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    PeticionAdjunto2FileNew = False
End Function

Function ProyectoIDMAdjunto2File(ProjId, ProjSubId, ProjSubSId, adj_tipo, adj_file, savePath) As Boolean
    Dim txtCommand
    Dim strmFile As ADODB.Stream
    On Error Resume Next
    docRST.Close
    Set docRST = Nothing
    On Error GoTo Err_SP

    txtCommand = ""
    txtCommand = txtCommand & "SELECT * FROM ProyectoIDMAdjuntos WHERE "
    txtCommand = txtCommand & "ProjId=" & ClearNull(ProjId) & " AND "
    txtCommand = txtCommand & "ProjSubId=" & ClearNull(ProjSubId) & " AND "
    txtCommand = txtCommand & "ProjSubSId=" & ClearNull(ProjSubSId) & " AND "
    txtCommand = txtCommand & "adj_tipo='" & ClearNull(adj_tipo) & "' AND "
    txtCommand = txtCommand & "adj_file='" & ClearNull(adj_file) & "' "
    docRST.Open txtCommand, aplCONN, adOpenForwardOnly, adLockReadOnly
    If Not (docRST.EOF) Then
        Set strmFile = New ADODB.Stream
        With strmFile
            .Type = adTypeBinary
            .Open
            .Write docRST!adj_objeto
            .SaveToFile savePath & adj_file, adSaveCreateOverWrite
            .Close
        End With
        docRST.Close
        ProyectoIDMAdjunto2File = True
    End If
    On Error GoTo 0
'}
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    ProyectoIDMAdjunto2File = False

'    Dim txtCommand
'    Dim cn As ADODB.Connection
'    Set cn = New ADODB.Connection
'    cn.CursorLocation = adUseClient
'    On Error Resume Next
'    docRST.Close
'    Set docRST = Nothing
'    On Error GoTo Err_SP
'    cn.Open aplCONN.ConnectionString
'    txtCommand = ""
'    txtCommand = txtCommand & "SELECT * FROM ProyectoIDMAdjuntos WHERE "
'    txtCommand = txtCommand & "ProjId=" & ClearNull(ProjId) & " AND "
'    txtCommand = txtCommand & "ProjSubId=" & ClearNull(ProjSubId) & " AND "
'    txtCommand = txtCommand & "ProjSubSId=" & ClearNull(ProjSubSId) & " AND "
'    txtCommand = txtCommand & "adj_tipo='" & ClearNull(adj_tipo) & "' AND "
'    txtCommand = txtCommand & "adj_file='" & ClearNull(adj_file) & "' "
'    docRST.Open txtCommand, aplCONN, adOpenKeyset, adLockReadOnly
'    If Not (docRST.EOF) Then
'        BlobToFile docRST!adj_objeto, savePath & adj_file, docRST!adj_objeto.ActualSize
'        docRST.Close
'        ProyectoIDMAdjunto2File = True
'    End If
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    ProyectoIDMAdjunto2File = False
End Function

Public Function CargarArchivo(strTextFile) As Variant
    Dim F As Integer
    Dim i As Long
    Dim str_Linea As String
    Dim vector() As Long

    F = FreeFile

    Open strTextFile For Input As #F
    Do
        Line Input #F, str_Linea
        ReDim Preserve vector(i)
        vector(i) = CLng(str_Linea)
        i = i + 1
    Loop While Not EOF(F)
    Close #F
    
    CargarArchivo = vector
End Function

'{ add -015- a. Funci�n que comprueba si una direcci�n de email es v�lida
Public Function Validar_Email(ByVal email As String) As Boolean
    On Local Error GoTo Err_Sub
    Dim i As Integer, iLen As Integer, caracter As String
    Dim Pos As Integer, bp As Boolean, iPos As Integer, iPos2 As Integer
    email = Trim$(email)
    If email = vbNullString Then
        Exit Function
    End If
    email = LCase$(email)
    iLen = Len(email)
    For i = 1 To iLen
        caracter = Mid(email, i, 1)
        If (Not (caracter Like "[a-z]")) And (Not (caracter Like "[0-9]")) Then
            If InStr(1, "_-" & "." & "@", caracter) > 0 Then
                If bp = True Then
                   Exit Function
                Else
                    bp = True
                    If i = 1 Or i = iLen Then
                        Exit Function
                    End If
                    If caracter = "@" Then
                        If iPos = 0 Then
                            iPos = i
                        Else
                            Exit Function
                        End If
                    End If
                    If caracter = "." Then
                        iPos2 = i
                    End If
                End If
            Else
                Exit Function
            End If
        Else
            bp = False
        End If
    Next i
    If iPos = 0 Or iPos2 = 0 Then
        Exit Function
    End If
    If iPos2 < iPos Then
        Exit Function
    End If
    Validar_Email = True
Exit Function
Err_Sub:
    On Local Error Resume Next
    Validar_Email = False
End Function
'}

Public Function ValidarNumero(ByRef oControl As TextBox) As Boolean
    ValidarNumero = False
    If Len(oControl) > 0 Then
        If IsNumeric(oControl) Then
            If CDbl(oControl) > 0 Then
                oControl = Replace(oControl, ".", ",", 1, , vbTextCompare)
                oControl = Format(oControl, "########0.00")
                ValidarNumero = True
            End If
        End If
    End If
End Function

Public Function TimeToMillisecond() As String
    Dim sAns As String
    Dim typTime As SYSTEMTIME
    
    On Error Resume Next
    GetSystemTime typTime
    sAns = Hour(Now) & ":" & Minute(Now) & ":" & Second(Now) & ":" & typTime.wMilliseconds
    TimeToMillisecond = sAns
End Function

Public Sub SetValorCombo(ctrl As ComboBox, text As String, value As String)
    With ctrl
        .AddItem text & ESPACIOS & ESPACIOS & ESPACIOS & ESPACIOS & "||" & value
    End With
End Sub

Public Sub SetInitCombo(ctrl As ComboBox)
    ctrl.Clear
End Sub

