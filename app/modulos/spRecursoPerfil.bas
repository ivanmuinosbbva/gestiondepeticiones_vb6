Attribute VB_Name = "spRecursoPerfil"
' -001- a. FJS 15.01.2010 - Se agrega un nuevo SP para el informe de solicitudes por EMErgencia.

Option Explicit

Function sp_GetRecursoPerfil(cod_recurso, cod_perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoPerfil"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecursoPerfil = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRecursoPerfiles(cod_recurso, cod_perfiles) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoPerfiles"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_perfiles", adChar, adParamInput, 100, cod_perfiles)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecursoPerfiles = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoPerfiles = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRecursoPerfil(cod_recurso, cod_perfil, cod_nivel, cod_area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateRecursoPerfil"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
      .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
      Set aplRST = .Execute
    End With
    sp_UpdateRecursoPerfil = True
'    If Not IsEmpty(aplRST) And (Not aplRST.EOF) And aplRST(0).Name = "ErrCode" Then
'        MsgBox (AnalizarErrorSQL(aplRST, 30000))
'        sp_UpdateGrupoGES = False
'    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateRecursoPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function


Function sp_DeleteRecursoPerfil(cod_recurso, cod_perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteRecursoPerfil"
      .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      Set aplRST = .Execute
    End With
    sp_DeleteRecursoPerfil = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteRecursoPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'Function sp_GetPerfil(cod_perfil, parm1) As Boolean
Function sp_GetPerfil(cod_perfil, Optional Habilitado, Optional visible) As Boolean
    On Error Resume Next
    If aplRST.State <> adStateClosed Then aplRST.Close
    'aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    
    If IsMissing(Habilitado) Then Habilitado = Null
    If IsMissing(visible) Then visible = Null
    
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPerfil"
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@habilitado", adChar, adParamInput, 1, IIf(IsNull(Habilitado), Null, Habilitado))
      .Parameters.Append .CreateParameter("@visible", adChar, adParamInput, 1, IIf(IsNull(visible), Null, visible))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPerfil = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -001- a.
Public Function sp_GetRecursosPorAreaDyd(cod_perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetRecursosPorAreaDyd"
        .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecursosPorAreaDyd = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursosPorAreaDyd = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
