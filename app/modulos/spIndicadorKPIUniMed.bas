Attribute VB_Name = "spIndicadorKPIUniMed"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertIndicadorKPIUniMed(kpiid, unimedId) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_InsertIndicadorKPIUniMed"
		.Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , kpiid)
		.Parameters.Append .CreateParameter("@unimedId", adChar, adParamInput, 10, unimedId)
		Set aplRST = .Execute
	End With
	sp_InsertIndicadorKPIUniMed = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_InsertIndicadorKPIUniMed = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_UpdateIndicadorKPIUniMed(kpiid, unimedId) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_UpdateIndicadorKPIUniMed"
		.Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , kpiid)
		.Parameters.Append .CreateParameter("@unimedId", adChar, adParamInput, 10, unimedId)
		Set aplRST = .Execute
	End With
	sp_UpdateIndicadorKPIUniMed = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_UpdateIndicadorKPIUniMed = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_DeleteIndicadorKPIUniMed(kpiid, unimedId) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_DeleteIndicadorKPIUniMed"
		.Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , kpiid)
		.Parameters.Append .CreateParameter("@unimedId", adChar, adParamInput, 10, unimedId)
		Set aplRST = .Execute
	End With
	sp_DeleteIndicadorKPIUniMed = True
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_DeleteIndicadorKPIUniMed = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

Function sp_GetIndicadorKPIUniMed(kpiid, unimedId) as Boolean
	On Error Resume Next
	aplRST.Close
	Set aplRST = Nothing
	On Error GoTo Err_SP
	Dim objCommand As ADODB.Command
	Set objCommand = New ADODB.Command
	With objCommand
		.ActiveConnection = aplCONN
		.CommandType = adCmdStoredProc
		.CommandText = "sp_GetIndicadorKPIUniMed"
		.Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , kpiid)
		.Parameters.Append .CreateParameter("@unimedId", adChar, adParamInput, 10, unimedId)
		Set aplRST = .Execute
	End With
	If Not aplRST.EOF then
		sp_GetIndicadorKPIUniMed = True
	End If
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
	On Error GoTo 0
Exit Function
Err_SP:
	MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
	sp_GetIndicadorKPIUniMed = False
	Set objCommand.ActiveConnection = Nothing
	Set objCommand = Nothing
End Function

