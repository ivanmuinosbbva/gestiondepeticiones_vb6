Attribute VB_Name = "spHEDT012"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertHEDT012(tipo_id, subtipo_id, subtipo_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertHEDT012"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, , tipo_id)
                .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, , subtipo_id)
                .Parameters.Append .CreateParameter("@subtipo_nom", adChar, adParamInput, , subtipo_nom)
                Set aplRST = .Execute
        End With
        sp_InsertHEDT012 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertHEDT012 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteHEDT012(tipo_id, subtipo_id, subtipo_nom) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteHEDT012"
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, , tipo_id)
                .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, , subtipo_id)
                .Parameters.Append .CreateParameter("@subtipo_nom", adChar, adParamInput, , subtipo_nom)
                Set aplRST = .Execute
        End With
        sp_DeleteHEDT012 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteHEDT012 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetHEDT012(tipo_id, subtipo_id, subtipo_nom, rutina) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
            .ActiveConnection = aplCONN
            .CommandType = adCmdStoredProc
            .CommandText = "sp_GetHEDT012"
            .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, 1, tipo_id)
            .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, 1, subtipo_id)
            .Parameters.Append .CreateParameter("@subtipo_nom", adChar, adParamInput, 50, subtipo_nom)
            .Parameters.Append .CreateParameter("@rutina", adChar, adParamInput, 8, rutina)
            Set aplRST = .Execute
            If aplRST.EOF Then
                sp_GetHEDT012 = False
            Else
                sp_GetHEDT012 = True
            End If
        End With
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_GetHEDT012 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function
