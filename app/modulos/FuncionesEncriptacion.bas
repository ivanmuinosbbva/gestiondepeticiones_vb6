Attribute VB_Name = "FuncionesEncriptacion"
' M�dulo de c�digo basado en la librer�a Criptomagic desarrollada por la gente de Canales y Mercados.

Option Explicit

Private Const sClaveOriginal = "DE03DA7700DBA31001011FA5D00005FFFF060DAC"
Private Const sCompensador = "01143614011821101917000313141921212919332410191200312021281412031014312317030800"
Private bErrorProcesoDesencriptado As Boolean
Private sClave As String
Private bErrorProceso As Boolean

'Dim sClave As String
Public Function Encriptar(Texto As String) As String
    Dim nIndice As Integer
    Dim nDesvioTexto As Integer
    Dim nCaracterTexto As Integer
    Dim nCaracterClaveEncriptado As Integer
    Dim nCaracterNuevo As Integer
    Dim sClaveEncriptado As String
    Dim sTextoNuevo As String
    On Error GoTo Err_Encriptar
    Screen.MousePointer = vbHourglass
    
    sClave = "ESTAESLACADENADEENCRIPTADODELCRIPTOMAGIC"
    
    Texto = DepurarCadena(Texto)
    If CadenaValida(Texto) Then
        sTextoNuevo = ""
        sClaveEncriptado = sClave
        For nIndice = 0 To (Len(Texto) \ Len(sClave))
            sClaveEncriptado = sClaveEncriptado & sClave
        Next nIndice
        sClaveEncriptado = Left(sClaveEncriptado, Len(Texto))
        Randomize (Timer)
        nDesvioTexto = Int((65 - 32 + 1) * Rnd + 32)
        For nIndice = 1 To Len(Texto)
            nCaracterTexto = Asc(Mid(Texto, nIndice, 1))
            nCaracterClaveEncriptado = Asc(Mid(sClaveEncriptado, nIndice, 1))
            nCaracterNuevo = nCaracterTexto + nCaracterClaveEncriptado Xor nDesvioTexto
            sTextoNuevo = sTextoNuevo + Chr(nCaracterNuevo)
        Next nIndice
        Texto = (Ascii2Hex(sTextoNuevo, Len(sTextoNuevo))) & (Ascii2Hex(Trim(CStr(nDesvioTexto)), Len(Trim(CStr(nDesvioTexto)))))
    Else
        Err.Raise 9999
    End If
    Screen.MousePointer = vbNormal
    Encriptar = Texto
    On Error GoTo 0
Exit Function

Err_Encriptar:
    Screen.MousePointer = vbNormal
    Encriptar = "Error en el Proceso de Encriptado"
End Function

Public Function Desencriptar(ByVal Texto As String) As String
    Dim nIndice As Integer
    Dim sClaveDesencriptado As String
    Dim sDesvioTexto As String
    Dim nCaracterTexto As Integer
    Dim nCaracterClaveDesencriptado As Integer
    Dim nCaracterNuevo As Integer
    Dim sTextoNuevo As String
    On Error GoTo Err_Desencriptar
    Screen.MousePointer = vbHourglass
    
    sClave = "ESTAESLACADENADEENCRIPTADODELCRIPTOMAGIC"   ' Inicializaci�n de la clave
    
    bErrorProcesoDesencriptado = False
    ArmarClave
    If bErrorProcesoDesencriptado Then Err.Raise 9999
    sTextoNuevo = ""
    sClaveDesencriptado = sClave
    For nIndice = 0 To (Len(Texto) \ Len(sClave))
        sClaveDesencriptado = sClaveDesencriptado & sClave
    Next nIndice
    sDesvioTexto = Right(Texto, 4)
    sDesvioTexto = Hex2Ascii(sDesvioTexto, Len(sDesvioTexto))
    Texto = Left(Texto, Len(Texto) - 4)
    Texto = Hex2Ascii(Texto, Len(Texto))
    sClaveDesencriptado = Left(sClaveDesencriptado, Len(Texto))
    For nIndice = 1 To Len(Texto)
        nCaracterTexto = Asc(Mid(Texto, nIndice, 1))
        nCaracterClaveDesencriptado = Asc(Mid(sClaveDesencriptado, nIndice, 1))
        nCaracterNuevo = (nCaracterTexto Xor CInt(sDesvioTexto)) - nCaracterClaveDesencriptado
        sTextoNuevo = sTextoNuevo & Chr(nCaracterNuevo)
    Next nIndice
    Texto = sTextoNuevo
    If bErrorProcesoDesencriptado Then Err.Raise 9999
    Screen.MousePointer = vbNormal
    Desencriptar = Texto
    On Error GoTo 0
Exit Function

Err_Desencriptar:
    Screen.MousePointer = vbNormal
    bErrorProcesoDesencriptado = True
    Desencriptar = "Error en el Proceso de Desencriptado"
End Function

Private Function DepurarCadena(sTexto As String) As String
    Dim nIndice As Integer
    Dim nCaracter As Integer
    For nIndice = 1 To Len(sTexto)
        nCaracter = Asc(Mid(sTexto, nIndice, 1))
        Select Case nCaracter
            Case 225            '� => a
                nCaracter = 97
            Case 233            '� => e
                nCaracter = 101
            Case 237            '� => i
                nCaracter = 105
            Case 243            '� => o
                nCaracter = 111
            Case 250            '� => u
                nCaracter = 117
            Case 241            '� => n
                nCaracter = 110
            Case 209            '� => N
                nCaracter = 78
            Case Else
        End Select
        DepurarCadena = DepurarCadena & Chr(nCaracter)
    Next nIndice
End Function

Private Function CadenaValida(sTexto As String) As Boolean
    Dim nIndice As Integer
    CadenaValida = True
    For nIndice = 1 To Len(sTexto)
        If Asc(Mid(sTexto, nIndice, 1)) < 32 Or Asc(Mid(sTexto, nIndice, 1)) > 127 Then
            If Asc(Mid(sTexto, nIndice, 1)) <> 13 And Asc(Mid(sTexto, nIndice, 1)) <> 10 Then
                CadenaValida = False
                Exit Function
            End If
        End If
    Next nIndice
End Function

Private Function Ascii2Hex(sBuffer As String, nLen_Buffer As Integer) As String
    Dim sBuffer_TMP As String
    Dim sStrHex As String
    Dim nI As Integer
    Dim nValor As Integer
    On Error GoTo Err_Ascii2Hex
    sBuffer_TMP = ""
    For nI = 1 To nLen_Buffer
        nValor = Asc(Mid$(sBuffer, nI, 1))
        sStrHex = Hex$(nValor)
        If (Len(sStrHex) = 1) Then
            sBuffer_TMP = sBuffer_TMP & "0"
        End If
        sBuffer_TMP = sBuffer_TMP & sStrHex
    Next nI
    Ascii2Hex = sBuffer_TMP
    On Error GoTo 0
Exit Function

Err_Ascii2Hex:
    bErrorProcesoDesencriptado = True
    Resume Next
End Function

Private Function Hex2Ascii(sBuffer, nLen_Buffer As Integer) As String
    Dim sBuffer_TMP As String
    Dim sStrHex As String
    Dim nI As Integer
    Dim nValor As Integer
    On Error GoTo Err_Hex2Ascii
    sBuffer_TMP = ""
    For nI = 1 To nLen_Buffer Step 2
        sStrHex = "&H" & Mid$(sBuffer, nI, 2)
        nValor = CInt(sStrHex)
        sBuffer_TMP = sBuffer_TMP & Chr$(nValor)
    Next nI
    Hex2Ascii = sBuffer_TMP
    On Error GoTo 0
Exit Function

Err_Hex2Ascii:
    bErrorProcesoDesencriptado = True
    Resume Next
End Function

Private Sub ArmarClave()
    Dim nIndiceClave As Integer
    Dim nIndiceCompensador As Integer
    On Error GoTo Err_ArmarClave
    
    sClave = "ESTAESLACADENADEENCRIPTADODELCRIPTOMAGIC"   ' Inicializaci�n de la clave
    
    nIndiceCompensador = 1
    For nIndiceClave = 1 To Len(sClaveOriginal)
        sClave = sClave & Chr(Asc(Mid(sClaveOriginal, nIndiceClave, 1)) + Val(Mid(sCompensador, nIndiceCompensador, 2)))
        nIndiceCompensador = nIndiceCompensador + 2
    Next nIndiceClave
    On Error GoTo 0
Exit Sub

Err_ArmarClave:
    bErrorProcesoDesencriptado = True
    Resume Next
End Sub
