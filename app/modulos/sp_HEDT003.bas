Attribute VB_Name = "spHEDT003"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertHEDT003(dsn_id, cpy_id, cpo_id, cpo_nrobyte, cpo_canbyte, cpo_type, cpo_decimals, cpo_dsc, cpo_nomrut) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertHEDT003"
                .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
                .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
                .Parameters.Append .CreateParameter("@cpo_id", adChar, adParamInput, 18, cpo_id)
                .Parameters.Append .CreateParameter("@cpo_nrobyte", adChar, adParamInput, 4, cpo_nrobyte)
                .Parameters.Append .CreateParameter("@cpo_canbyte", adChar, adParamInput, 4, cpo_canbyte)
                .Parameters.Append .CreateParameter("@cpo_type", adChar, adParamInput, 1, cpo_type)
                .Parameters.Append .CreateParameter("@cpo_decimals", adChar, adParamInput, 4, cpo_decimals)
                .Parameters.Append .CreateParameter("@cpo_dsc", adChar, adParamInput, 20, cpo_dsc)
                .Parameters.Append .CreateParameter("@cpo_nomrut", adChar, adParamInput, 8, cpo_nomrut)
                Set aplRST = .Execute
        End With
        sp_InsertHEDT003 = True
        If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
        End If
        If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
            If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
                GoTo Err_SP
            End If
        End If
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_InsertHEDT003 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateHEDT003(dsn_id, cpy_id, cpo_id, cpo_nrobyte, cpo_canbyte, cpo_type, cpo_decimals, cpo_dsc, cpo_nomrut) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateHEDT003"
                .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
                .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
                .Parameters.Append .CreateParameter("@cpo_id", adChar, adParamInput, 18, cpo_id)
                .Parameters.Append .CreateParameter("@cpo_nrobyte", adChar, adParamInput, 4, cpo_nrobyte)
                .Parameters.Append .CreateParameter("@cpo_canbyte", adChar, adParamInput, 4, cpo_canbyte)
                .Parameters.Append .CreateParameter("@cpo_type", adChar, adParamInput, 1, cpo_type)
                .Parameters.Append .CreateParameter("@cpo_decimals", adChar, adParamInput, 4, cpo_decimals)
                .Parameters.Append .CreateParameter("@cpo_dsc", adChar, adParamInput, 20, cpo_dsc)
                .Parameters.Append .CreateParameter("@cpo_nomrut", adChar, adParamInput, 8, cpo_nomrut)
                Set aplRST = .Execute
        End With
        sp_UpdateHEDT003 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateHEDT003 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteHEDT003(dsn_id, cpy_id, cpo_id) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteHEDT003"
                .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
                .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
                .Parameters.Append .CreateParameter("@cpo_id", adChar, adParamInput, 18, cpo_id)
                Set aplRST = .Execute
        End With
        sp_DeleteHEDT003 = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteHEDT003 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetHEDT003(dsn_id, cpy_id, cpo_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT003"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
        .Parameters.Append .CreateParameter("@cpo_id", adChar, adParamInput, 18, cpo_id)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT003 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHEDT003 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHEDT003a(dsn_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT003a"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT003a = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHEDT003a = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
