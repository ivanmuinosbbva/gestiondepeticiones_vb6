Attribute VB_Name = "FuncionesSQL"
' -001- a. FJS 30.06.2016 - Nuevo: se muestran todos los mensajes de la colecci�n Errors del objecto Connection.

Option Explicit

Global Const FMT_2DEC = 2
Global Const FMT_IVA = 3
Global Const FMT_4DEC = 4

Function AnalizarErrorSQL(rsERR As ADODB.Recordset, lError As Double, Optional adoCONN As ADODB.Connection) As String
    Dim i As Integer
    
    If (Not IsEmpty(adoCONN)) And Not (adoCONN Is Nothing) Then
        If adoCONN.Errors.Count > 0 Then
'            '{ add -001- a.
'            For i = 0 To adoCONN.Errors.Count - 1
'                AnalizarErrorSQL = AnalizarErrorSQL & adoCONN.Errors(i).NativeError & " - " & adoCONN.Errors(i).DESCRIPTION
'            Next i
'            '}
            AnalizarErrorSQL = UCase(adoCONN.Errors(0).NativeError & " - " & ExtraeMensajeErrorSQL(adoCONN.Errors(0).DESCRIPTION))     ' del -001- a.
            Exit Function
        Else
            AnalizarErrorSQL = Trim(Err.Source) & " - " & Trim(Err.DESCRIPTION)
        End If
    End If
    If ((Not IsEmpty(rsERR)) And Not (rsERR Is Nothing)) Then
        If (Not rsERR.State = adStateClosed) Then
            If ((Not rsERR.EOF) And (rsERR(0).name = "ErrCode")) Then
                   AnalizarErrorSQL = "Error " & rsERR!ErrCode & " : " & rsERR!ErrDesc
            End If
        End If
    Else
        AnalizarErrorSQL = Err.Number & " : " & Err.DESCRIPTION
    End If
    On Error GoTo 0
    AnalizarErrorSQL = UCase(AnalizarErrorSQL)
End Function

Function AnalizarErrorADO(adoCONN As ADODB.Connection) As String
    AnalizarErrorADO = UCase(adoCONN.Errors(0).NativeError & " - " & ExtraeMensajeErrorSQL(adoCONN.Errors(0).DESCRIPTION))
End Function

Function ExtraeMensajeErrorSQL(mensaje As String) As String
    Dim Pos As Integer
    Pos = InStr(1, mensaje, "]")
    Do While Pos > 0
        mensaje = Right(mensaje, Len(mensaje) - Pos)
        Pos = InStr(1, mensaje, "]")
    Loop
    ExtraeMensajeErrorSQL = mensaje
End Function

Function syb2txt(valor, Optional sFmt) As String
    Dim sValor As String
    If IsMissing(sFmt) Then
        sFmt = FMT_2DEC
    End If
    sValor = ReplaceComma(ClearNull("" & valor))
    Select Case sFmt
    Case FMT_2DEC
        sValor = ReplaceComma(Format(Val(sValor), "########0.00"))
    Case FMT_IVA
        sValor = ReplaceComma(Format(Val(sValor), "#0.00"))
    Case FMT_4DEC
        sValor = ReplaceComma(Format(Val(sValor), "########0.0000"))
    End Select
    syb2txt = sValor
End Function

Function syb2val(valor, Optional sFmt) As String
    If IsMissing(sFmt) Then
        sFmt = FMT_2DEC
    End If
    syb2val = Val(syb2txt(valor, sFmt))
End Function
