Attribute VB_Name = "API_Functions"
Option Explicit

Dim vComponentes(28) As udtComponentes

Private Type udtComponentes
    compNombre As String
    compVersion As String
    compDescription As String
End Type

Private Type VS_FIXEDFILEINFO
    dwSignature As Long
    dwStrucVersionl As Integer
    dwStrucVersionh As Integer
    dwFileVersionMSl As Integer
    dwFileVersionMSh As Integer
    dwFileVersionLSl As Integer
    dwFileVersionLSh As Integer
    dwProductVersionMSl As Integer
    dwProductVersionMSh As Integer
    dwProductVersionLSl As Integer
    dwProductVersionLSh As Integer
    dwFileFlagsMask As Long
    dwFileFlags As Long
    dwFileOS As Long
    dwFileType As Long
    dwFileSubtype As Long
    dwFileDateMS As Long
    dwFileDateLS As Long
End Type

Private Declare Function GetFileVersionInfo Lib "Version.dll" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwhandle As Long, ByVal dwlen As Long, lpData As Any) As Long
Private Declare Function GetFileVersionInfoSize Lib "Version.dll" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long
Private Declare Function VerQueryValue Lib "Version.dll" Alias "VerQueryValueA" (pBlock As Any, ByVal lpSubBlock As String, lplpBuffer As Any, puLen As Long) As Long
Private Declare Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (dest As Any, ByVal Source As Long, ByVal length As Long)

Public Function ValidarComponentes() As Boolean
    Dim i As Integer
    Dim cMensajes As String
    
    cMensajes = "Los siguientes componentes tienen versi�n incorrecta: " & vbCrLf & vbCrLf
    
    ValidarComponentes = True
    Inicializar
    For i = 0 To UBound(vComponentes)
        If Not VerVersion(vComponentes(i).compNombre) = vComponentes(i).compVersion Then
            cMensajes = cMensajes & " - " & vComponentes(i).compNombre & " de " & vComponentes(i).compDescription & " (" & vComponentes(i).compVersion & ")" & vbCrLf
            ValidarComponentes = False
        End If
    Next i
    If Not ValidarComponentes Then
        MsgBox cMensajes, vbCritical + vbOKOnly, "Incompatibilidad de los componentes registrados"
    End If
End Function

Private Function VerVersion(Fichero As String) As String
    Dim lBufferLen As Long, udtVerBuffer As VS_FIXEDFILEINFO
    Dim sBuffer() As Byte, rc As Long, lAux As Long
    Dim lVerPointer As Long, lVerbufferLen As Long
   
    VerVersion = ""
    'obtenemos el tama�o de la informaci�n de versi�n
    lBufferLen = GetFileVersionInfoSize(Fichero, lAux)
    'el fichero no contiene informaci�n de versi�n
    If lBufferLen < 1 Then Exit Function
    'guardamos la informaci�n en la estructura udtVerBuffer
    ReDim sBuffer(lBufferLen)
    rc = GetFileVersionInfo(Fichero, 0&, lBufferLen, sBuffer(0))
    rc = VerQueryValue(sBuffer(0), "\", lVerPointer, lVerbufferLen)
    MoveMemory udtVerBuffer, lVerPointer, Len(udtVerBuffer)
    'Formamos la versi�n del fichero
    VerVersion = Format$(udtVerBuffer.dwFileVersionMSh) & "." & Format$(udtVerBuffer.dwFileVersionMSl) & "." & Format$(udtVerBuffer.dwFileVersionLSh) & "." & Format$(udtVerBuffer.dwFileVersionLSl)
End Function

Private Sub Inicializar()
    vComponentes(0).compNombre = "c:\windows\system32\msvbvm50.dll"
    vComponentes(0).compVersion = "5.2.82.44"
    vComponentes(0).compDescription = "Visual Basic Virtual Machine"
    
    vComponentes(1).compNombre = "c:\windows\system32\ADO_CONNECTION.OCX"
    vComponentes(1).compVersion = "2.1.0.3"
    vComponentes(1).compDescription = "Control de usuario para conexi�n a bases de datos"
    
    vComponentes(2).compNombre = "c:\windows\system32\kernel32.dll"
    vComponentes(2).compVersion = "5.1.2600.3119"
    vComponentes(2).compDescription = "DLL de cliente API BASE de Windows NT"
    
    vComponentes(3).compNombre = "c:\windows\system32\user32.dll"
    vComponentes(3).compVersion = "5.1.2600.3099"
    vComponentes(3).compDescription = "DLL de cliente USER API de Windows XP"
    
    vComponentes(4).compNombre = "c:\windows\system32\gdi32.dll"
    vComponentes(4).compVersion = "5.1.2600.3316"
    vComponentes(4).compDescription = "GDI Client DLL"
    
    vComponentes(5).compNombre = "c:\windows\system32\advapi32.dll"
    vComponentes(5).compVersion = "5.1.2600.2180"
    vComponentes(5).compDescription = "API base de Windows 32 avanzado"
    
    vComponentes(6).compNombre = "c:\windows\system32\ole32.dll"
    vComponentes(6).compVersion = "5.1.2600.2726"
    vComponentes(6).compDescription = "Microsoft OLE para Windows"
    
    vComponentes(7).compNombre = "c:\windows\system32\oleaut32.dll"
    vComponentes(7).compVersion = "5.1.2600.3266"
    vComponentes(7).compDescription = "Microsoft OLE 2.20 for Windows NT(TM) and Windows 95(TM) Operating Systems"
    
    vComponentes(8).compNombre = "c:\windows\system32\STDOLE2.TLB"
    vComponentes(8).compVersion = "3.50.5014.0"
    vComponentes(8).compDescription = "Microsoft OLE 3.50 for Windows NT(TM) and Windows 95(TM) Operating Systems"
    
    vComponentes(9).compNombre = "c:\windows\system32\OLEPRO32.DLL"
    vComponentes(9).compVersion = "5.1.2600.2180"
    vComponentes(9).compDescription = "Microsoft (R) OLE Property Support DLL"
    
    vComponentes(10).compNombre = "c:\windows\system32\ASYCFILT.DLL"
    vComponentes(10).compVersion = "5.1.2600.2180"
    vComponentes(10).compDescription = "Microsoft OLE 2.20 for Windows NT(TM) and Windows 95(TM) Operating Systems"
    
    vComponentes(11).compNombre = "c:\windows\system32\CTL3D32.DLL"
    vComponentes(11).compVersion = "2.31.0.0"
    vComponentes(11).compDescription = "Ctl3D 3D Windows Controls"
    
    vComponentes(12).compNombre = "c:\windows\system32\COMCAT.DLL"
    vComponentes(12).compVersion = "5.0.2600.1"
    vComponentes(12).compDescription = "Microsoft Component Category Manager Library"
    
    vComponentes(13).compNombre = "c:\windows\system32\COMCTL32.OCX"
    vComponentes(13).compVersion = "6.0.81.5"
    vComponentes(13).compDescription = "Windows Common Controls ActiveX Control DLL"
    
    vComponentes(14).compNombre = "c:\windows\system32\MSFLXGRD.OCX"
    vComponentes(14).compVersion = "6.1.97.82"
    vComponentes(14).compDescription = "MSFlexGrid"
    
    vComponentes(15).compNombre = "c:\windows\system32\AT_MASKTEXT.OCX"
    vComponentes(15).compVersion = "5.0.0.0"
    vComponentes(15).compDescription = "MaskText Control"
    
    vComponentes(16).compNombre = "c:\windows\system32\CRYSTL32.OCX"
    vComponentes(16).compVersion = "4.6.37.14"
    vComponentes(16).compDescription = "Crystal Reports OLE Control DLL"
    
    vComponentes(17).compNombre = "c:\windows\system32\CRPE32.DLL"
    vComponentes(17).compVersion = "4.6.1.112"
    vComponentes(17).compDescription = "Crystal Reports Print Engine"
    
    vComponentes(18).compNombre = "c:\windows\system32\IMPLODE.DLL"
    vComponentes(18).compVersion = "1.0.0.1"
    vComponentes(18).compDescription = "Implode Application"
    
    vComponentes(19).compNombre = "c:\windows\system32\PG32.DLL"
    vComponentes(19).compVersion = "1.0.0.11"
    vComponentes(19).compDescription = "Presentations Graphics DLL - Professional Version"
    
    vComponentes(20).compNombre = "c:\windows\system32\CO2C40EN.DLL"
    vComponentes(20).compVersion = "4.6.1.106"
    vComponentes(20).compDescription = "Crystal Reports OLE 2 UI"
    
    vComponentes(21).compNombre = "c:\windows\system32\MSVCRT20.DLL"
    vComponentes(21).compVersion = "2.12.0.0"
    vComponentes(21).compDescription = "Microsoft� C Runtime Library"
    
    vComponentes(22).compNombre = "C:\windows\Crystal\CRXLAT32.DLL"
    vComponentes(22).compVersion = "4.6.1.106"
    vComponentes(22).compDescription = "Crystal Reports ToWords"
    
    vComponentes(23).compNombre = "c:\windows\system32\P2BBND.DLL"
    vComponentes(23).compVersion = "4.6.1.106"
    vComponentes(23).compDescription = "Crystal Reports Bound Control Database"
    
    vComponentes(24).compNombre = "c:\windows\system32\P2SODBC.DLL"
    vComponentes(24).compVersion = "4.6.1.106"
    vComponentes(24).compDescription = "Crystal Reports ODBC Server"
    
    vComponentes(25).compNombre = "C:\windows\Crystal\U2DMAPI.DLL"
    'vComponentes(25).compVersion = "5.0.0.0"
    vComponentes(25).compVersion = "6.0.0.0"
    vComponentes(25).compDescription = "MAPI Export Destination DLL for Crystal Reports"
    
    vComponentes(26).compNombre = "c:\archivos de programa\archivos comunes\system\ado\MSADO15.DLL"
    'vComponentes(26).compVersion = "2.81.1128.0"
    vComponentes(26).compVersion = "3.81.1128.0"
    vComponentes(26).compDescription = "Microsoft Data Access - ActiveX Data Objects"
    
    vComponentes(27).compNombre = "c:\windows\system32\odbccp32.dll"
    'vComponentes(27).compVersion = "3.525.1117.0"
    vComponentes(27).compVersion = "4.525.1117.0"
    vComponentes(27).compDescription = "Microsoft Data Access - ODBC Installer"
End Sub

