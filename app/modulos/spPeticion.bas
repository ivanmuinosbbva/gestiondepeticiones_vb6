Attribute VB_Name = "spPeticion"
' -001- a. FJS 27.06.2007 - Se agregan los par�metros necesarios para contemplar los nuevos campos (Clase de petici�n e indicador si corresponde impacto tecnol�gico).
' -001- b. FJS 18.07.2007 - Se agrega un nuevo par�metro para inicializar en la tabla el campo pet_sox001 que indica si se realizan o no los nuevos controles de SOX para documentos adjuntos y conformes de usuario.
' -002- a. FJS 23.08.2007 - Se agrega soporte para llamar al SP sp_GetUnaPeticionTodas (que devuelve todos los campos de la tabla)
' -003- a. FJS 10.09.2008 - Se agrega el par�metro de Regulatorio.
' -003- b. FJS 18.09.2008 - Se agrega la parte de proyectos IDM.
' -004- a. FJS 17.10.2008 - Se modifica el SP para guardar los datos completos de los textos modificados. [ELIMINADO]
' -005- a. FJS 25.03.2009 - Se agregan dos nuevos SP para el manejo de peticiones hist�ricas.
' -006- a. FJS 08.04.2009 - Replanificaciones.
' -007- a. FJS 03.11.2009 - Se agrega llamada a SP para devolver peticiones que contengan un grupo espec�fico.
' -008- a. FJS 17.03.2010 - Nuevo SP para listar los controles en una petici�n.
' -009- a. FJS 28.01.2016 - El dato de fecha requerida no es necesario. Si se recibe en NULO se envia NULO.
' -010- a. FJS 29.01.2016 - Se agrega funcionalidad para la nueva gerencia de BPE.
' -011- a. FJS 13.05.2016 - Nuevo: se agrega la versi�n del SP para usar n�meros reales.
' -012- GMT    23.10.2019 - Se agrega nuevo sp, para realizar update a la priorizacion, para categorizar la pet como PRI

Option Explicit

Function sp_GetUnaPeticion(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetUnaPeticion"
        If IsNull(pet_nrointerno) Then
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , Null)
        Else
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        End If
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPeticion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPeticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
' RP Optimizacion - ini
Function sp_GetUnaPeticionSinCategoria(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetUnaPeticionSinCategoria"
        If IsNull(pet_nrointerno) Then
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , Null)
        Else
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        End If
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPeticionSinCategoria = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPeticionSinCategoria = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'RP Optimizacion - fin

Function sp_GetUnaPeticionXt(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnaPeticionXt"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPeticionXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPeticionXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -005- a.
Function sp_GetUnahPeticionXt(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnahPeticionXt"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnahPeticionXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnahPeticionXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Function sp_GetUnaPeticionAsig(pet_nroasignado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnaPeticionAsig"
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , CLng(Val(pet_nroasignado)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPeticionAsig = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPeticionAsig = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetUnaPeticionAsigSinProy(pet_nroasignado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnaPeticionAsigSinProy"
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , CLng(Val(pet_nroasignado)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPeticionAsigSinProy = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPeticionAsigSinProy = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionAnexadas(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionAnexadas"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionAnexadas = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionAnexadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetUnaPeticionTitulo(Titulo, auxEstado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnaPeticionTitulo"
      .Parameters.Append .CreateParameter("@titulo", adChar, adParamInput, 50, ClearNull(Titulo))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, auxEstado)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPeticionTitulo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPeticionTitulo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -007- a.
Function sp_GetUnaPetTituloGrp(pet_nrointerno, Titulo, auxEstado, cod_sector, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnaPetTituloGrp"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
      .Parameters.Append .CreateParameter("@titulo", adChar, adParamInput, 50, ClearNull(Titulo))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, auxEstado)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPetTituloGrp = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPetTituloGrp = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Function sp_GetUnaPeticionTituloSinProy(Titulo, auxEstado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnaPeticionTituloSinProy"
      .Parameters.Append .CreateParameter("@titulo", adChar, adParamInput, 50, ClearNull(Titulo))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, auxEstado)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnaPeticionTituloSinProy = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnaPeticionTituloSinProy = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionArea(pet_nroasignado, modo_recurso, cod_nivel, cod_area, cod_estado, auxAgrup, flgAnexadas, PrioEjecuc, Optional cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    If IsMissing(flgAnexadas) Then
        flgAnexadas = "N"
    End If
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionArea"
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , CLng(Val(pet_nroasignado)))
      .Parameters.Append .CreateParameter("@modo_recurso", adChar, adParamInput, 4, modo_recurso)
      .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
      .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, cod_estado)
      .Parameters.Append .CreateParameter("@anx_especial", adChar, adParamInput, 1, flgAnexadas)
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
      .Parameters.Append .CreateParameter("@prio_ejecuc", adInteger, adParamInput, , CLng(Val(PrioEjecuc)))
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, IIf(cod_recurso = "" Or IsNull(cod_recurso), Null, cod_recurso))    ' add -012- a.
      Set aplRST = .Execute
    End With
    'Debug.Print "sp_GetPeticionArea" & " " & pet_nroasignado & ", '" & Trim(cod_bpar) & "', '" & Trim(cod_estado) & "', '" & CLng(Val(auxAgrup)) & "', " & CLng(Val(PrioEjecuc))
    If Not (aplRST.EOF) Then
        sp_GetPeticionArea = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionArea = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionAreaBP(pet_nroasignado, cod_bpar, cod_estado, auxAgrup, flgAnexadas, PrioEjecuc, Gestion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    If IsMissing(flgAnexadas) Then
        flgAnexadas = "N"
    End If
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionAreaBP"
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , CLng(Val(pet_nroasignado)))
      .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, cod_estado)
      .Parameters.Append .CreateParameter("@anx_especial", adChar, adParamInput, 1, flgAnexadas)
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
      .Parameters.Append .CreateParameter("@prio_ejecuc", adInteger, adParamInput, , CLng(Val(PrioEjecuc)))
      .Parameters.Append .CreateParameter("@gestion", adChar, adParamInput, 8, IIf(IsNull(Gestion) Or Gestion = "NULL", Null, Gestion))
      Set aplRST = .Execute
    End With
    Debug.Print "sp_GetPeticionAreaBP" & " " & pet_nroasignado & ", '" & Trim(cod_bpar) & "', '" & Trim(cod_estado) & "', '" & CLng(Val(auxAgrup)) & "', " & CLng(Val(PrioEjecuc))
    If Not (aplRST.EOF) Then
        sp_GetPeticionAreaBP = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionAreaBP = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionAreaBPE(pet_nroasignado, cod_bpar, cod_estado, auxAgrup, flgAnexadas, PrioEjecuc, Gestion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    If IsMissing(flgAnexadas) Then
        flgAnexadas = "N"
    End If
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionAreaBPE"
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , CLng(Val(pet_nroasignado)))
      .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, cod_estado)
      .Parameters.Append .CreateParameter("@anx_especial", adChar, adParamInput, 1, flgAnexadas)
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
      .Parameters.Append .CreateParameter("@prio_ejecuc", adInteger, adParamInput, , CLng(Val(PrioEjecuc)))
      .Parameters.Append .CreateParameter("@gestion", adChar, adParamInput, 8, IIf(IsNull(Gestion) Or Gestion = "NULL", Null, Gestion))
      Set aplRST = .Execute
    End With
    'Debug.Print "sp_GetPeticionAreaBPE" & " " & pet_nroasignado & ", '" & Trim(cod_bpar) & "', '" & Trim(cod_estado) & "', '" & CLng(Val(auxAgrup)) & "', " & CLng(Val(PrioEjecuc))
    If Not (aplRST.EOF) Then
        sp_GetPeticionAreaBPE = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionAreaBPE = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionAreaBPE1(pet_nroasignado, cod_bpar, cod_estado, auxAgrup, flgAnexadas, PrioEjecuc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    If IsMissing(flgAnexadas) Then
        flgAnexadas = "N"
    End If
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionAreaBPE1"
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , CLng(Val(pet_nroasignado)))
      .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, IIf(IsNull(cod_bpar), Null, cod_bpar))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, cod_estado)
      .Parameters.Append .CreateParameter("@anx_especial", adChar, adParamInput, 1, flgAnexadas)
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
      .Parameters.Append .CreateParameter("@prio_ejecuc", adInteger, adParamInput, , CLng(Val(PrioEjecuc)))
      Set aplRST = .Execute
    End With
    Debug.Print "sp_GetPeticionAreaBPE" & " " & pet_nroasignado & ", '" & Trim(cod_bpar) & "', '" & Trim(cod_estado) & "', '" & CLng(Val(auxAgrup)) & "', " & CLng(Val(PrioEjecuc))
    If Not (aplRST.EOF) Then
        sp_GetPeticionAreaBPE1 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionAreaBPE1 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionAreaRec(cod_area, cod_estado, cod_recurso, PrioEjecuc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionAreaRec"
      .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, cod_estado)
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 250, cod_recurso)
      .Parameters.Append .CreateParameter("@prio_ejecuc", adInteger, adParamInput, , CLng(Val(PrioEjecuc)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionAreaRec = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionAreaRec = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePetField(pet_nrointerno, campo, valortxt, valordate, valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePetField"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
      .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 120, valortxt)
      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
      .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum), 0, CLng(Val("" & valorNum))))
      Set aplRST = .Execute
    End With
    sp_UpdatePetField = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    'MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    If InStr(1, Err.Description, "Attempt to insert NULL value into column 'cod_situacion', table 'GesPet.dbo.Mensajes'", vbTextCompare) = 0 Then
        'Resume Next
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Else
        Debug.Print "Error raro! (" & pet_nrointerno & ")"
    End If
    sp_UpdatePetField = False
End Function

'{ add -011- a.
Function sp_UpdatePetField2(pet_nrointerno, campo, valortxt, valordate, valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePetField2"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, ClearNull(campo))
      .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 120, valortxt)
      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
      .Parameters.Append .CreateParameter("@valornum", adDouble, adParamInput, , IIf(IsNull(valorNum), 0, valorNum))
      Set aplRST = .Execute
    End With
    sp_UpdatePetField2 = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePetField2 = False
End Function
'}

'{ add -xxx- a.
Function sp_UpdatePetSubField(pet_nrointerno, Grupo, campo, valortxt, valordate, valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePetSubField"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@grupo", adChar, adParamInput, 8, Grupo)
      .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
      .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
      .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum), 0, CLng(Val("" & valorNum))))
      Set aplRST = .Execute
    End With
    sp_UpdatePetSubField = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePetSubField = False
End Function
'}

Function sp_UpdatePeticion(pet_nrointerno, pet_nroasignado, _
                        Titulo, cod_tipo_peticion, _
                        prioridad, corp_local, _
                        cod_orientacion, importancia_cod, _
                        importancia_prf, importancia_usr, _
                        pet_nroanexada, prj_nrointerno, _
                        fe_pedido, _
                        fe_requerida, _
                        fe_comite, _
                        fe_ini_plan, fe_fin_plan, _
                        fe_ini_real, fe_fin_real, _
                        horaspresup, _
                        cod_direccion, _
                        cod_gerencia, _
                        cod_sector, _
                        cod_usualta, _
                        cod_solicitante, _
                        cod_referente, _
                        cod_supervisor, _
                        cod_director, _
                        cod_estado, _
                        fe_estado, _
                        cod_situacion, cod_bpar, cod_clase, pet_imptech, pet_sox001, pet_regulatorio, ProjId, ProjSubId, ProjSubSId, pet_emp, pet_ro, cod_bpe, fecha_BPE, gestiona, cargaBeneficios) As Boolean ' upd -010- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePeticion"
      .Parameters.Append .CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue)
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val("" & pet_nrointerno)))
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , CLng(Val(pet_nroasignado)))
      .Parameters.Append .CreateParameter("@titulo", adChar, adParamInput, 120, ClearNull(Titulo))
      .Parameters.Append .CreateParameter("@cod_tipo_peticion", adChar, adParamInput, 3, ClearNull(cod_tipo_peticion))
      .Parameters.Append .CreateParameter("@prioridad", adChar, adParamInput, 1, ClearNull(prioridad))
      .Parameters.Append .CreateParameter("@corp_local", adChar, adParamInput, 1, ClearNull(corp_local))
      .Parameters.Append .CreateParameter("@cod_orientacion", adChar, adParamInput, 2, ClearNull(cod_orientacion))
      .Parameters.Append .CreateParameter("@importancia_cod", adChar, adParamInput, 2, ClearNull(importancia_cod))
      .Parameters.Append .CreateParameter("@importancia_prf", adChar, adParamInput, 4, ClearNull(importancia_prf))
      .Parameters.Append .CreateParameter("@importancia_usr", adChar, adParamInput, 10, ClearNull(importancia_usr))
      .Parameters.Append .CreateParameter("@pet_nroanexada", adInteger, adParamInput, , CLng(Val(pet_nroanexada)))
      .Parameters.Append .CreateParameter("@prj_nrointerno", adInteger, adParamInput, , CLng(Val(prj_nrointerno)))
      .Parameters.Append .CreateParameter("@fe_pedido", SQLDdateType, adParamInput, , fe_pedido)
      .Parameters.Append .CreateParameter("@fe_requerida", SQLDdateType, adParamInput, , IIf(IsNull(fe_requerida), Null, fe_requerida))     ' upd -009- a.
      .Parameters.Append .CreateParameter("@fe_comite", SQLDdateType, adParamInput, , IIf(IsNull(fe_comite) Or fe_comite = "", Null, fe_comite))
      .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , fe_ini_plan)
      .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , fe_fin_plan)
      .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , fe_ini_real)
      .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , fe_fin_real)
      .Parameters.Append .CreateParameter("@horaspresup", adSmallInt, adParamInput, , CInt(Val(horaspresup)))
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, ClearNull(cod_direccion))
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, ClearNull(cod_gerencia))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, ClearNull(cod_sector))
      .Parameters.Append .CreateParameter("@cod_usualta", adChar, adParamInput, 10, ClearNull(cod_usualta))
      .Parameters.Append .CreateParameter("@cod_solicitante", adChar, adParamInput, 10, ClearNull(cod_solicitante))
      .Parameters.Append .CreateParameter("@cod_referente", adChar, adParamInput, 10, ClearNull(cod_referente))
      .Parameters.Append .CreateParameter("@cod_supervisor", adChar, adParamInput, 10, ClearNull(cod_supervisor))
      .Parameters.Append .CreateParameter("@cod_director", adChar, adParamInput, 10, ClearNull(cod_director))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, ClearNull(cod_estado))
      .Parameters.Append .CreateParameter("@fe_estado", SQLDdateType, adParamInput, , fe_estado)
      .Parameters.Append .CreateParameter("@cod_situacion", adChar, adParamInput, 6, ClearNull(cod_situacion))
      .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, ClearNull(cod_bpar))
      .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, ClearNull(cod_clase))
      .Parameters.Append .CreateParameter("@cod_imptech", adChar, adParamInput, 1, UCase(Left(pet_imptech, 1)))
      .Parameters.Append .CreateParameter("@pet_sox001", adTinyInt, adParamInput, 1, pet_sox001)
      .Parameters.Append .CreateParameter("@pet_regulatorio", adChar, adParamInput, 1, UCase(Left(pet_regulatorio, 1)))
      .Parameters.Append .CreateParameter("@projid", adInteger, adParamInput, , ProjId)
      .Parameters.Append .CreateParameter("@projsubid", adInteger, adParamInput, , ProjSubId)
      .Parameters.Append .CreateParameter("@projsubsid", adInteger, adParamInput, , ProjSubSId)
      .Parameters.Append .CreateParameter("@pet_emp", adInteger, adParamInput, , CLng(Val(pet_emp)))
      .Parameters.Append .CreateParameter("@pet_ro", adChar, adParamInput, 1, IIf(pet_ro = "" Or pet_ro = "-", Null, pet_ro))
      '{ add -010- a.
      .Parameters.Append .CreateParameter("@cod_BPE", adChar, adParamInput, 10, IIf(cod_bpe = "", Null, cod_bpe))
      .Parameters.Append .CreateParameter("@fecha_BPE", SQLDdateType, adParamInput, , IIf(fecha_BPE = "", Null, fecha_BPE))
      .Parameters.Append .CreateParameter("@driver", adChar, adParamInput, 8, IIf(gestiona = "", Null, gestiona))
      '.Parameters.Append .CreateParameter("@cargaBeneficios", adChar, adParamInput, 1, IIf(cargaBeneficios.value = 1, "N", "S"))
      If cargaBeneficios = "0" Or cargaBeneficios = "1" Then Stop
      .Parameters.Append .CreateParameter("@cargaBeneficios", adChar, adParamInput, 1, cargaBeneficios)
      '}
      Set aplRST = .Execute
    End With
    sp_UpdatePeticion = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticion = False
    'Resume
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePetFechas(pet_nrointerno, fe_ini_plan, fe_fin_plan, fe_ini_real, fe_fin_real, horaspresup) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePetFechas"
      .Parameters.Append .CreateParameter("@xnrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@fe_ini_plan", adChar, adParamInput, 8, IIf(IsNull(fe_ini_plan), Null, Format(fe_ini_plan, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@fe_fin_plan", adChar, adParamInput, 8, IIf(IsNull(fe_fin_plan), Null, Format(fe_fin_plan, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , fe_ini_real)
      .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , fe_fin_real)
      .Parameters.Append .CreateParameter("@horaspresup", adInteger, adParamInput, , CInt(horaspresup))
      Set aplRST = .Execute
    End With
    sp_UpdatePetFechas = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePetFechas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_AnexarPeticion(ori_nrointerno, des_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_AnexarPeticion"
      .Parameters.Append .CreateParameter("@ori_nrointerno", adInteger, adParamInput, , CLng(Val(ori_nrointerno)))
      .Parameters.Append .CreateParameter("@des_nrointerno", adInteger, adParamInput, , CLng(Val(des_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_AnexarPeticion = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_AnexarPeticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function


Function sp_DeletePeticion(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeletePeticion"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_DeletePeticion = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
  
    'aplRST.Close
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateMemo(pet_nrointerno, NombreCampo, CampoTexto) As Boolean
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim rsAux As ADODB.Recordset
    Dim dFechaHora As Date
    
    Set rsAux = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePeticionMemo"
      .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1)
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput)
      .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10)
      .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput)
      .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255)
    End With
    
    sp_UpdateMemo = True
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(CampoTexto, 255)
        With objCommand
            If Len(CampoTexto) > 255 Then
                .Parameters(0).Value = "N"
            Else
                bFlg = False
                .Parameters(0).Value = "S"
            End If
            .Parameters(1).Value = CLng(Val(pet_nrointerno))
            .Parameters(2).Value = NombreCampo
            .Parameters(3).Value = CInt(Secuencia)
            .Parameters(4).Value = TmpTexto & ""
            Set rsAux = .Execute
        End With
        Secuencia = Secuencia + 1
        CampoTexto = Mid(CampoTexto, 256)
    Loop
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(rsAux, Err))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_UpdateMemo = False
End Function

Function sp_GetMemo(pet_nrointerno, NombreCampo) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    If aplRST.State <> adStateClosed Then aplRST.Close
    'aplRST.Close
    Set aplRST = Nothing
    
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionMemo"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
      .Parameters.Append .CreateParameter("@NombreCampo", adVarChar, adParamInput, 10, NombreCampo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        bGetMemo = True
    End If
    
    If bGetMemo Then
        Do While Not aplRST.EOF
            sGetMemo = sGetMemo & ClearNull(aplRST(0))
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetMemo = sGetMemo
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetMemo = ""
End Function

''{ add -005- a.
'Function sp_GetMemoH(pet_nrointerno, NombreCampo) As String
'    Dim bGetMemo As Boolean
'    Dim sGetMemo As String
'    sGetMemo = ""
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_GethPeticionMemo"
'      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'      .Parameters.Append .CreateParameter("@NombreCampo", adVarChar, adParamInput, 10, NombreCampo)
'      Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        bGetMemo = True
'    End If
'
'    If bGetMemo Then
'        Do While Not aplRST.EOF
'            sGetMemo = sGetMemo & ClearNull(aplRST(0))
'            aplRST.MoveNext
'        Loop
'        aplRST.Close
'    End If
'
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'    sp_GetMemoH = sGetMemo
'Exit Function
'
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    sp_GetMemoH = ""
'End Function
'
'Public Function sp_GetPeticionEsHistorica(pet_nrointerno) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_GetPeticionEsHistorica"
'      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'      Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPeticionEsHistorica = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPeticionEsHistorica = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
''}

Function sp_GetPeticionImport(fec_import) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionImport"
      .Parameters.Append .CreateParameter("@fec_import", SQLDdateType, adParamInput, , fec_import)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionImport = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionImport = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -005- a.
Function sp_InsertPeticionAHistorica(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionAHistorica"
        If IsNull(pet_nrointerno) Then
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , Null)
        Else
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        End If
        Set aplRST = .Execute
    End With
    sp_InsertPeticionAHistorica = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionAHistorica = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertPeticionDeHistorica(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionDeHistorica"
        If IsNull(pet_nrointerno) Then
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , Null)
        Else
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        End If
        Set aplRST = .Execute
    End With
    sp_InsertPeticionDeHistorica = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionDeHistorica = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -006- a.
Function sp_GetPeticionEsReplan(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionEsReplan"
        If IsNull(pet_nrointerno) Then
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , Null)
        Else
            .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        End If
        Set aplRST = .Execute
    End With
    If aplRST.State <> adStateClosed Then
        If Not (aplRST.EOF) Then
            sp_GetPeticionEsReplan = True
        End If
    Else
        sp_GetPeticionEsReplan = False
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionEsReplan = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Function sp_UpdateEstadoPeticion(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateEstadoPeticion"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_UpdateEstadoPeticion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateEstadoPeticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -008- a.
Function sp_GetControlPeticion(pet_nrointerno, Recurso, modo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetControlPeticion"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@recurso", adChar, adParamInput, 10, Recurso)
        .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, modo)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetControlPeticion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetControlPeticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'-012- GMT01 - INI
Function sp_UpdPeticionPri(pet_nrointerno, prioridadBPE) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdPeticionPri"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@prioridadBPE", adInteger, adParamInput, , CLng(Val(prioridadBPE)))
      Set aplRST = .Execute
    End With
    sp_UpdPeticionPri = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdPeticionPri = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'-012- GMT01 - FIN

