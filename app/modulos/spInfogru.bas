Attribute VB_Name = "spInfogru"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertInfogru(info_gruid, info_grunom, info_gruorden, info_gruhab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertInfogru"
        .Parameters.Append .CreateParameter("@info_gruid", adInteger, adParamInput, , info_gruid)
        .Parameters.Append .CreateParameter("@info_grunom", adChar, adParamInput, 50, info_grunom)
        .Parameters.Append .CreateParameter("@info_gruorden", adInteger, adParamInput, , info_gruorden)
        .Parameters.Append .CreateParameter("@info_gruhab", adChar, adParamInput, 1, info_gruhab)
        Set aplRST = .Execute
    End With
    sp_InsertInfogru = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertInfogru = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateInfogru(info_gruid, info_grunom, info_gruorden, info_gruhab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateInfogru"
                .Parameters.Append .CreateParameter("@info_gruid", adInteger, adParamInput, , info_gruid)
                .Parameters.Append .CreateParameter("@info_grunom", adChar, adParamInput, 50, info_grunom)
                .Parameters.Append .CreateParameter("@info_gruorden", adInteger, adParamInput, , info_gruorden)
                .Parameters.Append .CreateParameter("@info_gruhab", adChar, adParamInput, 1, info_gruhab)
                Set aplRST = .Execute
        End With
        sp_UpdateInfogru = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateInfogru = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateInfogruField(info_gruid, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateInfogruField"
                .Parameters.Append .CreateParameter("@info_gruid", adInteger, adParamInput, , info_gruid)
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateInfogruField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateInfogruField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteInfogru(info_gruid) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteInfogru"
                .Parameters.Append .CreateParameter("@info_gruid", adInteger, adParamInput, , info_gruid)
                Set aplRST = .Execute
        End With
        sp_DeleteInfogru = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteInfogru = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetInfogru(info_gruid) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_GetInfogru"
                .Parameters.Append .CreateParameter("@info_gruid", adInteger, adParamInput, , info_gruid)
                Set aplRST = .Execute
        End With
        If Not aplRST.EOF Then
                sp_GetInfogru = True
        End If
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_GetInfogru = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

