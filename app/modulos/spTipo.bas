Attribute VB_Name = "spTipo"
Option Explicit

Function sp_GetTipoPrj(cod_tipoprj, Optional flg_habil) As Boolean
    If IsMissing(flg_habil) Then
        flg_habil = Null
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetTipoPrj"
      .Parameters.Append .CreateParameter("@cod_tipoprj", adChar, adParamInput, 8, cod_tipoprj)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetTipoPrj = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetTipoPrj = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateTipoPrj(cod_tipoprj, nom_tipoprj, flg_habil, Secuencia) As Boolean
'Function sp_UpdateTipoPrj(modo, cod_tipoprj, nom_tipoprj, flg_habil, es_ejecutor) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateTipoPrj"
      .Parameters.Append .CreateParameter("@cod_tipoprj", adChar, adParamInput, 8, cod_tipoprj)
      .Parameters.Append .CreateParameter("@nom_tipoprj", adChar, adParamInput, 30, nom_tipoprj)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      .Parameters.Append .CreateParameter("@secuencia", adInteger, adParamInput, 3, Secuencia)
      Set aplRST = .Execute
    End With
    sp_UpdateTipoPrj = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateTipoPrj = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteTipoPrj(cod_tipoprj) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteTipoPrj"
      .Parameters.Append .CreateParameter("@cod_tipoprj", adChar, adParamInput, 8, cod_tipoprj)
      Set aplRST = .Execute
    End With
    sp_DeleteTipoPrj = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteTipoPrj = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertTipoPrj(cod_tipoprj, nom_tipoprj, flg_habil, Secuencia) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertTipoPrj"
      .Parameters.Append .CreateParameter("@cod_tipoprj", adChar, adParamInput, 8, cod_tipoprj)
      .Parameters.Append .CreateParameter("@nom_tipoprj", adChar, adParamInput, 30, nom_tipoprj)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      .Parameters.Append .CreateParameter("@secuencia", adInteger, adParamInput, 3, Secuencia)
      Set aplRST = .Execute
    End With
    sp_InsertTipoPrj = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertTipoPrj = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

