Attribute VB_Name = "Declaraciones"
' -001- a. FJS 03.07.2007 - Se agrega un nuevo recordset para trabajos auxiliares.
' -001- b. FJS 11.07.2007 - Se agrega una variable global para mantener en memoria si el registro es nuevo o viejo (a efecto de los controles SOX para documentaci�n) y
'                           adem�s otra variable global para guardar si la petici�n lleva o no impacto tecnol�gico.
' -001- c. FJS 03.08.2007 - Se definen constantes globales para los mensajes de confirmaci�n al adjuntar documentos o conformes.
' -001- d. FJS 15.08.2007 - Se definen algunas variables globales a fin de poder acceder de manera transparente a la nueva rutina de acceso a datos (creaci�n de par�metros de forma din�mica)
' -002- a. FJS 16.08.2007 - Se agrega la declaraci�n de dos funciones del sistema para obtener el nombre del usuario logueado en la estaci�n de trabajo y el nombre de la estaci�n de trabajo.
' -003- a. FJS 21.09.2007 - Implementaci�n de numeraci�n de versiones (m�todo de Pressman).
' -003- b. FJS 03.10.2007 - Se agrega una variable global para indicar funciones de programaci�n especiales.
' -004- a. FJS 14.04.2008 - Se agrega una funci�n de la API de Windows para obtener la versi�n del OS
' -005- a. FJS 15.04.2008 - Se agrega una funci�n de la API de Windows para obtener el 'shortpath' de un archivo.
' -006- a. FJS 23.05.2008 - Se agrega una constante para definir el tama�o m�ximo permitido para agregar archivos a la DB. Este tama�o es de 2 Mb.
' -007- a. FJS 03.10.2008 -
' -008- a. FJS 05.12.2008 - Variable global para indicar el formateo de una grilla.
' -009- b. FJS 23.12.2008 - Nuevas variables globales para el login/logout al aplicativo.
' -010- a. FJS 07.04.2009 - Replanificaciones.
' -011- a. FJS 16.06.2009 - Nuevo UDT p�blico para manejo de proyectos IDM.
' -012- a. FJS 28.10.2009 - Nueva variable p�blica para manejar el cat�logo de archivos de enmascaramiento.
' -013- a. FJS 16.04.2010 - Nueva funci�n del API para determinar si estoy ejecutando interpretado o desde archivo compilado.
' -014- a. FJS 08.11.2010 - Nuevo vector gen�rico para utilizar con la "funci�n" Split.
' -015- a. FJS 09.11.2010 - Nuevo udt para tener los documentos del sistema.
' -016- a. FJS 10.11.2010 - Elimino todas las referencia a la API de Windows que no se usan.
' -017- a. FJS 15.12.2010 - Se agregan funciones para comprimir archivos.
' -018- a. AA  27.11.2010 - Variables Globales que usaremos para las modificaciones referentes a CGM-111111-1
' -019- a. FJS 16.06.2015 - Nuevo: nuevo proceso para determinar la versi�n de MS Office.
' -020- a. FJS 27.10.2015 - Nuevo: variable global para manejar los errores de envio de mail autom�ticos.

Option Explicit

Public Declare Function GetTickCount Lib "kernel32" () As Long
Public Declare Function GetLastInputInfo Lib "user32" (plii As Any) As Long

Public Enum ORDENAMIENTO_TIPO
    DETERMINAR = 0
    NUMERICO = 1
    ALFANUMERICO = 2
End Enum

'Public Enum Perfiles
'    Solicitante
'    Referente
'    Autorizante
'    Supevidor
'    ReferenteDeSistemas
'    ReferenteDeRGyP
'    Analista
'    ResponsableGrupo
'    ResponsableSector
'    ResponsableGerencia
'    ResponsableDireccion
'    homologador
'End Enum

'Public Enum PeticionTipo
'    NORMAL = 0
'    ESPECIAL = 1
'    PROPIA = 2
'    PROYECTO_ = 3
'    AUDITORIA_INTERNA = 4
'    AUDITORIA_EXTERNA = 5
'End Enum

Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Type udtResponsable
    Legajo As String
    email As String
    Items As New Collection
End Type

Public Type udtLOGINSYBASE
    UserId As String            ' Server user ID
    UserFullName As String      ' Full name of the user
    LockReason As Long
    LoginCount As Long          ' Number of failed login attempts; reset to 0 by a successful login
    Pwdate As Date              ' Date the password was last changed
    LastLoginDate As Date       ' Timestamp for the user�s last login
    Status As Long              ' Status of the account (see Table 1-16)
End Type

Public Type LASTINPUTINFO
    cbSize As Long
    dwTime As Long
End Type

Public Const FORM_HEIGHT As Integer = 8505
Public Const FORM_WIDTH As Integer = 11985
Public bFormateando As Boolean      ' add -008- c.
Public vElemento() As String        ' add -014- a.

Private Const SW_SHOWNORMAL = 1

'{ add -014- a.
Global vecGOrdenClistShell(12) As String * 1
Global solInterv As String
Global vecGOrdenMSI(12) As String * 1
'}

Global bDebuggear As Boolean

'{ add -003- b.
Public Type oTip
    Version As String
    Nro As Integer
    Rng As Integer
    Texto As String
    fecha As String
End Type
'}

'{ add -011- a.
Public Type oProyectoIDM
    ProjId As String
    ProjSubId As String
    ProjSubSId As String
    projnom As String
    ProjFchAlta As String
    ProjCateg As String
    ProjClase As String
    ProjMarca As String
End Type
'}

'{ add -017- a.
Public Type ZIPUSERFUNCTIONS
    DLLPrnt As Long
    DLLPassword As Long
    DLLComment As Long
    DLLService As Long
End Type

Public Type ZPOPT
    fSuffix As Long
    fEncrypt As Long
    fSystem As Long
    fVolume As Long
    fExtra As Long
    fNoDirEntries As Long
    fExcludeDate As Long
    fIncludeDate As Long
    fVerbose As Long
    fQuiet As Long
    fCRLF_LF As Long
    fLF_CRLF As Long
    fJunkDir As Long
    fRecurse As Long
    fGrow As Long
    fForce As Long
    fMove As Long
    fDeleteEntries As Long
    fUpdate As Long
    fFreshen As Long
    fJunkSFX As Long
    fLatestTime As Long
    fComment As Long
    fOffsets As Long
    fPrivilege As Long
    fEncryption As Long
    fRepair As Long
    flevel As Byte
    date As String
    szRootDir As String
End Type

Public Type ZIPnames
    s(0 To 99) As String
End Type

Public Type CBChar
    ch(4096) As Byte
End Type

Public Declare Function ZpInit Lib "zip32.dll" (ByRef Zipfun As ZIPUSERFUNCTIONS) As Long
Public Declare Function ZpSetOptions Lib "zip32.dll" (ByRef Opts As ZPOPT) As Long
Public Declare Function ZpArchive Lib "zip32.dll" (ByVal argc As Long, ByVal funame As String, ByRef argv As ZIPnames) As Long
'}

'*************************************************************************************************************************************************************************************************************************************************************************
' Declaracion de Funciones Externas
'*************************************************************************************************************************************************************************************************************************************************************************
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal lSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As Any, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFilename As String) As Long
Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Declare Function GetShortPathName Lib "kernel32" Alias "GetShortPathNameA" (ByVal lpszLongPath As String, ByVal lpszShortPath As String, ByVal cchBuffer As Long) As Long   ' add -005- a.
Declare Function GetModuleFileName Lib "kernel32" Alias "GetModuleFileNameA" (ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long               ' add -013- a.
Declare Function PathFindExtension Lib "shlwapi" Alias "PathFindExtensionA" (ByVal pPath As String) As Long
Declare Function lstrcpyA Lib "kernel32" (ByVal RetVal As String, ByVal Ptr As Long) As Long
Declare Function lstrlenA Lib "kernel32" (ByVal Ptr As Any) As Long
Declare Sub GetSystemTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)

'Public Enum PET_TIPO
'    NORMAL = "NOR"
'    PROPIA = "PRO"
'    ESPECIAL = "ESP"
'    Proyecto = "PRJ"
'    AUDITORIA_INTERNA = "AUI"
'    AUDITORIA_EXTERNA = "AUX"
'End Enum
'
'Public Enum PET_CLASE
'    SinClase = "SINC"
'    NUEVO = "NUEV"
'    EVOLUTIVO = "EVOL"
'    OPTIMIZACION = "OPTI"
'    ATENCION_USUARIO = "ATEN"
'    CORRECTIVO = "CORR"
'    SPUFI = "SPUF"
'End Enum

'*************************************************************************************************************************************************************************************************************************************************************************
' Constantes Globales de Sistema
'*************************************************************************************************************************************************************************************************************************************************************************
Global Const ESPACIOS = "                                                                                "  ' 80 espacios
Global Const sNewComponents = "UpgrComp.lst"
Global Const ArchivoINI = "PET.INI"
Global Const COL_NEGRO = &H0&
Global Const COL_BLANCO = &HFFFFFF
Global Const COL_ROJO = &HFF&
Global Const COL_AZUL = &HFF0000
Global Const COL_AMARILLO = &HFFFF&
'{ add -003- a.
Global Const VERSION_RELEASEALPHA = " Alpha"
Global Const VERSION_RELEASEBETHA = " Betha"
Global Const VERSION_RELEASECANDIDATE = "-rc"
'}
'Global Const ARCHIVOS_ADJUNTOS_MAXSIZE = 2097152           ' add -006- a. - Este valor representa 2 Mb.
Global Const ARCHIVOS_ADJUNTOS_MAXSIZE = 4194304            ' En bytes
Global Const saltoLinea As String = "%0D%0A"                ' Constante para los saltos de l�nea o saltos de carro en el cuerpo del mensaje

'*************************************************************************************************************************************************************************************************************************************************************************
' Variables globales de sistema
'*************************************************************************************************************************************************************************************************************************************************************************

Global glCrystalNewVersion As Boolean
'{ add -019- a.
Global glMSOfficeExcelIsEnabled As Boolean
Global glMSOfficeExcelDefaultExtension As String
Global glMSOfficeExcelVersion As String
'}
Global SQLDdateType As Long
Global glArchivoINI As String
Global glWRKDIR As String
Global glLSTDIR As String
Global glHEADAPLICACION As String
Global glHEADEMPPRESA As String
Global glDRIVER_ODBC As String
Global glSERVER_SEGURIDAD As String
Global glSERVER_DSN As String
Global glSERVER_PUERTO As String
Global glBASE_SEGURIDAD As String
Global glUSUARIO_SEGURIDAD As String
Global glPASSWORD_SEGURIDAD As String
Global glPASSWORD_ENCRIPTADO As String
Global glAPLICACION_SYBASE As String
Global glBASE_APLICACION As String
Global glESQUEMA_SEGURIDAD As String
Global glONLINE As Boolean
Global glAdoError As String
Global glProgramOwner As String             ' add -003- b.
Global glFormateando_Grilla As Boolean      ' add -new-

Global glEsHomologador As Boolean                               ' Indica si el recurso actuante es un homologador o un usuario
Global glEsTecnologia As Boolean                                ' Indica si el recurso actuante es tecnolog�a
Global glEsSeguridadInformatica As Boolean                      ' Indica si el recurso actuante es Seguridad inform�tica (ER&CA)

'Variables Globales para la conexion ADO (ex RDO)
Global aplCONN As ADODB.Connection
Global aplRST As New ADODB.Recordset
Global aplRST1 As New ADODB.Recordset
Global docRST As New ADODB.Recordset
Global drvRST As New ADODB.Recordset            ' Usado para guardar las opciones de valoraci�n para drivers
'Global adoRSAux1 As New ADODB.Recordset     ' add -001- a.
' Modo global de Peticion
'Global glflgProyecto As Boolean
'Global glNumeroProyecto As String
'Global glModoProyecto As String
'Global glModoProyectoReal As String
'Global glEstadoProyecto As String
Global glNumeroPeticion As String
Global glNumeroPeriodo As String        ' Tiene que ver con la planificaci�n DYD
'{ add -001- b.
'Global glPeticion_sox001 As Byte        ' Variable de control para los controles SOX
'Global glPETFechaEnvioAlBP As Date      ' Fecha de envi� al BP (antes Comit�)
'Global glPETImpacto As String           ' Petici�n con impacto tecnol�gico
'Global glPETClase As String             ' Clase de la petici�n
'Global glPETTipo As String              ' Tipo de petici�n
'Global glPETModelo As Boolean           ' Modelo de control: True: Nuevo / False: Anterior
'Global glPETEstado As String            ' Estado general de la petici�n
'}
Global glNroAsignado As String
Global glModoPeticion As String
Global glModoPeticionSector As String
Global glModoPeticionGrupo As String
Global glNumeroAgrup As String
Global glSelectAgrup As String
Global glSelectAgrupStr As String
Global glModoAgrup As String
Global glModoSelectAgrup As String
''Global glNivelAgrup As String
''Global glAreaAgrup As String

' Variable globales para controlar al Usuario Logueado
Global glLOGIN_ID_REAL As String
Global glLOGIN_ID_REEMPLAZO As String
'Global glLOGIN_NAME_REAL As String
Global glLOGIN_NAME_REEMPLAZO As String
Global glLOGIN_PASSWORD As String
Global glLOGIN_Direccion As String      ' a que sector pertenece
Global glLOGIN_Gerencia As String       ' a que gerencia pertenece
Global glLOGIN_Sector As String         ' a que sector pertenece
Global glLOGIN_Grupo As String          ' a que sector pertenece
'Global glLOGIN_ABMTablas As String
Global glLOGIN_Reportes As String
Global glLOGIN_SuperUser As Boolean
'Global glLOGIN_MultiUser As Boolean
Global glENTORNO As String
Global glBASE As String
Global glBASEUSR As String
Global glBASEPWD As String
Global glSMTPMODO As String             ' Modo de envio de correo (0:VBForm 1:SMTP)
Global glSMTPNAME As String
Global glSMTPPORT As Integer
Global glSMTPAUTH As Boolean
Global glSMTPSSL As Boolean
Global glSMTPSNDR As String             ' Sender: CGM@bbva.com

'{ add -009- b.
Global glLOGIN_Equipo As String         ' Workstation
Global glLOGIN_Fecha As Date
Global glLOGIN_Hora As String
'}

'Variables Globales para la Aplicaci�n
Global sOpcionMenu As String
Global glFechaDesde As Date
Global glFechaHasta As Date
Global glAuxRetorno As String
Global glAuxEstado As String
Global glEsMinuta As Boolean

' Variables para detectar el recurso y/o proyecto seleccionado (que se esta trabajando)
Global glSector As String
Global glGrupo As String
Global glGerencia As String
Global glDireccion As String
'Global glGerenciaRecurso As String
' TODO: revisar y quitar esta variable global: glCodigoRecurso
Global glCodigoRecurso As String
Global glUsrPerfilActual As String
Global glEstadoPeticion As String
Global glEstadoSector As String
Global glEstadoGrupo As String
Global glEstadoQuery As Variant
Global glModoQuery As Variant
Global glModoEvaluar As String
Global glForzarEvaluar As Boolean
Global glLockProceso As Boolean
'Global glHoras As Integer
Public glUsrPerfiles()
Public glTablaPerfiles()

Public vAgrPublico()                        ' Agrupamientos p�blicos

'Global Const CTRL_DISABLE = "DIS"
'Global Const CTRL_NORMAL = "NOR"
'Global Const CTRL_OBLIGATORIO = "OBL"

Global Const DISABLE = "DIS"
Global Const NORMAL = "NOR"
Global Const OBLIGATORIO = "OBL"

'{ add -012- a.
Global glCatalogoENM As String
Global glCatalogoNombre As String
'}

Global glHomologacionGrupo As String
Global glHomologacionSector As String
Global glHomologacionGrupoNombre As String
Global glHomologacionSectorNombre As String
Global glHomologaci�nNombreResponsable As String

Global glSegInfGrupo As String
Global glSegInfSector As String
Global glSegInfGrupoNombre As String
Global glSegInfSectorNombre As String
Global glSegInfNombreResponsable As String

Global glTecnoGrupo As String
Global glTecnoSector As String
Global glTecnoGrupoNombre As String
Global glTecnoSectorNombre As String
Global glTecnoNombreResponsable As String

'Global glParameterCounter As Long       ' Usado como contador en la rutina DefinirParametros para llevar la cuenta de la cantidad de par�metros creados    ' add -001- d.

''{ add -004- a.
'Public Declare Function GetVersionExA Lib "kernel32" (lpVersionInformation As OSVERSIONINFO) As Integer
'Public Type OSVERSIONINFO
'    dwOSVersionInfoSize As Long
'    dwMajorVersion As Long
'    dwMinorVersion As Long
'    dwBuildNumber As Long
'    dwPlatformId As Long
'    szCSDVersion As String * 128
'End Type

Private Declare Function GetVersionEx2 Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFOEX) As Long
' Declaradas en WinNT.h
Public Enum eSuiteMask
    VER_SUITE_SMALLBUSINESS = &H1 ' Microsoft Small Business Server was once installed on the system, but may have been upgraded to another version of Windows
    VER_SUITE_ENTERPRISE = &H2 ' Windows Server 2003, Enterprise Edition, Windows 2000 Advanced Server, or Windows NT 4.0 Enterprise Edition
    VER_SUITE_BACKOFFICE = &H4 ' Microsoft BackOffice
    VER_SUITE_COMMUNICATIONS = &H8
    VER_SUITE_TERMINAL = &H10 ' Terminal Services is installed
    VER_SUITE_SMALLBUSINESS_RESTRICTED = &H20 ' Microsoft Small Business Server is installed with the restrictive client license in force
    VER_SUITE_EMBEDDEDNT = &H40 ' Windows XP Embedded
    VER_SUITE_DATACENTER = &H80 ' Windows Server 2003, Datacenter Edition or Windows 2000 Datacenter Server
    VER_SUITE_SINGLEUSERTS = &H100 ' Terminal Services is installed, but only one interactive session is supported
    VER_SUITE_PERSONAL = &H200 ' Windows XP Home Edition
    VER_SUITE_BLADE = &H400 ' Windows Server 2003, Web Edition
    VER_SUITE_STORAGE_SERVER = &H2000 ' Windows Storage Server 2003 R2 or Windows Storage Server 2003
    VER_SUITE_COMPUTE_SERVER = &H4000 ' Windows Server 2003, Compute Cluster Edition
End Enum
'// RtlVerifyVersionInfo() type mask bits
Private Const VER_MINORVERSION = &H1
Private Const VER_MAJORVERSION = &H2
Private Const VER_BUILDNUMBER = &H4
Private Const VER_PLATFORMID = &H8
Private Const VER_SERVICEPACKMINOR = &H10
Private Const VER_SERVICEPACKMAJOR = &H20
Private Const VER_SUITENAME = &H40
Private Const VER_PRODUCT_TYPE = &H80
'// RtlVerifyVersionInfo() os product type values
Public Enum eProductType
    VER_NT_WORKSTATION = &H1
    VER_NT_DOMAIN_CONTROLLER = &H2
    VER_NT_SERVER = &H3
End Enum
'// dwPlatformId defines:
Public Enum ePlatformId
    VER_PLATFORM_WIN32s = 0&
    VER_PLATFORM_WIN32_WINDOWS = 1&
    VER_PLATFORM_WIN32_NT = 2&
End Enum

Public Type OSVERSIONINFOEX_Enum
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As ePlatformId
    szCSDVersion As String * 128
    wServicePackMajor As Integer
    wServicePackMinor As Integer
    wSuiteMask As eSuiteMask
    wProductType As eProductType
    wReserved As Byte
End Type

Public Type OSVERSIONINFOEX
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
    wServicePackMajor As Integer
    wServicePackMinor As Integer
    wSuiteMask As Integer
    wProductType As Byte
    wReserved As Byte
End Type

'{ add -039- b.
Public cArchivos() As udtArchivosAdjuntos
Public Type udtArchivosAdjuntos
    Nombre As String
    Tipo As String
    tamanio As Long
    Extension As String
    ubicacion As String
End Type
'}

'{ add -015- a.
Public vTipoDocumento() As udtTipoDocumento
Public Type udtTipoDocumento
    Codigo As String
    Nombre As String
    Habilitado As Boolean
End Type
'}

'{ add -020- a.
Global glEMAIL_STATUS As Integer
Global Const EMAIL_STATUS_SUCCESSFUL = 0           ' Se envi� el email
Global Const EMAIL_STATUS_ERROR = 1                ' No pudo enviarse el email (no se conoce el motivo)
'}

'Constantes de retorno de error para ShellExecute
Private Const ERROR_FILE_NOT_FOUND = 2&
Private Const ERROR_PATH_NOT_FOUND = 3&
Private Const ERROR_BAD_FORMAT = 11&
Private Const SE_ERR_ACCESSDENIED = 5
Private Const SE_ERR_ASSOCINCOMPLETE = 27
Private Const SE_ERR_DDEBUSY = 30
Private Const SE_ERR_DDEFAIL = 29
Private Const SE_ERR_DDETIMEOUT = 28
Private Const SE_ERR_DLLNOTFOUND = 32
Private Const SE_ERR_FNF = 2
Private Const SE_ERR_NOASSOC = 31
Private Const SE_ERR_OOM = 8
Private Const SE_ERR_PNF = 3
Private Const SE_ERR_SHARE = 26

Public Sub EXEC_ShellExecute(hWnd, FileName)
    Select Case ShellExecute(hWnd, "open", FileName, vbNullString, vbNullString, SW_SHOWNORMAL)
        Case 0: MsgBox "Fuera de memoria o de recursos "
        Case ERROR_BAD_FORMAT: MsgBox "Formato inv�lido de archivo"
        Case SE_ERR_ACCESSDENIED: MsgBox " Acceso denegado al Intentar abrir el archivo"
        Case SE_ERR_ASSOCINCOMPLETE: MsgBox "Extensi�n incompleta o inv�lida del nombre del archivo"
        Case SE_ERR_DDEBUSY: MsgBox " DDE ocupado "
        Case SE_ERR_DDEFAIL: MsgBox " Fall en la Transacci�n DDE"
        Case SE_ERR_DDETIMEOUT: MsgBox "Petici�n DDE fuera de tiempo"
        Case SE_ERR_DLLNOTFOUND: MsgBox "DLL not Found"
        Case ERROR_FILE_NOT_FOUND, SE_ERR_FNF: MsgBox "File not found"
        Case SE_ERR_NOASSOC: MsgBox " El archivo no est� asociado a ninguna aplicaci�n"
        Case SE_ERR_OOM: MsgBox " Fuera de memoria"
        Case ERROR_PATH_NOT_FOUND, SE_ERR_PNF: MsgBox " Path not found "
        Case SE_ERR_SHARE: MsgBox "Sharing violation"
    End Select
End Sub

Public Function getOSVersion() As String
    'Dim OSInfo As OSVERSIONINFO
    Dim OSInfoEx As OSVERSIONINFOEX
    Dim osvi As OSVERSIONINFOEX
    Dim ret As Long
    Dim s As String
    '
    'Move (Screen.Width - Width) \ 4, 0
    '
    ' Usando GetVersion
    'ret = GetVersion
    
'    Dim vMajor As Long, vMinor As Long, vBuild As Long
'
'    vMajor = LoByte(LoWord(ret))
'    vMinor = HiByte(LoWord(ret))

    ' A partir de NT 4 tiene el Build (no en Me/9x)
'    If ret < &H80000000 Then
'        vBuild = HiWord(ret)
'    End If
'    vBuild = HiWord(ret)
    
'    Me.LabelGetVersion.Caption = "Versi�n: " & _
'                vMajor & "." & vMinor & "." & vBuild
'
'
    '
'    OSInfo.szCSDVersion = Space$(128)
'    OSInfo.dwOSVersionInfoSize = Len(OSInfo) '148
'    ret = GetVersionEx(OSInfo)
'    s = "MajorVersion     " & OSInfo.dwMajorVersion & vbCrLf & _
'        "MinorVersion     " & OSInfo.dwMinorVersion & vbCrLf & _
'        "BuildNumber      " & OSInfo.dwBuildNumber & vbCrLf & _
'        "PlatformId       " & OSInfo.dwPlatformId & vbCrLf & _
'        "CSDVersion       " & szTrim(OSInfo.szCSDVersion) & vbCrLf & _
'        "ret              " & ret
'    Me.txtOSVersion.Text = s
    '
    '
    ' Usando OSVERSIONINFOEX
    OSInfoEx.szCSDVersion = Space$(128)
    OSInfoEx.dwOSVersionInfoSize = Len(OSInfoEx)
    ret = GetVersionEx2(OSInfoEx)
'    s = "MajorVersion     " & OSInfoEx.dwMajorVersion & vbCrLf & _
'        "MinorVersion     " & OSInfoEx.dwMinorVersion & vbCrLf & _
'        "BuildNumber      " & OSInfoEx.dwBuildNumber & vbCrLf & _
'        "PlatformId       " & OSInfoEx.dwPlatformId & vbCrLf & _
'        "CSDVersion       " & szTrim(OSInfoEx.szCSDVersion) & vbCrLf & _
'        "ServicePackMajor " & OSInfoEx.wServicePackMajor & vbCrLf & _
'        "ServicePackMinor " & OSInfoEx.wServicePackMinor & vbCrLf & _
'        "SuiteMask        " & OSInfoEx.wSuiteMask & vbCrLf & _
'        "ProductType      " & OSInfoEx.wProductType & vbCrLf
'        '"ret              " & ret
'    s = s & vbCrLf
    '
    '
    LSet osvi = OSInfoEx
    '
    Select Case (osvi.dwPlatformId)
        Case VER_PLATFORM_WIN32_NT
            '// Test for the product.
            If (osvi.dwMajorVersion <= 4) Then
                s = s & ("Microsoft Windows NT ")
            End If
            '
            If (osvi.dwMajorVersion = 5 And osvi.dwMinorVersion = 0) Then
                s = s & ("Microsoft Windows 2000 ")
            End If
            '
            If (osvi.dwMajorVersion = 5 And osvi.dwMinorVersion = 1) Then
                s = s & ("Microsoft Windows XP ")
            End If
            '
            If (osvi.dwMajorVersion = 6) Then
                s = s & ("Microsoft Windows Vista ")
            End If
            '
            '// Test for product type.
            If (ret) Then
                If (osvi.wProductType = VER_NT_WORKSTATION) Then
                    If (osvi.wSuiteMask And VER_SUITE_PERSONAL) Then
                        s = s & ("Personal ")
                    Else
                        s = s & ("Professional ")
                    End If
                ElseIf (osvi.wProductType = VER_NT_SERVER) Then
                    If (osvi.wSuiteMask And VER_SUITE_DATACENTER) Then
                        s = s & ("DataCenter Server ")
                    ElseIf (osvi.wSuiteMask And VER_SUITE_ENTERPRISE) Then
                        s = s & ("Advanced Server ")
                    Else
                        s = s & ("Server ")
                     End If
                End If
            End If
            '
            '// Display version, service pack (if any), and build number.
            If (osvi.dwMajorVersion >= 4) Then
                s = s & "version " & CStr(osvi.dwMajorVersion) & "." & _
                    CStr(osvi.dwMinorVersion) & " " & _
                    (szTrim(osvi.szCSDVersion)) & _
                    " Build " & CStr(osvi.dwBuildNumber And &HFFFF)
            Else
                s = s & "version " & (szTrim(osvi.szCSDVersion)) & _
                    " Build " & CStr(osvi.dwBuildNumber And &HFFFF)
            End If
        Case VER_PLATFORM_WIN32_WINDOWS
            If (osvi.dwMajorVersion = 4 And osvi.dwMinorVersion = 0) Then
                s = s & "Microsoft Windows 95 "
                If (Mid$(szTrim(osvi.szCSDVersion), 2) = "C") Then _
                    s = s & ("OSR2 ")
            End If
            '
            If (osvi.dwMajorVersion = 4 And osvi.dwMinorVersion = 10) Then
                s = s & ("Microsoft Windows 98 ")
                If Mid$(szTrim(osvi.szCSDVersion), 2) = "A" Then _
                    s = s & ("SE ")
            End If
            If (osvi.dwMajorVersion = 4 And osvi.dwMinorVersion = 90) Then _
                s = s & ("Microsoft Windows Me ")
                
        Case VER_PLATFORM_WIN32s
            s = s & ("Microsoft Win32s ")
    End Select
    
    getOSVersion = s
End Function

Private Function szTrim(ByVal s As String) As String
    ' Quita los caracteres en blanco y los Chr$(0)                  (13/Dic/01)
    Dim i As Long
    '
    i = InStr(s, vbNullChar)
    If i Then
        s = Left$(s, i - 1)
    End If
    s = Trim$(s)
    
    szTrim = s
End Function

Private Function LOWORD(ByVal Numero As Long) As Long
    ' Devuelve el LoWord del n�mero pasado como par�metro
    LOWORD = Numero And &HFFFF&
End Function

Private Function HiWord(ByVal Numero As Long) As Long
    ' Devuelve el HiWord del n�mero pasado como par�metro
    HiWord = Numero \ &H10000 And &HFFFF&
End Function

Private Function LoByte(ByVal Numero As Integer) As Integer
    ' Devuelve el LoByte del n�mero pasado como par�metro
    LoByte = Numero And &HFF
End Function

Private Function HiByte(ByVal Numero As Integer) As Integer
    ' Devuelve el HiByte del n�mero pasado como par�metro
    HiByte = Numero \ &H100 And &HFF
End Function

