Attribute VB_Name = "spPeticionGrupo"
' -001- a. FJS 17.03.2008 - Se agrega el par�metro de fecha prevista de pasaje a producci�n al grupo
' -002- a. FJS 09.05.2008 - Se agregan los nuevos par�metros de fecha de fin de suspensi�n y el c�digo de motivo de suspensi�n
' -003- a. FJS 26.03.2009 - Se agregan nuevos SP para soportar el manejo de peticiones hist�ricas.
' -004- a. FJS 13.10.2009 - Se agrega un nuevo par�metro para indicar de d�nde viene el cambio a realizar (
'                           PET: es una reasignaci�n de un grupo ejecutor por otro desde una petici�n en particular.
'                           Deben eliminarse las relaciones recursos - peticiones.
'                           ORG: es un cambio estructural. Deben mantenerse las relaciones existentes (actualizando los datos solamente
'                           entre recursos y peticiones.)
' -005- a. FJS 08.03.2010 - Nueva funci�n para agregar al grupo homologador a una petici�n

Option Explicit

Function sp_GetPeticionGrupo(pet_nrointerno, cod_sector, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionGrupo"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionGrupo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionGrupoXt(pet_nrointerno, cod_sector, cod_grupo, Optional cod_estado) As Boolean
    If IsMissing(cod_estado) Then
        cod_estado = ""
    End If
    
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionGrupoXt"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 250, cod_estado)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionGrupoXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionGrupoXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionGrupo(pet_nrointerno, _
                    cod_grupo, _
                    fe_ini_plan, _
                    fe_fin_plan, _
                    fe_ini_real, _
                    fe_fin_real, _
                    fe_produccion, _
                    fe_suspension, _
                    cod_motivo, _
                    horaspresup, _
                    cod_estado, _
                    fe_estado, _
                    cod_situacion, _
                    hst_nrointerno_sol, _
                    hst_nrointerno_rsp) As Boolean      ' upd -002- a. Se agregan los par�metros 'fe_suspension' y 'cod_motivo'
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionGrupo"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , IIf(fe_ini_plan = "00:00:00", Null, fe_ini_plan))
        .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , IIf(fe_fin_plan = "00:00:00", Null, fe_fin_plan))
        .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , IIf(fe_ini_real = "00:00:00", Null, fe_ini_real))
        .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , IIf(fe_fin_real = "00:00:00", Null, fe_fin_real))
        .Parameters.Append .CreateParameter("@fe_produccion", SQLDdateType, adParamInput, , IIf(fe_produccion = "00:00:00", Null, fe_produccion))  ' add -001- a.
        .Parameters.Append .CreateParameter("@fe_suspension", SQLDdateType, adParamInput, , IIf(fe_suspension = "00:00:00", Null, fe_suspension))
        .Parameters.Append .CreateParameter("@cod_motivo", adInteger, adParamInput, , cod_motivo)
        .Parameters.Append .CreateParameter("@horaspresup", adInteger, adParamInput, , horaspresup)
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@fe_estado", SQLDdateType, adParamInput, , fe_estado)
        .Parameters.Append .CreateParameter("@cod_situacion", adChar, adParamInput, 6, cod_situacion)
        .Parameters.Append .CreateParameter("@hst_nrointerno_sol", adInteger, adParamInput, , CLng(hst_nrointerno_sol))
        .Parameters.Append .CreateParameter("@hst_nrointerno_rsp", adInteger, adParamInput, , CLng(hst_nrointerno_rsp))
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionGrupo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionGrupo = False
End Function

Function sp_InsertPeticionGrupo(pet_nrointerno, _
                    cod_grupo, _
                    fe_ini_plan, _
                    fe_fin_plan, _
                    fe_ini_real, _
                    fe_fin_real, _
                    fe_produccion, _
                    fe_suspension, _
                    cod_motivo, _
                    horaspresup, _
                    cod_estado, _
                    fe_estado, _
                    cod_situacion, _
                    hst_nrointerno_sol, _
                    hst_nrointerno_rsp) As Boolean  ' upd -001- a. - Se agrega el par�metro 'fe_produccion'     ' upd -002- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionGrupo"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , fe_ini_plan)
        .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , fe_fin_plan)
        .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , fe_ini_real)
        .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , fe_fin_real)
        .Parameters.Append .CreateParameter("@fe_produccion", SQLDdateType, adParamInput, , fe_produccion)  ' add -001- a.
        '{ add -002- a.
        .Parameters.Append .CreateParameter("@fe_produccion", SQLDdateType, adParamInput, , fe_suspension)
        .Parameters.Append .CreateParameter("@fe_produccion", adInteger, adParamInput, , cod_motivo)
        '}
        .Parameters.Append .CreateParameter("@horaspresup", adInteger, adParamInput, , horaspresup)
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@fe_estado", SQLDdateType, adParamInput, , fe_estado)
        .Parameters.Append .CreateParameter("@cod_situacion", adChar, adParamInput, 6, cod_situacion)
        .Parameters.Append .CreateParameter("@hst_nrointerno_sol", adInteger, adParamInput, , CLng(hst_nrointerno_sol))
        .Parameters.Append .CreateParameter("@hst_nrointerno_rsp", adInteger, adParamInput, , CLng(hst_nrointerno_rsp))
      Set aplRST = .Execute
    End With
    sp_InsertPeticionGrupo = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionGrupo = False
End Function

Function sp_DeletePeticionGrupo(pet_nrointerno, cod_sector, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionGrupo"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    sp_DeletePeticionGrupo = True
'    If Not IsEmpty(rsges) And (Not rsges.EOF) And rsges(0).Name = "ErrCode" Then
'        MsgBox (AnalizarErrorSQL(rsges, 30000))
'        sp_UpdateGrupoGES = False
'    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionGrupo = False
End Function

Function sp_UpdatePeticionGrupoAdic(pet_nrointerno, _
                    cod_grupo, _
                    prio_ejecuc, _
                    info_adicio) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionGrupoAdic"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@prio_ejecuc", adInteger, adParamInput, , CLng(prio_ejecuc))
        .Parameters.Append .CreateParameter("@info_adicio", adChar, adParamInput, 250, info_adicio)
      Set aplRST = .Execute
    End With
    sp_UpdatePeticionGrupoAdic = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionGrupoAdic = False
End Function

Function sp_ChangePeticionGrupo(pet_nrointerno, _
                    old_sector, _
                    old_grupo, _
                    new_sector, _
                    new_grupo, _
                    origen) As Boolean      ' upd -004- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_ChangePeticionGrupo"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@old_sector", adChar, adParamInput, 8, old_sector)
        .Parameters.Append .CreateParameter("@old_grupo", adChar, adParamInput, 8, old_grupo)
        .Parameters.Append .CreateParameter("@new_sector", adChar, adParamInput, 8, new_sector)
        .Parameters.Append .CreateParameter("@new_grupo", adChar, adParamInput, 8, new_grupo)
        .Parameters.Append .CreateParameter("@origen", adChar, adParamInput, 3, origen)     ' add -004- a.
        Set aplRST = .Execute
    End With
    sp_ChangePeticionGrupo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ChangePeticionGrupo = False
End Function

Function sp_InsertPeticionRecurso(pet_nrointerno, _
                    cod_recurso, _
                    cod_grupo, _
                    cod_sector, _
                    cod_gerencia, _
                    cod_direccion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionRecurso"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      Set aplRST = .Execute
    End With
    sp_InsertPeticionRecurso = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionRecurso = False
End Function

Function sp_DeletePeticionRecurso(pet_nrointerno, cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionRecurso"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      Set aplRST = .Execute
    End With
    sp_DeletePeticionRecurso = True
'    If Not IsEmpty(rsges) And (Not rsges.EOF) And rsges(0).Name = "ErrCode" Then
'        MsgBox (AnalizarErrorSQL(rsges, 30000))
'        sp_UpdateGrupoGES = False
'    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionRecurso = False
End Function

Function sp_GetPeticionRecurso(pet_nrointerno, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionRecurso"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionRecurso = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionRecurso = False
End Function

Function sp_GetPeticionRecursoHoras(pet_nrointerno, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionRecursoHoras"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionRecursoHoras = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionRecursoHoras = False
End Function

'{ add -003- a.
Function sp_GetPeticionRecursoH(pet_nrointerno, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionRecursoH"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionRecursoH = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionRecursoH = False
End Function
'}

'{ add -005- a.
Public Function sp_AgregarHomologaci�n(pet_nrointerno) As Boolean

End Function
'}
