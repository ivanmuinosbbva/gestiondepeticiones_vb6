Attribute VB_Name = "spValoracion"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertValoracion(valorId, valorTexto, valorNum, valorOrden, valorHab, valorDriver) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertValoracion"
        .Parameters.Append .CreateParameter("@valorId", adInteger, adParamInput, , valorId)
        .Parameters.Append .CreateParameter("@valorTexto", adChar, adParamInput, 255, valorTexto)
        .Parameters.Append .CreateParameter("@valorNum", adInteger, adParamInput, , valorNum)
        .Parameters.Append .CreateParameter("@valorOrden", adInteger, adParamInput, , valorOrden)
        .Parameters.Append .CreateParameter("@valorHab", adChar, adParamInput, 1, valorHab)
        .Parameters.Append .CreateParameter("@valorDriver", adInteger, adParamInput, , valorDriver)
        Set aplRST = .Execute
    End With
    sp_InsertValoracion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertValoracion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateValoracion(valorId, valorTexto, valorNum, valorOrden, valorHab, valorDriver) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateValoracion"
        .Parameters.Append .CreateParameter("@valorId", adInteger, adParamInput, , valorId)
        .Parameters.Append .CreateParameter("@valorTexto", adChar, adParamInput, 255, valorTexto)
        .Parameters.Append .CreateParameter("@valorNum", adDouble, adParamInput, , valorNum)
        .Parameters.Append .CreateParameter("@valorOrden", adInteger, adParamInput, , valorOrden)
        .Parameters.Append .CreateParameter("@valorHab", adChar, adParamInput, 1, valorHab)
        .Parameters.Append .CreateParameter("@valorDriver", adInteger, adParamInput, , valorDriver)
        Set aplRST = .Execute
    End With
    sp_UpdateValoracion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateValoracion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteValoracion(valorId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteValoracion"
        .Parameters.Append .CreateParameter("@valorId", adInteger, adParamInput, , valorId)
        Set aplRST = .Execute
    End With
    sp_DeleteValoracion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteValoracion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetValoracion(driverId, valorId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetValoracion"
        .Parameters.Append .CreateParameter("@valorId", adInteger, adParamInput, , IIf(IsNull(driverId), Null, driverId))
        .Parameters.Append .CreateParameter("@valorId", adInteger, adParamInput, , IIf(IsNull(valorId), Null, valorId))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetValoracion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetValoracion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
