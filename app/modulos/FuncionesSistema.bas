Attribute VB_Name = "FuncionesSistema"
' -001- a. FJS 29.06.2007 - Se agrega un nuevo tipo de enumeracion de publico alcance para indicar el tipo de objeto en ejecucion.
' -001- b. FJS 05.07.2007 - Se agrega al registro de Windows por defecto que la exportaci�n a Excel de la consulta general exporte
'                           las columnas de clase de petici�n e indicador de impacto tecnol�gico.
' -002- a. FJS 16.08.2007 - Se agrega un nuevo tipo de enumeraci�n para los modos de pantalla (principalmente para los formularios
'                           transaccionales).
' -002- b. FJS 16.08.2007 - Nueva funci�n para logueo autom�tico de mi usuario (exclusivamente).
' -002- c. FJS 11.09.2007 - Se condiciona el menu de la informaci�n de ADO para el usuario desarrollador exclusivamente (mi persona).
' -003- a. FJS 03.10.2007 - Se guarda en una variable global el usuario de OS del programador (Hardcode).
' -004- a. FJS 08.10.2007 - Se realiza una validaci�n de la versi�n del ejecutable contra la DB para evitar que un usuario ejecute CGM con
'                           una versi�n desactualizada.
' -005- a. FJS 11.03.2008 - Se agregan nuevas opciones de exportaci�n de datos de peticiones (Horas reales)
' -006- a. FJS 17.03.2008 - Homologaci�n: Se agrega la fecha prevista de pasaje a producci�n para un Sector/Grupo
' -007- a. FJS 16.05.2008 - Se agregan opciones por defecto al registro de Windows por el tema de Exportaciones
' -008- a. FJS 19.05.2008 - Se agrega una variable global para guardar el usuario seleccionado en el modo de selecci�n de usuarios
' -009- a. FJS 08.08.2008 - Se agrega la validaci�n de perfil para la habilitaci�n de los men�es (catalogadores).
' -010- a. FJS 13.08.2008 - Se modifica la manera de validar el perfil de catalogador: ahora si es un perfil de CGM. Si lo tiene y solo ese, entonces
'                           solo habilito esa opci�n. Si tiene m�s, adem�s del men� de Catalogaci�n, todo lo dem�s.
' -010- b. FJS 13.08.2008 - Se agrega nueva funci�n p�blica para saber si un recurso posee m�s de un perfil, adem�s del pasado por par�metro.
' -011- a. FJS 11.09.2008 - Se agrega la opci�n para exportar del atributo Regulatorio.
' -011- b. FJS 11.09.2008 - Se agrega la opci�n para exportar del atributo de Conformes de Homologaci�n.
' -011- c. FJS 26.09.2008 - Se agrega la opci�n para exportar del atributo de Proyecto IDM.
' -012- a. FJS 02.10.2008 - Se agrega aqu� una comprobaci�n para determinar si puede utilizarse el control CommonDialog v6.0 para la selecci�n
'                           m�ltiple de documentos para adjuntar.
' -012- b. FJS 10.10.2008 - Se agrega la opci�n de actualizaci�n de pantalla autom�tica por defecto.
' -012- c. FJS 15.10.2008 - Se agrega la �ltima ubicaci�n desde donde se adjuntaron documentos (Last Position Object).
' -013- a. FJS 19.11.2008 - Se agrega un procedimiento para inicializar las preferencias del usuario.
' -014- a. FJS 24.11.2008 - Se agrega una pantalla de Tips, para mostrar las novedades de la versi�n al usuario.
' -015- a. FJS 13.01.2009 - Se agrega nueva funci�n para determinar si el usuario debe cambiar su password.
' -016- a. FJS 30.01.2009 - Se complementa la rutina con una autoactualizaci�n del archivo ejecutable.
' -017- a. FJS 20.04.2009 - Nuevo perfil de Analista ejecutor.
' -017- b. FJS 28.04.2009 - Se agregan restricciones al ambiente de Desarrollo.
' -018- a. FJS 08.05.2009 - Se agrega al men� principal las opciones para Proyectos IDM.
' -018- b. FJS 31.08.2010 - Se modifica: el abm de proyectos IDM estar� solamente habilitado al legajo A115544 (Elizabeth Schamidt). Ver definici�n con Marcet-Ricciardelli.
' -019- a. FJS 14.05.2009 - Se agrega al men� principal el acceso para Aplicativos.
' -020- a. FJS 25.06.2009 - Se agrega una nueva opci�n de men�, bajo el esquema de perfiles, para cursar solicitudes a Carga de M�quinas.
' -021- a. FJS 21.08.2009 - Se modifica el esquema del men� principal. El men� de Catalogaci�n desaparece y la opci�n del men� se pasa a las Consultas. El men� de Catalogaci�n pasa a ser el de Dise�o y Desarrollo.
' -022- a. FJS 08.10.2009 - Se restringe el men� de autorizaci�n de solicitudes: solo responsables de sector (Supervisores, nivel III) pueden autorizar solicitudes a CMAQ.
' -023- a. FJS 13.10.2009 - Se agrega un par�metro que indica el origen del cambio a realizar. Ver opciones en la definici�n del SP.
' -024- a. FJS 27.10.2009 - Se agrega un nuevo men� para el manejo del cat�logo de archivos para enmascaramiento de datos.
' -025- a. FJS 17.11.2009 - Se agrega el men� de acceso a ADAH (aplicativo de Homologaci�n) para los usuarios de DYD, Organizaci�n y TRYAPO (por ahora solo en Banco).
' -026- a. FJS 12.01.2010 - Se inhabilita esta opci�n de generaci�n de solicitudes a los Resp. de Sector.
' -027- a. FJS 13.01.2010 - Se agrega una nueva opci�n de informes para las solicitudes de transmisiones de Producci�n a Desarrollo TSO (solo para DYD).
' -028- a. FJS 28.01.2010 - Se elimina del men� principal el acceso al link del aplicativo ADAH (se descontinu� el mismo).
' -029- a. FJS 01.06.2010 - Se agregan las opciones por defecto para el abm de proyectos IDM.
' -030- a. FJS 22.07.2010 - Planificaci�n: nuevo perfil para Oficina de Proyectos.
' -030- b. FJS 11.05.2011 - Planificaci�n: siempre habilitado.
' -031- a. FJS 20.08.2010 - IGM: nuevo perfil para administrador de IGM.
' -032- a. FJS 04.11.2010 - Reingenier�a del aplicativo (separaci�n en dos m�dulos: operativo y configuraciones).
' -033- a. FJS 27.09.2011 - Nuevo: nueva opci�n de men� para la administraci�n de proyectos IDM por parte de perfiles BPs.
' -034- a. FJS 20.09.2013 - Nuevo: nuevo reporte de "Peticiones Finalizadas con Total de Hs." para administradores.
' -035- a. FJS 07.11.2013 - Nuevo: se habilita un nuevo perfil (PLAN) para visualizar las planificaciones.
' -036- a. FJS 08.07.2014 - Nuevo: se habilita que los responsables de ejecuci�n DyD puedan autorizar solicitudes sin enmascarar antes que el subgerente.
' -037- a. FJS 20.04.2015 - Nuevo: se agrega un nuevo atributo para sectores, que indica si el mismo es v�lido para usar como sector solicitante en peticiones especiales.
' -038- a. FJS 10.08.2015 - Nuevo: se agrega el men� para los informes de homologaci�n.
' -039- a. FJS 24.11.2015 - Nuevo: se agrega una opci�n para reportes de nuestra �rea.
' -040- GMT01  25.10.2019 - PET 70881 - Se FILTRA PERFIL POR MARCA DE HABILITACION

Option Explicit

Global m_strFind As String
Global m_lastFind As Integer
Global m_lastCOL As Integer
Global m_lastROW As Integer

Dim Hab_RGyP As Boolean

'{ add -new- !!!
Public Enum prmColorFilaGrilla
    prmGridFillRowColorGreen = 1
    prmGridFillRowColorBlue = 2
    prmGridFillRowColorRose = 3
    prmGridFillRowColorDarkGreen = 4
    prmGridFillRowColorRed = 5
    prmGridFillRowColorYellow = 6
    prmGridFillRowColorLightBlue = 7
    prmGridFillRowColorLightOrange = 8
    prmGridFillRowColorLightGrey = 9
    prmGridFillRowColorDarkGrey = 10
    prmGridFillRowColorBlueLink = 11
    prmGridFillRowColorLightBlue2 = 12
    prmGridFillRowColorDarkGreen2 = 13
    prmGridFillRowColorLightGreen = 14  ' rgb(217,244,224)
    prmGridFillRowColorDarkRed = 20
    prmGridFillRowColorWhite = 21
    prmGridFillRowColorBlack = 22
    prmGridFillRowColorDarkBlue = 23
    prmGridFillRowColorLightGrey2 = 24
    prmGridFillRowColorLightGrey3 = 25
    prmGridFillRowColorLightGrey4 = 26
End Enum

Public Enum prmEfectoFilaGrilla
    prmGridEffectFontBold = 1
    prmGridEffectFontItalic = 2
    prmGridEffectFontStrikeThrough = 3
    prmGridEffectFontUnderline = 4
    prmGridEffectFontStyle_Flat = 5
    prmGridEffectFontStyle_Inset = 6
    prmGridEffectFontStyle_InsetLight = 7
    prmGridEffectFontStyle_Raised = 8
    prmGridEffectFontStyle_RaisedLight = 9
End Enum
'}

'**************************************************************************
Sub CerrarForms(Optional vPantalla As Variant)
'**************************************************************************
'Cierra todas las forms activas, excepto vPantalla y los mdi
'Parametros:    vPantalla (Form que no debe cerrar)
'**************************************************************************
    Dim fPantalla As Form
    'Call Status("Listo.")  ' no debe ir aqu�
    
    Debug.Print "*******************"
    Debug.Print "CerrarForms !!!!!!!"
    Debug.Print "*******************"
    
    For Each fPantalla In Forms
        If TypeOf fPantalla Is MDIForm Then
            DoEvents
        ElseIf Not IsMissing(vPantalla) Then
            If UCase(Trim(fPantalla.name)) = UCase(Trim(vPantalla)) Then
                DoEvents
            End If
        Else
            Unload fPantalla
        End If
    Next
    Call Status("Listo.")   ' aqui debe ir, al final
End Sub

Sub LimpiarForm(fPantalla As Form)
'**************************************************************************
'Limpia los controles de la pantalla indicada
'Parametros:    fPantalla (form a limpiar)
'**************************************************************************
    Dim cControl As Control
    For Each cControl In fPantalla.Controls
        If TypeOf cControl Is TextBox _
            Or InStr(1, cControl.Tag, "CLEAR") > 0 Then
                cControl = ""
        ElseIf TypeOf cControl Is MaskText Then
                cControl.text = ""
        ElseIf TypeOf cControl Is MSFlexGrid Then
                cControl.Clear
        ElseIf TypeOf cControl Is ListBox Then
'            Or TypeOf cControl Is ComboBox Then
                cControl.Clear
        End If
    Next
End Sub

Sub IniciarMenues()
    ' Deshabilita todo, para que no quede nada mientras se carga Login
    With mdiPrincipal
        .mnuLogin.Enabled = False                'LOGIN
        .mnuLogout.Enabled = False               'LOGOUT
        .mnuCambioContrase�a.Enabled = False     'CAMBIO CONTRASE�A
        .mnuSalir.Enabled = True                 'SALIR
        .mnuMsg.Enabled = False                  'MENSAJES
        .mnuPrc.Enabled = False                  'PROCESOS
        .mnuOtros.Enabled = False                ' add -033- a.
        .mnuRpt.Enabled = False
        .mnuHoras.Enabled = False
        .mnuViewPetRec.Enabled = False
        .mnuDYD.Enabled = False                 ' add -021- a. Inicializaci�n
        .mnuAyuda.Enabled = False
        ' BARRA PRINCIPAL
        .tbSession.Buttons(1).Enabled = False
        .tbSession.Buttons(2).Enabled = False
        .tbSession.Buttons(4).Enabled = False
        .tbSession.Buttons(5).Enabled = False
        '.cboSeleccionarPerfil.ListIndex = -1
        'Call setHabilCtrl(.cboSeleccionarPerfil, "DIS")
        'If .cboSeleccionarPerfil.ListCount = 0 Then Call setHabilCtrl(.cboSeleccionarPerfil, "DIS")
    End With
End Sub

Sub HabilitarMenues(bOpcion As Boolean)
    Dim bFlagBarra As Boolean
    
    bFlagBarra = False
    Call IniciarMenues
    
    mdiPrincipal.mnuLogin.Enabled = True
    
    'despues lo habilita depending on
    If bOpcion And glONLINE Then
        With mdiPrincipal
        
            'Se busca los swiches de habilitaciones -> GMT01
            Call recupHabilitaciones 'GMT01
            
            .mnuLogin.Enabled = False
            .mnuLogout.Enabled = True
            'If (InStr(1, "TEST|DESA|", glENTORNO, vbTextCompare) = 0 Or SoyElAnalista) Then .mnuCambioContrase�a.Enabled = True
            If InStr(1, "TEST|DESA|", glENTORNO, vbTextCompare) = 0 Then .mnuCambioContrase�a.Enabled = True
            .mnuSalir.Enabled = True
            .mnuMsg.Enabled = True
            .mnuPrc.Enabled = True
            .mnuRpt.Enabled = True
            .mnuHoras.Enabled = True
            .mnuViewPetRec.Enabled = True
            '.mnuOfiProy.Enabled = False                 ' add -035- a.
            .mnuViewPlanificar.Enabled = True               ' TODO: temporalmente
            
            'GMT01 - INI
            If Hab_RGyP = False Then
               .mnuViewPlanificar.Enabled = False
            End If
            'GMT01 - FIN
            
            If glLOGIN_Direccion = "MEDIO" Then
                .mnuOtros.Enabled = True                ' OTROS
            Else
                .mnuOtros.Enabled = IIf(InPerfil("PRJ2"), True, False)
            End If
            ' NUEVO
            If glLOGIN_SuperUser Then
                .mnuSoli.Enabled = True
                .mnuRefe.Enabled = True
                .mnuSupe.Enabled = True
                .mnuDire.Enabled = True
                .mnuAnal.Enabled = True
                .mnuCgru.Enabled = True
                .mnuCsec.Enabled = True
                .mnuBpar.Enabled = True
                .mnuCger.Enabled = True
                .mnuCdir.Enabled = True
                .mnuRGP.Enabled = True
                .mnuViewPlanificar.Enabled = True
                .mnuHomo.Enabled = True         ' add -038- a.
            Else
                .mnuSoli.Enabled = IIf(InPerfil("SOLI"), True, False)
                .mnuRefe.Enabled = IIf(InPerfil("REFE"), True, False)
                .mnuSupe.Enabled = IIf(InPerfil("SUPE"), True, False)
                .mnuDire.Enabled = IIf(InPerfil("AUTO"), True, False)
                .mnuAnal.Enabled = IIf(InPerfil("ANAL"), True, False)
                .mnuCgru.Enabled = IIf(InPerfil("CGRU"), True, False)
                .mnuCsec.Enabled = IIf(InPerfil("CSEC"), True, False)
                .mnuBpar.Enabled = IIf(InPerfil("BPAR"), True, False)
                .mnuCger.Enabled = IIf(InPerfil("CGCI"), True, False)
                .mnuCdir.Enabled = IIf(InPerfil("CDIR"), True, False)
                .mnuRGP.Enabled = IIf(InPerfil("GBPE"), True, False)
'                '{ add -035- a.
'                If InPerfil("PRJ1") Or InPerfil("PLAN") Then
'                    .mnuOfiProy.Enabled = True
'                End If
'                '}
                '.mnuOfiProy.Enabled = IIf(InPerfil("PRJ1"), True, False)       ' del -035- a.
                .mnuHomo.Enabled = IIf(InPerfil("HOMO"), True, False)           ' add -038- a.
                
                'GMT01 - INI
                If .mnuRGP.Enabled = True And Hab_RGyP = False Then
                   .mnuRGP.Enabled = False
                End If
                'GMT01 - FIN
                
            End If
            ' La parte de solicitudes de archivos enmascarados, vale solo para banco
            If glBASE = "BF" Then
                .mnuDYD.Enabled = True
                If glLOGIN_Gerencia = "DESA" Then
                    ' Cat�logo
                    .mnuDYD_catg_host.Enabled = True
                    .mnuDYD_catg_ssdd.Enabled = False           ' Por ahora deshabilitado.
                    ' Solicitudes
                    .mnuDYD_host_shell1.Enabled = True
                    .mnuDYD_ssdd.Enabled = False                ' For ahora deshabilitado
                    .mnuDYD_ssdd_shell1.Enabled = False         ' Por ahora deshabilitado
                    ' Autorizaciones
                    If (InPerfil("ADMI") Or glLOGIN_SuperUser) Or InPerfil("CGRU") Or InPerfil("CSEC") Or InPerfil("CGCI") Or InPerfil("CDIR") Then
                        .mnuDYD_ssdd_autorizar.Enabled = False
                        .mnuDYD_host_autorizar.Enabled = True
                    Else
                        .mnuDYD_ssdd_autorizar.Enabled = False
                        .mnuDYD_host_autorizar.Enabled = False
                    End If
                Else
                    If Not (InPerfil("ADMI") Or glLOGIN_SuperUser Or InPerfil("ASEG")) Then
                        .mnuDYD.Enabled = False
                    End If
                End If
            End If
            '.mnuRpt_Solicitudes.Enabled = False        ' add -027- a.
            .mnuRpt_Solicitudes.Enabled = True
            .mnuAdmi.Enabled = False
            .mnuSuperAdmi.Enabled = False
            .mnuAyuda.Enabled = True
            .mnuRpt_Otros.Enabled = False               ' add -034
            If InPerfil("ADMI") Or glLOGIN_SuperUser Then
                .mnuAdmi.Enabled = True
                .mnuSuperAdmi.Enabled = True
                .mnuRpt_Otros.Enabled = True            ' add -034- a.
                .mnuRpt_Varios2.Enabled = True          ' add -039- a.
            End If
            If (InPerfil("ADMI") Or glLOGIN_SuperUser) Or InPerfil("CGRU") Or InPerfil("CSEC") Or InPerfil("CGCI") Or InPerfil("CDIR") Then
                .rptHsPetRecuEjec.Enabled = True
            End If
            'If glLOGIN_ID_REAL <> glLOGIN_ID_REEMPLAZO Then .mnuRecDelega.Enabled = False
            .mnuRecDelega.Enabled = IIf(glLOGIN_ID_REAL = glLOGIN_ID_REEMPLAZO, True, False)
            ' BARRA PRINCIPAL
            If bFlagBarra Then
                .tbSession.Buttons(1).Enabled = True
                .tbSession.Buttons(2).Enabled = True
                .tbSession.Buttons(4).Enabled = True
                .tbSession.Buttons(5).Enabled = True
            End If
        End With
    End If
End Sub

Function InicializarRecurso(cod_recurso) As Boolean
    Dim i As Integer
    Dim bFlagBarra As Boolean
    
    bFlagBarra = False
    InicializarRecurso = False
    
    ' Estos son los datos del recurso actuante que ahora son inicializados
    glLOGIN_Direccion = "N"
    glLOGIN_Gerencia = ""
    glLOGIN_Sector = ""
    glLOGIN_Grupo = ""
    glLOGIN_ID_REEMPLAZO = cod_recurso      ' Aqu� se reemplaza el recurso actuante por el recurso reemplazante
    glLOGIN_NAME_REEMPLAZO = ""

    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        InicializarRecurso = True
        glLOGIN_NAME_REEMPLAZO = ClearNull(aplRST!nom_recurso)
        If Not (ClearNull(aplRST!estado_recurso) = "A") Then
            MsgBox "El usuario seleccionado no se encuentra en estado activo.", vbExclamation + vbOKOnly, "Recurso no activo"
            InicializarRecurso = False
            Exit Function
        End If
        If Not (IsNull(aplRST!cod_direccion)) Then
            glLOGIN_Direccion = ClearNull(aplRST!cod_direccion)
        End If
        If Not (IsNull(aplRST!cod_gerencia)) Then
            glLOGIN_Gerencia = ClearNull(aplRST!cod_gerencia)
        End If
        If Not (IsNull(aplRST!cod_sector)) Then
            glLOGIN_Sector = ClearNull(aplRST!cod_sector)
        End If
        If Not (IsNull(aplRST!cod_grupo)) Then
            glLOGIN_Grupo = ClearNull(aplRST!cod_grupo)
        End If
        glUsrPerfilActual = ""
        
        ReDim glUsrPerfiles(1 To 3, 1 To 1)
        'setea un perfil ficticio por las dudas no tenga ninguno
        If ClearNull(glLOGIN_Direccion) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "DIRE"
            glUsrPerfiles(3, 1) = glLOGIN_Direccion
        End If
        If ClearNull(glLOGIN_Gerencia) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "GERE"
            glUsrPerfiles(3, 1) = glLOGIN_Gerencia
        End If
        If ClearNull(glLOGIN_Sector) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "SECT"
            glUsrPerfiles(3, 1) = glLOGIN_Sector
        End If
        If ClearNull(glLOGIN_Grupo) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "GRUP"
            glUsrPerfiles(3, 1) = glLOGIN_Grupo
        End If
        i = 1
        If sp_GetRecursoPerfil(glLOGIN_ID_REEMPLAZO, Null) Then
            'mdiPrincipal.cboSeleccionarPerfil.Clear
            Do Until aplRST.EOF
                If ClearNull(aplRST!flg_area) <> "N" Then
                    i = i + 1
                    ReDim Preserve glUsrPerfiles(1 To 3, 1 To i)
                    glUsrPerfiles(1, i) = ClearNull(aplRST!cod_perfil)
                    glUsrPerfiles(2, i) = ClearNull(aplRST!cod_nivel)
                    glUsrPerfiles(3, i) = ClearNull(aplRST!cod_area)
                    ''{ new
                    'If bFlagBarra Then
                    '    mdiPrincipal.cboSeleccionarPerfil.AddItem ClearNull(aplRST!nom_perfil) & ESPACIOS & "||" & ClearNull(aplRST!cod_perfil)
                    'End If
                    ''}
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        If InPerfil("ADMI") Then
            glUsrPerfilActual = "ADMI"
        ElseIf InPerfil("CDIR") Then
            glUsrPerfilActual = "CDIR"
        ElseIf InPerfil("CGCI") Then
            glUsrPerfilActual = "CGCI"
        ElseIf InPerfil("GBPE") Then
            glUsrPerfilActual = "GBPE"
        ElseIf InPerfil("BPAR") Then
            glUsrPerfilActual = "BPAR"
        ElseIf InPerfil("CSEC") Then
            glUsrPerfilActual = "CSEC"
        ElseIf InPerfil("CGRU") Then
            glUsrPerfilActual = "CGRU"
        ElseIf InPerfil("ANAL") Then
            glUsrPerfilActual = "ANAL"
        ElseIf InPerfil("AUTO") Then
            glUsrPerfilActual = "AUTO"
        ElseIf InPerfil("SUPE") Then
            glUsrPerfilActual = "SUPE"
        ElseIf InPerfil("REFE") Then
            glUsrPerfilActual = "REFE"
        ElseIf InPerfil("SOLI") Then
            glUsrPerfilActual = "SOLI"
        ElseIf InPerfil("CHRS") Then
            glUsrPerfilActual = "CHRS"
        Else
            glUsrPerfilActual = "VIEW"
        End If
        mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
        'mdiPrincipal.cboSeleccionarPerfil.ListIndex = PosicionCombo(mdiPrincipal.cboSeleccionarPerfil, glUsrPerfilActual, True)
        'If mdiPrincipal.cboSeleccionarPerfil.ListCount <= 1 Then
        '    Call setHabilCtrl(mdiPrincipal.cboSeleccionarPerfil, "DIS")
        'Else
        '    Call setHabilCtrl(mdiPrincipal.cboSeleccionarPerfil, "NOR")
        'End If
        
        'If sp_GetRecursoEsHomologacion(glLOGIN_ID_REEMPLAZO) Then
        '    glEsHomologador = True
        'End If
        glEsHomologador = False
        glEsSeguridadInformatica = False
        glEsTecnologia = False
        If sp_GetGrupoXt(glLOGIN_Grupo, glLOGIN_Sector) Then
            Select Case ClearNull(aplRST.Fields!grupo_homologacion)
                Case "H": glEsHomologador = True
                Case "1": glEsSeguridadInformatica = True
                Case "2": glEsTecnologia = True
            End Select
        End If
        DoEvents
        If glLOGIN_SuperUser Then
            InicializarRecurso = True
            Exit Function
        End If
    Else
        MsgBox "El usuario indicado NO est� definido como recurso en el aplicativo.", vbExclamation, "Inicializar recurso"
    End If
End Function

Public Sub PerfilCrear()
    Dim i As Integer
    ReDim glTablaPerfiles(2, 1)
    i = 0
    
    If sp_GetPerfil(Null, Null) Then
        Do While Not aplRST.EOF
            i = i + 1
            ReDim Preserve glTablaPerfiles(2, i)
            glTablaPerfiles(1, i) = ClearNull(aplRST!cod_perfil)
            glTablaPerfiles(2, i) = ClearNull(aplRST!nom_perfil)
            aplRST.MoveNext
        Loop
    End If
    'Agrega un perfil virtual que no existe, por las duds no tiene ninguno
    i = i + 1
    ReDim Preserve glTablaPerfiles(2, i)
    glTablaPerfiles(1, i) = "VIEW"
    glTablaPerfiles(2, i) = "Consulta"
End Sub

Function InPerfil(Codigo) As Boolean
    'devuelve true si el usuario logueado posee determinado perfil habilitado
    Dim i As Integer
    InPerfil = False
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) = Codigo Then
            InPerfil = True
            Exit Function
        End If
    Next i
End Function

'{ add -010- b.
Function TieneOtroPerfil(Codigo) As Boolean
    ' Devuelve TRUE si adem�s del perfil solicitado, posee otro u otros adicionales
    Dim i As Integer
    TieneOtroPerfil = False
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) <> Codigo Then
            TieneOtroPerfil = True
            Exit Function
        End If
    Next i
End Function

Function GetOtroPerfil(Codigo) As String
    ' Devuelve otro perfil que el solicitado
    Dim i As Integer
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) <> Codigo Then
            GetOtroPerfil = glUsrPerfiles(1, i)
            Exit Function
        End If
    Next i
End Function
'}

Function getPerfNivel(Codigo) As String
    'devuelve el nivel que tiene el recurso para determinado perfil
    Dim i As Integer
    getPerfNivel = ""
    If Codigo = "SADM" Then
        getPerfNivel = "BBVA"
        Exit Function
    End If
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) = Codigo Then
            getPerfNivel = glUsrPerfiles(2, i)
            Exit Function
        End If
    Next i
End Function

Function getPerfArea(Codigo) As String
    'devuelve el area que tiene el recurso para determinado perfil
    Dim i As Integer
    getPerfArea = ""
    If Codigo = "SADM" Then
        getPerfArea = "BBVA"
        Exit Function
    End If
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) = Codigo Then
            getPerfArea = glUsrPerfiles(3, i)
            Exit Function
        End If
    Next i
End Function

Function getDescPerfil(Codigo) As String
'devuelve la descripcion de un perfil
    Dim i As Integer
    getDescPerfil = ""
    For i = 1 To UBound(glTablaPerfiles, 2)
        If glTablaPerfiles(1, i) = Codigo Then
            getDescPerfil = glTablaPerfiles(2, i)
            Exit Function
        End If
    Next i
End Function

Function getModoPerfil(Codigo) As String
'devuelve el modo (SOLI,EJEC) de determinado tipo de perfil
    Dim i As Integer
    getModoPerfil = ""
    For i = 1 To UBound(glTablaPerfiles, 2)
        If glTablaPerfiles(1, i) = Codigo Then
            getModoPerfil = glTablaPerfiles(3, i)
            Exit Function
        End If
    Next i
End Function

Function getDescNvAgrup(Codigo) As String
getDescNvAgrup = ""
Select Case Codigo
    Case "PUB"
        getDescNvAgrup = "P�blico"
    Case "USR"
        getDescNvAgrup = "Privado"
    Case "DIR"
        getDescNvAgrup = "Direcci�n"
    Case "GER"
        getDescNvAgrup = "Gerencia"
    Case "SEC"
        getDescNvAgrup = "Sector"
    Case "GRU"
        getDescNvAgrup = "Grupo"
End Select
End Function

Function chkNvAgrup(aNiv, aDir, aGer, aSec, aGru, aUsr, uDir, uGer, uSec, uGru, uUsr) As Boolean
chkNvAgrup = False
Select Case aNiv
    Case "PUB"
        chkNvAgrup = True
    Case "USR"
        If ClearNull(uUsr) = ClearNull(aUsr) Then
            chkNvAgrup = True
        End If
    Case "DIR"
        If ClearNull(uDir) = ClearNull(aDir) Then
            chkNvAgrup = True
        End If
    Case "GER"
        If ClearNull(uGer) = ClearNull(aGer) Then
            chkNvAgrup = True
        End If
        If ClearNull(uGer) = "" And ClearNull(aDir) = ClearNull(uDir) Then
            chkNvAgrup = True
        End If
    Case "SEC"
        If ClearNull(uSec) = ClearNull(aSec) Then
            chkNvAgrup = True
        End If
        If ClearNull(uSec) = "" And ClearNull(aGer) = ClearNull(uGer) Then
            chkNvAgrup = True
        End If
        If ClearNull(uGer) = "" And ClearNull(aDir) = ClearNull(uDir) Then
            chkNvAgrup = True
        End If
    Case "GRU"
        If ClearNull(uGru) = ClearNull(aGru) Then
            chkNvAgrup = True
        End If
        If ClearNull(uGru) = "" And ClearNull(aSec) = ClearNull(uSec) Then
            chkNvAgrup = True
        End If
        If ClearNull(uSec) = "" And ClearNull(aGer) = ClearNull(uGer) Then
            chkNvAgrup = True
        End If
        If ClearNull(uGer) = "" And ClearNull(aDir) = ClearNull(uDir) Then
            chkNvAgrup = True
        End If
End Select
End Function

Function getDescNivel(Codigo) As String
getDescNivel = ""
Select Case Codigo
    Case "BBVA"
        getDescNivel = "BBVA"
    Case "DIRE"
        getDescNivel = "Direcci�n"
    Case "GERE"
        getDescNivel = "Gerencia"
    Case "SECT"
        getDescNivel = "Sector"
    Case "GRUP"
        getDescNivel = "Grupo"
End Select
End Function

Public Function FindGrid(ByRef grdDatos As MSFlexGrid, ByRef rowInicial As Integer, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Boolean
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    FindGrid = False
    If rowInicial = 0 Then
        rowInicial = 1
    End If
    With grdDatos
        If .Rows < 2 Then
            Exit Function
        End If
        .rowSel = 0
        .ColSel = 0
        For i = rowInicial To .Rows - 1
           If InStr(Trim(.TextMatrix(i, ptrColumn)), Trim(strSeek)) > 0 Then
                If flgPosicion Then
                    .TopRow = i
                    .row = i
                    .rowSel = i
                    .col = 0
                    .ColSel = .cols - 1
                End If
                rowInicial = i
                FindGrid = True
                Exit Function
           End If
        Next
    End With
End Function

Function GridSeek(objForm As Form, objGrid As MSFlexGrid, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Integer
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    GridSeek = 0
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, ptrColumn)) = ClearNull(strSeek) Then
                GridSeek = i
                If flgPosicion Then
                    .TopRow = i
                    .row = i
                    .rowSel = i
                    .col = 0
                    .ColSel = .cols - 1
                    
                    Call objForm.MostrarSeleccion(False)
                End If
                Exit Function
           End If
        Next
    End With
End Function

Function GridSeek2(objForm As Form, objGrid As MSFlexGrid, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Integer
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    GridSeek2 = 0
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        For i = 1 To .Rows - 1
            If InStr(1, ClearNull(.TextMatrix(i, ptrColumn)), ClearNull(strSeek), vbTextCompare) > 0 Then
                GridSeek2 = i
                If flgPosicion Then
                    .TopRow = i
                    .row = i
                    .rowSel = i
                    .col = 0
                    .ColSel = .cols - 1
                    Call objForm.MostrarSeleccion(False)
                End If
                Exit Function
           End If
        Next
    End With
End Function

Sub GenArchivoTabla(objGrid As MSFlexGrid, lastCol As Integer, ByVal sArchivo As String)
    Dim nFile As Integer
    Dim sDatos As String
        
    Dim nColumnas As Integer, i, J As Integer
    On Error Resume Next
    
    ' Abre el archivo
    Screen.MousePointer = vbHourglass
    nFile = FreeFile
    On Error GoTo errOpen
    Open sArchivo For Output As #nFile
    On Error GoTo 0
    
    With objGrid
        If .Rows < 2 Then
            Screen.MousePointer = vbNormal
            Status ("Listo")
            Exit Sub
        End If
        
        'encabezados
        sDatos = ""
        For J = 0 To lastCol
            sDatos = sDatos & ClearNull(.TextMatrix(0, J)) & vbTab
        Next
        sDatos = Left(sDatos, Len(sDatos) - 1)
        Print #nFile, sDatos
        
        'datos
        For i = 1 To .Rows - 1
           sDatos = ""
           For J = 0 To lastCol
               sDatos = sDatos & ClearNull(.TextMatrix(i, J)) & vbTab
           Next
           sDatos = Left(sDatos, Len(sDatos) - 1)
           Print #nFile, sDatos
        Next
    End With
    
    Close #nFile
    Screen.MousePointer = vbNormal
    Status ("Listo")
    Exit Sub
errOpen:
    Screen.MousePointer = vbNormal
    Status ("Listo")
    MsgBox ("No pudo crear el archivo. " & Err.DESCRIPTION)
    Exit Sub
End Sub

Function valTareaHabil(xTarea) As Boolean
    valTareaHabil = False
    If sp_GetTarea(xTarea) Then
        If ClearNull(aplRST!flg_habil) = "S" Then
            valTareaHabil = True
        End If
        'aplRST.Close
    End If
End Function

Function getBpSector(xSector) As String
    getBpSector = ""
    If sp_GetSector(xSector, Null, Null) Then
        getBpSector = ClearNull(aplRST!cod_bpar)
        'aplRST.Close
    End If
End Function

Function valSectorHabil(xSector) As Boolean
    valSectorHabil = False
    If sp_GetSector(xSector, Null, Null) Then
        If ClearNull(aplRST!flg_habil) = "N" Then
            valSectorHabil = False
        Else
            valSectorHabil = True
        End If
    End If
End Function

'{ add -037- a.
Function valSectorHabilParaSolicitar(xSector) As Boolean
    valSectorHabilParaSolicitar = False
    If sp_GetSector(xSector, Null, Null) Then
        If ClearNull(aplRST!soli_hab) = "N" Then
            valSectorHabilParaSolicitar = False
        Else
            valSectorHabilParaSolicitar = True
        End If
    End If
End Function
'}

Function valGrupoHabil(xGrupo) As Boolean
        valGrupoHabil = False
        If sp_GetGrupo(xGrupo, Null, Null) Then
            If ClearNull(aplRST!flg_habil) = "N" Then
                valGrupoHabil = False
            Else
                valGrupoHabil = True
            End If
            aplRST.Close
        End If
End Function

Function valGerenciaHabil(xGerencia) As Boolean
        valGerenciaHabil = False
        If sp_GetGerencia(xGerencia, Null, Null) Then
            If ClearNull(aplRST!flg_habil) = "N" Then
                valGerenciaHabil = False
            Else
                valGerenciaHabil = True
            End If
            aplRST.Close
        End If
End Function

Function valDireccionHabil(xDireccion) As Boolean
        valDireccionHabil = False
        If sp_GetDireccion(xDireccion, Null) Then
            If ClearNull(aplRST!flg_habil) = "N" Then
                valDireccionHabil = False
            Else
                valDireccionHabil = True
            End If
            aplRST.Close
        End If
End Function

Sub reasignarPeticionGrupo(xNumeroPeticion, xSector, xGrupo, ySector, yGrupo, origen)   ' upd -023- a.
    Dim EstadoPeticion As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim NuevoEstadoSector As String
    Dim lblSector As String
    Dim lblGrupo As String
    Dim NroHistorial As Long
    Dim fEstado
    Dim sTipoPeticion As String
    Dim sClasePeticion As String
    
    If sp_GetUnaPeticion(xNumeroPeticion) Then
        EstadoPeticion = ClearNull(aplRST!cod_estado)
        sTipoPeticion = ClearNull(aplRST!cod_tipo_peticion)
        sClasePeticion = ClearNull(aplRST!cod_clase)
    End If
    If sp_GetPeticionSector(xNumeroPeticion, xSector) Then
        lblSector = ClearNull(aplRST!nom_sector)
    End If
    If sp_GetPeticionGrupo(xNumeroPeticion, xSector, xGrupo) Then
        EstadoGrupo = ClearNull(aplRST!cod_estado)
        lblGrupo = ClearNull(aplRST!nom_grupo)
    End If

    If sp_ChangePeticionGrupo(xNumeroPeticion, xSector, xGrupo, ySector, yGrupo, origen) Then    ' upd -023- a.
        NroHistorial = sp_AddHistorial(xNumeroPeticion, "GREASIG", EstadoPeticion, ySector, Null, yGrupo, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Sector/Grupo anterior: " & Trim(lblSector) & "/" & Trim(lblGrupo) & " �")
        '' el sector saliente
        EstadoSector = ""
        If sp_GetPeticionSector(xNumeroPeticion, xSector) Then
            EstadoSector = ClearNull(aplRST!cod_estado)
            fEstado = date
            Call getIntegracionGrupo(xNumeroPeticion, xSector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
            NuevoEstadoSector = getNuevoEstadoSector(xNumeroPeticion, xSector)
            If NuevoEstadoSector <> "" Then
                If NuevoEstadoSector <> "TERMIN" Then
                    fFinReal = Null
                End If
                fEstado = date
                If Not IsNull(fIniPlan) Then fEstado = fIniPlan
                If Not IsNull(fFinPlan) Then fEstado = fFinPlan
                If Not IsNull(fIniReal) Then fEstado = fIniReal
                If Not IsNull(fFinReal) Then fEstado = fFinReal
                If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
                    Call sp_UpdatePeticionSector(xNumeroPeticion, xSector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, fEstado, "", 0, 0)
                Else
                    Call sp_UpdatePeticionSector(xNumeroPeticion, xSector, Null, Null, Null, Null, 0, NuevoEstadoSector, date, "", 0, 0)
                End If
            End If
        End If
        '' el sector ENTRANTE
        EstadoSector = ""
        If sp_GetPeticionSector(xNumeroPeticion, ySector) Then
            EstadoSector = ClearNull(aplRST!cod_estado)
            Call getIntegracionGrupo(xNumeroPeticion, ySector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
            NuevoEstadoSector = getNuevoEstadoSector(xNumeroPeticion, ySector)
            If NuevoEstadoSector <> "" Then
                If NuevoEstadoSector <> "TERMIN" Then fFinReal = Null
                fEstado = date
                If Not IsNull(fIniPlan) Then fEstado = fIniPlan
                If Not IsNull(fFinPlan) Then fEstado = fFinPlan
                If Not IsNull(fIniReal) Then fEstado = fIniReal
                If Not IsNull(fFinReal) Then fEstado = fFinReal
                If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
                    Call sp_UpdatePeticionSector(xNumeroPeticion, ySector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, fEstado, "", 0, 0)
                Else
                    Call sp_UpdatePeticionSector(xNumeroPeticion, ySector, Null, Null, Null, Null, 0, NuevoEstadoSector, date, "", 0, 0)  ' upd -006- a.
                End If
            End If
        End If
        ''la PETICION
        Call getIntegracionSector(xNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
        NuevoEstadoPeticion = getNuevoEstadoPeticion(xNumeroPeticion)
        Call sp_UpdatePetField(xNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
        'Call sp_UpdatePetField(xNumeroPeticion, "SITUAC", "", Null, Null)
        If NuevoEstadoPeticion <> "TERMIN" Then fFinReal = Null
        If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL|", NuevoEstadoPeticion) > 0 Then
            Call sp_UpdatePetFechas(xNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
        Else
            Call sp_UpdatePetFechas(xNumeroPeticion, Null, Null, Null, Null, 0)
        End If
        Call sp_UpdHistorial(NroHistorial, "GREASIG", NuevoEstadoPeticion, ySector, NuevoEstadoSector, yGrupo, EstadoGrupo, glLOGIN_ID_REEMPLAZO)
        '{ add -06.02.2013-
        'If Not InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", NuevoEstadoPeticion, vbTextCompare) > 0 Then
        If InStr(1, "APROBA|EJECUC|SUSPEN|PLANOK|PLANIF|PLANRK|EJECRK|ESTIOK|ESTIRK|ESTIMA|", NuevoEstadoPeticion, vbTextCompare) > 0 Then
            If sClasePeticion <> "SINC" Then
                'If Not sp_GetPetGrupoHomologacion(xNumeroPeticion) Then
                If Not sp_GetPetGrupoHomologacion(xNumeroPeticion, "H") Then
                    Call AgregarGrupoHomologador(xNumeroPeticion)
                End If
            End If
        End If
        '}
    End If
End Sub

'{ add -06.02.2013-
Public Sub AgregarGrupoHomologador(pet_nrointerno)
    Dim cGrupoHomologador As String
    Dim cSectorHomologador As String
    Dim NroHistorial As Long
    Dim EstadoPeticion As String
    Dim sTipoPeticion As String
    Dim sClasePeticion As String
    Dim bFlag As Boolean
    
    bFlag = False
    If sp_GetUnaPeticion(pet_nrointerno) Then
        EstadoPeticion = ClearNull(aplRST!cod_estado)
        sTipoPeticion = ClearNull(aplRST!cod_tipo_peticion)
        sClasePeticion = ClearNull(aplRST!cod_clase)
    End If
    If sp_GetPeticionGrupo(pet_nrointerno, Null, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST.Fields!cod_gerencia) = "DESA" Then
                If Not InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) > 0 Then
                    bFlag = True
                    Exit Do
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
        If bFlag Then
'            If sp_GetGrupoHomologacion Then
'                cGrupoHomologador = ClearNull(aplRST.Fields!cod_grupo)
'                cSectorHomologador = ClearNull(aplRST.Fields!cod_sector)
'            End If
            'If Not sp_GetPeticionSector(pet_nrointerno, cSectorHomologador) Then
            If Not sp_GetPeticionSector(pet_nrointerno, glHomologacionSector) Then
                'Call sp_InsertPeticionSector(pet_nrointerno, cSectorHomologador, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
                Call sp_InsertPeticionSector(pet_nrointerno, glHomologacionSector, Null, Null, Null, Null, 0, "ESTIMA", date, "", 0, "0")
            End If
            'If sp_InsertPeticionGrupo(pet_nrointerno, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", 0, "0") Then
            If sp_InsertPeticionGrupo(pet_nrointerno, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", 0, "0") Then
                'Call sp_DoMensaje("GRU", "GNEW000", pet_nrointerno, cGrupoHomologador, "ESTIMA", "", "")                                              ' Se envian los mensajes correspondientes a Responsables de Grupo
                ' Se envian los mensajes correspondientes a Responsables de Grupo
                Call sp_DoMensaje("GRU", "GNEW000", pet_nrointerno, glHomologacionGrupo, "ESTIMA", "", "")
                Select Case sClasePeticion
                    Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(pet_nrointerno, "PAUTHOM", EstadoPeticion, glHomologacionSector, "ESTIMA", glHomologacionGrupo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso REDUCIDO) �")
                    Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(pet_nrointerno, "PAUTHOM", EstadoPeticion, glHomologacionSector, "ESTIMA", glHomologacionGrupo, "ESTIMA", glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso COMPLETO) �")
                    'Case "SPUF", "CORR", "ATEN": NroHistorial = sp_AddHistorial(pet_nrointerno, "PAUTHOM", EstadoPeticion, cSectorHomologador, "ESTIMA", cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso REDUCIDO) �")
                    'Case "OPTI", "EVOL", "NUEV": NroHistorial = sp_AddHistorial(pet_nrointerno, "PAUTHOM", EstadoPeticion, cSectorHomologador, "ESTIMA", cGrupoHomologador, "ESTIMA", glLOGIN_ID_REEMPLAZO, "� " & "Asignaci�n autom�tica del grupo Homologador: 1� Punto de Control (Proceso COMPLETO) �")
                End Select
                'Call sp_UpdatePeticionGrupo(pet_nrointerno, cGrupoHomologador, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")   ' upd -010- a.  ' 7. Esta llamada es solo para darle el numero de historial
                ' 7. Esta llamada es solo para darle el numero de historial
                Call sp_UpdatePeticionGrupo(pet_nrointerno, glHomologacionGrupo, Null, Null, Null, Null, Null, Null, 0, 0, "ESTIMA", date, "", NroHistorial, "0")
            End If
        End If
    End If
End Sub
'}

Sub doLog(ByVal sTexto As String)
    Dim nFile As Integer
        
    On Error Resume Next
   
    ' Abre el archivo
    nFile = FreeFile
    On Error GoTo errOpen
    'Open glWRKDIR & "LOGINS_CORRECT_" & glLOGIN_ID_REAL & "_log.txt" For Append As #nFile
    Open glWRKDIR & glLOGIN_ID_REAL & "_log.txt" For Append As #nFile
    On Error GoTo 0
    
    'Print #nFile, Format(date, "yyyymmdd") & ":" & Format(Time, "hhmss") & ":" & sTexto
    Print #nFile, Format(date, "yyyymmdd") & "|" & Format(Time, "hh:mm:ss") & "|" & sTexto
    
    Close #nFile
    Exit Sub
errOpen:
    MsgBox ("No pudo crear el archivo de log. " & Err.DESCRIPTION)
    Exit Sub
End Sub

Sub doFile(ByVal cFileName As String, ByVal sTexto As String)
    Dim nFile As Integer
    
    On Error Resume Next
   
    ' Abre el archivo
    nFile = FreeFile
    On Error GoTo errOpen
    Open glWRKDIR & cFileName For Append As #nFile
    On Error GoTo 0
    
    Print #nFile, sTexto
    
    Close #nFile
    Exit Sub
errOpen:
    MsgBox ("No pudo crear el archivo. " & Err.DESCRIPTION)
    Exit Sub
End Sub

Sub doFile2(ByVal cFileName As String, ByVal sTexto As String)
    Dim nFile As Integer
    
    On Error Resume Next
   
    ' Abre el archivo
    nFile = FreeFile
    On Error GoTo errOpen
    Open cFileName For Append As #nFile
    On Error GoTo 0
    
    Print #nFile, sTexto
    
    Close #nFile
    Exit Sub
errOpen:
    MsgBox ("No pudo crear el archivo. " & Err.DESCRIPTION)
    Exit Sub
End Sub

''{ add -004- a.
'Public Function CheckedAccess() As Boolean
'    Dim cAppVersion As String
'    '{ add -016- a.
'    'Dim dFechaVersion As Date
'    'Dim cUbicacionOrigen As String
'    'Dim cUbicacionDestino As String
'    '}
'    Dim lVersionNumber As Long
'
'    CheckedAccess = True
'    If sp_GetVarios("CGMVER") Then
'        If Not aplRST.EOF Then
'            cAppVersion = "v" & App.Major & "." & App.Minor & "." & App.Revision
'            'dFechaVersion = aplRST.Fields!var_fecha     ' add -016- a.
'            'If FileDateTime(App.Path & "\pet.exe") <> dFechaVersion Then
'            '    ' Debo forzar la actualizaci�n del ejecutable
'            '    cUbicacionOrigen = "\\bbvdfs\dfs\BBVAR_F\Bin\GesPet\pet.exe"
'            '    cUbicacionDestino = "c:\Archivos de programa\peticiones\pet.exe.temp"
'            '    'FileCopy cUbicacionOrigen, cUbicacionDestino
'            '
'            '
'            'End If
'
'
'
'            If Trim(cAppVersion) <> Trim(aplRST.Fields!var_texto) Then
'                ' Esto queda para terminar de desarrollar!!!
'                'If Not ValidVersionNumber(Trim(aplRST.Fields!var_texto)) Then
'                    MsgBox "Esta intentando ejecutar una versi�n incorrecta de la aplicaci�n." & vbCrLf & vbCrLf & _
'                           "Versi�n del sistema: " & Trim(aplRST.Fields("var_texto")) & vbCrLf & _
'                           "Versi�n del usuario: " & Trim(cAppVersion) & vbCrLf & vbCrLf & _
'                           "A continuaci�n, la aplicaci�n se cerrar�." & vbCrLf & vbCrLf & vbCrLf & vbCrLf & _
'                           "Importante:" & vbCrLf & vbCrLf & _
'                           "� REINICIE SU EQUIPO �" & vbCrLf & vbCrLf & _
'                           "Esto puede deberse a que existe una nueva versi�n de la aplicaci�n. Reinicie el equipo" & vbCrLf & _
'                           "para que pueda actualizarse automaticamente a la nueva versi�n del aplicativo. Si a�n" & vbCrLf & _
'                           "continua con el mismo problema, luego de reiniciar, comun�quese directamente al GAC.", vbExclamation + vbOKOnly, "Versi�n incorrecta de CGM"
'                    aplRST.Close
'                    CheckedAccess = False
'                'End If
'            Else
'                If InStr(1, "XA00309|H16726", cstmGetUserName, vbTextCompare) = 0 Then
'                    If CLng(aplRST.Fields!var_numero) = 0 Then
'                        MsgBox "Por cuestiones de desarrollo e implementaci�n de versiones, se ha deshabilitado el acceso al sistema." & vbCrLf & vbCrLf & _
'                               "Vuelva a intentar el ingreso al sistema m�s tarde.", vbExclamation + vbOKOnly, "CGM offline"
'                               aplRST.Close
'                        CheckedAccess = False
'                    End If
'                End If
'            End If
'        End If
'    End If
'End Function

Private Function ValidVersionNumber(cSystemVersion As String) As Boolean
    Dim lSystemVersion As Long
    Dim lUserVersion As Long
    Dim i As Integer
    Dim Num(1 To 3) As Integer
    Dim a As Integer
    Dim b As Integer
    Dim c As Integer
    Dim Pos As Integer
    Dim cAuxiliar As String
    
    ValidVersionNumber = True       ' Por defecto la versi�n es correcta
    
    cAuxiliar = Mid(cSystemVersion, 2)
    
    For i = 1 To 3
        If Pos = 0 Then Pos = 1
        If Not InStr(Pos, cAuxiliar, ".", vbTextCompare) = 0 Then
            Pos = InStr(Pos, cAuxiliar, ".", vbTextCompare)
            Num(i) = Val(Mid(cAuxiliar, Pos - 1))
        Else
            Num(i) = Val(Mid(cAuxiliar, Pos))
        End If
        Pos = Pos + 1
    Next i
    
    ' Obtengo un n�mero de 8 bits
    lUserVersion = (CLng(App.Major) * 1000000) + _
                     (CLng(App.Minor) * 1000) + _
                     (CLng(App.Revision) * 1)
    ' Obtengo el n�mero de versi�n del usuario
    lSystemVersion = (CLng(Num(1)) * 1000000) + _
                     (CLng(Num(2)) * 1000) + _
                     (CLng(Num(3)) * 1)
    If lUserVersion < lSystemVersion Then
        ValidVersionNumber = False
    End If
End Function
'}

Public Sub ObtenerTiempo(Iniciar As Boolean)
    Static Ini As Long
    Static Fin As Long
    If Iniciar Then
        Ini = GetTickCount ' / 1000     ' Para pasar a segundos
    Else
        Fin = GetTickCount ' / 1000     ' Para pasar a segundos
        Debug.Print Fin & " - " & Ini & " = " & Fin - Ini & " milisegundo(s)"
    End If
End Sub

'' Leonardo Azpurua -06/05/08-
'Public Function estaRegistrada(rutaDLL As String) As Boolean
'    On Error GoTo Errores
'    Dim C As TLI.TLIApplication
'    Dim t As TLI.TypeLibInfo
'    Dim R As TLI.TypeLibInfo
'    Dim RetVal As Boolean
'
'    ' vas a devolver True a menos que hayaun error
'    RetVal = True
'
'    'Dim d As TLI.TLIApplication
'    'Set d = New TLI.TLIApplication
'    'd.TypeLibInfoFromFile (rutaDLL)
'
'
'    ' creas una aplicacion TLI
'    Set C = CreateObject("TLI.TLIApplication")
'
'    ' obtienes la informaci�n de la DLL del archivo
'    Set t = C.TypeLibInfoFromFile(rutaDLL)
'    If Err.Number Then
'        MsgBox "ERROR: " & Err.DESCRIPTION
'        RetVal = False
'    Else
'        ' ... con la informaci�n que obtuviste
'        Set R = C.TypeLibInfoFromRegistry(t.Guid, t.MajorVersion, t.MinorVersion, t.LCID)
'        If Err.Number Then
'            RetVal = False
'        End If
'    End If
'
'    estaRegistrada = RetVal
'    Exit Function
'Errores:
'    'MsgBox "ERROR EN eRRORES: " & Err.DESCRIPTION, vbCritical
'    RetVal = False
'End Function

Public Sub ImportarListadoPeticiones(ByRef vPeticionNroInterno() As Long)
    Dim vPeticionNroAsignado() As Long
    Dim TotalPeticiones As Long                 ' Nro. total de peticiones a procesar desde el archivo
    Dim sFile As String
    Dim i As Long
    
    Erase vPeticionNroInterno

    sFile = AbrirArchivo("Listado de peticiones a importar")
    If Len(sFile) > 0 Then
        If Dir(sFile, vbArchive) <> "" Then
            Call Status("Procesando... ")
            vPeticionNroAsignado = CargarArchivo(sFile)
            ' Obtengo los nros internos de petici�n a partir del listado de nro asignados de petici�n
            For i = 0 To UBound(vPeticionNroAsignado)
                Call Status("Procesando... " & i + 1)
                ReDim Preserve vPeticionNroInterno(i)
                If sp_GetUnaPeticionAsig(vPeticionNroAsignado(i)) Then
                    vPeticionNroInterno(i) = ClearNull(aplRST.Fields!pet_nrointerno)
                End If
                TotalPeticiones = TotalPeticiones + 1
            Next i
        End If
    End If
End Sub

Public Sub InicializarGruposDeControl()
    On Error GoTo Errores
    '********************************************************************************************************************************
    ' 1. Obtengo el grupo Homologador
    '********************************************************************************************************************************
    If sp_GetGrupoHomologacion("H") Then
        glHomologacionGrupo = ClearNull(aplRST.Fields!cod_grupo)
        glHomologacionSector = ClearNull(aplRST.Fields!cod_sector)
        glHomologacionGrupoNombre = ClearNull(aplRST.Fields!nom_grupo)
        glHomologacionSectorNombre = ClearNull(aplRST.Fields!nom_sector)
        glHomologaci�nNombreResponsable = ClearNull(aplRST.Fields!nom_recurso)
    End If

    '********************************************************************************************************************************
    ' 2. Obtengo el grupo de Seguridad Inform�tica
    '********************************************************************************************************************************
    If sp_GetGrupoHomologacion("1") Then
        glSegInfGrupo = ClearNull(aplRST.Fields!cod_grupo)
        glSegInfSector = ClearNull(aplRST.Fields!cod_sector)
        glSegInfGrupoNombre = ClearNull(aplRST.Fields!nom_grupo)
        glSegInfSectorNombre = ClearNull(aplRST.Fields!nom_sector)
        glSegInfNombreResponsable = ClearNull(aplRST.Fields!nom_recurso)
    End If
    
    '********************************************************************************************************************************
    ' 3. Obtengo el grupo de Tecnolog�a
    '********************************************************************************************************************************
    If sp_GetGrupoHomologacion("2") Then
        glTecnoGrupo = ClearNull(aplRST.Fields!cod_grupo)
        glTecnoSector = ClearNull(aplRST.Fields!cod_sector)
        glTecnoGrupoNombre = ClearNull(aplRST.Fields!nom_grupo)
        glTecnoSectorNombre = ClearNull(aplRST.Fields!nom_sector)
        glTecnoNombreResponsable = ClearNull(aplRST.Fields!nom_recurso)
    End If
    
   'GMT01 - INI - SE SACA PORQUE ESTA MAL ASIGNADAS LAS VARIABLES
    '********************************************************************************************************************************
    ' 4. Obtengo el grupo T�cnico
    '********************************************************************************************************************************
   ' If sp_GetGrupoHomologacion("S") Then
   '     glTecnoGrupotec = ClearNull(aplRST.Fields!cod_grupo)
   '     glTecnoSectortec = ClearNull(aplRST.Fields!cod_sector)
   '     glTecnoGrupoNombretec = ClearNull(aplRST.Fields!nom_grupo)
   '     glTecnoSectorNombretec = ClearNull(aplRST.Fields!nom_sector)
   '     glTecnoNombreResponsabletec = ClearNull(aplRST.Fields!nom_recurso)
   ' End If
   'GMT01 - FIN
    Exit Sub
Errores:
    If Err.Number = 3265 Then Resume Next
End Sub

Public Function EsGrupoEspecial(cod_grupo) As Boolean
    If cod_grupo = glHomologacionGrupo Or cod_grupo = glTecnoGrupo Or cod_grupo = glSegInfGrupo Then
        EsGrupoEspecial = True
    Else
        EsGrupoEspecial = False
    End If
End Function

Public Sub InicializarObjetosComunes()
    ' Carga los tipos de documentos que pueden ser adjuntados en una petici�n
    Dim i As Long
    
    i = 0: Erase vTipoDocumento
    If sp_GetDocumento(Null, Null) Then
        Do While Not aplRST.EOF
            ReDim Preserve vTipoDocumento(i)
            vTipoDocumento(i).Codigo = ClearNull(aplRST.Fields!cod_doc)
            vTipoDocumento(i).Nombre = ClearNull(aplRST.Fields!nom_doc)
            vTipoDocumento(i).Habilitado = IIf(ClearNull(aplRST.Fields!hab_doc) = "S", True, False)
            aplRST.MoveNext
            i = i + 1
            DoEvents
        Loop
    End If
    
End Sub

'GMT01 - INI
Private Function recupHabilitaciones() As Boolean
    
    'se recuperan los valores para saber que flujo de trabajo esta habilitado
    recupHabilitaciones = False
    Hab_RGyP = True
    
    'indica si ver la solapa de vinculados
    If sp_GetVarios("HAB_RGyP") Then
        Hab_RGyP = ClearNull(aplRST.Fields!var_numero)
    End If
    
    recupHabilitaciones = True
 End Function
'GMT01 - FIN


