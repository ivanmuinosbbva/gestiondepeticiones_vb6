Attribute VB_Name = "FuncionesPreferencias"
Option Explicit
Dim Hab_Tips As Integer

Sub DefaultPreferencias()
    ' Preferencias del usuario
    ' GENERAL
    If Len(GetSetting("GesPet", "Preferencias\General", "IniciarTips", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\General", "IniciarTips", "N"                          ' No iniciar los Tips
    If Len(GetSetting("GesPet", "Preferencias\General", "PromptActiveUsersOnly", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\General", "PromptActiveUsersOnly", "N"      ' Mostrar solo los recursos en estado activo
        
    ' CARGA DE HORAS
    If Len(GetSetting("GesPet", "Preferencias\Horas", "Feriados", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\Horas", "Feriados", "N"                 ' No marcar de manera diferencias los d�as feriados y no h�biles
    If Len(GetSetting("GesPet", "Preferencias\Horas", "Porcentajes", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\Horas", "Porcentajes", "N"           ' No mostrar los porcentajes de horas
    
    ' ADJUNTAR ARCHIVOS
    ' Clave: HKEY_CURRENT_USER\Software\VB and VBA Program Settings\GesPet\Preferencias\Adjuntos
    If Len(GetSetting("GesPet", "Preferencias\Adjuntos", "ModoSeleccion", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\Adjuntos", "ModoSeleccion", "N"     ' Normal
    ' HISTORIAL
    ' Clave: HKEY_CURRENT_USER\Software\VB and VBA Program Settings\GesPet\Preferencias\Historial
    ' Anchos de columnas
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth01", 1500)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth01", 1500
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth02", 2400)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth02", 2400
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth03", 1500)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth03", 1500
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth04", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth04", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth05", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth05", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth06", 3000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth06", 3000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth07", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth07", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth08", 1300)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth08", 1300
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth09", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth09", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth10", 1300)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth10", 1300
    
    ' Posicionamiento y tama�o de la ventana
    If Len(GetSetting("GesPet", "Preferencias\Historial\Posicion", "Top", 3495)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Posicion", "Top", 3495
    If Len(GetSetting("GesPet", "Preferencias\Historial\Posicion", "Left", 4005)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Posicion", "Left", 4005
    
    If Len(GetSetting("GesPet", "Preferencias\Historial\Tamanio", "Height", 7950)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Tamanio", "Height", 7950
    If Len(GetSetting("GesPet", "Preferencias\Historial\Tamanio", "Width", 11175)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Tamanio", "Width", 11175
End Sub

Sub Default_Preferencias_IDM()
    ' Comprueba que existan todas las entradas para los valores
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "ImportLastFile") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "ImportLastFile", "ImportFile.xls"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "ImportLastPath") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "ImportLastPath", glWRKDIR
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "PreAnalizeFile") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "PreAnalizeFile", "N"
    
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "TipoProyecto") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "TipoProyecto", "NULL"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "MacroProyecto") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "MacroProyecto", "1"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "SubProyecto") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "SubProyecto", "0"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "Categoria") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "Categoria", "1"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "Clase") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "Clase", "1"
End Sub

Sub DafaultExpor01()
    If GetSetting("GesPet", "Export01", "CHKpet_nroasignado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_nroasignado", "SI"
    If GetSetting("GesPet", "Export01", "FileExport") = "" Then SaveSetting "GesPet", "Export01", "FileExport", glWRKDIR & "EXPORTACION.XLS"
    If GetSetting("GesPet", "Export01", "CHKpet_nroasignado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_nroasignado", "SI"
    If GetSetting("GesPet", "Export01", "CHKcod_tipo_peticion") = "" Then SaveSetting "GesPet", "Export01", "CHKcod_tipo_peticion", "SI"
    If GetSetting("GesPet", "Export01", "CHKtitulo") = "" Then SaveSetting "GesPet", "Export01", "CHKtitulo", "SI"
    If GetSetting("GesPet", "Export01", "CHKdetalle") = "" Then SaveSetting "GesPet", "Export01", "CHKdetalle", "SI"
    If GetSetting("GesPet", "Export01", "CHKcaracte") = "" Then SaveSetting "GesPet", "Export01", "CHKcaracte", "SI"
    If GetSetting("GesPet", "Export01", "CHKmotivos") = "" Then SaveSetting "GesPet", "Export01", "CHKmotivos", "SI"
    If GetSetting("GesPet", "Export01", "CHKbenefic") = "" Then SaveSetting "GesPet", "Export01", "CHKbenefic", "SI"
    If GetSetting("GesPet", "Export01", "CHKrelacio") = "" Then SaveSetting "GesPet", "Export01", "CHKrelacio", "SI"
    If GetSetting("GesPet", "Export01", "CHKobserva") = "" Then SaveSetting "GesPet", "Export01", "CHKobserva", "SI"
    If GetSetting("GesPet", "Export01", "CHKprioridad") = "" Then SaveSetting "GesPet", "Export01", "CHKprioridad", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ing_comi") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ing_comi", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_fe_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_fe_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_situacion") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_situacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_sector") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_sector", "SI"
    If GetSetting("GesPet", "Export01", "CHKbpar") = "" Then SaveSetting "GesPet", "Export01", "CHKbpar", "SI"
    If GetSetting("GesPet", "Export01", "CHKsoli") = "" Then SaveSetting "GesPet", "Export01", "CHKsoli", "SI"
    If GetSetting("GesPet", "Export01", "CHKrefe") = "" Then SaveSetting "GesPet", "Export01", "CHKrefe", "SI"
    If GetSetting("GesPet", "Export01", "CHKsupe") = "" Then SaveSetting "GesPet", "Export01", "CHKsupe", "SI"
    If GetSetting("GesPet", "Export01", "CHKauto") = "" Then SaveSetting "GesPet", "Export01", "CHKauto", "SI"
    If GetSetting("GesPet", "Export01", "CHKhoraspresup") = "" Then SaveSetting "GesPet", "Export01", "CHKhoraspresup", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ini_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ini_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_fin_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_fin_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ini_real") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ini_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_fin_real") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_fin_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ini_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ini_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_fin_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_fin_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKrepla") = "" Then SaveSetting "GesPet", "Export01", "CHKrepla", "SI"
    If GetSetting("GesPet", "Export01", "CHKimportancia_nom") = "" Then SaveSetting "GesPet", "Export01", "CHKimportancia_nom", "SI"
    If GetSetting("GesPet", "Export01", "CHKcorp_local") = "" Then SaveSetting "GesPet", "Export01", "CHKcorp_local", "SI"
    If GetSetting("GesPet", "Export01", "CHKnom_orientacion") = "" Then SaveSetting "GesPet", "Export01", "CHKnom_orientacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_sector") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_sector", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_situacion") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_situacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_horaspresup") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_horaspresup", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_ini_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_fin_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_ini_real") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_fin_real") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_ini_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_fin_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_sector") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_sector", "SI"
    If GetSetting("GesPet", "Export01", "CHKprio_ejecuc") = "" Then SaveSetting "GesPet", "Export01", "CHKprio_ejecuc", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_situacion") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_situacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_horaspresup") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_horaspresup", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_ini_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_fin_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_ini_real") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_fin_real") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_ini_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_fin_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_info_adic") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_info_adic", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_recursos") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_recursos", "SI"
    If GetSetting("GesPet", "Export01", "CHKgantt") = "" Then SaveSetting "GesPet", "Export01", "CHKgantt", "NO"
    If GetSetting("GesPet", "Export01", "chkPetClass") = "" Then SaveSetting "GesPet", "Export01", "chkPetClass", "SI"
    If GetSetting("GesPet", "Export01", "chkPetImpTech") = "" Then SaveSetting "GesPet", "Export01", "chkPetImpTech", "SI"
    If GetSetting("GesPet", "Export01", "chkPetDocu") = "" Then SaveSetting "GesPet", "Export01", "chkPetDocu", "SI"
    If GetSetting("GesPet", "Export01", "chkHorasReales") = "" Then SaveSetting "GesPet", "Export01", "chkHorasReales", "SI"
    If GetSetting("GesPet", "Export01", "CHKpcf") = "" Then SaveSetting "GesPet", "Export01", "CHKpcf", "SI"
    If GetSetting("GesPet", "Export01", "CHKseguimN") = "" Then SaveSetting "GesPet", "Export01", "CHKseguimN", "SI"
    If GetSetting("GesPet", "Export01", "CHKseguimT") = "" Then SaveSetting "GesPet", "Export01", "CHKseguimT", "SI"
    If GetSetting("GesPet", "Export01", "chkFechaProduccion") = "" Then SaveSetting "GesPet", "Export01", "chkFechaProduccion", "SI"
    If GetSetting("GesPet", "Export01", "chkFechaSuspension") = "" Then SaveSetting "GesPet", "Export01", "chkFechaSuspension", "SI"
    If GetSetting("GesPet", "Export01", "chkMotivoSuspension") = "" Then SaveSetting "GesPet", "Export01", "chkMotivoSuspension", "SI"
    If GetSetting("GesPet", "Export01", "chkObservEstado") = "" Then SaveSetting "GesPet", "Export01", "chkObservEstado", "SI"
    If GetSetting("GesPet", "Export01", "chkPetRegulatorio") = "" Then SaveSetting "GesPet", "Export01", "chkPetRegulatorio", "SI"
    If GetSetting("GesPet", "Export01", "chkPetConfHomo") = "" Then SaveSetting "GesPet", "Export01", "chkPetConfHomo", "SI"
    If GetSetting("GesPet", "Export01", "chkPetProyectoIDM") = "" Then SaveSetting "GesPet", "Export01", "chkPetProyectoIDM", "SI"
    If GetSetting("GesPet", "Export01", "chkAutoFilter") = "" Then SaveSetting "GesPet", "Export01", "chkAutoFilter", "NO"
    If GetSetting("GesPet", "Export01", "chkAdjuntoLPO") = "" Then SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", "c:\gespet"
End Sub
