Attribute VB_Name = "spDocumento"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertDocumento(cod_doc, nom_doc, hab_doc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertDocumento"
        .Parameters.Append .CreateParameter("@cod_doc", adChar, adParamInput, 6, cod_doc)
        .Parameters.Append .CreateParameter("@nom_doc", adChar, adParamInput, 80, nom_doc)
        .Parameters.Append .CreateParameter("@hab_doc", adChar, adParamInput, 1, hab_doc)
        Set aplRST = .Execute
    End With
    sp_InsertDocumento = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_InsertDocumento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateDocumento(cod_doc, nom_doc, hab_doc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateDocumento"
        .Parameters.Append .CreateParameter("@cod_doc", adChar, adParamInput, 6, cod_doc)
        .Parameters.Append .CreateParameter("@nom_doc", adChar, adParamInput, 80, nom_doc)
        .Parameters.Append .CreateParameter("@hab_doc", adChar, adParamInput, 1, hab_doc)
        Set aplRST = .Execute
    End With
    sp_UpdateDocumento = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdateDocumento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteDocumento(cod_doc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteDocumento"
        .Parameters.Append .CreateParameter("@cod_doc", adChar, adParamInput, 6, cod_doc)
        Set aplRST = .Execute
    End With
    sp_DeleteDocumento = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteDocumento = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetDocumento(cod_doc, hab_doc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetDocumento"
        .Parameters.Append .CreateParameter("@cod_doc", adChar, adParamInput, 6, cod_doc)
        .Parameters.Append .CreateParameter("@hab_doc", adChar, adParamInput, 1, hab_doc)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetDocumento = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetDocumento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

