Attribute VB_Name = "spProyectoIDM"
' -001- a. FJS 16.03.2011 - IGM: segunda parte.
' -002- a. FJS 10.03.2015 - Agregado: se crean dos nuevos atributos (exposición y clasificación) para poder clasificar los hitos cargados según una tabla de referencia.
' -003- a. FJS 30.11.2015 - Modificación: se agrega el cambio en el motor de cursos porque en Windows Server 2012 con ADO 6.3 se genera el siguiente error
'                           y no adjunta los datos: Multiple step operation generated errors. Check each status value.
' -004- a. FJS 12.01.2016 - Modificación: se cambia el tipo de datos de empresa de char a integer.

Option Explicit

Function sp_InsertProyectoIDM(codigo, subcodigo, subsubcodigo, Titulo, categoria, clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertProyectoIDM"
      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , CLng(Val(codigo)))
      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , CLng(Val(subcodigo)))
      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , CLng(Val(subsubcodigo)))
      .Parameters.Append .CreateParameter("@ProjNom", adVarChar, adParamInput, 100, ClearNull(Mid(Titulo, 1, 100)))
      .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , CLng(Val(categoria)))
      .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , CLng(Val(clase)))
      Set aplRST = .Execute
    End With
    sp_InsertProyectoIDM = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoIDM = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDM(codigo, subcodigo, subsubcodigo, Titulo, categoria, clase, plan_tyo, empresa, area, clas3, semaphore, cod_direccion, cod_gerencia, cod_sector, cod_direccion_p, cod_gerencia_p, cod_sector_p, cod_grupo_p, avance, fe_inicio, fe_fin, fe_finreprog, fe_ult_nove, tipo_proyecto, totalhs_gerencia, totalhs_sector, totalreplan, codigo_gps) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDM"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , CLng(Val(codigo)))
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , CLng(Val(subcodigo)))
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , CLng(Val(subsubcodigo)))
        .Parameters.Append .CreateParameter("@ProjNom", adVarChar, adParamInput, 100, ClearNull(Mid(Titulo, 1, 100)))
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , CLng(Val(categoria)))
        .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , CLng(Val(clase)))
        .Parameters.Append .CreateParameter("@plan_tyo", adChar, adParamInput, 1, plan_tyo)
        '.Parameters.Append .CreateParameter("@empresa", adChar, adParamInput, 1, empresa)      ' del -004- a.
        .Parameters.Append .CreateParameter("@empresa", adInteger, adParamInput, , IIf(IsNull(empresa) Or empresa = "", Null, empresa))      ' add -004- a.
        .Parameters.Append .CreateParameter("@area", adChar, adParamInput, 1, area)
        .Parameters.Append .CreateParameter("@clas3", adChar, adParamInput, 1, clas3)
        .Parameters.Append .CreateParameter("@semaphore", adChar, adParamInput, 1, semaphore)
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_direccion_p", adChar, adParamInput, 8, cod_direccion_p)
        .Parameters.Append .CreateParameter("@cod_gerencia_p", adChar, adParamInput, 8, cod_gerencia_p)
        .Parameters.Append .CreateParameter("@cod_sector_p", adChar, adParamInput, 8, cod_sector_p)
        .Parameters.Append .CreateParameter("@cod_grupo_p", adChar, adParamInput, 8, cod_grupo_p)
        .Parameters.Append .CreateParameter("@avance", adInteger, adParamInput, , avance)
        .Parameters.Append .CreateParameter("@fe_inicio", SQLDdateType, adParamInput, , IIf(fe_inicio = "", Null, fe_inicio))
        .Parameters.Append .CreateParameter("@fe_fin", SQLDdateType, adParamInput, , IIf(fe_fin = "", Null, fe_fin))
        .Parameters.Append .CreateParameter("@fe_finreprog", SQLDdateType, adParamInput, , IIf(fe_finreprog = "", Null, fe_finreprog))
        .Parameters.Append .CreateParameter("@fe_ult_nove", SQLDdateType, adParamInput, , IIf(fe_ult_nove = "", Null, fe_ult_nove))
        .Parameters.Append .CreateParameter("@tipo_proyecto", adChar, adParamInput, 1, tipo_proyecto)
        .Parameters.Append .CreateParameter("@totalhs_gerencia", adInteger, adParamInput, , CLng(Val(totalhs_gerencia)))
        .Parameters.Append .CreateParameter("@totalhs_sector", adInteger, adParamInput, , CLng(Val(totalhs_sector)))
        .Parameters.Append .CreateParameter("@totalreplan", adInteger, adParamInput, , CLng(Val(totalreplan)))
        .Parameters.Append .CreateParameter("@codigo_gps", adChar, adParamInput, 11, codigo_gps)
        Set aplRST = .Execute
    End With
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    sp_UpdateProyectoIDM = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDM = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDM2(codigo, subcodigo, subsubcodigo, Titulo, categoria, clase, cod_estado, marca, plan_tyo, empresa, area, clas3, semaphore, cod_direccion, cod_gerencia, cod_sector, cod_direccion_p, cod_gerencia_p, cod_sector_p, cod_grupo_p, avance, fe_inicio, fe_fin, fe_finreprog, fe_ult_nove) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDM2"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , CLng(Val(codigo)))
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , CLng(Val(subcodigo)))
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , CLng(Val(subsubcodigo)))
        .Parameters.Append .CreateParameter("@ProjNom", adVarChar, adParamInput, 100, ClearNull(Mid(Titulo, 1, 100)))
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , CLng(Val(categoria)))
        .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , CLng(Val(clase)))
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, IIf(cod_estado = "", Null, cod_estado))
        .Parameters.Append .CreateParameter("@marca", adChar, adParamInput, 1, IIf(marca = "", Null, marca))
        .Parameters.Append .CreateParameter("@plan_tyo", adChar, adParamInput, 1, IIf(plan_tyo = "", Null, plan_tyo))
        '.Parameters.Append .CreateParameter("@empresa", adChar, adParamInput, 1, IIf(empresa = "", Null, empresa))
        .Parameters.Append .CreateParameter("@empresa", adInteger, adParamInput, , CLng(Val(empresa)))
        .Parameters.Append .CreateParameter("@area", adChar, adParamInput, 1, IIf(area = "", Null, area))
        .Parameters.Append .CreateParameter("@clas3", adChar, adParamInput, 1, IIf(clas3 = "", Null, ClearNull(clas3)))
        .Parameters.Append .CreateParameter("@semaphore", adChar, adParamInput, 1, IIf(semaphore = "", Null, semaphore))
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, IIf(cod_direccion = "", Null, cod_direccion))
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, IIf(cod_gerencia = "", Null, cod_gerencia))
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, IIf(cod_sector = "", Null, cod_sector))
        .Parameters.Append .CreateParameter("@cod_direccion_p", adChar, adParamInput, 8, IIf(cod_direccion_p = "", Null, cod_direccion_p))
        .Parameters.Append .CreateParameter("@cod_gerencia_p", adChar, adParamInput, 8, IIf(cod_gerencia_p = "", Null, cod_gerencia_p))
        .Parameters.Append .CreateParameter("@cod_sector_p", adChar, adParamInput, 8, IIf(cod_sector_p = "", Null, cod_sector_p))
        .Parameters.Append .CreateParameter("@cod_grupo_p", adChar, adParamInput, 8, IIf(cod_grupo_p = "", Null, cod_grupo_p))
        .Parameters.Append .CreateParameter("@avance", adInteger, adParamInput, , CLng(Val(avance)))
        .Parameters.Append .CreateParameter("@fe_inicio", SQLDdateType, adParamInput, , IIf(fe_inicio = "", Null, fe_inicio))
        .Parameters.Append .CreateParameter("@fe_fin", SQLDdateType, adParamInput, , IIf(fe_fin = "", Null, fe_fin))
        .Parameters.Append .CreateParameter("@fe_finreprog", SQLDdateType, adParamInput, , IIf(fe_finreprog = "", Null, fe_finreprog))
        .Parameters.Append .CreateParameter("@fe_ult_nove", SQLDdateType, adParamInput, , IIf(fe_ult_nove = "", Null, fe_ult_nove))
        Set aplRST = .Execute
    End With
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    sp_UpdateProyectoIDM2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDM2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteProyectoIDM(codigo, subcodigo, subsubcodigo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteProyectoIDM"
      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , CLng(Val(codigo)))
      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , CLng(Val(subcodigo)))
      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , CLng(Val(subsubcodigo)))
      Set aplRST = .Execute
    End With
    sp_DeleteProyectoIDM = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoIDM = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyIDMCategoria(codigo, Descripcion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyIDMCategoria"
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , IIf(IsNull(codigo), Null, codigo))
        .Parameters.Append .CreateParameter("@ProjCatNom", adVarChar, adParamInput, 100, ClearNull(Descripcion))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyIDMCategoria = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyIDMCategoria = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertProyectoCategoria(ProjCatId, ProjCatNom) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoCategoria"
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , ProjCatId)
        .Parameters.Append .CreateParameter("@ProjCatNom", adChar, adParamInput, 100, ProjCatNom)
        Set aplRST = .Execute
    End With
    sp_InsertProyectoCategoria = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoCategoria = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoCategoria(ProjCatId, ProjCatNom) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoCategoria"
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , ProjCatId)
        .Parameters.Append .CreateParameter("@ProjCatNom", adChar, adParamInput, 100, ProjCatNom)
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoCategoria = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoCategoria = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteProyectoCategoria(ProjCatId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoCategoria"
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , ProjCatId)
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoCategoria = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoCategoria = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertProyectoClase(ProjCatId, ProjClaseId, ProjClaseNom) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoClase"
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , ProjCatId)
        .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , ProjClaseId)
        .Parameters.Append .CreateParameter("@ProjClaseNom", adChar, adParamInput, 100, ProjClaseNom)
        Set aplRST = .Execute
    End With
    sp_InsertProyectoClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoClase(ProjCatId, ProjClaseId, ProjClaseNom) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoClase"
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , ProjCatId)
        .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , ProjClaseId)
        .Parameters.Append .CreateParameter("@ProjClaseNom", adChar, adParamInput, 100, ProjClaseNom)
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteProyectoClase(ProjClaseId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoClase"
        .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , ProjClaseId)
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyIDMClase(ProjCatId, ProjClaseId, ProjClaseNom) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyIDMClase"
        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , IIf(IsNull(ProjCatId) Or ProjCatId = "", Null, ProjCatId))
        .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , IIf(IsNull(ProjClaseId) Or ProjClaseId = "", Null, ProjClaseId))
        .Parameters.Append .CreateParameter("@ProjClaseNom", adVarChar, adParamInput, 100, ClearNull(ProjClaseNom))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyIDMClase = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyIDMClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDM(ProjId, ProjSubId, ProjSubSId, projnom, Estados, flg_area, cod_nivel, cod_area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDM"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@ProjNom", adVarChar, adParamInput, 100, projnom)
        .Parameters.Append .CreateParameter("@cod_estado", adVarChar, adParamInput, 250, IIf(Estados = "NULL", Null, Estados))
        .Parameters.Append .CreateParameter("@flg_area", adVarChar, adParamInput, 4, IIf(cod_nivel = "NULL", Null, flg_area))
        .Parameters.Append .CreateParameter("@cod_nivel", adVarChar, adParamInput, 4, IIf(cod_nivel = "NULL", Null, cod_nivel))
        .Parameters.Append .CreateParameter("@cod_area", adVarChar, adParamInput, 8, IIf(cod_area = "NULL", Null, cod_area))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyectoIDM = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDM = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDM2(ProjId, ProjSubId, ProjSubSId, projnom, tipo, nivel) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDM2"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@ProjNom", adVarChar, adParamInput, 100, projnom)
        .Parameters.Append .CreateParameter("@tipo", adVarChar, adParamInput, 1, tipo)
        .Parameters.Append .CreateParameter("@nivel", adInteger, adParamInput, , IIf(IsNull(nivel), Null, nivel))
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyectoIDM2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDM2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMAlertaMayor(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetProyectoIDMAlertaMayor"
      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyectoIDMAlertaMayor = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMAlertaMayor = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMNivel(nivel, ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetProyectoIDMNivel"
      .Parameters.Append .CreateParameter("@Nivel", adChar, adParamInput, 1, nivel)
      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyectoIDMNivel = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMNivel = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetNombreProyectoIDM(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetNombreProyectoIDM"
      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetNombreProyectoIDM = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetNombreProyectoIDM = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

' Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~
Function sp_UpdateProyectoIDMField(ProjId, ProjSubId, ProjSubSId, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMField"
        .Parameters.Append .CreateParameter("@projId", adInteger, adParamInput, , CLng(Val(ProjId)))
        .Parameters.Append .CreateParameter("@projSubId", adInteger, adParamInput, , CLng(Val(ProjSubId)))
        .Parameters.Append .CreateParameter("@projSubsId", adInteger, adParamInput, , CLng(Val(ProjSubSId)))
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, IIf(valortxt = "", Null, valortxt))
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetEstadoProyectoIDM(ProjId, ProjSubId, ProjSubSId) As String
    Dim cod_estado As String
    Dim rsAux As ADODB.Recordset
    
    Set rsAux = Nothing
    
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN.ConnectionString    ' add -001- a.
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetEstadoProyectoIDM"
        .Parameters.Append .CreateParameter("@projid", adInteger, adParamInput, , CLng(Val(ProjId)))
        .Parameters.Append .CreateParameter("@projsubid", adInteger, adParamInput, , CLng(Val(ProjSubId)))
        .Parameters.Append .CreateParameter("@projsubsid", adInteger, adParamInput, , CLng(Val(ProjSubSId)))
        .Parameters.Append .CreateParameter("@p_cod_estado", adChar, adParamOutput, 6)
        .Execute
        cod_estado = .Parameters(3).Value
    End With
    sp_GetEstadoProyectoIDM = cod_estado
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(rsAux, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetEstadoProyectoIDM = ""
End Function

'Function sp_GetProyectoIDM(codigo, subcodigo, subsubcodigo, titulo, categoria, clase) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_GetProyectoIDM"
'      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , CLng(Val(codigo)))
'      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , CLng(Val(subcodigo)))
'      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , CLng(Val(subsubcodigo)))
'      .Parameters.Append .CreateParameter("@ProjNom", adVarChar, adParamInput, 100, ClearNull(titulo))
'      .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , CLng(Val(categoria)))
'      .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , CLng(Val(clase)))
'      Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetProyectoIDM = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err))
'    sp_GetProyectoIDM = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function

Function sp_InsertProyectoIDMAlertas(ProjId, ProjSubId, ProjSubSId, alert_tipo, alert_desc, accion_id, alert_fe_ini, alert_fe_fin, responsables, observaciones, Status) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoIDMAlertas"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@alert_tipo", adChar, adParamInput, 1, alert_tipo)
        .Parameters.Append .CreateParameter("@alert_desc", adVarChar, adParamInput, 180, Mid(alert_desc, 1, 180))
        .Parameters.Append .CreateParameter("@accion_id", adVarChar, adParamInput, 180, Mid(accion_id, 1, 180))
        .Parameters.Append .CreateParameter("@alert_fe_ini", SQLDdateType, adParamInput, , IIf(alert_fe_ini = "", Null, ClearNull(alert_fe_ini)))
        .Parameters.Append .CreateParameter("@alert_fe_fin", SQLDdateType, adParamInput, , IIf(alert_fe_fin = "", Null, ClearNull(alert_fe_fin)))
        .Parameters.Append .CreateParameter("@responsables", adVarChar, adParamInput, 180, Mid(responsables, 1, 180))
        .Parameters.Append .CreateParameter("@observaciones", adVarChar, adParamInput, 180, Mid(observaciones, 180))
        .Parameters.Append .CreateParameter("@status", adChar, adParamInput, 1, Mid(Status, 1, 1))
        Set aplRST = .Execute
    End With
    sp_InsertProyectoIDMAlertas = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoIDMAlertas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMAlertas(ProjId, ProjSubId, ProjSubSId, alert_itm, alert_tipo, alert_desc, accion_id, alert_fe_ini, alert_fe_fin, responsables, observaciones, Status, fec_resolucion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMAlertas"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@alert_itm", adInteger, adParamInput, , alert_itm)
        .Parameters.Append .CreateParameter("@alert_tipo", adChar, adParamInput, 1, alert_tipo)
        .Parameters.Append .CreateParameter("@alert_desc", adChar, adParamInput, 180, alert_desc)
        .Parameters.Append .CreateParameter("@accion_id", adChar, adParamInput, 180, accion_id)
        .Parameters.Append .CreateParameter("@alert_fe_ini", SQLDdateType, adParamInput, , IIf(alert_fe_ini = "", Null, alert_fe_ini))
        .Parameters.Append .CreateParameter("@alert_fe_fin", SQLDdateType, adParamInput, , IIf(alert_fe_fin = "", Null, alert_fe_fin))
        .Parameters.Append .CreateParameter("@responsables", adChar, adParamInput, 180, responsables)
        .Parameters.Append .CreateParameter("@observaciones", adChar, adParamInput, 180, observaciones)
        .Parameters.Append .CreateParameter("@status", adChar, adParamInput, 1, Status)
        .Parameters.Append .CreateParameter("@fec_resolucion", SQLDdateType, adParamInput, , IIf(fec_resolucion = "", Null, fec_resolucion))
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMAlertas = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMAlertas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteProyectoIDMAlertas(ProjId, ProjSubId, ProjSubSId, alert_item) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoIDMAlertas"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@alert_item", adInteger, adParamInput, , alert_item)
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoIDMAlertas = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoIDMAlertas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMAlertas(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMAlertas"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMAlertas = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMAlertas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertProyectoIDMHitos(ProjId, ProjSubId, ProjSubSId, hito_nombre, hito_descripcion, hito_fe_ini, hito_fe_fin, hito_estado, cod_nivel, cod_area, orden, hito_expos, hito_id) As Boolean      ' upd -002- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoIDMHitos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@hito_nombre", adChar, adParamInput, 100, Mid(hito_nombre, 1, 100))
        .Parameters.Append .CreateParameter("@hito_descripcion", adChar, adParamInput, 255, Mid(hito_descripcion, 1, 255))
        .Parameters.Append .CreateParameter("@hito_fe_ini", adChar, adParamInput, 20, IIf(hito_fe_ini = "" Or IsNull(hito_fe_ini), Null, Format(hito_fe_ini, "mm/dd/yyyy hh:MM:ss")))
        .Parameters.Append .CreateParameter("@hito_fe_fin", adChar, adParamInput, 20, IIf(hito_fe_fin = "" Or IsNull(hito_fe_fin), Null, Format(hito_fe_fin, "mm/dd/yyyy hh:MM:ss")))
        .Parameters.Append .CreateParameter("@hito_estado", adChar, adParamInput, 6, Mid(hito_estado, 1, 6))
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, Mid(cod_nivel, 1, 4))
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, Mid(cod_area, 1, 8))
        .Parameters.Append .CreateParameter("@orden", adInteger, adParamInput, , IIf(orden = "", Null, orden))
        '{ add -002- a.
        .Parameters.Append .CreateParameter("@hito_expos", adChar, adParamInput, 1, IIf(IsNull(hito_expos) Or hito_expos = "", Null, hito_expos))
        .Parameters.Append .CreateParameter("@hito_id", adInteger, adParamInput, , IIf(IsNull(hito_id) Or hito_id = "", Null, hito_id))
        '}
        Set aplRST = .Execute
    End With
    sp_InsertProyectoIDMHitos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoIDMHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMHitos(ProjId, ProjSubId, ProjSubSId, hit_nrointerno, hito_nombre, hito_descripcion, hito_fe_ini, hito_fe_fin, hito_estado, cod_nivel, cod_area, orden, hito_expos, hito_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMHitos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@hit_nrointerno", adInteger, adParamInput, , hit_nrointerno)
        .Parameters.Append .CreateParameter("@hito_nombre", adChar, adParamInput, 100, hito_nombre)
        .Parameters.Append .CreateParameter("@hito_descripcion", adChar, adParamInput, 255, hito_descripcion)
        .Parameters.Append .CreateParameter("@hito_fe_ini", adChar, adParamInput, 20, IIf(IsNull(hito_fe_ini) Or hito_fe_ini = "", Null, Format(hito_fe_ini, "mm/dd/yyyy hh:MM:ss")))
        .Parameters.Append .CreateParameter("@hito_fe_fin", adChar, adParamInput, 20, IIf(IsNull(hito_fe_fin) Or hito_fe_fin = "", Null, Format(hito_fe_fin, "mm/dd/yyyy hh:MM:ss")))
        .Parameters.Append .CreateParameter("@hito_estado", adChar, adParamInput, 6, hito_estado)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        .Parameters.Append .CreateParameter("@orden", adInteger, adParamInput, , IIf(orden = "", Null, orden))
        '{ add -002- a.
        .Parameters.Append .CreateParameter("@hito_expos", adChar, adParamInput, 1, IIf(IsNull(hito_expos) Or hito_expos = "NULL", Null, hito_expos))
        .Parameters.Append .CreateParameter("@hito_id", adInteger, adParamInput, , IIf(IsNull(hito_id) Or hito_id = "", Null, hito_id))
        '}
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMHitos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMHitosField(ProjId, ProjSubId, ProjSubSId, hit_nrointerno, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST1.Close
    Set aplRST1 = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMHitosField"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@hit_nrointerno", adInteger, adParamInput, , hit_nrointerno)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 30, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 255, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST1 = .Execute
    End With
    sp_UpdateProyectoIDMHitosField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST1, Err, aplCONN))
    sp_UpdateProyectoIDMHitosField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'Function sp_UpdateProyectoIDMHitosField(ProjId, ProjSubId, ProjSubSId, hit_nrointerno, campo, valortxt, valordate, valornum) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_UpdateProyectoIDMHitosField"
'        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
'        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
'        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
'        .Parameters.Append .CreateParameter("@hit_nrointerno", adInteger, adParamInput, , hit_nrointerno)
'        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 30, campo)
'        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 80, valortxt)
'        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
'        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
'        Set aplRST = .Execute
'    End With
'    sp_UpdateProyectoIDMHitosField = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_UpdateProyectoIDMHitosField = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function

Function sp_DeleteProyectoIDMHitos(ProjId, ProjSubId, ProjSubSId, hit_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoIDMHitos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@hit_nrointerno", adInteger, adParamInput, , hit_nrointerno)
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoIDMHitos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoIDMHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMHitos(ProjId, ProjSubId, ProjSubSId, hito_nombre, hit_nrointerno, Exposicion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMHitos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@hito_nombre", adChar, adParamInput, 100, IIf(IsNull(hito_nombre), Null, ClearNull(hito_nombre)))
        .Parameters.Append .CreateParameter("@hit_nrointerno", adInteger, adParamInput, , hit_nrointerno)
        .Parameters.Append .CreateParameter("@exposicion", adChar, adParamInput, 1, IIf(IsNull(Exposicion), Null, ClearNull(Exposicion)))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMHitos = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMHitos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMHitosOrden(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMHitosOrden"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMHitosOrden = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMHitosOrden = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMExport(ProjId, ProjSubId, ProjSubSId, Hitos, Exposicion, projnom, PlanTyO, Activos, flg_area, cod_nivel, cod_area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMExport"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , IIf(ProjId = "", Null, ProjId))
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , IIf(ProjSubId = "", Null, ProjSubId))
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , IIf(ProjSubSId = "", Null, ProjSubSId))
        .Parameters.Append .CreateParameter("@Hitos", adChar, adParamInput, 1, Hitos)
        .Parameters.Append .CreateParameter("@HitosExpos", adChar, adParamInput, 1, Exposicion)
        .Parameters.Append .CreateParameter("@projnom", adChar, adParamInput, 100, IIf(projnom = "", Null, projnom))
        .Parameters.Append .CreateParameter("@PlanTyO", adChar, adParamInput, 1, IIf(PlanTyO = "T", Null, PlanTyO))
        .Parameters.Append .CreateParameter("@Activos", adChar, adParamInput, 100, IIf(Activos = "NULL", Null, Activos))
        .Parameters.Append .CreateParameter("@flg_area", adChar, adParamInput, 4, IIf(flg_area = "PROP", Null, flg_area))
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, IIf(cod_nivel = "NULL", Null, cod_nivel))
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, IIf(cod_area = "NULL", Null, cod_area))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMExport = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMExport = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'Function sp_GetProyectoIDMHitos2(ProjId, ProjSubId, ProjSubSId) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetProyectoIDMHitos2"
'        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
'        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
'        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
'        Set aplRST = .Execute
'    End With
'    If Not aplRST.EOF Then
'        sp_GetProyectoIDMHitos2 = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetProyectoIDMHitos2 = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function

Function sp_InsertProyectoIDMNovedades(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo, mem_texto, cod_usuario) As Boolean
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim rsAux As ADODB.Recordset
    Dim dFechaHora As Date

    Set rsAux = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoIDMNovedades"
        .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1)
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@mem_fecha", adChar, adParamInput, 20)
        '.Parameters.Append .CreateParameter("@mem_fecha", SQLDdateType, adParamInput, mem_fecha)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput)
        .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255)
        .Parameters.Append .CreateParameter("@cod_usuario", adChar, adParamInput, 10)
    End With
    sp_InsertProyectoIDMNovedades = True
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(mem_texto, 255)
        With objCommand
            If Len(mem_texto) > 255 Then
                .Parameters(0).Value = "N"
            Else
                bFlg = False
                .Parameters(0).Value = "S"
            End If
            .Parameters(1).Value = CLng(Val(ProjId))
            .Parameters(2).Value = CLng(Val(ProjSubId))
            .Parameters(3).Value = CLng(Val(ProjSubSId))
            .Parameters(4).Value = Format(mem_fecha, "yyyymmdd hh:MM:ss")
            .Parameters(5).Value = mem_campo
            .Parameters(6).Value = CInt(Secuencia)
            .Parameters(7).Value = TmpTexto & ""
            .Parameters(8).Value = cod_usuario & ""
            Set rsAux = .Execute
        End With
        Secuencia = Secuencia + 1
        mem_texto = Mid(mem_texto, 256)
    Loop
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(rsAux, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_InsertProyectoIDMNovedades = False
End Function

Function sp_UpdateProyectoIDMNovedades(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo, mem_texto, cod_usuario) As Boolean
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim rsAux As ADODB.Recordset
    Dim dFechaHora As Date
    
    Set rsAux = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMNovedades"
        .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1)
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@mem_fecha", adChar, adParamInput, 20)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput)
        .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255)
        .Parameters.Append .CreateParameter("@cod_usuario", adChar, adParamInput, 10)
    End With
    
    sp_UpdateProyectoIDMNovedades = True
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(mem_texto, 255)
        With objCommand
            If Len(mem_texto) > 255 Then
                .Parameters(0).Value = "N"
            Else
                bFlg = False
                .Parameters(0).Value = "S"
            End If
            .Parameters(1).Value = CLng(Val(ProjId))
            .Parameters(2).Value = CLng(Val(ProjSubId))
            .Parameters(3).Value = CLng(Val(ProjSubSId))
            .Parameters(4).Value = Format(mem_fecha, "yyyymmdd hh:MM:ss")
            .Parameters(5).Value = mem_campo
            .Parameters(6).Value = CInt(Secuencia)
            .Parameters(7).Value = TmpTexto & ""
            .Parameters(8).Value = cod_usuario
            Set rsAux = .Execute
        End With
        Secuencia = Secuencia + 1
        mem_texto = Mid(mem_texto, 256)
    Loop
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(rsAux, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_UpdateProyectoIDMNovedades = False
End Function

Function sp_UpdateProyectoIDMNovedades2(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo, mem_secuencia, mem_texto, cod_usuario) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMNovedades"
        .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1, "N")
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_fecha", SQLDdateType, adParamInput, , mem_fecha)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
        .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255, Mid(mem_texto, 1, 255))
        .Parameters.Append .CreateParameter("@cod_usuario", adChar, adParamInput, 10, cod_usuario)
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMNovedades2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_UpdateProyectoIDMNovedades2 = False
End Function

Function sp_DeleteProyectoIDMNovedades(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo, mem_secuencia, modo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoIDMNovedades"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_fecha", adChar, adParamInput, 20, IIf(IsNull(mem_fecha), Null, Format(mem_fecha, "yyyymmdd hh:MM:ss")))
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, IIf(IsNull(mem_campo), Null, mem_campo))
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , IIf(IsNull(mem_secuencia), Null, mem_secuencia))
        .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 4, IIf(IsNull(modo), Null, modo))
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoIDMNovedades = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoIDMNovedades = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMNovedades(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo, mem_secuencia) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMNovedades"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_fecha", SQLDdateType, adParamInput, , mem_fecha)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMNovedades = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMNovedades = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMNovedades2(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMNovedades2"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_fecha", SQLDdateType, adParamInput, , IIf(IsNull(mem_fecha), Null, mem_fecha))
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMNovedades2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMNovedades2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMNovedades3(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo, mem_secuencia) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMNovedades"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        '.Parameters.Append .CreateParameter("@mem_fecha", SQLDdateType, adParamInput, , mem_fecha)
        .Parameters.Append .CreateParameter("@mem_fecha", adChar, adParamInput, 20, IIf(IsNull(mem_fecha) Or mem_fecha = "", Null, Format(mem_fecha, "mm/dd/yyyy hh:MM:ss")))
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        .Parameters.Append .CreateParameter("@mem_secuencia", adInteger, adParamInput, , IIf(IsNull(mem_secuencia) Or mem_secuencia = "", Null, mem_secuencia))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        bGetMemo = True
    End If
    If bGetMemo Then
        Do While Not aplRST.EOF
            'sGetMemo = sGetMemo & ClearNull(aplRST.Fields!mem_texto) & IIf(ClearNull(aplRST.Fields!mem_secuencia) = "0", Chr(10), "")
            If ClearNull(aplRST.Fields!mem_accion) <> "DEL" Then
                If InStr(1, ClearNull(aplRST.Fields!mem_texto), Chr(13), vbTextCompare) > 0 Then
                    sGetMemo = sGetMemo & ReplaceString(ClearNull(aplRST.Fields!mem_texto), 13, "")
                Else
                    sGetMemo = sGetMemo & ClearNull(aplRST.Fields!mem_texto)
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
        aplRST.Close
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetProyectoIDMNovedades3 = sGetMemo
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMNovedades3 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetProyectoIDMNovedades3 = ""
End Function

Function sp_InsertProyectoIDMMemo(ProjId, ProjSubId, ProjSubSId, mem_campo, mem_texto) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoIDMMemo"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255, mem_texto)
        Set aplRST = .Execute
    End With
    sp_InsertProyectoIDMMemo = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoIDMMemo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMMemo(ProjId, ProjSubId, ProjSubSId, mem_campo, mem_texto) As Boolean
    On Error Resume Next
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim dFechaHora As Date
    
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMMemo"
        .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1)
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput)
        .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255)
    End With
    sp_UpdateProyectoIDMMemo = True
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(mem_texto, 255)
        With objCommand
            If Len(mem_texto) > 255 Then
                .Parameters(0).Value = "N"
            Else
                bFlg = False
                .Parameters(0).Value = "S"
            End If
            .Parameters(1).Value = CLng(Val(ProjId))
            .Parameters(2).Value = CLng(Val(ProjSubId))
            .Parameters(3).Value = CLng(Val(ProjSubSId))
            .Parameters(4).Value = mem_campo
            .Parameters(5).Value = CInt(Secuencia)
            .Parameters(6).Value = TmpTexto & ""
            Set aplRST = .Execute
        End With
        Secuencia = Secuencia + 1
        mem_texto = Mid(mem_texto, 256)
    Loop
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMMemo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMMemo2(ProjId, ProjSubId, ProjSubSId, mem_campo, mem_secuencia, mem_texto) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMMemo"
        .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1, Null)
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
        .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255, Mid(mem_texto, 1, 255))
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMMemo2 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMMemo2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteProyectoIDMMemo(ProjId, ProjSubId, ProjSubSId, mem_campo, mem_secuencia) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoIDMMemo"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , IIf(IsNull(mem_secuencia), Null, mem_secuencia))
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoIDMMemo = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoIDMMemo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMMemo(ProjId, ProjSubId, ProjSubSId, mem_campo, mem_secuencia) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMMemo"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput, , mem_secuencia)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        bGetMemo = True
    End If
    If bGetMemo Then
        Do While Not aplRST.EOF
            'If InStr(1, ClearNull(aplRST.Fields!mem_texto), Chr(13), vbTextCompare) > 0 Then
            '    sGetMemo = sGetMemo & ReplaceString(ClearNull(aplRST.Fields!mem_texto), 13, "")
            'Else
            '    sGetMemo = sGetMemo & ClearNull(aplRST.Fields!mem_texto)
            'End If
            sGetMemo = sGetMemo & ClearNull(aplRST.Fields!mem_texto)
            aplRST.MoveNext
            DoEvents
        Loop
        aplRST.Close
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetProyectoIDMMemo = sGetMemo
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetProyectoIDMMemo = ""
End Function

Function sp_GetProyectoIDMNovedadesMemo(ProjId, ProjSubId, ProjSubSId, mem_fecha, mem_campo) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMNovedadesMemo"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@mem_fecha", adChar, adParamInput, 20, Format(mem_fecha, "yyyymmdd hh:MM:ss"))
        .Parameters.Append .CreateParameter("@mem_campo", adChar, adParamInput, 10, mem_campo)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        bGetMemo = True
    End If
    If bGetMemo Then
        Do While Not aplRST.EOF
            sGetMemo = sGetMemo & ClearNull(aplRST.Fields!mem_texto)
            aplRST.MoveNext
            DoEvents
        Loop
        aplRST.Close
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetProyectoIDMNovedadesMemo = sGetMemo
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetProyectoIDMNovedadesMemo = ""
End Function

'Function sp_UpdateProyectoIDM(codigo, subcodigo, subsubcodigo, titulo, categoria, clase, marca) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_UpdateProyectoIDM"
'        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , CLng(Val(codigo)))
'        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , CLng(Val(subcodigo)))
'        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , CLng(Val(subsubcodigo)))
'        .Parameters.Append .CreateParameter("@ProjNom", adVarChar, adParamInput, 100, ClearNull(Mid(titulo, 1, 100)))
'        .Parameters.Append .CreateParameter("@ProjCatId", adInteger, adParamInput, , CLng(Val(categoria)))
'        .Parameters.Append .CreateParameter("@ProjClaseId", adInteger, adParamInput, , CLng(Val(clase)))
'        .Parameters.Append .CreateParameter("@marca", adChar, adParamInput, 1, marca)
'        Set aplRST = .Execute
'    End With
'    If aplCONN.Errors.Count > 0 Then
'        GoTo Err_SP
'    End If
'    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
'        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
'            GoTo Err_SP
'        End If
'    End If
'    sp_UpdateProyectoIDM = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err))
'    sp_UpdateProyectoIDM = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function

Function sp_InsertProyectoIDMResponsables(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, propietaria) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoIDMResp"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        .Parameters.Append .CreateParameter("@propietaria", adChar, adParamInput, 1, propietaria)
        Set aplRST = .Execute
    End With
    sp_InsertProyectoIDMResponsables = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoIDMResponsables = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMResponsablesField(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMResponsablesField"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMResponsablesField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMResponsablesField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteProyectoIDMResponsables(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoIDMResp"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoIDMResponsables = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoIDMResponsables = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertProyectoIDMGrupos(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, grupo_user, grupo_fecha) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertProyectoIDMGrupos"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
                .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
                .Parameters.Append .CreateParameter("@grupo_user", adChar, adParamInput, 10, grupo_user)
                .Parameters.Append .CreateParameter("@grupo_fecha", SQLDdateType, adParamInput, , grupo_fecha)
                Set aplRST = .Execute
        End With
        sp_InsertProyectoIDMGrupos = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_InsertProyectoIDMGrupos = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMGrupos(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, grupo_user, grupo_fecha) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateProyectoIDMGrupos"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
                .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
                .Parameters.Append .CreateParameter("@grupo_user", adChar, adParamInput, 10, grupo_user)
                .Parameters.Append .CreateParameter("@grupo_fecha", SQLDdateType, adParamInput, , grupo_fecha)
                Set aplRST = .Execute
        End With
        sp_UpdateProyectoIDMGrupos = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateProyectoIDMGrupos = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMGruposField(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateProyectoIDMGruposField"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
                .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 80, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateProyectoIDMGruposField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateProyectoIDMGruposField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteProyectoIDMGrupos(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteProyectoIDMGrupos"
                .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
                .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
                .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
                .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
                .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
                Set aplRST = .Execute
        End With
        sp_DeleteProyectoIDMGrupos = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteProyectoIDMGrupos = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMGrupos(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMGrupos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMGrupos = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMGrupos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMAdjuntos(ProjId, ProjSubId, ProjSubSId, adj_tipo, adj_file) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMAdjuntos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)
        .Parameters.Append .CreateParameter("@adj_file", adChar, adParamInput, 100, adj_file)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMAdjuntos = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMAdjuntos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMResponsables(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMResponsables"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMResponsables = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMResponsables = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyIDMPet(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetProyIDMPet"
      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyIDMPet = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyIDMPet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMFicha(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetProyectoIDMFicha"
      .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
      .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
      .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetProyectoIDMFicha = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMFicha = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertIDMAdjuntos(ProjId, ProjSubId, ProjSubSId, file, FilePath, FileTexto) As Boolean
    On Error Resume Next
    
    Dim objFileSystemObject As Scripting.FileSystemObject
    Dim objFile As Scripting.file
    'Dim strSQL As String
    Dim strPathToFile As String, strFileName As String
    
    Dim txtCommand
    Dim lFileSize As Long
    
    Call Puntero(True)
    aplRST.Close
    Set aplRST = Nothing
    
    Set objFileSystemObject = New Scripting.FileSystemObject
    Set objFile = objFileSystemObject.GetFile(FilePath & file)
    If objFile.Attributes And ReadOnly Then
        'MsgBox "The file is read-only." & vbCrLf & _
        '       "This sample requires a file that's not read-only", vbExclamation + vbOKOnly
        MsgBox "El archivo es de solo lectura.", vbExclamation + vbOKOnly
    End If
    DoEvents
  
    'Call Puntero(True)
    txtCommand = "SELECT ProjId, ProjSubId, ProjSubSId, adj_tipo, adj_file, adj_texto, adj_objeto, audit_user, audit_date, cod_nivel, cod_area FROM ProyectoIDMAdjuntos WHERE ProjId = 1"

    aplCONN.CursorLocation = adUseServer        ' add -003- a.
    With aplRST
        .Open txtCommand, aplCONN, adOpenStatic, adLockOptimistic, adCmdText
        .AddNew
        .Fields!ProjId = ProjId
        .Fields!ProjSubId = ProjSubId
        .Fields!ProjSubSId = ProjSubSId
        .Fields!adj_tipo = ""
        .Fields!adj_file = file
        .Fields!adj_texto = ClearNull(FileTexto)
        .Fields!cod_nivel = getPerfNivel(glUsrPerfilActual)
        .Fields!cod_area = getPerfArea(glUsrPerfilActual)
        .Fields!audit_user = ClearNull(glLOGIN_ID_REAL)
        .Fields!audit_date = Now
        Call FileToBlob(FilePath & file, .Fields!adj_objeto)
        .Update
        If .State = adStateOpen Then .Close
    End With
    Call Puntero(False)
    aplCONN.CursorLocation = adUseClient        ' add -003- a.
    
    sp_InsertIDMAdjuntos = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    On Error GoTo 0
    If Not objFile Is Nothing Then Set objFile = Nothing
    If Not objFileSystemObject Is Nothing Then Set objFileSystemObject = Nothing
    Call Puntero(False)
    Call Status("Listo.")
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(docRST, Err, aplCONN))
    sp_InsertIDMAdjuntos = False
    Call Puntero(False)
End Function

'Function sp_InsertIDMAdjuntos(ProjId, ProjSubId, ProjSubSId, file, FilePath, FileTexto) As Boolean
'    On Error Resume Next
'    Dim txtCommand
'    Dim lFileSize As Long
'
'    docRST.Close
'    Set docRST = Nothing
'    On Error GoTo Err_SP
'
'    Call Puntero(True)
'    txtCommand = "SELECT ProjId, ProjSubId, ProjSubSId, adj_tipo, adj_file, adj_texto, adj_objeto, audit_user, audit_date, cod_nivel, cod_area FROM ProyectoIDMAdjuntos WHERE ProjId = 1"
'
'    With docRST
'        .Open txtCommand, aplCONN.ConnectionString, adOpenKeyset, adLockOptimistic, adCmdText
'        .AddNew
'        .Fields!ProjId = ProjId
'        .Fields!ProjSubId = ProjSubId
'        .Fields!ProjSubSId = ProjSubSId
'        .Fields!adj_tipo = ""
'        .Fields!adj_file = file
'        .Fields!adj_texto = ClearNull(FileTexto)
'        .Fields!cod_nivel = getPerfNivel(glUsrPerfilActual)
'        .Fields!cod_area = getPerfArea(glUsrPerfilActual)
'        .Fields!audit_user = ClearNull(glLOGIN_ID_REAL)
'        .Fields!audit_date = Now
'        Call FileToBlob(FilePath & file, .Fields!adj_objeto)
'        .Update
'        .Close
'    End With
'    Call Puntero(False)
'    sp_InsertIDMAdjuntos = True
'    If aplCONN.Errors.Count > 0 Then
'        GoTo Err_SP
'    End If
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(docRST, Err, aplCONN))
'    sp_InsertIDMAdjuntos = False
'    Call Puntero(False)
'End Function

Function sp_UpdateIDMAdjuntos(ProjId, ProjSubId, ProjSubSId, adj_tipo, adj_file, adj_texto) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMAdjuntos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)
        .Parameters.Append .CreateParameter("@adj_file", adChar, adParamInput, 100, adj_file)
        .Parameters.Append .CreateParameter("@adj_texto", adChar, adParamInput, 250, adj_texto)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, getPerfNivel(glUsrPerfilActual))
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, getPerfArea(glUsrPerfilActual))
        Set aplRST = .Execute
    End With
    sp_UpdateIDMAdjuntos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateIDMAdjuntos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteIDMAdjuntos(ProjId, ProjSubId, ProjSubSId, adj_tipo, adj_file) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoIDMAdjuntos"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)
        .Parameters.Append .CreateParameter("@adj_file", adChar, adParamInput, 100, adj_file)
        Set aplRST = .Execute
    End With
    sp_DeleteIDMAdjuntos = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteIDMAdjuntos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertProyectoIDMHoras(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, cant_horas) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertProyectoIDMHoras"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 3, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        .Parameters.Append .CreateParameter("@cant_horas", adInteger, adParamInput, , cant_horas)
        Set aplRST = .Execute
        If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
        End If
    End With
    sp_InsertProyectoIDMHoras = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertProyectoIDMHoras = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMHoras(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, cant_horas) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMHoras"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 3, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 3, cod_area)
        .Parameters.Append .CreateParameter("@cant_horas", adInteger, adParamInput, , cant_horas)
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMHoras = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMHoras = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMHorasField(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMHorasField"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 3, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 3, cod_area)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMHorasField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMHorasField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteProyectoIDMHoras(ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteProyectoIDMHoras"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 3, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        Set aplRST = .Execute
    End With
    sp_DeleteProyectoIDMHoras = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteProyectoIDMHoras = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetProyectoIDMHoras(ProjId, ProjSubId, ProjSubSId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetProyectoIDMHoras"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetProyectoIDMHoras = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetProyectoIDMHoras = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateProyectoIDMHistoField(ProjId, ProjSubId, ProjSubSId, hst_nrointerno, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateProyectoIDMHistoField"
        .Parameters.Append .CreateParameter("@ProjId", adInteger, adParamInput, , ProjId)
        .Parameters.Append .CreateParameter("@ProjSubId", adInteger, adParamInput, , ProjSubId)
        .Parameters.Append .CreateParameter("@ProjSubSId", adInteger, adParamInput, , ProjSubSId)
        .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdateProyectoIDMHistoField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateProyectoIDMHistoField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
