Attribute VB_Name = "spClase"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertClase(cod_clase, nom_clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertClase"
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, cod_clase)
        .Parameters.Append .CreateParameter("@nom_clase", adChar, adParamInput, 40, nom_clase)
        Set aplRST = .Execute
    End With
    sp_InsertClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateClase(cod_clase, nom_clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateClase"
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, cod_clase)
        .Parameters.Append .CreateParameter("@nom_clase", adChar, adParamInput, 40, nom_clase)
        Set aplRST = .Execute
    End With
    sp_UpdateClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteClase(cod_clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteClase"
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, cod_clase)
        Set aplRST = .Execute
    End With
    sp_DeleteClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetClase(cod_clase) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetClase"
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, IIf(IsNull(cod_clase), Null, cod_clase))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetClase = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
