Attribute VB_Name = "FuncionesGrid"
Option Explicit

'Example submitted by Leondias Frost (a993435@yahoo.com)
Private Const MF_CHECKED = &H8&
Private Const MF_APPEND = &H100&
Private Const MF_DISABLED = &H2&
Private Const MF_GRAYED = &H1&
Private Const MF_SEPARATOR = &H800&
Private Const MF_STRING = &H0&
Private Const TPM_LEFTALIGN = &H0&
Private Const TPM_RETURNCMD = &H100&
Private Const TPM_RIGHTBUTTON = &H2&

Private Const GRIDFILLROWCOLOR_Green = 14346960
Private Const GRIDFILLROWCOLOR_Blue = 16762520
Private Const GRIDFILLROWCOLOR_Rose = 14209532
Private Const GRIDFILLROWCOLOR_DarkGreen = 8963736
Private Const GRIDFILLROWCOLOR_Red = 10727930
Private Const GRIDFILLROWCOLOR_Yellow = 13170677
Private Const GRIDFILLROWCOLOR_LightBlue = 16245683
Private Const GRIDFILLROWCOLOR_LightOrange = 14346750
Private Const GRIDFILLROWCOLOR_LightGrey = 12632256
Private Const GRIDFILLROWCOLOR_LightGrey2 = &HF4F4F4
Private Const GRIDFILLROWCOLOR_DarkGrey = 12632256
Private Const GRIDFILLROWCOLOR_LightBlue2 = 16708320
Private Const GRIDFILLROWCOLOR_DarkGreen2 = &H8000&
Private Const GRIDFILLROWCOLOR_LightGreen = 15399140
Private Const GRIDFILLROWCOLOR_DarkRed = 4873437
Private Const GRIDFILLROWCOLOR_White = 16777215
Private Const GRIDFILLROWCOLOR_Black = 0

'Private Const GRIDFILLROWCOLOR_Green = 14346960
'Private Const GRIDFILLROWCOLOR_Blue = 16762520
'Private Const GRIDFILLROWCOLOR_Rose = 14209532
'Private Const GRIDFILLROWCOLOR_DarkGreen = 8963736
'Private Const GRIDFILLROWCOLOR_Red = 10727930
'Private Const GRIDFILLROWCOLOR_Yellow = 13170677
'Private Const GRIDFILLROWCOLOR_LightBlue = 16245683
'Private Const GRIDFILLROWCOLOR_LightOrange = 14346750
'Private Const GRIDFILLROWCOLOR_LightGrey = 12632256
'Private Const GRIDFILLROWCOLOR_LightGrey2 = &HF4F4F4
'Private Const GRIDFILLROWCOLOR_DarkGrey = 12632256
'Private Const GRIDFILLROWCOLOR_LightBlue2 = 16708320
'Private Const GRIDFILLROWCOLOR_DarkGreen2 = &H8000&
'Private Const GRIDFILLROWCOLOR_LightGreen = 15399140
'Private Const GRIDFILLROWCOLOR_DarkRed = 4873437
'Private Const GRIDFILLROWCOLOR_White = 16777215
'Private Const GRIDFILLROWCOLOR_Black = 0

Private Type POINTAPI
    x As Long
    y As Long
End Type

'{ add -000- a.
Private Declare Function CreatePopupMenu Lib "user32" () As Long
Private Declare Function TrackPopupMenuEx Lib "user32" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal x As Long, ByVal y As Long, ByVal hWnd As Long, ByVal lptpm As Any) As Long
Private Declare Function AppendMenu Lib "user32" Alias "AppendMenuA" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpNewItem As Any) As Long
Private Declare Function DestroyMenu Lib "user32" (ByVal hMenu As Long) As Long
Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
'}

Dim hMenu As Long
Dim m_lastFind As Integer
Dim m_strFind As String
Dim m_lastCOL As Integer

Sub MenuGridFind(objGrid As MSFlexGrid)
    Dim pt As POINTAPI
    Dim ret As Long
    
    If objGrid.Rows = 1 Then Exit Sub
    
    If m_lastCOL <> objGrid.MouseCol Then
        m_lastCOL = objGrid.MouseCol
        m_lastFind = 0
    End If
    
    hMenu = CreatePopupMenu()
    AppendMenu hMenu, MF_GRAYED Or MF_DISABLED, 1, "Columna: " & objGrid.TextMatrix(0, m_lastCOL)
    AppendMenu hMenu, MF_SEPARATOR, ByVal 0&, ByVal 0&
    AppendMenu hMenu, MF_STRING, 2, "Buscar"
    If m_lastFind > 0 Then
        AppendMenu hMenu, MF_STRING, 3, "Pr�ximo"
    Else
        AppendMenu hMenu, MF_GRAYED Or MF_DISABLED, 3, "Pr�ximo"
    End If
    GetCursorPos pt
    ret = TrackPopupMenuEx(hMenu, TPM_LEFTALIGN Or TPM_RETURNCMD Or TPM_RIGHTBUTTON, pt.x, pt.y, objGrid.hWnd, ByVal 0&)
    DestroyMenu hMenu
    Select Case ret
    Case 2
        m_lastFind = 0
        If objGrid.Rows > 1 Then
            m_strFind = InputBox("Ingrese el texto a buscar:", "Buscar", m_strFind)
            If m_strFind <> "" Then
                Call Find(objGrid, m_lastFind, m_lastCOL, m_strFind)
            End If
        End If
    Case 3
        m_lastFind = m_lastFind + 1
        Call Find(objGrid, m_lastFind, m_lastCOL, m_strFind)
    End Select
    'Debug.Print ret
End Sub

Function skipAcento(strIn As String) As String
    Dim i As Integer
    strIn = UCase(strIn)
    For i = 1 To Len(strIn)
        Select Case Mid(strIn, i, 1)
        Case "�", "�"
            Mid(strIn, i, 1) = "A"
        Case "�", "�"
            Mid(strIn, i, 1) = "E"
        Case "�", "�"
            Mid(strIn, i, 1) = "I"
        Case "�", "�"
            Mid(strIn, i, 1) = "O"
        Case "�", "�"
            Mid(strIn, i, 1) = "U"
        End Select
    Next
    skipAcento = strIn
End Function

Public Function Find(objGrid As MSFlexGrid, ByRef rowInicial As Integer, ptrColumn As Integer, strSeek As String) As Boolean
    'asume que la grila tiene un row de titulos, por eso busca desde i=1
    Dim i As Integer
    Find = False
    If rowInicial = 0 Then
        rowInicial = 1
    End If
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        For i = rowInicial To .Rows - 1
           If InStr(skipAcento(Trim(.TextMatrix(i, ptrColumn))), skipAcento(Trim(strSeek))) > 0 Then
                .TopRow = i
                .row = i
                .rowSel = i
                .col = 0
                .ColSel = .cols - 1
                rowInicial = i
                Find = True
                Exit Function
           End If
        Next
        For i = 1 To rowInicial
           If InStr(skipAcento(Trim(.TextMatrix(i, ptrColumn))), skipAcento(Trim(strSeek))) > 0 Then
                .TopRow = i
                .row = i
                .rowSel = i
                .col = 0
                .ColSel = .cols - 1
                rowInicial = i
                Find = True
                Exit Function
           End If
        Next
    End With
End Function

' Pinta una linea o fila completa de un color determinado en un control MSFlexGrid
Public Sub PintarLinea(ByRef oGrid As MSFlexGrid, Optional Color As prmColorFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    
    bFormateando = True
    If IsMissing(Color) Then
        Color = 0
    End If
    With oGrid
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .rowSel
        original_ColSel = .ColSel
        ' Si no estoy sobre la �ltima, tomo la actual
        If Not .rowSel > 1 Then
            .row = .Rows - 1
        Else
            .row = .rowSel
        End If

        For x = 0 To .cols - 1
            .col = x
            Select Case Color
                Case 24: .CellBackColor = GRIDFILLROWCOLOR_LightGrey2   ' Gris claro (para pintar filas)
                Case 1: .CellBackColor = GRIDFILLROWCOLOR_Green         ' Verde claro
                Case 2: .CellBackColor = GRIDFILLROWCOLOR_Blue          ' Azul claro
                Case 3: .CellBackColor = GRIDFILLROWCOLOR_Rose          ' Rosado
                Case 4: .CellBackColor = GRIDFILLROWCOLOR_DarkGreen     ' Verde oscuro
                Case 5: .CellBackColor = GRIDFILLROWCOLOR_Red           ' Rojo claro
                Case 6: .CellBackColor = GRIDFILLROWCOLOR_Yellow
                Case 7: .CellBackColor = GRIDFILLROWCOLOR_LightBlue
                Case 8: .CellBackColor = GRIDFILLROWCOLOR_LightOrange   ' Naranja suave
                Case 9: .CellBackColor = GRIDFILLROWCOLOR_DarkGrey      ' Gris oscuro
                Case 12: .CellBackColor = GRIDFILLROWCOLOR_LightBlue2   ' Azul muy clarito
                Case 13: .CellBackColor = GRIDFILLROWCOLOR_DarkGreen2   ' Otro verde oscuro
                Case 14: .CellBackColor = GRIDFILLROWCOLOR_LightGreen   ' LightGreen
                Case 20: .CellBackColor = GRIDFILLROWCOLOR_DarkRed      ' Rojo oscuro
                Case 21: .CellBackColor = GRIDFILLROWCOLOR_White        ' Blanco
                Case 22: .CellBackColor = GRIDFILLROWCOLOR_Black        ' Negro
                Case Else
                    .CellBackColor = Color                              ' Definido por el usuario
            End Select
        Next x
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .rowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub PintarLineaNoFix(ByRef oGrid As MSFlexGrid, Optional Color As prmColorFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    
    bFormateando = True
    If IsMissing(Color) Then
        Color = 0
    End If
    With oGrid
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .rowSel
        original_ColSel = .ColSel
        ' Si no estoy sobre la �ltima, tomo la actual
        If Not .rowSel > 1 Then
            .row = .Rows - 1
        Else
            .row = .rowSel
        End If

        'For x = 0 To .cols - 1
        For x = IIf(oGrid.FixedCols = 1, 1, 0) To .cols - 1
            .col = x
            Select Case Color
                Case 24: .CellBackColor = GRIDFILLROWCOLOR_LightGrey2   ' Gris claro (para pintar filas)
                Case 1: .CellBackColor = GRIDFILLROWCOLOR_Green         ' Verde claro
                Case 2: .CellBackColor = GRIDFILLROWCOLOR_Blue          ' Azul claro
                Case 3: .CellBackColor = GRIDFILLROWCOLOR_Rose          ' Rosado
                Case 4: .CellBackColor = GRIDFILLROWCOLOR_DarkGreen     ' Verde oscuro
                Case 5: .CellBackColor = GRIDFILLROWCOLOR_Red           ' Rojo claro
                Case 6: .CellBackColor = GRIDFILLROWCOLOR_Yellow
                Case 7: .CellBackColor = GRIDFILLROWCOLOR_LightBlue
                Case 8: .CellBackColor = GRIDFILLROWCOLOR_LightOrange   ' Naranja suave
                Case 9: .CellBackColor = GRIDFILLROWCOLOR_DarkGrey      ' Gris oscuro
                Case 12: .CellBackColor = GRIDFILLROWCOLOR_LightBlue2   ' Azul muy clarito
                Case 13: .CellBackColor = GRIDFILLROWCOLOR_DarkGreen2   ' Otro verde oscuro
                Case 14: .CellBackColor = GRIDFILLROWCOLOR_LightGreen   ' LightGreen
                Case 20: .CellBackColor = GRIDFILLROWCOLOR_DarkRed      ' Rojo oscuro
                Case 21: .CellBackColor = GRIDFILLROWCOLOR_White        ' Blanco
                Case 22: .CellBackColor = GRIDFILLROWCOLOR_Black        ' Negro
                Case Else
                    .CellBackColor = Color                              ' Definido por el usuario
            End Select
        Next x
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .rowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub


Public Sub PintarCelda(ByRef oGrid As MSFlexGrid, Optional Color As prmColorFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    
    bFormateando = True
    If IsMissing(Color) Then
        Color = 0
    End If
    With oGrid
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .rowSel
        original_ColSel = .ColSel
        .row = .rowSel
        Select Case Color
            Case 1: .CellBackColor = RGB(208, 234, 218)     ' Verde claro
            Case 2: .CellBackColor = RGB(152, 198, 255)     ' Azul claro
            Case 3: .CellBackColor = RGB(252, 209, 216)     ' Rosa / Rojo
            Case 4: .CellBackColor = RGB(152, 198, 136)     ' Verde oscuro
            Case 5: .CellBackColor = RGB(250, 177, 163)     ' Rojito
            Case 6: .CellBackColor = RGB(245, 247, 200)
            Case 7: .CellBackColor = RGB(179, 227, 247)
            Case 8: .CellBackColor = RGB(254, 233, 218)     ' Naranja suave
            Case 9: .CellBackColor = RGB(190, 190, 190)     ' Gris claro
            Case 10: .CellBackColor = RGB(147, 147, 147)    ' Gris oscuro
            Case 11: .CellBackColor = RGB(41, 28, 208)      ' Azul oscuro (hiperv�nculos)
            Case 12: .CellBackColor = RGB(224, 242, 254)    ' Azul muy clarito
            Case 13: .CellBackColor = &H8000&               ' Otro verde oscuro
            Case 14: .CellBackColor = RGB(228, 248, 234)    ' LightGreen
            Case 20: .CellBackColor = RGB(221, 92, 74)      ' Rojo oscuro
            Case 21: .CellBackColor = RGB(255, 255, 255)    ' Blanco
            Case 22: .CellBackColor = RGB(0, 0, 0)          ' Negro
            Case 23: .CellBackColor = 12582912              ' Azul oscuro
            Case Else: .CellBackColor = Color               ' A elecci�n
        End Select
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .rowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub CambiarForeColorLinea(ByRef oGrid As MSFlexGrid, Color As prmColorFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    With oGrid
        bFormateando = True
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .rowSel
        original_ColSel = .ColSel
        ' Si no estoy sobre la �ltima, tomo la actual
        If Not .rowSel > 1 Then
            .row = .Rows - 1
        Else
            .row = .rowSel
        End If
        For x = 0 To .cols - 1
            .col = x
            Select Case Color
                Case 1: .CellForeColor = RGB(208, 234, 218)         ' Verde claro
                Case 2: .CellForeColor = RGB(152, 198, 255)         ' Azul claro
                Case 3: .CellForeColor = RGB(252, 209, 216)         ' Rosa / Rojo
                Case 4: .CellForeColor = RGB(152, 198, 136)         ' Verde oscuro
                Case 5: .CellForeColor = RGB(250, 177, 163)         ' Rojito
                Case 6: .CellForeColor = RGB(245, 247, 200)
                Case 9: .CellForeColor = RGB(190, 190, 190)         ' Gris claro
                Case 10: .CellForeColor = RGB(147, 147, 147)        ' Gris oscuro
                Case 11: .CellForeColor = RGB(41, 28, 208)          ' Azul oscuro (hiperv�nculos)
                Case 13: .CellForeColor = &H8000&                   ' Otro verde oscuro
                Case 20: .CellForeColor = RGB(221, 92, 74)          ' Rojo oscuro
                Case 21: .CellForeColor = RGB(255, 255, 255)        ' Blanco
                Case 22: .CellForeColor = RGB(0, 0, 0)              ' Negro
                Case 23: .CellForeColor = &HC00000                  ' Azul oscuro
                Case Else
                    .CellForeColor = Color                          ' Definido por el usuario
            End Select
        Next x
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .rowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub CambiarForeColorCelda(ByRef oGrid As MSFlexGrid, lCol As Long, lRow As Long, Color As prmColorFilaGrilla)
    Dim x As Integer
    With oGrid
        bFormateando = True
        ' Establezco las posiciones a cambiar
        .col = lCol
        .row = lRow
        Select Case Color
            Case 1: .CellForeColor = RGB(208, 234, 218)     ' Verde claro
            Case 2: .CellForeColor = RGB(152, 198, 255)     ' Azul claro
            Case 3: .CellForeColor = RGB(252, 209, 216)     ' Rosa / Rojo
            Case 4: .CellForeColor = RGB(152, 198, 136)     ' Verde oscuro
            Case 5: .CellForeColor = RGB(250, 177, 163)     ' Rojito
            Case 6: .CellForeColor = RGB(245, 247, 200)
            Case 9: .CellForeColor = RGB(190, 190, 190)     ' Gris claro
            Case 10: .CellForeColor = RGB(147, 147, 147)    ' Gris oscuro
            Case 11: .CellForeColor = RGB(41, 28, 208)      ' Azul oscuro (hiperv�nculos)
            Case 13: .CellForeColor = &H8000&               ' Otro verde oscuro
            Case 20: .CellForeColor = RGB(221, 92, 74)      ' Rojo oscuro
            Case 21: .CellForeColor = RGB(255, 255, 255)    ' Blanco
            Case 22: .CellForeColor = RGB(0, 0, 0)          ' Negro
            Case 23: .CellForeColor = &HC00000              ' Azul oscuro
            Case Else
                .CellForeColor = Color                      ' Definido por el usuario
        End Select
    End With
    bFormateando = False
End Sub

Public Sub CambiarEfectoLinea(ByRef oGrid As MSFlexGrid, Efecto As prmEfectoFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    
    bFormateando = True
    
    With oGrid
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .rowSel
        original_ColSel = .ColSel
        ' Si no estoy sobre la �ltima, tomo la actual
        If Not .rowSel > 1 Then
            .row = .Rows - 1
        Else
            .row = .rowSel
        End If
        For x = 0 To .cols - 1
            .col = x
            Select Case Efecto
                Case 1: .CellFontBold = True
                Case 2: .CellFontItalic = True
                Case 3: .CellFontStrikeThrough = True
                Case 4: .CellFontUnderline = True
                Case 5: .CellTextStyle = flexTextFlat
                Case 6: .CellTextStyle = flexTextInset
                Case 7: .CellTextStyle = flexTextInsetLight
                Case 8: .CellTextStyle = flexTextRaised
                Case 9: .CellTextStyle = flexTextRaisedLight
            End Select
        Next x
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .rowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub CambiarEfectoCelda(ByRef oGrid As MSFlexGrid, lCol As Long, lRow As Long, Efecto As prmEfectoFilaGrilla)
    Dim x As Integer
    bFormateando = True
    With oGrid
        .col = lCol
        .row = lRow
        Select Case Efecto
            Case 1: .CellFontBold = True
            Case 2: .CellFontItalic = True
            Case 3: .CellFontStrikeThrough = True
            Case 4: .CellFontUnderline = True
            Case 5: .CellTextStyle = flexTextFlat
            Case 6: .CellTextStyle = flexTextInset
            Case 7: .CellTextStyle = flexTextInsetLight
            Case 8: .CellTextStyle = flexTextRaised
            Case 9: .CellTextStyle = flexTextRaisedLight
        End Select
    End With
    bFormateando = False
End Sub

Public Sub OrdenarGrilla(ByRef oGrilla As MSFlexGrid, ByRef lUltimaColumnaOrdenada As Long, Optional Skip As Boolean, Optional columnaSkip As Integer, Optional columnaFirstRowNum As Boolean, Optional vectorCols)
    Dim i As Long
    Dim colsOrderType As Boolean
    Dim orden As ORDENAMIENTO_TIPO
    
    If oGrilla.MouseRow > 0 Then Exit Sub
    If IsMissing(Skip) Then Skip = False
    If IsMissing(columnaFirstRowNum) Then columnaFirstRowNum = False
    
    ' Ordenamiento:
    ' �: Ascendente
    ' �: Descendente
    
    With oGrilla
        .visible = False
        .col = .MouseCol
        Debug.Print "Columna a ordenar: " & .TextMatrix(0, .col)
        If .MouseRow = 0 And .Rows > 1 Then
            If IsMissing(vectorCols) Then
                colsOrderType = False
                If IsNumeric(.TextMatrix(1, .col)) Then     ' Num�rico
                    orden = NUMERICO
                Else                                        ' Alfanum�rico
                    orden = ALFANUMERICO
                End If
            Else
                Debug.Print .col & ": " & vectorCols(.col)
                colsOrderType = True
                If vectorCols(.col) = ORDENAMIENTO_TIPO.DETERMINAR Then
                    If IsNumeric(.TextMatrix(1, .col)) Then     ' Num�rico
                        orden = NUMERICO
                    Else                                        ' Alfanum�rico
                        orden = ALFANUMERICO
                    End If
                Else
                    orden = vectorCols(.col)
                End If
            End If
        'If .MouseRow = 0 And .Rows > 1 Then
            '.visible = False
            '.col = .MouseCol
            ' **************************************************************************************************
            ' NUMERICO
            ' **************************************************************************************************
            Select Case orden
                Case ORDENAMIENTO_TIPO.NUMERICO
                    If Left(.TextMatrix(0, .col), 1) = "�" Then
                        ' Quito la marca de ordenamiento de la �ltima columna ordenada
                        If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                            .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        End If
                        If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                        .Sort = flexSortNumericDescending
                    ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
                        ' Quito la marca de ordenamiento de la �ltima columna ordenada
                        If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                            .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        End If
                        If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                        .Sort = flexSortNumericAscending
                    Else
                        ' Quito la marca de ordenamiento de la �ltima columna ordenada
                        If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                            .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        End If
                        .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                        .Sort = flexSortNumericAscending
                    End If
                Case ORDENAMIENTO_TIPO.ALFANUMERICO
                    If Left(.TextMatrix(0, .col), 1) = "�" Then
                        ' Quito la marca de ordenamiento de la �ltima columna ordenada
                        If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                            .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        End If
                        If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                        .Sort = flexSortStringNoCaseDescending
                    ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
                        ' Quito la marca de ordenamiento de la �ltima columna ordenada
                        If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                            .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        End If
                        If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                        .Sort = flexSortStringNoCaseAscending
                    Else
                        ' Quito la marca de ordenamiento de la �ltima columna ordenada
                        If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                            .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                        End If
                        .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                        .Sort = flexSortStringNoCaseAscending
                        '.Sort = flexSortGenericAscending
                    End If
            End Select
' Este es el original
'            If (ClearNull(.TextMatrix(0, .col)) = "Ord." Or IsNumeric(.TextMatrix(1, .col))) Then
'                If Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito la marca de ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortNumericDescending
'                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito la marca de ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortNumericAscending
'                Else
'                    ' Quito la marca de ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    '.TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    '.Sort = flexSortNumericDescending
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortNumericAscending
'                End If
'            Else
'                ' **************************************************************************************************
'                ' ALFANUMERICO
'                ' **************************************************************************************************
'                If Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito la marca de ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortStringNoCaseDescending
'                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito la marca de ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortStringNoCaseAscending
'                Else
'                    ' Quito la marca de ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    '.TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    '.Sort = flexSortStringNoCaseDescending
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortStringNoCaseAscending
'                    '.Sort = flexSortGenericAscending
'                End If
'            End If
            lUltimaColumnaOrdenada = .col
            If Skip Then
                Call RepintarGrillaConSkipColumn(oGrilla, columnaSkip)
            Else
                Call RepintarGrilla(oGrilla)
            End If
            If columnaFirstRowNum And .FixedCols > 0 Then
                For i = 1 To .Rows - 1
                    .TextMatrix(i, 0) = i
                Next i
            End If
            .visible = True
        End If
    End With
End Sub

' ORIGINAL QUE FUNCIONA
'Public Sub OrdenarGrilla(ByRef oGrilla As MSFlexGrid, ByRef lUltimaColumnaOrdenada As Long, Optional Skip As Boolean, Optional columnaSkip As Integer)
'    If IsMissing(Skip) Then Skip = False
'
'    ' Ordenamiento:
'    ' �: Ascendente
'    ' �: Descendente
'
'    With oGrilla
'        If .MouseRow = 0 And .Rows > 1 Then
'            .visible = False
'            .col = .MouseCol
'            If ClearNull(.TextMatrix(0, .col)) = "Ord." Or IsNumeric(.TextMatrix(1, .col)) Then
'                If Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito el ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortNumericDescending
'                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito el ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortNumericAscending
'                Else
'                    ' Quito el ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortNumericDescending
'                End If
'            Else
'                If Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito el ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortStringNoCaseDescending
'                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
'                    ' Quito el ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortStringNoCaseAscending
'                Else
'                    ' Quito el ordenamiento de la �ltima columna ordenada
'                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
'                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
'                    End If
'                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
'                    .Sort = flexSortStringNoCaseDescending
'                End If
'            End If
'            lUltimaColumnaOrdenada = .col
'            If Skip Then
'                Call RepintarGrillaConSkipColumn(oGrilla, columnaSkip)
'            Else
'                Call RepintarGrilla(oGrilla)
'            End If
'            .visible = True
'        End If
'    End With
'End Sub

Public Sub RepintarGrilla(ByRef oGrilla As MSFlexGrid)
    Dim i As Long
    Dim J As Long
    Dim x As Long
    
    J = oGrilla.Rows - oGrilla.FixedRows
    
    With oGrilla
        For i = 1 To J
            .row = i '  + 1
            If i Mod 2 > 0 Then
                'For x = 0 To .cols - 1
                For x = .FixedCols To .cols - 1
                    .col = x
                    .CellBackColor = GRIDFILLROWCOLOR_LightGrey2
                Next x
            Else
                'For x = 0 To .cols - 1
                For x = .FixedCols To .cols - 1
                    .col = x
                    .CellBackColor = GRIDFILLROWCOLOR_White
                Next x
            End If
            DoEvents
        Next i
    End With
End Sub

Public Sub RepintarGrillaConSkipColumn(ByRef oGrilla As MSFlexGrid, Columna As Integer)
    Dim i As Long
    Dim J As Long
    Dim x As Long
    
    J = oGrilla.Rows - oGrilla.FixedRows
    
    With oGrilla
        For i = 1 To J
            .row = i '  + 1
            If i Mod 2 > 0 Then
                'For x = 0 To .cols - 1
                For x = .FixedCols To .cols - 1
                    If x <> Columna Then
                        .col = x
                        .CellBackColor = GRIDFILLROWCOLOR_LightGrey2
                    End If
                Next x
            Else
                'Call PintarLinea(oGrilla, prmGridFillRowColorWhite)
                'For x = 0 To .cols - 1
                For x = .FixedCols To .cols - 1
                    If x <> Columna Then
                        .col = x
                        .CellBackColor = GRIDFILLROWCOLOR_White
                    End If
                Next x
            End If
            DoEvents
        Next i
    End With
End Sub

Public Sub GrillaAgregarItem(ByRef oGrilla As MSFlexGrid, ByRef Coleccion As Collection)
    Dim i As Long
    Dim sItem As String
    
    With oGrilla
        For i = 1 To Coleccion.Count ' - 1
            sItem = sItem & Coleccion.Item(i) & vbTab
        Next i
        .AddItem sItem, .rowSel + 1
    End With
End Sub

Public Sub GrillaEliminarItem(ByRef oGrilla As MSFlexGrid, ByVal row As Long)
    With oGrilla
        If .Rows > 1 Then
            If .Rows = 2 Then
                .Rows = 1
            Else
                If .rowSel > 0 Then
                   .RemoveItem (row)
                End If
            End If
        End If
    End With
End Sub
