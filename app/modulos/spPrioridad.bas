Attribute VB_Name = "spPrioridad"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertPrioridad(prior_id, prior_dsc, prior_ord, prior_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPrioridad"
        .Parameters.Append .CreateParameter("@prior_id", adChar, adParamInput, 1, prior_id)
        .Parameters.Append .CreateParameter("@prior_dsc", adChar, adParamInput, 50, prior_dsc)
        .Parameters.Append .CreateParameter("@prior_ord", adInteger, adParamInput, , CLng(Val(prior_ord)))
        .Parameters.Append .CreateParameter("@prior_hab", adChar, adParamInput, 1, prior_hab)
        Set aplRST = .Execute
    End With
    sp_InsertPrioridad = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_InsertPrioridad = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePrioridad(prior_id, prior_dsc, prior_ord, prior_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePrioridad"
        .Parameters.Append .CreateParameter("@prior_id", adChar, adParamInput, 1, prior_id)
        .Parameters.Append .CreateParameter("@prior_dsc", adChar, adParamInput, 50, prior_dsc)
        .Parameters.Append .CreateParameter("@prior_ord", adInteger, adParamInput, , CLng(Val(prior_ord)))
        .Parameters.Append .CreateParameter("@prior_hab", adChar, adParamInput, 1, prior_hab)
        Set aplRST = .Execute
    End With
    sp_UpdatePrioridad = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdatePrioridad = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePrioridadField(prior_id, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePrioridadField"
        .Parameters.Append .CreateParameter("@prior_id", adInteger, adParamInput, , CLng(Val(prior_id)))
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 80, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 255, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdatePrioridadField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdatePrioridadField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePrioridad(prior_id, prior_dsc, prior_ord, prior_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePrioridad"
        .Parameters.Append .CreateParameter("@prior_id", adChar, adParamInput, , prior_id)
        .Parameters.Append .CreateParameter("@prior_dsc", adChar, adParamInput, , prior_dsc)
        .Parameters.Append .CreateParameter("@prior_ord", adInteger, adParamInput, , prior_ord)
        .Parameters.Append .CreateParameter("@prior_hab", adChar, adParamInput, , prior_hab)
        Set aplRST = .Execute
    End With
    sp_DeletePrioridad = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_DeletePrioridad = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPrioridad(prior_id, prior_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPrioridad"
        .Parameters.Append .CreateParameter("@prior_id", adChar, adParamInput, 1, prior_id)
        .Parameters.Append .CreateParameter("@prior_hab", adChar, adParamInput, 1, prior_hab)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPrioridad = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPrioridad = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

