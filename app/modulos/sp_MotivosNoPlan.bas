Attribute VB_Name = "spPlanMotivosNoPlan"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertMotivosNoPlan(cod_motivo_noplan, nom_motivo_noplan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertMotivosNoPlan"
                .Parameters.Append .CreateParameter("@cod_motivo_noplan", adInteger, adParamInput, , cod_motivo_noplan)
                .Parameters.Append .CreateParameter("@nom_motivo_noplan", adChar, adParamInput, 50, nom_motivo_noplan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, 1, estado_hab)
                Set aplRST = .Execute
        End With
        sp_InsertMotivosNoPlan = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertMotivosNoPlan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateMotivosNoPlan(cod_motivo_noplan, nom_motivo_noplan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateMotivosNoPlan"
                .Parameters.Append .CreateParameter("@cod_motivo_noplan", adInteger, adParamInput, , cod_motivo_noplan)
                .Parameters.Append .CreateParameter("@nom_motivo_noplan", adChar, adParamInput, 50, nom_motivo_noplan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, 1, estado_hab)
                Set aplRST = .Execute
        End With
        sp_UpdateMotivosNoPlan = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateMotivosNoPlan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateMotivosNoPlanField(x, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateMotivosNoPlanField"
                .Parameters.Append .CreateParameter("@x", adInteger, adParamInput, , CLng(Val(x)))
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateMotivosNoPlanField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateMotivosNoPlanField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteMotivosNoPlan(cod_motivo_noplan) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteMotivosNoPlan"
                .Parameters.Append .CreateParameter("@cod_motivo_noplan", adInteger, adParamInput, , cod_motivo_noplan)
                Set aplRST = .Execute
        End With
        sp_DeleteMotivosNoPlan = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteMotivosNoPlan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetMotivosNoPlan(cod_motivo_noplan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_GetMotivosNoPlan"
                .Parameters.Append .CreateParameter("@cod_motivo_noplan", adInteger, adParamInput, , cod_motivo_noplan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, 1, estado_hab)
                Set aplRST = .Execute
        End With
        sp_GetMotivosNoPlan = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_GetMotivosNoPlan = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function
