Attribute VB_Name = "spAgrup"
'GMT01 - PET 72319 Se agrega consulta por titulo

Option Explicit

Function sp_GetAgrup(agr_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrup"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrup = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAgrupAgrup(agr_nropadre) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupAgrup"
      .Parameters.Append .CreateParameter("@agr_nropadre", adInteger, adParamInput, , CLng(Val(agr_nropadre)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupAgrup = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupAgrup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAgrupDepen(agr_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupDepen"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupDepen = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupDepen = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAgrupNivel(visibilidad, area, vigencia) As Boolean
''    If IsMissing(flg_habil) Then
''        flg_habil = Null
''    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupNivel"
      .Parameters.Append .CreateParameter("@visibilidad", adChar, adParamInput, 3, visibilidad)
      .Parameters.Append .CreateParameter("@area", adChar, adParamInput, 10, area)
      .Parameters.Append .CreateParameter("@vigencia", adChar, adParamInput, 1, vigencia)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupNivel = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupNivel = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAgrupNivelRecur(modo, nivel, area, vigencia) As Boolean
''    If IsMissing(flg_habil) Then
''        flg_habil = Null
''    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupNivelRecur"
      .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 4, modo)
      .Parameters.Append .CreateParameter("@nivel", adChar, adParamInput, 3, nivel)
      .Parameters.Append .CreateParameter("@area", adChar, adParamInput, 250, area)
      .Parameters.Append .CreateParameter("@vigencia", adChar, adParamInput, 1, vigencia)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupNivelRecur = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupNivelRecur = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdAgrup(agr_nrointerno, agr_nropadre, agr_titulo, agr_vigente, cod_direccion, cod_gerencia, cod_sector, cod_grupo, cod_usualta, agr_visibilidad, agr_actualiza, agr_admpet) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdAgrup"
        .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
        .Parameters.Append .CreateParameter("@agr_nropadre", adInteger, adParamInput, , CLng(Val(agr_nropadre)))
        .Parameters.Append .CreateParameter("@agr_titulo", adChar, adParamInput, 50, agr_titulo)
        .Parameters.Append .CreateParameter("@agr_vigente", adChar, adParamInput, 1, agr_vigente)
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@cod_usualta", adChar, adParamInput, 10, cod_usualta)
        .Parameters.Append .CreateParameter("@agr_visibilidad", adChar, adParamInput, 3, agr_visibilidad)
        .Parameters.Append .CreateParameter("@agr_actualiza", adChar, adParamInput, 3, agr_actualiza)
        .Parameters.Append .CreateParameter("@agr_admpet", adChar, adParamInput, 1, agr_admpet)
        Set aplRST = .Execute
    End With
    sp_UpdAgrup = True
'    If Not IsEmpty(aplRST) And (Not aplRST.EOF) And aplRST(0).Name = "ErrCode" Then
'        MsgBox (AnalizarErrorSQL(aplRST, 30000))
'        sp_UpdAgrup = False
'    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdAgrup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdAgrupAlcance(agr_nrointerno, cod_usualta, agr_visibilidad, agr_actualiza, agr_vigente, cod_direccion, cod_gerencia, cod_sector, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdAgrupAlcance"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_usualta", adChar, adParamInput, 10, cod_usualta)
      .Parameters.Append .CreateParameter("@agr_visibilidad", adChar, adParamInput, 3, agr_visibilidad)
      .Parameters.Append .CreateParameter("@agr_actualiza", adChar, adParamInput, 3, agr_actualiza)
      .Parameters.Append .CreateParameter("@agr_vigente", adChar, adParamInput, 1, agr_vigente)
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    sp_UpdAgrupAlcance = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdAgrupAlcance = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsAgrup(agr_nropadre, agr_titulo, agr_vigente, cod_direccion, cod_gerencia, cod_sector, cod_grupo, cod_usualta, agr_visibilidad, agr_actualiza, agr_admpet) As Integer
    Dim var_numero As Integer
    var_numero = 0
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsAgrup"
      .Parameters.Append .CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue)
      .Parameters.Append .CreateParameter("@agr_nropadre", adInteger, adParamInput, , CLng(Val(agr_nropadre)))
      .Parameters.Append .CreateParameter("@agr_titulo", adChar, adParamInput, 50, agr_titulo)
      .Parameters.Append .CreateParameter("@agr_vigente", adChar, adParamInput, 1, agr_vigente)
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@cod_usualta", adChar, adParamInput, 10, cod_usualta)
      .Parameters.Append .CreateParameter("@agr_visibilidad", adChar, adParamInput, 3, agr_visibilidad)
      .Parameters.Append .CreateParameter("@agr_actualiza", adChar, adParamInput, 3, agr_actualiza)
      .Parameters.Append .CreateParameter("@agr_admpet", adChar, adParamInput, 1, agr_admpet)
      Set aplRST = .Execute
    End With
    If var_numero = 0 Then
        sp_InsAgrup = 0
        Exit Function
    End If
    sp_InsAgrup = var_numero
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            sp_InsAgrup = 0
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsAgrup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DelAgrup(agr_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelAgrup"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_DelAgrup = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelAgrup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_rptEstructura(cod_direccion, cod_gerencia, cod_sector, cod_grupo, detalle) As Boolean
''    If IsMissing(flg_habil) Then
''        flg_habil = Null
''    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_rptEstructura"
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@detalle", adChar, adParamInput, 1, detalle)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_rptEstructura = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_rptEstructura = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAgrupPetic(agr_nrointerno, pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupPetic"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupPetic = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupPetic = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'trae todos los nodos que dependen de agr_raiz donde esta la peticion
Function sp_GetAgrupPeticHijo(agr_raiz, pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupPeticHijo"
      .Parameters.Append .CreateParameter("@agr_nroraiz", adInteger, adParamInput, , CLng(Val(agr_raiz)))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_GetAgrupPeticHijo = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupPeticHijo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAgrupPeticDup(agr_nrointerno, pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupPeticDup"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupPeticDup = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupPeticDup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DelAgrupPetic(agr_nrointerno, pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelAgrupPetic"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_DelAgrupPetic = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelAgrupPetic = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsAgrupPetic(agr_nrointerno, pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsAgrupPetic"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_InsAgrupPetic = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsAgrupPetic = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DelAgrupAgrup(agr_nrointerno, agr_nropadre) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelAgrupAgrup"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      .Parameters.Append .CreateParameter("@agr_nropadre", adInteger, adParamInput, , CLng(Val(agr_nropadre)))
      Set aplRST = .Execute
    End With
    sp_DelAgrupAgrup = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelAgrupAgrup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsAgrupAgrup(agr_nrointerno, agr_nropadre) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsAgrupAgrup"
        .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
        .Parameters.Append .CreateParameter("@agr_nropadre", adInteger, adParamInput, , CLng(Val(agr_nropadre)))
        Set aplRST = .Execute
    End With
    sp_InsAgrupAgrup = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsAgrupAgrup = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAgrupPadre(agr_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupPadre"
      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(agr_nrointerno)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupPadre = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupPadre = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'GMT01 - INI
Function sp_GetAgrupTitulo(agr_titulo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAgrupTitulo"
      .Parameters.Append .CreateParameter("@agr_titulo", adChar, adParamInput, 50, agr_titulo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAgrupTitulo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAgrupTitulo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'GMT01 - FIN
