Attribute VB_Name = "spHistorial"
' -001- a. FJS 26.07.2007 - En determinado momento del testeo, en situaciones en particular, ADO dispar� el error nro. -2147467259 con el siguiente mensaje:
'                           [SYBASE][ODBC Sybase driver]Sybase does not allow more than one active statement when retrieving results without a cursor.
'                           No se han podido determinar a este momento las causas que generaron el error, pero se pudo corregir, acorde con la naturaleza del mensaje
'                           determinando una nueva conexi�n generada implicitamente a partir de una cadena de conexi�n v�lida. Dicha conexi�n reemplaz� a la conexi�n activa
'                           general de la aplicaci�n, permitiendo que el objeto command ejecute el SP.
'                           Condiciones: hasta ahora se sabe que, en ambiente DESA con el usuario de Susana Dopazo, con perfil de responsable de ejecuci�n (grupo), pasando
'                           una petici�n propia de tipo optimizaci�n al estado "Ejecuci�n".
' -002- a. FJS 28.05.2008 - Se crea la llamada al SP GetHistorial
' -003- a. FJS 26.03.2009 - Se crea un nuevo SP para soportar el manejo de peticiones hist�ricas.
' -004- a. FJS 05.03.2010 - Se mejora la performance de esta consulta: DoEvents

Option Explicit

Function sp_AddHistorial(pet_nrointerno, cod_evento, pet_estado, cod_sector, sec_estado, cod_grupo, gru_estado, cod_recurso, cod_perfil, CampoTexto) As Long
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim hst_nrointerno As Long
    'Dim hst_nrointerno As Integer
    'Dim rsAux As ADODB.Recordset
    hst_nrointerno = 0
    
    'Set rsAux = Nothing
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP

    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN                    ' del -001- a.
        '.ActiveConnection = aplCONN.ConnectionString    ' add -001- a.
        .CommandType = adCmdStoredProc
        .CommandText = "sp_AddHistorial"
        .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamOutput)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 8, cod_evento)
        .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 6, pet_estado)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 6, sec_estado)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@gru_estado", adChar, adParamInput, 6, gru_estado)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
        .Execute
        hst_nrointerno = .Parameters(0).value
    End With
    
    If hst_nrointerno = 0 Then
        sp_AddHistorial = 0
        Exit Function
    End If
    sp_AddHistorial = hst_nrointerno
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN                      ' del -001- a.
      '.ActiveConnection = aplCONN.ConnectionString      ' add -001- a.
      .CommandType = adCmdStoredProc
      .CommandText = "sp_AddHistorialMemo"
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput)
      .Parameters.Append .CreateParameter("@hst_secuencia", adSmallInt, adParamInput)
      .Parameters.Append .CreateParameter("@mem_texto", adVarChar, adParamInput, 255)
    End With
    
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(CampoTexto, 255)
        With objCommand
            .Parameters(0).value = hst_nrointerno
            .Parameters(1).value = CInt(Secuencia)
            .Parameters(2).value = TmpTexto & ""
            'Set rsAux = .Execute
            Set aplRST = .Execute
        End With
'        rsAUX.Close
        Secuencia = Secuencia + 1
        CampoTexto = Mid(CampoTexto, 256)
        If Len(CampoTexto) = 0 Then
            bFlg = False
        End If
    Loop
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    'MsgBox (AnalizarErrorSQL(rsAux, Err))
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_AddHistorial = 0
End Function

' ORIGINAL
'Function sp_AddHistorial(pet_nrointerno, cod_evento, pet_estado, cod_sector, sec_estado, cod_grupo, gru_estado, cod_recurso, CampoTexto) As Long
'    Dim bFlg As Boolean
'    Dim Secuencia As Integer
'    Dim TmpTexto As String
'    Dim Resto As Integer
'    Dim hst_nrointerno As Long
'    'Dim hst_nrointerno As Integer
'    Dim rsAux As ADODB.Recordset
'    hst_nrointerno = 0
'
'    Set rsAux = Nothing
'
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        '.ActiveConnection = aplCONN                    ' del -001- a.
'        .ActiveConnection = aplCONN.ConnectionString    ' add -001- a.
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_AddHistorial"
'        .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamOutput)
'        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
'        .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 8, cod_evento)
'        .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 6, pet_estado)
'        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
'        .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 6, sec_estado)
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'        .Parameters.Append .CreateParameter("@gru_estado", adChar, adParamInput, 6, gru_estado)
'        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
'        .Execute
'        hst_nrointerno = .Parameters(0).Value
'    End With
'
'    If hst_nrointerno = 0 Then
'        sp_AddHistorial = 0
'        Exit Function
'    End If
'    sp_AddHistorial = hst_nrointerno
'
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    Set objCommand = New ADODB.Command
'    With objCommand
'      '.ActiveConnection = aplCONN                      ' del -001- a.
'      .ActiveConnection = aplCONN.ConnectionString      ' add -001- a.
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_AddHistorialMemo"
'      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput)
'      .Parameters.Append .CreateParameter("@hst_secuencia", adSmallInt, adParamInput)
'      .Parameters.Append .CreateParameter("@mem_texto", adVarChar, adParamInput, 255)
'    End With
'
'    bFlg = True
'    Secuencia = 0
'    Do While bFlg = True
'        TmpTexto = Left(CampoTexto, 255)
'        With objCommand
'            .Parameters(0).Value = hst_nrointerno
'            .Parameters(1).Value = CInt(Secuencia)
'            .Parameters(2).Value = TmpTexto & ""
'            Set rsAux = .Execute
'        End With
''        rsAUX.Close
'        Secuencia = Secuencia + 1
'        CampoTexto = Mid(CampoTexto, 256)
'        If Len(CampoTexto) = 0 Then
'            bFlg = False
'        End If
'    Loop
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'
'Err_SP:
'    MsgBox (AnalizarErrorSQL(rsAux, Err))
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    sp_AddHistorial = 0
'End Function

Function sp_UpdHistorial(hst_nrointerno, cod_evento, pet_estado, cod_sector, sec_estado, cod_grupo, gru_estado, cod_recurso) As Boolean
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdHistorial"
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 8, cod_evento)
      .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 6, pet_estado)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 6, sec_estado)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@gru_estado", adChar, adParamInput, 6, gru_estado)
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      Set aplRST = .Execute
    End With
    sp_UpdHistorial = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_UpdHistorial = False
End Function

Function sp_UpdHistorialField(hst_nrointerno, campo, valortxt, valordate, valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateHistorialField"
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
      .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
      .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum), 0, CLng(Val("" & valorNum))))
      .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 120, valortxt)
      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , IIf(IsNull(valordate), Null, valordate))
      Set aplRST = .Execute
    End With
    sp_UpdHistorialField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_UpdHistorialField = False
End Function

'Function sp_UpdHistorialMemo(hst_nrointerno, valortxt) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_AddHistorialMemo"
'      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput)
'      .Parameters.Append .CreateParameter("@hst_secuencia", adSmallInt, adParamInput)
'      .Parameters.Append .CreateParameter("@mem_texto", adVarChar, adParamInput, 255)
'    End With
'
'    bFlg = True
'    Secuencia = 0
'    Do While bFlg = True
'        TmpTexto = Left(CampoTexto, 255)
'        With objCommand
'            .Parameters(0).value = hst_nrointerno
'            .Parameters(1).value = CInt(Secuencia)
'            .Parameters(2).value = TmpTexto & ""
'            'Set rsAux = .Execute
'            Set aplRST = .Execute
'        End With
''        rsAUX.Close
'        Secuencia = Secuencia + 1
'        CampoTexto = Mid(CampoTexto, 256)
'        If Len(CampoTexto) = 0 Then
'            bFlg = False
'        End If
'    Loop
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'
'Err_SP:
'    'MsgBox (AnalizarErrorSQL(rsAux, Err))
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    sp_AddHistorial = 0
'
'
'
'
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_UpdateHistorialField"
'      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
'      .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
'      .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valorNum), 0, CLng(Val("" & valorNum))))
'      .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 120, valortxt)
'      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , IIf(IsNull(valordate), Null, valordate))
'      Set aplRST = .Execute
'    End With
'    sp_UpdHistorialField = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    sp_UpdHistorialField = False
'End Function

Function sp_GetHistorialMemo(hst_nrointerno) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHistorialMemo"
        .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        bGetMemo = True
    End If
    
    If bGetMemo Then
        Do While Not aplRST.EOF
            sGetMemo = sGetMemo & Trim(aplRST!mem_texto)
            aplRST.MoveNext
            'DoEvents        ' add -004- a.
        Loop
        ' TODO: dejar esto habilitado esto (05/09/2017)
        aplRST.Close
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetHistorialMemo = sGetMemo
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetHistorialMemo = ""
End Function

Function sp_GetHistorialSector(pet_nrointerno, cod_sector) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    Dim Nombre As String
    
    sGetMemo = ""
    
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHistorialSector"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        bGetMemo = True
    End If
    
    Nombre = ""
    If bGetMemo Then
        Do While Not aplRST.EOF
            If Nombre <> ClearNull(aplRST!Nombre) Then
                If Len(sGetMemo) > 0 Then
                    sGetMemo = sGetMemo & vbCrLf
                End If
                sGetMemo = sGetMemo & ClearNull(aplRST!Nombre) & ": " & ClearNull(aplRST!Estado)
                Nombre = ClearNull(aplRST!Nombre)
            End If
            sGetMemo = sGetMemo & ClearNull(aplRST!mem_texto)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetHistorialSector = sGetMemo
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHistorialSector = ""
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHistorialXt(pet_nrointerno, hst_nrointerno) As Boolean
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHistorialXt"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetHistorialXt = True
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetHistorialXt = False
End Function

'{ add -002- a.
Function sp_GetHistorial(pet_nrointerno, hst_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHistorial"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetHistorial = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetHistorial = False
End Function
'}

Function sp_DelHistorial(pet_nrointerno, Optional flag, Optional cod_evento) As Boolean
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    
    If IsMissing(flag) Then
        flag = "ALL"
    End If
    If IsMissing(cod_evento) Then
        cod_evento = ""
    End If
    
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelHistorial"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@flag", adChar, adParamInput, 3, flag)
      .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 250, cod_evento)
      Set aplRST = .Execute
    End With
    sp_DelHistorial = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_DelHistorial = False
End Function

'{ nuevo
Function sp_DeleteHistorialPet(pet_nrointerno, hst_nrointerno) As Boolean
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteHistorialPet"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_DeleteHistorialPet = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_DeleteHistorialPet = False
End Function

Function sp_GetHistorialEvento(pet_nrointerno, hst_nrointerno, hst_cod_evento, hst_fecha_desde, hst_fecha_hasta) As Boolean
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHistorialEvento"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , IIf(IsNull(hst_nrointerno), Null, hst_nrointerno))
      .Parameters.Append .CreateParameter("@hst_cod_evento", adChar, adParamInput, 8, hst_cod_evento)
      .Parameters.Append .CreateParameter("@hst_fecha_desde", SQLDdateType, adParamInput, , hst_fecha_desde)
      .Parameters.Append .CreateParameter("@hst_fecha_hasta", SQLDdateType, adParamInput, , hst_fecha_hasta)
      Set aplRST = .Execute
    End With
    sp_GetHistorialEvento = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetHistorialEvento = False
End Function
'}

'{ add -003- a.
Function sp_GetHistorialXtH(pet_nrointerno, hst_nrointerno) As Boolean
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHistorialXtH"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetHistorialXtH = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetHistorialXtH = False
End Function

Function sp_GetHistorialMemoH(hst_nrointerno) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHistorialMemoH"
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , hst_nrointerno)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        bGetMemo = True
    End If
    If bGetMemo Then
        Do While Not aplRST.EOF
            sGetMemo = sGetMemo & Trim(aplRST!mem_texto)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetHistorialMemoH = sGetMemo
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetHistorialMemoH = ""
End Function
'}

Function sp_AddHistorial2(pet_nrointerno, cod_evento, pet_estado, cod_sector, sec_estado, cod_grupo, gru_estado, cod_recurso, ByVal CampoTexto, audit_user) As Long
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim hst_nrointerno As Long
    hst_nrointerno = 0
    
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP

    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_AddHistorial2"
        .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamOutput)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@cod_evento", adChar, adParamInput, 8, cod_evento)
        .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 6, pet_estado)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 6, sec_estado)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@gru_estado", adChar, adParamInput, 6, gru_estado)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@audit_user", adChar, adParamInput, 10, audit_user)
        .Execute
        hst_nrointerno = .Parameters(0).value
    End With
    
    If hst_nrointerno = 0 Then
        sp_AddHistorial2 = 0
        Exit Function
    End If
    sp_AddHistorial2 = hst_nrointerno
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_AddHistorialMemo"
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput)
      .Parameters.Append .CreateParameter("@hst_secuencia", adSmallInt, adParamInput)
      .Parameters.Append .CreateParameter("@mem_texto", adVarChar, adParamInput, 255)
    End With
    
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(CampoTexto, 255)
        With objCommand
            .Parameters(0).value = hst_nrointerno
            .Parameters(1).value = CInt(Secuencia)
            .Parameters(2).value = TmpTexto & ""
            Set aplRST = .Execute
        End With
        Secuencia = Secuencia + 1
        CampoTexto = Mid(CampoTexto, 256)
        If Len(CampoTexto) = 0 Then
            bFlg = False
        End If
    Loop
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_AddHistorial2 = 0
End Function

