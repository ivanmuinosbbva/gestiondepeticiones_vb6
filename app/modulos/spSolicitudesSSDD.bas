Attribute VB_Name = "spSolicitudesSSDD"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertSolicitudesSSDD(solicitante, sol_mask, jus_codigo, sol_texto, pet_nroasignado, tablaId, sol_archivo, sol_eme, sol_diferida) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertSolicitudesSSDD"
        .Parameters.Append .CreateParameter("@solicitante", adChar, adParamInput, 10, solicitante)
        .Parameters.Append .CreateParameter("@sol_mask", adChar, adParamInput, 1, sol_mask)
        .Parameters.Append .CreateParameter("@jus_codigo", adInteger, adParamInput, , jus_codigo)
        .Parameters.Append .CreateParameter("@sol_texto", adChar, adParamInput, 255, sol_texto)
        .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , Val(pet_nroasignado))
        .Parameters.Append .CreateParameter("@tablaId", adInteger, adParamInput, , Val(tablaId))
        .Parameters.Append .CreateParameter("@sol_archivo", adChar, adParamInput, 80, sol_archivo)
        .Parameters.Append .CreateParameter("@sol_eme", adChar, adParamInput, 1, sol_eme)
        .Parameters.Append .CreateParameter("@sol_diferida", adChar, adParamInput, 1, sol_diferida)
        Set aplRST = .Execute
    End With
    sp_InsertSolicitudesSSDD = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertSolicitudesSSDD = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateSolicitudesSSDD(sol_nroasignado, sol_mask, jus_codigo, sol_texto, pet_nroasignado, pet_nrointerno, tablaId, sol_archivo, sol_eme, sol_diferida) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateSolicitudesSSDD"
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamInput, , sol_nroasignado)
        .Parameters.Append .CreateParameter("@sol_mask", adChar, adParamInput, 1, sol_mask)
        .Parameters.Append .CreateParameter("@jus_codigo", adInteger, adParamInput, , jus_codigo)
        .Parameters.Append .CreateParameter("@sol_texto", adChar, adParamInput, 255, sol_texto)
        .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , pet_nroasignado)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@tablaId", adInteger, adParamInput, , tablaId)
        .Parameters.Append .CreateParameter("@sol_archivo", adChar, adParamInput, 80, sol_archivo)
        .Parameters.Append .CreateParameter("@sol_eme", adChar, adParamInput, 1, sol_eme)
        .Parameters.Append .CreateParameter("@sol_diferida", adChar, adParamInput, 1, sol_diferida)
        Set aplRST = .Execute
    End With
    sp_UpdateSolicitudesSSDD = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateSolicitudesSSDD = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteSolicitudesSSDD(sol_nroasignado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteSolicitudesSSDD"
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamInput, , Val(sol_nroasignado))
        Set aplRST = .Execute
    End With
    sp_DeleteSolicitudesSSDD = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteSolicitudesSSDD = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSolicitudesSSDD(sol_nroasignado, sol_recurso, sol_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetSolicitudesSSDD"
        .Parameters.Append .CreateParameter("@sol_nroasignado", adInteger, adParamInput, , sol_nroasignado)
        .Parameters.Append .CreateParameter("@sol_recurso", adChar, adParamInput, 10, IIf(sol_recurso = "", Null, sol_recurso))
        .Parameters.Append .CreateParameter("@sol_estado", adChar, adParamInput, 1, IIf(sol_estado = "", Null, sol_estado))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetSolicitudesSSDD = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSolicitudesSSDD = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
