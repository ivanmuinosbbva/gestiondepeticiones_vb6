Attribute VB_Name = "spPeticionBeneficios"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertPeticionBeneficios(pet_nrointerno, indicadorId, subindicadorId, Valor) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionBeneficios"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , indicadorId)
        .Parameters.Append .CreateParameter("@subindicadorId", adInteger, adParamInput, , subindicadorId)
        .Parameters.Append .CreateParameter("@valor", adInteger, adParamInput, , Valor)
        Set aplRST = .Execute
    End With
    sp_InsertPeticionBeneficios = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionBeneficios = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionBeneficios(pet_nrointerno, indicadorId, subindicadorId, Valor) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionBeneficios"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , indicadorId)
        .Parameters.Append .CreateParameter("@subindicadorId", adInteger, adParamInput, , subindicadorId)
        .Parameters.Append .CreateParameter("@valor", adInteger, adParamInput, , IIf(IsNull(Valor), Null, Valor))
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionBeneficios = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionBeneficios = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionBeneficios(pet_nrointerno, indicadorId, subindicadorId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionBeneficios"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , IIf(IsNull(indicadorId), Null, indicadorId))
        .Parameters.Append .CreateParameter("@subindicadorId", adInteger, adParamInput, , IIf(IsNull(subindicadorId), Null, subindicadorId))
        Set aplRST = .Execute
    End With
    sp_DeletePeticionBeneficios = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionBeneficios = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionKPI(pet_nrointerno, Empresa) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionKPI"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , IIf(pet_nrointerno = "", Null, CLng(Val(pet_nrointerno))))
        .Parameters.Append .CreateParameter("@empresa", adInteger, adParamInput, , IIf(IsNull(Empresa), Null, Empresa))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionKPI = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionKPI = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionBeneficios(pet_nrointerno, exportacion, Empresa) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionBeneficios"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , IIf(pet_nrointerno = "", Null, CLng(Val(pet_nrointerno))))
        .Parameters.Append .CreateParameter("@export", adChar, adParamInput, 1, IIf(IsNull(exportacion), "N", "S"))
        .Parameters.Append .CreateParameter("@empresa", adInteger, adParamInput, , IIf(IsNull(Empresa), Null, Empresa))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionBeneficios = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionBeneficios = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'Function sp_GetValoracion(driver, valorId) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetValoracion"
'        .Parameters.Append .CreateParameter("@driver", adInteger, adParamInput, , IIf(IsNull(driver), Null, driver))
'        .Parameters.Append .CreateParameter("@valorId", adInteger, adParamInput, , IIf(IsNull(valorId) Or valorId = "", Null, valorId))
'        Set aplRST = .Execute
'    End With
'    If Not aplRST.EOF Then
'        sp_GetValoracion = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetValoracion = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function

Function sp_InsertPeticionKPI(pet_nrointerno, pet_kpiid, pet_kpidesc, kpi_unimedId, valorEsperado, valorPartida, tiempoEsperado, unimedTiempo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionKPI"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@pet_kpiid", adInteger, adParamInput, , pet_kpiid)
        .Parameters.Append .CreateParameter("@pet_kpidesc", adChar, adParamInput, 150, Mid(pet_kpidesc, 1, 150))
        .Parameters.Append .CreateParameter("@kpi_unimedId", adChar, adParamInput, 10, Mid(kpi_unimedId, 1, 10))
        .Parameters.Append .CreateParameter("@valorEsperado", adDouble, adParamInput, , IIf(valorEsperado = "", 0, CDbl(valorEsperado)))
        .Parameters.Append .CreateParameter("@valorPartida", adDouble, adParamInput, , IIf(valorPartida = "", 0, CDbl(valorPartida)))
        .Parameters.Append .CreateParameter("@tiempoEsperado", adDouble, adParamInput, , IIf(tiempoEsperado = "", 0, CDbl(tiempoEsperado)))
        .Parameters.Append .CreateParameter("@unimedTiempo", adChar, adParamInput, 10, Mid(unimedTiempo, 1, 10))
        Set aplRST = .Execute
    End With
    sp_InsertPeticionKPI = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionKPI = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionKPI(pet_nrointerno, petkpiId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionKPI"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@petkpiId", adInteger, adParamInput, , IIf(IsNull(petkpiId), Null, petkpiId))
        Set aplRST = .Execute
    End With
    sp_DeletePeticionKPI = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionKPI = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetKPIUniMed(kpiid) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetKPIUniMed"
        .Parameters.Append .CreateParameter("@kpiid", adInteger, adParamInput, , IIf(IsNull(kpiid) Or kpiid = "", Null, kpiid))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetKPIUniMed = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetKPIUniMed = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'GMT01 - INI
Function sp_GetPeticionBeneficios2(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionBeneficios2"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , IIf(pet_nrointerno = "", Null, CLng(Val(pet_nrointerno))))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionBeneficios2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionBeneficios2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'GMT01 - FIN
