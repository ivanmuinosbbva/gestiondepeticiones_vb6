Attribute VB_Name = "spRecurso"
' -001- a. FJS 23.04.2008 - Se agrega la llamada a un nuevo SP para determinar si el recurso pertenece al grupo Homologador.
' -001- b. FJS 23.04.2008 - Se agrega la funci�n booleana EsRecursoHomologador para determinar si un recurso pertenece al grupo homologador o no.
' -002- a. FJS 06.05.2008 - Se agrega un nuevo procedimiento para llamar a una nueva versi�n del SP GetRecursosActuanteArea (m�s performante).
' -003- a. FJS 27.06.2008 - Se agrega un SP para registrar el ingreso y egreso de usuarios al sistema
' -003- b. FJS 23.12.2008 - Se agrega una variable para guardar el usuario real.
' -004- a. FJS 15.01.2009 - Nuevo SP para actualizar datos espec�ficos de la tabla de Recursos.
' -004- b. FJS 15.01.2009 - Nuevo SP para devolver datos espec�ficos de la tabla de RecursosSeguridad.
' -005- a. FJS 10.12.2009 - Se reactiva porque se utilizar� (ya modificado) para las opciones de correo electr�nico).
' -006- a. FJS 04.06.2015 - Nuevo: nuevo SP para obtener la direcci�n de email de un recurso.

Option Explicit

Function sp_GetRecurso(cod_query, modo_query, Optional flg_estado) As Boolean
    On Error Resume Next
    If IsMissing(flg_estado) Then
        flg_estado = Null
    End If
    If aplRST.State <> adStateClosed Then aplRST.Close
    'aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecurso"
      .Parameters.Append .CreateParameter("@cod_query", adChar, adParamInput, 10, cod_query)
      .Parameters.Append .CreateParameter("@modo_query", adChar, adParamInput, 2, modo_query)
      .Parameters.Append .CreateParameter("@flg_estado", adChar, adParamInput, 1, IIf(flg_estado = "NULL", "", flg_estado))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecurso = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRecursoOtros(cod_query, modo_query, Optional flg_estado) As Boolean
    On Error Resume Next
    If IsMissing(flg_estado) Then
        flg_estado = Null
    End If
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoOtros"
      .Parameters.Append .CreateParameter("@cod_query", adChar, adParamInput, 10, cod_query)
      .Parameters.Append .CreateParameter("@modo_query", adChar, adParamInput, 2, modo_query)
      .Parameters.Append .CreateParameter("@flg_estado", adChar, adParamInput, 1, IIf(flg_estado = "NULL", "", flg_estado))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecursoOtros = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoOtros = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -xxx- a.
Function sp_GetRecurso2() As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecurso2"
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecurso2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecurso2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Function sp_GetRecursoPosible(cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoPosible"
      .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, cod_recurso)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
     sp_GetRecursoPosible = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoPosible = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertRecurso(cod_recurso, _
                        nom_recurso, _
                        vinculo, _
                        cod_direccion, _
                        cod_gerencia, _
                        cod_sector, _
                        cod_grupo, _
                        flg_cargoarea, _
                        estado_recurso, _
                        horasdiarias, _
                        observaciones, _
                        dlg_recurso, _
                        dlg_desde, _
                        dlg_hasta, _
                        email, _
                        euser) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsertRecurso"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@nom_recurso", adChar, adParamInput, 50, nom_recurso)
      .Parameters.Append .CreateParameter("@vinculo", adChar, adParamInput, 1, vinculo)
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@flg_cargoarea", adChar, adParamInput, 1, flg_cargoarea)
      .Parameters.Append .CreateParameter("@estado_recurso", adChar, adParamInput, 1, estado_recurso)
      .Parameters.Append .CreateParameter("@horasdiarias", adSmallInt, adParamInput, , CInt(horasdiarias))
      .Parameters.Append .CreateParameter("@observaciones", adChar, adParamInput, 255, ClearNull(observaciones))
      .Parameters.Append .CreateParameter("@dlg_recurso", adChar, adParamInput, 10, dlg_recurso)
      .Parameters.Append .CreateParameter("@dlg_desde", SQLDdateType, adParamInput, , dlg_desde)
      .Parameters.Append .CreateParameter("@dlg_hasta", SQLDdateType, adParamInput, , dlg_hasta)
      .Parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 60, email)
      .Parameters.Append .CreateParameter("@euser", adChar, adParamInput, 1, euser)
      Set aplRST = .Execute
    End With
    sp_InsertRecurso = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRecurso(cod_recurso, _
                        nom_recurso, _
                        vinculo, _
                        cod_direccion, _
                        cod_gerencia, _
                        cod_sector, _
                        cod_grupo, _
                        flg_cargoarea, _
                        estado_recurso, _
                        horasdiarias, _
                        observaciones, _
                        dlg_recurso, _
                        dlg_desde, _
                        dlg_hasta, _
                        email, _
                        euser) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateRecurso"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@nom_recurso", adChar, adParamInput, 50, nom_recurso)
      .Parameters.Append .CreateParameter("@vinculo", adChar, adParamInput, 1, vinculo)
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@flg_cargoarea", adChar, adParamInput, 1, flg_cargoarea)
      .Parameters.Append .CreateParameter("@estado_recurso", adChar, adParamInput, 1, estado_recurso)
      .Parameters.Append .CreateParameter("@horasdiarias", adSmallInt, adParamInput, , CInt(horasdiarias))
      .Parameters.Append .CreateParameter("@observaciones", adChar, adParamInput, 255, ClearNull(observaciones))
      .Parameters.Append .CreateParameter("@dlg_recurso", adChar, adParamInput, 10, dlg_recurso)
      .Parameters.Append .CreateParameter("@dlg_desde", SQLDdateType, adParamInput, , dlg_desde)
      .Parameters.Append .CreateParameter("@dlg_hasta", SQLDdateType, adParamInput, , dlg_hasta)
      .Parameters.Append .CreateParameter("@email", adVarChar, adParamInput, 60, email)
      .Parameters.Append .CreateParameter("@euser", adChar, adParamInput, 1, euser)
      Set aplRST = .Execute
    End With
    sp_UpdateRecurso = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRecursoEstructura(cod_recurso, cod_direccion, cod_gerencia, cod_sector, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateRecursoEstructura"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    sp_UpdateRecursoEstructura = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateRecursoEstructura = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRecursoField(cod_recurso, _
                            campo, _
                            valortxt, _
                            valordate, _
                            valorNum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateRecursoField"
      .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@campo", adVarChar, adParamInput, 15, campo)
      .Parameters.Append .CreateParameter("@valortxt", adVarChar, adParamInput, 10, valortxt)
      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
      .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , valorNum)
      Set aplRST = .Execute
    End With
    sp_UpdateRecursoField = True
'    If Not IsEmpty(aplRST) And (Not aplRST.EOF) And aplRST(0).Name = "ErrCode" Then
'        MsgBox (AnalizarErrorSQL(aplRST, 30000))
'        sp_UpdateGrupoGES = False
'    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateRecursoField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteRecurso(cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteRecurso"
      .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, cod_recurso)
      Set aplRST = .Execute
    End With
    sp_DeleteRecurso = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRecursoActuantePeticion(pet_nrointerno, cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoActuantePeticion"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, ClearNull(cod_estado))
      Set aplRST = .Execute
    End With
    sp_GetRecursoActuantePeticion = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoActuantePeticion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRecursoActuanteEstado(cod_alcance, cod_nivel, cod_area, cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoActuanteEstado"
      .Parameters.Append .CreateParameter("@cod_alcance", adChar, adParamInput, 3, ClearNull(cod_alcance))
      .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, ClearNull(cod_nivel))
      .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, ClearNull(cod_area))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, ClearNull(cod_estado))
      Set aplRST = .Execute
    End With
    sp_GetRecursoActuanteEstado = False
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    If Not (aplRST.EOF) Then
     sp_GetRecursoActuanteEstado = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox AnalizarErrorSQL(aplRST, Err, aplCONN), vbExclamation + vbOKOnly, "Advertencia"
    sp_GetRecursoActuanteEstado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DelegaRecurso(cod_recurso, dlg_recurso, dlg_desde, dlg_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelegaRecurso"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@dlg_recurso", adChar, adParamInput, 10, dlg_recurso)
      .Parameters.Append .CreateParameter("@dlg_desde", SQLDdateType, adParamInput, , dlg_desde)
      .Parameters.Append .CreateParameter("@dlg_hasta", SQLDdateType, adParamInput, , dlg_hasta)
      Set aplRST = .Execute
    End With
    sp_DelegaRecurso = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelegaRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRecursoActuanteArea(cod_direccion, cod_gerencia, cod_sector, cod_grupo, perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoActuanteArea"
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, ClearNull(cod_direccion))
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, ClearNull(cod_gerencia))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, ClearNull(cod_sector))
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, ClearNull(cod_grupo))
      .Parameters.Append .CreateParameter("@perfil", adChar, adParamInput, 8, ClearNull(perfil))
      Set aplRST = .Execute
    End With
    sp_GetRecursoActuanteArea = False
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    If Not (aplRST.EOF) Then
        sp_GetRecursoActuanteArea = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoActuanteArea = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -002- a.
Function sp_GetRecursoActuanteArea2(cod_direccion, cod_gerencia, cod_sector, cod_grupo, perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoActuanteArea2"
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@perfil", adChar, adParamInput, 8, ClearNull(perfil))
      Set aplRST = .Execute
    End With
    sp_GetRecursoActuanteArea2 = False
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    If Not (aplRST.EOF) Then
        sp_GetRecursoActuanteArea2 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoActuanteArea2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -005- a.
' *** SE BORRA PORQUE NO SIRVE - AHORA HAY OTRO (15.05.2009) *** Esto es viejo, estaba de antes, ahora se reactiv� (10.12.2009)
Function sp_GetRecursoAcargoArea(cod_direccion, _
                                 cod_gerencia, _
                                 cod_sector, _
                                 cod_grupo) As Boolean
    On Error Resume Next
    sp_GetRecursoAcargoArea = False
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetRecursoAcargoArea"
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetRecursoAcargoArea = True
    End If
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST1)) And (Not aplRST1.State = adStateClosed)) Then
        If ((Not aplRST1.EOF) And (aplRST1(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoAcargoArea = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -001- a.
Public Function sp_GetRecursoEsHomologacion(cod_recurso) As Boolean
    On Error Resume Next
    If aplRST.State <> adStateClosed Then aplRST.Close
    'aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoEsHomologacion"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetRecursoEsHomologacion = True
    End If
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetRecursoEsHomologacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -001- b.
Public Function EsRecursoHomologador(cRecurso As String) As Boolean
    EsRecursoHomologador = False
    If sp_GetRecursoEsHomologacion(cRecurso) Then
        If Not aplRST.EOF Then
            EsRecursoHomologador = True
            aplRST.Close
        End If
    End If
End Function
'}

'{ add -003- a.
Function sp_GetRecursosApp(Tipo, cod_recurso, cod_equipo, fe_ingreso, fe_egreso, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = mdiPrincipal.AdoConnection.ConexionBase
        .CommandType = adCmdStoredProc
        Select Case Tipo
            Case "ALL"
                .CommandText = "sp_GetRecursosApp"
            Case "ON"
                .CommandText = "sp_GetRecursosAppOn"
        End Select
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_equipo", adChar, adParamInput, 20, cod_equipo)
        .Parameters.Append .CreateParameter("@fe_ingreso", SQLDdateType, adParamInput, , fe_ingreso)
        .Parameters.Append .CreateParameter("@fe_egreso", SQLDdateType, adParamInput, , fe_egreso)
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetRecursosApp = True
    Else
        sp_GetRecursosApp = False
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetRecursosApp = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertRecursoApp(cod_recurso, cod_equipo, fe_ingreso, hr_ingreso) As Boolean
    Dim cUsuarioReal As String      ' add -003- b.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    
    cUsuarioReal = cstmGetUserName()
    With objCommand
        .ActiveConnection = mdiPrincipal.AdoConnection.ConexionBase
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertRecursoApp"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_equipo", adChar, adParamInput, 20, cod_equipo)
        .Parameters.Append .CreateParameter("@fe_ingreso", SQLDdateType, adParamInput, , fe_ingreso)
        '.Parameters.Append .CreateParameter("@fe_ingreso", adDBTime, adParamInput, , fe_ingreso)
        .Parameters.Append .CreateParameter("@hr_ingreso", adChar, adParamInput, 8, hr_ingreso)
        .Parameters.Append .CreateParameter("@cod_usuario", adChar, adParamInput, 8, cUsuarioReal)  ' add -003- b.
        Set aplRST = .Execute
    End With
    sp_InsertRecursoApp = True
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    If aplRST.State <> 0 Then
        MsgBox (AnalizarErrorSQL(aplRST, Err))
    Else
        MsgBox IIf(Len(Err.source) > 0, Err.source & vbCrLf, "") & Err.DESCRIPTION & " (" & Err.Number & ")"
    End If
    sp_InsertRecursoApp = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRecursoApp(cod_recurso, cod_equipo, fe_ingreso, hr_ingreso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = mdiPrincipal.AdoConnection.ConexionBase
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateRecursoApp"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_equipo", adChar, adParamInput, 20, cod_equipo)
        .Parameters.Append .CreateParameter("@fe_ingreso", SQLDdateType, adParamInput, , fe_ingreso)
        .Parameters.Append .CreateParameter("@hr_ingreso", adChar, adParamInput, 8, hr_ingreso)
        Set aplRST = .Execute
    End With
    sp_UpdateRecursoApp = True
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdateRecursoApp = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -004- a.
Function sp_GetRecursoSeg(cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = mdiPrincipal.AdoConnection.ConexionBase
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetRecursoSeg"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        Set aplRST = .Execute
    End With
    sp_GetRecursoSeg = True
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetRecursoSeg = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertRecursoSeg(cod_recurso, fe_actualizacion, recurso_data) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = mdiPrincipal.AdoConnection.ConexionBase
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertRecursoSeg"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@fe_actualizacion", SQLDdateType, adParamInput, , fe_actualizacion)
        .Parameters.Append .CreateParameter("@recurso_data", adChar, adParamInput, 24, recurso_data)
        Set aplRST = .Execute
    End With
    sp_InsertRecursoSeg = True
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_InsertRecursoSeg = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Function sp_GetRecursoEstructura(cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoEstructura"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecursoEstructura = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoEstructura = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -xxx-
Function sp_GetRecursoPerfilArea(cod_recurso, cod_perfil, nivel, area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoPerfilArea"
      .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@nivel", adChar, adParamInput, 4, nivel)
      .Parameters.Append .CreateParameter("@area", adChar, adParamInput, 8, area)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecursoPerfilArea = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoPerfilArea = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -006- a.
Function sp_GetRecursoMail(cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetRecursoMail"
        .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, cod_recurso)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetRecursoMail = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRecursoMail = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
