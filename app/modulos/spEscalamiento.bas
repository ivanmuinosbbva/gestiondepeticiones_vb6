Attribute VB_Name = "spEscalamiento"
Option Explicit

Function sp_GetEscalamiento(evt_alcance, cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetEscalamiento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetEscalamiento = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetEscalamiento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateEscalamiento(evt_alcance, cod_estado, pzo_desde, pzo_hasta, cod_accion, fec_activo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
'      .CommandType = adCmdText
      .CommandText = "sp_UpdateEscalamiento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@pzo_desde", adSmallInt, adParamInput, , CInt(pzo_desde))
      .Parameters.Append .CreateParameter("@pzo_hasta", adSmallInt, adParamInput, , CInt(pzo_hasta))
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@fec_activo", SQLDdateType, adParamInput, , fec_activo)
      Set aplRST = .Execute
    End With
    sp_UpdateEscalamiento = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateEscalamiento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsEscalamiento(evt_alcance, cod_estado, pzo_desde, pzo_hasta, cod_accion, fec_activo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_InsEscalamiento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@pzo_desde", adSmallInt, adParamInput, , CInt(pzo_desde))
      .Parameters.Append .CreateParameter("@pzo_hasta", adSmallInt, adParamInput, , CInt(pzo_hasta))
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@fec_activo", SQLDdateType, adParamInput, , fec_activo)
      Set aplRST = .Execute
    End With
    sp_InsEscalamiento = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsEscalamiento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function


Function sp_DelEscalamiento(evt_alcance, cod_estado, pzo_desde, pzo_hasta, cod_accion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelEscalamiento"
      .Parameters.Append .CreateParameter("@evt_alcance", adChar, adParamInput, 3, evt_alcance)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@pzo_desde", adSmallInt, adParamInput, , CInt(pzo_desde))
      .Parameters.Append .CreateParameter("@pzo_hasta", adSmallInt, adParamInput, , CInt(pzo_hasta))
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      Set aplRST = .Execute
    End With
    sp_DelEscalamiento = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelEscalamiento = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

