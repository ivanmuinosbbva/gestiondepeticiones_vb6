Attribute VB_Name = "spDrivers"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertDrivers(driverId, driverNom, driverAyuda, driverValor, driverResp, driverHab, indicadorId) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertDrivers"
                .Parameters.Append .CreateParameter("@driverId", adInteger, adParamInput, , driverId)
                .Parameters.Append .CreateParameter("@driverNom", adChar, adParamInput, 255, driverNom)
                .Parameters.Append .CreateParameter("@driverAyuda", adChar, adParamInput, 255, driverAyuda)
                .Parameters.Append .CreateParameter("@driverValor", adInteger, adParamInput, , driverValor)
                .Parameters.Append .CreateParameter("@driverResp", adChar, adParamInput, 4, driverResp)
                .Parameters.Append .CreateParameter("@driverHab", adChar, adParamInput, 1, driverHab)
                .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , indicadorId)
                Set aplRST = .Execute
        End With
        sp_InsertDrivers = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_InsertDrivers = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateDrivers(driverId, driverNom, driverAyuda, driverValor, driverResp, driverHab, indicadorId) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateDrivers"
                .Parameters.Append .CreateParameter("@driverId", adInteger, adParamInput, , driverId)
                .Parameters.Append .CreateParameter("@driverNom", adChar, adParamInput, 255, driverNom)
                .Parameters.Append .CreateParameter("@driverAyuda", adChar, adParamInput, 255, driverAyuda)
                .Parameters.Append .CreateParameter("@driverValor", adInteger, adParamInput, , driverValor)
                .Parameters.Append .CreateParameter("@driverResp", adChar, adParamInput, 4, driverResp)
                .Parameters.Append .CreateParameter("@driverHab", adChar, adParamInput, 1, driverHab)
                .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , indicadorId)
                Set aplRST = .Execute
        End With
        sp_UpdateDrivers = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateDrivers = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteDrivers(driverId) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteDrivers"
                .Parameters.Append .CreateParameter("@driverId", adInteger, adParamInput, , driverId)
                Set aplRST = .Execute
        End With
        sp_DeleteDrivers = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteDrivers = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetDrivers(driverId, hab, Optional indicador, Optional empresa) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetDrivers"
        .Parameters.Append .CreateParameter("@driverId", adInteger, adParamInput, , driverId)
        .Parameters.Append .CreateParameter("@hab", adChar, adParamInput, 1, hab)
        .Parameters.Append .CreateParameter("@indicador", adInteger, adParamInput, , indicador)
        If Not IsMissing(empresa) Then
            .Parameters.Append .CreateParameter("@empresa", adInteger, adParamInput, , empresa)
        End If
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetDrivers = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetDrivers = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

