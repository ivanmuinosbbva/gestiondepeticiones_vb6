Attribute VB_Name = "spEstados"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertEstados(cod_estado, nom_estado, flg_rnkup, flg_herdlt1, flg_petici, flg_secgru, IGM_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertEstados"
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, , cod_estado)
        .Parameters.Append .CreateParameter("@nom_estado", adChar, adParamInput, , nom_estado)
        .Parameters.Append .CreateParameter("@flg_rnkup", adChar, adParamInput, , flg_rnkup)
        .Parameters.Append .CreateParameter("@flg_herdlt1", adChar, adParamInput, , flg_herdlt1)
        .Parameters.Append .CreateParameter("@flg_petici", adChar, adParamInput, , flg_petici)
        .Parameters.Append .CreateParameter("@flg_secgru", adChar, adParamInput, , flg_secgru)
        .Parameters.Append .CreateParameter("@IGM_hab", adChar, adParamInput, , IGM_hab)
        Set aplRST = .Execute
    End With
    sp_InsertEstados = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_InsertEstados = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateEstados(cod_estado, nom_estado, flg_rnkup, flg_herdlt1, flg_petici, flg_secgru, IGM_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateEstados"
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, , cod_estado)
        .Parameters.Append .CreateParameter("@nom_estado", adChar, adParamInput, , nom_estado)
        .Parameters.Append .CreateParameter("@flg_rnkup", adChar, adParamInput, , flg_rnkup)
        .Parameters.Append .CreateParameter("@flg_herdlt1", adChar, adParamInput, , flg_herdlt1)
        .Parameters.Append .CreateParameter("@flg_petici", adChar, adParamInput, , flg_petici)
        .Parameters.Append .CreateParameter("@flg_secgru", adChar, adParamInput, , flg_secgru)
        .Parameters.Append .CreateParameter("@IGM_hab", adChar, adParamInput, , IGM_hab)
        Set aplRST = .Execute
    End With
    sp_UpdateEstados = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdateEstados = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteEstados(cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteEstados"
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, , cod_estado)
        Set aplRST = .Execute
    End With
    sp_DeleteEstados = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_DeleteEstados = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetEstados(cod_estado, nom_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetEstados"
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@nom_estado", adChar, adParamInput, 30, nom_estado)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetEstados = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetEstados = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
