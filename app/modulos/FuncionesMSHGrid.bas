Attribute VB_Name = "FuncionesMSHGrid"
Option Explicit

'Example submitted by Leondias Frost (a993435@yahoo.com)
Private Const MF_CHECKED = &H8&
Private Const MF_APPEND = &H100&
Private Const MF_DISABLED = &H2&
Private Const MF_GRAYED = &H1&
Private Const MF_SEPARATOR = &H800&
Private Const MF_STRING = &H0&
Private Const TPM_LEFTALIGN = &H0&
Private Const TPM_RETURNCMD = &H100&
Private Const TPM_RIGHTBUTTON = &H2&

Private Const GRIDFILLROWCOLOR_Green = 14346960
Private Const GRIDFILLROWCOLOR_Blue = 16762520
Private Const GRIDFILLROWCOLOR_Rose = 14209532
Private Const GRIDFILLROWCOLOR_DarkGreen = 8963736
Private Const GRIDFILLROWCOLOR_Red = 10727930
Private Const GRIDFILLROWCOLOR_Yellow = 13170677
Private Const GRIDFILLROWCOLOR_LightBlue = 16245683
Private Const GRIDFILLROWCOLOR_LightOrange = 14346750
Private Const GRIDFILLROWCOLOR_LightGrey = 12632256
Private Const GRIDFILLROWCOLOR_LightGrey2 = &HF4F4F4
Private Const GRIDFILLROWCOLOR_LightGrey4 = &H8000000F
Private Const GRIDFILLROWCOLOR_DarkGrey = 12632256
Private Const GRIDFILLROWCOLOR_LightBlue2 = 16708320
Private Const GRIDFILLROWCOLOR_DarkGreen2 = &H8000&
Private Const GRIDFILLROWCOLOR_LightGreen = 15399140
Private Const GRIDFILLROWCOLOR_DarkRed = 4873437
Private Const GRIDFILLROWCOLOR_White = 16777215
Private Const GRIDFILLROWCOLOR_Black = 0

'Private Const GRIDFILLROWCOLOR_Green = 14346960
'Private Const GRIDFILLROWCOLOR_Blue = 16762520
'Private Const GRIDFILLROWCOLOR_Rose = 14209532
'Private Const GRIDFILLROWCOLOR_DarkGreen = 8963736
'Private Const GRIDFILLROWCOLOR_Red = 10727930
'Private Const GRIDFILLROWCOLOR_Yellow = 13170677
'Private Const GRIDFILLROWCOLOR_LightBlue = 16245683
'Private Const GRIDFILLROWCOLOR_LightOrange = 14346750
'Private Const GRIDFILLROWCOLOR_LightGrey = 12632256
'Private Const GRIDFILLROWCOLOR_LightGrey2 = &HF4F4F4
'Private Const GRIDFILLROWCOLOR_DarkGrey = 12632256
'Private Const GRIDFILLROWCOLOR_LightBlue2 = 16708320
'Private Const GRIDFILLROWCOLOR_DarkGreen2 = &H8000&
'Private Const GRIDFILLROWCOLOR_LightGreen = 15399140
'Private Const GRIDFILLROWCOLOR_DarkRed = 4873437
'Private Const GRIDFILLROWCOLOR_White = 16777215
'Private Const GRIDFILLROWCOLOR_Black = 0

' declaraciones del api
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" ( _
    ByVal hWnd As Long, _
    ByVal nIndex As Long, _
    ByVal dwNewLong As Long) As Long

Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" ( _
    ByVal lpPrevWndFunc As Long, _
    ByVal hWnd As Long, _
    ByVal Msg As Long, _
    ByVal wParam As Long, _
    ByVal lParam As Long) As Long

Private Declare Function SendMessage Lib "user32.dll" Alias "SendMessageA" ( _
    ByVal hWnd As Long, _
    ByVal Msg As Long, _
    wParam As Any, lParam As Any) As Long

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Constantes
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Const GWL_WNDPROC = (-4)
Private Const WM_MOUSEWHEEL = &H20A
Private Const WM_VSCROLL As Integer = &H115

Dim PrevProc As Long

Private Type POINTAPI
    x As Long
    y As Long
End Type

Private Declare Function CreatePopupMenu Lib "user32" () As Long
Private Declare Function TrackPopupMenuEx Lib "user32" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal x As Long, ByVal y As Long, ByVal hWnd As Long, ByVal lptpm As Any) As Long
Private Declare Function AppendMenu Lib "user32" Alias "AppendMenuA" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpNewItem As Any) As Long
Private Declare Function DestroyMenu Lib "user32" (ByVal hMenu As Long) As Long
Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long

Dim hMenu As Long
Dim m_lastFind As Integer
Dim m_strFind As String
Dim m_lastCOL As Integer

Sub MenuGridFind2(objGrid As MSHFlexGrid)
    Dim pt As POINTAPI
    Dim ret As Long
    
    If objGrid.Rows = 1 Then Exit Sub
    
    If m_lastCOL <> objGrid.MouseCol Then
        m_lastCOL = objGrid.MouseCol
        m_lastFind = 0
    End If
    
    hMenu = CreatePopupMenu()
    AppendMenu hMenu, MF_GRAYED Or MF_DISABLED, 1, "Columna: " & objGrid.TextMatrix(0, m_lastCOL)
    AppendMenu hMenu, MF_SEPARATOR, ByVal 0&, ByVal 0&
    AppendMenu hMenu, MF_STRING, 2, "Buscar"
    If m_lastFind > 0 Then
        AppendMenu hMenu, MF_STRING, 3, "Pr�ximo"
    Else
        AppendMenu hMenu, MF_GRAYED Or MF_DISABLED, 3, "Pr�ximo"
    End If
    GetCursorPos pt
    ret = TrackPopupMenuEx(hMenu, TPM_LEFTALIGN Or TPM_RETURNCMD Or TPM_RIGHTBUTTON, pt.x, pt.y, objGrid.hWnd, ByVal 0&)
    DestroyMenu hMenu
    Select Case ret
    Case 2
        m_lastFind = 0
        If objGrid.Rows > 1 Then
            m_strFind = InputBox("Ingrese el texto a buscar:", "Buscar", m_strFind)
            If m_strFind <> "" Then
                Call Find(objGrid, m_lastFind, m_lastCOL, m_strFind)
            End If
        End If
    Case 3
        m_lastFind = m_lastFind + 1
        Call Find(objGrid, m_lastFind, m_lastCOL, m_strFind)
    End Select
    'Debug.Print ret
End Sub

Function skipAcento2(strIn As String) As String
    Dim i As Integer
    strIn = UCase(strIn)
    For i = 1 To Len(strIn)
        Select Case Mid(strIn, i, 1)
        Case "�", "�"
            Mid(strIn, i, 1) = "A"
        Case "�", "�"
            Mid(strIn, i, 1) = "E"
        Case "�", "�"
            Mid(strIn, i, 1) = "I"
        Case "�", "�"
            Mid(strIn, i, 1) = "O"
        Case "�", "�"
            Mid(strIn, i, 1) = "U"
        End Select
    Next
    skipAcento2 = strIn
End Function

Public Function Find2(objGrid As MSHFlexGrid, ByRef rowInicial As Integer, ptrColumn As Integer, strSeek As String) As Boolean
    'asume que la grila tiene un row de titulos, por eso busca desde i=1
    Dim i As Integer
    Find2 = False
    If rowInicial = 0 Then
        rowInicial = 1
    End If
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        For i = rowInicial To .Rows - 1
           If InStr(skipAcento(Trim(.TextMatrix(i, ptrColumn))), skipAcento(Trim(strSeek))) > 0 Then
                .TopRow = i
                .row = i
                .RowSel = i
                .col = 0
                .ColSel = .cols - 1
                rowInicial = i
                Find2 = True
                Exit Function
           End If
        Next
        For i = 1 To rowInicial
           If InStr(skipAcento(Trim(.TextMatrix(i, ptrColumn))), skipAcento(Trim(strSeek))) > 0 Then
                .TopRow = i
                .row = i
                .RowSel = i
                .col = 0
                .ColSel = .cols - 1
                rowInicial = i
                Find2 = True
                Exit Function
           End If
        Next
    End With
End Function

' Pinta una linea o fila completa de un color determinado en un control MSFlexGrid
Public Sub PintarLinea2(ByRef oGrid As MSHFlexGrid, Optional Color As prmColorFilaGrilla, Optional ColumnaInicial As Integer)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    
    bFormateando = True
    If IsMissing(Color) Then
        Color = 0
    End If
    
    If IsMissing(ColumnaInicial) Then
        ColumnaInicial = 0
    End If
    
    With oGrid
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .RowSel
        original_ColSel = .ColSel
        ' TODO: esto estaba comentado... revisar 16.11.2016
        ' Si no estoy sobre la �ltima, tomo la actual
        If Not .RowSel > 1 Then
            .row = .Rows - 1
        Else
            .row = .RowSel
        End If
        'For X = 0 To .cols - 1
        For x = ColumnaInicial To .cols - 1
            .col = x
            Select Case Color
                Case 1: .CellBackColor = GRIDFILLROWCOLOR_Green        ' Verde claro
                Case 2: .CellBackColor = GRIDFILLROWCOLOR_Blue         ' Azul claro
                Case 3: .CellBackColor = GRIDFILLROWCOLOR_Rose         ' Rosado
                Case 4: .CellBackColor = GRIDFILLROWCOLOR_DarkGreen    ' Verde oscuro
                Case 5: .CellBackColor = GRIDFILLROWCOLOR_Red          ' Rojo claro
                Case 6: .CellBackColor = GRIDFILLROWCOLOR_Yellow
                Case 7: .CellBackColor = GRIDFILLROWCOLOR_LightBlue
                Case 8: .CellBackColor = GRIDFILLROWCOLOR_LightOrange  ' Naranja suave
                Case 9: .CellBackColor = GRIDFILLROWCOLOR_DarkGrey     ' Gris oscuro
                Case 12: .CellBackColor = GRIDFILLROWCOLOR_LightBlue2   ' Azul muy clarito
                Case 13: .CellBackColor = GRIDFILLROWCOLOR_DarkGreen2   ' Otro verde oscuro
                Case 14: .CellBackColor = GRIDFILLROWCOLOR_LightGreen   ' LightGreen
                Case 20: .CellBackColor = GRIDFILLROWCOLOR_DarkRed      ' Rojo oscuro
                Case 21: .CellBackColor = GRIDFILLROWCOLOR_White        ' Blanco
                Case 22: .CellBackColor = GRIDFILLROWCOLOR_Black        ' Negro
                Case 24: .CellBackColor = GRIDFILLROWCOLOR_LightGrey2   ' Gris claro
                Case 25: .CellBackColor = &H8000000F                    ' Gris suave tipo fondo de formulario
                Case 26: .CellBackColor = GRIDFILLROWCOLOR_LightGrey4   ' idem 25
                Case Else
                    .CellBackColor = Color                          ' Definido por el usuario
            End Select
        Next x
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .RowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub PintarCelda2(ByRef oGrid As MSHFlexGrid, Optional Color As prmColorFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    
    bFormateando = True
    If IsMissing(Color) Then
        Color = 0
    End If
    With oGrid
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .RowSel
        original_ColSel = .ColSel
        ' Si no estoy sobre la �ltima, tomo la actual
        'If Not .RowSel > 1 Then
        '    .Row = .Rows - 1
        'Else
        '    .Row = .RowSel
        'End If
        .row = .RowSel
        Select Case Color
            Case 1: .CellBackColor = RGB(208, 234, 218)     ' Verde claro
            Case 2: .CellBackColor = RGB(152, 198, 255)     ' Azul claro
            Case 3: .CellBackColor = RGB(252, 209, 216)     ' Rosa / Rojo
            Case 4: .CellBackColor = RGB(152, 198, 136)     ' Verde oscuro
            Case 5: .CellBackColor = RGB(250, 177, 163)     ' Rojito
            Case 6: .CellBackColor = RGB(245, 247, 200)
            Case 7: .CellBackColor = RGB(179, 227, 247)
            Case 8: .CellBackColor = RGB(254, 233, 218)     ' Naranja suave
            Case 9: .CellBackColor = RGB(190, 190, 190)     ' Gris claro
            Case 10: .CellBackColor = RGB(147, 147, 147)    ' Gris oscuro
            Case 11: .CellBackColor = RGB(41, 28, 208)      ' Azul oscuro (hiperv�nculos)
            Case 12: .CellBackColor = RGB(224, 242, 254)    ' Azul muy clarito
            Case 13: .CellBackColor = &H8000&               ' Otro verde oscuro
            Case 14: .CellBackColor = RGB(228, 248, 234)    ' LightGreen
            Case 20: .CellBackColor = RGB(221, 92, 74)      ' Rojo oscuro
            Case 21: .CellBackColor = RGB(255, 255, 255)    ' Blanco
            Case 22: .CellBackColor = RGB(0, 0, 0)          ' Negro
            Case 23: .CellBackColor = 12582912              ' Azul oscuro
            Case 25: .CellBackColor = RGB(192, 192, 192)    ' Gris suave tipo fondo de formulario
            Case Else: .CellBackColor = Color               ' A elecci�n
        End Select
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .RowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub CambiarForeColorLinea2(ByRef oGrid As MSHFlexGrid, Color As prmColorFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    With oGrid
        bFormateando = True
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .RowSel
        original_ColSel = .ColSel
        ' Si no estoy sobre la �ltima, tomo la actual
        If Not .RowSel > 1 Then
            .row = .Rows - 1
        Else
            .row = .RowSel
        End If
        For x = 0 To .cols - 1
            .col = x
            Select Case Color
                Case 1: .CellForeColor = RGB(208, 234, 218)         ' Verde claro
                Case 2: .CellForeColor = RGB(152, 198, 255)         ' Azul claro
                Case 3: .CellForeColor = RGB(252, 209, 216)         ' Rosa / Rojo
                Case 4: .CellForeColor = RGB(152, 198, 136)         ' Verde oscuro
                Case 5: .CellForeColor = RGB(250, 177, 163)         ' Rojito
                Case 6: .CellForeColor = RGB(245, 247, 200)
                Case 9: .CellForeColor = RGB(190, 190, 190)         ' Gris claro
                Case 10: .CellForeColor = RGB(147, 147, 147)        ' Gris oscuro
                Case 11: .CellForeColor = RGB(41, 28, 208)          ' Azul oscuro (hiperv�nculos)
                Case 13: .CellForeColor = &H8000&                   ' Otro verde oscuro
                Case 20: .CellForeColor = RGB(221, 92, 74)          ' Rojo oscuro
                Case 21: .CellForeColor = RGB(255, 255, 255)        ' Blanco
                Case 22: .CellForeColor = RGB(0, 0, 0)              ' Negro
                Case 23: .CellForeColor = &HC00000                  ' Azul oscuro
                Case Else
                    .CellForeColor = Color                          ' Definido por el usuario
            End Select
        Next x
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .RowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub CambiarForeColorCelda2(ByRef oGrid As MSHFlexGrid, lCol As Long, lRow As Long, Color As prmColorFilaGrilla)
    Dim x As Integer
    With oGrid
        bFormateando = True
        ' Establezco las posiciones a cambiar
        .col = lCol
        .row = lRow
        Select Case Color
            Case 1: .CellForeColor = RGB(208, 234, 218)     ' Verde claro
            Case 2: .CellForeColor = RGB(152, 198, 255)     ' Azul claro
            Case 3: .CellForeColor = RGB(252, 209, 216)     ' Rosa / Rojo
            Case 4: .CellForeColor = RGB(152, 198, 136)     ' Verde oscuro
            Case 5: .CellForeColor = RGB(250, 177, 163)     ' Rojito
            Case 6: .CellForeColor = RGB(245, 247, 200)
            Case 9: .CellForeColor = RGB(190, 190, 190)     ' Gris claro
            Case 10: .CellForeColor = RGB(147, 147, 147)    ' Gris oscuro
            Case 11: .CellForeColor = RGB(41, 28, 208)      ' Azul oscuro (hiperv�nculos)
            Case 13: .CellForeColor = &H8000&               ' Otro verde oscuro
            Case 20: .CellForeColor = RGB(221, 92, 74)      ' Rojo oscuro
            Case 21: .CellForeColor = RGB(255, 255, 255)    ' Blanco
            Case 22: .CellForeColor = RGB(0, 0, 0)          ' Negro
            Case 23: .CellForeColor = &HC00000              ' Azul oscuro
            Case Else
                .CellForeColor = Color                      ' Definido por el usuario
        End Select
    End With
    bFormateando = False
End Sub

Public Sub CambiarEfectoLinea2(ByRef oGrid As MSHFlexGrid, Efecto As prmEfectoFilaGrilla)
    Dim original_Row As Long
    Dim original_Col As Long
    Dim original_RowSel As Long
    Dim original_ColSel As Long
    Dim x As Integer
    
    bFormateando = True
    
    With oGrid
        ' Me guardo las posiciones originales de la selecci�n
        original_Row = .row
        original_Col = .col
        original_RowSel = .RowSel
        original_ColSel = .ColSel
        ' Si no estoy sobre la �ltima, tomo la actual
        If Not .RowSel > 1 Then
            .row = .Rows - 1
        Else
            .row = .RowSel
        End If
        For x = 0 To .cols - 1
            .col = x
            Select Case Efecto
                Case 1: .CellFontBold = True
                Case 2: .CellFontItalic = True
                Case 3: .CellFontStrikeThrough = True
                Case 4: .CellFontUnderline = True
                Case 5: .CellTextStyle = flexTextFlat
                Case 6: .CellTextStyle = flexTextInset
                Case 7: .CellTextStyle = flexTextInsetLight
                Case 8: .CellTextStyle = flexTextRaised
                Case 9: .CellTextStyle = flexTextRaisedLight
            End Select
        Next x
        ' Reestablezco los valores originales luego de modificar la grilla
        .row = original_Row
        .col = original_Col
        .RowSel = original_RowSel
        .ColSel = original_ColSel
    End With
    bFormateando = False
End Sub

Public Sub CambiarEfectoCelda2(ByRef oGrid As MSHFlexGrid, lCol As Long, lRow As Long, Efecto As prmEfectoFilaGrilla)
    Dim x As Integer
    bFormateando = True
    With oGrid
        .col = lCol
        .row = lRow
        Select Case Efecto
            Case 1: .CellFontBold = True
            Case 2: .CellFontItalic = True
            Case 3: .CellFontStrikeThrough = True
            Case 4: .CellFontUnderline = True
            Case 5: .CellTextStyle = flexTextFlat
            Case 6: .CellTextStyle = flexTextInset
            Case 7: .CellTextStyle = flexTextInsetLight
            Case 8: .CellTextStyle = flexTextRaised
            Case 9: .CellTextStyle = flexTextRaisedLight
        End Select
    End With
    bFormateando = False
End Sub

Public Sub OrdenarGrilla2(ByRef oGrilla As MSHFlexGrid, ByRef lUltimaColumnaOrdenada As Long)
    With oGrilla
        If .MouseRow = 0 And .Rows > 1 Then
            .visible = False
            .col = .MouseCol
            If ClearNull(.TextMatrix(0, .col)) = "Ord." Or IsNumeric(.TextMatrix(1, .col)) Then
                If Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortNumericDescending
                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortNumericAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortNumericDescending
                End If
            Else
                If Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortStringNoCaseDescending
                ElseIf Left(.TextMatrix(0, .col), 1) = "�" Then
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    If lUltimaColumnaOrdenada <> .col Then .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortStringNoCaseAscending
                Else
                    ' Quito el ordenamiento de la �ltima columna ordenada
                    If InStr(1, "�|�", Left(.TextMatrix(0, lUltimaColumnaOrdenada), 1), vbTextCompare) > 0 Then
                        .TextMatrix(0, lUltimaColumnaOrdenada) = Mid(.TextMatrix(0, lUltimaColumnaOrdenada), 3)
                    End If
                    .TextMatrix(0, .col) = "� " & .TextMatrix(0, .col)
                    .Sort = flexSortStringNoCaseDescending
                End If
            End If
            lUltimaColumnaOrdenada = .col
            .visible = True
        End If
    End With
End Sub

Public Sub GrillaAgregarItem2(ByRef oGrilla As MSHFlexGrid, ByRef Coleccion As Collection)
    Dim i As Long
    Dim sItem As String

    With oGrilla
        For i = 1 To Coleccion.Count ' - 1
            sItem = sItem & Coleccion.Item(i) & vbTab
        Next i
        .AddItem sItem, .RowSel + 1
    End With
End Sub

Public Sub HookForm(Obj As Object)
    PrevProc = SetWindowLong(Obj.hWnd, GWL_WNDPROC, AddressOf WindowProc)
End Sub

Public Sub UnHookForm(Obj As Object)
    SetWindowLong Obj.hWnd, GWL_WNDPROC, PrevProc
End Sub

' Procedimiento qie intercepta los mensajes de windows, en este caso para _
  interceptar el uso del Scroll del mouse
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function WindowProc(ByVal hWnd As Long, _
                           ByVal uMsg As Long, _
                           ByVal wParam As Long, _
                           ByVal lParam As Long) As Long

    WindowProc = CallWindowProc(PrevProc, hWnd, uMsg, wParam, lParam)
    
    If uMsg = WM_MOUSEWHEEL Then
        If wParam < 0 Then
            ' envia mediante SendMessage el comando para mover el Scroll hacia abajo
            SendMessage hWnd, WM_VSCROLL, ByVal 1, ByVal 0
        Else
            ' Mueve el scroll hacia arriba
            SendMessage hWnd, WM_VSCROLL, ByVal 0, ByVal 0
        End If
    End If
End Function

' ORIGINAL QUE FUNCIONA!!!
'Public Function MenuMSHGridGet(objGrid As MSHFlexGrid, driver) As String
'    Dim Pt As POINTAPI
'    Dim ret As Long
'    Dim i As Integer
'
'    'i = 97
'
'    If objGrid.Rows = 1 Then Exit Function
'    If m_lastCOL <> objGrid.MouseCol Then
'        m_lastCOL = objGrid.MouseCol
'        m_lastFind = 0
'    End If
'
'    hMenu = CreatePopupMenu()
'    AppendMenu hMenu, MF_GRAYED Or MF_DISABLED, 1, "Opciones:"
'    AppendMenu hMenu, MF_SEPARATOR, ByVal 0&, ByVal 0&
'    'AppendMenu hMenu, MF_STRING, CStr("N"), "- "
'    If sp_GetValoracion(driver, Null) Then
'        Do While Not aplRST.EOF
'            'AppendMenu hMenu, MF_STRING, CStr(aplRST.Fields!valorId), CStr(Chr(i)) & ". " & CStr(aplRST.Fields!valorTexto)
'            AppendMenu hMenu, MF_STRING, CStr(aplRST.Fields!valorId), "- " & CStr(aplRST.Fields!valorTexto)
'            aplRST.MoveNext
'            i = i + 1
'            DoEvents
'        Loop
'    End If
'    GetCursorPos Pt
'    ret = TrackPopupMenuEx(hMenu, TPM_LEFTALIGN Or TPM_RETURNCMD Or TPM_RIGHTBUTTON, Pt.X, Pt.Y, objGrid.hwnd, ByVal 0&)
'    DestroyMenu hMenu
'    MenuMSHGridGet = CStr(ret)
'End Function

' Nueva versi�n para evitar las lecturas a la base de datos
Public Function MenuMSHGridGet(objGrid As MSHFlexGrid, driver) As String
    Dim pt As POINTAPI
    Dim ret As Long
    Dim i As Integer
    
    'i = 97
    If drvRST.State = adStateClosed Then
        Call InicializarOpcionesValoracion
    End If
    
    If objGrid.Rows = 1 Then Exit Function
    If m_lastCOL <> objGrid.MouseCol Then
        m_lastCOL = objGrid.MouseCol
        m_lastFind = 0
    End If
    
    hMenu = CreatePopupMenu()
    AppendMenu hMenu, MF_GRAYED Or MF_DISABLED, 1, "Opciones:"
    AppendMenu hMenu, MF_SEPARATOR, ByVal 0&, ByVal 0&
    'AppendMenu hMenu, MF_STRING, CStr("N"), "- "
    'If sp_GetValoracion(driver, Null) Then
    With drvRST
        .Filter = adFilterNone
        .Filter = "valorDriver = " & driver
        .MoveFirst
        Do While Not drvRST.EOF
            AppendMenu hMenu, MF_STRING, CStr(drvRST.Fields!valorId), "- " & CStr(drvRST.Fields!valorTexto)
            drvRST.MoveNext
            'i = i + 1
            DoEvents
        Loop
    End With
    GetCursorPos pt
    ret = TrackPopupMenuEx(hMenu, TPM_LEFTALIGN Or TPM_RETURNCMD Or TPM_RIGHTBUTTON, pt.x, pt.y, objGrid.hWnd, ByVal 0&)
    DestroyMenu hMenu
    MenuMSHGridGet = CStr(ret)
End Function

Public Sub InicializarOpcionesValoracion()
    Set drvRST = New ADODB.Recordset
    
    If sp_GetValoracion(Null, Null) Then
        Set drvRST = aplRST.Clone
        If aplRST.State = adStateOpen Then aplRST.Close
        Set aplRST = Nothing
    End If
End Sub

Public Function MenuMSHGridGet2(objGrid As MSHFlexGrid) As String
    Dim pt As POINTAPI
    Dim ret As Long
    Dim i As Integer
    
'    'i = 97
'    If drvRST.State = adStateClosed Then
'        Call InicializarOpcionesValoracion
'    End If
    
    If objGrid.Rows = 1 Then Exit Function
    If m_lastCOL <> objGrid.MouseCol Then
        m_lastCOL = objGrid.MouseCol
        m_lastFind = 0
    End If
    
    hMenu = CreatePopupMenu()
    AppendMenu hMenu, MF_GRAYED Or MF_DISABLED, 1, "Opciones:"
    AppendMenu hMenu, MF_SEPARATOR, ByVal 0&, ByVal 0&
    
    AppendMenu hMenu, MF_STRING, 1, "- " & CStr("Si")
    AppendMenu hMenu, MF_STRING, 2, "- " & CStr("No")
    
    AppendMenu hMenu, MF_SEPARATOR, ByVal 0&, ByVal 0&
    
    AppendMenu hMenu, MF_STRING, 3, "- " & CStr("Todo si")
    AppendMenu hMenu, MF_STRING, 4, "- " & CStr("Todo no")
    
'    'AppendMenu hMenu, MF_STRING, CStr("N"), "- "
'    With drvRST
'        .Filter = adFilterNone
'        .Filter = "valorDriver = " & driver
'        .MoveFirst
'        Do While Not drvRST.EOF
'            AppendMenu hMenu, MF_STRING, CStr(drvRST.Fields!valorId), "- " & CStr(drvRST.Fields!valorTexto)
'            drvRST.MoveNext
'            'i = i + 1
'            DoEvents
'        Loop
'    End With
    GetCursorPos pt
    ret = TrackPopupMenuEx(hMenu, TPM_LEFTALIGN Or TPM_RETURNCMD Or TPM_RIGHTBUTTON, pt.x, pt.y, objGrid.hWnd, ByVal 0&)
    DestroyMenu hMenu
    MenuMSHGridGet2 = CStr(ret)
End Function
