Attribute VB_Name = "FuncionesCorreo"
' -001- a. FJS 10.12.2009 - Obtengo la direcci�n del responsable a cargo de la gerencia de DYD.
' -002- a. FJS 28.12.2009 - Envio de correo para solicitar restore de backup a Carga de M�quina.
' -003- a. FJS 26.01.2010 - Para la solicitud de restore, se agrega al final del asunto, el n� de solicitud
' -004- a. FJS 15.02.2010 - Por cambios en la estructura en la direcci�n de Medios y en la gerencia DYD, se cambia la direcci�n
'                           de correo electr�nico de Efron por Siciliano.
' -005- a. FJS 16.02.2010 - Bug: estaba tomando el campo equivocado. Se condiciona correctamente.
' -005- b. FJS 10.11.2010 - Se quitan las referencias a la API de Windows que no se usan o redundantes.
' -006- a. AA  29.02.2010 - Se agrega metodo para el envio de mails informativos para los usuarios a los que se le ha rechazado una solicitud.
' -006- b. FJS 21.01.2011 - Arreglo: si el destinatario no tiene email en CGM, deja el campo "Para" en blanco.
' -006- c. FJS 01.02.2011 - Agregado: se agrega una funci�n para el envio de correo a varios destinatarios por rechazo de DSN (SI).
' -007- a. FJS 22.03.2011 - Agregado: se especifica en el correo electr�nico el c�digo del dsn rechazado.

Option Explicit

Private Const VARIABLES_I = "// VARIABLES"
Private Const VARIABLES_F = "// FIN_VARIABLES"

Private Type udtComponentes
    compNombre As String
    compVersionWXP As String
    compVersionW2K As String
    compDescription As String
End Type

Private Type VS_FIXEDFILEINFO
    dwSignature As Long
    dwStrucVersionl As Integer
    dwStrucVersionh As Integer
    dwFileVersionMSl As Integer
    dwFileVersionMSh As Integer
    dwFileVersionLSl As Integer
    dwFileVersionLSh As Integer
    dwProductVersionMSl As Integer
    dwProductVersionMSh As Integer
    dwProductVersionLSl As Integer
    dwProductVersionLSh As Integer
    dwFileFlagsMask As Long
    dwFileFlags As Long
    dwFileOS As Long
    dwFileType As Long
    dwFileSubtype As Long
    dwFileDateMS As Long
    dwFileDateLS As Long
End Type

'{ add -006- c.
Private Type udt_RechazoDSN
    Usuario As String
    Propietario As String
    email As String
    ArchivoProduccion As String
    ArchivoCatalogo As String
    TieneMail As Boolean
    codigo As String
End Type
'}

'Constante para los saltos de l�nea o saltos de carro en el cuerpo del mensaje
Const saltoLinea As String = "%0D%0A"
Const tabulador As String = "%09"

Dim vUsuarios() As udt_RechazoDSN       ' add -006- c.

Public Sub EnviarCorreo_CargaDeMaquinas(lSolicitud As Long)
    Dim objFolder As Outlook.MAPIFolder
    Dim objItem As Outlook.MailItem
    
    Dim strAddress As String
    Dim cTo As String
    Dim cCC As String     ' add -001- a.
    Dim cHTMLBody As String
    
    Set objFolder = adhGetOutlook.GetDefaultFolder(olFolderOutbox)
    Set objItem = objFolder.Items.Add(olMailItem)
    
    '{ add -001- a.
    If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, "", "") Then
        cCC = ClearNull(aplRST.Fields!email)        ' El responsable a cargo de la gerencia del usuario solicitante
    Else
        '{ del -004- a.
        '' Se pone de prepo al gerente Ruben Efron
        'cCC = "refron@correo1.arg.igrupobbva"
        '}
        cCC = "gsiciliano@correo1.arg.igrupobbva"   ' add -004- a. Se pone de prepo al gerente Gustavo Siciliano
    End If
    '}
    
    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then ' add -006- a.
        With objItem
            .BodyFormat = olFormatHTML
            cTo = "CM@correo1.arg.igrupobbva" & "; " & "rev_seginf@correo1.arg.igrupobbva"
            .To = cTo
            .CC = cCC & "; " & ClearNull(aplRST.Fields!mail_recurso)        ' upd -001- a.
            .Subject = "EME - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - Transmisi�n PROD/DESA sin enmascaramiento. Aprobado por Dise�o y Desarrollo"
            ' Cuerpo del mensaje de correo electr�nico
            cHTMLBody = cHTMLBody & "<!DOCTYPE HTML PUBLIC " & Chr(36) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<HTML>"
            cHTMLBody = cHTMLBody & "<HEAD>"
            cHTMLBody = cHTMLBody & "<TITLE> Correo para Carga de M�quina </TITLE>"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Generator" & Chr(36) & " CONTENT=" & Chr(36) & "EditPlus" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Author" & Chr(36) & " CONTENT=" & Chr(36) & "Fernando J. Spitz (Metodolog�a y F�bricas" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Keywords" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Description" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "</HEAD>"
            cHTMLBody = cHTMLBody & "<BODY>"
            cHTMLBody = cHTMLBody & "<PRE>"
            cHTMLBody = cHTMLBody & "<BR>La solicitud de transmisi�n n�mero <B>" & lSolicitud & "</B> ha sido aprobada para ejecutar en ambiente <B>Producci�n.</B>"
            cHTMLBody = cHTMLBody & "<BR>"
            cHTMLBody = cHTMLBody & "<BR>Datos de la transmisi�n solicitada:"
            cHTMLBody = cHTMLBody & "<P><BR>"
            cHTMLBody = cHTMLBody & "<P><BR>"
            cHTMLBody = cHTMLBody & "<P><BR>"
            cHTMLBody = cHTMLBody & "<CENTER>"
            cHTMLBody = cHTMLBody & "<TABLE BORDER=3 WIDTH=50%>"
            cHTMLBody = cHTMLBody & "<CAPTION><B>Solicitud N� " & lSolicitud & "</B></CAPTION>"
            cHTMLBody = cHTMLBody & "<TR><TD>TIPO</TD><TD><B>" & ClearNull(aplRST.Fields!sol_tipo) & "</B></TD></TR>"
            Select Case ClearNull(aplRST.Fields!sol_tipo)
                Case "XCOM"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO IN </TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_prod) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_desa) & "</B></TD></TR>"
                Case "UNLO"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>LIB SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_lib_Sysin) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
                Case "TCOR"
                    cHTMLBody = cHTMLBody & "<TR><TD>INPUT SORT </TD><TD><B>" & ClearNull(aplRST.Fields!sol_nrotabla) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
                Case "SORT"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO INP</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_prod) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
                Case "XCOM*"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO IN </TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_desa) & "</B></TD></TR>"
                Case "SORT*"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO INP</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_desa) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
            End Select
            cHTMLBody = cHTMLBody & "<TR><TD>MOTIVO</TD><TD><B>" & ClearNull(aplRST.Fields!jus_desc) & "</B></TD></TR>"
                    cHTMLBody = cHTMLBody & "<TR><TD>COMENTARIOS</TD><TD><B>" & ClearNull(aplRST.Fields!sol_texto) & "</B></TD></TR>"
            cHTMLBody = cHTMLBody & "<TR><TD>SOLICITANTE</TD><TD><B>" & ClearNull(aplRST.Fields!sol_recurso) & vbTab & ClearNull(aplRST.Fields!nom_recurso) & "</B></TD></TR>"
            cHTMLBody = cHTMLBody & "<TR><TD>SUPERVISOR</TD><TD><B>" & ClearNull(aplRST.Fields!sol_resp_sect) & vbTab & ClearNull(aplRST.Fields!nom_resp_sect) & "</B></TD></TR>"
            cHTMLBody = cHTMLBody & "<TR><TD ALIGN=CENTER COLSPAN=2>" & ClearNull(aplRST.Fields!fe_formato) & "</TD></TR>"
            cHTMLBody = cHTMLBody & "</TABLE>"
            cHTMLBody = cHTMLBody & "</CENTER>"
            cHTMLBody = cHTMLBody & "<P><BR>"
            cHTMLBody = cHTMLBody & "<P><BR>"
            cHTMLBody = cHTMLBody & "<P><BR>"
            'cHTMLBody = cHTMLBody & "<P><BR> * Este mail ha sido generado en forma autom�tica por el sistema CGM (Peticiones). No es necesario que usted envie respuesta al emisor."
            cHTMLBody = cHTMLBody & "</PRE>"
            cHTMLBody = cHTMLBody & "</BODY>"
            cHTMLBody = cHTMLBody & "</HTML>"
            .HTMLBody = cHTMLBody
            .Importance = olImportanceHigh
            .Display
            '.Actions
        End With
    End If
    
    Set objItem = Nothing
    Set objFolder = Nothing
    Logoff
End Sub

'{ add -002- a.
Public Sub EnviarCorreo_Restore(lSolicitud As Long)
    Dim objFolder As Outlook.MAPIFolder
    Dim objItem As Outlook.MailItem
    
    Dim strAddress As String
    Dim cTo As String
    Dim cCC As String
    Dim cHTMLBody As String
    Dim bEmergencia As Boolean
    Dim bResponsable As Boolean
    
    Set objFolder = adhGetOutlook.GetDefaultFolder(olFolderOutbox)
    Set objItem = objFolder.Items.Add(olMailItem)
    
    ' Primero determino si la solicitud es con enmascaramiento o expl�citamente no
    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
        If ClearNull(aplRST.Fields!sol_mask = "S") Then
            bEmergencia = False
        Else
            bEmergencia = True
        End If
    End If
    
    'If Not bEmergencia Then
        If Not InPerfil("CGRU") And Not InPerfil("CSEC") Then
            ' Es un analista, debe enviarse el email al l�der
            bResponsable = False
            If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo) Then
                cTo = ClearNull(aplRST.Fields!email)
                cCC = ""
            Else
                ' Si no encuentra el email del l�der, envia directamente al supervisor
                If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, Null) Then
                    cTo = ClearNull(aplRST.Fields!email)
                    cCC = ""
                Else
                    ' Si no encontr� al supervisor, entonces deja en blanco para que el usuario complete
                    cTo = ""
                    cCC = ""
                End If
            End If
        Else
            ' O es un l�der o un supervisor, debe ir directamente a Carga de M�quina
            bResponsable = True
            cTo = "CM@correo1.arg.igrupobbva"
        End If
    'Else
    If bEmergencia Then
        If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, "", "") Then
            ' El responsable a cargo de la gerencia del usuario solicitante
            cCC = ClearNull(aplRST.Fields!email)
        Else
            ' Se pone de prepo al gerente Ruben Efron
            cCC = "refron@correo1.arg.igrupobbva"
        End If
    End If
    
    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
        With objItem
            .BodyFormat = olFormatHTML
            If bEmergencia Then
                .Subject = "EME - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - RESTORE y transmisi�n PROD/DESA sin enmascaramiento. Solicitud N� " & ClearNull(aplRST.Fields!sol_nroasignado)    ' upd -003- a.
                cTo = "CM@correo1.arg.igrupobbva" & "; " & "rev_seginf@correo1.arg.igrupobbva"
                cCC = cCC & "; " & ClearNull(aplRST.Fields!mail_recurso)
            Else
                .Subject = "EXC - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - RESTORE -  Solicitud N� " & ClearNull(aplRST.Fields!sol_nroasignado)    ' upd -003- a.
            End If
            .To = cTo
            .CC = cCC
            ' Cuerpo del mensaje de correo electr�nico
            cHTMLBody = cHTMLBody & "<!DOCTYPE HTML PUBLIC " & Chr(36) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<HTML>"
            cHTMLBody = cHTMLBody & "<HEAD>"
            cHTMLBody = cHTMLBody & "<TITLE> Correo para Carga de M�quina </TITLE>"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Generator" & Chr(36) & " CONTENT=" & Chr(36) & "EditPlus" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Author" & Chr(36) & " CONTENT=" & Chr(36) & "CGM - Control de Gestion de Medios" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Keywords" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Description" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
            cHTMLBody = cHTMLBody & "</HEAD>"
            cHTMLBody = cHTMLBody & "<BODY>"
            'cHTMLBody = cHTMLBody & "<PRE>"
            If Not bResponsable Then
                cHTMLBody = cHTMLBody & "Por favor, autorizar y reenviar a Carga de M�quina la siguiente tarea: "
                cHTMLBody = cHTMLBody & "<BR>"
                cHTMLBody = cHTMLBody & "<BR>"
            End If
            '{ add -002- a.
            If ClearNull(aplRST.Fields!sol_tipo) = "XCOM*" Then
                cHTMLBody = cHTMLBody & "<BR>Obtener BACK UP de archivo <B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B> de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:"
                cHTMLBody = cHTMLBody & "<BR>"
                cHTMLBody = cHTMLBody & "<BR> - " & ClearNull(aplRST.Fields!sol_file_prod)
            Else
            '}
                cHTMLBody = cHTMLBody & "<BR>Obtener BACK UP de archivo <B>" & ClearNull(aplRST.Fields!sol_file_prod) & "</B> de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:"
                cHTMLBody = cHTMLBody & "<BR>"
                '{ add -005- a.
                If ClearNull(aplRST.Fields!sol_mask) = "S" Then
                    cHTMLBody = cHTMLBody & "<BR> - " & ClearNull(aplRST.Fields!sol_file_desa)
                Else
                    cHTMLBody = cHTMLBody & "<BR> - " & ClearNull(aplRST.Fields!sol_file_out)
                End If
                '}
                'cHTMLBody = cHTMLBody & "<BR> - " & ClearNull(aplRST.Fields!sol_file_out)  ' del -005- a.
            End If      ' add -002- a.
            cHTMLBody = cHTMLBody & "<BR>"
            cHTMLBody = cHTMLBody & "<BR>"
            cHTMLBody = cHTMLBody & "<BR>Motivo : " & ClearNull(aplRST.Fields!jus_desc)
            cHTMLBody = cHTMLBody & "<BR>Justificaci�n : " & ClearNull(aplRST.Fields!sol_texto)
            cHTMLBody = cHTMLBody & "<BR>"
            cHTMLBody = cHTMLBody & "<BR>"
            cHTMLBody = cHTMLBody & "<BR>Muchas gracias."
            cHTMLBody = cHTMLBody & "</PRE>"
            cHTMLBody = cHTMLBody & "</BODY>"
            cHTMLBody = cHTMLBody & "</HTML>"
            .HTMLBody = cHTMLBody
            .Importance = olImportanceHigh
            .Display
            '.Actions
        End With
    End If
    
    Set objItem = Nothing
    Set objFolder = Nothing
    Logoff
End Sub
'}

Public Function ProcesarCorreo_Rechazo(sUsuarioProceso, sFechaProceso, sEstado, sMotivo) As Boolean
    Dim cPara As String, cCC As String, cAsunto As String, cMensaje As String
    Dim sDestinatarios() As String
    Dim i As Integer
    
    If sp_GetHEDT101b("A", sUsuarioProceso, sFechaProceso, sEstado) Then
        Do While Not aplRST.EOF
            ReDim Preserve sDestinatarios(i)
            sDestinatarios(i) = ClearNull(aplRST.Fields!dsn_userid)
            i = i + 1
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    If i > 0 Then
        For i = 0 To UBound(sDestinatarios)
            'Call EnviarCorreo_Rechazo(sDestinatarios(i), sFechaProceso, sEstado)
            Call FuncionesCorreo2.EnviarCorreo_Rechazo(sDestinatarios(i), sFechaProceso, sEstado, cPara, cCC, cAsunto, cMensaje)
            Call Puntero(False)
            With auxMensajeCorreo
                .cPara = cPara
                .cCC = cCC
                .cAsunto = cAsunto
                .cMensaje = cMensaje
                .Show 1
            End With
        Next i
    End If
End Function

Private Sub EnviarCorreo_Rechazo(sRecurso, sFechaProceso, sEstado)
    Dim objFolder As Outlook.MAPIFolder, objItem As Outlook.MailItem
    Dim cHTMLBody As String
    Dim cAttachFile As String, cLink As String, sTo As String, sMotivoRechazo As String
    
    Set objFolder = adhGetOutlook.GetDefaultFolder(olFolderOutbox)
    Set objItem = objFolder.Items.Add(olMailItem)
    cAttachFile = App.Path & "/Enmascaramiento de Datos Sensibles - Transmisi�n de Archivos a Desarrollo - Ambiente Host.msg"
    cLink = "\\bbvdfs\dfs\Fpven1_e\Usr\Metodologia\Proteccion de datos sensibles\Circuitos Protecci�n de Informaci�n Sensible.pps"
    
    With objItem
        .BodyFormat = olFormatHTML
        .Subject = "Rechazo de archivos declarados en cat�logo"
        If Len(Dir(cAttachFile)) > 0 Then      ' Solo si existe el archivo, lo adjunta al mail del usuario
            .Attachments.Add (cAttachFile)
        End If
        cHTMLBody = cHTMLBody & "<!DOCTYPE HTML PUBLIC " & Chr(34) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(34) & ">"
        cHTMLBody = cHTMLBody & "<HTML>"
        cHTMLBody = cHTMLBody & "<HEAD>"
        cHTMLBody = cHTMLBody & "<TITLE> Enmascaramiento de Datos Sensibles </TITLE>"
        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Generator" & Chr(34) & " CONTENT=" & Chr(34) & "EditPlus" & Chr(34) & ">"
        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Author" & Chr(34) & " CONTENT=" & Chr(34) & "Fernando J. Spitz (Metodolog�a y F�bricas" & Chr(34) & ">"
        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Keywords" & Chr(34) & " CONTENT=" & Chr(34) & "" & Chr(34) & ">"
        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Description" & Chr(34) & " CONTENT=" & Chr(34) & "" & Chr(34) & ">"
        cHTMLBody = cHTMLBody & "</HEAD>"
        cHTMLBody = cHTMLBody & "<BODY>"
        cHTMLBody = cHTMLBody & "<FONT COLOR=black FACE=" & Chr(34) & "Arial" & Chr(34) & " SIZE=2>"
        cHTMLBody = cHTMLBody & "<P>Se ha rechazado la declaraci�n de archivos DSN enunciados a continuaci�n:</P><P>"
        If sp_GetHEDT101b("B", sRecurso, sFechaProceso, sEstado) Then
            sTo = IIf(ClearNull(aplRST.Fields!email) = "", ClearNull(aplRST.Fields!dsn_userid), ClearNull(aplRST.Fields!email))
            sMotivoRechazo = ClearNull(aplRST.Fields!dsn_txt)
            Do While Not aplRST.EOF
                cHTMLBody = cHTMLBody & ClearNull(aplRST.Fields!dsn_id) & " - " & ClearNull(aplRST.Fields!dsn_nom) & " (" & "Productivo: " & ClearNull(aplRST.Fields!dsn_nomprod) & ")<BR>"
                aplRST.MoveNext
                DoEvents
            Loop
        End If
        cHTMLBody = cHTMLBody & "</P>" & _
                    "<P>Motivos: " & sMotivoRechazo & "</P>" & _
                    "<P>Saluda atte.<BR>" & _
                    "<B>Seguridad Inform�tica</B><BR>" & _
                    "<FONT COLOR=black FACE=" & Chr(34) & "Arial" & Chr(34) & " SIZE=1>" & _
                    "Para ampliar detalles puede consultar la presentaci�n haciendo <A HREF=" & Chr(34) & GetShortName(cLink) & Chr(34) & ">click aqu�</A></P></FONT>"
        cHTMLBody = cHTMLBody & "</FONT></BODY>"
        cHTMLBody = cHTMLBody & "</HTML>"
        .HTMLBody = cHTMLBody
        .Importance = olImportanceHigh
        .To = sTo
        .Display
    End With
    Set objItem = Nothing
    Set objFolder = Nothing
    Logoff
End Sub

Private Function adhGetOutlook() As Outlook.NameSpace
    Dim objOutlook As New Outlook.Application
    Dim objNameSpace As Outlook.NameSpace
    Dim strProfile As String
    Dim strPassword As String
    
    strProfile = ""
    strPassword = ""
    
    Set objNameSpace = objOutlook.GetNamespace("MAPI")
    Call objNameSpace.Logon("", "", True, False)
    Set adhGetOutlook = objNameSpace
End Function

Public Function Validar_Email(ByVal email As String) As Boolean     ' Funci�n que comprueba si una direcci�n de email es v�lida
    On Local Error GoTo Err_Sub
    Dim i As Integer, iLen As Integer, caracter As String
    Dim Pos As Integer, bp As Boolean, iPos As Integer, iPos2 As Integer
    email = Trim$(email)
    If email = vbNullString Then
        Exit Function
    End If
    email = LCase$(email)
    iLen = Len(email)
    For i = 1 To iLen
        caracter = Mid(email, i, 1)
        If (Not (caracter Like "[a-z]")) And (Not (caracter Like "[0-9]")) Then
            If InStr(1, "_-" & "." & "@", caracter) > 0 Then
                If bp = True Then
                   Exit Function
                Else
                    bp = True
                    If i = 1 Or i = iLen Then
                        Exit Function
                    End If
                    If caracter = "@" Then
                        If iPos = 0 Then
                            iPos = i
                        Else
                            Exit Function
                        End If
                    End If
                    If caracter = "." Then
                        iPos2 = i
                    End If
                End If
            Else
                Exit Function
            End If
        Else
            bp = False
        End If
    Next i
    If iPos = 0 Or iPos2 = 0 Then
        Exit Function
    End If
    If iPos2 < iPos Then
        Exit Function
    End If
    Validar_Email = True
Exit Function
Err_Sub:
    On Local Error Resume Next
    Validar_Email = False
End Function
'}

Private Function URLencode(Text As String) As String
    Dim A As Long, Temp As String
    For A = 1 To Len(Text)
    Temp = Temp & "%" & Right$("0" & Hex$(Asc(Mid$(Text, A, 1))), 2)
    Next A
    URLencode = Temp
End Function

Private Sub Logoff()
    If Not adhGetOutlook Is Nothing Then
        adhGetOutlook.Logoff
        'Set adhGetOutlook = Nothing
    End If
End Sub

Private Function ParserHTML() As String
    On Error Resume Next
    Dim nFile As Integer
    Dim cString As String
    Dim cAuxiliar As String
    Dim i As Long
    Dim Pos As Long
    
    Dim vVariables() As String
    
    Dim reading_VARIABLES As Boolean
    Dim reading_FORMAT As Boolean
    
    
    
    'vParametros() As Variant
    
    nFile = FreeFile
    On Error GoTo errOpen
    Open App.Path & "\formatoHTML.html" For Input As #nFile
    On Error GoTo 0
    
    ' Primero cargo en memoria todo
    'Do While Not EOF(nFile)
    '    Line Input #nFile, cString
    '    cAuxiliar = cAuxiliar & cString
    '    'cAuxiliar = Mid(cString, InStr(1, cString, VARIABLES_I, vbTextCompare) + VARIABLES_I, InStr(InStr(1, cString, VARIABLES_I, vbTextCompare) + Len(VARIABLES_I), cString, VARIABLES_F, vbTextCompare))
    '    'for i = 0 to
    '    'cAuxiliar = cAuxiliar & ""
    '    'DoEvents
    'Loop
    
    Do While Not EOF(nFile)
        Line Input #nFile, cString
        ' Declaraci�n de variables
        'If reading_VARIABLES Then
            
        'End If
        
        If reading_VARIABLES Or InStr(1, cString, VARIABLES_I, vbTextCompare) > 0 Then
            If Not reading_VARIABLES Then
                reading_VARIABLES = True
                Line Input #nFile, cString  ' Fuerza la siguiente l�nea
            End If
            Do While Len(Trim(cString)) > 0
                ReDim Preserve vVariables(i)
                vVariables(i) = Mid(cString, 1, InStr(1, cString, ";", vbTextCompare) - 1)
                cString = Trim(Mid(cString, InStr(1, cString, ";", vbTextCompare) + 1))
                i = i + 1
                'DoEvents
            Loop
            'InStr(1, cString, VARIABLES_I, vbTextCompare) + Len(VARIABLES_I)
            
        ElseIf InStr(1, cString, VARIABLES_F, vbTextCompare) > 0 Then
            reading_VARIABLES = False
        End If
        
        'cAuxiliar = Mid(cString, InStr(1, cString, VARIABLES_I, vbTextCompare) + VARIABLES_I, InStr(InStr(1, cString, VARIABLES_I, vbTextCompare) + Len(VARIABLES_I), cString, VARIABLES_F, vbTextCompare))
        'for i = 0 to
        'cAuxiliar = cAuxiliar & ""
        'DoEvents
    Loop
    'Print #nFile, Format(date, "yyyymmdd") & ":" & Format(Time, "hhmss") & ":" & sTexto

    Close #nFile
    Exit Function
errOpen:
    MsgBox ("" & Err.DESCRIPTION)
    Exit Function
End Function

' *******************************************************************************************************************
' BACKUP de proceso actual en PROD
' *******************************************************************************************************************
'Public Sub EnviarCorreo_CargaDeMaquinas(lSolicitud As Long)
'    Dim objFolder As Outlook.MAPIFolder
'    Dim objItem As Outlook.MailItem
'
'    Dim strAddress As String
'    Dim cTo As String
'    Dim cCC As String     ' add -001- a.
'    Dim cHTMLBody As String
'
'    Set objFolder = adhGetOutlook.GetDefaultFolder(olFolderOutbox)
'    Set objItem = objFolder.Items.Add(olMailItem)
'
'    '{ add -001- a.
'    If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, "", "") Then
'        ' El responsable a cargo de la gerencia del usuario solicitante
'        cCC = ClearNull(aplRST.Fields!email)
'    Else
'        '{ del -004- a.
'        '' Se pone de prepo al gerente Ruben Efron
'        'cCC = "refron@correo1.arg.igrupobbva"
'        '}
'        '{ add -004- a.
'        ' Se pone de prepo al gerente Gustavo Siciliano
'        cCC = "gsiciliano@correo1.arg.igrupobbva"
'        '}
'    End If
'    '}
'
'    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then ' add -006- a.
'        With objItem
'            .BodyFormat = olFormatHTML
'            cTo = "CM@correo1.arg.igrupobbva" & "; " & "rev_seginf@correo1.arg.igrupobbva"
'            .To = cTo
'            .CC = cCC & "; " & ClearNull(aplRST.Fields!mail_recurso)        ' upd -001- a.
'            .Subject = "EME - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - Transmisi�n PROD/DESA sin enmascaramiento. Aprobado por Dise�o y Desarrollo"
'            ' Cuerpo del mensaje de correo electr�nico
'            cHTMLBody = cHTMLBody & "<!DOCTYPE HTML PUBLIC " & Chr(36) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(36) & ">"
'            cHTMLBody = cHTMLBody & "<HTML>"
'            cHTMLBody = cHTMLBody & "<HEAD>"
'            cHTMLBody = cHTMLBody & "<TITLE> Correo para Carga de M�quina </TITLE>"
'            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Generator" & Chr(36) & " CONTENT=" & Chr(36) & "EditPlus" & Chr(36) & ">"
'            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Author" & Chr(36) & " CONTENT=" & Chr(36) & "Fernando J. Spitz (Metodolog�a y F�bricas" & Chr(36) & ">"
'            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Keywords" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
'            cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Description" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
'            cHTMLBody = cHTMLBody & "</HEAD>"
'            cHTMLBody = cHTMLBody & "<BODY>"
'            cHTMLBody = cHTMLBody & "<PRE>"
'            cHTMLBody = cHTMLBody & "<BR>La solicitud de transmisi�n n�mero <B>" & lSolicitud & "</B> ha sido aprobada para ejecutar en ambiente <B>Producci�n.</B>"
'            cHTMLBody = cHTMLBody & "<BR>"
'            cHTMLBody = cHTMLBody & "<BR>Datos de la transmisi�n solicitada:"
'            cHTMLBody = cHTMLBody & "<P><BR>"
'            cHTMLBody = cHTMLBody & "<P><BR>"
'            cHTMLBody = cHTMLBody & "<P><BR>"
'            cHTMLBody = cHTMLBody & "<CENTER>"
'            cHTMLBody = cHTMLBody & "<TABLE BORDER=3 WIDTH=50%>"
'            cHTMLBody = cHTMLBody & "<CAPTION><B>Solicitud N� " & lSolicitud & "</B></CAPTION>"
'            cHTMLBody = cHTMLBody & "<TR><TD>TIPO</TD><TD><B>" & ClearNull(aplRST.Fields!sol_tipo) & "</B></TD></TR>"
'            Select Case ClearNull(aplRST.Fields!sol_tipo)
'                Case "XCOM"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO IN </TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_prod) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_desa) & "</B></TD></TR>"
'                    'cHTMLBody = cHTMLBody & "<TR><TD>LIB SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_lib_Sysin) & "</B></TD></TR>"
'                    'cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
'                    'cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
'                Case "UNLO"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>LIB SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_lib_Sysin) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
'                Case "TCOR"
'                    cHTMLBody = cHTMLBody & "<TR><TD>INPUT SORT </TD><TD><B>" & ClearNull(aplRST.Fields!sol_nrotabla) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
'                Case "SORT"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO INP</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_prod) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
'                Case "XCOM*"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO IN </TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_desa) & "</B></TD></TR>"
'                    'cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
'                    'cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
'                Case "SORT*"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO INP</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_desa) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>ARCHIVO OUT</TD><TD><B>" & ClearNull(aplRST.Fields!sol_file_out) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>SYSIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_mem_Sysin) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>JOIN</TD><TD><B>" & ClearNull(aplRST.Fields!sol_join) & "</B></TD></TR>"
'            End Select
'            cHTMLBody = cHTMLBody & "<TR><TD>MOTIVO</TD><TD><B>" & ClearNull(aplRST.Fields!jus_desc) & "</B></TD></TR>"
'                    cHTMLBody = cHTMLBody & "<TR><TD>COMENTARIOS</TD><TD><B>" & ClearNull(aplRST.Fields!sol_texto) & "</B></TD></TR>"
'            cHTMLBody = cHTMLBody & "<TR><TD>SOLICITANTE</TD><TD><B>" & ClearNull(aplRST.Fields!sol_recurso) & vbTab & ClearNull(aplRST.Fields!nom_recurso) & "</B></TD></TR>"
'            cHTMLBody = cHTMLBody & "<TR><TD>SUPERVISOR</TD><TD><B>" & ClearNull(aplRST.Fields!sol_resp_sect) & vbTab & ClearNull(aplRST.Fields!nom_resp_sect) & "</B></TD></TR>"
'            cHTMLBody = cHTMLBody & "<TR><TD ALIGN=CENTER COLSPAN=2>" & ClearNull(aplRST.Fields!fe_formato) & "</TD></TR>"
'            cHTMLBody = cHTMLBody & "</TABLE>"
'            cHTMLBody = cHTMLBody & "</CENTER>"
'            cHTMLBody = cHTMLBody & "<P><BR>"
'            cHTMLBody = cHTMLBody & "<P><BR>"
'            cHTMLBody = cHTMLBody & "<P><BR>"
'            'cHTMLBody = cHTMLBody & "<P><BR> * Este mail ha sido generado en forma autom�tica por el sistema CGM (Peticiones). No es necesario que usted envie respuesta al emisor."
'            cHTMLBody = cHTMLBody & "</PRE>"
'            cHTMLBody = cHTMLBody & "</BODY>"
'            cHTMLBody = cHTMLBody & "</HTML>"
'            .HTMLBody = cHTMLBody
'            .Importance = olImportanceHigh
'            .Display
'            '.Actions
'        End With
'    End If
'
'    Set objItem = Nothing
'    Set objFolder = Nothing
'    Logoff
'End Sub

' VIEJO METODO POR SHELL ***********************************************************************************************************************
'Public Function EnviarCorreoRechazoMasivo(vListaDeArchivos(), MotivoRechazo As String, NombreProductivoFmt) As Boolean
'    Dim i, x, y As Integer
'    Dim lRes As Long
'    Dim cLog As String
'    Dim cDestinatario, cAuxiliar As String
'    Dim cAsunto As String
'    Dim cCuerpoDelMensaje As String
'    Dim cEmail As String
'    Dim cEstructura As String
'
'    ' Por cada archivo, obtengo el propietario, el archivo de salida y el nombre productivo
'    Erase vUsuarios
'    For i = 0 To UBound(vListaDeArchivos)
'        If sp_GetHEDT001(vListaDeArchivos(i), Null, Null) Then
'            ReDim Preserve vUsuarios(x)
'            vUsuarios(x).Usuario = ClearNull(aplRST.Fields!cod_recurso)
'            vUsuarios(x).Propietario = ClearNull(aplRST.Fields!nom_usuario)
'            vUsuarios(x).ArchivoProduccion = ClearNull(aplRST.Fields!dsn_nomprod)
'            vUsuarios(x).ArchivoCatalogo = ClearNull(aplRST.Fields!dsn_nom)
'            vUsuarios(x).codigo = ClearNull(aplRST.Fields!dsn_id)       ' add -007- a.
'        End If
'        x = x + 1
'    Next i
'
'    ' Ahora obtengo los datos que me faltan, de la tabla de Recursos
'    For i = 0 To UBound(vUsuarios)
'        If ClearNull(vUsuarios(0).Usuario) <> "" Then
'            If sp_GetRecurso(vUsuarios(i).Usuario, "R", Null) Then
'                If InStr(1, cAuxiliar, vUsuarios(i).Usuario, vbTextCompare) = 0 Then
'                    If Validar_Email(aplRST.Fields!email) Then
'                        vUsuarios(i).email = ClearNull(aplRST.Fields!email)
'                        vUsuarios(i).TieneMail = True
'                    Else
'                        vUsuarios(i).email = ""
'                        vUsuarios(i).TieneMail = False
'                    End If
'                    cAuxiliar = cAuxiliar & "|" & vUsuarios(i).Usuario
'                End If
'            End If
'        End If
'    Next i
'
'    ' Armo el campo Para con todos los usuarios destinatarios (que tengan correo)
'    cDestinatario = "mailto:"
'    For i = 0 To UBound(vUsuarios)
'        If vUsuarios(i).TieneMail Then
'            If InStr(1, cDestinatario, ClearNull(vUsuarios(i).email), vbTextCompare) = 0 Then
'                cDestinatario = cDestinatario & IIf(ClearNull(vUsuarios(i).email) = "", "", ClearNull(vUsuarios(i).email) & ";")
'            End If
'        End If
'    Next i
'    cDestinatario = cDestinatario & "?"
'    ' Armo el asunto
'    'cAsunto = "subject=Rechazo de archivos declarados en cat�logo (Nombre prod.: " & vUsuarios(0).ArchivoProduccion & ")"
'    cAsunto = "subject=Rechazo de archivos declarados en cat�logo (Nombre prod.: " & NombreProductivoFmt & ")"
'    ' Armo el mensaje
'    cEstructura = "<!DOCTYPE html PUBLIC " & Chr(34) & "-//W3C//DTD XHTML 1.0 Transitional//EN" & Chr(34) & saltoLinea & _
'                  Chr(34) & "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" & Chr(34) & ">" & saltoLinea & _
'                  "<html xmlns=" & Chr(34) & "http://www.w3.org/1999/xhtml" & Chr(34) & " lang=" & Chr(34) & "en" & Chr(34) & ">" & saltoLinea & _
'                  "<head>" & saltoLinea & _
'                  "<title>Title here</title>" & saltoLinea & _
'                  "<meta http-equiv=" & Chr(34) & "content-type" & Chr(34) & " content=" & Chr(34) & "text/html; charset=ISO-8859-1" & Chr(34) & " />"
'    cLog = cLog & "Se ha rechazado la declaraci�n de archivos DSN enunciados a continuaci�n:" & saltoLinea & saltoLinea
'    For i = 0 To UBound(vUsuarios)
'        cLog = cLog & vUsuarios(i).codigo & " - " & vUsuarios(i).ArchivoCatalogo & " (" & vUsuarios(i).Usuario & ": " & vUsuarios(i).Propietario & ")" & saltoLinea ' upd -007- a.
'    Next i
'    cLog = cLog & saltoLinea & _
'                  "Motivos: " & Trim(MotivoRechazo) & saltoLinea & saltoLinea & _
'                  "Saluda atte." & saltoLinea & saltoLinea & _
'                  "Seguridad Inform�tica"   ' upd -007- a.
'    ' Armo el cuerpo del email
'    cCuerpoDelMensaje = Chr(38) & "body=" & cLog
'    cEmail = cDestinatario & cAsunto & cCuerpoDelMensaje
'    lRes = ShellExecute(mdiPrincipal.HWnd, "open", cEmail, vbNullString, "c:\", SW_SHOWNORMAL)
'End Function

'Public Function EnviarCorreoRechazo(codigo As String, Usuario As String, MotivoRechazo As String, ArchivoProductivo As String, NombreArchivoSalida As String) As Boolean
'    Dim lRes As Long
'    Dim cLog As String
'    Dim cDestinatario As String
'    Dim cAsunto As String
'    Dim cCuerpoDelMensaje As String
'    Dim cEmail As String
'    Dim cEstructura As String
'    Dim MailInex As String
'    Dim cAttach As String
'
'    ' Parametrizaci�n del mensaje de correo electr�nico
'    If Usuario <> "" Then
'        If sp_GetRecurso(Usuario, "R", Null) Then
'            cDestinatario = "mailto:" + ClearNull(aplRST.Fields!email) & "?"
'        End If
'    Else
'        MsgBox "El archivo no tiene propietario."
'        Exit Function
'    End If
'    cAsunto = "subject=Rechazo de archivo declarados en cat�logo (Nombre prod.: " & ArchivoProductivo & ")"
'    ' Armo el mensaje
'    cEstructura = "<!DOCTYPE html PUBLIC " & Chr(34) & "-//W3C//DTD XHTML 1.0 Transitional//EN" & Chr(34) & saltoLinea & _
'                  Chr(34) & "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" & Chr(34) & ">" & saltoLinea & _
'                  "<html xmlns=" & Chr(34) & "http://www.w3.org/1999/xhtml" & Chr(34) & " lang=" & Chr(34) & "en" & Chr(34) & ">" & saltoLinea & _
'                  "<head>" & saltoLinea & _
'                  "<title>Rechazo de archivo</title>" & saltoLinea & _
'                  "<meta http-equiv=" & Chr(34) & "content-type" & Chr(34) & " content=" & Chr(34) & "text/html; charset=ISO-8859-1" & Chr(34) & " />"
'    cLog = cLog & "Se ha rechazado la declaraci�n de archivos DSN: " & codigo & " - " & NombreArchivoSalida & saltoLinea & saltoLinea & _
'                  "Motivos: " & Trim(MotivoRechazo) & saltoLinea & saltoLinea & _
'                  "Saluda atte." & saltoLinea & saltoLinea & _
'                  "Seguridad Inform�tica"
'    ' Armo el cuerpo del email
'    cCuerpoDelMensaje = Chr(38) & "body=" & cLog
'    cEmail = cDestinatario & cAsunto & cCuerpoDelMensaje
'    '{ del -006- b.
'    'MailInex = Mid(aplRST.Fields!email, 1, 4)
'    'If MailInex = "#N/A" Then
'    '    MsgBox "El propietario no tiene asignado en el sistema una direccion de correo.", vbExclamation + vbOKOnly, "Sin direcci�n de email"
'    'End If
'    '}
'    lRes = ShellExecute(mdiPrincipal.HWnd, "open", cEmail, vbNullString, "c:\", SW_SHOWNORMAL)
'End Function

'Public Function EnviarCorreoRechazoMasivo(vListaDeArchivos(), MotivoRechazo As String, NombreProductivoFmt) As Boolean
'    Dim objFolder As Outlook.MAPIFolder, objItem As Outlook.MailItem
'    Dim cTo As String, cCC As String, cHTMLBody As String
'    Dim i, x As Integer
'    Dim cAuxiliar As String
'    Dim cAttachFile As String
'
'    cAttachFile = App.Path & "/Enmascaramiento de Datos Sensibles - Transmisi�n de Archivos a Desarrollo - Ambiente Host.msg"
'
'    Set objFolder = adhGetOutlook.GetDefaultFolder(olFolderOutbox)
'    Set objItem = objFolder.Items.Add(olMailItem)
'
'    Erase vUsuarios
'    For i = 0 To UBound(vListaDeArchivos)   ' Por cada archivo, obtengo el propietario, el archivo de salida y el nombre productivo
'        If sp_GetHEDT001(vListaDeArchivos(i), Null, Null) Then
'            ReDim Preserve vUsuarios(x)
'            vUsuarios(x).Usuario = ClearNull(aplRST.Fields!cod_recurso)
'            vUsuarios(x).Propietario = ClearNull(aplRST.Fields!nom_usuario)
'            vUsuarios(x).ArchivoProduccion = ClearNull(aplRST.Fields!dsn_nomprod)
'            vUsuarios(x).ArchivoCatalogo = ClearNull(aplRST.Fields!dsn_nom)
'            vUsuarios(x).codigo = ClearNull(aplRST.Fields!dsn_id)
'        End If
'        x = x + 1
'    Next i
'    For i = 0 To UBound(vUsuarios)          ' Ahora obtengo los datos que me faltan, de la tabla de Recursos
'        If ClearNull(vUsuarios(0).Usuario) <> "" Then
'            If sp_GetRecurso(vUsuarios(i).Usuario, "R", Null) Then
'                If InStr(1, cAuxiliar, vUsuarios(i).Usuario, vbTextCompare) = 0 Then
'                    If Validar_Email(aplRST.Fields!email) Then
'                        vUsuarios(i).email = ClearNull(aplRST.Fields!email)
'                        vUsuarios(i).TieneMail = True
'                    Else
'                        vUsuarios(i).email = ""
'                        vUsuarios(i).TieneMail = False
'                    End If
'                    cAuxiliar = cAuxiliar & "|" & vUsuarios(i).Usuario
'                End If
'            End If
'        End If
'    Next i
'
'    cTo = ""
'    For i = 0 To UBound(vUsuarios)          ' Armo el campo Para con todos los usuarios destinatarios (que tengan correo)
'        If vUsuarios(i).TieneMail Then
'            If InStr(1, cTo, ClearNull(vUsuarios(i).email), vbTextCompare) = 0 Then
'                cTo = cTo & IIf(ClearNull(vUsuarios(i).email) = "", "", ClearNull(vUsuarios(i).email) & ";")
'            End If
'        End If
'    Next i
'
'    With objItem                            ' Armo el mensaje
'        .BodyFormat = olFormatHTML
'        .To = cTo
'        .Subject = "Rechazo de archivos declarados en cat�logo (Nombre prod.: " & NombreProductivoFmt & ")"
'        If Len(Dir(cAttachFile)) > 0 Then      ' Solo si existe el archivo, lo adjunta al mail del usuario
'            .Attachments.Add (cAttachFile)
'        End If
'        cHTMLBody = cHTMLBody & "<!DOCTYPE HTML PUBLIC " & Chr(36) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(36) & ">"
'        cHTMLBody = cHTMLBody & "<HTML>"
'        cHTMLBody = cHTMLBody & "<HEAD>"
'        cHTMLBody = cHTMLBody & "<TITLE> Enmascaramiento de Datos Sensibles </TITLE>"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Generator" & Chr(36) & " CONTENT=" & Chr(36) & "EditPlus" & Chr(36) & ">"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Author" & Chr(36) & " CONTENT=" & Chr(36) & "Fernando J. Spitz (Metodolog�a y F�bricas" & Chr(36) & ">"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Keywords" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Description" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
'        cHTMLBody = cHTMLBody & "</HEAD>"
'        cHTMLBody = cHTMLBody & "<BODY>"
'        cHTMLBody = cHTMLBody & "<FONT COLOR=black FACE=" & Chr(36) & "Arial" & Chr(36) & " SIZE=2>"
'        cHTMLBody = cHTMLBody & "<P>Se ha rechazado la declaraci�n de archivos DSN enunciados a continuaci�n:</P><P>"
'        For i = 0 To UBound(vUsuarios)
'            cHTMLBody = cHTMLBody & vUsuarios(i).codigo & " - " & vUsuarios(i).ArchivoCatalogo & " (" & vUsuarios(i).Usuario & ": " & vUsuarios(i).Propietario & ")<BR>"
'        Next i
'        cHTMLBody = cHTMLBody & "</P>" & _
'                    "<P>Motivos: " & Trim(MotivoRechazo) & "</P>" & _
'                    "<P>Saluda atte.<BR>" & _
'                    "Seguridad Inform�tica<BR>" & _
'                    "<FONT COLOR=black FACE=" & Chr(36) & "Arial" & Chr(36) & " SIZE=1>" & _
'                    "Para ampliar detalles pueden consultar la presentaci�n haciendo <A HREF=" & Chr(36) & "\\bbvdfs\dfs\Fpven1_e\Usr\Metodologia\Proteccion de datos sensibles\Circuitos Protecci�n de Informaci�n Sensible.ppt" & Chr(36) & ">click aqu�</A></P></FONT>"
'        cHTMLBody = cHTMLBody & "</FONT></BODY>"
'        cHTMLBody = cHTMLBody & "</HTML>"
'        .HTMLBody = cHTMLBody
'        .Importance = olImportanceHigh
'        .Display
'    End With
'    Set objItem = Nothing
'    Set objFolder = Nothing
'    Logoff
'End Function

'Private Sub EnviarCorreoRechazo(Usuario, cTo, MotivoRechazo)
'    Dim objFolder As Outlook.MAPIFolder, objItem As Outlook.MailItem
'    Dim cHTMLBody As String
'    Dim cAttachFile As String, cLink As String
'    Dim i As Integer
'
'    Set objFolder = adhGetOutlook.GetDefaultFolder(olFolderOutbox)
'    Set objItem = objFolder.Items.Add(olMailItem)
'    cAttachFile = App.Path & "/Enmascaramiento de Datos Sensibles - Transmisi�n de Archivos a Desarrollo - Ambiente Host.msg"
'    cLink = "\\bbvdfs\dfs\Fpven1_e\Usr\Metodologia\Proteccion de datos sensibles\Circuitos Protecci�n de Informaci�n Sensible.pps"
'
'    With objItem
'        .BodyFormat = olFormatHTML
'        .To = cTo
'        .Subject = "Rechazo de archivos declarados en cat�logo (Nombre prod.: "
'        For i = 0 To UBound(vUsuarios)
'            If Usuario = vUsuarios(i).Usuario Then
'                .Subject = .Subject & IIf(Len(.Subject) > 58, ", " & vUsuarios(i).ArchivoProduccion, vUsuarios(i).ArchivoProduccion)
'            End If
'        Next i
'        .Subject = .Subject & ")"
'        If Len(Dir(cAttachFile)) > 0 Then      ' Solo si existe el archivo, lo adjunta al mail del usuario
'            .Attachments.Add (cAttachFile)
'        End If
'        cHTMLBody = cHTMLBody & "<!DOCTYPE HTML PUBLIC " & Chr(34) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(34) & ">"
'        cHTMLBody = cHTMLBody & "<HTML>"
'        cHTMLBody = cHTMLBody & "<HEAD>"
'        cHTMLBody = cHTMLBody & "<TITLE> Enmascaramiento de Datos Sensibles </TITLE>"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Generator" & Chr(34) & " CONTENT=" & Chr(34) & "EditPlus" & Chr(34) & ">"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Author" & Chr(34) & " CONTENT=" & Chr(34) & "Fernando J. Spitz (Metodolog�a y F�bricas" & Chr(34) & ">"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Keywords" & Chr(34) & " CONTENT=" & Chr(34) & "" & Chr(34) & ">"
'        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(34) & "Description" & Chr(34) & " CONTENT=" & Chr(34) & "" & Chr(34) & ">"
'        cHTMLBody = cHTMLBody & "</HEAD>"
'        cHTMLBody = cHTMLBody & "<BODY>"
'        cHTMLBody = cHTMLBody & "<FONT COLOR=black FACE=" & Chr(34) & "Arial" & Chr(34) & " SIZE=2>"
'        cHTMLBody = cHTMLBody & "<P>Se ha rechazado la declaraci�n de archivos DSN enunciados a continuaci�n:</P><P>"
'        For i = 0 To UBound(vUsuarios)
'            If Usuario = vUsuarios(i).Usuario Then
'                cHTMLBody = cHTMLBody & vUsuarios(i).codigo & " - " & vUsuarios(i).ArchivoCatalogo & " (" & "Productivo: " & vUsuarios(i).ArchivoProduccion & ")<BR>"
'            End If
'        Next i
'        cHTMLBody = cHTMLBody & "</P>" & _
'                    "<P>Motivos: " & Trim(MotivoRechazo) & "</P>" & _
'                    "<P>Saluda atte.<BR>" & _
'                    "<B>Seguridad Inform�tica</B><BR>" & _
'                    "<FONT COLOR=black FACE=" & Chr(34) & "Arial" & Chr(34) & " SIZE=1>" & _
'                    "Para ampliar detalles puede consultar la presentaci�n haciendo <A HREF=" & Chr(34) & GetShortName(cLink) & Chr(34) & ">click aqu�</A></P></FONT>"
'        cHTMLBody = cHTMLBody & "</FONT></BODY>"
'        cHTMLBody = cHTMLBody & "</HTML>"
'        .HTMLBody = cHTMLBody
'        .Importance = olImportanceHigh
'        .Display
'    End With
'    Set objItem = Nothing
'    Set objFolder = Nothing
'    Logoff
'End Sub

''Public Function EnviarCorreoRechazoMasivo(vListaDeArchivos(), MotivoRechazo As String, NombreProductivoFmt) As Boolean
'Public Function EnviarCorreoRechazoMasivo(vListaDeArchivos(), MotivoRechazo As String, sFechaProceso) As Boolean
'    Dim cTo As String, cCC As String, cHTMLBody As String
'    Dim i  As Integer, j  As Integer    ', x As Integer
'    Dim cAuxiliar As String
'    Dim vDestinatarios() As String
'
'    Erase vUsuarios
'    For i = 0 To UBound(vListaDeArchivos)       ' Por cada archivo, obtengo el propietario, el archivo de salida y el nombre productivo
'        If sp_GetHEDT001(vListaDeArchivos(i), Null, Null) Then
'            ReDim Preserve vUsuarios(j)
'            vUsuarios(j).Usuario = ClearNull(aplRST.Fields!cod_recurso)
'            vUsuarios(j).Propietario = ClearNull(aplRST.Fields!nom_usuario)
'            vUsuarios(j).ArchivoCatalogo = ClearNull(aplRST.Fields!dsn_nom)
'            vUsuarios(j).codigo = ClearNull(aplRST.Fields!DSN_Id)
'        End If
'        If sp_GetHEDT101a(vListaDeArchivos(i)) Then
'            vUsuarios(j).ArchivoProduccion = ClearNull(aplRST.Fields!dsn_nomprodfmt)
'        End If
'        j = j + 1
'    Next i
'    For i = 0 To UBound(vUsuarios)              ' Ahora obtengo los datos que me faltan, de la tabla de Recursos
'        If ClearNull(vUsuarios(0).Usuario) <> "" Then
'            If sp_GetRecurso(vUsuarios(i).Usuario, "R", Null) Then
'                If InStr(1, cAuxiliar, vUsuarios(i).Usuario, vbTextCompare) = 0 Then
'                    If Validar_Email(aplRST.Fields!email) Then
'                        vUsuarios(i).email = ClearNull(aplRST.Fields!email)
'                        vUsuarios(i).TieneMail = True
'                    Else
'                        vUsuarios(i).email = ""
'                        vUsuarios(i).TieneMail = False
'                    End If
'                    cAuxiliar = cAuxiliar & "|" & vUsuarios(i).Usuario
'                End If
'            End If
'        End If
'    Next i
'
'    cTo = ""
'    For i = 0 To UBound(vUsuarios)              ' Armo el campo Para con todos los usuarios destinatarios (que tengan correo)
'        If vUsuarios(i).TieneMail Then
'            If InStr(1, cTo, ClearNull(vUsuarios(i).email), vbTextCompare) = 0 Then
'                cTo = cTo & IIf(ClearNull(vUsuarios(i).email) = "", "", ClearNull(vUsuarios(i).email) & ";")
'                ReDim Preserve vDestinatarios(j)
'                vDestinatarios(j) = vUsuarios(i).Usuario
'                j = j + 1
'            End If
'        End If
'    Next i
'    'j = 0
'    For j = 0 To UBound(vDestinatarios)
'        Call EnviarCorreoRechazo(vDestinatarios(j), vUsuarios(j).email, MotivoRechazo)
'    Next j
'
''    With objItem                                ' Armo el mensaje
''        .BodyFormat = olFormatHTML
''        .To = cTo
''        .Subject = "Rechazo de archivos declarados en cat�logo (Nombre prod.: " & NombreProductivoFmt & ")"
''        If Len(Dir(cAttachFile)) > 0 Then      ' Solo si existe el archivo, lo adjunta al mail del usuario
''            .Attachments.Add (cAttachFile)
''        End If
''        cHTMLBody = cHTMLBody & "<!DOCTYPE HTML PUBLIC " & Chr(36) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(36) & ">"
''        cHTMLBody = cHTMLBody & "<HTML>"
''        cHTMLBody = cHTMLBody & "<HEAD>"
''        cHTMLBody = cHTMLBody & "<TITLE> Enmascaramiento de Datos Sensibles </TITLE>"
''        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Generator" & Chr(36) & " CONTENT=" & Chr(36) & "EditPlus" & Chr(36) & ">"
''        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Author" & Chr(36) & " CONTENT=" & Chr(36) & "Fernando J. Spitz (Metodolog�a y F�bricas" & Chr(36) & ">"
''        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Keywords" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
''        cHTMLBody = cHTMLBody & "<META NAME=" & Chr(36) & "Description" & Chr(36) & " CONTENT=" & Chr(36) & "" & Chr(36) & ">"
''        cHTMLBody = cHTMLBody & "</HEAD>"
''        cHTMLBody = cHTMLBody & "<BODY>"
''        cHTMLBody = cHTMLBody & "<FONT COLOR=black FACE=" & Chr(36) & "Arial" & Chr(36) & " SIZE=2>"
''        cHTMLBody = cHTMLBody & "<P>Se ha rechazado la declaraci�n de archivos DSN enunciados a continuaci�n:</P><P>"
''        For i = 0 To UBound(vUsuarios)
''            cHTMLBody = cHTMLBody & vUsuarios(i).codigo & " - " & vUsuarios(i).ArchivoCatalogo & " (" & vUsuarios(i).Usuario & ": " & vUsuarios(i).Propietario & ")<BR>"
''        Next i
''        cHTMLBody = cHTMLBody & "</P>" & _
''                    "<P>Motivos: " & Trim(MotivoRechazo) & "</P>" & _
''                    "<P>Saluda atte.<BR>" & _
''                    "Seguridad Inform�tica<BR>" & _
''                    "<FONT COLOR=black FACE=" & Chr(36) & "Arial" & Chr(36) & " SIZE=1>" & _
''                    "Para ampliar detalles pueden consultar la presentaci�n haciendo <A HREF=" & Chr(36) & "\\bbvdfs\dfs\Fpven1_e\Usr\Metodologia\Proteccion de datos sensibles\Circuitos Protecci�n de Informaci�n Sensible.ppt" & Chr(36) & ">click aqu�</A></P></FONT>"
''        cHTMLBody = cHTMLBody & "</FONT></BODY>"
''        cHTMLBody = cHTMLBody & "</HTML>"
''        .HTMLBody = cHTMLBody
''        .Importance = olImportanceHigh
''        .Display
''    End With
''    Set objItem = Nothing
''    Set objFolder = Nothing
''    Logoff
'End Function


