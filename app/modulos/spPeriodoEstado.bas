Attribute VB_Name = "spPeriodoEstado"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertPeriodoEstado(cod_estado_per, nom_estado_per) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertPeriodoEstado"
                .Parameters.Append .CreateParameter("@cod_estado_per", adChar, adParamInput, , cod_estado_per)
                .Parameters.Append .CreateParameter("@nom_estado_per", adChar, adParamInput, , nom_estado_per)
                Set aplRST = .Execute
        End With
        sp_InsertPeriodoEstado = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertPeriodoEstado = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdatePeriodoEstado(cod_estado_per, nom_estado_per) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdatePeriodoEstado"
                .Parameters.Append .CreateParameter("@cod_estado_per", adChar, adParamInput, , cod_estado_per)
                .Parameters.Append .CreateParameter("@nom_estado_per", adChar, adParamInput, , nom_estado_per)
                Set aplRST = .Execute
        End With
        sp_UpdatePeriodoEstado = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdatePeriodoEstado = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdatePeriodoEstadoField(x, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdatePeriodoEstadoField"
                .Parameters.Append .CreateParameter("@x", adInteger, adParamInput, , CLng(Val(x)))
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdatePeriodoEstadoField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdatePeriodoEstadoField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeletePeriodoEstado(cod_estado_per, nom_estado_per) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeletePeriodoEstado"
                .Parameters.Append .CreateParameter("@cod_estado_per", adChar, adParamInput, , cod_estado_per)
                .Parameters.Append .CreateParameter("@nom_estado_per", adChar, adParamInput, , nom_estado_per)
                Set aplRST = .Execute
        End With
        sp_DeletePeriodoEstado = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeletePeriodoEstado = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetPeriodoEstado(cod_estado_per) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeriodoEstado"
        .Parameters.Append .CreateParameter("@cod_estado_per", adChar, adParamInput, 8, cod_estado_per)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeriodoEstado = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeriodoEstado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

