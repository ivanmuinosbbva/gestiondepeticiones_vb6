Attribute VB_Name = "spGrupo"
' -001- a. FJS 22.02.2008 - Se modifican la cantidad de parametros del SP sp_UpdateGrupo (Homologaci�n)
' -001- b. FJS 27.02.2008 - Se agrega una nueva funci�n para llamar al SP sp_GetGrupoHomologable
' -002- a. FJS 14.10.2009 - Nuevo SP para traspasar peticiones entre grupos.
' -003- a. FJS 10.11.2009 - Nuevo SP para validar petici�n-grupo ejecutor (Solicitudes CMAQ).
' -004- a. FJS 01.03.2011 - Mejora: se modifica la llamada a la funci�n.

Option Explicit

Public vPeticiones() As Long    ' add -002- a.

Function sp_GetGrupo(cod_grupo, cod_sector, Optional flg_habil) As Boolean
    If IsMissing(flg_habil) Then
        flg_habil = Null
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetGrupo"
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetGrupo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetGrupoXt(cod_grupo, cod_sector, Optional flg_habil) As Boolean
    If IsMissing(flg_habil) Then
        flg_habil = Null
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetGrupoXt"
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetGrupoXt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetGrupoXt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateGrupo(modo, cod_grupo, nom_grupo, cod_sector, flg_habil, es_ejecutor, cod_bpar, cHomologacion, cAbreviatura, cPeticiones, inchoras, incfechas, incestados) As Boolean     ' upd -001- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateGrupo"
      .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, modo)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@nom_grupo", adChar, adParamInput, 80, nom_grupo)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      .Parameters.Append .CreateParameter("@es_ejecutor", adChar, adParamInput, 1, es_ejecutor)
      .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
      .Parameters.Append .CreateParameter("@homologacion", adChar, adParamInput, 1, cHomologacion)  ' add -001- a.
      .Parameters.Append .CreateParameter("@abreviatura", adChar, adParamInput, 30, cAbreviatura)
      .Parameters.Append .CreateParameter("@peticiones", adChar, adParamInput, 1, cPeticiones)
      .Parameters.Append .CreateParameter("@inchoras", adChar, adParamInput, 1, IIf(inchoras.value = 1, "S", "N"))
      .Parameters.Append .CreateParameter("@incfechas", adChar, adParamInput, 1, IIf(incfechas.value = 1, "S", "N"))
      .Parameters.Append .CreateParameter("@incestados", adChar, adParamInput, 1, IIf(incestados.value = 1, "S", "N"))
      Set aplRST = .Execute
    End With
    sp_UpdateGrupo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteGrupo(cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteGrupo"
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    sp_DeleteGrupo = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_ChangeGrupoSector(cod_grupo, new_sector) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_ChangeGrupoSector"
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@new_sector", adChar, adParamInput, 8, new_sector)
      Set aplRST = objCommand.Execute
    End With
    sp_ChangeGrupoSector = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ChangeGrupoSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -001- b.
Function sp_GetGrupoHomologable(cod_grupo) As Boolean
    ' Esta funci�n recibe como par�metro el c�digo de un grupo para saber si es homologable. Si el recordset devuelto
    ' viene vacio no es homologable. Si es homologable es devuelto en el recordset. Si la funci�n recibe NULL como
    ' par�metro entonces son devueltos en el recordset todos los grupos que son homologables.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetGrupoHomologable"
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        Set aplRST = objCommand.Execute
    End With
    If Not aplRST.EOF Then
        sp_GetGrupoHomologable = True
    End If
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetGrupoHomologable = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetGrupoHomologacion(Optional tipo As String) As Boolean
    On Error Resume Next
    If IsMissing(tipo) Then tipo = "H"          ' add
    sp_GetGrupoHomologacion = False
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetGrupoHomologacion"
        .Parameters.Append .CreateParameter("@tipo", adChar, adParamInput, 1, tipo)
        Set aplRST = objCommand.Execute
    End With
    'sp_GetGrupoHomologacion = True     ' del -004- a.
    '{ add -004- a.
    If Not aplRST.EOF Then
        sp_GetGrupoHomologacion = True
    End If
    '}
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetGrupoHomologacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPetGrupoHomologacion(pet_nrointerno, tipo) As Boolean
    On Error Resume Next
    sp_GetPetGrupoHomologacion = False
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPetGrupoHomologacion"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
      .Parameters.Append .CreateParameter("@tipo", adChar, adParamInput, 1, tipo)
      Set aplRST = objCommand.Execute
    End With
    '{ add -004- a.
    If Not aplRST.EOF Then
        sp_GetPetGrupoHomologacion = True
    End If
    '}
    'sp_GetPetGrupoHomologacion = True  ' del -004- a.
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPetGrupoHomologacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPetGrupoHomologable(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPetGrupoHomologable"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        Set aplRST = objCommand.Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPetGrupoHomologable = True
    End If
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPetGrupoHomologable = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -002- a.
Function sp_TraspasarPeticionAGrupo(pet_nrointerno, old_sector, old_grupo, new_sector, new_grupo, finaliza) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_TraspasarPeticionAGrupo"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
      .Parameters.Append .CreateParameter("@old_sector", adChar, adParamInput, 8, old_sector)
      .Parameters.Append .CreateParameter("@old_grupo", adChar, adParamInput, 8, old_grupo)
      .Parameters.Append .CreateParameter("@new_sector", adChar, adParamInput, 8, new_sector)
      .Parameters.Append .CreateParameter("@new_grupo", adChar, adParamInput, 8, new_grupo)
      .Parameters.Append .CreateParameter("@finaliza", adChar, adParamInput, 1, finaliza)
      Set aplRST = objCommand.Execute
    End With
    sp_TraspasarPeticionAGrupo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_TraspasarPeticionAGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_TraspasarPeticionesAGrupo(old_sector, old_grupo, new_sector, new_grupo, finaliza) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_TraspasarPeticionesAGrupo"
      .Parameters.Append .CreateParameter("@old_sector", adChar, adParamInput, 8, old_sector)
      .Parameters.Append .CreateParameter("@old_grupo", adChar, adParamInput, 8, old_grupo)
      .Parameters.Append .CreateParameter("@new_sector", adChar, adParamInput, 8, new_sector)
      .Parameters.Append .CreateParameter("@new_grupo", adChar, adParamInput, 8, new_grupo)
      .Parameters.Append .CreateParameter("@finaliza", adChar, adParamInput, 1, finaliza)
      Set aplRST = objCommand.Execute
    End With
    sp_TraspasarPeticionesAGrupo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_TraspasarPeticionesAGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -003- a.
Function sp_GetPeticionGrupoSol(pet_nroasignado, cod_grupo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionGrupoSol"
      .Parameters.Append .CreateParameter("@pet_nroasignado", adInteger, adParamInput, , pet_nroasignado)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionGrupoSol = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionGrupoSol = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
