Attribute VB_Name = "spOtros"
' -001- a. FJS 09.05.2008 - Se agregan nuevos SP para el manejo de Motivos de suspensi�n temporaria de un sector/grupo.
' -002- a. FJS 28.05.2008 - Se agrega una funci�n para determinar la cantidad de dias ocurridos entre dos fechas tomando en cuenta solo los d�as h�biles.
' -003- a. FJS 17.04.2009 - Replanificaciones.
' -004- a. FJS 22.02.2016 - BPE: unidades de medida para KPI.

Option Explicit

'{ add -001- a.
Function sp_GetMotivos(cod_motivo, flg_habil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetMotivo"
      .Parameters.Append .CreateParameter("@cod_motivo", adInteger, adParamInput, , IIf(IsNull(cod_motivo), Null, cod_motivo))
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetMotivos = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetMotivos = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -002- a.
Public Function DiasOcurridos(ByVal dFecha1 As Date, ByVal dFecha2 As Date) As Long
    ' Esta funci�n devuelve la cantidad de d�as ocurrida entre dos fechas
    ' teniendo en cuenta solo los d�as h�biles (descartando los fines de
    ' semana y d�as feriados)
   
    Call Status("Aguarde, por favor...")
    Screen.MousePointer = vbHourglass
    
    DiasOcurridos = 0
    dFecha1 = dFecha1 + 1
    
    Do While dFecha1 <= dFecha2
        If Not EsFeriado(dFecha1) Then
            If Not Weekday(dFecha1, vbSunday) = 7 Then
                If Not Weekday(dFecha1, vbSunday) = 1 Then
                    dFecha1 = dFecha1 + 1: DiasOcurridos = DiasOcurridos + 1
                Else
                    dFecha1 = dFecha1 + 1
                End If
            Else
                dFecha1 = dFecha1 + 2
            End If
        Else
            dFecha1 = dFecha1 + 1
        End If
        DoEvents
    Loop
    Call Status("Listo.")
    Screen.MousePointer = vbNormal
End Function

Private Function EsFeriado(dFecha As Date) As Boolean
    If sp_GetFeriado(dFecha, dFecha) Then
        EsFeriado = True
    Else
        EsFeriado = False
    End If
End Function
'}

'{ add -003- a.
Function sp_GetCausasPlanif(cod_causa) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetCausasPlanif"
      .Parameters.Append .CreateParameter("@cod_causa", adInteger, adParamInput, , IIf(IsNull(cod_causa), Null, cod_causa))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetCausasPlanif = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetCausasPlanif = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'Function sp_GetBalanceRubro(cod_BalanceRubro, Optional flg_habil) As Boolean
'    If IsMissing(flg_habil) Then
'        flg_habil = Null
'    End If
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_GetBalanceRubro"
'      .Parameters.Append .CreateParameter("@cod_BalanceRubro", adChar, adParamInput, 8, cod_BalanceRubro)
'      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
'      Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetBalanceRubro = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetBalanceRubro = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function

'{ add -004- a.
Function sp_GetUnidadMedida(unimedId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetUnidadMedida"
      .Parameters.Append .CreateParameter("@unimedId", adChar, adParamInput, 10, IIf(IsNull(unimedId), Null, Mid(unimedId, 1, 10)))
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetUnidadMedida = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetUnidadMedida = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

