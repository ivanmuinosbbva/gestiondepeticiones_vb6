Attribute VB_Name = "spPlanEstadosDesarrollo"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertEstadosDesarrollo(cod_estado_desa, nom_estado_desa, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertEstadosDesarrollo"
                .Parameters.Append .CreateParameter("@cod_estado_desa", adInteger, adParamInput, , cod_estado_desa)
                .Parameters.Append .CreateParameter("@nom_estado_desa", adChar, adParamInput, , nom_estado_desa)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_InsertEstadosDesarrollo = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertEstadosDesarrollo = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateEstadosDesarrollo(cod_estado_desa, nom_estado_desa, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateEstadosDesarrollo"
                .Parameters.Append .CreateParameter("@cod_estado_desa", adInteger, adParamInput, , cod_estado_desa)
                .Parameters.Append .CreateParameter("@nom_estado_desa", adChar, adParamInput, , nom_estado_desa)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_UpdateEstadosDesarrollo = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateEstadosDesarrollo = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateEstadosDesarrolloField(x, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateEstadosDesarrolloField"
                .Parameters.Append .CreateParameter("@x", adInteger, adParamInput, , CLng(Val(x)))
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateEstadosDesarrolloField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateEstadosDesarrolloField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteEstadosDesarrollo(cod_estado_desa) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteEstadosDesarrollo"
                .Parameters.Append .CreateParameter("@cod_estado_desa", adInteger, adParamInput, , cod_estado_desa)
                Set aplRST = .Execute
        End With
        sp_DeleteEstadosDesarrollo = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteEstadosDesarrollo = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetEstadosDesarrollo(cod_estado_desa, estado_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetEstadosDesarrollo"
        .Parameters.Append .CreateParameter("@cod_estado_desa", adInteger, adParamInput, , cod_estado_desa)
        .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, 1, estado_hab)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetEstadosDesarrollo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetEstadosDesarrollo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
