Attribute VB_Name = "spHEDT001"
' C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~

' -001- a. FJS 06.07.2010 - Se actualiza la longitud del segundo par�metro (de 1 a 20)
' -002- a. AA  25.11.2010 - Nueva llamada al stored sp_GetHEDTSolInt para la nueva funcionalidad referente al form "frmMostrarSolIntervinientes".
' -003- a. FJS 07.01.2011 - Arreglo: estaban faltando las rutinas para el mantenimiento del nuevo dato de Nombre Productivo.
' -004- a. FJS 09.02.2011 - Nuevo: nuevo par�metro para cargar, en primera instancia, los DSNs propios. Luego, si el usuario lo requiere, el resto del cat�logo (performance).
' -005- a. FJS 25.03.2015 - Nuevo: se agrega funcionalidad para filtrar la grilla por nombre de archivos y nombre productivo.

Option Explicit

Function sp_InsertHEDT001(dsn_id, dsn_nom, dsn_desc, cod_recurso, dsn_enmas, dsn_nomrut, app_id, nom_prod, dsn_cpy, dsn_cpybbl, dsn_cpynom) As Boolean  ' upd -003- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertHEDT001"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@dsn_nom", adChar, adParamInput, 50, dsn_nom)
        .Parameters.Append .CreateParameter("@dsn_desc", adChar, adParamInput, 255, dsn_desc)
        .Parameters.Append .CreateParameter("@dsn_enmas", adChar, adParamInput, 1, dsn_enmas)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@dsn_nomrut", adChar, adParamInput, 8, dsn_nomrut)
        .Parameters.Append .CreateParameter("@dsn_appid", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@dsn_nomprod", adChar, adParamInput, 50, nom_prod)     ' add -003- a.
        .Parameters.Append .CreateParameter("@dsn_cpy", adChar, adParamInput, 1, dsn_cpy)
        .Parameters.Append .CreateParameter("@dsn_cpybbl", adChar, adParamInput, 50, dsn_cpybbl)
        .Parameters.Append .CreateParameter("@dsn_cpynom", adChar, adParamInput, 50, dsn_cpynom)
        Set aplRST = .Execute
    End With
    sp_InsertHEDT001 = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertHEDT001 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHEDT001(dsn_id, dsn_nom, dsn_desc, cod_recurso, dsn_enmas, dsn_nomrut, app_id, nom_prod, dsn_cpy, dsn_cpybbl, dsn_cpynom) As Boolean    ' upd -003- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateHEDT001"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@dsn_nom", adChar, adParamInput, 50, dsn_nom)
        .Parameters.Append .CreateParameter("@dsn_desc", adChar, adParamInput, 255, dsn_desc)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@dsn_enmas", adChar, adParamInput, 1, dsn_enmas)
        .Parameters.Append .CreateParameter("@dsn_nomrut", adChar, adParamInput, 8, dsn_nomrut)
        .Parameters.Append .CreateParameter("@dsn_appid", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@dsn_nomprod", adChar, adParamInput, 50, nom_prod)     ' add -003- a.
        .Parameters.Append .CreateParameter("@dsn_cpy", adChar, adParamInput, 1, dsn_cpy)
        .Parameters.Append .CreateParameter("@dsn_cpybbl", adChar, adParamInput, 50, dsn_cpybbl)
        .Parameters.Append .CreateParameter("@dsn_cpynom", adChar, adParamInput, 50, dsn_cpynom)
        Set aplRST = .Execute
        sp_UpdateHEDT001 = True
        If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
        End If
        If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
            If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
                GoTo Err_SP
            End If
        End If
    End With
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHEDT001 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHEDT101(dsn_id, dsn_vb_id, dsn_txt, dsn_dtf, cod_recurso) As Boolean            ' add -002- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateHEDT101"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@dsn_vb_id", adChar, adParamInput, 1, dsn_vb_id)
        .Parameters.Append .CreateParameter("@dsn_txt", adChar, adParamInput, 255, dsn_txt)
        .Parameters.Append .CreateParameter("@dsn_dtf", adChar, adParamInput, 15, dsn_dtf)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        Set aplRST = .Execute
    End With
    sp_UpdateHEDT101 = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    Dim sdsn As String
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHEDT101 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteHEDT001(dsn_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteHEDT001"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        Set aplRST = .Execute
    End With
    sp_DeleteHEDT001 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteHEDT001 = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetHEDT001(dsn_id, dsn_nom, dsn_nomprod, owner, fecha_alta, estado) As Boolean      ' upd -005- a.
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT001"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@dsn_nom", adChar, adParamInput, 50, IIf(dsn_nom = "", Null, dsn_nom))                 ' upd -005- a.
        .Parameters.Append .CreateParameter("@dsn_nomprod", adChar, adParamInput, 50, IIf(dsn_nomprod = "", Null, dsn_nomprod))     ' add -005- a.
        .Parameters.Append .CreateParameter("@dsn_owner", adChar, adParamInput, 10, IIf(owner = "S", glLOGIN_ID_REEMPLAZO, Null))
        .Parameters.Append .CreateParameter("@fecha_alta", SQLDdateType, adParamInput, , fecha_alta)
        .Parameters.Append .CreateParameter("@estado", adChar, adParamInput, 1, IIf(estado = "NULL", Null, estado))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT001 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHEDT001 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -005- a.
Function sp_GetHEDT001a(dsn_nom) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT001a"
        .Parameters.Append .CreateParameter("@dsn_nom", adChar, adParamInput, 50, IIf(dsn_nom = "", Null, dsn_nom))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT001a = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHEDT001a = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'Function sp_GetHEDT101(dsn_id, dsn_vb) As Boolean
Function sp_GetHEDT101(dsn_id, dsn_nom, fch_desde, fch_hasta, Estados) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT101"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, IIf(IsNull(dsn_id) Or ClearNull(dsn_id) = "", Null, dsn_id))
        .Parameters.Append .CreateParameter("@dsn_nom", adChar, adParamInput, 50, IIf(IsNull(dsn_nom) Or ClearNull(dsn_nom) = "", Null, dsn_nom))
        .Parameters.Append .CreateParameter("@fch_desde", SQLDdateType, adParamInput, , IIf(IsNull(fch_desde) Or fch_desde = "", Null, fch_desde))
        .Parameters.Append .CreateParameter("@fch_hasta", SQLDdateType, adParamInput, , IIf(IsNull(fch_hasta) Or fch_hasta = "", Null, fch_hasta))
        .Parameters.Append .CreateParameter("@estados", adChar, adParamInput, 30, IIf(IsNull(Estados) Or ClearNull(Estados) = "", Null, ClearNull(Estados)))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT101 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHEDT101 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHEDT101a(dsn_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT101a"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT101a = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHEDT101a = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHEDT101b(modo, cod_recurso, fecha, estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT101b"
        .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, modo)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, IIf(cod_recurso = "", Null, cod_recurso))
        .Parameters.Append .CreateParameter("@fecha", adChar, adParamInput, 15, fecha)
        .Parameters.Append .CreateParameter("@estado", adChar, adParamInput, 1, estado)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT101b = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHEDT101b = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHEDT101c(dsn_nomprod) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT101c"
        .Parameters.Append .CreateParameter("@dsn_nomprod", adChar, adParamInput, 50, dsn_nomprod)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT101c = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHEDT101c = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHEDT101d(dsn_nomprod) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT101d"
        .Parameters.Append .CreateParameter("@dsn_nomprod", adChar, adParamInput, 50, dsn_nomprod)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT101d = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHEDT101d = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHEDT001Field(dsn_id, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateHEDT001Field"
      .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
      .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 255, campo)
      .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 255, valortxt)
      .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
      .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
      Set aplRST = .Execute
    End With
    sp_UpdateHEDT001Field = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHEDT001Field = False
End Function

'{ add -003- a.
Function sp_GetHEDTSolInt(dsn_nom)
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDTSolInt"
        .Parameters.Append .CreateParameter("@dsn_nom", adChar, adParamInput, 50, dsn_nom)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDTSolInt = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHEDTSolInt = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
