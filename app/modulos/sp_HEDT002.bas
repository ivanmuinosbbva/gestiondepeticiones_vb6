Attribute VB_Name = "spHEDT002"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertHEDT002(dsn_id, cpy_id, cpy_dsc, cpy_nomrut) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertHEDT002"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
        .Parameters.Append .CreateParameter("@cpy_dsc", adChar, adParamInput, 50, cpy_dsc)
        .Parameters.Append .CreateParameter("@cpy_nomrut", adChar, adParamInput, 8, cpy_nomrut)
        Set aplRST = .Execute
    End With
    sp_InsertHEDT002 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_InsertHEDT002 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHEDT002(dsn_id, cpy_id, cpy_dsc, cpy_nomrut) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateHEDT002"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
        .Parameters.Append .CreateParameter("@cpy_dsc", adChar, adParamInput, 50, cpy_dsc)
        .Parameters.Append .CreateParameter("@cpy_nomrut", adChar, adParamInput, 8, cpy_nomrut)
        Set aplRST = .Execute
    End With
    sp_UpdateHEDT002 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_UpdateHEDT002 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteHEDT002(dsn_id, cpy_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteHEDT002"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
        Set aplRST = .Execute
    End With
    sp_DeleteHEDT002 = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_DeleteHEDT002 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHEDT002(dsn_id, cpy_id, cpy_dsc) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHEDT002"
        .Parameters.Append .CreateParameter("@dsn_id", adChar, adParamInput, 8, dsn_id)
        .Parameters.Append .CreateParameter("@cpy_id", adChar, adParamInput, 8, cpy_id)
        .Parameters.Append .CreateParameter("@cpy_dsc", adChar, adParamInput, 50, cpy_dsc)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHEDT002 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHEDT002 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
