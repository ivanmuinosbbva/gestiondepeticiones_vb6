Attribute VB_Name = "FuncionesCorreo"
' -001- a. FJS 27.08.2014 - Nuevo: se pasa a html el formato de los mails generados por CGM.
' -002- a. FJS 04.06.2015 - Nuevo: envio autom�tico de mails de aviso de Informe de homologaci�n a involucrados en la petici�n con adjunto del mismo.
' -002- b. FJS 28.07.2015 - Modificaci�n: no se env�a el mail al homologador sino al pool de homologaci�n.
' -003- a. FJS 15.07.2015 - Nuevo: envio autom�tico de mail solicitando el desbloqueo de usuario CGM en Sybase.
' -004- a. FJS 24.08.2015 - Nuevo: envio autom�tico del informe de homologaci�n a los responsables con el detalle de las observaciones (solo OMA).
' -005- a. FJS 27.10.2015 - Nuevo: variable global para manejar los errores de envio de mail autom�ticos.

Option Explicit

Private Const POOL_CM As String = "explotacion-argentina@bbva.com"
Private Const POOL_SI As String = "controlseginf-arg@bbva.com"          ' Antes: "rev_seginf@bbvafrances.com.ar"
Private Const CRYPTO_PREFIJO As String = "9FF3FD979FF38D979398908E86909C809F8491F03533"
'Private Const FOOTER1 = "*** NO RESPONDER AL EMISOR DE ESTE MAIL. SI TIENE QUE RESPONDER A TODOS, DEBE ELIMINAR A CGM@bbva.com ***"
Private Const FOOTER1 = "*** NO RESPONDER AL EMISOR DE ESTE MAIL. SI TIENE QUE RESPONDER A TODOS, DEBE ELIMINAR cgm-peticiones-arg@bbva.com ***"
Private Const FOOTER2 = "*** NO RESPONDER AL EMISOR DE ESTE MAIL. SI TIENE QUE RESPONDER, DEBE DIRIGIRSE AL POOL controlseginf-arg@bbva.com ***"

Dim cTo As String
Dim cCC As String
Dim cBCC As String
Dim cSubject As String
Dim cBody As String

Dim bIsHTMLFormatMail As Boolean
Dim LINEBREAK As String

Private Enum Alineacion
    Izquierda = 1
    Derecha = 2
    Centro = 3
End Enum

Private Type udtColumna
    AnchoMinimo As Integer
    Alineacion As Alineacion
    Titulo As String
End Type

Public Type udtDestinatarios
    'Solicitante As String
    Legajo As String
    Nombre As String
    Habilitado As Boolean
    email As String
    peticiones As New Collection
End Type

Public Sub EnviarCorreo_EMERGENCIA(lSolicitud As Long, bAvisoEnvio As Boolean)
    Dim sCodigoVerificador As String
    Dim bRestore As Boolean
    
    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
    
    cTo = ""
    cCC = ""
    cBCC = ""
    cSubject = ""
    cBody = ""
    
    ' Determina el gerente (con copia a �l siempre)
    If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, "", "") Then    ' El responsable a cargo de la gerencia del usuario solicitante
        cCC = ClearNull(aplRST.Fields!email)
    Else
        cCC = "gsiciliano@bbva.com"                                                 ' Se pone por defecto al gerente Gustavo Siciliano
    End If
    
    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
        cTo = POOL_CM & ", " & POOL_SI
        cCC = cCC & ", " & ClearNull(aplRST.Fields!mail_recurso) & ", " & IIf(ClearNull(aplRST.Fields!sol_recurso) = ClearNull(aplRST.Fields!SOL_RESP_SECT), "", ClearNull(aplRST.Fields!mail_resp_ejec) & ", " & ClearNull(aplRST.Fields!mail_resp_sect))
        If ClearNull(aplRST.Fields!pet_nroasignado) <> "" Then
            cSubject = "EME - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - Transmisi�n PROD/DESA sin enmascaramiento. Aprobado por Dise�o y Desarrollo (Sol. N� " & lSolicitud & ")"
        Else
            cSubject = "EME - Transmisi�n PROD/DESA sin enmascaramiento. Aprobado por Dise�o y Desarrollo (Sol. N� " & lSolicitud & ")"
        End If
        cBody = IIf(bIsHTMLFormatMail, "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">", "")
        cBody = cBody & "La solicitud de transmisi�n n�mero " & lSolicitud & " ha sido aprobada para ejecutar en ambiente Producci�n." & LINEBREAK & LINEBREAK
        
        bRestore = IIf(Val(ClearNull(aplRST.Fields!sol_tipocod)) > 4, True, False)
        
        If bRestore Then
            cBody = cBody & "Realizar el siguiente RESTORE:" & LINEBREAK & LINEBREAK
        Else
            cBody = cBody & "Datos de la transmisi�n solicitada:" & LINEBREAK & LINEBREAK
        End If
        
        Select Case ClearNull(aplRST.Fields!sol_tipo)
            Case "XCOM"
                cBody = cBody & "TIPO       : " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
                cBody = cBody & "ARCHIVO IN : " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
            Case "UNLO"
                cBody = cBody & "TIPO       : " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                cBody = cBody & "LIB SYSIN  : " & ClearNull(aplRST.Fields!sol_LIB_SYSIN) & LINEBREAK
                cBody = cBody & "SYSIN      : " & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & LINEBREAK
                cBody = cBody & "JOIN       : " & ClearNull(aplRST.Fields!SOL_JOIN) & LINEBREAK
            Case "TCOR"
                cBody = cBody & "TIPO       : " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
                cBody = cBody & "INPUT SORT : " & ClearNull(aplRST.Fields!SOL_NROTABLA) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
            Case "SORT"
                cBody = cBody & "TIPO       : " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
                cBody = cBody & "ARCHIVO INP: " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                cBody = cBody & "SYSIN      : " & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & LINEBREAK
                cBody = cBody & "JOIN       : " & ClearNull(aplRST.Fields!SOL_JOIN) & LINEBREAK
            Case "XCOM*"
                cBody = cBody & "TIPO       : " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
                cBody = cBody & "FECHA BCKUP: " & ClearNull(aplRST.Fields!sol_fe_auto3) & LINEBREAK
                cBody = cBody & "ARCHIVO IN : " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
            Case "UNLO*"
                cBody = cBody & "TIPO       : " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
                cBody = cBody & "TABLA             : " & ClearNull(aplRST.Fields!sol_bckuptabla) & LINEBREAK
                cBody = cBody & "JOB DE BACKUP     : " & ClearNull(aplRST.Fields!sol_bckupjob) & LINEBREAK
                cBody = cBody & "FECHA DE BACKUP   : " & ClearNull(aplRST.Fields!sol_fe_auto3) & LINEBREAK
                cBody = cBody & "FILTRO A APLICAR  : " & ClearNull(aplRST.Fields!sol_LIB_SYSIN) & " (" & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & ")" & LINEBREAK
                cBody = cBody & "ARCHIVO RESULTANTE: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
            Case "SORT*"
                cBody = cBody & "TIPO       : " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
                cBody = cBody & "FECHA BCKUP: " & ClearNull(aplRST.Fields!sol_fe_auto3) & LINEBREAK
                cBody = cBody & "ARCHIVO INP: " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK    ' Antes DESA
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                cBody = cBody & "SYSIN      : " & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & LINEBREAK
                cBody = cBody & "JOIN       : " & ClearNull(aplRST.Fields!SOL_JOIN) & LINEBREAK
        End Select
        cBody = cBody & "MOTIVO     : " & ClearNull(aplRST.Fields!jus_desc) & LINEBREAK
        cBody = cBody & "COMENTARIOS: " & ClearNull(aplRST.Fields!sol_texto) & LINEBREAK
        cBody = cBody & "SOLICITANTE: " & ClearNull(aplRST.Fields!sol_recurso) & Space(10 - Len(ClearNull(aplRST.Fields!sol_recurso))) & ClearNull(aplRST.Fields!nom_recurso) & LINEBREAK
        cBody = cBody & "SUPERVISOR : " & ClearNull(aplRST.Fields!SOL_RESP_SECT) & Space(10 - Len(ClearNull(aplRST.Fields!SOL_RESP_SECT))) & ClearNull(aplRST.Fields!nom_resp_sect) & LINEBREAK
        cBody = cBody & ClearNull(aplRST.Fields!fe_formato) & LINEBREAK & LINEBREAK
        cBody = cBody & "Muchas gracias." & LINEBREAK & LINEBREAK & IIf(glSMTPMODO = 0, "", FOOTER1) & LINEBREAK & LINEBREAK
        cBody = cBody & "C�digo Verificador: "
        sCodigoVerificador = MD5_Solo_09AZaz_y_punto(CRYPTO_PREFIJO & cBody)
        cBody = cBody & sCodigoVerificador
        cBody = cBody & IIf(bIsHTMLFormatMail, "</font>", "")
        Call EnviarCorreo(cTo, cCC, "", cSubject, cBody, bAvisoEnvio, , bIsHTMLFormatMail)
    End If
End Sub

Public Sub EnviarCorreo_CargaDeMaquinas(lSolicitud As Long, bAvisoEnvio As Boolean)
    Dim sCodigoVerificador As String
    
    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
    
    cTo = ""
    cCC = ""
    cBCC = ""
    cSubject = ""
    cBody = ""
    
    If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, "", "") Then    ' El responsable a cargo de la gerencia del usuario solicitante
        cCC = ClearNull(aplRST.Fields!email)
    Else
        cCC = "gsiciliano@bbva.com"                                                 ' Se pone por defecto al gerente Gustavo Siciliano
    End If
    
    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
        cTo = POOL_CM & ", " & POOL_SI
        If ClearNull(aplRST.Fields!sol_recurso) <> ClearNull(aplRST.Fields!SOL_RESP_EJEC) Then
            cCC = cCC & ", " & ClearNull(aplRST.Fields!mail_recurso) & ", " & ClearNull(aplRST.Fields!mail_resp_ejec) & ", " & ClearNull(aplRST.Fields!mail_resp_sect)
        Else
            cCC = cCC & ", " & ClearNull(aplRST.Fields!mail_recurso) & ", " & ClearNull(aplRST.Fields!mail_resp_sect)
        End If
        cSubject = "EXC - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - Transmisi�n PROD/DESA sin enmascaramiento. Aprobado por Dise�o y Desarrollo (Sol. N� " & lSolicitud & ")"
        cBody = IIf(bIsHTMLFormatMail, "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">", "")
        cBody = cBody & "La solicitud de transmisi�n n�mero " & lSolicitud & " ha sido aprobada para ejecutar en ambiente Producci�n." & LINEBREAK & LINEBREAK
        cBody = cBody & "Datos de la transmisi�n solicitada:" & LINEBREAK & LINEBREAK
        cBody = cBody & "TIPO: " & ClearNull(aplRST.Fields!sol_tipo) & LINEBREAK
        Select Case ClearNull(aplRST.Fields!sol_tipo)
            Case "XCOM"
                cBody = cBody & "ARCHIVO IN: " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
            Case "UNLO"
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                cBody = cBody & "LIB SYSIN: " & ClearNull(aplRST.Fields!sol_LIB_SYSIN) & LINEBREAK
                cBody = cBody & "SYSIN: " & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & LINEBREAK
                cBody = cBody & "JOIN: " & ClearNull(aplRST.Fields!SOL_JOIN) & LINEBREAK
            Case "UNLO*"        ' Esto no har�a falta (va por el proceso de RESTORE)
                cBody = cBody & "Realizar el siguiente RESTORE:" & vbCrLf & vbCrLf
                cBody = cBody & "Tabla: " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
                cBody = cBody & "Job de backup: " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
                cBody = cBody & "Fecha de backup: " & ClearNull(aplRST.Fields!sol_fe_auto3) & vbCrLf
                cBody = cBody & "Filtro a aplicar: " & ClearNull(aplRST.Fields!sol_LIB_SYSIN) & " (" & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & ")" & vbCrLf
                cBody = cBody & "Archivo resultante: " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
            Case "TCOR"
                cBody = cBody & "INPUT SORT: " & ClearNull(aplRST.Fields!SOL_NROTABLA) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
            Case "SORT"
                cBody = cBody & "ARCHIVO INP: " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                cBody = cBody & "SYSIN: " & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & LINEBREAK
                cBody = cBody & "JOIN: " & ClearNull(aplRST.Fields!SOL_JOIN) & LINEBREAK
            Case "XCOM*"
                cBody = cBody & "ARCHIVO IN: " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
            Case "SORT*"
                cBody = cBody & "ARCHIVO INP: " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "ARCHIVO OUT: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                cBody = cBody & "SYSIN: " & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & LINEBREAK
                cBody = cBody & "JOIN: " & ClearNull(aplRST.Fields!SOL_JOIN) & LINEBREAK
        End Select
        cBody = cBody & "MOTIVO: " & ClearNull(aplRST.Fields!jus_desc) & LINEBREAK
        cBody = cBody & "COMENTARIOS: " & ClearNull(aplRST.Fields!sol_texto) & LINEBREAK
        cBody = cBody & "SOLICITANTE: " & ClearNull(aplRST.Fields!sol_recurso) & Space(10 - Len(ClearNull(aplRST.Fields!sol_recurso))) & ClearNull(aplRST.Fields!nom_recurso) & LINEBREAK
        cBody = cBody & "SUPERVISOR: " & ClearNull(aplRST.Fields!SOL_RESP_SECT) & Space(10 - Len(ClearNull(aplRST.Fields!SOL_RESP_SECT))) & ClearNull(aplRST.Fields!nom_resp_sect) & LINEBREAK
        cBody = cBody & ClearNull(aplRST.Fields!fe_formato) & LINEBREAK & LINEBREAK
        cBody = cBody & "Muchas gracias." & vbCrLf & vbCrLf & IIf(glSMTPMODO = 0, "", FOOTER1) & LINEBREAK & LINEBREAK
        cBody = cBody & "C�digo Verificador: "
        sCodigoVerificador = MD5_Solo_09AZaz_y_punto(CRYPTO_PREFIJO & cBody)
        cBody = cBody & sCodigoVerificador
        cBody = cBody & IIf(bIsHTMLFormatMail, "</font>", "")
        Call EnviarCorreo(cTo, cCC, "", cSubject, cBody, bAvisoEnvio, , bIsHTMLFormatMail)
    End If
End Sub

Public Sub EnviarCorreo_Restore(lSolicitud As Long, bAvisoEnvio As Boolean)
    Dim sCodigoVerificador As String
    Dim bResponsable As Boolean
    Dim bSinEnmascarar As Boolean
    Dim bDatosSensibles As Boolean      ' Auxiliar: DSN indica si tiene o no datos sensibles declarados
    Dim bDiferida As Boolean
    
    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
    
    ' Si la solicitud fuera por EMErgencia, va por otro camino
    cTo = ""
    cCC = ""
    cBCC = ""
    cSubject = ""
    cBody = ""
    
    bSinEnmascarar = False
    bDatosSensibles = False
    bDiferida = False
    
    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
        bSinEnmascarar = IIf(ClearNull(aplRST.Fields!sol_mask) = "S", False, True)
        bDatosSensibles = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "S", True, False)
        bDiferida = IIf(ClearNull(aplRST.Fields!SOL_DIFERIDA) = "S", True, False)
        cBody = IIf(bIsHTMLFormatMail, "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">", "")
        If bSinEnmascarar Then
            cSubject = "EXC - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - RESTORE y transmisi�n PROD/DESA sin enmascaramiento. Solicitud N� " & ClearNull(aplRST.Fields!sol_nroasignado)    ' upd -003- a.
            ' Si el solicitante no es el l�der
            If ClearNull(aplRST.Fields!SOL_ESTADO) = "E" Then
                cTo = POOL_CM & ", " & POOL_SI
                cCC = IIf(ClearNull(aplRST.Fields!sol_recurso) = ClearNull(aplRST.Fields!SOL_RESP_EJEC), ClearNull(aplRST.Fields!mail_recurso), ClearNull(aplRST.Fields!mail_recurso) & ", " & ClearNull(aplRST.Fields!mail_resp_ejec)) & ", " & ClearNull(aplRST.Fields!mail_resp_sect)
                cBCC = ""
            Else
                cTo = ClearNull(aplRST.Fields!mail_resp_ejec)
                cCC = ClearNull(aplRST.Fields!mail_recurso)
                cBCC = ""
                cBody = cBody & "Por favor, autorizar y reenviar a Carga de M�quina la siguiente tarea: " & LINEBREAK & LINEBREAK
            End If
        Else
            cSubject = "EXC - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - RESTORE -  Solicitud N� " & ClearNull(aplRST.Fields!sol_nroasignado)
            cTo = POOL_CM
            cCC = ClearNull(aplRST.Fields!mail_recurso)
            cBCC = ""
        End If

        Select Case ClearNull(aplRST.Fields!sol_tipo)
            Case "XCOM*"
                cBody = cBody & "Obtener BACK UP de archivo " & ClearNull(aplRST.Fields!sol_file_prod) & " de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:" & LINEBREAK & LINEBREAK
                If bDatosSensibles Then
                    cBody = cBody & " - " & IIf(bDiferida, ClearNull(aplRST.Fields!sol_file_inter), ClearNull(aplRST.Fields!sol_file_desa)) & LINEBREAK
                Else
                    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                End If
            Case "UNLO*"
                cBody = cBody & "Realizar el siguiente RESTORE:" & LINEBREAK & LINEBREAK
                cBody = cBody & "Tabla: " & ClearNull(aplRST.Fields!sol_bckuptabla) & LINEBREAK
                cBody = cBody & "Job de backup: " & ClearNull(aplRST.Fields!sol_bckupjob) & LINEBREAK
                cBody = cBody & "Fecha de backup: " & ClearNull(aplRST.Fields!sol_fe_auto3) & LINEBREAK
                cBody = cBody & "Filtro a aplicar: " & ClearNull(aplRST.Fields!sol_LIB_SYSIN) & " (" & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & ")" & LINEBREAK
                If bDiferida Then
                    cBody = cBody & "Archivo resultante: " & IIf(bDatosSensibles, ClearNull(aplRST.Fields!sol_file_inter), ClearNull(aplRST.Fields!sol_file_desa)) & LINEBREAK
                Else
                    cBody = cBody & "Archivo resultante: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                End If
            Case "SORT*"
                cBody = cBody & "Realizar el siguiente RESTORE:" & LINEBREAK & LINEBREAK
                cBody = cBody & "Archivo: " & ClearNull(aplRST.Fields!sol_file_prod) & LINEBREAK
                cBody = cBody & "Fecha de backup: " & ClearNull(aplRST.Fields!sol_fe_auto3) & LINEBREAK
                cBody = cBody & "SORT a aplicar: " & ClearNull(aplRST.Fields!sol_LIB_SYSIN) & " (" & ClearNull(aplRST.Fields!SOL_MEM_SYSIN) & ")" & LINEBREAK
                If bDiferida Then
                    cBody = cBody & "Archivo resultante: " & IIf(bDatosSensibles, ClearNull(aplRST.Fields!sol_file_inter), ClearNull(aplRST.Fields!sol_file_desa)) & LINEBREAK
                Else
                    cBody = cBody & "Archivo resultante: " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                End If
            Case Else
                cBody = cBody & "Obtener BACK UP de archivo " & ClearNull(aplRST.Fields!sol_file_prod) & " de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:" & LINEBREAK & LINEBREAK
                If ClearNull(aplRST.Fields!sol_mask) = "S" Then
                    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                Else
                    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_desa) & LINEBREAK
                End If
        End Select
        cBody = cBody & LINEBREAK
        cBody = cBody & "Motivo: " & ClearNull(aplRST.Fields!jus_desc) & LINEBREAK
        cBody = cBody & "Justificaci�n: " & ClearNull(aplRST.Fields!sol_texto) & LINEBREAK
        cBody = cBody & LINEBREAK & LINEBREAK
        cBody = cBody & "Muchas gracias." & LINEBREAK & LINEBREAK & IIf(glSMTPMODO = 0, "", FOOTER1) & LINEBREAK & LINEBREAK
        If bSinEnmascarar Then
            cBody = cBody & "C�digo Verificador: "
            sCodigoVerificador = MD5_Solo_09AZaz_y_punto(CRYPTO_PREFIJO & cBody)
            cBody = cBody & sCodigoVerificador
        End If
        cBody = cBody & IIf(bIsHTMLFormatMail, "</font>", "")
        Call EnviarCorreo(cTo, cCC, "", cSubject, cBody, bAvisoEnvio, , bIsHTMLFormatMail)
    End If
End Sub

Public Sub EnviarCorreo_Recordatorio(lSolicitud As Long, bAvisoEnvio As Boolean)
    Dim sMotivoRechazo As String
    Dim sSupervisor As String
    Dim sLider As String
    Dim bPendientes As Boolean
    
    Dim sEstadoSol As String
    Dim sMailRecurso As String
    Dim sMailResponsableGrupo As String
    Dim sMailResponsableSector As String
    
    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
    
    cTo = ""
    cCC = ""
    cBCC = ""
    cSubject = ""
    cBody = ""
    bPendientes = False
    
    ' Obtengo los datos de la solicitud
    If sp_GetSolicitudNro(lSolicitud) Then
        sSupervisor = ClearNull(aplRST.Fields!SOL_RESP_SECT)
        sLider = ClearNull(aplRST.Fields!SOL_RESP_EJEC)
        sEstadoSol = ClearNull(aplRST.Fields!SOL_ESTADO)
        sMailRecurso = ClearNull(aplRST.Fields!mail_recurso)
        sMailResponsableGrupo = ClearNull(aplRST.Fields!mail_resp_ejec)
        sMailResponsableSector = ClearNull(aplRST.Fields!mail_resp_sect)
    End If
    
    ' Determina si hay que detallar pendientes de aprobaci�n
    bPendientes = sp_GetSolicitudListado("2", lSolicitud, IIf(sEstadoSol = "A", sLider, sSupervisor), IIf(sEstadoSol = "A", "CGRU", "CSEC"))
    
    cSubject = "CGM - Transmisiones de PROD a DESA"
    cBody = cBody & "Tiene una solicitud de transmisi�n CON DATOS SENSIBLES pero SIN ENMASCARAR <strong>pendiente de aprobaci�n</strong>." & LINEBREAK
    cBody = cBody & "<br/>" & "Por favor, aprobar ingresando a CGM." & LINEBREAK & LINEBREAK
    Select Case sEstadoSol
        Case "A"        ' Envia a N4
            cTo = sMailResponsableGrupo
            cCC = sMailRecurso
            cBody = cBody & "<br/>" & ArmarTabla("1", lSolicitud, "", "")
            If bPendientes Then
                cBody = cBody & "<br/><br/>Adem�s, tiene solicitudes anteriores pendientes de aprobaci�n:" & LINEBREAK & LINEBREAK
                cBody = cBody & "<br/>" & ArmarTabla("2", lSolicitud, sLider, "CGRU")
            End If
        Case "B"        ' Envia a N3
            cTo = sMailResponsableSector
            cCC = ""
            cBody = cBody & "<br/>" & ArmarTabla("1", lSolicitud, "", "")
            If bPendientes Then
                cBody = cBody & "<br/><br/>Adem�s, tiene solicitudes anteriores pendientes de aprobaci�n:" & LINEBREAK & LINEBREAK
                cBody = cBody & "<br/>" & ArmarTabla("2", lSolicitud, sSupervisor, "CSEC")
            End If
    End Select
    Call EnviarCorreo(cTo, cCC, "", cSubject, cBody, bAvisoEnvio, , bIsHTMLFormatMail)
End Sub

Public Sub EnviarCorreo_Rechazo(sRecurso, sFechaProceso, sEstado, bAvisoEnvio As Boolean)
    Dim sMotivoRechazo As String
    
    cTo = ""
    cCC = ""
    cBCC = ""
    cSubject = ""
    cBody = ""
    
    cSubject = "CGM - Rechazo de archivos declarados en cat�logo"
    cBody = cBody & "Se ha rechazado la declaraci�n de archivos DSN enunciados a continuaci�n:" & vbCrLf & vbCrLf
    If sp_GetHEDT101b("B", sRecurso, sFechaProceso, sEstado) Then
        cTo = IIf(ClearNull(aplRST.Fields!email) = "", ClearNull(aplRST.Fields!dsn_userid), ClearNull(aplRST.Fields!email))
        cCC = IIf(glSMTPMODO = 0, "", ClearNull(aplRST.Fields!mail_si))
        sMotivoRechazo = ClearNull(aplRST.Fields!dsn_txt)
        Do While Not aplRST.EOF
            cBody = cBody & ClearNull(aplRST.Fields!dsn_id) & " - " & ClearNull(aplRST.Fields!dsn_nom) & " (" & "Productivo: " & ClearNull(aplRST.Fields!dsn_nomprod) & ")" & vbCrLf
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    cBody = cBody & vbCrLf
    cBody = cBody & "Motivos: " & sMotivoRechazo & vbCrLf & vbCrLf & _
                    "Saluda atte." & vbCrLf & _
                    "Seguridad Inform�tica" & vbCrLf & vbCrLf & IIf(glSMTPMODO = 0, "", FOOTER2)
    Call EnviarCorreo(cTo, cCC, "", cSubject, cBody, bAvisoEnvio)
End Sub

Private Sub EnviarCorreo(cTo As String, cCC As String, cBCC As String, cSubject As String, cBody As String, bAvisoEnvio As Boolean, Optional Path_Adjunto As String, Optional bHTMLFormat As Boolean)
    Dim HTMLFormat As Boolean
    
    If IsMissing(bHTMLFormat) Then
        HTMLFormat = False
    Else
        HTMLFormat = bHTMLFormat
    End If
    
    Select Case glSMTPMODO
        Case "0"            ' No envia mail (presenta formulario en pantalla)
            Call Puntero(False)
            With auxMensajeCorreo
                .cPara = cTo
                .cCC = cCC
                .cAsunto = cSubject
                .cMensaje = cBody
                .Show 1
            End With
        Case "", "1"        ' Envia mail directamente por CDO
            If Enviar_Mail_CDO(cTo, glSMTPSNDR, cCC, cBCC, cSubject, cBody, Path_Adjunto, bHTMLFormat) Then
                If bAvisoEnvio Then
                    MsgBox "Email(s) enviado(s).", vbInformation
                End If
            Else
                If MsgBox("�Reintentar o cancelar y realizar el envio del mail manual?", vbQuestion + vbRetryCancel, "No se pudo enviar el mail") = vbRetry Then
                    Call EnviarCorreo(cTo, cCC, cBCC, cSubject, cBody, bAvisoEnvio, Path_Adjunto, bHTMLFormat)
                Else
                    Call Puntero(False)
                    With auxMensajeCorreo
                        .cPara = cTo
                        .cCC = cCC
                        .cAsunto = cSubject
                        .cMensaje = cBody
                        .Show 1
                    End With
                End If
            End If
    End Select
End Sub

Private Function Enviar_Mail_CDO(cTo, cFrom, cCC, cBCC, cSubject, cBody, Optional Path_Adjunto As String, Optional bHTMLFormat As Boolean) As Boolean
    Dim Usuario As String
    Dim Password As String
    Dim HTMLFormat As Boolean
    Dim Obj_Email As CDO.message
    
    glEMAIL_STATUS = -1                         ' add -005- a.
    Set Obj_Email = New CDO.message
    
    If IsMissing(bHTMLFormat) Then
        HTMLFormat = False
    Else
        HTMLFormat = bHTMLFormat
    End If
        
    Const CDO_CONFIG = "http://schemas.microsoft.com/cdo/configuration/"
    Const cdoSendUsingPickup = 1                ' Send message using the local SMTP service pickup directory.
    Const cdoSendUsingPort = 2                  ' Send the message using the network (SMTP over the network).
    Const cdoAnonymous = 0                      ' Do not authenticate
    Const cdoBasic = 1                          ' basic (clear-text) authentication
    Const cdoNTLM = 2                           ' NTLM
    
    Usuario = ""
    Password = ""
    
    glSMTPNAME = IIf(glSMTPNAME = "", "gatewaymailgw.arg.igrupobbva", glSMTPNAME)
    glSMTPPORT = IIf(glSMTPPORT = 0, 25, glSMTPPORT)
    
    With Obj_Email
        .Configuration.Fields.item(CDO_CONFIG & "smtpserver") = glSMTPNAME                             ' Indica el servidor Smtp para poder enviar el Mail ( puede ser el nombre del servidor o su direcci�n IP )
        .Configuration.Fields.item(CDO_CONFIG & "sendusing") = cdoSendUsingPort
        .Configuration.Fields.item(CDO_CONFIG & "smtpserverport") = CLng(glSMTPPORT)                   ' Puerto. Por defecto se usa el puerto 25, en el caso de Gmail se usan los puertos 465 o el puerto 587 ( este �ltimo me dio error )
        .Configuration.Fields.item(CDO_CONFIG & "smtpauthenticate") = Abs(glSMTPAUTH)
        .Configuration.Fields.item(CDO_CONFIG & "smtpconnectiontimeout") = 10                          ' Tiempo m�ximo de espera en segundos para la conexi�n
        If glSMTPAUTH Then                                                                                                     ' Configura las opciones para el login en el SMTP
            .Configuration.Fields.item(CDO_CONFIG & "sendusername") = Usuario
            .Configuration.Fields.item(CDO_CONFIG & "sendpassword") = Password
            .Configuration.Fields.item(CDO_CONFIG & "smtpusessl") = glSMTPSSL
        End If
        
        .From = IIf(cFrom = "", "cgm-peticiones-arg@bbva.com", cFrom)           ' Direcci�n del remitente
        .Subject = cSubject                                                     ' Asunto del mensaje
        
        ' Esto es solo para ambiente DESARROLLO
        If glENTORNO <> "PROD" Then
            'If cstmGetUserName() = "A125958" Then
            '    .To = "fernandojavier.spitz@bbva.com"
            'End If
            .To = "fernandojavier.spitz@bbva.com"
            .CC = ""
            .BCC = ""
            ' Antes del cuerpo del mail, agrego un detalle de los destinatarios a los que se enviar�a este correo
            cBody = IIf(HTMLFormat, "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">", "") & _
                    "------------------ DESARROLLO ---------------------" & IIf(HTMLFormat, "<br/>", vbCrLf) & _
                    "Para: " & cTo & IIf(HTMLFormat, "<br/>", vbCrLf) & _
                    "CC  : " & cCC & IIf(HTMLFormat, "<br/>", vbCrLf) & _
                    "BCC : " & cBCC & IIf(HTMLFormat, "<br/>", vbCrLf) & _
                    "---------------------------------------------------" & IIf(HTMLFormat, "<br/><br/>", vbCrLf & vbCrLf) & _
                    IIf(HTMLFormat, "</font>", "") & cBody
        Else    ' PRODUCCI�N
            .To = cTo                               ' Direcci�n del Destinatario
            .CC = cCC
            .BCC = cBCC
        End If
        If HTMLFormat Then
            .HTMLBody = cBody                   ' Cuerpo del mensaje (HTML format)
        Else
            .TextBody = cBody                   ' Cuerpo del mensaje
        End If
        If Path_Adjunto <> vbNullString Then    ' Ruta del archivo adjunto
            .AddAttachment (Path_Adjunto)
        End If
        .Configuration.Fields.Update            ' Actualiza los datos antes de enviar
        On Error Resume Next
        .Send                                   ' Env�a el email
        Select Case Err.Number
            Case 0
                Enviar_Mail_CDO = True
                glEMAIL_STATUS = EMAIL_STATUS_SUCCESSFUL        ' add -005- a.
            Case -2147220973, -2147220975
                glEMAIL_STATUS = EMAIL_STATUS_ERROR             ' add -005- a.
                MsgBox "Es posible que esta funcionalidad se encuentre bloqueada." & vbCrLf & vbCrLf & _
                    "Comun�quese con al GAC para su habilitaci�n informando" & vbCrLf & _
                    "el nombre de su equipo, que es " & glLOGIN_Equipo & "." & vbCrLf & vbCrLf & _
                    "Desc.: " & Err.DESCRIPTION & "C�d.: " & Err.Number, vbExclamation + vbOKOnly, " Error al enviar el email"
            Case Else
                glEMAIL_STATUS = EMAIL_STATUS_ERROR             ' add -005- a.
                MsgBox Err.DESCRIPTION & " (" & Err.Number & ")", vbCritical, " Error al enviar el email"
        End Select
    End With
    If Not Obj_Email Is Nothing Then            ' Descarga la referencia
        Set Obj_Email = Nothing
    End If
    On Error GoTo 0
End Function

Private Function ReemplazarHTML_Espacios(sCadena As String) As String
    If sCadena <> "" Then
        ReemplazarHTML_Espacios = Replace(sCadena, " ", "&nbsp;", 1)
    End If
End Function

Private Function ArmarTabla(sModo As String, lSolicitud As Long, sRecurso As String, sPerfilActual) As String
    Dim i As Integer, j As Integer, anchoTotal As Integer
    Dim Columnas As Integer
    Dim sOutput As String
    Dim cols() As udtColumna
    
    Columnas = 6
    ReDim Preserve cols(Columnas)
    
    cols(0).AnchoMinimo = 9         ' Sol. Nro.
    cols(0).Alineacion = Izquierda
    cols(0).Titulo = "Sol. Nro."
    
    cols(1).AnchoMinimo = 18         ' Solicitante
    cols(1).Alineacion = Derecha
    cols(1).Titulo = "Solicitante"
    
    cols(2).AnchoMinimo = 5         ' Tipo
    cols(2).Alineacion = Derecha
    cols(2).Titulo = "Tipo"
    
    cols(3).AnchoMinimo = 3         ' EME
    cols(3).Alineacion = Derecha
    cols(3).Titulo = "EME"
    
    'cols(4).AnchoMinimo = 3         ' ENM
    'cols(4).Alineacion = Derecha
    'cols(4).Titulo = "ENM"
    
    cols(4).AnchoMinimo = 19         ' Fecha
    cols(4).Alineacion = Derecha
    cols(4).Titulo = "Fecha sol."
    
    'cols(6).AnchoMinimo = 60         ' Motivo
    'cols(6).Alineacion = Derecha
    'cols(6).Titulo = "Motivo"
    
    cols(5).AnchoMinimo = 30         ' Archivo
    cols(5).Alineacion = Derecha
    cols(5).Titulo = "Archivo"

    ArmarTabla = "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">"

    ' Cabecera
    For i = 1 To 3
        If i = 1 Or i = 3 Then
            sOutput = ""
            For j = 0 To Columnas - 1
                sOutput = sOutput & IIf(j = 0, "+", "") & String(cols(j).AnchoMinimo, "-") & "+"
            Next j
            'Debug.Print sOutput
            ArmarTabla = ArmarTabla & "<br/>" & sOutput
        End If
        If i = 2 Then
            sOutput = ""
            For j = 0 To Columnas - 1
                'sOutput = sOutput & IIf(j = 0, "|", "") & IIf(Len(cols(j).Titulo) <= cols(j).AnchoMinimo, cols(j).Titulo & ReemplazarHTML_Espacios(Space(cols(j).AnchoMinimo - Len(cols(j).Titulo))) & "|", Mid(cols(j).Titulo, 1, Len(cols(j).Titulo) - 3) & "...|")
                sOutput = sOutput & IIf(j = 0, "|", "") & "<strong>" & IIf(Len(cols(j).Titulo) <= cols(j).AnchoMinimo, cols(j).Titulo & ReemplazarHTML_Espacios(Space(cols(j).AnchoMinimo - Len(cols(j).Titulo))) & "</strong>" & "|", Mid(cols(j).Titulo, 1, Len(cols(j).Titulo) - 3) & "..." & "</strong>" & "|")
            Next j
            'Debug.Print sOutput
            ArmarTabla = ArmarTabla & "<br/>" & sOutput
        End If
    Next i
    
    ' Detalle
    If sp_GetSolicitudListado(sModo, lSolicitud, sRecurso, sPerfilActual) Then
        Do While Not aplRST.EOF
            For i = 1 To 2
                If i = 1 Then
                    sOutput = ""
                    For j = 0 To Columnas - 1
                        If Len(Trim(aplRST.Fields(j))) <= cols(j).AnchoMinimo Then
                            sOutput = sOutput & IIf(j = 0, "|", "") & Trim(aplRST.Fields(j)) & ReemplazarHTML_Espacios(Space(cols(j).AnchoMinimo - Len(Trim(aplRST.Fields(j))))) & "|"
                        Else
                            sOutput = sOutput & IIf(j = 0, "|", "") & Mid(Trim(aplRST.Fields(j)), 1, cols(j).AnchoMinimo - 3) & "...|"
                        End If
                    Next j
                    'Debug.Print sOutput
                    ArmarTabla = ArmarTabla & "<br/>" & sOutput
                End If
            Next i
            aplRST.MoveNext
            DoEvents
        Loop
        'Debug.Print sOutput
        'ArmarTabla = ArmarTabla & vbCrLf & sOutput
    Else
        anchoTotal = 0
        For j = 0 To Columnas - 1
            anchoTotal = anchoTotal + cols(j).AnchoMinimo
        Next j
        sOutput = ""
        sOutput = sOutput & "| SIN DATOS" & Space(anchoTotal - Len("| SIN DATOS") + Columnas) & "|"
        'Debug.Print sOutput
        ArmarTabla = ArmarTabla & "<br/>" & sOutput
    End If
    ' Terminaci�n de la tabla
    sOutput = ""
    For j = 0 To Columnas - 1
        sOutput = sOutput & IIf(j = 0, "+", "") & String(cols(j).AnchoMinimo, "-") & "+"
    Next j
    'Debug.Print sOutput
    ArmarTabla = ArmarTabla & "<br/>" & sOutput & "</font>"
End Function

' RESTORE anterior: 06.08.2014
'Public Sub EnviarCorreo_Restore(lSolicitud As Long, bAvisoEnvio As Boolean)
'    Dim sCodigoVerificador As String
'    Dim bResponsable As Boolean
'    Dim bSinEnmascarar As Boolean
'    Dim bDatosSensibles As Boolean      ' Auxiliar: DSN indica si tiene o no datos sensibles declarados
'
'    cTo = ""
'    cCC = ""
'    cBCC = ""
'    cSubject = ""
'    cBody = ""
'
'    ' Primero determino si la solicitud es con enmascaramiento (normal) o SIN enmascaramiento de datos
'    bSinEnmascarar = False
'    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
'        cCC = IIf(glSMTPMODO = 0, "", ClearNull(aplRST.Fields!mail_recurso))
'        bDatosSensibles = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "S", True, False)
'        If ClearNull(aplRST.Fields!sol_mask) = "N" Then
'            bSinEnmascarar = True
'        End If
'    End If
'
'
'
'    If Not InPerfil("CGRU") And Not InPerfil("CSEC") Then       ' Es un analista, debe enviarse el email al l�der
'        bResponsable = False
'        If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, glLOGIN_Grupo) Then
'            cTo = ClearNull(aplRST.Fields!email)
'        Else        ' Si no encuentra el email del l�der, envia directamente al supervisor
'            If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, glLOGIN_Sector, Null) Then
'                cTo = ClearNull(aplRST.Fields!email)
'            Else    ' Si no encontr� al supervisor, entonces deja en blanco para que el usuario complete
'                cTo = ""
'            End If
'        End If
'    Else            ' O es un l�der o un supervisor, debe ir directamente a Carga de M�quina
'        bResponsable = True
'        cTo = POOL_CM
'    End If
'
'    If bSinEnmascarar Then
'        If sp_GetRecursoAcargoArea(glLOGIN_Direccion, glLOGIN_Gerencia, "", "") Then
'            If glSMTPMODO = 0 Then
'                cCC = ClearNull(aplRST.Fields!email)    ' El responsable a cargo de la gerencia del usuario solicitante
'            Else
'                cCC = IIf(Len(cCC) = 0, ClearNull(aplRST.Fields!email), cCC & ", " & ClearNull(aplRST.Fields!email))
'            End If
'        Else
'            If glSMTPMODO = 0 Then
'                cCC = "gsiciliano@bbva.com"
'            Else
'                cCC = IIf(Len(cCC) = 0, "gsiciliano@bbva.com", cCC & ", gsiciliano@bbva.com")  ' Se pone de prepo al gerente
'            End If
'        End If
'    End If
'
'    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
'        If bSinEnmascarar Then
'            cSubject = "EXC - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - RESTORE y transmisi�n PROD/DESA sin enmascaramiento. Solicitud N� " & ClearNull(aplRST.Fields!sol_nroasignado)    ' upd -003- a.
'            cTo = POOL_CM & ", " & POOL_SI
'            If glSMTPMODO = 0 Then
'                cCC = cCC & ", " & ClearNull(aplRST.Fields!mail_recurso)
'            Else
'                cCC = IIf(Len(cCC) = 0, ClearNull(aplRST.Fields!mail_recurso) & ", " & ClearNull(aplRST.Fields!mail_resp_sect), cCC & ", " & ClearNull(aplRST.Fields!mail_recurso) & ", " & ClearNull(aplRST.Fields!mail_resp_sect))
'            End If
'        Else
'            cSubject = "EXC - Petici�n N� " & ClearNull(aplRST.Fields!pet_nroasignado) & " - RESTORE -  Solicitud N� " & ClearNull(aplRST.Fields!sol_nroasignado)
'        End If
'        If Not bResponsable Then
'            If bSinEnmascarar Then      ' add xxx
'                cBody = cBody & "Por favor, autorizar y reenviar a Carga de M�quina la siguiente tarea: " & vbCrLf
'            End If                      ' add xxx
'        End If
'
'        Select Case ClearNull(aplRST.Fields!SOL_TIPO)
'            Case "XCOM*"
'                cBody = cBody & "Obtener BACK UP de archivo " & ClearNull(aplRST.Fields!sol_file_out) & " de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:" & vbCrLf & vbCrLf
'                If bDatosSensibles Then
'                    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'                Else
'                    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'                End If
'            Case "UNLO*"
'                cBody = ""
'                cBody = cBody & "Realizar el siguiente RESTORE:" & vbCrLf & vbCrLf
'                cBody = cBody & "Tabla: " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'                cBody = cBody & "Job de backup: " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'                cBody = cBody & "Fecha de backup: " & ClearNull(aplRST.Fields!sol_fe_auto3) & vbCrLf
'                cBody = cBody & "Filtro a aplicar: " & ClearNull(aplRST.Fields!sol_lib_Sysin) & " (" & ClearNull(aplRST.Fields!sol_mem_Sysin) & ")" & vbCrLf
'                cBody = cBody & "Archivo resultante: " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'            Case Else
'                cBody = cBody & "Obtener BACK UP de archivo " & ClearNull(aplRST.Fields!sol_file_prod) & " de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:" & vbCrLf & vbCrLf
'                If ClearNull(aplRST.Fields!sol_mask) = "S" Then
'                    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'                Else
'                    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'                End If
'        End Select
'        '{ del -000- a.
'        'If ClearNull(aplRST.Fields!SOL_TIPO) = "XCOM*" Then
'        '    cBody = cBody & "Obtener BACK UP de archivo " & ClearNull(aplRST.Fields!sol_file_out) & " de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:" & vbCrLf & vbCrLf
'        '    cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_prod) & vbCrLf
'        'Else
'        '    cBody = cBody & "Obtener BACK UP de archivo " & ClearNull(aplRST.Fields!sol_file_prod) & " de fecha " & ClearNull(aplRST.Fields!sol_fe_auto3) & " y bajar a disco como:" & vbCrLf & vbCrLf
'        '    If ClearNull(aplRST.Fields!sol_mask) = "S" Then
'        '        cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_desa) & vbCrLf
'        '    Else
'        '        cBody = cBody & " - " & ClearNull(aplRST.Fields!sol_file_out) & vbCrLf
'        '    End If
'        'End If
'        '}
'        cBody = cBody & vbCrLf
'        cBody = cBody & "Motivo: " & ClearNull(aplRST.Fields!jus_desc) & vbCrLf
'        cBody = cBody & "Justificaci�n: " & ClearNull(aplRST.Fields!SOL_TEXTO) & vbCrLf
'        cBody = cBody & vbCrLf & vbCrLf
'        cBody = cBody & "Muchas gracias." & vbCrLf & vbCrLf & IIf(glSMTPMODO = 0, "", FOOTER1) & Chr(13) & Chr(13)
'        If bSinEnmascarar Then
'            cBody = cBody & "C�digo Verificador: "
'            sCodigoVerificador = MD5_Solo_09AZaz_y_punto(CRYPTO_PREFIJO & cBody)
'            cBody = cBody & sCodigoVerificador
'        End If
'        Call EnviarCorreo(cTo, cCC, "", cSubject, cBody, bAvisoEnvio)
'    End If
'End Sub

Private Function EnviarCorreo_GENERAL(lSolicitud As Long, bAvisoEnvio As Boolean) As String
    Dim sMensaje As String
    Dim sCodigoVerificador As String
    
    Dim sTipoSolicitud As String
    Dim bEMErgencia As Boolean
    Dim bResponsable As Boolean
    Dim bSinEnmascarar As Boolean
    Dim bDatosSensibles As Boolean      ' Auxiliar: DSN indica si tiene o no datos sensibles declarados
    Dim bRestore As Boolean
    Dim bDiferida As Boolean
    
    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
    
    cTo = ""
    cCC = ""
    cBCC = ""
    cSubject = ""
    cBody = ""
    
    If sp_GetSolicitud(Null, Null, lSolicitud, Null, Null, Null, Null) Then
        sTipoSolicitud = ClearNull(aplRST.Fields!sol_tipo)
        bEMErgencia = IIf(ClearNull(aplRST.Fields!sol_eme) = "S", True, False)
        bSinEnmascarar = IIf(ClearNull(aplRST.Fields!sol_mask) = "S", False, True)
        bDatosSensibles = IIf(ClearNull(aplRST.Fields!dsn_enmas) = "S", True, False)
        bRestore = IIf(ClearNull(aplRST.Fields!dsn_restore) = "S", True, False)
        bDiferida = IIf(ClearNull(aplRST.Fields!SOL_DIFERIDA) = "S", True, False)
        cBody = IIf(bIsHTMLFormatMail, "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">", "")
        
        If bEMErgencia Then
        
        Else
            If bSinEnmascarar Then
            
            Else
            
            End If
        End If
        
        
    End If
End Function

'{ add -002- a.
Public Sub EnviarCorreo_INFORMESHOM(pet_nrointerno, sTipoInforme, sInformeAdjunto As String, homologador As String)
    Dim sRecurso As String
    Dim sRecursos() As String
    Dim sClase As String
    Dim sMail As String
    Dim sMensaje As String
    Dim sTitulo As String
    Dim i As Integer
    
    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
    
    cTo = ""
    cCC = ""
    cBCC = ""
    cSubject = ""
    cBody = ""
    
    Select Case sTipoInforme
        Case "IDH1"
            sTitulo = "SOB"
            sMensaje = "Sin Observaciones (SOB)"
        Case "IDH2"
            sTitulo = "OME"
            sMensaje = "Con Observaciones Menores (OME)"
        Case "IDH3"
            sTitulo = "OMA"
            sMensaje = "Con Observaciones Mayores (OMA)"
    End Select
    
    If sp_GetUnaPeticion(pet_nrointerno) Then
        cSubject = "CGM: Informe de Homologaci�n " & sTitulo & " Pet. Nro. " & ClearNull(aplRST.Fields!pet_nroasignado)
        cBody = cBody & "Se han realizado verificaciones de Homologaci�n a la petici�n de referencia " & sMensaje & ". VER ADJUNTO" & LINEBREAK & LINEBREAK & LINEBREAK
        cBody = cBody & "* Este mail ha sido generado en forma automatica por el sistema de CGM (Peticiones). No es necesario que usted envie respuesta al emisor."
        sClase = ClearNull(aplRST.Fields!cod_clase)
        sRecurso = ClearNull(aplRST.Fields!cod_bpar)
        ' Referente de Sistema
        If sp_GetRecursoMail(sRecurso) Then
            cTo = cTo & ClearNull(aplRST.Fields!email)
        End If
    End If
    
    If sp_GetPeticionGrupo(pet_nrointerno, Null, Null) Then
        ' Guardar en un vector din�mico los legajos de los responsables
        Do While Not aplRST.EOF
            If InStr(1, "RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|", ClearNull(aplRST.Fields!cod_estado), vbTextCompare) = 0 Then
                If ClearNull(aplRST.Fields!cod_responsable) <> "" Then
                    ReDim Preserve sRecursos(i)
                    sRecursos(i) = ClearNull(aplRST.Fields!cod_responsable)
                    i = i + 1
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If

    For i = 0 To UBound(sRecursos)
        If sp_GetRecursoMail(sRecursos(i)) Then
            If ClearNull(aplRST.Fields!euser) = "S" Then
                sMail = ClearNull(aplRST.Fields!email)
                Debug.Print sRecursos(i) & ": " & sMail
                If sMail <> "" Then cCC = cCC & IIf(cCC = "", sMail, ", " & sMail)
            End If
        End If
    Next i
    
    ' Email del homologador
    '{ add -002- b.
    If ClearNull(cCC) <> "" Then
        cCC = cCC & ", " & "homologacion-dyd-arg@bbva.com"
    Else
        cCC = "homologacion-dyd-arg@bbva.com"
    End If
    '}
    '{ del -002- b.
    'sMail = ""
    'If sp_GetRecursoMail(homologador) Then
    '    sMail = ClearNull(aplRST.Fields!email)
    '    If sMail <> "" Then cCC = cCC & ", " & sMail
    'End If
    '}

    If cTo <> "" Then
        Call EnviarCorreo(cTo, cCC, cBCC, cSubject, cBody, False, sInformeAdjunto, True)
    End If
End Sub
'}

'{ add -003- a.
Public Sub EnviarCorreo_SEGURIDADINFO(sLegajo As String, sMensaje As String)
    Dim i As Integer
    Dim cFooter As String
    
    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
   
    cTo = "seginf.ar@bbva.com"
    cCC = ""
    cBCC = ""
    cSubject = "CGM (Peticiones) - BBVA Frances"
    cBody = IIf(bIsHTMLFormatMail, "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">", "")
    cBody = cBody & sMensaje & sLegajo & LINEBREAK & LINEBREAK
    cFooter = cFooter & "" & LINEBREAK
    cFooter = cFooter & "* Este mail ha sido generado en forma automatica por el sistema de CGM" & LINEBREAK
    cFooter = cFooter & "(Peticiones). No es necesario que usted envie respuesta al emisor." & LINEBREAK
    cBody = cBody & LINEBREAK & LINEBREAK & cFooter
    Call EnviarCorreo(cTo, cCC, cBCC, cSubject, cBody, False, "", True)
End Sub
'}

Public Sub EnviarCorreo_CancelaPeticion(sMail, peticiones As Collection)
    Dim i As Integer
    Dim cFooter As String

    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)

    cTo = sMail
    'cCC = "fernandojavier.spitz@bbva.com"
    cCC = ""
    cBCC = ""
    'cBCC = "fernandojavier.spitz@bbva.com"
    cSubject = "CGM (Peticiones) - BBVA Frances"
    cBody = IIf(bIsHTMLFormatMail, "<font face=" & Chr(34) & "Courier New" & Chr(34) & ">", "")
    'cBody = cBody & "Se procedi� a cancelar por falta de prioridad las siguientes peticiones: " & LINEBREAK & LINEBREAK
    cBody = cBody & "Se realiz� la cancelaci�n de las siguientes peticiones: " & LINEBREAK & LINEBREAK

    For i = 1 To peticiones.Count
        cBody = cBody & peticiones(i) & LINEBREAK
    Next i

    cFooter = cFooter & "" & LINEBREAK
    cFooter = cFooter & "* Este mail ha sido generado en forma automatica por el sistema de CGM" & LINEBREAK
    cFooter = cFooter & "(Peticiones). No es necesario que usted envie respuesta al emisor." & LINEBREAK
    cFooter = cFooter & "" & LINEBREAK
    'cFooter = cFooter & "* Los mensajes contenidos en este mail pueden ser consultados en el sistema" & LINEBREAK
    'cFooter = cFooter & "de CGM (Peticiones) mediante la opcion [Ver Mensajes] del menu [Perfiles]." & LINEBREAK
    'cFooter = cFooter & "Esta funcionalidad permite acceder a la peticion directamente desde cada mensaje." & LINEBREAK
    'cFooter = cFooter & "" & LINEBREAK
    cFooter = cFooter & "* Si usted no desea recibir por mail los mensajes generados por el sistema" & LINEBREAK
    cFooter = cFooter & "de CGM (Peticiones), por favor avise al Administrador, quien procedera a deshabilitar el envio." & LINEBREAK
    cFooter = cFooter & "" & LINEBREAK
    cFooter = cFooter & "* Por cualquier consulta comunicarse al correo:" & LINEBREAK
    cFooter = cFooter & "gestion_demanda-arg@bbva.com"
    cBody = cBody & LINEBREAK & LINEBREAK & cFooter
    Call EnviarCorreo(cTo, cCC, cBCC, cSubject, cBody, False, "", True)
End Sub

'{ add -004- a.
'Public Sub EnviarCorreo_InformeHomologacion(pet_nrointerno, info_id, sDestinatario, sCopias, sTitulo, sMensaje)
Public Sub EnviarCorreo_InformeHomologacion(pet_nrointerno, info_id, info_verid, oResponsable As udtResponsable)
    Const TABLE_ROW_FORMAT = "style='padding:7px 5px;'"
    Const TABLE_DATA_FORMAT = "style='padding:10px 5px;  border:1px solid #FFF;'"
    
    Dim i As Integer
    Dim cHTML_HEAD As String
    Dim cHTML_BODY As String
    Dim cEncabezadoGrupo As String
    Dim cObservaciones As String
    Dim cFooter As String
    Dim bOdds As Boolean
    Dim DOUBLE_QUOTES As String
    Dim lPetNroAsignado As Long

    bIsHTMLFormatMail = True
    LINEBREAK = IIf(bIsHTMLFormatMail, "<br/>", vbCrLf)
    DOUBLE_QUOTES = Chr(34)
    
    cSubject = ""
    cBody = ""
    cEncabezadoGrupo = ""
    
    cHTML_HEAD = ""
    cHTML_BODY = ""
    
    If sp_GetUnaPeticion(pet_nrointerno) Then
        lPetNroAsignado = ClearNull(aplRST.Fields!pet_nroasignado)
    End If
    
    ' Encabezado
    cHTML_HEAD = cHTML_HEAD & "<head>"
    cHTML_HEAD = cHTML_HEAD & "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>"
    'cHTML_HEAD = cHTML_HEAD & "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'><style>*{margin:0; padding:0;}body{font-family:Arial, Verdana, sans-serif;"
    'cHTML_HEAD = cHTML_HEAD & "font-size:12px; color:#4c4c4c; padding:20px;}.oma{color:#c8175e;}.rowS{background-color:#eeeeee; color:#4c4c4c; font-size:14px; line-height:12px; font-variant:small-caps; "
    'cHTML_HEAD = cHTML_HEAD & "text-align:center;}.rowS td{ padding:10px 5px;  border:1px solid #FFF;}.rowT{background-color:#009ee5; padding:0px 10px; color:#FFF; font-size:20px; font-variant:small-caps;"
    'cHTML_HEAD = cHTML_HEAD & "border-top-width:20px; border-bottom:2px solid #009ee5!important; background-color:#FFF; color:#009ee5; margin:0; margin-top:25px; margin-bottom:5px;}.row1{border-bottom:1px solid"
    'cHTML_HEAD = cHTML_HEAD & "#ccc;}.row1 td{padding:7px 5px;}.row2{border-bottom:1px solid #ccc;}.row2 td{padding:7px 5px;}.row3 td{padding:10px 5px; line-height:18px;}.subtitulo{font-family: 'Open Sans',"
    'cHTML_HEAD = cHTML_HEAD & "sans-serif; font-weight:300; font-size:24px; margin-bottom:35px;}table{font-family:Arial, Verdana, sans-serif; font-size:12px; display:table; border-collapse:collapse;"
    'cHTML_HEAD = cHTML_HEAD & "color:#4c4c4c;}.titulo{font-family: 'Open Sans', sans-serif; font-weight:300; font-size:30px; color:#094fa4; margin-top:15px;}</style>"
    cHTML_HEAD = cHTML_HEAD & "</head>"

    ' Detalle
    cHTML_BODY = cHTML_BODY & cHTML_HEAD & "<body style='font-family:Arial, Verdana, sans-serif; font-size:12px; color:#4c4c4c; padding:20px;'>"
    cHTML_BODY = cHTML_BODY & cHTML_HEAD & "<p style='font-family: " & DOUBLE_QUOTES & "Open Sans" & DOUBLE_QUOTES & ", sans-serif; font-weight:300; font-size:30px; color:#094fa4; margin-top:15px;'><span>Peticion Nro.: </span>" & lPetNroAsignado & "</p>"
    cHTML_BODY = cHTML_BODY & cHTML_HEAD & "<p style='font-family: " & DOUBLE_QUOTES & "Open Sans" & DOUBLE_QUOTES & ", sans-serif; font-weight:300; font-size:24px; margin-bottom:35px;'><span>Informe de homologaci�n Nro.: </span>" & info_id & "</p>"
    cHTML_BODY = cHTML_BODY & "<table cellspacing='0' style='font-family:Arial, Verdana, sans-serif; font-size:12px; display:table; border-collapse:collapse; color:#4c4c4c;'>"
    cHTML_BODY = cHTML_BODY & "<tr style='background-color:#eeeeee; color:#4c4c4c; font-size:14px; line-height:12px; font-variant:small-caps; text-align:center;'>"
    cHTML_BODY = cHTML_BODY & "<td style='padding:10px 5px;  border:1px solid #FFF;'>item</td>"
    cHTML_BODY = cHTML_BODY & "<td style='padding:10px 5px;  border:1px solid #FFF;'>actividad planificada</td>"
    cHTML_BODY = cHTML_BODY & "<td style='padding:10px 5px;  border:1px solid #FFF;'>obligatorio / recomendado</td>"
    cHTML_BODY = cHTML_BODY & "<td style='padding:10px 5px;  border:1px solid #FFF;'>estado</td>"
    cHTML_BODY = cHTML_BODY & "<td style='padding:10px 5px;  border:1px solid #FFF;'>observ.</td>"
    cHTML_BODY = cHTML_BODY & "<td style='padding:10px 5px;  border:1px solid #FFF;'>comentarios</td>"
    cHTML_BODY = cHTML_BODY & "<td style='padding:10px 5px;  border:1px solid #FFF;'>responsables</td>"
    cHTML_BODY = cHTML_BODY & "</tr>"
    
    ' Recorro el detalle para cada responsable
    If sp_GetPeticionInfohrm(pet_nrointerno, info_id, info_verid, oResponsable.Legajo) Then
        Do While Not aplRST.EOF
            For i = 1 To oResponsable.Items.Count
                If ClearNull(aplRST.Fields!info_item) = oResponsable.Items(i) Then
                    If LCase(cEncabezadoGrupo) = LCase(ClearNull(aplRST.Fields!encabezado)) Then
                        ' Hace el HTML para el item y sigue con el pr�ximo...
                        cHTML_BODY = cHTML_BODY & "<tr>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!info_item) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!infoprot_itemdsc) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!req_dsc) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!nom_estado) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!valor_dsc) & "</td>"
                        'If ClearNull(aplRST.Fields!valor_dsc) = "OMA" Then
                        '    cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'><span style='color:#c8175e;'>" & ClearNull(aplRST.Fields!valor_dsc) & "</span></td>"
                        'Else
                        '    cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!valor_dsc) & "</td>"
                        'End If
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!info_comen) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!nom_recurso) & "</td>"
                        cHTML_BODY = cHTML_BODY & "</tr>" '& LINEBREAK
                    Else
                        cEncabezadoGrupo = UCase(ClearNull(aplRST.Fields!encabezado))
                        'If UCase(cEncabezadoGrupo) = "PRUEBAS" Then Stop   ' DESARROLLO
                        cHTML_BODY = cHTML_BODY & "<tr><td colspan=" & DOUBLE_QUOTES & "7" & DOUBLE_QUOTES & "><p style='background-color:#009ee5; padding:0px 10px; color:#FFF; font-size:20px; font-variant:small-caps; border-top-width:20px; border-bottom:2px solid #009ee5!important; background-color:#FFF; color:#009ee5; margin:0; margin-top:25px; margin-bottom:5px;'>" & cEncabezadoGrupo & "</p></td></tr>"
                        cHTML_BODY = cHTML_BODY & "<tr>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!info_item) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!infoprot_itemdsc) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!req_dsc) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!nom_estado) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!valor_dsc) & "</td>"
                        'If ClearNull(aplRST.Fields!valor_dsc) = "OMA" Then
                        '    cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'><span style='color:#c8175e;'>" & ClearNull(aplRST.Fields!valor_dsc) & "</span></td>"
                        'Else
                        '    cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!valor_dsc) & "</td>"
                        'End If
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!info_comen) & "</td>"
                        cHTML_BODY = cHTML_BODY & "<td style='padding:7px 5px; border-bottom:1px solid #ccc;'>" & ClearNull(aplRST.Fields!nom_recurso) & "</td>"
                        cHTML_BODY = cHTML_BODY & "</tr>" '& LINEBREAK
                    End If
                    Exit For
                End If
            Next i
            aplRST.MoveNext
            DoEvents
        Loop
        ' Si existen observaciones generales, se agregan al final
        cObservaciones = sp_GetPeticionInfohMemo(pet_nrointerno, info_id, info_verid)
        If Trim(ClearNull(cObservaciones)) <> "" Then
            cHTML_BODY = cHTML_BODY & "<tr><td colspan='7'><p style='background-color:#009ee5; padding:0px 10px; color:#FFF; font-size:20px; font-variant:small-caps; border-top-width:20px; border-bottom:2px solid #009ee5!important; background-color:#FFF; color:#009ee5; margin:0; margin-top:25px; margin-bottom:5px;'>" & UCase("observaciones generales") & "</p></td></tr>"
            cHTML_BODY = cHTML_BODY & "<tr class=" & DOUBLE_QUOTES & "row3" & DOUBLE_QUOTES & ">" & "<td colspan=" & DOUBLE_QUOTES & "7" & DOUBLE_QUOTES & " style='padding:10px 5px; line-height:18px;'>"
            cHTML_BODY = cHTML_BODY & "<p>" & ClearNull(cObservaciones) & "</p>"
            cHTML_BODY = cHTML_BODY & "</td></tr>"
        End If
        cHTML_BODY = cHTML_BODY & "</table><body>"
    End If

    cTo = oResponsable.email
    cCC = ""
    cBCC = ""
    cSubject = "Informe de homologaci�n"
    cBody = cBody & cHTML_BODY
    If cTo <> "" Then
        Call EnviarCorreo(cTo, cCC, cBCC, cSubject, cBody, True, "", bIsHTMLFormatMail)
        If glEMAIL_STATUS = EMAIL_STATUS_SUCCESSFUL Then
            For i = 1 To oResponsable.Items.Count
                Call sp_UpdatePeticionInfohrField(pet_nrointerno, info_id, info_verid, oResponsable.Items(i), oResponsable.Legajo, "FECHA_MAIL", Null, Now, Null)
                DoEvents
            Next i
        End If
    End If
End Sub
'}
