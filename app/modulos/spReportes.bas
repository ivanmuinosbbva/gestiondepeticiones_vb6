Attribute VB_Name = "spReportes"
' -001- a. FJS 05.07.2007 - Se agregan los par�metros requeridos para soportar los nuevos datos de la consulta (clase de petici�n, impacto tecnol�gico, y los documentos asociados).
' -002- a. FJS 09.04.2008 - Nueva funci�n para cargar un recordset con informaci�n de horas reales cargadas a una petici�n
' -003- a. FJS 21.07.2008 - Se agrega nuevo par�metro para devolver peticiones con documentos adjuntos o no.
' -004- a. FJS 10.09.2008 - Se agrega un nuevo par�metro para Regulatorio.
' -005- a. FJS 17.03.2009 - Se agrega un nuevo par�metro para filtrar por tipo de documento adjunto.
' -006- a. FJS 26.03.2009 - Se agrega un nuevo par�metro para indicar d�nde deben buscarse las peticiones: hist�ricos o en l�nea.
' -007- a. FJS 31.03.2009 - Se agregan nuevos par�metros.
' -008- a. FJS 03.06.2009 - Se agregan tres nuevos par�metros para filtrar la consulta por proyectos IDM (PrjId, PrjSubId, PrjSubsId).
' -008- b. FJS 03.06.2009 - Se quita el c�digo que soportaba los viejos datos de proyectos (no proyectos IDM) por limitaciones del lenguaje VB5.
' -009- a. FJS 15.03.2010 - Se agrega una rutina para listar el comando SQL con los par�metros del mismo (debug).
' -010- a. FJS 16.02.2016 - Nuevo: se agrega funcionalidad para la nueva gerencia BPE.
' -011- a. FJS 30.06.2016 - Nuevo: nuevo filtro por categor�a.
' -012- a. FJS 24.08.2016 - Nuevo: se adaptan las consultas para extender el timeout del objeto command para evitar cancelaciones.

Option Explicit

Public Sub DESA_MostrarLlamada(ByVal cmd As ADODB.Command)
    Dim sSQLCommand As String
    Dim sSQLParametros As String
    Dim i As Long
    
    With cmd
        sSQLCommand = Mid(cmd.CommandText, InStr(1, cmd.CommandText, "sp_", vbTextCompare), InStr(1, cmd.CommandText, "(", vbTextCompare) - 8)
        sSQLCommand = sSQLCommand & " "
        For i = 0 To .Parameters.Count - 1
            Select Case .Parameters(i).Type
                Case adChar, adVarChar: sSQLCommand = sSQLCommand & IIf(IsNull(.Parameters(i).value), "NULL", "'" & .Parameters(i).value & "'")
                Case adInteger, adNumeric: sSQLCommand = sSQLCommand & IIf(IsNull(.Parameters(i).value), "0", .Parameters(i).value)
                Case adDBDate, adDate: sSQLCommand = sSQLCommand & "'" & Format(.Parameters(i).value, "yyyyMMdd") & "'"
                Case Else
                    Debug.Print "Excepcion"
                    sSQLCommand = sSQLCommand & "'" & .Parameters(i).value & "'"
            End Select
            If .Parameters.Count - 1 > i Then
                sSQLCommand = sSQLCommand & ", "
            End If
        Next i
        
        For i = 0 To .Parameters.Count - 1
            Select Case .Parameters(i).Type
                Case adChar, adVarChar
                    sSQLParametros = sSQLParametros & cmd.Parameters(i).name & ": " & IIf(IsNull(.Parameters(i).value), "NULL", "'" & .Parameters(i).value & "'") & vbCrLf
                Case adInteger, adNumeric
                    sSQLParametros = sSQLParametros & cmd.Parameters(i).name & ": " & IIf(IsNull(.Parameters(i).value), "0", .Parameters(i).value) & vbCrLf
                Case adDBDate, adDate
                    sSQLParametros = sSQLParametros & cmd.Parameters(i).name & ": " & "'" & Format(.Parameters(i).value, "yyyyMMdd") & "'" & vbCrLf
                Case Else
                    Debug.Print "Excepcion"
                    sSQLParametros = sSQLParametros & "'" & .Parameters(i).value & "'" & vbCrLf
            End Select
        Next i
        Debug.Print sSQLParametros
    End With
    Debug.Print sSQLCommand
End Sub

'Function sp_rptPeticion00(pet_nrodesde, pet_nrohasta, pet_tipo, pet_prioridad, pet_desde_alta, pet_hasta_alta, pet_nivel, pet_area, pet_estado, pet_desde_estado, pet_hasta_estado, _
'                          pet_situacion, pet_titulo, sec_nivel, sec_dire, sec_gere, sec_sect, sec_grup, sec_estado, sec_situacion, sec_desde_iplan, sec_hasta_iplan, sec_desde_fplan, _
'                          sec_hasta_fplan, sec_desde_ireal, sec_hasta_ireal, sec_desde_freal, sec_hasta_freal, auxAgrup, cod_importancia, cod_bpar, cod_clase, pet_imptech, pet_Docu, _
'                          pet_regulatorio, adj_tipo, tipo_texto, filtro_texto, criterio_texto, cod_recurso, fecha_pedido_desde, fecha_pedido_hasta, prjid, prjsubid, prjsubsid, pet_emp, _
'                          pet_ro, cod_BPE, fechaBPEDesde, fechaBPEHasta, categoria) As Boolean    ' upd -004- a.    ' upd -005- a. ' upd -008- a.  ' upd -011- a.

Function sp_rptPeticion00(pet_nrodesde, pet_nrohasta, pet_tipo, pet_prioridad, pet_desde_alta, pet_hasta_alta, pet_nivel, pet_area, pet_estado, pet_desde_estado, pet_hasta_estado, _
                          pet_situacion, pet_titulo, sec_nivel, sec_dire, sec_gere, sec_sect, sec_grup, sec_estado, sec_situacion, sec_desde_iplan, sec_hasta_iplan, sec_desde_fplan, _
                          sec_hasta_fplan, sec_desde_ireal, sec_hasta_ireal, sec_desde_freal, sec_hasta_freal, auxAgrup, cod_importancia, cod_bpar, cod_clase, pet_imptech, _
                          pet_regulatorio, prjid, prjsubid, prjsubsid, pet_emp, pet_ro, cod_bpe, fechaBPEDesde, fechaBPEHasta, categoria, pet_alta_desde, pet_alta_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_rptPeticion00"
        .CommandTimeout = 300
        .Parameters.Append .CreateParameter("@pet_nrodesde", adInteger, adParamInput, , CLng(Val(pet_nrodesde)))
        .Parameters.Append .CreateParameter("@pet_nrohasta", adInteger, adParamInput, , CLng(Val(pet_nrohasta)))
        .Parameters.Append .CreateParameter("@pet_tipo", adChar, adParamInput, 4, ClearNull(pet_tipo))
        .Parameters.Append .CreateParameter("@pet_prioridad", adChar, adParamInput, 4, ClearNull(pet_prioridad))
        .Parameters.Append .CreateParameter("@pet_desde_alta", adChar, adParamInput, 8, IIf(IsNull(pet_desde_alta), "NULL", Format(pet_desde_alta, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_alta", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_alta), "NULL", Format(pet_hasta_alta, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_nivel", adChar, adParamInput, 4, ClearNull(pet_nivel))
        .Parameters.Append .CreateParameter("@pet_area", adChar, adParamInput, 8, ClearNull(pet_area))
        .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 250, ClearNull(pet_estado))
        .Parameters.Append .CreateParameter("@pet_desde_estado", adChar, adParamInput, 8, IIf(IsNull(pet_desde_estado), "NULL", Format(pet_desde_estado, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_estado", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_estado), "NULL", Format(pet_hasta_estado, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_situacion", adChar, adParamInput, 8, ClearNull(pet_situacion))
        .Parameters.Append .CreateParameter("@pet_titulo", adChar, adParamInput, 59, ClearNull(pet_titulo))
        .Parameters.Append .CreateParameter("@sec_nivel", adChar, adParamInput, 4, ClearNull(sec_nivel))
        .Parameters.Append .CreateParameter("@sec_dire", adChar, adParamInput, 8, ClearNull(sec_dire))
        .Parameters.Append .CreateParameter("@sec_gere", adChar, adParamInput, 8, ClearNull(sec_gere))
        .Parameters.Append .CreateParameter("@sec_sect", adChar, adParamInput, 8, ClearNull(sec_sect))
        .Parameters.Append .CreateParameter("@sec_grup", adChar, adParamInput, 8, ClearNull(sec_grup))
        .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 250, ClearNull(sec_estado))
        .Parameters.Append .CreateParameter("@sec_situacion", adChar, adParamInput, 8, ClearNull(sec_situacion))
        .Parameters.Append .CreateParameter("@sec_desde_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_iplan), "NULL", Format(sec_desde_iplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_iplan), "NULL", Format(sec_hasta_iplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_fplan), "NULL", Format(sec_desde_fplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_fplan), "NULL", Format(sec_hasta_fplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_ireal), "NULL", Format(sec_desde_ireal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_ireal), "NULL", Format(sec_hasta_ireal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_freal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_freal), "NULL", Format(sec_desde_freal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_freal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_freal), "NULL", Format(sec_hasta_freal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
        .Parameters.Append .CreateParameter("@cod_importancia", adChar, adParamInput, 4, ClearNull(cod_importancia))
        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, ClearNull(cod_bpar))
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, IIf(cod_clase = "NULL", Null, cod_clase))
        .Parameters.Append .CreateParameter("@pet_imptech", adChar, adParamInput, 1, IIf(pet_imptech = "NULL", Null, pet_imptech))
        .Parameters.Append .CreateParameter("@pet_regulatorio", adChar, adParamInput, 1, IIf(pet_regulatorio = "NULL", Null, pet_regulatorio))    ' add -004- a.
        .Parameters.Append .CreateParameter("@PrjId", adInteger, adParamInput, , IIf(prjid = 0, Null, CLng(Val(prjid))))
        .Parameters.Append .CreateParameter("@PrjSubId", adInteger, adParamInput, , IIf(prjsubid = 0, Null, CLng(Val(prjsubid))))
        .Parameters.Append .CreateParameter("@PrjSubsId", adInteger, adParamInput, , IIf(prjsubsid = 0, Null, CLng(Val(prjsubsid))))
        .Parameters.Append .CreateParameter("@pet_emp", adInteger, adParamInput, , pet_emp)
        .Parameters.Append .CreateParameter("@pet_ro", adChar, adParamInput, 1, IIf(pet_ro = "NULL", Null, pet_ro))
        .Parameters.Append .CreateParameter("@cod_GBPE", adChar, adParamInput, 10, IIf(ClearNull(cod_bpe) = "NULL", Null, ClearNull(cod_bpe)))
        .Parameters.Append .CreateParameter("@fechaDesdeBPE", adChar, adParamInput, 8, IIf(IsNull(fechaBPEDesde) Or fechaBPEDesde = "", "NULL", Format(fechaBPEDesde, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@fechaHastaBPE", adChar, adParamInput, 8, IIf(IsNull(fechaBPEHasta) Or fechaBPEHasta = "", "NULL", Format(fechaBPEHasta, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@categoria", adChar, adParamInput, 3, IIf(ClearNull(categoria) = "NULL", Null, ClearNull(categoria)))      ' add -011- a.
        .Parameters.Append .CreateParameter("@altaPetDesde", adChar, adParamInput, 8, IIf(IsNull(pet_alta_desde), "NULL", Format(pet_alta_desde, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@altaPetHasta", adChar, adParamInput, 8, IIf(IsNull(pet_alta_hasta), "NULL", Format(pet_alta_hasta, "yyyymmdd")))
        Call DESA_MostrarLlamada(objCommand)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_rptPeticion00 = True
    End If
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    '{ upd -012- a.
    If InStr(1, Trim(Err.DESCRIPTION), "Read from the server has timed out.", vbTextCompare) > 0 Then
        Call Status("La consulta demorar� unos minutos m�s... aguarde...")
        objCommand.CommandTimeout = objCommand.CommandTimeout + 60          ' Se va adicionando de a un minuto
        Resume
    Else
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_rptPeticion00 = False
    End If
    '}
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

''{ add -005- a.
'Function sp_rptPeticion00h(pet_nrodesde, pet_nrohasta, pet_tipo, pet_prioridad, pet_desde_alta, pet_hasta_alta, pet_nivel, pet_area, pet_estado, pet_desde_estado, pet_hasta_estado, pet_situacion, pet_titulo, sec_nivel, sec_dire, sec_gere, sec_sect, sec_grup, sec_estado, sec_situacion, sec_desde_iplan, sec_hasta_iplan, sec_desde_fplan, sec_hasta_fplan, sec_desde_ireal, sec_hasta_ireal, sec_desde_freal, sec_hasta_freal, auxAgrup, cod_importancia, cod_bpar, _
'        cod_clase, pet_imptech, pet_Docu, pet_regulatorio, adj_tipo, prjid, prjsubid, prjsubsid) As Boolean    ' upd -004- a.    ' upd -005- a.  ' upd -008- a.
'    On Error Resume Next
'
'    Dim i As Long
'    Dim cSQL As String
'
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'      .ActiveConnection = aplCONN
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_rptPeticion00h"
'      .Parameters.Append .CreateParameter("@pet_nrodesde", adInteger, adParamInput, , CLng(Val(pet_nrodesde)))
'      .Parameters.Append .CreateParameter("@pet_nrohasta", adInteger, adParamInput, , CLng(Val(pet_nrohasta)))
'      .Parameters.Append .CreateParameter("@pet_tipo", adChar, adParamInput, 4, ClearNull(pet_tipo))
'      .Parameters.Append .CreateParameter("@pet_prioridad", adChar, adParamInput, 4, ClearNull(pet_prioridad))
'      .Parameters.Append .CreateParameter("@pet_desde_alta", adChar, adParamInput, 8, IIf(IsNull(pet_desde_alta), "NULL", Format(pet_desde_alta, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@pet_hasta_alta", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_alta), "NULL", Format(pet_hasta_alta, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@pet_nivel", adChar, adParamInput, 4, ClearNull(pet_nivel))
'      .Parameters.Append .CreateParameter("@pet_area", adChar, adParamInput, 8, ClearNull(pet_area))
'      .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 250, ClearNull(pet_estado))
'      .Parameters.Append .CreateParameter("@pet_desde_estado", adChar, adParamInput, 8, IIf(IsNull(pet_desde_estado), "NULL", Format(pet_desde_estado, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@pet_hasta_estado", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_estado), "NULL", Format(pet_hasta_estado, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@pet_situacion", adChar, adParamInput, 8, ClearNull(pet_situacion))
'      .Parameters.Append .CreateParameter("@pet_titulo", adChar, adParamInput, 120, ClearNull(pet_titulo))
'      .Parameters.Append .CreateParameter("@sec_nivel", adChar, adParamInput, 4, ClearNull(sec_nivel))
'      .Parameters.Append .CreateParameter("@sec_dire", adChar, adParamInput, 8, ClearNull(sec_dire))
'      .Parameters.Append .CreateParameter("@sec_gere", adChar, adParamInput, 8, ClearNull(sec_gere))
'      .Parameters.Append .CreateParameter("@sec_sect", adChar, adParamInput, 8, ClearNull(sec_sect))
'      .Parameters.Append .CreateParameter("@sec_grup", adChar, adParamInput, 8, ClearNull(sec_grup))
'      .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 250, ClearNull(sec_estado))
'      .Parameters.Append .CreateParameter("@sec_situacion", adChar, adParamInput, 8, ClearNull(sec_situacion))
'      .Parameters.Append .CreateParameter("@sec_desde_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_iplan), "NULL", Format(sec_desde_iplan, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@sec_hasta_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_iplan), "NULL", Format(sec_hasta_iplan, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@sec_desde_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_fplan), "NULL", Format(sec_desde_fplan, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@sec_hasta_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_fplan), "NULL", Format(sec_hasta_fplan, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@sec_desde_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_ireal), "NULL", Format(sec_desde_ireal, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@sec_hasta_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_ireal), "NULL", Format(sec_hasta_ireal, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@sec_desde_freal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_freal), "NULL", Format(sec_desde_freal, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@sec_hasta_freal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_freal), "NULL", Format(sec_hasta_freal, "yyyymmdd")))
'      .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
'      .Parameters.Append .CreateParameter("@cod_importancia", adChar, adParamInput, 4, ClearNull(cod_importancia))
'      .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, ClearNull(cod_bpar))
'      '{ del -008- b.
'      '.Parameters.Append .CreateParameter("@prj_filtrar", adChar, adParamInput, 1, ClearNull(prj_filtrar))
'      '.Parameters.Append .CreateParameter("@prj_nrointerno", adInteger, adParamInput, , CLng(Val(prj_nrointerno)))
'      '.Parameters.Append .CreateParameter("@cod_tipoprj", adChar, adParamInput, 8, ClearNull(cod_tipoprj))
'      '.Parameters.Append .CreateParameter("@prj_titulo", adChar, adParamInput, 50, ClearNull(prj_titulo))
'      '.Parameters.Append .CreateParameter("@prj_bpar", adChar, adParamInput, 10, ClearNull(prj_bpar))
'      '.Parameters.Append .CreateParameter("@prj_cod_orientacion", adChar, adParamInput, 4, ClearNull(prj_cod_orientacion))
'      '.Parameters.Append .CreateParameter("@prj_importancia_cod", adChar, adParamInput, 4, ClearNull(prj_importancia_cod))
'      '.Parameters.Append .CreateParameter("@prj_estado", adChar, adParamInput, 250, ClearNull(prj_estado))
'      '.Parameters.Append .CreateParameter("@prj_nivel", adChar, adParamInput, 4, ClearNull(prj_nivel))
'      '.Parameters.Append .CreateParameter("@prj_area", adChar, adParamInput, 8, ClearNull(prj_area))
'      '.Parameters.Append .CreateParameter("@prj_desde_ini_plan", adChar, adParamInput, 8, IIf(IsNull(prj_desde_ini_plan), "NULL", Format(prj_desde_ini_plan, "yyyymmdd")))
'      '.Parameters.Append .CreateParameter("@prj_hasta_ini_plan", adChar, adParamInput, 8, IIf(IsNull(prj_hasta_ini_plan), "NULL", Format(prj_hasta_ini_plan, "yyyymmdd")))
'      '.Parameters.Append .CreateParameter("@prj_desde_fin_plan", adChar, adParamInput, 8, IIf(IsNull(prj_desde_fin_plan), "NULL", Format(prj_desde_fin_plan, "yyyymmdd")))
'      '.Parameters.Append .CreateParameter("@prj_hasta_fin_plan", adChar, adParamInput, 8, IIf(IsNull(prj_hasta_fin_plan), "NULL", Format(prj_hasta_fin_plan, "yyyymmdd")))
'      '.Parameters.Append .CreateParameter("@prj_desde_ini_real", adChar, adParamInput, 8, IIf(IsNull(prj_desde_ini_real), "NULL", Format(prj_desde_ini_real, "yyyymmdd")))
'      '.Parameters.Append .CreateParameter("@prj_hasta_ini_real", adChar, adParamInput, 8, IIf(IsNull(prj_hasta_ini_real), "NULL", Format(prj_hasta_ini_real, "yyyymmdd")))
'      '.Parameters.Append .CreateParameter("@prj_desde_fin_real", adChar, adParamInput, 8, IIf(IsNull(prj_desde_fin_real), "NULL", Format(prj_desde_fin_real, "yyyymmdd")))
'      '.Parameters.Append .CreateParameter("@prj_hasta_fin_real", adChar, adParamInput, 8, IIf(IsNull(prj_hasta_fin_real), "NULL", Format(prj_hasta_fin_real, "yyyymmdd")))
'      '}
'      .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, IIf(cod_clase = "TODO", "NULL", cod_clase))
'      .Parameters.Append .CreateParameter("@pet_imptech", adChar, adParamInput, 1, IIf(pet_imptech = "T", "T", pet_imptech))
'      .Parameters.Append .CreateParameter("@pet_docu", adChar, adParamInput, 1, IIf(pet_Docu = "T", "T", pet_Docu))     ' add -003- a.
'      .Parameters.Append .CreateParameter("@pet_regulatorio", adChar, adParamInput, 1, IIf(pet_regulatorio = "NULL", Null, pet_regulatorio))     ' add -004- a.
'      .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)                   ' add -005- a.
'      '{ add -008- a.
'      .Parameters.Append .CreateParameter("@PrjId", adInteger, adParamInput, , CLng(Val(prjid)))
'      .Parameters.Append .CreateParameter("@PrjSubId", adInteger, adParamInput, , CLng(Val(prjsubid)))
'      .Parameters.Append .CreateParameter("@PrjSubsId", adInteger, adParamInput, , CLng(Val(prjsubsid)))
'      '}
'      Set aplRST = .Execute
'    End With
'    sp_rptPeticion00h = True
'    If aplCONN.Errors.Count > 0 Then
'        GoTo Err_SP
'    End If
'    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
'        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
'            GoTo Err_SP
'        End If
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_rptPeticion00h = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
''}

Function sp_rptPeticion01(Ambito As Byte, pet_nrodesde, pet_nrohasta, pet_tipo, pet_prioridad, pet_desde_bpar, pet_hasta_bpar, pet_nivel, pet_area, pet_estado, pet_desde_estado, pet_hasta_estado, pet_situacion, pet_titulo, sec_nivel, sec_dire, sec_gere, sec_sect, sec_grup, sec_estado, sec_situacion, sec_desde_iplan, sec_hasta_iplan, sec_desde_fplan, sec_hasta_fplan, sec_desde_ireal, sec_hasta_ireal, sec_desde_freal, sec_hasta_freal, auxAgrup, cod_importancia, cod_bpar, _
                          cod_clase, pet_imptech, pet_regulatorio, pet_emp, pet_ro, prjid, prjsubid, prjsubsid, cod_bpe, fechaBPEDesde, fechaBPEHasta, categoria, pet_desde_alta, pet_hasta_alta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    Call Puntero(True)
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = IIf(Ambito > 0, "sp_rptPeticion01h", "sp_rptPeticion01")       ' add -006- a.
        .CommandTimeout = 0                                                           ' Se explicita el valor del timeout indefinidamente para evitar cancelaciones
        .Parameters.Append .CreateParameter("@pet_nrodesde", adInteger, adParamInput, , CLng(Val(pet_nrodesde)))
        .Parameters.Append .CreateParameter("@pet_nrohasta", adInteger, adParamInput, , CLng(Val(pet_nrohasta)))
        .Parameters.Append .CreateParameter("@pet_tipo", adChar, adParamInput, 4, ClearNull(pet_tipo))
        .Parameters.Append .CreateParameter("@pet_prioridad", adChar, adParamInput, 4, ClearNull(pet_prioridad))
        .Parameters.Append .CreateParameter("@pet_desde_bpar", adChar, adParamInput, 8, IIf(IsNull(pet_desde_bpar), "NULL", Format(pet_desde_bpar, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_bpar", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_bpar), "NULL", Format(pet_hasta_bpar, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_nivel", adChar, adParamInput, 4, ClearNull(pet_nivel))
        .Parameters.Append .CreateParameter("@pet_area", adChar, adParamInput, 8, ClearNull(pet_area))
        .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 250, ClearNull(pet_estado))
        .Parameters.Append .CreateParameter("@pet_desde_estado", adChar, adParamInput, 8, IIf(IsNull(pet_desde_estado), "NULL", Format(pet_desde_estado, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_estado", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_estado), "NULL", Format(pet_hasta_estado, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_situacion", adChar, adParamInput, 8, ClearNull(pet_situacion))
        .Parameters.Append .CreateParameter("@pet_titulo", adChar, adParamInput, 59, ClearNull(pet_titulo))
        .Parameters.Append .CreateParameter("@sec_nivel", adChar, adParamInput, 4, ClearNull(sec_nivel))
        .Parameters.Append .CreateParameter("@sec_dire", adChar, adParamInput, 8, ClearNull(sec_dire))
        .Parameters.Append .CreateParameter("@sec_gere", adChar, adParamInput, 8, ClearNull(sec_gere))
        .Parameters.Append .CreateParameter("@sec_sect", adChar, adParamInput, 8, ClearNull(sec_sect))
        .Parameters.Append .CreateParameter("@sec_grup", adChar, adParamInput, 8, ClearNull(sec_grup))
        .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 250, ClearNull(sec_estado))
        .Parameters.Append .CreateParameter("@sec_situacion", adChar, adParamInput, 8, ClearNull(sec_situacion))
        .Parameters.Append .CreateParameter("@sec_desde_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_iplan), "NULL", Format(sec_desde_iplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_iplan), "NULL", Format(sec_hasta_iplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_fplan), "NULL", Format(sec_desde_fplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_fplan), "NULL", Format(sec_hasta_fplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_ireal), "NULL", Format(sec_desde_ireal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_ireal), "NULL", Format(sec_hasta_ireal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_freal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_freal), "NULL", Format(sec_desde_freal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_freal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_freal), "NULL", Format(sec_hasta_freal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
        .Parameters.Append .CreateParameter("@cod_importancia", adChar, adParamInput, 4, ClearNull(cod_importancia))
        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, ClearNull(cod_bpar))
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, IIf(cod_clase = "NULL", Null, cod_clase))
        .Parameters.Append .CreateParameter("@pet_imptech", adChar, adParamInput, 1, IIf(pet_imptech = "NULL", Null, pet_imptech))
        .Parameters.Append .CreateParameter("@pet_regulatorio", adChar, adParamInput, 1, IIf(pet_regulatorio = "NULL", Null, pet_regulatorio))     ' add -004- a.
        .Parameters.Append .CreateParameter("@pet_emp", adInteger, adParamInput, , IIf(pet_emp = "NULL", Null, pet_emp))
        .Parameters.Append .CreateParameter("@pet_ro", adChar, adParamInput, 1, IIf(pet_ro = "NULL", Null, pet_ro))
        '.Parameters.Append .CreateParameter("@prjid", adInteger, adParamInput, , IIf(prjid = "", Null, CLng(Val(prjid))))
        '.Parameters.Append .CreateParameter("@prjsubid", adInteger, adParamInput, , IIf(prjsubid = "", Null, CLng(Val(prjsubid))))
        '.Parameters.Append .CreateParameter("@prjsubsid", adInteger, adParamInput, , IIf(prjsubsid = "", Null, CLng(Val(prjsubsid))))
        .Parameters.Append .CreateParameter("@prjid", adInteger, adParamInput, , IIf(prjid = 0, Null, CLng(Val(prjid))))
        .Parameters.Append .CreateParameter("@prjsubid", adInteger, adParamInput, , IIf(prjsubid = 0, Null, CLng(Val(prjsubid))))
        .Parameters.Append .CreateParameter("@prjsubsid", adInteger, adParamInput, , IIf(prjsubsid = 0, Null, CLng(Val(prjsubsid))))
        '{ add -010- a.
        .Parameters.Append .CreateParameter("@cod_GBPE", adChar, adParamInput, 10, IIf(ClearNull(cod_bpe) = "NULL", Null, ClearNull(cod_bpe)))
        .Parameters.Append .CreateParameter("@fechaDesdeBPE", adChar, adParamInput, 8, IIf(IsNull(fechaBPEDesde) Or fechaBPEDesde = "", "NULL", Format(fechaBPEDesde, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@fechaHastaBPE", adChar, adParamInput, 8, IIf(IsNull(fechaBPEHasta) Or fechaBPEHasta = "", "NULL", Format(fechaBPEHasta, "yyyymmdd")))
        '}
        .Parameters.Append .CreateParameter("@categoria", adChar, adParamInput, 3, IIf(categoria = "NULL", Null, categoria))
        .Parameters.Append .CreateParameter("@pet_desde_alta", adChar, adParamInput, 8, IIf(IsNull(pet_desde_alta), "NULL", Format(pet_desde_alta, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_alta", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_alta), "NULL", Format(pet_hasta_alta, "yyyymmdd")))
        'Call DESA_MostrarLlamada(objCommand)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then sp_rptPeticion01 = True
    'sp_rptPeticion01 = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_rptPeticion01 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_rptPeticion02(pet_tipo, pet_estado, cod_importancia, pet_desde_alta, pet_hasta_alta, pet_desde_estado, pet_hasta_estado, pet_desde_iplan, pet_hasta_iplan, pet_desde_fplan, pet_hasta_fplan, pet_desde_plani, pet_hasta_plani, pet_desde_repla, pet_hasta_repla, pet_desde_ireal, pet_hasta_ireal, pet_desde_freal, pet_hasta_freal) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_rptPeticion02"
      'IIf(IsNull(pet_desde_alta), "NULL",Format(pet_desde_alta, "yyyymmdd"))
      .Parameters.Append .CreateParameter("@pet_tipo", adChar, adParamInput, 4, ClearNull(pet_tipo))
      .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 250, ClearNull(pet_estado))
      .Parameters.Append .CreateParameter("@cod_importancia", adChar, adParamInput, 4, ClearNull(cod_importancia))
      .Parameters.Append .CreateParameter("@pet_desde_alta", adChar, adParamInput, 8, IIf(IsNull(pet_desde_alta), "NULL", Format(pet_desde_alta, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_alta", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_alta), "NULL", Format(pet_hasta_alta, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_desde_estado", adChar, adParamInput, 8, IIf(IsNull(pet_desde_estado), "NULL", Format(pet_desde_estado, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_estado", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_estado), "NULL", Format(pet_hasta_estado, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_desde_iplan", adChar, adParamInput, 8, IIf(IsNull(pet_desde_iplan), "NULL", Format(pet_desde_iplan, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_iplan", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_iplan), "NULL", Format(pet_hasta_iplan, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_desde_fplan", adChar, adParamInput, 8, IIf(IsNull(pet_desde_fplan), "NULL", Format(pet_desde_fplan, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_fplan", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_fplan), "NULL", Format(pet_hasta_fplan, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_desde_plani", adChar, adParamInput, 8, IIf(IsNull(pet_desde_plani), "NULL", Format(pet_desde_plani, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_plani", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_plani), "NULL", Format(pet_hasta_plani, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_desde_repla", adChar, adParamInput, 8, IIf(IsNull(pet_desde_repla), "NULL", Format(pet_desde_repla, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_repla", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_repla), "NULL", Format(pet_hasta_repla, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_desde_ireal", adChar, adParamInput, 8, IIf(IsNull(pet_desde_ireal), "NULL", Format(pet_desde_ireal, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_ireal", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_ireal), "NULL", Format(pet_hasta_ireal, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_desde_freal", adChar, adParamInput, 8, IIf(IsNull(pet_desde_freal), "NULL", Format(pet_desde_freal, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@pet_hasta_freal", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_freal), "NULL", Format(pet_hasta_freal, "yyyymmdd")))
      Set aplRST = .Execute
    End With
    sp_rptPeticion02 = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_rptPeticion02 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_rptPeticion03(Ambito As Byte, pet_nrodesde, pet_nrohasta, pet_tipo, pet_prioridad, pet_desde_bpar, pet_hasta_bpar, pet_nivel, pet_area, pet_estado, pet_desde_estado, pet_hasta_estado, pet_situacion, pet_titulo, sec_nivel, sec_dire, sec_gere, sec_sect, sec_grup, sec_estado, sec_situacion, sec_desde_iplan, sec_hasta_iplan, sec_desde_fplan, sec_hasta_fplan, sec_desde_ireal, sec_hasta_ireal, sec_desde_freal, sec_hasta_freal, auxAgrup, cod_importancia, cod_bpar, _
                          cod_clase, pet_imptech, pet_regulatorio, pet_emp, pet_ro, prjid, prjsubid, prjsubsid, cod_bpe, fechaBPEDesde, fechaBPEHasta, categoria, pet_desde_alta, pet_hasta_alta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = IIf(Ambito > 0, "sp_rptPeticion03h", "sp_rptPeticion03")       ' add -006- a.
        .CommandTimeout = 0                                                           ' Se explicita el valor del timeout indefinidamente para evitar cancelaciones
        'IIf(IsNull(pet_desde_alta), "NULL",Format(pet_desde_alta, "yyyymmdd"))
        .Parameters.Append .CreateParameter("@pet_nrodesde", adInteger, adParamInput, , CLng(Val(pet_nrodesde)))
        .Parameters.Append .CreateParameter("@pet_nrohasta", adInteger, adParamInput, , CLng(Val(pet_nrohasta)))
        .Parameters.Append .CreateParameter("@pet_tipo", adChar, adParamInput, 4, ClearNull(pet_tipo))
        .Parameters.Append .CreateParameter("@pet_prioridad", adChar, adParamInput, 4, ClearNull(pet_prioridad))
        .Parameters.Append .CreateParameter("@pet_desde_bpar", adChar, adParamInput, 8, IIf(IsNull(pet_desde_bpar), "NULL", Format(pet_desde_bpar, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_bpar", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_bpar), "NULL", Format(pet_hasta_bpar, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_nivel", adChar, adParamInput, 4, ClearNull(pet_nivel))
        .Parameters.Append .CreateParameter("@pet_area", adChar, adParamInput, 8, ClearNull(pet_area))
        .Parameters.Append .CreateParameter("@pet_estado", adChar, adParamInput, 250, ClearNull(pet_estado))
        .Parameters.Append .CreateParameter("@pet_desde_estado", adChar, adParamInput, 8, IIf(IsNull(pet_desde_estado), "NULL", Format(pet_desde_estado, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_estado", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_estado), "NULL", Format(pet_hasta_estado, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_situacion", adChar, adParamInput, 8, ClearNull(pet_situacion))
        .Parameters.Append .CreateParameter("@pet_titulo", adChar, adParamInput, 59, ClearNull(pet_titulo))
        .Parameters.Append .CreateParameter("@sec_nivel", adChar, adParamInput, 4, ClearNull(sec_nivel))
        .Parameters.Append .CreateParameter("@sec_dire", adChar, adParamInput, 8, ClearNull(sec_dire))
        .Parameters.Append .CreateParameter("@sec_gere", adChar, adParamInput, 8, ClearNull(sec_gere))
        .Parameters.Append .CreateParameter("@sec_sect", adChar, adParamInput, 8, ClearNull(sec_sect))
        .Parameters.Append .CreateParameter("@sec_grup", adChar, adParamInput, 8, ClearNull(sec_grup))
        .Parameters.Append .CreateParameter("@sec_estado", adChar, adParamInput, 250, ClearNull(sec_estado))
        .Parameters.Append .CreateParameter("@sec_situacion", adChar, adParamInput, 8, ClearNull(sec_situacion))
        .Parameters.Append .CreateParameter("@sec_desde_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_iplan), "NULL", Format(sec_desde_iplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_iplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_iplan), "NULL", Format(sec_hasta_iplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_desde_fplan), "NULL", Format(sec_desde_fplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_fplan", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_fplan), "NULL", Format(sec_hasta_fplan, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_ireal), "NULL", Format(sec_desde_ireal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_ireal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_ireal), "NULL", Format(sec_hasta_ireal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_desde_freal", adChar, adParamInput, 8, IIf(IsNull(sec_desde_freal), "NULL", Format(sec_desde_freal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@sec_hasta_freal", adChar, adParamInput, 8, IIf(IsNull(sec_hasta_freal), "NULL", Format(sec_hasta_freal, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@agr_nrointerno", adInteger, adParamInput, , CLng(Val(auxAgrup)))
        .Parameters.Append .CreateParameter("@cod_importancia", adChar, adParamInput, 4, ClearNull(cod_importancia))
        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, ClearNull(cod_bpar))
        .Parameters.Append .CreateParameter("@cod_clase", adChar, adParamInput, 4, IIf(cod_clase = "TODO", "NULL", cod_clase))
        '.Parameters.Append .CreateParameter("@pet_imptech", adChar, adParamInput, 1, IIf(pet_imptech = "T", "T", pet_imptech))
        .Parameters.Append .CreateParameter("@pet_imptech", adChar, adParamInput, 1, IIf(pet_imptech = "NULL", Null, pet_imptech))
        .Parameters.Append .CreateParameter("@pet_regulatorio", adChar, adParamInput, 1, IIf(pet_regulatorio = "NULL", Null, pet_regulatorio))     ' add -004- a.
        '.Parameters.Append .CreateParameter("@pet_emp", adChar, adParamInput, 20, pet_emp)
        .Parameters.Append .CreateParameter("@pet_emp", adInteger, adParamInput, , IIf(pet_emp = "NULL", Null, pet_emp))
        .Parameters.Append .CreateParameter("@pet_ro", adChar, adParamInput, 1, IIf(pet_ro = "NULL", Null, pet_ro))
'        .Parameters.Append .CreateParameter("@prjid", adInteger, adParamInput, , IIf(prjid = "", Null, CLng(Val(prjid))))
'        .Parameters.Append .CreateParameter("@prjsubid", adInteger, adParamInput, , IIf(prjsubid = "", Null, CLng(Val(prjsubid))))
'        .Parameters.Append .CreateParameter("@prjsubsid", adInteger, adParamInput, , IIf(prjsubsid = "", Null, CLng(Val(prjsubsid))))
        .Parameters.Append .CreateParameter("@prjid", adInteger, adParamInput, , IIf(prjid = 0, Null, CLng(Val(prjid))))
        .Parameters.Append .CreateParameter("@prjsubid", adInteger, adParamInput, , IIf(prjsubid = 0, Null, CLng(Val(prjsubid))))
        .Parameters.Append .CreateParameter("@prjsubsid", adInteger, adParamInput, , IIf(prjsubsid = 0, Null, CLng(Val(prjsubsid))))
        '{ add -010- a.
        .Parameters.Append .CreateParameter("@cod_GBPE", adChar, adParamInput, 10, IIf(ClearNull(cod_bpe) = "NULL", Null, ClearNull(cod_bpe)))
        .Parameters.Append .CreateParameter("@fechaDesdeBPE", adChar, adParamInput, 8, IIf(IsNull(fechaBPEDesde) Or fechaBPEDesde = "", "NULL", Format(fechaBPEDesde, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@fechaHastaBPE", adChar, adParamInput, 8, IIf(IsNull(fechaBPEHasta) Or fechaBPEHasta = "", "NULL", Format(fechaBPEHasta, "yyyymmdd")))
        '}
        .Parameters.Append .CreateParameter("@categoria", adChar, adParamInput, 3, IIf(categoria = "NULL", Null, categoria))
        .Parameters.Append .CreateParameter("@pet_desde_alta", adChar, adParamInput, 8, IIf(IsNull(pet_desde_alta), "NULL", Format(pet_desde_alta, "yyyymmdd")))
        .Parameters.Append .CreateParameter("@pet_hasta_alta", adChar, adParamInput, 8, IIf(IsNull(pet_hasta_alta), "NULL", Format(pet_hasta_alta, "yyyymmdd")))
        Set aplRST = .Execute
    End With
    sp_rptPeticion03 = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_rptPeticion03 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_rptHsTrabAreaEjec(FechaDesde, FechaHasta, nivel, area, detalle) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_rptHsTrabAreaEjec"
      .Parameters.Append .CreateParameter("@fdesde", adChar, adParamInput, 8, IIf(IsNull(FechaDesde), "NULL", Format(FechaDesde, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@fhasta", adChar, adParamInput, 8, IIf(IsNull(FechaHasta), "NULL", Format(FechaHasta, "yyyymmdd")))
      .Parameters.Append .CreateParameter("@nivel", adChar, adParamInput, 4, ClearNull(nivel))
      .Parameters.Append .CreateParameter("@area", adChar, adParamInput, 8, ClearNull(area))
      .Parameters.Append .CreateParameter("@detalle", adInteger, adParamInput, , CLng(Val(detalle)))
      Set aplRST = .Execute
    End With
    sp_rptHsTrabAreaEjec = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_rptHsTrabAreaEjec = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -002- a.
'Function sp_GetHorasReales(agrup, Tipo, pet_nrointerno, pet_sect, pet_grup) As ADODB.Recordset
Function sp_GetHorasReales(agrup, pet_nrointerno, pet_sect, pet_grup) As ADODB.Recordset
    'On Error Resume Next
    On Error GoTo Err_SP
    Dim cnAux As ADODB.Connection
    Set cnAux = New ADODB.Connection
    
    cnAux.ConnectionString = aplCONN.ConnectionString: cnAux.Open
    
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = cnAux
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetHorasReales"
      .Parameters.Append .CreateParameter("@agrup", adChar, adParamInput, 3, agrup)
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
      .Parameters.Append .CreateParameter("@pet_sect", adChar, adParamInput, 8, pet_sect)
      .Parameters.Append .CreateParameter("@pet_grup", adChar, adParamInput, 8, pet_grup)
      Set sp_GetHorasReales = .Execute
      'Set sp_GetHorasReales = aplCONN.Execute(.CommandText)
    End With
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    Set cnAux = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -009- a.
Private Sub Mostrar_SQL(ByVal cSQL As String, ByRef oCmd As ADODB.Command)
    Dim x As Long
    
    With oCmd
        cSQL = cSQL & " "
        For x = 0 To .Parameters.Count - 1
            If IsNull(.Parameters(x).value) Then
                cSQL = cSQL & "NULL, "
            Else
                If .Parameters(x).Type = adChar Then
                    cSQL = cSQL & "'" & .Parameters(x).value & "', "
                Else
                    cSQL = cSQL & .Parameters(x).value & ", "
                End If
            End If
        Next x
        Debug.Print cSQL
    End With
End Sub
'}

'{ add -new-
'cSQL = "sp_rptPeticion00 "
'For i = 0 To .Parameters.Count - 1
'    If .Parameters(i).Type = adChar Then
'        If i = 0 Then
'            cSQL = cSQL & Trim(.Parameters(i).Value) & "'"
'        Else
'            If .Parameters(i).Name = "@pet_tipo" Then
'                cSQL = cSQL & ", " & Trim(.Parameters(i).Value)
'            Else
'                cSQL = cSQL & ", '" & Trim(.Parameters(i).Value) & "'"
'            End If
'        End If
'    ElseIf .Parameters(i).Type = adInteger Then
'        If i = 0 Then
'            cSQL = cSQL & .Parameters(i).Value
'        Else
'            cSQL = cSQL & ", " & .Parameters(i).Value
'        End If
'    End If
'Next i
'}

Function sp_InsertRptcab(rptid, rptnom, rpthab, rptfch) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertRptcab"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, rptid)
        .Parameters.Append .CreateParameter("@rptnom", adChar, adParamInput, 50, rptnom)
        .Parameters.Append .CreateParameter("@rpthab", adChar, adParamInput, 1, rpthab)
        .Parameters.Append .CreateParameter("@rptfch", SQLDdateType, adParamInput, , rptfch)
        Set aplRST = .Execute
    End With
    sp_InsertRptcab = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertRptcab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRptcab(rptid, rptnom, rpthab, rptfch) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateRptcab"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, rptid)
        .Parameters.Append .CreateParameter("@rptnom", adChar, adParamInput, 50, rptnom)
        .Parameters.Append .CreateParameter("@rpthab", adChar, adParamInput, 1, rpthab)
        .Parameters.Append .CreateParameter("@rptfch", SQLDdateType, adParamInput, , rptfch)
        Set aplRST = .Execute
    End With
    sp_UpdateRptcab = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateRptcab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteRptcab(rptid) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteRptcab"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, rptid)
        Set aplRST = .Execute
    End With
    sp_DeleteRptcab = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteRptcab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRptcab(rptid) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetRptcab"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, IIf(ClearNull(rptid) = "", Null, rptid))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetRptcab = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRptcab = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertRptdet(rptid, rptitem, rptnom, rpthab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertRptdet"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, rptid)
        .Parameters.Append .CreateParameter("@rptitem", adInteger, adParamInput, , rptitem)
        .Parameters.Append .CreateParameter("@rptnom", adChar, adParamInput, 100, rptnom)
        .Parameters.Append .CreateParameter("@rpthab", adChar, adParamInput, 1, rpthab)
        Set aplRST = .Execute
    End With
    sp_InsertRptdet = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertRptdet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateRptdet(rptid, rptitem, rptnom, rpthab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateRptdet"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, rptid)
        .Parameters.Append .CreateParameter("@rptitem", adInteger, adParamInput, , rptitem)
        .Parameters.Append .CreateParameter("@rptnom", adChar, adParamInput, 100, rptnom)
        .Parameters.Append .CreateParameter("@rpthab", adChar, adParamInput, 1, rpthab)
        Set aplRST = .Execute
    End With
    sp_UpdateRptdet = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateRptdet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteRptdet(rptid, rptitem) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteRptdet"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, rptid)
        .Parameters.Append .CreateParameter("@rptitem", adInteger, adParamInput, , rptitem)
        Set aplRST = .Execute
    End With
    sp_DeleteRptdet = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteRptdet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetRptdet(rptid) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetRptdet"
        .Parameters.Append .CreateParameter("@rptid", adChar, adParamInput, 10, IIf(ClearNull(rptid) = "", Null, rptid))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetRptdet = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetRptdet = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
