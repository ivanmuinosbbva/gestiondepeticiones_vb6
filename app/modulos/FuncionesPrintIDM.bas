Attribute VB_Name = "FuncionesPrintIDM"
Option Explicit

Dim nBegCol() As Long

Const MargenIzq = 20
Const OffX = 50
Const OffY = 40

Sub PrintFormularioIDM(pProjId, pProjSubid, pProjSubSId)
    Dim memDescripcion As String
    Dim memBeneficios As String
    Dim memNovedades As String
   
'    Dim txtAux As String
'    Dim sTitulo As String
'    Dim HalfWidth, HalfHeight, sTxt
'    Dim nAux As Integer
'    Dim aSector()
'    Dim hAux As Integer
'    Dim i As Integer
    
    'memDescripcion = sp_GetMemo(xNumeroPeticion, "DESCRIPCIO")
    'memBeneficios = sp_GetMemo(xNumeroPeticion, "BENEFICIOS")
    'memNovedades = sp_GetMemo(xNumeroPeticion, "MOTIVOS")
    
    Call Status("Imprimiendo formulario...")
    On Error GoTo ErrorHandler
    
    Call PrintCab
    Call PrintDetalle
    Call Status("Listo.")
Exit Sub
ErrorHandler:

End Sub

Private Sub PrintDetalle()
    Dim txtAux As String
    Dim sTitulo As String
    Dim HalfWidth, HalfHeight, sTxt
    Dim nAux As Integer
    Dim aSector()
    Dim hAux As Integer
    Dim i As Integer

    'If Not sp_GetProyectoIDMFicha(pProjId, pProjSubid, pProjSubSId) Then Exit Sub
    If Not sp_GetProyectoIDMFicha(1, 0, 0) Then Exit Sub
    If aplRST.EOF Then
        Exit Sub
    End If

    ReDim nBegCol(2)
    nBegCol(1) = 0
    nAux = ((frmPreview.ScaleWidth - MargenIzq) / 7)
    ReDim nBegCol(3)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 6
    frmPreview.FontSize = 10
    frmPreview.FontName = "Arial"
    frmPreview.FontBold = True
    frmPreview.FontItalic = True
    Call PrintText("T�tulo del proyecto", True, 1, 1, True)
    Call PrintText("C�digo", True, 1, 2, False)
    frmPreview.FontBold = True
    frmPreview.FontItalic = False
    Call PrintText(ClearNull(aplRST!ProjNom), True, 0, 1, True)
    Call PrintText(aplRST.Fields!projId & "." & aplRST.Fields!projSubId & "." & aplRST.Fields!projSubsId, True, 0, 2, False)
    frmPreview.FontBold = False
    frmPreview.FontSize = 9
    Call PrintText("Solicitante: " & ClearNull(aplRST!nom_areasoli), True, 0, 1, True)
    Call PrintText("Alta: " & Format(ClearNull(aplRST.Fields!ProjFchAlta), "dd/mm/yyyy"), True, 0, 2, False)
    Call PrintText("Propietario: " & ClearNull(aplRST!nom_areaprop), True, 0, 1, True)
    Call PrintText("Act.: " & Format(ClearNull(aplRST.Fields!fe_modif), "dd/mm/yyyy"), True, 0, 2, False)
    Call PrintText("", True, 0, 1, True)
    Call PrintText("", True, 0, 2, False)
    Skip

'    ReDim nBegCol(2)
'    nBegCol(1) = 0
'    frmPreview.FontSize = 10
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Area Solicitante", True, 2, 1, True)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    Call PrintText(ClearNull(aplRST!nom_direccion) & " >> " & ClearNull(aplRST!nom_gerencia) & " >> " & ClearNull(aplRST!nom_sector), True, 0, 1, True)
'
'    ReDim nBegCol(2)
'    nBegCol(1) = 0
'    frmPreview.FontSize = 10
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Business Partner", True, 2, 1, True)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    Call PrintText(ClearNull(aplRST!nom_bpar), True, 0, 1, True)
'
'
'    nAux = ((frmPreview.ScaleWidth - MargenIzq) / 7)
'    ReDim nBegCol(4)
'    nBegCol(1) = 0
'    nBegCol(2) = nAux * 3
'    nBegCol(3) = nBegCol(2) + nAux * 3
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Solicitante", True, 1, 1, True)
'    Call PrintText("Supervisor", True, 1, 2, False)
'    Call PrintText("F.Pedido", True, 1, 3, False)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    Call PrintText(ClearNull(aplRST!nom_solicitante), True, 0, 1, True)
'    Call PrintText(ClearNull(aplRST!nom_supervisor), True, 0, 2, False)
'    Call PrintText(IIf(Not IsNull(aplRST!fe_pedido), Format(aplRST!fe_pedido, "dd/mm/yyyy"), ""), True, 0, 3, False)
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Referente", True, 1, 1, True)
'    Call PrintText("Autorizante", True, 1, 2, False)
'    Call PrintText("Fecha deseada", True, 1, 3, False)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    Call PrintText(ClearNull(aplRST!nom_referente), True, 0, 1, True)
'    Call PrintText(ClearNull(aplRST!nom_director), True, 0, 2, False)
'    Call PrintText(IIf(Not IsNull(aplRST!fe_requerida), Format(aplRST!fe_requerida, "dd/mm/yyyy"), ""), True, 0, 3, False)
'    Skip
'
'    ReDim nBegCol(2)
'    nBegCol(1) = 0
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Areas Comprometidas", True, 2, 1, True)
'
'    nAux = ((frmPreview.ScaleWidth - MargenIzq) / 20)
'    ReDim nBegCol(8)
'    nBegCol(1) = 0
'    nBegCol(2) = nAux * 9
'    nBegCol(3) = nBegCol(2) + nAux * 2
'    nBegCol(4) = nBegCol(3) + nAux * 2
'    nBegCol(5) = nBegCol(4) + nAux * 1
'    nBegCol(6) = nBegCol(5) + nAux * 4
'    nBegCol(7) = nBegCol(6) + nAux * 2
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    frmPreview.FontSize = 8
'    Call PrintText("Sector", True, 1, 1, True)
'    Call PrintText("F.Inicio Plan.", True, 1, 2, False)
'    Call PrintText("F.Fin Plan.", True, 1, 3, False)
'    Call PrintText("Horas", True, 1, 4, False)
'    Call PrintText("Estado", True, 1, 5, False)
'
'    ReDim aSector(1 To 1)
'    hAux = 0
'
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    If sp_GetPeticionSectorXt(xNumeroPeticion, Null) Then
'        Do While Not aplRST.EOF
'            hAux = hAux + 1
'            ReDim Preserve aSector(1 To nAux)
'            aSector(hAux) = ClearNull(aplRST!cod_sector)
'            aplRST.MoveNext
'        Loop
'        aplRST.Close
'    End If
'
'    For i = 1 To hAux
'        If sp_GetPeticionSectorXt(xNumeroPeticion, aSector(i)) Then
'            Call PrintText(ClearNull(aplRST!nom_gerencia) & " : " & ClearNull(aplRST!nom_sector), True, 0, 1, True)
'            Call PrintText(IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), ""), True, 0, 2, False)
'            Call PrintText(IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), ""), True, 0, 3, False)
'            Call PrintText(ClearNull(aplRST!horaspresup), True, 0, 4, False)
'            Call PrintText(ClearNull(aplRST!nom_estado), True, 0, 5, False)
'            Call PrintText(ClearNull(aplRST!nom_situacion), True, 0, 6, False)
'            aplRST.Close
'            If sp_GetPeticionGrupoXt(xNumeroPeticion, aSector(i), Null) Then
'                Do While Not aplRST.EOF
'                    Call PrintText("   " & ClearNull(aplRST!nom_grupo), True, 0, 1, True)
'                    Call PrintText(IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), ""), True, 0, 2, False)
'                    Call PrintText(IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), ""), True, 0, 3, False)
'                    Call PrintText(ClearNull(aplRST!horaspresup), True, 0, 4, False)
'                    Call PrintText(ClearNull(aplRST!nom_estado), True, 0, 5, False)
'                    Call PrintText(" ", True, 0, 6, False)
'                    aplRST.MoveNext
'                Loop
'                aplRST.Close
'            End If
'        End If
'    Next i
'
'
'    Skip
'
'
'    ReDim nBegCol(2)
'    nBegCol(1) = 0
'    frmPreview.FontSize = 9
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Descripci�n del Pedido", True, 1, 1, True)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
'    OutTab memDescripcion
'    Call PrintText(memDescripcion, False, 0, 1, True, True)
'    Skip
'
'    frmPreview.FontSize = 9
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Caracter�sticas Principales", True, 1, 2, True)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
'    OutTab memCaracteristicas
'    Call PrintText(memCaracteristicas, False, 0, 1, True, True)
'    Skip
'
'    frmPreview.FontSize = 9
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Motivo del Requerimiento", True, 1, 2, True)
'    frmPreview.FontItalic = False
'    frmPreview.FontBold = False
'    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
'    OutTab memMotivos
'    Call PrintText(memMotivos, False, 0, 1, True, True)
'    Skip
'
'    frmPreview.FontSize = 9
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Beneficios Esperados", True, 1, 2, True)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
'    OutTab memBeneficios
'    Call PrintText(memBeneficios, False, 0, 1, True, True)
'    Skip
'
'    frmPreview.FontSize = 9
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Relaci�n con otros Requerimientos", True, 1, 2, True)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
'    OutTab memRelaciones
'    Call PrintText(memRelaciones, False, 0, 1, True, True)
'    Skip
'
'    frmPreview.FontSize = 9
'    frmPreview.FontBold = True
'    frmPreview.FontItalic = True
'    Call PrintText("Observaciones", True, 1, 2, True)
'    frmPreview.FontBold = False
'    frmPreview.FontItalic = False
'    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
'    OutTab memObservaciones
'    Call PrintText(memObservaciones, False, 0, 1, True, True)
'    Skip
'
'    PrintPie (False)
'    frmPreview.EndDoc
'    Call Status("Listo.")
Exit Sub
ErrorHandler:
    On Error GoTo 0
    MsgBox "Error con la impresora."
    'frmPreview.EndDoc
    Exit Sub
End Sub

Private Sub PrintCab()
    Dim nAuxY
    Dim nAux As Integer
    Dim sTxt As String
    Dim xFntName As String
    Dim xFntBold As Boolean
    Dim xFntItalic As Boolean
    Dim xFntSize As Integer
    Dim BegY As Long
    Dim EndY As Long
    
    xFntName = frmPreview.FontName
    xFntBold = frmPreview.FontBold
    xFntItalic = frmPreview.FontItalic
    xFntSize = frmPreview.FontSize
    
    frmPreview.CurrentX = 0
    frmPreview.CurrentY = 0
    Skip
    Skip
    Skip
    
    nAuxY = frmPreview.CurrentY
    nAux = ((frmPreview.ScaleWidth - MargenIzq) / 10)
    ReDim nBegCol(2)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 7
    
    frmPreview.Cls                              ' Limpia el formulario
    'frmPreview.Orientation = cdlLandscape      ' Apaisada
    frmPreview.FontName = "Times New Roman"
    frmPreview.FontBold = True
    frmPreview.FontItalic = False
    frmPreview.FontSize = 10
    frmPreview.Print glHEADEMPPRESA
    
    frmPreview.CurrentY = nAuxY
    frmPreview.FontBold = False
    frmPreview.FontItalic = False
    frmPreview.FontSize = 8
    sTxt = glHEADAPLICACION
    frmPreview.CurrentX = (frmPreview.ScaleWidth - MargenIzq) - frmPreview.TextWidth(sTxt) - 40
    frmPreview.Print sTxt
    
    sTxt = "DIRECCION DE MEDIOS - PROYECTOS IDM"
    frmPreview.FontSize = 10
    frmPreview.FontBold = True
    frmPreview.CurrentX = (frmPreview.ScaleWidth - MargenIzq) / 2 - frmPreview.TextWidth(sTxt) / 2
    frmPreview.Print sTxt
    Skip
    Skip
    frmPreview.Line (0, frmPreview.CurrentY)-((frmPreview.ScaleWidth - MargenIzq), frmPreview.CurrentY)
    Skip
    frmPreview.Print
    BegY = frmPreview.CurrentY
    
    frmPreview.FontName = xFntName
    frmPreview.FontBold = xFntBold
    frmPreview.FontItalic = xFntItalic
    frmPreview.FontSize = xFntSize
End Sub

Private Sub PrintText(ByVal LongText As String, ByVal Boxed As Boolean, ByVal nShadow As Integer, ByVal nCol As Integer, ByVal NewLine As Boolean, Optional wrdWrap)
    Dim BegY As Long
    Dim EndY As Long
    Static LastY As Long
    Dim Word As String
    Dim EdgeX As Long
    
    If IsMissing(wrdWrap) Then
        wrdWrap = False
    End If
    'wrdWrap = True


    If NewLine = False Then
        frmPreview.CurrentY = LastY
    End If
    BegY = frmPreview.CurrentY

    If nCol = UBound(nBegCol) Then
        EdgeX = frmPreview.ScaleWidth
    ElseIf nBegCol(nCol + 1) = 0 Then
        EdgeX = frmPreview.ScaleWidth
    Else
        EdgeX = nBegCol(nCol + 1)
    End If
    
    frmPreview.CurrentX = nBegCol(nCol) + OffX
    frmPreview.CurrentY = BegY + OffY

    If Boxed = True Then
        PrintLongText LongText, EdgeX, wrdWrap, True
        EndY = frmPreview.CurrentY + OffY
        Select Case nShadow
        Case 0
            frmPreview.DrawWidth = 2
            frmPreview.FillStyle = 1
            frmPreview.FillColor = RGB(255, 255, 255)
        Case 1
            frmPreview.DrawWidth = 10
            frmPreview.FillStyle = 0
            frmPreview.FillColor = RGB(192, 192, 192)
        Case 2
            frmPreview.DrawWidth = 15
            frmPreview.FillStyle = 0
            frmPreview.FillColor = RGB(127, 127, 127)
        End Select
        frmPreview.Line (nBegCol(nCol), BegY)-((frmPreview.ScaleWidth - MargenIzq), EndY), , B
        frmPreview.CurrentX = nBegCol(nCol) + OffX
        frmPreview.CurrentY = BegY + OffY
    End If


    PrintLongText LongText, EdgeX, wrdWrap, False

    EndY = frmPreview.CurrentY + OffY

    frmPreview.FillStyle = 1
    frmPreview.CurrentY = EndY
    LastY = BegY
    frmPreview.CurrentX = 0
End Sub

Private Function PrintLongText(ByVal LongText As String, ByVal EdgeX As Long, ByVal wrdWrap As Boolean, ByVal bFlgTransp As Boolean) As Boolean
    Dim BegY As Long
    Dim EndY As Long
    Dim i As Integer
    Dim Word As String
    Dim auxCurrentY As Integer
    Dim maxCurrentY As Integer
    If bFlgTransp = True Then
        frmPreview.ForeColor = QBColor(7)
    End If
    PrintLongText = True
    If wrdWrap = True Then
        Do Until LongText = ""
              Word = Token(LongText, " ")
              If frmPreview.TextWidth(Word) + frmPreview.CurrentX > (EdgeX - MargenIzq) - frmPreview.TextWidth("Z") Then
                'hacer el corte de renglon
                 frmPreview.Print
                 'frmPreview.CurrentX = OffX
              End If
              If frmPreview.CurrentY + (frmPreview.TextHeight("ZZZ") * 4) + 300 >= frmPreview.ScaleHeight Then
                'hacer el corte de pagina
                 PrintPie (True)
                 PrintCab
                 'frmPreview.CurrentX = OffX
              End If
              frmPreview.Print Word + " ";
        Loop
    Else
        Word = LongText
        i = Len(Word)
        Do Until i = 0
              Word = Mid(LongText, 1, i)
              If frmPreview.TextWidth(Word) + frmPreview.CurrentX > (EdgeX - MargenIzq) - frmPreview.TextWidth("I") Then
                 i = i - 1
              Else
                 frmPreview.Print Word;
                 i = 0
              End If
        Loop
    End If
    frmPreview.Print
    frmPreview.ForeColor = QBColor(0)
End Function

Private Sub PrintPie(bFlgPage As Boolean)
    Dim nAux As Integer
    Dim xFntName As String
    Dim xFntBold As Boolean
    Dim xFntItalic As Boolean
    Dim xFntSize As Integer
    
    xFntName = frmPreview.FontName
    xFntBold = frmPreview.FontBold
    xFntItalic = frmPreview.FontItalic
    xFntSize = frmPreview.FontSize
    
    nAux = ((frmPreview.ScaleWidth - MargenIzq) / 10)
    
    frmPreview.CurrentX = 0
    frmPreview.CurrentY = frmPreview.ScaleHeight - frmPreview.TextHeight("ZZZ") - 10
    
    frmPreview.FontName = "Times New Roman"
    frmPreview.FontBold = False
    frmPreview.FontItalic = False
    frmPreview.FontSize = 8
    frmPreview.Print "Fecha: " & Format(date, "dd/mm/yyyy")
    
    'frmPreview.CurrentX = (frmPreview.ScaleWidth - MargenIzq) - frmPreview.TextWidth("Hoja: " & frmPreview.Page) - 10
    frmPreview.CurrentY = frmPreview.ScaleHeight - frmPreview.TextHeight("ZZZ") - 10
    'frmPreview.Print "Hoja: " & frmPreview.Page
    
    frmPreview.FontName = xFntName
    frmPreview.FontBold = xFntBold
    frmPreview.FontItalic = xFntItalic
    frmPreview.FontSize = xFntSize
    
'    If bFlgPage = True Then
'        frmPreview.NewPage
'    End If
End Sub

Private Function Token(tmp$, search$) As String
    Dim x As Integer
    x = InStr(1, tmp$, search$)
    If x Then
       Token = Mid$(tmp$, 1, x - 1)
       tmp$ = Mid$(tmp$, x + 1)
    Else
       Token = tmp$
       tmp$ = ""
    End If
End Function

Private Sub Skip()
    frmPreview.CurrentY = frmPreview.CurrentY + 100
End Sub
