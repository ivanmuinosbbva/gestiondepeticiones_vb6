Attribute VB_Name = "FuncionesDSN"
Option Explicit
Private Const ODBC_ADD_DSN = 1
Private Const ODBC_CONFIG_DSN = 2
Private Const ODBC_REMOVE_DSN = 3
Private Const vbAPINull As Long = 0
Private Declare Function SQLConfigDataSource Lib "ODBCCP32.DLL" (ByVal hwndParent As Long, ByVal fRequest As Long, ByVal lpszDriver As String, ByVal lpszAttributes As String) As Long

Function CrearDSN(DRIVER, SERVER, DESCRIPTION, DSN, DATABASE) As Boolean
    Dim intRet As Long
    Dim strAttributes As String
    strAttributes = ""

    strAttributes = strAttributes & "DSN=" & DSN & Chr$(0)

    intRet = SQLConfigDataSource(vbAPINull, ODBC_REMOVE_DSN, "SQL Server", strAttributes)
    intRet = SQLConfigDataSource(vbAPINull, ODBC_REMOVE_DSN, "Sybase System 11", strAttributes)
    intRet = SQLConfigDataSource(vbAPINull, ODBC_REMOVE_DSN, "Sybase System 12", strAttributes)
    
    'MULTIPLATAFORMING PARA CREAR DSN
    If InStr(UCase(DRIVER), "SYBASE") > 0 Then
        strAttributes = strAttributes & "SRVR=" & SERVER & Chr$(0)
    ElseIf InStr(UCase(DRIVER), "SQL SERVER") > 0 Then
        strAttributes = strAttributes & "SERVER=" & SERVER & Chr$(0)
    Else
        MsgBox ("Invalid driver - CrearDSN")
        CrearDSN = False
        Exit Function
    End If

    strAttributes = strAttributes & "DESCRIPTION=" & DESCRIPTION & Chr$(0)
    strAttributes = strAttributes & "DATABASE=" & DATABASE & Chr$(0)
    intRet = SQLConfigDataSource(vbAPINull, ODBC_ADD_DSN, DRIVER, strAttributes)
    CrearDSN = intRet
End Function
