Attribute VB_Name = "spPeticionPlanificar"
Option Explicit

'Function sp_GetPetPlanificar(per_nrointerno, cod_bpe, cod_bpar, Situacion, Estado, cod_grupo, priorizadas) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPetPlanificar"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(IsNull(per_nrointerno), Null, per_nrointerno))
'        .Parameters.Append .CreateParameter("@cod_bpe", adChar, adParamInput, 10, IIf(cod_bpe = "NULL", Null, cod_bpe))
'        .Parameters.Append .CreateParameter("@cod_bpa", adChar, adParamInput, 10, IIf(cod_bpar = "NULL", Null, cod_bpar))
'        .Parameters.Append .CreateParameter("@cod_bpa", adChar, adParamInput, 4, IIf(Situacion = "NULL", Null, Situacion))
'        .Parameters.Append .CreateParameter("@estado", adChar, adParamInput, 4, IIf(Estado = "NULL", Null, Estado))
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, IIf(cod_grupo = "NULL", Null, cod_grupo))
'        .Parameters.Append .CreateParameter("@priorizadas", adChar, adParamInput, 1, IIf(priorizadas = "NULL", Null, priorizadas))
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPetPlanificar = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPetPlanificar = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_GetPetPlanificarPrioridad(per_nrointerno) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPetPlanificarPrioridad"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(IsNull(per_nrointerno), Null, per_nrointerno))
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPetPlanificarPrioridad = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPetPlanificarPrioridad = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_GetPetPlanificarBP_Prior(cod_bpar, per_nrointerno, prioridad) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPetPlanificarBP1"
'        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = "NULL", Null, CLng(Val(per_nrointerno))))
'        .Parameters.Append .CreateParameter("@prioridad", adChar, adParamInput, 1, IIf(prioridad = "NULL", Null, prioridad))
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPetPlanificarBP_Prior = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPetPlanificarBP_Prior = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_GetPetPlanificarBP_Orden(cod_bpar, cod_grupo, per_nrointerno, prioridad) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPetPlanificarBP2"
'        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, IIf(cod_grupo = "NULL", Null, cod_grupo))
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = "NULL", Null, CLng(Val(per_nrointerno))))
'        .Parameters.Append .CreateParameter("@prioridad", adChar, adParamInput, 1, IIf(prioridad = "NULL", Null, prioridad))
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPetPlanificarBP_Orden = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPetPlanificarBP_Orden = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_GetPetPlanificarPRJ(cod_bpar, cod_grupo, per_nrointerno, prioridad) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPetPlanificarPRJ"
'        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, IIf(cod_bpar = "NULL", Null, cod_bpar))
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, IIf(cod_grupo = "NULL", Null, cod_grupo))
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = "NULL", Null, CLng(Val(per_nrointerno))))
'        .Parameters.Append .CreateParameter("@prioridad", adChar, adParamInput, 1, IIf(prioridad = "NULL", Null, prioridad))
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPetPlanificarPRJ = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPetPlanificarPRJ = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_GetPetPlanificarDYD(per_nrointerno, cod_grupo, cod_sector, prioridad, cod_bpar) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPetPlanificarDYD"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = "NULL", Null, CLng(Val(per_nrointerno))))
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
'        .Parameters.Append .CreateParameter("@prioridad", adChar, adParamInput, 1, IIf(prioridad = "NULL", Null, prioridad))
'        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, IIf(cod_bpar = "NULL", Null, cod_bpar))
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPetPlanificarDYD = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPetPlanificarDYD = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_UpdatePeticionPlancab2(per_nrointerno, pet_nrointerno, modo) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_UpdatePeticionPlancab2"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'        .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 10, modo)
'        Set aplRST = .Execute
'    End With
'    sp_UpdatePeticionPlancab2 = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err))
'    sp_UpdatePeticionPlancab2 = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_UpdatePeticionPlancab3(per_nrointerno, pet_nrointerno, cod_grupo) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_UpdatePeticionPlancab3"
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , CLng(Val(per_nrointerno)))
'        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'        Set aplRST = .Execute
'    End With
'    sp_UpdatePeticionPlancab3 = True
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err))
'    sp_UpdatePeticionPlancab3 = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
'
'Function sp_GetPetPlanificarBP_Agregar(cod_bpar, per_nrointerno) As Boolean
'    On Error Resume Next
'    aplRST.Close
'    Set aplRST = Nothing
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    With objCommand
'        .ActiveConnection = aplCONN
'        .CommandType = adCmdStoredProc
'        .CommandText = "sp_GetPetPlanificarBP0"
'        .Parameters.Append .CreateParameter("@cod_bpar", adChar, adParamInput, 10, cod_bpar)
'        .Parameters.Append .CreateParameter("@per_nrointerno", adInteger, adParamInput, , IIf(per_nrointerno = "NULL", Null, CLng(Val(per_nrointerno))))
'        Set aplRST = .Execute
'    End With
'    If Not (aplRST.EOF) Then
'        sp_GetPetPlanificarBP_Agregar = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'    sp_GetPetPlanificarBP_Agregar = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
