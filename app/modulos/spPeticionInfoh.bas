Attribute VB_Name = "spPeticionInformesHomologacion"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Public Function sp_GetPeticionInformes(pet_nrointerno, cod_estado, cod_recurso, fecha_desde, fecha_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionInformes"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@cod_estado", adVarChar, adParamInput, 100, IIf(cod_estado = "NULL", Null, cod_estado))
        .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, IIf(cod_recurso = "NULL", Null, cod_recurso))
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fecha_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fecha_hasta)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionInformes = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionInformes = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Public Function AgregarInformeManual(pet_nrointerno, cod_recurso, prot_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionInformeManual"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , prot_id)
        Set aplRST = .Execute
    End With
    AgregarInformeManual = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    AgregarInformeManual = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertPeticionInfohc(pet_nrointerno, info_id, info_tipo, info_fecha, info_recurso, info_estado, infoprot_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionInfohc"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_tipo", adChar, adParamInput, 3, info_tipo)
        .Parameters.Append .CreateParameter("@info_fecha", SQLDdateType, adParamInput, , info_fecha)
        .Parameters.Append .CreateParameter("@info_recurso", adChar, adParamInput, 10, info_recurso)
        .Parameters.Append .CreateParameter("@info_estado", adChar, adParamInput, 6, info_estado)
        .Parameters.Append .CreateParameter("@infoprot_id", adInteger, adParamInput, , infoprot_id)
        Set aplRST = .Execute
    End With
    sp_InsertPeticionInfohc = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionInfohc = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionInfohc(pet_nrointerno, info_id, info_idver, info_tipo, info_punto, info_recurso, info_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionInfohc"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , info_idver)
        .Parameters.Append .CreateParameter("@info_tipo", adChar, adParamInput, 3, info_tipo)
        .Parameters.Append .CreateParameter("@info_punto", adInteger, adParamInput, , Val(info_punto))
        .Parameters.Append .CreateParameter("@info_recurso", adChar, adParamInput, 10, info_recurso)
        .Parameters.Append .CreateParameter("@info_estado", adChar, adParamInput, 6, info_estado)
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionInfohc = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionInfohc = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionInfohcField(pet_nrointerno, info_id, info_idver, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionInfohcField"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , IIf(IsNull(info_idver), 0, info_idver))
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionInfohcField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionInfohcField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionInformes(pet_nrointerno, info_id, info_idver) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionInforme"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , IIf(IsNull(info_id), Null, info_id))
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , IIf(IsNull(info_idver), Null, info_idver))
        Set aplRST = .Execute
    End With
    sp_DeletePeticionInformes = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionInformes = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionInfohc(pet_nrointerno, info_id, info_idver) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionInfohc"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , IIf(IsNull(info_idver), Null, info_idver))
        Set aplRST = .Execute
    End With
    sp_DeletePeticionInfohc = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionInfohc = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionInfohc(pet_nrointerno, info_id, info_idver, info_tipo, info_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionInfohc"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , info_idver)
        .Parameters.Append .CreateParameter("@info_tipo", adChar, adParamInput, 1, Null)
        .Parameters.Append .CreateParameter("@info_estado", adChar, adParamInput, 80, IIf(info_estado = "", Null, info_estado))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionInfohc = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionInfohc = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertPeticionInfohd(pet_nrointerno, info_id, info_idver, info_item, info_valor, info_orden, info_comen) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionInfohd"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , info_idver)
        .Parameters.Append .CreateParameter("@info_item", adInteger, adParamInput, , info_item)
        .Parameters.Append .CreateParameter("@info_valor", adInteger, adParamInput, , info_valor)
        .Parameters.Append .CreateParameter("@info_orden", adInteger, adParamInput, , info_orden)
        .Parameters.Append .CreateParameter("@info_comen", adVarChar, adParamInput, 255, Mid(info_comen, 1, 255))
        Set aplRST = .Execute
    End With
    sp_InsertPeticionInfohd = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionInfohd = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionInfohd(pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionInfohd"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , info_idver)
        .Parameters.Append .CreateParameter("@info_item", adInteger, adParamInput, , info_item)
        .Parameters.Append .CreateParameter("@info_estado", adChar, adParamInput, 1, IIf(info_estado = "", Null, info_estado))
        .Parameters.Append .CreateParameter("@info_valor", adInteger, adParamInput, , IIf(info_valor = "", Null, info_valor))
        .Parameters.Append .CreateParameter("@info_orden", adInteger, adParamInput, , info_orden)
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionInfohd = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionInfohd = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionInfohdField(pet_nrointerno, info_id, info_item, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionInfohdField"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_item", adInteger, adParamInput, , info_item)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionInfohdField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionInfohdField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionInfohd(pet_nrointerno, info_id, info_item) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionInfohd"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_item", adInteger, adParamInput, , info_item)
        Set aplRST = .Execute
    End With
    sp_DeletePeticionInfohd = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionInfohd = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionInfohd(pet_nrointerno, info_id, info_idver, info_req) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionInfohd"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , info_idver)
        .Parameters.Append .CreateParameter("@info_req", adChar, adParamInput, 1, IIf(info_req = "S", "S", Null))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionInfohd = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionInfohd = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionInfohr(pet_nrointerno, info_id, info_idver) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionInfohr"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , IIf(IsNull(info_idver), 0, info_idver))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionInfohr = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionInfohr = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionInfohrm(pet_nrointerno, info_id, info_idver, cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionInfohrm"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , IIf(IsNull(info_idver), 0, info_idver))
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionInfohrm = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionInfohrm = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionInfohrField(pet_nrointerno, info_id, info_idver, info_item, cod_recurso, campo, valortxt, valordate, valornum) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionInfohrField"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , IIf(IsNull(info_idver) Or info_idver = "", 0, info_idver))
        .Parameters.Append .CreateParameter("@info_item", adInteger, adParamInput, , info_item)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
        .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 255, Mid(valortxt, 1, 255))
        .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
        .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionInfohrField = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionInfohrField = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertPeticionInfohr(pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionInfohr"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , info_idver)
        .Parameters.Append .CreateParameter("@info_item", adInteger, adParamInput, , info_item)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
        .Parameters.Append .CreateParameter("@cod_nivel", adChar, adParamInput, 4, cod_nivel)
        .Parameters.Append .CreateParameter("@cod_area", adChar, adParamInput, 8, cod_area)
        .Parameters.Append .CreateParameter("@info_valor", adInteger, adParamInput, , info_valor)
        .Parameters.Append .CreateParameter("@info_comen", adChar, adParamInput, 255, info_comen)
        Set aplRST = .Execute
    End With
    sp_InsertPeticionInfohr = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionInfohr = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionInfohr(pet_nrointerno, info_id, info_idver, info_item, cod_recurso) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionInfohr"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@info_id", adInteger, adParamInput, , info_id)
        .Parameters.Append .CreateParameter("@info_idver", adInteger, adParamInput, , info_idver)
        .Parameters.Append .CreateParameter("@info_item", adInteger, adParamInput, , info_item)
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        Set aplRST = .Execute
    End With
    sp_DeletePeticionInfohr = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionInfohr = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionResponsables(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionInfoResp"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionResponsables = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionResponsables = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionInfohMemo(pet_nrointerno, pet_infoid, pet_infover, sTexto) As Boolean
    Dim bFlg As Boolean
    Dim Secuencia As Integer
    Dim TmpTexto As String
    Dim Resto As Integer
    Dim rsAux As ADODB.Recordset
    Dim dFechaHora As Date
    
    Set rsAux = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdatePeticionInfoht"
      .Parameters.Append .CreateParameter("@borra_last", adChar, adParamInput, 1)
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput)
      .Parameters.Append .CreateParameter("@pet_infoid", adInteger, adParamInput)
      .Parameters.Append .CreateParameter("@pet_infoidver", adInteger, adParamInput)
      .Parameters.Append .CreateParameter("@mem_secuencia", adSmallInt, adParamInput)
      .Parameters.Append .CreateParameter("@mem_texto", adChar, adParamInput, 255)
    End With
    
    sp_UpdatePeticionInfohMemo = True
    bFlg = True
    Secuencia = 0
    Do While bFlg = True
        TmpTexto = Left(sTexto, 255)
        With objCommand
            '.Parameters(0).Value = IIf(Len(sTexto) > 255, "N", "S")
            If Len(sTexto) > 255 Then
                .Parameters(0).Value = "N"
            Else
                bFlg = False
                .Parameters(0).Value = "S"
            End If
            .Parameters(1).Value = CLng(Val(pet_nrointerno))
            .Parameters(2).Value = CLng(Val(pet_infoid))
            .Parameters(3).Value = CLng(Val(pet_infover))
            .Parameters(4).Value = CInt(Secuencia)
            .Parameters(5).Value = TmpTexto & ""
            Set rsAux = .Execute
        End With
        Secuencia = Secuencia + 1
        sTexto = Mid(sTexto, 256)
    Loop
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(rsAux, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_UpdatePeticionInfohMemo = False
End Function

Function sp_GetPeticionInfohMemo(pet_nrointerno, pet_infoid, pet_infover) As String
    Dim bGetMemo As Boolean
    Dim sGetMemo As String
    sGetMemo = ""
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionInfoht"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
      .Parameters.Append .CreateParameter("@pet_infoid", adInteger, adParamInput, , pet_infoid)
      .Parameters.Append .CreateParameter("@pet_infover", adInteger, adParamInput, , pet_infover)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        bGetMemo = True
    End If
    
    If bGetMemo Then
        Do While Not aplRST.EOF
            'sGetMemo = sGetMemo & ClearNull(aplRST(0))
            sGetMemo = sGetMemo & aplRST.Fields!info_renglontexto
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
    sp_GetPeticionInfohMemo = sGetMemo
Exit Function

Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    sp_GetPeticionInfohMemo = ""
End Function
