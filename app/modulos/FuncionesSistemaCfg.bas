Attribute VB_Name = "FuncionesSistema"
' -001- a. FJS 13.06.2016 - Nuevo: men� para administrar las tablas espec�ficas para BPE.
' -002- GMT01  28.10.2019 - Nuevo Opcion para cargar tips

Option Explicit

Global glb_bMultiSelectModo As Boolean

Global m_strFind As String
Global m_lastFind As Integer
Global m_lastCOL As Integer
Global m_lastROW As Integer
'{ add -new-!!!
Global gdFechaIngreso As Date
Global gdHoraIngreso As String
Global gcComputerName As String
'}

Public Enum vbObjectType
    vbObjType_Unknow = 0
    vbObjType_Form = 1
    vbObjType_MDIForm = 2
    vbObjType_Module = 3
    vbObjType_ClassModule = 4
    vbObjType_UserControl = 5
    vbObjType_PropertyPage = 6
    vbObjType_UserDocument = 7
End Enum

Public Enum vbFormMode
    vbFormModeInsert = 1
    vbFormModeUpdate = 2
    vbFormModeDelete = 3
    vbFormModeView = 4
End Enum

Public Enum prmColorFilaGrilla
    prmGridFillRowColorGreen = 1
    prmGridFillRowColorBlue = 2
    prmGridFillRowColorRose = 3
    prmGridFillRowColorDarkGreen = 4
    prmGridFillRowColorRed = 5
    prmGridFillRowColorYellow = 6
    prmGridFillRowColorLightBlue = 7
    prmGridFillRowColorLightOrange = 8
    prmGridFillRowColorLightGrey = 9
    prmGridFillRowColorDarkGrey = 10
    prmGridFillRowColorBlueLink = 11
    prmGridFillRowColorLightBlue2 = 12
    prmGridFillRowColorDarkGreen2 = 13
    prmGridFillRowColorLightGreen = 14  ' rgb(217,244,224)
    prmGridFillRowColorDarkRed = 20     ' rgb()
    prmGridFillRowColorWhite = 21       ' rgb()
    prmGridFillRowColorBlack = 22       ' rgb()
    prmGridFillRowColorDarkBlue = 23
    prmGridFillRowColorLightGrey2 = 24
End Enum

Public Enum prmEfectoFilaGrilla
    prmGridEffectFontBold = 1
    prmGridEffectFontItalic = 2
    prmGridEffectFontStrikeThrough = 3
    prmGridEffectFontUnderline = 4
    prmGridEffectFontStyle_Flat = 5
    prmGridEffectFontStyle_Inset = 6
    prmGridEffectFontStyle_InsetLight = 7
    prmGridEffectFontStyle_Raised = 8
    prmGridEffectFontStyle_RaisedLight = 9
End Enum

Public glsUsuario As String
Public glsPuesto As String
Public glsPassword As String
Public glcRecursoLegajo As String

'**************************************************************************
Sub CerrarForms(Optional vPantalla As Variant)
'**************************************************************************
'Cierra todas las forms activas, excepto vPantalla y los mdi
'Parametros:    vPantalla (Form que no debe cerrar)
'**************************************************************************
    Dim fPantalla As Form
    'Call Status("Listo.")  ' no debe ir aqu�
    For Each fPantalla In Forms
        If TypeOf fPantalla Is MDIForm Then
            DoEvents
        ElseIf Not IsMissing(vPantalla) Then
            If UCase(Trim(fPantalla.name)) = UCase(Trim(vPantalla)) Then
                DoEvents
            End If
        Else
            Unload fPantalla
        End If
    Next
    Call Status("Listo.")   ' aqui debe ir, al final
End Sub

Sub LimpiarForm(fPantalla As Form)
'**************************************************************************
'Limpia los controles de la pantalla indicada
'Parametros:    fPantalla (form a limpiar)
'**************************************************************************
    Dim cControl As Control
    For Each cControl In fPantalla.Controls
        If TypeOf cControl Is TextBox _
            Or InStr(1, cControl.Tag, "CLEAR") > 0 Then
                cControl = ""
        ElseIf TypeOf cControl Is MaskText Then
                cControl.text = ""
        ElseIf TypeOf cControl Is MSFlexGrid Then
                cControl.Clear
        ElseIf TypeOf cControl Is ListBox Then
'            Or TypeOf cControl Is ComboBox Then
                cControl.Clear
        End If
    Next
End Sub

Sub IniciarMenues()
    'deshabilita todo, para que no quede nada mientras se carga Login
     With mdiPrincipal
        .mnuLogin.Enabled = False                   ' LOGIN
        .mnuLogout.Enabled = False                  ' LOGOUT
        .mnuCambioContrase�a.Enabled = False        ' CAMBIO CONTRASE�A
        .mnuSalir.Enabled = True                    ' SALIR
        .mnuTablas.Enabled = False                  ' TABLAS
        .mnuDevelopment.visible = False             ' Nuevo
    End With
End Sub

Sub HabilitarMenues(bOpcion As Boolean)
    Dim bIsOnlyCatalogador As Boolean
    Dim bIsCatalogadorToo As Boolean

    'primero deshabilita todo
     With mdiPrincipal
        .mnuLogin.Enabled = True                'LOGIN
        .mnuLogout.Enabled = False              'LOGOUT
        .mnuCambioContrase�a.Enabled = False    'CAMBIO CONTRASE�A
        .mnuConexion_Preferencias.Enabled = False
        .mnuSalir.Enabled = True                'SALIR
        .mnuTablas.Enabled = False              'TABLAS
        .mnuBPE.Enabled = False                 ' add -001- a.
        .mnuPrcEsp.Enabled = False              'PROCESOS
        .mnuDevelopment.Enabled = False
        .mnuAyuda.Enabled = False
    End With
    
    'despues lo habilita depending on
    If bOpcion And glONLINE Then
        With mdiPrincipal
            .mnuLogin.Enabled = False              'LOGIN
            .mnuLogout.Enabled = True              'LOGOUT
            If InStr(1, "DESA|TEST|", glENTORNO, vbTextCompare) = 0 Then .mnuCambioContrase�a.Enabled = True
            .mnuConexion_Preferencias.Enabled = False
            .mnuSalir.Enabled = True                'SALIR
            If InPerfil("ASEG") Or glLOGIN_SuperUser Then
                .mnuTablas.Enabled = True
                '.mnuRecurso.Enabled = True
                '.mnuBPE.Enabled = True              ' add -001- a.
            Else
                .mnuRecurso.Enabled = False
            End If
            .mnuRecurso.Enabled = True
            .mnuTarea.Enabled = False
            .mnuTareaSector.Enabled = False
            .mnuAcciones.Enabled = False
            .mnuDireccion.Enabled = False
            .mnuGerencia.Enabled = False
            .mnuSector.Enabled = False
            .mnuTareaSector.Enabled = False
            .mnuGrupo.Enabled = False
            .mnuFeriado.Enabled = False
            .mnuTablas_ProyectosIDM.Enabled = False
            .mnuTablas_Aplicativos.Enabled = False
            .mnuPeriodos.Enabled = False
            .mnuAcciones.Enabled = False
            .mnuPrcEsp.Enabled = False
            '.mnuSuperAdm.Enabled = False
            .mnuDevelopment.visible = False
            .mnuVarios.Enabled = False
            .mnuAccPerf.Enabled = False
            .mnuTablasFabricas.Enabled = False
            .mnuTablasPerfiles.Enabled = False
            .mnuTablasPeticion.Enabled = False
            .mnuTablasConfiguracion.Enabled = False
            .mnuTablasTips.Enabled = False                 'GMT01
            .mnuBPE.Enabled = False
            .mnuSagrup.Enabled = False
            .mnuAyuda.Enabled = True
            If InPerfil("ADMI") Or glLOGIN_SuperUser Then
                .mnuTablas.Enabled = True
                .mnuDireccion.Enabled = True
                .mnuGerencia.Enabled = True
                .mnuSector.Enabled = True
                .mnuGrupo.Enabled = True
                .mnuTareaSector.Enabled = True
                .mnuTarea.Enabled = True
                .mnuFeriado.Enabled = True
                .mnuTablas_Aplicativos.Enabled = False              ' Por ahora queda deshabilitado siempre
                .mnuPeriodos.Enabled = True
                .mnuAcciones.Enabled = False
                .mnuAccPerf.Enabled = False
                .mnuPrcEsp.Enabled = True
                .mnuTablas_ProyectosIDM.Enabled = True
                .mnuVarios.Enabled = True
                '.mnuBPE.Enabled = True
                .mnuTablasConfiguracion.Enabled = True
                .mnuTablasFabricas.Enabled = True
                .mnuTablasPerfiles.Enabled = True
                .mnuTablasTips.Enabled = True                 'GMT01
            End If
            If InPerfil("CSEC") Then
                .mnuTareaSector.Enabled = True
            End If
        End With
    End If
End Sub

Sub InicializarTablas()
    Call PerfilCrear
End Sub

Function InicializarRecurso(cod_recurso) As Boolean
    Dim i As Integer
    InicializarRecurso = False
    
    ' Estos son los datos del recurso actuante que ahora son inicializados
    glLOGIN_Gerencia = ""
    glLOGIN_Sector = ""
    glLOGIN_Grupo = ""
    glLOGIN_Direccion = "N"
    glLOGIN_ID_REEMPLAZO = cod_recurso      ' Aqu� se reemplaza el recurso actuante por el recurso reemplazante
    glLOGIN_NAME_REEMPLAZO = ""

    If sp_GetRecurso(glLOGIN_ID_REEMPLAZO, "R") Then
        InicializarRecurso = True
        glLOGIN_NAME_REEMPLAZO = ClearNull(aplRST!nom_recurso)
        If Not (ClearNull(aplRST!estado_recurso) = "A") Then
            MsgBox "El usuario seleccionado no se encuentra en estado Activo.", vbExclamation + vbOKOnly, "Recurso no activo"
            InicializarRecurso = False
            Exit Function
        End If
        If Not (IsNull(aplRST!cod_direccion)) Then
            glLOGIN_Direccion = ClearNull(aplRST!cod_direccion)
        End If
        If Not (IsNull(aplRST!cod_gerencia)) Then
            glLOGIN_Gerencia = ClearNull(aplRST!cod_gerencia)
        End If
        If Not (IsNull(aplRST!cod_sector)) Then
            glLOGIN_Sector = ClearNull(aplRST!cod_sector)
        End If
        If Not (IsNull(aplRST!cod_grupo)) Then
            glLOGIN_Grupo = ClearNull(aplRST!cod_grupo)
        End If

        glUsrPerfilActual = ""
      
        ReDim glUsrPerfiles(1 To 3, 1 To 1)
        'setea un perfil ficticio por las dudas no tenga ninguno
        If ClearNull(glLOGIN_Direccion) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "DIRE"
            glUsrPerfiles(3, 1) = glLOGIN_Direccion
        End If
        If ClearNull(glLOGIN_Gerencia) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "GERE"
            glUsrPerfiles(3, 1) = glLOGIN_Gerencia
        End If
        If ClearNull(glLOGIN_Sector) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "SECT"
            glUsrPerfiles(3, 1) = glLOGIN_Sector
        End If
        If ClearNull(glLOGIN_Grupo) <> "" Then
            glUsrPerfiles(1, 1) = "VIEW"
            glUsrPerfiles(2, 1) = "GRUP"
            glUsrPerfiles(3, 1) = glLOGIN_Grupo
        End If
        i = 1
        If sp_GetRecursoPerfil(glLOGIN_ID_REEMPLAZO, Null) Then
              Do Until aplRST.EOF
                  If ClearNull(aplRST!flg_area) <> "N" Then
                      i = i + 1
                      ReDim Preserve glUsrPerfiles(1 To 3, 1 To i)
                      glUsrPerfiles(1, i) = ClearNull(aplRST!cod_perfil)
                      glUsrPerfiles(2, i) = ClearNull(aplRST!cod_nivel)
                      glUsrPerfiles(3, i) = ClearNull(aplRST!cod_area)
                  End If
                  aplRST.MoveNext
              Loop
              aplRST.Close
        End If
      
        If InPerfil("ADMI") Then
            glUsrPerfilActual = "ADMI"
        ElseIf InPerfil("CDIR") Then
            glUsrPerfilActual = "CDIR"
        ElseIf InPerfil("CGCI") Then
            glUsrPerfilActual = "CGCI"
        ElseIf InPerfil("BPAR") Then
            glUsrPerfilActual = "BPAR"
        ElseIf InPerfil("CSEC") Then
            glUsrPerfilActual = "CSEC"
        ElseIf InPerfil("CGRU") Then
            glUsrPerfilActual = "CGRU"
        ElseIf InPerfil("ANAL") Then
            glUsrPerfilActual = "ANAL"
        ElseIf InPerfil("AUTO") Then
            glUsrPerfilActual = "AUTO"
        ElseIf InPerfil("SUPE") Then
            glUsrPerfilActual = "SUPE"
        ElseIf InPerfil("REFE") Then
            glUsrPerfilActual = "REFE"
        ElseIf InPerfil("SOLI") Then
            glUsrPerfilActual = "SOLI"
        ElseIf InPerfil("CHRS") Then
            glUsrPerfilActual = "CHRS"
        Else
            glUsrPerfilActual = "VIEW"
        End If
      
        mdiPrincipal.sbPrincipal.Panels(4) = getDescPerfil(glUsrPerfilActual)
        DoEvents
        If glLOGIN_SuperUser = True Then
            InicializarRecurso = True
            Exit Function
        End If
    Else
        MsgBox "El usuario ingresado NO est� definido como recurso.", vbExclamation
    End If
End Function

Sub PerfilCrear()
    Dim i As Integer
    ReDim glTablaPerfiles(2, 1)
    i = 0
    
    If sp_GetPerfil(Null, Null) Then
        Do While Not aplRST.EOF
            i = i + 1
            ReDim Preserve glTablaPerfiles(2, i)
            glTablaPerfiles(1, i) = ClearNull(aplRST!cod_perfil)
            glTablaPerfiles(2, i) = ClearNull(aplRST!nom_perfil)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    'Agrega un perfil virtual que no existe, por las duds no tiene ninguno
    i = i + 1
    ReDim Preserve glTablaPerfiles(2, i)
    glTablaPerfiles(1, i) = "VIEW"
    glTablaPerfiles(2, i) = "Consulta"
End Sub

Function InPerfil(Codigo) As Boolean
    'devuelve true si el usuario logueado posee determinado perfil habilitado
    Dim i As Integer
    InPerfil = False
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) = Codigo Then
            InPerfil = True
            Exit Function
        End If
    Next i
End Function

'{ add -010- b.
Function TieneOtroPerfil(Codigo) As Boolean
    ' Devuelve TRUE si adem�s del perfil solicitado, posee otro u otros adicionales
    Dim i As Integer
    TieneOtroPerfil = False
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) <> Codigo Then
            TieneOtroPerfil = True
            Exit Function
        End If
    Next i
End Function

Function GetOtroPerfil(Codigo) As String
    ' Devuelve otro perfil que el solicitado
    Dim i As Integer
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) <> Codigo Then
            GetOtroPerfil = glUsrPerfiles(1, i)
            Exit Function
        End If
    Next i
End Function
'}

Function getPerfNivel(Codigo) As String
    'devuelve el nivel que tiene el recurso para determinado perfil
    Dim i As Integer
    getPerfNivel = ""
    If Codigo = "SADM" Then
        getPerfNivel = "BBVA"
        Exit Function
    End If
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) = Codigo Then
            getPerfNivel = glUsrPerfiles(2, i)
            Exit Function
        End If
    Next i
End Function

Function getPerfArea(Codigo) As String
    ' Devuelve el area que tiene el recurso para determinado perfil
    Dim i As Integer
    getPerfArea = ""
    If Codigo = "SADM" Then
        getPerfArea = "BBVA"
        Exit Function
    End If
    For i = 1 To UBound(glUsrPerfiles, 2)
        If glUsrPerfiles(1, i) = Codigo Then
            getPerfArea = glUsrPerfiles(3, i)
            Exit Function
        End If
    Next i
End Function

Function getDescPerfil(Codigo) As String
    ' Devuelve la descripcion de un perfil
    Dim i As Integer
    getDescPerfil = ""
    For i = 1 To UBound(glTablaPerfiles, 2)
        If glTablaPerfiles(1, i) = Codigo Then
            getDescPerfil = glTablaPerfiles(2, i)
            Exit Function
        End If
    Next i
End Function

Function getModoPerfil(Codigo) As String
    ' Devuelve el modo (SOLI,EJEC) de determinado tipo de perfil
    Dim i As Integer
    getModoPerfil = ""
    For i = 1 To UBound(glTablaPerfiles, 2)
        If glTablaPerfiles(1, i) = Codigo Then
            getModoPerfil = glTablaPerfiles(3, i)
            Exit Function
        End If
    Next i
End Function

Function getDescNvAgrup(Codigo) As String
    getDescNvAgrup = ""
    Select Case Codigo
        Case "PUB": getDescNvAgrup = "P�blico"
        Case "USR": getDescNvAgrup = "Privado"
        Case "DIR": getDescNvAgrup = "Direcci�n"
        Case "GER": getDescNvAgrup = "Gerencia"
        Case "SEC": getDescNvAgrup = "Sector"
        Case "GRU": getDescNvAgrup = "Grupo"
    End Select
End Function

Function chkNvAgrup(aNiv, aDir, aGer, aSec, aGru, aUsr, uDir, uGer, uSec, uGru, uUsr) As Boolean
    chkNvAgrup = False
    Select Case aNiv
        Case "PUB"
            chkNvAgrup = True
        Case "USR"
            If ClearNull(uUsr) = ClearNull(aUsr) Then
                chkNvAgrup = True
            End If
        Case "DIR"
            If ClearNull(uDir) = ClearNull(aDir) Then
                chkNvAgrup = True
            End If
        Case "GER"
            If ClearNull(uGer) = ClearNull(aGer) Then
                chkNvAgrup = True
            End If
            If ClearNull(uGer) = "" And ClearNull(aDir) = ClearNull(uDir) Then
                chkNvAgrup = True
            End If
        Case "SEC"
            If ClearNull(uSec) = ClearNull(aSec) Then
                chkNvAgrup = True
            End If
            If ClearNull(uSec) = "" And ClearNull(aGer) = ClearNull(uGer) Then
                chkNvAgrup = True
            End If
            If ClearNull(uGer) = "" And ClearNull(aDir) = ClearNull(uDir) Then
                chkNvAgrup = True
            End If
        Case "GRU"
            If ClearNull(uGru) = ClearNull(aGru) Then
                chkNvAgrup = True
            End If
            If ClearNull(uGru) = "" And ClearNull(aSec) = ClearNull(uSec) Then
                chkNvAgrup = True
            End If
            If ClearNull(uSec) = "" And ClearNull(aGer) = ClearNull(uGer) Then
                chkNvAgrup = True
            End If
            If ClearNull(uGer) = "" And ClearNull(aDir) = ClearNull(uDir) Then
                chkNvAgrup = True
            End If
    End Select
End Function

Function getDescNivel(Codigo) As String
    getDescNivel = ""
    Select Case Codigo
        Case "BBVA": getDescNivel = "BBVA"
        Case "DIRE": getDescNivel = "Direcci�n"
        Case "GERE": getDescNivel = "Gerencia"
        Case "SECT": getDescNivel = "Sector"
        Case "GRUP": getDescNivel = "Grupo"
    End Select
End Function

Public Function FindGrid(ByRef grdDatos As MSFlexGrid, ByRef rowInicial As Integer, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Boolean
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    FindGrid = False
    If rowInicial = 0 Then
        rowInicial = 1
    End If
    With grdDatos
        If .Rows < 2 Then
            Exit Function
        End If
        .RowSel = 0
        .ColSel = 0
        For i = rowInicial To .Rows - 1
           If InStr(Trim(.TextMatrix(i, ptrColumn)), Trim(strSeek)) > 0 Then
                If flgPosicion Then
                    .TopRow = i
                    .row = i
                    .RowSel = i
                    .Col = 0
                    .ColSel = .cols - 1
                End If
                rowInicial = i
                FindGrid = True
                Exit Function
           End If
        Next
    End With
End Function

Function GridSeek(objForm As Form, objGrid As MSFlexGrid, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Integer
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    GridSeek = 0
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        For i = 1 To .Rows - 1
           If ClearNull(.TextMatrix(i, ptrColumn)) = ClearNull(strSeek) Then
                GridSeek = i
                If flgPosicion Then
                    .TopRow = i
                    .row = i
                    .RowSel = i
                    .Col = 0
                    .ColSel = .cols - 1
                    
                    Call objForm.MostrarSeleccion(False)
                End If
                Exit Function
           End If
        Next
    End With
End Function

Function GridSeek2(objForm As Form, objGrid As MSFlexGrid, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Integer
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    GridSeek2 = 0
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        For i = 1 To .Rows - 1
            If InStr(1, ClearNull(.TextMatrix(i, ptrColumn)), ClearNull(strSeek), vbTextCompare) > 0 Then
                GridSeek2 = i
                If flgPosicion Then
                    .TopRow = i
                    .row = i
                    .RowSel = i
                    .Col = 0
                    .ColSel = .cols - 1
                    Call objForm.MostrarSeleccion(False)
                End If
                Exit Function
           End If
        Next
    End With
End Function

Sub GenArchivoTabla(objGrid As MSFlexGrid, lastCol As Integer, ByVal sArchivo As String)
    Dim nFile As Integer
    Dim sDatos As String
        
    Dim nColumnas As Integer, i, J As Integer
    On Error Resume Next
    
    ' Abre el archivo
    Screen.MousePointer = vbHourglass
    nFile = FreeFile
    On Error GoTo errOpen
    Open sArchivo For Output As #nFile
    On Error GoTo 0
    
    With objGrid
        If .Rows < 2 Then
            Screen.MousePointer = vbNormal
            Status ("Listo")
            Exit Sub
        End If
        
        'encabezados
        sDatos = ""
        For J = 0 To lastCol
            sDatos = sDatos & ClearNull(.TextMatrix(0, J)) & vbTab
        Next
        sDatos = Left(sDatos, Len(sDatos) - 1)
        Print #nFile, sDatos
        
        'datos
        For i = 1 To .Rows - 1
           sDatos = ""
           For J = 0 To lastCol
               sDatos = sDatos & ClearNull(.TextMatrix(i, J)) & vbTab
           Next
           sDatos = Left(sDatos, Len(sDatos) - 1)
           Print #nFile, sDatos
        Next
    End With
    
    Close #nFile
    Screen.MousePointer = vbNormal
    Status ("Listo")
    Exit Sub
errOpen:
    Screen.MousePointer = vbNormal
    Status ("Listo")
    MsgBox ("No pudo crear el archivo. " & Err.DESCRIPTION)
    Exit Sub
End Sub

Function valTareaHabil(xTarea) As Boolean
    valTareaHabil = False
    If sp_GetTarea(xTarea) Then
        If ClearNull(aplRST!flg_habil) = "S" Then
            valTareaHabil = True
        End If
        aplRST.Close
    End If
End Function

Function getBpSector(xSector) As String
    getBpSector = ""
    If sp_GetSector(xSector, Null, Null) Then
        getBpSector = ClearNull(aplRST!cod_bpar)
        aplRST.Close
    End If
End Function

Function valSectorHabil(xSector) As Boolean
        valSectorHabil = False
        If sp_GetSector(xSector, Null, Null) Then
            If ClearNull(aplRST!flg_habil) = "N" Then
                valSectorHabil = False
            Else
                valSectorHabil = True
            End If
            aplRST.Close
        End If
End Function

Function valGrupoHabil(xGrupo) As Boolean
        valGrupoHabil = False
        If sp_GetGrupo(xGrupo, Null, Null) Then
            If ClearNull(aplRST!flg_habil) = "N" Then
                valGrupoHabil = False
            Else
                valGrupoHabil = True
            End If
            aplRST.Close
        End If
End Function

Function valGerenciaHabil(xGerencia) As Boolean
        valGerenciaHabil = False
        If sp_GetGerencia(xGerencia, Null, Null) Then
            If ClearNull(aplRST!flg_habil) = "N" Then
                valGerenciaHabil = False
            Else
                valGerenciaHabil = True
            End If
            aplRST.Close
        End If
End Function

Function valDireccionHabil(xDireccion) As Boolean
        valDireccionHabil = False
        If sp_GetDireccion(xDireccion, Null) Then
            If ClearNull(aplRST!flg_habil) = "N" Then
                valDireccionHabil = False
            Else
                valDireccionHabil = True
            End If
            aplRST.Close
        End If
End Function

Sub reasignarPeticionGrupo(xNumeroPeticion, xSector, xGrupo, ySector, yGrupo, origen)
    Dim EstadoPeticion As String
    Dim NuevoEstadoPeticion As String
    Dim EstadoGrupo As String
    Dim EstadoSector As String
    Dim NuevoEstadoSector As String
    Dim lblSector As String
    Dim lblGrupo As String
    Dim NroHistorial As Long
    Dim fEstado
    
    If sp_GetUnaPeticionSinCategoria(xNumeroPeticion) Then 'RP Optimizacion
        If Not aplRST.EOF Then
            EstadoPeticion = ClearNull(aplRST!cod_estado)
        End If
        aplRST.Close
    End If
    If sp_GetPeticionSector(xNumeroPeticion, xSector) Then
       If Not aplRST.EOF Then
          lblSector = ClearNull(aplRST!nom_sector)
        End If
        aplRST.Close
    End If
    If sp_GetPeticionGrupo(xNumeroPeticion, xSector, xGrupo) Then
       If Not aplRST.EOF Then
          EstadoGrupo = ClearNull(aplRST!cod_estado)
          lblGrupo = ClearNull(aplRST!nom_grupo)
        End If
        aplRST.Close
    End If

    If sp_ChangePeticionGrupo(xNumeroPeticion, xSector, xGrupo, ySector, yGrupo, origen) Then    ' upd -023- a.
        NroHistorial = sp_AddHistorial(xNumeroPeticion, "GREASIG", EstadoPeticion, ySector, Null, yGrupo, Null, glLOGIN_ID_REEMPLAZO, glUsrPerfilActual, "� Sector/Grupo anterior: " & Trim(lblSector) & "/" & Trim(lblGrupo) & " �")
        '' el sector saliente
        EstadoSector = ""
        If sp_GetPeticionSector(xNumeroPeticion, xSector) Then
            If Not aplRST.EOF Then
                EstadoSector = ClearNull(aplRST!cod_estado)
            End If
            aplRST.Close
            
            fEstado = date
            
            Call getIntegracionGrupo(xNumeroPeticion, xSector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
            NuevoEstadoSector = getNuevoEstadoSector(xNumeroPeticion, xSector)
            If NuevoEstadoSector <> "" Then
                If NuevoEstadoSector <> "TERMIN" Then
                    fFinReal = Null
                End If
'''''           'el RECHTE no se propaga
'''''           If NuevoEstadoSector = "RECHTE" Then
'''''              NuevoEstadoSector = EstadoSector
'''''           End If
'''''   ------------------------------------------
'       AHORA Si, se propaga el rechte (2005-11-08)
'''''   ------------------------------------------
                fEstado = date
                If Not IsNull(fIniPlan) Then
                    fEstado = fIniPlan
                End If
                If Not IsNull(fFinPlan) Then
                    fEstado = fFinPlan
                End If
                If Not IsNull(fIniReal) Then
                    fEstado = fIniReal
                End If
                If Not IsNull(fFinReal) Then
                    fEstado = fFinReal
                End If
                If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
                    Call sp_UpdatePeticionSector(xNumeroPeticion, xSector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, fEstado, "", 0, 0)
                Else
                    Call sp_UpdatePeticionSector(xNumeroPeticion, xSector, Null, Null, Null, Null, 0, NuevoEstadoSector, date, "", 0, 0)
                End If
            End If
        End If
            
        '' el sector ENTRANTE
        EstadoSector = ""
        If sp_GetPeticionSector(xNumeroPeticion, ySector) Then
            If Not aplRST.EOF Then
              EstadoSector = ClearNull(aplRST!cod_estado)
            End If
            aplRST.Close
            
            Call getIntegracionGrupo(xNumeroPeticion, ySector, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
            NuevoEstadoSector = getNuevoEstadoSector(xNumeroPeticion, ySector)
            If NuevoEstadoSector <> "" Then
                If NuevoEstadoSector <> "TERMIN" Then
                    fFinReal = Null
                End If
                '''''   'el RECHTE no se propaga
                '''''   If NuevoEstadoSector = "RECHTE" Then
                '''''       NuevoEstadoSector = EstadoSector
                '''''   End If
                '''''   ------------------------------------------
                '       AHORA Si, se propaga el rechte (2005-11-08)
                '''''   ------------------------------------------
                fEstado = date
                If Not IsNull(fIniPlan) Then
                    fEstado = fIniPlan
                End If
                If Not IsNull(fFinPlan) Then
                    fEstado = fFinPlan
                End If
                If Not IsNull(fIniReal) Then
                    fEstado = fIniReal
                End If
                If Not IsNull(fFinReal) Then
                    fEstado = fFinReal
                End If
                If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoSector) > 0 Then
                    Call sp_UpdatePeticionSector(xNumeroPeticion, ySector, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup, NuevoEstadoSector, fEstado, "", 0, 0)
                Else
                    Call sp_UpdatePeticionSector(xNumeroPeticion, ySector, Null, Null, Null, Null, 0, NuevoEstadoSector, date, "", 0, 0)  ' upd -006- a.
                End If
            End If
        End If
        
        ''la PETICION
        Call getIntegracionSector(xNumeroPeticion, Null, "ESTIOK|PLANOK|EJECUC|TERMIN")
        NuevoEstadoPeticion = getNuevoEstadoPeticion(xNumeroPeticion)
        Call sp_UpdatePetField(xNumeroPeticion, "ESTADO", NuevoEstadoPeticion, Null, Null)
        'Call sp_UpdatePetField(xNumeroPeticion, "SITUAC", "", Null, Null)
        If NuevoEstadoPeticion <> "TERMIN" Then
            fFinReal = Null
        End If
        If InStr(1, "ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|REVISA|CANCEL", NuevoEstadoPeticion) > 0 Then
            Call sp_UpdatePetFechas(xNumeroPeticion, fIniPlan, fFinPlan, fIniReal, fFinReal, hsPresup)
        Else
            Call sp_UpdatePetFechas(xNumeroPeticion, Null, Null, Null, Null, 0)
        End If
        
        Call sp_UpdHistorial(NroHistorial, "GREASIG", NuevoEstadoPeticion, ySector, NuevoEstadoSector, yGrupo, EstadoGrupo, glLOGIN_ID_REEMPLAZO)
    End If
End Sub

Sub doLog(ByVal sTexto As String)
    Dim nFile As Integer
        
    On Error Resume Next
   
    ' Abre el archivo
    nFile = FreeFile
    On Error GoTo errOpen
    Open glWRKDIR & glLOGIN_ID_REAL & "_log.txt" For Append As #nFile
    On Error GoTo 0
    
    Print #nFile, Format(date, "yyyymmdd") & ":" & Format(Time, "hhmss") & ":" & sTexto
    
    Close #nFile
    Exit Sub
errOpen:
    MsgBox ("No pudo crear el archivo de log. " & Err.DESCRIPTION)
    Exit Sub
End Sub

Sub doFile(ByVal cFileName As String, ByVal sTexto As String)
    Dim nFile As Integer
        
    On Error Resume Next
   
    ' Abre el archivo
    nFile = FreeFile
    On Error GoTo errOpen
    Open glWRKDIR & cFileName For Append As #nFile
    On Error GoTo 0
    
    Print #nFile, sTexto
    
    Close #nFile
    Exit Sub
errOpen:
    MsgBox ("No pudo crear el archivo. " & Err.DESCRIPTION)
    Exit Sub
End Sub

'Private Function ValidVersionNumber(cSystemVersion As String) As Boolean
'    Dim lSystemVersion As Long
'    Dim lUserVersion As Long
'    Dim i As Integer
'    Dim Num(1 To 3) As Integer
'    Dim a As Integer
'    Dim b As Integer
'    Dim c As Integer
'    Dim Pos As Integer
'    Dim cAuxiliar As String
'
'    ValidVersionNumber = True       ' Por defecto la versi�n es correcta
'
'    cAuxiliar = Mid(cSystemVersion, 2)
'
'    For i = 1 To 3
'        If Pos = 0 Then Pos = 1
'        If Not InStr(Pos, cAuxiliar, ".", vbTextCompare) = 0 Then
'            Pos = InStr(Pos, cAuxiliar, ".", vbTextCompare)
'            Num(i) = Val(Mid(cAuxiliar, Pos - 1))
'        Else
'            Num(i) = Val(Mid(cAuxiliar, Pos))
'        End If
'        Pos = Pos + 1
'    Next i
'
'    ' Obtengo un n�mero de 8 bits
'    lUserVersion = (CLng(App.Major) * 1000000) + _
'                     (CLng(App.Minor) * 1000) + _
'                     (CLng(App.Revision) * 1)
'    ' Obtengo el n�mero de versi�n del usuario
'    lSystemVersion = (CLng(Num(1)) * 1000000) + _
'                     (CLng(Num(2)) * 1000) + _
'                     (CLng(Num(3)) * 1)
'    If lUserVersion < lSystemVersion Then
'        ValidVersionNumber = False
'    End If
'End Function
'}

'{ add -013- a.
Sub DefaultPreferencias()
    ' Preferencias del usuario
    ' GENERAL
    If Len(GetSetting("GesPet", "Preferencias\General", "IniciarTips", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\General", "IniciarTips", "N"                          ' No iniciar los Tips
    If Len(GetSetting("GesPet", "Preferencias\General", "PromptActiveUsersOnly", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\General", "PromptActiveUsersOnly", "N"      ' Mostrar solo los recursos en estado activo
    
    ' CARGA DE HORAS
    If Len(GetSetting("GesPet", "Preferencias\Horas", "Feriados", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\Horas", "Feriados", "N"                 ' No marcar de manera diferencias los d�as feriados y no h�biles
    If Len(GetSetting("GesPet", "Preferencias\Horas", "Porcentajes", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\Horas", "Porcentajes", "N"           ' No mostrar los porcentajes de horas
    
    ' ADJUNTAR ARCHIVOS
    ' Clave: HKEY_CURRENT_USER\Software\VB and VBA Program Settings\GesPet\Preferencias\Adjuntos
    If Len(GetSetting("GesPet", "Preferencias\Adjuntos", "ModoSeleccion", "N")) = 0 Then SaveSetting "GesPet", "Preferencias\Adjuntos", "ModoSeleccion", "N"     ' Normal
    ' HISTORIAL
    ' Clave: HKEY_CURRENT_USER\Software\VB and VBA Program Settings\GesPet\Preferencias\Historial
    ' Anchos de columnas
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth01", 1500)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth01", 1500
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth02", 2400)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth02", 2400
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth03", 1500)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth03", 1500
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth04", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth04", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth05", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth05", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth06", 3000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth06", 3000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth07", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth07", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth08", 1300)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth08", 1300
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth09", 2000)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth09", 2000
    If Len(GetSetting("GesPet", "Preferencias\Historial", "ColWidth10", 1300)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial", "ColWidth10", 1300
    
    ' Posicionamiento y tama�o de la ventana
    If Len(GetSetting("GesPet", "Preferencias\Historial\Posicion", "Top", 3495)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Posicion", "Top", 3495
    If Len(GetSetting("GesPet", "Preferencias\Historial\Posicion", "Left", 4005)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Posicion", "Left", 4005
    
    If Len(GetSetting("GesPet", "Preferencias\Historial\Tamanio", "Height", 7950)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Tamanio", "Height", 7950
    If Len(GetSetting("GesPet", "Preferencias\Historial\Tamanio", "Width", 11175)) = 0 Then SaveSetting "GesPet", "Preferencias\Historial\Tamanio", "Width", 11175
End Sub
'}

'{ add -029- a.
Sub Default_Preferencias_IDM()
    ' Comprueba que existan todas las entradas para los valores
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "ImportLastFile") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "ImportLastFile", "ImportFile.xls"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "ImportLastPath") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "ImportLastPath", glWRKDIR
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "PreAnalizeFile") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "PreAnalizeFile", "N"
    
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "TipoProyecto") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "TipoProyecto", "NULL"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "MacroProyecto") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "MacroProyecto", "1"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "SubProyecto") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "SubProyecto", "0"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "Categoria") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "Categoria", "1"
    If GetSetting("GesPet", "Preferencias\ProyectoIDM", "Clase") = "" Then SaveSetting "GesPet", "Preferencias\ProyectoIDM", "Clase", "1"
End Sub
'}

Sub DafaultExpor01()
    '{ add -007- a.
    ' Comprueba que existan todas las entradas para los valores
    If GetSetting("GesPet", "Export01", "CHKpet_nroasignado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_nroasignado", "SI"
    If GetSetting("GesPet", "Export01", "FileExport") = "" Then SaveSetting "GesPet", "Export01", "FileExport", glWRKDIR & "EXPORTACION.XLS"
    If GetSetting("GesPet", "Export01", "CHKpet_nroasignado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_nroasignado", "SI"
    If GetSetting("GesPet", "Export01", "CHKcod_tipo_peticion") = "" Then SaveSetting "GesPet", "Export01", "CHKcod_tipo_peticion", "SI"
    If GetSetting("GesPet", "Export01", "CHKtitulo") = "" Then SaveSetting "GesPet", "Export01", "CHKtitulo", "SI"
    If GetSetting("GesPet", "Export01", "CHKdetalle") = "" Then SaveSetting "GesPet", "Export01", "CHKdetalle", "SI"
    If GetSetting("GesPet", "Export01", "CHKcaracte") = "" Then SaveSetting "GesPet", "Export01", "CHKcaracte", "SI"
    If GetSetting("GesPet", "Export01", "CHKmotivos") = "" Then SaveSetting "GesPet", "Export01", "CHKmotivos", "SI"
    If GetSetting("GesPet", "Export01", "CHKbenefic") = "" Then SaveSetting "GesPet", "Export01", "CHKbenefic", "SI"
    If GetSetting("GesPet", "Export01", "CHKrelacio") = "" Then SaveSetting "GesPet", "Export01", "CHKrelacio", "SI"
    If GetSetting("GesPet", "Export01", "CHKobserva") = "" Then SaveSetting "GesPet", "Export01", "CHKobserva", "SI"
    If GetSetting("GesPet", "Export01", "CHKprioridad") = "" Then SaveSetting "GesPet", "Export01", "CHKprioridad", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ing_comi") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ing_comi", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_fe_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_fe_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_situacion") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_situacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKpet_sector") = "" Then SaveSetting "GesPet", "Export01", "CHKpet_sector", "SI"
    If GetSetting("GesPet", "Export01", "CHKbpar") = "" Then SaveSetting "GesPet", "Export01", "CHKbpar", "SI"
    If GetSetting("GesPet", "Export01", "CHKsoli") = "" Then SaveSetting "GesPet", "Export01", "CHKsoli", "SI"
    If GetSetting("GesPet", "Export01", "CHKrefe") = "" Then SaveSetting "GesPet", "Export01", "CHKrefe", "SI"
    If GetSetting("GesPet", "Export01", "CHKsupe") = "" Then SaveSetting "GesPet", "Export01", "CHKsupe", "SI"
    If GetSetting("GesPet", "Export01", "CHKauto") = "" Then SaveSetting "GesPet", "Export01", "CHKauto", "SI"
    If GetSetting("GesPet", "Export01", "CHKhoraspresup") = "" Then SaveSetting "GesPet", "Export01", "CHKhoraspresup", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ini_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ini_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_fin_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_fin_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ini_real") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ini_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_fin_real") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_fin_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_ini_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_ini_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKfe_fin_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKfe_fin_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKrepla") = "" Then SaveSetting "GesPet", "Export01", "CHKrepla", "SI"
    If GetSetting("GesPet", "Export01", "CHKimportancia_nom") = "" Then SaveSetting "GesPet", "Export01", "CHKimportancia_nom", "SI"
    If GetSetting("GesPet", "Export01", "CHKcorp_local") = "" Then SaveSetting "GesPet", "Export01", "CHKcorp_local", "SI"
    If GetSetting("GesPet", "Export01", "CHKnom_orientacion") = "" Then SaveSetting "GesPet", "Export01", "CHKnom_orientacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_sector") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_sector", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_situacion") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_situacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_horaspresup") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_horaspresup", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_ini_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_fin_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_ini_real") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_fin_real") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_ini_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_ini_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKsec_fe_fin_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKsec_fe_fin_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_sector") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_sector", "SI"
    If GetSetting("GesPet", "Export01", "CHKprio_ejecuc") = "" Then SaveSetting "GesPet", "Export01", "CHKprio_ejecuc", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_estado") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_estado", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_situacion") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_situacion", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_horaspresup") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_horaspresup", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_ini_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_fin_plan") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_plan", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_ini_real") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_fin_real") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_real", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_ini_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_ini_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_fe_fin_orig") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_fe_fin_orig", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_info_adic") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_info_adic", "SI"
    If GetSetting("GesPet", "Export01", "CHKgru_recursos") = "" Then SaveSetting "GesPet", "Export01", "CHKgru_recursos", "SI"
    If GetSetting("GesPet", "Export01", "CHKgantt") = "" Then SaveSetting "GesPet", "Export01", "CHKgantt", "NO"
    If GetSetting("GesPet", "Export01", "chkPetClass") = "" Then SaveSetting "GesPet", "Export01", "chkPetClass", "SI"
    If GetSetting("GesPet", "Export01", "chkPetImpTech") = "" Then SaveSetting "GesPet", "Export01", "chkPetImpTech", "SI"
    If GetSetting("GesPet", "Export01", "chkPetDocu") = "" Then SaveSetting "GesPet", "Export01", "chkPetDocu", "SI"
    If GetSetting("GesPet", "Export01", "chkHorasReales") = "" Then SaveSetting "GesPet", "Export01", "chkHorasReales", "SI"
    If GetSetting("GesPet", "Export01", "CHKpcf") = "" Then SaveSetting "GesPet", "Export01", "CHKpcf", "SI"
    If GetSetting("GesPet", "Export01", "CHKseguimN") = "" Then SaveSetting "GesPet", "Export01", "CHKseguimN", "SI"
    If GetSetting("GesPet", "Export01", "CHKseguimT") = "" Then SaveSetting "GesPet", "Export01", "CHKseguimT", "SI"
    If GetSetting("GesPet", "Export01", "chkFechaProduccion") = "" Then SaveSetting "GesPet", "Export01", "chkFechaProduccion", "SI"
    If GetSetting("GesPet", "Export01", "chkFechaSuspension") = "" Then SaveSetting "GesPet", "Export01", "chkFechaSuspension", "SI"
    If GetSetting("GesPet", "Export01", "chkMotivoSuspension") = "" Then SaveSetting "GesPet", "Export01", "chkMotivoSuspension", "SI"
    If GetSetting("GesPet", "Export01", "chkObservEstado") = "" Then SaveSetting "GesPet", "Export01", "chkObservEstado", "SI"
    '}
    If GetSetting("GesPet", "Export01", "chkPetRegulatorio") = "" Then SaveSetting "GesPet", "Export01", "chkPetRegulatorio", "SI"  ' add -011- a.
    If GetSetting("GesPet", "Export01", "chkPetConfHomo") = "" Then SaveSetting "GesPet", "Export01", "chkPetConfHomo", "SI"  ' add -011- b.
    If GetSetting("GesPet", "Export01", "chkPetProyectoIDM") = "" Then SaveSetting "GesPet", "Export01", "chkPetProyectoIDM", "SI"  ' add -011- c.
    If GetSetting("GesPet", "Export01", "chkAutoFilter") = "" Then SaveSetting "GesPet", "Export01", "chkAutoFilter", "NO"  ' add -012- b.
    If GetSetting("GesPet", "Export01", "chkAdjuntoLPO") = "" Then SaveSetting "GesPet", "Export01", "chkAdjuntoLPO", "c:\gespet"  ' add -012- c.
End Sub

Public Sub ObtenerTiempo(Iniciar As Boolean)
    Static ini As Long
    Static Fin As Long
    If Iniciar Then
        ini = GetTickCount '/ 1000
    Else
        Fin = GetTickCount '/ 1000
        Debug.Print Fin & " - " & ini & " = " & Fin - ini & " milisegundo(s)"
    End If
End Sub

' Leonardo Azpurua -06/05/08-
Public Function estaRegistrada(rutaDLL As String) As Boolean
    On Error GoTo Errores
    Dim c As TLI.TLIApplication
    Dim t As TLI.TypeLibInfo
    Dim R As TLI.TypeLibInfo
    Dim RetVal As Boolean
 
    ' vas a devolver True a menos que hayaun error
    RetVal = True
 
    ' creas una aplicacion TLI
    Set c = CreateObject("TLI.TLIApplication")
 
    ' obtienes la informaci�n de la DLL del archivo
    Set t = c.TypeLibInfoFromFile(rutaDLL)
    If Err.Number Then
        MsgBox "ERROR: " & Err.DESCRIPTION
        RetVal = False
    Else
        ' ... con la informaci�n que obtuviste
        Set R = c.TypeLibInfoFromRegistry(t.Guid, t.MajorVersion, t.MinorVersion, t.LCID)
        If Err.Number Then
            RetVal = False
        End If
    End If
 
    estaRegistrada = RetVal
    Exit Function
Errores:
    'MsgBox "ERROR: " & Err.DESCRIPTION, vbCritical
    RetVal = False
End Function

Public Sub InicializarGruposDeControl()
    ' Muleto
End Sub


Public Sub InicializarObjetosComunes()
  ' Muleto
End Sub

