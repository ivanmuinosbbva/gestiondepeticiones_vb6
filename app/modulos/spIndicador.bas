Attribute VB_Name = "spIndicador"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertIndicador(indicadorId, indicadorNom, indicadorHab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertIndicador"
        .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , indicadorId)
        .Parameters.Append .CreateParameter("@indicadorNom", adChar, adParamInput, 60, indicadorNom)
        .Parameters.Append .CreateParameter("@indicadorHab", adChar, adParamInput, 1, indicadorHab)
        Set aplRST = .Execute
    End With
    sp_InsertIndicador = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertIndicador = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateIndicador(indicadorId, indicadorNom, indicadorHab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateIndicador"
        .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , indicadorId)
        .Parameters.Append .CreateParameter("@indicadorNom", adChar, adParamInput, 60, indicadorNom)
        .Parameters.Append .CreateParameter("@indicadorHab", adChar, adParamInput, 1, indicadorHab)
        Set aplRST = .Execute
    End With
    sp_UpdateIndicador = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateIndicador = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteIndicador(indicadorId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteIndicador"
        .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , indicadorId)
        Set aplRST = .Execute
    End With
    sp_DeleteIndicador = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteIndicador = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetIndicador(indicadorId, empresa) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetIndicador"
        .Parameters.Append .CreateParameter("@indicadorId", adInteger, adParamInput, , IIf(IsNull(indicadorId), Null, indicadorId))
        .Parameters.Append .CreateParameter("@empresa", adInteger, adParamInput, , IIf(IsNull(empresa), Null, empresa))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetIndicador = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetIndicador = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
