Attribute VB_Name = "spPeticionValidacion"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertPeticionValidacion(pet_nrointerno, valid, valitem, valitemvalor) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertPeticionValidacion"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        .Parameters.Append .CreateParameter("@valitemvalor", adChar, adParamInput, 1, valitemvalor)
        Set aplRST = .Execute
    End With
    sp_InsertPeticionValidacion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertPeticionValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdatePeticionValidacion(pet_nrointerno, valid, valitem, valitemvalor) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdatePeticionValidacion"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        .Parameters.Append .CreateParameter("@valitemvalor", adChar, adParamInput, 1, valitemvalor)
        Set aplRST = .Execute
    End With
    sp_UpdatePeticionValidacion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdatePeticionValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeletePeticionValidacion(pet_nrointerno, valid, valitem) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeletePeticionValidacion"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        Set aplRST = .Execute
    End With
    sp_DeletePeticionValidacion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeletePeticionValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionValidacion(pet_nrointerno, valid, valitem) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetPeticionValidacion"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , pet_nrointerno)
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetPeticionValidacion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
