Attribute VB_Name = "spEstadosIDM"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~

Option Explicit

Function sp_InsertEstadosIDM(cod_estado, nom_estado, flg_rnkup, flg_herdlt1, flg_petici, flg_secgru) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertEstadosIDM"
                .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
                .Parameters.Append .CreateParameter("@nom_estado", adChar, adParamInput, 30, nom_estado)
                .Parameters.Append .CreateParameter("@flg_rnkup", adChar, adParamInput, 2, flg_rnkup)
                .Parameters.Append .CreateParameter("@flg_herdlt1", adChar, adParamInput, 1, flg_herdlt1)
                .Parameters.Append .CreateParameter("@flg_petici", adChar, adParamInput, 1, flg_petici)
                .Parameters.Append .CreateParameter("@flg_secgru", adChar, adParamInput, 1, flg_secgru)
                Set aplRST = .Execute
        End With
        sp_InsertEstadosIDM = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_InsertEstadosIDM = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateEstadosIDM(cod_estado, nom_estado, flg_rnkup, flg_herdlt1, flg_petici, flg_secgru) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateEstadosIDM"
                .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
                .Parameters.Append .CreateParameter("@nom_estado", adChar, adParamInput, 30, nom_estado)
                .Parameters.Append .CreateParameter("@flg_rnkup", adChar, adParamInput, 2, flg_rnkup)
                .Parameters.Append .CreateParameter("@flg_herdlt1", adChar, adParamInput, 1, flg_herdlt1)
                .Parameters.Append .CreateParameter("@flg_petici", adChar, adParamInput, 1, flg_petici)
                .Parameters.Append .CreateParameter("@flg_secgru", adChar, adParamInput, 1, flg_secgru)
                Set aplRST = .Execute
        End With
        sp_UpdateEstadosIDM = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateEstadosIDM = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateEstadosIDMField(cod_estado, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateEstadosIDMField"
                .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateEstadosIDMField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateEstadosIDMField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteEstadosIDM(cod_estado) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteEstadosIDM"
                .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
                Set aplRST = .Execute
        End With
        sp_DeleteEstadosIDM = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteEstadosIDM = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetEstadosIDM(cod_estado, tipo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetEstadosIDM"
        .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@tipo", adChar, adParamInput, 1, tipo)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetEstadosIDM = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_GetEstadosIDM = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

