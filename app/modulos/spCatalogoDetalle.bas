Attribute VB_Name = "spCatalogoDetalle"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertCatalogoDetalle(id, campo, pos_desde, longitud, tipo_id, subtipo_id, rutina) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertCatalogoDetalle"
        .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , id)
        .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 50, campo)
        .Parameters.Append .CreateParameter("@pos_desde", adInteger, adParamInput, , pos_desde)
        .Parameters.Append .CreateParameter("@longitud", adInteger, adParamInput, , longitud)
        .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, 1, tipo_id)
        .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, 1, subtipo_id)
        .Parameters.Append .CreateParameter("@rutina", adChar, adParamInput, 8, rutina)
        Set aplRST = .Execute
    End With
    sp_InsertCatalogoDetalle = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertCatalogoDetalle = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateCatalogoDetalle(id, campo, pos_desde, longitud, tipo_id, subtipo_id, rutina) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateCatalogoDetalle"
                .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , id)
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 50, campo)
                .Parameters.Append .CreateParameter("@pos_desde", adInteger, adParamInput, , pos_desde)
                .Parameters.Append .CreateParameter("@longitud", adInteger, adParamInput, , longitud)
                .Parameters.Append .CreateParameter("@tipo_id", adChar, adParamInput, 1, tipo_id)
                .Parameters.Append .CreateParameter("@subtipo_id", adChar, adParamInput, 1, subtipo_id)
                .Parameters.Append .CreateParameter("@rutina", adChar, adParamInput, 8, rutina)
                Set aplRST = .Execute
        End With
        sp_UpdateCatalogoDetalle = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_UpdateCatalogoDetalle = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteCatalogoDetalle(id, campo) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteCatalogoDetalle"
                .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , id)
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 50, campo)
                Set aplRST = .Execute
        End With
        sp_DeleteCatalogoDetalle = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
        sp_DeleteCatalogoDetalle = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetCatalogoDetalle(id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetCatalogoDetalle"
        .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , id)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetCatalogoDetalle = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetCatalogoDetalle = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

