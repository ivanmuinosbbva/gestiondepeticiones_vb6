Attribute VB_Name = "spValidacion"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertValidacion(valid, valitem, valdesc, valorden, valhab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertValidacion"
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        .Parameters.Append .CreateParameter("@valdesc", adChar, adParamInput, 255, valdesc)
        .Parameters.Append .CreateParameter("@valorden", adInteger, adParamInput, , valorden)
        .Parameters.Append .CreateParameter("@valhab", adChar, adParamInput, 1, valhab)
        Set aplRST = .Execute
    End With
    sp_InsertValidacion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateValidacion(valid, valitem, valdesc, valorden, valhab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateValidacion"
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        .Parameters.Append .CreateParameter("@valdesc", adChar, adParamInput, 255, valdesc)
        .Parameters.Append .CreateParameter("@valorden", adInteger, adParamInput, , valorden)
        .Parameters.Append .CreateParameter("@valhab", adChar, adParamInput, 1, valhab)
        Set aplRST = .Execute
    End With
    sp_UpdateValidacion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteValidacion(valid, valitem) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteValidacion"
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        Set aplRST = .Execute
    End With
    sp_DeleteValidacion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetValidacion(valid, valitem) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetValidacion"
        .Parameters.Append .CreateParameter("@valid", adInteger, adParamInput, , valid)
        .Parameters.Append .CreateParameter("@valitem", adInteger, adParamInput, , valitem)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetValidacion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetValidacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
