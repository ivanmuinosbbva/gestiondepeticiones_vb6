Attribute VB_Name = "FuncionesCrystal10"
Option Explicit

Dim CPProperty As CRAXDRT.ConnectionProperty

Public Sub ConectarDB(ByRef crRpt As Variant)
    Dim bFlg1 As Boolean, bFlg2 As Boolean
    
    Call Puntero(True)
    For Each CPProperty In crRpt.DATABASE.Tables(1).ConnectionProperties
        'Debug.Print CPProperty.name & ": " & CPProperty.Value
        If bFlg1 And bFlg2 Then Exit For
        If InStr(1, CPProperty.name, "User ID", vbTextCompare) > 0 Then
            CPProperty.Value = IIf(InStr(1, "DESA|TEST|", glENTORNO, vbTextCompare) > 0, "OdeGesPet", mdiPrincipal.AdoConnection.UsuarioAplicacion)
            bFlg1 = True
        ElseIf InStr(1, CPProperty.name, "Password", vbTextCompare) > 0 Then
            CPProperty.Value = IIf(InStr(1, "DESA|TEST|", glENTORNO, vbTextCompare) > 0, "eisenach", mdiPrincipal.AdoConnection.PasswordAplicacion & mdiPrincipal.AdoConnection.ComponenteSeguridad)
            bFlg2 = True
        End If
    Next
End Sub
