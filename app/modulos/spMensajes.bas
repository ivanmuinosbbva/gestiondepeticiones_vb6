Attribute VB_Name = "spMensajes"
' -001- a. - FJS 02.10.2007 - Se modifica el valor de CommandTimeOut porque en algunos casos esta cancelando (expira este per�odo de tiempo). Se prob� y en el caso del usuario A53790 se corrigi�.
'                             De todos modos, hay un problema de eficiencia en el SP sp_GetMensajeRecurso ya que tarda demasiado tiempo. Esta modificaci�n en la performance del SP queda pendiente.
' -002- a. - FJS 09.06.2008 - Se agrega un nuevo SP para agregar los mensajes personalizados para el grupo Homologador.
' -003- a. - FJS 11.09.2008 - Se agrega un nuevo SP para agregar los mensajes personalizados.

Option Explicit

Function sp_GetMensajeRecurso(cod_recurso, fecdesde, fechasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetMensajeRecurso"
      .CommandTimeout = 120     ' upd -001- a. - Se increment� el valor de 60 a 120
      .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, cod_recurso)
      .Parameters.Append .CreateParameter("@fecdesde", adVarChar, adParamInput, 8, fecdesde)
      .Parameters.Append .CreateParameter("@fechasta", adVarChar, adParamInput, 8, fechasta)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
     sp_GetMensajeRecurso = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetMensajeRecurso = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteMensaje(pet_nrointerno, msg_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteMensaje"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@msg_nrointerno", adInteger, adParamInput, , CLng(Val(msg_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_DeleteMensaje = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteMensaje = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DelMensajePerfil(pet_nrointerno, cod_perfil, cod_nivel, cod_area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelMensajePerfil"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_perfil", adVarChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_nivel", adVarChar, adParamInput, 4, cod_nivel)
      .Parameters.Append .CreateParameter("@cod_area", adVarChar, adParamInput, 8, cod_area)
      Set aplRST = .Execute
    End With
    sp_DelMensajePerfil = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelMensajePerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -002- a.
Function sp_DoMensajeHomologacion(pet_nrointerno, cod_perfil, cod_area, cod_txtmsg, cod_estado, txt_txtmsg) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DoMensajeHomologacion"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_perfil", adVarChar, adParamInput, 4, cod_perfil)
        .Parameters.Append .CreateParameter("@cod_area", adVarChar, adParamInput, 8, cod_area)
        .Parameters.Append .CreateParameter("@cod_txtmsg", adInteger, adParamInput, , CLng(Val(cod_txtmsg)))
        .Parameters.Append .CreateParameter("@cod_estado", adVarChar, adParamInput, 6, cod_estado)
        .Parameters.Append .CreateParameter("@txt_txtmsg", adVarChar, adParamInput, 250, txt_txtmsg)
        Set aplRST = .Execute
    End With
    sp_DoMensajeHomologacion = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DoMensajeHomologacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ add -003- a.
Function sp_DoMensajePersonalizado(pet_nrointerno, cod_txtmsg, txt_txtmsg) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DoMensajesPersonalizado"
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
        .Parameters.Append .CreateParameter("@cod_txtmsg", adInteger, adParamInput, , CInt(Val(cod_txtmsg)))
        .Parameters.Append .CreateParameter("@cod_txtmsg", adVarChar, adParamInput, 250, Trim(txt_txtmsg))
        Set aplRST = .Execute
    End With
    sp_DoMensajePersonalizado = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DoMensajePersonalizado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
