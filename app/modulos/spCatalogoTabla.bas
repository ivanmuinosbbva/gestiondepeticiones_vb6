Attribute VB_Name = "spCatalogoTabla"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertCatalogoTabla(id, nombreTabla, baseDeDatos, Esquema, DBMS, servidorNombre, servidorPuerto, app_id, datosSensibles) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertCatalogoTabla"
        .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , id)
        .Parameters.Append .CreateParameter("@nombreTabla", adChar, adParamInput, 40, nombreTabla)
        .Parameters.Append .CreateParameter("@baseDeDatos", adChar, adParamInput, 40, baseDeDatos)
        .Parameters.Append .CreateParameter("@esquema", adChar, adParamInput, 40, Esquema)
        .Parameters.Append .CreateParameter("@DBMS", adInteger, adParamInput, , DBMS)
        .Parameters.Append .CreateParameter("@servidorNombre", adChar, adParamInput, 40, servidorNombre)
        .Parameters.Append .CreateParameter("@servidorPuerto", adInteger, adParamInput, , servidorPuerto)
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@datosSensibles", adChar, adParamInput, 1, datosSensibles)
        Set aplRST = .Execute
    End With
    sp_InsertCatalogoTabla = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertCatalogoTabla = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateCatalogoTabla(id, nombreTabla, baseDeDatos, Esquema, DBMS, servidorNombre, servidorPuerto, app_id, datosSensibles) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateCatalogoTabla"
        .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , id)
        .Parameters.Append .CreateParameter("@nombreTabla", adChar, adParamInput, 40, nombreTabla)
        .Parameters.Append .CreateParameter("@baseDeDatos", adChar, adParamInput, 40, baseDeDatos)
        .Parameters.Append .CreateParameter("@esquema", adChar, adParamInput, 40, Esquema)
        .Parameters.Append .CreateParameter("@DBMS", adInteger, adParamInput, , DBMS)
        .Parameters.Append .CreateParameter("@servidorNombre", adChar, adParamInput, 40, servidorNombre)
        .Parameters.Append .CreateParameter("@servidorPuerto", adInteger, adParamInput, , servidorPuerto)
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, Mid(app_id, 1, 30))
        .Parameters.Append .CreateParameter("@datosSensibles", adChar, adParamInput, 1, datosSensibles)
        Set aplRST = .Execute
    End With
    sp_UpdateCatalogoTabla = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateCatalogoTabla = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteCatalogoTabla(id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteCatalogoTabla"
        .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , id)
        Set aplRST = .Execute
    End With
    sp_DeleteCatalogoTabla = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteCatalogoTabla = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetCatalogoTabla(id, dbname, appid, DDBBId) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetCatalogoTabla"
        .Parameters.Append .CreateParameter("@id", adInteger, adParamInput, , IIf(IsNull(id), Null, id))
        .Parameters.Append .CreateParameter("@dbname", adChar, adParamInput, 40, IIf(ClearNull(dbname) = "", Null, dbname))
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, IIf(ClearNull(appid) = "NULL", Null, appid))
        .Parameters.Append .CreateParameter("@DDBBId", adInteger, adParamInput, , IIf(IsNull(DDBBId) Or DDBBId = "", Null, DDBBId))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetCatalogoTabla = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetCatalogoTabla = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAllDataBases(DBMS) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetAllDataBases"
        .Parameters.Append .CreateParameter("@DBMS", adInteger, adParamInput, , IIf(IsNull(DBMS), Null, DBMS))
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetAllDataBases = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAllDataBases = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
