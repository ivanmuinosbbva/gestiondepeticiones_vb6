Attribute VB_Name = "mMain"
' *** SE FUERZA LA RECOMPILACION POR PROBLEMAS CON EL CHANGEMAN DS (29/08/2017) ***
' -001- a. FJS 19.06.2015 - Nuevo: se verifica primero que todo si se encuentra instalada alguna versi�n de MS Excel en el equipo.

Option Explicit

Sub Main()
    'Call InitCommonControlsVB      ' Habilitar esta l�nea para usar el tema del XP (Look & Feel)
    DoEvents
    Call Puntero(True)
    Call Inicializar
    glONLINE = False
    glFechaDesde = date
    glFechaHasta = date
    glEstadoQuery = ""
    glModoQuery = ""
    glArchivoINI = App.Path & "\" & ArchivoINI                              ' add -002- c.
    
    ' MS EXCEL: comprueba si existe instalado en el equipo que ejecuta el Microsoft Office
    Call Puntero(True)
    'Call setMSOfficeVersion
    
    Dim fso As New FileSystemObject
    
    If UCase(Dir(glArchivoINI)) <> "" Then
        glENTORNO = LeerINI("SISTEMA", "ENTORNO", "", glArchivoINI)
        glBASE = LeerINI("SISTEMA", "BASE", "DESA", glArchivoINI)
        glHEADAPLICACION = LeerINI("HEAD_" & glBASE, "APLICACION", "<aplicaci�n>", glArchivoINI)
        glHEADEMPPRESA = LeerINI("HEAD_" & glBASE, "EMPRESA", "<empresa>", glArchivoINI)
        glDRIVER_ODBC = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "DRIVER", glDRIVER_ODBC, glArchivoINI)
        glBASE_APLICACION = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "BASE_APLICACION", "", glArchivoINI)
        glBASE_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "BASE_SEGURIDAD", "", glArchivoINI)
        glSERVER_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "SERVIDOR_SEGURIDAD", "", glArchivoINI)
        glSERVER_DSN = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "SERVIDOR_DSN", "", glArchivoINI)            ' add *
        glSERVER_PUERTO = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "SERVIDOR_PUERTO", "", glArchivoINI)      ' add *
        glAPLICACION_SYBASE = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "APLICACION_SYBASE", "", glArchivoINI)
        glUSUARIO_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "USUARIO_SEGURIDAD", "", glArchivoINI)
        glESQUEMA_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "ESQUEMA_SEGURIDAD", "SI", glArchivoINI)
        glPASSWORD_ENCRIPTADO = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "PASSWORD_ENCRIPTADO", "0", glArchivoINI)
        glPASSWORD_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "PASSWORD_SEGURIDAD", "", glArchivoINI)
        glBASEUSR = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "ARG_STACK0", "", glArchivoINI)
        glBASEPWD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "ARG_STACK1", "", glArchivoINI)
        glWRKDIR = LeerINI("EXPORT", "WRKDIR", "c:\GesPet\", glArchivoINI)
        If InStr(UCase(glDRIVER_ODBC), "SYBASE") > 0 Then
            SQLDdateType = adDBDate
        ElseIf InStr(UCase(glDRIVER_ODBC), "SQL SERVER") > 0 Then
            SQLDdateType = adDBTimeStamp
        Else
            SQLDdateType = adDBDate
        End If
        On Error Resume Next
        
'        If UCase(glHEADEMPPRESA) <> "HOME" Then Call MkDir(glWRKDIR)        ' Se asegura que exista el directorio de trabajo por defecto
        ' Para reemplazar el directorio local de trabajo
        If Not fso.FolderExists(glWRKDIR) Then
            glWRKDIR = fso.CreateFolder(Environ$("TMP") & "\GesPet") & "\"
            'glWRKDIR = glWRKDIR & "\"
        End If
        On Error GoTo 0
        Call Puntero(True)
        Call IniciarMenues      ' TODO: ver si puede ejecutar ac� (antes de la carga del mdi)
        mdiPrincipal.Caption = mdiPrincipal.Caption & IIf(Len(Trim(glHEADEMPPRESA)) > 0, " (" & Trim(glHEADEMPPRESA) & ")", "")
        mdiPrincipal.Show
        Call setMSOfficeVersion
        'Call IniciarMenues
        'Call FuncionesGenerales.CaptureCommandLineArguments          ' add -002- c.
        If Not glONLINE Then
            frmLogin.Show 1
            Unload frmLogin
        End If
        If glONLINE Then
            If Not ValidarVersionDelAplicativo Then
                End
            End If
            '{ add 28.05.2013
            ' *** CONFIGURACI�N DE CORREO ELECTR�NICO ***
            If sp_GetVarios("SMTP") Then
                glSMTPNAME = ClearNull(aplRST.Fields!var_texto)
                glSMTPPORT = ClearNull(aplRST.Fields!var_numero)
                glSMTPAUTH = False
                glSMTPSSL = False
            End If
            If sp_GetVarios("MAILING") Then
                glSMTPMODO = ClearNull(aplRST.Fields!var_numero)
            End If
            If sp_GetVarios("MAILSNDR") Then
                glSMTPSNDR = ClearNull(aplRST.Fields!var_texto)
            End If
            '}
            'If estaRegistrada(Environ$("CommonProgramFiles") & "\Crystal Decisions\2.5\bin\crviewer.dll") Then
            ' *** CONFIGURACI�N DE VERSI�N DE CRYSTAL REPORT ***
            glCrystalNewVersion = False
            If Dir(Environ$("CommonProgramFiles") & "\Crystal Decisions\2.5\bin\crviewer.dll", vbArchive) <> "" Then
                glCrystalNewVersion = True
            ElseIf Dir(Environ$("CommonProgramFiles") & "\Crystal Decisions\2.5\crystalreportviewers10\ActiveXControls\crviewer.dll", vbArchive) <> "" Then
                glCrystalNewVersion = True
            End If
            bDebuggear = False
            If glENTORNO <> "PROD" Then
                If SoyElAnalista() Then
                    If sp_GetVarios("DEBUG") Then
                        If ClearNull(aplRST.Fields!var_numero) = "1" Then
                            If ClearNull(aplRST.Fields!var_texto) = ClearNull(cstmGetComputerName) Then
                                bDebuggear = True
                            End If
                        End If
                    End If
                End If
                Debug.Print "DEBUGGER: " & IIf(bDebuggear, "Activado", "Desactivado")
            End If
            ' Establezco el grupo homologador, el grupo de Seg. inf. y el grupo de tecnolog�a (variables globales)
            Call InicializarGruposDeControl
            If GetSetting("GesPet", "Preferencias\General", "IniciarTips", "N") = "S" Then
                Load frmTip
                If frmTip.iTipsCount > 0 Then
                    frmTip.Show 1
                Else
                     Unload frmTip
                End If
            End If
            Call InicializarObjetosComunes
        End If
    Else
        Call Puntero(False)
        Call Status("Listo.")
        MsgBox "Archivo de inicializaci�n inv�lido.", vbCritical, "Error de configuraci�n"
        End
    End If
End Sub

Private Sub Inicializar()
    Call Inicializar_Preferencias
End Sub

Private Sub Inicializar_Preferencias()
    Call DafaultExpor01
    Call DefaultPreferencias
    Call Default_Preferencias_IDM
End Sub
