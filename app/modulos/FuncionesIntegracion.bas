Attribute VB_Name = "FuncionesIntegracion"
Option Explicit

Global fIniPlan, fFinPlan, fIniReal, fFinReal, fPasajeProduccion, fFechaFinSuspension
Global hsPresup As Integer

Function chkEstadoGrupo(NumeroPeticion, xSector, xGrupo, sEstado As String, ByRef nCount As Integer) As Boolean
    'devuelve true si:
    'todos los grupos escrutados estan en alguno de los estados cuestionados
    '(si no existe ningun grupo tambien es true)
    nCount = 0
    On Error Resume Next
    chkEstadoGrupo = True
    If sp_GetPeticionGrupo(NumeroPeticion, xSector, xGrupo) Then
        Do While Not aplRST.EOF
            If sEstado <> "NULL" And InStr(1, sEstado, ClearNull(aplRST!cod_estado)) = 0 Then
                chkEstadoGrupo = False
            Else
                nCount = nCount + 1
            End If
            aplRST.MoveNext
        Loop
    End If
    aplRST.Close
    On Error GoTo 0
End Function

Function chkEstadoSector(NumeroPeticion, xSector, sEstado As String, ByRef nCount As Integer) As Boolean
    'devuelve true si:
    'todos los sectores escrutados estan en alguno de los estados cuestionados
    '(si no existe ningun sector tambien es true)
    nCount = 0
    On Error Resume Next
    chkEstadoSector = True
    If sp_GetPeticionSector(NumeroPeticion, xSector) Then
        Do While Not aplRST.EOF
            If sEstado <> "NULL" And InStr(1, sEstado, ClearNull(aplRST!cod_estado)) = 0 Then
                chkEstadoSector = False
            Else
                nCount = nCount + 1
            End If
            aplRST.MoveNext
        Loop
    End If
    aplRST.Close
    On Error GoTo 0
End Function

Function getIntegracionSector(NumeroPeticion, xSector, sEstado As String) As Boolean
    'los ESTIOK solo integran HOras
    'si existe algun PLANIF,ESTIMA,ESTIOK
    'o alguno que no tenga fechas planificadas y tenga estados INTEGRABLES
    '==> se nullea las fechas de integracion planifi,
    'porque de lo contrario estariamos presentando una fecha planificada
    'pero hay alguien que adeuda ese dato
    
    'aparentemente el estiok solo se usa prar computar las horas
    
    On Error Resume Next
    Dim auxHS As Integer
    fIniPlan = Null         ' Global: Fecha de inicio de planificaci�n a nivel Sector
    fFinPlan = Null         ' Global: Fecha de fin de planificaci�n a nivel Sector
    fIniReal = Null         ' Global: Fecha de inicio real a nivel Sector
    fFinReal = Null         ' Global: Fecha de fin real a nivel Sector
    hsPresup = 0            ' Horas presupuestadas a nivel Petici�n
    auxHS = 0
    Dim flgPlanif As Boolean
    Dim flgSkip As Boolean  ' Flag para saber si el grupo homologador es el �nico grupo del sector
    
    flgPlanif = False
    flgSkip = True
    
    ' Obtengo el total de horas presupuestadas a nivel de petici�n
    If sp_GetUnaPeticionSinCategoria(NumeroPeticion) Then  'RP Optimizacion
        If Not aplRST.EOF Then
            hsPresup = Val(ClearNull(aplRST!horaspresup))
        End If
    End If
    
    If sp_GetPeticionSector(NumeroPeticion, xSector) Then
        Do While Not aplRST.EOF
            If Val(aplRST!considerar) > 0 Then
                ' Si el estado del sector es alguno de estos:
                ' (frmPeticionesGrupoOperar / cmdConfirmar_Click) -->>
                ' "Esfuerzo estimado" o "Planificado" o "En ejecuci�n" o "Finalizado"
                If InStr(1, sEstado, ClearNull(aplRST!cod_estado)) > 0 Then
                    If ClearNull(aplRST!cod_estado) <> "ESTIOK" Then
                        If Not IsNull(aplRST!fe_ini_plan) Then
                            ' Si no tiene o la fecha actual de inicio de planificaci�n del sector es menor
                            ' a la misma fecha de otro sector, entonces tomo la fecha mayor y actualizo la
                            ' variable global
                            If IsNull(fIniPlan) Or (aplRST!fe_ini_plan < fIniPlan) Then
                                fIniPlan = aplRST!fe_ini_plan
                            End If
                        Else
                            ' El sector no tiene fecha de inicio de planificaci�n: hay que planificar (flag)
                            flgPlanif = True
                        End If
                        ' Si tiene la fecha de fin de planificaci�n...
                        If Not IsNull(aplRST!fe_fin_plan) Then
                            ' Si no tiene o la fecha actual de fin de planificaci�n del sector es menor
                            ' a la misma fecha de otro sector, entonces tomo la fecha mayor y actualizo la
                            ' variable global
                            If IsNull(fFinPlan) Or (aplRST!fe_fin_plan > fFinPlan) Then
                                fFinPlan = aplRST!fe_fin_plan
                            End If
                        Else
                            ' El sector no tiene fecha de fin de planificaci�n: hay que planificar (flag)
                            flgPlanif = True
                        End If
                        ' La misma evaluaci�n se hace para las fecha de inicio y fin real
                        If Not IsNull(aplRST!fe_ini_real) Then
                            If IsNull(fIniReal) Or (aplRST!fe_ini_real < fIniReal) Then
                                fIniReal = aplRST!fe_ini_real
                            End If
                        End If
                        If Not IsNull(aplRST!fe_fin_real) Then
                            If IsNull(fFinReal) Or (aplRST!fe_fin_real > fFinReal) Then
                                fFinReal = aplRST!fe_fin_real
                            End If
                        End If
                    End If
                    ' Si la petici�n ya tenia horas presupuestadas, entonces
                    ' adiciona las del sector actual
                    If Not IsNull(aplRST!horaspresup) Then
                        auxHS = auxHS + aplRST!horaspresup
                    End If
                    ' Actualiza la variable que contiene el total de horas presupuestadas
                    ' por petici�n
                    'If auxHS > hsPresup Then
                        hsPresup = auxHS
                    'End If
                End If
                ' Si el estado de alguno de los sectores es "A planificar" o "A estimar esfuerzo" o
                ' "Esfuerzo estimado", entonces a�n no puede estar planificado a nivel de sector
                ' (entonces inicializo las fechas de inicio y fin de planificaci�n a nivel de la
                ' petici�n)
                If ClearNull(aplRST!cod_estado) = "PLANIF" Or _
                   ClearNull(aplRST!cod_estado) = "ESTIMA" Or _
                   ClearNull(aplRST!cod_estado) = "ESTIOK" Then
                        flgPlanif = True
                End If
            End If
            DoEvents
            aplRST.MoveNext
        Loop
    End If
    ' Si alg�n sector a�n no estuvo "Planificado", entonces inicializo las fechas de
    ' planificaci�n para la petici�n
    If flgPlanif Then
        fIniPlan = Null
        fFinPlan = Null
    End If
    On Error GoTo 0
End Function

Function getIntegracionGrupo(NumeroPeticion, xSector, xGrupo, sEstado As String) As Boolean
    'los ESTIOK solo integran HOras
    'si existe algun PLANIF,ESTIMA,ESTIOK
    'o alguno que no tenga fechas planificadas y tenga estados INTEGRABLES
    '==> se nullea las fechas de integracion planifi,
    'porque de lo contrario estariamos presentando una fecha planificada
    'pero hay alguien que adeuda ese dato
    'aparentemente el estiok solo se usa prar computar las horas

    On Error Resume Next
    Dim auxHS As Integer
    fIniPlan = Null
    fFinPlan = Null
    fIniReal = Null
    fFinReal = Null
    hsPresup = 0
    auxHS = 0
    Dim flgPlanif As Boolean
    flgPlanif = False

    ' Determina si a nivel general en la petici�n ya existe horas presupuestadas de esfuerzo
    If sp_GetPeticionSector(NumeroPeticion, xSector) Then
        If Not aplRST.EOF Then
            hsPresup = Val(ClearNull(aplRST!horaspresup))
        End If
    End If

    If sp_GetPeticionGrupo(NumeroPeticion, xSector, xGrupo) Then
        Do While Not aplRST.EOF
            If InStr(1, "H|1|2|", ClearNull(aplRST!grupo_homologacion), vbTextCompare) = 0 Then
                If InStr(1, sEstado, ClearNull(aplRST!cod_estado)) > 0 Then
                    If ClearNull(aplRST!cod_estado) <> "ESTIOK" Then
                        If Not IsNull(aplRST!fe_ini_plan) Then
                            If IsNull(fIniPlan) Or (aplRST!fe_ini_plan < fIniPlan) Then
                                fIniPlan = aplRST!fe_ini_plan
                            End If
                        Else
                            flgPlanif = True
                        End If
                        If Not IsNull(aplRST!fe_fin_plan) Then
                            If IsNull(fFinPlan) Or (aplRST!fe_fin_plan > fFinPlan) Then
                                fFinPlan = aplRST!fe_fin_plan
                            End If
                        Else
                            flgPlanif = True
                        End If
                        If Not IsNull(aplRST!fe_ini_real) Then
                            If IsNull(fIniReal) Or (aplRST!fe_ini_real < fIniReal) Then
                                fIniReal = aplRST!fe_ini_real
                            End If
                        End If
                        If Not IsNull(aplRST!fe_fin_real) Then
                            If IsNull(fFinReal) Or (aplRST!fe_fin_real > fFinReal) Then
                                fFinReal = aplRST!fe_fin_real
                            End If
                        End If
                    End If
                    If Not IsNull(aplRST!horaspresup) Then
                        auxHS = auxHS + aplRST!horaspresup
                    End If
                    'If auxHS > hsPresup Then
                        hsPresup = auxHS
                    'End If
                End If
                'detectamos si existe algun planif o suspen
                If InStr(1, "PLANIF|ESTIMA|ESTIOK|", ClearNull(aplRST!cod_estado), vbTextCompare) > 0 Then
                    flgPlanif = True
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    aplRST.Close
    'si hubo algun planif, NO arrastra las fechas planok        ' TODO: ver esto!!!
    If flgPlanif Then
        fIniPlan = Null
        fFinPlan = Null
    End If
    On Error GoTo 0
End Function

Function getNuevoEstadoPeticion(xNumeroPeticion) As String
    Dim sEstadoAnterior As String
    Dim nEstadosector As Integer
    Dim sEstadoSector As String
    Dim sEstadoPeticion As String
    Dim flg1 As Boolean
    Dim flg2 As Boolean
    Dim flg3 As Boolean
    On Error Resume Next
    
    'If sp_GetUnaPeticion(xNumeroPeticion) Then
    If sp_GetUnaPeticionSinCategoria(xNumeroPeticion) Then 'RP Optimizacion
        sEstadoPeticion = ClearNull(aplRST.Fields!cod_estado)
    End If
    
    flg1 = False
    flg2 = False
    nEstadosector = 0
    sEstadoSector = ""
    
    ' Determinar el estado de la petici�n
    If sp_GetPeticionSectorXt(xNumeroPeticion, Null) Then
        Do While Not aplRST.EOF
            ' Aqu� la condici�n es:
            ' Si debo contemplar al sector (Homologaci�n) porque hay otros grupos, no evaluo nada.
            ' Si no debo tomarlo en cuenta, entonces pregunto por la igualdad (distinto del grupo Homologador...)
            'If flg3 Or (Not flg3 And glHomologacionSector <> ClearNull(aplRST.Fields!cod_sector)) Then  ' add -003- a.
            If Val(aplRST!especial) = 0 Or (Val(aplRST!especial) > 0 And Val(aplRST!considerar) > 0) Then
                If InStr(1, "TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then
                    flg1 = True
                End If
                'If InStr(1, "PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC", ClearNull(aplRST!cod_estado)) > 0 Then   ' del -004- a.
                If InStr(1, "PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|OPINIO|OPINOK|EVALUA|EVALOK|", ClearNull(aplRST!cod_estado)) > 0 Then   ' add -004- a.
                    flg2 = True
                End If
                If Val(ClearNull(aplRST!flg_rnkup)) > nEstadosector Then
                    nEstadosector = Val(ClearNull(aplRST!flg_rnkup))
                    sEstadoSector = ClearNull(aplRST!cod_estado)
                End If
            End If  ' add -003- a.
            DoEvents
            aplRST.MoveNext
        Loop
    End If
    On Error GoTo 0
    If flg1 = True And flg2 = True Then
        sEstadoSector = "EJECUC"
    End If
    getNuevoEstadoPeticion = IIf(sEstadoSector = "", sEstadoPeticion, sEstadoSector)
End Function

Function getNuevoEstadoGerencia(xNumeroPeticion, sGerencia) As String
    Dim sEstadoAnterior As String
    Dim nEstadoGerencia As Integer
    Dim sEstadoGerencia As String
    Dim flg1 As Boolean
    Dim flg2 As Boolean
    On Error Resume Next
    
    flg1 = False
    flg2 = False
    nEstadoGerencia = 0
    sEstadoGerencia = ""
 
    If sp_GetPeticionSectorXt(xNumeroPeticion, Null) Then
        Do While Not aplRST.EOF
            If ClearNull(aplRST!cod_gerencia) = sGerencia Then
                If InStr(1, "TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then
                    flg1 = True
                End If
                If InStr(1, "PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC", ClearNull(aplRST!cod_estado)) > 0 Then
                    flg2 = True
                End If
                If Val(ClearNull(aplRST!flg_rnkup)) > nEstadoGerencia Then
                    nEstadoGerencia = Val(ClearNull(aplRST!flg_rnkup))
                    sEstadoGerencia = ClearNull(aplRST!cod_estado)
                End If
            End If
            aplRST.MoveNext
        Loop
    End If
    aplRST.Close
    On Error GoTo 0
    If flg1 = True And flg2 = True Then
        sEstadoGerencia = "EJECUC"
    End If
    getNuevoEstadoGerencia = sEstadoGerencia
End Function

Function getNuevoEstadoSector(xNumeroPeticion, xSector) As String
    Dim sEstadoAnterior As String
    Dim nEstadosector As Integer
    Dim sEstadoSector As String
    Dim flg1 As Boolean
    Dim flg2 As Boolean
    On Error Resume Next
    
    flg1 = False        ' Flag para determinar si existe alg�n grupo en estado "Finalizado"
    flg2 = False        ' Flag para determinar si existe alg�n grupo en estados:
                        
                        ' - Planificado             RnkUp = 35
                        ' - A planificar            RnkUp = 45
                        ' - Esfuerzo estimado       RnkUp = 40
                        ' - A estimar esfuerzo      RnkUp = 50
                        
                        ' - En ejecuci�n            RnkUp = 55
                        
                        ' - En evaluaci�n           RnkUp = 75
                        ' - Con evaluaci�n          RnkUp = 65
                        ' - En opini�n              RnkUp = 75
                        ' - Con opini�n             RnkUp = 65
                        
    ' Inicializa los estados (sEstadoSector) y ranking de estados (nEstadosector) del sector
    nEstadosector = 0
    sEstadoSector = ""
 
    ' Devuelve los grupos y sus respectivos estados para el sector actual
    If sp_GetPeticionGrupoXt(xNumeroPeticion, xSector, Null) Then
        ' Recorriendo todos los grupos del sector...
        Do While Not aplRST.EOF
            If InStr(1, "H|1|2|", ClearNull(aplRST!grupo_homologacion), vbTextCompare) = 0 Then
                If InStr(1, "TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then        ' Existe al menos un grupo en estado "Finalizado"
                    flg1 = True
                End If
                If InStr(1, "PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|OPINIO|OPINOK|EVALUA|EVALOK|", ClearNull(aplRST!cod_estado)) > 0 Then ' Existe al menos un grupo en alguno de los otros estados
                    flg2 = True
                End If
                If Val(ClearNull(aplRST!flg_rnkup)) > nEstadosector Then            ' El estado del grupo con mayor valor en "RnkUp" determina el estado actual del sector
                    nEstadosector = Val(ClearNull(aplRST!flg_rnkup))
                    sEstadoSector = ClearNull(aplRST!cod_estado)
                End If
            End If
            aplRST.MoveNext
            DoEvents
        Loop
    End If
    '{ add -002- a.
    ' Si no pudo determinar el estado del sector, es porque solo existe el grupo Homologaci�n
    ' Determino el nuevo estado del sector, entonces, como si Homologaci�n fuera un grupo de DyD
    ' como cualquier otro
    If Len(sEstadoSector) = 0 Then
        If sp_GetPeticionGrupoXt(xNumeroPeticion, xSector, Null) Then
            Do While Not aplRST.EOF
                If InStr(1, "TERMIN", ClearNull(aplRST!cod_estado)) > 0 Then        ' El grupo est� en estado "Finalizado"
                    flg1 = True
                End If
                If InStr(1, "PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|OPINIO|OPINOK|EVALUA|EVALOK|", ClearNull(aplRST!cod_estado)) > 0 Then    ' El grupo se encuentra en alguno de los otros estados
                    flg2 = True
                End If
                ' El estado del grupo con mayor valor en "RnkUp" determina el estado actual del sector
                If Val(ClearNull(aplRST!flg_rnkup)) > nEstadosector Then
                    nEstadosector = Val(ClearNull(aplRST!flg_rnkup))
                    sEstadoSector = ClearNull(aplRST!cod_estado)
                End If
                aplRST.MoveNext
                DoEvents
            Loop
        End If
    End If
    '}
    On Error GoTo 0
    ' Si al menos un grupo ha finalizado y los dem�s se encuentran en alguno de los estados
    ' mencionados anteriormente, el estado del sector permanece "En ejecuci�n" (si alguno ha
    ' finalizado ya, ya estuvo anteriormente en ejecuci�n)
    ' Todo lo anterior vale sin tener en cuenta el estado del grupo/sector Homologaci�n     ' add -002- a.
    If flg1 = True And flg2 = True Then
        sEstadoSector = "EJECUC"
    End If
    getNuevoEstadoSector = sEstadoSector
End Function

Function chkGrupoOpiEva(NumeroPeticion, xSector) As Boolean
    'reacomoda el estado del sector
    'para los casos puntuales de OPINOK y EVALOK
    Dim sSituacion As String
    Dim flg1 As Boolean
    Dim flg2 As Boolean
    flg1 = False
    flg2 = False
    
    On Error Resume Next
    If sp_GetPeticionSector(NumeroPeticion, xSector) Then
        If Not aplRST.EOF Then
            glEstadoSector = ClearNull(aplRST!cod_estado)
            sSituacion = ClearNull(aplRST!cod_situacion)
        End If
    End If
    aplRST.Close
    
    Select Case glEstadoSector
        Case "OPINIO"   ' Estado: "En opini�n..."
            If sp_GetPeticionGrupoXt(NumeroPeticion, xSector, Null) Then
                Do While Not aplRST.EOF
                    If InStr(1, "OPINOK", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg1 = True
                    End If
                    If InStr(1, "OPINIO", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg2 = True
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            aplRST.Close
            If flg1 = True And flg2 = False And sSituacion = "" Then
                Call sp_UpdatePetSecField(glNumeroPeticion, xSector, "SITUAC", "OPINOK", Null, Null)
            End If
        Case "EVALUA"   ' Estado: "En evaluaci�n..."
            If sp_GetPeticionGrupoXt(NumeroPeticion, xSector, Null) Then
                Do While Not aplRST.EOF
                    If InStr(1, "EVALOK", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg1 = True
                    End If
                    If InStr(1, "EVALUA", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg2 = True
                    End If
                    aplRST.MoveNext
                    DoEvents
                Loop
            End If
            aplRST.Close
            If flg1 = True And flg2 = False And sSituacion = "" Then
                Call sp_UpdatePetSecField(glNumeroPeticion, xSector, "SITUAC", "EVALOK", Null, Null)
            End If
    End Select
End Function

Function chkSectorOpiEva(NumeroPeticion) As Boolean
    'reacomoda el estado de la peticion
    'para los casos puntuales de OPINOK y EVALOK
    Dim sSituacion As String
    Dim sGestion As String
    Dim flg1 As Boolean
    Dim flg2 As Boolean
    
    flg1 = False
    flg2 = False
    
    On Error Resume Next
    If sp_GetUnaPeticion(NumeroPeticion) Then
        If Not aplRST.EOF Then
            glEstadoPeticion = ClearNull(aplRST!cod_estado)
            sSituacion = ClearNull(aplRST!cod_situacion)
            sGestion = IIf(ClearNull(aplRST.Fields!pet_driver) = "", "DESA", ClearNull(aplRST.Fields!pet_driver))
        End If
    End If
    
    Select Case glEstadoPeticion
        Case "OPINIO"
            If sp_GetPeticionSectorXt(NumeroPeticion, Null) Then
                Do While Not aplRST.EOF
                    If InStr(1, "OPINOK", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg1 = True
                    End If
                    If InStr(1, "OPINIO", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg2 = True
                    End If
                    aplRST.MoveNext
                Loop
            End If
            If flg2 = False Then
                glEstadoPeticion = IIf(sGestion = "DESA", "COMITE", "REFBPE")
                Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", glEstadoPeticion, Null, Null)
                If flg1 = True Then
                    Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "OPINOK", Null, Null)
                End If
            End If
        Case "EVALUA"
            If sp_GetPeticionSectorXt(NumeroPeticion, Null) Then
                Do While Not aplRST.EOF
                    If InStr(1, "EVALOK", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg1 = True
                    End If
                    If InStr(1, "EVALUA", ClearNull(aplRST!cod_estado)) > 0 Then
                        flg2 = True
                    End If
                    aplRST.MoveNext
                Loop
            End If
            If flg2 = False Then
                glEstadoPeticion = IIf(sGestion = "DESA", "COMITE", "REFBPE")
                Call sp_UpdatePetField(glNumeroPeticion, "ESTADO", glEstadoPeticion, Null, Null)
                If flg1 = True Then
                    Call sp_UpdatePetField(glNumeroPeticion, "SITUAC", "EVALOK", Null, Null)
                End If
            End If
    End Select
End Function
