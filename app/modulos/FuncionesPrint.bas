Attribute VB_Name = "FuncionesPrint"
' -001- a. FJS 11.09.2008 - Se agregan los datos a la impresi�n de: Tipo, Clase, Impacto y Regulatorio.

Option Explicit

Dim nBegCol() As Long

Const MargenIzq = 20
Const OffX = 50
Const OffY = 40

Sub PrintFormulario(xNumeroPeticion)
    Dim memDescripcion As String
    Dim memCaracteristicas As String
    Dim memMotivos As String
    Dim memBeneficios As String
    Dim memRelaciones As String
    Dim memObservaciones As String
    
    Dim txtAux As String
    Dim sTitulo As String
    Dim HalfWidth, HalfHeight, sTxt
    Dim nAux As Integer
    Dim aSector()
    Dim hAux As Integer
    Dim i As Integer
    
    
    memDescripcion = sp_GetMemo(xNumeroPeticion, "DESCRIPCIO")
    memCaracteristicas = sp_GetMemo(xNumeroPeticion, "CARACTERIS")
    memMotivos = sp_GetMemo(xNumeroPeticion, "MOTIVOS")
    memBeneficios = sp_GetMemo(xNumeroPeticion, "BENEFICIOS")
    memRelaciones = sp_GetMemo(xNumeroPeticion, "RELACIONES")
    memObservaciones = sp_GetMemo(xNumeroPeticion, "OBSERVACIO")
    
    If Not sp_GetUnaPeticionXt(xNumeroPeticion) Then Exit Sub
    If aplRST.EOF Then
        Exit Sub
    End If
    
    Call Status("Imprimiendo formulario...")
    
    On Error GoTo ErrorHandler
    
    PrintCab
    
    ReDim nBegCol(2)
    nBegCol(1) = 0
    nAux = ((Printer.ScaleWidth - MargenIzq) / 7)
    ReDim nBegCol(3)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 6
    Printer.FontSize = 10
    Printer.FontName = "Arial"
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("T�tulo del Pedido", True, 1, 1, True)
    Call PrintText("N�mero", True, 1, 2, False)
    Printer.FontBold = True
    Printer.FontItalic = False
    Call PrintText(ClearNull(aplRST!Titulo), True, 0, 1, True)
    Call PrintText(IIf(Val(ClearNull(aplRST!pet_nroasignado)) > 0, ClearNull(aplRST!pet_nroasignado), "S/N"), True, 0, 2, False)
    Printer.FontBold = False
    Printer.FontSize = 9
    Call PrintText("ESTADO: " & ClearNull(aplRST!nom_estado), True, 0, 1, True)
    Call PrintText("CAT.: " & ClearNull(aplRST!prioridad), True, 0, 2, False)
    '{ add -001- a.
    Call PrintText("TIPO: " & ClearNull(aplRST!nom_tipo_peticion), True, 0, 1, True)
    If ClearNull(aplRST!pet_regulatorio) = "-" Then
        Call PrintText("REG.: " & "--", True, 0, 2, False)
    ElseIf ClearNull(aplRST!pet_regulatorio) = "N" Then
        Call PrintText("REG.: " & "No", True, 0, 2, False)
    Else
        Call PrintText("REG.: " & "Si", True, 0, 2, False)
    End If
    Call PrintText("CLASE: " & ClearNull(aplRST!nom_clase), True, 0, 1, True)
    If ClearNull(aplRST!pet_imptech) = "-" Then
        Call PrintText("I.T.: " & "--", True, 0, 2, False)
    ElseIf ClearNull(aplRST!pet_imptech) = "N" Then
        Call PrintText("I.T.: " & "No", True, 0, 2, False)
    Else
        Call PrintText("I.T.: " & "Si", True, 0, 2, False)
    End If
    '}
    Skip
    
    
    ReDim nBegCol(2)
    nBegCol(1) = 0
    Printer.FontSize = 10
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Area Solicitante", True, 2, 1, True)
    Printer.FontBold = False
    Printer.FontItalic = False
    'Call PrintText(ClearNull(aplRST!nom_direccion) & " >> " & ClearNull(aplRST!nom_gerencia) & " >> " & ClearNull(aplRST!nom_sector), True, 0, 1, True)
    Call PrintText(ClearNull(aplRST!nom_direccion) & " � " & ClearNull(aplRST!nom_gerencia) & " � " & ClearNull(aplRST!nom_sector), True, 0, 1, True)
    
    ReDim nBegCol(2)
    nBegCol(1) = 0
    Printer.FontSize = 10
    Printer.FontBold = True
    Printer.FontItalic = True
    'Call PrintText("Business Partner", True, 2, 1, True)
    Call PrintText("Referente de Sistema", True, 2, 1, True)
    Printer.FontBold = False
    Printer.FontItalic = False
    Call PrintText(ClearNull(aplRST!nom_bpar), True, 0, 1, True)
    
    
    nAux = ((Printer.ScaleWidth - MargenIzq) / 7)
    ReDim nBegCol(4)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 3
    nBegCol(3) = nBegCol(2) + nAux * 3
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Solicitante", True, 1, 1, True)
    Call PrintText("Supervisor", True, 1, 2, False)
    Call PrintText("F.Pedido", True, 1, 3, False)
    Printer.FontBold = False
    Printer.FontItalic = False
    Call PrintText(ClearNull(aplRST!nom_solicitante), True, 0, 1, True)
    Call PrintText(ClearNull(aplRST!nom_supervisor), True, 0, 2, False)
    Call PrintText(IIf(Not IsNull(aplRST!fe_pedido), Format(aplRST!fe_pedido, "dd/mm/yyyy"), ""), True, 0, 3, False)
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Referente", True, 1, 1, True)
    Call PrintText("Autorizante", True, 1, 2, False)
    Call PrintText("Fecha deseada", True, 1, 3, False)
    Printer.FontBold = False
    Printer.FontItalic = False
    Call PrintText(ClearNull(aplRST!nom_referente), True, 0, 1, True)
    Call PrintText(ClearNull(aplRST!nom_director), True, 0, 2, False)
    Call PrintText(IIf(Not IsNull(aplRST!fe_requerida), Format(aplRST!fe_requerida, "dd/mm/yyyy"), ""), True, 0, 3, False)
    Skip
    
    ReDim nBegCol(2)
    nBegCol(1) = 0
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Areas Comprometidas", True, 2, 1, True)
    
    nAux = ((Printer.ScaleWidth - MargenIzq) / 20)
    ReDim nBegCol(8)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 9
    nBegCol(3) = nBegCol(2) + nAux * 2
    nBegCol(4) = nBegCol(3) + nAux * 2
    nBegCol(5) = nBegCol(4) + nAux * 1
    nBegCol(6) = nBegCol(5) + nAux * 4
    nBegCol(7) = nBegCol(6) + nAux * 2
    Printer.FontBold = True
    Printer.FontItalic = True
    Printer.FontSize = 8
    Call PrintText("Sector", True, 1, 1, True)
    Call PrintText("F.Inicio Plan.", True, 1, 2, False)
    Call PrintText("F.Fin Plan.", True, 1, 3, False)
    Call PrintText("Horas", True, 1, 4, False)
    Call PrintText("Estado", True, 1, 5, False)
    
    ReDim aSector(1 To 1)
    hAux = 0
    
    Printer.FontBold = False
    Printer.FontItalic = False
    If sp_GetPeticionSectorXt(xNumeroPeticion, Null) Then
        Do While Not aplRST.EOF
            hAux = hAux + 1
            ReDim Preserve aSector(1 To nAux)
            aSector(hAux) = ClearNull(aplRST!cod_sector)
            aplRST.MoveNext
        Loop
        aplRST.Close
    End If
    
    For i = 1 To hAux
        If sp_GetPeticionSectorXt(xNumeroPeticion, aSector(i)) Then
            Call PrintText(ClearNull(aplRST!nom_gerencia) & " : " & ClearNull(aplRST!nom_sector), True, 0, 1, True)
            Call PrintText(IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), ""), True, 0, 2, False)
            Call PrintText(IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), ""), True, 0, 3, False)
            Call PrintText(ClearNull(aplRST!horaspresup), True, 0, 4, False)
            Call PrintText(ClearNull(aplRST!nom_estado), True, 0, 5, False)
            Call PrintText(ClearNull(aplRST!nom_situacion), True, 0, 6, False)
            aplRST.Close
            If sp_GetPeticionGrupoXt(xNumeroPeticion, aSector(i), Null) Then
                Do While Not aplRST.EOF
                    Call PrintText("   " & ClearNull(aplRST!nom_grupo), True, 0, 1, True)
                    Call PrintText(IIf(Not IsNull(aplRST!fe_ini_plan), Format(aplRST!fe_ini_plan, "dd/mm/yyyy"), ""), True, 0, 2, False)
                    Call PrintText(IIf(Not IsNull(aplRST!fe_fin_plan), Format(aplRST!fe_fin_plan, "dd/mm/yyyy"), ""), True, 0, 3, False)
                    Call PrintText(ClearNull(aplRST!horaspresup), True, 0, 4, False)
                    Call PrintText(ClearNull(aplRST!nom_estado), True, 0, 5, False)
                    Call PrintText(" ", True, 0, 6, False)
                    aplRST.MoveNext
                Loop
                aplRST.Close
            End If
        End If
    Next i
    
    
    Skip
    
    
    ReDim nBegCol(2)
    nBegCol(1) = 0
    Printer.FontSize = 9
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Descripci�n del Pedido", True, 1, 1, True)
    Printer.FontBold = False
    Printer.FontItalic = False
    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
    OutTab memDescripcion
    Call PrintText(memDescripcion, False, 0, 1, True, True)
    Skip
    
    Printer.FontSize = 9
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Caracter�sticas Principales", True, 1, 2, True)
    Printer.FontBold = False
    Printer.FontItalic = False
    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
    OutTab memCaracteristicas
    Call PrintText(memCaracteristicas, False, 0, 1, True, True)
    Skip
    
    Printer.FontSize = 9
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Motivo del Requerimiento", True, 1, 2, True)
    Printer.FontItalic = False
    Printer.FontBold = False
    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
    OutTab memMotivos
    Call PrintText(memMotivos, False, 0, 1, True, True)
    Skip
    
    Printer.FontSize = 9
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Beneficios Esperados", True, 1, 2, True)
    Printer.FontBold = False
    Printer.FontItalic = False
    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
    OutTab memBeneficios
    Call PrintText(memBeneficios, False, 0, 1, True, True)
    Skip
    
    Printer.FontSize = 9
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Relaci�n con otros Requerimientos", True, 1, 2, True)
    Printer.FontBold = False
    Printer.FontItalic = False
    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
    OutTab memRelaciones
    Call PrintText(memRelaciones, False, 0, 1, True, True)
    Skip
    
    Printer.FontSize = 9
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("Observaciones", True, 1, 2, True)
    Printer.FontBold = False
    Printer.FontItalic = False
    nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
    OutTab memObservaciones
    Call PrintText(memObservaciones, False, 0, 1, True, True)
    Skip
    
    PrintPie (False)
    Printer.EndDoc
    Call Status("Listo.")
Exit Sub
ErrorHandler:
    On Error GoTo 0
    MsgBox "Error con la impresora."
    Printer.EndDoc
    Exit Sub
End Sub

Sub PrintHitorial(xNumeroPeticion)
    Dim txtAux As String
    Dim txtHst As String
    Dim sTitulo As String
    Dim HalfWidth, HalfHeight, sTxt
    Dim nAux As Integer
    Dim hAux, i As Integer
    Dim aHisto()
    
    If Not sp_GetUnaPeticionXt(xNumeroPeticion) Then Exit Sub
    If aplRST.EOF Then
        Exit Sub
    End If
    
    Call Status("Imprimiendo historial...")
    
    On Error GoTo ErrorHandler
    
    PrintCab
    
    ReDim nBegCol(2)
    nBegCol(1) = 0
    nAux = ((Printer.ScaleWidth - MargenIzq) / 7)
    ReDim nBegCol(3)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 6
    Printer.FontSize = 10
    Printer.FontName = "Arial"
    Printer.FontBold = True
    Printer.FontItalic = True
    Call PrintText("HISTORIAL", True, 1, 1, True)
    Call PrintText("Titulo del Pedido", True, 1, 1, True)
    Call PrintText("N�mero", True, 1, 2, False)
    Printer.FontBold = True
    Printer.FontItalic = False
    Call PrintText(ClearNull(aplRST!Titulo), True, 0, 1, True)
    Call PrintText(IIf(Val(ClearNull(aplRST!pet_nroasignado)) > 0, ClearNull(aplRST!pet_nroasignado), "S/N"), True, 0, 2, False)
    Printer.FontBold = False
    Printer.FontSize = 9
    Call PrintText("ESTADO: " & ClearNull(aplRST!nom_estado), True, 0, 1, True)
    Call PrintText("Cat.: " & ClearNull(aplRST!prioridad), True, 0, 2, False)
    
    aplRST.Close
    Skip
    
    ReDim aHisto(1 To 1)
    hAux = 0
    
    
    If Not sp_GetHistorialXt(xNumeroPeticion, Null) Then GoTo Final
    If aplRST.EOF Then GoTo Final
    
    Do While Not aplRST.EOF
        hAux = hAux + 1
        ReDim Preserve aHisto(1 To nAux)
        aHisto(hAux) = ClearNull(aplRST!hst_nrointerno)
        aplRST.MoveNext
    Loop
    aplRST.Close
    
    Call Status("Imprimiendo historial...")
    
    nAux = ((Printer.ScaleWidth - MargenIzq) / 64)
    ReDim nBegCol(8)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 6
    nBegCol(3) = nBegCol(2) + nAux * 8
    nBegCol(4) = nBegCol(3) + nAux * 8
    nBegCol(5) = nBegCol(4) + nAux * 7
    nBegCol(6) = nBegCol(5) + nAux * 12
    nBegCol(7) = nBegCol(6) + nAux * 6
    nBegCol(8) = nBegCol(7) + nAux * 11
    Printer.FontBold = True
    Printer.FontItalic = True
    Printer.FontSize = 8
    Call PrintText("Fecha", True, 1, 1, True)
    Call PrintText("Acci�n", True, 1, 2, False)
    Call PrintText("Usuario", True, 1, 3, False)
    Call PrintText("Estado Petic.", True, 1, 4, False)
    Call PrintText("Sector", True, 1, 5, False)
    Call PrintText("Est. Sector", True, 1, 6, False)
    Call PrintText("Grupo", True, 1, 7, False)
    Call PrintText("Est. Grup.", True, 1, 8, False)
    
    For i = 1 To hAux
        If sp_GetHistorialXt(xNumeroPeticion, aHisto(i)) Then
            
            Printer.FontBold = False
            Printer.FontItalic = False
           
            nAux = ((Printer.ScaleWidth - MargenIzq) / 64)
            ReDim nBegCol(8)
            nBegCol(1) = 0
            nBegCol(2) = nAux * 6
            nBegCol(3) = nBegCol(2) + nAux * 8
            nBegCol(4) = nBegCol(3) + nAux * 8
            nBegCol(5) = nBegCol(4) + nAux * 7
            nBegCol(6) = nBegCol(5) + nAux * 12
            nBegCol(7) = nBegCol(6) + nAux * 6
            nBegCol(8) = nBegCol(7) + nAux * 11
            Printer.FontBold = False
            Printer.FontItalic = False
            Printer.FontSize = 8
            Call PrintText(IIf(Not IsNull(aplRST!hst_fecha), Format(aplRST!hst_fecha, "dd/mm/yyyy"), ""), True, 0, 1, True)
            Call PrintText(ClearNull(aplRST!nom_accion), True, 0, 2, False)
            Call PrintText(ClearNull(aplRST!nom_audit), True, 0, 3, False)
            Call PrintText(ClearNull(aplRST!pet_nom_estado), True, 0, 4, False)
            Call PrintText(ClearNull(aplRST!nom_sector), True, 0, 5, False)
            Call PrintText(ClearNull(aplRST!sec_nom_estado), True, 0, 6, False)
            Call PrintText(ClearNull(aplRST!nom_grupo), True, 0, 7, False)
            Call PrintText(ClearNull(aplRST!gru_nom_estado), True, 0, 8, False)
            
            aplRST.Close
        
            ReDim nBegCol(2)
            nBegCol(1) = -OffX 'es una truchada para que no tenga el offset en el 1er char
            Printer.FontSize = 9
            txtHst = sp_GetHistorialMemo(ClearNull(aHisto(i)))
            Call PrintText(ClearNull(txtHst), False, 0, 1, True, True)
            Skip
            'aplRST.Close
    
        End If
    Next
Final:
    PrintPie (False)
    Printer.EndDoc
    Call Status("Listo.")

    Exit Sub
ErrorHandler:
    On Error GoTo 0
    MsgBox "Error con la impresora."
    Printer.EndDoc
    Exit Sub
End Sub

Private Function Token(tmp$, search$) As String
    Dim x As Integer
    x = InStr(1, tmp$, search$)
    If x Then
       Token = Mid$(tmp$, 1, x - 1)
       tmp$ = Mid$(tmp$, x + 1)
    Else
       Token = tmp$
       tmp$ = ""
    End If
End Function

Private Sub PrintText(ByVal LongText As String, ByVal Boxed As Boolean, ByVal nShadow As Integer, ByVal nCol As Integer, ByVal NewLine As Boolean, Optional wrdWrap)
    Dim BegY As Long
    Dim EndY As Long
    Static LastY As Long
    Dim Word As String
    Dim EdgeX As Long
    
    If IsMissing(wrdWrap) Then
        wrdWrap = False
    End If
    'wrdWrap = True


    If NewLine = False Then
        Printer.CurrentY = LastY
    End If
    BegY = Printer.CurrentY

    If nCol = UBound(nBegCol) Then
        EdgeX = Printer.ScaleWidth
    ElseIf nBegCol(nCol + 1) = 0 Then
        EdgeX = Printer.ScaleWidth
    Else
        EdgeX = nBegCol(nCol + 1)
    End If
    
    Printer.CurrentX = nBegCol(nCol) + OffX
    Printer.CurrentY = BegY + OffY

    If Boxed = True Then
        PrintLongText LongText, EdgeX, wrdWrap, True
        EndY = Printer.CurrentY + OffY
        Select Case nShadow
        Case 0
            Printer.DrawWidth = 2
            Printer.FillStyle = 1
            Printer.FillColor = RGB(255, 255, 255)
        Case 1
            Printer.DrawWidth = 10
            Printer.FillStyle = 0
            Printer.FillColor = RGB(192, 192, 192)
        Case 2
            Printer.DrawWidth = 15
            Printer.FillStyle = 0
            Printer.FillColor = RGB(127, 127, 127)
        End Select
        Printer.Line (nBegCol(nCol), BegY)-((Printer.ScaleWidth - MargenIzq), EndY), , B
        Printer.CurrentX = nBegCol(nCol) + OffX
        Printer.CurrentY = BegY + OffY
    End If


    PrintLongText LongText, EdgeX, wrdWrap, False

    EndY = Printer.CurrentY + OffY

    Printer.FillStyle = 1
    Printer.CurrentY = EndY
    LastY = BegY
    Printer.CurrentX = 0

End Sub

Private Sub Skip()
    Printer.CurrentY = Printer.CurrentY + 100
End Sub

Private Function PrintLongText(ByVal LongText As String, ByVal EdgeX As Long, ByVal wrdWrap As Boolean, ByVal bFlgTransp As Boolean) As Boolean
    Dim BegY As Long
    Dim EndY As Long
    Dim i As Integer
    Dim Word As String
    Dim auxCurrentY As Integer
    Dim maxCurrentY As Integer
    If bFlgTransp = True Then
        Printer.ForeColor = QBColor(7)
    End If
    PrintLongText = True
    If wrdWrap = True Then
        Do Until LongText = ""
              Word = Token(LongText, " ")
              If Printer.TextWidth(Word) + Printer.CurrentX > (EdgeX - MargenIzq) - Printer.TextWidth("Z") Then
                'hacer el corte de renglon
                 Printer.Print
                 'Printer.CurrentX = OffX
              End If
              If Printer.CurrentY + (Printer.TextHeight("ZZZ") * 4) + 300 >= Printer.ScaleHeight Then
                'hacer el corte de pagina
                 PrintPie (True)
                 PrintCab
                 'Printer.CurrentX = OffX
              End If
              Printer.Print Word + " ";
        Loop
    Else
        Word = LongText
        i = Len(Word)
        Do Until i = 0
              Word = Mid(LongText, 1, i)
              If Printer.TextWidth(Word) + Printer.CurrentX > (EdgeX - MargenIzq) - Printer.TextWidth("I") Then
                 i = i - 1
              Else
                 Printer.Print Word;
                 i = 0
              End If
        Loop
    End If
    Printer.Print
    Printer.ForeColor = QBColor(0)
End Function

Private Sub PrintPie(bFlgPage As Boolean)

Dim nAux As Integer
Dim xFntName As String
Dim xFntBold As Boolean
Dim xFntItalic As Boolean
Dim xFntSize As Integer

xFntName = Printer.FontName
xFntBold = Printer.FontBold
xFntItalic = Printer.FontItalic
xFntSize = Printer.FontSize

nAux = ((Printer.ScaleWidth - MargenIzq) / 10)

Printer.CurrentX = 0
Printer.CurrentY = Printer.ScaleHeight - Printer.TextHeight("ZZZ") - 10

Printer.FontName = "Times New Roman"
Printer.FontBold = False
Printer.FontItalic = False
Printer.FontSize = 8
Printer.Print "Fecha: " & Format(date, "dd/mm/yyyy")

Printer.CurrentX = (Printer.ScaleWidth - MargenIzq) - Printer.TextWidth("Hoja: " & Printer.Page) - 10
Printer.CurrentY = Printer.ScaleHeight - Printer.TextHeight("ZZZ") - 10
Printer.Print "Hoja: " & Printer.Page

Printer.FontName = xFntName
Printer.FontBold = xFntBold
Printer.FontItalic = xFntItalic
Printer.FontSize = xFntSize

If bFlgPage = True Then
    Printer.NewPage
End If
End Sub

Private Sub PrintCab()
    Dim nAuxY
    Dim nAux As Integer
    Dim sTxt As String
    Dim xFntName As String
    Dim xFntBold As Boolean
    Dim xFntItalic As Boolean
    Dim xFntSize As Integer
    Dim BegY As Long
    Dim EndY As Long
    
    xFntName = Printer.FontName
    xFntBold = Printer.FontBold
    xFntItalic = Printer.FontItalic
    xFntSize = Printer.FontSize
    
    Printer.CurrentX = 0
    Printer.CurrentY = 0
    Skip
    Skip
    Skip
    
    nAuxY = Printer.CurrentY
    nAux = ((Printer.ScaleWidth - MargenIzq) / 10)
    ReDim nBegCol(2)
    nBegCol(1) = 0
    nBegCol(2) = nAux * 7
    Printer.FontName = "Times New Roman"
    Printer.FontBold = True
    Printer.FontItalic = False
    Printer.FontSize = 10
    Printer.Print glHEADEMPPRESA
    
    Printer.CurrentY = nAuxY
    Printer.FontBold = False
    Printer.FontItalic = False
    Printer.FontSize = 8
    sTxt = glHEADAPLICACION
    Printer.CurrentX = (Printer.ScaleWidth - MargenIzq) - Printer.TextWidth(sTxt) - 40
    Printer.Print sTxt
    
    sTxt = "DIRECCION DE MEDIOS - ADMINISTRACION DE PEDIDOS"
    Printer.FontSize = 10
    Printer.FontBold = True
    Printer.CurrentX = (Printer.ScaleWidth - MargenIzq) / 2 - Printer.TextWidth(sTxt) / 2
    Printer.Print sTxt
    Skip
    Skip
    Printer.Line (0, Printer.CurrentY)-((Printer.ScaleWidth - MargenIzq), Printer.CurrentY)
    Skip
    Printer.Print
    BegY = Printer.CurrentY
    
    Printer.FontName = xFntName
    Printer.FontBold = xFntBold
    Printer.FontItalic = xFntItalic
    Printer.FontSize = xFntSize
End Sub
