Attribute VB_Name = "CargaConsol"
Option Explicit

Sub LoadConsolidar()
    Const colNROASI = 1
    Const colTIPO = 2
    Const colTitulo = 3
    Const colDESCRI = 4
    'Const colPRIORI = 4
    Const colFECALT = 5
    Const colFECREQ = 7
    'Const colFECCOM = 7
    Const colOBSERV = 8
    Const colSector = 9
    Const colSOLICI = 10
    Const colREFERE = 12
    Const colAUTORI = 14
    Const colEJEC_SEC = 16
    Const colEJEC_GRU = 17
    Const colEstado = 20
    'Const colCARACT = 14
    'Const colMOTIVO = 15
    'Const colBENEFI = 16
    'Const colRELACI = 18
    Const colERROR = 22

    Dim sSoli, sRefe, sSupe, sDire As String
    Dim sDir, sGer, sSec As String
    Dim dFecalt, dFecreq, dFeccom, dFecPlan As Variant
    Dim sTitulo, sTipo, sPrioridad, sEstado As String
    Dim xError As String
    Dim bError As Boolean
    Dim bPerfil As Boolean
    Dim bAsigna As Boolean
    Dim sSecEjec, sGruEjec As String
    
    Dim sDESCRI, sCARACT, sMOTIVO, sBENEFI, sOBSERV, sRELACI As String
    Dim ExcelApp, ExcelWrk, xlSheet As Object
   ' Dim xls1 As Excel.Application
   ' Dim xlsw As Excel.Workbook
   ' Dim xlss As Excel.Worksheet
    Dim xRow, iRow As Long
    Dim pOK, pBD As Long
    Dim nUltimoAsignado As Integer
    Dim nNumeroAsignado As Integer
    
    selFile.prpPattern = "*.xls"
    selFile.prpDrive = glWRKDIR
    selFile.prpPath = glWRKDIR
    selFile.Show vbModal
    If selFile.prpPathFile = "" Then
        GoTo finImpo
    End If
    If Dir(selFile.prpPathFile) = "" Then
        MsgBox ("Archivo inexistente")
        GoTo finImpo
    End If
    
    
    
    ''copiar a un archiovo de errores
    ''FileCopy glSelFile, glWRKDIR & "ERR_" & glSelFileFile
    
    
    Set ExcelApp = CreateObject("Excel.Application")
    ExcelApp.Application.Visible = False
    On Error Resume Next
    Set ExcelWrk = ExcelApp.Workbooks.Open(selFile.prpPathFile)
    If ExcelWrk Is Nothing Then
        MsgBox ("Error al abrir planilla")
        GoTo finImpo
    End If
    
    Set xlSheet = ExcelWrk.Sheets("PETICIONES")
    If xlSheet Is Nothing Then
        ExcelWrk.Close
        ExcelApp.Application.Quit
        MsgBox ("Error, hoja PETICIONES inexistente")
        GoTo finImpo
    End If
    
    xRow = 1
'''    xRow = xlSheet.Range("A1", "A100").Find("HEADER").Row
'''    If xRow < 0 Then
'''        ExcelWrk.Close
'''        ExcelApp.Application.Quit
'''        MsgBox ("Error, falta marca de inicio")
'''        GoTo finImpo
'''    End If
    
    On Error GoTo 0

    xRow = xRow + 1
    iRow = 0
    
    bError = False
    
    Screen.MousePointer = vbHourglass
    
    While ClearNull(xlSheet.Cells(xRow, colTIPO)) <> ""
        iRow = iRow + 1
        
        Call Status("Proc: " & iRow)
        
        DoEvents
        xError = ""
        
        sSoli = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSOLICI))), 10)
        sRefe = Left(UCase(ClearNull(xlSheet.Cells(xRow, colREFERE))), 10)
        sDire = Left(UCase(ClearNull(xlSheet.Cells(xRow, colAUTORI))), 10)

        sDir = ""
        sGer = ""
        sSec = Left(UCase(ClearNull(xlSheet.Cells(xRow, colSector))), 8)

        sTitulo = Left(ClearNull(xlSheet.Cells(xRow, colTitulo)), 50)
        sTipo = Left(UCase(ClearNull(xlSheet.Cells(xRow, colTIPO))), 3)
        '''sPrioridad = Left(ClearNull(xlSheet.Cells(xRow, colPRIORI)), 1)
        sPrioridad = "2"
        
        sEstado = Left(UCase(ClearNull(xlSheet.Cells(xRow, colEstado))), 6)
        
        dFecPlan = Null
        If sEstado = "T" Then
            sEstado = "TERMIN"
            dFecPlan = Date
        ElseIf sEstado = "A" Then
            sEstado = "EVALUA"
        ElseIf sEstado = "D" Then
            sEstado = "PLANIF"
        Else
             xError = xError & "Falta Estado : "
        End If
        
        nNumeroAsignado = 0
        nNumeroAsignado = ClearNull(xlSheet.Cells(xRow, colNROASI))
        If Val(nNumeroAsignado) = 0 Then
            xError = xError & "Error nro Peticion : "
        End If
        
        sSecEjec = Left(UCase(ClearNull(xlSheet.Cells(xRow, colEJEC_SEC))), 8)
        sGruEjec = Left(UCase(ClearNull(xlSheet.Cells(xRow, colEJEC_GRU))), 8)
        
        sDESCRI = ClearNull(xlSheet.Cells(xRow, colDESCRI))
        sOBSERV = ClearNull(xlSheet.Cells(xRow, colOBSERV))
        sCARACT = "."
        sMOTIVO = "."
        sBENEFI = "."
'''        sCARACT = ClearNull(xlSheet.Cells(xRow, colCARACT))
'''        sMOTIVO = ClearNull(xlSheet.Cells(xRow, colMOTIVO))
'''        sBENEFI = ClearNull(xlSheet.Cells(xRow, colBENEFI))
'''        sRELACI = ClearNull(xlSheet.Cells(xRow, colRELACI))
        
        sDESCRI = ReplaceString(sDESCRI, 10, vbNewLine)
        sOBSERV = ReplaceString(sOBSERV, 10, vbNewLine)
'''        sCARACT = ReplaceString(sCARACT, 10, vbNewLine)
'''        sMOTIVO = ReplaceString(sMOTIVO, 10, vbNewLine)
'''        sBENEFI = ReplaceString(sBENEFI, 10, vbNewLine)
'''        sRELACI = ReplaceString(sRELACI, 10, vbNewLine)
       
        If sTipo = "" Then
            xError = xError & "Falta Tipo de Peticion : "
        ElseIf InStr("NOR|PRO", sTipo) = 0 Then
            xError = xError & "Tipo de Peticion incorrecta : "
        End If
        
        If sTitulo = "" Then
            xError = xError & "Falta T�tulo : "
        End If
        
        
        
        'fecha de alta
        If IsEmpty(xlSheet.Cells(xRow, colFECALT)) Then
            dFecalt = Date
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECALT)) Then
            xError = xError & "Fecha alta inv�lida : "
            dFecalt = Date
        Else
            dFecalt = xlSheet.Cells(xRow, colFECALT)
        End If
        If dFecalt > Date Then
            xError = xError & "Fecha alta fuera de rango : "
        End If
        
        'fecha requerida
        If IsEmpty(xlSheet.Cells(xRow, colFECREQ)) Then
            dFecreq = Null
        ElseIf Not IsDate(xlSheet.Cells(xRow, colFECREQ)) Then
            xError = xError & "Fecha requerida inv�lida : "
            dFecreq = Null
        Else
            dFecreq = xlSheet.Cells(xRow, colFECREQ)
        End If
        If Not IsNull(dFecreq) Then
            If dFecreq < dFecalt Then
                xError = xError & "Fecha requerida fuera de rango : "
            End If
        End If
        
        'fecha comite
         dFeccom = Date
        
        'validar SECTOR/GERE/DIREC
        If sSec = "" Then
            xError = xError & "Falta Sector : "
        ElseIf Not sp_GetSectorXt(sSec, Null, Null) Then
            sSec = ""
            xError = xError & "Sector inexistente : "
        ElseIf aplRST.EOF Then
            sSec = ""
            xError = xError & "Sector inexistente : "
        Else
            sDir = ClearNull(aplRST!cod_direccion)
            sGer = ClearNull(aplRST!cod_gerencia)
            aplRST.Close
            If Not valSectorHabil(sSec) Then
                xError = xError & "Sector no habilitado : "
            Else
                If sGer = "" Then
                    xError = xError & "Error en Gerencia : "
                End If
                If sDir = "" Then
                    xError = xError & "Error en Direccion : "
                End If
            End If
        End If
        
        'Validacion SOLICITANTE
        bPerfil = False
        If sSoli <> "" Then
            If sp_GetRecurso(sSoli, "R") Then
                aplRST.Close
                If sSec <> "" Then
                    If sp_GetRecursoActuanteEstado("PET", "SECT", sSec, "CONFEC") Then
                        While Not aplRST.EOF
                            If sSoli = ClearNull(aplRST!cod_recurso) Then
                                bPerfil = True
                            End If
                            aplRST.MoveNext
                        Wend
                        aplRST.Close
                        If Not bPerfil Then
                            xError = xError & "El SOLICITANTE no puede procesar esta peticion : "
                        End If
                    Else
                        xError = xError & "No existen SOLICITANTES que puedan procesar esta peticion : "
                    End If
                End If
            Else
                xError = xError & "SOLICITANTE inexistente : "
            End If
        Else
            xError = xError & "Falta SOLICITANTE : "
        End If
        
        'Validacion REFERENTE
        bPerfil = False
        If sRefe <> "" Then
            If sp_GetRecurso(sRefe, "R") Then
                aplRST.Close
                If sSec <> "" Then
                    If sp_GetRecursoActuanteEstado("PET", "SECT", sSec, "REFERE") Then
                        While Not aplRST.EOF
                            If sRefe = ClearNull(aplRST!cod_recurso) Then
                                bPerfil = True
                            End If
                            aplRST.MoveNext
                        Wend
                        aplRST.Close
                        If Not bPerfil Then
                            xError = xError & "El REFERENTE no puede procesar esta peticion : "
                        End If
                    Else
                        xError = xError & "No existen REFERENTES que puedan procesar esta peticion : "
                    End If
                End If
            Else
                xError = xError & "REFERENTE inexistente : "
            End If
        Else
            xError = xError & "Falta REFERENTE : "
        End If
        
        'Validacion AUTORIZANTE
        bPerfil = False
        If sDire <> "" Then
            If sp_GetRecurso(sDire, "R") Then
                aplRST.Close
                If sSec <> "" Then
                    If sp_GetRecursoActuanteEstado("PET", "SECT", sSec, "AUTORI") Then
                        While Not aplRST.EOF
                            If sDire = ClearNull(aplRST!cod_recurso) Then
                                bPerfil = True
                            End If
                            aplRST.MoveNext
                        Wend
                        aplRST.Close
                        If Not bPerfil Then
                            xError = xError & "El AUTORIZANTE no puede procesar esta peticion : "
                        End If
                    Else
                        xError = xError & "No existen AUTORIZANTES que puedan procesar esta peticion : "
                    End If
                End If
            Else
                xError = xError & "AUTORIZANTE inexistente : "
            End If
        Else
            xError = xError & "Falta AUTORIZANTE : "
        End If
        
        
        On Error Resume Next
        If sSecEjec <> "" Then
            If Not sp_GetSectorXt(sSecEjec, Null, Null) Then
                sSecEjec = ""
                xError = xError & "Sector Ejec. inexistente : "
                sGruEjec = ""
            ElseIf aplRST.EOF Then
                sSecEjec = ""
                xError = xError & "Sector Ejec. inexistente : "
                sGruEjec = ""
            End If
            aplRST.Close
        Else
            sGruEjec = ""
        End If
        If sGruEjec <> "" Then
            If Not sp_GetGrupoXt(sGruEjec, sSecEjec, Null) Then
                sGruEjec = ""
                xError = xError & "Grupo Ejec. inexistente : "
            ElseIf aplRST.EOF Then
                sGruEjec = ""
                xError = xError & "Grupo Ejec. inexistente : "
            End If
            aplRST.Close
        End If
        On Error GoTo 0
     
        
        
        'TEXTOS
        If sDESCRI = "" Then
            xError = xError & "Falta Descripci�n : "
        End If
        If sCARACT = "" Then
            xError = xError & "Falta Caracter�sticas : "
        End If
        If sMOTIVO = "" Then
            xError = xError & "Falta Motivo : "
        End If
        If sBENEFI = "" Then
            xError = xError & "Falta Beneficios : "
        End If
        
        
        'ALTA de la PETICION
        If xError <> "" Then
            GoTo errRow
        End If
            
        glNumeroPeticion = 0
        If sp_UpdatePeticion(Null, 0, sTitulo, sTipo, sPrioridad, "", "", "", "", "", 0, dFecalt, dFecreq, dFeccom, dFecPlan, dFecPlan, dFecPlan, dFecPlan, 0, sDir, sGer, sSec, "A73030", sSoli, sRefe, "", sDire, sEstado, Date, "") Then
        'recupera el ultimo numero generado
            If Not aplRST.EOF Then
                glNumeroPeticion = IIf(Not IsNull(aplRST!valor_clave), aplRST!valor_clave, "")
                aplRST.Close
            End If
        End If
        
        If Val(glNumeroPeticion) = 0 Then
            xError = xError & "Error al generar Peticion : "
            GoTo errRow
        End If
        
        'nNumeroAsignado = sp_ProximoNumero("ULPETASI")
        'If Val(nNumeroAsignado) = 0 Then
        '    xError = xError & "Error al generar nroAsignado : "
        '    GoTo errRow
        'End If
        
        
        If sBENEFI <> "" Then
            Call sp_UpdateMemo(glNumeroPeticion, "BENEFICIOS", sBENEFI)
        End If
        If sCARACT <> "" Then
            Call sp_UpdateMemo(glNumeroPeticion, "CARACTERIS", sCARACT)
        End If
        If sDESCRI <> "" Then
            Call sp_UpdateMemo(glNumeroPeticion, "DESCRIPCIO", sDESCRI)
        End If
        If sMOTIVO <> "" Then
            Call sp_UpdateMemo(glNumeroPeticion, "MOTIVOS", sMOTIVO)
        End If
        If sOBSERV <> "" Then
            Call sp_UpdateMemo(glNumeroPeticion, "OBSERVACIO", sOBSERV)
        End If
        If sRELACI <> "" Then
            Call sp_UpdateMemo(glNumeroPeticion, "RELACIONES", sRELACI)
        End If
        
        bAsigna = True
        If Not sp_UpdatePetField(glNumeroPeticion, "NROASIG", Null, Null, nNumeroAsignado) Then
            bAsigna = False
            xError = xError & "Error al asignar n�mero : "
        End If
        
        If bAsigna Then
            Call sp_AddHistorial(glNumeroPeticion, "PIMPORT", sEstado, Null, Null, Null, Null, "A73030", "<<Importaci�n de Petici�n: " & nNumeroAsignado & ">>")
            If ClearNull(sSecEjec) <> "" Then
                Call sp_InsertPeticionSector(glNumeroPeticion, sSecEjec, dFecPlan, dFecPlan, dFecPlan, dFecPlan, 0, sEstado, Date, "", 0, "0")
                If ClearNull(sGruEjec) <> "" Then
                    Call sp_InsertPeticionGrupo(glNumeroPeticion, sGruEjec, dFecPlan, dFecPlan, dFecPlan, dFecPlan, 0, sEstado, Date, "", 0, "0")
                End If
            End If
        Else
            Call sp_DeletePeticion(glNumeroPeticion)
        End If
errRow:
        If xError <> "" Then
            xlSheet.Cells(xRow, colERROR) = xError
            bError = True
            xRow = xRow + 1
            pBD = pBD + 1
        Else
            xlSheet.Cells(xRow, colERROR) = ""
            xRow = xRow + 1
            pOK = pOK + 1
        End If
    Wend

    Screen.MousePointer = vbNormal
    Status ("Listo")
    
'    If bError Then
        ExcelWrk.Save
        ExcelWrk.Close
        ExcelApp.Application.Quit
        MsgBox ("Procesadas: " & iRow & Chr$(13) & Chr$(13) & "Correctas: " & pOK & Chr$(13) & Chr$(13) & "Con error: " & pBD & Chr$(13) & Chr$(13) & "La descripci�n de los errores se encuentra en la planilla")
'    Else
'        ExcelWrk.Save
'        ExcelWrk.Close
'        ExcelApp.Application.Quit
'        Kill glWRKDIR & "ERR_" & glSelFileFile
'        MsgBox ("El proceso de importaci�n finaliz� sin errores")
'    End If
    Exit Sub
errCopy:
    ''MsgBox ("Error al crear archivo temporario: " & Chr$(13) & glWRKDIR & "ERR_" & glSelFileFile & Chr$(13) & Chr$(13) & Err.DESCRIPTION)
    Exit Sub
finImpo:
        
End Sub
