Attribute VB_Name = "spMensajesTexto"
Option Explicit

Function sp_GetMensajesTexto(cod_txtmsg) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetMensajesTexto"
      .Parameters.Append .CreateParameter("@cod_txtmsg", adInteger, adParamInput, , cod_txtmsg)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
     sp_GetMensajesTexto = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetMensajesTexto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DelMensajesTexto(cod_txtmsg) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DelMensajesTexto"
      .Parameters.Append .CreateParameter("@cod_txtmsg", adInteger, adParamInput, , CLng(Val(cod_txtmsg)))
      Set aplRST = .Execute
    End With
    sp_DelMensajesTexto = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DelMensajesTexto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdMensajesTexto(cod_txtmsg, msg_texto) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdMensajesTexto"
      .Parameters.Append .CreateParameter("@cod_txtmsg", adInteger, adParamInput, , CLng(Val(cod_txtmsg)))
      .Parameters.Append .CreateParameter("@msg_texto", adChar, adParamInput, 255, msg_texto)
      Set aplRST = .Execute
    End With
    sp_UpdMensajesTexto = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdMensajesTexto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
