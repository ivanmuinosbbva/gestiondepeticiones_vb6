Attribute VB_Name = "spVarios"
' -001- a. FJS 02.07.2007 - Se agrega un nuevo proc. para ejecutar el SP para la consulta de peticiones finalizadas
' -001- b. FJS 03.07.2007 - Se copia y modifica el proc. sp_GetAdjuntoPet porque debe utilizarse otro recordset auxiliar para obtener
'                           la cadena con los documentos adjuntos a cada petici�n.
' -001- c. FJS 15.12.2015 - Se remueve porque no se utiliza m�s (no se soportan los m�ltiples recordset).
' -001- d. FJS 24.07.2007 - Se agrega un nuevo proc. para llamar a un SP que obtiene el nombre de pila y apellido del usuario (es m�s que nada para
'                           la parte de testing y depuraci�n de la aplicaci�n).
' -002- a. FJS 22.08.2007 - Nuevo procedimiento para obtener la ruta para exportaci�n de archivos a HOST
' -003- a. FJS 21.07.2008 - Se modifican los par�metros por compatibilidad. Por otro lado, se agrega un nuevo proceso para llamar al nuevo SP sp_GetVariosTexto
' -004- a. FJS 05.09.2011 - Se agregan los procesos para ABM de Acciones (nuevo ABM).
' -005- GMT01  25.10.2019 - Pet 70881 - se agrega el llamado al sp sp_HabilitaRGyP

Option Explicit

Function sp_GetAccion(cod_accion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetAccion"
        .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAccion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAccion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -004- a.
Function sp_InsertAccion(cod_accion, nom_accion, IGM_hab, tipo_accion, agrupador, leyenda, orden, porEstado, multiplesEstados) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertAcciones"
        .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
        .Parameters.Append .CreateParameter("@nom_accion", adChar, adParamInput, 100, nom_accion)
        .Parameters.Append .CreateParameter("@IGM_hab", adChar, adParamInput, 1, IGM_hab)
        .Parameters.Append .CreateParameter("@tipo_accion", adChar, adParamInput, 6, tipo_accion)
        .Parameters.Append .CreateParameter("@agrupador", adInteger, adParamInput, , agrupador)
        .Parameters.Append .CreateParameter("@leyenda", adChar, adParamInput, 100, leyenda)
        .Parameters.Append .CreateParameter("@orden", adInteger, adParamInput, , orden)
        .Parameters.Append .CreateParameter("@porEstado", adChar, adParamInput, 1, porEstado)
        .Parameters.Append .CreateParameter("@multiplesEstados", adChar, adParamInput, 1, multiplesEstados)
        Set aplRST = .Execute
    End With
    sp_InsertAccion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertAccion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateAccion(cod_accion, nom_accion, IGM_hab, tipo_accion, agrupador, leyenda, orden, porEstado, multiplesEstados) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateAcciones"
        .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
        .Parameters.Append .CreateParameter("@nom_accion", adChar, adParamInput, 100, nom_accion)
        .Parameters.Append .CreateParameter("@IGM_hab", adChar, adParamInput, 1, IGM_hab)
        .Parameters.Append .CreateParameter("@tipo_accion", adChar, adParamInput, 6, tipo_accion)
        .Parameters.Append .CreateParameter("@agrupador", adInteger, adParamInput, , agrupador)
        .Parameters.Append .CreateParameter("@leyenda", adChar, adParamInput, 100, leyenda)
        .Parameters.Append .CreateParameter("@orden", adInteger, adParamInput, , orden)
        .Parameters.Append .CreateParameter("@porEstado", adChar, adParamInput, 1, porEstado)
        .Parameters.Append .CreateParameter("@multiplesEstados", adChar, adParamInput, 1, multiplesEstados)
        Set aplRST = .Execute
    End With
    sp_UpdateAccion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateAccion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteAccion(cod_accion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteAccion"
        .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
        Set aplRST = .Execute
    End With
    sp_DeleteAccion = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteAccion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Function sp_GetAccionPerfil(cod_accion, cod_perfil, cod_estado, parm1, parm2) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAccionPerfil"
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAccionPerfil = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAccionPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAccionPerfilEstados(cod_version, cod_accion, cod_perfil, cod_estado, parm1, parm2) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    sp_GetAccionPerfilEstados = False
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAccionPerfilEstados"
      .Parameters.Append .CreateParameter("@cod_version", adInteger, adParamInput, , cod_version)
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAccionPerfilEstados = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAccionPerfilEstados = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAccionPerfilConfig(agrupador, cod_perfil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetAccionPerfilConfig"
      .Parameters.Append .CreateParameter("@cod_accion", adInteger, adParamInput, , agrupador)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAccionPerfilConfig = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetAccionPerfilConfig = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetEstado(cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetEstado"
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetEstado = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetEstado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_ChgEstadoSector(pet_nrointerno, cod_sector, cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_ChgEstadoSector"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      Set aplRST = .Execute
    End With
    'If Not (aplRST.EOF) Then
    '    sp_ChgEstadoSector = True
    'End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ChgEstadoSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_ChgEstadoGrupo(pet_nrointerno, cod_sector, cod_grupo, cod_estado) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_ChgEstadoGrupo"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      Set aplRST = .Execute
    End With
    'If Not (aplRST.EOF) Then
    '    sp_ChgEstadoGrupo = True
    'End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ChgEstadoGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_ChgEstadoAprobado(pet_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_ChgEstadoAprobado"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      Set aplRST = .Execute
    End With
    'If Not (aplRST.EOF) Then
    '    sp_ChgEstadoAprobado = True
    'End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ChgEstadoAprobado = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_FuerzaEstadoPlanok(pet_nrointerno, hst_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_FuerzaEstadoPlanok"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
      Set aplRST = .Execute
    End With
    'If Not (aplRST.EOF) Then
    '    sp_FuerzaEstadoPlanok = True
    'End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_FuerzaEstadoPlanok = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_HeredaEstadoPlanok(pet_nrointerno, cod_sector, cod_estado, fe_ini_plan, fe_fin_plan, hst_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_HeredaEstadoPlanok"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , fe_ini_plan)
      .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , fe_fin_plan)
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
      Set aplRST = .Execute
    End With
     sp_HeredaEstadoPlanok = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_HeredaEstadoPlanok = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetPeticionSectorMinuta(pet_nrointerno, cod_gerencia, cod_sector, cod_estado, fe_desde, fe_hasta) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionSectorMinuta"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_estado", adVarChar, adParamInput, 100, cod_estado)
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionSectorMinuta = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionSectorMinuta = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -001- a.
Function sp_GetPeticionesFinalizadas(pet_nrointerno, _
                                     cod_gerencia, _
                                     cod_sector, _
                                     cod_estado, _
                                     fe_desde, _
                                     fe_hasta, _
                                     pet_type, _
                                     pet_class, _
                                     pet_imptech) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetPeticionesFinalizadas"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_estado", adVarChar, adParamInput, 100, cod_estado)
      .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
      .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
      .Parameters.Append .CreateParameter("@pet_type", adVarChar, adParamInput, 100, pet_type)
      .Parameters.Append .CreateParameter("@pet_class", adVarChar, adParamInput, 100, pet_class)
      .Parameters.Append .CreateParameter("@pet_imptech", adVarChar, adParamInput, 100, pet_imptech)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetPeticionesFinalizadas = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetPeticionesFinalizadas = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'{ del -001- c.
''{ add -001- b.
'Function sp_GetAdjuntosPet2(pet_nrointerno, adj_tipo, cod_sector, cod_grupo) As Boolean    ' upd -003- a.
'    On Error Resume Next
'    Dim txtCommand As String
'    Dim cnAux As ADODB.Connection
'    adoRSAux1.Close
'    Set adoRSAux1 = Nothing
'    cnAux.Close
'    Set cnAux = Nothing
'    Set cnAux = New ADODB.Connection
'    cnAux.CursorLocation = adUseClient
'    cnAux.Open aplCONN.ConnectionString
'    On Error GoTo Err_SP
'    Dim objCommand As ADODB.Command
'    Set objCommand = New ADODB.Command
'    Set adoRSAux1 = New ADODB.Recordset
'    With objCommand
'      .ActiveConnection = cnAux
'      .CommandType = adCmdStoredProc
'      .CommandText = "sp_GetAdjuntosPet"
'      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
'      .Parameters.Append .CreateParameter("@adj_tipo", adChar, adParamInput, 8, adj_tipo)
'      '{ add -003- a.
'      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
'      .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
'      '}
'      Set adoRSAux1 = .Execute
'    End With
'    If Not (adoRSAux1.EOF) Then
'        sp_GetAdjuntosPet2 = True
'    End If
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'    On Error GoTo 0
'Exit Function
'Err_SP:
'    MsgBox (AnalizarErrorSQL(adoRSAux1, Err, aplCONN))
'    sp_GetAdjuntosPet2 = False
'    Set objCommand.ActiveConnection = Nothing
'    Set objCommand = Nothing
'End Function
''}
'}

Function sp_HeredaEstadoGrupo(pet_nrointerno, cod_sector, cod_estado, fe_ini_plan, fe_fin_plan, fe_ini_real, fe_fin_real, horaspresup, estado_filtro, que_hereda, hst_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_HeredaEstadoGrupo"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , fe_ini_plan)
      .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , fe_fin_plan)
      .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , fe_ini_real)
      .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , fe_fin_real)
      .Parameters.Append .CreateParameter("@horaspresup", adSmallInt, adParamInput, , CInt(Val(horaspresup)))
      .Parameters.Append .CreateParameter("@estado_filtro", adVarChar, adParamInput, 255, estado_filtro)
      .Parameters.Append .CreateParameter("@que_hereda", adChar, adParamInput, 1, que_hereda)
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_HeredaEstadoGrupo = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_HeredaEstadoGrupo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_HeredaEstadoSector(pet_nrointerno, cod_estado, fe_ini_plan, fe_fin_plan, fe_ini_real, fe_fin_real, horaspresup, estado_filtro, que_hereda, hst_nrointerno) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_HeredaEstadoSector"
      .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(pet_nrointerno)))
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@fe_ini_plan", SQLDdateType, adParamInput, , fe_ini_plan)
      .Parameters.Append .CreateParameter("@fe_fin_plan", SQLDdateType, adParamInput, , fe_fin_plan)
      .Parameters.Append .CreateParameter("@fe_ini_real", SQLDdateType, adParamInput, , fe_ini_real)
      .Parameters.Append .CreateParameter("@fe_fin_real", SQLDdateType, adParamInput, , fe_fin_real)
      .Parameters.Append .CreateParameter("@horaspresup", adSmallInt, adParamInput, , CInt(Val(horaspresup)))
      .Parameters.Append .CreateParameter("@estado_filtro", adVarChar, adParamInput, 255, estado_filtro)
      .Parameters.Append .CreateParameter("@que_hereda", adChar, adParamInput, 1, que_hereda)
      .Parameters.Append .CreateParameter("@hst_nrointerno", adInteger, adParamInput, , CLng(Val(hst_nrointerno)))
      Set aplRST = .Execute
    End With
    sp_HeredaEstadoSector = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_HeredaEstadoSector = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetVarios(var_codigo) As Boolean
    On Error Resume Next
    If aplRST.State <> adStateClosed Then aplRST.Close
    'aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetVarios"
      .Parameters.Append .CreateParameter("@var_codigo", adChar, adParamInput, 8, var_codigo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetVarios = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetVarios = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -003- a.
Function sp_GetVariosTexto(var_codigo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetVariosTexto"
      .Parameters.Append .CreateParameter("@var_codigo", adChar, adParamInput, 30, var_codigo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetVariosTexto = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetVariosTexto = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

Function sp_UpdateVarios(var_codigo, var_numero, var_texto, var_fecha) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateVarios"
      .Parameters.Append .CreateParameter("@var_codigo", adChar, adParamInput, 8, var_codigo)
      .Parameters.Append .CreateParameter("@var_numero", adInteger, adParamInput, , var_numero)
      .Parameters.Append .CreateParameter("@var_texto", adChar, adParamInput, 30, Left(var_texto, 30))
      .Parameters.Append .CreateParameter("@var_fecha", adDate, adParamInput, , var_fecha)
      Set aplRST = .Execute
    End With
    sp_UpdateVarios = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateVarios = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteVarios(var_codigo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteVarios"
      .Parameters.Append .CreateParameter("@var_codigo", adChar, adParamInput, 8, var_codigo)
      Set aplRST = .Execute
    End With
    sp_DeleteVarios = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteVarios = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -002- a.
Function sp_GetVarios1(var_codigo) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetVarios1"
      .Parameters.Append .CreateParameter("@var_codigo", adChar, adParamInput, 8, var_codigo)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetVarios1 = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetVarios1 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}

'Function sp_ProximoNumero(var_codigo) As Integer
Function sp_ProximoNumero(var_codigo) As Long
    'Dim var_numero As Integer
    Dim var_numero As Long
    var_numero = 0
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_ProximoNumero"
      .Parameters.Append .CreateParameter("@var_codigo", adChar, adParamInput, 8, var_codigo)
      .Parameters.Append .CreateParameter("@var_numero", adInteger, adParamOutput, , var_numero)
      .Execute
      var_numero = .Parameters(1).value
    End With
    
    If var_numero = 0 Then
        sp_ProximoNumero = 0
        Exit Function
    End If
    sp_ProximoNumero = var_numero
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_ProximoNumero = 0
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateAccionesPerfil(cod_accion, cod_perfil, cod_estado, cod_estnew) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateAccionesPerfil"
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@cod_estnew", adChar, adParamInput, 6, cod_estnew)
      Set aplRST = .Execute
    End With
    sp_UpdateAccionesPerfil = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateAccionesPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteAccionesPerfil(cod_accion, cod_perfil, cod_estado, cod_estnew) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteAccionesPerfil"
      .Parameters.Append .CreateParameter("@cod_accion", adChar, adParamInput, 8, cod_accion)
      .Parameters.Append .CreateParameter("@cod_perfil", adChar, adParamInput, 4, cod_perfil)
      .Parameters.Append .CreateParameter("@cod_estado", adChar, adParamInput, 6, cod_estado)
      .Parameters.Append .CreateParameter("@cod_estnew", adChar, adParamInput, 6, cod_estnew)
      Set aplRST = .Execute
    End With
    sp_DeleteAccionesPerfil = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteAccionesPerfil = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetSituacion(cod_situacion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetSituacion"
      .Parameters.Append .CreateParameter("@cod_situacion", adChar, adParamInput, 6, cod_situacion)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetSituacion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetSituacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'''Function sp_VerEscalamiento() As Boolean
'''    On Error Resume Next
'''    aplRST.Close
'''    Set aplRST = Nothing
'''    On Error GoTo Err_SP
'''    Dim objCommand As ADODB.Command
'''    Set objCommand = New ADODB.Command
'''    With objCommand
'''      .ActiveConnection = aplCONN
'''      .CommandType = adCmdStoredProc
'''      .CommandText = "sp_VerEscalamiento"
'''      Set aplRST = .Execute
'''    End With
'''    sp_VerEscalamiento = True
'''    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
'''        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
'''            GoTo Err_SP
'''        End If
'''    End If
'''    Set objCommand.ActiveConnection = Nothing
'''    Set objCommand = Nothing
'''    On Error GoTo 0
'''Exit Function
'''Err_SP:
'''    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'''    sp_VerEscalamiento = False
'''    Set objCommand.ActiveConnection = Nothing
'''    Set objCommand = Nothing
'''End Function


'''Function sp_CleanEscalamiento() As Boolean
'''    On Error Resume Next
'''    aplRST.Close
'''    Set aplRST = Nothing
'''    On Error GoTo Err_SP
'''    Dim objCommand As ADODB.Command
'''    Set objCommand = New ADODB.Command
'''    With objCommand
'''      .ActiveConnection = aplCONN
'''      .CommandType = adCmdStoredProc
'''      .CommandText = "sp_CleanEscalamiento"
'''      Set aplRST = .Execute
'''    End With
'''    sp_CleanEscalamiento = True
'''    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
'''        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
'''            GoTo Err_SP
'''        End If
'''    End If
'''    Set objCommand.ActiveConnection = Nothing
'''    Set objCommand = Nothing
'''    On Error GoTo 0
'''Exit Function
'''Err_SP:
'''    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
'''    sp_CleanEscalamiento = False
'''    Set objCommand.ActiveConnection = Nothing
'''    Set objCommand = Nothing
'''End Function


Function sp_GetImportancia(cod_importancia, flg_habil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetImportancia"
      .Parameters.Append .CreateParameter("@cod_importancia", adChar, adParamInput, 2, cod_importancia)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetImportancia = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetImportancia = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetOrientacion(cod_orientacion, flg_habil) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetOrientacion"
      .Parameters.Append .CreateParameter("@cod_orientacion", adChar, adParamInput, 2, cod_orientacion)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetOrientacion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetOrientacion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_password(newPassword, oldPassword, Usuario) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_password"
      .Parameters.Append .CreateParameter("@old", adVarChar, adParamInput, 30, oldPassword)
      .Parameters.Append .CreateParameter("@new", adVarChar, adParamInput, 31, newPassword)
      .Parameters.Append .CreateParameter("@loginame", adVarChar, adParamInput, 30, Usuario)
      Set aplRST = .Execute
    End With
    sp_password = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox ("Error: " & Err)
    sp_password = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

'{ add -001- d. - Nuevo procedimiento que devuelve el nombre de pila y apellido del recurso actual
Function sp_GetRecursoNombre(Recurso) As String
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetRecursoNombre"
      .Parameters.Append .CreateParameter("@cod_recurso", adVarChar, adParamInput, 10, Recurso)
      Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetRecursoNombre = Trim(aplRST.Fields(0).value)
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox "Error: " & Err & vbCrLf & vbCrLf & "Descripci�n: " & Err.DESCRIPTION & vbCrLf & "Origen: " & Trim(Err.source), vbCritical + vbOKOnly
    sp_GetRecursoNombre = ""
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'}
'GMT01 - INI
Function sp_HabilitaRGyP(habilitar) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_HabilitaRGyP"
      .Parameters.Append .CreateParameter("@habilita", adChar, adParamInput, 1, habilitar)
      Set aplRST = .Execute
    End With
    sp_HabilitaRGyP = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_HabilitaRGyP = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
'GMT01 - FIN
