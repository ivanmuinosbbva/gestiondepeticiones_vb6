Attribute VB_Name = "spHitoClase"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.22 by Fernando J. Spitz (2015) ~

Option Explicit

Function sp_InsertHitoClase(hitoid, hitodesc, hitoexpos) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertHitoClase"
        .Parameters.Append .CreateParameter("@hitoid", adInteger, adParamInput, , hitoid)
        .Parameters.Append .CreateParameter("@hitodesc", adChar, adParamInput, 60, hitodesc)
        .Parameters.Append .CreateParameter("@hitoexpos", adChar, adParamInput, 1, hitoexpos)
        Set aplRST = .Execute
    End With
    sp_InsertHitoClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertHitoClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHitoClase(hitoid, hitodesc, hitoexpos) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateHitoClase"
        .Parameters.Append .CreateParameter("@hitoid", adInteger, adParamInput, , hitoid)
        .Parameters.Append .CreateParameter("@hitodesc", adChar, adParamInput, 60, hitodesc)
        .Parameters.Append .CreateParameter("@hitoexpos", adChar, adParamInput, 1, hitoexpos)
        Set aplRST = .Execute
    End With
    sp_UpdateHitoClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHitoClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteHitoClase(hitoid) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteHitoClase"
        .Parameters.Append .CreateParameter("@hitoid", adInteger, adParamInput, , hitoid)
        Set aplRST = .Execute
    End With
    sp_DeleteHitoClase = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteHitoClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHitoClase(hitoid) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHitoClase"
        .Parameters.Append .CreateParameter("@hitoid", adInteger, adParamInput, , hitoid)
        Set aplRST = .Execute
    End With
    If Not aplRST.EOF Then
        sp_GetHitoClase = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetHitoClase = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

