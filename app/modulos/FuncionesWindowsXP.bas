Attribute VB_Name = "FuncionesWindowsXP"
Option Explicit

'*************************************************************
'* MsgBoxEx() - Written by Aaron Young, February 7th 2000
'*            - Edited by Philip Manavopoulos, May 19th 2005
'*************************************************************

Private Type CWPSTRUCT
    lParam As Long
    wParam As Long
    message As Long
    hWnd As Long
End Type
 
'Added by manavo11
Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
 
Private Type LOGBRUSH
    lbStyle As Long
    lbColor As Long
    lbHatch As Long
End Type
'Added by manavo11
 
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function SetWindowsHookEx Lib "user32" Alias "SetWindowsHookExA" (ByVal idHook As Long, ByVal lpfn As Long, ByVal hmod As Long, ByVal dwThreadId As Long) As Long
Private Declare Function CallNextHookEx Lib "user32" (ByVal hHook As Long, ByVal nCode As Long, ByVal wParam As Long, lParam As Any) As Long
Private Declare Function UnhookWindowsHookEx Lib "user32" (ByVal hHook As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Private Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Private Declare Function GetClassName Lib "user32" Alias "GetClassNameA" (ByVal hWnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long
 
'Added by manavo11
Private Declare Function SetTextColor Lib "gdi32" (ByVal hdc As Long, ByVal crColor As Long) As Long
Private Declare Function SetBkColor Lib "gdi32" (ByVal hdc As Long, ByVal crColor As Long) As Long
Private Declare Function CreateBrushIndirect Lib "gdi32" (lpLogBrush As LOGBRUSH) As Long
Private Declare Function GetSysColor Lib "user32" (ByVal nIndex As Long) As Long
 
Private Declare Function SetFocus Lib "user32" (ByVal hWnd As Long) As Long
 
Private Declare Function MoveWindow Lib "user32" (ByVal hWnd As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal bRepaint As Long) As Long
Private Declare Function GetWindowRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT) As Long
Private Declare Function GetClientRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT) As Long
 
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" (ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpsz1 As String, ByVal lpsz2 As String) As Long
'Added by manavo11
 
Private Const WH_CALLWNDPROC = 4
Private Const GWL_WNDPROC = (-4)
Private Const WM_CTLCOLORBTN = &H135
Private Const WM_DESTROY = &H2
Private Const WM_SETTEXT = &HC
Private Const WM_CREATE = &H1
 
'Added by manavo11
' System Color Constants
Private Const COLOR_BTNFACE = 15
Private Const COLOR_BTNTEXT = 18
 
' Windows Messages
Private Const WM_CTLCOLORSTATIC = &H138
Private Const WM_CTLCOLORDLG = &H136
 
Private Const WM_SHOWWINDOW As Long = &H18
'Added by manavo11
 
Private lHook As Long
Private lPrevWnd As Long
 
Private bCustom As Boolean
Private sButtons() As String
Private lButton As Long
Private sHwnd As String
 
'Added by manavo11
Private lForecolor As Long
Private lBackcolor As Long
 
Private sDefaultButton As String
 
Private iX As String
Private iY As String
Private iWidth As String
Private iHeight As String
 
Private iButtonCount As Integer
Private iButtonWidth As Integer
'Added by manavo11

Declare Sub InitCommonControls Lib "comctl32.dll" ()

' The NMHDR structure contains information about a notification message. The pointer
' to this structure is specified as the lParam member of the WM_NOTIFY message.
Public Type NMHDR
  hwndFrom As Long   ' Window handle of control sending message
  idFrom As Long        ' Identifier of control sending message
  code  As Long          ' Specifies the notification code
End Type

Public Type POINTAPI   ' pt
  x As Long
  y As Long
End Type

'Public Type RECT   ' rct
'  Left As Long
'  Top As Long
'  Right As Long
'  Bottom As Long
'End Type

'Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
'                            (ByVal hWnd As Long, _
'                            ByVal wMsg As Long, _
'                            ByVal wParam As Long, _
'                            lParam As Any) As Long   ' <---

Public Const WM_USER = &H400

Declare Function CreateWindowEx Lib "user32" Alias "CreateWindowExA" _
                            (ByVal dwExStyle As Long, ByVal lpClassName As String, _
                             ByVal lpWindowName As String, ByVal dwStyle As Long, _
                             ByVal x As Long, ByVal y As Long, _
                             ByVal nWidth As Long, ByVal nHeight As Long, _
                             ByVal hwndParent As Long, ByVal hMenu As Long, _
                             ByVal hInstance As Long, lpParam As Any) As Long

Declare Function DestroyWindow Lib "user32" (ByVal hWnd As Long) As Long

Declare Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (pDest As Any, pSource As Any, ByVal dwLength As Long)

' can also be set in the make tab of the Project Properties dialog
#Const WIN32_IE = &H400   ' 1024

'Private Const WM_USER = &H400

'Private Type POINTAPI   ' pt
'  x As Long
'  y As Long
'End Type

'Private Type RECT   ' rct
'  Left As Long
'  Top As Long
'  Right As Long
'  Bottom As Long
'End Type

'' The NMHDR structure contains information about a notification message. The pointer
'' to this structure is specified as the lParam member of the WM_NOTIFY message.
'Private Type NMHDR
'  hwndFrom As Long   ' Window handle of control sending message
'  idFrom As Long        ' Identifier of control sending message
'  code  As Long          ' Specifies the notification code
'End Type

' ========================================================================
' tooltip definitions

Public Const TOOLTIPS_CLASS = "tooltips_class32"

' Styles
Public Const TTS_ALWAYSTIP = &H1
Public Const TTS_NOPREFIX = &H2

Public Type TOOLINFO
  cbSize As Long
  uFlags As TT_Flags
  hWnd As Long
  uId As Long
  RECT As RECT
  hinst As Long
  lpszText As String   ' Long
#If (WIN32_IE >= &H300) Then
  lParam As Long
#End If
End Type   ' TOOLINFO

Public Enum TT_Flags
  TTF_IDISHWND = &H1
  TTF_CENTERTIP = &H2
  TTF_RTLREADING = &H4
  TTF_SUBCLASS = &H10
#If (WIN32_IE >= &H300) Then
  TTF_TRACK = &H20
  TTF_ABSOLUTE = &H80
  TTF_TRANSPARENT = &H100
  TTF_DI_SETITEM = &H8000&        ' valid only on the TTN_NEEDTEXT callback
#End If     ' WIN32_IE >= =&H0300
End Enum   ' TT_Flags

Public Enum TT_DelayTime
  TTDT_AUTOMATIC = 0
  TTDT_RESHOW = 1
  TTDT_AUTOPOP = 2
  TTDT_INITIAL = 3
End Enum

Public Type TTHITTESTINFO
  hWnd As Long
  pt As POINTAPI
  ti As TOOLINFO
End Type

' ========================================================================
' messages

Public Enum TT_Msgs
  TTM_ACTIVATE = (WM_USER + 1)
  TTM_SETDELAYTIME = (WM_USER + 3)
  TTM_RELAYEVENT = (WM_USER + 7)
  TTM_GETTOOLCOUNT = (WM_USER + 13)
  TTM_WINDOWFROMPOINT = (WM_USER + 16)
    
#If UNICODE Then
  TTM_ADDTOOL = (WM_USER + 50)
  TTM_DELTOOL = (WM_USER + 51)
  TTM_NEWTOOLRECT = (WM_USER + 52)
  TTM_GETTOOLINFO = (WM_USER + 53)
  TTM_SETTOOLINFO = (WM_USER + 54)
  TTM_HITTEST = (WM_USER + 55)
  TTM_GETTEXT = (WM_USER + 56)
  TTM_UPDATETIPTEXT = (WM_USER + 57)
  TTM_ENUMTOOLS = (WM_USER + 58)
  TTM_GETCURRENTTOOL = (WM_USER + 59)
#Else
  TTM_ADDTOOL = (WM_USER + 4)
  TTM_DELTOOL = (WM_USER + 5)
  TTM_NEWTOOLRECT = (WM_USER + 6)
  TTM_GETTOOLINFO = (WM_USER + 8)
  TTM_SETTOOLINFO = (WM_USER + 9)
  TTM_HITTEST = (WM_USER + 10)
  TTM_GETTEXT = (WM_USER + 11)
  TTM_UPDATETIPTEXT = (WM_USER + 12)
  TTM_ENUMTOOLS = (WM_USER + 14)
  TTM_GETCURRENTTOOL = (WM_USER + 15)
#End If   ' UNICODE

#If (WIN32_IE >= &H300) Then
  TTM_TRACKACTIVATE = (WM_USER + 17)       ' wParam = TRUE/FALSE start end  lparam = LPTOOLINFO
  TTM_TRACKPOSITION = (WM_USER + 18)       ' lParam = dwPos
  TTM_SETTIPBKCOLOR = (WM_USER + 19)
  TTM_SETTIPTEXTCOLOR = (WM_USER + 20)
  TTM_GETDELAYTIME = (WM_USER + 21)
  TTM_GETTIPBKCOLOR = (WM_USER + 22)
  TTM_GETTIPTEXTCOLOR = (WM_USER + 23)
  TTM_SETMAXTIPWIDTH = (WM_USER + 24)
  TTM_GETMAXTIPWIDTH = (WM_USER + 25)
  TTM_SETMARGIN = (WM_USER + 26)           ' lParam = lprc
  TTM_GETMARGIN = (WM_USER + 27)           ' lParam = lprc
  TTM_POP = (WM_USER + 28)
#End If   ' (WIN32_IE >= &H300)

#If (WIN32_IE >= &H400) Then
  TTM_UPDATE = (WM_USER + 29)
#End If
End Enum   ' TT_Msgs

' ========================================================================
' notifications

Public Enum TT_Notifications
  TTN_FIRST = -520&   '   (0U-520U)
  TTN_LAST = -549&    '   (0U-549U)
#If UNICODE Then
  TTN_NEEDTEXT = (TTN_FIRST - 10)   ' is now TTN_GETDISPINFO
#Else
  TTN_NEEDTEXT = (TTN_FIRST - 0)
#End If   ' UNICODE
  TTN_SHOW = (TTN_FIRST - 1)
  TTN_POP = (TTN_FIRST - 2)
End Enum   ' TT_Notifications

Public Type NMTTDISPINFO
  hdr As NMHDR
  lpszText As Long
#If UNICODE Then
  szText As String * 160
#Else
  szText As String * 80
#End If   ' UNICODE
  hinst As Long
  uFlags As Long
#If (WIN32_IE >= &H300) Then
  lParam As Long
#End If
End Type   ' NMTTDISPINFO
 
Public Function SubMsgBox(ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim sText As String
    
    Select Case Msg
    
    'Added by manavo11
    Case WM_SHOWWINDOW
        Dim MsgBoxRect As RECT
        
        GetWindowRect hWnd, MsgBoxRect
        
        If StrPtr(iX) = 0 Then
            iX = MsgBoxRect.Left
        End If
        
        If StrPtr(iY) = 0 Then
            iY = MsgBoxRect.Top
        End If
        
        If StrPtr(iWidth) = 0 Then
            iWidth = MsgBoxRect.Right - MsgBoxRect.Left
        Else
            Dim i As Integer
            Dim h As Long
            
            Dim ButtonRECT As RECT
            
            For i = 0 To iButtonCount
                h = FindWindowEx(hWnd, h, "Button", vbNullString)
                
                GetWindowRect h, ButtonRECT
                
                MoveWindow h, 14 + (iButtonWidth * i) + (6 * i), iHeight - (ButtonRECT.Bottom - ButtonRECT.Top) - 40, iButtonWidth, ButtonRECT.Bottom - ButtonRECT.Top, 1
            Next
        End If
        
        If StrPtr(iHeight) = 0 Then
            iHeight = MsgBoxRect.Bottom - MsgBoxRect.Top
        End If
        
        MoveWindow hWnd, iX, iY, iWidth, iHeight, 1
    Case WM_CTLCOLORDLG, WM_CTLCOLORSTATIC
        Dim tLB As LOGBRUSH
        'Debug.Print wParam
        
        Call SetTextColor(wParam, lForecolor)
        Call SetBkColor(wParam, lBackcolor)
        
        tLB.lbColor = lBackcolor
        
        SubMsgBox = CreateBrushIndirect(tLB)
        Exit Function
    'Added by manavo11
    
    Case WM_CTLCOLORBTN
        'Customize the MessageBox Buttons if neccessary..
        'First Process the Default Action of the Message (Draw the Button)
        SubMsgBox = CallWindowProc(lPrevWnd, hWnd, Msg, wParam, ByVal lParam)
        'Now Change the Button Text if Required
        If Not bCustom Then Exit Function
        If lButton = 0 Then sHwnd = ""
        'If this Button has Been Modified Already then Exit
        If InStr(sHwnd, " " & Trim(Str(lParam)) & " ") Then Exit Function
        sText = sButtons(lButton)
        sHwnd = sHwnd & " " & Trim(Str(lParam)) & " "
        lButton = lButton + 1
        'Modify the Button Text
        SendMessage lParam, WM_SETTEXT, Len(sText), ByVal sText
        
        'Added by manavo11
        If sText = sDefaultButton Then
            SetFocus lParam
        End If
        'Added by manavo11
        
        Exit Function
        
    Case WM_DESTROY
        'Remove the MsgBox Subclassing
        Call SetWindowLong(hWnd, GWL_WNDPROC, lPrevWnd)
    End Select
    SubMsgBox = CallWindowProc(lPrevWnd, hWnd, Msg, wParam, ByVal lParam)
End Function
 
Private Function HookWindow(ByVal nCode As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Dim tCWP As CWPSTRUCT
    Dim sClass As String
    'This is where you need to Hook the Messagebox
    CopyMemory tCWP, ByVal lParam, Len(tCWP)
    If tCWP.message = WM_CREATE Then
        sClass = Space(255)
        sClass = Left(sClass, GetClassName(tCWP.hWnd, ByVal sClass, 255))
        If sClass = "#32770" Then
            'Subclass the Messagebox as it's created
            lPrevWnd = SetWindowLong(tCWP.hWnd, GWL_WNDPROC, AddressOf SubMsgBox)
        End If
    End If
    HookWindow = CallNextHookEx(lHook, nCode, wParam, ByVal lParam)
End Function
 
Public Function MsgBoxEx(ByVal Prompt As String, Optional ByVal Buttons As Long = vbOKOnly, Optional ByVal Title As String, Optional ByVal HelpFile As String, Optional ByVal Context As Long, Optional ByRef CustomButtons As Variant, Optional DefaultButton As String, Optional x As String, Optional y As String, Optional Width As String, Optional Height As String, Optional ByVal ForeColor As ColorConstants = -1, Optional ByVal BackColor As ColorConstants = -1) As Long
    Dim lReturn As Long
    
    bCustom = (Buttons = vbCustom)
    If bCustom And IsMissing(CustomButtons) Then
        MsgBox "When using the Custom option you need to supply some Buttons in the ""CustomButtons"" Argument.", vbExclamation + vbOKOnly, "Error"
        Exit Function
    End If
    lHook = SetWindowsHookEx(WH_CALLWNDPROC, AddressOf HookWindow, App.hInstance, App.ThreadID)
    'Set the Defaults
    If Len(Title) = 0 Then Title = App.Title
    If bCustom Then
        'User wants to use own Button Titles..
        If TypeName(CustomButtons) = "String" Then
            ReDim sButtons(0)
            sButtons(0) = CustomButtons
            Buttons = 0
        Else
            sButtons = CustomButtons
            Buttons = UBound(sButtons)
        End If
    End If
    
    'Added by manavo11
    lForecolor = GetSysColor(COLOR_BTNTEXT)
    lBackcolor = GetSysColor(COLOR_BTNFACE)
    
    If ForeColor >= 0 Then lForecolor = ForeColor
    If BackColor >= 0 Then lBackcolor = BackColor
    
    sDefaultButton = DefaultButton
    
    iX = x
    iY = y
    iWidth = Width
    iHeight = Height
    
    iButtonCount = UBound(sButtons)
    iButtonWidth = (iWidth - (2 * 14) - (6 * (Buttons + 1))) / (Buttons + 1)
    'Added by manavo11
    
    lButton = 0
    
    'Show the Modified MsgBox
    lReturn = MsgBox(Prompt, Buttons, Title, HelpFile, Context)
    Call UnhookWindowsHookEx(lHook)
    'If it's a Custom Button MsgBox, Alter the Return Value
    If bCustom Then lReturn = lReturn - (UBound(CustomButtons) + 1)
    bCustom = False
    MsgBoxEx = lReturn
End Function

' Returns the low-order word from the given 32-bit value.

Public Function LOWORD(dwValue As Long) As Integer
  MoveMemory LOWORD, dwValue, 2
End Function

' Returns the larger of the two passed params

Public Function Max(param1 As Long, param2 As Long) As Long
  If param1 > param2 Then Max = param1 Else Max = param2
End Function

Public Function GetStrFromBufferA(szA As String) As String
  If InStr(szA, vbNullChar) Then
    GetStrFromBufferA = Left$(szA, InStr(szA, vbNullChar) - 1)
  Else
    ' If sz had no null char, the Left$ function
    ' above would rtn a zero length string ("").
    GetStrFromBufferA = szA
  End If
End Function

