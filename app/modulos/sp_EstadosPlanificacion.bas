Attribute VB_Name = "spPlanEstadosPlanificacion"
' Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~

Option Explicit

Function sp_InsertEstadosPlanificacion(cod_estado_plan, nom_estado_plan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_InsertEstadosPlanificacion"
                .Parameters.Append .CreateParameter("@cod_estado_plan", adInteger, adParamInput, , cod_estado_plan)
                .Parameters.Append .CreateParameter("@nom_estado_plan", adChar, adParamInput, , nom_estado_plan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_InsertEstadosPlanificacion = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_InsertEstadosPlanificacion = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateEstadosPlanificacion(cod_estado_plan, nom_estado_plan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateEstadosPlanificacion"
                .Parameters.Append .CreateParameter("@cod_estado_plan", adInteger, adParamInput, , cod_estado_plan)
                .Parameters.Append .CreateParameter("@nom_estado_plan", adChar, adParamInput, , nom_estado_plan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, , estado_hab)
                Set aplRST = .Execute
        End With
        sp_UpdateEstadosPlanificacion = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateEstadosPlanificacion = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_UpdateEstadosPlanificacionField(x, campo, valortxt, valordate, valornum) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_UpdateEstadosPlanificacionField"
                .Parameters.Append .CreateParameter("@x", adInteger, adParamInput, , CLng(Val(x)))
                .Parameters.Append .CreateParameter("@campo", adChar, adParamInput, 15, campo)
                .Parameters.Append .CreateParameter("@valortxt", adChar, adParamInput, 10, valortxt)
                .Parameters.Append .CreateParameter("@valordate", SQLDdateType, adParamInput, , valordate)
                .Parameters.Append .CreateParameter("@valornum", adInteger, adParamInput, , IIf(IsNull(valornum), 0, CLng(Val("" & valornum))))
                Set aplRST = .Execute
        End With
        sp_UpdateEstadosPlanificacionField = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_UpdateEstadosPlanificacionField = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_DeleteEstadosPlanificacion(cod_estado_plan) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_DeleteEstadosPlanificacion"
                .Parameters.Append .CreateParameter("@cod_estado_plan", adInteger, adParamInput, , cod_estado_plan)
                Set aplRST = .Execute
        End With
        sp_DeleteEstadosPlanificacion = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_DeleteEstadosPlanificacion = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

Function sp_GetEstadosPlanificacion(cod_estado_plan, estado_hab) As Boolean
        On Error Resume Next
        aplRST.Close
        Set aplRST = Nothing
        On Error GoTo Err_SP
        Dim objCommand As ADODB.Command
        Set objCommand = New ADODB.Command
        With objCommand
                .ActiveConnection = aplCONN
                .CommandType = adCmdStoredProc
                .CommandText = "sp_GetEstadosPlanificacion"
                .Parameters.Append .CreateParameter("@cod_estado_plan", adInteger, adParamInput, , cod_estado_plan)
                '.Parameters.Append .CreateParameter("@nom_estado_plan", adChar, adParamInput, 50, nom_estado_plan)
                .Parameters.Append .CreateParameter("@estado_hab", adChar, adParamInput, 1, estado_hab)
                Set aplRST = .Execute
        End With
        sp_GetEstadosPlanificacion = True
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
        On Error GoTo 0
Exit Function
Err_SP:
        MsgBox (AnalizarErrorSQL(aplRST, Err))
        sp_GetEstadosPlanificacion = False
        Set objCommand.ActiveConnection = Nothing
        Set objCommand = Nothing
End Function

