Attribute VB_Name = "spDireccion"
Option Explicit

Function sp_GetDireccion(cod_direccion, Optional flg_habil) As Boolean
    If IsMissing(flg_habil) Then
        flg_habil = Null
    End If
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_GetDireccion"
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetDireccion = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_GetDireccion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateDireccion(modo, cod_direccion, nom_direccion, flg_habil, es_ejecutor, IGM, cAbreviatura) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_UpdateDireccion"
      .Parameters.Append .CreateParameter("@modo", adChar, adParamInput, 1, modo)
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      .Parameters.Append .CreateParameter("@nom_direccion", adChar, adParamInput, 80, nom_direccion)
      .Parameters.Append .CreateParameter("@flg_habil", adChar, adParamInput, 1, flg_habil)
      .Parameters.Append .CreateParameter("@es_ejecutor", adChar, adParamInput, 1, es_ejecutor)
      .Parameters.Append .CreateParameter("@IGM", adChar, adParamInput, 1, IGM)
      .Parameters.Append .CreateParameter("@abreviatura", adChar, adParamInput, 30, cAbreviatura)
      Set aplRST = .Execute
    End With
    sp_UpdateDireccion = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateDireccion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteDireccion(cod_direccion) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objError As Errors
    Dim objCommand As New ADODB.Command
    With objCommand
      .ActiveConnection = aplCONN
      .CommandType = adCmdStoredProc
      .CommandText = "sp_DeleteDireccion"
      .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
      Set aplRST = .Execute
    End With
    sp_DeleteDireccion = True
    If aplCONN.Errors.Count > 0 Then
            GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteDireccion = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

