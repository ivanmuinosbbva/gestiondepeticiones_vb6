Attribute VB_Name = "spAplicativo"
Option Explicit

Function sp_InsertAplicativo(app_id, app_nombre, app_hab, cod_direccion, cod_gerencia, cod_sector, cod_grupo, cod_r_direccion, cod_r_gerencia, cod_r_sector, cod_r_grupo, app_amb) As Boolean
    Dim var_numero As Integer
    var_numero = 0
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertAplicativo"
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@app_nombre", adChar, adParamInput, 100, app_nombre)
        .Parameters.Append .CreateParameter("@app_hab", adChar, adParamInput, 1, app_hab)
        .Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
        .Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        .Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@cod_r_direccion", adChar, adParamInput, 10, Null)
        .Parameters.Append .CreateParameter("@cod_r_gerencia", adChar, adParamInput, 10, Null)
        .Parameters.Append .CreateParameter("@cod_r_sector", adChar, adParamInput, 10, Null)
        .Parameters.Append .CreateParameter("@cod_r_grupo", adChar, adParamInput, 10, Null)
        '.Parameters.Append .CreateParameter("@cod_r_direccion", adChar, adParamInput, 10, cod_r_direccion)
        '.Parameters.Append .CreateParameter("@cod_r_gerencia", adChar, adParamInput, 10, cod_r_gerencia)
        '.Parameters.Append .CreateParameter("@cod_r_sector", adChar, adParamInput, 10, cod_r_sector)
        '.Parameters.Append .CreateParameter("@cod_r_grupo", adChar, adParamInput, 10, cod_r_grupo)
        .Parameters.Append .CreateParameter("@app_amb", adChar, adParamInput, 1, app_amb)
        Set aplRST = .Execute
    End With
    sp_InsertAplicativo = True
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertAplicativo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetAplicativo(app_id, app_nombre, app_hab) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetAplicativo"
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@app_nombre", adChar, adParamInput, 100, app_nombre)
        .Parameters.Append .CreateParameter("@app_hab", adChar, adParamInput, 1, app_hab)
        '.Parameters.Append .CreateParameter("@cod_direccion", adChar, adParamInput, 8, cod_direccion)
        '.Parameters.Append .CreateParameter("@cod_gerencia", adChar, adParamInput, 8, cod_gerencia)
        '.Parameters.Append .CreateParameter("@cod_sector", adChar, adParamInput, 8, cod_sector)
        '.Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        '.Parameters.Append .CreateParameter("@cod_r_direccion", adChar, adParamInput, 10, Null)
        '.Parameters.Append .CreateParameter("@cod_r_gerencia", adChar, adParamInput, 10, Null)
        '.Parameters.Append .CreateParameter("@cod_r_sector", adChar, adParamInput, 10, Null)
        '.Parameters.Append .CreateParameter("@cod_r_grupo", adChar, adParamInput, 10, Null)
        '.Parameters.Append .CreateParameter("@app_amb", adChar, adParamInput, 1, app_amb)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetAplicativo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetAplicativo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetResponsableAreaFinal(nivel, area) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetResponsableAreaFinal"
        .Parameters.Append .CreateParameter("@nivel", adChar, adParamInput, 4, nivel)
        .Parameters.Append .CreateParameter("@area", adChar, adParamInput, 8, area)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetResponsableAreaFinal = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetResponsableAreaFinal = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetGrupoAplicativo(cod_grupo, app_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetGrupoAplicativo"
        .Parameters.Append .CreateParameter("@cod_grupo", adChar, adParamInput, 8, cod_grupo)
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetGrupoAplicativo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetGrupoAplicativo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_GetHsTrabAplicativo(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, app_id, app_horas) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_GetHsTrabAplicativo"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        .Parameters.Append .CreateParameter("@horas", adSmallInt, adParamInput, , horas)
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@app_horas", adSmallInt, adParamInput, , app_horas)
        Set aplRST = .Execute
    End With
    If Not (aplRST.EOF) Then
        sp_GetHsTrabAplicativo = True
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    sp_GetHsTrabAplicativo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_InsertHsTrabAplicativo(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, app_id, app_horas, hsapp_texto) As Boolean
    Dim var_numero As Integer
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_InsertHsTrabAplicativo"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        .Parameters.Append .CreateParameter("@horas", adSmallInt, adParamInput, , horas)
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@app_horas", adSmallInt, adParamInput, , app_horas)
        .Parameters.Append .CreateParameter("@hsapp_texto", adVarChar, adParamInput, 255, hsapp_texto)
        Set aplRST = .Execute
    End With
    sp_InsertHsTrabAplicativo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_InsertHsTrabAplicativo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHsTrabAplicativo(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, app_id, app_horas, hsapp_texto) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateHsTrabAplicativo"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        .Parameters.Append .CreateParameter("@horas", adSmallInt, adParamInput, , horas)
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        .Parameters.Append .CreateParameter("@app_horas", adSmallInt, adParamInput, , app_horas)
        .Parameters.Append .CreateParameter("@hsapp_texto", adVarChar, adParamInput, 255, hsapp_texto)
        Set aplRST = .Execute
    End With
    sp_UpdateHsTrabAplicativo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHsTrabAplicativo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_UpdateHsTrabAplicativo2(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas_ant, horas_act) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_UpdateHsTrabAplicativo2"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        .Parameters.Append .CreateParameter("@horas_ant", adSmallInt, adParamInput, , Val(horas_ant))
        .Parameters.Append .CreateParameter("@horas_act", adSmallInt, adParamInput, , Val(horas_act))
        Set aplRST = .Execute
    End With
    sp_UpdateHsTrabAplicativo2 = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_UpdateHsTrabAplicativo2 = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function

Function sp_DeleteHsTrabAplicativo(cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, app_id) As Boolean
    On Error Resume Next
    aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    Dim objCommand As ADODB.Command
    Set objCommand = New ADODB.Command
    With objCommand
        .ActiveConnection = aplCONN
        .CommandType = adCmdStoredProc
        .CommandText = "sp_DeleteHsTrabAplicativo"
        .Parameters.Append .CreateParameter("@cod_recurso", adChar, adParamInput, 10, cod_recurso)
        .Parameters.Append .CreateParameter("@cod_tarea", adChar, adParamInput, 8, cod_tarea)
        .Parameters.Append .CreateParameter("@pet_nrointerno", adInteger, adParamInput, , CLng(Val(ClearNull(pet_nrointerno))))
        .Parameters.Append .CreateParameter("@fe_desde", SQLDdateType, adParamInput, , fe_desde)
        .Parameters.Append .CreateParameter("@fe_hasta", SQLDdateType, adParamInput, , fe_hasta)
        .Parameters.Append .CreateParameter("@horas", adSmallInt, adParamInput, , horas)
        .Parameters.Append .CreateParameter("@app_id", adChar, adParamInput, 30, app_id)
        Set aplRST = .Execute
    End With
    sp_DeleteHsTrabAplicativo = True
    If aplCONN.Errors.Count > 0 Then
        GoTo Err_SP
    End If
    If ((Not IsEmpty(aplRST)) And (Not aplRST.State = adStateClosed)) Then
        If ((Not aplRST.EOF) And (aplRST(0).Name = "ErrCode")) Then
            GoTo Err_SP
        End If
    End If
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    MsgBox (AnalizarErrorSQL(aplRST, Err, aplCONN))
    sp_DeleteHsTrabAplicativo = False
    Set objCommand.ActiveConnection = Nothing
    Set objCommand = Nothing
End Function
