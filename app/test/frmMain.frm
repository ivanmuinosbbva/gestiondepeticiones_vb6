VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Begin VB.Form frmMain 
   Caption         =   "Form1"
   ClientHeight    =   7215
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12720
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7215
   ScaleWidth      =   12720
   StartUpPosition =   3  'Windows Default
   Begin ComCtl3.CoolBar cbSuites 
      Align           =   1  'Align Top
      Height          =   495
      Left            =   0
      TabIndex        =   5
      Top             =   570
      Width           =   12720
      _ExtentX        =   22437
      _ExtentY        =   873
      BandCount       =   1
      _CBWidth        =   12720
      _CBHeight       =   495
      _Version        =   "6.7.9782"
      Caption1        =   "Test:"
      Child1          =   "pbMain"
      MinHeight1      =   435
      Width1          =   2055
      NewRow1         =   0   'False
      Begin MSComctlLib.ProgressBar pbMain 
         Height          =   435
         Left            =   615
         TabIndex        =   6
         Top             =   30
         Width           =   12015
         _ExtentX        =   21193
         _ExtentY        =   767
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.StatusBar sbMain 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   4
      Top             =   6900
      Width           =   12720
      _ExtentX        =   22437
      _ExtentY        =   556
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tbMain 
      Align           =   1  'Align Top
      Height          =   570
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   12720
      _ExtentX        =   22437
      _ExtentY        =   1005
      ButtonWidth     =   1349
      ButtonHeight    =   953
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imgSuites"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Conectar"
            Key             =   "btnConectar"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Ejecutar"
            Key             =   "btnEjecutar"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Detener"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Nuevo"
            Key             =   "NewsOne"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.TreeView trwPaquetes 
      Height          =   5775
      Left            =   60
      TabIndex        =   2
      Top             =   1080
      Width           =   3915
      _ExtentX        =   6906
      _ExtentY        =   10186
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      HotTracking     =   -1  'True
      SingleSel       =   -1  'True
      ImageList       =   "imgSuites"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList imgSuites 
      Left            =   12060
      Top             =   1080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":10BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1658
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1BF2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":218C
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2726
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2CC0
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":325A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":37F4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin GesPetTest.AdoConnection AdoConnection 
      Left            =   12120
      Top             =   60
      _ExtentX        =   873
      _ExtentY        =   873
      Version         =   "1.0.0"
   End
   Begin MSComctlLib.ListView lswTests 
      Height          =   2775
      Left            =   4020
      TabIndex        =   1
      Top             =   4080
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   4895
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "imgSuites"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lswSuites 
      Height          =   2955
      Left            =   4020
      TabIndex        =   0
      Top             =   1080
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   5212
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "imgSuites"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'' Declaraci�n de las API's de Windows para el manejo y control de la apariencia de Windows XP
'Private Declare Function LoadLibrary Lib "kernel32" Alias _
'  "LoadLibraryA" (ByVal lpLibFileName As String) As Long
'Private Declare Function FreeLibrary Lib "kernel32" ( _
'  ByVal hLibModule As Long) As Long
'Private Declare Function SetErrorMode Lib "kernel32" ( _
'  ByVal wMode As Long) As Long
'Private Declare Function GetUserName Lib "advapi32.dll" _
'  Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
'
'Private Const SEM_FAILCRITICALERRORS = &H1
'Private Const SEM_NOGPFAULTERRORBOX = &H2
'Private Const SEM_NOOPENFILEERRORBOX = &H8000&
'Private m_hMod As Long

' -----------------------------------------------------------------------------------------------------------------------

Dim glAPLICACION_SYBASE As String
Dim glBASE_APLICACION As String
Dim glSERVER_DSN As String
Dim glSERVER_SEGURIDAD As String
Dim glPASSWORD_SEGURIDAD As String
Dim glBASE_SEGURIDAD As String
Dim glUSUARIO_SEGURIDAD As String
Dim glPASSWORD_ENCRIPTADO As String
Dim glESQUEMA_SEGURIDAD As String

Dim glENTORNO As String
Dim glBASE As String
Dim glSERVER_PUERTO As String
Dim glBASEUSR As String
Dim glBASEPWD As String
Dim glWRKDIR As String

Dim glArchivoINI As String
Dim glONLINE As Boolean

Private Sub Form_Load()
'    ' Inicializamos la aplicacion del estilo visual a la ventana actual
'    Call InitCommonControlsVB
'    m_hMod = LoadLibrary("shell32.dll")
    Call InicializarPantalla
    Call lswSuites_CargarLista
    pbMain.Value = 65
End Sub

Private Sub InicializarPantalla()
    Const IMG_PAQUETES = 10
    
    Call Color_Fondo(pbMain.hwnd, vbWhite)          ' Establece el Backcolor
    Call Color_Progreso(pbMain.hwnd, vbGreen)     ' Establece el color del progress

    With trwPaquetes
        .Nodes.Clear
        .Nodes.Add , , "root", "Paquetes", 8
        
        .Nodes.Add "root", tvwChild, "general", "General", IMG_PAQUETES
        .Nodes.Add "root", tvwChild, "perfiles", "Perfiles", IMG_PAQUETES
        .Nodes.Add "root", tvwChild, "proyectos", "Proyectos", IMG_PAQUETES
        .Nodes.Add "root", tvwChild, "consultas", "Consultas", IMG_PAQUETES
        .Nodes.Add "root", tvwChild, "transmisiones", "Transmisiones", IMG_PAQUETES
        .Nodes.Add "root", tvwChild, "horas", "Carga de horas", IMG_PAQUETES
        .Nodes.Add "root", tvwChild, "reportes", "Reportes", IMG_PAQUETES
        .Nodes.Add "root", tvwChild, "info", "Info", IMG_PAQUETES
        
        .Nodes.Add "general", tvwChild, "login", "Login", 4
        .Nodes.Add "general", tvwChild, "logout", "Logout", 4
        .Nodes.Add "general", tvwChild, "cambiopwd", "Cambio de contrase�a", 4
        .Nodes.Add "general", tvwChild, "salir", "Salir (desconexi�n)", 4
        
        .Nodes.Add "perfiles", tvwChild, "solicitante", "Solicitante", 4
        .Nodes.Add "perfiles", tvwChild, "referente", "Referente", 4
        .Nodes.Add "perfiles", tvwChild, "supervisor", "Supervisor", 4
        .Nodes.Add "perfiles", tvwChild, "autorizante", "Autorizante", 4
        .Nodes.Add "perfiles", tvwChild, "administrador", "Administrador", 4
        .Nodes.Add "perfiles", tvwChild, "superadministrador", "Superadministrador", 4
        .Nodes.Add "perfiles", tvwChild, "referente_sistema", "Referente de sistema", 4
        .Nodes.Add "perfiles", tvwChild, "referente_rgp", "Referente de RGP", 4
        .Nodes.Add "perfiles", tvwChild, "planificacion", "Planificaci�n", 4
        .Nodes.Add "perfiles", tvwChild, "informes_homo", "Informes de homologaci�n", 4
        .Nodes.Add "perfiles", tvwChild, "analista", "Analista ejecutor", 4
        .Nodes.Add "perfiles", tvwChild, "responsable_grupo", "Responsable de ejecuci�n", 4
        .Nodes.Add "perfiles", tvwChild, "responsable_sector", "Responsable de sector", 4
        .Nodes.Add "perfiles", tvwChild, "responsable_gerencia", "Responsable de gerencia", 4
        .Nodes.Add "perfiles", tvwChild, "responsable_direccion", "Responsable de Direcci�n", 4
        .Nodes.Add "perfiles", tvwChild, "agrupamientos", "Agrupamientos", 4
        .Nodes.Add "perfiles", tvwChild, "mensajes", "Mensajes", 4
        .Nodes.Add "perfiles", tvwChild, "reemplazar", "Reemplazar recurso", 4
        .Nodes.Add "perfiles", tvwChild, "delegar", "Delegar funciones", 4
        
        .Nodes.Add "proyectos", tvwChild, "proyecto", "Administrar proyectos IDM", 4
        
        .Nodes.Add "consultas", tvwChild, "consulta_general", "Consulta general de peticiones", 4
        .Nodes.Add "consultas", tvwChild, "exportacion_general", "Exportaci�n general de peticiones", 4
        .Nodes.Add "consultas", tvwChild, "consulta_recursos", "Consulta de peticiones por recurso", 4
        .Nodes.Add "consultas", tvwChild, "buscar_peticion", "Buscar una petici�n", 4
        
        .Nodes.Add "transmisiones", tvwChild, "solicitudes_host", "Solicitudes HOST", 4
        .Nodes.Add "transmisiones", tvwChild, "catalogo_host", "Cat�logo HOST", 4
        .Nodes.Add "transmisiones", tvwChild, "solicitudes_ssdd", "Solicitudes SSDD", 4
        .Nodes.Add "transmisiones", tvwChild, "catalogo_ssdd", "Cat�logo SSDD", 4
        .Nodes.Add "transmisiones", tvwChild, "autorizaciones", "Autorizaciones", 4
        
        .Nodes.Add "horas", tvwChild, "carga_horas", "Carga de horas", 4
        
        .Nodes.Add "reportes", tvwChild, "generar_reportes", "Generaci�n de reportes", 4
        
        .Nodes.Add "info", tvwChild, "novedades", "Novedades de la versi�n", 4
        .Nodes.Add "info", tvwChild, "versionado", "Versionado", 4

        .Nodes(1).Expanded = True
    End With

    With lswSuites
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "Id.", 800
        .ColumnHeaders.Add , , "Suite Name", 4000
        .ColumnHeaders.Add , , "Suite status", 2000
        .ColumnHeaders.Add , , "Suite code name", 2000
        .ColumnHeaders.Add , , "Paquete", 2000
        .ListItems.Clear
    End With
    
    With lswTests
        .ColumnHeaders.Clear
        .ColumnHeaders.Add , , "Paquete", 2000
        .ColumnHeaders.Add , , "Id.", 2000
        .ColumnHeaders.Add , , "Name", 3500
        .ColumnHeaders.Add , , "Status", 2000
        .ColumnHeaders.Add , , "Test name", 2000
        .ListItems.Clear
    End With
End Sub

Private Sub lswSuites_CargarLista()
    Dim item As ListItem
    
    With lswSuites
        Set item = .ListItems.Add(, , "1", , 2)
        item.SubItems(1) = "Suite - CatalogoTabla"
        item.SubItems(2) = "Pendiente"
        item.SubItems(3) = "TESTSuite_CatalogoTabla"
        item.SubItems(4) = "Solicitudes SSDD"
    End With
    Call lswTests_CargarLista
End Sub

Private Sub lswTests_CargarLista()
    Dim item As ListItem
    
    With lswTests
        Set item = .ListItems.Add(, , "Solicitudes SDD", , 2)
        item.SubItems(1) = "TEST_sp_InsertCatalogoTabla"
        item.SubItems(2) = "Pendiente"
        item.SubItems(3) = "TEST_sp_InsertCatalogoTabla"
        
        Set item = .ListItems.Add(, , "2", , 2)
        item.SubItems(1) = "TEST_sp_UpdateCatalogoTabla"
        item.SubItems(2) = "Pendiente"
        item.SubItems(3) = "TEST_sp_UpdateCatalogoTabla"
        
        Set item = .ListItems.Add(, , "3", , 2)
        item.SubItems(1) = "TEST_sp_DeleteCatalogoTabla"
        item.SubItems(2) = "Pendiente"
        item.SubItems(3) = "TEST_sp_DeleteCatalogoTabla"
        
        Set item = .ListItems.Add(, , "4", , 2)
        item.SubItems(1) = "TEST_sp_GetCatalogoTabla"
        item.SubItems(2) = "Pendiente"
        item.SubItems(3) = "TEST_sp_GetCatalogoTabla"
    End With
End Sub


Private Sub Command1_Click()
    Dim i As Long
    'Call TESTSuite_CatalogoTabla
    'Call CallByName(Test_spCatalogoTabla.TESTSuite_CatalogoTabla, "TESTSuite_CatalogoTabla", VbMethod)
    With lswTests
        For i = 0 To .ListItems.Count
            If .ListItems(i).Checked Then
                If .ListItems(i).ListSubItems(3).Text = "TEST_sp_InsertCatalogoTabla" Then
                    Call TEST_sp_InsertCatalogoTabla
                End If
            End If
        Next i
    End With
End Sub

Public Function InvisibleLogin(sUserName As String, sUserPassword As String) As Boolean
    glAdoError = ""
    With frmMain
        .AdoConnection.CerrarConexion
        .AdoConnection.SetODBCDriver glDRIVER_ODBC
        .AdoConnection.UsuarioAplicacion = sUserName
        .AdoConnection.PasswordAplicacion = sUserPassword
        .AdoConnection.BaseAplicacion = ""
        .AdoConnection.ComponenteSeguridad = ""
        .AdoConnection.Aplicacion = glAPLICACION_SYBASE
        .AdoConnection.BaseAplicacion = glBASE_APLICACION
        .AdoConnection.ServidorDSN = glSERVER_DSN
        .AdoConnection.Servidor = glSERVER_SEGURIDAD
        .AdoConnection.PasswordSeguridad = glPASSWORD_SEGURIDAD
        .AdoConnection.BaseSeguridad = glBASE_SEGURIDAD
        .AdoConnection.UsuarioSeguridad = glUSUARIO_SEGURIDAD
        .AdoConnection.TimeOut = 120
        .AdoConnection.Encriptado = IIf(CInt(glPASSWORD_ENCRIPTADO) > 0, True, False)
        .AdoConnection.EsquemaSeguridad = IIf(glESQUEMA_SEGURIDAD = "SI", True, False)
        .AdoConnection.AbrirConexion
        If .AdoConnection.IsOnLine Then
            If Not .AdoConnection.CrearDSN("Conexion Para Crystal", "CRREP", .AdoConnection.BaseAplicacion) Then
                MsgBox "Error DSN CRREPORT:" & Chr$(13) & glAdoError
            End If
            '.sbPrincipal.Panels(2) = sUserName
            '.sbPrincipal.Panels(6) = .AdoConnection.Servidor '& " (" & .AdoConnection.DRIVER & "/" & glENTORNO & ")"
            '.sbPrincipal.Panels(6).ToolTipText = .sbPrincipal.Panels(6).Text
            'glLOGIN_ID_REAL = sUserName
            'glLOGIN_PASSWORD = sUserPassword
            Set aplCONN = .AdoConnection.ConexionBase
            'Call PerfilCrear
            '.sbPrincipal.Panels(4) = ""
            glONLINE = True
            InvisibleLogin = True
            'cmdContinuar_Click
        Else
            'Call setHabilCtrl(cmdAceptar, Normal)
            glONLINE = False
            InvisibleLogin = False
        End If
    End With
End Function

Private Sub Inicializar()
    'Call InitCommonControlsVB      ' Habilitar esta l�nea para usar el tema del XP (Look & Feel)
    DoEvents
    glONLINE = False
    glArchivoINI = App.Path & "\" & "pet.ini"
    If UCase(Dir(glArchivoINI)) <> "" Then
        glENTORNO = LeerINI("SISTEMA", "ENTORNO", "", glArchivoINI)
        glBASE = LeerINI("SISTEMA", "BASE", "DESA", glArchivoINI)
        'glHEADAPLICACION = LeerINI("HEAD_" & glBASE, "APLICACION", "<aplicaci�n>", glArchivoINI)
        'glHEADEMPPRESA = LeerINI("HEAD_" & glBASE, "EMPRESA", "<empresa>", glArchivoINI)
        glDRIVER_ODBC = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "DRIVER", glDRIVER_ODBC, glArchivoINI)
        glBASE_APLICACION = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "BASE_APLICACION", "", glArchivoINI)
        glBASE_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "BASE_SEGURIDAD", "", glArchivoINI)
        glSERVER_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "SERVIDOR_SEGURIDAD", "", glArchivoINI)
        glSERVER_DSN = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "SERVIDOR_DSN", "", glArchivoINI)            ' add *
        glSERVER_PUERTO = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "SERVIDOR_PUERTO", "", glArchivoINI)      ' add *
        glAPLICACION_SYBASE = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "APLICACION_SYBASE", "", glArchivoINI)
        glUSUARIO_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "USUARIO_SEGURIDAD", "", glArchivoINI)
        glESQUEMA_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "ESQUEMA_SEGURIDAD", "SI", glArchivoINI)
        glPASSWORD_ENCRIPTADO = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "PASSWORD_ENCRIPTADO", "0", glArchivoINI)
        glPASSWORD_SEGURIDAD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "PASSWORD_SEGURIDAD", "", glArchivoINI)
        glBASEUSR = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "ARG_STACK0", "", glArchivoINI)
        glBASEPWD = LeerINI("CONEXION_" & glBASE & "_" & glENTORNO, "ARG_STACK1", "", glArchivoINI)
        glWRKDIR = LeerINI("EXPORT", "WRKDIR", "c:\GesPet\", glArchivoINI)
        If InStr(UCase(glDRIVER_ODBC), "SYBASE") > 0 Then
            SQLDdateType = adDBDate
        ElseIf InStr(UCase(glDRIVER_ODBC), "SQL SERVER") > 0 Then
            SQLDdateType = adDBTimeStamp
        Else
            SQLDdateType = adDBDate
        End If
        On Error Resume Next
        On Error GoTo 0
        Call DeclaracionesGenerales.CaptureCommandLineArguments
        If glONLINE Then
'            If Not ValidarVersionDelAplicativo Then
'                End
'            End If
            MsgBox "Conectado!", vbInformation + vbOKOnly, "DB Connection"
        End If
    Else
        'Call Puntero(False)
        'Call Status("Listo.")
        MsgBox "Archivo de inicializaci�n inv�lido.", vbCritical, "Error de configuraci�n"
        End
    End If
End Sub

Private Sub Command2_Click()
    Call Inicializar
End Sub

Private Sub Form_Resize()
    If Me.WindowState <> vbMinimized Then
        trwPaquetes.Height = Me.ScaleHeight - tbMain.Height - sbMain.Height - 100
        
        'cbSuites.Width = Me.ScaleWidth - trwPaquetes.Width - 200
        
        lswSuites.Height = (trwPaquetes.Height / 2) - 100
        'lswSuites.Top = trwPaquetes.Top
        lswSuites.Top = trwPaquetes.Top + 50
        lswSuites.Width = Me.ScaleWidth - trwPaquetes.Width - 200
        lswSuites.Left = trwPaquetes.Width + 100
        
        lswTests.Height = (trwPaquetes.Height / 2) - cbSuites.Height - 50
        lswTests.Top = lswSuites.Height + 1200
        lswTests.Width = Me.ScaleWidth - trwPaquetes.Width - 200
        lswTests.Left = trwPaquetes.Width + 100
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If glONLINE Then
        Call AdoConnection.CerrarConexion
    End If
    End
End Sub

Private Sub tbMain_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "btnEjecutar"
            Call Ejecutar
        Case "btnConectar"
            Call Inicializar
        Case "NewsOne"
            Load frmPreferencias
            frmPreferencias.Show
    End Select
End Sub

Private Sub Ejecutar()
    Dim i As Long
    'Call TESTSuite_CatalogoTabla
    'Call CallByName(Test_spCatalogoTabla.TESTSuite_CatalogoTabla, "TESTSuite_CatalogoTabla", VbMethod)
    With lswTests
        For i = 0 To .ListItems.Count
            If .ListItems(i).Checked Then
                If .ListItems(i).ListSubItems(3).Text = "TEST_sp_InsertCatalogoTabla" Then
                    Call TEST_sp_InsertCatalogoTabla
                End If
            End If
        Next i
    End With
End Sub
