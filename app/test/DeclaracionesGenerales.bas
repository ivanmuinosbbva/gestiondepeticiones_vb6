Attribute VB_Name = "DeclaracionesGenerales"
Option Explicit

Global aplCONN As ADODB.Connection
Global aplRST As New ADODB.Recordset
Global glAdoError As String
Global ARCHIVOS_ADJUNTOS_MAXSIZE As Long
Global glDRIVER_ODBC As String
Global SQLDdateType As Long

Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal lSize As Long, ByVal lpFileName As String) As Long

Public Type udtLOGINSYBASE
    UserId As String            ' Server user ID
    UserFullName As String      ' Full name of the user
    LockReason As Long
    LoginCount As Long          ' Number of failed login attempts; reset to 0 by a successful login
    Pwdate As Date              ' Date the password was last changed
    LastLoginDate As Date       ' Date of the last login
    Status As Long              ' Status of the account (see Table 1-16)
End Type

Public Sub Status(sMensaje As String)
    With frmMain
'        .sbPrincipal.Panels(1) = Trim(sMensaje)
'        If Len(sMensaje) > 30 Then
'            .sbPrincipal.Panels(1).ToolTipText = Trim(sMensaje)
'        Else
'            .sbPrincipal.Panels(1).ToolTipText = ""
'        End If
    End With
    DoEvents
End Sub

Function ClearNull(sTexto As Variant)
    If IsNull(sTexto) Then
        ClearNull = ""
    Else
        ClearNull = Trim(sTexto)
    End If
End Function

Public Sub Puntero(bOpcion As Boolean)
    Screen.MousePointer = IIf(bOpcion, vbHourglass, vbNormal)
End Sub

Function ReplaceComma(cValue As String)
    'Cambia la coma decimal por punto en un string
    Dim nPos%
    If InStr(cValue, ",") <> 0 Then
       nPos = InStr(cValue, ",")
       cValue = Left$(cValue, nPos - 1) & "." & Mid$(cValue, nPos + 1)
    End If
    ReplaceComma = cValue
End Function

Function LeerINI(sApp As String, sKey As String, sDefault As String, sINI As String) As String
    '**************************************************************************
    'Lee valores de un archivo INI
    'Parametros:    sApp (aplicacion)
    '               sKey (item)
    '               sDefault (valor default)
    '               sINI (archivo INI a leer)
    '**************************************************************************
    Dim lRetCode As Long
    LeerINI = String(254, Chr(0))
    lRetCode = GetPrivateProfileString(sApp, sKey, sDefault, LeerINI, 254, sINI)
    LeerINI = Left(LeerINI, InStr(1, LeerINI, Chr(0)) - 1)
End Function

Public Sub CaptureCommandLineArguments()
    Dim sCommandLineArguments As String         ' Cadena completa de argumentos y valores
    Dim sSimpleCommandLineArgument As String    ' Nombre del argumento
    Dim nInicio As Integer                      ' Para indicar la posici�n inicial de cada vuelta
    Dim nPosicionInicialValor As Integer
    Dim nPosicionFinalValor As Integer
    Dim pUsuario, pPassword As String
    Dim bAutomaticLogOutlogIn As Boolean
    
    sCommandLineArguments = Trim(Command)       ' Obtiene los argumentos en bruto
    
    nInicio = 1: nPosicionInicialValor = 1
    Do While nPosicionInicialValor <> 0 And nInicio < Len(sCommandLineArguments)
        nPosicionInicialValor = InStr(nInicio, sCommandLineArguments, "-")
        sSimpleCommandLineArgument = Mid(sCommandLineArguments, nPosicionInicialValor + 1, 3)
        Select Case Trim(UCase(sSimpleCommandLineArgument))
            Case "USR"  ' Indica el nombre del usuario de Windows
                nPosicionFinalValor = InStr(nPosicionInicialValor + 4, sCommandLineArguments, "-", vbTextCompare) - 1
                If nPosicionFinalValor = -1 Then nPosicionFinalValor = Len(sCommandLineArguments)
                pUsuario = Trim(Mid(sCommandLineArguments, nPosicionInicialValor + 5, (nPosicionFinalValor - nPosicionInicialValor) - 4))
            Case "PWD"  ' Contrase�a del usuario
                nPosicionFinalValor = InStr(nPosicionInicialValor + 4, sCommandLineArguments, "-", vbTextCompare) - 1
                If nPosicionFinalValor = -1 Then nPosicionFinalValor = Len(sCommandLineArguments)
                pPassword = Trim(Mid(sCommandLineArguments, nPosicionInicialValor + 5, (nPosicionFinalValor - nPosicionInicialValor) - 4))
            Case "OFF"  ' Habilita el logout autom�tico
                nPosicionFinalValor = nPosicionFinalValor + 4
                bAutomaticLogOutlogIn = True
            Case Else
                ' Se pas� un argumento inexistente (unhandled), es omitido y se pasa a evaluar el siguiente
                nPosicionFinalValor = nPosicionFinalValor + 4
        End Select
        nInicio = nPosicionFinalValor + 1
    Loop
    If Len(pUsuario) > 0 And Len(pPassword) > 0 Then
        ' Se han pasado como argumentos expl�citamente el usuario y la contrase�a: se realiza el login autom�tico
        frmMain.InvisibleLogin UCase(pUsuario), FuncionesEncriptacion.Desencriptar(pPassword)
    End If
End Sub
