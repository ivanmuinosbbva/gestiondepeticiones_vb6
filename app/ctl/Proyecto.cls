VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Proyecto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Proyecto"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarProyecto As Long 'local copy
Private mvarSubProyecto As Long 'local copy
Private mvarMacroProyecto As Long 'local copy
Private mvarTitulo As String 'local copy
Public Property Let Titulo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Titulo = 5
    mvarTitulo = vData
End Property


Public Property Get Titulo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Titulo
    Titulo = mvarTitulo
End Property



Public Property Let MacroProyecto(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.MacroProyecto = 5
    mvarMacroProyecto = vData
End Property


Public Property Get MacroProyecto() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.MacroProyecto
    MacroProyecto = mvarMacroProyecto
End Property



Public Property Let SubProyecto(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SubProyecto = 5
    mvarSubProyecto = vData
End Property


Public Property Get SubProyecto() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SubProyecto
    SubProyecto = mvarSubProyecto
End Property



Public Property Let Proyecto(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Proyecto = 5
    mvarProyecto = vData
End Property


Public Property Get Proyecto() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Proyecto
    Proyecto = mvarProyecto
End Property



