VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "HorasTrabajadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarLegajo As String 'local copy
Private mvarTarea As String 'local copy
Private mvarPeticionNroInterno As Long 'local copy
Private mvarFechaDesde As Date 'local copy
Private mvarFechaHasta As Date 'local copy
Private mvarHoras As Long 'local copy
Private mvarSinAsignar As Boolean 'local copy
Private mvarObservaciones As String 'local copy

Public Property Let Observaciones(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Observaciones = 5
    mvarObservaciones = vData
End Property


Public Property Get Observaciones() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Observaciones
    Observaciones = mvarObservaciones
End Property



Public Property Let SinAsignar(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.SinAsignar = 5
    mvarSinAsignar = vData
End Property


Public Property Get SinAsignar() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SinAsignar
    SinAsignar = mvarSinAsignar
End Property



Public Property Let Horas(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Horas = 5
    mvarHoras = vData
End Property


Public Property Get Horas() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Horas
    Horas = mvarHoras
End Property



Public Property Let FechaHasta(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FechaHasta = 5
    mvarFechaHasta = vData
End Property


Public Property Get FechaHasta() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FechaHasta
    FechaHasta = mvarFechaHasta
End Property



Public Property Let FechaDesde(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FechaDesde = 5
    mvarFechaDesde = vData
End Property


Public Property Get FechaDesde() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FechaDesde
    FechaDesde = mvarFechaDesde
End Property



Public Property Let PeticionNroInterno(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PeticionNroInterno = 5
    mvarPeticionNroInterno = vData
End Property


Public Property Get PeticionNroInterno() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PeticionNroInterno
    PeticionNroInterno = mvarPeticionNroInterno
End Property



Public Property Let Tarea(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tarea = 5
    mvarTarea = vData
End Property


Public Property Get Tarea() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tarea
    Tarea = mvarTarea
End Property



Public Property Let Legajo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Legajo = 5
    mvarLegajo = vData
End Property


Public Property Get Legajo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Legajo
    Legajo = mvarLegajo
End Property



