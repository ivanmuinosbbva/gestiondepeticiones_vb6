VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Grupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Sector"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarCodigo As String 'local copy
Private mvarNombre As String 'local copy
Private mvarSector As Sector 'local copy
Private mvarHabilitado As Boolean 'local copy
Private mvarEjecutor As Boolean 'local copy
Private mvarReferente As Recurso 'local copy
Private mvarAbreviatura As String 'local copy
Private mvarTipo As String 'local copy
Private mvarConsideraHorasEstimadas As Boolean 'local copy
Private mvarConsideraFechasPlanificacion As Boolean 'local copy
Private mvarConsideraEstados As Boolean 'local copy
'local variable(s) to hold property value(s)
Private mvarPeticionesEspeciales As String 'local copy

Public Property Let PeticionesEspeciales(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PeticionesEspeciales = 5
    mvarPeticionesEspeciales = vData
End Property


Public Property Get PeticionesEspeciales() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PeticionesEspeciales
    PeticionesEspeciales = mvarPeticionesEspeciales
End Property

Public Property Let ConsideraEstados(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ConsideraEstados = 5
    mvarConsideraEstados = vData
End Property

Public Property Get ConsideraEstados() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ConsideraEstados
    ConsideraEstados = mvarConsideraEstados
End Property



Public Property Let ConsideraFechasPlanificacion(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ConsideraFechasPlanificacion = 5
    mvarConsideraFechasPlanificacion = vData
End Property


Public Property Get ConsideraFechasPlanificacion() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ConsideraFechasPlanificacion
    ConsideraFechasPlanificacion = mvarConsideraFechasPlanificacion
End Property



Public Property Let ConsideraHorasEstimadas(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ConsideraHorasEstimadas = 5
    mvarConsideraHorasEstimadas = vData
End Property


Public Property Get ConsideraHorasEstimadas() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ConsideraHorasEstimadas
    ConsideraHorasEstimadas = mvarConsideraHorasEstimadas
End Property



Public Property Let Tipo(ByVal vData As String)
Attribute Tipo.VB_Description = "Tipo de grupo:\r\nHomologación, Técnico, Funcional, Seguridad Informática, Tecnología, etc."
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tipo = 5
    mvarTipo = vData
End Property


Public Property Get Tipo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tipo
    Tipo = mvarTipo
End Property



Public Property Let Abreviatura(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Abreviatura = 5
    mvarAbreviatura = vData
End Property


Public Property Get Abreviatura() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Abreviatura
    Abreviatura = mvarAbreviatura
End Property



Public Property Set Referente(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Referente = Form1
    Set mvarReferente = vData
End Property


Public Property Get Referente() As Recurso
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Referente
    Set Referente = mvarReferente
End Property



Public Property Let Ejecutor(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Ejecutor = 5
    mvarEjecutor = vData
End Property


Public Property Get Ejecutor() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Ejecutor
    Ejecutor = mvarEjecutor
End Property



Public Property Let Habilitado(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Habilitado = 5
    mvarHabilitado = vData
End Property


Public Property Get Habilitado() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Habilitado
    Habilitado = mvarHabilitado
End Property



Public Property Set Sector(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Sector = Form1
    Set mvarSector = vData
End Property


Public Property Get Sector() As Sector
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Sector
    Set Sector = mvarSector
End Property



Public Property Let Nombre(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nombre = 5
    mvarNombre = vData
End Property


Public Property Get Nombre() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nombre
    Nombre = mvarNombre
End Property



Public Property Let Codigo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Codigo = 5
    mvarCodigo = vData
End Property


Public Property Get Codigo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Codigo
    Codigo = mvarCodigo
End Property



