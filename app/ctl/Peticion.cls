VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Peticion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Proyecto"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarTipo As String 'local copy
Private mvarClase As String 'local copy
Private mvarNumeroInterno As Long 'local copy
'local variable(s) to hold property value(s)
Private mvarTitulo As String 'local copy
'local variable(s) to hold property value(s)
Private mvarNumeroAsignado As Long 'local copy
Private mvarFechaAlta As Date 'local copy
Private mvarFechaPedido As Date 'local copy
Private mvarInicioPlanificacion As Date 'local copy
Private mvarFinPlanificacion As Variant 'local copy
Private mvarInicioReal As Date 'local copy
Private mvarFinReal As Date 'local copy
Private mvarHorasPresupuestadas As Long 'local copy
'local variable(s) to hold property value(s)
Private mvarSectorSolicitante As Sector 'local copy
Private mvarSolicitante As Recurso 'local copy
Private mvarReferente As Recurso 'local copy
Private mvarSupervisor As Recurso 'local copy
Private mvarAutorizante As Recurso 'local copy
Private mvarEstado As String 'local copy
Private mvarSituacion As String 'local copy
Private mvarCorporativo As Boolean 'local copy
Private mvarReferenteDeSistema As Recurso 'local copy
Private mvarReferenteDeRGyP As Recurso 'local copy
Private mvarImpactoTecnologico As Boolean 'local copy
Private mvarRegulatorio As Boolean 'local copy
Private mvarProyecto As Proyecto 'local copy
Private mvarEmpresa As Long 'local copy
Private mvarRiesgoOperacional As String 'local copy
Private mvarGestion As String 'local copy
Public Property Let Gestion(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Gestion = 5
    mvarGestion = vData
End Property


Public Property Get Gestion() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Gestion
    Gestion = mvarGestion
End Property



Public Property Let RiesgoOperacional(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.RiesgoOperacional = 5
    mvarRiesgoOperacional = vData
End Property


Public Property Get RiesgoOperacional() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.RiesgoOperacional
    RiesgoOperacional = mvarRiesgoOperacional
End Property



Public Property Let Empresa(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Empresa = 5
    mvarEmpresa = vData
End Property


Public Property Get Empresa() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Empresa
    Empresa = mvarEmpresa
End Property



Public Property Set Proyecto(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Proyecto = Form1
    Set mvarProyecto = vData
End Property


Public Property Get Proyecto() As Proyecto
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Proyecto
    Set Proyecto = mvarProyecto
End Property



Public Property Let Regulatorio(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Regulatorio = 5
    mvarRegulatorio = vData
End Property


Public Property Get Regulatorio() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Regulatorio
    Regulatorio = mvarRegulatorio
End Property



Public Property Let ImpactoTecnologico(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ImpactoTecnologico = 5
    mvarImpactoTecnologico = vData
End Property


Public Property Get ImpactoTecnologico() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ImpactoTecnologico
    ImpactoTecnologico = mvarImpactoTecnologico
End Property



Public Property Set ReferenteDeRGyP(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.ReferenteDeRGyP = Form1
    Set mvarReferenteDeRGyP = vData
End Property


Public Property Get ReferenteDeRGyP() As Recurso
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ReferenteDeRGyP
    Set ReferenteDeRGyP = mvarReferenteDeRGyP
End Property



Public Property Set ReferenteDeSistema(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.ReferenteDeSistema = Form1
    Set mvarReferenteDeSistema = vData
End Property


Public Property Get ReferenteDeSistema() As Recurso
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ReferenteDeSistema
    Set ReferenteDeSistema = mvarReferenteDeSistema
End Property



Public Property Let Corporativo(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Corporativo = 5
    mvarCorporativo = vData
End Property


Public Property Get Corporativo() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Corporativo
    Corporativo = mvarCorporativo
End Property



Public Property Let Situacion(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Situacion = 5
    mvarSituacion = vData
End Property


Public Property Get Situacion() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Situacion
    Situacion = mvarSituacion
End Property



Public Property Let Estado(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Estado = 5
    mvarEstado = vData
End Property


Public Property Get Estado() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Estado
    Estado = mvarEstado
End Property



Public Property Set Autorizante(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Autorizante = Form1
    Set mvarAutorizante = vData
End Property


Public Property Get Autorizante() As Recurso
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Autorizante
    Set Autorizante = mvarAutorizante
End Property



Public Property Set Supervisor(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Supervisor = Form1
    Set mvarSupervisor = vData
End Property


Public Property Get Supervisor() As Recurso
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Supervisor
    Set Supervisor = mvarSupervisor
End Property



Public Property Set Referente(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Referente = Form1
    Set mvarReferente = vData
End Property


Public Property Get Referente() As Recurso
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Referente
    Set Referente = mvarReferente
End Property



Public Property Set Solicitante(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Solicitante = Form1
    Set mvarSolicitante = vData
End Property


Public Property Get Solicitante() As Recurso
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Solicitante
    Set Solicitante = mvarSolicitante
End Property



Public Property Set SectorSolicitante(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.SectorSolicitante = Form1
    Set mvarSectorSolicitante = vData
End Property


Public Property Get SectorSolicitante() As Sector
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SectorSolicitante
    Set SectorSolicitante = mvarSectorSolicitante
End Property




Public Property Let HorasPresupuestadas(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.HorasPresupuestadas = 5
    mvarHorasPresupuestadas = vData
End Property


Public Property Get HorasPresupuestadas() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.HorasPresupuestadas
    HorasPresupuestadas = mvarHorasPresupuestadas
End Property



Public Property Let FinReal(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FinReal = 5
    mvarFinReal = vData
End Property


Public Property Get FinReal() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FinReal
    FinReal = mvarFinReal
End Property



Public Property Let InicioReal(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.InicioReal = 5
    mvarInicioReal = vData
End Property


Public Property Get InicioReal() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.InicioReal
    InicioReal = mvarInicioReal
End Property



Public Property Let FinPlanificacion(ByVal vData As Variant)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FinPlanificacion = 5
    mvarFinPlanificacion = vData
End Property


Public Property Set FinPlanificacion(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.FinPlanificacion = Form1
    Set mvarFinPlanificacion = vData
End Property


Public Property Get FinPlanificacion() As Variant
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FinPlanificacion
    If IsObject(mvarFinPlanificacion) Then
        Set FinPlanificacion = mvarFinPlanificacion
    Else
        FinPlanificacion = mvarFinPlanificacion
    End If
End Property



Public Property Let InicioPlanificacion(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.InicioPlanificacion = 5
    mvarInicioPlanificacion = vData
End Property


Public Property Get InicioPlanificacion() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.InicioPlanificacion
    InicioPlanificacion = mvarInicioPlanificacion
End Property



Public Property Let FechaPedido(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FechaPedido = 5
    mvarFechaPedido = vData
End Property


Public Property Get FechaPedido() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FechaPedido
    FechaPedido = mvarFechaPedido
End Property



Public Property Let FechaAlta(ByVal vData As Date)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FechaAlta = 5
    mvarFechaAlta = vData
End Property


Public Property Get FechaAlta() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FechaAlta
    FechaAlta = mvarFechaAlta
End Property



Public Property Let NumeroAsignado(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumeroAsignado = 5
    mvarNumeroAsignado = vData
End Property


Public Property Get NumeroAsignado() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumeroAsignado
    NumeroAsignado = mvarNumeroAsignado
End Property




Public Property Let Titulo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Titulo = 5
    mvarTitulo = vData
End Property


Public Property Get Titulo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Titulo
    Titulo = mvarTitulo
End Property




Public Property Let NumeroInterno(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumeroInterno = 5
    mvarNumeroInterno = vData
End Property


Public Property Get NumeroInterno() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumeroInterno
    NumeroInterno = mvarNumeroInterno
End Property



Public Property Let Clase(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Clase = 5
    mvarClase = vData
End Property


Public Property Get Clase() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Clase
    Clase = mvarClase
End Property



Public Property Let Tipo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tipo = 5
    mvarTipo = vData
End Property


Public Property Get Tipo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tipo
    Tipo = mvarTipo
End Property



