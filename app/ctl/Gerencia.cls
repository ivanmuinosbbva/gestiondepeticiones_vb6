VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Gerencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Direccion"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarCodigo As String 'local copy
Private mvarNombre As String 'local copy
Private mvarDireccion As direccion 'local copy
Private mvarHabilitado As Boolean 'local copy
Private mvarEjecutor As Boolean 'local copy
Private mvarAbreviatura As String 'local copy

Public Property Let Abreviatura(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Abreviatura = 5
    mvarAbreviatura = vData
End Property


Public Property Get Abreviatura() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Abreviatura
    Abreviatura = mvarAbreviatura
End Property



Public Property Let Ejecutor(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Ejecutor = 5
    mvarEjecutor = vData
End Property


Public Property Get Ejecutor() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Ejecutor
    Ejecutor = mvarEjecutor
End Property



Public Property Let Habilitado(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Habilitado = 5
    mvarHabilitado = vData
End Property


Public Property Get Habilitado() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Habilitado
    Habilitado = mvarHabilitado
End Property



Public Property Set direccion(ByVal vData As Object)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Direccion = Form1
    Set mvarDireccion = vData
End Property


Public Property Get direccion() As direccion
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Direccion
    Set direccion = mvarDireccion
End Property



Public Property Let Nombre(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nombre = 5
    mvarNombre = vData
End Property


Public Property Get Nombre() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nombre
    Nombre = mvarNombre
End Property



Public Property Let Codigo(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Codigo = 5
    mvarCodigo = vData
End Property


Public Property Get Codigo() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Codigo
    Codigo = mvarCodigo
End Property



