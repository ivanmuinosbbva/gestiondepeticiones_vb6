VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTipoConforme"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
' -001- a. FJS 15.09.2008 - Se agregan los nuevos tipos de conformes a la clase

Option Explicit

Const vSIZEtipo = 11
Const vSIZEmodo = 1

Private vTipoNombre(vSIZEtipo) As String
Private vTipoCodigo(vSIZEtipo) As String
Private vModoNombre(vSIZEmodo) As String
Private vModoCodigo(vSIZEmodo) As String

Public Function SizeTipo() As Integer
    SizeTipo = CInt(vSIZEtipo)
End Function

Public Function SizeModo() As Integer
    SizeModo = CInt(vSIZEmodo)
End Function

Public Function GetCodigoTipo(ByVal nIndex As Integer) As String
    GetCodigoTipo = vTipoCodigo(nIndex)
End Function

Public Function GetCodigoModo(ByVal nIndex As Integer) As String
    GetCodigoModo = vModoCodigo(nIndex)
End Function

Public Function GetNombreTipo(ByVal sCodigo As String) As String
    Dim x As Integer
    GetNombreTipo = ""
    For x = 0 To vSIZEtipo
        If ClearNull(sCodigo) = ClearNull(vTipoCodigo(x)) Then
            GetNombreTipo = vTipoNombre(x)
            Exit For
        End If
    Next
End Function

Public Function GetNombreModo(ByVal sCodigo As String) As String
    Dim x As Integer
    GetNombreModo = ""
    For x = 0 To vSIZEmodo
        If ClearNull(sCodigo) = ClearNull(vModoCodigo(x)) Then
            GetNombreModo = vModoNombre(x)
            Exit For
        End If
    Next
End Function

Private Sub Class_Initialize()
    vModoCodigo(0) = "REQ": vModoNombre(0) = " "
    vModoCodigo(1) = "EXC": vModoNombre(1) = "No Requerido"

    vTipoCodigo(0) = "ALCA": vTipoNombre(0) = "Ok Documento de Alcance"
    vTipoCodigo(1) = "ALCP": vTipoNombre(1) = "Ok Parcial Doc. Alcance"
    vTipoCodigo(2) = "TEST": vTipoNombre(2) = "Ok Prueba de Usuario"
    vTipoCodigo(3) = "TESP": vTipoNombre(3) = "Ok Parcial Prueba Usuario"
    '{ add -001- a.
    vTipoCodigo(4) = "HSOB.P": vTipoNombre(4) = "Conf. parcial Homologación S/Observ."
    vTipoCodigo(5) = "HSOB.F": vTipoNombre(5) = "Conf. final Homologación S/Observ."
    
    vTipoCodigo(6) = "HOME.P": vTipoNombre(6) = "Conf. parcial Homologación C/Observ. menor"
    vTipoCodigo(7) = "HOME.F": vTipoNombre(7) = "Conf. final Homologación C/Observ. menor"
    
    vTipoCodigo(8) = "HOMA.P": vTipoNombre(8) = "Conf. parcial Homologación C/Observ. mayor"
    vTipoCodigo(9) = "HOMA.F": vTipoNombre(9) = "Conf. final Homologación C/Observ. mayor"
    
    vTipoCodigo(10) = "OKPP": vTipoNombre(10) = "Conf. parcial Supervisor S/Homologación"
    vTipoCodigo(11) = "OKPF": vTipoNombre(11) = "Conf. final Supervisor S/Homologación"
    '}
End Sub
