VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.UserControl PXedgrd 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   ClientHeight    =   1620
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1470
   ControlContainer=   -1  'True
   KeyPreview      =   -1  'True
   ScaleHeight     =   1620
   ScaleWidth      =   1470
   Begin VB.TextBox objEdit 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   285
      Left            =   30
      TabIndex        =   1
      Top             =   1230
      Visible         =   0   'False
      Width           =   300
   End
   Begin MSFlexGridLib.MSFlexGrid objGrid 
      Height          =   1185
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   1275
      _ExtentX        =   2249
      _ExtentY        =   2090
      _Version        =   65541
      FixedCols       =   0
      AllowBigSelection=   0   'False
      AllowUserResizing=   1
   End
   Begin VB.Menu mGridMenu 
      Caption         =   "Acciones"
      Begin VB.Menu mGrdAdd 
         Caption         =   "&Insertar Linea"
      End
      Begin VB.Menu mGrdDelete 
         Caption         =   "&Eliminar Linea"
      End
      Begin VB.Menu mGrdFind 
         Caption         =   "&Buscar"
         Enabled         =   0   'False
      End
      Begin VB.Menu mGrdNext 
         Caption         =   "&Pr�ximo"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
End
Attribute VB_Name = "PXedgrd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Dim currCOL As Integer
Dim currROW As Integer
Dim strFind As String
Dim m_lastFind As Integer
Dim m_lastCOL As Integer
Dim m_lastROW As Integer
Dim m_lastVAL As String
Dim x_isEditing As Boolean

Dim m_AutoEdit As Boolean
Dim m_CanEdit As Boolean
Dim m_ToUpperCase As Boolean
Dim m_ValidChars As String
Dim m_MaxLength As Long
Dim m_Type As EdtTypeDef
Dim m_NegValue As Boolean
Dim m_DigitosDecimal As Integer
Dim m_DigitosEntero As Integer
Dim m_EdtEditColor As OLE_COLOR
Dim m_EdtNormColor As OLE_COLOR

Dim x_ToUpperCase As Boolean
Dim x_MaxLength As Long
Dim x_ValidChars As String
Dim x_Type As EdtTypeDef
Dim x_DigitosDecimal As Integer
Dim x_DigitosEntero As Integer
Dim x_NegValue As Boolean

Public Enum EdtTypeDef
    ET_TEXT
    ET_NUMERIC
End Enum
Public Enum AligTypeDef
    LG_LEFT
    LG_RIGHT
    LG_CENTER
End Enum
Event ChangeCell(Row As Integer, Col As Integer, validData As String)
Event ValidInput(Row As Integer, Col As Integer, newData As String, ByRef isValid As Boolean)
Event TestEditable(Row As Integer, Col As Integer, ByRef isEditable As Boolean)
Event PrepareEdit(Row As Integer, Col As Integer, ByRef xType As EdtTypeDef, ByRef xEnteros As Integer, ByRef xDecimales As Integer, ByRef xNegValue As Boolean, ByRef MaxLength As Long, ByRef ValidChars As String, ByRef ToUpper As Boolean)
Event FormatCell(Row As Integer, Col As Integer, DataString As String, ByRef DataFormat As String)
Event BeforeInsert(NewRow As Integer)
Event BeforeDelete(NewRow As Integer)
''Event Click(NewRow As Integer, NewCol As Integer)
Event RowChange(NewRow As Integer, NewCol As Integer)
Event RowFind(NewRow As Integer)
'***********************************************
'PROPIEDADES
'***********************************************
Public Property Let ColAlignment(ByVal lCol As Long, ByVal iAlign As AligTypeDef)
    If iAlign = LG_CENTER Then
        objGrid.ColAlignment(lCol) = flexAlignCenterCenter
    End If
    If iAlign = LG_RIGHT Then
        objGrid.ColAlignment(lCol) = flexAlignRightCenter
    End If
    If iAlign = LG_LEFT Then
        objGrid.ColAlignment(lCol) = flexAlignLeftCenter
    End If
End Property
Public Property Get ColAlignment(ByVal lCol As Long) As AligTypeDef
    ColAlignment = objGrid.ColAlignment(lCol)
End Property
Public Property Get BackColor() As OLE_COLOR
    BackColor = objGrid.BackColor
End Property
Public Property Let BackColor(ByVal New_BackColor As OLE_COLOR)
    objGrid.BackColor() = New_BackColor
    PropertyChanged "BackColor"
End Property
Public Property Get ForeColor() As OLE_COLOR
    ForeColor = objGrid.ForeColor
End Property
Public Property Let ForeColor(ByVal New_ForeColor As OLE_COLOR)
    objGrid.ForeColor() = New_ForeColor
    PropertyChanged "ForeColor"
End Property
Public Property Get Font() As Font
    Set Font = objGrid.Font
End Property
Public Property Set Font(ByVal New_Font As Font)
    Set objGrid.Font = New_Font
    PropertyChanged "Font"
End Property
Public Property Get Cols() As Integer
    Cols = objGrid.Cols
End Property
Public Property Let Cols(ByVal New_Cols As Integer)
    objGrid.Cols = New_Cols
    PropertyChanged "Cols"
End Property
Public Property Get Rows() As Integer
    Rows = objGrid.Rows
End Property
Public Property Let Rows(ByVal New_Rows As Integer)
    objGrid.Rows = New_Rows
    PropertyChanged "Rows"
End Property

Public Property Get RowSel() As Integer
    RowSel = objGrid.Row
End Property
Public Property Let Col(ByVal New_Col As Integer)
    objGrid.Col = New_Col
End Property
Public Property Let RowSel(ByVal New_RowSel As Integer)
    objGrid.RowSel = New_RowSel
End Property
Public Property Let Sort(ByVal New_Sort As Integer)
    objGrid.Sort = New_Sort
End Property

Public Property Get TextMatrix(Row As Integer, Col As Integer) As String
    TextMatrix = objGrid.TextMatrix(Row, Col)
End Property
Public Property Let TextMatrix(Row As Integer, Col As Integer, New_String As String)
    objGrid.TextMatrix(Row, Col) = New_String
End Property
Public Property Let ColWidth(ByVal lCol As Long, ByVal lWidth As Long)
    objGrid.ColWidth(lCol) = lWidth
End Property
Public Property Get ColWidth(ByVal lCol As Long) As Long
    ColWidth = objGrid.ColWidth(lCol)
End Property

Public Property Get Enabled() As Boolean
    Enabled = objGrid.Enabled
End Property
Public Property Let Enabled(ByVal New_Enabled As Boolean)
    objGrid.Enabled = New_Enabled
    PropertyChanged "Enabled"
End Property
Public Property Get Visible() As Boolean
    Visible = objGrid.Visible
End Property
Public Property Let Visible(ByVal New_Visible As Boolean)
    objGrid.Visible = New_Visible
    PropertyChanged "Visible"
End Property


Public Property Let CanInsert(ByVal New_CanInsert As Boolean)
    UserControl.mGrdAdd.Visible = New_CanInsert
    PropertyChanged "CanInsert"
End Property
Public Property Let CanDelete(ByVal New_CanDelete As Boolean)
    UserControl.mGrdDelete.Visible = New_CanDelete
    PropertyChanged "CanDelete"
End Property
Public Property Get CanInsert() As Boolean
    CanInsert = UserControl.mGrdAdd.Visible
End Property
Public Property Get CanDelete() As Boolean
    CanDelete = UserControl.mGrdDelete.Visible
End Property

'------------------------------------------------
Public Property Get EdtNormColor() As OLE_COLOR
    EdtNormColor = m_EdtNormColor
End Property
Public Property Let EdtNormColor(ByVal New_BackColor As OLE_COLOR)
    m_EdtNormColor = New_BackColor
    PropertyChanged "EdtNormColor"
End Property
Public Property Get EdtEditColor() As OLE_COLOR
    EdtEditColor = m_EdtEditColor
End Property
Public Property Let EdtEditColor(ByVal New_EditColor As OLE_COLOR)
    m_EdtEditColor = New_EditColor
    PropertyChanged "EdtEditColor"
End Property
Public Property Get EdtText() As String
Attribute EdtText.VB_UserMemId = 0
    EdtText = objEdit.Text
End Property
Public Property Let EdtText(ByVal NewText As String)
'''    If Trim(NewText) = "" Then
'''        objEdit.Text = ""
'''    Else
'''        objEdit.Text = NewText
'''    End If
End Property
Public Property Get EdtMaxLength() As Long
    EdtMaxLength = m_MaxLength
End Property
Public Property Let EdtMaxLength(ByVal New_MaxLength As Long)
    m_MaxLength = New_MaxLength
    PropertyChanged "EdtMaxLength"
End Property
Public Property Get EdtValidChars() As String
    EdtValidChars = m_ValidChars
End Property
Public Property Let EdtValidChars(ByVal New_ValidChars As String)
    m_ValidChars = New_ValidChars
    PropertyChanged "EdtValidChars"
End Property
Public Property Get EdtDigitosDecimal() As Integer
    EdtDigitosDecimal = m_DigitosDecimal
End Property
Public Property Let EdtDigitosDecimal(ByVal New_DigitosDecimal As Integer)
    If New_DigitosDecimal > 12 Then New_DigitosDecimal = 12
    If New_DigitosDecimal < 0 Then New_DigitosDecimal = 0
    m_DigitosDecimal = New_DigitosDecimal
    PropertyChanged "EdtDigitosDecimal"
End Property
Public Property Get EdtDigitosEntero() As Integer
    EdtDigitosEntero = m_DigitosEntero
End Property
Public Property Let EdtDigitosEntero(ByVal New_DigitosEntero As Integer)
    If New_DigitosEntero > 12 Then New_DigitosEntero = 12
    If New_DigitosEntero < 1 Then New_DigitosEntero = 1
    m_DigitosEntero = New_DigitosEntero
    PropertyChanged "EdtDigitosEntero"
End Property
Public Property Get EdtType() As EdtTypeDef
    EdtType = m_Type
End Property
Public Property Let EdtType(ByVal New_EdtType As EdtTypeDef)
    m_Type = New_EdtType
    PropertyChanged "EdtType"
End Property
Public Property Get EdtUpperCase() As Boolean
    EdtUpperCase = m_ToUpperCase
End Property
Public Property Let EdtUpperCase(ByVal New_ToUpperCase As Boolean)
    m_ToUpperCase = New_ToUpperCase
    PropertyChanged "EdtUpperCase"
End Property
Public Property Get EdtNegValue() As Boolean
    EdtNegValue = m_NegValue
End Property
Public Property Let EdtNegValue(ByVal New_NegValue As Boolean)
    m_NegValue = New_NegValue
    PropertyChanged "EdtNegValue"
End Property

Public Property Get CanEdit() As Boolean
    CanEdit = m_CanEdit
End Property
Public Property Let CanEdit(ByVal New_CanEdit As Boolean)
    m_CanEdit = New_CanEdit
    PropertyChanged "CanEdit"
End Property
Public Property Get AutoEdit() As Boolean
    AutoEdit = m_AutoEdit
End Property
Public Property Let AutoEdit(ByVal New_AutoEdit As Boolean)
    m_AutoEdit = New_AutoEdit
    PropertyChanged "AutoEdit"
End Property
'------------------------------------------------------------------

Private Sub mGrdAdd_Click()
    On Error GoTo exitAdd
    objGrid.AddItem "", m_lastROW
    objGrid.SetFocus
    RaiseEvent BeforeInsert(m_lastROW + 1)
exitAdd:
End Sub
Private Sub mGrdDelete_Click()
    On Error GoTo exitDelete
    If m_lastROW > 0 And objGrid.Rows > 2 Then
        objGrid.RemoveItem m_lastROW
    End If
exitDelete:
End Sub
Private Sub mGrdFind_Click()
    UserControl.mGrdNext.Enabled = False
    m_lastFind = 0
    If objGrid.Rows > 1 Then
        strFind = InputBox("Texto a buscar:", strFind)
        If strFind <> "" Then
            If Find(m_lastFind, m_lastCOL, strFind, True) Then
                UserControl.mGrdNext.Enabled = True
                RaiseEvent RowFind(m_lastFind)
            End If
        End If
    End If
End Sub
Private Sub mGrdNext_Click()
    If objGrid.Rows > 1 Then
        If strFind <> "" Then
            If Find(m_lastFind + 1, m_lastCOL, strFind, True) Then
                RaiseEvent RowFind(m_lastFind)
            End If
        End If
    End If
End Sub


Private Sub UserControl_GotFocus()
    UserControl.mGridMenu.Visible = False
End Sub

'***********************************************
'EVENTOS CONTROL
'***********************************************
Private Sub UserControl_InitProperties()
    m_ToUpperCase = False
    UserControl.Height = 500
    UserControl.Width = 2000
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    Set Font = PropBag.ReadProperty("Font", Ambient.Font)
'''    objEdit.MaxLength = 0
    objGrid.Cols = PropBag.ReadProperty("Cols", 2)
    objGrid.Rows = PropBag.ReadProperty("Rows", 2)
    objGrid.BackColor = PropBag.ReadProperty("BackColor", &H80000005)
    objGrid.ForeColor = PropBag.ReadProperty("ForeColor", &H80000008)
'''    objEdit.Text = ""
    UserControl.mGrdAdd.Visible = PropBag.ReadProperty("CanInsert", False)
    UserControl.mGrdDelete.Visible = PropBag.ReadProperty("CanDelete", False)
    m_EdtNormColor = PropBag.ReadProperty("EdtNormColor", &H80000018)
    m_EdtEditColor = PropBag.ReadProperty("EdtEditColor", &H80000016)
    m_Type = PropBag.ReadProperty("EdtType", ET_TEXT)
    m_ToUpperCase = PropBag.ReadProperty("EdtUpperCase", False)
    m_ValidChars = PropBag.ReadProperty("EdtValidChars", "")
    m_MaxLength = PropBag.ReadProperty("EdtMaxLength", 0)
    m_DigitosDecimal = PropBag.ReadProperty("EdtDigitosDecimal", 4)
    m_DigitosEntero = PropBag.ReadProperty("EdtDigitosEntero", 2)
    m_CanEdit = PropBag.ReadProperty("CanEdit", False)
    m_AutoEdit = PropBag.ReadProperty("AutoEdit", False)
    m_NegValue = PropBag.ReadProperty("EdtNegValue", False)
    objGrid.Enabled = PropBag.ReadProperty("Enabled", True)
    objGrid.Visible = PropBag.ReadProperty("Visible", True)
    UserControl.mGrdNext.Enabled = False
End Sub
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("Font", Font, Ambient.Font)
    Call PropBag.WriteProperty("BackColor", objGrid.BackColor, &H80000005)
    Call PropBag.WriteProperty("ForeColor", objGrid.ForeColor, &H80000008)
    Call PropBag.WriteProperty("Cols", objGrid.Cols, 2)
    Call PropBag.WriteProperty("Rows", objGrid.Rows, 2)
    Call PropBag.WriteProperty("CanInsert", UserControl.mGrdAdd.Visible, False)
    Call PropBag.WriteProperty("CanDelete", UserControl.mGrdDelete.Visible, False)
    Call PropBag.WriteProperty("CanEdit", m_CanEdit, False)
    Call PropBag.WriteProperty("AutoEdit", m_AutoEdit, False)
    Call PropBag.WriteProperty("Enabled", objGrid.Enabled, True)
    Call PropBag.WriteProperty("Visible", objGrid.Visible, True)
    
    Call PropBag.WriteProperty("EdtNormColor", m_EdtNormColor, &H80000018)
    Call PropBag.WriteProperty("EdtEditColor", m_EdtEditColor, &H80000016)
    Call PropBag.WriteProperty("EdtType", m_Type, ET_TEXT)
    Call PropBag.WriteProperty("EdtNegValue", m_NegValue, False)
    Call PropBag.WriteProperty("EdtUpperCase", m_ToUpperCase, False)
    Call PropBag.WriteProperty("EdtValidChars", m_ValidChars, "")
    Call PropBag.WriteProperty("EdtMaxLength", m_MaxLength, 0)
    Call PropBag.WriteProperty("EdtDigitosDecimal", m_DigitosDecimal, 2)
    Call PropBag.WriteProperty("EdtDigitosEntero", m_DigitosEntero, 4)
End Sub
Private Sub UserControl_Resize()
    objGrid.Top = 0
    objGrid.Left = 0
    objGrid.Height = UserControl.Height
    objGrid.Width = UserControl.Width
End Sub
Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)
   If KeyCode = 8 Then
        Exit Sub
    End If
End Sub
Private Sub UserControl_LostFocus()
    currROW = 0
    currCOL = 0
End Sub


'***********************************************
'EVENTOS GRILLA
'***********************************************
Private Sub objGrid_Scroll()
'''    objEdit.Move (objGrid.Left + objGrid.CellLeft), (objGrid.Top + objGrid.CellTop), objGrid.CellWidth, objGrid.CellHeight
End Sub

Private Sub objGrid_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Debug.Print "objGrid_MouseUp"
    UserControl.mGridMenu.Visible = False
    objGrid.Refresh
    
    If objGrid.MouseRow = 0 And objGrid.Rows > 1 Then
       objGrid.RowSel = 1
       objGrid.RowSel = 1
       objGrid.Col = objGrid.MouseCol
       objGrid.Sort = flexSortStringNoCaseAscending
       Exit Sub
    End If
    
    If Button <> 2 Then
        Exit Sub
    End If
    m_lastROW = objGrid.MouseRow
    m_lastCOL = objGrid.MouseCol
    objGrid.CellBackColor = 0
    UserControl.PopupMenu UserControl.mGridMenu
End Sub
Private Sub objGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim isEditable As Boolean
    Debug.Print "objGrid_KeyDown"
    x_ToUpperCase = m_ToUpperCase
    x_MaxLength = m_MaxLength
    x_ValidChars = m_ValidChars
    x_Type = m_Type
    x_DigitosDecimal = m_DigitosDecimal
    x_DigitosEntero = m_DigitosEntero
    x_NegValue = m_NegValue
    If KeyCode = 13 Then
        isEditable = False
        RaiseEvent TestEditable(objGrid.Row, objGrid.Col, isEditable)
        If isEditable And m_CanEdit Then
            RaiseEvent PrepareEdit(objGrid.Row, objGrid.Col, x_Type, x_DigitosEntero, x_DigitosDecimal, x_NegValue, x_MaxLength, x_ValidChars, x_ToUpperCase)
            setMaxLength
            EditON
''            objEdit.SetFocus
''            objEdit.SelStart = 1000
        End If
    End If
End Sub

Private Sub objGrid_KeyPress(KeyAscii As Integer)
    Dim isEditable As Boolean
    Debug.Print "objGrid_KeyPress"
    
    If x_isEditing Then
        Exit Sub
    End If
    
    If KeyAscii = 27 Then
        Exit Sub
    End If
    
    x_ToUpperCase = m_ToUpperCase
    x_MaxLength = m_MaxLength
    x_ValidChars = m_ValidChars
    x_Type = m_Type
    x_DigitosDecimal = m_DigitosDecimal
    x_DigitosEntero = m_DigitosEntero
    x_NegValue = m_NegValue
    isEditable = False
    
    RaiseEvent TestEditable(objGrid.Row, objGrid.Col, isEditable)
    If isEditable And m_CanEdit Then
        RaiseEvent PrepareEdit(objGrid.Row, objGrid.Col, x_Type, x_DigitosEntero, x_DigitosDecimal, x_NegValue, x_MaxLength, x_ValidChars, x_ToUpperCase)
        setMaxLength
        EditON
        If KeyAscii <> 13 Then
'            objEdit.Text = Trim(objEdit.Text) & Chr(ValChar(KeyAscii))
            objEdit.Text = Chr(ValChar(KeyAscii))
            objEdit.SelStart = 2
        End If
''        objEdit.SetFocus
''        objEdit.SelStart = 1000
    End If
End Sub
Private Sub objGrid_GotFocus()
    Debug.Print "objGrid_GotFocus"
    UserControl.mGridMenu.Visible = False
    
''    If x_isEditing Then
''        If Not x_ValidInput() Then Exit Sub
''    End If
    If objGrid.Row <> currROW Then
        RaiseEvent RowChange(objGrid.Row, objGrid.Col)
    End If
'    If objGrid.Row = currROW And objGrid.Col = currCOL Then
'        If objGrid.Col < objGrid.Cols - 1 Then objGrid.Col = objGrid.Col + 1
'    End If
    EditPos
End Sub
Private Sub objGrid_LeaveCell()
   
    Exit Sub
   
   Debug.Print "objGrid_LeaveCell"
    objGrid.CellBackColor = 0
End Sub
Private Sub objGrid_EnterCell()
   
    Exit Sub
   
   Debug.Print "objGrid_EnterCell"
    If x_isEditing Then
        If Not x_ValidInput() Then Exit Sub
    End If
    If objGrid.Row <> currROW Then
        RaiseEvent RowChange(objGrid.Row, objGrid.Col)
    End If
    currROW = objGrid.Row
    
    EditPos
End Sub
Private Sub objGrid_DblClick()
    Dim isEditable As Boolean
    If objGrid.Row <> currROW Then
        RaiseEvent RowChange(objGrid.Row, objGrid.Col)
    End If
    'Debug.Print "objGrid_DblClick"
    currROW = objGrid.Row
    
    Exit Sub
    
    If Not x_isEditing Then
        isEditable = False
        RaiseEvent TestEditable(objGrid.Row, objGrid.Col, isEditable)
        If isEditable And m_CanEdit Then
            RaiseEvent PrepareEdit(objGrid.Row, objGrid.Col, x_Type, x_DigitosEntero, x_DigitosDecimal, x_NegValue, x_MaxLength, x_ValidChars, x_ToUpperCase)
            setMaxLength
            EditON
        End If
    End If
End Sub


'***********************************************
'EVENTOS OBJNPUT
'***********************************************

Private Sub objEdit_KeyPress(KeyAscii As Integer)
    If KeyAscii = 8 Then
        Exit Sub
    End If
    If KeyAscii = 13 Then
        Exit Sub
    End If
   KeyAscii = ValChar(KeyAscii)
End Sub

Private Sub objEdit_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim DataValid As Boolean
    Dim DataFormat As String
    DataValid = True
    Select Case KeyCode
        Case 27
            x_isEditing = False
            objGrid.SetFocus
        Case 13
            If Not x_ValidInput() Then Exit Sub
            objGrid.SetFocus
    End Select
End Sub
Private Sub objEdit_LostFocus()
    Dim DataValid As Boolean
    Dim DataFormat As String
    'Debug.Print "objEdit_LostFocus"
    DataValid = True
    If x_isEditing Then
        If Not x_ValidInput() Then Exit Sub
        If objGrid.Row = currROW And objGrid.Col = currCOL Then
            If objGrid.Col < objGrid.Cols - 1 Then objGrid.Col = objGrid.Col + 1
        End If
    End If
End Sub
'***********************************************
'VARIOS
'***********************************************
Private Function x_ValidInput() As Boolean
    Dim DataValid As Boolean
    Dim DataFormat As String
    Dim DataPrev As String
    DataValid = True
    x_ValidInput = True
    'Debug.Print "x_ValidInput"
    
    
    If x_isEditing Then
        'NO habria que disparar el validinput,changecell si se esta ingresando el mismo valor!!
        DataPrev = objGrid.TextMatrix(currROW, currCOL)
        RaiseEvent FormatCell(currROW, currCOL, objEdit.Text, DataFormat)
        If Trim(DataPrev) = Trim(DataFormat) Then
            EditOFF
            Exit Function
        End If
        RaiseEvent ValidInput(currROW, currCOL, objEdit.Text, DataValid)
        If DataValid Then
            objGrid.TextMatrix(currROW, currCOL) = DataFormat
            RaiseEvent ChangeCell(currROW, currCOL, DataFormat)
            EditOFF
        Else
            x_ValidInput = False
            EditON
        End If
   End If
End Function

Private Sub EditON()
    Exit Sub
    
    Debug.Print "EditON"
    If m_CanEdit Then
        x_isEditing = True
        objEdit.Visible = True
        objEdit.Enabled = True
        objEdit.BackColor = m_EdtEditColor
        objEdit.SetFocus
        objEdit.MaxLength = x_MaxLength
        objEdit.SelStart = 0
        objEdit.SelLength = x_MaxLength
    End If
End Sub
Private Sub EditOFF()
    'Debug.Print "EditOFF"
    objEdit.Visible = False
    objEdit.Enabled = False
    x_isEditing = False
    objEdit.BackColor = m_EdtNormColor
End Sub

Private Sub EditPos()
    'Debug.Print "EditPos"
    Dim isEditable As Boolean
    On Error GoTo xSub
    
    currROW = objGrid.Row
    currCOL = objGrid.Col
    
    Exit Sub
    
    
    objEdit.Move (objGrid.Left + objGrid.CellLeft), (objGrid.Top + objGrid.CellTop), objGrid.CellWidth, objGrid.CellHeight
    objEdit.Text = Trim(objGrid)
    objEdit.Visible = False
    EditOFF
    isEditable = False
    RaiseEvent TestEditable(objGrid.Row, objGrid.Col, isEditable)
    If isEditable And m_CanEdit Then
        objGrid.CellBackColor = m_EdtNormColor
        If m_AutoEdit Then
            RaiseEvent PrepareEdit(objGrid.Row, objGrid.Col, x_Type, x_DigitosEntero, x_DigitosDecimal, x_NegValue, x_MaxLength, x_ValidChars, x_ToUpperCase)
            setMaxLength
            EditON
        End If
    End If
xSub:
End Sub
Private Function ValChar(KeyAscii As Integer) As Integer
    Dim posPunto As Integer
    Dim posSigno As Integer
    ValChar = KeyAscii
    Select Case x_Type
    Case ET_TEXT
        If (Len(objEdit) >= x_MaxLength) And (x_MaxLength > 0) Then
            ValChar = 0
            Exit Function
        End If
        If x_ToUpperCase And ValChar > 96 And ValChar < 123 Then
            ValChar = ValChar - 32
        End If
        If Len(x_ValidChars) > 0 Then
            If (InStr(1, x_ValidChars, Chr(ValChar)) = 0) Then
                ValChar = 0
                Exit Function
            End If
        End If
    Case ET_NUMERIC
        posPunto = InStr(objEdit, ".")
        posSigno = InStr(objEdit, "-")
        If objEdit.SelStart = 0 And Left(objEdit, 1) = Chr(45) Then
            objEdit.SelStart = 1
        End If
        'el punto decimal
        If (ValChar = 44) Then
            ValChar = 46
        End If
        If (ValChar = 46 And x_DigitosDecimal = 0) Then
            ValChar = 0
            Exit Function
        End If
        If (ValChar = 46 And x_DigitosDecimal > 0 And posPunto = 0) Then
            Exit Function
        End If
        If ValChar = 46 And posPunto > 0 Then
            objEdit.SelStart = posPunto - 1
            objEdit.SelLength = 1
            Exit Function
        End If
        If ValChar = 45 And Not x_NegValue Then
            ValChar = 0
            Exit Function
        End If
        If ValChar = 45 And posSigno = 0 Then
            objEdit.SelStart = 0
            objEdit.SelLength = 0
            Exit Function
        End If
        If ValChar = 45 And posSigno > 0 Then
            objEdit.SelStart = posSigno - 1
            objEdit.SelLength = 1
            Exit Function
        End If
        
        'que sea un numero
        If (InStr(1, "0123456789", Chr(ValChar)) = 0) Then
            ValChar = 0
            Exit Function
        End If
        'verifica cantidad de decimales
        If posPunto > 0 And objEdit.SelStart >= posPunto + x_DigitosDecimal Then
            ValChar = 0
            Exit Function
        End If
        'verifica cantidad de enteros
        If posPunto > 0 And objEdit.SelStart < posPunto And objEdit.SelStart >= x_DigitosEntero Then
            ValChar = 0
            Exit Function
        End If
        If posPunto = 0 And objEdit.SelStart >= x_DigitosEntero Then
            ValChar = 0
            Exit Function
        End If
        'por las dudas longitud total
        If Len(objEdit) - objEdit.SelLength >= x_MaxLength Then
            ValChar = 0
            Exit Function
        End If
        
    End Select
End Function

Private Sub setMaxLength()
    If x_Type = ET_NUMERIC Then
        x_MaxLength = x_DigitosEntero + x_DigitosDecimal + IIf(x_DigitosDecimal > 0, 1, 0)
    End If
End Sub
Public Function Clear()
    objGrid.Clear
End Function

Public Function Find(ByRef rowInicial As Integer, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Boolean
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    Find = False
    If rowInicial = 0 Then
        rowInicial = 1
    End If
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        .RowSel = 0
        .ColSel = 0
        For i = rowInicial To .Rows - 1
           If Trim(.TextMatrix(i, ptrColumn)) = Trim(strSeek) Then
                If flgPosicion Then
                    .TopRow = i
                    .Row = i
                    .RowSel = i
                    .Col = 0
                    .ColSel = .Cols - 1
                End If
                rowInicial = i
                Find = True
                Exit Function
           End If
        Next
    End With
End Function

