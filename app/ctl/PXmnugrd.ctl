VERSION 5.00
Begin VB.UserControl PXmnugrd 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   ClientHeight    =   495
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   435
   ControlContainer=   -1  'True
   KeyPreview      =   -1  'True
   ScaleHeight     =   495
   ScaleWidth      =   435
   Begin VB.Menu mGridMenu 
      Caption         =   "Acciones"
      Begin VB.Menu mGrdFind 
         Caption         =   "&Buscar"
         Enabled         =   0   'False
      End
      Begin VB.Menu mGrdNext 
         Caption         =   "&Pr�ximo"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
End
Attribute VB_Name = "PXmnugrd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Dim currCOL As Integer
Dim currROW As Integer
Dim strFind As String
Dim m_lastFind As Integer
Dim m_lastCOL As Integer
Dim m_lastROW As Integer
Dim m_lastVAL As String
Dim x_isEditing As Boolean

'***********************************************
'PROPIEDADES
'***********************************************
'------------------------------------------------------------------

Private Sub mGrdFind_Click()
    UserControl.mGrdNext.Enabled = False
    m_lastFind = 0
    If objGrid.Rows > 1 Then
        strFind = InputBox("Texto a buscar:", strFind)
        If strFind <> "" Then
            If Find(m_lastFind, m_lastCOL, strFind, True) Then
                UserControl.mGrdNext.Enabled = True
                RaiseEvent RowFind(m_lastFind)
            End If
        End If
    End If
End Sub
Private Sub mGrdNext_Click()
    If objGrid.Rows > 1 Then
        If strFind <> "" Then
            If Find(m_lastFind + 1, m_lastCOL, strFind, True) Then
                RaiseEvent RowFind(m_lastFind)
            End If
        End If
    End If
End Sub

Private Sub UserControl_GotFocus()
    UserControl.mGridMenu.Visible = False
End Sub

'***********************************************
'EVENTOS CONTROL
'***********************************************
Private Sub UserControl_InitProperties()
    m_ToUpperCase = False
    UserControl.Height = 500
    UserControl.Width = 2000
End Sub



'***********************************************
'EVENTOS GRILLA
'***********************************************

Private Sub objGrid_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Debug.Print "objGrid_MouseUp"
    UserControl.mGridMenu.Visible = False
    objGrid.Refresh
    If Button <> 2 Then
        Exit Sub
    End If
    m_lastROW = objGrid.MouseRow
    m_lastCOL = objGrid.MouseCol
    objGrid.CellBackColor = 0
    UserControl.PopupMenu UserControl.mGridMenu
End Sub

Public Function Find(ByRef objGrid As MSFlexGrid, ByRef rowInicial As Integer, ptrColumn As Integer, strSeek As String, flgPosicion As Boolean) As Boolean
    'asume que la grila tiene un row de titulos,
    'por eso busca desde i=1
    Dim i As Integer
    Find = False
    If rowInicial = 0 Then
        rowInicial = 1
    End If
    With objGrid
        If .Rows < 2 Then
            Exit Function
        End If
        .RowSel = 0
        .ColSel = 0
        For i = rowInicial To .Rows - 1
           If Trim(.TextMatrix(i, ptrColumn)) = Trim(strSeek) Then
                If flgPosicion Then
                    .TopRow = i
                    .Row = i
                    .RowSel = i
                    .Col = 0
                    .ColSel = .Cols - 1
                End If
                rowInicial = i
                Find = True
                Exit Function
           End If
        Next
    End With
End Function

