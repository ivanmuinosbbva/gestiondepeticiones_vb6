VERSION 5.00
Begin VB.UserControl AdoConnection 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   CanGetFocus     =   0   'False
   ClientHeight    =   495
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   495
   ClipControls    =   0   'False
   InvisibleAtRuntime=   -1  'True
   Picture         =   "ADO_Connection.ctx":0000
   ScaleHeight     =   495
   ScaleWidth      =   495
   ToolboxBitmap   =   "ADO_Connection.ctx":0442
End
Attribute VB_Name = "AdoConnection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' -001- a. FJS 03.09.2007 - Se agrega esta l�nea para cerrar un recordset que mantenia abierto un resultset cuando el motor de base de datos no se banca m�s de uno abierto al mismo tiempo.
' -002- a. FJS 22.05.2008 - Se cambia el modo de conexi�n para habilitar el motor de cursor de ADO (cursores extremo de cliente).
' -003- a. FJS 26.05.2008 - Se agrega este atributo para cambiar el default de tama�o especificado por defecto para el m�ximo en bytes que el DBMS devuelve para campos de tipo BLOB.
' -004- a. FJS 04.08.2008 - Se modifica el mensaje de aviso en casos que no puede lograrse la conexi�n tanto a la base de datos de seguridad como la del aplicativo mismo.
' -005- a. FJS 11.11.2013 - Nuevo: adaptaci�n para Adaptive Server Enterprise 15.0 (Sybase: https://www.connectionstrings.com/adaptive-server-enterprise-odbc-driver/)
' -006- a. FJS 21.05.2015 - Nuevo: se agrega una modificaci�n para soportar la convivencia con otros controladores para Sybase (MicroStrategy).
' -007- a. FJS 28.05.2015 - Nuevo: se adapta la configuraci�n del driver ODBC para Crystal Reports, para dar de alta el DSN de sistema, no de usuario.
' -008- a. FJS 15.07.2015 - Nuevo: se agrega una rutina para consultar el motivo del rechazo del login al usuario.
' -009- a. FJS 24.06.2016 - Modificaci�n: se modifica el proceso de cambio de password (blanqueo) adaptando el llamado a los comandos de la versi�n 15 de Sybase (sp_password qued� deprecada para Sybase 15).

Option Explicit

'{ add -008- a.
Private Enum LOGIN_REJECTIONREASON
    LOGIN_REJECTBYLOCKED = 2
    LOGIN_REJECTBYPWDEXPIRATION = 4
End Enum
'}

'Connection Variables:
Dim WithEvents cnnADO As ADODB.Connection                  ' Esta conexi�n se usa para la parte de seguridad inform�tica
Attribute cnnADO.VB_VarHelpID = -1

' ODBC Driver Names
Const ODBCDRIVERNAME_SYBASE15 = "ADAPTIVE SERVER ENTERPRISE"
Const ODBCDRIVERNAME_SYBASE12_5 = "SYBASE ASE ODBC DRIVER"

'Default Property Values:
Const m_def_Encriptado = False
Const m_def_Version = "0.0.0"
Const m_def_BaseAplicacion = ""
Const m_def_IsOnLine = False
Const m_def_TimeOut = 60
Const m_def_Provider = ""
Const m_def_BaseSeguridad = "AdmUsuarios"
Const m_def_UsuarioSeguridad = ""
Const m_def_PasswordSeguridad = ""
Const m_def_EsquemaSeguridad = True
Const m_def_UsuarioAplicacion = ""
Const m_def_PasswordAplicacion = ""
Const m_def_Driver = "Adaptive Server Enterprise"       ' Por defecto
Const m_def_Aplicacion = ""
Const m_def_ServidorDSN = ""
Const m_def_Servidor = ""
Const m_def_ServidorPuerto = ""                         ' add -005- a.
Const m_def_ComponenteSeguridad = ""

'Property Variables:
Dim m_Encriptado As Boolean
Dim m_Version As String
Dim m_BaseAplicacion As String
Dim m_IsOnLine As Boolean
Dim m_TimeOut As Integer
Dim m_Provider As String
Dim m_BaseSeguridad As String
Dim m_UsuarioSeguridad As String
Dim m_PasswordSeguridad As String
Dim m_EsquemaSeguridad As Boolean
Dim m_UsuarioAplicacion As String
Dim m_PasswordAplicacion As String
Dim m_Driver As String
Dim m_Aplicacion As String
Dim m_Servidor As String
Dim m_ServidorDSN As String             ' add -005- b.
Dim m_ServidorPuerto As String          ' add -005- a.
Dim WithEvents m_ConexionBase As ADODB.Connection               ' upd
Attribute m_ConexionBase.VB_VarHelpID = -1
Dim m_ComponenteSeguridad As String

Dim usuarioSybase As udtLOGINSYBASE     ' add -008- a.

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'para encriptar
Const sClaveOriginal = "DE03DA7700DBA31001011FA5D00005FFFF060DAC"
Const sCompensador = "01143614011821101917000313141921212919332410191200312021281412031014312317030800"
Dim bErrorProcesoDesencriptado As Boolean
Dim sClave As String
'para crear DSN
Private Const ODBC_ADD_DSN = 1
Private Const ODBC_CONFIG_DSN = 2
Private Const ODBC_REMOVE_DSN = 3
'{ add -007- a.
Private Const ODBC_ADD_SYS_DSN = 4
Private Const ODBC_CONFIG_SYS_DSN = 5
Private Const ODBC_REMOVE_SYS_DSN = 6
'}
Private Const vbAPINull As Long = 0
Private Declare Function SQLConfigDataSource Lib "ODBCCP32.DLL" (ByVal hwndParent As Long, ByVal fRequest As Long, ByVal lpszDriver As String, ByVal lpszAttributes As String) As Long
Private Declare Function SQLGetInstalledDrivers Lib "ODBCCP32.DLL" (ByVal lpszBuf As String, ByVal cbBufMax As Long, ByRef cbBufOut As Long) As Boolean
'Event Declarations:
Event EnumTables(Tabla As String, Clase As String)
Event EnumProcedures(Nombre As String)
Event ADOError(Codigo As Long, MensajeError As String, Modulo As String)

Private Sub UserControl_InitProperties()
    m_UsuarioSeguridad = m_def_UsuarioSeguridad
    m_PasswordSeguridad = m_def_PasswordSeguridad
    m_EsquemaSeguridad = m_def_EsquemaSeguridad
    m_UsuarioAplicacion = m_def_UsuarioAplicacion
    m_PasswordAplicacion = m_def_PasswordAplicacion
    m_Driver = m_def_Driver
    m_Aplicacion = m_def_Aplicacion
    m_Servidor = m_def_Servidor
    m_ServidorDSN = m_def_ServidorDSN                   ' add -005- b.
    m_ServidorPuerto = m_def_ServidorPuerto             ' add -005- a.
    m_ComponenteSeguridad = m_def_ComponenteSeguridad
    m_BaseSeguridad = m_def_BaseSeguridad
    m_Provider = m_def_Provider
    m_TimeOut = m_def_TimeOut
    m_IsOnLine = m_def_IsOnLine
    m_BaseAplicacion = m_def_BaseAplicacion
    m_Version = App.Major & "." & App.Minor & "." & App.Revision
    m_Encriptado = m_def_Encriptado
End Sub

'Load property values from storage
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    Set m_ConexionBase = PropBag.ReadProperty("ConexionBase", Nothing)
    m_UsuarioSeguridad = PropBag.ReadProperty("UsuarioSeguridad", m_def_UsuarioSeguridad)
    m_PasswordSeguridad = PropBag.ReadProperty("PasswordSeguridad", m_def_PasswordSeguridad)
    m_EsquemaSeguridad = PropBag.ReadProperty("EsquemaSeguridad", m_def_EsquemaSeguridad)
    m_UsuarioAplicacion = PropBag.ReadProperty("UsuarioAplicacion", m_def_UsuarioAplicacion)
    m_PasswordAplicacion = PropBag.ReadProperty("PasswordAplicacion", m_def_PasswordAplicacion)
    m_Driver = PropBag.ReadProperty("Driver", m_def_Driver)
    m_Aplicacion = PropBag.ReadProperty("Aplicacion", m_def_Aplicacion)
    m_Servidor = PropBag.ReadProperty("Servidor", m_def_Servidor)
    m_ServidorDSN = PropBag.ReadProperty("ServidorDSN", m_def_ServidorDSN)                  ' add -005- b.
    m_ServidorPuerto = PropBag.ReadProperty("ServidorPuerto", m_def_ServidorPuerto)         ' add -005- a.
    m_ComponenteSeguridad = PropBag.ReadProperty("ComponenteSeguridad", m_def_ComponenteSeguridad)
    m_BaseSeguridad = PropBag.ReadProperty("BaseSeguridad", m_def_BaseSeguridad)
    m_Provider = PropBag.ReadProperty("Provider", m_def_Provider)
    m_TimeOut = PropBag.ReadProperty("TimeOut", m_def_TimeOut)
    m_IsOnLine = PropBag.ReadProperty("IsOnLine", m_def_IsOnLine)
    m_BaseAplicacion = PropBag.ReadProperty("BaseAplicacion", m_def_BaseAplicacion)
    m_Version = PropBag.ReadProperty("Version", m_def_Version)
    m_Encriptado = PropBag.ReadProperty("Encriptado", m_def_Encriptado)
End Sub

Private Sub UserControl_Resize()
    UserControl.Height = 500
    UserControl.Width = 500
End Sub

'Write property values to storage
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("UsuarioSeguridad", m_UsuarioSeguridad, m_def_UsuarioSeguridad)
    Call PropBag.WriteProperty("PasswordSeguridad", m_PasswordSeguridad, m_def_PasswordSeguridad)
    Call PropBag.WriteProperty("EsquemaSeguridad", m_EsquemaSeguridad, m_def_EsquemaSeguridad)
    Call PropBag.WriteProperty("UsuarioAplicacion", m_UsuarioAplicacion, m_def_UsuarioAplicacion)
    Call PropBag.WriteProperty("PasswordAplicacion", m_PasswordAplicacion, m_def_PasswordAplicacion)
    Call PropBag.WriteProperty("Driver", m_Driver, m_def_Driver)
    Call PropBag.WriteProperty("Aplicacion", m_Aplicacion, m_def_Aplicacion)
    Call PropBag.WriteProperty("Servidor", m_Servidor, m_def_Servidor)
    Call PropBag.WriteProperty("ServidorDSN", m_ServidorDSN, m_def_ServidorDSN)                     ' add -005- b.
    Call PropBag.WriteProperty("ServidorPuerto", m_ServidorPuerto, m_def_ServidorPuerto)            ' add -005- a.
    Call PropBag.WriteProperty("ConexionBase", m_ConexionBase, Nothing)
    Call PropBag.WriteProperty("ComponenteSeguridad", m_ComponenteSeguridad, m_def_ComponenteSeguridad)
    Call PropBag.WriteProperty("BaseSeguridad", m_BaseSeguridad, m_def_BaseSeguridad)
    Call PropBag.WriteProperty("Provider", m_Provider, m_def_Provider)
    Call PropBag.WriteProperty("TimeOut", m_TimeOut, m_def_TimeOut)
    Call PropBag.WriteProperty("IsOnLine", m_IsOnLine, m_def_IsOnLine)
    Call PropBag.WriteProperty("BaseAplicacion", m_BaseAplicacion, m_def_BaseAplicacion)
    Call PropBag.WriteProperty("Version", m_Version, m_def_Version)
    Call PropBag.WriteProperty("Encriptado", m_Encriptado, m_def_Encriptado)
End Sub
'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=9
Public Function AbrirConexion() As Object
'*************************************************************
' Ejecuta los pasos necesarios para la conexi�n con la BD
' devuelve un objeto del tipo adoconnection
'*************************************************************
    If m_EsquemaSeguridad Then If Not ConexionSeguridad Then Exit Function
    Call ConexionAplicacion
End Function

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=0
Public Function CambiarPassword(newPassword As String) As Boolean
'*************************************************************
' Ejecuta SP_password
'*************************************************************
    Dim cmdStoredProcedure As ADODB.Command
    Dim prmParametros As ADODB.Parameter
    Dim rstResultado As ADODB.Recordset
    On Error GoTo Err_CambiarPassword
    Screen.MousePointer = vbHourglass
    RaiseEvent ADOError(0, "", "")
    
    Set cmdStoredProcedure = New ADODB.Command
    With cmdStoredProcedure
        If InStr(1, UCase(m_Driver), "SQL SERVER") > 0 Then
            .CommandText = "sp_password"
        ElseIf InStr(1, UCase(m_Driver), "ACCESS") > 0 Then
            .CommandText = "sp_password"
        ElseIf InStr(1, UCase(m_Driver), "SYBASE") > 0 Then
            .CommandText = "sybsystemprocs..sp_password"
        ''{ add -005- a.
        'ElseIf InStr(1, UCase(m_Driver), "ADAPTIVE") > 0 Then
        '    .CommandText = "sybsystemprocs..sp_password"
        ''}
        '{ add -005- a.
        ElseIf InStr(1, UCase(m_Driver), "ADAPTIVE") > 0 Then   ' SYBASE 15: Adaptive Server Enterprise
            .CommandText = "sybsystemprocs..sp_password"
'            'm_ConexionBase.Execute ("alter login " & m_UsuarioAplicacion & " modify password inmmediately " & PasswordAplicacion & ComponenteSeguridad)
'            'mensaje = "alter login '" & m_UsuarioAplicacion & "' with password '" & PasswordAplicacion & ComponenteSeguridad & "' modify password immediately '" & newPassword & ComponenteSeguridad & "'"
'            'MsgBox mensaje
'
'            'MsgBox "Por ejecutar el use master..."
'            'm_ConexionBase.Execute ("use master")
'
'            MsgBox "Por ejecutar el alter login..."
'            m_ConexionBase.Execute ("alter login '" & m_UsuarioAplicacion & "' with password '" & PasswordAplicacion & ComponenteSeguridad & "' modify password immediately '" & newPassword & ComponenteSeguridad & "'")
'
'            'MsgBox "Por ejecutar el use GesPet..."
'            'm_ConexionBase.Execute ("use GesPet")
'            PasswordAplicacion = newPassword
'            CambiarPassword = True
'            Exit Function
        '}
        Else
            RaiseEvent ADOError(9999, "Invalid Driver Name", "[CambiarPassword]")
            Exit Function
        End If
        .CommandType = adCmdStoredProc
        .CommandTimeout = m_TimeOut
    End With
    'Prepara los par�metros del SP
    Set prmParametros = New ADODB.Parameter
    With prmParametros
        .Type = adVarChar
        .Size = 30
        .Direction = adParamInput
        .value = m_PasswordAplicacion & m_ComponenteSeguridad
    End With
    cmdStoredProcedure.Parameters.Append prmParametros
    Set prmParametros = New ADODB.Parameter
    With prmParametros
        .Type = adVarChar
        .Size = 31
        .Direction = adParamInput
        .value = newPassword & m_ComponenteSeguridad
    End With
    cmdStoredProcedure.Parameters.Append prmParametros
'''    Set prmParametros = New ADODB.Parameter
'''    With prmParametros
'''        .Type = adVarChar
'''        .Size = 30
'''        .Direction = adParamInput
'''        .Value = m_UsuarioAplicacion
'''    End With
'''    cmdStoredProcedure.Parameters.Append prmParametros
    'Ejecuta el SP
    Set cmdStoredProcedure.ActiveConnection = m_ConexionBase
    If aplRST.State = adStateOpen Then aplRST.Close             ' add -001- a.
    Set rstResultado = cmdStoredProcedure.Execute
    PasswordAplicacion = newPassword
    'Cierra la conexi�n
    Set rstResultado = Nothing
    CambiarPassword = True
    Screen.MousePointer = vbNormal
    On Error GoTo 0
Exit Function

Err_CambiarPassword:
    If InStr(1, Err.DESCRIPTION, "The specified password does not have any numeric characters. New passwords must have at least one numeric character.", vbTextCompare) > 0 Then
        CambiarPassword = False
        Screen.MousePointer = vbNormal
        'RaiseEvent ADOError(Err, Error, "[CambiarPassword]")
        glAdoError = vbCrLf & "La contrase�a especificada no contiene ning�n caracter num�rico. Las nuevas contrase�as deben contener al menos un caracter num�rico."
    Else
        CambiarPassword = False
        Screen.MousePointer = vbNormal
        RaiseEvent ADOError(Err, Error, "[CambiarPassword]")
    End If
End Function

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=0
Public Function InicializarPassword() As Boolean
'******************************************************************
' Ejecuta SP_password cuando el password del usuario est� en blanco
'******************************************************************
    Dim cmdStoredProcedure As ADODB.Command
    Dim prmParametros As ADODB.Parameter
    Dim rstResultado As ADODB.Recordset
    On Error GoTo Err_InicializarPassword
    Screen.MousePointer = vbHourglass
    RaiseEvent ADOError(0, "", "")
    
    'Prepara la ejecucion del SP
    Set cmdStoredProcedure = New ADODB.Command
    With cmdStoredProcedure
        If InStr(1, UCase(m_Driver), "SQL SERVER") > 0 Then
            .CommandText = "sp_password"
        ElseIf InStr(1, UCase(m_Driver), "ACCESS") > 0 Then
            .CommandText = "sp_password"
        ElseIf InStr(1, UCase(m_Driver), "SYBASE") > 0 Then
            .CommandText = "sybsystemprocs..sp_password"
        '{ add -005- a.
        ElseIf InStr(1, UCase(m_Driver), "ADAPTIVE") > 0 Then   ' SYBASE 15: Adaptive Server Enterprise
            .CommandText = "sybsystemprocs..sp_password"                       ' del -009- a.
'            '{ add -009- a.
'            m_ConexionBase.Execute ("alter login " & m_UsuarioAplicacion & " modify password inmmediately " & PasswordAplicacion & ComponenteSeguridad)
'            InicializarPassword = True
'            Exit Function
'            '}
        '}
        Else
            RaiseEvent ADOError(9999, "Invalid Driver Name", "[InicializarPassword]")
            Exit Function
        End If
        .CommandType = adCmdStoredProc
        .CommandTimeout = m_TimeOut
    End With
    
    'Prepara los par�metros del SP
    Set prmParametros = New ADODB.Parameter
    With prmParametros
        .Type = adVarChar
        .Size = 30
        .Direction = adParamInput
        .value = ComponenteSeguridad
    End With
    cmdStoredProcedure.Parameters.Append prmParametros
    Set prmParametros = New ADODB.Parameter
    With prmParametros
        .Type = adVarChar
        .Size = 31
        .Direction = adParamInput
        .value = PasswordAplicacion & ComponenteSeguridad
    End With
    cmdStoredProcedure.Parameters.Append prmParametros
'''    Set prmParametros = New ADODB.Parameter
'''    With prmParametros
'''        .Type = adVarChar
'''        .Size = 30
'''        .Direction = adParamInput
'''        .Value = UsuarioAplicacion
'''    End With
'''    cmdStoredProcedure.Parameters.Append prmParametros
    'Ejecuta el SP
    Set cmdStoredProcedure.ActiveConnection = m_ConexionBase
    'If aplRST.State = adStateOpen Then aplRST.Close             ' add -001- a.
    Set rstResultado = cmdStoredProcedure.Execute
    'Cierra la conexi�n
    Set rstResultado = Nothing
    InicializarPassword = True
    Screen.MousePointer = vbNormal
    On Error GoTo 0
Exit Function

Err_InicializarPassword:
    InicializarPassword = False
    Screen.MousePointer = vbNormal
    RaiseEvent ADOError(Err, Error, "[InicializarPassword]")
End Function

Public Function InicializarPassword2(ByRef conn As ADODB.Connection, ByVal PasswordOld As String, ByVal PasswordNew As String) As Boolean
'******************************************************************
' Ejecuta SP_password cuando el password del usuario est� en blanco
'******************************************************************
    Dim cmdStoredProcedure As ADODB.Command
    Dim prmParametros As ADODB.Parameter
    Dim rstResultado As ADODB.Recordset
    
    On Error GoTo Err_InicializarPassword
    Screen.MousePointer = vbHourglass
    RaiseEvent ADOError(0, "", "")
    
    'Prepara la ejecucion del SP
    Set cmdStoredProcedure = New ADODB.Command
    With cmdStoredProcedure
        If InStr(1, UCase(m_Driver), "SQL SERVER") > 0 Then
            .CommandText = "sp_password"
        ElseIf InStr(1, UCase(m_Driver), "ACCESS") > 0 Then
            .CommandText = "sp_password"
        ElseIf InStr(1, UCase(m_Driver), "SYBASE") > 0 Then
            .CommandText = "sybsystemprocs..sp_password"
        ElseIf InStr(1, UCase(m_Driver), "ADAPTIVE") > 0 Then
            .CommandText = "sybsystemprocs..sp_password"
        Else
            RaiseEvent ADOError(9999, "Invalid Driver Name", "[InicializarPassword]")
            Exit Function
        End If
        .CommandType = adCmdStoredProc
        .CommandTimeout = m_TimeOut
    End With
    
    'Prepara los par�metros del SP
    Set prmParametros = New ADODB.Parameter
    With prmParametros
        .Type = adVarChar
        .Size = 30
        .Direction = adParamInput
        '.Value = ComponenteSeguridad
        .value = PasswordOld
    End With
    cmdStoredProcedure.Parameters.Append prmParametros
    Set prmParametros = New ADODB.Parameter
    With prmParametros
        .Type = adVarChar
        .Size = 31
        .Direction = adParamInput
        '.Value = PasswordAplicacion & ComponenteSeguridad
        .value = PasswordNew
    End With
    cmdStoredProcedure.Parameters.Append prmParametros
    'Ejecuta el SP
    Set cmdStoredProcedure.ActiveConnection = conn
    Set rstResultado = cmdStoredProcedure.Execute
    'Cierra la conexi�n
    Set rstResultado = Nothing
    InicializarPassword2 = True
    Screen.MousePointer = vbNormal
    On Error GoTo 0
Exit Function

Err_InicializarPassword:
    InicializarPassword2 = False
    Screen.MousePointer = vbNormal
    RaiseEvent ADOError(Err, Error, "[InicializarPassword2]")
End Function


'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=0
Public Function CerrarConexion() As Boolean
    Screen.MousePointer = vbHourglass
    ' Registro el logout del recurso en el sistema
    If Not (ConexionBase Is Nothing) Then
        If ConexionBase.State = adStateOpen Then ConexionBase.Close
        Set ConexionBase = Nothing
    End If
    IsOnLine = False
    Screen.MousePointer = vbNormal
End Function

Private Function ConexionSeguridad() As Boolean
'*************************************************************
' Abre la conexi�n con la base de seguridad informatica
' y busca los valores de conexi�n con la base de la aplicaci�n
'*************************************************************
    Dim cmdStoredProcedure As ADODB.Command
    Dim prmParametros As ADODB.Parameter
    Dim rstResultado As ADODB.Recordset
    Dim sPassword As String
    '{ add -xxx- a.
    Dim cMensajeError As String
    Dim cMensajeMotor As String
    '}
    Dim bLoginTry As Boolean
    Dim connStr As String
    
    bLoginTry = True
    
    On Error GoTo Err_ConexionSeguridad
    Call Puntero(True)
    RaiseEvent ADOError(0, "", "")
    'Desencripta el password en caso de ser necesario
    If m_Encriptado Then
        sPassword = Desencriptar(m_PasswordSeguridad)
        If bErrorProcesoDesencriptado Then
            ConexionSeguridad = False
            Call Puntero(False)
            RaiseEvent ADOError(9999, "Error en el proceso de desencriptado", "[ConexionSeguridad]")
            Exit Function
        End If
    Else
        sPassword = m_PasswordSeguridad
    End If
    'Inicializa la conexi�n con el servidor de Seguridad Inform�tica
    Set cnnADO = New ADODB.Connection
    If InStr(1, UCase(m_Driver), "SQL SERVER") > 0 Then
        cnnADO.Open ("DRIVER=" & m_Driver & ";SERVER=" & m_Servidor & ";DB=" & m_BaseSeguridad & ";UID=" & m_UsuarioSeguridad & ";PWD=" & sPassword)
    ElseIf InStr(1, UCase(m_Driver), "ACCESS") > 0 Then
        cnnADO.Open ("DRIVER=" & m_Driver & ";DBQ=" & m_BaseSeguridad & ";UID=" & m_UsuarioSeguridad & ";PWD=" & sPassword)
    '{ add -005- a.
    'ElseIf InStr(1, UCase(m_Driver), "ADAPTIVE") > 0 Then
    ElseIf InStr(1, UCase(m_Driver), ODBCDRIVERNAME_SYBASE15) > 0 Then
        cnnADO.Open ("DRIVER=" & m_Driver & ";SERVER=" & m_Servidor & ";PORT=" & m_ServidorPuerto & ";DB=" & m_BaseSeguridad & ";UID=" & m_UsuarioSeguridad & ";PWD=" & sPassword & ";LANG=us_english")
    '}
    'ElseIf InStr(1, UCase(m_Driver), "SYBASE") > 0 Then
    ElseIf InStr(1, UCase(m_Driver), ODBCDRIVERNAME_SYBASE12_5) > 0 Then
        'cnnADO.Open ("DRIVER=" & m_Driver & ";SRVR=" & m_Servidor & ";DB=" & m_BaseSeguridad & ";UID=" & m_UsuarioSeguridad & ";PWD=" & sPassword)
        cnnADO.Open ("DRIVER=" & m_Driver & ";SRVR=" & m_ServidorDSN & ";DB=" & m_BaseSeguridad & ";UID=" & m_UsuarioSeguridad & ";PWD=" & sPassword)
    Else
        RaiseEvent ADOError(9999, "Invalid Driver Name", "[ConexionSeguridad]")
        Exit Function
    End If

    'Prepara la ejecucion del SP
    Set cmdStoredProcedure = New ADODB.Command
    With cmdStoredProcedure
        .CommandText = "ObtenerDatosSI"
        .CommandType = adCmdStoredProc
        .CommandTimeout = m_TimeOut
    End With
    'Prepara los par�metros del SP
    Set prmParametros = New ADODB.Parameter
    With prmParametros
        .Type = adVarChar
        .Size = 10
        .Direction = adParamInput
        .value = Left(m_Aplicacion, .Size)
    End With
    cmdStoredProcedure.Parameters.Append prmParametros
    'Ejecuta el SP
    Set cmdStoredProcedure.ActiveConnection = cnnADO
    Set rstResultado = cmdStoredProcedure.Execute
    'Actualiza propiedades del control
    BaseAplicacion = rstResultado(0)
    'Servidor = rstResultado(1)
    ComponenteSeguridad = rstResultado(2)
    
    Set rstResultado = Nothing                                      ' add -008- a.
    Call ObtenerDatosUsuarioSybase(m_UsuarioAplicacion)             ' add -008- a.
    'Cierra la conexi�n

    cnnADO.Close
    Set cnnADO = Nothing
    'Set rstResultado = Nothing                                     ' del -008- a.
    ConexionSeguridad = True
    'Screen.MousePointer = vbNormal
    On Error GoTo 0
Exit Function

Err_ConexionSeguridad:
    ConexionSeguridad = False
    
    If bLoginTry Then
        bLoginTry = False
        If InStr(1, cnnADO.Errors(0).DESCRIPTION, "user api layer: internal Client Library error: HAFAILOVER:Trying to connect to", vbTextCompare) > 0 Then
            m_Servidor = m_ServidorDSN
            Resume
        End If
    End If
    '{ add -004- a.
    cMensajeError = "No se ha podido establecer la conexi�n con el entorno de seguridad." & vbCrLf & _
                    "Es posible que el servidor este fuera de servicio en estos momentos." & vbCrLf & vbCrLf & _
                    "Vuelva a intentar el ingreso al sistema en unos minutos."
    MsgBox cMensajeError, vbOKOnly + vbExclamation, "Conexi�n: Seguridad"
    Dim i As Integer
    For i = 0 To cnnADO.Errors.Count - 1
        cMensajeMotor = cMensajeMotor & " - " & cnnADO.Errors(i).DESCRIPTION & vbCrLf
    Next i
    RaiseEvent ADOError(Err, cMensajeMotor, "[ConexionSeguridad]")
    '}
End Function

Private Function ConexionAplicacion() As Boolean
'*************************************************************
' Abre la conexi�n con la base de datos de la aplicaci�n
'*************************************************************
    Dim bInicializarPassword As Boolean
    Dim cMensajeError As String
    Dim cMensajeMotor As String
    Dim sMensajeParaSI As String
    
    On Error GoTo Err_ConexionAplicacion
    RaiseEvent ADOError(0, "", "")
    bInicializarPassword = False
    
    'Inicializa la conexi�n con el servidor de la aplicaci�n
    Set ConexionBase = New ADODB.Connection
    If InStr(1, UCase(m_Driver), "SQL SERVER") > 0 Then
        ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";SERVER=" & m_Servidor & ";DATABASE=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_PasswordAplicacion & m_ComponenteSeguridad & ";APP=" & App.EXEName)
    ElseIf InStr(1, UCase(m_Driver), "ACCESS") > 0 Then
        ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";DBQ=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_PasswordAplicacion & m_ComponenteSeguridad)
    '{ add -005- a.
    ElseIf InStr(1, UCase(m_Driver), ODBCDRIVERNAME_SYBASE15) > 0 Then
        ' ADO Ole DB Sybase 15 (esta es la que vale! 2016.09.08)
'        ConexionBase.ConnectionString = "Provider=ASEOLEDB;Data Source=" & m_Servidor & ":" & m_ServidorPuerto & ";Initial Catalog=" & m_BaseAplicacion & ";User Id=" & m_UsuarioAplicacion & ";Password=" & m_PasswordAplicacion & m_ComponenteSeguridad & ";"
        
        ConexionBase.ConnectionString = "DRIVER={" & m_Driver & "}" & _
                                        ";SERVER=" & m_Servidor & _
                                        ";PORT=" & m_ServidorPuerto & _
                                        ";DB=" & m_BaseAplicacion & _
                                        ";UID=" & m_UsuarioAplicacion & _
                                        ";PWD=" & m_PasswordAplicacion & m_ComponenteSeguridad & _
                                        ";ENABLESERVERPACKETSIZE=0" & _
                                        ";LANGUAGE=us_english" & _
                                        ";PACKETSIZE=512" & _
                                        ";TEXTSIZE=4194304"
    '}
    ElseIf InStr(1, UCase(m_Driver), ODBCDRIVERNAME_SYBASE12_5) > 0 Then
        'ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";SRVR=" & m_Servidor & ";DB=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_PasswordAplicacion & m_ComponenteSeguridad & ";DLDBL=" & Trim(CStr(ARCHIVOS_ADJUNTOS_MAXSIZE / 1024))) ' upd -003- a. Se agrega el par�metro DLDBL para subir el m�xmimo permitido para archivos BLOB
        ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";SRVR=" & m_ServidorDSN & ";DB=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_PasswordAplicacion & m_ComponenteSeguridad & ";DLDBL=" & Trim(CStr(ARCHIVOS_ADJUNTOS_MAXSIZE / 1024))) ' upd -003- a. Se agrega el par�metro DLDBL para subir el m�xmimo permitido para archivos BLOB
    Else
        RaiseEvent ADOError(9999, "Invalid Driver Name", "[ConexionAplicacion]")
        Exit Function
    End If
    'MsgBox ConexionBase.ConnectionString
    
    ConexionBase.CursorLocation = adUseClient   ' add -002- a. Importante: este agregado habilita el Motor de Cursor nativo de ADO
    'MsgBox "Primer intento de conexi�n..."
    ConexionBase.Open
    If bInicializarPassword Then
        'MsgBox "Se conect� bien luego de detectar una inicializaci�n de pwd..."
        Call InicializarPassword
        'Set ConexionBase = Nothing
        bInicializarPassword = False
        Call ConexionAplicacion
    End If
    'Provider = ConexionBase.Provider
    IsOnLine = True
    ConexionAplicacion = True
    On Error GoTo 0
Exit Function

Err_ConexionAplicacion:
    'MsgBox Err.Number & ": " & Err.DESCRIPTION & " (" & Err.source & ")"
    'Call doLog(Err.Number & ": " & Err.DESCRIPTION & " (" & Err.Source & ")")
    
    'usuarioSybase.Pwdate:          Date the password was last changed.
    'usuarioSybase.LastLoginDate:   Timestamp for the user�s last login.
    
    ' Si el �ltimo cambio de password es mayor al �ltimo login efectuado, se pidi� un blanqueo, hay que inicializar
    If (usuarioSybase.Pwdate > usuarioSybase.LastLoginDate Or usuarioSybase.LastLoginDate = "00:00:00") Then
        bInicializarPassword = True
    
    'If Err = -2147467259 And Not bInicializarPassword Then
        ' Si el �ltimo cambio de password es mayor al �ltimo login efectuado, se pidi� un blanqueo, hay que inicializar
        'If usuarioSybase.Pwdate > usuarioSybase.LastLoginDate Then
        '    bInicializarPassword = True
       
        If InStr(1, UCase(m_Driver), "SQL SERVER") > 0 Then
            ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";SERVER=" & m_Servidor & ";DB=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_ComponenteSeguridad)
        ElseIf InStr(1, UCase(m_Driver), "SYBASE") > 0 Then
            'ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";SRVR=" & m_Servidor & ";DB=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_ComponenteSeguridad & ";DLDBL=" & Trim(CStr(ARCHIVOS_ADJUNTOS_MAXSIZE / 1024))) ' upd -003- a. Se agrega el par�metro DLDBL para subir el m�xmimo permitido para archivos BLOB
            ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";SRVR=" & m_ServidorDSN & ";DB=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_ComponenteSeguridad & ";DLDBL=4096")         '' & Trim(CStr(ARCHIVOS_ADJUNTOS_MAXSIZE / 1024)))
        ElseIf InStr(1, UCase(m_Driver), "ADAPTIVE") > 0 Then
            'ConexionBase.ConnectionString = ("DRIVER=" & m_Driver & ";SERVER=" & m_Servidor & ";PORT=" & m_ServidorPuerto & ";DB=" & m_BaseAplicacion & ";UID=" & m_UsuarioAplicacion & ";PWD=" & m_ComponenteSeguridad & ";LANG=us_english" & ";EnableServerPacketSize=0;RestrictMaximumPacketSize=0;TextSize=" & ARCHIVOS_ADJUNTOS_MAXSIZE & ";PacketSize=" & ARCHIVOS_ADJUNTOS_MAXSIZE)
            ConexionBase.ConnectionString = "DRIVER={" & m_Driver & "}" & _
                                            ";SERVER=" & m_Servidor & _
                                            ";PORT=" & m_ServidorPuerto & _
                                            ";DB=" & m_BaseAplicacion & _
                                            ";UID=" & m_UsuarioAplicacion & _
                                            ";PWD=" & m_PasswordAplicacion & m_ComponenteSeguridad & _
                                            ";ENABLESERVERPACKETSIZE=0" & _
                                            ";LANGUAGE=us_english" & _
                                            ";PACKETSIZE=512" & _
                                            ";OldPassword=" & m_ComponenteSeguridad & _
                                            ";TEXTSIZE=4194304"
        End If
        Resume
    Else
        If ClearNull(usuarioSybase.UserId) <> "" Then
            If usuarioSybase.LockReason <> 0 Then
                Select Case usuarioSybase.LockReason
                    Case LOGIN_REJECTIONREASON.LOGIN_REJECTBYLOCKED
                        cMensajeError = "La cuenta se encuentra bloqueada." & vbCrLf & "Gestione el DESBLOQUEO mediante el portal de SI (CRM)."
                    Case LOGIN_REJECTIONREASON.LOGIN_REJECTBYPWDEXPIRATION
                        cMensajeError = "La contrase�a de la cuenta ha expirado." & vbCrLf & "Gestione el BLANQUEO de clave mediante el portal de SI (CRM)."
                    Case Else
                        cMensajeError = cMensajeError & "No se ha podido establecer la conexi�n con el entorno del aplicativo." & vbCrLf & _
                                        "Verifique que los datos de usuario y contrase�a ingresados sean correctos."
                End Select
            Else
                cMensajeError = cMensajeError & "No se ha podido establecer la conexi�n con el entorno del aplicativo." & vbCrLf & _
                        "Verifique que los datos de usuario y contrase�a ingresados sean correctos."
            End If
        Else
            cMensajeError = "No se ha podido establecer la conexi�n con el entorno del aplicativo." & vbCrLf & _
                        "Verifique que los datos de usuario y contrase�a ingresados sean correctos."
        End If
        If ClearNull(cMensajeError) <> "" Then MsgBox cMensajeError, vbOKOnly + vbExclamation, "Conexi�n: Aplicaci�n"
        Dim i As Integer
        For i = 0 To ConexionBase.Errors.Count - 1
            cMensajeMotor = cMensajeMotor & " - " & ConexionBase.Errors(i).DESCRIPTION
        Next i
        If cMensajeMotor <> "" Then RaiseEvent ADOError(Err, cMensajeMotor, "[ConexionAplicacion]")
        Set ConexionBase = Nothing
        ConexionAplicacion = False
        Call Puntero(False)
        '}
    End If
End Function

Public Property Get UsuarioSeguridad() As String
    UsuarioSeguridad = m_UsuarioSeguridad
End Property

Public Property Let UsuarioSeguridad(ByVal New_UsuarioSeguridad As String)
    m_UsuarioSeguridad = New_UsuarioSeguridad
    PropertyChanged "UsuarioSeguridad"
End Property

Public Property Get PasswordSeguridad() As String
    PasswordSeguridad = m_PasswordSeguridad
End Property

Public Property Let PasswordSeguridad(ByVal New_PasswordSeguridad As String)
    m_PasswordSeguridad = New_PasswordSeguridad
    PropertyChanged "PasswordSeguridad"
End Property

Public Property Get EsquemaSeguridad() As Boolean
    EsquemaSeguridad = m_EsquemaSeguridad
End Property

Public Property Let EsquemaSeguridad(ByVal New_EsquemaSeguridad As Boolean)
    m_EsquemaSeguridad = New_EsquemaSeguridad
    PropertyChanged "EsquemaSeguridad"
End Property

Public Property Get UsuarioAplicacion() As String
    UsuarioAplicacion = m_UsuarioAplicacion
End Property

Public Property Let UsuarioAplicacion(ByVal New_UsuarioAplicacion As String)
    m_UsuarioAplicacion = New_UsuarioAplicacion
    PropertyChanged "UsuarioAplicacion"
End Property

Public Property Get PasswordAplicacion() As String
    PasswordAplicacion = m_PasswordAplicacion
End Property

Public Property Let PasswordAplicacion(ByVal New_PasswordAplicacion As String)
    m_PasswordAplicacion = New_PasswordAplicacion
    PropertyChanged "PasswordAplicacion"
End Property

Public Property Get driver() As String
    driver = m_Driver
End Property

Public Property Let driver(ByVal New_Driver As String)
    m_Driver = New_Driver
    PropertyChanged "Driver"
End Property

Public Property Get Aplicacion() As String
    Aplicacion = m_Aplicacion
End Property

Public Property Let Aplicacion(ByVal New_Aplicacion As String)
    m_Aplicacion = New_Aplicacion
    PropertyChanged "Aplicacion"
End Property

Public Property Get ServidorDSN() As String
    ServidorDSN = m_ServidorDSN
End Property

Public Property Get Servidor() As String
    Servidor = m_Servidor
End Property

Public Property Get servidorPuerto() As String
    servidorPuerto = m_ServidorPuerto
End Property

Public Property Let Servidor(ByVal New_Servidor As String)
    m_Servidor = New_Servidor
    PropertyChanged "Servidor"
End Property

Public Property Let ServidorDSN(ByVal New_Servidor As String)
    m_ServidorDSN = New_Servidor
    PropertyChanged "ServidorDSN"
End Property

Public Property Let servidorPuerto(ByVal New_ServidorPuerto As String)
    m_ServidorPuerto = New_ServidorPuerto
    PropertyChanged "ServidorPuerto"
End Property

Public Property Get ConexionBase() As ADODB.Connection
    Set ConexionBase = m_ConexionBase
End Property

Public Property Set ConexionBase(ByVal New_ConexionBase As ADODB.Connection)
    If Ambient.UserMode = False Then Err.Raise 383
    Set m_ConexionBase = New_ConexionBase
    PropertyChanged "ConexionBase"
End Property

Public Property Get ComponenteSeguridad() As String
    ComponenteSeguridad = m_ComponenteSeguridad
End Property

Public Property Let ComponenteSeguridad(ByVal New_ComponenteSeguridad As String)
    If Ambient.UserMode = False Then Err.Raise 382
    m_ComponenteSeguridad = New_ComponenteSeguridad
    PropertyChanged "ComponenteSeguridad"
End Property

Public Property Get BaseSeguridad() As String
    BaseSeguridad = m_BaseSeguridad
End Property

Public Property Let BaseSeguridad(ByVal New_BaseSeguridad As String)
    m_BaseSeguridad = New_BaseSeguridad
    PropertyChanged "BaseSeguridad"
End Property

Public Property Get Provider() As String
    Provider = m_Provider
End Property

Public Property Let Provider(ByVal New_Provider As String)
    If Ambient.UserMode = False Then Err.Raise 382
    m_Provider = New_Provider
    PropertyChanged "Provider"
End Property

Public Property Get TimeOut() As Integer
    TimeOut = m_TimeOut
End Property

Public Property Let TimeOut(ByVal New_TimeOut As Integer)
    m_TimeOut = New_TimeOut
    PropertyChanged "TimeOut"
End Property

Public Property Get IsOnLine() As Boolean
    IsOnLine = m_IsOnLine
End Property

Public Property Let IsOnLine(ByVal New_IsOnLine As Boolean)
    If Ambient.UserMode = False Then Err.Raise 382
    m_IsOnLine = New_IsOnLine
    PropertyChanged "IsOnLine"
End Property

Public Property Get BaseAplicacion() As Variant
    BaseAplicacion = m_BaseAplicacion
End Property

Public Property Let BaseAplicacion(ByVal New_BaseAplicacion As Variant)
    m_BaseAplicacion = New_BaseAplicacion
    PropertyChanged "BaseAplicacion"
End Property

Public Property Get Version() As String
    Version = m_Version
End Property

Public Property Let Version(ByVal New_Version As String)
    If Ambient.UserMode = False Then Exit Property
    If Ambient.UserMode Then Exit Property
    m_Version = New_Version
    PropertyChanged "Version"
End Property

Public Property Get Encriptado() As Boolean
    Encriptado = m_Encriptado
End Property

Public Property Let Encriptado(ByVal New_Encriptado As Boolean)
    m_Encriptado = New_Encriptado
    PropertyChanged "Encriptado"
End Property

'*****************************************************************
'funciones desencriptado
'*****************************************************************
Private Function Desencriptar(Texto As String) As String
    Dim nIndice As Integer
    Dim sClaveDesencriptado As String
    Dim sDesvioTexto As String
    Dim nCaracterTexto As Integer
    Dim nCaracterClaveDesencriptado As Integer
    Dim nCaracterNuevo As Integer
    Dim sTextoNuevo As String
    On Error GoTo Err_Desencriptar
    Screen.MousePointer = vbHourglass
    bErrorProcesoDesencriptado = False
    ArmarClave
    If bErrorProcesoDesencriptado Then Err.Raise 9999
    sTextoNuevo = ""
    sClaveDesencriptado = sClave
    For nIndice = 0 To (Len(Texto) \ Len(sClave))
        sClaveDesencriptado = sClaveDesencriptado & sClave
    Next nIndice
    sDesvioTexto = Right(Texto, 4)
    sDesvioTexto = Hex2Ascii(sDesvioTexto, Len(sDesvioTexto))
    Texto = Left(Texto, Len(Texto) - 4)
    Texto = Hex2Ascii(Texto, Len(Texto))
    sClaveDesencriptado = Left(sClaveDesencriptado, Len(Texto))
    For nIndice = 1 To Len(Texto)
        nCaracterTexto = Asc(Mid(Texto, nIndice, 1))
        nCaracterClaveDesencriptado = Asc(Mid(sClaveDesencriptado, nIndice, 1))
        nCaracterNuevo = (nCaracterTexto Xor CInt(sDesvioTexto)) - nCaracterClaveDesencriptado
        sTextoNuevo = sTextoNuevo & Chr(nCaracterNuevo)
    Next nIndice
    Texto = sTextoNuevo
    If bErrorProcesoDesencriptado Then Err.Raise 9999
    Screen.MousePointer = vbNormal
    Desencriptar = Texto
    On Error GoTo 0
Exit Function

Err_Desencriptar:
    Screen.MousePointer = vbNormal
    bErrorProcesoDesencriptado = True
    Desencriptar = "Error en el Proceso de Desencriptado"
End Function

'''Private Function DepurarCadena(sTexto As String) As String
'''    Dim nIndice As Integer
'''    Dim nCaracter As Integer
'''    For nIndice = 1 To Len(sTexto)
'''        nCaracter = Asc(Mid(sTexto, nIndice, 1))
'''        Select Case nCaracter
'''            Case 225            '� => a
'''                nCaracter = 97
'''            Case 233            '� => e
'''                nCaracter = 101
'''            Case 237            '� => i
'''                nCaracter = 105
'''            Case 243            '� => o
'''                nCaracter = 111
'''            Case 250            '� => u
'''                nCaracter = 117
'''            Case 241            '� => n
'''                nCaracter = 110
'''            Case 209            '� => N
'''                nCaracter = 78
'''            Case Else
'''        End Select
'''        DepurarCadena = DepurarCadena & Chr(nCaracter)
'''    Next nIndice
'''End Function
'''Private Function CadenaValida(sTexto As String) As Boolean
'''    Dim nIndice As Integer
'''    CadenaValida = True
'''    For nIndice = 1 To Len(sTexto)
'''        If Asc(Mid(sTexto, nIndice, 1)) < 32 Or Asc(Mid(sTexto, nIndice, 1)) > 127 Then
'''            If Asc(Mid(sTexto, nIndice, 1)) <> 13 And Asc(Mid(sTexto, nIndice, 1)) <> 10 Then
'''                CadenaValida = False
'''                Exit Function
'''            End If
'''        End If
'''    Next nIndice
'''End Function

Private Function Ascii2Hex(sBuffer As String, nLen_Buffer As Integer) As String
    Dim sBuffer_TMP As String
    Dim sStrHex As String
    Dim nI As Integer
    Dim nValor As Integer
    On Error GoTo Err_Ascii2Hex
    sBuffer_TMP = ""
    For nI = 1 To nLen_Buffer
        nValor = Asc(Mid$(sBuffer, nI, 1))
        sStrHex = Hex$(nValor)
        If (Len(sStrHex) = 1) Then
            sBuffer_TMP = sBuffer_TMP & "0"
        End If
        sBuffer_TMP = sBuffer_TMP & sStrHex
    Next nI
    Ascii2Hex = sBuffer_TMP
    On Error GoTo 0
Exit Function

Err_Ascii2Hex:
    bErrorProcesoDesencriptado = True
    Resume Next
End Function

Private Function Hex2Ascii(sBuffer, nLen_Buffer As Integer) As String
    Dim sBuffer_TMP As String
    Dim sStrHex As String
    Dim nI As Integer
    Dim nValor As Integer
    On Error GoTo Err_Hex2Ascii
    sBuffer_TMP = ""
    For nI = 1 To nLen_Buffer Step 2
        sStrHex = "&H" & Mid$(sBuffer, nI, 2)
        nValor = CInt(sStrHex)
        sBuffer_TMP = sBuffer_TMP & Chr$(nValor)
    Next nI
    Hex2Ascii = sBuffer_TMP
    On Error GoTo 0
Exit Function

Err_Hex2Ascii:
    bErrorProcesoDesencriptado = True
    Resume Next
End Function

Private Sub ArmarClave()
    Dim nIndiceClave As Integer
    Dim nIndiceCompensador As Integer
    On Error GoTo Err_ArmarClave
    nIndiceCompensador = 1
    For nIndiceClave = 1 To Len(sClaveOriginal)
        sClave = sClave & Chr(Asc(Mid(sClaveOriginal, nIndiceClave, 1)) + Val(Mid(sCompensador, nIndiceCompensador, 2)))
        nIndiceCompensador = nIndiceCompensador + 2
    Next nIndiceClave
    On Error GoTo 0
Exit Sub

Err_ArmarClave:
    bErrorProcesoDesencriptado = True
    Resume Next
End Sub
'*****************************************************************
Public Function CrearDSN(DESCRIPTION, DSN, DATABASE) As Boolean
    Dim intRet As Long
    Dim strAttributes As String
    Dim vODBC_DRIVERS() As String
    Dim i As Integer
    Dim bRetryCreate As Boolean
    
    bRetryCreate = True
    strAttributes = ""
    
    RaiseEvent ADOError(0, "", "")
    On Error GoTo Err_CrearDSN

    strAttributes = strAttributes & "DSN=" & DSN & Chr$(0)
    vODBC_DRIVERS = GetODBCDrivers()        ' Obtengo el vector con todos los drivers instalados
    
    For i = 0 To UBound(vODBC_DRIVERS)
        If InStr(1, Trim(UCase(vODBC_DRIVERS(i))), "ORACLE", vbTextCompare) = 0 Then
            Call SQLConfigDataSource(vbAPINull, ODBC_REMOVE_DSN, Trim(vODBC_DRIVERS(i)), strAttributes)
        End If
    Next i
   
    'MULTIPLATAFORMING PARA CREAR DSN
    If InStr(UCase(m_Driver), "SYBASE") > 0 Then
        'strAttributes = strAttributes & "Driver={SYBASE ASE ODBC Driver}" & Chr$(0)     ' add -xxx-
        'strAttributes = strAttributes & "SRVR=" & m_Servidor & Chr$(0)
        strAttributes = strAttributes & "SRVR=" & m_ServidorDSN & Chr$(0)
        strAttributes = strAttributes & "DistributedTransactionModel=0" & Chr$(0)
        strAttributes = strAttributes & "RaiseerrorPositionBehavior=0" & Chr$(0)
        strAttributes = strAttributes & "OPTIMIZEPREPARE=2" & Chr$(0)
        strAttributes = strAttributes & "SELECTMETHOD=1" & Chr$(0)
        strAttributes = strAttributes & "DefaultLongDataBuffLen=2048" & Chr$(0)
        strAttributes = strAttributes & "DATABASE=" & DATABASE & Chr$(0)
    ElseIf InStr(UCase(m_Driver), "ADAPTIVE") > 0 Then
        ' * Adaptive Server Enterprise 15.0 *
        ' Driver={Adaptive Server Enterprise};app=myAppName;server=myServerAddress;
        ' port=myPortnumber;db=myDataBase;uid=myUsername;pwd=myPassword;
        strAttributes = strAttributes & "Driver={Adaptive Server Enterprise}" & Chr$(0)
        strAttributes = strAttributes & "app=CGM" & Chr$(0)
        strAttributes = strAttributes & "server=" & m_Servidor & Chr$(0)
        'strAttributes = strAttributes & "server=" & m_ServidorDSN & Chr$(0)
        strAttributes = strAttributes & "port=" & m_ServidorPuerto & Chr$(0)
        strAttributes = strAttributes & "db=" & m_BaseAplicacion & Chr$(0)
        strAttributes = strAttributes & "uid=" & m_UsuarioAplicacion & Chr$(0)
        strAttributes = strAttributes & "pwd=" & m_PasswordAplicacion & m_ComponenteSeguridad & Chr$(0)
        strAttributes = strAttributes & "EnableServerPacketSize=0" & Chr$(0)
        strAttributes = strAttributes & "RestrictMaximumPacketSize=0" & Chr$(0)
        strAttributes = strAttributes & "PacketSize=512" & Chr$(0)
        strAttributes = strAttributes & "TextSize=4194304" & Chr$(0)
    ElseIf InStr(UCase(m_Driver), "SQL SERVER") > 0 Then
        strAttributes = strAttributes & "SERVER=" & m_Servidor & Chr$(0)
        strAttributes = strAttributes & "DATABASE=" & DATABASE & Chr$(0)
    Else
        CrearDSN = False
        RaiseEvent ADOError(9999, "Invalid Driver Name", "[CrearDSN]")
        Exit Function
    End If

    strAttributes = strAttributes & "DESCRIPTION=" & DESCRIPTION & Chr$(0)
    'strAttributes = strAttributes & "DATABASE=" & DATABASE & Chr$(0)
    intRet = SQLConfigDataSource(vbAPINull, ODBC_ADD_DSN, m_Driver, strAttributes)
    CrearDSN = intRet
    Exit Function
Err_CrearDSN:
    Screen.MousePointer = vbNormal
    ' sadfas
    If bRetryCreate Then
        
        Resume
    End If
    CrearDSN = False
    RaiseEvent ADOError(Err, Error, "[CrearDSN]")
End Function
'*****************************************************************

'*****************************************************************
Public Function GetODBCDrivers() As String()
    Dim intRet As Boolean
    Dim bufStr As String
    Dim bufMax  As Long
    Dim bufOut As Long
    
    bufStr = String(8192, Chr(32))
    bufMax = 8192
    intRet = SQLGetInstalledDrivers(bufStr, bufMax, bufOut)
    If intRet Then
        GetODBCDrivers = Split(Left(bufStr, bufOut), Chr(0))
    End If
Exit Function

Err_GetDriverr:
    'GetODBCDrivers = ""
    Erase GetODBCDrivers
    RaiseEvent ADOError(Err, Error, "[GetODBCDrivers]")
End Function

''*****************************************************************
'Public Function GetODBCDrivers() As String
'    GetODBCDrivers = ""
'    Dim intRet As Boolean
'    Dim bufStr As String
'    Dim bufMax  As Long
'    Dim bufOut As Long
'
'    bufStr = String(8192, Chr(0))
'    bufMax = 8192
'    intRet = SQLGetInstalledDrivers(bufStr, bufMax, bufOut)
'    If intRet Then
'        GetODBCDrivers = Left(bufStr, bufOut)
'    End If
'Exit Function
'
'Err_GetDriverr:
'    GetODBCDrivers = ""
'    RaiseEvent ADOError(Err, Error, "[GetODBCDrivers]")
'End Function

'***************************************************************
Public Function TestODBCDriver(strDriver As String) As Boolean
    Dim intRet As Boolean
    Dim bufStr As String
    Dim bufMax  As Long
    Dim bufOut As Long
    bufStr = String(8192, Chr(0))
    bufMax = 8192
    TestODBCDriver = False
    intRet = SQLGetInstalledDrivers(bufStr, bufMax, bufOut)
    If intRet Then
        bufStr = Left(bufStr, bufOut)
        If InStr(bufStr, strDriver) > 0 Then
            TestODBCDriver = True
        End If
    End If
Exit Function

Err_GetDriverr:
    TestODBCDriver = False
    RaiseEvent ADOError(Err, Error, "[TestODBCDriver]")
End Function

'************************************************************
Public Function SetODBCDriver(strDriver As String) As Boolean
    Dim vDrivers() As String
    Dim i As Integer
    Dim intRet As Boolean
    Dim bufStr As String
    Dim bufMax  As Long
    Dim bufOut As Long
    bufStr = String(8192, Chr(0))
    bufMax = 8192
    SetODBCDriver = False
    
    ' Esta versi�n de la funci�n, detecta autom�ticamente el Driver de DBMS
    ' seg�n un orden de preferencia: Sybase (12.5), Sybase (15) y SQL Server
    
    intRet = SQLGetInstalledDrivers(bufStr, bufMax, bufOut)
    vDrivers = Split(Left(bufStr, bufOut), Chr(0))
    
    'If InStr(1, Trim(UCase(bufStr)), "ADAPTIVE", vbTextCompare) > 0 Then
    If InStr(1, Trim(UCase(bufStr)), ODBCDRIVERNAME_SYBASE15, vbTextCompare) > 0 Then
        For i = 0 To UBound(vDrivers)
            If InStr(1, Trim(UCase(vDrivers(i))), ODBCDRIVERNAME_SYBASE15, vbTextCompare) > 0 Then
                m_Driver = vDrivers(i)
                If glDRIVER_ODBC = "" Then glDRIVER_ODBC = m_Driver
                SetODBCDriver = True
                Exit Function
            End If
        Next i
    'ElseIf InStr(1, Trim(UCase(bufStr)), "SYBASE", vbTextCompare) > 0 Then
    ElseIf InStr(1, Trim(UCase(bufStr)), ODBCDRIVERNAME_SYBASE12_5, vbTextCompare) > 0 Then
        For i = 0 To UBound(vDrivers)
            'Debug.Print vDrivers(i)
            If InStr(1, Trim(UCase(vDrivers(i))), ODBCDRIVERNAME_SYBASE12_5, vbTextCompare) > 0 Then
                m_Driver = vDrivers(i)
                If glDRIVER_ODBC = "" Then glDRIVER_ODBC = m_Driver
                SetODBCDriver = True
                Exit Function
            End If
        Next i
    ElseIf InStr(1, Trim(UCase(bufStr)), "SQL", vbTextCompare) > 0 Then
        For i = 0 To UBound(vDrivers)
            If InStr(1, Trim(UCase(vDrivers(i))), "SQL", vbTextCompare) > 0 Then
                m_Driver = vDrivers(i)
                If glDRIVER_ODBC = "" Then glDRIVER_ODBC = m_Driver
                SetODBCDriver = True
                Exit Function
            End If
        Next i
    Else
        SetODBCDriver = False
        RaiseEvent ADOError(9999, "No se especifico StrDriver", "[SetODBCDriver]")
        Exit Function
    End If
   
'    If InStr(1, Trim(UCase(bufStr)), "SYBASE", vbTextCompare) > 0 Then
'        For i = 0 To UBound(vDrivers)
'            If InStr(1, Trim(UCase(vDrivers(i))), "SYBASE", vbTextCompare) > 0 Then
'                m_Driver = vDrivers(i)
'                If glDRIVER_ODBC = "" Then glDRIVER_ODBC = m_Driver
'                SetODBCDriver = True
'                Exit Function
'            End If
'        Next i
'    ElseIf InStr(1, Trim(UCase(bufStr)), "ADAPTIVE", vbTextCompare) > 0 Then
'        For i = 0 To UBound(vDrivers)
'            If InStr(1, Trim(UCase(vDrivers(i))), "ADAPTIVE", vbTextCompare) > 0 Then
'                m_Driver = vDrivers(i)
'                If glDRIVER_ODBC = "" Then glDRIVER_ODBC = m_Driver
'                SetODBCDriver = True
'                Exit Function
'            End If
'        Next i
'    ElseIf InStr(1, Trim(UCase(bufStr)), "SQL", vbTextCompare) > 0 Then
'        For i = 0 To UBound(vDrivers)
'            If InStr(1, Trim(UCase(vDrivers(i))), "SQL", vbTextCompare) > 0 Then
'                m_Driver = vDrivers(i)
'                If glDRIVER_ODBC = "" Then glDRIVER_ODBC = m_Driver
'                SetODBCDriver = True
'                Exit Function
'            End If
'        Next i
'    Else
'        SetODBCDriver = False
'        RaiseEvent ADOError(9999, "No se especifico StrDriver", "[SetODBCDriver]")
'        Exit Function
'    End If
    
'    If ParseString(vDrivers, strDriver, "|") = 0 Then
'        SetODBCDriver = False
'        RaiseEvent ADOError(9999, "No se especifico StrDriver", "[SetODBCDriver]")
'        Exit Function
'    End If
    
'    intRet = SQLGetInstalledDrivers(bufStr, bufMax, bufOut)
'    If intRet Then
'        bufStr = Left(bufStr, bufOut)
'        For i = 1 To UBound(vDrivers)
'            If InStr(bufStr, Trim(vDrivers(i))) > 0 Then
'                m_Driver = vDrivers(i)
'                SetODBCDriver = True
'                Exit Function
'            End If
'        Next i
'    End If
Exit Function

Err_GetDriverr:
    SetODBCDriver = False
    RaiseEvent ADOError(Err, Error, "[SetODBCDriver]")
End Function
'******************************************************

'{ add -008- a.
Private Function ObtenerDatosUsuarioSybase(usuarioApp) As Long
    ' call sp_displaylogin
    On Error Resume Next
    If aplRST.State <> adStateClosed Then aplRST.Close
    'aplRST.Close
    Set aplRST = Nothing
    On Error GoTo Err_SP
    
    'Debug.Print "Call ObtenerDatosUsuarioSybase()"
    Dim sSQL As String
    sSQL = "SELECT name, fullname, lockreason, logincount, pwdate, lastlogindate, status FROM master..syslogins WHERE name = '" & ClearNull(usuarioApp) & "'"
    aplRST.Open sSQL, cnnADO, adOpenForwardOnly, adLockReadOnly
    If Not aplRST.EOF Then
        usuarioSybase.UserId = ClearNull(aplRST.Fields!name)
        usuarioSybase.UserFullName = ClearNull(aplRST.Fields!fullname)
        usuarioSybase.LockReason = ClearNull(IIf(IsNull(aplRST.Fields!LockReason), 0, aplRST.Fields!LockReason))
        usuarioSybase.LoginCount = ClearNull(IIf(IsNull(aplRST.Fields!LoginCount), 0, aplRST.Fields!LoginCount))
        If Not IsNull(aplRST.Fields!Pwdate) Then
            usuarioSybase.Pwdate = aplRST.Fields!Pwdate
        End If
        If Not IsNull(aplRST.Fields!LastLoginDate) Then
            usuarioSybase.LastLoginDate = aplRST.Fields!LastLoginDate
        End If
        usuarioSybase.Status = ClearNull(IIf(IsNull(aplRST.Fields!Status), "", aplRST.Fields!Status))
    End If
    If usuarioSybase.Status <> 0 Then
        ObtenerDatosUsuarioSybase = usuarioSybase.Status
    End If
    If aplRST.State = adStateOpen Then aplRST.Close
    Set aplRST.ActiveConnection = Nothing
    Set aplRST = Nothing
    On Error GoTo 0
Exit Function
Err_SP:
    ObtenerDatosUsuarioSybase = 0
    MsgBox (AnalizarErrorSQL(aplRST, Err))
    Set aplRST.ActiveConnection = Nothing
    Set aplRST = Nothing
End Function
'}

' ************************************************************************************************************************************************************************************************
' EVENTS
' ************************************************************************************************************************************************************************************************
Private Sub m_ConexionBase_WillConnect(ConnectionString As String, UserId As String, Password As String, Options As Long, adStatus As ADODB.EventStatusEnum, ByVal pConnection As ADODB.Connection)
    Dim strConnInfo As String
    'Call Status("m_ConexionBase_WillConnect")
'    strConnInfo = "ConnectionString: " & ConnectionString & vbCrLf & _
'                  "UserId: " & UserID & vbCrLf & _
'                  "Password" & Password
'    MsgBox strConnInfo, vbInformation
End Sub

Private Sub m_ConexionBase_ConnectComplete(ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pConnection As ADODB.Connection)
    Dim strConnInfo As String
    'strConnInfo = strConnInfo & "Usr: " & m_UsuarioAplicacion & vbCrLf
    'strConnInfo = strConnInfo & "Pwd: " & m_PasswordAplicacion & vbCrLf
    'strConnInfo = strConnInfo & "Old: " & m_ConexionBase.Properties("Oldpassword")
    
    'adStatus:
    ' 1:adStatusOK (OK)                             ' Indicates that the operation that has caused this event to occur completed successfully.
    ' 2:adStatusErrorsOccurred (ERRORSOCCURRED)     ' Indicates that the operation that has caused this event to occur did not complete successfully.
    
    'Call Status("m_ConexionBase_ConnectComplete: " + CStr(adStatus))
    'MsgBox ConexionBase.ConnectionString
    'MsgBox "m_ConexionBase:Conectado!" & vbCrLf & m_ConexionBase.Properties("Extended properties").Value, vbInformation
End Sub

Private Sub m_ConexionBase_Disconnect(adStatus As ADODB.EventStatusEnum, ByVal pConnection As ADODB.Connection)
    'Call Status("m_ConexionBase_Disconnect")
    'MsgBox "m_ConexionBase:Desconectado", vbExclamation
End Sub
