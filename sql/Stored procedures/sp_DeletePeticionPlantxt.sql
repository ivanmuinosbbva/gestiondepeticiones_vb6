/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 07.10.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionPlantxt'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionPlantxt' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionPlantxt
go

create procedure dbo.sp_DeletePeticionPlantxt
	@per_nrointerno	int,
	@pet_nrointerno	int,
	@cod_grupo		char(8),
	@mem_campo		char(10),
	@mem_secuencia	smallint
as
	delete from
		GesPet..PeticionPlantxt
	where
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(cod_grupo = @cod_grupo or @cod_grupo is null) and
		(mem_campo = @mem_campo or @mem_campo is null) and
		(mem_secuencia = @mem_secuencia or @mem_secuencia is null)
	return(0)
go

grant execute on dbo.sp_DeletePeticionPlantxt to GesPetUsr
go

print 'Actualización realizada.'
