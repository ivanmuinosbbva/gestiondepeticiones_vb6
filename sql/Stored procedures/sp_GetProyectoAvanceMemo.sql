use GesPet
go
print 'sp_GetProyectoAvanceMemo'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoAvanceMemo' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoAvanceMemo
go
create procedure dbo.sp_GetProyectoAvanceMemo 
	@prj_nrointerno   int,   
	@pav_aamm   char(6),
	@mem_campo      char(6)   
   
as   
    select mem_texto   
    from GesPet..ProyectoAvanceMem   
    where prj_nrointerno = @prj_nrointerno and   
		RTRIM(pav_aamm) = RTRIM(@pav_aamm) and
		RTRIM(mem_campo) = RTRIM(@mem_campo) 
    order by mem_secuencia   
return(0)   
go



grant execute on dbo.sp_GetProyectoAvanceMemo to GesPetUsr 
go


