use GesPet
go

print 'Creando/actualizando SP: sp_GetMotivo'
go

if exists (select * from sysobjects where name = 'sp_GetMotivo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetMotivo
go

create procedure dbo.sp_GetMotivo
	@cod_motivo		int=null,
	@flg_habil		char(1)=''
as 
	select 
		cod_motivo,
		nom_motivo,
		hab_motivo
	from
		GesPet.dbo.Motivos
	where
		(cod_motivo = @cod_motivo or @cod_motivo is null) and
		(hab_motivo = RTRIM(@flg_habil) or @flg_habil is null)
	order by
		cod_motivo

return(0) 
go

grant execute on dbo.sp_GetMotivo to GesPetUsr
go

print 'Actualización realizada.'
