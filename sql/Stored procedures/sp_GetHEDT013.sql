/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 12.11.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT013'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT013' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT013
go

create procedure dbo.sp_GetHEDT013
	@tipo_id	char(1)=null,
	@subtipo_id	char(1)=null,
	@item	int=null
as
	select 
		a.tipo_id,
		a.subtipo_id,
		a.item,
		a.item_nom
	from 
		GesPet.dbo.HEDT013 a
	where
		(a.tipo_id = @tipo_id or @tipo_id is null) and
		(a.subtipo_id = @subtipo_id or @subtipo_id is null) and
		(a.item = @item or @item is null)
go

grant execute on dbo.sp_GetHEDT013 to GesPetUsr
go

print 'Actualización realizada.'
