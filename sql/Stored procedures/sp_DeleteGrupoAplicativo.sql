/*
-000- a. FJS 12.05.2009 - Nuevo SP para manejo de Aplicativos por Grupo ejecutor
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteGrupoAplicativo'
go

if exists (select * from sysobjects where name = 'sp_DeleteGrupoAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteGrupoAplicativo
go

create procedure dbo.sp_DeleteGrupoAplicativo
	@cod_grupo	char(8)=null,
	@app_id		char(30)=null
as
	delete from
		GesPet..GrupoAplicativo
	where
		cod_grupo = @cod_grupo and
		app_id = @app_id
go

grant execute on dbo.sp_DeleteGrupoAplicativo to GesPetUsr
go

print 'Actualización realizada.'
