/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 20.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHEDT011'
go

if exists (select * from sysobjects where name = 'sp_InsertHEDT011' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHEDT011
go

create procedure dbo.sp_InsertHEDT011
	@tipo_id	char(1)=null,
	@tipo_nom	char(50)=null
as
	insert into GesPet.dbo.HEDT011 (
		tipo_id,
		tipo_nom)
	values (
		@tipo_id,
		@tipo_nom)
go

grant execute on dbo.sp_InsertHEDT011 to GesPetUsr
go

print 'Actualización realizada.'
