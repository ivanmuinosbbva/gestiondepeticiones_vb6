/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'sp_DelegaRecurso'
go

if exists (select * from sysobjects where name = 'sp_DelegaRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_DelegaRecurso
go

create procedure dbo.sp_DelegaRecurso 
    @cod_recurso	char(10), 
    @dlg_recurso    char(10)=' ', 
    @dlg_desde      smalldatetime=null, 
    @dlg_hasta      smalldatetime=null 

as 
	if ((RTRIM(@dlg_recurso) is not null and RTRIM(@dlg_recurso)<>'')) 
	begin 
	if exists (select cod_recurso from GesPet..Recurso where 
		  RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
		  RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
		  convert(char(8),@dlg_desde,112) <= convert(char(8),dlg_desde,112) and 
		  convert(char(8),@dlg_hasta,112) >= convert(char(8),dlg_hasta,112) 
		  ) 
		  begin 
			raiserror 30011 'Superposicion de Delegación'   
			select 30011 as ErrCode , 'Superposicion de Delegación' as ErrDesc   
			return (30011)   
		  end 
	if exists (select cod_recurso from GesPet..Recurso where 
		  RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
		  RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
		  convert(char(8),@dlg_desde,112) >= convert(char(8),dlg_desde,112) and 
		  convert(char(8),@dlg_desde,112) <= convert(char(8),dlg_hasta,112) 
		  ) 
		  begin 
			raiserror 30012 'Superposicion de Delegación'   
			select 30012 as ErrCode , 'Superposicion de Delegación' as ErrDesc   
			return (30012)   
		  end 
	if exists (select cod_recurso from GesPet..Recurso where 
		  RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
		  RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
		  convert(char(8),@dlg_hasta,112) >= convert(char(8),dlg_desde,112) and 
		  convert(char(8),@dlg_hasta,112) <= convert(char(8),dlg_hasta,112) 
		  ) 
		  begin 
			raiserror 30013 'Superposicion de Delegación'   
			select 30013 as ErrCode , 'Superposicion de Delegación' as ErrDesc   
			return (30013)   
		  end 
	end 
    update GesPet..Recurso 
    set dlg_recurso     = @dlg_recurso, 
		dlg_desde   = @dlg_desde, 
		dlg_hasta   = @dlg_hasta, 
		audit_user = SUSER_NAME(),   
		audit_date = getdate()   
    where RTRIM(cod_recurso) = RTRIM(@cod_recurso)
	
	return(0)
go

grant execute on dbo.sp_DelegaRecurso to GesPetUsr 
go


