use GesPet
go

print 'Creando/actualizando SP: sp_DeleteMotivo'
go

if exists (select * from sysobjects where name = 'sp_DeleteMotivo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteMotivo
go

create procedure dbo.sp_DeleteMotivo
	@cod_motivo		int=null
as 
	delete
	from 
		GesPet.dbo.Motivos
	where 
		cod_motivo = @cod_motivo

return(0) 
go

grant execute on dbo.sp_DeleteMotivo to GesPetUsr
go

print 'Actualización realizada.'
