/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 28.10.2010 - SPs para el mantenimiento de la tabla Estados
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateEstados'
go

if exists (select * from sysobjects where name = 'sp_UpdateEstados' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEstados
go

create procedure dbo.sp_UpdateEstados
	@cod_estado		char(6),
	@nom_estado		char(30),
	@flg_rnkup		char(2),
	@flg_herdlt1	char(1),
	@flg_petici		char(1),
	@flg_secgru		char(1),
	@IGM_hab		char(1),
	@terminal		char(1)='N',
	@imagen			int=5,
	@default_est	char(6)=''
as
	update 
		GesPet.dbo.Estados
	set
		nom_estado  = @nom_estado,
		flg_rnkup   = @flg_rnkup,
		flg_herdlt1 = @flg_herdlt1,
		flg_petici  = @flg_petici,
		flg_secgru  = @flg_secgru,
		IGM_hab		= @IGM_hab,
		terminal	= @terminal,
		imagen		= @imagen,
		default_est	= @default_est
	where 
		cod_estado = @cod_estado
	return(0)
go

grant execute on dbo.sp_UpdateEstados to GesPetUsr
go

print 'Actualización realizada.'
go
