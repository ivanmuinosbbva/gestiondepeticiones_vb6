/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetEstadosIDM'
go

if exists (select * from sysobjects where name = 'sp_GetEstadosIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEstadosIDM
go

create procedure dbo.sp_GetEstadosIDM
	@cod_estado	char(6)=null,
	@tipo		char(1)=null
as
	select 
		p.cod_estado,
		p.nom_estado,
		p.flg_rnkup,
		p.flg_herdlt1,
		p.flg_petici,
		p.flg_secgru
	from 
		GesPet..EstadosIDM p
	where
		(@cod_estado is null or p.cod_estado = @cod_estado) and
		(@tipo is null or p.flg_petici = @tipo)
go

grant execute on dbo.sp_GetEstadosIDM to GesPetUsr
go

print 'Actualización realizada.'
go
