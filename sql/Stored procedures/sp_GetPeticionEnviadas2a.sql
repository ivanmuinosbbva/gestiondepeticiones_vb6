/*
-000- a. FJS 05.05.2009 - Nuevo SP para devolver todos los registros de peticiones en estado "En ejecución"
-001- a. FJS 27.05.2009 - Se modifica para solo recibir los tres parámetros claves.
-002- a. FJS 15.09.2009 - Se modifica para control.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionEnviadas2a'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionEnviadas2a' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionEnviadas2a
go

create procedure dbo.sp_GetPeticionEnviadas2a
	@pet_nrointerno	int=null,
	@pet_sector		char(8)=null,
	@pet_grupo		char(8)=null
as
	select distinct
		a.pet_nrointerno,
		a.pet_sector,
		a.pet_grupo,
		a.pet_record,
		a.pet_usrid,
		a.pet_date,
		a.pet_done
	from 
		GesPet..PeticionEnviadas2 a left join 
		GesPet..PeticionConf b on a.pet_nrointerno = b.pet_nrointerno
	where
		(a.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(a.pet_sector = @pet_sector or @pet_sector is null) and
		(a.pet_grupo = @pet_grupo or @pet_grupo is null) and
		(b.ok_tipo not in ('TESP', 'TEST') or b.ok_tipo is null)
go

grant execute on dbo.sp_GetPeticionEnviadas2a to GesPetUsr
go

print 'Actualización realizada.'
