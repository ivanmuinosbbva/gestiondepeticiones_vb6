/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

-001- a. FJS 23.03.2010 - Nuevo SP para devolver un solo campo tipo Text con el contenido completo de un campo de texto de una petici�n.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionMemo2'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionMemo2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionMemo2
go

create procedure dbo.sp_GetPeticionMemo2
    @pet_nrointerno		int,
	@mem_campo			char(10)
as
	-- Variables locales (para el cursor)
	declare @c_pet_nrointerno int, @c_mem_campo	char(10), @c_mem_secuencia smallint
	declare @texto	char(255)

	create table #Peticiones (
		pet_nrointerno		int	null,
		mem_campo			char(10)	null,
		mem_secuencia		smallint	null,
		mem_texto			char(255)	null)

	insert into #Peticiones
		select
			pet_nrointerno,
			mem_campo,
			mem_secuencia,
			mem_texto
		from GesPet..PeticionMemo
		where pet_nrointerno = @pet_nrointerno and mem_campo = @mem_campo

	declare curNumeros cursor for 
		select pet_nrointerno, mem_campo, mem_secuencia
			from #Peticiones 
			where pet_nrointerno = @pet_nrointerno and mem_campo = @mem_campo
			group by pet_nrointerno, mem_campo, mem_secuencia
		for read only 

		open curNumeros
		fetch curNumeros into @c_pet_nrointerno, @c_mem_campo, @c_mem_secuencia
		while @@sqlstatus = 0 
			begin 
				select @texto = (select isnull(ltrim(rtrim(mem_texto)), '') from GesPet..PeticionMemo where pet_nrointerno = @c_pet_nrointerno and mem_campo = @c_mem_campo and mem_secuencia = @c_mem_secuencia)
				if char_length(rtrim(@texto)) is null or char_length(rtrim(@texto)) > 0
					begin
						while charindex(char(13), @texto) > 0
							select @texto = stuff(@texto, charindex(char(13), @texto), 1, space(1))
						while charindex(char(10), @texto) > 0
							select @texto = stuff(@texto, charindex(char(10), @texto), 1, space(1))
						while charindex(char(9), @texto) > 0
							select @texto = stuff(@texto, charindex(char(9), @texto), 1, space(1))
						while charindex(char(7), @texto) > 0
							select @texto = stuff(@texto, charindex(char(7), @texto), 1, space(1))
						-- Por �ltimo, comprueba si la cadena comienza con un gui�n que en realidad es un signo menos 
						-- (lo que trae problemas con el Microsoft Excel) Se agregan, adem�s. las transformaciones para
						-- +, *, / e =
						if left(@texto, 1) = char(45)
							select @texto = stuff(@texto, 1, 1, char(173))
						if left(@texto, 1) = char(43)
							select @texto = stuff(@texto, 1, 1, char(173))
						if left(@texto, 1) = char(42)
							select @texto = stuff(@texto, 1, 1, char(173))
						if left(@texto, 1) = char(47)
							select @texto = stuff(@texto, 1, 1, char(173))
						if left(@texto, 1) = char(61)
							select @texto = stuff(@texto, 1, 1, char(173))

						update #Peticiones 
							set mem_texto = RTRIM(@texto) 
							where pet_nrointerno = @c_pet_nrointerno and mem_campo = @c_mem_campo and mem_secuencia = @c_mem_secuencia
						fetch curNumeros into @c_pet_nrointerno, @c_mem_campo, @c_mem_secuencia
					end
			end 
		close curNumeros
	deallocate cursor curNumeros 
	
	select
		pet_nrointerno,
		mem_campo,
		mem_secuencia,
		mem_texto
	from #Peticiones
	where 
		pet_nrointerno = @pet_nrointerno and
		mem_campo = @mem_campo
	
	drop table  #Peticiones
go

grant execute on dbo.sp_GetPeticionMemo2 to GesPetUsr 
go

print 'Actualizaci�n realizada.'