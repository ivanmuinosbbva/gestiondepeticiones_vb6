/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateRecursoPerfilAudit'
go

if exists (select * from sysobjects where name = 'sp_UpdateRecursoPerfilAudit' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRecursoPerfilAudit
go

create procedure dbo.sp_UpdateRecursoPerfilAudit
	@cod_recurso	char(10),
	@cod_perfil		char(4),
	@cod_ope		int,
	@cod_nivel		char(4),
	@cod_area		char(8),
	@auditfch		smalldatetime,
	@auditusr		char(10)
as
	update 
		GesPet.dbo.RecursoPerfilAudit
	set
		cod_nivel = @cod_nivel,
		cod_area = @cod_area,
		auditfch = @auditfch,
		auditusr = @auditusr
	where 
		cod_recurso = @cod_recurso and
		cod_perfil = @cod_perfil and
		cod_ope = @cod_ope
go

grant execute on dbo.sp_UpdateRecursoPerfilAudit to GesPetUsr
go

print 'Actualización realizada.'
go
