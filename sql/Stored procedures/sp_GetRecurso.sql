/*
-001- a. FJS 13.01.2009 - Optimizaci�n de SP para mejorar la performance del aplicativo.
-002- a. FJS 13.01.2009 - Se agrega la devoluci�n de la fecha de ultima actualizaci�n de la contrase�a de seguridad.
-003- a. FJS 11.03.2009 - Se adecua la condici�n para devolver seg�n el estado del recurso solicitado.
-004- a. FJS 04.03.2013 - Se restringe para solo visualizar recurso (exceptuando los recursos - f�brica).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetRecurso'
go

if exists (select * from sysobjects where name = 'sp_GetRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecurso
go

create procedure dbo.sp_GetRecurso 
	@cod_query		char(10) = null, 
	@modo_query		char(2)  = 'R', 
	@flg_estado		char(1)  = null
/*

Variable @modo_query :
======================

- 'R'  Busca el codigo de recurso = @cod_query o todos
- 'DI' Busca los recursos que pertenecen a la direccion = @cod_query
- 'GE' Busca los recursos que pertenecen a la gerencia = @cod_query
- 'GR' Busca los recursos que pertenecen al sector = @cod_query
- 'SU' Busca los recursos que pertenecen al grupo = @cod_query

*/ 
as 
	declare @ret_status     int 
	
	/*
	if RTRIM(@flg_estado) = 'F'
		begin
			select @flg_estado = 'A'

			select 
				Rc.cod_recurso, 
				Rc.nom_recurso, 
				Rc.vinculo, 
				Rc.cod_gerencia, 
				Rc.cod_direccion, 
				Rc.cod_sector, 
				Rc.cod_grupo, 
				Rc.flg_cargoarea, 
				Rc.estado_recurso, 
				Rc.horasdiarias, 
				Rc.observaciones, 
				Rc.dlg_recurso, 
				Rc.dlg_desde, 
				Rc.dlg_hasta,
				Rc.email,
				Rc.euser
			from 
				GesPet..Recurso Rc
			where
				((@modo_query = 'DI' and Rc.cod_direccion = @cod_query) or 
				 (@modo_query = 'GE' and Rc.cod_gerencia = @cod_query) or 
				 (@modo_query = 'GR' and Rc.cod_sector = @cod_query) or 
				 (@modo_query = 'SU' and Rc.cod_grupo = @cod_query)) and
				(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado)) and
				Rc.vinculo = 'F'
				/*
				(@cod_query is null or RTRIM(@cod_query) is null or RTRIM(@cod_query)='') and 
				(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado)) and
				Rc.vinculo = 'F'
				*/
			return(0)
		end
	*/
	
	-- MODO: busca TODOS los recursos
	if @modo_query is null or RTRIM(@modo_query) is null or RTRIM(@modo_query)='' or RTRIM(@modo_query) = 'R' 
		-- MODO: busca TODOS los recursos
		if @cod_query is null or RTRIM(@cod_query) is null or RTRIM(@cod_query)=''		-- add -001- a.
			begin 
				select 
					Rc.cod_recurso, 
					Rc.nom_recurso, 
					Rc.vinculo, 
					Rc.cod_gerencia, 
					Rc.cod_direccion, 
					Rc.cod_sector, 
					Rc.cod_grupo, 
					Rc.flg_cargoarea, 
					Rc.estado_recurso, 
					Rc.horasdiarias, 
					Rc.observaciones, 
					Rc.dlg_recurso, 
					Rc.dlg_desde, 
					Rc.dlg_hasta,
					Rc.email,
					Rc.euser
				from 
					GesPet..Recurso Rc
				where 
					(@cod_query is null or RTRIM(@cod_query) is null or RTRIM(@cod_query)='') and 
					(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado))
					--and Rc.vinculo <> 'F'
				order by 
					Rc.nom_recurso,
					Rc.estado_recurso	-- add -001- a.
				
				return(0) 
			end 
		--{ add -001- a.
		else
			-- MODO: busca UN recurso en particular
			begin 
				select 
					Rc.cod_recurso, 
					Rc.nom_recurso, 
					Rc.vinculo, 
					Rc.cod_gerencia, 
					Rc.cod_direccion, 
					Rc.cod_sector, 
					Rc.cod_grupo, 
					Rc.flg_cargoarea, 
					Rc.estado_recurso, 
					Rc.horasdiarias, 
					Rc.observaciones, 
					Rc.dlg_recurso, 
					Rc.dlg_desde, 
					Rc.dlg_hasta,
					Rc.email,
					Rc.euser/*,
					Rc.fe_seguridad		-- add -002- a. Solo cuando pido un recurso espec�fico.
					*/
				from 
					GesPet..Recurso Rc
				where 
					RTRIM(Rc.cod_recurso) = RTRIM(@cod_query) and 
					(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado))
				order by 
					Rc.cod_recurso,
					Rc.estado_recurso
				
				return(0) 
			end 
		--}
	
	-- MODO: busca recursos de una Direcci�n espec�fica
	if @modo_query = 'DI' 
		begin 
			select 
				Rc.cod_recurso, 
				Rc.nom_recurso, 
				Rc.vinculo, 
				Rc.cod_gerencia, 
				Rc.cod_direccion, 
				Rc.cod_sector, 
				Rc.cod_grupo, 
				Rc.flg_cargoarea, 
				Rc.estado_recurso, 
				Rc.horasdiarias, 
				Rc.observaciones, 
				Rc.dlg_recurso, 
				Rc.dlg_desde, 
				Rc.dlg_hasta,
				Rc.email,
				Rc.euser 
			from 
				GesPet..Recurso Rc
			where 
				RTRIM(Rc.cod_direccion) = RTRIM(@cod_query) and 
				(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado)) 
				--and Rc.vinculo <> 'F' 
			order by 
				Rc.cod_direccion,	-- add -001- a.
				Rc.nom_recurso,
				Rc.estado_recurso	-- add -001- a.
			return(0) 
		end 
	-- MODO: busca recursos de una Gerencia espec�fica
	if @modo_query = 'GE' 
		begin 
			select 
				Rc.cod_recurso, 
				Rc.nom_recurso, 
				Rc.vinculo, 
				Rc.cod_gerencia, 
				Rc.cod_direccion, 
				Rc.cod_sector, 
				Rc.cod_grupo, 
				Rc.flg_cargoarea, 
				Rc.estado_recurso, 
				Rc.horasdiarias, 
				Rc.observaciones, 
				Rc.dlg_recurso, 
				Rc.dlg_desde, 
				Rc.dlg_hasta,
				Rc.email,
				Rc.euser 
			from 
				GesPet..Recurso Rc
			where 
				RTRIM(Rc.cod_gerencia) = RTRIM(@cod_query) and 
				(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado)) 
				--and Rc.vinculo <> 'F'
			order by 
				Rc.cod_gerencia,	-- add -001- a.
				Rc.nom_recurso,
				Rc.estado_recurso	-- add -001- a.
			return(0) 
		end 
	-- MODO: busca recursos de un Sector espec�fico
	if @modo_query = 'GR' 
		begin 
			select 
				Rc.cod_recurso, 
				Rc.nom_recurso, 
				Rc.vinculo, 
				Rc.cod_gerencia, 
				Rc.cod_direccion, 
				Rc.cod_sector, 
				Rc.cod_grupo, 
				Rc.flg_cargoarea, 
				Rc.estado_recurso, 
				Rc.horasdiarias, 
				Rc.observaciones, 
				Rc.dlg_recurso, 
				Rc.dlg_desde, 
				Rc.dlg_hasta,
				Rc.email,
				Rc.euser 
			from 
				GesPet..Recurso Rc
			where 
				RTRIM(Rc.cod_sector) = RTRIM(@cod_query) and 
				(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado)) 
				--and Rc.vinculo <> 'F'
			order by 
				Rc.cod_sector,		-- add -001- a.
				Rc.nom_recurso,
				Rc.estado_recurso	-- add -001- a.
			return(0)
		end 
	-- MODO: busca recursos de un Grupo espec�fico
	if @modo_query = 'SU' 
		begin 
			select 
				Rc.cod_recurso, 
				Rc.nom_recurso, 
				Rc.vinculo, 
				Rc.cod_gerencia, 
				Rc.cod_direccion, 
				Rc.cod_sector, 
				Rc.cod_grupo, 
				Rc.flg_cargoarea, 
				Rc.estado_recurso, 
				Rc.horasdiarias, 
				Rc.observaciones, 
				Rc.dlg_recurso, 
				Rc.dlg_desde, 
				Rc.dlg_hasta,
				Rc.email,
				Rc.euser
			from 
				GesPet..Recurso Rc
			where 
				RTRIM(Rc.cod_grupo) = RTRIM(@cod_query) and 
				(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(Rc.estado_recurso) = RTRIM(@flg_estado)) 
				--and Rc.vinculo <> 'F'
			order by 
				Rc.cod_grupo,		-- add -001- a.
				Rc.nom_recurso,
				Rc.estado_recurso	-- add -001- a.
			return(0) 
		end 
return(0) 
go

sp_procxmode 'sp_GetRecurso', anymode
go

grant execute on dbo.sp_GetRecurso to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go