/*
execute sp_rptProyecto00
	@prj_nrodesde		=0,
	@prj_nrohasta		=0,
	@prj_titulo		='NULL',
	@cod_bpar		='NULL',
	@prj_cod_orientacion	='NULL',
	@prj_importancia_cod	='NULL',
	@prj_cod_estado		='NULL',
	@prj_nivel		='NULL',
	@prj_area		='NULL',
	@prj_desde_ini_plan	='NULL',
	@prj_hasta_ini_plan	='NULL',
	@prj_desde_fin_plan	='NULL',
	@prj_hasta_fin_plan	='NULL',
	@prj_desde_ini_real	='NULL',
	@prj_hasta_ini_real	='NULL',
	@prj_desde_fin_real	='NULL',
	@prj_hasta_fin_real	='NULL',
	@prj_cant_planif	=0
*/
use GesPet
go
print 'sp_rptProyecto00'
go
if exists (select * from sysobjects where name = 'sp_rptProyecto00' and sysstat & 7 = 4)
drop procedure dbo.sp_rptProyecto00
go
create procedure dbo.sp_rptProyecto00
	@prj_nrodesde		int=0,
	@prj_nrohasta		int=0,
	@prj_titulo		varchar(50)='NULL',
	@cod_bpar		varchar(10)='NULL',
	@prj_cod_orientacion	varchar(4)='NULL',
	@prj_importancia_cod	varchar(4)='NULL',
	@prj_cod_estado		varchar(250)='NULL',
	@prj_nivel		varchar(4)='NULL',
	@prj_area		varchar(8)='NULL',
	@prj_desde_ini_plan	varchar(8)='NULL',
	@prj_hasta_ini_plan	varchar(8)='NULL',
	@prj_desde_fin_plan	varchar(8)='NULL',
	@prj_hasta_fin_plan	varchar(8)='NULL',
	@prj_desde_ini_real	varchar(8)='NULL',
	@prj_hasta_ini_real	varchar(8)='NULL',
	@prj_desde_fin_real	varchar(8)='NULL',
	@prj_hasta_fin_real	varchar(8)='NULL',
	@prj_cant_planif	int=0,
	@cod_tipoprj	varchar(6)='NULL'
as

declare @secuencia int
declare @ret_status     int

if @cod_bpar='TEST'
begin
	select prj_nrointerno=123456,
		titulo=space(100),
		corp_local='C',
		cod_orientacion='D',
		nom_orientacion=space(30),
		importancia_cod='  ',
		nom_importancia=space(30),
		importancia_prf=space(4),
		importancia_usr=space(10),
		fec_requerida=getdate(),
		cod_direccion='XXXXXXXX',
		nom_direccion=space(40),
		cod_gerencia='XXXXXXXX',
		nom_gerencia=space(40),
		cod_sector='XXXXXXXX',
		nom_sector=space(40),
		cod_solicitante=space(10),
		nom_solicitante=space(40),
		cod_bpar=space(10),
		nom_bpar=space(40),
		prj_costo='A',
		fe_ini_esti=getdate(),
		fe_fin_esti=getdate(),
		cod_estado=space(6),
		nom_estado=space(30),
		fe_estado=getdate(),
		fe_ini_plan=getdate(),
		fe_fin_plan=getdate(),
		fe_ini_real=getdate(),
		fe_fin_real=getdate(),
		fe_fec_plan=getdate(),
		fe_ini_orig=getdate(),
		fe_fin_orig=getdate(),
		fe_fec_orig=getdate(),
		cod_usualta=space(10),
		fec_alta=getdate(),
		horaspresup=9876,
		cant_planif=10,
		ult_accion=space(6),
		cod_tipoprj="XXXX",
		nom_tipoprj=space(40)
return(0)
end


	select PRJ.prj_nrointerno,
		PRJ.titulo,
		PRJ.cod_tipoprj,
		ord_tipoprj = (select TIPRJ.secuencia from TipoPrj TIPRJ where RTRIM(TIPRJ.cod_tipoprj)=RTRIM(PRJ.cod_tipoprj)),
		nom_tipoprj = (select TIPRJ.nom_tipoprj from TipoPrj TIPRJ where RTRIM(TIPRJ.cod_tipoprj)=RTRIM(PRJ.cod_tipoprj)),
		PRJ.corp_local,
		PRJ.cod_orientacion,
		nom_orientacion = isnull((select Ot.nom_orientacion from Orientacion Ot where RTRIM(Ot.cod_orientacion)=RTRIM(PRJ.cod_orientacion)),''),
		PRJ.importancia_cod,
		nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PRJ.importancia_cod)),''),
		PRJ.importancia_prf,
		PRJ.importancia_usr,
		PRJ.fec_requerida,
		PRJ.cod_direccion,
		nom_direccion = (select DIRPRJ.nom_direccion from Direccion DIRPRJ where RTRIM(DIRPRJ.cod_direccion)=RTRIM(PRJ.cod_direccion)),
		PRJ.cod_gerencia,
		nom_gerencia = (select GERPRJ.nom_gerencia from Gerencia GERPRJ where RTRIM(GERPRJ.cod_gerencia)=RTRIM(PRJ.cod_gerencia)),
		PRJ.cod_sector,
		nom_sector = (select SECPRJ.nom_sector from Sector SECPRJ where RTRIM(SECPRJ.cod_sector)=RTRIM(PRJ.cod_sector)),
		PRJ.cod_solicitante,
		nom_solicitante = (select RC.nom_recurso from Recurso RC where RTRIM(RC.cod_recurso)=RTRIM(PRJ.cod_solicitante)),
		PRJ.cod_bpar,
		nom_bpar = (select REC.nom_recurso from Recurso REC where RTRIM(REC.cod_recurso)=RTRIM(PRJ.cod_bpar)),
		PRJ.prj_costo,
		PRJ.fe_ini_esti,
		PRJ.fe_fin_esti,
		PRJ.cod_estado,
		nom_estado = (select ESTPRJ.nom_estado from Estados ESTPRJ where RTRIM(ESTPRJ.cod_estado)=RTRIM(PRJ.cod_estado)),
		PRJ.fe_estado,
		PRJ.fe_ini_plan,
		PRJ.fe_fin_plan,
		PRJ.fe_ini_real,
		PRJ.fe_fin_real,
		PRJ.fe_fec_plan,
		PRJ.fe_ini_orig,
		PRJ.fe_fin_orig,
		PRJ.fe_fec_orig,
		PRJ.cod_usualta,
		PRJ.fec_alta,
		PRJ.horaspresup,
		PRJ.cant_planif,
		PRJ.ult_accion

	from
		Proyecto PRJ
where	
	(@prj_nrodesde = 0 or PRJ.prj_nrointerno >= @prj_nrodesde) and
	(@prj_nrohasta = 0 or PRJ.prj_nrointerno <= @prj_nrohasta) and
	(@prj_titulo is null or RTRIM(@prj_titulo)='NULL' or charindex(UPPER(RTRIM(@prj_titulo)),UPPER(RTRIM(PRJ.titulo)))>0) and
	(@cod_bpar is null or RTRIM(@cod_bpar)='NULL' or RTRIM(PRJ.cod_bpar)=RTRIM(@cod_bpar)) and
	(@prj_cod_estado is null or RTRIM(@prj_cod_estado)='NULL' or charindex(RTRIM(PRJ.cod_estado),RTRIM(@prj_cod_estado))>0) and
	(@prj_importancia_cod is null or RTRIM(@prj_importancia_cod)='NULL' or RTRIM(PRJ.importancia_cod)=RTRIM(@prj_importancia_cod)) and
	(@prj_cod_orientacion is null or RTRIM(@prj_cod_orientacion)='NULL' or RTRIM(PRJ.cod_orientacion)=RTRIM(@prj_cod_orientacion)) and
	((@prj_nivel is null or RTRIM(@prj_nivel)='NULL') or
	(@prj_nivel='DIRE' and RTRIM(PRJ.cod_direccion)=RTRIM(@prj_area)) or
	(@prj_nivel='GERE' and RTRIM(PRJ.cod_gerencia)=RTRIM(@prj_area)) or
	(@prj_nivel='SECT' and RTRIM(PRJ.cod_sector)=RTRIM(@prj_area))) and
	(RTRIM(@prj_desde_ini_plan)='NULL' or convert(char(8),PRJ.fe_ini_plan,112) >= @prj_desde_ini_plan) and
	(RTRIM(@prj_hasta_ini_plan)='NULL' or convert(char(8),PRJ.fe_ini_plan,112) <= @prj_hasta_ini_plan) and
	(RTRIM(@prj_desde_fin_plan)='NULL' or convert(char(8),PRJ.fe_fin_plan,112) >= @prj_desde_fin_plan) and
	(RTRIM(@prj_hasta_fin_plan)='NULL' or convert(char(8),PRJ.fe_fin_plan,112) <= @prj_hasta_fin_plan) and
	(RTRIM(@prj_desde_ini_real)='NULL' or convert(char(8),PRJ.fe_ini_real,112) >= @prj_desde_ini_real) and
	(RTRIM(@prj_hasta_ini_real)='NULL' or convert(char(8),PRJ.fe_ini_real,112) <= @prj_hasta_ini_real) and
	(RTRIM(@prj_desde_fin_real)='NULL' or convert(char(8),PRJ.fe_fin_real,112) >= @prj_desde_fin_real) and
	(RTRIM(@prj_hasta_fin_real)='NULL' or convert(char(8),PRJ.fe_fin_real,112) <= @prj_hasta_fin_real) and
	(@prj_cant_planif = 0 or PRJ.cant_planif  >= @prj_cant_planif) and 
	(@cod_tipoprj is null or RTRIM(@cod_tipoprj)='NULL' or charindex(RTRIM(PRJ.cod_tipoprj),RTRIM(@cod_tipoprj))>0)

order by ord_tipoprj,prj_nrointerno

return (0)
go

grant execute on dbo.sp_rptProyecto00 to GesPetUsr
go
