/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 20.10.2009 - 
-001- a. FJS 12.11.2009 - Se agrega la cl�usula ORDER BY.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT012'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT012' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT012
go

create procedure dbo.sp_GetHEDT012
	@tipo_id		char(1)=null,
	@subtipo_id		char(1)=null,
	@subtipo_nom	char(50)=null,
	@rutina			char(8)=null
as
	select 
		a.tipo_id,
		a.subtipo_id,
		a.subtipo_nom,
		a.rutina
	from 
		GesPet.dbo.HEDT012 a
	where
		(a.tipo_id = @tipo_id or @tipo_id is null) and
		(a.subtipo_id = @subtipo_id or @subtipo_id is null) and
		(a.subtipo_nom = @subtipo_nom or @subtipo_nom is null) and
		(a.rutina = @rutina or @rutina is null)
	order by 
		a.tipo_id,
		a.subtipo_nom
go

grant execute on dbo.sp_GetHEDT012 to GesPetUsr
go

print 'Actualizaci�n realizada.'
