/*
-000- a. FJS 05.05.2009 - Nuevo SP para agregar peticiones en estado "En ejecuci�n"
-001- a. FJS 21.09.2009 - Modificaci�n: hay que tener en cuenta que no puede existir una petici�n como "V�lida" (PeticionEnviadas2) y como "En ejecuci�n" 
						  (PeticionEnviadas/PeticionChangeMan) al mismo tiempo. Esto es necesario para controlar las que se encuentran "En ejecuci�n" porque
						  cuando son referenciadas, no pueden tener conformes de usuario que las hayan habilitado para pasajes a producci�n. Se supone que los
						  usuarios que validan o referencias contra la tabla "En ejecuci�n" lo hacen a fin de solicitar datos para pruebas que a�n no se han probado
						  o desarrollado, por lo que no pueden haber pasado a Producci�n.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionEnviadas2'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionEnviadas2' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionEnviadas2
go

create procedure dbo.sp_InsertPeticionEnviadas2
	@pet_nrointerno	int=null,
	@pet_sector		char(8)=null,
	@pet_grupo		char(8)=null
as

declare @pet_record		char(80)
declare @hacer			char(1)

select @pet_record = ''
select @hacer = 'S'

if not exists (select pet_nrointerno
               from GesPet..PeticionEnviadas2
               where pet_nrointerno = @pet_nrointerno and pet_sector = @pet_sector and pet_grupo = @pet_grupo)
	begin
		--{ add -002- a. Valido su situaci�n como petici�n v�lida (primero en el hist�rico y luego en la bandeja diaria)
		if exists (select pet_nrointerno
					from GesPet..PeticionEnviadas
					where pet_nrointerno = @pet_nrointerno)
			select @hacer = 'N'
		
		if exists (select pet_nrointerno
					from GesPet..PeticionChangeMan
					where pet_nrointerno = @pet_nrointerno)
			select @hacer = 'N'
		
		if @hacer = 'S'
			begin
		--}
				if @pet_sector is null
					select @pet_sector = ''
				if @pet_grupo is null
					select @pet_grupo = ''

				execute sp_FormatEnviadas @pet_nrointerno, @pet_sector, @pet_grupo, @pet_record output
				
				if datalength(@pet_record) < 80
					select @pet_record = @pet_record + space(datalength(@pet_record) - 80)

				insert into GesPet.dbo.PeticionEnviadas2 (
					pet_nrointerno,
					pet_sector,
					pet_grupo,
					pet_record,
					pet_usrid,
					pet_date,
					pet_done)
				values (
					@pet_nrointerno,
					@pet_sector,
					@pet_grupo,
					@pet_record,
					SUSER_NAME(),
					getdate(),
					null)
		--{ add -002- a.
			end
		--}
	end
else
	--{ add -002- a. Valido su situaci�n como petici�n v�lida (primero en el hist�rico y luego en la bandeja diaria)
	if exists (select pet_nrointerno
				from GesPet..PeticionEnviadas
				where pet_nrointerno = @pet_nrointerno)
		select @hacer = 'N'
	
	if exists (select pet_nrointerno
				from GesPet..PeticionChangeMan
				where pet_nrointerno = @pet_nrointerno)
		select @hacer = 'N'
	
	if @hacer = 'N'
		-- Si ya existe en alguna de las tablas de peticiones v�lidas, entonces no hay que actualizar,
		-- hay que eliminar el registro.
		begin
			delete 
			from GesPet..PeticionEnviadas2 
			where pet_nrointerno = @pet_nrointerno
		end
	else
	--}
		begin
			execute sp_FormatEnviadas @pet_nrointerno, @pet_sector, @pet_grupo, @pet_record output

			if datalength(@pet_record) < 80
				select @pet_record = @pet_record + space(datalength(@pet_record) - 80)

			update   
				GesPet..PeticionEnviadas2
			set   
				pet_record = @pet_record,
				pet_usrid  = SUSER_NAME(),
				pet_date   = getdate()
			where  
				pet_nrointerno = @pet_nrointerno and   
				pet_sector     = @pet_sector and   
				pet_grupo      = @pet_grupo 
		end
go

grant execute on dbo.sp_InsertPeticionEnviadas2 to GesPetUsr
go

print 'Actualizaci�n realizada.'
