/*  Código generado automáticamente por el programa ~ SQL Contructor v1.1.1 by Fernando J. Spitz ~ */

/*
-000- a. FJS 18.09.2009 - Para manejo detallado de actualizaciones a solicitudes
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSolicitudesField'
go

if exists (select * from sysobjects where name = 'sp_UpdateSolicitudesField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSolicitudesField
go

create procedure dbo.sp_UpdateSolicitudesField
	@sol_nroasignado	int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if rtrim(@campo)='SOL_NROASIGNADO'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_nroasignado = @valornum
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_TIPO'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_tipo = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_MASK'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_mask = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_FECHA'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_fecha = @valordate
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_RECURSO'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_recurso = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='PET_NROINTERNO'
	update 
		GesPet.dbo.Solicitudes
	set
		pet_nrointerno = @valornum
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_RESP_EJEC'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_resp_ejec = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_RESP_SECT'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_resp_sect = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_SEGURIDAD'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_seguridad = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='JUS_CODIGO'
	update 
		GesPet.dbo.Solicitudes
	set
		jus_codigo = @valornum
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_TEXTO'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_texto = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_FE_AUTO1'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_fe_auto1 = @valordate
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_FE_AUTO2'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_fe_auto2 = @valordate
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_FE_AUTO3'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_fe_auto3 = @valordate
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_FILE_PROD'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_file_prod = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_FILE_DESA'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_file_desa = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_FILE_OUT'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_file_out = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_LIB_SYSIN'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_lib_Sysin = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_MEM_SYSIN'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_mem_Sysin = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_JOIN'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_join = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_NROTABLA'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_nrotabla = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='FE_FORMATO'
	update 
		GesPet.dbo.Solicitudes
	set
		fe_formato = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_ESTADO'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_estado = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_TRNM_FE'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_trnm_fe = @valordate
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_TRNM_USR'
	update 
		GesPet.dbo.Solicitudes
	set
		sol_trnm_usr = @valortxt
	where 
		(sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

if rtrim(@campo)='SOL_PROD_CPYBBL'
	update GesPet.dbo.Solicitudes
	set sol_prod_cpybbl = @valortxt
	where (sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)
if rtrim(@campo)='SOL_PROD_CPYNOM'
	update GesPet.dbo.Solicitudes
	set sol_prod_cpynom = @valortxt
	where (sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)
if rtrim(@campo)='DSN_ID'
	update GesPet.dbo.Solicitudes
	set dsn_id = @valortxt
	where (sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null)

go

grant execute on dbo.sp_UpdateSolicitudesField to GesPetUsr
go

print 'Actualización realizada.'
go
