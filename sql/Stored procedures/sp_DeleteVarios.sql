/*
-000- a. FJS 23.01.2009 - Nuevo SP para eliminar datos de la tabla Varios.
-001- a. FJS 25.04.2011 - Se quita el default del parámetro de entrada, para evitar problemas por ejecución erronea.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteVarios'
go

if exists (select * from sysobjects where name = 'sp_DeleteVarios' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteVarios
go

create procedure dbo.sp_DeleteVarios
	@var_codigo		varchar(8)
as 
	delete from GesPet..Varios
	where var_codigo = @var_codigo
go

grant execute on dbo.sp_DeleteVarios to GesPetUsr
go

print 'Actualización realizada.'
