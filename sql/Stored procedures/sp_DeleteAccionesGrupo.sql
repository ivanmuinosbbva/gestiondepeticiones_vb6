/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 12.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteAccionesGrupo'
go

if exists (select * from sysobjects where name = 'sp_DeleteAccionesGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteAccionesGrupo
go

create procedure dbo.sp_DeleteAccionesGrupo
	@accionGrupoId	int
as
	delete from
		GesPet.dbo.AccionesGrupo
	where
		accionGrupoId = @accionGrupoId
go

grant execute on dbo.sp_DeleteAccionesGrupo to GesPetUsr
go

print 'Actualización realizada.'
go
