use GesPet
go

print 'sp_ProximoNumero'
go

if exists (select * from sysobjects where name = 'sp_ProximoNumero' and sysstat & 7 = 4)
	drop procedure dbo.sp_ProximoNumero
go

create procedure dbo.sp_ProximoNumero 
             @var_codigo    char(8), 
             @var_numero    int OUTPUT 
as 
	select @var_numero  = 0 
	 
	if exists (select var_codigo from GesPet..Varios where  RTRIM(var_codigo) = RTRIM(@var_codigo)) 
	begin 
		select @var_numero=var_numero 
			from   GesPet..Varios 
			where  RTRIM(var_codigo) = RTRIM(@var_codigo) 
	 
		select @var_numero = @var_numero + 1 
	 
		update GesPet..Varios 
			set var_numero = @var_numero, var_texto = '', var_fecha=getdate() 
			where RTRIM(var_codigo) = RTRIM(@var_codigo) 
	end 
	else 
	begin 
		select @var_numero = 1 
	 
		insert into GesPet..Varios  (var_codigo, var_numero, var_texto, var_fecha) 
			values  (RTRIM(@var_codigo), 1, '', getdate()) 
	end 
	return(0) 
go

sp_procxmode 'sp_ProximoNumero', anymode
go

grant execute on dbo.sp_ProximoNumero to GesPetUsr 
go

grant execute on dbo.sp_ProximoNumero to RolGesIncConex
go

print 'Actualización finalizada.'
go
