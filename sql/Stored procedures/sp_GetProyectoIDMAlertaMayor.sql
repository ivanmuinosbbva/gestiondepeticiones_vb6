/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMAlertaMayor'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMAlertaMayor' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMAlertaMayor
go

create procedure dbo.sp_GetProyectoIDMAlertaMayor
	@ProjId		int,
	@ProjSubId	int,
	@ProjSubSId	int
as
	if (select count(*) from GesPet..ProyectoIDMAlertas where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId) > 0
		begin
			create table #tmp (
				alerta_id	char(1),
				alerta_num	int)

			insert into #tmp values ('R',100)
			insert into #tmp values ('A',50)

			select 
				max(t.alerta_num) as alerta_num,
				max(a.alert_tipo) as alert_tipo
			from 
				GesPet.dbo.ProyectoIDMAlertas a inner join
				#tmp t on (t.alerta_id = a.alert_tipo)
			where
				a.ProjId	 = @ProjId and
				a.ProjSubId  = @ProjSubId and
				a.ProjSubSId = @ProjSubSId and
				a.alert_status = 'N'
			drop table #tmp
		end
	else
		select 
			0	as alerta_num,
			'-' as alert_tipo
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMAlertaMayor to GesPetUsr
go

print 'Actualización realizada.'
go