use GesPet
go

print 'Creando / modificando stored procedure: sp_GetRecursoActuanteEstado'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoActuanteEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoActuanteEstado
go

create procedure dbo.sp_GetRecursoActuanteEstado
	@cod_alcance	char(3)='PET',
	@cod_nivel  char(4)='DIRE',
	@cod_area   char(8)=null,
	@cod_estado	 char(6)=null
as
	declare @cod_direccion	  char(8)
	declare @cod_gerencia	   char(8)
	declare @cod_sector		 char(8)
	declare @cod_grupo		  char(8)
	declare @flg_termin		 char(1)
	declare @cod_accion		 char(8)
	
	-- Controles de inconsistencia
	if @cod_nivel='GERE'
		begin
			select @cod_gerencia = @cod_area
			select @cod_direccion=cod_direccion from Gerencia where RTRIM(cod_gerencia)=RTRIM(@cod_area)
			if RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)=''
			begin
				select 30010 as ErrCode , 'Inconsistencia DIRECCION' as ErrDesc
				raiserror  30010 'Inconsistencia DIRECCION'
				return (30010)
			end
		end
	if @cod_nivel='SECT'
		begin
			select @cod_sector = @cod_area
			select @cod_gerencia=cod_gerencia from Sector where RTRIM(cod_sector)=RTRIM(@cod_area)
			if RTRIM(@cod_gerencia) is null or RTRIM(@cod_gerencia)=''
				begin
					select 30010 as ErrCode , 'Inconsistencia GERENCIA' as ErrDesc
					raiserror  30010 'Inconsistencia GERENCIA'
					return (30010)
				end
			select @cod_direccion=cod_direccion from Gerencia where RTRIM(cod_gerencia)=RTRIM(@cod_gerencia)
			if RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)=''
				begin
					select 30010 as ErrCode , 'Inconsistencia DIRECCION' as ErrDesc
					raiserror  30010 'Inconsistencia DIRECCION'
					return (30010)
				end
		end
	if @cod_nivel='GRUP'
		begin
			select @cod_grupo = @cod_area
			select @cod_sector=cod_sector from Grupo where RTRIM(cod_grupo)=RTRIM(@cod_area)
			if RTRIM(@cod_sector) is null or RTRIM(@cod_sector)=''
			begin
				select 30010 as ErrCode , 'Inconsistencia SECTOR' as ErrDesc
				raiserror  30010 'Inconsistencia SECTOR'
				return (30010)
			end
			select @cod_gerencia=cod_gerencia from Sector where RTRIM(cod_sector)=RTRIM(@cod_sector)
			if RTRIM(@cod_gerencia) is null or RTRIM(@cod_gerencia)=''
			begin
				select 30010 as ErrCode , 'Inconsistencia GERENCIA' as ErrDesc
				raiserror  30010 'Inconsistencia GERENCIA'
				return (30010)
			end
			select @cod_direccion=cod_direccion from Gerencia where RTRIM(cod_gerencia)=RTRIM(@cod_gerencia)
			if RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)=''
			begin
				select 30010 as ErrCode , 'Inconsistencia DIRECCION' as ErrDesc
				raiserror  30010 'Inconsistencia DIRECCION'
				return (30010)
			end
		end
	-- Aqu� la verdadera evaluaci�n
	if @cod_alcance='PET'
		begin
			select @flg_termin=RTRIM(flg_petici) from GesPet..Estados where RTRIM(cod_estado) = RTRIM(@cod_estado)
			select @cod_accion='PCHGEST'
		end
	if @cod_alcance='SEC'
		begin
			select @flg_termin=RTRIM(flg_secgru) from GesPet..Estados where RTRIM(cod_estado) = RTRIM(@cod_estado)
			select @cod_accion='SCHGEST'
		end
	if @cod_alcance='GRU'
		begin
			select @flg_termin=RTRIM(flg_secgru) from GesPet..Estados where RTRIM(cod_estado) = RTRIM(@cod_estado)
			select @cod_accion='GCHGEST'
		end
		/*
		print '%1! %2! %3! %4!',
		@cod_direccion,@cod_gerencia,@cod_sector,@cod_grupo
		*/
		select  
			Re.cod_recurso,
			Re.nom_recurso,
			Rp.cod_perfil,
			nom_perfil = (select Pe.nom_perfil from Perfil Pe where RTRIM(Pe.cod_perfil) = RTRIM(Rp.cod_perfil)),
			Rp.cod_nivel,
			Rp.cod_area
		from
			GesPet..RecursoPerfil Rp, 
			GesPet..Recurso Re
		where
			((RTRIM(Rp.cod_perfil) <> 'SADM') or (RTRIM(@flg_termin)='1')) and
			((Rp.cod_nivel='BBVA') or
			(Rp.cod_nivel='DIRE' and ISNULL(@cod_direccion,'')=ISNULL(Rp.cod_area,'')) or
			(Rp.cod_nivel='GERE' and ISNULL(@cod_gerencia,'')=ISNULL(Rp.cod_area,'')) or
			(Rp.cod_nivel='SECT' and ISNULL(@cod_sector,'')=ISNULL(Rp.cod_area,'')) or
			(Rp.cod_nivel='GRUP' and ISNULL(@cod_grupo,'')=ISNULL(Rp.cod_area,''))) and
			Rp.cod_perfil in (
				select DISTINCT cod_perfil 
				from GesPet..AccionesPerfilEstados where 
				(RTRIM(cod_accion)=RTRIM(@cod_accion) and 
				 RTRIM(cod_estado)=RTRIM(@cod_estado))) and
			/*
			Rp.cod_perfil in (
				select DISTINCT cod_perfil 
				from GesPet..AccionesPerfil where 
				(RTRIM(cod_accion)=RTRIM(@cod_accion) and 
				 RTRIM(cod_estado)=RTRIM(@cod_estado))) and
			*/
			(RTRIM(Rp.cod_recurso) = RTRIM(Re.cod_recurso)) and
			(RTRIM(Re.estado_recurso)='A')
	return(0)
go

grant execute on dbo.sp_GetRecursoActuanteEstado to GesPetUsr
go

print 'Stored procedure creado / modificado exitosamente.'
go
