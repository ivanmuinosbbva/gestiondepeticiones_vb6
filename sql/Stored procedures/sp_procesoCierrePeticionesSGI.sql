use GesPet
go

print 'Creando/actualizando SP: sp_procesoCierrePeticionesSGI'
go

if exists (select * from sysobjects where name = 'sp_procesoCierrePeticionesSGI' and sysstat & 7 = 4)
	drop procedure dbo.sp_procesoCierrePeticionesSGI
go

create procedure dbo.sp_procesoCierrePeticionesSGI
as 
	declare @hst_nrointerno				int
	declare @cod_grupo_homologacion		char(8)
	declare @c_pet_nrointerno			int
	declare @c_pet_nrointerno_estado	char(6)
	declare @c_pet_cod_grupo			char(8)
	declare @c_pet_cod_grupo_estado		char(6)
	declare @c_pet_cod_sector			char(8)
	declare @c_pet_cod_sector_estado	char(6)
	declare @c_pet_sgi_incidencia		varchar(20)

	declare @fe_ini_plan				smalldatetime
	declare @fe_fin_plan				smalldatetime
	declare @fe_ini_real				smalldatetime
	declare @fe_fin_real				smalldatetime
	declare @horaspresup				smallint
	declare @historialmemo				varchar(255)
	declare @logmessage					varchar(255)

	declare @fIniPlan					smalldatetime
	declare @fFinPlan					smalldatetime
	declare @fIniReal					smalldatetime
	declare @fFinReal					smalldatetime
	declare @hsPresup					smallint
	declare @new_estado					char(6)
	declare @new_estado_nombre			varchar(30)

	-- Identifico al grupo homologador
	select @cod_grupo_homologacion = g.cod_grupo
	from Grupo g
	where g.grupo_homologacion = 'H'

	-- Busco todas las peticiones candidatas a cerrar
	declare CursPeticiones cursor for
		select 
			p.pet_nrointerno,
			p.cod_estado,
			ps.cod_sector,
			ps.cod_estado,
			pg.cod_grupo,
			pg.cod_estado,
			p.sgi_incidencia
		from 
			Peticion p inner join
			PeticionGrupo pg on (pg.pet_nrointerno = p.pet_nrointerno) inner join
			PeticionSector ps on (pg.pet_nrointerno = ps.pet_nrointerno AND pg.cod_sector = ps.cod_sector)
		where
			p.cod_clase = 'CORR' and 
			p.sgi_incidencia <> '' and 
			charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD|') = 0 and 
			charindex(pg.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD|') = 0 and 
			pg.cod_grupo <> @cod_grupo_homologacion and 
			p.pet_nrointerno IN (
				select p.pet_nrointerno
				from 
					Peticion p inner join
					PeticionGrupo pg on (pg.pet_nrointerno = p.pet_nrointerno) 
				where
					p.cod_clase = 'CORR' and 
					p.sgi_incidencia <> '' and 
					charindex(pg.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD|') > 0 and 
					pg.cod_grupo = @cod_grupo_homologacion)
	for read only

	open CursPeticiones fetch CursPeticiones into @c_pet_nrointerno, @c_pet_nrointerno_estado, @c_pet_cod_sector, @c_pet_cod_sector_estado, @c_pet_cod_grupo, @c_pet_cod_grupo_estado, @c_pet_sgi_incidencia
		while (@@sqlstatus = 0) 
			begin
				select @logmessage = 'pet_nrointerno ' + LTRIM(RTRIM(convert(char(10),@c_pet_nrointerno))) + ' ' + @c_pet_nrointerno_estado + ' ' + @c_pet_cod_sector + ' ' + @c_pet_cod_sector_estado + ' ' + @c_pet_cod_grupo + ' ' + @c_pet_cod_grupo_estado
				PRINT @logmessage
				
				select 
					@fe_ini_plan = pg.fe_ini_plan,
					@fe_fin_plan = pg.fe_fin_plan,
					@fe_ini_real = pg.fe_ini_real,
					@fe_fin_real = pg.fe_fin_real,
					@horaspresup = pg.horaspresup
				from 
					PeticionGrupo pg
				where 
					pg.pet_nrointerno = @c_pet_nrointerno and 
					pg.cod_grupo = @c_pet_cod_grupo

				select @historialmemo = '� FINALIZACION Inicio Planif: ' + 
					convert(char(10),@fe_ini_plan,103) + ' | Finalizaci�n Planif: ' + 
					convert(char(10),@fe_ini_plan,103) + ' | Fch. Pasaje a Prod.: - | Horas Planif: ' + 
					ISNULL(LTRIM(RTRIM(convert(char(10),@horaspresup))),'0') + ' hs. �'

				execute sp_AddHistorial 
					@hst_nrointerno  OUTPUT,
					@c_pet_nrointerno,
					'GCHGEST',
					@c_pet_nrointerno_estado,
					@c_pet_cod_sector,
					@c_pet_cod_sector_estado,
					@c_pet_cod_grupo,
					@c_pet_cod_grupo_estado,
					''
				
				insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
					(@hst_nrointerno, 0, LTRIM(RTRIM(@historialmemo)))

				-- Reactiva al grupo heredero
				update 
					GesPet..PeticionGrupo
				set 
					cod_estado			= 'TERMIN',
					fe_estado			= getdate(),
					hst_nrointerno_sol  = @hst_nrointerno,
					hst_nrointerno_rsp  = 0,
					audit_user			= SUSER_NAME(),
					audit_date			= getdate()
				where 
					pet_nrointerno	= @c_pet_nrointerno and
					cod_sector		= @c_pet_cod_sector and
					cod_grupo		= @c_pet_cod_grupo
				
				-- Integro el estado del sector (seg�n sus grupos componedores)
				execute sp_GetIntegracionGrupos @c_pet_nrointerno, @c_pet_cod_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

				-- Si cambio el estado del sector destino, debo registrarlo en el historial de la petici�n
				if @new_estado <> @c_pet_cod_sector_estado
					begin
						-- Agrego el evento en el historial
						execute sp_AddHistorial 
							@hst_nrointerno  OUTPUT,
							@c_pet_nrointerno,
							'SCHGEST',
							@c_pet_nrointerno_estado,
							@c_pet_cod_sector,
							@c_pet_cod_sector_estado,
							'',
							'',
							''
						
						select 
							@fe_ini_plan = ps.fe_ini_plan,
							@fe_fin_plan = ps.fe_fin_plan,
							@fe_ini_real = ps.fe_ini_real,
							@fe_fin_real = ps.fe_fin_real,
							@horaspresup = ps.horaspresup
						from 
							PeticionSector ps
						where 
							ps.pet_nrointerno = @c_pet_nrointerno and 
							ps.cod_sector = @c_pet_cod_sector

						select @new_estado_nombre = e.nom_estado
						from Estados e 
						where e.cod_estado = @new_estado

						select @historialmemo = '� ' + UPPER(LTRIM(RTRIM(@new_estado_nombre))) + ' Inicio Planif: ' + 
							convert(char(10),@fe_ini_plan,103) + ' | Finalizaci�n Planif: ' + 
							convert(char(10),@fe_ini_plan,103) + ' | Fch. Pasaje a Prod.: - | Horas Planif: ' + 
							ISNULL(LTRIM(RTRIM(convert(char(10),@horaspresup))),'0') + ' hs. �'
						
						insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values (@hst_nrointerno, 0, LTRIM(RTRIM(@historialmemo)))
					end
				
				-- Actualizo los datos del sector
				update GesPet..PeticionSector
				set
					fe_ini_plan = @fIniPlan,
					fe_fin_plan = @fFinPlan,
					fe_ini_real = @fIniReal,
					fe_fin_real = @fFinReal,
					horaspresup = @hsPresup,
					cod_estado	= @new_estado,
					fe_estado	= getdate(),
					audit_user	= SUSER_NAME(),
					audit_date	= getdate(),
					hst_nrointerno_sol = @hst_nrointerno
				where
					pet_nrointerno = @c_pet_nrointerno and
					cod_sector = @c_pet_cod_sector
				
				-- Integro el estado de la petici�n (seg�n sus sectores componedores)
				execute sp_GetIntegracionSectores @c_pet_nrointerno, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT
				
				-- Actualizo los datos de la petici�n
				update GesPet..Peticion
				set
					fe_ini_plan = @fIniPlan,
					fe_fin_plan = @fFinPlan,
					fe_ini_real = @fIniReal,
					fe_fin_real = @fFinReal,
					horaspresup = @hsPresup,
					cod_estado	= @new_estado,
					fe_estado	= getdate(),
					audit_user	= SUSER_NAME(),
					audit_date	= getdate()
				where
					pet_nrointerno = @c_pet_nrointerno

				-- Si cambio el estado de la petici�n, debo registrarlo en el historial
				if @new_estado <> @c_pet_nrointerno_estado
					begin
						execute sp_AddHistorial 
							@hst_nrointerno  OUTPUT,
							@c_pet_nrointerno,
							'PCHGEST',
							@c_pet_nrointerno_estado,
							'',
							'',
							'',
							'',
							''

						select @new_estado_nombre = e.nom_estado
						from Estados e 
						where e.cod_estado = @new_estado

						select @historialmemo = '� ' + UPPER(LTRIM(RTRIM(@new_estado_nombre))) + ' Inicio Planif: ' + 
							convert(char(10),@fIniPlan,103) + ' | Finalizaci�n Planif: ' + 
							convert(char(10),@fFinPlan,103) + ' | Fch. Pasaje a Prod.: - | Horas Planif: ' + 
							ISNULL(LTRIM(RTRIM(convert(char(10),@hsPresup))),'0') + ' hs. �'

						insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
							(@hst_nrointerno, 0, LTRIM(RTRIM(@historialmemo)))
					end
				
				UPDATE GesInc..SGI_INCIDENCIAS
				SET 
					ESTADO_ARG_ID = 4,		-- CERRADO
					ESTADO_ID     = 2,		-- CERRADO
					COMPLETO	  = '1'		-- COMPLETO
				WHERE  
					SGI_INCIDENCIA_ID = @c_pet_sgi_incidencia

				fetch CursPeticiones into @c_pet_nrointerno, @c_pet_nrointerno_estado, @c_pet_cod_sector, @c_pet_cod_sector_estado, @c_pet_cod_grupo, @c_pet_cod_grupo_estado, @c_pet_sgi_incidencia
			end
	close CursPeticiones
	deallocate cursor CursPeticiones

	return(0)
go

sp_procxmode 'sp_procesoCierrePeticionesSGI', anymode
go

grant Execute on dbo.sp_procesoCierrePeticionesSGI to GesPetUsr 
go

grant execute on dbo.sp_procesoCierrePeticionesSGI to RolGesIncConex
go

print 'Actualizaci�n realizada.'
go
