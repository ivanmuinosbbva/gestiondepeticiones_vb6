/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 12.11.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteHEDT013'
go

if exists (select * from sysobjects where name = 'sp_DeleteHEDT013' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHEDT013
go

create procedure dbo.sp_DeleteHEDT013
	@tipo_id	char(1)=null,
	@subtipo_id	char(1)=null,
	@item	int=null,
	@item_nom	char(50)=null
as
	delete from
		GesPet.dbo.HEDT013
	where
		(tipo_id = @tipo_id or @tipo_id is null) and
		(subtipo_id = @subtipo_id or @subtipo_id is null) and
		(item = @item or @item is null) and
		(item_nom = @item_nom or @item_nom is null)
go

grant execute on dbo.sp_DeleteHEDT013 to GesPetUsr
go

print 'Actualización realizada.'
