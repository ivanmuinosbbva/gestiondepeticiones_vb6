/*
-000- a. FJS 11.10.2012 - Nuevo SP para listar los recurso de un perfil autorizados a fecha.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptAutorPorPerfilFecha'
go

if exists (select * from sysobjects where name = 'sp_rptAutorPorPerfilFecha' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptAutorPorPerfilFecha
go

create procedure dbo.sp_rptAutorPorPerfilFecha
	@fecha	char(8),
	@perfil	char(4)
as 
	select
		r.cod_recurso,
		r.nom_recurso,
		r.dlg_desde as 'desde',
		r.dlg_hasta as 'hasta',
		--convert(char(8),r.dlg_desde,112) as 'desde',
		--convert(char(8),r.dlg_hasta,112) as 'hasta',
		cod_recurso_reemp = (case 
			when (convert(smalldatetime,@fecha) >= r.dlg_desde and convert(smalldatetime,@fecha) <= r.dlg_hasta) then r.dlg_recurso
			else r.cod_recurso
		end),
		nom_recurso_reemp = (case 
			when (r.dlg_recurso is null or dlg_recurso <> '') and (@fecha >= r.dlg_desde and @fecha <= r.dlg_hasta) then 
				(select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = r.dlg_recurso)
			else r.nom_recurso
		end)
	from 
		GesPet..Recurso r left join
		GesPet..RecursoPerfil pf on (r.cod_recurso = pf.cod_recurso)
	where
		(r.dlg_recurso is not null and 
		 r.dlg_recurso <> '') and
		 r.dlg_recurso <> r.cod_recurso and
		 pf.cod_perfil = @perfil
	return(0)
go

grant execute on dbo.sp_rptAutorPorPerfilFecha to GesPetUsr
go

print 'Actualización realizada.'
go