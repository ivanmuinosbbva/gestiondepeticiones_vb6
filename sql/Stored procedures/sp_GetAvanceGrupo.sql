use GesPet
go
print 'sp_GetAvanceGrupo'
go
if exists (select * from sysobjects where name = 'sp_GetAvanceGrupo' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAvanceGrupo
go

create procedure dbo.sp_GetAvanceGrupo
		@cod_AvanceGrupo	char(8)=null,
		@nom_AvanceGrupo	char(30)=null,		
		@flg_habil		char(1)=null,
		@txt_AvanceGrupo	char(100)=null
as
begin
	select
		cod_AvanceGrupo,
		nom_AvanceGrupo,		
		flg_habil,
		txt_AvanceGrupo
	from
		GesPet..AvanceGrupo
	where
		(@cod_AvanceGrupo is null or RTRIM(@cod_AvanceGrupo) is null or RTRIM(@cod_AvanceGrupo)='' or RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo)) and
		(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil))
	order by nom_AvanceGrupo ASC
end
return(0)
go

grant execute on dbo.sp_GetAvanceGrupo to GesPetUsr
go
