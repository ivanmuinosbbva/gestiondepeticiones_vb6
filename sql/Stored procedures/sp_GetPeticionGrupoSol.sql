/*
-000- a. FJS 10.11.2009 - Nuevo SP para comprobar que un grupo pertenezca a una petición como ejecutor.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionGrupoSol'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionGrupoSol' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionGrupoSol
go

create procedure dbo.sp_GetPeticionGrupoSol
	@pet_nroasignado	int, 
	@cod_grupo			char(8)
as 
	select
		a.pet_nrointerno,
		b.pet_nroasignado,
		b.titulo,
		b.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = b.cod_estado),
		a.cod_grupo
	from
		GesPet..PeticionGrupo a inner join 
		GesPet..Peticion b on (a.pet_nrointerno = b.pet_nrointerno)
	where
		b.pet_nroasignado = @pet_nroasignado and
		a.cod_grupo = @cod_grupo
	return(0)
go

grant execute on dbo.sp_GetPeticionGrupoSol to GesPetUsr
go

print 'Actualización realizada.'
