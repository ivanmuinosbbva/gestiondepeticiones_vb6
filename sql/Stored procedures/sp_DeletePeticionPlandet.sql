/*
-000- a. FJS 04.08.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionPlandet'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionPlandet' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionPlandet
go

create procedure dbo.sp_DeletePeticionPlandet
	@per_nrointerno		int,
	@pet_nrointerno		int,
	@cod_grupo			char(8)
as 
	delete from GesPet..PeticionPlandet
	where
		(@per_nrointerno is null or per_nrointerno = @per_nrointerno) and 
		(@pet_nrointerno is null or pet_nrointerno = @pet_nrointerno) and
		(@cod_grupo is null or cod_grupo = @cod_grupo)
	return(0)
go

grant execute on dbo.sp_DeletePeticionPlandet to GesPetUsr
go

print 'Actualización realizada.'
go