/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 04.11.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoCategoria'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoCategoria' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoCategoria
go

create procedure dbo.sp_DeleteProyectoCategoria
	@ProjCatId	int
as
	if (select count(1) from GesPet..ProyectoClase where ProjCatId = @ProjCatId) > 0
		begin 
			raiserror 30011 'No puede eliminarse porque hay definidas clases para esta categoría'
			select 30011 as ErrCode , 'No puede eliminarse porque hay definidas clases para esta categoría' as ErrDesc
			return (30011)
		end 
	else
		delete from
			GesPet.dbo.ProyectoCategoria
		where
			ProjCatId = @ProjCatId
		return(0)
go

grant execute on dbo.sp_DeleteProyectoCategoria to GesPetUsr
go

print 'Actualización realizada.'
go
