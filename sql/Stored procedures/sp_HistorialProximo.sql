use GesPet
go
print 'sp_HistorialProximo'
go
if exists (select * from sysobjects where name = 'sp_HistorialProximo' and sysstat & 7 = 4)
drop procedure dbo.sp_HistorialProximo
go
create procedure dbo.sp_HistorialProximo 
             @var_numero    int OUTPUT 
as 
select @var_numero  = 0 
 
if exists ( select var_numero 
   from   GesPet..Varios 
   where  var_codigo = 'ULTHIST') 
 begin 
  select @var_numero=var_numero 
   from   GesPet..Varios 
   where  var_codigo = 'ULTHIST' 
  select @var_numero = @var_numero + 1 
  update GesPet..Varios 
      set    var_numero = @var_numero, 
             var_texto = '' 
      where  var_codigo = 'ULTHIST' 
 end 
 else 
 begin 
  select @var_numero = 1 
  insert into GesPet..Varios 
         ( var_codigo, var_numero, var_texto) 
      values 
         ( 'ULTHIST', 1, '') 
 end 
return(0) 
go



grant execute on dbo.sp_HistorialProximo to GesPetUsr 
go


