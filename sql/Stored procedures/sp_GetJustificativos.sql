/*
-000- a. FJS 04.08.2009 - 
-001- a. FJS 29.10.2009 - Se agrega el manejo del campo 'jus_hab'
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetJustificativos'
go

if exists (select * from sysobjects where name = 'sp_GetJustificativos' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetJustificativos
go

create procedure dbo.sp_GetJustificativos
	@jus_codigo	int=null,
	@flag_habil	char(1)=null
as
	select 
		a.jus_codigo,
		a.jus_descripcion,
		a.jus_hab
	from 
		GesPet.dbo.Justificativos a
	where
		(a.jus_codigo = @jus_codigo or @jus_codigo is null) and
		(a.jus_hab = @flag_habil or @flag_habil is null)
go

grant execute on dbo.sp_GetJustificativos to GesPetUsr
go

print 'Actualización realizada.'
