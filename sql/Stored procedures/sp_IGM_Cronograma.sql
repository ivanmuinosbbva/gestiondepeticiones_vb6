/*
-001- a. FJS 20.05.2010 - Nuevo SP para sistema IGM: Cronogramas por Direcci�n y Gerencia.
-002- a. FJS 28.10.2010 - Mejora en la legibilidad del c�digo.
-003- a. FJS 30.11.2010 - Mejora: se comenz� a manejar la tabla de saldo de horas trabajadas por una cuesti�n de performance.
-004- a. DTM 24.01.2011 - Optimizaci�n.
-005- a. FJS 26.01.2011 - Modificaci�n: deben incluirse peticiones en estado terminal vinculadas a proyectos y aquellas que no lo est�n, 
						  solo del a�o vigente y el anterior (�caro).
-006- a. FJS 16.02.2011 - Modificaci�n: se modifica la fecha a tomar para determinar la visualizaci�n de una petici�n. Antes se tomaba la fecha del �ltimo estado, ahora
						  se toma la fecha de fin real.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_Cronograma'
go

if exists (select * from sysobjects where name = 'sp_IGM_Cronograma' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_Cronograma
go

create procedure dbo.sp_IGM_Cronograma
	@cod_direccion	char(8)=null,
	@cod_gerencia	char(8)=null,
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null
as
	declare @fecha_actual_desde		smalldatetime
	declare @fecha_actual_hasta		smalldatetime
	declare @cod_direccion_eje		char(8)
	
	select @fecha_actual_desde = convert(smalldatetime,convert(char(4),year(getdate())) + '0101 00:00')
	select @fecha_actual_hasta = convert(smalldatetime,convert(char(4),year(getdate())) + '1231 23:59')
	select @cod_direccion_eje  = 'MEDIO'

	select
		a.cod_direccion,
		nom_direccion = (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = a.cod_direccion),
		a.cod_gerencia,
		nom_gerencia = (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = a.cod_gerencia),
		'pet_projid' = isnull(a.pet_projid,0),
		'pet_projsubid' = isnull(a.pet_projsubid,0),
		'pet_projsubsid'= isnull(a.pet_projsubsid,0),
		b.ProjNom,
		b.cod_estado as 'prj_cod_estado',
		prj_nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = b.cod_estado),
		b.fch_estado as 'prj_fe_estado',
		a.pet_nrointerno,
		a.pet_nroasignado,
		a.titulo,
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),
		a.fe_estado,
		horas_tyo_actual = isnull(convert(numeric(10,4),
							(select sum(h.horas)
							 from GesPet..HorasTrabajadas h 
							 where  h.pet_nrointerno = a.pet_nrointerno and
									h.cod_recurso in (select r.cod_recurso from GesPet..Recurso r 
													  where r.cod_direccion = @cod_direccion_eje) and
									(h.fe_desde >= @fecha_actual_desde) and (h.fe_hasta <= @fecha_actual_hasta)
						)/convert(numeric(10,4),60)),0), 
		horas_tyo_total = isnull(convert(numeric(10,4),
							(select sum(h.horas)
							 from GesPet..HorasTrabajadas h 
							 where	h.pet_nrointerno = a.pet_nrointerno and
									h.cod_recurso in (select r.cod_recurso from GesPet..Recurso r where r.cod_direccion = @cod_direccion_eje) and
									h.fe_hasta <= @fecha_actual_hasta
						)/convert(numeric(10,4),60)),0), 
		a.fe_ini_plan,
		a.fe_fin_plan,
		a.fe_ini_real,
		a.fe_fin_real,
		fe_ultcarga = (select max(x.fe_hasta) from GesPet..HorasTrabajadas x where x.pet_nrointerno = a.pet_nrointerno),
		visibilidad = isnull((select x.nom_importancia from GesPet..Importancia x where x.cod_importancia = a.importancia_cod),''),
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),
		orden = (case 
			when a.pet_projid is null or a.pet_projid = 0 then 2
			else 1
		end)
	from
		GesPet..Peticion a left join 
		GesPet..ProyectoIDM b on (a.pet_projid = b.ProjId and a.pet_projsubid = b.ProjSubId and a.pet_projsubsid = b.ProjSubSId) 
	where
		(a.cod_estado in (select x.cod_estado from GesPet..Estados x where x.IGM_hab = 'S')) and
		(
		 (charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0) or 
		 (charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0 and datediff(yy,a.fe_fin_real,getdate())<=1)
		 --(charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0 and (a.pet_projid is not null and a.pet_projid <> 0)) or
		 --(charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0 and (a.pet_projid is null or a.pet_projid = 0) and datediff(yy,a.fe_fin_real,getdate())<=1)
		) and
		(@cod_direccion is null or a.cod_direccion = @cod_direccion) and 
		(@cod_gerencia is null or a.cod_gerencia = @cod_gerencia) and 
		(@ProjId is null or a.pet_projid = @ProjId) and 
		(@ProjSubId is null or a.pet_projsubid = @ProjSubId) and 
		(@ProjSubSId is null or a.pet_projsubsid = @ProjSubSId)
	order by
		a.orden,
		a.cod_direccion,
		a.cod_gerencia,
		a.pet_projid,
		a.pet_projsubid,
		a.pet_projsubsid,
		a.pet_nroasignado
	return(0)
go

grant execute on dbo.sp_IGM_Cronograma to GesPetUsr
go

grant execute on dbo.sp_IGM_Cronograma to GrpTrnIGM
go

print 'Actualizaci�n realizada.'
go


/*
horas_tyo_actual = isnull(
							(select sum(convert(numeric(10,4),h.horas))
							 from GesPet..HorasTrabajadas h 
							 where  h.pet_nrointerno = a.pet_nrointerno and
									h.cod_recurso in (select r.cod_recurso from GesPet..Recurso r 
													  where r.cod_direccion = @cod_direccion_eje) and
									(h.fe_desde >= @fecha_actual_desde) and (h.fe_hasta <= @fecha_actual_hasta)
						)/convert(numeric(10,4),60),0), 
		horas_tyo_total = isnull(
							(select sum(convert(numeric(10,4),h.horas))
							 from GesPet..HorasTrabajadas h 
							 where	h.pet_nrointerno = a.pet_nrointerno and
									h.cod_recurso in (select r.cod_recurso from GesPet..Recurso r where r.cod_direccion = @cod_direccion_eje) and
									h.fe_hasta <= @fecha_actual_hasta
						)/convert(numeric(10,4),60),0), 
*/