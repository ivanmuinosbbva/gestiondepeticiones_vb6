/*
-000- a. FJS 15.09.2008 - Nuevo sp para ver los conformes de homologación de una petición en particular.
*/

use GesPet
go

print 'Creando/actualizando el SP: sp_GetPeticionConfHomo'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionConfHomo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionConfHomo
go

create procedure dbo.sp_GetPeticionConfHomo
	@pet_nrointerno	int=null
as 
	select
		pet_nrointerno,
		HSOB = (select count(*) from GesPet.dbo.PeticionConf where pet_nrointerno = Pc.pet_nrointerno and (charindex("HSOB", ok_tipo) > 0)),
		HOME = (select count(*) from GesPet.dbo.PeticionConf where pet_nrointerno = Pc.pet_nrointerno and (charindex("HOME", ok_tipo) > 0)),
		HOMA = (select count(*) from GesPet.dbo.PeticionConf where pet_nrointerno = Pc.pet_nrointerno and (charindex("HOMA", ok_tipo) > 0))
	from 
		GesPet.dbo.PeticionConf Pc
	where 
		pet_nrointerno = @pet_nrointerno
	group by 
		pet_nrointerno

return(0) 
go

grant execute on dbo.sp_GetPeticionConfHomo to GesPetUsr
go

print 'Actualización realizada.'


/*
 and  
		((charindex('HSOB', Pc.ok_tipo) > 0) or 
		 (charindex('HOME', Pc.ok_tipo) > 0) or 
		 (charindex('HOMA', Pc.ok_tipo) > 0))
*/