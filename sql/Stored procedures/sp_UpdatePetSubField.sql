/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 17.03.2008 - Se agrega la actualización para el campo de Fecha prevista de pasaje a Producción.
-002- a. FJS 26.01.2011 - Agregado: si se actualiza el estado del grupo, se vuelve a recalcular el estado del proyecto IDM al que estuviera vinculado la petición.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePetSubField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePetSubField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePetSubField
go

create procedure dbo.sp_UpdatePetSubField   
    @pet_nrointerno     int,   
    @cod_grupo      char(8)=null, 
    @campo            char(15),   
    @valortxt         char(10)=null,   
    @valordate        smalldatetime=null,   
    @valornum         int=null   
as   
	declare @cod_estado		char(6)
	--{ add -002- a.
	declare @pet_projid			int
	declare @pet_projsubid		int
	declare @pet_projsubsid		int
	declare @new_estado			char(6)
	declare @old_estado			char(6)
	declare @tipo_proyecto		char(1)
	--}

	if RTRIM(@campo)='FECHAIRL'   
		update PeticionGrupo  
			set fe_ini_real = @valordate,   
				audit_user = SUSER_NAME(),   
				audit_date = getdate()   
			where  pet_nrointerno = @pet_nrointerno    and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
	if RTRIM(@campo)='FECHAFRL'   
		update PeticionGrupo  
			set fe_fin_real = @valordate,   
				audit_user = SUSER_NAME(),   
				audit_date = getdate()   
			where  pet_nrointerno = @pet_nrointerno    and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
	if RTRIM(@campo)='FECHAIPN'   
		update PeticionGrupo  
			set fe_ini_plan = @valordate,   
				audit_user = SUSER_NAME(),   
				audit_date = getdate()   
			where  pet_nrointerno = @pet_nrointerno    and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
	if RTRIM(@campo)='FECHAFPN'   
		update PeticionGrupo  
			set fe_fin_plan = @valordate,   
				audit_user = SUSER_NAME(),   
				audit_date = getdate()   
			where  pet_nrointerno = @pet_nrointerno    and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
	if RTRIM(@campo)='ESTADO'   
		BEGIN
			select @cod_estado=cod_estado
			from	PeticionGrupo
			where pet_nrointerno = @pet_nrointerno and RTRIM(cod_grupo) = RTRIM(@cod_grupo)

			if RTRIM(@cod_estado)<>RTRIM(@valortxt)
			begin
				update PeticionGrupo  
				set cod_estado = @valortxt,   
					hst_nrointerno_sol = 0,
					fe_estado = getdate(), 
					audit_user = SUSER_NAME(),   
					audit_date = getdate()   
				where  pet_nrointerno = @pet_nrointerno    and 
				(RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 

				--{ add -002- a.
				select 
					@pet_projid = p.pet_projid,
					@pet_projsubid = p.pet_projsubid,
					@pet_projsubsid = p.pet_projsubsid
				from GesPet..Peticion p
				where p.pet_nrointerno = @pet_nrointerno

				if exists 
					(select 
						pr.ProjId
					 from GesPet..ProyectoIDM pr
					 where 
						pr.ProjId = @pet_projid and
						pr.ProjSubId = @pet_projsubid and 
						pr.ProjSubSId = @pet_projsubsid)

						select @tipo_proyecto = pr.tipo_proyecto, @old_estado	= pr.cod_estado
					 from GesPet..ProyectoIDM pr
					 where 
						pr.ProjId = @pet_projid and
						pr.ProjSubId = @pet_projsubid and 
						pr.ProjSubSId = @pet_projsubsid

					begin
						if charindex(@old_estado,'DESEST|CANCEL|TERMIN|')=0 and @tipo_proyecto = 'P'
							begin
								-- Obtengo el nuevo estado
								exec sp_GetEstadoProyectoIDM @pet_projid, @pet_projsubid, @pet_projsubsid, @new_estado output
								update 
									GesPet..ProyectoIDM
								set
									cod_estado  = @new_estado,
									fch_estado  = getdate(),
									usr_estado  = SUSER_NAME()
								where 
									ProjId = @pet_projid and
									ProjSubId = @pet_projsubid and
									ProjSubSId = @pet_projsubsid
							end
					end
				--}
			end
		END
	if RTRIM(@campo)='SITUAC'   
			update PeticionGrupo  
			set cod_situacion = @valortxt,   
				ult_accion  = '', 
				audit_user = SUSER_NAME(),   
				audit_date = getdate()   
			where  pet_nrointerno = @pet_nrointerno  and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
	if RTRIM(@campo)='ULTACC'   
			update PeticionGrupo  
			set ult_accion = @valortxt,   
				audit_user = SUSER_NAME(),   
				audit_date = getdate()   
			where  pet_nrointerno = @pet_nrointerno  and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
	if RTRIM(@campo)='HSTSOL'   
			update PeticionGrupo  
			set hst_nrointerno_sol = @valornum,   
				audit_user = SUSER_NAME(),   
				audit_date = getdate()   
			where  pet_nrointerno = @pet_nrointerno  and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
	 --{ add -001- a.
	if RTRIM(@campo)='FCHPRD'
		update GesPet..PeticionGrupo
		set 
			fe_produccion = @valordate,
			audit_user	  = SUSER_NAME(),
			audit_date	  = getdate()
		where  
			pet_nrointerno = @pet_nrointerno  and  
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(cod_grupo) = RTRIM(@cod_grupo))
	 --}
	return(0)
go



grant execute on dbo.sp_UpdatePetSubField to GesPetUsr 
go

print 'Actualización realizada.'