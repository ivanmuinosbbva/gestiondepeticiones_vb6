use GesPet
go

print 'sp_DeleteProyecto'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyecto' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteProyecto
go
create procedure dbo.sp_DeleteProyecto 
       @prj_nrointerno  int 
as 

if exists (select prj_nrointerno from GesPet..Peticion where prj_nrointerno = @prj_nrointerno)
begin
	select 30010 as ErrCode , 'Existen Peticiones asociadas al Proyecto, baja no efectuada' as ErrDesc
	raiserror  30010 'Existen Peticiones asociadas al Proyecto, baja no efectuada'
	return (30010)
end
 
begin tran 
	delete from GesPet..Proyecto
	where  prj_nrointerno = @prj_nrointerno 
commit tran 
return(0) 
go

grant execute on dbo.sp_DeleteProyecto to GesPetUsr 
go


