/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateValoracion'
go

if exists (select * from sysobjects where name = 'sp_UpdateValoracion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateValoracion
go

create procedure dbo.sp_UpdateValoracion
	@valorId		int,
	@valorTexto		char(255),
	@valorNum		REAL,
	@valorOrden		int,
	@valorHab		char(1),
	@valorDriver	int
as
	update 
		GesPet.dbo.Valoracion
	set
		valorTexto	= @valorTexto,
		valorNum	= @valorNum,
		valorOrden	= @valorOrden,
		valorHab	= @valorHab,
		valorDriver = @valorDriver
	where 
		valorId = @valorId
go

grant execute on dbo.sp_UpdateValoracion to GesPetUsr
go

print 'Actualización realizada.'
go
