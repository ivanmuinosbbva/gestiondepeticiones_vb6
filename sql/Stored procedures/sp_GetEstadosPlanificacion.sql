/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetEstadosPlanificacion'
go

if exists (select * from sysobjects where name = 'sp_GetEstadosPlanificacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEstadosPlanificacion
go

create procedure dbo.sp_GetEstadosPlanificacion
	@cod_estado_plan	int=null,
	@estado_hab			char(1)=null
as
	select 
		a.cod_estado_plan,
		a.nom_estado_plan,
		a.estado_hab
	from 
		GesPet.dbo.EstadosPlanificacion a
	where
		(a.cod_estado_plan = @cod_estado_plan or @cod_estado_plan is null) and
		(a.estado_hab = @estado_hab or @estado_hab is null)
	return(0)
go

grant execute on dbo.sp_GetEstadosPlanificacion to GesPetUsr
go

print 'Actualización realizada.'
