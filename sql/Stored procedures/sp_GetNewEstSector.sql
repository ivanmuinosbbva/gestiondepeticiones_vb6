/* 
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR 
Intenta replicar la funcionalidad del VB
*/

/*
-001- a. FJS 23.04.2009 - Sin usar.
-002- a. FJS 23.02.2010 - Se descarta la participación del grupo Homologador.
*/


use GesPet
go

print 'Creando/actualizando SP: sp_GetNewEstSector'
go

if exists (select * from sysobjects where name = 'sp_GetNewEstSector' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetNewEstSector
go

create procedure dbo.sp_GetNewEstSector
	@pet_nrointerno     int,
	@cod_sector			char(8)=null,
	@new_estado			char(6) OUTPUT
as
	declare @cod_hijo	varchar(8)
	declare @cod_estado varchar(6)
	declare @flg_rnkup	int

	declare @aEstado	int
	declare @nEstado	int
	declare @xEstado	char(6)

	select @nEstado=0
	select @xEstado=''

	declare @flg1		char(1)
	declare @flg2		char(1)

	select @flg1='N'
	select @flg2='N'


	DECLARE CursHijo CURSOR FOR
		select	
			PSG.cod_grupo,
			PSG.cod_estado,
			rnkup = CONVERT(int,EST.flg_rnkup)
		from	
			PeticionGrupo PSG, 
			Estados EST,
			Grupo GRP	-- add -002- a.
		where	
			(@pet_nrointerno = PSG.pet_nrointerno) and
			(@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(PSG.cod_sector) = RTRIM(@cod_sector)) and
			(RTRIM(EST.cod_estado) = RTRIM(PSG.cod_estado)) and
			(GRP.cod_grupo = PSG.cod_grupo and GRP.grupo_homologacion <> 'H')	-- add -002- a.
		order by 
			rnkup
		for read only

		OPEN CursHijo
			FETCH CursHijo INTO 
				@cod_hijo,
				@cod_estado,
				@flg_rnkup
			WHILE (@@sqlstatus = 0)
				BEGIN
					IF charindex(RTRIM(@cod_estado),RTRIM('TERMIN'))>0
					BEGIN
						select @flg1 = 'S'
					END
					IF charindex(RTRIM(@cod_estado),RTRIM('PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC'))>0
					BEGIN
						select @flg2 = 'S'
					END
					IF @flg_rnkup > @nEstado
					BEGIN
						select @nEstado = @flg_rnkup
						select @xEstado = @cod_estado
					END

					FETCH CursHijo INTO 
						@cod_hijo,
						@cod_estado,
						@flg_rnkup
				END
		CLOSE CursHijo
	DEALLOCATE CURSOR CursHijo

	IF @flg1='S' AND @flg2='S'
	BEGIN
		select @xEstado = 'EJECUC'
	END

	select @new_estado=@xEstado
return(0)
go

grant execute on dbo.sp_GetNewEstSector to GesPetUsr
go

print 'Actualización realizada.'