use GesPet
go
print 'sp_DeleteGerencia'
go
if exists (select * from sysobjects where name = 'sp_DeleteGerencia' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteGerencia
go
create procedure dbo.sp_DeleteGerencia 
           @cod_gerencia    char(8) 
as 
 
if exists (select cod_gerencia from GesPet..Gerencia where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)) 
begin 
    if exists (select cod_gerencia from GesPet..Sector where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)) 
    begin 
        raiserror  30010 'Existe alg�n Sector asociado a esta Gerencia'  
        select 30010 as ErrCode , 'Existe alg�n Sector asociado a esta Gerencia'  as ErrDesc 
    return (30010) 
    end 
 
    if exists (select cod_recurso from GesPet..RecursoPerfil where RTRIM(cod_nivel) = 'GERE' and RTRIM(cod_area) = RTRIM(@cod_gerencia)) 
    begin 
        raiserror  30011 'Existen Perfiles asociados a esta Gerencia'  
        select 30011 as ErrCode , 'Existen Perfiles asociados a esta Gerencia'  as ErrDesc 
    return (30011) 
    end 
     
    if exists (select cod_gerencia from GesPet..Recurso where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)) 
    begin 
        raiserror  30011 'Existen Recursos asociados a esta Gerencia'  
        select 30011 as ErrCode , 'Existen Recursos asociados a esta Gerencia'  as ErrDesc 
    return (30011) 
    end 
    if exists (select cod_gerencia from GesPet..Peticion where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a esta Gerencia'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a esta Gerencia'  as ErrDesc 
    return (30011) 
    end 
 
    if exists (select cod_gerencia from GesPet..PeticionSector where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a esta Gerencia'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a esta Gerencia'  as ErrDesc 
    return (30011) 
    end 
    delete GesPet..Gerencia 
      where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia) 
end 
else 
begin 
    raiserror 30001 'Gerencia Inexistente'   
    select 30001 as ErrCode , 'Gerencia Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteGerencia to GesPetUsr 
go


