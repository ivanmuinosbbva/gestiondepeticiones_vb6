/*
-000- a. FJS 18.06.2010 - Nuevo SP para listar los DSN en cat�logo HOST para SI.
-001- a. FJS 27.12.2010 - Se ajusta el formato de salida del sp.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptHEDT001'
go

if exists (select * from sysobjects where name = 'sp_rptHEDT001' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHEDT001
go

create procedure dbo.sp_rptHEDT001
as 
	select
		a.dsn_id,
		a.dsn_nom,
		a.dsn_enmas,					-- upd -001- a.
		a.dsn_feult,
		nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_userid),
		dsn_vb = 
		case
			when a.dsn_vb = 'N' then 'Pendiente'
			when a.dsn_vb = 'R' then 'Rechazado'
			when a.dsn_vb = 'S' then 'Visado'
		end,
		a.dsn_vb_fe,
		a.dsn_vb_userid,
		a.dsn_txt,
		a.dsn_vb_ultfch,
		orden = 
		case
			when a.dsn_vb = 'R' then 1	-- Rechazado
			when a.dsn_vb = 'N' then 2	-- Pendiente
			when a.dsn_vb = 'S' then 3	-- Visado
		end,
		a.dsn_nomprod					-- add -001- a.
	from 
		GesPet..HEDT001 a
	where 
		--a.estado = 'C'					-- Solo debe listar aquellos que ya est�n en HOST.
		charindex(a.estado, 'B|C|')>0
	order by 
		a.dsn_id
	return(0)
go

grant execute on dbo.sp_rptHEDT001 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go