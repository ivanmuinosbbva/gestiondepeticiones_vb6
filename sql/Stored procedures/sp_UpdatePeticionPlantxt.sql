/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 07.10.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlantxt'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlantxt' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlantxt
go

create procedure dbo.sp_UpdatePeticionPlantxt
	@borra_last		char(1)='N',
	@per_nrointerno	int,
	@pet_nrointerno	int,
	@cod_grupo		char(8),
	@mem_campo		char(10),
	@mem_secuencia	smallint,
	@mem_texto		char(255)=null
as
	if exists (select pet_nrointerno from GesPet..PeticionPlantxt 
				where per_nrointerno = @per_nrointerno and pet_nrointerno = @pet_nrointerno and cod_grupo = @cod_grupo and 
					RTRIM(mem_campo) = RTRIM(@mem_campo) and mem_secuencia = @mem_secuencia)
		begin
				update 
					GesPet.dbo.PeticionPlantxt
				set
					mem_texto = @mem_texto
				where 
					(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
					(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
					(cod_grupo = @cod_grupo or @cod_grupo is null) and
					(mem_campo = @mem_campo or @mem_campo is null) and
					(mem_secuencia = @mem_secuencia or @mem_secuencia is null)
		end
	else
		begin
			insert into GesPet..PeticionPlantxt    
			   (per_nrointerno,
				pet_nrointerno,
				cod_grupo,
				mem_campo,
				mem_secuencia,
				mem_texto)
		   values
			   (@per_nrointerno,
				@pet_nrointerno,
				@cod_grupo,
				@mem_campo,
				@mem_secuencia,
				@mem_texto)
		end

	    
	if @borra_last = 'S'    
		begin    
			delete from GesPet..PeticionPlantxt    
			where 
				per_nrointerno = @per_nrointerno and
				pet_nrointerno = @pet_nrointerno and
				cod_grupo = @cod_grupo and 
				RTRIM(mem_campo) = RTRIM(@mem_campo) and
				mem_secuencia > @mem_secuencia
		end    
	return(0)
go

grant execute on dbo.sp_UpdatePeticionPlantxt to GesPetUsr
go

print 'Actualización realizada.'
