use GesPet
go
print 'sp_GetProyectoAvanceDet'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoAvanceDet' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoAvanceDet
go
create procedure dbo.sp_GetProyectoAvanceDet 
	@prj_nrointerno	int=0,
	@pav_aamm	char(6)=null
as 

select 
	AVD.prj_nrointerno,
	AVD.pav_aamm,
	AVD.cod_AvanceGrupo,
	nom_AvanceGrupo = isnull((select AG.nom_AvanceGrupo from AvanceGrupo AG where RTRIM(AG.cod_AvanceGrupo)=RTRIM(AVD.cod_AvanceGrupo)),''),
	AVD.cod_AvanceCoef,
	nom_AvanceCoef = isnull((select AC.nom_AvanceCoef from AvanceCoef AC where RTRIM(AC.cod_AvanceCoef)=RTRIM(AVD.cod_AvanceCoef)),''),
	AVD.pav_completado,
	AVD.pav_porcentaje,
	AVD.audit_user,
	AVD.audit_date
from  ProyectoAvanceDet AVD
where	@prj_nrointerno=0 or AVD.prj_nrointerno=@prj_nrointerno and
	(@pav_aamm is null or RTRIM(@pav_aamm) is null or RTRIM(@pav_aamm)='' or RTRIM(AVD.pav_aamm)=RTRIM(@pav_aamm))
order by AVD.prj_nrointerno,AVD.pav_aamm,AVD.cod_AvanceGrupo,AVD.cod_AvanceCoef
return(0) 
go

grant execute on dbo.sp_GetProyectoAvanceDet to GesPetUsr 
go
