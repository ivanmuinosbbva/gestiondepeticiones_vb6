/*
-000- a. FJS 25.06.2008 - Nuevo SP para obtener la carga de horas en peticiones por recursos para Responsables.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHorasRealesResp'
go

if exists (select * from sysobjects where name = 'sp_GetHorasRealesResp' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHorasRealesResp
go

create procedure dbo.sp_GetHorasRealesResp
    @pet_nrointerno     int=0,
    @pet_sect           char(8)='NULL',
    @pet_grup           char(8)='NULL',
	@horas				int		output
as

-- Inicializo el retorno
select @horas = 0

begin
	-- Horas de Responsables (solo petición)
	select
		@horas = sum(cast(HRS.horas as float(1)))/60
	from
		GesPet..HorasTrabajadas HRS inner join
		GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso left join
		GesPet..PeticionGrupo PET on HRS.pet_nrointerno = PET.pet_nrointerno and REC.cod_sector = PET.cod_sector and REC.cod_grupo = PET.cod_grupo
	where
		HRS.pet_nrointerno = @pet_nrointerno and
		HRS.cod_recurso in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and
		(RTRIM(REC.cod_sector) = RTRIM(@pet_sect) or @pet_sect is null) and
		(RTRIM(REC.cod_grupo) = RTRIM(@pet_grup) or @pet_grup is null)
	group by 
		REC.cod_sector, REC.cod_grupo, PET.pet_nrointerno
end

return(0)

go

grant Execute on dbo.sp_GetHorasRealesResp to GesPetUsr
go

print 'Actualización realizada.'