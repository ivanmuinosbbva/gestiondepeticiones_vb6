use GesPet
go
print 'sp_UpdateBalanceSubRubro'
go
if exists (select * from sysobjects where name = 'sp_UpdateBalanceSubRubro' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateBalanceSubRubro
go
create procedure dbo.sp_UpdateBalanceSubRubro
	@cod_BalanceRubro	char(8)=null,
	@cod_BalanceSubRubro	char(8)=null,
	@nom_BalanceSubRubro	char(30)=null,
	@flg_habil		char(1)=null,
	@nom_largo		char(100)=null,
	@signo			char(1)=null,
	@secuencia		int = 1

as


declare @old_secuencia int  

select  @old_secuencia = BSR.secuencia  
	from  BalanceSubRubro BSR
	where 	RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and RTRIM(cod_BalanceSubRubro)<>RTRIM(@cod_BalanceSubRubro)

if @old_secuencia = @secuencia 
	begin 
		raiserror 30011 'Ya existe esa secuencia para ese elemento'   
		select 30011 as ErrCode , 'Ya existe esa secuencia para ese elemento' as ErrDesc   
		return (30011)   
	end 

if not exists (select cod_BalanceSubRubro from BalanceSubRubro where RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and RTRIM(cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro))
	begin 
		raiserror 30011 'SubRubro Inexistente'   
		select 30011 as ErrCode , 'SubRubro Inexistente' as ErrDesc   
		return (30011)   
	end 

Update BalanceSubRubro
	set	
	nom_BalanceSubRubro=@nom_BalanceSubRubro,
	flg_habil=	    @flg_habil,
	nom_largo=	    @nom_largo,
	signo=		    @signo,
	secuencia=	    @secuencia
	where RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and RTRIM(cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro)

return(0)
go

grant execute on dbo.sp_UpdateBalanceSubRubro to GesPetUsr
go
