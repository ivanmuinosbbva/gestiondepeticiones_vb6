/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteAccionesPerfilIDM'
go

if exists (select * from sysobjects where name = 'sp_DeleteAccionesPerfilIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteAccionesPerfilIDM
go

create procedure dbo.sp_DeleteAccionesPerfilIDM
	@cod_accion	char(8)='',
	@cod_perfil	char(4)='',
	@cod_estado	char(6)='',
	@cod_estnew	char(6)=''
as
	delete from
		GesPet.dbo.AccionesPerfilIDM
	where
		cod_accion = @cod_accion and
		cod_perfil = @cod_perfil and
		cod_estado = @cod_estado and
		cod_estnew = @cod_estnew
go

grant execute on dbo.sp_DeleteAccionesPerfilIDM to GesPetUsr
go

print 'Actualización realizada.'
go
