use GesPet
go
print 'sp_GetDiasHabiles'
go
if exists (select * from sysobjects where name = 'sp_GetDiasHabiles' and sysstat & 7 = 4)
drop procedure dbo.sp_GetDiasHabiles
go
create procedure dbo.sp_GetDiasHabiles 
             @fechadesde        smalldatetime, 
             @fechahasta        smalldatetime, 
             @diashabiles       int OUTPUT 
as 
declare @diasferiados int 
   select @diashabiles=datediff(day, @fechadesde, @fechahasta) + 1 
   select @diasferiados=count(1) from Feriado 
       where  fecha >= @fechadesde and fecha <= @fechahasta 
   if @diasferiados < @diashabiles 
       select @diashabiles=@diashabiles - @diasferiados 
   return(0) 
go



grant execute on dbo.sp_GetDiasHabiles to GesPetUsr 
go


