/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteCatalogoDetalle'
go

if exists (select * from sysobjects where name = 'sp_DeleteCatalogoDetalle' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteCatalogoDetalle
go

create procedure dbo.sp_DeleteCatalogoDetalle
	@id		int,
	@campo	varchar(50)
as
	delete from
		GesPet.dbo.CatalogoDetalle
	where
		id = @id and
		campo = LTRIM(RTRIM(@campo))
go

grant execute on dbo.sp_DeleteCatalogoDetalle to GesPetUsr
go
grant execute on dbo.sp_DeleteCatalogoDetalle to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_DeleteCatalogoDetalle','anymode'
go

print 'Actualización realizada.'
go
