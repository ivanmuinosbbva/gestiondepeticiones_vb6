/*
-000- a. FJS 19.05.2009 - Nuevo SP para manejo de carga de horas desglosado por Aplicativo
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHsTrabAplicativo'
go

if exists (select * from sysobjects where name = 'sp_InsertHsTrabAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHsTrabAplicativo
go

create procedure dbo.sp_InsertHsTrabAplicativo
	@cod_recurso	char(10)=null,
	@cod_tarea		char(8)=null,
	@pet_nrointerno	int=null,
	@fe_desde		smalldatetime,
	@fe_hasta		smalldatetime,
	@horas			smallint=null,
	@app_id			char(30)=null,
	@app_horas		smallint=null,
	@hsapp_texto	varchar(255)
as
	if exists (select 1 from GesPet..HorasTrabajadasAplicativo where cod_recurso = @cod_recurso and cod_tarea = @cod_tarea and pet_nrointerno = @pet_nrointerno and fe_desde = @fe_desde and fe_hasta = @fe_hasta and horas = @horas and app_id = @app_id)
		begin
			raiserror 30001 'Ya han sido cargadas las horas para este aplicativo. Modifique.'
			select 30001 as ErrCode , 'Ya han sido cargadas las horas para este aplicativo. Modifique.' as ErrDesc
			return (30001)
		end
	else
		begin
			insert into GesPet..HorasTrabajadasAplicativo (
				cod_recurso,
				cod_tarea,
				pet_nrointerno,
				fe_desde,
				fe_hasta,
				horas,
				app_id,
				app_horas,
				hsapp_texto)
			values (
				@cod_recurso,
				@cod_tarea,
				@pet_nrointerno,
				@fe_desde,
				@fe_hasta,
				@horas,
				@app_id,
				@app_horas,
				@hsapp_texto)
		end
	return(0)
go

grant execute on dbo.sp_InsertHsTrabAplicativo to GesPetUsr
go

print 'Actualización realizada.'