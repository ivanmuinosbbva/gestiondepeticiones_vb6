/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteSolicitudesSSDD'
go

if exists (select * from sysobjects where name = 'sp_DeleteSolicitudesSSDD' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteSolicitudesSSDD
go

create procedure dbo.sp_DeleteSolicitudesSSDD
	@sol_nroasignado	int
as
	delete from
		GesPet.dbo.SolicitudesSSDD
	where
		sol_nroasignado = @sol_nroasignado
go

grant execute on dbo.sp_DeleteSolicitudesSSDD to GesPetUsr
go

grant execute on dbo.sp_DeleteSolicitudesSSDD to TrnCGMEnmascara
go

sp_procxmode 'dbo.sp_DeleteSolicitudesSSDD','anymode'
go

print 'Actualización realizada.'
go
