use GesPet
go

print 'sp_UpdateRecursoPerfil'
go

if exists (select * from sysobjects where name = 'sp_UpdateRecursoPerfil' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRecursoPerfil
go
create procedure dbo.sp_UpdateRecursoPerfil 
	@cod_recurso char(10), 
    @cod_perfil  char(4), 
    @cod_nivel   char(4), 
    @cod_area    char(8) 
as 

	if exists (select cod_recurso from GesPet..RecursoPerfil where RTRIM(cod_recurso) = RTRIM(@cod_recurso) and RTRIM(cod_perfil) = RTRIM(@cod_perfil)) 
		begin 
			update GesPet..RecursoPerfil 
			set cod_nivel = @cod_nivel, 
				cod_area = @cod_area 
			where 
				RTRIM(cod_recurso) = RTRIM(@cod_recurso) and 
				RTRIM(cod_perfil) = RTRIM(@cod_perfil) 
			
			INSERT INTO GesPet..RecursoPerfilAudit 
			VALUES (@cod_recurso, @cod_perfil, 2, @cod_nivel, @cod_area, getdate(), suser_name())
		end 
	else 
		begin 
			insert into GesPet..RecursoPerfil (cod_recurso, cod_perfil, cod_nivel, cod_area) 
			values (@cod_recurso, @cod_perfil, @cod_nivel ,@cod_area)

			INSERT INTO GesPet..RecursoPerfilAudit 
			VALUES (@cod_recurso, @cod_perfil, 1, @cod_nivel, @cod_area, getdate(), suser_name())
		end
	return(0)
go

grant execute on dbo.sp_UpdateRecursoPerfil to GesPetUsr 
go


