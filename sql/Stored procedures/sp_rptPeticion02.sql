
use GesPet
go
print 'sp_rptPeticion02'
go
if exists (select * from sysobjects where name = 'sp_rptPeticion02' and sysstat & 7 = 4)
drop procedure dbo.sp_rptPeticion02
go
create procedure dbo.sp_rptPeticion02 
        @pet_tipo   varchar(4)='NULL', 
        @pet_estado varchar(250)='NULL', 
        @cod_importancia    varchar(4)='NULL',
        @pet_desde_alta varchar(8)='NULL', 
        @pet_hasta_alta varchar(8)='NULL', 
        @pet_desde_estado varchar(8)='NULL', 
        @pet_hasta_estado varchar(8)='NULL', 
        @pet_desde_iplan    varchar(8)='NULL', 
        @pet_hasta_iplan    varchar(8)='NULL', 
        @pet_desde_fplan    varchar(8)='NULL', 
        @pet_hasta_fplan    varchar(8)='NULL',
        @pet_desde_plani    varchar(8)='NULL', 
        @pet_hasta_plani    varchar(8)='NULL',
        @pet_desde_repla    varchar(8)='NULL', 
        @pet_hasta_repla    varchar(8)='NULL',
        @pet_desde_ireal    varchar(8)='NULL', 
        @pet_hasta_ireal    varchar(8)='NULL', 
        @pet_desde_freal    varchar(8)='NULL', 
        @pet_hasta_freal    varchar(8)='NULL'
as 

select PET.pet_nrointerno, 
	PET.pet_nroasignado, 
	PET.cod_tipo_peticion,
	PET.cod_estado,          
	nom_estado = (select ESTPET.nom_estado from Estados ESTPET where ESTPET.cod_estado=PET.cod_estado),
	PET.titulo,  
	PET.corp_local,
	PET.cod_orientacion,
	nom_orientacion = (select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),
	PET.importancia_cod,
	importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod),
	PET.fe_estado,           
	PET.cod_situacion, 
	nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion), 
	PET.fe_ini_plan, 
	PET.fe_fin_plan,             
	PET.horaspresup,
	PET.fe_ini_orig,
	PET.fe_fin_orig
from  Peticion PET
where   
	(@pet_tipo is null or RTRIM(@pet_tipo)='NULL' or RTRIM(PET.cod_tipo_peticion) = RTRIM(@pet_tipo)) and 
	(@pet_estado is null or RTRIM(@pet_estado)='NULL' or charindex(RTRIM(PET.cod_estado),RTRIM(@pet_estado))>0) and 
	(@cod_importancia is null or RTRIM(@cod_importancia)='NULL' or RTRIM(PET.importancia_cod) = RTRIM(@cod_importancia)) and 
	(RTRIM(@pet_desde_estado)='NULL' or convert(char(8),PET.fe_estado,112) >= @pet_desde_estado) and 
	(RTRIM(@pet_hasta_estado)='NULL' or convert(char(8),PET.fe_estado,112) <= @pet_hasta_estado) and 

	(RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_alta) and 
	(RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_alta) and 

	(RTRIM(@pet_desde_iplan)='NULL' or convert(char(8),PET.fe_ini_plan,112) >= @pet_desde_iplan) and 
	(RTRIM(@pet_hasta_iplan)='NULL' or convert(char(8),PET.fe_ini_plan,112) <= @pet_hasta_iplan) and 

	(RTRIM(@pet_desde_fplan)='NULL' or convert(char(8),PET.fe_fin_plan,112) >= @pet_desde_fplan) and 
	(RTRIM(@pet_hasta_fplan)='NULL' or convert(char(8),PET.fe_fin_plan,112) <= @pet_hasta_fplan) and 

	(RTRIM(@pet_desde_ireal)='NULL' or convert(char(8),PET.fe_ini_real,112) >= @pet_desde_ireal) and 
	(RTRIM(@pet_hasta_ireal)='NULL' or convert(char(8),PET.fe_ini_real,112) <= @pet_hasta_ireal) and 

	(RTRIM(@pet_desde_freal)='NULL' or convert(char(8),PET.fe_fin_real,112) >= @pet_desde_freal) and 
	(RTRIM(@pet_hasta_freal)='NULL' or convert(char(8),PET.fe_fin_real,112) <= @pet_hasta_freal) and 

	(RTRIM(@pet_desde_plani)='NULL' or convert(char(8),PET.fe_fec_orig,112) >= @pet_desde_plani) and 
	(RTRIM(@pet_hasta_plani)='NULL' or convert(char(8),PET.fe_fec_orig,112) <= @pet_hasta_plani) and 

	(RTRIM(@pet_desde_repla)='NULL' or (PET.cant_planif>1 and convert(char(8),PET.fe_fec_plan,112)>=@pet_desde_repla)) and 
	(RTRIM(@pet_hasta_repla)='NULL' or (PET.cant_planif>1 and convert(char(8),PET.fe_fec_plan,112)<=@pet_hasta_repla)) 

order by PET.pet_nroasignado,PET.pet_nrointerno 
return(0) 
go



grant execute on dbo.sp_rptPeticion02 to GesPetUsr 
go

