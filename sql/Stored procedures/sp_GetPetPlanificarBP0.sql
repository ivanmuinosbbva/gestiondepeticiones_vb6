use GesPet
go

print 'Creando/actualizando SP: sp_GetPetPlanificarBP0'
go

if exists (select * from sysobjects where name = 'sp_GetPetPlanificarBP0' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetPlanificarBP0
go

create procedure dbo.sp_GetPetPlanificarBP0
	@cod_bpar		char(8)=null,
	@per_nrointerno	int=null
as
	declare @secuencia		int
	declare @ret_status     int
	declare	@agr_SDA		int

	create table #AgrupXPetic(
		pet_nrointerno 	int)

	create table #AgrupArbol(
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)
	
	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
		end

	select 
		p.pet_nroasignado,
		p.cod_tipo_peticion,
		p.cod_clase,
		p.fe_pedido,
		p.titulo,
		nom_estado = (select e.nom_estado from Estados e where e.cod_estado = p.cod_estado),
		p.prioridad,
		nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = p.cod_bpar),
		p.cod_BPE,
		nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = p.cod_BPE),
		p.pet_nrointerno,
		p.cod_bpar,
		p.cod_estado,
		p.puntuacion
	from Peticion p
	where 
		p.cod_tipo_peticion in ('NOR','ESP','PRJ') and 
		p.cod_clase in ('NUEV','EVOL') and 
		p.pet_regulatorio = 'N' and 
		charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and 
		(p.cod_bpar is not null and LTRIM(RTRIM(p.cod_bpar)) <> '') and 
		((p.valor_impacto <> 0 OR p.valor_facilidad <> 0) OR 
		 (p.pet_projid IN (
			select px.ProjId
			from ProyectoIDM px 
			where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId)))
		and p.pet_nrointerno not in (
			select pp.pet_nrointerno
			from PeticionPlancab pp) 
		and p.pet_nrointerno not in (
			select sda.pet_nrointerno
			from #AgrupXPeticSDA sda 
			group by sda.pet_nrointerno)
	order by
		puntuacion DESC
go

grant execute on dbo.sp_GetPetPlanificarBP0 to GesPetUsr
go

print 'Actualización realizada.'
go
