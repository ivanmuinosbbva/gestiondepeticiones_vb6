/*
-000- a. FJS 11.01.2016 - Nuevo SP para mantener la tabla Empresa.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetEmpresa'
go

if exists (select * from sysobjects where name = 'sp_GetEmpresa' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEmpresa
go

create procedure dbo.sp_GetEmpresa
	@empid		int=null,
	@empnom		varchar(50)=null
as 
	SELECT
		e.empid,
		e.empnom,
		e.emphab,
		e.empabrev
	FROM Empresa e 
	WHERE
		(@empid is null OR e.empid = @empid) and
		(@empnom is null OR UPPER(LTRIM(RTRIM(e.empnom))) LIKE '%' + UPPER(LTRIM(RTRIM(@empnom))) + '%')
	return(0)
go

grant execute on dbo.sp_GetEmpresa to GesPetUsr
go

print 'Actualización realizada.'
go
