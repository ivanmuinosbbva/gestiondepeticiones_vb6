/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoPerfilAudit'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoPerfilAudit' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoPerfilAudit
go

create procedure dbo.sp_GetRecursoPerfilAudit
	@cod_recurso	char(10)=null
as
	select 
		ag.cod_recurso,
		ag.cod_perfil,
		ag.cod_ope,
		ag.cod_nivel,
		ag.cod_area,
		ag.auditfch,
		ag.auditusr
	from 
		GesPet.dbo.RecursoPerfilAudit ag
	where
		(ag.cod_recurso = @cod_recurso or @cod_recurso is null)
	order by
		ag.cod_recurso,
		ag.auditfch
go

grant execute on dbo.sp_GetRecursoPerfilAudit to GesPetUsr
go

print 'Actualización realizada.'
go
