use GesPet
go
print 'sp_GetPerfilCostBenef'
go
if exists (select * from sysobjects where name = 'sp_GetPerfilCostBenef' and sysstat & 7 = 4)
drop procedure dbo.sp_GetPerfilCostBenef
go
create procedure dbo.sp_GetPerfilCostBenef
		@cod_PerfilCostBenef	char(8)=null,
		@flg_habil		char(1)=null
as
begin
	select
		cod_PerfilCostBenef,
		nom_PerfilCostBenef,
		flg_habil		
	from
		GesPet..PerfilCostBenef
	where
		(@cod_PerfilCostBenef is null or RTRIM(@cod_PerfilCostBenef) is null or RTRIM(@cod_PerfilCostBenef)='' or RTRIM(cod_PerfilCostBenef) = RTRIM(@cod_PerfilCostBenef)) and
		(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil))
	order by nom_PerfilCostBenef ASC
end
return(0)
go

grant execute on dbo.sp_GetPerfilCostBenef to GesPetUsr
go
