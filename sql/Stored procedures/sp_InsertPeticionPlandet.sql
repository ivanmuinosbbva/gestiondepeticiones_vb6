-- ESTE SP DEBE SER convertIDO PARA CONSOLIDAR

/*
-000- a. FJS 13.07.2010 - Nuevo SP para...
-001- a. FJS 29.09.2010 - Se agregan por defecto con el estado de planificación 1: Sin planificar.
-002- a. FJS 11.03.2011 - Modificación: se agrega un nuevo campo para reflejar la primera fecha de planificación cuando una petición nunca ha sido planificada.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionPlandet'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionPlandet' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionPlandet
go

create procedure dbo.sp_InsertPeticionPlandet
	@per_nrointerno		int,
	@cod_grupo			char(8),
	@pet_nrointerno		int,
	@plan_orden			int
as 
	if not exists (select pet_nrointerno from GesPet..PeticionPlandet where per_nrointerno = @per_nrointerno and pet_nrointerno = @pet_nrointerno and cod_grupo = @cod_grupo)
		begin	
			insert into GesPet..PeticionPlandet (
				per_nrointerno,
				pet_nrointerno,
				cod_grupo,
				plan_orden,
				plan_ordenfin,
				plan_estado,
				plan_actfch,
				plan_actusr,
				plan_act2fch,
				plan_act2usr,
				plan_ori,
				plan_estadoini,
				plan_motnoplan,
				plan_fealcadef,
				plan_fefunctst,
				plan_horasper,
				plan_desaestado,
				plan_motivsusp,
				plan_motivrepl,
				plan_feplanori,
				plan_feplannue,
				plan_cantreplan,
				plan_horasreplan,
				plan_fchreplan,
				plan_fchestadoini)		-- add -002- a.
			values (
				@per_nrointerno,
				@pet_nrointerno,
				@cod_grupo,
				@plan_orden,
				null,
				'PLANIF',
				getdate(),
				suser_name(),
				null,
				null,
				1,
				1,			-- upd -001- a. Por defecto, se ingresan "Sin planificar"
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null)		-- add -002- a.
		end
	else
		begin
			update GesPet..PeticionPlandet
			set
				plan_orden  = @plan_orden,
				plan_actfch = getdate(),
				plan_actusr = suser_name()
			where
				per_nrointerno = @per_nrointerno and
				pet_nrointerno = @pet_nrointerno and
				cod_grupo	   = @cod_grupo
		end
	return(0)
go

grant execute on dbo.sp_InsertPeticionPlandet to GesPetUsr
go

print 'Actualización realizada.'