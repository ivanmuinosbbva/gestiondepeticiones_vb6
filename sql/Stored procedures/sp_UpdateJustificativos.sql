/*
-000- a. FJS 04.08.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateJustificativos'
go

if exists (select * from sysobjects where name = 'sp_UpdateJustificativos' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateJustificativos
go

create procedure dbo.sp_UpdateJustificativos
	@jus_codigo			int=0,
	@jus_descripcion	varchar(255)='',
	@jus_hab			char(1)=''
as
	update 
		GesPet.dbo.Justificativos
	set
		jus_descripcion = @jus_descripcion,
		jus_hab = @jus_hab
	where 
		jus_codigo = @jus_codigo
go

grant execute on dbo.sp_UpdateJustificativos to GesPetUsr
go

print 'Actualización realizada.'
