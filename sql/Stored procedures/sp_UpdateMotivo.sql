use GesPet
go

print 'Creando/actualizando SP: sp_UpdateMotivo'
go

if exists (select * from sysobjects where name = 'sp_UpdateMotivo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateMotivo
go

create procedure dbo.sp_UpdateMotivo
	@cod_motivo		int=null,
	@nom_motivo		char(120)='',
	@hab_motivo		char(1)=''
as 
	update
		GesPet.dbo.Motivos
	set
        nom_motivo = RTRIM(@nom_motivo),
		hab_motivo = RTRIM(@hab_motivo)
	where
		cod_motivo = @cod_motivo

return(0) 
go

grant execute on dbo.sp_UpdateMotivo to GesPetUsr
go

print 'Actualización realizada.'
