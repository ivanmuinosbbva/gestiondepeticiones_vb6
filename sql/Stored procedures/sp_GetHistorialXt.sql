/*
-001- a. FJS 14.11.2008 - Se modifica para permitir obtener el historial completo.
-002- a. FJS 20.01.2009 - Se modifica la cl�usula ORDER BY para optimizar la consulta y utilizar el �ndice 'idx_PetHistorial'.
-003- a. FJS 03.03.2009 - Se modifica la condici�n y el join para optimizar la consulta.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHistorialXt'
go

if exists (select * from sysobjects where name = 'sp_GetHistorialXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHistorialXt
go

create procedure dbo.sp_GetHistorialXt
	@pet_nrointerno	int=0,
	@hst_nrointerno	int=0
as
	select
		Hs.pet_nrointerno,
		Hs.hst_nrointerno,
		Hs.hst_fecha,
		Hs.cod_evento,
		nom_accion = (select Ax.nom_accion from Acciones Ax where RTRIM(Ax.cod_accion) = RTRIM(Hs.cod_evento)),
		Hs.pet_estado,
		pet_nom_estado = (select ESP.nom_estado from Estados ESP where RTRIM(ESP.cod_estado) = RTRIM(Hs.pet_estado)),
		Hs.cod_sector,
		nom_sector = (select Se.nom_sector from Sector Se where RTRIM(Se.cod_sector) = RTRIM(Hs.cod_sector)),
		Hs.sec_estado,
		sec_nom_estado = (select ESS.nom_estado from Estados ESS where RTRIM(ESS.cod_estado) = RTRIM(Hs.sec_estado)),
		Hs.cod_grupo,
		nom_grupo = (select Gr.nom_grupo from Grupo Gr where RTRIM(Gr.cod_grupo)=RTRIM(Hs.cod_grupo)),
		Hs.gru_estado,
		gru_nom_estado = (select ESG.nom_estado from Estados ESG where RTRIM(ESG.cod_estado) = RTRIM(Hs.gru_estado)),
		Hs.replc_user,
		Hs.audit_user,
		nom_audit = (select Ra.nom_recurso from Recurso Ra where RTRIM(Ra.cod_recurso)=RTRIM(Hs.audit_user)),
		nom_replc = (select Rr.nom_recurso from Recurso Rr where RTRIM(Rr.cod_recurso)=RTRIM(Hs.replc_user)),
		--Hm.mem_texto,
		mem_texto = '',
		--mem_texto = (select LTRIM(RTRIM(hm.mem_texto)) from HistorialMemo hm where hm.hst_nrointerno = Hs.hst_nrointerno),
		Hs.cod_perfil,
		nom_perfil = (select p.nom_perfil from Perfil p where p.cod_perfil = Hs.cod_perfil)
	from
		GesPet..Historial Hs
		/*
		GesPet..Historial Hs left join 
		GesPet..HistorialMemo Hm on (Hs.hst_nrointerno = Hm.hst_nrointerno)
		*/
		/* del -003- a.
		GesPet..Historial Hs,
		GesPet..HistorialMemo Hm
		*/
	where 
		/* del -003- a.
		(@pet_nrointerno is null or Hs.pet_nrointerno = @pet_nrointerno) and
		(@hst_nrointerno is null or Hs.hst_nrointerno=@hst_nrointerno) and
		(Hs.hst_nrointerno = Hm.hst_nrointerno) and
		*/
		Hs.pet_nrointerno = @pet_nrointerno and
		(@hst_nrointerno is null or Hs.hst_nrointerno=@hst_nrointerno)	/* and
		Hm.hst_secuencia  = 0
		*/
	order by 
		--Hs.pet_nrointerno, Hs.hst_nrointerno desc		-- upd -002- a.
		Hs.hst_fecha		desc,
		Hs.hst_nrointerno	desc
return(0)
go

grant execute on dbo.sp_GetHistorialXt to GesPetUsr
go

print 'Actualizaci�n realizada.'