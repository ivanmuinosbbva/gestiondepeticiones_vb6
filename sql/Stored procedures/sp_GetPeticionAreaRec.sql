/*
-001- a. FJS 06.07.2007 - Se agrega la columna cod_clase (clase de la petici�n) al conjunto de resultados.
-002- a. FJS 18.09.2007 - Se agrega al conjunto de resultados la columna de indicador de m�todo SOx esquema nuevo o viejo.
-003- a. FJS 11.09.2008 - Se agrega la columna que indica si es Regulatorio.
-004- a. FJS 30.06.2016 - Nuevo: nuevo campo calculado para determinar la puntuaci�n de una petici�n (basado en f�rmula de valorizaci�n).
-005- a. FJS 30.06.2016 - Nuevo: nuevo campo calculado para determinar la categor�a de una petici�n.
-006- a. FJS 30.06.2016 - Nuevo: se agrega a la salida el campo de impacto tecnol�gico de la petici�n.
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetPeticionAreaRec'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionAreaRec' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionAreaRec
go

create procedure dbo.sp_GetPeticionAreaRec
	@cod_area		varchar(8)=null,
	@cod_estado		varchar(250)=null,
	@cod_recurso	varchar(250)=null,
	@prio_ejecuc    int=0
as
	/*
	--{ add -004- a.
	declare @sum_impacto			real
	declare @sum_facilidad			real
	declare @sum_impacto_facilidad	real
	
	select @sum_impacto = SUM(d.driverValor)
	from Drivers d
	where d.indicadorId = 1		-- Impacto

	select @sum_facilidad = SUM(d.driverValor)
	from Drivers d
	where d.indicadorId = 2		-- Impacto

	select @sum_impacto_facilidad = @sum_impacto + @sum_facilidad
	--}
	*/

	--{ add -005- a.
	declare @secuencia		int
	declare @ret_status     int

	create table #AgrupArbol(
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)

	create table #tablaCriterio (
		pet_nrointerno		int,
		categoria			char(6))

	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	
	declare	@agr_SDA	int
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
			
			INSERT INTO #tablaCriterio
			select 
				p.pet_nrointerno,
				categoria = case
					when p.cod_tipo_peticion in ('AUI','AUX') OR p.pet_regulatorio = 'S' then '1. REG'
					when exists (select x.pet_nrointerno from #AgrupXPeticSDA x where x.pet_nrointerno = p.pet_nrointerno) then '2. SDA'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then '3. PES'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then '4. ENG'
					when p.cod_clase in ('OPTI','CORR','SPUF','ATEN') then '6. CSI'
					when exists (
						 select count(1) 
						 from ProyectoIDM px 
						 where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
						 having count(1) > 0) OR (p.valor_impacto <> 0 OR p.valor_facilidad <>0) then '5. PRI'
					when p.cod_clase = 'SINC' then '---'
					else '7. SPR'
				end
			from Peticion p 
		end
	--}

	if  @cod_recurso='NULL'
		begin
			 select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
				cod_area_hij=GRU.cod_grupo,
				nom_area_hij = (select NOMH.nom_grupo from Grupo NOMH where RTRIM(NOMH.cod_grupo)=RTRIM(GRU.cod_grupo)),
				cod_estado_hij=GRU.cod_estado,
				nom_estado_hij = (select ESTHIJ.nom_estado from Estados ESTHIJ where RTRIM(ESTHIJ.cod_estado)=RTRIM(GRU.cod_estado)),
				cod_situacion_hij=GRU.cod_situacion,
				nom_situacion_hij=(select SITHIJ.nom_situacion from Situaciones SITHIJ where RTRIM(GRU.cod_situacion)=RTRIM(SITHIJ.cod_situacion)),
				fe_ini_plan_hij=GRU.fe_ini_plan,
				fe_fin_plan_hij=GRU.fe_fin_plan,
				horaspresup_hij=GRU.horaspresup,
				GRU.prio_ejecuc,
				PET.pet_sox001,		-- add -002-
				--cod_recurso=''	-- del -001-
				--{ add -001-
				cod_recurso='',
				PET.cod_clase,
				--}
				PET.pet_regulatorio,		-- add -003- a.
				PET.puntuacion,
				/*
				--{ add -004- a. 
				,puntuacion = (
					(PET.valor_facilidad * (@sum_facilidad / @sum_impacto_facilidad)) +
					(PET.valor_impacto * (@sum_impacto / @sum_impacto_facilidad)))
				--}
				*/
				c.categoria,		-- add -005- a.
				PET.pet_imptech		-- add -006- a.
			from
				GesPet..PeticionGrupo GRU,
				GesPet..Peticion PET INNER JOIN 
					#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where
				(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(GRU.cod_estado),RTRIM(@cod_estado))>0) and
				(@prio_ejecuc=0 or @prio_ejecuc=GRU.prio_ejecuc) and
				(RTRIM(GRU.cod_grupo)=RTRIM(@cod_area)) and
				PET.pet_nrointerno = GRU.pet_nrointerno
			order by 
				PET.pet_nroasignado,
				PET.pet_nrointerno
			
			return(0)
		end
	else
		begin
			 select
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
				cod_area_hij=GRU.cod_grupo,
				nom_area_hij = (select NOMH.nom_grupo from Grupo NOMH where RTRIM(NOMH.cod_grupo)=RTRIM(GRU.cod_grupo)),
				cod_estado_hij=GRU.cod_estado,
				nom_estado_hij = (select ESTHIJ.nom_estado from Estados ESTHIJ where RTRIM(ESTHIJ.cod_estado)=RTRIM(GRU.cod_estado)),
				cod_situacion_hij=GRU.cod_situacion,
				nom_situacion_hij=(select SITHIJ.nom_situacion from Situaciones SITHIJ where RTRIM(GRU.cod_situacion)=RTRIM(SITHIJ.cod_situacion)),
				fe_ini_plan_hij=GRU.fe_ini_plan,
				fe_fin_plan_hij=GRU.fe_fin_plan,
				horaspresup_hij=GRU.horaspresup,
				PET.pet_sox001,	-- add -002-
				--GRU.prio_ejecuc	-- del -001-
				--{ add -001-
				GRU.prio_ejecuc,
				PET.cod_clase
				--}
				/*	    PR.cod_recurso,
				nom_recurso=(select R.nom_recurso from Recurso R where R.cod_recurso=PR.cod_recurso)  */
				,PET.pet_regulatorio	-- add -003- a.
				,PET.puntuacion
				/*
				--{ add -004- a. 
				,puntuacion = (
					(PET.valor_facilidad * (@sum_facilidad / @sum_impacto_facilidad)) +
					(PET.valor_impacto * (@sum_impacto / @sum_impacto_facilidad)))
				--}
				*/
				,c.categoria		-- add -005- a.
				,PET.pet_imptech	-- add -006- a.
			from
				GesPet..PeticionGrupo GRU,
				GesPet..Peticion PET INNER JOIN 
					#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where
				(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(GRU.cod_estado),RTRIM(@cod_estado))>0) and
				(@prio_ejecuc=0 or @prio_ejecuc=GRU.prio_ejecuc) and
				(@cod_area is null or RTRIM(@cod_area) is null or RTRIM(@cod_area)=''  or RTRIM(GRU.cod_grupo)=RTRIM(@cod_area)) and
				PET.pet_nrointerno = GRU.pet_nrointerno and
				GRU.cod_grupo in (select  PR.cod_grupo from GesPet..PeticionRecurso PR where PR.pet_nrointerno=PET.pet_nrointerno and (charindex(RTRIM(PR.cod_recurso),RTRIM(@cod_recurso))>0))
			order by 
				PET.pet_nroasignado,
				PET.pet_nrointerno
			return(0)
		end
	/*
		GRU.pet_nrointerno = PR.pet_nrointerno and
		GRU.cod_grupo = PR.cod_grupo and
		(charindex(RTRIM(PR.cod_recurso),RTRIM(@cod_recurso))>0)
	*/
	return(0)
go

grant execute on dbo.sp_GetPeticionAreaRec to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go