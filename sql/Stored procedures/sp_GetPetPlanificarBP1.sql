/*
-000- a. FJS 10.07.2010 - Nuevo SP para mostrar en la grilla, todas las peticiones de un BP, que continen al menos un grupo homologable en estado activo,
						  de clase EVOL o NUEV.
-001- a. FJS 15.11.2010 - Planificaci�n DYD (2� etapa): se identifica las peticiones agregadas por DYD.
-002- a. FJS 02.12.2010 - Arreglo: se permite que se vean peticiones planificadas para cualquier per�odo.
-002- b. FJS 06.12.2010 - Mejora: se visualiza la descripci�n abreviada del per�odo.
-003- a. FJS 06.01.2011 - Arreglo: para la vista de trabajo del BP. El BP no puede cambiar la prioridad de una petici�n se alguno de los registros en [PeticionPlandet] 
						  esta confirmado por Icaro.
-004- a. FJS 04.03.2011 - Cambios: desde la bandeja de trabajo de planificaci�n para BP, estos deben poder ver tambi�n lo planificado por los otros BPs (a pedido de Icaro).
-005- a. FJS 25.06.2012 - Arreglo: estaba faltando contemplar la situaci�n cuando est� priorizada y pendiente (PLANIF).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetPlanificarBP1'
go

if exists (select * from sysobjects where name = 'sp_GetPetPlanificarBP1' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetPlanificarBP1
go

create procedure dbo.sp_GetPetPlanificarBP1
	@cod_bpar		char(8)=null,
	@per_nrointerno	int=null,
	@prioridad		char(1)=null
as 
	select 
		isnull(pc.per_nrointerno,0) as 'per_nrointerno',
		a.pet_nroasignado,
		a.pet_nrointerno,
		a.cod_tipo_peticion,
		a.cod_clase,
		a.pet_regulatorio,
		isnull(a.pet_projid,0) as pet_projid,
		isnull(a.pet_projsubid,0) as pet_projsubid,
		isnull(a.pet_projsubsid,0) as pet_projsubsid,
		a.titulo,
		pc.prioridad as 'prioridad',
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),
		a.cod_sector,
		nom_sector    = (select RTRIM(x.nom_sector) from GesPet..Sector x where x.cod_sector = a.cod_sector),
		nom_gerencia  = (select RTRIM(x.nom_gerencia) from GesPet..Gerencia x where x.cod_gerencia = a.cod_gerencia),
		nom_direccion = (select RTRIM(x.nom_direccion) from GesPet..Direccion x where x.cod_direccion = a.cod_direccion),
		'alca' = (
		case
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'ALCA') > 0 then 'U'
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'ALCA') = 0 then 'S'
			else 'N'
		end),
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),	-- add -004- a.
		existe_dyd = case
			when (
				select count(*)
				from 
					GesPet..PeticionGrupo x inner join
					GesPet..Grupo y on (x.cod_grupo = y.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and 
					y.grupo_homologacion = 'S'
				) > 0 then 'Si'
			else 'No'
		end,
		dyd_activo = case
			when (
				select count(*) 
				from 
					GesPet..PeticionGrupo x inner join
					GesPet..Grupo y on (x.cod_grupo = y.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and 
					x.cod_gerencia = 'DESA' and
					y.grupo_homologacion = 'S' and
					(charindex(x.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(charindex(x.cod_estado, 'ESTIOK|ESTIRK|ESTIMA|PLANOK|PLANIF|PLANRK|EJECRK|EJECUC|SUSPEN|') > 0)) 
				) > 0 then 'Si'
			else 'No'
		end,
		dyd_default_prio = case 
			when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) >= 1 then '1'
			else ''
		end,
		pc.plan_estado,
		'plan_situacion' = '',
		'plan_situacion_dsc' = case
			when a.cod_clase <> 'OPTI' and (pc.prioridad is null or pc.prioridad = '') then 'Pendiente'
			when a.cod_clase <> 'OPTI' and (pc.prioridad = '1' or pc.prioridad = '2') and (pc.plan_estado = 'PLANIF') then 'Pendiente'		-- add -005- a.
			when a.cod_clase <> 'OPTI' and (pc.prioridad = '1' or pc.prioridad = '2') and (pc.plan_estado <> 'PLANOK') then 'Priorizado'
			when a.cod_clase <> 'OPTI' and (pc.prioridad = '1' or pc.prioridad = '2') and (pc.plan_estado = 'PLANOK') then 'Confirmado'
			when a.cod_clase = 'OPTI' then '-'
			else ''
		end,
		pc.plan_actfch,
		pc.plan_actusr,
		'plan_actusrnom' = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = pc.plan_actusr),
		pc.plan_act2fch,
		pc.plan_act2usr,
		'plan_act2usrnom' = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = pc.plan_act2usr),
		'total_grupos' = (
			select count(*)
			from 
				GesPet..PeticionGrupo pg inner join
				GesPet..Peticion p on (pg.pet_nrointerno = p.pet_nrointerno) inner join
				GesPet..Grupo g on (pg.cod_grupo = g.cod_grupo)
			where
				pg.pet_nrointerno = a.pet_nrointerno and
				pg.cod_gerencia = 'DESA' and
				charindex(pg.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and 
				g.grupo_homologacion = 'S'),
		'pendientes' = (
			select count(*) 
			from GesPet..PeticionPlandet x 
			where x.per_nrointerno = pc.per_nrointerno and 
				  x.pet_nrointerno = pc.pet_nrointerno and 
				  x.plan_estado = 'PLANIF'),
		pc.plan_stage
		,per_abrev = isnull((select x.per_abrev from GesPet..Periodo x where x.per_nrointerno = pc.per_nrointerno),'-')	-- add -002- b.
		--{ add -003- a.
		,hab = case
			when (select count(1) from GesPet..PeticionPlandet x where x.per_nrointerno = pc.per_nrointerno and x.pet_nrointerno = pc.pet_nrointerno and x.plan_estado = 'APROBA')>0 then 'N'
			else 'S'
		end
		--}
	from 
		GesPet..Peticion a left join
		GesPet..PeticionPlancab pc on (pc.pet_nrointerno = a.pet_nrointerno and pc.cod_bpar = a.cod_bpar)
	where 
		((@cod_bpar is null and a.cod_bpar <> '') or a.cod_bpar = @cod_bpar) and 
		--a.cod_clase in ('NUEV','EVOL') and	-- del -001- a.
		--{ add -001- a.
		(charindex(a.cod_clase,'NUEV|EVOL|')>0 or
		(charindex(a.cod_clase,'OPTI|')>0 and pc.pet_nrointerno is not null)) and
		--}
		charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
		(pc.per_nrointerno is null or @per_nrointerno is null or pc.per_nrointerno = @per_nrointerno) and
		(@prioridad is null or pc.prioridad = @prioridad) 
	order by
		a.pet_nroasignado
	return (0)
go

grant execute on dbo.sp_GetPetPlanificarBP1 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go

/*
			--when (a.cod_clase <> 'OPTI' and (pc.prioridad = '1' or pc.prioridad = '2') and (pc.plan_estado = 'PLANOK')) or
			--	 (select count(1) from GesPet..PeticionPlandet x where x.per_nrointerno = pc.per_nrointerno and x.pet_nrointerno = pc.pet_nrointerno and x.plan_estado = 'APROBA')>0 then 'Confirmado'

			*/