/*
-000- a. FJS 27.06.2008 - Nuevo SP para actualizar datos del logout de usuarios online.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateRecursoApp'
go

if exists (select * from sysobjects where name = 'sp_UpdateRecursoApp' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRecursoApp
go

create procedure dbo.sp_UpdateRecursoApp
	@cod_recurso		char(10)=null,
	@cod_equipo			char(20)=null,
	@fe_ingreso			smalldatetime=null,
	@hr_ingreso			char(8)=null
as 
	update 
		GesPet..Aucab 
	set 
		fe_egreso = getdate() 
	where 
		cod_recurso = @cod_recurso and 
		convert(char(8), fe_ingreso, 112) = convert(char(10), @fe_ingreso, 112) and 
		hr_ingreso = @hr_ingreso and
		cod_equipo = @cod_equipo and
		(fe_egreso is null or fe_egreso = '')
go 

grant execute on dbo.sp_UpdateRecursoApp to GesPetUsr
go

print 'Actualización realizada.'
