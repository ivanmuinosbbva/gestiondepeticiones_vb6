/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 09.11.2010 - Manejo de documentos adjuntos del sistema
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetDocumento'
go

if exists (select * from sysobjects where name = 'sp_GetDocumento' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetDocumento
go

create procedure dbo.sp_GetDocumento
	@cod_doc	char(6)=null,
	@hab_doc	char(1)=null
as
	select 
		a.cod_doc,
		a.nom_doc,
		a.hab_doc,
		a.actual
	from 
		GesPet.dbo.Documento a
	where
		(a.cod_doc = @cod_doc or @cod_doc is null) and
		(a.hab_doc = @hab_doc or @hab_doc is null)
	order by
		a.actual DESC
go

grant execute on dbo.sp_GetDocumento to GesPetUsr
go

sp_procxmode 'sp_GetDocumento', 'anymode'
go

print 'Actualización realizada.'
go