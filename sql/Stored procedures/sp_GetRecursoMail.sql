use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoMail'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoMail' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoMail
go

create procedure dbo.sp_GetRecursoMail 
	@cod_recurso  char(10) = null
as 
	select 
		R.cod_recurso, 
		R.nom_recurso, 
		R.euser,
		R.email,
		R.dlg_recurso, 
		R.dlg_desde, 
		R.dlg_hasta,
		dlg_email = (select D.email from GesPet..Recurso D where D.cod_recurso=R.cod_recurso), 
		dlg_euser = (select D.euser from GesPet..Recurso D where D.cod_recurso=R.cod_recurso)
	from GesPet..Recurso R
	where 
		(@cod_recurso is null or RTRIM(@cod_recurso) is null or RTRIM(@cod_recurso)='' or RTRIM(R.cod_recurso) = RTRIM(@cod_recurso)) 
		--and (RTRIM(R.estado_recurso) = 'A') 
	order by 
		R.cod_recurso
	
	return(0) 
go

grant execute on dbo.sp_GetRecursoMail to GesPetUsr 
go

print 'Actualización realizada.'
go
