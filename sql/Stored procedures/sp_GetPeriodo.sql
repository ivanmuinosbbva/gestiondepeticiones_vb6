/*
-000- a. FJS 13.07.2010 - Nuevo SP para visualizar los períodos de planificación definidos en el sistema.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeriodo'
go

if exists (select * from sysobjects where name = 'sp_GetPeriodo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeriodo
go

create procedure dbo.sp_GetPeriodo
	@per_nrointerno		int=null,
	@per_habil			char(1)='S'
as 
	select
		a.per_nrointerno,
		a.per_nombre,
		a.per_tipo,
		nom_tipo = 
		(case
			when a.per_tipo = 'Z' then 'Semanal'
			when a.per_tipo = 'Q' then 'Quincenal'
			when a.per_tipo = 'M' then 'Mensual'
			when a.per_tipo = 'B' then 'Bimestral'
			when a.per_tipo = 'T' then 'Trimestral'
			when a.per_tipo = 'C' then 'Cuatrimestral'
			when a.per_tipo = 'S' then 'Semestral'
			when a.per_tipo = 'A' then 'Anual'
			when a.per_tipo = 'X' then 'Personalizado'
		end),
		a.fe_desde,
		a.fe_hasta,
		a.per_estado,
		nom_estado = (select x.nom_estado_per from GesPet..PeriodoEstado x where x.cod_estado_per = a.per_estado),
		a.per_ultusrid,
		nom_ultusrid = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.per_ultusrid),
		a.per_ultfchest,
		nom_grupo = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = r.cod_grupo),
		nom_sector = (select x.nom_sector from GesPet..Sector x where x.cod_sector = r.cod_sector),
		nom_gerencia = (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = r.cod_gerencia),
		nom_direccion = (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = r.cod_direccion),
		vigente = case
			when (getdate() >= a.fe_desde and getdate() <= a.fe_hasta) then 'S'
			else 'N'
		end,
		a.per_abrev
	from
		GesPet..Periodo a left join
		GesPet..Recurso r on (r.cod_recurso = a.per_ultusrid)
	where
		(@per_nrointerno is null or a.per_nrointerno = @per_nrointerno) and
		(@per_habil is null or (@per_habil = 'S' and a.per_estado <> 'PLAN40'))
	order by 
		a.fe_desde
	return(0)
go

grant execute on dbo.sp_GetPeriodo to GesPetUsr
go

print 'Actualización realizada.'
go