/*
-000- a. FJS 17.07.2014 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSolicitudListado'
go

if exists (select * from sysobjects where name = 'sp_GetSolicitudListado' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSolicitudListado
go

create procedure dbo.sp_GetSolicitudListado
	@modo				char(1)='1',
	@sol_nroasignado	int,
	@cod_recurso		char(10)=null,
	@cod_perfil			char(4)=null	
as
	if @modo = '1'
		begin
			select 
				a.sol_nroasignado,
				nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
				sol_tipo = (select x.nom_tipo from GesPet..TipoSolicitud x where x.cod_tipo = a.sol_tipo),
				a.sol_eme,
				a.sol_fecha,
				file_prod = (case
					when a.sol_tipo = '7' then a.sol_bckuptabla
					when a.sol_tipo = '3' then a.sol_nrotabla
					when a.sol_tipo = '2' then '-'
					else a.sol_file_prod
				end)
				/*
				file_prod = (case
					when a.sol_tipo in ('2','3','7') then a.sol_file_out
					else a.sol_file_prod
				end)
				*/
			from GesPet..Solicitudes a
			where a.sol_nroasignado = @sol_nroasignado
		end
	else
		begin
			select 
				a.sol_nroasignado,
				nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
				sol_tipo = (select x.nom_tipo from GesPet..TipoSolicitud x where x.cod_tipo = a.sol_tipo),
				a.sol_eme,
				a.sol_fecha,
				file_prod = (case
					when a.sol_tipo = '7' then a.sol_bckuptabla
					when a.sol_tipo = '3' then a.sol_nrotabla
					when a.sol_tipo = '2' then '-'
					else a.sol_file_prod
				end)
				/*
				file_prod = (case
					when a.sol_tipo in ('2','3','7') then a.sol_file_out
					else a.sol_file_prod
				end)
				*/
			from GesPet..Solicitudes a
			where 
				((@cod_perfil = 'CGRU' and a.sol_resp_ejec = @cod_recurso) or (@cod_perfil = 'CSEC' and a.sol_resp_sect = @cod_recurso)) and
				((@cod_perfil = 'CGRU' and a.sol_estado = 'A') or (@cod_perfil = 'CSEC' and a.sol_estado = 'B')) and
				a.sol_nroasignado <> @sol_nroasignado
		end
	return(0)
go

grant execute on dbo.sp_GetSolicitudListado to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
