use GesPet
go
print 'sp_DeleteSector'
go
if exists (select * from sysobjects where name = 'sp_DeleteSector' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteSector
go
create procedure dbo.sp_DeleteSector 
           @cod_sector          char(8) 
as 
 
if exists (select cod_sector from GesPet..Sector where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
begin 
    if exists (select cod_sector from GesPet..Grupo where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
    begin 
        raiserror  30010 'Existe alg�n Grupo asociado a este Sector'  
        select 30010 as ErrCode , 'Existe alg�n Grupo asociado a este Sector'  as ErrDesc 
    return (30010) 
    end 
     
    if exists (select cod_sector from GesPet..Recurso where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
    begin 
        raiserror  30011 'Existen Recursos asociados a este Sector'  
        select 30011 as ErrCode , 'Existen Recursos asociados a este Sector'  as ErrDesc 
    return (30011) 
    end 
 
    if exists (select cod_recurso from GesPet..RecursoPerfil where RTRIM(cod_nivel) = 'SECT' and RTRIM(cod_area) = RTRIM(@cod_sector)) 
    begin 
        raiserror  30011 'Existen Perfiles asociados a este Sector'  
        select 30011 as ErrCode , 'Existen Perfiles asociados a este Sector'  as ErrDesc 
    return (30011) 
    end 
     
    if exists (select cod_sector from GesPet..Peticion where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a este Sector'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a este Sector'  as ErrDesc 
    return (30011) 
    end 
     
    if exists (select cod_sector from GesPet..PeticionSector where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a este Sector'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a este Sector'  as ErrDesc 
    return (30011) 
    end 
 
    delete GesPet..Sector 
      where RTRIM(cod_sector) = RTRIM(@cod_sector) 
end 
else 
begin 
    raiserror 30001 'Sector Inexistente'   
    select 30001 as ErrCode , 'Sector Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteSector to GesPetUsr 
go


