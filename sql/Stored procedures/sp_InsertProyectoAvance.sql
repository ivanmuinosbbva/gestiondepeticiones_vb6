use GesPet
go
/* execute sp_InsertProyectoAvance 3,'200704' */
print 'sp_InsertProyectoAvance'
go
if exists (select * from sysobjects where name = 'sp_InsertProyectoAvance' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertProyectoAvance
go
create procedure dbo.sp_InsertProyectoAvance
	@prj_nrointerno		int,
	@pav_aamm	char(6)
as

declare	@xpav_aamm	char(6)
declare	@pav_calcul	int

if exists (select prj_nrointerno from ProyectoAvanceCab where prj_nrointerno=@prj_nrointerno and pav_aamm=@pav_aamm)
	begin 
		raiserror 30011 'Planilla de avance ya generada'   
		select 30011 as ErrCode , 'Planilla de avance ya generada' as ErrDesc   
		return (30011)   
	end 


/* busca el ultimo generado */
select @xpav_aamm=MAX(pav_aamm) from ProyectoAvanceCab where prj_nrointerno=@prj_nrointerno

/* inserta el cabezal */
insert into ProyectoAvanceCab
	(prj_nrointerno,
	pav_aamm,
	pav_manual, 
	pav_calcul,
	audit_user, 
	audit_date)
values
	(@prj_nrointerno,
	@pav_aamm, 
	0,
	0,
	SUSER_NAME(),
	getdate())

/* genera detalle con plancheta b�sica */
insert into ProyectoAvanceDet
	(prj_nrointerno,
	pav_aamm,
	cod_AvanceGrupo,
	cod_AvanceCoef,
	pav_completado,
	pav_porcentaje,
	audit_user,
	audit_date)
	select
		@prj_nrointerno,
		@pav_aamm,
		AVC.cod_AvanceGrupo,
		AVC.cod_AvanceCoef,
		'N',
		AVC.porcentaje,
		SUSER_NAME(),
		getdate()
	from AvanceCoef AVC
	where AVC.flg_habil<>'N'

/* completa detalle con ultimo avance */
if @xpav_aamm is not null
begin
	update ProyectoAvanceDet 
	set PD.pav_completado='S'
	FROM ProyectoAvanceDet PD, ProyectoAvanceDet PX
	where PD.prj_nrointerno=@prj_nrointerno and 
		PD.pav_aamm=@pav_aamm and 
		PD.pav_completado='N' and
		PX.prj_nrointerno=@prj_nrointerno and 
		PX.pav_aamm=@xpav_aamm and 
		PD.cod_AvanceGrupo=PX.cod_AvanceGrupo and
		PD.cod_AvanceCoef=PX.cod_AvanceCoef and
		PX.pav_completado='S'
end


/* recalcula el total de cabezal */
select @pav_calcul=SUM(pav_porcentaje)
	from ProyectoAvanceDet
	where	prj_nrointerno=@prj_nrointerno and 
		pav_aamm=@pav_aamm and 
		pav_completado='S'


if @pav_calcul is null
	select @pav_calcul=0


update ProyectoAvanceCab
set pav_calcul=@pav_calcul
where	prj_nrointerno=@prj_nrointerno and 
	pav_aamm=@pav_aamm

return(0)
go

grant execute on dbo.sp_InsertProyectoAvance to GesPetUsr
go
