/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionValidacion'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionValidacion
go

create procedure dbo.sp_InsertPeticionValidacion
	@pet_nrointerno	int,
	@valid			int,
	@valitem		int,
	@valitemvalor	char(1)
as
	insert into GesPet.dbo.PeticionValidacion (
		pet_nrointerno,
		valid,
		valitem,
		valitemvalor,
		audituser,
		auditfecha)
	values (
		@pet_nrointerno,
		@valid,
		@valitem,
		@valitemvalor,
		suser_name(),
		getDate())
go

grant execute on dbo.sp_InsertPeticionValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
