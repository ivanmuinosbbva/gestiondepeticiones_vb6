use GesPet
go
print 'sp_GetProyecto'
go
if exists (select * from sysobjects where name = 'sp_GetProyecto' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyecto
go

create procedure dbo.sp_GetProyecto
	@prj_nrointerno		int=0
as

	select PRJ.prj_nrointerno,
		PRJ.titulo,
		PRJ.cod_tipoprj,
		ord_tipoprj = (select TIPRJ.secuencia from TipoPrj TIPRJ where RTRIM(TIPRJ.cod_tipoprj)=RTRIM(PRJ.cod_tipoprj)),
		nom_tipoprj = (select ESTPRJ.nom_tipoprj from TipoPrj ESTPRJ where RTRIM(ESTPRJ.cod_tipoprj)=RTRIM(PRJ.cod_tipoprj)),
		PRJ.corp_local,
		PRJ.cod_orientacion,
		PRJ.importancia_cod,
		nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PRJ.importancia_cod)),''),
		PRJ.importancia_prf,
		PRJ.importancia_usr,
		PRJ.fec_requerida,
		PRJ.cod_direccion,
		nom_direccion = (select DIRPRJ.nom_direccion from Direccion DIRPRJ where RTRIM(DIRPRJ.cod_direccion)=RTRIM(PRJ.cod_direccion)),
		PRJ.cod_gerencia,
		nom_gerencia = (select GERPRJ.nom_gerencia from Gerencia GERPRJ where RTRIM(GERPRJ.cod_gerencia)=RTRIM(PRJ.cod_gerencia)),
		PRJ.cod_sector,
		nom_sector = (select SECPRJ.nom_sector from Sector SECPRJ where RTRIM(SECPRJ.cod_sector)=RTRIM(PRJ.cod_sector)),
		PRJ.cod_solicitante,
		nom_solicitante = (select RC.nom_recurso from Recurso RC where RTRIM(RC.cod_recurso)=RTRIM(PRJ.cod_solicitante)),
		PRJ.cod_bpar,
		nom_bpar = (select REC.nom_recurso from Recurso REC where RTRIM(REC.cod_recurso)=RTRIM(PRJ.cod_bpar)),
		PRJ.prj_costo,
		PRJ.fe_ini_esti,
		PRJ.fe_fin_esti,
		PRJ.cod_estado,
		nom_estado = (select ESTPRJ.nom_estado from Estados ESTPRJ where RTRIM(ESTPRJ.cod_estado)=RTRIM(PRJ.cod_estado)),
		PRJ.fe_estado,
		PRJ.fe_ini_plan,
		PRJ.fe_fin_plan,
		PRJ.fe_ini_real,
		PRJ.fe_fin_real,
		PRJ.fe_fec_plan,
		PRJ.fe_ini_orig,
		PRJ.fe_fin_orig,
		PRJ.fe_fec_orig,
		PRJ.horaspresup,
		PRJ.cant_planif,
		PRJ.cod_usualta,
		PRJ.fec_alta,
		PRJ.ult_accion

	from
		GesPet..Proyecto PRJ
	where
		@prj_nrointerno=0 or @prj_nrointerno=PRJ.prj_nrointerno

return(0)
go


grant execute on dbo.sp_GetProyecto to GesPetUsr
go


