use GesPet
go
print 'sp_GetProyectoBalanceDet'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoBalanceDet' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoBalanceDet
go
create procedure dbo.sp_GetProyectoBalanceDet 
	@prj_nrointerno	int=0,
	@cod_BalanceRubro	char(8)=null,
	@cod_BalanceSubRubro	char(8)=null
as 

select 
	BDE.prj_nrointerno,
	BDE.cod_BalanceRubro,
	BDE.cod_BalanceSubRubro,
	BDE.secuencia as bde_secuencia,
	BSU.secuencia as bsu_secuencia,
	nom_BalanceRubro = isnull((select BRU.nom_BalanceRubro from BalanceRubro BRU where RTRIM(BRU.cod_BalanceRubro)=RTRIM(BDE.cod_BalanceRubro)),''),
	BSU.nom_BalanceSubRubro,
	BDE.bde_moneda,
	BDE.bde_estimado,
	BDE.bde_real, 
	BDE.bde_texto
from  BalanceSubRubro BSU, ProyectoBalanceDet BDE
where	@prj_nrointerno=0 or BDE.prj_nrointerno=@prj_nrointerno and
	(@cod_BalanceRubro is null or RTRIM(@cod_BalanceRubro) is null or RTRIM(@cod_BalanceRubro)='' or RTRIM(BDE.cod_BalanceRubro)=RTRIM(@cod_BalanceRubro)) and 
	(@cod_BalanceSubRubro is null or RTRIM(@cod_BalanceSubRubro) is null or RTRIM(@cod_BalanceSubRubro)='' or RTRIM(BDE.cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro)) and 
	BDE.cod_BalanceRubro=BSU.cod_BalanceRubro and
	BDE.cod_BalanceSubRubro=BSU.cod_BalanceSubRubro
order by BDE.prj_nrointerno,BDE.cod_BalanceRubro,BSU.secuencia
return(0) 
go

grant execute on dbo.sp_GetProyectoBalanceDet to GesPetUsr 
go
