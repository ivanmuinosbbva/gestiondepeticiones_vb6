use GesPet
go
print 'sp_VerPeticionHoras'
go
if exists (select * from sysobjects where name = 'sp_VerPeticionHoras' and sysstat & 7 = 4)
drop procedure dbo.sp_VerPeticionHoras
go
create procedure dbo.sp_VerPeticionHoras
	@pet_nrointerno	int
as
	select distinct pet_nrointerno
	from HorasTrabajadas
	where	pet_nrointerno=@pet_nrointerno and
		(cod_tarea is null or RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='')
return(0)
go

grant execute on dbo.sp_VerPeticionHoras to GesPetUsr
go
