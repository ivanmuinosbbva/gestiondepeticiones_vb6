/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateMotivosNoPlan'
go

if exists (select * from sysobjects where name = 'sp_UpdateMotivosNoPlan' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateMotivosNoPlan
go

create procedure dbo.sp_UpdateMotivosNoPlan
	@cod_motivo_noplan	int,
	@nom_motivo_noplan	char(50),
	@estado_hab			char(1)
as
	update 
		GesPet.dbo.MotivosNoPlan
	set
		nom_motivo_noplan = @nom_motivo_noplan,
		estado_hab		  = @estado_hab
	where 
		(cod_motivo_noplan = @cod_motivo_noplan or @cod_motivo_noplan is null)
	return(0)
go

grant execute on dbo.sp_UpdateMotivosNoPlan to GesPetUsr
go

print 'Actualización realizada.'
