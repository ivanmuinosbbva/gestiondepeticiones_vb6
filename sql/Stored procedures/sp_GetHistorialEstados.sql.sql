/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHistorialEstados'
go

if exists (select * from sysobjects where name = 'sp_GetHistorialEstados' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHistorialEstados
go

create procedure dbo.sp_GetHistorialEstados
	@pet_nrointerno		int,
	@hst_nrointerno		int
as 
	-- Sectores/Grupos
	select 
		tipo =
		case
			when h.cod_grupo is null then 'SEC'
			else 'GRU'
		end,
		h.cod_sector,
		nom_sector = (select x.nom_sector from GesPet..Sector x where x.cod_sector = h.cod_sector),
		sec_estado = (
					select y.sec_estado 
					from GesPet..Historial y 
					where 
					y.pet_nrointerno = @pet_nrointerno and
					y.cod_sector = h.cod_sector and 
					y.cod_evento in ('SNEW000','SCHGEST') and
					y.hst_nrointerno in (
						select max(w.hst_nrointerno)
						from GesPet..Historial w
						where
						w.pet_nrointerno = @pet_nrointerno and
						w.cod_sector = h.cod_sector and 
						w.cod_evento in ('SNEW000','SCHGEST') and
						w.hst_nrointerno <= @hst_nrointerno)),
		h.cod_grupo,
		nom_grupo = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = h.cod_grupo),
		gru_estado = (
					select y.gru_estado 
					from GesPet..Historial y 
					where 
					y.pet_nrointerno = @pet_nrointerno and
					y.cod_grupo = h.cod_grupo and 
					y.cod_evento in ('GNEW000','GCHGEST') and
					y.hst_nrointerno in (
						select max(w.hst_nrointerno)
						from GesPet..Historial w
						where
						w.pet_nrointerno = @pet_nrointerno and
						w.cod_grupo = h.cod_grupo and 
						w.cod_evento in ('GNEW000','GCHGEST') and
						w.hst_nrointerno <= @hst_nrointerno))
	from 
		GesPet..Historial h
	where 
		h.pet_nrointerno = @pet_nrointerno and
		h.hst_nrointerno <= @hst_nrointerno and
		h.cod_evento in ('SNEW000','GNEW000')
	order by 
		h.hst_fecha,
		h.hst_nrointerno

	return(0)
go

grant execute on dbo.sp_GetHistorialEstados to GesPetUsr
go

print 'Actualización realizada.'
go
