/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 27.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPerfil'
go

if exists (select * from sysobjects where name = 'sp_InsertPerfil' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPerfil
go

create procedure dbo.sp_InsertPerfil
	@cod_perfil	char(4),
	@nom_perfil	char(30)=null
as
	insert into GesPet.dbo.Perfil (
		cod_perfil,
		nom_perfil)
	values (
		@cod_perfil,
		@nom_perfil)
	return(0)
go

grant execute on dbo.sp_InsertPerfil to GesPetUsr
go

print 'Actualización realizada.'
