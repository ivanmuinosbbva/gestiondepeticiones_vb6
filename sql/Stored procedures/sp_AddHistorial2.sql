use GesPet
go
print 'sp_AddHistorial2'
go
if exists (select * from sysobjects where name = 'sp_AddHistorial2' and sysstat & 7 = 4)
drop procedure dbo.sp_AddHistorial2
go

create procedure dbo.sp_AddHistorial2
    @hst_nrointerno  int OUTPUT,  
    @pet_nrointerno  int,  
    @cod_evento varchar(8)=null,  
    @pet_estado varchar(6)=null,  
    @cod_sector char(8)=null,  
    @sec_estado varchar(6)=null,  
    @cod_grupo  char(8)=null,  
    @gru_estado varchar(6)=null,  
    @replc_user varchar(10)=null,
    @audit_user varchar(10)=null
as

declare @proxnumero int  
select @proxnumero = 0  
execute sp_HistorialProximo @proxnumero OUTPUT  
  
select @hst_nrointerno=@proxnumero  
    insert into GesPet..Historial  
    (pet_nrointerno,  
     hst_nrointerno,  
     cod_evento,  
     pet_estado,  
     cod_sector,  
     sec_estado,  
     cod_grupo,  
     gru_estado,  
     replc_user,  
     audit_user,  
     hst_fecha)  
    values    
    (@pet_nrointerno,  
     @hst_nrointerno,  
     @cod_evento,  
     @pet_estado,  
     @cod_sector,  
     @sec_estado,
     @cod_grupo,
     @gru_estado,
     @replc_user,
     @audit_user,
     convert(smalldatetime,convert(varchar(12),getdate())))

return(0)
go

grant Execute  on dbo.sp_AddHistorial2 to GesPetUsr 
go

