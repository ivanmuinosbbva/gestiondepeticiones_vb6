/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertInfoproi'
go

if exists (select * from sysobjects where name = 'sp_InsertInfoproi' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertInfoproi
go

create procedure dbo.sp_InsertInfoproi
	@infoprot_item		int,
	@infoprot_itemdsc	varchar(255),
	@infoprot_itemest	char(1),
	@infoprot_itemtpo	char(1),
	@infoprot_itemres	char(4),
	@infoprot_itemresp	varchar(255)
as
	insert into GesPet.dbo.Infoproi (
		infoprot_item,
		infoprot_itemdsc,
		infoprot_itemest,
		infoprot_itemtpo,
		infoprot_itemres,
		infoprot_itemresp)
	values (
		@infoprot_item,
		@infoprot_itemdsc,
		@infoprot_itemest,
		@infoprot_itemtpo,
		@infoprot_itemres,
		@infoprot_itemresp)
go

grant execute on dbo.sp_InsertInfoproi to GesPetUsr
go

print 'Actualización realizada.'
go
