/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_InsertAdjunto'
go
if exists (select * from sysobjects where name = 'sp_InsertAdjunto' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertAdjunto
go
create procedure dbo.sp_InsertAdjunto
	@pet_nrointerno	int,
	@adj_file	char(50),
	@adj_path	char(250)='',
	@adj_texto	char(250)=''
as

if exists (select adj_file from Adjunto where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file))
begin
	raiserror 30001 'El archivo ya estaba declarado como adjunto.'
	select 30001 as ErrCode , 'El archivo ya estaba declarado como adjunto.' as ErrDesc
	return (30001)
end
else
begin
	insert into Adjunto(
		pet_nrointerno,
		adj_file,
		adj_path,
		adj_texto,
		audit_user,
		audit_date )
		values(@pet_nrointerno,
		@adj_file,
		@adj_path,
		@adj_texto,
		SUSER_NAME(),
		getdate())

end
return(0)
go

grant execute on dbo.sp_InsertAdjunto to GesPetUsr
go
