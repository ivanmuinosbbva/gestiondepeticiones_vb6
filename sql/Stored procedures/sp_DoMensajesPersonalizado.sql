/*
-000- a. FJS 11.09.2008 - Nuevo SP para mandar mensajes personalizados. En este caso, se utiliza para mandar el mismo mensaje a todos 
						  los responsables de Ejecuci�n de una petici�n.
-001- a. FJS 12.11.2008 - Se agrega la condici�n para evitar el envio de los mensajes a responsables si el grupo se encuentra en alguno de los estados
						  terminales.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DoMensajesPersonalizado'
go

if exists (select * from sysobjects where name = 'sp_DoMensajesPersonalizado' and sysstat & 7 = 4)
	drop procedure dbo.sp_DoMensajesPersonalizado
go

create procedure dbo.sp_DoMensajesPersonalizado
	@pet_nrointerno		int    =null, 
	@cod_txtmsg			int    =null,
	@txt_txtmsg			varchar(250)=null
as 
	declare @cod_grupo				char(8)
	declare @cod_grupohomologador	char(8)
	declare @cod_estado				char(6)
	declare @cod_bpar				char(10)
	declare @proxnumero				int

	-- Inicializaci�n de variables 
	select @proxnumero = 0

	-- Obtengo los datos de la petici�n: Sector, estado actual, situaci�n actual, tipo de petici�n y BP  
	select   
		@cod_estado = PET.cod_estado,
		@cod_bpar	= PET.cod_bpar
	from  
		GesPet..Peticion PET
	where  
		PET.pet_nrointerno = @pet_nrointerno

	-- Obtengo el grupo homologador
	select @cod_grupohomologador = (select cod_grupo from Grupo where grupo_homologacion = 'H')

	--  GRUPOS ---------------------------------------------------------------------------------------------------------------------
	-- Cargo en un cursor todos los grupos de la petici�n
	declare CurxGrupos cursor for 
		select	 cod_grupo
		from	 GesPet..PeticionGrupo GRU
		where	 pet_nrointerno = @pet_nrointerno and 
				 charindex(cod_estado, 'RECHAZ|RECHTE|ANULAD|CANCEL|ANEXAD|TERMIN') = 0
		group by cod_grupo
		for read only

	open CurxGrupos 
		fetch CurxGrupos into @cod_grupo
		while @@sqlstatus = 0
			begin
				if @cod_grupohomologador <> @cod_grupo
					begin
						execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT
						-- Agrego los mensajes para cada responsable de ejecuci�n
						insert into 
							GesPet..Mensajes 
						values
							(@proxnumero,  
							 @pet_nrointerno,
							 getdate(),
							 'CGRU', 
							 'GRUP', 
							 @cod_grupo,		-- Es el grupo ejecutor de la petici�n
							 @cod_txtmsg,
							 @cod_estado,		-- El estado de la petici�n 
							 '',
							 @txt_txtmsg)
					end
				fetch CurxGrupos into @cod_grupo
			end
	close CurxGrupos
	deallocate cursor CurxGrupos
go

grant execute on dbo.sp_DoMensajesPersonalizado to GesPetUsr
go

print 'Actualizaci�n realizada.'
