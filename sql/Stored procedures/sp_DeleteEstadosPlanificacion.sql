/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteEstadosPlanificacion'
go

if exists (select * from sysobjects where name = 'sp_DeleteEstadosPlanificacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteEstadosPlanificacion
go

create procedure dbo.sp_DeleteEstadosPlanificacion
	@cod_estado_plan	int
as
	delete from
		GesPet.dbo.EstadosPlanificacion
	where
		(cod_estado_plan = @cod_estado_plan or @cod_estado_plan is null)
	return(0)
go

grant execute on dbo.sp_DeleteEstadosPlanificacion to GesPetUsr
go

print 'Actualización realizada.'
