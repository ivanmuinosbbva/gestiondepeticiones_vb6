use GesPet
go
print 'sp_DeleteMensaje'
go
if exists (select * from sysobjects where name = 'sp_DeleteMensaje' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteMensaje
go
create procedure dbo.sp_DeleteMensaje 
             @pet_nrointerno    int=0, 
             @msg_nrointerno    int=0 
as 
if @msg_nrointerno>0 
    delete GesPet..Mensajes 
    where  @msg_nrointerno=msg_nrointerno 
else 
    delete GesPet..Mensajes 
    where  @pet_nrointerno=pet_nrointerno 
return(0) 
go



grant execute on dbo.sp_DeleteMensaje to GesPetUsr 
go


