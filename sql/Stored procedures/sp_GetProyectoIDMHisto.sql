/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.11.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMHisto'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMHisto' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMHisto
go

create procedure dbo.sp_GetProyectoIDMHisto
	@ProjId		int=null,
	@ProjSubId	int=null,
	@ProjSubSId	int=null
as
	select 
		p.ProjId,
		p.ProjSubId,
		p.ProjSubSId,
		p.hst_nrointerno,
		p.hst_fecha,
		mem_texto = (select x.mem_texto from GesPet..ProyectoIDMHistoMemo x where x.hst_nrointerno = p.hst_nrointerno and x.mem_secuencia = 0),
		p.cod_evento,
		a.nom_accion,
		p.proj_estado,
		e.nom_estado,
		p.replc_user,
		p.audit_user,
		nom_audit = (select Ra.nom_recurso from Recurso Ra where RTRIM(Ra.cod_recurso)=RTRIM(p.audit_user)),
		nom_replc = (select Rr.nom_recurso from Recurso Rr where RTRIM(Rr.cod_recurso)=RTRIM(p.replc_user))
	from 
		GesPet..ProyectoIDMHisto p inner join
		GesPet..AccionesIDM a on (LTRIM(RTRIM(p.cod_evento)) = LTRIM(RTRIM(a.cod_accion))) inner join
		GesPet..EstadosIDM e on (LTRIM(RTRIM(p.proj_estado)) = LTRIM(RTRIM(e.cod_estado)))
	where
		(@ProjId is null or p.ProjId = @ProjId) and
		(@ProjSubId is null or p.ProjSubId = @ProjSubId) and
		(@ProjSubSId is null or p.ProjSubSId = @ProjSubSId)
	order by
		p.ProjId,
		p.ProjSubId,
		p.ProjSubSId,
		p.hst_fecha			DESC,
		p.hst_nrointerno	DESC

	return(0)
go

grant execute on dbo.sp_GetProyectoIDMHisto to GesPetUsr
go

print 'Actualización realizada.'
go
