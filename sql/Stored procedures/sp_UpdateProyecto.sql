/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'sp_UpdateProyecto'
go
if exists (select * from sysobjects where name = 'sp_UpdateProyecto' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateProyecto
go
create procedure dbo.sp_UpdateProyecto
	@prj_nrointerno		int,
	@titulo			char(100),
	@corp_local		char(1),
	@cod_orientacion	char(2)='',
	@importancia_cod	char(2)='',
	@importancia_prf	char(4)='',
	@importancia_usr	char(10)='',
	@fec_requerida		smalldatetime=null,
	@cod_direccion		char(8) ,
	@cod_gerencia		char(8) ,
	@cod_sector		char(8) ,
	@cod_solicitante	char(10),
	@cod_bpar		char(10) ,
	@prj_costo		char(1),
	@fe_ini_esti		smalldatetime=null,
	@fe_fin_esti		smalldatetime=null,
	@cod_tipoprj		char(8)

as
/*BEGIN TRAN*/

if exists (select prj_nrointerno from GesPet..Proyecto where prj_nrointerno = @prj_nrointerno)
	begin
		update GesPet..Proyecto set
		prj_nrointerno	= @prj_nrointerno,
		titulo		= @titulo,
		corp_local	= @corp_local,
		cod_orientacion	= @cod_orientacion,
		importancia_cod	= @importancia_cod,
		importancia_prf	= @importancia_prf,
		importancia_usr	= @importancia_usr,
		fec_requerida	= @fec_requerida,
		cod_direccion	= @cod_direccion,
		cod_gerencia	= @cod_gerencia,
		cod_sector	= @cod_sector,
		cod_solicitante	= @cod_solicitante,
		cod_bpar	= @cod_bpar,
		cod_tipoprj	= @cod_tipoprj,
		prj_costo	= @prj_costo,
		fe_ini_esti	= @fe_ini_esti,
		fe_fin_esti	= @fe_fin_esti,
		audit_user	= SuseR_NAME(),
		audit_date	= getdate()
		where prj_nrointerno = @prj_nrointerno
	end

else
	begin
		select 30010 as ErrCode , 'Error al intentar guardar el Proyecto' as ErrDesc
		raiserror  30010 'Existen Problemas guardar el Proyecto'
		return (30010)
end

select @prj_nrointerno as valor_clave
/*COMMIT TRAN*/
return(0)
go

grant execute on dbo.sp_UpdateProyecto to GesPetUsr
go


