/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 15.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeriodoEstado'
go

if exists (select * from sysobjects where name = 'sp_DeletePeriodoEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeriodoEstado
go

create procedure dbo.sp_DeletePeriodoEstado
	@cod_estado_per	char(8)
as
	delete from
		GesPet.dbo.PeriodoEstado
	where
		cod_estado_per = @cod_estado_per
	return(0)
go

grant execute on dbo.sp_DeletePeriodoEstado to GesPetUsr
go

print 'Actualización realizada.'
