/*
-000- a. FJS 02.06.2008 - Nuevo SP basado en el sp_GetRecursoActuanteArea original para mantener la funcionalidad del
						  reporte de "Perfiles actuantes por �rea" original.
-001- a. FJS 14.04.2009 - Arreglo: cuando se piden todos los perfiles, no est� respentando las �reas solicitadas.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoActuanteAreaRpt'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoActuanteAreaRpt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoActuanteAreaRpt
go

create procedure dbo.sp_GetRecursoActuanteAreaRpt 
    @cod_direccion      varchar(8)='NULL', 
    @cod_gerencia       varchar(8)='NULL', 
    @cod_sector         varchar(8)='NULL', 
    @cod_grupo          varchar(8)='NULL', 
    @perfil				varchar(8)='NULL' 
as 
	CREATE TABLE #TmpSalida( 
			cod_direccion char(10) null, 
			nom_direccion char(40) null, 
			cod_gerencia char(10) null, 
			nom_gerencia char(40) null, 
			cod_sector char(10) null, 
			nom_sector char(40) null, 
			cod_grupo char(10) null, 
			nom_grupo char(40) null, 
			cod_recurso  char(10) null, 
			nom_recurso char(50) null, 
			cod_perfil char(5) null, 
			nom_perfil char(40) null, 
			cod_nivel char(5) null, 
			cod_area char(8) null, 
			nom_area char(40) null 
	) 
	create  nonclustered index idx_Tmp1 on #TmpSalida (cod_recurso,cod_perfil) 
	create  nonclustered index idx_Tmp2 on #TmpSalida (cod_direccion,cod_gerencia,cod_sector,cod_grupo,cod_recurso,cod_perfil) 

		-- ************************************************************************************************************************************************************************
		-- Todo BBVA
		-- ************************************************************************************************************************************************************************
		insert #TmpSalida 
			select 
				'   ', 
				'  Todo el BBVA', 
				'   ', 
				'', 
				'   ', 
				'', 
				'', 
				'', 
				Rp.cod_recurso, 
				Re.nom_recurso, 
				Rp.cod_perfil, 
				Pe.nom_perfil, 
				'BBVA', 
				'BBVA', 
				'  Todo el BBVA' 
			from 
				GesPet..RecursoPerfil Rp,  
				GesPet..Recurso Re,  
				GesPet..Perfil Pe 
			where   
				(@cod_direccion = 'NULL' and @cod_gerencia ='NULL' and @cod_sector ='NULL' and @cod_grupo ='NULL') and	-- add -001- a.
				(RTRIM(@perfil)='NULL' or RTRIM(Rp.cod_perfil) = RTRIM(@perfil)) and 
				(RTRIM(Rp.cod_recurso) = RTRIM(Re.cod_recurso)) and 
				(RTRIM(Re.estado_recurso) = 'A') and  
				(not exists (select 1 from #TmpSalida Tx where RTRIM(Rp.cod_recurso)=RTRIM(Tx.cod_recurso) and RTRIM(Rp.cod_perfil)=RTRIM(Tx.cod_perfil))) and 
				(Rp.cod_nivel='BBVA') and 
				(RTRIM(Rp.cod_perfil) *= RTRIM(Pe.cod_perfil)) 
	 
		-- ************************************************************************************************************************************************************************
		-- Direcci�n
		-- ************************************************************************************************************************************************************************
		insert #TmpSalida 
			select 
				Di.cod_direccion, 
				Di.nom_direccion, 
				'', 
				'', 
				'', 
				'', 
				'', 
				'', 
				Rp.cod_recurso, 
				Re.nom_recurso, 
				Rp.cod_perfil, 
				Pe.nom_perfil, 
				Rp.cod_nivel, 
				Rp.cod_area, 
				Di.nom_direccion 
			from 
				GesPet..RecursoPerfil Rp, 
				GesPet..Recurso Re, 
				GesPet..Direccion Di,  
				GesPet..Perfil Pe 
			where   
				(RTRIM(@perfil)='NULL' or RTRIM(Rp.cod_perfil) = RTRIM(@perfil)) and 
				(RTRIM(Rp.cod_recurso) = RTRIM(Re.cod_recurso)) and 
				(RTRIM(Re.estado_recurso) = 'A') and  
				(not exists (select 1 from #TmpSalida Tx where RTRIM(Rp.cod_recurso)=RTRIM(Tx.cod_recurso) and RTRIM(Rp.cod_perfil)=RTRIM(Tx.cod_perfil))) and 
				((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion) = RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') and 
				((Rp.cod_nivel='DIRE' and RTRIM(Di.cod_direccion)=RTRIM(Rp.cod_area))) and 
				(RTRIM(Rp.cod_perfil) *= RTRIM(Pe.cod_perfil)) 

		-- ************************************************************************************************************************************************************************
		-- Gerencia
		-- ************************************************************************************************************************************************************************
		insert #TmpSalida 
		select  
			Di.cod_direccion, 
			Di.nom_direccion, 
			Ge.cod_gerencia, 
			Ge.nom_gerencia, 
			'', 
			'', 
			'', 
			'', 
			Rp.cod_recurso, 
			Re.nom_recurso, 
			Rp.cod_perfil, 
			Pe.nom_perfil, 
			Rp.cod_nivel, 
			Rp.cod_area, 
			Ge.nom_gerencia 
		from 
			GesPet..RecursoPerfil Rp, 
			GesPet..Recurso Re,  
			GesPet..Direccion Di, 
			GesPet..Gerencia Ge,  
			GesPet..Perfil Pe 
		where   
			(RTRIM(@perfil)='NULL' or RTRIM(Rp.cod_perfil) = RTRIM(@perfil)) and 
			(RTRIM(Rp.cod_recurso) = RTRIM(Re.cod_recurso)) and 
			(RTRIM(Re.estado_recurso) = 'A') and  
			(not exists (select 1 from #TmpSalida Tx where RTRIM(Rp.cod_recurso)=RTRIM(Tx.cod_recurso) and RTRIM(Rp.cod_perfil)=RTRIM(Tx.cod_perfil))) and 
			((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion)=RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') and 
			(((RTRIM(@cod_gerencia)='NULL' and RTRIM(Di.cod_direccion)=RTRIM(Ge.cod_direccion))or RTRIM(Ge.cod_gerencia)=RTRIM(@cod_gerencia)) and ISNULL(Ge.flg_habil,'S')<>'N') and  
			((Rp.cod_nivel='DIRE' and RTRIM(Di.cod_direccion)=RTRIM(Rp.cod_area)) or 
			(Rp.cod_nivel='GERE' and RTRIM(Ge.cod_gerencia)=RTRIM(Rp.cod_area))) and 
			(RTRIM(Rp.cod_perfil) *= RTRIM(Pe.cod_perfil)) 

		-- ************************************************************************************************************************************************************************
		-- Sector
		-- ************************************************************************************************************************************************************************
		insert #TmpSalida 
		select  
			Di.cod_direccion, 
			Di.nom_direccion, 
			Ge.cod_gerencia, 
			Ge.nom_gerencia, 
			Gr.cod_sector, 
			Gr.nom_sector, 
			'', 
			'', 
			Rp.cod_recurso, 
			Re.nom_recurso, 
			Rp.cod_perfil, 
			Pe.nom_perfil, 
			Rp.cod_nivel, 
			Rp.cod_area, 
			Gr.nom_sector 
		from 
			GesPet..RecursoPerfil Rp, 
			GesPet..Recurso Re,  
			GesPet..Direccion Di, 
			GesPet..Gerencia Ge, 
			GesPet..Sector Gr,  
			GesPet..Perfil Pe 
		where   
			(RTRIM(@perfil)='NULL' or RTRIM(Rp.cod_perfil) = RTRIM(@perfil)) and 
			(RTRIM(Rp.cod_recurso) = RTRIM(Re.cod_recurso)) and 
			(RTRIM(Re.estado_recurso) = 'A') and  
			(not exists (select 1 from #TmpSalida Tx where RTRIM(Rp.cod_recurso)=RTRIM(Tx.cod_recurso) and RTRIM(Rp.cod_perfil)=RTRIM(Tx.cod_perfil))) and 
			((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion)=RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') and 
			(((RTRIM(@cod_gerencia)='NULL' and RTRIM(Ge.cod_direccion)=RTRIM(Di.cod_direccion)) or RTRIM(Ge.cod_gerencia)=RTRIM(@cod_gerencia)) and ISNULL(Ge.flg_habil,'S')<>'N') and  
			(((RTRIM(@cod_sector)='NULL' and RTRIM(Gr.cod_gerencia)=RTRIM(Ge.cod_gerencia)) or RTRIM(Gr.cod_sector)=RTRIM(@cod_sector)) and ISNULL(Gr.flg_habil,'S')<>'N') and 
			((Rp.cod_nivel='DIRE' and RTRIM(Di.cod_direccion)=RTRIM(Rp.cod_area)) or 
			(Rp.cod_nivel='GERE' and RTRIM(Ge.cod_gerencia)=RTRIM(Rp.cod_area)) or 
			(Rp.cod_nivel='SECT' and RTRIM(Gr.cod_sector)=RTRIM(Rp.cod_area))) and 
			(RTRIM(Rp.cod_perfil) *= RTRIM(Pe.cod_perfil)) 
		
		-- ************************************************************************************************************************************************************************
		-- Grupo
		-- ************************************************************************************************************************************************************************
		insert #TmpSalida 
		select  
			Di.cod_direccion, 
			Di.nom_direccion, 
			Ge.cod_gerencia, 
			Ge.nom_gerencia, 
			Gr.cod_sector, 
			Gr.nom_sector, 
			Su.cod_grupo, 
			Su.nom_grupo, 
			Rp.cod_recurso, 
			Re.nom_recurso, 
			Rp.cod_perfil, 
			Pe.nom_perfil, 
			Rp.cod_nivel, 
			Rp.cod_area, 
			Su.nom_grupo 
		from 
			GesPet..RecursoPerfil Rp, 
			GesPet..Recurso Re, 
			GesPet..Direccion Di, 
			GesPet..Gerencia Ge, 
			GesPet..Sector Gr, 
			GesPet..Grupo Su, 
			GesPet..Perfil Pe 
		where   
			(RTRIM(@perfil)='NULL' or RTRIM(Rp.cod_perfil) = RTRIM(@perfil)) and 
			(RTRIM(Rp.cod_recurso) = RTRIM(Re.cod_recurso)) and 
			(RTRIM(Re.estado_recurso) = 'A') and  
			(not exists (select 1 from #TmpSalida Tx where RTRIM(Rp.cod_recurso)=RTRIM(Tx.cod_recurso) and RTRIM(Rp.cod_perfil)=RTRIM(Tx.cod_perfil))) and 
			((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion)=RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') and 
			(((RTRIM(@cod_gerencia)='NULL' and RTRIM(Ge.cod_direccion)=RTRIM(Di.cod_direccion)) or RTRIM(Ge.cod_gerencia)=RTRIM(@cod_gerencia)) and ISNULL(Ge.flg_habil,'S')<>'N') and  
			(((RTRIM(@cod_sector)='NULL' and RTRIM(Gr.cod_gerencia)=RTRIM(Ge.cod_gerencia)) or RTRIM(Gr.cod_sector)=RTRIM(@cod_sector)) and ISNULL(Gr.flg_habil,'S')<>'N') and 
			(((RTRIM(@cod_grupo)='NULL' and RTRIM(Su.cod_sector)=RTRIM(Gr.cod_sector)) or RTRIM(Su.cod_grupo)=RTRIM(@cod_grupo)) and ISNULL(Su.flg_habil,'S')<>'N') and  
			((Rp.cod_nivel='DIRE' and RTRIM(Di.cod_direccion)=RTRIM(Rp.cod_area)) or 
			(Rp.cod_nivel='GERE' and RTRIM(Ge.cod_gerencia)=RTRIM(Rp.cod_area)) or 
			(Rp.cod_nivel='SECT' and RTRIM(Gr.cod_sector)=RTRIM(Rp.cod_area)) or 
			(Rp.cod_nivel='GRUP' and RTRIM(Su.cod_grupo)=RTRIM(Rp.cod_area))) and 
			(RTRIM(Rp.cod_perfil) *= RTRIM(Pe.cod_perfil)) 
		 
	update  #TmpSalida 
	set nom_gerencia = ' � ' + RTRIM(nom_gerencia) 
	where (RTRIM(nom_gerencia) is not null and RTRIM(nom_gerencia)<>'') 
	 
	update  #TmpSalida 
	set nom_sector = ' � ' + RTRIM(nom_sector) 
	where (RTRIM(nom_sector) is not null and RTRIM(nom_sector)<>'') 
	 
	update  #TmpSalida 
	set nom_grupo = ' � ' + RTRIM(nom_grupo) 
	where (RTRIM(nom_grupo) is not null and RTRIM(nom_grupo)<>'') 
		 
	select   
		RTRIM(Ts.nom_direccion) + RTRIM(Ts.nom_gerencia) + RTRIM(Ts.nom_sector) + RTRIM(Ts.nom_grupo) as nom_sector, 
		Ts.cod_direccion, 
		Ts.cod_gerencia, 
		Ts.cod_sector, 
		Ts.cod_grupo, 
		Ts.cod_recurso, 
		Ts.nom_recurso, 
		Ts.cod_perfil, 
		Ts.nom_perfil, 
		Ts.cod_nivel, 
		Ts.cod_area, 
		Ts.nom_area 
	from 
		#TmpSalida Ts 
	order by 
		Ts.cod_direccion, 
		Ts.cod_gerencia, 
		Ts.cod_sector, 
		Ts.cod_grupo,
		Ts.nom_recurso 

	return(0) 
go

grant execute on dbo.sp_GetRecursoActuanteAreaRpt to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go