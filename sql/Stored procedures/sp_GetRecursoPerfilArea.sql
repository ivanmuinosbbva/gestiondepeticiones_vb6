use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoPerfilArea'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoPerfilArea' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoPerfilArea
go

create procedure dbo.sp_GetRecursoPerfilArea
	@cod_recurso	char(10)=null,
	@cod_perfil		char(4)=null,
	@nivel			char(4), 
	@area			char(8) 
as
	begin
		select
			a.cod_recurso,
			a.nom_recurso
		from
			GesPet..Recurso a inner join GesPet..RecursoPerfil b on (a.cod_recurso = b.cod_recurso and (@cod_perfil is null or RTRIM(b.cod_perfil) = RTRIM(@cod_perfil)))
		where
			(a.cod_recurso = @cod_recurso or @cod_recurso is null) and
			((a.cod_recurso = @cod_recurso or @cod_recurso is null) and @nivel = 'BBVA') or
			((a.cod_recurso = @cod_recurso or @cod_recurso is null) and @nivel = 'DIRE' and RTRIM(a.cod_direccion) = RTRIM(@area) or @area is null) or
			((a.cod_recurso = @cod_recurso or @cod_recurso is null) and @nivel = 'GERE' and RTRIM(a.cod_gerencia) = RTRIM(@area) or @area is null) or
			((a.cod_recurso = @cod_recurso or @cod_recurso is null) and @nivel = 'SECT' and RTRIM(a.cod_sector) = RTRIM(@area) or @area is null) or
			((a.cod_recurso = @cod_recurso or @cod_recurso is null) and @nivel = 'GRUP' and RTRIM(a.cod_grupo) = RTRIM(@area) or @area is null)
	end
return(0)
go

grant execute on dbo.sp_GetRecursoPerfilArea to GesPetUsr
go

print 'Actualización realizada.'
go
