/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT002'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT002' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT002
go

create procedure dbo.sp_UpdateHEDT002
	@dsn_id		char(8)=null,
	@cpy_id		char(8)=null,
	@cpy_dsc	char(50)=null,
	@cpy_nomrut	char(8)=null
as
	update 
		GesPet.dbo.HEDT002
	set
		dsn_id		= @dsn_id,
		cpy_id		= @cpy_id,
		cpy_dsc		= @cpy_dsc,
		cpy_nomrut  = @cpy_nomrut
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)
go

grant execute on dbo.sp_UpdateHEDT002 to GesPetUsr
go

print 'Actualización realizada.'
