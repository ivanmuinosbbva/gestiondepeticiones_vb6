/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdMotivosReplanField'
go

if exists (select * from sysobjects where name = 'sp_UpdMotivosReplanField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdMotivosReplanField
go

create procedure dbo.sp_UpdMotivosReplanField
	@cod_motivo_replan	int,
	@campo				char(80),
	@valortxt			char(255),
	@valordate			smalldatetime,
	@valornum			int
as
	if rtrim(@campo)='COD_MOTIVO_REPLAN'
		update 
			GesPet.dbo.MotivosReplan
		set
			cod_motivo_replan = @valornum
		where 
			(cod_motivo_replan = @cod_motivo_replan or @cod_motivo_replan is null)

	if rtrim(@campo)='NOM_MOTIVO_REPLAN'
		update 
			GesPet.dbo.MotivosReplan
		set
			nom_motivo_replan = @valortxt
		where 
			(cod_motivo_replan = @cod_motivo_replan or @cod_motivo_replan is null)

	if rtrim(@campo)='ESTADO_HAB'
		update 
			GesPet.dbo.MotivosReplan
		set
			estado_hab = @valortxt
		where 
			(cod_motivo_replan = @cod_motivo_replan or @cod_motivo_replan is null)
	return(0)
go

grant execute on dbo.sp_UpdMotivosReplanField to GesPetUsr
go

print 'Actualización realizada.'
