/*
Collation Name para el order: "Latin-1 Spanish no case (56)" (Sortkey)

-001- a. FJS 20.05.2010 - Nuevo SP para el sistema IGM: Gerencias del CGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_Gerencia'
go

if exists (select * from sysobjects where name = 'sp_IGM_Gerencia' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_Gerencia
go

create procedure dbo.sp_IGM_Gerencia
	@cod_gerencia	char(8)=null,
	@cod_direccion	char(8)=null,
	@habil			char(1)=null
as 
	select
		a.cod_gerencia	as 'codigo',
		a.nom_gerencia	as 'nombre',
		a.cod_direccion	as 'direccion',
		a.flg_habil		as 'habil',
		a.IGM_hab
	from
		GesPet..Gerencia a
	where
		(@cod_gerencia is null or a.cod_gerencia = @cod_gerencia) and
		(@cod_direccion is null or a.cod_direccion = @cod_direccion) and
		(@habil is null or a.flg_habil = @habil)
	order by
		sortkey(a.nom_gerencia,56)
	return(0)
go

grant execute on dbo.sp_IGM_Gerencia to GesPetUsr
go

grant execute on dbo.sp_IGM_Gerencia to GrpTrnIGM
go

print 'Actualización realizada.'
go