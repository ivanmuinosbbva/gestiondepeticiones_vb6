/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/* DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-000- a. FJS 27.10.2009 - Este SP se utiliza para grabar los datos del visado por parte de Seguridad Informática.
-001- a. FJS 16.12.2009 - Se modifica para permitir el "desvisado" de archivos por parte de usuarios de seguridad informática.
-002- a. FJS 17.06.2010 - Se agregan los nuevos datos para el visado por parte de SI.
-003- a, FJS 24.06.2011 - Se agrega el parámetro de fecha/hora (STRING) y recurso (STRING) para identificar las actualizaciones.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT101'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT101' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT101
go

create procedure dbo.sp_UpdateHEDT101
	@dsn_id			char(8),
	@dsn_vb_id		char(1),
	@dsn_txt		char(255),
	@dsn_dtf		char(15),
	@cod_recurso	char(10)
as
	update 
		GesPet.dbo.HEDT001
	set
		dsn_vb		  = @dsn_vb_id,
		dsn_vb_fe     = convert(smalldatetime,@dsn_dtf),
		dsn_vb_userid = @cod_recurso,
		dsn_txt       = RTRIM(LTRIM(@dsn_txt))
	where 
		LTRIM(RTRIM(dsn_id)) = LTRIM(RTRIM(@dsn_id)) 
go

grant execute on dbo.sp_UpdateHEDT101 to GesPetUsr
go

print 'Actualización realizada.'
go

--dsn_vb_fe     = getdate(),
--dsn_vb_userid = SUSER_NAME(),