/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteRptdet'
go

if exists (select * from sysobjects where name = 'sp_DeleteRptdet' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteRptdet
go

create procedure dbo.sp_DeleteRptdet
	@rptid		char(10),
	@rptitem	int
as
	delete from GesPet.dbo.Rptdet
	where rptid = @rptid and rptitem = @rptitem

go

grant execute on dbo.sp_DeleteRptdet to GesPetUsr
go

print 'Actualización realizada.'
go
