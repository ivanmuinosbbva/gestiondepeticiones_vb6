/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.05.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMGrupos'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMGrupos' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMGrupos
go

create procedure dbo.sp_GetProyectoIDMGrupos
	@ProjId		int=null,
	@ProjSubId	int=null,
	@ProjSubSId	int=null,
	@cod_sector	char(8)=null,
	@cod_grupo	char(8)=null
as
	select
		pg.cod_sector,
		s.nom_sector,
		pg.cod_grupo,
		g.nom_grupo,
		isnull(e.nom_estado,'-') as nom_estado,
		p.pet_nrointerno,
		p.pet_nroasignado
	from
		GesPet..Peticion p inner join
		GesPet..PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno) inner join
		GesPet..Sector s on (pg.cod_sector = s.cod_sector) inner join
		GesPet..Grupo g on (pg.cod_grupo = g.cod_grupo) left join
		GesPet..Estados e on (pg.cod_estado = e.cod_estado)
	where
		p.pet_projid = @ProjId and 
		p.pet_projsubid = @ProjSubId and 
		p.pet_projsubsid = @ProjSubSId and
		(@cod_sector is null or pg.cod_sector = @cod_sector) and
		(@cod_grupo is null or pg.cod_grupo = @cod_grupo)
	group by
		pg.cod_sector,
		s.nom_sector,
		pg.cod_grupo,
		g.nom_grupo,
		e.nom_estado,
		p.pet_nrointerno,
		p.pet_nroasignado
	order by
		pg.cod_sector,
		pg.cod_grupo
go

grant execute on dbo.sp_GetProyectoIDMGrupos to GesPetUsr
go

print 'Actualización realizada.'
go
