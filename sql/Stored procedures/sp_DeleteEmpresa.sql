/*
-000- a. FJS 28.10.2010 - SPs para el mantenimiento de la tabla Empresa.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteEmpresa'
go

if exists (select * from sysobjects where name = 'sp_DeleteEmpresa' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteEmpresa
go

create procedure dbo.sp_DeleteEmpresa
	@empid		int
as
	DELETE 
	FROM GesPet.dbo.Empresa
	WHERE 
		empid = @empid
	return(0)
go

grant execute on dbo.sp_DeleteEmpresa to GesPetUsr
go

print 'Actualización realizada.'
go