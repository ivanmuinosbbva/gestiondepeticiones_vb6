use GesPet
go
print 'sp_CleanEscalamiento'
go
if exists (select * from sysobjects where name = 'sp_CleanEscalamiento' and sysstat & 7 = 4)
drop procedure dbo.sp_CleanEscalamiento
go
create procedure dbo.sp_CleanEscalamiento 
as 
 
 
update  GesPet..Peticion 
set cod_situacion = '' 
where   RTRIM(cod_situacion) = 'DEMOR1' or RTRIM(cod_situacion) = 'DEMOR2' 
 
update  GesPet..Peticion 
set ult_accion = '' 
 
update  GesPet..PeticionSector 
set cod_situacion = '' 
where   RTRIM(cod_situacion) = 'DEMOR1' or RTRIM(cod_situacion) = 'DEMOR2' 
 
update  GesPet..PeticionSector 
set ult_accion = '' 
 
update  GesPet..PeticionGrupo 
set cod_situacion = '' 
where   RTRIM(cod_situacion) = 'DEMOR1' or RTRIM(cod_situacion) = 'DEMOR2' 
 
update  GesPet..PeticionGrupo 
set ult_accion = '' 
 
 
return(0) 
go



grant execute on dbo.sp_CleanEscalamiento to GesPetUsr 
go


