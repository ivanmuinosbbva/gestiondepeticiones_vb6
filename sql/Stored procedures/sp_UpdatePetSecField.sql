/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_UpdatePetSecField'
go
if exists (select * from sysobjects where name = 'sp_UpdatePetSecField' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePetSecField
go
create procedure dbo.sp_UpdatePetSecField   
    @pet_nrointerno     int,   
    @cod_sector     char(8)=null, 
    @campo            char(15),   
    @valortxt         char(10)=null,   
    @valordate        smalldatetime=null,   
    @valornum         int=null   
as   
declare @cod_estado char(6)

if RTRIM(@campo)='FECHAIRL'   
    update PeticionSector  
        set fe_ini_real = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  pet_nrointerno = @pet_nrointerno    and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) 
if RTRIM(@campo)='FECHAFRL'   
    update PeticionSector  
        set fe_fin_real = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  pet_nrointerno = @pet_nrointerno    and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) 
if RTRIM(@campo)='FECHAIPN'   
    update PeticionSector  
        set fe_ini_plan = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  pet_nrointerno = @pet_nrointerno    and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) 
if RTRIM(@campo)='FECHAFPN'   
    update PeticionSector  
        set fe_fin_plan = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  pet_nrointerno = @pet_nrointerno    and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) 
if RTRIM(@campo)='ESTADO'   
BEGIN
	select @cod_estado=cod_estado
	from	PeticionSector
	where pet_nrointerno = @pet_nrointerno and RTRIM(cod_sector) = RTRIM(@cod_sector)
	if RTRIM(@cod_estado)<>RTRIM(@valortxt)
	begin
	    update PeticionSector  
		set cod_estado = @valortxt,   
		    hst_nrointerno_sol = 0,
		    fe_estado = getdate(), 
		    audit_user = SUSER_NAME(),   
		    audit_date = getdate()   
		where  pet_nrointerno = @pet_nrointerno    and 
		RTRIM(cod_sector) = RTRIM(@cod_sector)
	end
END
if RTRIM(@campo)='SITUAC'   
        update PeticionSector  
        set cod_situacion = @valortxt,   
            ult_accion  = '', 
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  pet_nrointerno = @pet_nrointerno  and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) 
if RTRIM(@campo)='ULTACC'   
        update PeticionSector  
        set ult_accion = @valortxt,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  pet_nrointerno = @pet_nrointerno  and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) 
 if RTRIM(@campo)='HSTSOL'   
        update PeticionSector  
        set hst_nrointerno_sol = @valornum,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  pet_nrointerno = @pet_nrointerno  and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) 

 
return(0)   
go



grant execute on dbo.sp_UpdatePetSecField to GesPetUsr 
go


