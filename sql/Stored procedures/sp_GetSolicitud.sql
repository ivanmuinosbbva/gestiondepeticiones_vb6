/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
-001- a. FJS 08.09.2009 - Se agregan dos nuevas columnas para saber el resultado de la transmisi�n.
-002- a. FJS 08.10.2009 - Se agregan nuevos campos al resultado.
-003- a. FJS 12.11.2009 - Se completan las leyendas.
-004- a. FJS 28.12.2009 - Se adapta la consulta para reflejar solicitudes con restore de archivos a fecha.
-005- a, FJS 20.01.2010 - Nuevo: Solicitudes de ejecuci�n en diferido.
-006- a. FJS 06.07.2010 - Se actualiza el nombre descriptivo del estado E porque trae confusi�n al usuario.
-007- a. AA  24.11.2010 - Se agrega parametros de entrada para la validaci�n del enmascaramiento. 
-007- b. AA  24.11.2010 - Se agrega parametros de entrada para la validaci�n del rango de fechas.
-008- a. FJS 27.04.2014 - Nuevo: se agrega campo de solicitud de EMErgencia.
-009- a. FJS 29.07.2014 - Nuevo: se agrega la tabla con las descripciones de los tipos de solicitud.
-010- a. FJS 22.08.2014 - Nuevo: se agregan 3 nuevos campos: sol_file_inter, sol_bckuptabla, sol_bckupjob.
-011- a. FJS 03.09.2014 - Nuevo: se agrega el nombre del archivo usado para generar la solicitud.



Estados de una solicitud:
*************************

A: Pendiente aprobaci�n N4 (L�der)
B: Pendiente aprobaci�n N3 (Subgerente)
C: Aprobado
D: Aprobado y en proceso de envio
E: Enviado y finalizado
F: En espera de Restore
G: En espera (ejecuci�n diferida)



********************************************************************
REGLA PARA MOSTRAR LOS ARCHIVOS
********************************************************************

XCOM
****

C / RESTORE:

sol_file_prod		ARCHIVO DESARROLLO (DYD)
sol_file_desa		ARCHIVO DESARROLLO
sol_file_out		ARCHIVO PRODUCCION
sol_fe_auto3		FECHA DE BACKUP
sol_file_inter		NULL
sol_bckuptabla		NULL
sol_bckupjob		NULL
sol_nrotabla		NULL


S / RESTORE:

sol_file_prod		ARCHIVO PRODUCCION
sol_file_desa		ARCHIVO DESARROLLO
sol_file_out		NULL
sol_fe_auto3		NULL
sol_file_inter		NULL
sol_bckuptabla		NULL
sol_bckupjob		NULL
sol_nrotabla		NULL


UNLO
****

C / RESTORE:

sol_file_prod		???
sol_file_desa		???
sol_file_out		ARCHIVO SALIDA
sol_fe_auto3		FECHA DE BACKUP
sol_file_inter		ARCHIVO SALIDA (DYD)
sol_bckuptabla		TABLA DE BACKUP
sol_bckupjob		JOB DE BACKUP
sol_nrotabla		NULL


S / RESTORE:

sol_file_prod		NULL
sol_file_desa		NULL
sol_file_out		ARCHIVO SALIDA
sol_fe_auto3		NULL
sol_file_inter		NULL
sol_bckuptabla		NULL
sol_bckupjob		NULL
sol_nrotabla		NULL


TCOR
****

sol_file_prod		NULL
sol_file_desa		NULL
sol_file_out		ARCHIVO DESARROLLO
sol_fe_auto3		NULL
sol_file_inter		NULL
sol_bckuptabla		NULL
sol_bckupjob		NULL
sol_nrotabla		NRO. DE TABLA A EXTRAER


SORT
****

C / RESTORE:

sol_file_prod		ARCHIVO PRODUCCION
sol_file_desa		ARCHIVO INTERMEDIO (DYD)
sol_file_out		ARCHIVO DESARROLLO
sol_fe_auto3		FECHA DE BACKUP
sol_file_inter		NULL
sol_bckuptabla		NULL
sol_bckupjob		NULL
sol_nrotabla		NULL



*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSolicitud'
go

if exists (select * from sysobjects where name = 'sp_GetSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSolicitud
go

create procedure dbo.sp_GetSolicitud
	@cod_recurso		char(10)=null,
	@sol_tipo			char(1)=null,
	@sol_nroasignado	int=null,
	@sol_estado			char(1)=null,
	@sol_mask			char(1)=null,				-- add -007- a.
	@sol_fechaini		smalldatetime=null,			-- add -007- b.
	@sol_fechafin		smalldatetime=null			-- add -007- b.
as
	select 
		a.sol_nroasignado,
		a.sol_tipo as sol_tipocod,																		-- upd -009- a.
		sol_tipo = (select x.nom_tipo from GesPet..TipoSolicitud x where x.cod_tipo = a.sol_tipo),		-- add -009- a.
		/* { del -009- a.
		sol_tipo
		case
			when a.sol_tipo = '1' then 'XCOM'
			when a.sol_tipo = '2' then 'UNLO'
			when a.sol_tipo = '3' then 'TCOR'
			when a.sol_tipo = '4' then 'SORT'
			when a.sol_tipo = '5' then 'XCOM*'		-- * (Restore)
			when a.sol_tipo = '6' then 'SORT*'		-- * (Restore)
		end,
		} */
		a.sol_mask,
		a.sol_eme,			-- add -008- a.
		a.sol_fecha,
		a.sol_recurso,
		nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
		mail_recurso = (select x.email from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
		a.pet_nrointerno,
		pet_nroasignado = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		pet_titulo = (select substring(x.titulo,1,50) from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		a.sol_resp_ejec,
		nom_resp_ejec = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec),
		mail_resp_ejec = (select x.email from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec),			-- add -008- a.
		a.sol_resp_sect,
		nom_resp_sect = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect),
		mail_resp_sect = (select x.email from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect),
		a.sol_seguridad,
		nom_sol_seguridad = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_seguridad),
		a.jus_codigo,
		jus_desc = (select x.jus_descripcion from GesPet..Justificativos x where x.jus_codigo = a.jus_codigo),
		a.sol_texto,
		a.sol_fe_auto1,
		a.sol_fe_auto2,
		a.sol_fe_auto3,
		a.sol_file_prod,
		a.sol_file_desa,
		a.sol_file_out,
		/*
		file_prod = (case
			when a.sol_tipo in ('2','3') then a.sol_file_out
			else a.sol_file_prod
		end),
		file_desa = (case
			when a.sol_tipo in ('2','3') then '-'
			when a.sol_tipo in ('4','6') then a.sol_file_out
			else a.sol_file_desa
		end),
		*/
		a.sol_lib_Sysin,
		a.sol_mem_Sysin,
		a.sol_join,
		a.sol_nrotabla,
		a.fe_formato,
		a.sol_estado,
		nom_estado = (select x.nom_estado from GesPet..EstadoSolicitud x where x.cod_estado = a.sol_estado),
		/* { del -008- a.
		nom_estado = 
		case
			when a.sol_estado = 'A' then 'Pendiente de aprobaci�n'
			when a.sol_estado = 'B' then 'Pendiente de aprobaci�n SI'
			when a.sol_estado = 'C' then 'Aprobado'
			when a.sol_estado = 'D' then 'Aprobado y en proceso de envio'
			when a.sol_estado = 'E' then 'Enviado'							-- upd -006- a.
			when a.sol_estado = 'F' then 'En espera de Restore'				-- add -004- a.
			when a.sol_estado = 'G' then 'En espera (ejecuci�n diferida)'	-- add -005- a.
		end,
		b.cod_grupo,
		b.cod_sector,
		b.cod_gerencia,
		b.cod_direccion,
		} */
		aprobado = 
		case
			/* { del -008- a.
			when a.sol_fe_auto1 is not null then 'L�der: ' + rtrim((select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec)) + ' el d�a ' + rtrim(convert(char(30), a.sol_fe_auto1,103))
			when a.sol_fe_auto2 is not null then 'Supervisor: ' + rtrim((select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect)) + ' el d�a ' + rtrim(convert(char(30), a.sol_fe_auto2,103))
			--when a.sol_fe_auto3 is not null then 'Seg. Inf.: ' + rtrim((select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_seguridad)) + ' el d�a ' + rtrim(convert(char(30), a.sol_fe_auto3,103))	-- del -004- a.
			when a.sol_mask = 'N' then 'Requiere aprobaci�n de nivel III'
			else 'No requiere aprobaci�n'									-- add -003- a.
			} */
			when a.sol_eme = 'S' and a.sol_mask = 'N' and LTRIM(RTRIM(a.sol_recurso)) <> LTRIM(RTRIM(a.sol_resp_sect)) then 'Requiere aprobaci�n nivel 3'
			when a.sol_eme = 'N' and a.sol_mask = 'N' and LTRIM(RTRIM(a.sol_recurso)) = LTRIM(RTRIM(a.sol_resp_ejec)) then 'Requiere aprobaci�n nivel 3'
			when a.sol_eme = 'N' and a.sol_mask = 'N' then 'Requiere aprobaci�n nivel 4 y 3'
			when a.sol_eme = 'N' and a.sol_mask = 'S' then 'No requiere aprobaci�n'
			else 'No requiere aprobaci�n'
		end
		,sol_trnm_fe
		,sol_trnm_usr
		,sol_prod_cpybbl
		,sol_prod_cpynom
		,dsn_id
		,dsn_nom = (select x.dsn_nom from GesPet..HEDT001 x where x.dsn_id = a.dsn_id)		-- add -011- a.
		,dsn_enmas = (select x.dsn_enmas from GesPet..HEDT001 x where x.dsn_id = a.dsn_id)		-- add -008- a.
		-- { add -010- a.
		,sol_file_inter
		,sol_bckuptabla
		,sol_bckupjob
		,sol_diferida
		-- }
	from
		GesPet..Solicitudes a
		/* { del -008- a.
		GesPet..Solicitudes a left join 
		GesPet..Recurso b on a.sol_recurso = b.cod_recurso
		} */
	where
		(@cod_recurso is null or a.sol_recurso = @cod_recurso) and
		(@sol_tipo is null or a.sol_tipo = @sol_tipo) and 
		(@sol_nroasignado is null or a.sol_nroasignado = @sol_nroasignado) and
		(@sol_estado is null or a.sol_estado = @sol_estado) and				
		(@sol_mask is null or a.sol_mask = @sol_mask) and				-- add -007- a.
		(@sol_fechaini is null or convert(char(8),a.sol_fecha,112) >= convert(char(8),@sol_fechaini,112)) and 
		(@sol_fechafin is null or convert(char(8),a.sol_fecha,112) <= convert(char(8),@sol_fechafin,112))
	return(0)
go

grant execute on dbo.sp_GetSolicitud to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
