use GesPet
go

print 'Creando/actualizando SP: sp_GetUnaPeticionAPlanificar'
go

if exists (select * from sysobjects where name = 'sp_GetUnaPeticionAPlanificar' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetUnaPeticionAPlanificar
go

create procedure dbo.sp_GetUnaPeticionAPlanificar
	@pet_nroasignado	int    
as 
	declare @secuencia		int
	declare @ret_status     int
	declare	@agr_SDA		int

	create table #AgrupXPetic(
		pet_nrointerno 	int)

	create table #AgrupArbol(
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)
	
	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
		end
	
	select p.pet_nrointerno, p.pet_nroasignado
	from Peticion p
	where 
		p.pet_nroasignado = @pet_nroasignado and 
		p.cod_tipo_peticion in ('NOR','ESP','PRJ') and 
		p.cod_clase in ('NUEV','EVOL') and 
		p.pet_regulatorio = 'N' and 
		p.cod_estado IN ('REFBPE','COMITE') and				-- Se incluyen en la planificación aquellas peticiones que han sido completadas por los referentes de RGyP y designado Ref. de sistemas.
		(p.cod_bpar is not null and LTRIM(RTRIM(p.cod_bpar)) <> '') and 
		((p.valor_impacto <> 0 OR p.valor_facilidad <> 0) OR 
		 (p.pet_projid IN (
			select px.ProjId
			from ProyectoIDM px 
			where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId))) 
		-- Evita que peticiones categorizadas como SDA sean consideradas
		and p.pet_nrointerno NOT IN (
			select sda.pet_nrointerno
			from #AgrupXPeticSDA sda 
			group by sda.pet_nrointerno)
	return(0)
go

grant Execute on dbo.sp_GetUnaPeticionAPlanificar to GesPetUsr 
go

print 'Actualización realizada.'
go
