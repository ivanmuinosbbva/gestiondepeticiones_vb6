use GesPet
go
print 'sp_DeleteBalanceRubro'
go
if exists (select * from sysobjects where name = 'sp_DeleteBalanceRubro' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteBalanceRubro
go
create procedure dbo.sp_DeleteBalanceRubro 
           @cod_BalanceRubro       char(8) 
as 
 
if exists (select cod_BalanceRubro from GesPet..BalanceRubro where RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro)) 
begin 
    if exists (select cod_BalanceRubro from GesPet..BalanceSubRubro where RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro)) 
    begin 
        select 30010 as ErrCode , 'Existen SubRubros asociadas a esta Balance Rubro' as ErrDesc 
        raiserror  30010 'Existen SubRubros asociadas a este Balance Rubro'  
    return (30010) 
    end 
 
    delete GesPet..BalanceRubro 
      where RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro) 
end 
else 
begin 
    raiserror 30001 'Balance Rubro Inexistente'   
    select 30001 as ErrCode , 'Balance Rubro Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteBalanceRubro to GesPetUsr 
go


