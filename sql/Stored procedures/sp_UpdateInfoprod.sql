/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateInfoprod'
go

if exists (select * from sysobjects where name = 'sp_UpdateInfoprod' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateInfoprod
go

create procedure dbo.sp_UpdateInfoprod
	@infoprot_id		int,
	@infoprot_item		int,
	@infoprot_req		char(1),
	@infoprot_itemest	char(1),
	@infoprot_gru		int,
	@infoprot_orden		int
as
	update 
		GesPet.dbo.Infoprod
	set
		infoprot_req		= @infoprot_req,
		infoprot_itemest	= @infoprot_itemest,
		infoprot_gru		= @infoprot_gru,
		infoprot_orden		= @infoprot_orden
	where 
		infoprot_id = @infoprot_id and
		infoprot_item = @infoprot_item 
go

grant execute on dbo.sp_UpdateInfoprod to GesPetUsr
go

print 'Actualización realizada.'
go
