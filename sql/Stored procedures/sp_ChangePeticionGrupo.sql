/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Objetivo:		Cambiar un Grupo ejecutor por otro en peticiones

Descripci�n:	1. Carga los datos jer�rquicos del grupo anterior y del nuevo grupo a reasignar.
				2. Agrego o actualizo los datos del nuevo grupo ejecutor (el reemplazante por el reemplazado)
				3. Elimino el grupo ejecutor anterior (el reemplazado)
				4. Si el nuevo grupo ejecutor es de un sector que no estaba involucrado en la petici�n, lo agrego.
				5. Si el cambio es estructural, respeto las asignaciones existentes entre Peticiones y Recursos. Si es una reasignaci�n de grupo en una petici�n,
				   elimino los relaciones existentes entre recursos y el grupo anterior. Deben asignarse los recursos de manera manual para el nuevo grupo.
				6. Si el sector del grupo anterior no tiene m�s grupos, elimino el Sector y los recursos de todo el sector
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-001- a. FJS 09.10.2009 - Se comenta la parte en que elimina la relaci�n Petici�n - Recurso porque debe mantenerla.
-001- b. FJS 13.10.2009 - Se agrega un par�metro para saber cuando eliminar las relaciones entre peticiones y recursos.
-002- a. FJS 12.09.2011 - Se agrega la eliminaci�n del grupo original de la planificaci�n inicial.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_ChangePeticionGrupo'
go
	
if exists (select * from sysobjects where name = 'sp_ChangePeticionGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_ChangePeticionGrupo
go

create procedure dbo.sp_ChangePeticionGrupo 
	@pet_nrointerno  int=0,
    @old_sector		char(8),
    @old_grupo		char(8), 
    @new_sector		char(8),
    @new_grupo		char(8),
	@origen			char(3)=null	
as 
	declare @new_direccion	char(8) 
	declare @new_gerencia	char(8) 
	declare @cod_recurso	char(10) 
	declare @old_gerencia	char(8) 
	declare @old_direccion	char(8) 
	declare @periodo_vig	int

	--{ add -002- a.
	select @periodo_vig = null

	select @periodo_vig = p.per_nrointerno
	from GesPet..Periodo p
	where (getdate() >= p.fe_desde and getdate() <= p.fe_hasta)
	--}

	-- 1.a. Cargo la estructura jer�rquica del sector/grupo anterior
	select  
		@old_direccion = Ge.cod_direccion,  
		@old_gerencia  = Se.cod_gerencia
	from  
		Sector Se, Gerencia Ge 
	where 	
		RTRIM(Se.cod_sector) = RTRIM(@old_sector) and 
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia) 
	
	-- 1.b. Cargo la estructura jer�rquica del nuevo sector/grupo
	select
		@new_direccion = Ge.cod_direccion,
		@new_gerencia  = Se.cod_gerencia 
	from  
		Sector Se ,Gerencia Ge 
	where	
		RTRIM(Se.cod_sector) = RTRIM(@new_sector) and 
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia)

	begin transaction 
		/* 
		2. Si no existe el nuevo grupo ya, lo agrego con todos los datos del grupo ejecutor anterior (hereda de �ste las fechas de planificaci�n, de ejecuci�n,
		   las horas presupuestadas, el estado de grupo anterior, etc.)
		*/
		if not exists (select pet_nrointerno from PeticionGrupo where pet_nrointerno = @pet_nrointerno and RTRIM(cod_grupo) = RTRIM(@new_grupo))
			begin
				-- Agrega el nuevo grupo en PeticionGrupo
				insert into GesPet..PeticionGrupo( 
					pet_nrointerno, 
					cod_grupo, 
					cod_sector, 
					cod_gerencia, 
					cod_direccion, 
					fe_ini_plan,     
					fe_fin_plan,     
					fe_ini_real,     
					fe_fin_real,     
					horaspresup,     
					cod_estado,  
					fe_estado,   
					cod_situacion, 
					ult_accion, 
					hst_nrointerno_sol, 
					hst_nrointerno_rsp, 
					audit_user,   
					audit_date) 
				select 
					@pet_nrointerno, 
					@new_grupo, 
					@new_sector, 
					@new_gerencia, 
					@new_direccion, 
					Og.fe_ini_plan,    
					Og.fe_fin_plan,    
					Og.fe_ini_real,    
					Og.fe_fin_real,    
					Og.horaspresup,    
					Og.cod_estado,     
					Og.fe_estado,  
					Og.cod_situacion, 
					'', 
					Og.hst_nrointerno_sol,
					Og.hst_nrointerno_rsp,
					SUSER_NAME(),
					getdate()
				from 
					PeticionGrupo Og
				where
					Og.pet_nrointerno = @pet_nrointerno and
					RTRIM(Og.cod_sector) = RTRIM(@old_sector) and
					RTRIM(Og.cod_grupo) = RTRIM(@old_grupo) 
			end
		else
			begin
				-- Actualiza los que ya existan
				update 
					GesPet..PeticionGrupo 
				set	
					cod_sector	  = @new_sector, 
					cod_gerencia  = @new_gerencia, 
					cod_direccion = @new_direccion 
				where   
					pet_nrointerno = @pet_nrointerno and
					RTRIM(cod_grupo) = RTRIM(@new_grupo) 
			end
		
		-- Elimina el grupo ejecutor anterior de PeticionGrupo
		if  RTRIM(@old_grupo) <> RTRIM(@new_grupo)
			begin
				delete 
					GesPet..PeticionGrupo 
				where   
					pet_nrointerno    = @pet_nrointerno and
					RTRIM(cod_sector) = RTRIM(@old_sector) and
					RTRIM(cod_grupo)  = RTRIM(@old_grupo)
			end

	/* 
	4. Si no exist�a el nuevo grupo porque el sector tampoco, lo agrego con todos los datos del sector ejecutor anterior (hereda de �ste las fechas de planificaci�n, 
	   de ejecuci�n, las horas presupuestadas, el estado del sector anterior, etc.)
	*/

	if not exists (select pet_nrointerno from PeticionSector where pet_nrointerno = @pet_nrointerno and RTRIM(cod_sector) = RTRIM(@new_sector))
		begin
			-- Agrega el sector en PeticionSector
			  insert into GesPet..PeticionSector( 
				pet_nrointerno, 
				cod_sector, 
				cod_gerencia, 
				cod_direccion, 
				fe_ini_plan,     
				fe_fin_plan,     
				fe_ini_real,     
				fe_fin_real,     
				horaspresup,     
				cod_estado,  
				fe_estado,   
				cod_situacion, 
				ult_accion, 
				hst_nrointerno_sol, 
				hst_nrointerno_rsp, 
				audit_user,   
				audit_date) 
			  select 
				@pet_nrointerno, 
				@new_sector, 
				@new_gerencia, 
				@new_direccion, 
				Og.fe_ini_plan,    
				Og.fe_fin_plan,    
				Og.fe_ini_real,    
				Og.fe_fin_real,    
				Og.horaspresup,    
				Og.cod_estado,     
				Og.fe_estado,  
				Og.cod_situacion, 
				'', 
				Og.hst_nrointerno_sol, 
				Og.hst_nrointerno_rsp, 
				SUSER_NAME(),   
				getdate()
			from 
				PeticionSector Og
			where   
				Og.pet_nrointerno = @pet_nrointerno and
				RTRIM(Og.cod_sector) = RTRIM(@old_sector)
		end

	-- updatea el PeticionRecurso
	/*
	update 
		PeticionRecurso 
	set	
		cod_sector    = @new_sector, 
		cod_gerencia  = @new_gerencia, 
		cod_direccion = @new_direccion
	where   
		pet_nrointerno = @pet_nrointerno and
		RTRIM(cod_grupo) = RTRIM(@new_grupo) 
	*/
	
	/* 
		5. Actualizo los datos de los recursos asignados a la petici�n que pertenecian al grupo ejecutor anterior:
		   Si es un cambio estructural, es decir, que el grupo cambi� de sector desde el organigrama, entonces debo respetar esas asignaciones de recurso a peticiones,
		   pero si esto es una reasignaci�n desde una petici�n donde se cambia un grupo ejecutor por otro, entonces si deben ser eliminadas esas relaciones.
	*/

	--{ add -001- a.
	if @origen = 'PET'
		begin
			delete 
				GesPet..PeticionRecurso 
			where 
				pet_nrointerno = @pet_nrointerno and
				RTRIM(cod_grupo) = RTRIM(@old_grupo)
		end
	else
		begin
			update 
				GesPet..PeticionRecurso 
			set	
				cod_grupo     = @new_grupo,
				cod_sector    = @new_sector, 
				cod_gerencia  = @new_gerencia, 
				cod_direccion = @new_direccion 
			where   
				pet_nrointerno	  = @pet_nrointerno and
				RTRIM(cod_sector) = RTRIM(@old_sector) and
				RTRIM(cod_grupo)  = RTRIM(@old_grupo)
		end
	--}
	
	/* del -001- a.
	-- Elimina los recursos asociados (porque trae problemas)
	delete PeticionRecurso 
	where pet_nrointerno = @pet_nrointerno and
		RTRIM(cod_grupo) = RTRIM(@old_grupo)
	*/

	-- 6. Si el sector del grupo anterior no tiene m�s grupos, elimino el Sector y los recursos de todo el sector
	if not exists (select pet_nrointerno from PeticionGrupo where   pet_nrointerno = @pet_nrointerno and RTRIM(cod_sector) = RTRIM(@old_sector))
		begin
			delete GesPet..PeticionSector
			where   
				pet_nrointerno = @pet_nrointerno and
				RTRIM(cod_sector) = RTRIM(@old_sector)
			
			delete GesPet..PeticionRecurso 
			where   
				pet_nrointerno = @pet_nrointerno and
				RTRIM(cod_sector) = RTRIM(@old_sector)
		end
	commit transaction
return(0) 
go

grant execute on dbo.sp_ChangePeticionGrupo to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go
