/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionInfohrField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionInfohrField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionInfohrField
go

create procedure dbo.sp_UpdatePeticionInfohrField
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_item		int,
	@cod_recurso	char(10),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if UPPER(LTRIM(RTRIM(@campo)))='INFO_COMEN'
	update GesPet.dbo.PeticionInfohr
	set info_comen = LTRIM(RTRIM(@valortxt))
	where 
		pet_nrointerno = @pet_nrointerno and
		info_id = @info_id and
		info_item = @info_item and
		info_idver = @info_idver and
		cod_recurso = @cod_recurso

if UPPER(LTRIM(RTRIM(@campo)))='FECHA_ENVIO'
	update GesPet.dbo.PeticionInfohr
	set fecha_envio = @valordate
	where 
		pet_nrointerno = @pet_nrointerno and
		info_id = @info_id and
		info_item = @info_item and
		info_idver = @info_idver and
		cod_recurso = @cod_recurso

if UPPER(LTRIM(RTRIM(@campo)))='FECHA_REENVIO'
	update GesPet.dbo.PeticionInfohr
	set fecha_reenvio = @valordate
	where 
		pet_nrointerno = @pet_nrointerno and
		info_id = @info_id and
		info_item = @info_item and
		info_idver = @info_idver and
		cod_recurso = @cod_recurso 

if UPPER(LTRIM(RTRIM(@campo)))='FECHA_MAIL'
	begin
		declare @fecha	smalldatetime

		select @fecha = fecha_envio 
		from PeticionInfohr
		where pet_nrointerno = @pet_nrointerno and
			info_id = @info_id and
			info_item = @info_item and
			info_idver = @info_idver and
			cod_recurso = @cod_recurso 

		IF (@fecha is null) BEGIN
			update GesPet.dbo.PeticionInfohr
			set fecha_envio = @valordate
			where 
				pet_nrointerno = @pet_nrointerno and
				info_id = @info_id and
				info_item = @info_item and
				info_idver = @info_idver and
				cod_recurso = @cod_recurso
		END
		ELSE BEGIN
			update GesPet.dbo.PeticionInfohr
			set fecha_reenvio = @valordate
			where 
				pet_nrointerno = @pet_nrointerno and
				info_id = @info_id and
				info_item = @info_item and
				info_idver = @info_idver and
				cod_recurso = @cod_recurso 
		END	
	end
go

grant execute on dbo.sp_UpdatePeticionInfohrField to GesPetUsr
go

print 'Actualización realizada.'
go
