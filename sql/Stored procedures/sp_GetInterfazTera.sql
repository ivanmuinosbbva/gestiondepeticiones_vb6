/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 06.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetInterfazTera'
go

if exists (select * from sysobjects where name = 'sp_GetInterfazTera' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetInterfazTera
go

create procedure dbo.sp_GetInterfazTera
	@objhab	char(1)=NULL
as
	select 
		ag.dbName,
		ag.objname,
		ag.objlabel,
		ag.objhab
	from 
		GesPet.dbo.InterfazTera ag
	where
		(ag.objhab = @objhab OR @objhab IS NULL)
go

grant execute on dbo.sp_GetInterfazTera to GesPetUsr
go

print 'Actualización realizada.'
go
