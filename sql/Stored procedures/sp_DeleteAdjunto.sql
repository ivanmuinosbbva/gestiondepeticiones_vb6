use GesPet
go
print 'sp_DeleteAdjunto'
go
if exists (select * from sysobjects where name = 'sp_DeleteAdjunto' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteAdjunto
go
create procedure dbo.sp_DeleteAdjunto
	@pet_nrointerno	int,
	@adj_file	char(50)
as

if not exists (select adj_file from Adjunto where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file))
begin
	raiserror 30001 'Adjunto Inexistente'
	select 30001 as ErrCode , 'Adjunto Inexistente' as ErrDesc
	return (30001)
end
else
begin
	delete Adjunto
	where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file)
end
return(0)
go


grant execute on dbo.sp_DeleteAdjunto to GesPetUsr
go
