/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoIDMAlertas'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoIDMAlertas' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoIDMAlertas
go

create procedure dbo.sp_InsertProyectoIDMAlertas
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@alert_tipo		char(1),
	@alert_desc		varchar(180),
	@accion_id		varchar(180),
	@alert_fe_ini	smalldatetime,
	@alert_fe_fin	smalldatetime,
	@responsables	varchar(180),
	@observaciones	varchar(180),
	@status			char(1)
as
	declare @proxnumero		int 
	declare @alert_itm		int

	select  @proxnumero = 0 
	execute sp_ProximoNumero 'ULTPRJAL', @proxnumero OUTPUT 
	select  @alert_itm = @proxnumero 

	insert into GesPet.dbo.ProyectoIDMAlertas (
		ProjId,
		ProjSubId,
		ProjSubSId,
		alert_itm,
		alert_tipo,
		alert_desc,
		accion_id,
		alert_fe_ini,
		alert_fe_fin,
		responsables,
		observaciones,
		alert_status,
		alert_fecres,
		alert_fecha,
		alert_usuario)
	values (
		@ProjId,
		@ProjSubId,
		@ProjSubSId,
		@alert_itm,
		@alert_tipo,
		@alert_desc,
		@accion_id,
		@alert_fe_ini,
		@alert_fe_fin,
		@responsables,
		@observaciones,
		@status,
		null,
		getdate(),
		SUSER_NAME())
	return(0)
go

grant execute on dbo.sp_InsertProyectoIDMAlertas to GesPetUsr
go

print 'Actualización realizada.'
go