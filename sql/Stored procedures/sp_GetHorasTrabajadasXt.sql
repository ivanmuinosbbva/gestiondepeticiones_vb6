/*
-001- a. FJS 22.05.2009 - Se agregan datos del desglose de horas trabajadas por aplicativo.
-002- a. FJS 02.10.2014 - Se optimiza la consulta para incrementar la performance.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHorasTrabajadasXt'
go

if exists (select * from sysobjects where name = 'sp_GetHorasTrabajadasXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHorasTrabajadasXt
go

create procedure dbo.sp_GetHorasTrabajadasXt
	@cod_recurso	char(10),
	@fechadesde		smalldatetime=null,
	@fechahasta		smalldatetime=null
as
	begin
		select
			Ht.cod_recurso,
			Ht.cod_tarea,
			nom_tarea = (select Ta.nom_tarea from Tarea Ta where Ta.cod_tarea=Ht.cod_tarea),
			Ht.pet_nrointerno,
			pet_nroasignado = (select x.pet_nroasignado from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno),
			pet_titulo = (select x.titulo from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno),
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas,
			Ht.trabsinasignar,
			Ht.observaciones,
			Ht.tipo
		from
			GesPet..HorasTrabajadas Ht
		where
			Ht.fe_desde <= @fechahasta and
			Ht.fe_hasta >= @fechadesde and
			Ht.cod_recurso = @cod_recurso
	end
	return(0)
go

grant execute on dbo.sp_GetHorasTrabajadasXt to GesPetUsr
go

print 'Actualización realizada.'
go

print 'Fin de proceso.'
go
