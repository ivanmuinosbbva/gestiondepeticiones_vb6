/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 13.01.2009 - Se agregan los nuevos datos para auditor�a.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertRecurso'
go

if exists (select * from sysobjects where name = 'sp_InsertRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertRecurso
go

create procedure dbo.sp_InsertRecurso 
    @cod_recurso		char(10), 
    @nom_recurso		char(50), 
    @vinculo			char(1)=' ', 
    @cod_direccion		char(8)=' ', 
    @cod_gerencia		char(8)=' ', 
    @cod_sector			char(8)=' ', 
    @cod_grupo			char(8)=' ', 
    @flg_cargoarea		char(1)=' ', 
    @estado_recurso		char(1)=' ', 
    @horasdiarias		smallint=8, 
    @observaciones		varchar(255)=null, 
    @dlg_recurso		char(10)=' ', 
    @dlg_desde			smalldatetime=null, 
    @dlg_hasta			smalldatetime=null,
    @email				varchar(60)='',
    @euser				char(1)=' '

as 
 
if ((RTRIM(@dlg_recurso) is not null and RTRIM(@dlg_recurso)<>'')) 
begin 
if exists (select cod_recurso from GesPet..Recurso where 
      RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
      RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
      convert(char(8),@dlg_desde,112) <= convert(char(8),dlg_desde,112) and 
      convert(char(8),@dlg_hasta,112) >= convert(char(8),dlg_hasta,112) 
      ) 
      begin 
        raiserror 30011 'Superposicion de Delegaci�n'   
        select 30011 as ErrCode , 'Superposicion de Delegaci�n' as ErrDesc   
        return (30011)   
      end 
if exists (select cod_recurso from GesPet..Recurso where 
      RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
      RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
      convert(char(8),@dlg_desde,112) >= convert(char(8),dlg_desde,112) and 
      convert(char(8),@dlg_desde,112) <= convert(char(8),dlg_hasta,112) 
      ) 
      begin 
        raiserror 30012 'Superposicion de Delegaci�n'   
        select 30012 as ErrCode , 'Superposicion de Delegaci�n' as ErrDesc   
        return (30012)   
      end 
if exists (select cod_recurso from GesPet..Recurso where 
      RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
      RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
      convert(char(8),@dlg_hasta,112) >= convert(char(8),dlg_desde,112) and 
      convert(char(8),@dlg_hasta,112) <= convert(char(8),dlg_hasta,112) 
      ) 
      begin 
        raiserror 30013 'Superposicion de Delegaci�n'   
        select 30013 as ErrCode , 'Superposicion de Delegaci�n' as ErrDesc   
        return (30013)   
      end 
end 
 
 
if ((RTRIM(@flg_cargoarea) is not null and RTRIM(@flg_cargoarea)<>'')) 
begin 
if exists (select cod_recurso from GesPet..Recurso where 
      ('X' + RTRIM(cod_direccion) = 'X' + RTRIM(@cod_direccion)) and 
      ('X' + RTRIM(cod_gerencia) = 'X' + RTRIM(@cod_gerencia)) and 
      ('X' + RTRIM(cod_sector) = 'X' + RTRIM(@cod_sector)) and 
      ('X' + RTRIM(cod_grupo) = 'X' + RTRIM(@cod_grupo)) and 
      (RTRIM(flg_cargoarea) = 'S') and 
      (RTRIM(cod_recurso) <> RTRIM(@cod_recurso)) ) 
    begin 
        raiserror 30012 'Ya existe un recurso a cargo del Area'   
        select 30012 as ErrCode , 'Ya existe un recurso a cargo del Area' as ErrDesc   
        return (30012)   
    end 
if (RTRIM(@estado_recurso)<>'A') 
    begin 
        raiserror 30013 'No puede estar a cargo de Area un recurso no Activo'   
        select 30013 as ErrCode , 'No puede estar a cargo de Area un recurso no Activo' as ErrDesc   
        return (30013)   
    end 
end 
 
if exists (select cod_recurso from GesPet..Recurso where RTRIM(cod_recurso) = RTRIM(@cod_recurso)) 
	begin 
		raiserror 30012 'Ya existe un recurso con ese c�digo'   
		select 30012 as ErrCode , 'Ya existe un recurso con ese c�digo' as ErrDesc   
		return (30012)   
	end 
else 
	begin 
		  insert into GesPet..Recurso 
			(cod_recurso,        
			nom_recurso,         
			vinculo,         
			cod_direccion,    
			cod_gerencia,     
			cod_sector,      
			cod_grupo,     
			flg_cargoarea,    
			estado_recurso,  
			horasdiarias,     
			observaciones,    
			dlg_recurso,     
			dlg_desde,       
			dlg_hasta,
			email, 
			euser,
			audit_user, 
			audit_date)	-- add -001- a.
		  values 
			(@cod_recurso,       
			@nom_recurso, 
			@vinculo, 
			@cod_direccion, 
			@cod_gerencia, 
			@cod_sector, 
			@cod_grupo, 
			@flg_cargoarea, 
			@estado_recurso, 
			@horasdiarias, 
			@observaciones, 
			@dlg_recurso, 
			@dlg_desde, 
			@dlg_hasta, 
			@email, 
			@euser,
			SUSER_NAME(), 
			getdate())		-- add -001- a.
	end 

return(0) 
go

sp_procxmode 'sp_InsertRecurso', anymode
go

grant execute on dbo.sp_InsertRecurso to GesPetUsr 
go

print 'Actualizaci�n realizada.'
