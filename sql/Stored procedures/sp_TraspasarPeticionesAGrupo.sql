/*
-001- a. FJS 14.10.2009 - Nuevo SP para ejecutar la transferencia de cartera activa de peticiones a otro grupo ejecutor. Este ejecuta por grupo, todas aquellas
						  peticiones donde el grupo que cede se encuentra participando de manera activa.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_TraspasarPeticionesAGrupo'
go

if exists (select * from sysobjects where name = 'sp_TraspasarPeticionesAGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_TraspasarPeticionesAGrupo
go

create procedure dbo.sp_TraspasarPeticionesAGrupo
	@cod_sector		char(8), 
	@cod_grupo		char(8), 
	@new_sector		char(8),
	@new_grupo		char(8),
	@finaliza		char(1)
as 

declare @pet_nrointerno	int

begin tran
	declare curPeticiones cursor for  
		-- Peticiones de el grupo que transfiere
		select a.pet_nrointerno
		from GesPet..PeticionGrupo a
		where 
			a.cod_sector = @cod_sector and 
			a.cod_grupo = @cod_grupo and 
			a.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD')
			--charindex(a.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0
		group by a.pet_nrointerno
		for read only 

		open curPeticiones 
		fetch curPeticiones into @pet_nrointerno  
		while @@sqlstatus = 0  
			begin  
				execute sp_TraspasarPeticionAGrupo @pet_nrointerno, @cod_sector, @cod_grupo, @new_sector, @new_grupo, @finaliza
				fetch curPeticiones into @pet_nrointerno 
			end
		close curPeticiones 
	deallocate cursor curPeticiones  
commit tran
go

grant execute on dbo.sp_TraspasarPeticionesAGrupo to GesPetUsr
go

print 'Actualización realizada.'
go