/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 20.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHEDT012'
go

if exists (select * from sysobjects where name = 'sp_InsertHEDT012' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHEDT012
go

create procedure dbo.sp_InsertHEDT012
	@tipo_id		char(1)=null,
	@subtipo_id		char(1)=null,
	@subtipo_nom	char(50)=null,
	@rutina			char(8)=null
as
	if exists (select x.rutina from GesPet..HEDT012 x where RTRIM(x.rutina) = RTRIM(@rutina))
		begin 
			raiserror 30011 'La rutina ya existe y esta asociada a un tipo y subtipo específico.'
			select 30011 as ErrCode , 'La rutina ya existe y esta asociada a un tipo y subtipo específico.' as ErrDesc
			return (30011)
		end
	else
		begin
			insert into GesPet.dbo.HEDT012 (
				tipo_id,
				subtipo_id,
				subtipo_nom,
				rutina)
			values (
				@tipo_id,
				@subtipo_id,
				@subtipo_nom,
				@rutina)
		end
go

grant execute on dbo.sp_InsertHEDT012 to GesPetUsr
go

print 'Actualización realizada.'
