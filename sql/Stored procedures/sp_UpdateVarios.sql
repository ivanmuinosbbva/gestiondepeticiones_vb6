use GesPet
go
print 'sp_UpdateVarios'
go
if exists (select * from sysobjects where name = 'sp_UpdateVarios' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateVarios
go
create procedure dbo.sp_UpdateVarios   
    @var_codigo         char(8),   
    @var_numero         int=null,   
    @var_texto          char(30)=null,   
    @var_fecha          smalldatetime=null   
   
as   
if exists (select var_codigo from GesPet..Varios where RTRIM(var_codigo) = RTRIM(@var_codigo))   
begin   
    update GesPet..Varios   
      set    var_numero = @var_numero,   
             var_texto = @var_texto, 
             var_fecha = @var_fecha 
      where  RTRIM(var_codigo) = RTRIM(@var_codigo)   
end   
else   
begin   
      insert into GesPet..Varios   
             (var_codigo, var_numero, var_texto, var_fecha)   
      values   
             (@var_codigo, @var_numero, @var_texto, @var_fecha)   
end   
   
return(0)   
go



grant execute on dbo.sp_UpdateVarios to GesPetUsr 
go


