
use GesPet
go

print 'sp_GetHorasPeriodo'
go

if exists (select * from sysobjects where name = 'sp_GetHorasPeriodo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHorasPeriodo
go

create procedure dbo.sp_GetHorasPeriodo
	@sol_desde	smalldatetime,
	@sol_hasta	smalldatetime,
	@inf_desde	smalldatetime,
	@inf_hasta	smalldatetime,
	@inf_horas	int,
	@ret_desde	smalldatetime OUTPUT,
	@ret_hasta	smalldatetime OUTPUT,
	@ret_dias	int OUTPUT,
	@ret_horas	int OUTPUT
as

	set rowcount 0

	declare @inf_habiles	int
	declare @aux_habiles	int
	declare @diasferiados	int
	declare @aux_desde		smalldatetime
	declare @aux_hasta		smalldatetime
	declare @x1				numeric(10,2)
	declare @x2				numeric(10,2)
	declare @x3				numeric(10,2)
	declare @x4				numeric(10,2)
	declare @auxtxt			varchar(50) /*variable para debugger*/

		/* 1.calcular los dias que entran en la solicitud*/
		IF @sol_desde<@inf_desde
			SELECT @aux_desde=@inf_desde
		ELSE
			SELECT @aux_desde=@sol_desde
		IF @sol_hasta<@inf_hasta
			SELECT @aux_hasta=@sol_hasta
		ELSE
			SELECT @aux_hasta=@inf_hasta

		SELECT @ret_desde=@aux_desde
		SELECT @ret_hasta=@aux_hasta

		/* dias habiles del periodo informado */
		select @inf_habiles=datediff(day, @inf_desde, @inf_hasta) + 1
		select @diasferiados=count(1) from Feriado
			where  fecha >= @inf_desde and fecha <= @inf_hasta
		if @diasferiados < @inf_habiles
			select @inf_habiles=@inf_habiles - @diasferiados
	/*
	select @auxtxt=convert(char(30) ,@inf_habiles)
	print @auxtxt
	*/
		/* dias habiles del periodo que entra en la solicitud */
		select @aux_habiles=datediff(day, @aux_desde, @aux_hasta) + 1
		select @diasferiados=count(1) from Feriado
			where  fecha >= @aux_desde and fecha <= @aux_hasta
		if @diasferiados < @aux_habiles
			select @aux_habiles=@aux_habiles - @diasferiados

	/*
	select @auxtxt=convert(char(30) ,@aux_habiles)
	print @auxtxt
	*/
		select @ret_dias=@aux_habiles

		IF @inf_habiles>0
		BEGIN
			select @x1 = @inf_horas
			select @x2 = @inf_habiles
			select @x3 = @aux_habiles
			SELECT @x4= convert(numeric(10,2) ,((@x1  * 100)/ @x2) * @x3)
			SELECT @ret_horas= convert(int, @x4)
		END
		ELSE
		BEGIN
			SELECT @ret_horas= 0
		END
	/*
	select @auxtxt=convert(char(30) ,@ret_horas)
	print @auxtxt
	*/
	return(0)
go

grant execute on dbo.sp_GetHorasPeriodo to GesPetUsr
go


