use GesPet
go
print 'sp_DeleteAvanceGrupo'
go
if exists (select * from sysobjects where name = 'sp_DeleteAvanceGrupo' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteAvanceGrupo
go
create procedure dbo.sp_DeleteAvanceGrupo 
           @cod_AvanceGrupo       char(8) 
as 
 
if exists (select cod_AvanceGrupo from GesPet..AvanceGrupo where RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo)) 
begin 
    if exists (select cod_AvanceGrupo from GesPet..AvanceCoef where RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo)) 
    begin 
        select 30010 as ErrCode , 'Existen SubRubros asociadas a esta Avance Grupo' as ErrDesc 
        raiserror  30010 'Existen SubRubros asociadas a este Avance Grupo'  
    return (30010) 
    end 
 
    delete GesPet..AvanceGrupo 
      where RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo) 
end 
else 
begin 
    raiserror 30001 'AvanceGrupo Inexistente'   
    select 30001 as ErrCode , 'AvanceGrupo Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteAvanceGrupo to GesPetUsr 
go


