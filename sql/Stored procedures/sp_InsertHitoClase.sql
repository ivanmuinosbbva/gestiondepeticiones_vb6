/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.22 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHitoClase'
go

if exists (select * from sysobjects where name = 'sp_InsertHitoClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHitoClase
go

create procedure dbo.sp_InsertHitoClase
	@hitoid	int,
	@hitodesc	char(60),
	@hitoexpos	char(1)
as
	insert into GesPet.dbo.HitoClase (
		hitoid,
		hitodesc,
		hitoexpos)
	values (
		@hitoid,
		@hitodesc,
		@hitoexpos)
go

grant execute on dbo.sp_InsertHitoClase to GesPetUsr
go

print 'Actualización realizada.'
go
