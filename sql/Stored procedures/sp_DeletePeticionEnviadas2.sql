/*
-000- a. FJS 05.05.2009 - Nuevo SP para eliminar peticiones en estado "En ejecuci�n"
-001- a. FJS 27.05.2009 - Se modifican las condiciones para solo borrar el registro si es recibido el n�mero de la petici�n
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionEnviadas2'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionEnviadas2' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionEnviadas2
go

create procedure dbo.sp_DeletePeticionEnviadas2
	@pet_nrointerno	int
as
	delete from
		GesPet..PeticionEnviadas2
	where
		pet_nrointerno = @pet_nrointerno
go

grant execute on dbo.sp_DeletePeticionEnviadas2 to GesPetUsr
go

print 'Actualizaci�n realizada.'
