/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 08.05.2013 - 
-001- a. FJS 24.06.2015 - Nuevo: se agreg� el campo de tipo de f�brica.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertFabrica'
go

if exists (select * from sysobjects where name = 'sp_InsertFabrica' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertFabrica
go

create procedure dbo.sp_InsertFabrica
	@cod_fab		char(10),
	@nom_fab		char(50),
	@cod_gerencia	char(8),
	@cod_direccion	char(8),
	@cod_sector		char(8),
	@cod_grupo		char(8),
	@estado_fab		char(1),
	@tipo_fab		int			-- add -001- a.
as
	insert into GesPet.dbo.Fabrica (
		cod_fab,
		nom_fab,
		cod_gerencia,
		cod_direccion,
		cod_sector,
		cod_grupo,
		estado_fab,
		tipo_fab)
	values (
		@cod_fab,
		@nom_fab,
		@cod_gerencia,
		@cod_direccion,
		@cod_sector,
		@cod_grupo,
		@estado_fab,
		@tipo_fab)
go

grant execute on dbo.sp_InsertFabrica to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
