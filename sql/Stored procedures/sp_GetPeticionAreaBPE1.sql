use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetPeticionAreaBPE1'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionAreaBPE1' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionAreaBPE1
go

create procedure dbo.sp_GetPeticionAreaBPE1
	@pet_nroasignado	int=0,
	@cod_bpar			varchar(10),
	@cod_estado			varchar(250)=null,
	@anx_especial		varchar(1)=null,
	@agr_nrointerno		int=0,
	@prio_ejecuc		int=0
as
	declare @secuencia		int
	declare @ret_status     int
	
	create table #AgrupXPetic(
		pet_nrointerno 	int)

	create table #AgrupArbol(
				secuencia		int,
				nivel 			int,
				agr_nrointerno 	int)
	
	--{ add -007- a.
	create table #tablaCriterio (
		pet_nrointerno		int,
		categoria			char(6))

	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	--}

	--{ add -007- a. Esto es para obtener los agrupamientos de SDA
	declare	@agr_SDA	int
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
			
			INSERT INTO #tablaCriterio
			select 
				p.pet_nrointerno,
				categoria = case
					when p.cod_tipo_peticion in ('AUI','AUX') OR p.pet_regulatorio = 'S' then '1. REG'
					when exists (select x.pet_nrointerno from #AgrupXPeticSDA x where x.pet_nrointerno = p.pet_nrointerno) then '2. SDA'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then '3. PES'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then '4. ENG'
					when p.cod_clase in ('OPTI','CORR','SPUF','ATEN') then '6. CSI'
					when exists (
						 select count(1) 
						 from ProyectoIDM px 
						 where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
						 having count(1) > 0) OR (p.valor_impacto <> 0 OR p.valor_facilidad <>0) then '5. PRI'
					when p.cod_clase = 'SINC' then '---'
					else '7. SPR'
				end
			from Peticion p 
		end
	--}

	/* esto es si se pidio agrupamiento */
	if @agr_nrointerno<>0
		begin
			truncate table #AgrupArbol
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT
			if (@ret_status <> 0)
				begin
					if (@ret_status = 30003)
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
					return (@ret_status)
				end
			insert into #AgrupXPetic (pet_nrointerno)
				select	AP.pet_nrointerno
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where AA.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_vigente='S'
		end

	select 
		PET.pet_nrointerno,
		PET.pet_nroasignado,
		PET.cod_tipo_peticion,
		PET.titulo,
		PET.prioridad,
		PET.importancia_cod,
		nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
		PET.fe_ini_plan,
		PET.fe_fin_plan,
		PET.horaspresup,
		PET.cod_usualta,
		PET.cod_solicitante,
		PET.cod_bpar,
		nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
		PET.cod_BPE,
		nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
		PET.cod_estado,
		nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
		PET.cod_situacion,
		nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
		PET.cod_clase,
		PET.pet_imptech,
		horaspresup_hij=0,
		PET.pet_regulatorio,
		pet_ro = isnull(PET.pet_ro,'-'),
		pet_riesgo = (case
			when PET.pet_ro = 'A' then 'Alta'
			when PET.pet_ro = 'M' then 'Media'
			when PET.pet_ro = 'B' then 'Baja'
			else 'No'
		end),
		PET.puntuacion,
		c.categoria,
		ISNULL(prioridadBPE, 999999)	as prioridadBPE,
		analisis = case 
			when horaspresup > 0 then '•'
			else '-'
		end,
		planificado = case
			when (PET.fe_ini_plan is not null and PET.fe_fin_plan is not null) then '•'
			else '-'
		end
	from  GesPet..Peticion PET inner join
			#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
	where   
		(@pet_nroasignado=0 or @pet_nroasignado=PET.pet_nroasignado) and
		(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
		(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0) and
		(@cod_bpar is null or RTRIM(PET.cod_BPE) = RTRIM(@cod_bpar)) and 
		(PET.cod_bpar is not null AND LTRIM(RTRIM(PET.cod_bpar)) <> '') and 
		(PET.valor_facilidad > 0 and PET.valor_impacto > 0)
	order by 
		prioridadBPE
		
	return(0)
go

grant execute on dbo.sp_GetPeticionAreaBPE1 to GesPetUsr
go

print 'Actualización finalizada.'
go
