use GesPet
go
print 'sp_UpdatePeticionConf'
go
if exists (select * from sysobjects where name = 'sp_UpdatePeticionConf' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePeticionConf
go
create procedure dbo.sp_UpdatePeticionConf
	@pet_nrointerno	int,
	@ok_tipo	char(8),
	@ok_modo	char(3),
	@ok_texto	char(250)
as

if not exists (select ok_tipo from PeticionConf where @pet_nrointerno=pet_nrointerno and RTRIM(ok_tipo)=RTRIM(@ok_tipo))
begin
	raiserror 30001 'La confirmacion no estaba declarada.'
	select 30001 as ErrCode , 'La confirmacion no estaba declarada.' as ErrDesc
	return (30001)
end
else
begin
	update PeticionConf
		set ok_texto=@ok_texto,
			ok_modo=@ok_modo
		where @pet_nrointerno=pet_nrointerno and RTRIM(ok_tipo)=RTRIM(@ok_tipo)
end
return(0)
go

grant execute on dbo.sp_UpdatePeticionConf to GesPetUsr
go
