use GesPet
go
print 'sp_DelAgrup'
go
if exists (select * from sysobjects where name = 'sp_DelAgrup' and sysstat & 7 = 4)
drop procedure dbo.sp_DelAgrup
go
create procedure dbo.sp_DelAgrup 
	@agr_nrointerno		int=0
as 
if exists (select agr_nrointerno from Agrup where agr_nropadre=@agr_nrointerno)
begin
	select 30010 as ErrCode , 'Error: El agrupamiento posee agrupamientos dependientes. Debe quitar la relaci�n' as ErrDesc 
	raiserror  30010 'Error: El agrupamiento posee agrupamientos dependientes. Debe quitar la relaci�n'  
	return (30010) 
end
begin tran
    delete AgrupPetic where agr_nrointerno=@agr_nrointerno 
    delete Agrup where agr_nrointerno=@agr_nrointerno 
commit
return(0)   
go



grant execute on dbo.sp_DelAgrup to GesPetUsr 
go
