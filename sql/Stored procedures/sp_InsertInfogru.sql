/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertInfogru'
go

if exists (select * from sysobjects where name = 'sp_InsertInfogru' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertInfogru
go

create procedure dbo.sp_InsertInfogru
	@info_gruid		int,
	@info_grunom	char(50),
	@info_gruorden	int,
	@info_gruhab	char(1)
as
	insert into GesPet.dbo.Infogru (
		info_gruid,
		info_grunom,
		info_gruorden,
		info_gruhab)
	values (
		@info_gruid,
		@info_grunom,
		@info_gruorden,
		@info_gruhab)
go

grant execute on dbo.sp_InsertInfogru to GesPetUsr
go

print 'Actualización realizada.'
go
