/*
-000- a. FJS 11.12.2008 - Nuevo SP para modificar las leyendas de las acciones registradas en el historial.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateAcciones'
go

if exists (select * from sysobjects where name = 'sp_UpdateAcciones' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateAcciones
go

create procedure dbo.sp_UpdateAcciones
	@cod_accion			char(8),
	@nom_accion			varchar(100)='',
	@IGM_hab			char(1),
	@tipo_accion		char(6),
	@agrupador			int,
	@leyenda			varchar(100),
	@orden				int,
	@porEstado			char(1),
	@multiplesEstados	char(1)
		
as 
	update 
		GesPet..Acciones
	set 
		nom_accion		 = @nom_accion,
		IGM_hab			 = @IGM_hab,
		tipo_accion		 = @tipo_accion,
		agrupador		 = @agrupador,
		leyenda			 = @leyenda,
		orden			 = @orden,
		porEstado		 = @porEstado,
		multiplesEstados = @multiplesEstados
	where
		LTRIM(RTRIM(cod_accion)) = LTRIM(RTRIM(@cod_accion))
	return(0)
go

grant execute on dbo.sp_UpdateAcciones to GesPetUsr
go

print 'Actualización realizada.'
go
