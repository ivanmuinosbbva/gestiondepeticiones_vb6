/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 21.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteInterfazTeraDet'
go

if exists (select * from sysobjects where name = 'sp_DeleteInterfazTeraDet' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteInterfazTeraDet
go

create procedure dbo.sp_DeleteInterfazTeraDet
	@dbName		varchar(30)='',
	@objname	varchar(50)='',
	@objfield	varchar(50)=''
as
	delete from
		GesPet.dbo.InterfazTeraDet
	where
		LTRIM(RTRIM(dbName)) = LTRIM(RTRIM(@dbName)) and
		LTRIM(RTRIM(objname)) = LTRIM(RTRIM(@objname)) and
		LTRIM(RTRIM(objfield)) = LTRIM(RTRIM(@objfield))
go

grant execute on dbo.sp_DeleteInterfazTeraDet to GesPetUsr
go

print 'Actualización realizada.'
go
