/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetAccionesPerfilIDM'
go

if exists (select * from sysobjects where name = 'sp_GetAccionesPerfilIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAccionesPerfilIDM
go

create procedure dbo.sp_GetAccionesPerfilIDM
	@cod_accion	char(8)=null,
	@cod_perfil	char(4)=null,
	@cod_estado	char(6)=null,
	@cod_estnew	char(6)=null
as
	select 
		p.cod_accion,
		nom_accion = (select x.nom_accion from GesPet..AccionesIDM x where x.cod_accion = p.cod_accion),
		p.cod_perfil,
		nom_perfil = (select x.nom_perfil from GesPet..Perfil x where x.cod_perfil = p.cod_perfil),
		p.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = p.cod_estado),
		p.cod_estnew,
		nom_estadonew = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = p.cod_estnew),
		p.flg_hereda
	from 
		GesPet.dbo.AccionesPerfilIDM p
	where
		(@cod_accion is null or p.cod_accion = @cod_accion) and
		(@cod_perfil is null or p.cod_perfil = @cod_perfil) and
		(@cod_estado is null or p.cod_estado = @cod_estado) and
		(@cod_estnew is null or p.cod_estnew = @cod_estnew)
go

grant execute on dbo.sp_GetAccionesPerfilIDM to GesPetUsr
go

print 'Actualización realizada.'
go
