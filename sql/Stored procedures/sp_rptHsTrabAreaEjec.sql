/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

-001- a. FJS 29.10.2008 - Se agrega una nueva columna para identificar de mejor manera las horas trabajadas en Tareas.
-002- a. FJS 14.04.2009 - Se agrega la clase de la petici�n.
-003- a. FJS 07.09.2009 - Se agrega un nuevo par�metro para discriminar peticiones o tareas (o ambas).
-004- a. DMA 24-01-2011 - Optimizaci�n.
-005- a. FJS 08.11.2013 - Nuevo: se agregan nuevos datos al reporte.
-006- a. FJS 28.07.2015 - Nuevo: se unifica en una vista la conjunci�n de Recursos y F�bricas.
-007- a. FJS 04.05.2016 - Nuevo: se agrega filtro por proyecto y por agrupamientos.

Detalle:
********

5:	Direcci�n
4:	Gerencia
3:	Sector
2:	Grupo
1:	Recurso
0:	Sin agrupar
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_rptHsTrabAreaEjec'
go

if exists (select * from sysobjects where name = 'sp_rptHsTrabAreaEjec' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHsTrabAreaEjec
go

create procedure dbo.sp_rptHsTrabAreaEjec
	@fdesde			varchar(8)='NULL',
	@fhasta			varchar(8)='NULL',
	@nivel			varchar(4)='NULL',
	@area			varchar(8)='NULL',
	@recurso		varchar(10)='NULL',
	@detalle		varchar(1)='0',
	@incluir		char(3)='ALL',				-- add -003- a.
	--{ add -007- a.
	@agr_nrointerno	int=null,
	@projid			int=null,
	@projsubid		int=null,
	@projsubsid		int=null,
	@tipoproj		char(1)='S'
	--}
as
	declare @cod_direccion		varchar (10)
	declare @cod_gerencia		varchar (10)
	declare @cod_sector			varchar (10)
	declare @cod_grupo			varchar (10)
	declare @cod_recurso		varchar (10)
	declare @cod_tarea			varchar (10)
	declare @nom_tarea			varchar (50)
	declare @pet_nrointerno		int
	declare @pet_nroasignado	int
	declare @titulo				varchar(50)
	declare @fe_desde			smalldatetime
	declare @fe_hasta			smalldatetime
	declare @horas				int
	declare @trabsinasignar		char(1)
	declare @sol_desde			smalldatetime
	declare @sol_hasta			smalldatetime
	declare @ret_dias			int
	declare @ret_horas			int
	declare @horas_si			numeric(10,2)
	declare @horas_no			numeric(10,2)
	declare @xhoras_si			numeric(10,2)
	declare @xhoras_no			numeric(10,2)
	declare @ret_desde			smalldatetime
	declare @ret_hasta			smalldatetime
	declare @tip_asig			varchar(3)
	declare @cod_asig			varchar(10)
	declare @cod_real			varchar(10)
	declare @nom_asig			varchar(50)
	--declare	@observaciones		varchar(255)
	--{ add -004- a.
	declare @cod_direccion_fija		char(8)	
	declare @cod_gerencia_fija		char(8)
	declare @cod_sector_fija		char(8)
	declare @cod_direccion_ant		char(8)
	declare @cod_gerencia_ant		char(8)
	declare @cod_sector_ant			char(8)
	declare @cod_grupo_ant			char(8)
	declare @cod_recurso_ant		char(10)
	declare @nom_direccion_act		char(80)
	declare @nom_gerencia_act		char(80)
	declare @nom_sector_act			char(80)
	declare @nom_grupo_act			char(80)
	declare @nom_recurso_act		char(50)
	declare @cod_clase				char(4)
	--declare @origen					char(1)
	--{ add -005- a.
	declare @cod_tipo_peticion		char(3)
	declare @pet_regulatorio		char(1)
	declare @proyecto_idm			char(14)
	declare @agrupId				int
	declare @pet_projid				int
	declare @pet_projsubid			int
	declare @pet_projsubsid			int
	--}

	set dateformat ymd

	--{ add -007- a.
	declare @secuencia int  
	declare @ret_status     int  
	 
	create table #AgrupXPetic (
		pet_nrointerno		int,
		agr_nrointerno		int)  
	  
	-- Esto es si se pidio agrupamiento
	if @agr_nrointerno<>0  
		begin  
			create table #AgrupArbol(  
				secuencia		int,  
				nivel			int,  
				agr_nrointerno  int)  
			
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin  
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError  
						return (@ret_status)  
					end  
				end  
			insert into #AgrupXPetic (pet_nrointerno, agr_nrointerno)  
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_vigente='S'
		end
	--}

	if @nivel='GRUP'
		begin
			select @cod_sector_fija    = (select Gr.cod_sector    from GesPet..Grupo    Gr where Gr.cod_grupo    = RTRIM(@area))
			select @cod_gerencia_fija  = (select Se.cod_gerencia  from GesPet..Sector   Se where Se.cod_sector   = @cod_sector_fija)
			select @cod_direccion_fija = (select Ge.cod_direccion from GesPet..Gerencia Ge where Ge.cod_gerencia = @cod_gerencia_fija)
		end
	if @nivel='SECT'
		begin
			select @cod_gerencia_fija  = (select Se.cod_gerencia  from GesPet..Sector   Se where Se.cod_sector   = RTRIM(@area))
			select @cod_direccion_fija = (select Ge.cod_direccion from GesPet..Gerencia Ge where Ge.cod_gerencia = @cod_gerencia_fija)
		end
	if @nivel='GERE'
		begin
			select @cod_direccion_fija = (select Ge.cod_direccion from GesPet..Gerencia Ge where Ge.cod_gerencia = RTRIM(@area))
		end
	--}

	if RTRIM(@fdesde)='NULL'
		begin
			select @sol_desde=dateadd(mm,-1,getdate())
			select @fdesde=convert(char(8),@sol_desde,112)
		end
	else
		select @sol_desde=@fdesde

	if RTRIM(@fhasta)='NULL'
		begin
			select @sol_hasta=dateadd(mm,1,getdate())
			select @fhasta=convert(char(8),@sol_hasta,112)
		end
	else
		select @sol_hasta=@fhasta

	CREATE TABLE #TmpSalida(
		cod_direccion		char(10) null,
		cod_gerencia		char(10) null,
		cod_sector			char(10) null,
		cod_grupo			char(10) null,
		cod_recurso			char(10) null,
		nom_direccion		char(80) null,
		nom_gerencia		char(80) null,
		nom_sector			char(80) null,
		nom_grupo			char(80) null,
		nom_recurso			char(50) null,
		tip_asig			char(3),				-- PET / TAR
		cod_asig			char(10) null,
		cod_real			char(10) null,
		cod_clase			char(4)	 null,			-- add -002- a.
		nom_asig			char(50) null,
		entre_desde			smalldatetime,
		entre_hasta			smalldatetime,
		horas_si			numeric(10,2),
		horas_no			numeric(10,2),
		horas_tarea			numeric(10,2),			-- add -001- a.
		--origen				char(1),
		--{ add -005- a.
		cod_tipo_peticion	char(3)		null,
		pet_regulatorio		char(1)		null,
		pet_projid			int			null,
		pet_projsubid		int			null,
		pet_projsubsid		int			null,
		proyecto_idm		char(14)	null,
		agrupId				int			null)
		--}

	DECLARE CursHoras CURSOR FOR
		select
			cod_direccion = (case when @nivel='DIRE' then RTRIM(@area)
						  when (@nivel='GERE' or @nivel='SECT' or @nivel='GRUP') then @cod_direccion_fija
						  else (select Rec.cod_direccion from GesPet..vw_RecursoFabrica Rec where Rec.cod_recurso = Ht.cod_recurso) end),
			cod_gerencia  = (case when @nivel='GERE' then RTRIM(@area)
						  when (@nivel='SECT' or @nivel='GRUP') then @cod_gerencia_fija
						  else  (select Rec.cod_gerencia from GesPet..vw_RecursoFabrica Rec where Rec.cod_recurso = Ht.cod_recurso) end),
			cod_sector    = (case when @nivel='SECT' then RTRIM(@area) 
						  when (@nivel='GRUP') then @cod_sector_fija
						  else (select Rec.cod_sector    from GesPet..vw_RecursoFabrica Rec where Rec.cod_recurso = Ht.cod_recurso) end),
			cod_grupo     = (case when @nivel='GRUP' then RTRIM(@area)
						  else (select Rec.cod_grupo     from GesPet..vw_RecursoFabrica Rec where Rec.cod_recurso = Ht.cod_recurso) end),
			Ht.cod_recurso,
			Ht.cod_tarea,
			nom_tarea = (select Ta.nom_tarea from GesPet..Tarea Ta where Ta.cod_tarea = Ht.cod_tarea),
			/*
			nom_tarea = (case when (Ht.pet_nrointerno is null or Ht.pet_nrointerno = 0) then (select Ta.nom_tarea from GesPet..Tarea Ta where Ta.cod_tarea = Ht.cod_tarea)
						else null end),
			*/
			Ht.pet_nrointerno,
			pet_nroasignado = (select Pt.pet_nroasignado from GesPet..Peticion Pt where Ht.pet_nrointerno = Pt.pet_nrointerno),
			cod_clase 	= (select Pt.cod_clase from GesPet..Peticion Pt where Ht.pet_nrointerno = Pt.pet_nrointerno),		
			titulo 		= (select Pt.titulo from GesPet..Peticion Pt where Ht.pet_nrointerno = Pt.pet_nrointerno),
			Ht.trabsinasignar,
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas
			--{ add -005- a.
			,cod_tipo_peticion = (select x.cod_tipo_peticion from GesPet..Peticion x where x.pet_nrointerno = Ht.pet_nrointerno)
			,pet_regulatorio = (select x.pet_regulatorio from GesPet..Peticion x where x.pet_nrointerno = Ht.pet_nrointerno)
			,pet_projid = (select x.pet_projid from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno)
			,pet_projsubid = (select x.pet_projsubid from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno)
			,pet_projsubsid = (select x.pet_projsubsid from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno)
			,proyecto_idm = (
				select 
					LTRIM(RTRIM(convert(char(10),isnull(x.pet_projid,0)))) + '.' + 
					LTRIM(RTRIM(convert(char(10),isnull(x.pet_projsubid,0)))) + '.' + 
					LTRIM(RTRIM(convert(char(10),isnull(x.pet_projsubsid,0)))) 
				from GesPet..Peticion x where x.pet_nrointerno = Ht.pet_nrointerno)
			--}
			--,agrupId = (select ag.agr_nrointerno from AgrupPetic ag where ag.agr_nrointerno = @agr_nrointerno and ag.pet_nrointerno = Ht.pet_nrointerno)	-- del -007- a.
			,agrupId = (select MAX(ag.agr_nrointerno) from #AgrupXPetic ag where ag.pet_nrointerno = Ht.pet_nrointerno group by ag.pet_nrointerno)	-- add -007- a.
			--,agrupId = (select ag.agr_nrointerno from AgrupPetic ag where ag.pet_nrointerno = Ht.pet_nrointerno AND ag.agr_nrointerno = @agr_nrointerno)	-- add -007- a.
		from
			GesPet..HorasTrabajadas Ht
		where
			((@incluir = 'ALL') or ((@incluir = 'TAR' and Ht.pet_nrointerno = 0) OR (@incluir = 'PET' and Ht.pet_nrointerno <> 0))) AND
			(RTRIM(@fdesde)='NULL' or convert(char(8),Ht.fe_hasta,112) >= @fdesde) AND
			(RTRIM(@fhasta)='NULL' or convert(char(8),Ht.fe_desde,112) <= @fhasta) AND
			--{ add -006- a.
			((((RTRIM(@recurso)='NULL') and RTRIM(Ht.cod_recurso) in (select RTRIM(ReFa.cod_recurso) 
											from GesPet..vw_RecursoFabrica ReFa
											where (@nivel is null or RTRIM(@nivel)='NULL') or
											(@nivel='DIRE' and RTRIM(ReFa.cod_direccion)=RTRIM(@area)) or
											(@nivel='GERE' and RTRIM(ReFa.cod_gerencia)=RTRIM(@area)) or
											(@nivel='SECT' and RTRIM(ReFa.cod_sector)=RTRIM(@area)) or
											(@nivel='GRUP' and RTRIM(ReFa.cod_grupo)=RTRIM(@area)))))
			  OR (RTRIM(Ht.cod_recurso)=RTRIM(@recurso))) 
			--}
			--{ add -007- a.
			AND
			((@agr_nrointerno=0 OR @agr_nrointerno IS NULL) or 
				(@incluir='ALL' AND (Ht.pet_nrointerno = 0 OR Ht.pet_nrointerno IN (select AXP.pet_nrointerno from #AgrupXPetic AXP))) OR 
				(@incluir='PET' AND Ht.pet_nrointerno IN (select AXP.pet_nrointerno from #AgrupXPetic AXP)) OR 
				(@incluir='TAR' AND Ht.pet_nrointerno = 0)) 
			AND 
			((@projid is null or @projid = 0) OR 
				(@incluir='TAR' AND Ht.pet_nrointerno = 0) OR
				(@incluir='PET' AND Ht.pet_nrointerno IN ( 
					select p.pet_nrointerno
					from Peticion p
					where 
						(@tipoproj = 'S' AND p.pet_projid = @projid and
							p.pet_projsubid = @projsubid and 
							p.pet_projsubsid = @projsubsid) OR 
						(@tipoproj <> 'S' AND p.pet_projid = @projid and
							((@projsubid is null OR @projsubid = 0) OR p.pet_projsubid = @projsubid) AND
							((@projsubsid is null OR @projsubsid = 0) OR p.pet_projsubsid = @projsubsid)))
				) OR 
				(@incluir='ALL' AND (Ht.pet_nrointerno = 0 OR Ht.pet_nrointerno IN ( 
					select p.pet_nrointerno
					from Peticion p
					where 
						(@tipoproj = 'S' AND p.pet_projid = @projid and
							p.pet_projsubid = @projsubid and 
							p.pet_projsubsid = @projsubsid) OR 
						(@tipoproj <> 'S' AND p.pet_projid = @projid and
							((@projsubid is null OR @projsubid = 0) OR p.pet_projsubid = @projsubid) AND
							((@projsubsid is null OR @projsubsid = 0) OR p.pet_projsubsid = @projsubsid))))
				)
			)
			--}
		order by 
			cod_direccion,
			cod_gerencia,
			cod_sector,
			cod_grupo,
			cod_recurso
		for read only

		select @cod_direccion_ant = ''
		select @cod_gerencia_ant  = ''
		select @cod_sector_ant    = ''
		select @cod_grupo_ant     = ''
		select @cod_recurso_ant   = ''

		OPEN CursHoras
		FETCH CursHoras INTO
			@cod_direccion,
			@cod_gerencia,
			@cod_sector,
			@cod_grupo,
			@cod_recurso,
			@cod_tarea,
			@nom_tarea,
			@pet_nrointerno,
			@pet_nroasignado,
			@cod_clase,			-- add -002- a.
			@titulo,
			@trabsinasignar,
			@fe_desde,
			@fe_hasta,
			@horas,
			--@origen,
			--{ add -005- a.
			@cod_tipo_peticion,
			@pet_regulatorio,
			@pet_projid,
			@pet_projsubid,
			@pet_projsubsid,
			@proyecto_idm,
			@agrupId			-- add -007- a.
			--}
		WHILE (@@sqlstatus = 0)
			BEGIN
				execute sp_GetHorasPeriodo @sol_desde, @sol_hasta, @fe_desde, @fe_hasta, @horas, @ret_desde OUTPUT, @ret_hasta OUTPUT, @ret_dias OUTPUT, @ret_horas OUTPUT

				if RTRIM(@trabsinasignar)='S'
					begin
						select @horas_si = 0
						select @horas_no = @ret_horas
					end
				else
					begin
						select @horas_si = @ret_horas
						select @horas_no = 0
					end

				if @cod_tarea is null or RTRIM(@cod_tarea) is null or RTRIM(@cod_tarea)=''
					begin
						select @tip_asig='PET'
						select @cod_asig=convert(varchar(10),@pet_nroasignado)
						select @cod_real=convert(varchar(10),@pet_nrointerno)
						select @nom_asig=@titulo
					end
				else
					begin
						select @tip_asig='TAR'
						select @cod_clase=''			-- add -002- a.
						select @cod_tipo_peticion=''
						select @pet_regulatorio=''
						select @proyecto_idm=''
						select @cod_asig=@cod_tarea
						select @cod_real=@cod_tarea
						select @nom_asig=@nom_tarea
					end

				--{ add -004- a.
				if @cod_direccion <> @cod_direccion_ant
					begin
						select @nom_direccion_act = (select nom_direccion from GesPet..Direccion where cod_direccion = @cod_direccion)
						select @cod_direccion_ant = @cod_direccion
					end
				if @cod_gerencia <> @cod_gerencia_ant and @detalle in ('0', '1', '2', '3', '4')
					begin
						select @nom_gerencia_act = (select nom_gerencia from GesPet..Gerencia where cod_gerencia = @cod_gerencia)
						select @cod_gerencia_ant = @cod_gerencia
					end
				if @cod_sector <> @cod_sector_ant and @detalle in ('0', '1', '2', '3')
					begin
						select @nom_sector_act = (select nom_sector from GesPet..Sector where cod_sector = @cod_sector)
						select @cod_sector_ant = @cod_sector
					end
				if @cod_grupo <> @cod_grupo_ant and @detalle in ('0', '1', '2')
					begin
						select @nom_grupo_act = (select nom_grupo from GesPet..Grupo where cod_grupo = @cod_grupo)
						select @cod_grupo_ant = @cod_grupo
					end
				if @cod_recurso <> @cod_recurso_ant and @detalle in ('0', '1')
					begin
						select @nom_recurso_act = (select nom_recurso from GesPet..vw_RecursoFabrica where cod_recurso = @cod_recurso)
						select @cod_recurso_ant = @cod_recurso
					end
				--}

				--{ add -001- a.
				if @tip_asig = 'TAR'
					insert #TmpSalida
						(cod_direccion,
						cod_gerencia,
						cod_sector,
						cod_grupo,
						cod_recurso,
						--{ add -004- a.
						nom_direccion,
						nom_gerencia,
						nom_sector,
						nom_grupo,
						nom_recurso,
						--}
						tip_asig,
						cod_asig,
						cod_real,
						nom_asig,
						cod_clase,			-- add -002- a.
						entre_desde,
						entre_hasta,
						horas_si,
						horas_no,
						horas_tarea,		-- add -001- a.
						--origen,
						--{ add -005- a.
						cod_tipo_peticion,
						pet_regulatorio,
						proyecto_idm,
						agrupId,
						pet_projid,
						pet_projsubid,
						pet_projsubsid)
						--}
					values
						(@cod_direccion,
						@cod_gerencia,
						@cod_sector,
						@cod_grupo,
						@cod_recurso,
						--{ add -004- a.
						@nom_direccion_act,
						@nom_gerencia_act,
						@nom_sector_act,
						@nom_grupo_act,
						@nom_recurso_act,
						--}
						@tip_asig,
						@cod_asig,
						@cod_real,
						@nom_asig,
						@cod_clase,					-- add -002- a.
						@ret_desde,
						@ret_hasta,
						0,
						0,
						@horas_si + @horas_no,		-- add -001- a.
						--@origen,
						--{ add -005- a.
						@cod_tipo_peticion,
						@pet_regulatorio,
						@proyecto_idm,
						0,
						0,
						0,
						0)
						--}
				else
				--}
					insert #TmpSalida
						(cod_direccion,
						cod_gerencia,
						cod_sector,
						cod_grupo,
						cod_recurso,
						nom_direccion,		-- add -004- a.
						nom_gerencia,		-- add -004- a.
						nom_sector,		-- add -004- a.
						nom_grupo,		-- add -004- a.
						nom_recurso,		-- add -004- a.
						tip_asig,
						cod_asig,
						cod_real,
						cod_clase,		-- add -002- a.
						nom_asig,
						entre_desde,
						entre_hasta,
						horas_si,
						horas_no,
						horas_tarea,	-- add -001- a.
						--origen,
						--{ add -005- a.
						cod_tipo_peticion,
						pet_regulatorio,
						proyecto_idm,
						agrupId,
						pet_projid,
						pet_projsubid,
						pet_projsubsid)
						--}
					values
						(@cod_direccion,
						@cod_gerencia,
						@cod_sector,
						@cod_grupo,
						@cod_recurso,
						@nom_direccion_act,	-- add -004- a.
						@nom_gerencia_act,	-- add -004- a.
						@nom_sector_act,	-- add -004- a.
						@nom_grupo_act,		-- add -004- a.
						@nom_recurso_act,	-- add -004- a.
						@tip_asig,
						@cod_asig,
						@cod_real,
						@cod_clase,			-- add -002- a.
						@nom_asig,
						@ret_desde,
						@ret_hasta,
						@horas_si,
						@horas_no,
						0,					-- add -001- a.
						--@origen,
						--{ add -005- a.
						@cod_tipo_peticion,
						@pet_regulatorio,
						@proyecto_idm,
						@agrupId,
						@pet_projid,
						@pet_projsubid,
						@pet_projsubsid)
						--}
				FETCH CursHoras INTO
					@cod_direccion,
					@cod_gerencia,
					@cod_sector,
					@cod_grupo,
					@cod_recurso,
					@cod_tarea,
					@nom_tarea,
					@pet_nrointerno,
					@pet_nroasignado,
					@cod_clase,		-- add -002- a.
					@titulo,
					@trabsinasignar,
					@fe_desde,
					@fe_hasta,
					@horas,
					--@origen,
					--{ add -005- a.
					@cod_tipo_peticion,
					@pet_regulatorio,
					@pet_projid,
					@pet_projsubid,
					@pet_projsubsid,
					@proyecto_idm,
					@agrupId
					--}
			END
		CLOSE CursHoras
		DEALLOCATE CURSOR CursHoras
	
	if @detalle='0'
		select  
			TS.cod_direccion,
			TS.nom_direccion,										-- add -004- a.
			TS.cod_gerencia,
			TS.nom_gerencia,										-- add -004- a.
			TS.cod_sector,
			TS.nom_sector,											-- add -004- a.
			TS.cod_grupo,
			TS.nom_grupo,											-- add -004- a.
			TS.cod_recurso,
			TS.nom_recurso,											-- add -004- a.
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			TS.entre_desde as desde,
			TS.entre_hasta as hasta,
			convert(numeric(10,2),TS.horas_si/6000) as asignadas_si,
			convert(numeric(10,2),TS.horas_no/6000) as asignadas_no,
			convert(numeric(10,2),TS.horas_tarea/6000) as hs_tareas,	-- add -001- a.
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId,					-- add -007- a.
			agr_nombre = (select x.agr_titulo from Agrup x where x.agr_nrointerno = TS.agrupId),
			prj_nombre = (select x.ProjNom from ProyectoIDM x where x.ProjId = TS.pet_projid and x.ProjSubId = TS.pet_projsubid and x.ProjSubSId = TS.pet_projsubsid)
			--}
		from #TmpSalida  TS
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.cod_recurso,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.nom_asig
	
	if @detalle='1'
		select  distinct
			TS.cod_direccion,
			TS.nom_direccion,										-- add -004- a.
			TS.cod_gerencia,
			TS.nom_gerencia,										-- add -004- a.
			TS.cod_sector,
			TS.nom_sector,											-- add -004- a.
			TS.cod_grupo,
			TS.nom_grupo,											-- add -004- a.
			TS.cod_recurso,
			TS.nom_recurso,											-- add -004- a.
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			min(TS.entre_desde) as desde,
			max(TS.entre_hasta) as hasta,
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no,
			convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas,	-- add -001- a.
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId,					-- add -007- a.
			agr_nombre = (select x.agr_titulo from Agrup x where x.agr_nrointerno = TS.agrupId),
			prj_nombre = (select x.ProjNom from ProyectoIDM x where x.ProjId = TS.pet_projid and x.ProjSubId = TS.pet_projsubid and x.ProjSubSId = TS.pet_projsubsid)
			--}
		from #TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.cod_recurso,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm
			--}
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.cod_recurso,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.nom_asig
	
	if @detalle='2'
		select  distinct
			TS.cod_direccion,
			TS.nom_direccion,										-- add -004- a.
			TS.cod_gerencia,
			TS.nom_gerencia,										-- add -004- a.
			TS.cod_sector,
			TS.nom_sector,											-- add -004- a.
			TS.cod_grupo,
			TS.nom_grupo,											-- add -004- a.
			'' as cod_recurso,
			'' as nom_recurso,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			min(TS.entre_desde) as desde,
			max(TS.entre_hasta) as hasta,
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no,
			convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas,	-- add -001- a.
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId,					-- add -007- a.
			agr_nombre = (select x.agr_titulo from Agrup x where x.agr_nrointerno = TS.agrupId),
			prj_nombre = (select x.ProjNom from ProyectoIDM x where x.ProjId = TS.pet_projid and x.ProjSubId = TS.pet_projsubid and x.ProjSubSId = TS.pet_projsubsid)
			--}
		from #TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId					-- add -007- a.
			--}
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.nom_asig

	if @detalle='3'
		select  distinct
			TS.cod_direccion,
			TS.nom_direccion,										-- add -004- a.
			TS.cod_gerencia,
			TS.nom_gerencia,										-- add -004- a.
			TS.cod_sector,
			TS.nom_sector,											-- add -004- a.
			'' as cod_grupo,
			'' as nom_grupo,
			'' as cod_recurso,
			'' as nom_recurso,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			min(TS.entre_desde) as desde,
			max(TS.entre_hasta) as hasta,
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no,
			convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas,	-- add -001- a.
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId,					-- add -007- a.
			agr_nombre = (select x.agr_titulo from Agrup x where x.agr_nrointerno = TS.agrupId),
			prj_nombre = (select x.ProjNom from ProyectoIDM x where x.ProjId = TS.pet_projid and x.ProjSubId = TS.pet_projsubid and x.ProjSubSId = TS.pet_projsubsid)
			--}
		from #TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId					-- add -007- a.
			--}
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.nom_asig

	if @detalle='4'
		select  distinct
			TS.cod_direccion,
			TS.nom_direccion,										-- add -004- a.
			TS.cod_gerencia,
			TS.nom_gerencia,										-- add -004- a.
			'' as cod_sector,
			'' as nom_sector,
			'' as cod_grupo,
			'' as nom_grupo,
			'' as cod_recurso,
			'' as nom_recurso,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			min(TS.entre_desde) as desde,
			max(TS.entre_hasta) as hasta,
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no,
			convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas,	-- add -001- a.
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId,					-- add -007- a.
			agr_nombre = (select x.agr_titulo from Agrup x where x.agr_nrointerno = TS.agrupId),
			prj_nombre = (select x.ProjNom from ProyectoIDM x where x.ProjId = TS.pet_projid and x.ProjSubId = TS.pet_projsubid and x.ProjSubSId = TS.pet_projsubsid)
			--}
		from #TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId					-- add -007- a.
			--}
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.nom_asig
	
	if @detalle='5'
		select  distinct
			TS.cod_direccion,
			TS.nom_direccion,										-- add -004- a.
			'' as cod_gerencia,
			'' as nom_gerencia,
			'' as cod_sector,
			'' as nom_sector,
			'' as cod_grupo,
			'' as nom_grupo,
			'' as cod_recurso,
			'' as nom_recurso,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			min(TS.entre_desde) as desde,
			max(TS.entre_hasta) as hasta,
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no,
			convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas,	-- add -001- a.
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId,					-- add -007- a.
			agr_nombre = (select x.agr_titulo from Agrup x where x.agr_nrointerno = TS.agrupId),
			prj_nombre = (select x.ProjNom from ProyectoIDM x where x.ProjId = TS.pet_projid and x.ProjSubId = TS.pet_projsubid and x.ProjSubSId = TS.pet_projsubsid)
			--}
		from #TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.cod_clase,	-- add -002- a.
			TS.nom_asig,
			--{ add -005- a.
			TS.cod_tipo_peticion,
			TS.pet_regulatorio,
			TS.proyecto_idm,
			TS.agrupId					-- add -007- a.
			--}
		order by 
			TS.cod_direccion,
			TS.tip_asig,
			TS.cod_real,
			TS.cod_asig,
			TS.nom_asig
	return(0)
go

grant execute on dbo.sp_rptHsTrabAreaEjec to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go
