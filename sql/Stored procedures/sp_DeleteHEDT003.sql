/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
-001- a. FJS 02.11.2009 - Se modifica la longitud de la variable porque no esta eliminando cuando supera el valor y se simplifican los parámetros del SP.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteHEDT003'
go

if exists (select * from sysobjects where name = 'sp_DeleteHEDT003' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHEDT003
go

create procedure dbo.sp_DeleteHEDT003
	@dsn_id			char(8)=null,
	@cpy_id			char(8)=null,
	@cpo_id			char(18)=null	-- upd -001- a.
	/* del -001- a.
	,
	@cpo_nrobyte	char(4)=null,
	@cpo_canbyte	char(4)=null,
	@cpo_type		char(1)=null,
	@cpo_decimals	char(4)=null,
	@cpo_dsc		char(50)=null,
	@cpo_nomrut		char(8)=null,
	@cpo_feult		smalldatetime=null,
	@cpo_userid		char(10)=null
	*/
as
	delete from
		GesPet.dbo.HEDT003
	where
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)
		/* del -001- a.
		and
		(cpo_nrobyte = @cpo_nrobyte or @cpo_nrobyte is null) and
		(cpo_canbyte = @cpo_canbyte or @cpo_canbyte is null) and
		(cpo_type = @cpo_type or @cpo_type is null) and
		(cpo_decimals = @cpo_decimals or @cpo_decimals is null) and
		(cpo_dsc = @cpo_dsc or @cpo_dsc is null) and
		(cpo_nomrut = @cpo_nomrut or @cpo_nomrut is null) and
		(cpo_feult = @cpo_feult or @cpo_feult is null) and
		(cpo_userid = @cpo_userid or @cpo_userid is null)
		*/
go

grant execute on dbo.sp_DeleteHEDT003 to GesPetUsr
go

print 'Actualización realizada.'
