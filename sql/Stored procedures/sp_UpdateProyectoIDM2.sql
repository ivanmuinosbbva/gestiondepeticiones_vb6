/* TSQL / Sybase */
/*
-000- a. FJS 05.05.2009 - Actualiza los nuevos atributos de la tabla ProyectoIDM (para la migración inicial).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDM2'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDM2' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDM2
go

create procedure dbo.sp_UpdateProyectoIDM2
	@ProjId				int,
	@ProjSubId			int,
	@ProjSubSId			int,
	@ProjNom			varchar(100),
	@ProjCatId			int,
	@ProjClaseId		int,
	@cod_estado			char(6),
	@marca				char(1),
	@plan_tyo			char(1),
	--@empresa			char(1),
	@empresa			int,
	@area				char(1),
	@clas3				char(1),
	@semaphore			char(1),
	@cod_direccion		char(8),
	@cod_gerencia		char(8),
	@cod_sector			char(8),
	@cod_direccion_p	char(8),
	@cod_gerencia_p		char(8),
	@cod_sector_p		char(8),
	@cod_grupo_p		char(8),
	@avance				int,
	@fe_inicio			smalldatetime,
	@fe_fin				smalldatetime,
	@fe_finreprog		smalldatetime,
	@fe_ult_nove		smalldatetime
as
	update 
		GesPet.dbo.ProyectoIDM
	set
		ProjNom			= @ProjNom,
		ProjFchAlta     = getdate(),
		ProjCatId       = @ProjCatId,
		ProjClaseId     = @ProjClaseId,
		cod_estado      = @cod_estado,
		fch_estado      = getdate(),
		usr_estado      = SUSER_NAME(),
		marca           = @marca,
		plan_tyo		= @plan_tyo,
		--empresa			= @empresa,
		codigoEmpresa	= @empresa,
		area			= @area,
		clas3			= @clas3,
		semaphore		= @semaphore,
		cod_direccion	= @cod_direccion,
		cod_gerencia	= @cod_gerencia,
		cod_sector		= @cod_sector,
		cod_direccion_p = @cod_direccion_p,
		cod_gerencia_p  = @cod_gerencia_p,
		cod_sector_p	= @cod_sector_p,
		cod_grupo_p		= @cod_grupo_p,
		avance			= @avance,
		fe_modif		= getdate(),
		fe_inicio		= @fe_inicio,
		fe_fin			= @fe_fin,
		fe_finreprog	= @fe_finreprog,
		fe_ult_nove		= @fe_ult_nove
	where 
		ProjId			= @ProjId and
		ProjSubId		= @ProjSubId and
		ProjSubSId		= @ProjSubSId
	return(0)
go

grant execute on dbo.sp_UpdateProyectoIDM2 to GesPetUsr
go

print 'Actualización realizada.'
go
