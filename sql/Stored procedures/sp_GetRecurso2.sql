/*
-000- a. FJS 16.05.2008 - Nuevo SP para utilizar en la selecci�n de recursos y perfiles din�mico.
*/

print 'Creando/actualizando SP: sp_GetRecurso2'
go

if exists (select * from sysobjects where name = 'sp_GetRecurso2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecurso2
go

create procedure dbo.sp_GetRecurso2 
	@nom_recurso	char(50),
	@nom_direccion	char(80),
	@nom_gerencia	char(80),
	@nom_sector		char(80),
	@nom_grupo		char(80),
	@flg_cargoarea	char(1),
	@estado_recurso	char(1),
	@perfil			char(100)
as  
	select
		r.cod_recurso,
		r.nom_recurso,
		nom_recurso_plano = (str_replace(str_replace(str_replace(str_replace(str_replace(r.nom_recurso,'�','u'),'�','o'),'�','i'),'�','e'),'�','a')),
		nom_direccion = (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = r.cod_direccion),
		nom_gerencia  = (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = r.cod_gerencia),
		nom_sector	  = (select x.nom_sector from GesPet..Sector x where x.cod_sector = r.cod_sector),
		nom_grupo	  = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = r.cod_grupo),
		r.flg_cargoarea,
	    r.estado_recurso
	from 
		GesPet..Recurso r
	where 
		(@nom_recurso is null or r.nom_recurso like '%' + LTRIM(RTRIM(@nom_recurso)) + '%') and
		@nom_direccion
		@nom_gerencia
		@nom_sector
		@nom_grupo
		@flg_cargoarea
		@estado_recurso
		@perfil


	order by
		r.nom_recurso
	return(0)
go

grant Execute  on sp_GetRecurso2 to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go




		perfiles = (
			LTRIM(isnull((select '|SOLI' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'SOLI'),''))+
			LTRIM(isnull((select '|REFE' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'REFE'),''))+
			LTRIM(isnull((select '|SUPE' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'SUPE'),''))+
			LTRIM(isnull((select '|AUTO' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'AUTO'),''))+
			LTRIM(isnull((select '|ADMI' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ADMI'),''))+
			LTRIM(isnull((select '|SADM' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'SADM'),''))+
			LTRIM(isnull((select '|CGCI' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CGCI'),''))+
			LTRIM(isnull((select '|CDIR' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CDIR'),''))+
			LTRIM(isnull((select '|CHRS' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CHRS'),''))+
			LTRIM(isnull((select '|CGRU' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CGRU'),''))+
			LTRIM(isnull((select '|CSEC' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CSEC'),''))+
			LTRIM(isnull((select '|ASEG' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ASEG'),''))+
			LTRIM(isnull((select '|BPAR' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'BPAR'),''))+
			LTRIM(isnull((select '|CTLG' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CTLG'),''))+
			LTRIM(isnull((select '|ANAL' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ANAL'),''))+
			LTRIM(isnull((select '|PRJ1' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'PRJ1'),''))+
			LTRIM(isnull((select '|ADM1' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ADM1'),''))+
			LTRIM(isnull((select '|PRJ2' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'PRJ2'),'')))



/*

select
		r.cod_recurso,
		r.nom_recurso,
		nom_recurso_plano = (str_replace(str_replace(str_replace(str_replace(str_replace(r.nom_recurso,'�','u'),'�','o'),'�','i'),'�','e'),'�','a')),
		nom_direccion = (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = r.cod_direccion),
		nom_gerencia  = (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = r.cod_gerencia),
		nom_sector	  = (select x.nom_sector from GesPet..Sector x where x.cod_sector = r.cod_sector),
		nom_grupo	  = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = r.cod_grupo),
		r.flg_cargoarea,
	    r.estado_recurso,
		perfiles = (
			LTRIM(isnull((select '|SOLI' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'SOLI'),''))+
			LTRIM(isnull((select '|REFE' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'REFE'),''))+
			LTRIM(isnull((select '|SUPE' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'SUPE'),''))+
			LTRIM(isnull((select '|AUTO' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'AUTO'),''))+
			LTRIM(isnull((select '|ADMI' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ADMI'),''))+
			LTRIM(isnull((select '|SADM' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'SADM'),''))+
			LTRIM(isnull((select '|CGCI' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CGCI'),''))+
			LTRIM(isnull((select '|CDIR' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CDIR'),''))+
			LTRIM(isnull((select '|CHRS' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CHRS'),''))+
			LTRIM(isnull((select '|CGRU' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CGRU'),''))+
			LTRIM(isnull((select '|CSEC' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CSEC'),''))+
			LTRIM(isnull((select '|ASEG' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ASEG'),''))+
			LTRIM(isnull((select '|BPAR' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'BPAR'),''))+
			LTRIM(isnull((select '|CTLG' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'CTLG'),''))+
			LTRIM(isnull((select '|ANAL' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ANAL'),''))+
			LTRIM(isnull((select '|PRJ1' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'PRJ1'),''))+
			LTRIM(isnull((select '|ADM1' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'ADM1'),''))+
			LTRIM(isnull((select '|PRJ2' from GesPet..RecursoPerfil t where t.cod_recurso = r.cod_recurso and t.cod_perfil = 'PRJ2'),'')))
	from 
		GesPet..Recurso r
	order by
		r.nom_recurso
	return(0)
*/