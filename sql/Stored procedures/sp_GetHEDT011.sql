/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 20.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT011'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT011' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT011
go

create procedure dbo.sp_GetHEDT011
	@tipo_id	char(1)=null,
	@tipo_nom	char(50)=null
as
	select 
		a.tipo_id,
		a.tipo_nom
	from 
		GesPet.dbo.HEDT011 a
	where
		(a.tipo_id = @tipo_id or @tipo_id is null) and
		(a.tipo_nom = @tipo_nom or @tipo_nom is null)
go

grant execute on dbo.sp_GetHEDT011 to GesPetUsr
go

print 'Actualización realizada.'