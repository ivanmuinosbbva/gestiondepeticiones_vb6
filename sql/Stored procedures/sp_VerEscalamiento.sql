/*
-001- a. FJS 25.02.2008 - Se deshabilita el resultado para no generar el proceso de escalamiento.
*/

use GesPet
go

print 'Actualizando el procedimiento almacenado: sp_VerEscalamiento'
go

if exists (select * from sysobjects where name = 'sp_VerEscalamiento' and sysstat & 7 = 4)
	drop procedure dbo.sp_VerEscalamiento
go

create procedure dbo.sp_VerEscalamiento 
as 
declare @hst_nrointerno  int 
declare @pet_nrointerno int 
declare @pzo_desde int 
declare @pzo_hasta int 
declare @pzo_hoy int 
declare @cod_hijo varchar(8) 
declare @cod_grupo varchar(8) 
declare @cod_sector varchar(8) 
declare @cod_estado varchar(6) 
declare @cod_accion varchar(8) 
declare @ult_accion varchar(8) 
declare @fe_ini_plan smalldatetime 
declare @fe_fin_plan smalldatetime 
declare @fec_estado smalldatetime 
declare @fec_hoy smalldatetime 
declare @nom_estado varchar(40) 
declare @aux_txtmsg varchar(250) 
declare @aux_txtprx varchar(250) 
declare @txt_extend varchar(250) 
declare @txt_pzofin varchar(1) 
 
select @fec_hoy = getdate() 
 
declare @fIniPlan    smalldatetime 
declare @fFinPlan    smalldatetime 
declare @fIniReal    smalldatetime 
declare @fFinReal    smalldatetime 
declare @hsPresup    smallint 
 
 
declare     @pet_estado char(6) 
declare     @sec_estado char(6) 
declare     @gru_estado char(6) 
 
 
/* 
PRIMERO LOS GRUPOS 
SI ALGUN GRUPO CAMBIA, QUIZA CAMBIE EL SECTOR 
*/ 
DECLARE CursGrupo CURSOR FOR 
select  
    PSG.pet_nrointerno, 
    PSG.cod_sector, 
    PSG.cod_grupo, 
    PSG.cod_estado, 
    PSG.ult_accion, 
    PSG.fe_estado, 
    PSG.fe_ini_plan, 
    PSG.fe_fin_plan, 
    ESC.pzo_desde, 
    ESC.pzo_hasta, 
    ESC.cod_accion 
from
    Peticion PET,   -- add -001-
    PeticionGrupo PSG, 
    Escalamientos ESC 
where   
	--{ add -001-
    (PET.pet_nrointerno = PSG.pet_nrointerno) and
	(PET.cod_tipo_peticion <> PET.cod_tipo_peticion) and		-- Esta línea deshabilita todo el proceso
	--}
    (RTRIM(ESC.evt_alcance) = 'GRU') and 
    (RTRIM(ESC.cod_estado) = RTRIM(PSG.cod_estado)) 
order by 
	PSG.pet_nrointerno 
for read only
 
OPEN CursGrupo 
FETCH CursGrupo INTO 
    @pet_nrointerno, 
    @cod_sector, 
    @cod_grupo, 
    @cod_estado, 
    @ult_accion, 
    @fec_estado, 
    @fe_ini_plan, 
    @fe_fin_plan, 
    @pzo_desde, 
    @pzo_hasta, 
    @cod_accion 
WHILE (@@sqlstatus = 0) 
BEGIN 
    select @nom_estado = nom_estado from Estados where  RTRIM(cod_estado)=RTRIM(@cod_estado) 
    select @aux_txtprx='' 
    select @aux_txtmsg='' 
 
    if @cod_estado<>'PLANOK' and @cod_estado<>'EJECUC' 
    begin 
        select @pzo_hoy = datediff(day,@fec_estado,@fec_hoy) 
        select @aux_txtmsg=' El Grupo se encuentra en el estado: ' + RTRIM(@nom_estado) + '  desde el: ' + convert(varchar(10),@fec_estado,103) + '.' 
    end 
    if @cod_estado='PLANOK' 
    begin 
        select @pzo_hoy = datediff(day,@fe_ini_plan,@fec_hoy) 
        select @aux_txtmsg=' El Grupo se encuentra en el  estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de inicio: ' + convert(varchar(10),@fe_ini_plan,103) + '.' 
    end 
    if @cod_estado='EJECUC' 
    begin 
        select @pzo_hoy = datediff(day,@fe_fin_plan,@fec_hoy) 
        select @aux_txtmsg=' El Grupo se  encuentra en el estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de finalizacion: ' + convert(varchar(10),@fe_fin_plan,103) + '.' 
    end 
    if @cod_accion<>'CANCGRU' and @cod_accion<>'REVIGRU' 
    BEGIN 
        if @cod_estado<>'PLANOK' and @cod_estado<>'EJECUC' 
            select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fec_estado),103) + '.' 
 
        if @cod_estado='PLANOK' 
            select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_ini_plan),103) + '.' 
 
        if @cod_estado='EJECUC' 
            select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_fin_plan),103) + '.' 
    END 
 
        /* 
        print   '%1! %2! %3! %4! %5! %6! %7! %8! %9! %10!', 
            @cod_accion, 
            @pet_nrointerno, 
            @cod_grupo, 
            @cod_estado, 
            @fec_estado, 
            @fe_ini_plan, 
            @fe_fin_plan, 
            @pzo_desde, 
            @pzo_hoy, 
            @pzo_hasta 
        */ 
    -- Si la cantidad de dias transcurridos desde el estado del grupo hasta hoy se encuentra comprendido para el evento... 
    if (@pzo_hoy >= @pzo_desde and @pzo_hoy <= @pzo_hasta) and (RTRIM(@ult_accion) is null or RTRIM(@ult_accion)='' or RTRIM(@ult_accion)<>RTRIM(@cod_accion)) 
    begin 
        /* 
        print   '%1! %2! %3! %4! %5! %6! %7! %8! %9! %10!', 
            @cod_accion, 
            @pet_nrointerno, 
            @cod_grupo, 
            @cod_estado, 
            @fec_estado, 
            @fe_ini_plan, 
            @fe_fin_plan, 
            @pzo_desde, 
            @pzo_hoy, 
            @pzo_hasta 
        */ 
        if @cod_accion='ESC1GRU' 
        BEGIN 
            execute sp_UpdatePetSubField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_grupo=@cod_grupo, 
                @campo='SITUAC', 
                @valortxt='DEMOR1', 
                @valordate=null, 
                @valornum=null 
        END 
        if @cod_accion='ESC2GRU' 
        BEGIN 
            execute sp_UpdatePetSubField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_grupo=@cod_grupo, 
                @campo='SITUAC', 
                @valortxt='DEMOR2', 
                @valordate=null, 
                @valornum=null 
        END 
        if @cod_accion='REVIGRU' 
        BEGIN 
            execute sp_UpdatePetSubField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_grupo=@cod_grupo, 
                @campo='ESTADO', 
                @valortxt='REVISA', 
                @valordate=null, 
                @valornum=null 
                select @cod_estado='REVISA'  
        END 
        if @cod_accion='CANCGRU' 
        BEGIN 
            execute sp_UpdatePetSubField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_grupo=@cod_grupo, 
                @campo='ESTADO', 
                @valortxt='CANCEL', 
                @valordate=null, 
                @valornum=null 
            select  @cod_estado='CANCEL' 
        END 
        if @cod_accion='REVIGRU' or @cod_accion='CANCGRU' 
        BEGIN 
            execute sp_UpdatePetSubField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_grupo=@cod_grupo, 
                @campo='SITUAC', 
                @valortxt='', 
                @valordate=null, 
                @valornum=null 
            execute sp_GetIntegracionGrupos 
                @pet_nrointerno, 
                @cod_sector, 
                @fIniPlan OUTPUT, 
                @fFinPlan OUTPUT, 
                @fIniReal OUTPUT, 
                @fFinReal OUTPUT, 
                @hsPresup OUTPUT, 
                @sec_estado OUTPUT 
            execute sp_UpdatePetSecField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_sector=@cod_sector, 
                @campo='ESTADO', 
                @valortxt=@sec_estado, 
                @valordate=null, 
                @valornum=null 
            execute sp_UpdatePetSecFechas 
                @pet_nrointerno, 
                @cod_sector, 
                @fIniPlan, 
                @fFinPlan, 
                @fIniReal, 
                @fFinReal, 
                @hsPresup 
            execute sp_GetIntegracionSectores 
                @pet_nrointerno, 
                @fIniPlan OUTPUT, 
                @fFinPlan OUTPUT, 
                @fIniReal OUTPUT, 
                @fFinReal OUTPUT, 
                @hsPresup OUTPUT, 
                @pet_estado OUTPUT 
            execute sp_UpdatePetField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_sector=@cod_sector, 
                @campo='ESTADO', 
                @valortxt=@pet_estado, 
                @valordate=null, 
                @valornum=null 
            execute sp_UpdatePetFechas 
                @pet_nrointerno, 
                @fIniPlan, 
                @fFinPlan, 
                @fIniReal, 
                @fFinReal, 
                @hsPresup 
        END 
 
        if @cod_accion='REVIGRU' 
        BEGIN 
            execute sp_AddHistorial  
                @hst_nrointerno  OUTPUT,  
                @pet_nrointerno,  
                'GCHGEST',  
                @pet_estado,  
                @cod_sector,  
                @sec_estado,  
                @cod_grupo,  
                'REVISA',  
                ''  
            insert into HistorialMemo (hst_nrointerno,hst_secuencia,mem_texto)    
            values   (@hst_nrointerno,0,'Cambio  automático de estado')    
        END 
 
 
        if @cod_accion='CANCGRU' 
        BEGIN 
            execute sp_AddHistorial  
                @hst_nrointerno  OUTPUT,  
                @pet_nrointerno,  
                'GCHGEST',  
                @pet_estado,  
                @cod_sector,  
                @sec_estado,  
                @cod_grupo,  
                'CANCEL',  
                '' 
            insert into HistorialMemo (hst_nrointerno,hst_secuencia,mem_texto)    
            values   (@hst_nrointerno,0,'Cambio automático de estado')    
        END 
 
        execute sp_UpdatePetSubField 
            @pet_nrointerno=@pet_nrointerno, 
            @cod_grupo=@cod_grupo, 
            @campo='ULTACC', 
            @valortxt=@cod_accion, 
            @valordate=null, 
            @valornum=null 
        execute sp_DoMensaje 
            @evt_alcance='GRU', 
            @cod_accion=@cod_accion, 
            @pet_nrointerno=@pet_nrointerno, 
            @cod_hijo=@cod_grupo, 
            @dsc_estado=@cod_estado, 
            @txt_txtmsg=@aux_txtmsg, 
            @txt_txtprx=@aux_txtprx 
 
    end 
    FETCH CursGrupo INTO 
        @pet_nrointerno, 
        @cod_sector, 
        @cod_grupo, 
        @cod_estado, 
        @ult_accion, 
        @fec_estado, 
        @fe_ini_plan, 
        @fe_fin_plan, 
        @pzo_desde, 
        @pzo_hasta, 
        @cod_accion 
END 
CLOSE CursGrupo 
DEALLOCATE CURSOR CursGrupo 
/* FIN GRUPO */ 
 
/* DESPUES LOS SECTORES */ 
DECLARE CursSector CURSOR FOR 
select  
    PGR.pet_nrointerno, 
    PGR.cod_sector, 
    PGR.cod_estado, 
    PGR.ult_accion, 
    PGR.fe_estado, 
    PGR.fe_ini_plan, 
    PGR.fe_fin_plan, 
    ESC.pzo_desde, 
    ESC.pzo_hasta, 
    ESC.cod_accion 
from    
    Peticion PET,           -- add -001-
    PeticionSector PGR, 
    Escalamientos ESC 
where   
	--{ add -001-
    (PET.pet_nrointerno = PGR.pet_nrointerno) and
	(PET.cod_tipo_peticion <> PET.cod_tipo_peticion) and		-- Esta línea desactiva todo el proceso.
	--}
    (RTRIM(ESC.evt_alcance) = 'SEC') and 
    (RTRIM(ESC.cod_estado) = RTRIM(PGR.cod_estado)) 
for read only 
 
OPEN CursSector 
FETCH CursSector INTO 
    @pet_nrointerno, 
    @cod_sector, 
    @cod_estado, 
    @ult_accion, 
    @fec_estado, 
    @fe_ini_plan, 
    @fe_fin_plan, 
    @pzo_desde, 
    @pzo_hasta, 
    @cod_accion 
WHILE (@@sqlstatus = 0) 
BEGIN 
    select @nom_estado = nom_estado from Estados where RTRIM(cod_estado)=RTRIM(@cod_estado) 
    select @aux_txtprx='' 
    select @aux_txtmsg='' 
 
    if @cod_estado<>'PLANOK' and @cod_estado<>'EJECUC' 
    begin 
        select @pzo_hoy = datediff(day,@fec_estado,@fec_hoy)  
        select @aux_txtmsg=' El Sector se encuentra en el estado: ' + RTRIM(@nom_estado) + '  desde el: ' + convert(varchar(10),@fec_estado,103) + '.' 
    end 
    if @cod_estado='PLANOK' 
    begin 
        select @pzo_hoy = datediff(day,@fe_ini_plan,@fec_hoy) 
        select @aux_txtmsg=' El Sector se encuentra en el estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de inicio: ' + convert(varchar(10),@fe_ini_plan,103) + '.' 
    end 
    if @cod_estado='EJECUC' 
    begin 
        select @pzo_hoy = datediff(day,@fe_fin_plan,@fec_hoy) 
        select @aux_txtmsg=' El Sector se encuentra en el estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de finalizacion: ' + convert(varchar(10),@fe_fin_plan,103) + '.' 
    end 
    if @cod_accion<>'CANCSEC' and @cod_accion<>'REVISEC' 
    BEGIN 
        if @cod_estado<>'PLANOK' and @cod_estado<>'EJECUC' 
            select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fec_estado),103) + '.' 
 
        if @cod_estado='PLANOK' 
            select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_ini_plan),103) + '.' 
 
        if @cod_estado='EJECUC' 
            select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_fin_plan),103) + '.' 
    END 
        /* 
        print   '%1! %2! %3! %4! %5! %6! %7! %8! %9! %10!', 
            @cod_accion, 
            @pet_nrointerno, 
            @cod_sector, 
            @cod_estado, 
            @fec_estado, 
            @fe_ini_plan, 
            @fe_fin_plan, 
            @pzo_desde, 
            @pzo_hoy, 
            @pzo_hasta 
        */ 
    if (@pzo_hoy >= @pzo_desde and @pzo_hoy <= @pzo_hasta) and (RTRIM(@ult_accion) is null or RTRIM(@ult_accion)='' or RTRIM(@ult_accion)<>RTRIM(@cod_accion)) 
    begin 
        /* 
        print   '%1! %2! %3! %4! %5! %6! %7! %8! %9! %10!',  
            @cod_accion, 
            @pet_nrointerno, 
            @cod_sector, 
            @cod_estado, 
            @fec_estado, 
            @fe_ini_plan, 
            @fe_fin_plan, 
            @pzo_desde, 
            @pzo_hoy, 
            @pzo_hasta 
        */ 
        if @cod_accion='ESC1SEC' 
        BEGIN 
            execute sp_UpdatePetSecField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_sector=@cod_sector, 
                @campo='SITUAC', 
                @valortxt='DEMOR1', 
                @valordate=null, 
                @valornum=null 
        END 
        if @cod_accion='ESC2SEC' 
        BEGIN 
            execute sp_UpdatePetSecField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_sector=@cod_sector, 
                @campo='SITUAC', 
                @valortxt='DEMOR2', 
                @valordate=null, 
                @valornum=null 
        END 
        if @cod_accion='REVISEC' 
        BEGIN 
            execute sp_UpdatePetSecField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_sector=@cod_sector, 
                @campo='ESTADO', 
                @valortxt='REVISA', 
                @valordate=null, 
                @valornum=null 
            select  @cod_estado='REVISA' 
        END 
        if @cod_accion='CANCSEC' 
        BEGIN 
            execute sp_UpdatePetSecField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_sector=@cod_sector, 
                @campo='ESTADO', 
                @valortxt='CANCEL', 
                @valordate=null, 
                @valornum=null 
            select  @cod_estado='CANCEL' 
        END 
        if @cod_accion='REVISEC' or @cod_accion='CANCSEC' 
        BEGIN 
            execute sp_GetIntegracionSectores 
                @pet_nrointerno, 
                @fIniPlan OUTPUT, 
                @fFinPlan OUTPUT, 
                @fIniReal OUTPUT, 
                @fFinReal OUTPUT, 
                @hsPresup OUTPUT, 
                @pet_estado OUTPUT 
            execute sp_UpdatePetField 
                @pet_nrointerno=@pet_nrointerno, 
                @cod_sector=@cod_sector, 
                @campo='ESTADO', 
                @valortxt=@pet_estado, 
                @valordate=null, 
                @valornum=null 
            execute sp_HeredaEstadoGrupo 
                @pet_nrointerno, 
                @cod_sector,  
                @cod_estado,  
                Null, 
                Null,  
                Null,  
                Null, 
                0, 
                'OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|SUSPEN|REVISA', 
                'E',  
                0 
            execute sp_UpdatePetFechas 
                @pet_nrointerno, 
                @fIniPlan, 
                @fFinPlan, 
                @fIniReal, 
                @fFinReal, 
                @hsPresup 
        END 
 
        if @cod_accion='REVISEC' 
        BEGIN 
            execute sp_AddHistorial  
                @hst_nrointerno  OUTPUT,  
                @pet_nrointerno,  
                'SCHGEST',  
                @pet_estado,  
                @cod_sector,  
                'REVISA',  
                '',  
                '',  
                ''  
            insert into HistorialMemo (hst_nrointerno,hst_secuencia,mem_texto)    
            values   (@hst_nrointerno,0,'Cambio automático de estado')    
        END 
        if @cod_accion='CANCSEC' 
        BEGIN 
            execute sp_AddHistorial  
                @hst_nrointerno  OUTPUT,  
                @pet_nrointerno,  
                'SCHGEST',  
                @pet_estado,  
                @cod_sector,  
                'CANCEL',  
                '',  
                '',  
                '' 
            insert into HistorialMemo (hst_nrointerno,hst_secuencia,mem_texto)    
            values   (@hst_nrointerno,0,'Cambio automático de estado')    
        END 
        execute sp_UpdatePetSecField 
            @pet_nrointerno=@pet_nrointerno, 
            @cod_sector=@cod_sector, 
            @campo='ULTACC', 
            @valortxt=@cod_accion, 
            @valordate=null, 
            @valornum=null 
        execute sp_DoMensaje 
            @evt_alcance='SEC', 
            @cod_accion=@cod_accion, 
            @pet_nrointerno=@pet_nrointerno, 
            @cod_hijo=@cod_sector, 
            @dsc_estado=@cod_estado, 
            @txt_txtmsg=@aux_txtmsg, 
            @txt_txtprx=@aux_txtprx 
 
    end 
    FETCH CursSector INTO 
        @pet_nrointerno, 
        @cod_sector, 
        @cod_estado, 
        @ult_accion, 
        @fec_estado, 
        @fe_ini_plan, 
        @fe_fin_plan, 
        @pzo_desde, 
        @pzo_hasta, 
        @cod_accion 
END 
CLOSE CursSector 
DEALLOCATE CURSOR CursSector 
/* FIN SECTOR */ 
 
/* POR ULTIMO LAS PETICIONES */ 
DECLARE CursPeticion CURSOR FOR 
select  
    PET.pet_nrointerno, 
    PET.cod_estado, 
    PET.ult_accion, 
    PET.fe_estado, 
    ESC.pzo_desde, 
    ESC.pzo_hasta, 
    ESC.cod_accion 
from    
    Peticion PET, 
    Escalamientos ESC 
where
	(PET.cod_tipo_peticion <> PET.cod_tipo_peticion) and	-- add -001- a.	Esta línea desactiva todo el proceso
    (RTRIM(ESC.evt_alcance) = 'PET') and 
    (RTRIM(ESC.cod_estado) = RTRIM(PET.cod_estado)) 
for read only 
 
OPEN CursPeticion 
FETCH CursPeticion INTO 
    @pet_nrointerno, 
    @cod_estado, 
    @ult_accion, 
    @fec_estado, 
    @pzo_desde, 
    @pzo_hasta, 
    @cod_accion 
WHILE (@@sqlstatus = 0) 
BEGIN 
    select @pzo_hoy = datediff(day,@fec_estado,@fec_hoy) 
    select @aux_txtprx='' 
    select @aux_txtmsg='' 
 
    if (@pzo_hoy >= @pzo_desde and @pzo_hoy <= @pzo_hasta) and (RTRIM(@ult_accion) is null or RTRIM(@ult_accion)='' or RTRIM(@ult_accion)<>RTRIM(@cod_accion)) 
    begin 
/* 
        print   '%1! %2! %3! %4! %5! %6! %7!', 
            @cod_accion, 
            @ult_accion, 
            @pet_nrointerno, 
            @cod_estado, 
            @fec_estado, 
            @pzo_hoy, 
            @pzo_hasta 
*/ 
        if @cod_accion='ESC1PET' 
            execute sp_UpdatePetField 
                @pet_nrointerno=@pet_nrointerno, 
                @campo='SITUAC', 
                @valortxt='DEMOR1', 
                @valordate=null, 
                @valornum=null 
        if @cod_accion='ESC2PET' 
            execute sp_UpdatePetField 
                @pet_nrointerno=@pet_nrointerno, 
                @campo='SITUAC', 
                @valortxt='DEMOR2', 
                @valordate=null, 
                @valornum=null 
        execute sp_UpdatePetField 
            @pet_nrointerno=@pet_nrointerno, 
            @campo='ULTACC', 
            @valortxt=@cod_accion, 
            @valordate=null, 
            @valornum=null 
 
        select @nom_estado = nom_estado from Estados where RTRIM(cod_estado)=RTRIM(@cod_estado) 
        select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fec_estado),103) + '.' 
        select @aux_txtmsg=' La Peticion se encuentra en el estado: ' + RTRIM(@nom_estado) + '  desde el: ' + convert(varchar(10),@fec_estado,103) +  '.' 
 
        execute sp_DoMensaje 
            @evt_alcance='PET', 
            @cod_accion=@cod_accion, 
            @pet_nrointerno=@pet_nrointerno, 
            @cod_hijo='', 
            @dsc_estado=@cod_estado, 
            @txt_txtmsg=@aux_txtmsg, 
            @txt_txtprx=@aux_txtprx 
    end 
 
    FETCH CursPeticion INTO 
        @pet_nrointerno, 
        @cod_estado, 
        @ult_accion, 
        @fec_estado, 
        @pzo_desde, 
        @pzo_hasta, 
        @cod_accion 
END 
CLOSE CursPeticion 
DEALLOCATE CURSOR CursPeticion 
/* listo PETICION */ 
 
return(0) 
go

grant Execute on dbo.sp_VerEscalamiento to GesPetUsr 
go

print 'Actualizado.'
go
