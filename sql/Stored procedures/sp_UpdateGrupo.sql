/*
-001- a. - FJS 30.04.2008 - Se agrega la parte para guardar los datos de Homologación (identificación de grupos homologables, 
							no homologables y grupo homologador)
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateGrupo'
go

if exists (select * from sysobjects where name = 'sp_UpdateGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateGrupo
go

create procedure dbo.sp_UpdateGrupo
	@modo			char(1)  = 'A',
	@cod_grupo		char(8),
	@nom_grupo		char(80),
	@cod_sector		char(8),
	@flg_habil		char(1)  = null,
	@es_ejecutor	char(1)  = 'S',
	@cod_bpar		char(10) = null
   ,@homologacion   char(1)		-- add -001- a.
   ,@abrev			char(30) = null
as

--{ add -001- a.
-- Declaración de variables
declare @cod_grupoHomologacion  char(8)

-- Obtengo el código del grupo homologador (si existe ya definido)
select @cod_grupoHomologacion = (
    select 
        cod_grupo
    from 
        GesPet.dbo.Grupo
    where
        RTRIM(grupo_homologacion) = 'H')

-- Existe un grupo homologador ya...
if not (@cod_grupoHomologacion is null or RTRIM(@cod_grupoHomologacion) = '' or RTRIM(@cod_grupoHomologacion) = RTRIM(@cod_grupo))
    begin
        if @homologacion = 'H'
            begin
		        raiserror 30011 'Ya existe un grupo Homologador definido.' 
		        select 30011 as ErrCode , 'Grupo Homologador existente' as ErrDesc 
		        return (30011) 
            end
    end
--}

if @modo='M'
	begin
		if not exists (select cod_grupo from GesPet..Grupo where RTRIM(cod_grupo) = RTRIM(@cod_grupo))
		begin
			raiserror 30011 'Grupo Inexistente'
			select 30011 as ErrCode , 'Grupo Inexistente' as ErrDesc
			return (30011)
		end
	end
else
	begin
		if exists (select cod_grupo from GesPet..Grupo where RTRIM(cod_grupo) = RTRIM(@cod_grupo))
		begin
			raiserror 30011 'Codigo de Grupo Existente'
			select 30011 as ErrCode , 'Codigo de Grupo Existente' as ErrDesc
			return (30011)
		end
	end

if @modo='M'
	begin
		update 
			GesPet..Grupo
		set 
			nom_grupo   = @nom_grupo,
			cod_sector  = @cod_sector,
			flg_habil   = @flg_habil,
			es_ejecutor = @es_ejecutor,
			cod_bpar	= @cod_bpar
		   ,grupo_homologacion = @homologacion		-- add -001- a.
		where 
			RTRIM(cod_grupo) = RTRIM(@cod_grupo)
	end
else
	begin
		insert into GesPet..Grupo
				(cod_grupo, 
				 nom_grupo, 
				 cod_sector, 
				 flg_habil, 
				 es_ejecutor,
				 --{ add -001- a.
				 cod_bpar,
				 grupo_homologacion)
				 --}
				 --cod_bpar)	-- del -001- a.
		values
				(@cod_grupo, 
				 @nom_grupo,
				 @cod_sector,
				 @flg_habil, 
				 @es_ejecutor, 
				 --{ add -001- a.
				 @cod_bpar,
				 @homologacion)
				 --}
				 --@cod_bpar, )	-- del -001- a.
	end

return(0)
go

grant execute on dbo.sp_UpdateGrupo to GesPetUsr
go

print 'Actualización realizada.'
go
