/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_AnexarHoras'
go
if exists (select * from sysobjects where name = 'sp_AnexarHoras' and sysstat & 7 = 4)
drop procedure dbo.sp_AnexarHoras
go
create procedure dbo.sp_AnexarHoras 
             @ori_nrointerno    int, 
             @des_nrointerno    int   
as 
 
declare @cod_recurso varchar(10) 
 
declare @pet_desde  smalldatetime 
declare @pet_hasta  smalldatetime 
declare @pet_horas  numeric(10,2) 
 
declare @aux_fecha  smalldatetime 
declare @aux_horas  numeric(10,2) 
 
declare @dias_totales int 
declare @dias_habiles int 
declare @diasferiados int 
declare @horasxdia int 
 
 
 
 
DECLARE CursHoras CURSOR FOR 
select  cod_recurso, 
    fe_desde, 
    fe_hasta, 
    horas 
from    GesPet..HorasTrabajadas 
where   pet_nrointerno = @ori_nrointerno 
for read only 
 
 
BEGIN TRAN 
 
OPEN CursHoras 
FETCH CursHoras INTO  
    @cod_recurso, 
    @pet_desde, 
    @pet_hasta, 
    @pet_horas 
WHILE (@@sqlstatus = 0) 
BEGIN 
    /* dias habiles del periodo para la peticion/recurso  */ 
    select @dias_totales = datediff(day, @pet_desde, @pet_hasta) + 1 
    select @dias_habiles = @dias_totales 
    select @diasferiados = count(1) from GesPet..Feriado 
       where  convert(char(8),fecha,112) >= convert(char(8),@pet_desde,112) and convert(char(8),fecha,112) <= convert(char(8),@pet_hasta,112) 
    if @diasferiados < @dias_totales 
       select @dias_habiles = @dias_habiles - @diasferiados 
 
    select @horasxdia = CONVERT(int, @pet_horas / @dias_habiles) 
 
    select @aux_fecha=@pet_desde 
     
    /* con cada dia del periodo */ 
    WHILE (convert(char(8),@aux_fecha,112) <= convert(char(8),@pet_hasta,112)) 
    BEGIN 
        /* si no es feriado */ 
        if not exists (select 1 from GesPet..Feriado  where  convert(char(8),fecha,112) = convert(char(8),@aux_fecha,112)) 
        BEGIN 
            if exists (select 1 from GesPet..HorasTrabajadas where  
                RTRIM(cod_recurso) = RTRIM(@cod_recurso) and  
                pet_nrointerno = @des_nrointerno and  
                convert(char(8),fe_desde,112) <= convert(char(8),@aux_fecha,112) and 
                convert(char(8),fe_hasta,112) >= convert(char(8),@aux_fecha,112)) 
            BEGIN 
                update GesPet..HorasTrabajadas 
                set horas = horas + @horasxdia 
                where   RTRIM(cod_recurso) = RTRIM(@cod_recurso) and  
                    pet_nrointerno = @des_nrointerno and  
                    convert(char(8),fe_desde,112) <= convert(char(8),@aux_fecha,112) and 
                    convert(char(8),fe_hasta,112) >= convert(char(8),@aux_fecha,112) 
            END 
            ELSE 
            BEGIN 
                insert into GesPet..HorasTrabajadas 
                    (cod_recurso, 
                    cod_tarea,   
                    pet_nrointerno, 
                    fe_desde, 
                    fe_hasta,    
                    horas,   
                    trabsinasignar, 
                    observaciones, 
                    audit_user, 
                    audit_date)  
                values 
                    (@cod_recurso, 
                    '',  
                    @des_nrointerno, 
                    @aux_fecha, 
                    @aux_fecha,  
                    @horasxdia,  
                    'S', 
                    'Anexada', 
                    SUSER_NAME(),   
                    getdate() )  
            END 
        END 
        select @aux_fecha = dateadd(day,1,@aux_fecha) 
    END 
    FETCH CursHoras INTO  
        @cod_recurso, 
        @pet_desde, 
        @pet_hasta, 
        @pet_horas 
END 
CLOSE CursHoras 
 
/* fnalmente elimina las horas originales */ 
 
delete  GesPet..HorasTrabajadas 
where   pet_nrointerno = @ori_nrointerno 
 
 
COMMIT TRAN 
 
DEALLOCATE CURSOR CursHoras 
 
return(0)   
go



grant execute on dbo.sp_AnexarHoras to GesPetUsr 
go


