/*
-000- a. FJS 23.08.2010 - Nuevo SP para mostrar en la grilla, todas las peticiones de un grupo.
-001- a. FJS 12.10.2010 - Corrección: se cambia el criterio de cálculo de la columna [alca] (se cambia TEST por ALCA).
-002- a. FJS 12.10.2010 - Corrección: una vez priorizada y/u ordenada la petición, no debe considerar el estado de la misma para mostrarse.
-003- a. FJS 13.10.2010 - Se agrega parámetro @cod_sector para utilizar esta vista para supervisores.
-004- a. FJS 15.11.2010 - Planificación DYD - 2da. etapa: agregado de peticiones de OPTimización.
-004- b. FJS 15.11.2010 - Planificación DYD - 2da. etapa: se identifica las peticiones agregadas por DYD.
-005- a. FJS 06.12.2010 - Mejora: se agrega la descripción del período de planificación.
-006- a. FJS 04.01.2011 - Arreglo: según el estado, se permite o no modificar el registro.

*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetPlanificarDYD'
go

if exists (select * from sysobjects where name = 'sp_GetPetPlanificarDYD' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetPlanificarDYD
go

create procedure dbo.sp_GetPetPlanificarDYD
	@per_nrointerno	int=null,
	@cod_grupo		char(8)=null,
	@cod_sector		char(8)=null,
	@prioridad		char(1)=null,
	@cod_bpar		char(10)=null
as 
	select 
		isnull(pd.per_nrointerno,0) as 'per_nrointerno',
		p.pet_nrointerno,
		p.pet_nroasignado,
		p.cod_tipo_peticion,
		p.cod_clase,
		p.pet_regulatorio,
		isnull(p.pet_projid,0) as pet_projid,
		isnull(p.pet_projsubid,0) as pet_projsubid,
		isnull(p.pet_projsubsid,0) as pet_projsubsid,
		p.titulo,
		pc.prioridad,
		pd.plan_ordenfin,
		nom_origen = (
			case
				when pd.plan_ori = 1 then 'Planificación inicial'
				when pd.plan_ori = 2 then 'Agregada'
				--{ add -004- b.
				when pd.plan_ori = 3 then 'Planificación inicial (DYD)'
				when pd.plan_ori = 4 then 'Agregada (DYD)'
				--}
				else '***'
			end),	-- Origen de planificación
		pg.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = pg.cod_estado),
		pg.fe_ini_plan,
		pg.fe_fin_plan,
		p.cod_sector,
		nom_sector    = (select RTRIM(x.nom_sector) from GesPet..Sector x where x.cod_sector = p.cod_sector),
		nom_gerencia  = (select RTRIM(x.nom_gerencia) from GesPet..Gerencia x where x.cod_gerencia = p.cod_gerencia),
		nom_direccion = (select RTRIM(x.nom_direccion) from GesPet..Direccion x where x.cod_direccion = p.cod_direccion),
		p.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.cod_bpar),
		'alca' = (
			case
				when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = pg.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
					 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = pg.pet_nrointerno and x.ok_tipo = 'ALCA') > 0 then 'U'
				when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = pg.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
					 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = pg.pet_nrointerno and x.ok_tipo = 'ALCA') = 0 then 'S'
				else 'N'
			end),
		pd.plan_estadoini,		-- Inicial: Estado de planificación
		nom_estadoini = (select x.nom_estado_plan from GesPet..EstadosPlanificacion x where x.cod_estado_plan = pd.plan_estadoini),
		pd.plan_motnoplan,		-- Inicial: Motivo de no planificación (no se puede planificar)
		nom_motnoplan = (select x.nom_motivo_noplan from GesPet..MotivosNoPlan x where x.cod_motivo_noplan = pd.plan_motnoplan),
		pd.plan_fealcadef,		-- Inicial: Fecha de definición de alcance
		pd.plan_fefunctst,		-- Inicial: Fecha de fin de pruebas funcionales
		pd.plan_horasper,		-- Inicial: Cantidad de horas planificadas para el presente período
		pd.plan_desaestado,		-- Seguimiento: Estado actual del desarrollo
		nom_desaestado = (select x.nom_estado_desa from GesPet..EstadosDesarrollo x where x.cod_estado_desa = pd.plan_desaestado),
		pd.plan_motivsusp,		-- Seguimiento: Motivo de Susp./Desplan./Canc./Rech.
		nom_motivsusp = (select x.nom_motivo_suspen from GesPet..MotivosSuspen x where x.cod_motivo_suspen = pd.plan_motivsusp),
		pd.plan_motivrepl,		-- Replanificar: Motivo de replanificación
		nom_motivrepl = (select x.nom_motivo_replan from GesPet..MotivosReplan x where x.cod_motivo_replan = pd.plan_motivrepl),
		pd.plan_feplanori,		-- Replanificar: Fecha original de planificación
		pd.plan_feplannue,		-- Replanificar: Nueva fecha de planificación
		pd.plan_cantreplan,		-- Replanificar: Cantidad de replanificaciones (contador)
		pd.plan_horasreplan,	-- Replanificar: Cantidad de horas totales replanificadas
		--nom_motivo_replan = (select x.nom_motivo_replan from GesPet..MotivosReplan x where x.cod_motivo_replan = pd.plan_motivrepl),
		pd.cod_grupo,
		nom_grupo = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = pd.cod_grupo)
		,per_abrev = isnull((select x.per_abrev from GesPet..Periodo x where x.per_nrointerno = pc.per_nrointerno),'-')	-- add -005- a.
		--{ add -006- a.
		,'plan_situacion_dsc' = case
			when (pd.plan_estado = 'PLANIF') then 'Pendiente'
			when (pd.plan_estado = 'PLANOK') then 'Ordenado'
			when (pd.plan_estado = 'APROBA') then 'Confirmado'
			else '-'
		end
		,hab = case
			when p.cod_clase <> 'OPTI' and charindex(pd.plan_estado,'APROBA|')>0 then 'S'
			when p.cod_clase = 'OPTI' then 'S'
			else 'N'
		end
		--}
	from
		GesPet..PeticionPlandet pd left join 
		GesPet..PeticionPlancab pc on (pc.per_nrointerno = pd.per_nrointerno and pc.pet_nrointerno = pd.pet_nrointerno) left join
		GesPet..PeticionGrupo pg on (pg.pet_nrointerno = pd.pet_nrointerno and pg.cod_grupo = pd.cod_grupo) left join
		GesPet..Peticion p on (pg.pet_nrointerno = p.pet_nrointerno)
	where
		(@cod_grupo is null or pg.cod_grupo = @cod_grupo) and
		(@cod_sector is null or pg.cod_sector = @cod_sector) and	-- add -003- a.
		--charindex(p.cod_clase, 'NUEV|EVOL|') > 0 and				-- del -004- a.
		--charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and		-- del -002- a. 
		--((pd.plan_estado = 'APROBA' and p.cod_clase in ('NUEV','EVOL')) or (pd.plan_estado = 'PLANIF' and p.cod_clase = 'OPTI')) and	-- del -004- a.
		(pd.per_nrointerno is null or @per_nrointerno is null or pd.per_nrointerno = @per_nrointerno) and
		(@prioridad is null or pc.prioridad = @prioridad) and
		(@cod_bpar is null or pc.cod_bpar = @cod_bpar)
	--{ add -004- a.
	union
	select 
		per_nrointerno = 0,
		p.pet_nrointerno,
		p.pet_nroasignado,
		p.cod_tipo_peticion,
		p.cod_clase,
		p.pet_regulatorio,
		isnull(p.pet_projid,0) as pet_projid,
		isnull(p.pet_projsubid,0) as pet_projsubid,
		isnull(p.pet_projsubsid,0) as pet_projsubsid,
		p.titulo,
		prioridad		= null,
		plan_ordenfin	= null,
		nom_origen		= '***',
		pg.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = pg.cod_estado),
		pg.fe_ini_plan,
		pg.fe_fin_plan,
		p.cod_sector,
		nom_sector    = (select RTRIM(x.nom_sector) from GesPet..Sector x where x.cod_sector = p.cod_sector),
		nom_gerencia  = (select RTRIM(x.nom_gerencia) from GesPet..Gerencia x where x.cod_gerencia = p.cod_gerencia),
		nom_direccion = (select RTRIM(x.nom_direccion) from GesPet..Direccion x where x.cod_direccion = p.cod_direccion),
		p.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.cod_bpar),
		'alca' = (
			case
				when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = pg.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
					 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = pg.pet_nrointerno and x.ok_tipo = 'ALCA') > 0 then 'U'
				when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = pg.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
					 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = pg.pet_nrointerno and x.ok_tipo = 'ALCA') = 0 then 'S'
				else 'N'
			end),
		plan_estadoini		= null,		-- Inicial: Estado de planificación
		nom_estadoini		= null,
		plan_motnoplan		= null,		-- Inicial: Motivo de no planificación (no se puede planificar)
		nom_motnoplan		= null,
		plan_fealcadef		= null,		-- Inicial: Fecha de definición de alcance
		plan_fefunctst		= null,		-- Inicial: Fecha de fin de pruebas funcionales
		plan_horasper		= null,		-- Inicial: Cantidad de horas planificadas para el presente período
		plan_desaestado		= null,		-- Seguimiento: Estado actual del desarrollo
		nom_desaestado		= null,
		plan_motivsusp		= null,		-- Seguimiento: Motivo de Susp./Desplan./Canc./Rech.
		nom_motivsusp		= null,
		plan_motivrepl		= null,		-- Replanificar: Motivo de replanificación
		nom_motivrepl		= null,
		plan_feplanori		= null,		-- Replanificar: Fecha original de planificación
		plan_feplannue		= null,		-- Replanificar: Nueva fecha de planificación
		plan_cantreplan		= null,		-- Replanificar: Cantidad de replanificaciones (contador)
		plan_horasreplan	= null,		-- Replanificar: Cantidad de horas totales replanificadas
		--nom_motivo_replan	= null,
		pg.cod_grupo,
		nom_grupo = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = pg.cod_grupo)
		,'-'		-- add -005- a.
		--{ add -006- a.
		,'plan_situacion_dsc'='-'
		,hab='S'
		--}
	from
		GesPet..PeticionGrupo pg left join
		GesPet..Peticion p on (pg.pet_nrointerno = p.pet_nrointerno) left join
		GesPet..Grupo g on (g.cod_grupo = pg.cod_grupo)
	where
		(@cod_grupo is null or pg.cod_grupo = @cod_grupo) and
		(@cod_sector is null or pg.cod_sector = @cod_sector) and
		charindex(p.cod_clase, 'OPTI|')>0 and
		(g.grupo_homologacion = 'S' and charindex(pg.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0) and
		(charindex(pg.cod_estado, 'ESTIOK|ESTIRK|ESTIMA|PLANOK|PLANIF|PLANRK|EJECRK|EJECUC|SUSPEN|') > 0) and
		(@cod_bpar is null or p.cod_bpar = @cod_bpar) and
		pg.pet_nrointerno not in (
			select p.pet_nrointerno
			from
				GesPet..PeticionPlandet pd left join 
				GesPet..PeticionPlancab pc on (pc.per_nrointerno = pd.per_nrointerno and pc.pet_nrointerno = pd.pet_nrointerno) left join
				GesPet..PeticionGrupo pg on (pg.pet_nrointerno = pd.pet_nrointerno and pg.cod_grupo = pd.cod_grupo) left join
				GesPet..Peticion p on (pg.pet_nrointerno = p.pet_nrointerno)
			where
				(@cod_grupo is null or pg.cod_grupo = @cod_grupo) and
				(@cod_sector is null or pg.cod_sector = @cod_sector) and
				(pd.per_nrointerno is null or @per_nrointerno is null or pd.per_nrointerno = @per_nrointerno) and
				(@prioridad is null or pc.prioridad = @prioridad) and
				(@cod_bpar is null or pc.cod_bpar = @cod_bpar))
	--}
	/*
	order by
		pd.per_nrointerno,
		pd.cod_grupo,
		pd.plan_ordenfin,
		p.pet_nroasignado
	*/
	order by
		1,42,12,3
	return (0)
go

grant execute on dbo.sp_GetPetPlanificarDYD to GesPetUsr
go

print 'Actualización realizada.'
go