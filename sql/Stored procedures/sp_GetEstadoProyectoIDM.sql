/* ESTE SP DEBE SER convertIDO PARA CONSOLIDAR */
use GesPet
go

print 'Creando/actualizando SP: sp_GetEstadoProyectoIDM'
go

if exists (select * from sysobjects where name = 'sp_GetEstadoProyectoIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEstadoProyectoIDM
go

create procedure dbo.sp_GetEstadoProyectoIDM
	@projid			int,
	@projsubid		int,
	@projsubsid		int,
	@new_estado		char(6) output
as
	declare @cod_hijo		int
	declare @flg_rnkup		int
	declare @nEstado		int
	declare @cod_estado		varchar(6)
	declare @xEstado		char(6)
	declare @flg1			char(1)
	declare @flg2			char(1)
	
	select @nEstado=0
	select @xEstado=''
	select @flg1='N'
	select @flg2='N'

	declare CursHijo cursor for
		select	
			p.pet_nrointerno,
			p.cod_estado,
			rnkup = convert(int,e.flg_rnkup)
		from	
			GesPet..Peticion p, 
			GesPet..Estados e
		where
			p.pet_projid = @projid and
			p.pet_projsubid = @projsubid and
			p.pet_projsubsid = @projsubsid and 
			(RTRIM(e.cod_estado) = RTRIM(p.cod_estado))
	for read only

	select @xEstado = 'PLANIF'	-- Por defecto "A planificar"

	open CursHijo
	fetch CursHijo into @cod_hijo, @cod_estado, @flg_rnkup
		while (@@sqlstatus = 0)
			begin
				if charindex(RTRIM(@cod_estado),RTRIM('TERMIN'))>0
					begin
						select @flg1 = 'S'
					end
				if charindex(RTRIM(@cod_estado),RTRIM('PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC|'))>0
					begin
						select @flg2 = 'S'
					end
				if @flg_rnkup > @nEstado
					begin
						select @nEstado = @flg_rnkup
						select @xEstado = @cod_estado
					end
				fetch CursHijo into @cod_hijo, @cod_estado, @flg_rnkup
			end
	close CursHijo
	deallocate cursor CursHijo

	if @flg1='S' and @flg2='S'
		begin
			select @xEstado = 'EJECUC'
		end
	
	select @new_estado = @xEstado
	return(0)
go

grant execute on dbo.sp_GetEstadoProyectoIDM to GesPetUsr
go

print 'Actualización realizada.'
go