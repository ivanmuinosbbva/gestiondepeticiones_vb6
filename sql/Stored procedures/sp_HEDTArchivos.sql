/*
-000- a. FJS 26.10.2009 - Nuevo SP para cargar el archivo de base para transmitir el catálogo de archivos enmascarables a HOST.
-001- a. FJS 07.07.2014 - Nuevo: se contemplan las solicitudes por EMErgencia.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_HEDTArchivos'
go

if exists (select * from sysobjects where name = 'sp_HEDTArchivos' and sysstat & 7 = 4)
	drop procedure dbo.sp_HEDTArchivos
go

create procedure dbo.sp_HEDTArchivos

as 

declare @tmp_dsn_sol	char(50)
declare @tmp_dsn_id		char(8)
declare	@tmp_dsn_nom	char(50)
declare @FixedString	char(50)
declare @usuario		char(10)

	begin
		-- Temporales
		create table #dsns1 ( 
			tmp_usuario		char(10)	null,
			tmp_dsn_sol		char(50)	null,
			tmp_dsn_nom		char(50)	null
		)

		create table #dsns2 ( 
			t_dsn_nom		char(50)	null
		)
		
		/*
			Guardo los datos del recurso y el archivo de catálogo usado 
			en las solicitudes lista para transmitir (en estado aprobadas).
		*/
		insert into #dsns1
			select
				sol_recurso,
				sol_file_desa,
				/*
				archivo = 
				case
					when sol_tipo = '1' then sol_file_desa
					else sol_file_out
				end,
				*/
				''
			from GesPet..Solicitudes
			where sol_estado = 'C' and sol_eme <> 'S'				-- upd -001- a.
		
		/*
		-- Guardo los numeros de solicitudes involucrados por cada archivo
		declare CursSOL cursor for
			select tmp_dsn_sol
			from #dsns1
			group by tmp_dsn_sol
			for read only 

			open CursSOL 
				fetch CursSOL into @tmp_dsn_sol
				while @@sqlstatus = 0
					begin
						
						fetch CursDSN into @tmp_dsn_sol
					end
			close CursSOL 
		deallocate cursor CursSOL
		*/

		-- Con un cursor recorro y transformo cada archivo
		declare CursDSN cursor for
			select tmp_dsn_sol, tmp_usuario
			from #dsns1
			group by tmp_dsn_sol, tmp_usuario
			for read only 

			open CursDSN 
				fetch CursDSN into @tmp_dsn_sol, @usuario
				while @@sqlstatus = 0
					begin
						select @FixedString = substring(@tmp_dsn_sol, 1, (charindex(RTRIM(@usuario), @tmp_dsn_sol) + char_length(RTRIM(@usuario))))
						select @tmp_dsn_nom = right(@tmp_dsn_sol, char_length(@tmp_dsn_sol) - char_length(rtrim(@FixedString)))

						update #dsns1
						set tmp_dsn_nom = rtrim(@tmp_dsn_nom)
						where RTRIM(tmp_dsn_sol) = RTRIM(@tmp_dsn_sol)

						fetch CursDSN into @tmp_dsn_sol, @usuario
					end
			close CursDSN 
		deallocate cursor CursDSN
		
		insert into #dsns2
			select tmp_dsn_nom from #dsns1 group by tmp_dsn_nom
		
		-- Muestro el resultado
		select *
		from #dsns2
	end
	return(0)
go

grant execute on dbo.sp_HEDTArchivos to GesPetUsr
go

print 'Actualización realizada.'
go
