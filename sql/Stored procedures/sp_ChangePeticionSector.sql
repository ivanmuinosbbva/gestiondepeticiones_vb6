/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_ChangePeticionSector'
go
if exists (select * from sysobjects where name = 'sp_ChangePeticionSector' and sysstat & 7 = 4)
drop procedure dbo.sp_ChangePeticionSector
go
create procedure dbo.sp_ChangePeticionSector 
       @pet_nrointerno   int=0,
       @old_sector  char(8),
       @new_sector  char(8)
as 
 
declare @new_direccion char(8) 
declare @new_gerencia  char(8) 
declare @cod_recurso char(10) 
declare @old_gerencia char(8) 
declare @old_direccion char(8) 
 

select  @old_direccion=Ge.cod_direccion,  
	@old_gerencia=Se.cod_gerencia
from  Sector Se, Gerencia Ge 
	where 	RTRIM(Se.cod_sector) = RTRIM(@old_sector) and 
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia) 


select  @new_direccion=Ge.cod_direccion,  
	@new_gerencia=Se.cod_gerencia 
from  Sector Se ,Gerencia Ge 
	where	RTRIM(Se.cod_sector) = RTRIM(@new_sector) and 
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia) 

BEGIN TRANSACTION 


/* elimina los viejos PeticionGrupo (y no crea ninguno)*/
delete PeticionGrupo 
	where   pet_nrointerno = @pet_nrointerno and
		RTRIM(cod_sector) = RTRIM(@old_sector)


/* crea el PeticionSector */
insert into GesPet..PeticionSector( 
	pet_nrointerno, 
	cod_sector, 
	cod_gerencia, 
	cod_direccion, 
	fe_ini_plan,     
	fe_fin_plan,     
	fe_ini_real,     
	fe_fin_real,     
	horaspresup,     
	cod_estado,  
	fe_estado,   
	cod_situacion, 
	ult_accion, 
	hst_nrointerno_sol, 
	hst_nrointerno_rsp, 
	audit_user,   
	audit_date ) 
select @pet_nrointerno, 
	@new_sector, 
	@new_gerencia, 
	@new_direccion, 
	Og.fe_ini_plan,    
	Og.fe_fin_plan,    
	Og.fe_ini_real,    
	Og.fe_fin_real,    
	Og.horaspresup,    
	Og.cod_estado,     
	Og.fe_estado,  
	Og.cod_situacion, 
	'', 
	Og.hst_nrointerno_sol, 
	Og.hst_nrointerno_rsp, 
	SUSER_NAME(),   
	getdate()
from PeticionSector Og
	where   Og.pet_nrointerno = @pet_nrointerno and
		RTRIM(Og.cod_sector) = RTRIM(@old_sector)

/* elimino el PeticionSector*/
delete PeticionSector
	where   pet_nrointerno = @pet_nrointerno and
		RTRIM(cod_sector) = RTRIM(@old_sector)

/* elimino el PeticionRecurso*/
delete PeticionRecurso 
	where   pet_nrointerno = @pet_nrointerno and
		RTRIM(cod_sector) = RTRIM(@old_sector)

COMMIT TRANSACTION 
 
return(0) 
go



grant execute on dbo.sp_ChangePeticionSector to GesPetUsr 
go


