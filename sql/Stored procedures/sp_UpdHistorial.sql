/*

-001- a. - FJS 28.05.2008 - Se cambia la manera de grabar el campo para incluir la hora adem�s.

*/

/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'Creando/actualizando SP: sp_UpdHistorial'
go

if exists (select * from sysobjects where name = 'sp_UpdHistorial' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdHistorial
go

create procedure dbo.sp_UpdHistorial 
    @hst_nrointerno		int, 
    @cod_evento			varchar(8)	= null, 
    @pet_estado			varchar(6)	= null, 
    @cod_sector			char(8)		= null, 
    @sec_estado			varchar(6)	= null, 
    @cod_grupo			char(8)		= null, 
    @gru_estado			varchar(6)  = null, 
    @replc_user			varchar(10) = null 
as
    update 
		GesPet..Historial 
	set 
        cod_evento = @cod_evento,
        pet_estado = @pet_estado, 
        cod_sector = @cod_sector, 
        sec_estado = @sec_estado, 
        cod_grupo  = @cod_grupo, 
        gru_estado = @gru_estado, 
        replc_user = @replc_user, 
        audit_user = SUSER_NAME(),
        --hst_fecha  = convert(smalldatetime,convert(varchar(12),getdate()))	-- del -001- 
		hst_fecha  = getdate()		-- add -001-
    where
		hst_nrointerno = @hst_nrointerno

return(0)   
go

grant execute on dbo.sp_UpdHistorial to GesPetUsr 
go

print 'Actualizaci�n realizada.'