/*
-000- a. FJS 26.09.2008 - Nuevo SP para devolver el nombre extendido de un Proyecto IDM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionProyectoNombre'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionProyectoNombre' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionProyectoNombre
go

create procedure dbo.sp_GetPeticionProyectoNombre
	@pet_nrointerno		int=null 
as 

	declare @pet_projid		int
	declare @pet_projsubid	int
	declare @pet_projsubsid	int
	declare @projnom		varchar(255)

	-- Me guardo los datos del proyecto IDM de la petición consultada
	select
		@pet_projid = PET.pet_projid,
		@pet_projsubid = PET.pet_projsubid,
		@pet_projsubsid = PET.pet_projsubsid
	from
		GesPet..Peticion PET
	where
		PET.pet_nrointerno = @pet_nrointerno
	order by
		PET.pet_nrointerno,
		PET.pet_projid,
		PET.pet_projsubid,
		PET.pet_projsubsid

	-- Armo el nombre extendido de un proyecto IDM
	if (@pet_projsubsid is null or @pet_projsubsid = 0)
		if (@pet_projsubid is null or @pet_projsubid = 0)
			-- Es un Master Project
			begin
				select @projnom = (select	max(PRJ.ProjNom)
								   from		GesPet..ProyectoIDM PRJ 
								   where	PRJ.ProjId = @pet_projid and PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0
								   group by PRJ.ProjNom)
			end
		else
			-- Es un proyecto
			begin
				-- Primero, el nombre del proyecto
				select @projnom = (select	max(PRJ.ProjNom)
								   from		GesPet..ProyectoIDM PRJ 
								   where	PRJ.ProjId = @pet_projid and PRJ.ProjSubId = @pet_projsubid and PRJ.ProjSubSId = 0
								   group by PRJ.ProjNom)
				-- Luego, concateno el Master Project
				select @projnom = RTRIM(@projnom) + ' - ' + 
								  (select	max(PRJ.ProjNom)
								   from		GesPet..ProyectoIDM PRJ 
								   where	PRJ.ProjId = @pet_projid and PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0
								   group by PRJ.ProjNom)
			end
	else
		-- Es un subproyecto
		begin
			-- Primero, el nombre del subproyecto
			select @projnom = (select	max(PRJ.ProjNom)
							   from		GesPet..ProyectoIDM PRJ 
							   where	PRJ.ProjId = @pet_projid and PRJ.ProjSubId = @pet_projsubid and PRJ.ProjSubSId = @pet_projsubsid
							   group by PRJ.ProjNom)
			-- Segundo, el nombre del proyecto
			select @projnom = RTRIM(@projnom) + ' - ' + 
							  (select	max(PRJ.ProjNom)
							   from		GesPet..ProyectoIDM PRJ 
							   where	PRJ.ProjId = @pet_projid and PRJ.ProjSubId = @pet_projsubid and PRJ.ProjSubSId = 0
							   group by PRJ.ProjNom)
			-- Luego, concateno el Master Project
			select @projnom = RTRIM(@projnom) + ' - ' + 
							  (select	max(PRJ.ProjNom)
							   from		GesPet..ProyectoIDM PRJ 
							   where	PRJ.ProjId = @pet_projid and PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0
							   group by PRJ.ProjNom)
		end

	select @projnom as ProjNom
go

grant execute on dbo.sp_GetPeticionProyectoNombre to GesPetUsr
go

print 'Actualización realizada.'
