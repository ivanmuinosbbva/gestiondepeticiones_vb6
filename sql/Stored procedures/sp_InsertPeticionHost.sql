/*
-001- a. FJS 21.09.2009 - Antes de agregar la petici�n a la bandeja diaria de las peticiones v�lidas para pasajes a Producci�n, la elimino directamente
						  de la tabla de peticiones "En ejecuci�n".
-002- a. FJS 24.02.2010 - Se modifica el modo de inserci�n: por ahora no tiene sentido seguir cargando el sector y grupo porque no aplica.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionHost'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionHost' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionHost
go

create procedure dbo.sp_InsertPeticionHost  
    (@pet_nrointerno    int,  
     @pet_sector        char(8),  
     @pet_grupo         char(8), 
     @pet_record        char(80)) 
as  
	--{ add -001- a.
	-- Se borra si existiera de la tabla de peticiones en ejecuci�n (PeticionesEnviadas2)
	delete
	from GesPet..PeticionEnviadas2
	where pet_nrointerno = @pet_nrointerno
	--}

	--{ add -002- a. Elimino, por las dudas, los registros preexistentes con detalle de sector y grupo
	delete from GesPet.dbo.PeticionEnviadas
	where pet_nrointerno = @pet_nrointerno

	select @pet_sector = ''
	select @pet_grupo  = ''
	--}

	if not exists (select pet_nrointerno  
				   from dbo.PeticionEnviadas  
				   where pet_nrointerno = @pet_nrointerno and pet_sector = @pet_sector and pet_grupo = @pet_grupo) 
		begin
			insert into GesPet.dbo.PeticionEnviadas (  
				pet_nrointerno,  
				pet_sector,  
				pet_grupo,  
				pet_record,
				pet_usrid,
				pet_date,
				pet_done)
			values (
				@pet_nrointerno,  
				@pet_sector, 
				@pet_grupo,  
				@pet_record,  
				SUSER_NAME(),  
				GETDATE(),  
				'N') 
		end 
	else 
		begin 
			update  
				GesPet.dbo.PeticionEnviadas  
			set  
				pet_record = @pet_record,  
				 pet_usrid = SUSER_NAME(),  
				pet_date   = GETDATE() 
			where 
				pet_nrointerno = @pet_nrointerno and  
				pet_sector = @pet_sector and  
				pet_grupo = @pet_grupo 
		end
		
	-- Actualizo la bandeja (PeticionChangeMan)
	exec sp_InsertPetChangeMan @pet_nrointerno, @pet_sector, @pet_grupo		-- add -002- a.

	return(0)  
go

grant Execute  on dbo.sp_InsertPeticionHost to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go