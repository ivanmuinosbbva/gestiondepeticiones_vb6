/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertIndicadorKPI'
go

if exists (select * from sysobjects where name = 'sp_InsertIndicadorKPI' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertIndicadorKPI
go

create procedure dbo.sp_InsertIndicadorKPI
	@kpiid	int,
	@kpinom	char(60),
	@kpireq	char(1),
	@kpihab	char(1),
	@unimedId	char(10),
	@kpiayuda	char(255)
as
	insert into GesPet.dbo.IndicadorKPI (
		kpiid,
		kpinom,
		kpireq,
		kpihab,
		unimedId,
		kpiayuda)
	values (
		@kpiid,
		@kpinom,
		@kpireq,
		@kpihab,
		@unimedId,
		@kpiayuda)
go

grant execute on dbo.sp_InsertIndicadorKPI to GesPetUsr
go

print 'Actualización realizada.'
go
