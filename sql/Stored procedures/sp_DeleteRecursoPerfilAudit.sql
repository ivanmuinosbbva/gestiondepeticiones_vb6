/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteRecursoPerfilAudit'
go

if exists (select * from sysobjects where name = 'sp_DeleteRecursoPerfilAudit' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteRecursoPerfilAudit
go

create procedure dbo.sp_DeleteRecursoPerfilAudit
	@cod_recurso	char(10),
	@cod_perfil		char(4),
	@cod_ope		int
as
	delete from
		GesPet.dbo.RecursoPerfilAudit
	where
		cod_recurso = @cod_recurso and
		cod_perfil = @cod_perfil and
		cod_ope = @cod_ope
go

grant execute on dbo.sp_DeleteRecursoPerfilAudit to GesPetUsr
go

print 'Actualización realizada.'
go
