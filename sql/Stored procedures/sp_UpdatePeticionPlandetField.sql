/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.09.2010 - 
-001- a. FJS 11.03.2011 - Modificación: se agrega un nuevo campo para reflejar la primera fecha de planificación cuando una petición nunca ha sido planificada.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlandetField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlandetField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlandetField
go

create procedure dbo.sp_UpdatePeticionPlandetField
	@per_nrointerno	int,
	@pet_nrointerno	int,
	@cod_grupo		char(8),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as
	if rtrim(@campo)='PER_NROINTERNO'
		update GesPet.dbo.PeticionPlandet set per_nrointerno = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PET_NROINTERNO'
		update GesPet.dbo.PeticionPlandet set pet_nrointerno = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='COD_GRUPO'
		update GesPet.dbo.PeticionPlandet set cod_grupo = @valortxt where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ORDEN'
		update GesPet.dbo.PeticionPlandet set plan_orden = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ORDENFIN'
		update GesPet.dbo.PeticionPlandet set plan_ordenfin = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ESTADO'
		update GesPet.dbo.PeticionPlandet set plan_estado = @valortxt where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ACTFCH'
		update GesPet.dbo.PeticionPlandet set plan_actfch = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ACTUSR'
		update GesPet.dbo.PeticionPlandet set plan_actusr = @valortxt where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ACT2FCH'
		update GesPet.dbo.PeticionPlandet set plan_act2fch = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ACT2USR'
		update GesPet.dbo.PeticionPlandet set plan_act2usr = @valortxt where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ORI'
		update GesPet.dbo.PeticionPlandet set plan_ori = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_ESTADOINI'
		update GesPet.dbo.PeticionPlandet set plan_estadoini = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_MOTNOPLAN'
		update GesPet.dbo.PeticionPlandet set plan_motnoplan = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_FEALCADEF'
		update GesPet.dbo.PeticionPlandet set plan_fealcadef = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_FEFUNCTST'
		update GesPet.dbo.PeticionPlandet set plan_fefunctst = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_HORASPER'
		update GesPet.dbo.PeticionPlandet set plan_horasper = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_DESAESTADO'
		update GesPet.dbo.PeticionPlandet set plan_desaestado = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_MOTIVSUSP'
		update GesPet.dbo.PeticionPlandet set plan_motivsusp = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_MOTIVREPL'
		update GesPet.dbo.PeticionPlandet set plan_motivrepl = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_FEPLANORI'
		update GesPet.dbo.PeticionPlandet set plan_feplanori = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_FEPLANNUE'
		update GesPet.dbo.PeticionPlandet set plan_feplannue = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_CANTREPLAN'
		update GesPet.dbo.PeticionPlandet set plan_cantreplan = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_HORASREPLAN'
		update GesPet.dbo.PeticionPlandet set plan_horasreplan = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)

	if rtrim(@campo)='PLAN_FCHREPLAN'
		update GesPet.dbo.PeticionPlandet set plan_fchreplan = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)
	--{ add -001- a.
	if rtrim(@campo)='PLAN_FCHESTADOINI'
		update GesPet.dbo.PeticionPlandet set plan_fchestadoini = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(cod_grupo = @cod_grupo or @cod_grupo is null)
	--}
	return(0)
go

grant execute on dbo.sp_UpdatePeticionPlandetField to GesPetUsr
go

print 'Actualización realizada.'
