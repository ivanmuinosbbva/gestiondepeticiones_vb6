/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 28.10.2010 - SPs para el mantenimiento de la tabla Estados
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertEstados'
go

if exists (select * from sysobjects where name = 'sp_InsertEstados' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertEstados
go

create procedure dbo.sp_InsertEstados
	@cod_estado		char(6),
	@nom_estado		char(30)='***Falta definir',
	@flg_rnkup		char(2),
	@flg_herdlt1	char(1),
	@flg_petici		char(1),
	@flg_secgru		char(1),
	@IGM_hab		char(1),
	@terminal		char(1)='N',
	@imagen			int=5,
	@default_est	char(6)=''
as
	insert into GesPet.dbo.Estados (
		cod_estado,
		nom_estado,
		flg_rnkup,
		flg_herdlt1,
		flg_petici,
		flg_secgru,
		IGM_hab,
		terminal,
		imagen,
		default_est)
	values (
		@cod_estado,
		@nom_estado,
		@flg_rnkup,
		@flg_herdlt1,
		@flg_petici,
		@flg_secgru,
		@IGM_hab,
		@terminal,
		@imagen,
		@default_est)
	return(0)
go

grant execute on dbo.sp_InsertEstados to GesPetUsr
go

print 'Actualización realizada.'
go