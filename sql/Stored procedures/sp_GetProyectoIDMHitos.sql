/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
-001- a. FJS 16.03.2015 - Se agregan nuevos datos: marca de visibilidad en hitos.
-002- a. FJS 21.10.2015 - Se agrega la opción de filtrar los hitos con el atributo exposición declarado.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMHitos'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMHitos' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMHitos
go

create procedure dbo.sp_GetProyectoIDMHitos
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null,
	@hito_nombre	char(100)=null,
	@hit_nrointerno	int=null,
	@exposicion		char(1)=null
as
	select 
		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		orden = case
			when a.hit_orden is null then 1000000
			when a.hit_orden = 0 then 0
			else a.hit_orden
		end,
		a.hit_nrointerno,
		a.hito_nombre,
		a.hito_descripcion,
		a.hito_fe_ini,
		a.hito_fe_fin,
		a.hito_estado,
		nom_estado = (select x.descripcion from GesPet..ProyectoIDMHitosEstados x where x.hito_estado = a.hito_estado),
		a.cod_nivel,
		a.cod_area,
		nom_area = (case 
			when a.cod_nivel = 'DIRE' then (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = a.cod_area)
			when a.cod_nivel = 'GERE' then (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = a.cod_area)
			when a.cod_nivel = 'SECT' then (select x.nom_sector from GesPet..Sector x where x.cod_sector = a.cod_area)
			when a.cod_nivel = 'GRUP' then (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = a.cod_area)
		end),
		dep_area = (case 
			when a.cod_nivel = 'GERE' then (
				select RTRIM(d.cod_direccion) + '/' + RTRIM(a.cod_area) 
				from GesPet..Gerencia d 
				where d.cod_gerencia = a.cod_area)
			when a.cod_nivel = 'SECT' then (
				select RTRIM(g.cod_direccion) + '/' + RTRIM(s.cod_gerencia) + '/' + RTRIM(a.cod_area)
				from GesPet..Sector s inner join
					 GesPet..Gerencia g on (s.cod_gerencia = g.cod_gerencia)
				where s.cod_sector = a.cod_area)
			when a.cod_nivel = 'GRUP' then (
				select RTRIM(ge.cod_direccion) + '/' + RTRIM(s.cod_gerencia) + '/' + RTRIM(g.cod_sector) + '/' + RTRIM(a.cod_area)
				from GesPet..Grupo g inner join
					 GesPet..Sector s on (g.cod_sector = s.cod_sector) inner join
					 GesPet..Gerencia ge on (s.cod_gerencia = ge.cod_gerencia)
				where g.cod_grupo = a.cod_area)
			else ''
		end),
		a.hito_fecha,
		a.hito_usuario,
		a.hst_nrointerno,
		a.hit_orden,
		--{ add -002- a.
		a.hito_expos,
		a.hito_id,
		hc.hitodesc,
		hc.hitoexpos	as expos_clase
		--}
	from
		--{ upd -002- a.
		GesPet..ProyectoIDMHitos a left join 
		GesPet..HitoClase hc on (hc.hitoid = a.hito_id)
		--}
	where
		(a.ProjId = @ProjId or @ProjId is null) and
		(a.ProjSubId = @ProjSubId or @ProjSubId is null) and
		(a.ProjSubSId = @ProjSubSId or @ProjSubSId is null) and
		(a.hit_nrointerno = @hit_nrointerno or @hit_nrointerno is null) and 
		(UPPER(LTRIM(RTRIM(a.hito_nombre))) = UPPER(LTRIM(RTRIM(@hito_nombre))) or @hito_nombre is null) and
		(@exposicion is null or a.hito_expos = 'S')
	order by 
		4
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMHitos to GesPetUsr
go

print 'Actualización realizada.'
go