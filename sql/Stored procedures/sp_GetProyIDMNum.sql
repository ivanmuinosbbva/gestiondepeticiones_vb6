/*
-000- a. FJS 17.06.2009 - Nuevo SP para devolver los �ltimos n�meros utilizados.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyIDMNum'
go

if exists (select * from sysobjects where name = 'sp_GetProyIDMNum' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyIDMNum
go

create procedure dbo.sp_GetProyIDMNum
	@ProjId			int=null, 
	@ProjSubId		int=null, 
	@ProjSubsId		int=null
as 
	select
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId
	from
		GesPet..ProyectoIDM PRJ
	where
		(PRJ.ProjId = @ProjId or @ProjId is null) and
		(PRJ.ProjSubId = @ProjSubId or @ProjSubId is null) and
		(PRJ.ProjSubSId = @ProjSubsId or @ProjSubsId is null)
	order by
		PRJ.ProjId desc, 
		PRJ.ProjSubId desc, 
		PRJ.ProjSubSId desc
return(0)
go

grant execute on dbo.sp_GetProyIDMNum to GesPetUsr
go

print 'Actualizaci�n realizada.'
