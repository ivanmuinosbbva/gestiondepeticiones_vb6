/*
-000- a. FJS 15.06.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_HistorialPeticion'
go

if exists (select * from sysobjects where name = 'sp_IGM_HistorialPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_HistorialPeticion
go

create procedure dbo.sp_IGM_HistorialPeticion
	@pet_nrointerno		int=null
as 
	select
		h.pet_nrointerno,
		p.pet_nroasignado,
		h.hst_fecha,
		h.cod_evento,
		nom_evento = isnull((select x.nom_accion from GesPet..Acciones x where x.cod_accion = h.cod_evento),''),
		h.pet_estado,
		pet_nom_estado = isnull((select x.nom_estado from GesPet..Estados x where x.cod_estado = p.cod_estado),''),
		h.cod_sector,
		nom_sector = (select x.nom_sector from GesPet..Sector x where x.cod_sector = h.cod_sector),
		h.sec_estado,
		sec_nom_estado = isnull((select x.nom_estado from GesPet..Estados x where x.cod_estado = h.sec_estado),''),
		h.cod_grupo,
		nom_grupo = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = h.cod_grupo),
		h.gru_estado,
		gru_nom_estado = isnull((select x.nom_estado from GesPet..Estados x where x.cod_estado = h.gru_estado),''),
		observaciones = isnull((select x.mem_texto from GesPet..HistorialMemo x where x.hst_nrointerno = h.hst_nrointerno and x.hst_secuencia = 0),'')
	from
		GesPet..Historial h left join
		GesPet..Peticion p on (h.pet_nrointerno = p.pet_nrointerno) left join
		GesPet..Acciones a on (h.cod_evento = a.cod_accion)
	where
		(@pet_nrointerno is null or h.pet_nrointerno = @pet_nrointerno) and
		(a.IGM_hab = 'S')
	order by
		h.pet_nrointerno,
		h.hst_fecha desc
	
	return(0)
go

grant execute on dbo.sp_IGM_HistorialPeticion to GesPetUsr
go

grant execute on dbo.sp_IGM_HistorialPeticion to GrpTrnIGM
go

print 'Actualización realizada.'
go
