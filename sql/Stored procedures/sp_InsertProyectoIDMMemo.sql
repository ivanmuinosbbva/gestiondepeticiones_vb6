/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 11.02.2011 - 

Referencias para mem_campo:

DESCRIPCIO:	descripciones
BENEFICIOS:	beneficios
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoIDMMemo'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoIDMMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoIDMMemo
go

create procedure dbo.sp_InsertProyectoIDMMemo
	@ProjId		int,
	@ProjSubId	int,
	@ProjSubSId	int,
	@mem_campo	char(10),
	@mem_texto	varchar(255)
as
	declare @secuencia	smallint

	select @secuencia = max(mem_secuencia)+1
	from GesPet.dbo.ProyectoIDMMemo
	where 
		ProjId = @ProjId and 
		ProjSubId = @ProjSubId and 
		ProjSubSId = @ProjSubSId and 
		mem_campo = @mem_campo
	group by mem_secuencia

	if @secuencia is null
		select @secuencia = 0
	
	insert into GesPet.dbo.ProyectoIDMMemo (
		ProjId,
		ProjSubId,
		ProjSubSId,
		mem_campo,
		mem_secuencia,
		mem_texto)
	values (
		@ProjId,
		@ProjSubId,
		@ProjSubSId,
		@mem_campo,
		@secuencia,
		@mem_texto)
	return(0)
go

grant execute on dbo.sp_InsertProyectoIDMMemo to GesPetUsr
go

print 'Actualización realizada.'
go
