/*
-000- a. FJS 27.02.2008 - Nuevo SP para obtener un grupo o grupos homologables.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetGrupoHomologable'
go

if exists (select * from sysobjects where name = 'sp_GetGrupoHomologable' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetGrupoHomologable
go

create procedure dbo.sp_GetGrupoHomologable
	@cod_grupo     char(8)
as 
    select
        G.cod_grupo,
        G.nom_grupo,
        G.cod_sector,
        G.flg_habil,
        G.es_ejecutor,
        G.cod_bpar,
        G.grupo_homologacion
    from
        GesPet.dbo.Grupo G
    where
        (G.cod_grupo = @cod_grupo or @cod_grupo is null) and 
        G.grupo_homologacion = 'S'

return(0) 
go

grant execute on dbo.sp_GetGrupoHomologable to GesPetUsr
go

print 'Actualización realizada.'
