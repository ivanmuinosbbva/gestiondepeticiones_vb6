/*
-000- a. FJS 24.11.2008 - Nuevo SP para actualizar datos de los TIPs.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateTips'
go

if exists (select * from sysobjects where name = 'sp_UpdateTips' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateTips
go

create procedure dbo.sp_UpdateTips
	@tips_version	char(10)=null,
	@tips_tipo		char(6)=null,
	@tips_nro		int=null,
	@tips_rng		int=null,
	@tips_txt		varchar(255)=null,
	@tips_date		smalldatetime=null,
	@tips_hab		char(1)=null
as
	update
		GesPet..Tips
	set
		tips_txt = @tips_txt,
		tips_date = @tips_date,
		tips_hab = @tips_hab
	where
		tips_version = @tips_version and 
		tips_tipo = @tips_tipo and 
		tips_nro = @tips_nro and 
		tips_rng = @tips_rng
go

grant execute on dbo.sp_UpdateTips to GesPetUsr
go

print 'Actualización realizada.'
