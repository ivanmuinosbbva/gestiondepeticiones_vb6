/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_UpdatePrjField'
go
if exists (select * from sysobjects where name = 'sp_UpdatePrjField' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePrjField
go
create procedure dbo.sp_UpdatePrjField   
    @prj_nrointerno     int,   
    @campo            char(15),   
    @valortxt         char(10)=null,   
    @valordate        smalldatetime=null,   
    @valornum         int=null   
as   
declare @cod_estado char(6) 
 
 
--if RTRIM(@campo)='PRIORI' 
--  update Proyecto  
--        set prioridad = @valortxt,   
--            audit_user = SUSER_NAME(),   
--            audit_date = getdate()   
--        where  prj_nrointerno = @prj_nrointerno   
if RTRIM(@campo)='FECHAIRL'   
  update Proyecto  
        set fe_ini_real = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where prj_nrointerno = @prj_nrointerno   
if RTRIM(@campo)='FECHAFRL'   
  update Proyecto  
        set fe_fin_real = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   
if RTRIM(@campo)='FECHAIPN'   
begin
  update Proyecto  
        set fe_ini_plan = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   
  if  @valordate is null
	update Proyecto
	set fe_fec_plan = null
	where  prj_nrointerno = @prj_nrointerno   
end
if RTRIM(@campo)='FECHAFPN'   
begin
  update Proyecto  
        set fe_fin_plan = @valordate,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   
  if  @valordate is null
	update Proyecto
	set fe_fec_plan = null
	where  prj_nrointerno = @prj_nrointerno   
end
if RTRIM(@campo)='ESTADO'   
begin 
		update Proyecto  
		set cod_estado = @valortxt,   
		    fe_estado = getdate(), 
		    audit_user = SUSER_NAME(),   
		    audit_date = getdate()   
		where  prj_nrointerno = @prj_nrointerno   
end 
if RTRIM(@campo)='ULTACC'   
        update Proyecto  
        set     ult_accion = @valortxt, 
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   
if RTRIM(@campo)='IMPOCOD'   
        update Proyecto  
        set importancia_cod = @valortxt,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   
if RTRIM(@campo)='IMPOUSR'   
        update Proyecto  
        set importancia_usr = @valortxt,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   
if RTRIM(@campo)='IMPONIV'   
        update Proyecto  
        set importancia_prf = @valortxt,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   
if RTRIM(@campo)='BPARTNE'   
        update Proyecto  
        set cod_bpar   = @valortxt,   
            audit_user = SUSER_NAME(),   
            audit_date = getdate()   
        where  prj_nrointerno = @prj_nrointerno   	

return(0)   

go

grant execute on dbo.sp_UpdatePrjField to GesPetUsr 
go
