/*
-001- a. FJS 22.02.2008 - Se agrega el campo al conjunto de resultado que indica el si es grupo homologable o el grupo homologador.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetGrupoXt'
go

if exists (select * from sysobjects where name = 'sp_GetGrupoXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetGrupoXt
go

create procedure dbo.sp_GetGrupoXt
	@cod_grupo	char(8) = null,
	@cod_sector	char(8) = null,
	@flg_habil	char(1) = null
as
	begin
		select
			Su.cod_grupo,
			Su.nom_grupo,
			Su.cod_sector,
			Gr.nom_sector,
			Gr.cod_gerencia,
			Ge.nom_gerencia,
			Ge.abrev_gerencia,
			Ge.cod_direccion,
			nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)),
			abrev_direccion = (select Di.abrev_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)), 
			Su.flg_habil,
			Su.es_ejecutor,
			Su.cod_bpar,
			nom_bpar = isnull((select R.nom_recurso from Recurso R where RTRIM(R.cod_recurso) = RTRIM(Su.cod_bpar)),'')
		   ,Su.grupo_homologacion,		-- add -001- a.
		   grupo_desc = case
				when Su.grupo_homologacion = 'H' then 'Homologador'
				when Su.grupo_homologacion = 'S' then 'T�cnico'
				when Su.grupo_homologacion = 'N' then 'Funcional'
				when Su.grupo_homologacion = '1' then 'Seguridad inform�tica'
				when Su.grupo_homologacion = '2' then 'Arquitectura t�cnica'
				--when Su.grupo_homologacion = '2' then 'Tecnolog�a'
			end
			,Su.grupo_tipopet
			,grupo_tipopetdesc = case
				when Su.grupo_tipopet is null or Su.grupo_tipopet = '0' then 'Por defecto'
				when Su.grupo_tipopet = '1' then 'Propias'
				when Su.grupo_tipopet = '2' then 'Propias y especiales'
				else 'Por defecto'
			end
			,Su.abrev_grupo
			,Su.grupo_inchoras
			,Su.grupo_incfechas
			,Su.grupo_incestado
		from
			GesPet..Grupo Su, 
			GesPet..Sector Gr, 
			GesPet..Gerencia Ge
		where
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(Su.cod_grupo) = RTRIM(@cod_grupo)) and
			(@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(Su.cod_sector) = RTRIM(@cod_sector)) and
			(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(Su.flg_habil) = RTRIM(@flg_habil)) and
			(RTRIM(Su.cod_sector) = RTRIM(Gr.cod_sector)) and
			(RTRIM(Gr.cod_gerencia) = RTRIM(Ge.cod_gerencia))
		order by 
			Ge.cod_direccion,
			Gr.cod_gerencia,
			Su.cod_sector,
			Su.cod_grupo ASC
	end
return(0)
go

grant execute on dbo.sp_GetGrupoXt to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
