use GesPet
go
print 'sp_GetTarea'
go
if exists (select * from sysobjects where name = 'sp_GetTarea' and sysstat & 7 = 4)
drop procedure dbo.sp_GetTarea
go
create procedure dbo.sp_GetTarea 
         @cod_tarea     char(8)=null 
as 
begin 
    select  
        cod_tarea, 
        nom_tarea, 
        flg_habil 
    from 
        GesPet..Tarea 
    where   
        (@cod_tarea is null or RTRIM(@cod_tarea) is null or RTRIM(@cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
    order by cod_tarea ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetTarea to GesPetUsr 
go


