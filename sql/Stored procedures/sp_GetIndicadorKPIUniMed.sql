/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.04.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetIndicadorKPIUniMed'
go

if exists (select * from sysobjects where name = 'sp_GetIndicadorKPIUniMed' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetIndicadorKPIUniMed
go

create procedure dbo.sp_GetIndicadorKPIUniMed
	@kpiid		int=0,
	@unimedId	char(10)=null
as
	select 
		ag.kpiid,
		ag.unimedId
	from 
		GesPet.dbo.IndicadorKPIUniMed ag
	where
		ag.kpiid = @kpiid and
		(@unimedId is null or LTRIM(RTRIM(ag.unimedId)) = LTRIM(RTRIM(@unimedId)))
go

grant execute on dbo.sp_GetIndicadorKPIUniMed to GesPetUsr
go

print 'Actualización realizada.'
go
