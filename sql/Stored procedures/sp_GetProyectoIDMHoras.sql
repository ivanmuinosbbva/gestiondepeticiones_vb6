/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.01.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMHoras'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMHoras' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMHoras
go

create procedure dbo.sp_GetProyectoIDMHoras
	@ProjId		int=null,
	@ProjSubId	int=null,
	@ProjSubSId	int=null
as
	select 
		p.ProjId,
		p.ProjSubId,
		p.ProjSubSId,
		p.cod_nivel,
		nom_nivel = (
			case 
				when p.cod_nivel = 'DIR' then 'Dirección'
				when p.cod_nivel = 'GER' then 'Gerencia'
				when p.cod_nivel = 'SEC' then 'Sector'
				when p.cod_nivel = 'GRU' then 'Grupo'
		end),
		p.cod_area,
		nom_area = (
			case
				when p.cod_nivel = 'DIR' then (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = p.cod_area)
				when p.cod_nivel = 'GER' then (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = p.cod_area)
				when p.cod_nivel = 'SEC' then (select x.nom_sector from GesPet..Sector x where x.cod_sector = p.cod_area)
				when p.cod_nivel = 'GRU' then (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = p.cod_area)
		end),
		p.cant_horas
	from 
		GesPet.dbo.ProyectoIDMHoras p
	where
		(@ProjId is null or p.ProjId = @ProjId) and
		(@ProjSubId is null or p.ProjSubId = @ProjSubId) and
		(@ProjSubSId is null or p.ProjSubSId = @ProjSubSId)
	order by p.cod_nivel
go

grant execute on dbo.sp_GetProyectoIDMHoras to GesPetUsr
go

print 'Actualización realizada.'
go
