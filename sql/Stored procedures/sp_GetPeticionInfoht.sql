/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionInfoht'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionInfoht' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionInfoht
go

create procedure dbo.sp_GetPeticionInfoht
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int
as
	select 
		pt.pet_nrointerno,
		pt.info_id,
		pt.info_idver,
		pt.info_renglon,
		pt.info_renglontexto
	from 
		GesPet..PeticionInfoht pt 
	where 
		pt.pet_nrointerno = @pet_nrointerno and
		pt.info_id = @info_id and
		(@info_idver is null or pt.info_idver = @info_idver)
	order by
		pt.pet_nrointerno,
		pt.info_id,
		pt.info_idver	
	return(0)
go

grant execute on dbo.sp_GetPeticionInfoht to GesPetUsr
go

print 'Actualización realizada.'
go
