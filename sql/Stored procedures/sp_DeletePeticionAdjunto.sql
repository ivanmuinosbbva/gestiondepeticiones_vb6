use GesPet
go
print 'sp_DeletePeticionAdjunto'
go
if exists (select * from sysobjects where name = 'sp_DeletePeticionAdjunto' and sysstat & 7 = 4)
drop procedure dbo.sp_DeletePeticionAdjunto
go
create procedure dbo.sp_DeletePeticionAdjunto
	@pet_nrointerno	int,
	@adj_tipo	char(8),
	@adj_file	char(100)
as

if not exists (select adj_file from PeticionAdjunto where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file) and RTRIM(adj_tipo)=RTRIM(@adj_tipo))
begin
	raiserror 30001 'Adjunto Inexistente'
	select 30001 as ErrCode , 'Adjunto Inexistente' as ErrDesc
	return (30001)
end
else
begin
	delete PeticionAdjunto
	where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file) and RTRIM(adj_tipo)=RTRIM(@adj_tipo)
end
return(0)
go


grant execute on dbo.sp_DeletePeticionAdjunto to GesPetUsr
go
