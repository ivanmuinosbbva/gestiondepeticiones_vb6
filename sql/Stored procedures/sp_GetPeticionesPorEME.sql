/*
-000- a. FJS 17.04.2018 - Nuevo SP para listar las peticiones entre fechas que fueron dadas de alta desde el SGI por emergencias.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesPorEME'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesPorEME' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesPorEME
go

create procedure dbo.sp_GetPeticionesPorEME
	@fe_desde		smalldatetime=null,
	@fe_hasta		smalldatetime=null
as 
	select 
		p.pet_nrointerno,
		p.pet_nroasignado,
		p.cod_tipo_peticion,
		p.cod_clase,
		p.titulo,
		p.cod_estado,
		nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
		p.fe_estado,
		p.fe_pedido,
		p.fe_fin_real,
		estado_homologacion = (
			select e.nom_estado
			from Estados e
			where e.cod_estado IN (
				select pgx.cod_estado
				from PeticionGrupo pgx 
				where pgx.pet_nrointerno = p.pet_nrointerno and pgx.cod_grupo = '24-03')),
		p.sgi_incidencia,
		pg.cod_sector	as sector_ejecutor,
		nom_sector_ejecutor = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
		pg.cod_grupo	as grupo_ejecutor,
		nom_grupo_ejecutor = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo)
	from
		Peticion p inner join 
		PeticionGrupo pg on (pg.pet_nrointerno = p.pet_nrointerno)
	where 
		pg.cod_grupo <> '24-03' and 	-- Descartamos siempre al grupo homologador
		(@fe_desde is null or p.fe_pedido >= @fe_desde) and 
		(@fe_hasta is null or p.fe_pedido <= @fe_hasta) and 
		LTRIM(RTRIM(p.sgi_incidencia)) <> ''

	return(0)
go

grant execute on dbo.sp_GetPeticionesPorEME to GesPetUsr
go

print 'Actualización realizada.'
go
