/*
-001- a. FJS 15.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetInfoGrupos'
go

if exists (select * from sysobjects where name = 'sp_GetInfoGrupos' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetInfoGrupos
go

create procedure dbo.sp_GetInfoGrupos 
	@modo				char(3)=null,
	@cod_direccion		char(8)=null, 
    @cod_gerencia		char(8)=null, 
	@cod_sector			char(8)=null, 
    @cod_grupo			char(8)=null
as 
	if @modo = 'GRP'
		begin
			-- Grupos en estado activo que tienen peticiones
			select
				a.cod_direccion as 'Direcci�n',
				a.cod_gerencia as 'Gerencia',
				a.cod_sector as 'Sector',
				a.cod_grupo as 'Grupo',
				b.nom_grupo as 'Nombre de grupo',
				count(*) as 'Activas',
				'Responsable' = isnull((select x.cod_recurso from GesPet..Recurso x where x.cod_direccion = a.cod_direccion and x.cod_gerencia = a.cod_gerencia and x.cod_sector = a.cod_sector and x.cod_grupo = a.cod_grupo and x.flg_cargoarea = 'S'),' - '),
				'Nombre y apellido del resp.' = isnull((select x.nom_recurso from GesPet..Recurso x where x.cod_direccion = a.cod_direccion and x.cod_gerencia = a.cod_gerencia and x.cod_sector = a.cod_sector and x.cod_grupo = a.cod_grupo and x.flg_cargoarea = 'S' ),' - '),
				'Perfil para el �rea' = isnull((select max(y.nom_perfil) from GesPet..Recurso w inner join GesPet..RecursoPerfil x on w.cod_recurso = x.cod_recurso inner join GesPet..Perfil y on x.cod_perfil = y.cod_perfil and x.cod_nivel = 'GRUP' and x.cod_area = a.cod_grupo where w.flg_cargoarea = 'S'), ' - ')
			from 
				GesPet..PeticionGrupo a inner join 
				GesPet..Grupo b on a.cod_grupo = b.cod_grupo
			where 
				a.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and
				(@cod_direccion is null or a.cod_direccion = @cod_direccion) and
				(@cod_gerencia is null or a.cod_gerencia = @cod_gerencia) and
				(@cod_sector is null or a.cod_sector = @cod_sector) and
				(@cod_grupo is null or a.cod_grupo = @cod_grupo)
			group by 
				a.cod_direccion, 
				a.cod_gerencia, 
				a.cod_sector,
				a.cod_grupo, 
				b.nom_grupo
			order by 
				a.cod_direccion,
				a.cod_gerencia,
				a.cod_sector,
				6 desc
		end
	if @modo = 'PET'
		begin
			select
				b.pet_nroasignado as 'Nro. asignado',
				b.titulo as 'T�tulo',
				b.cod_tipo_peticion as 'Tipo',
				b.cod_clase as 'Clase',
				--'Estado' = (select x.nom_estado from GesPet..Estados x where x.cod_estado = b.cod_estado),
				--b.fe_estado as 'F.Estado',
				a.pet_nrointerno as 'Interno',
				a.cod_sector as 'Sector',
				a.cod_grupo as 'Grupo',
				a.cod_estado as 'G.Estado'
				--'Nombre del grupo' = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = a.cod_grupo),
				--'G.Estado' = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado)--,
				--a.fe_estado as 'G.F.Estado'
			from
				GesPet..PeticionGrupo a inner join
				GesPet..Peticion b on a.pet_nrointerno = b.pet_nrointerno
			where
				a.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and
				(@cod_direccion is null or a.cod_direccion = @cod_direccion) and
				(@cod_gerencia is null or a.cod_gerencia = @cod_gerencia) and
				(@cod_sector is null or a.cod_sector = @cod_sector) and
				(@cod_grupo is null or a.cod_grupo = @cod_grupo)
		end

	return(0) 
go

grant execute on dbo.sp_GetInfoGrupos to GesPetUsr 
go

print 'Actualizaci�n realizada.'
