/*
-000- a. FJS 15.03.2010 - Nuevo SP para generar exportación parcial de peticiones por Agrupamiento para la gente de Organización (Walter Salvi).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetTest'
go

if exists (select * from sysobjects where name = 'sp_GetTest' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetTest
go

create procedure dbo.sp_GetTest
	@pet_nrointerno		int=null
as 
	declare @texto	char(255), @val varbinary(16)

	create table #Peticiones (
		pet_nrointerno		int				null,
		pet_caracteristicas	text			null)

	insert into #Peticiones
		select
			PET.pet_nrointerno,		-- N° petición
			null					-- Características
		from
			GesPet..Peticion PET
		where
			PET.pet_nrointerno = @pet_nrointerno

		-- *************************************************************************************************************************
		-- Aquí arma el campo "Características" por cada petición
		-- *************************************************************************************************************************

		-- Variables locales (para el cursor)
		declare @c_pet_nrointerno int, @c_mem_campo	char(10), @c_mem_secuencia smallint

		declare curTextos cursor for 
			select pet_nrointerno, mem_campo, mem_secuencia
			from PeticionMemo 
			where pet_nrointerno = @pet_nrointerno and mem_campo = 'CARACTERIS'
			group by pet_nrointerno, mem_campo, mem_secuencia
				for read only 

			open curTextos
			fetch curTextos into @c_pet_nrointerno, @c_mem_campo, @c_mem_secuencia 
			while @@sqlstatus = 0 
				begin
					select @texto = (select mem_texto from GesPet..PeticionMemo where pet_nrointerno = @c_pet_nrointerno and mem_campo = @c_mem_campo and mem_secuencia = @c_mem_secuencia)

					update #Peticiones
					set pet_caracteristicas = convert(text, @texto)
					fetch curTextos into @c_pet_nrointerno, @c_mem_campo, @c_mem_secuencia 
				end 
			close curTextos
		deallocate cursor curTextos

		select
			pet_nrointerno		as 'Petición',
			pet_caracteristicas	as 'Características'
		from
			#Peticiones

	return(0)
go

grant execute on dbo.sp_GetTest to GesPetUsr
go

print 'Actualización realizada.'

/*

*/