/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertMotivosSuspen'
go

if exists (select * from sysobjects where name = 'sp_InsertMotivosSuspen' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertMotivosSuspen
go

create procedure dbo.sp_InsertMotivosSuspen
	@cod_motivo_suspen	int,
	@nom_motivo_suspen	char(50),
	@estado_hab			char(1)='S'
as
	insert into GesPet.dbo.MotivosSuspen (
		cod_motivo_suspen,
		nom_motivo_suspen,
		estado_hab)
	values (
		@cod_motivo_suspen,
		@nom_motivo_suspen,
		@estado_hab)
	return(0)
go

grant execute on dbo.sp_InsertMotivosSuspen to GesPetUsr
go

print 'Actualización realizada.'
