/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionInfohcField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionInfohcField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionInfohcField
go

create procedure dbo.sp_UpdatePeticionInfohcField
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as
	if UPPER(RTRIM(@campo))='PET_NROINTERNO'
		update 
			GesPet.dbo.PeticionInfohc
		set
			pet_nrointerno = @valornum
		where 
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(info_id = @info_id or @info_id is null)
	
	if UPPER(RTRIM(@campo))='INFO_ID'
		update 
			GesPet.dbo.PeticionInfohc
		set
			info_id = @valornum
		where 
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(info_id = @info_id or @info_id is null)
	
	if UPPER(RTRIM(@campo))='INFO_TIPO'
		update 
			GesPet.dbo.PeticionInfohc
		set
			info_tipo = @valortxt
		where 
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(info_id = @info_id or @info_id is null)
	
	if UPPER(RTRIM(@campo))='INFO_FECHA'
		update 
			GesPet.dbo.PeticionInfohc
		set
			info_fecha = @valordate
		where 
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(info_id = @info_id or @info_id is null)
	
	if UPPER(RTRIM(@campo))='INFO_RECURSO'
		update 
			GesPet.dbo.PeticionInfohc
		set
			info_recurso = @valortxt
		where 
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(info_id = @info_id or @info_id is null)

	if UPPER(RTRIM(@campo))='INFO_ESTADO'
		update 
			GesPet.dbo.PeticionInfohc
		set
			info_estado = @valortxt
		where 
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(info_id = @info_id or @info_id is null)

	if UPPER(RTRIM(@campo))='INFOPROT_ID'
		update 
			GesPet.dbo.PeticionInfohc
		set
			infoprot_id = @valornum
		where 
			(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
			(info_id = @info_id or @info_id is null)
go

grant execute on dbo.sp_UpdatePeticionInfohcField to GesPetUsr
go

print 'Actualización realizada.'
go
