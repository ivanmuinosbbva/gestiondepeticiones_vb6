use GesPet
go

print 'Creando/actualizando SP: sp_GetUnaPeticionTitulo'
go

if exists (select * from sysobjects where name = 'sp_GetUnaPeticionTitulo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetUnaPeticionTitulo
go

create procedure dbo.sp_GetUnaPeticionTitulo
	@titulo		varchar(120)=null,
	@cod_estado	varchar(250)=null
as
	select PET.pet_nrointerno,
		PET.pet_nroasignado,
		PET.titulo,
		PET.cod_tipo_peticion,
		PET.cod_clase,
		PET.prioridad,
		PET.pet_nroanexada,
		PET.fe_pedido,
		PET.fe_requerida,
		PET.fe_comite,
		PET.fe_ini_plan,
		PET.fe_fin_plan,
		PET.fe_ini_real,
		PET.fe_fin_real,
		PET.horaspresup,
		PET.cod_direccion,
		PET.cod_gerencia,
		PET.cod_sector,
		PET.cod_usualta,
		PET.cod_solicitante,
		PET.cod_bpar,         
		PET.cod_referente,
		PET.cod_supervisor,
		PET.cod_director,
		PET.cod_estado,
		nom_estado = (select EST.nom_estado from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),
		PET.fe_estado,
		PET.cod_situacion,
		nom_situacion = (select SIT.nom_situacion from Situaciones SIT where RTRIM(SIT.cod_situacion) = RTRIM(PET.cod_situacion))
	from
		GesPet..Peticion PET
	where	
		(@titulo is null or RTRIM(@titulo) is null or RTRIM(@titulo)='' or UPPER(RTRIM(PET.titulo)) like '%' + UPPER(RTRIM(@titulo)) + '%') and 
		(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0)
	return(0)
go

grant execute on dbo.sp_GetUnaPeticionTitulo to GesPetUsr
go

print 'Actualización realizada.'
go
