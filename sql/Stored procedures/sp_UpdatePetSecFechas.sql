/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_UpdatePetSecFechas'
go
if exists (select * from sysobjects where name = 'sp_UpdatePetSecFechas' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePetSecFechas
go
create procedure dbo.sp_UpdatePetSecFechas 
    @pet_nrointerno     int, 
    @cod_sector         char(8), 
    @fe_ini_plan        smalldatetime=null, 
    @fe_fin_plan        smalldatetime=null, 
    @fe_ini_real        smalldatetime=null, 
    @fe_fin_real        smalldatetime=null, 
    @horaspresup        smallint=0
as 
declare @cod_estado char(6)
 
declare @x_fin_orig smalldatetime 
declare @x_ini_orig smalldatetime 
declare @x_fin_plan smalldatetime 
declare @x_ini_plan smalldatetime 
declare @cant_planif int
declare @replanif char(1)
select @replanif = 'N'

 
if exists (select cod_sector from PeticionSector where @pet_nrointerno=pet_nrointerno and RTRIM(cod_sector)=RTRIM(@cod_sector)) 
begin 
	select @cod_estado=cod_estado,@x_ini_orig=fe_ini_orig,@x_fin_orig=fe_fin_orig,  @x_ini_plan=fe_ini_plan, @x_fin_plan=fe_fin_plan,@cant_planif=cant_planif
	from   PeticionSector  
	where @pet_nrointerno=pet_nrointerno and 
		RTRIM(cod_sector)=RTRIM(@cod_sector)

	if  (@fe_ini_plan is not null or @fe_fin_plan is not null) and charindex(@cod_estado,'PLANOK|EJECUC|TERMIN')>0
	/* solo los estados que me interesan */
	/* y seguro que estoy mandando fechas */
	begin 
		if ((@x_ini_plan is null) or ((@x_ini_plan is not null) and ('X'+convert(char(8),@x_ini_plan,112)<>'X'+convert(char(8),@fe_ini_plan,112)))) or 
		   ((@x_fin_plan is null) or ((@x_fin_plan is not null) and ('X'+convert(char(8),@x_fin_plan,112)<>'X'+convert(char(8),@fe_fin_plan,112)))) 
		/* es una fecha nueva o distinta a la que existia */
		/* entonces puedo decir que estoy planificando una vez m�s */
		begin 
			if @cant_planif is null
				select @cant_planif = 0
			select @cant_planif = @cant_planif + 1
			if @cant_planif = 1
				select @replanif = 'P'
			else
				select @replanif = 'R'
		end
	end
	
	update PeticionSector 
	set 	fe_ini_plan =   @fe_ini_plan    , 
		fe_fin_plan =   @fe_fin_plan    , 
		fe_ini_real =   @fe_ini_real    , 
		fe_fin_real =   @fe_fin_real    , 
		horaspresup =   @horaspresup
	where @pet_nrointerno=pet_nrointerno and 
		RTRIM(cod_sector)=RTRIM(@cod_sector) 


	if @replanif = 'P'
	update PeticionSector  
	set fe_ini_orig = @fe_ini_plan, 
	    fe_fin_orig = @fe_fin_plan,         
	    cant_planif	= @cant_planif,
	    fe_fec_orig = getdate(),
	    fe_fec_plan = getdate()
	where @pet_nrointerno=pet_nrointerno and 
		RTRIM(cod_sector)=RTRIM(@cod_sector)

	if @replanif = 'R'
	update PeticionSector  
	set cant_planif	= @cant_planif,
	    fe_fec_plan = getdate()
	where @pet_nrointerno=pet_nrointerno and 
		RTRIM(cod_sector)=RTRIM(@cod_sector)

	if  (@fe_ini_plan is null or @fe_fin_plan is null)
	update PeticionSector  
	set fe_fec_plan = null
	where @pet_nrointerno=pet_nrointerno and 
		RTRIM(cod_sector)=RTRIM(@cod_sector)

end 
else 
begin 
    raiserror 30001 'Modificacion de Sector inexistente'   
    select 30001 as ErrCode , 'Modificacion de Sector inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_UpdatePetSecFechas to GesPetUsr 
go


