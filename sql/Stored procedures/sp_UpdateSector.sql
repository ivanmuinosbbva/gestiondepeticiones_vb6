/*
-001- a. FJS 20.04.2015 - Nuevo: se agrega un nuevo atributo para sectores, que indica si el mismo es v�lido para usar como sector solicitante en peticiones especiales.
-002- a. FJS 28.01.2016 - Nuevo: se agrega nuevo atributo para indicar quien maneja la demanda de peticiones, qu� perfil (referente de sistema o el nuevo ref. de RGP).

*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSector'
go

if exists (select * from sysobjects where name = 'sp_UpdateSector' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSector
go

create procedure dbo.sp_UpdateSector 
	@modo			char(1)='A',
	@cod_sector		char(8),
	@nom_sector		char(80),
	@cod_gerencia	char(8),
	@cod_abrev		char(3)=null,
	@flg_habil		char(1)=null,
	@es_ejecutor	char(1)='S',
	@cod_bpar		char(10)=null,
	--@abrev_sector	char(30)=null,
	@soli_hab		char(1)='S',
	@cod_perfil		char(4)='BPAR'		-- add -002- a.
as
	if @modo='M'
		begin
			if not exists (select cod_sector from GesPet..Sector where RTRIM(cod_sector) = RTRIM(@cod_sector))
				begin
					raiserror 30011 'Sector Inexistente'
					select 30011 as ErrCode , 'Sector Inexistente' as ErrDesc
					return (30011)
				end
		end
	else
		begin
			if exists (select cod_sector from GesPet..Sector where RTRIM(cod_sector) = RTRIM(@cod_sector))
				begin
					raiserror 30011 'Codigo de Sector Existente'
					select 30011 as ErrCode , 'Codigo de Sector Existente' as ErrDesc
					return (30011)
				end
		end
	
	/*
	if exists (select cod_sector from Sector where RTRIM(cod_abrev)=RTRIM(@cod_abrev) and RTRIM(cod_sector)<>RTRIM(@cod_sector))
		begin
			select 30010 as ErrCode , 'Codigo abreviado existente' as ErrDesc
			raiserror  30010 'Codigo abreviado existente'
			return (30010)
		end
	*/

	if @modo='M'
		begin
			BEGIN TRANSACTION
			if RTRIM(@flg_habil)='N'
				begin
					update GesPet..Grupo
					set flg_habil = 'N'
					from GesPet..Grupo Su
					where (RTRIM(Su.cod_sector) = RTRIM(@cod_sector))
				end
			
			update GesPet..Sector
			set 
				nom_sector		= @nom_sector,
				cod_gerencia	= @cod_gerencia,
				cod_abrev		= @cod_abrev,
				flg_habil		= @flg_habil,
				es_ejecutor		= @es_ejecutor,
				cod_bpar		= @cod_bpar,
				cod_perfil		= @cod_perfil,		-- add -002- a. 
				soli_hab		= @soli_hab			-- add -001- a.
			where RTRIM(cod_sector) = RTRIM(@cod_sector)
			COMMIT
		end
		else
			begin
				insert into GesPet..Sector (cod_sector, nom_sector, cod_gerencia, cod_abrev, flg_habil, es_ejecutor, cod_bpar, cod_perfil, soli_hab)
				values (@cod_sector, @nom_sector,@cod_gerencia, @cod_abrev, @flg_habil, @es_ejecutor, @cod_bpar, @cod_perfil, @soli_hab)
			end
	return(0)
go

grant execute on dbo.sp_UpdateSector to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
