/*
-001- a. FJS 04.07.2007 - Se agregan al conjunto de resultados las columnas cod_class y pet_imptech de la tabla Peticion
-002- a. FJS 05.07.2007 - Se agregan los par�metros nuevos para realizar la consulta.
-003- a. FJS 18.09.2007 - Se agrega una columna al conjunto de resultados (pet_sox001).
-004- a. FJS 21.07.2008 - Se agrega la capacidad de devolver discriminado peticiones con o sin documentos adjuntos
-005- a. FJS 10.09.2008 - Se agrega el campo 'pet_regulatorio' al conjunto de resultado y las opciones de filtro por este campo.
-006- a. FJS 31.10.2008 - Se agrega la opci�n de poder listar peticiones con el indicador de impacto tecnol�gico en Indeterminado.
-007- a. FJS 17.12.2008 - Se agrega una columna que identifica si la petici�n contiene al grupo Homologador.
-008- a. FJS 17.03.2009 - Se agrega un nuevo par�metro para filtrar por tipo de documento adjunto.
-009- a. FJS 30.03.2009 - Se agregan nuevos parametros para filtrar por las descripciones de la petici�n.
-009- b. FJS 20.11.2009 - Se cambia para que no sea Case-Sensitive.
-009- c. FJS 20.11.2009 - Se invierten los par�metros (estaban mal declarados en orden).
-010- a. FJS 03.06.2009 - Se agrega filtro por proyecto IDM.
-011- a. FJS 03.02.2011 - Mejora: se quita la parte de proyectos que no tiene utilidad.
-012- a. FJS 12.03.2012 - Se agrega el atributo empresa (pet_emp) por fusi�n Banco/Consolidar.
-012- b. FJS 24.04.2012 - Mejora: para filtrar por empresa, se envia una cadena con las empresas a filtrar.
-013- a. FJS 11.05.2012 - Mejora: se quita este dato que no se usa m�s.
-014- a. FJS 12.07.2012 - Nuevo: se agrega el campo para indentificar peticiones vinculadas a la mitigaci�n de factores de Riesgo Operacional.
-015- a. FJS 16.02.2016 - Nuevo: se agrega funcionalidad para la nueva gerencia de BPE.
-016- a. FJS 30.06.2016 - Nuevo: nuevos campos calculados para determinar la categor�a de una petici�n y para determinar la puntuaci�n de una 
						  petici�n (basado en f�rmula de valorizaci�n).
-017- a. FJS 20.07.2016 - Nuevo: se agregan los datos nombre del ref. de sistema + c�digo y nombre de ref. de BPE.
-018- a. FJS 25.08.2016 - Nuevo: optimizaci�n de la consulta (problemas de performance sobre consulta general de peticiones).
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_rptPeticion00'
go

if exists (select * from sysobjects where name = 'sp_rptPeticion00' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptPeticion00
go

create procedure dbo.sp_rptPeticion00
	@pet_nrodesde			int=0,
	@pet_nrohasta			int=0,
	@pet_tipo				varchar(4)='NULL',
	@pet_prioridad			varchar(4)='NULL',
	@pet_desde_bpar			varchar(8)='NULL',
	@pet_hasta_bpar			varchar(8)='NULL',
	--@pet_desde_alta			varchar(8)='NULL',
	--@pet_hasta_alta			varchar(8)='NULL',
	@pet_nivel				varchar(4)='NULL',
	@pet_area				varchar(8)='NULL',
	@pet_estado				varchar(250)='NULL',
	@pet_desde_estado		varchar(8)='NULL',
	@pet_hasta_estado		varchar(8)='NULL',
	@pet_situacion			varchar(8)='NULL',
	@pet_titulo				varchar(120)='NULL',
	@sec_nivel				varchar(4)='NULL',
	@sec_dire				varchar(8)='NULL',
	@sec_gere				varchar(8)='NULL',
	@sec_sect				varchar(8)='NULL',
	@sec_grup				varchar(8)='NULL',
	@sec_estado				varchar(250)='NULL',
	@sec_situacion			varchar(8)='NULL',
	@sec_desde_iplan		varchar(8)='NULL',
	@sec_hasta_iplan		varchar(8)='NULL',
	@sec_desde_fplan		varchar(8)='NULL',
	@sec_hasta_fplan		varchar(8)='NULL',
	@sec_desde_ireal		varchar(8)='NULL',
	@sec_hasta_ireal		varchar(8)='NULL',
	@sec_desde_freal		varchar(8)='NULL',
	@sec_hasta_freal		varchar(8)='NULL',
	@agr_nrointerno			int=0,
	@cod_importancia		varchar(4)='NULL',
	@cod_bpar				varchar(10)='NULL',
	@cod_clase				char(4)=null,
	@pet_imptech			char(1)=null,
	@pet_regulatorio		char(1)=null,
/*
	,@tipo_texto			char(10)=null
	,@criterio				char(5)=null
	,@filtro_texto			varchar(255)=null
	,@cod_usualta			char(10)=null
	,@cod_solicitante		char(10)=null
	,@cod_referente			char(10)=null
	,@cod_supervisor		char(10)=null
	,@cod_director			char(10)=null
	,@fecha_alta_desde		varchar(8)='NULL'
	,@fecha_alta_hasta		varchar(8)='NULL'
*/
	@projid					int=null,
	@projsubid				int=null,
	@projsubsid				int=null,
	@pet_emp				int=null,
	@pet_ro					char(1)=null,
	@cod_GBPE				char(10)=null,
	@fechaDesdeBPE			varchar(8)='NULL',
	@fechaHastaBPE			varchar(8)='NULL',
	@categoria				char(3)=null,
	@pet_desde_alta			varchar(8)='NULL',
	@pet_hasta_alta			varchar(8)='NULL'
as
	declare @secuencia		int
	declare @ret_status		int
	declare @categoria_loc	char(6)				-- add -018- a.

	--{ add -018- a.
	if @categoria is not null
		select @categoria_loc = case
			when @categoria = 'REG' then '1. REG'
			when @categoria = 'SDA' then '2. SDA'
			when @categoria = 'PES' then '3. PES'
			when @categoria = 'ENG' then '4. ENG'
			when @categoria = 'PRI' then '5. PRI'
			when @categoria = 'CSI' then '6. CSI'
			when @categoria = '---' then '---'
			when @categoria = 'SPR' then '7. SPR'
		end
	--}

	create table #PeticYHijo(
		pet_nrointerno 	int,
		pet_nroasignado	int,
		hij_cod_direccion varchar(8),
		hij_cod_gerencia varchar(8),
		hij_cod_sector varchar(8),
		hij_cod_grupo varchar(8))

	create table #AgrupXPetic(
		pet_nrointerno 	int)

	/*    Tabla Temporaria  */
	create table #AgrupArbol (
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)

	--{ add -016- a.
	create table #tablaCriterio (
		pet_nrointerno		int,
		categoria			char(6))

	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	
	--}
	
	/*
	declare @sum_impacto			real
	declare @sum_facilidad			real
	declare @sum_impacto_facilidad	real
	
	select @sum_impacto = SUM(d.driverValor)
	from Drivers d
	where d.indicadorId = 1		-- Impacto

	select @sum_facilidad = SUM(d.driverValor)
	from Drivers d
	where d.indicadorId = 2		-- Impacto

	select @sum_impacto_facilidad = @sum_impacto + @sum_facilidad
	*/

	--{ add -016- a. Esto es para obtener los agrupamientos de SDA
	declare	@agr_SDA	int
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
			
			INSERT INTO #tablaCriterio
			select 
				p.pet_nrointerno,
				categoria = case
					when p.cod_tipo_peticion in ('AUI','AUX') OR p.pet_regulatorio = 'S' then '1. REG'
					when exists (select x.pet_nrointerno from #AgrupXPeticSDA x where x.pet_nrointerno = p.pet_nrointerno) then '2. SDA'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then '3. PES'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then '4. ENG'
					when p.cod_clase in ('OPTI','CORR','SPUF','ATEN') then '6. CSI'
					when exists (
						 select count(1) 
						 from ProyectoIDM px 
						 where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
						 having count(1) > 0) OR (p.valor_impacto <> 0 OR p.valor_facilidad <> 0 ) then '5. PRI'
					when p.cod_clase = 'SINC' then '---'
					else '7. SPR'
				end
			from Peticion p 
		end
	--}

	/* esto es si se pidio agrupamiento */
	if @agr_nrointerno<>0
		begin
			truncate table #AgrupArbol

			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT
			if (@ret_status <> 0)
				begin
				if (@ret_status = 30003)
				begin
					select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
					return (@ret_status)
				end
				end
			insert into #AgrupXPetic (pet_nrointerno)
				select	AP.pet_nrointerno
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where AA.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_vigente='S'
		end

	/* si estoy seguro de que no me interesan los sector/grupo */
	if @sec_nivel='NULL' and @sec_dire='NULL' and @sec_gere='NULL' and @sec_sect='NULL' and @sec_grup='NULL' and @sec_estado='NULL' and @sec_situacion='NULL' and
		@sec_desde_iplan='NULL' and @sec_hasta_iplan='NULL' and @sec_desde_fplan='NULL' and @sec_hasta_fplan='NULL'  and
		@sec_desde_ireal='NULL' and @sec_hasta_ireal='NULL' and @sec_desde_freal='NULL' and @sec_hasta_freal='NULL'
		begin
			select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.titulo,
				PET.cod_tipo_peticion,
				PET.prioridad,
				PET.corp_local,
				PET.cod_orientacion,
				nom_orientacion = ISNULL((select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),'-'),
				PET.importancia_cod,
				importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod),
				PET.pet_nroanexada,
				PET.fe_pedido,
				PET.fe_requerida,
				PET.fe_comite,
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.fe_ini_real,
				PET.fe_fin_real,
				PET.horaspresup,
				PET.cod_direccion,
				PET.cod_gerencia,
				PET.cod_sector,
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where ESTPET.cod_estado=PET.cod_estado),
				PET.fe_estado,
				PET.cod_situacion,
				nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion),
				sec_cod_direccion=null,
				sec_nom_direccion=null,
				sec_cod_gerencia=null,
				sec_nom_gerencia=null,
				sec_cod_sector=null,
				sec_nom_sector=null,
				sec_cod_grupo=null,
				sec_nom_grupo=null,
				sec_cod_estado=null,
				sec_nom_estado=null,
				sec_cod_situacion=null,
				sec_nom_situacion=null,
				sec_horaspresup=0,
				sec_fe_ini_plan=null,
				sec_fe_fin_plan=null,
				sec_fe_ini_real=null,
				sec_fe_fin_real=null,
				sol_direccion = (select nom_direccion from Direccion where cod_direccion=PET.cod_direccion),
				sol_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=PET.cod_gerencia),
				sol_sector = (select nom_sector from Sector where cod_sector=PET.cod_sector),
				prio_ejecuc=0,
				info_adicio=null,
				PET.prj_nrointerno,
				prj_titulo = isnull((select PRJ.titulo from Proyecto PRJ where PET.prj_nrointerno=PRJ.prj_nrointerno),''),
				PET.pet_sox001,
				PET.cod_clase,
				PET.pet_imptech,
				PET.pet_regulatorio,
				PET.pet_emp,
				pet_ro = isnull(PET.pet_ro,'-'),
				pet_riesgo = (case
					when PET.pet_ro = 'A' then 'Alta'
					when PET.pet_ro = 'M' then 'Media'
					when PET.pet_ro = 'B' then 'Baja'
					else 'No'
				end),
				PET.cod_bpar,
				nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
				PET.cod_BPE,
				nom_BPE = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
				ISNULL(PET.puntuacion,0),
				/*
				puntuacion = 
					ISNULL(((PET.valor_facilidad * (@sum_facilidad / @sum_impacto_facilidad)) +
					 (PET.valor_impacto * (@sum_impacto / @sum_impacto_facilidad))),0),
				*/
				c.categoria
			from  
				GesPet..Peticion PET INNER JOIN 
				#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where   
				(@pet_emp is null or PET.pet_emp = @pet_emp) and 
				(@pet_ro is null or @pet_ro = PET.pet_ro) and 
				(@pet_nrodesde = 0 or PET.pet_nroasignado >= @pet_nrodesde) and
				(@pet_nrohasta = 0 or PET.pet_nroasignado <= @pet_nrohasta) and
				(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
				(@pet_estado is null or RTRIM(@pet_estado)='NULL' or charindex(RTRIM(PET.cod_estado),RTRIM(@pet_estado))>0) and
				(@pet_tipo is null or RTRIM(@pet_tipo)='NULL' or RTRIM(PET.cod_tipo_peticion) = RTRIM(@pet_tipo)) and
				(@cod_clase is null or @cod_clase='NULL' or PET.cod_clase = @cod_clase) and
				(@pet_imptech is null or PET.pet_imptech = @pet_imptech) and 
				(@pet_titulo is null or RTRIM(@pet_titulo)='NULL' or charindex(UPPER(RTRIM(@pet_titulo)),UPPER(RTRIM(PET.titulo)))>0) and
				(@pet_prioridad is null or RTRIM(@pet_prioridad)='NULL'or RTRIM(PET.prioridad) = RTRIM(@pet_prioridad)) and
				(@cod_importancia is null or RTRIM(@cod_importancia)='NULL' or RTRIM(PET.importancia_cod) = RTRIM(@cod_importancia)) and
				(RTRIM(@pet_desde_estado)='NULL' or convert(char(8),PET.fe_estado,112) >= @pet_desde_estado) and
				(RTRIM(@pet_hasta_estado)='NULL' or convert(char(8),PET.fe_estado,112) <= @pet_hasta_estado) and
				(RTRIM(@pet_desde_bpar)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_bpar) and
				(RTRIM(@pet_hasta_bpar)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_bpar) and
				/*
				(RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_alta) and
				(RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_alta) and
				*/
				(@pet_situacion is null or RTRIM(@pet_situacion)='NULL' or RTRIM(PET.cod_situacion) = RTRIM(@pet_situacion)) and
				(@cod_bpar is null or RTRIM(@cod_bpar)='NULL' or RTRIM(PET.cod_bpar) = RTRIM(@cod_bpar)) and
				((@pet_nivel is null or RTRIM(@pet_nivel)='NULL') or
				(@pet_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@pet_area)) or
				(@pet_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@pet_area)) or
				(@pet_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@pet_area)))
				and (PET.pet_regulatorio = @pet_regulatorio or @pet_regulatorio is null)
				and (
						(PET.pet_projid = @projid or @projid is null) and
						(PET.pet_projsubid = @projsubid or @projsubid is null) and
						(PET.pet_projsubsid = @projsubsid or @projsubsid is null)
					)
				and (@cod_GBPE is null or @cod_GBPE = 'NULL' or LTRIM(RTRIM(PET.cod_BPE)) = LTRIM(RTRIM(@cod_GBPE)))
				and (RTRIM(@fechaDesdeBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) >= @fechaDesdeBPE) 
				and (RTRIM(@fechaHastaBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) <= @fechaHastaBPE)
				and (@categoria IS NULL OR c.categoria = @categoria_loc) 
				and (RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_pedido,112) >= @pet_desde_alta)
				and (RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_pedido,112) <= @pet_hasta_alta)
			order by 
				PET.pet_nroasignado, 
				PET.pet_nrointerno
			return(0)
		end

	/* para grupo busca en grupos, para el resto lo hace en sectores */
	if @sec_nivel='GRUP'
	begin
		select 
			PET.pet_nrointerno,
			PET.pet_nroasignado,
			PET.titulo,
			PET.cod_tipo_peticion,
			PET.prioridad,
			PET.corp_local,
			PET.cod_orientacion,
			--nom_orientacion = (select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),
			nom_orientacion = ISNULL((select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),'-'),
			PET.importancia_cod,
			importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod),
			PET.pet_nroanexada,
			PET.fe_pedido,
			PET.fe_requerida,
			PET.fe_comite,
			PET.fe_ini_plan,
			PET.fe_fin_plan,
			PET.fe_ini_real,
			PET.fe_fin_real,
			PET.horaspresup,
			PET.cod_direccion,
			PET.cod_gerencia,
			PET.cod_sector,
			PET.cod_estado,
			nom_estado = (select ESTPET.nom_estado from Estados ESTPET where ESTPET.cod_estado=PET.cod_estado),
			PET.fe_estado,
			PET.cod_situacion,
			nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion),
			sec_cod_direccion=HIJ.cod_direccion,
			sec_nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=HIJ.cod_direccion),
			sec_cod_gerencia=HIJ.cod_gerencia,
			sec_nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where Ge.cod_gerencia=HIJ.cod_gerencia),
			sec_cod_sector=HIJ.cod_sector,
			sec_nom_sector = (select Gr.nom_sector from Sector Gr where Gr.cod_sector=HIJ.cod_sector),
			sec_cod_grupo=HIJ.cod_grupo,
			sec_nom_grupo = (select Sb.nom_grupo from Grupo Sb where Sb.cod_grupo=HIJ.cod_grupo),
			sec_cod_estado=HIJ.cod_estado,
			sec_nom_estado = (select ESTHIJ.nom_estado from Estados ESTHIJ where ESTHIJ.cod_estado=HIJ.cod_estado),
			sec_cod_situacion=HIJ.cod_situacion,
			sec_nom_situacion = (select SITHIJ.nom_situacion from Situaciones SITHIJ where SITHIJ.cod_situacion=HIJ.cod_situacion),
			sec_horaspresup=HIJ.horaspresup,
			sec_fe_ini_plan=HIJ.fe_ini_plan,
			sec_fe_fin_plan=HIJ.fe_fin_plan,
			sec_fe_ini_real=HIJ.fe_ini_real,
			sec_fe_fin_real=HIJ.fe_fin_real,
			sol_direccion = (select nom_direccion from Direccion where cod_direccion=PET.cod_direccion),
			sol_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=PET.cod_gerencia),
			sol_sector = (select nom_sector from Sector where cod_sector=PET.cod_sector),
			HIJ.prio_ejecuc,
			HIJ.info_adicio,
			PET.prj_nrointerno,
			prj_titulo = isnull((select PRJ.titulo from Proyecto PRJ where PET.prj_nrointerno=PRJ.prj_nrointerno),''),
			PET.pet_sox001,
			PET.cod_clase,
			PET.pet_imptech,
			PET.pet_regulatorio,
			PET.pet_emp,
			PET.cod_bpar,
			nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
			PET.cod_BPE,
			nom_BPE = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
			pet_ro = isnull(PET.pet_ro,'-'),
			pet_riesgo = (case
				when PET.pet_ro = 'A' then 'Alta'
				when PET.pet_ro = 'M' then 'Media'
				when PET.pet_ro = 'B' then 'Baja'
				else 'No'
			end),
			ISNULL(PET.puntuacion,0),
			/*
			puntuacion = 
				ISNULL(((PET.valor_facilidad * (@sum_facilidad / @sum_impacto_facilidad)) +
				 (PET.valor_impacto * (@sum_impacto / @sum_impacto_facilidad))),0),
			*/
			c.categoria
		from    
			GesPet..Peticion PET INNER JOIN 
			#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno),
			GesPet..PeticionGrupo HIJ 
		where 
			(@pet_emp is null or PET.pet_emp = @pet_emp) and 
			(@pet_ro is null or @pet_ro = PET.pet_ro) and 
			(@pet_nrodesde = 0 or PET.pet_nroasignado >= @pet_nrodesde) and
			(@pet_nrohasta = 0 or PET.pet_nroasignado <= @pet_nrohasta) and
			(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
			(@pet_estado is null or RTRIM(@pet_estado)='NULL' or charindex(RTRIM(PET.cod_estado),RTRIM(@pet_estado))>0) and
			(@pet_tipo is null or RTRIM(@pet_tipo)='NULL' or RTRIM(PET.cod_tipo_peticion) = RTRIM(@pet_tipo)) and
			(@cod_clase is null or @cod_clase='NULL' or PET.cod_clase = @cod_clase) and
			(@pet_imptech is null or PET.pet_imptech = @pet_imptech) and 
			(@pet_titulo is null or RTRIM(@pet_titulo)='NULL' or charindex(UPPER(RTRIM(@pet_titulo)),UPPER(RTRIM(PET.titulo)))>0) and
			(@pet_prioridad is null or RTRIM(@pet_prioridad)='NULL'or RTRIM(PET.prioridad) = RTRIM(@pet_prioridad)) and
			(@cod_importancia is null or RTRIM(@cod_importancia)='NULL' or RTRIM(PET.importancia_cod) = RTRIM(@cod_importancia)) and
			(RTRIM(@pet_desde_estado)='NULL' or convert(char(8),PET.fe_estado,112) >= @pet_desde_estado) and
			(RTRIM(@pet_hasta_estado)='NULL' or convert(char(8),PET.fe_estado,112) <= @pet_hasta_estado) and
			(RTRIM(@pet_desde_bpar)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_bpar) and
			(RTRIM(@pet_hasta_bpar)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_bpar) and
			/*
			(RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_alta) and
			(RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_alta) and
			*/
			(@pet_situacion is null or RTRIM(@pet_situacion)='NULL' or RTRIM(PET.cod_situacion) = RTRIM(@pet_situacion)) and
			(@cod_bpar is null or RTRIM(@cod_bpar)='NULL' or RTRIM(PET.cod_bpar) = RTRIM(@cod_bpar)) and
			((@pet_nivel is null or RTRIM(@pet_nivel)='NULL') or
			(@pet_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@pet_area)) or
			(@pet_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@pet_area)) or
			(@pet_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@pet_area))) and
			(RTRIM(@sec_desde_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) >= @sec_desde_iplan) and
			(RTRIM(@sec_hasta_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) <= @sec_hasta_iplan) and
			(RTRIM(@sec_desde_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) >= @sec_desde_fplan) and
			(RTRIM(@sec_hasta_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) <= @sec_hasta_fplan) and
			(RTRIM(@sec_desde_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) >= @sec_desde_ireal) and
			(RTRIM(@sec_hasta_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) <= @sec_hasta_ireal) and
			(RTRIM(@sec_desde_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) >= @sec_desde_freal) and
			(RTRIM(@sec_hasta_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) <= @sec_hasta_freal) and
			PET.pet_nrointerno = HIJ.pet_nrointerno and
			(RTRIM(@sec_dire)='NULL' or RTRIM(HIJ.cod_direccion)=RTRIM(@sec_dire)) and
			(RTRIM(@sec_gere)='NULL' or RTRIM(HIJ.cod_gerencia)=RTRIM(@sec_gere)) and
			(RTRIM(@sec_sect)='NULL' or RTRIM(HIJ.cod_sector)=RTRIM(@sec_sect)) and
			(RTRIM(@sec_grup)='NULL' or RTRIM(HIJ.cod_grupo)=RTRIM(@sec_grup)) and
			(@sec_estado is null or RTRIM(@sec_estado)='NULL' or charindex(RTRIM(HIJ.cod_estado),RTRIM(@sec_estado))>0) and
			(@sec_situacion is null or RTRIM(@sec_situacion)='NULL' or RTRIM(HIJ.cod_situacion) = RTRIM(@sec_situacion))
			and (PET.pet_regulatorio = @pet_regulatorio or @pet_regulatorio is null)
			and (
				(PET.pet_projid = @projid or @projid is null) and
				(PET.pet_projsubid = @projsubid or @projsubid is null) and
				(PET.pet_projsubsid = @projsubsid or @projsubsid is null))
			and (@cod_GBPE is null or LTRIM(RTRIM(PET.cod_BPE)) = LTRIM(RTRIM(@cod_GBPE)))
			and (RTRIM(@fechaDesdeBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) >= @fechaDesdeBPE) 
			and (RTRIM(@fechaHastaBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) <= @fechaHastaBPE)
			and (@categoria IS NULL OR c.categoria = @categoria_loc)
			and (RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_pedido,112) >= @pet_desde_alta)
			and (RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_pedido,112) <= @pet_hasta_alta)
		order by 
			PET.pet_nroasignado,
			PET.pet_nrointerno
	return(0)
	end

	/* resto lo hace en sectores */
	select 
		PET.pet_nrointerno,
		PET.pet_nroasignado,
		PET.titulo,
		PET.cod_tipo_peticion,
		PET.prioridad,
		PET.corp_local,
		PET.cod_orientacion,
		--nom_orientacion = (select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),
		nom_orientacion = ISNULL((select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),'-'),
		PET.importancia_cod,
		importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod),
		PET.pet_nroanexada,
		PET.fe_pedido,
		PET.fe_requerida,
		PET.fe_comite,
		PET.fe_ini_plan,
		PET.fe_fin_plan,
		PET.fe_ini_real,
		PET.fe_fin_real,
		PET.horaspresup,
		PET.cod_direccion,
		PET.cod_gerencia,
		PET.cod_sector,
		PET.cod_estado,
		nom_estado = (select ESTPET.nom_estado from Estados ESTPET where ESTPET.cod_estado=PET.cod_estado),
		PET.fe_estado,
		PET.cod_situacion,
		nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion),
		sec_cod_direccion=HIJ.cod_direccion,
		sec_nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=HIJ.cod_direccion),
		sec_cod_gerencia=HIJ.cod_gerencia,
		sec_nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where Ge.cod_gerencia=HIJ.cod_gerencia),
		sec_cod_sector=HIJ.cod_sector,
		sec_nom_sector = (select Gr.nom_sector from Sector Gr where Gr.cod_sector=HIJ.cod_sector),
		sec_cod_grupo='',
		sec_nom_grupo='',
		sec_cod_estado=HIJ.cod_estado,
		sec_nom_estado = (select ESTHIJ.nom_estado from Estados ESTHIJ where ESTHIJ.cod_estado=HIJ.cod_estado),
		sec_cod_situacion=HIJ.cod_situacion,
		sec_nom_situacion = (select SITHIJ.nom_situacion from Situaciones SITHIJ where SITHIJ.cod_situacion=HIJ.cod_situacion),
		sec_horaspresup=HIJ.horaspresup,
		sec_fe_ini_plan=HIJ.fe_ini_plan,
		sec_fe_fin_plan=HIJ.fe_fin_plan,
		sec_fe_ini_real=HIJ.fe_ini_real,
		sec_fe_fin_real=HIJ.fe_fin_real,
		sol_direccion = (select nom_direccion from Direccion where cod_direccion=PET.cod_direccion),
		sol_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=PET.cod_gerencia),
		sol_sector = (select nom_sector from Sector where cod_sector=PET.cod_sector),
		prio_ejecuc=0,
		info_adicio='',
		PET.prj_nrointerno,
		prj_titulo = isnull((select PRJ.titulo from Proyecto PRJ where PET.prj_nrointerno=PRJ.prj_nrointerno),''),
		PET.pet_sox001,
		PET.cod_clase,
		PET.pet_imptech,
		PET.pet_regulatorio,
		PET.pet_emp,
		PET.cod_bpar,
		nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
		PET.cod_BPE,
		nom_BPE = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
		pet_ro = isnull(PET.pet_ro,'-'),
		pet_riesgo = (case
			when PET.pet_ro = 'A' then 'Alta'
			when PET.pet_ro = 'M' then 'Media'
			when PET.pet_ro = 'B' then 'Baja'
			else 'No'
		end),
		ISNULL(PET.puntuacion,0),
		/*
		puntuacion = 
			ISNULL(((PET.valor_facilidad * (@sum_facilidad / @sum_impacto_facilidad)) +
			 (PET.valor_impacto * (@sum_impacto / @sum_impacto_facilidad))),0),
		*/
		c.categoria
	from
		GesPet..Peticion PET INNER JOIN 
		#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno),
		GesPet..PeticionSector HIJ 
	where
		(@pet_emp is null or PET.pet_emp = @pet_emp) and 
		(@pet_ro is null or @pet_ro = PET.pet_ro) and 
		(@pet_nrodesde = 0 or PET.pet_nroasignado >= @pet_nrodesde) and
		(@pet_nrohasta = 0 or PET.pet_nroasignado <= @pet_nrohasta) and
		(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
		(@pet_estado is null or RTRIM(@pet_estado)='NULL' or charindex(RTRIM(PET.cod_estado),RTRIM(@pet_estado))>0) and
		(@pet_tipo is null or RTRIM(@pet_tipo)='NULL' or RTRIM(PET.cod_tipo_peticion) = RTRIM(@pet_tipo)) and
		(@cod_clase is null or @cod_clase='NULL' or PET.cod_clase = @cod_clase) and
		(@pet_imptech is null or PET.pet_imptech = @pet_imptech) and 
		(@pet_titulo is null or RTRIM(@pet_titulo)='NULL' or charindex(UPPER(RTRIM(@pet_titulo)),UPPER(RTRIM(PET.titulo)))>0) and
		(@pet_prioridad is null or RTRIM(@pet_prioridad)='NULL'or RTRIM(PET.prioridad) = RTRIM(@pet_prioridad)) and
		(@cod_importancia is null or RTRIM(@cod_importancia)='NULL' or RTRIM(PET.importancia_cod) = RTRIM(@cod_importancia)) and
		(RTRIM(@pet_desde_estado)='NULL' or convert(char(8),PET.fe_estado,112) >= @pet_desde_estado) and
		(RTRIM(@pet_hasta_estado)='NULL' or convert(char(8),PET.fe_estado,112) <= @pet_hasta_estado) and
		(RTRIM(@pet_desde_bpar)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_bpar) and
		(RTRIM(@pet_hasta_bpar)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_bpar) and
		/*
		(RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_alta) and
		(RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_alta) and
		*/
		(@pet_situacion is null or RTRIM(@pet_situacion)='NULL' or RTRIM(PET.cod_situacion) = RTRIM(@pet_situacion)) and
		(@cod_bpar is null or RTRIM(@cod_bpar)='NULL' or RTRIM(PET.cod_bpar) = RTRIM(@cod_bpar)) and
		((@pet_nivel is null or RTRIM(@pet_nivel)='NULL') or
		(@pet_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@pet_area)) or
		(@pet_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@pet_area)) or
		(@pet_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@pet_area))) and
		(RTRIM(@sec_desde_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) >= @sec_desde_iplan) and
		(RTRIM(@sec_hasta_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) <= @sec_hasta_iplan) and
		(RTRIM(@sec_desde_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) >= @sec_desde_fplan) and
		(RTRIM(@sec_hasta_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) <= @sec_hasta_fplan) and
		(RTRIM(@sec_desde_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) >= @sec_desde_ireal) and
		(RTRIM(@sec_hasta_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) <= @sec_hasta_ireal) and
		(RTRIM(@sec_desde_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) >= @sec_desde_freal) and
		(RTRIM(@sec_hasta_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) <= @sec_hasta_freal) and
		PET.pet_nrointerno = HIJ.pet_nrointerno and
		(RTRIM(@sec_dire)='NULL' or RTRIM(HIJ.cod_direccion)=RTRIM(@sec_dire)) and
		(RTRIM(@sec_gere)='NULL' or RTRIM(HIJ.cod_gerencia)=RTRIM(@sec_gere)) and
		(RTRIM(@sec_sect)='NULL' or RTRIM(HIJ.cod_sector)=RTRIM(@sec_sect)) and
		(@sec_estado is null or RTRIM(@sec_estado)='NULL' or charindex(RTRIM(HIJ.cod_estado),RTRIM(@sec_estado))>0) and
		(@sec_situacion is null or RTRIM(@sec_situacion)='NULL' or RTRIM(HIJ.cod_situacion) = RTRIM(@sec_situacion))
		and (PET.pet_regulatorio = @pet_regulatorio or @pet_regulatorio is null)
		and (
			(PET.pet_projid = @projid or @projid is null) and
			(PET.pet_projsubid = @projsubid or @projsubid is null) and
			(PET.pet_projsubsid = @projsubsid or @projsubsid is null))
		and (@cod_GBPE is null or LTRIM(RTRIM(PET.cod_BPE)) = LTRIM(RTRIM(@cod_GBPE)))
		and (RTRIM(@fechaDesdeBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) >= @fechaDesdeBPE) 
		and (RTRIM(@fechaHastaBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) <= @fechaHastaBPE)
		and (@categoria IS NULL OR c.categoria = @categoria_loc)
		and (RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_pedido,112) >= @pet_desde_alta)
		and (RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_pedido,112) <= @pet_hasta_alta)
	order by 
		PET.pet_nroasignado,
		PET.pet_nrointerno
	return(0)
go

grant execute on dbo.sp_rptPeticion00 to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go
