/*
-000- a. FJS 17.01.2011 - Nuevo SP que detalla las solicitudes realizadas para un DSN.
-001- a. FJS 13.09.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDTSolInt'
go

if exists (select * from sysobjects where name = 'sp_GetHEDTSolInt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDTSolInt
go

create procedure dbo.sp_GetHEDTSolInt
	@dsn_nom		char(50)=null
as
	select 
		s.sol_nroasignado,
		s.sol_eme,
		sol_tipo = (select x.nom_tipo from GesPet..TipoSolicitud x where x.cod_tipo = s.sol_tipo),
		/*
		sol_tipo = 
			case
				when s.sol_tipo = '1' then 'XCOM'
				when s.sol_tipo = '2' then 'UNLO'
				when s.sol_tipo = '3' then 'TCOR'
				when s.sol_tipo = '4' then 'SORT'
				when s.sol_tipo = '5' then 'XCOM*'
				when s.sol_tipo = '6' then 'SORT*'
			end,
		*/
		s.sol_mask,
		nom_archivo_prod = (case
			when s.sol_tipo in ('2','7') then RTRIM(s.sol_lib_Sysin) + ' (' + RTRIM(s.sol_mem_Sysin) + ')'
			when s.sol_tipo = '3' then RTRIM(s.sol_nrotabla)
			else s.sol_file_prod
		end),
		s.sol_file_desa as nom_archivo_desa,
		/*
		nom_archivo_desa = 
		case 
			when s.sol_tipo in ('1','5') then LTRIM(RTRIM(s.sol_file_desa))
			else LTRIM(RTRIM(s.sol_file_out))
		end,
		*/
		s.sol_fecha,
		pet_nroasignado = (select x.pet_nroasignado from Peticion x where x.pet_nrointerno = s.pet_nrointerno),
		s.sol_recurso,
		nom_recurso = (select x.nom_recurso from Recurso x where x.cod_recurso = s.sol_recurso),
		s.sol_estado,
		nom_estado = (select x.nom_estado from GesPet..EstadoSolicitud x where x.cod_estado = s.sol_estado)
		/*
		nom_estado = 
		case
			when s.sol_estado = 'A' then 'Pendiente de aprobación'
			when s.sol_estado = 'B' then 'Pendiente de aprobación SI'
			when s.sol_estado = 'C' then 'Aprobado'
			when s.sol_estado = 'D' then 'Aprobado y en proceso de envio'
			when s.sol_estado = 'E' then 'Enviado'
			when s.sol_estado = 'F' then 'En espera de Restore'
			when s.sol_estado = 'G' then 'En espera (ejecución diferida)'
		end
		*/
	from
		GesPet..Solicitudes s
		/*
		GesPet..Solicitudes s inner join 
		GesPet..Recurso r on (s.sol_recurso = r.cod_recurso) inner join
		GesPet..Peticion p on (s.pet_nrointerno = p.pet_nrointerno)
		*/
	where
		@dsn_nom is null or (
		(s.sol_tipo in ('1','5') and s.sol_file_desa like '%'+ LTRIM(RTRIM(@dsn_nom)) +'%') or
		(s.sol_tipo not in ('1','5') and s.sol_file_out like '%'+ LTRIM(RTRIM(@dsn_nom)) +'%'))
	return(0)
go

grant execute on dbo.sp_GetHEDTSolInt to GesPetUsr
go

print 'Actualización realizada.'
go 

