/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdMotivosNoPlanField'
go

if exists (select * from sysobjects where name = 'sp_UpdMotivosNoPlanField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdMotivosNoPlanField
go

create procedure dbo.sp_UpdMotivosNoPlanField
	@cod_motivo_noplan	int,
	@campo				char(80),
	@valortxt			char(255),
	@valordate			smalldatetime,
	@valornum			int
as
	if rtrim(@campo)='COD_MOTIVO_NOPLAN'
		update 
			GesPet.dbo.MotivosNoPlan
		set
			cod_motivo_noplan = @valornum
		where 
			(cod_motivo_noplan = @cod_motivo_noplan or @cod_motivo_noplan is null)

	if rtrim(@campo)='NOM_MOTIVO_NOPLAN'
		update 
			GesPet.dbo.MotivosNoPlan
		set
			nom_motivo_noplan = @valortxt
		where 
			(cod_motivo_noplan = @cod_motivo_noplan or @cod_motivo_noplan is null)

	if rtrim(@campo)='ESTADO_HAB'
		update 
			GesPet.dbo.MotivosNoPlan
		set
			estado_hab = @valortxt
		where 
			(cod_motivo_noplan = @cod_motivo_noplan or @cod_motivo_noplan is null)
	return(0)
go

grant execute on dbo.sp_UpdMotivosNoPlanField to GesPetUsr
go

print 'Actualización realizada.'
