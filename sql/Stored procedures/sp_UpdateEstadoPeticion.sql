/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************
*/

use GesPet
go

print 'Creando / actualizando: sp_UpdateEstadoPeticion'
go

if exists (select * from sysobjects where name = 'sp_UpdateEstadoPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEstadoPeticion
go

create procedure dbo.sp_UpdateEstadoPeticion 
	@pet_nrointerno		int
as 
	declare @cod_sector		char(8)
	declare @new_estado_sec	char(6)
	declare @new_estado_pet	char(6)

	-- Cursor: sectores
	declare CursSectores cursor for
		select cod_sector
		from GesPet..PeticionSector
		where pet_nrointerno = @pet_nrointerno
		group by cod_sector
		for read only 

		open CursSectores 
			fetch CursSectores into @cod_sector
			while @@sqlstatus = 0
				begin
					-- Obtengo el nuevo estado del sector
					exec sp_GetNewEstSector @pet_nrointerno, @cod_sector, @new_estado_sec output
					
					-- Si no tiene grupos o viene vacio, el sector queda cancelado definitivamente
					if @new_estado_sec is null or @new_estado_sec = '' or @new_estado_sec = 'REVISA'
						select @new_estado_sec = 'CANCEL'

					-- Actualizo el estado del sector
					update 
						GesPet..PeticionSector
					set 
						cod_estado = @new_estado_sec,
						fe_estado  = getdate()
					where
						pet_nrointerno = @pet_nrointerno and
						cod_sector = @cod_sector
					
					fetch CursSectores into @cod_sector
				end
		close CursSectores 
		deallocate cursor CursSectores

		-- Obtengo el nuevo estado de la petición
		exec sp_GetNewEstPeticion @pet_nrointerno, @new_estado_pet output

		-- Actualizo el nuevo estado de la petición
		update 
			GesPet..Peticion
		set
			cod_estado = @new_estado_pet,
			fe_estado  = getdate()
		where
			pet_nrointerno = @pet_nrointerno

return(0) 
go

grant execute on dbo.sp_UpdateEstadoPeticion to GesPetUsr 
go

print 'Actualización realizada.'
go