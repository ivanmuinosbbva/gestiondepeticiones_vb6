/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateUnidadMedida'
go

if exists (select * from sysobjects where name = 'sp_UpdateUnidadMedida' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateUnidadMedida
go

create procedure dbo.sp_UpdateUnidadMedida
	@unimedId	char(10),
	@unimedDesc	char(60),
	@unimedSmb	char(4)
as
	update 
		GesPet.dbo.UnidadMedida
	set
		unimedDesc = @unimedDesc,
		unimedSmb = @unimedSmb
	where 
		unimedId = @unimedId
go

grant execute on dbo.sp_UpdateUnidadMedida to GesPetUsr
go

print 'Actualización realizada.'
go
