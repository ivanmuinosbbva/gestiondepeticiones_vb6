/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetInfogru'
go

if exists (select * from sysobjects where name = 'sp_GetInfogru' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetInfogru
go

create procedure dbo.sp_GetInfogru
	@info_gruid	int=null
as
	select 
		p.info_gruid,
		p.info_grunom,
		p.info_gruorden,
		p.info_gruhab
	from 
		GesPet.dbo.Infogru p
	where
		(p.info_gruid = @info_gruid or @info_gruid is null)
go

grant execute on dbo.sp_GetInfogru to GesPetUsr
go

print 'Actualización realizada.'
go
