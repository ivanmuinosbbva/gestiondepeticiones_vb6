/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteInfoproi'
go

if exists (select * from sysobjects where name = 'sp_DeleteInfoproi' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteInfoproi
go

create procedure dbo.sp_DeleteInfoproi
	@infoprot_item	int=0
as
	delete from
		GesPet.dbo.Infoproi
	where
		(infoprot_item = @infoprot_item or @infoprot_item is null)
go

grant execute on dbo.sp_DeleteInfoproi to GesPetUsr
go

print 'Actualización realizada.'
go
