/*
-000- a. FJS 16.04.2015 - Nuevo: crea de manera automática el encabezado y detalle de un informe de homologación.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionInforme'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionInforme' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionInforme
go

create procedure dbo.sp_InsertPeticionInforme
	@pet_nrointerno		int,
	@punto_control		char(1)
as
	-- Obtengo el siguiente nro. disponible (PK)
	declare @proxnumero			int 
	declare @clase				char(4)
	declare @tipo_proceso		char(1)		-- Tipo de proceso: (C)ompleto o (R)educido.
	declare @infoprot_id		int
	declare @cod_recurso		char(10)
	declare @texto				varchar(255)
	declare @hst_nrointerno		int
	declare @evento				char(8)
	declare @pet_estado			char(6)
	declare @nombre_proceso		char(8)
	declare @nombre_informe		varchar(255)
	
	select @proxnumero = 0 
	execute sp_ProximoNumero 'ULPETHOM', @proxnumero OUTPUT 
	
	select 
		@clase = p.cod_clase,
		@pet_estado = p.cod_estado
	from Peticion p
	where p.pet_nrointerno = @pet_nrointerno
	
	/*
	-- TODO: aquí hay que determinar el tipo de proceso según la parametrización de la tabla Infoflujop y la clase de la petición
	IF (@clase is not null and LTRIM(RTRIM(@clase)) <> '') 
		BEGIN
			SELECT @infoflujo_id = infoflujo_id
			FROM Infoflujop
			WHERE infoflujo_clase = LTRIM(RTRIM(@clase))

			SELECT @tipo_proceso = infoflujo_tipo
			FROM Infoflujoc
			WHERE infoflujo_id = @infoflujo_id


		END
	ELSE 
		BEGIN
		
		END
	*/
	/*
	IF charindex(@clase,'EVOL|NUEV|OPTI|') >0
		select @tipo_proceso = 'C'
	ELSE
		begin
			select @tipo_proceso = 'R'
			select @punto_control = '1'
		end
	*/

	/*
	-- Elijo un homologador automáticamente para asignarle a la petición
	select top 1 @cod_recurso = pc.info_recurso
	from PeticionInfohc pc
	group by pc.info_recurso
	order by 1 desc
	*/

	-- Encabezado
	SELECT 
		@infoprot_id = ic.infoprot_id,
		@nombre_informe = LTRIM(RTRIM(infoprot_nom)),
		@nombre_proceso = case
			when ic.infoprot_tipo = 'C' then 'Completo'
			when ic.infoprot_tipo = 'R' then 'Reducido'
		end
	FROM Infoproc ic
	WHERE ic.infoprot_tipo = @tipo_proceso and ic.infoprot_punto = @punto_control and ic.infoprot_estado = 'S'

	if @tipo_proceso = 'C'
		begin
			INSERT INTO PeticionInfohc (pet_nrointerno, info_id, info_idver, info_tipo, info_punto, info_fecha, info_recurso, info_estado, infoprot_id) 
				SELECT @pet_nrointerno, @proxnumero, 0, @tipo_proceso, convert(int,@punto_control), getdate(), @cod_recurso, 'CONFEC', @infoprot_id
				FROM Infoproc ic
				WHERE ic.infoprot_id = @infoprot_id and ic.infoprot_punto = @punto_control and ic.infoprot_estado = 'S'

			if @punto_control = '1'
				begin
					-- Detalle
					INSERT INTO PeticionInfohd (pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden) 
						SELECT @pet_nrointerno, @proxnumero, 0, id.infoprot_item, '', null, id.infoprot_orden
						FROM Infoprod id
						WHERE id.infoprot_id = @infoprot_id
				end
			else
				begin
					if @punto_control = '2'
						select @punto_control = '1'
					if @punto_control = '3'
						select @punto_control = '2'

					if exists (
						select pc.pet_nrointerno
						from PeticionInfohc pc
						where pc.pet_nrointerno = @pet_nrointerno and pc.info_punto = convert(int,@punto_control))
						begin
							INSERT INTO PeticionInfohd (pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden) 
								select
									@pet_nrointerno, @proxnumero, 0, 
									id.infoprot_item,
									pd.info_estado,
									pd.info_valor,
									id.infoprot_orden
								from 
									PeticionInfohd pd inner join
									PeticionInfohc pc on (pd.pet_nrointerno = pc.pet_nrointerno and pd.info_id = pc.info_id) inner join
									Infoprod id on (id.infoprot_id = pc.infoprot_id and id.infoprot_item = pd.info_item)
								where
									pd.pet_nrointerno = @pet_nrointerno and 
									pc.info_punto = convert(int,@punto_control) 
						end
					else
						begin
							-- Detalle
							INSERT INTO PeticionInfohd (pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden) 
								SELECT @pet_nrointerno, @proxnumero, 0, id.infoprot_item, '', null, id.infoprot_orden
								FROM Infoprod id
								WHERE id.infoprot_id = @infoprot_id
						end
					
					-- Agregamos el evento en el historial
					select @evento = LTRIM(RTRIM('INFOHP' + @punto_control))
					select @cod_recurso = SUSER_NAME()
					select @texto = 'Se crea un ' + LTRIM(RTRIM(@nombre_informe)) + ' (' + LTRIM(RTRIM(LOWER(@nombre_proceso))) + ')'
					--select @texto = 'Se crea el informe de homologación para el punto de control ' + @punto_control + ' (proceso Completo).'
					exec sp_AddHistorial @hst_nrointerno OUTPUT, @pet_nrointerno, @evento, @pet_estado, null, null, null, null, @cod_recurso
					exec sp_AddHistorialMemo @hst_nrointerno, 0, @texto
				end
		end
	else
		begin
			-- Compruebo si ya no existe el informe
			if not exists (
				select pc.pet_nrointerno
				from PeticionInfohc pc
				where pc.pet_nrointerno = @pet_nrointerno and pc.info_punto = convert(int,@punto_control))
				begin
					SELECT @infoprot_id = ic.infoprot_id
					FROM Infoproc ic
					WHERE ic.infoprot_tipo = @tipo_proceso and ic.infoprot_punto = @punto_control and ic.infoprot_estado = 'S'

					INSERT INTO PeticionInfohc (pet_nrointerno, info_id, info_idver, info_tipo, info_punto, info_fecha, info_recurso, info_estado, infoprot_id) 
						SELECT @pet_nrointerno, @proxnumero, 0, @tipo_proceso, convert(int,@punto_control), getdate(), @cod_recurso, 'CONFEC', @infoprot_id
						FROM Infoproc ic
						WHERE ic.infoprot_id = @infoprot_id and ic.infoprot_punto = @punto_control and ic.infoprot_estado = 'S'
					
					-- Detalle
					INSERT INTO PeticionInfohd (pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden) 
						SELECT @pet_nrointerno, @proxnumero, 0, id.infoprot_item, '', null, id.infoprot_orden
						FROM Infoprod id
						WHERE id.infoprot_id = @infoprot_id

					-- Agregamos el evento en el historial
					select @evento = LTRIM(RTRIM('INFOH' + @punto_control))
					select @cod_recurso = SUSER_NAME()
					--select @texto = 'Se crea el informe de homologación para el punto de control ' + @punto_control + ' (proceso Reducido).'
					select @texto = 'Se crea un ' + LTRIM(RTRIM(@nombre_informe)) + ' (' + LTRIM(RTRIM(LOWER(@nombre_proceso))) + ')'
					exec sp_AddHistorial @hst_nrointerno OUTPUT, @pet_nrointerno, @evento, @pet_estado, null, null, null, null, @cod_recurso
					exec sp_AddHistorialMemo @hst_nrointerno, 0, @texto
				end
		end
	return(0)
go

grant execute on dbo.sp_InsertPeticionInforme to GesPetUsr
go

print 'Actualización realizada.'
go
