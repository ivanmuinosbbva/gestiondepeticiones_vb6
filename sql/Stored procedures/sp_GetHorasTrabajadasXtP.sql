/*
Este SP se utiliza para el informe de Crystal Report: hsrecu.rpt (Horas cargadas en un período para el reporte que ejecuta).

-001- a. FJS 22.05.2009 - Se agregan datos del desglose de horas trabajadas por aplicativo.
Pepe
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHorasTrabajadasXtP'
go

if exists (select * from sysobjects where name = 'sp_GetHorasTrabajadasXtP' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHorasTrabajadasXtP
go

create procedure dbo.sp_GetHorasTrabajadasXtP
	@cod_recurso	char(10),
	@fechadesde		char(8)=null,
	@fechahasta		char(8)=null
as
	begin
		select
			Ht.cod_recurso,
			nom_recurso = (select r.nom_recurso from Recurso r where r.cod_recurso = Ht.cod_recurso),
			Ht.cod_tarea,
			nom_tarea = (select Ta.nom_tarea from Tarea Ta where Ta.cod_tarea=Ht.cod_tarea),
			Ht.pet_nrointerno,
			pet_nroasignado = (select x.pet_nroasignado from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno),
			pet_titulo = (select x.titulo from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno),
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas,
			Ht.trabsinasignar,
			Ht.observaciones
		from
			GesPet..HorasTrabajadas Ht
		where
			(@fechadesde is null or Ht.fe_desde >= @fechadesde) and
			(@fechahasta is null or Ht.fe_hasta <= @fechahasta) and
			Ht.cod_recurso = @cod_recurso
	end
	return(0)
go

grant execute on dbo.sp_GetHorasTrabajadasXtP to GesPetUsr
go

print 'Actualización realizada.'
go





/*
	begin
		select
			Ht.cod_recurso,
			nom_recurso = (select Re.nom_recurso from Recurso Re where RTRIM(Re.cod_recurso)=RTRIM(Ht.cod_recurso)),
			Ht.cod_tarea,
			nom_tarea = (select Ta.nom_tarea from Tarea Ta where RTRIM(Ta.cod_tarea)=RTRIM(Ht.cod_tarea)),
			Ht.pet_nrointerno,
			Pt.pet_nroasignado,
			pet_titulo=Pt.titulo,
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas,
			Ht.trabsinasignar,
			Ht.observaciones
			--{ add -001- a.
			,desglose = (select count(*) from GesPet..HorasTrabajadasAplicativo x where 
						 x.cod_recurso = Ht.cod_recurso and x.cod_tarea = Ht.cod_tarea and x.pet_nrointerno = Ht.pet_nrointerno and x.fe_desde = Ht.fe_desde and x.fe_hasta = Ht.fe_hasta and x.horas = Ht.horas)
			--}
		from
			GesPet..HorasTrabajadas Ht,
			GesPet..Peticion Pt
		where
			(RTRIM(@cod_recurso) = RTRIM(Ht.cod_recurso)) and
			(@fechadesde is null or convert(char(8),fe_hasta,112) >= @fechadesde) and
			(@fechahasta is null or convert(char(8),fe_desde,112) <= @fechahasta) and
			(Ht.pet_nrointerno *= Pt.pet_nrointerno)
		order by 
			Ht.cod_recurso,
			convert(char(8),Ht.fe_desde,112) asc
	end
*/