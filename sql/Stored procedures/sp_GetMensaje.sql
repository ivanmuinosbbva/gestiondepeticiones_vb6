use GesPet
go
print 'sp_GetMensaje'
go
if exists (select * from sysobjects where name = 'sp_GetMensaje' and sysstat & 7 = 4)
drop procedure dbo.sp_GetMensaje
go
create procedure dbo.sp_GetMensaje 
             @msg_nrointerno    int=null 
as 
    select  
    msg_nrointerno, 
    pet_nrointerno, 
    fe_mensaje, 
    cod_perfil, 
    cod_nivel, 
    cod_area, 
    cod_txtmsg, 
    cod_estado, 
    cod_situacion, 
    txt_txtmsg 
    from 
        GesPet..Mensajes 
    where   
        (@msg_nrointerno is null or @msg_nrointerno=msg_nrointerno) 
return(0) 
go



grant execute on dbo.sp_GetMensaje to GesPetUsr 
go


