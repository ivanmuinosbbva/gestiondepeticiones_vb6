/* REVISAR PARA CONSOLIDAR */

/*
-000- a. FJS 25.09.2009 - Reformatea la cadena para ChangeMan
*/

use GesPet
go

print 'Creando/actualizando SP: sp_PeticionEnviadas2Fmt'
go

if exists (select * from sysobjects where name = 'sp_PeticionEnviadas2Fmt' and sysstat & 7 = 4)
	drop procedure dbo.sp_PeticionEnviadas2Fmt
go

create procedure dbo.sp_PeticionEnviadas2Fmt
as 
	declare @pet_nrointerno	int
	declare @pet_record	char(80)
	
	select @pet_nrointerno = 0
	select @pet_record	= ''

	declare curPeticiones cursor for 
		select
			a.pet_nrointerno
		from
			GesPet..PeticionEnviadas2 a
		group by
			a.pet_nrointerno
		for read only 

		open curPeticiones
		fetch curPeticiones into @pet_nrointerno 
		while @@sqlstatus = 0 
			begin 
				execute sp_FormatEnviadas @pet_nrointerno, 'SECTOR', 'GRUPO', @pet_record output 
				
				if datalength(@pet_record) < 80 
					select @pet_record = @pet_record + space(datalength(@pet_record) - 80) 

				update
					GesPet..PeticionEnviadas2 
				set    
					pet_record = @pet_record
				where   
					pet_nrointerno = @pet_nrointerno
				
				fetch curPeticiones into @pet_nrointerno
			end 
		close curPeticiones
	deallocate cursor curPeticiones 
go

grant execute on dbo.sp_PeticionEnviadas2Fmt to GesPetUsr
go

print 'Actualización realizada.'