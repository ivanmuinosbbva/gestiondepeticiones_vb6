/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 12.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateAccionesGrupo'
go

if exists (select * from sysobjects where name = 'sp_UpdateAccionesGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateAccionesGrupo
go

create procedure dbo.sp_UpdateAccionesGrupo
	@accionGrupoId	int,
	@accionGrupoNom	varchar(50),
	@accionGrupoHab	char(1)
as
	update 
		GesPet.dbo.AccionesGrupo
	set
		accionGrupoNom = @accionGrupoNom,
		accionGrupoHab = @accionGrupoHab
	where 
		accionGrupoId = @accionGrupoId
go

grant execute on dbo.sp_UpdateAccionesGrupo to GesPetUsr
go

print 'Actualización realizada.'
go
