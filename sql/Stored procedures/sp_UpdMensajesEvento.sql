use GesPet
go
print 'sp_UpdMensajesEvento'
go
if exists (select * from sysobjects where name = 'sp_UpdMensajesEvento' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdMensajesEvento
go
create procedure dbo.sp_UpdMensajesEvento 
    @evt_alcance char(3), 
    @cod_accion char(8), 
    @cod_estado char(6), 
    @cod_perfil char(4), 
    @cod_txtmsg int=0, 
    @est_recept varchar(3), 
    @est_codigo varchar(255)='', 
    @txt_extend varchar(255)='', 
    @txt_pzofin varchar(1) ='N', 
    @flg_activo varchar(1) ='S' 
as 
 
if not exists (select 1 from GesPet..MensajesEvento where 
        RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
        RTRIM(cod_accion)=RTRIM(@cod_accion) and 
        RTRIM(cod_estado)=RTRIM(@cod_estado) and 
        RTRIM(cod_perfil)=RTRIM(@cod_perfil))   
begin   
     select 30012 as ErrCode ,'Evento/Mensaje Inexistente' as ErrDesc   
     raiserror 30012 'Evento/Mensaje Inexistente'   
     return (30012)   
end 
else 
begin 
    update GesPet..MensajesEvento  
    set     cod_txtmsg=@cod_txtmsg, 
        est_recept=@est_recept, 
        est_codigo=@est_codigo, 
        txt_extend=@txt_extend, 
        txt_pzofin=@txt_pzofin, 
        flg_activo=@flg_activo 
    where   RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
        RTRIM(cod_accion)=RTRIM(@cod_accion) and 
        RTRIM(cod_estado)=RTRIM(@cod_estado) and 
        RTRIM(cod_perfil)=RTRIM(@cod_perfil)   
end 
return(0) 
go



grant execute on dbo.sp_UpdMensajesEvento to GesPetUsr 
go


