/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteValidacion'
go

if exists (select * from sysobjects where name = 'sp_DeleteValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteValidacion
go

create procedure dbo.sp_DeleteValidacion
	@valid		int,
	@valitem	int
as
	delete from
		GesPet.dbo.Validacion
	where
		valid = @valid and
		valitem = @valitem
go

grant execute on dbo.sp_DeleteValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
