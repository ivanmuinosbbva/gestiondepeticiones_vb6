/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 11.08.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertEstadoSolicitud'
go

if exists (select * from sysobjects where name = 'sp_InsertEstadoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertEstadoSolicitud
go

create procedure dbo.sp_InsertEstadoSolicitud
	@cod_estado	char(1),
	@nom_estado	char(50)
as
	insert into GesPet.dbo.EstadoSolicitud (
		cod_estado,
		nom_estado)
	values (
		@cod_estado,
		@nom_estado)
go

grant execute on dbo.sp_InsertEstadoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
