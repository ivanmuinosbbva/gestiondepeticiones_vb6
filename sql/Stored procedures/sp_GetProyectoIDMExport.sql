/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 05.02.2013 - 
-001- a. FJS 20.03.2015 - Se agregan dos nuevas columnas en Hitos.
-002- a. FJS 05.11.2015 - Prueba. 
-003- a. FJS 12.01.2016 - Modificación: se agrega un nuevo campo para Empresa (codigoEmpresa) dependiente de una tabla de empresas.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMExport'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMExport' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMExport
go

create procedure dbo.sp_GetProyectoIDMExport
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null,
	@Hitos			char(1)='N',
	@HitosExpos		char(1)='N',
	@projnom		char(100)=null,
	@PlanTyO		char(1)=null,
	@Activos		char(100)=null,
	@flg_area		char(4)=null,
	@cod_nivel		char(4)=null,
	@cod_area		char(8)=null
as
	if @Hitos = 'S'
		begin
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				null,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where PRJ.ProjCatId = x.ProjCatId),
				PRJ.ProjClaseId,
				ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where PRJ.ProjCatId = x.ProjCatId and PRJ.ProjClaseId = x.ProjClaseId),
				'nivel' = 
					case
						when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
						when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
						when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
					end,
				PRJ.cod_estado,
				nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado),
				PRJ.fch_estado,
				PRJ.usr_estado,
				nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
				,PRJ.marca
				,PRJ.plan_tyo
				,plan_TYO_nom = case
					when PRJ.plan_tyo = 'S' then 'Si'
					when PRJ.plan_tyo = 'N' then 'No'
					else ''
				end
				/* { del -003- a.
				,PRJ.empresa
				,nom_empresa = case
					when PRJ.empresa = 'B' then 'Banco'
					when PRJ.empresa = 'C' then 'BBVA Seguros'
					when PRJ.empresa = 'D' then 'RCF'
					when PRJ.empresa = 'E' then 'PSA'
					when PRJ.empresa = 'F' then 'RCF/PSA'
					else ''
				end
				} */
				--{ add -003- a.
				,PRJ.codigoEmpresa	as empresa
				,nom_empresa = (select emp.empabrev from Empresa emp where emp.empid = PRJ.codigoEmpresa)
				--}
				,PRJ.area
				,area_nom = case
					when PRJ.area = 'C' then 'Corporativo'
					when PRJ.area = 'R' then 'Regional'
					when PRJ.area = 'L' then 'Local'
					else ''
				end
				,PRJ.clas3
				,clas3_nom = case
					when PRJ.clas3 = '1' then '2013'
					when PRJ.clas3 = '2' then '2014'
					when PRJ.clas3 = '3' then '2015'
					when PRJ.clas3 = '4' then '2013 a 2014'
					when PRJ.clas3 = '5' then '2013 a 2015'
					when PRJ.clas3 = '6' then '2014 a 2015'
					else ''
				end
				,PRJ.semaphore
				,PRJ.cod_direccion
				,PRJ.cod_gerencia
				,PRJ.cod_sector
				,nom_areasoli = (case
					when PRJ.cod_gerencia is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion))
					when PRJ.cod_sector is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)) + ' » ' + 
						RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector))
				end)
				,PRJ.cod_direccion_p
				,PRJ.cod_gerencia_p
				,PRJ.cod_sector_p
				,PRJ.cod_grupo_p
				,nom_areaprop = (case
					when PRJ.cod_gerencia_p is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
					when PRJ.cod_sector_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
					when PRJ.cod_grupo_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
						' » ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
				end)
				,PRJ.avance
				,PRJ.cod_estado2
				,PRJ.fe_modif
				,PRJ.fe_inicio
				,PRJ.fe_fin
				,PRJ.fe_finreprog
				,PRJ.semaphore2
				,PRJ.sema_fe_modif
				,PRJ.sema_usuario
				,PRJ.tipo_proyecto
				,PRJ.totalhs_gerencia
				,PRJ.totalhs_sector
				,PRJ.totalreplan
				,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId),
				null	as	hito_nombre,
				null	as	hito_descripcion,
				null	as	hito_fe_ini,
				null	as	hito_fe_fin,
				null	as	hito_estado,
				null	as	cod_nivel,
				null	as	cod_area,
				null	as	hito_fecha,
				null	as	hito_usuario,
				null	as	hit_orden,
				null	as	hito_expos,
				null	as	hito_id,
				null	as	hitodesc
			from
				GesPet..ProyectoIDM PRJ
			where
				(@ProjId is null or PRJ.ProjId = @ProjId) and
				(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
				(@ProjSubSId is null or PRJ.ProjSubSId = @ProjSubSId) and
				(@projnom is null or upper(rtrim(PRJ.ProjNom)) like '%' + upper(rtrim(@projnom)) + '%') and
				(@PlanTyO is null or @PlanTyO = PRJ.plan_tyo) and
				(@Activos is null or charindex(PRJ.cod_estado, RTRIM(@Activos))>0) and 
				(@flg_area is null or @flg_area = 'PROP' and (
					(@cod_nivel = 'DIRE' and PRJ.cod_direccion_p = @cod_area) or
					(@cod_nivel = 'GERE' and PRJ.cod_gerencia_p = @cod_area) or
					(@cod_nivel = 'SECT' and PRJ.cod_sector_p = @cod_area) or
					(@cod_nivel = 'GRUP' and PRJ.cod_grupo_p = @cod_area))
				)
		UNION ALL 
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				hito_orden = case
					when a.hit_orden is null then 1000000
					when a.hit_orden = 0 then 0
					else a.hit_orden
				end,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where PRJ.ProjCatId = x.ProjCatId),
				PRJ.ProjClaseId,
				ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where PRJ.ProjCatId = x.ProjCatId and PRJ.ProjClaseId = x.ProjClaseId),
				'nivel' = 
					case
						when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
						when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
						when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
					end,
				PRJ.cod_estado,
				nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado),
				PRJ.fch_estado,
				PRJ.usr_estado,
				nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
				,PRJ.marca
				,PRJ.plan_tyo
				,plan_TYO_nom = case
					when PRJ.plan_tyo = 'S' then 'Si'
					when PRJ.plan_tyo = 'N' then 'No'
					else ''
				end
				/* { del -003- a.
				,PRJ.empresa
				,nom_empresa = case
					when PRJ.empresa = 'B' then 'Banco'
					when PRJ.empresa = 'C' then 'BBVA Seguros'
					when PRJ.empresa = 'D' then 'RCF'
					when PRJ.empresa = 'E' then 'PSA'
					when PRJ.empresa = 'F' then 'RCF/PSA'
					else ''
				end
				}*/
				--{ add -003- a.
				,PRJ.codigoEmpresa	as empresa
				,nom_empresa = (select emp.empabrev from Empresa emp where emp.empid = PRJ.codigoEmpresa)
				--}
				,PRJ.area
				,area_nom = case
					when PRJ.area = 'C' then 'Corporativo'
					when PRJ.area = 'R' then 'Regional'
					when PRJ.area = 'L' then 'Local'
					else ''
				end
				,PRJ.clas3
				,clas3_nom = case
					when PRJ.clas3 = '1' then '2013'
					when PRJ.clas3 = '2' then '2014'
					when PRJ.clas3 = '3' then '2015'
					when PRJ.clas3 = '4' then '2013 a 2014'
					when PRJ.clas3 = '5' then '2013 a 2015'
					when PRJ.clas3 = '6' then '2014 a 2015'
					else ''
				end
				,PRJ.semaphore
				,PRJ.cod_direccion
				,PRJ.cod_gerencia
				,PRJ.cod_sector
				,nom_areasoli = (case
					when PRJ.cod_gerencia is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion))
					when PRJ.cod_sector is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)) + ' » ' + 
						RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector))
				end)
				,PRJ.cod_direccion_p
				,PRJ.cod_gerencia_p
				,PRJ.cod_sector_p
				,PRJ.cod_grupo_p
				,nom_areaprop = (case
					when PRJ.cod_gerencia_p is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
					when PRJ.cod_sector_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
					when PRJ.cod_grupo_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
						' » ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
				end)
				,PRJ.avance
				,PRJ.cod_estado2
				,PRJ.fe_modif
				,PRJ.fe_inicio
				,PRJ.fe_fin
				,PRJ.fe_finreprog
				,PRJ.semaphore2
				,PRJ.sema_fe_modif
				,PRJ.sema_usuario
				,PRJ.tipo_proyecto
				,PRJ.totalhs_gerencia
				,PRJ.totalhs_sector
				,PRJ.totalreplan
				,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId),
				a.hito_nombre,
				a.hito_descripcion,
				a.hito_fe_ini,
				a.hito_fe_fin,
				a.hito_estado,
				a.cod_nivel,
				a.cod_area,
				a.hito_fecha,
				a.hito_usuario,
				a.hit_orden,
				a.hito_expos,
				a.hito_id,
				hitodesc = (select hc.hitodesc from GesPet..HitoClase hc where hc.hitoid = a.hito_id)
			from
				GesPet..ProyectoIDM PRJ left join 
				GesPet..ProyectoIDMHitos a on (PRJ.ProjId = a.ProjId and PRJ.ProjSubId = a.ProjSubId and PRJ.ProjSubSId = a.ProjSubSId)
			where
				(@ProjId is null or PRJ.ProjId = @ProjId) and
				(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
				(@ProjSubSId is null or PRJ.ProjSubSId = @ProjSubSId) and 
				(@projnom is null or upper(rtrim(PRJ.ProjNom)) like '%' + upper(rtrim(@projnom)) + '%') and
				(@PlanTyO is null or @PlanTyO = PRJ.plan_tyo) and
				(@Activos is null or charindex(PRJ.cod_estado, RTRIM(@Activos))>0) and 
				(@flg_area is null or @flg_area = 'PROP' and (
					(@cod_nivel = 'DIRE' and PRJ.cod_direccion_p = @cod_area) or
					(@cod_nivel = 'GERE' and PRJ.cod_gerencia_p = @cod_area) or
					(@cod_nivel = 'SECT' and PRJ.cod_sector_p = @cod_area) or
					(@cod_nivel = 'GRUP' and PRJ.cod_grupo_p = @cod_area))
				) and ((@HitosExpos = 'S' and a.hito_expos = 'S') or @HitosExpos = 'N')
			order by
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				4
		end
	else
		begin
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where PRJ.ProjCatId = x.ProjCatId),
				PRJ.ProjClaseId,
				ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where PRJ.ProjCatId = x.ProjCatId and PRJ.ProjClaseId = x.ProjClaseId),
				'nivel' = 
					case
						when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
						when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
						when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
					end,
				PRJ.cod_estado,
				nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado),
				PRJ.fch_estado,
				PRJ.usr_estado,
				nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
				,PRJ.marca
				,PRJ.plan_tyo
				,plan_TYO_nom = case
					when PRJ.plan_tyo = 'S' then 'Si'
					when PRJ.plan_tyo = 'N' then 'No'
					else ''
				end
				/* { del -003- a.
				,PRJ.empresa
				,nom_empresa = case
					when PRJ.empresa = 'B' then 'Banco'
					when PRJ.empresa = 'C' then 'BBVA Seguros'
					when PRJ.empresa = 'D' then 'RCF'
					when PRJ.empresa = 'E' then 'PSA'
					when PRJ.empresa = 'F' then 'RCF/PSA'
					else ''
				end
				}*/
				--{ add -003- a.
				,PRJ.codigoEmpresa	as empresa
				,nom_empresa = (select emp.empabrev from Empresa emp where emp.empid = PRJ.codigoEmpresa)
				--}
				,PRJ.area
				,area_nom = case
					when PRJ.area = 'C' then 'Corporativo'
					when PRJ.area = 'R' then 'Regional'
					when PRJ.area = 'L' then 'Local'
					else ''
				end
				,PRJ.clas3
				,clas3_nom = case
					when PRJ.clas3 = '1' then '2013'
					when PRJ.clas3 = '2' then '2014'
					when PRJ.clas3 = '3' then '2015'
					when PRJ.clas3 = '4' then '2013 a 2014'
					when PRJ.clas3 = '5' then '2013 a 2015'
					when PRJ.clas3 = '6' then '2014 a 2015'
					else ''
				end
				,PRJ.semaphore
				,PRJ.cod_direccion
				,PRJ.cod_gerencia
				,PRJ.cod_sector
				,nom_areasoli = (case
					when PRJ.cod_gerencia is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion))
					when PRJ.cod_sector is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)) + ' » ' + 
						RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector))
				end)
				,PRJ.cod_direccion_p
				,PRJ.cod_gerencia_p
				,PRJ.cod_sector_p
				,PRJ.cod_grupo_p
				,nom_areaprop = (case
					when PRJ.cod_gerencia_p is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
					when PRJ.cod_sector_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
					when PRJ.cod_grupo_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
						' » ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
				end)
				,PRJ.avance
				,PRJ.cod_estado2
				,PRJ.fe_modif
				,PRJ.fe_inicio
				,PRJ.fe_fin
				,PRJ.fe_finreprog
				,PRJ.semaphore2
				,PRJ.sema_fe_modif
				,PRJ.sema_usuario
				,PRJ.tipo_proyecto
				,PRJ.totalhs_gerencia
				,PRJ.totalhs_sector
				,PRJ.totalreplan
				,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId)
			from
				GesPet..ProyectoIDM PRJ
			where
				(@ProjId is null or PRJ.ProjId = @ProjId) and
				(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
				(@ProjSubSId is null or PRJ.ProjSubSId = @ProjSubSId) and
				(@projnom is null or upper(rtrim(PRJ.ProjNom)) like '%' + upper(rtrim(@projnom)) + '%') and
				(@PlanTyO is null or @PlanTyO = PRJ.plan_tyo) and
				(@Activos is null or charindex(PRJ.cod_estado, RTRIM(@Activos))>0) and 
				(@flg_area is null or @flg_area = 'PROP' and (
					(@cod_nivel = 'DIRE' and PRJ.cod_direccion_p = @cod_area) or
					(@cod_nivel = 'GERE' and PRJ.cod_gerencia_p = @cod_area) or
					(@cod_nivel = 'SECT' and PRJ.cod_sector_p = @cod_area) or
					(@cod_nivel = 'GRUP' and PRJ.cod_grupo_p = @cod_area))
				)
			order by
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId
		end
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMExport to GesPetUsr
go

print 'Actualización realizada.'
go





/* ORIGINAL 05.11.2015

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMExport'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMExport' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMExport
go

create procedure dbo.sp_GetProyectoIDMExport
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null,
	@Hitos			char(1)='N',
	@HitosExpos		char(1)='N',
	@projnom		char(100)=null,
	@PlanTyO		char(1)=null,
	@Activos		char(100)=null,
	@flg_area		char(4)=null,
	@cod_nivel		char(4)=null,
	@cod_area		char(8)=null
as
	if @Hitos = 'S'
		begin
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				hito_orden = case
					when a.hit_orden is null then 1000000
					when a.hit_orden = 0 then 0
					else a.hit_orden
				end,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where PRJ.ProjCatId = x.ProjCatId),
				PRJ.ProjClaseId,
				ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where PRJ.ProjCatId = x.ProjCatId and PRJ.ProjClaseId = x.ProjClaseId),
				'nivel' = 
					case
						when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
						when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
						when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
					end,
				PRJ.cod_estado,
				nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado),
				PRJ.fch_estado,
				PRJ.usr_estado,
				nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
				,PRJ.marca
				,PRJ.plan_tyo
				,plan_TYO_nom = case
					when PRJ.plan_tyo = 'S' then 'Si'
					when PRJ.plan_tyo = 'N' then 'No'
					else ''
				end
				,PRJ.empresa
				,nom_empresa = case
					when PRJ.empresa = 'B' then 'Banco'
					when PRJ.empresa = 'C' then 'BBVA Seguros'
					when PRJ.empresa = 'D' then 'RCF'
					when PRJ.empresa = 'E' then 'PSA'
					when PRJ.empresa = 'F' then 'RCF/PSA'
					else ''
				end
				,PRJ.area
				,area_nom = case
					when PRJ.area = 'C' then 'Corporativo'
					when PRJ.area = 'R' then 'Regional'
					when PRJ.area = 'L' then 'Local'
					else ''
				end
				,PRJ.clas3
				,clas3_nom = case
					when PRJ.clas3 = '1' then '2013'
					when PRJ.clas3 = '2' then '2014'
					when PRJ.clas3 = '3' then '2015'
					when PRJ.clas3 = '4' then '2013 a 2014'
					when PRJ.clas3 = '5' then '2013 a 2015'
					when PRJ.clas3 = '6' then '2014 a 2015'
					else ''
				end
				,PRJ.semaphore
				,PRJ.cod_direccion
				,PRJ.cod_gerencia
				,PRJ.cod_sector
				,nom_areasoli = (case
					when PRJ.cod_gerencia is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion))
					when PRJ.cod_sector is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)) + ' » ' + 
						RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector))
				end)
				,PRJ.cod_direccion_p
				,PRJ.cod_gerencia_p
				,PRJ.cod_sector_p
				,PRJ.cod_grupo_p
				,nom_areaprop = (case
					when PRJ.cod_gerencia_p is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
					when PRJ.cod_sector_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
					when PRJ.cod_grupo_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
						' » ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
				end)
				,PRJ.avance
				,PRJ.cod_estado2
				,PRJ.fe_modif
				,PRJ.fe_inicio
				,PRJ.fe_fin
				,PRJ.fe_finreprog
				,PRJ.semaphore2
				,PRJ.sema_fe_modif
				,PRJ.sema_usuario
				,PRJ.tipo_proyecto
				,PRJ.totalhs_gerencia
				,PRJ.totalhs_sector
				,PRJ.totalreplan
				,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId),
				a.hito_nombre,
				a.hito_descripcion,
				a.hito_fe_ini,
				a.hito_fe_fin,
				a.hito_estado,
				--nom_estado_hito = isnull((select x.descripcion from GesPet..ProyectoIDMHitosEstados x where x.hito_estado = a.hito_estado),''),
				a.cod_nivel,
				a.cod_area,
				a.hito_fecha,
				a.hito_usuario,
				--{ add -001- a.
				a.hit_orden,
				a.hito_expos,
				a.hito_id,
				hitodesc = (select hc.hitodesc from GesPet..HitoClase hc where hc.hitoid = a.hito_id)
				--}
			from
				GesPet..ProyectoIDM PRJ left join 
				GesPet..ProyectoIDMHitos a on (PRJ.ProjId = a.ProjId and PRJ.ProjSubId = a.ProjSubId and PRJ.ProjSubSId = a.ProjSubSId)
			where
				(@ProjId is null or PRJ.ProjId = @ProjId) and
				(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
				(@ProjSubSId is null or PRJ.ProjSubSId = @ProjSubSId) and 
				(@projnom is null or upper(rtrim(PRJ.ProjNom)) like '%' + upper(rtrim(@projnom)) + '%') and
				(@PlanTyO is null or @PlanTyO = PRJ.plan_tyo) and
				(@Activos is null or charindex(PRJ.cod_estado, RTRIM(@Activos))>0) and 
				(@flg_area is null or @flg_area = 'PROP' and (
					(@cod_nivel = 'DIRE' and PRJ.cod_direccion_p = @cod_area) or
					(@cod_nivel = 'GERE' and PRJ.cod_gerencia_p = @cod_area) or
					(@cod_nivel = 'SECT' and PRJ.cod_sector_p = @cod_area) or
					(@cod_nivel = 'GRUP' and PRJ.cod_grupo_p = @cod_area))
				) and ((@HitosExpos = 'S' and a.hito_expos = 'S') or @HitosExpos = 'N')
			order by
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				4
		end
	else
		begin
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where PRJ.ProjCatId = x.ProjCatId),
				PRJ.ProjClaseId,
				ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where PRJ.ProjCatId = x.ProjCatId and PRJ.ProjClaseId = x.ProjClaseId),
				'nivel' = 
					case
						when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
						when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
						when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
					end,
				PRJ.cod_estado,
				nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado),
				PRJ.fch_estado,
				PRJ.usr_estado,
				nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
				,PRJ.marca
				,PRJ.plan_tyo
				,plan_TYO_nom = case
					when PRJ.plan_tyo = 'S' then 'Si'
					when PRJ.plan_tyo = 'N' then 'No'
					else ''
				end
				,PRJ.empresa
				,nom_empresa = case
					when PRJ.empresa = 'B' then 'Banco'
					when PRJ.empresa = 'C' then 'BBVA Seguros'
					when PRJ.empresa = 'D' then 'RCF'
					when PRJ.empresa = 'E' then 'PSA'
					when PRJ.empresa = 'F' then 'RCF/PSA'
					else ''
				end
				,PRJ.area
				,area_nom = case
					when PRJ.area = 'C' then 'Corporativo'
					when PRJ.area = 'R' then 'Regional'
					when PRJ.area = 'L' then 'Local'
					else ''
				end
				,PRJ.clas3
				,clas3_nom = case
					when PRJ.clas3 = '1' then '2013'
					when PRJ.clas3 = '2' then '2014'
					when PRJ.clas3 = '3' then '2015'
					when PRJ.clas3 = '4' then '2013 a 2014'
					when PRJ.clas3 = '5' then '2013 a 2015'
					when PRJ.clas3 = '6' then '2014 a 2015'
					else ''
				end
				,PRJ.semaphore
				,PRJ.cod_direccion
				,PRJ.cod_gerencia
				,PRJ.cod_sector
				,nom_areasoli = (case
					when PRJ.cod_gerencia is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion))
					when PRJ.cod_sector is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
						RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)) + ' » ' + 
						RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector))
				end)
				,PRJ.cod_direccion_p
				,PRJ.cod_gerencia_p
				,PRJ.cod_sector_p
				,PRJ.cod_grupo_p
				,nom_areaprop = (case
					when PRJ.cod_gerencia_p is null then 
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
					when PRJ.cod_sector_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
					when PRJ.cod_grupo_p is null then
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
					else
						RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
						' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
						' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
						' » ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
				end)
				,PRJ.avance
				,PRJ.cod_estado2
				,PRJ.fe_modif
				,PRJ.fe_inicio
				,PRJ.fe_fin
				,PRJ.fe_finreprog
				,PRJ.semaphore2
				,PRJ.sema_fe_modif
				,PRJ.sema_usuario
				,PRJ.tipo_proyecto
				,PRJ.totalhs_gerencia
				,PRJ.totalhs_sector
				,PRJ.totalreplan
				,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId)
			from
				GesPet..ProyectoIDM PRJ
			where
				(@ProjId is null or PRJ.ProjId = @ProjId) and
				(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
				(@ProjSubSId is null or PRJ.ProjSubSId = @ProjSubSId) and
				(@projnom is null or upper(rtrim(PRJ.ProjNom)) like '%' + upper(rtrim(@projnom)) + '%') and
				(@PlanTyO is null or @PlanTyO = PRJ.plan_tyo) and
				(@Activos is null or charindex(PRJ.cod_estado, RTRIM(@Activos))>0) and 
				(@flg_area is null or @flg_area = 'PROP' and (
					(@cod_nivel = 'DIRE' and PRJ.cod_direccion_p = @cod_area) or
					(@cod_nivel = 'GERE' and PRJ.cod_gerencia_p = @cod_area) or
					(@cod_nivel = 'SECT' and PRJ.cod_sector_p = @cod_area) or
					(@cod_nivel = 'GRUP' and PRJ.cod_grupo_p = @cod_area))
				)
			order by
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId
		end
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMExport to GesPetUsr
go

print 'Actualización realizada.'
go
*/