/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 12.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetAccionesGrupo'
go

if exists (select * from sysobjects where name = 'sp_GetAccionesGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAccionesGrupo
go

create procedure dbo.sp_GetAccionesGrupo
	@accionGrupoId	int=null,
	@accionGrupoNom	varchar(50)=null,
	@accionGrupoHab	char(1)='S'
as
	select 
		ag.accionGrupoId,
		ag.accionGrupoNom,
		ag.accionGrupoHab
	from 
		GesPet.dbo.AccionesGrupo ag
	where
		(@accionGrupoId is null or ag.accionGrupoId = @accionGrupoId) and
		(@accionGrupoNom is null or ag.accionGrupoNom = @accionGrupoNom) and
		(@accionGrupoHab is null or ag.accionGrupoHab = @accionGrupoHab)
	return(0)
go

grant execute on dbo.sp_GetAccionesGrupo to GesPetUsr
go

print 'Actualización realizada.'
go
