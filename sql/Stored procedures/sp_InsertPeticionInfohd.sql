/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionInfohd'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionInfohd' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionInfohd
go

create procedure dbo.sp_InsertPeticionInfohd
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_item		int,
	@info_valor		int,
	@info_orden		int
as
	insert into GesPet.dbo.PeticionInfohd (
		pet_nrointerno,
		info_id,
		info_idver,
		info_item,
		info_valor,
		info_orden)
	values (
		@pet_nrointerno,
		@info_id,
		@info_idver,
		@info_item,
		@info_valor,
		@info_orden)
go

grant execute on dbo.sp_InsertPeticionInfohd to GesPetUsr
go

print 'Actualización realizada.'
go
