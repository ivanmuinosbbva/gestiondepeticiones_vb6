/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 27.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePerfil'
go

if exists (select * from sysobjects where name = 'sp_UpdatePerfil' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePerfil
go

create procedure dbo.sp_UpdatePerfil
	@cod_perfil	char(4),
	@nom_perfil	char(30),
	@visible	char(1)=null,
	@habilitado	char(1)=null,
	@orden		int=null
as
	update 
		GesPet.dbo.Perfil
	set
		nom_perfil	= @nom_perfil,
		visible		= @visible,
		habilitado	= @habilitado,
		orden		= @orden 
	where 
		cod_perfil = @cod_perfil
	return(0)
go

grant execute on dbo.sp_UpdatePerfil to GesPetUsr
go

print 'Actualización realizada.'
go
