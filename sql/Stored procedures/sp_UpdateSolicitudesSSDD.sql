/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSolicitudesSSDD'
go

if exists (select * from sysobjects where name = 'sp_UpdateSolicitudesSSDD' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSolicitudesSSDD
go

create procedure dbo.sp_UpdateSolicitudesSSDD
	@sol_nroasignado	int,
	@sol_mask			char(1)='S',
	@jus_codigo			int=null,
	@sol_texto			varchar(255)=null,
	@pet_nroasignado	int,
	@pet_nrointerno		int,
	@tablaId			int,
	@sol_archivo		varchar(80)=null,
	@sol_eme			char(1)='N',
	@sol_diferida		char(1)='N'
as
	update 
		GesPet.dbo.SolicitudesSSDD
	set
		sol_mask		= @sol_mask,
		jus_codigo		= @jus_codigo,
		sol_texto		= @sol_texto,
		pet_nroasignado = @pet_nroasignado,
		pet_nrointerno	= @pet_nrointerno,
		tablaId			= @tablaId,
		sol_archivo		= @sol_archivo,
		sol_eme			= @sol_eme,
		sol_diferida	= @sol_diferida
	where 
		sol_nroasignado = @sol_nroasignado
	return(0)
go

grant execute on dbo.sp_UpdateSolicitudesSSDD to GesPetUsr
go

print 'Actualización realizada.'
go
