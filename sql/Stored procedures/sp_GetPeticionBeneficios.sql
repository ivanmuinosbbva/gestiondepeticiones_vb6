/*
-000- a. FJS 17.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionBeneficios'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionBeneficios' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionBeneficios
go

create procedure dbo.sp_GetPeticionBeneficios
	@pet_nrointerno		int,
	@export				char(1)='N',
	@empresa			int
as
	declare @empresa_inter		int
	declare @empresa_default	int 
	
	select @empresa_default = 1

	select @empresa_inter = isnull(ie.empid_indicador,@empresa_default)
	from
		Empresa e LEFT JOIN 
		IndicadoresEmpresa ie ON (e.empid = ie.empid_peticion)
	where e.empid = @empresa
	--where ie.empid_peticion = @empresa

	IF (@empresa_inter is not null OR @empresa_inter <> 0)
		begin 
			if (@export = 'N')
				begin
					if @pet_nrointerno = 0
						select
							0	as pet_nrointerno,
							i.indicadorId,
							i.indicadorNom,
							d.driverId,
							d.driverValor,
							d.driverNom		as subindicadorNom,
							d.driverAyuda, 
							d.driverResp,
							responsable = case
								when d.driverResp = 'SOLI'	then 'Usuario'
								when d.driverResp = 'BPE'	then 'RyGD'
							end,
							d.driverReq,
							0			as valor,
							null		as valor_nom,
							null		as valor_referencia,
							null		as audit_fe,
							null		as audit_usr,
							null		as porcentaje
						from 
							Drivers d inner join
							Indicador i on (i.indicadorId = d.indicadorId)
						where
							i.indicadorEmp = @empresa_inter
						order by
							d.driverResp	DESC,
							i.indicadorId
					else
						select
							pb.pet_nrointerno,
							i.indicadorId,
							i.indicadorNom,
							d.driverId,
							d.driverValor,
							d.driverNom		as subindicadorNom,
							d.driverAyuda,
							d.driverResp,
							responsable = case
								when d.driverResp = 'SOLI'	then 'Usuario'
								when d.driverResp = 'BPE'	then 'RyGD'
							end,
							d.driverReq,
							pb.valor,
							valor_nom = (select v.valorTexto from Valoracion v where v.valorId = pb.valor),
							valor_referencia = (select v.valorNum from Valoracion v where v.valorId = pb.valor),
							pb.audit_fe,
							pb.audit_usr,
							--porcentaje = convert(numeric(10,4), convert(numeric(10,4),d.driverValor) / (select convert(numeric(10,4),SUM(x.driverValor)) from Drivers x where x.indicadorId = i.indicadorId))
							porcentaje = convert(REAL, convert(REAL,d.driverValor) / (select convert(REAL,SUM(x.driverValor)) from Drivers x where x.indicadorId = i.indicadorId))
						from 
							Drivers d left join
							Indicador i on (i.indicadorId = d.indicadorId) inner join
							PeticionBeneficios pb on (pb.indicadorId = i.indicadorId and pb.subindicadorId = d.driverId)
						where
							pb.pet_nrointerno = @pet_nrointerno and 
							i.indicadorEmp = @empresa_inter
						order by
							d.driverResp		DESC,
							pb.indicadorId
				end
			else
				select
					pb.pet_nrointerno,
					i.indicadorId,
					i.indicadorNom,
					d.driverId,
					d.driverValor,
					d.driverNom		as subindicadorNom,
					d.driverAyuda,
					d.driverResp,
					responsable = case
						when d.driverResp = 'SOLI'	then 'Usuario'
						when d.driverResp = 'BPE'	then 'RyGD'
					end,
					d.driverReq,
					pb.valor,
					valor_nom = (select v.valorTexto from Valoracion v where v.valorId = pb.valor),
					valor_referencia = (select v.valorNum from Valoracion v where v.valorId = pb.valor),
					pb.audit_fe,
					pb.audit_usr,
					porcentaje = convert(REAL, convert(REAL,d.driverValor) / (select convert(REAL,SUM(x.driverValor)) from Drivers x where x.indicadorId = i.indicadorId))
				from 
					Drivers d left join
					Indicador i on (i.indicadorId = d.indicadorId) inner join
					PeticionBeneficios pb on (pb.indicadorId = i.indicadorId and pb.subindicadorId = d.driverId)
				where
					pb.pet_nrointerno = @pet_nrointerno and 
					i.indicadorEmp = @empresa
				order by
					i.indicadorId,
					d.driverId
			return(0)
	end
	return(0)
go

grant execute on dbo.sp_GetPeticionBeneficios to GesPetUsr
go

print 'Actualización realizada.'
go
