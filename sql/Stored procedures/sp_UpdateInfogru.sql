/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateInfogru'
go

if exists (select * from sysobjects where name = 'sp_UpdateInfogru' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateInfogru
go

create procedure dbo.sp_UpdateInfogru
	@info_gruid		int,
	@info_grunom	char(50),
	@info_gruorden	int,
	@info_gruhab	char(1)
as
	update 
		GesPet.dbo.Infogru
	set
		info_grunom		= @info_grunom,
		info_gruorden	= @info_gruorden,
		info_gruhab		= @info_gruhab
	where 
		info_gruid = @info_gruid
go

grant execute on dbo.sp_UpdateInfogru to GesPetUsr
go

print 'Actualización realizada.'
go
