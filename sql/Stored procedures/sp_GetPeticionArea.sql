use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetPeticionArea'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionArea' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionArea
go

create procedure dbo.sp_GetPeticionArea
	@pet_nroasignado	int=0,
	@modo_recurso		varchar(4) = null,
	@cod_nivel			varchar(4) = null,
	@cod_area			varchar(8) = null,		
	@cod_estado			varchar(250) = null,
	@anx_especial		varchar(1) = null,
	@agr_nrointerno		int = 0,
	@prio_ejecuc		int = 0,
	@cod_recurso		char(10)=null
as
	declare @secuencia		int
	declare @ret_status     int

	--{ add -003-
	declare @grupo_homologador		char(8)
	declare @sector_homologador		char(8)
	--}

	create table #AgrupXPetic(
		pet_nrointerno 	int)

	/*    Tabla Temporaria  */
	create table #AgrupArbol(
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)
	
	--{ add -009- a.
	create table #tablaCriterio (
		pet_nrointerno		int,
		categoria			char(6))

	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	--}

	--{ add -009- a. Esto es para obtener los agrupamientos de SDA
	declare	@agr_SDA	int
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
			
			INSERT INTO #tablaCriterio
			select 
				p.pet_nrointerno,
				categoria = case
					when p.cod_tipo_peticion in ('AUI','AUX') OR p.pet_regulatorio = 'S' then '1. REG'
					when exists (select x.pet_nrointerno from #AgrupXPeticSDA x where x.pet_nrointerno = p.pet_nrointerno) then '2. SDA'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then '3. PES'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then '4. ENG'
					when p.cod_clase in ('OPTI','CORR','SPUF','ATEN') then '6. CSI'
					when exists (
						 select count(1) 
						 from ProyectoIDM px 
						 where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
						 having count(1) > 0) OR (p.valor_impacto <> 0 OR p.valor_facilidad <>0) then '5. PRI'
					when p.cod_clase = 'SINC' then '999'
					else '7. SPR'
				end
			from Peticion p 
		end
	--}

	/* esto es si se pidio agrupamiento */
	if @agr_nrointerno<>0
		begin
			truncate table #AgrupArbol
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT
			if (@ret_status <> 0)
				begin
				   if (@ret_status = 30003)
				  select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
				   return (@ret_status)
				end
			insert into #AgrupXPetic (pet_nrointerno)
				select	AP.pet_nrointerno
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where AA.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_vigente='S'
		end /* @agr_nrointerno<>0 */

	if  @modo_recurso = 'SOLI' or
		@modo_recurso = 'REFE' or
		@modo_recurso = 'SUPE' or
		@modo_recurso = 'AUTO'
		/* pertenece a la rama solicitante */
		begin
			if @anx_especial = 'S'
				begin
					select 
						PET.pet_nrointerno,
						PET.pet_nroasignado,
						PET.cod_tipo_peticion,
						PET.titulo,
						PET.fe_pedido,
						PET.prioridad,
						PET.importancia_cod,
						nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
						PET.fe_ini_plan,
						PET.fe_fin_plan,
						PET.horaspresup,
						PET.cod_usualta,
						PET.cod_solicitante,
						PET.cod_bpar,
						PET.cod_BPE,
						nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
						nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
						PET.cod_estado,
						nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
						PET.cod_situacion,
						nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
						PET.cod_clase,
						pet_imptech = case
							when PET.pet_imptech = 'S' then 'Si'
							when PET.pet_imptech = 'N' then 'No'
						end,
						cod_area_hij='',
						nom_area_hij='',
						cod_estado_hij='',
						nom_estado_hij='',
						cod_situacion_hij='',
						nom_situacion_hij='',
						fe_ini_plan_hij=null,
						fe_fin_plan_hij=null,
						horaspresup_hij=0,
						pet_regulatorio = case 
							when PET.pet_regulatorio = 'S' then 'Si'
							when PET.pet_regulatorio = 'N' then 'No'
						end
						,''
						,''
					   ,pet_ro = isnull(PET.pet_ro,'-')
					   ,pet_riesgo = (case
							when PET.pet_ro = 'A' then 'Alta'
							when PET.pet_ro = 'M' then 'Media'
							when PET.pet_ro = 'B' then 'Baja'
							else 'No'
					   end)
						,PET.puntuacion
						,c.categoria
						,prioridadBPE = (select pc.prioridad from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno)
					from  
						GesPet..Peticion PET INNER JOIN 
						#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
					where	
						(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
						(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0) and
						((@cod_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@cod_area)) or
						(@cod_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@cod_area)) or
						(@cod_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@cod_area))) and 
						(@cod_recurso is null OR PET.pet_nrointerno IN (
							select pr.pet_nrointerno
							from PeticionRecurso pr 
							where 
								pr.pet_nrointerno = PET.pet_nrointerno and 
								pr.cod_recurso = @cod_recurso))
					UNION ALL
					 select 
						PET.pet_nrointerno,
						PET.pet_nroasignado,
						PET.cod_tipo_peticion,
						PET.titulo,
						PET.fe_pedido,
						PET.prioridad,
						PET.importancia_cod,
						nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
						PET.fe_ini_plan,
						PET.fe_fin_plan,
						PET.horaspresup,
						PET.cod_usualta,
						PET.cod_solicitante,
						PET.cod_bpar,
						PET.cod_BPE,
						nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
						nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
						PET.cod_estado,
						nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
						PET.cod_situacion,
						nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
						PET.cod_clase,
						pet_imptech = case
							when PET.pet_imptech = 'S' then 'Si'
							when PET.pet_imptech = 'N' then 'No'
						end,
						cod_area_hij='',
						nom_area_hij='',
						cod_estado_hij='',
						nom_estado_hij='',
						cod_situacion_hij='',
						nom_situacion_hij='',
						fe_ini_plan_hij=null,
						fe_fin_plan_hij=null,
						horaspresup_hij=0,
						pet_regulatorio = case 
							when PET.pet_regulatorio = 'S' then 'Si'
							when PET.pet_regulatorio = 'N' then 'No'
						end,
						'',
						'',
						pet_ro = isnull(PET.pet_ro,'-'),
						pet_riesgo = (case
							when PET.pet_ro = 'A' then 'Alta'
							when PET.pet_ro = 'M' then 'Media'
							when PET.pet_ro = 'B' then 'Baja'
							else 'No'
						end),
						PET.puntuacion,
						c.categoria,
						prioridadBPE = (select isnull(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno)
					from  
						GesPet..Peticion PET inner join
						#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
					where   
						(RTRIM(PET.cod_estado)='ANEXAD') and
						((@cod_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@cod_area)) or
						(@cod_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@cod_area)) or
						(@cod_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@cod_area))) and
						(PET.pet_nroanexada in (select XP.pet_nrointerno from GesPet..Peticion XP where PET.pet_nroanexada=XP.pet_nrointerno and @cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(XP.cod_estado),RTRIM(@cod_estado))>0))
					order by 
						prioridadBPE
					return(0)
				end
			else
				begin
					select 
						PET.pet_nrointerno,
						PET.pet_nroasignado,
						PET.cod_tipo_peticion,
						PET.titulo,
						PET.fe_pedido,
						PET.prioridad,
						PET.importancia_cod,
						nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
						PET.fe_ini_plan,
						PET.fe_fin_plan,
						PET.horaspresup,
						PET.cod_usualta,
						PET.cod_solicitante,
						PET.cod_bpar,
						PET.cod_BPE,
						nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
						nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
						PET.cod_estado,
						nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
						PET.cod_situacion,
						nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
						PET.cod_clase,
						pet_imptech = case
							when PET.pet_imptech = 'S' then 'Si'
							when PET.pet_imptech = 'N' then 'No'
						end,
						cod_area_hij='',
						nom_area_hij='',
						cod_estado_hij='',
						nom_estado_hij='',
						cod_situacion_hij='',
						nom_situacion_hij='',
						fe_ini_plan_hij=null,
						fe_fin_plan_hij=null,
						horaspresup_hij=0,
						pet_regulatorio = case 
							when PET.pet_regulatorio = 'S' then 'Si'
							when PET.pet_regulatorio = 'N' then 'No'
						end,
						'',
						'',
						pet_ro = isnull(PET.pet_ro,'-'),
						pet_riesgo = (case
							when PET.pet_ro = 'A' then 'Alta'
							when PET.pet_ro = 'M' then 'Media'
							when PET.pet_ro = 'B' then 'Baja'
							else 'No'
						end),
						PET.puntuacion,
						c.categoria,
						prioridadBPE = (select isnull(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno)
					from  
						GesPet..Peticion PET inner join
						#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
					where   
						(@pet_nroasignado=0 or @pet_nroasignado=PET.pet_nroasignado) and
						(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
						(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0) and
						((@cod_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@cod_area)) or
						(@cod_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@cod_area)) or
						(@cod_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@cod_area))) and 
						(@cod_recurso is null OR RTRIM(PET.cod_solicitante) = RTRIM(@cod_recurso))
					order by 
						prioridadBPE
					return(0)
				end
		end

	/* pertenece a la rama ejecutora CDIR CGCI CSEC */
	if  @modo_recurso = 'CGCI' or
		@modo_recurso = 'CDIR' or
		@modo_recurso = 'CSEC'
		begin

		-- add -003- Establezco el grupo homologador y el sector del grupo homologador
		select @grupo_homologador = (select cod_grupo from GesPet..Grupo where grupo_homologacion = 'H')
		select @sector_homologador = (select cod_sector from GesPet..Grupo where grupo_homologacion = 'H')

			 select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.fe_pedido,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				PET.cod_BPE,
				nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
				nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
				PET.cod_clase,
				pet_imptech = case
					when PET.pet_imptech = 'S' then 'Si'
					when PET.pet_imptech = 'N' then 'No'
				end,
				cod_area_hij=SEC.cod_sector,
				nom_area_hij = (select NOMH.nom_sector from Sector NOMH where RTRIM(NOMH.cod_sector)=RTRIM(SEC.cod_sector)),
				cod_estado_hij=SEC.cod_estado,
				nom_estado_hij = (select ESTHIJ.nom_estado from Estados ESTHIJ where RTRIM(ESTHIJ.cod_estado)=RTRIM(SEC.cod_estado)),
				cod_situacion_hij=SEC.cod_situacion,
				nom_situacion_hij=(select SITHIJ.nom_situacion from Situaciones SITHIJ where RTRIM(SEC.cod_situacion)=RTRIM(SITHIJ.cod_situacion)),
				fe_ini_plan_hij=SEC.fe_ini_plan,
				fe_fin_plan_hij=SEC.fe_fin_plan,
				horaspresup_hij=SEC.horaspresup,
				prio_ejecuc=0,
				cantnieto=(select count(1) from PeticionGrupo where RTRIM(cod_sector)=RTRIM(SEC.cod_sector) and pet_nrointerno = PET.pet_nrointerno),
				pet_regulatorio = case 
					when PET.pet_regulatorio = 'S' then 'Si'
					when PET.pet_regulatorio = 'N' then 'No'
				end,
				pet_ro = isnull(PET.pet_ro,'-'),
				pet_riesgo = (case
					when PET.pet_ro = 'A' then 'Alta'
					when PET.pet_ro = 'M' then 'Media'
					when PET.pet_ro = 'B' then 'Baja'
					else 'No'
				end),
				PET.puntuacion,
				c.categoria,
				prioridadBPE = (select isnull(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno)
			from
				GesPet..PeticionSector SEC,
				GesPet..Peticion PET inner join
				#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where
				(@pet_nroasignado = 0 or @pet_nroasignado = PET.pet_nroasignado) and
				(@agr_nrointerno = 0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
				(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(SEC.cod_estado),RTRIM(@cod_estado))>0) and
				((@cod_nivel='DIRE' and RTRIM(SEC.cod_direccion)=RTRIM(@cod_area)) or
				 (@cod_nivel='GERE' and RTRIM(SEC.cod_gerencia)=RTRIM(@cod_area)) or
				 (@cod_nivel='SECT' and RTRIM(SEC.cod_sector)=RTRIM(@cod_area))) and
				(PET.pet_nrointerno = SEC.pet_nrointerno)
				and
				(
				(@modo_recurso = 'CSEC' and @cod_nivel = 'SECT' and @cod_area <> @sector_homologador) or
				(@modo_recurso = 'CSEC' and @cod_nivel = 'SECT' and @cod_area = @sector_homologador and 
				 ((select count(1) from PeticionGrupo where RTRIM(cod_sector)=RTRIM(SEC.cod_sector) and pet_nrointerno = PET.pet_nrointerno) < 1) or 
				 (@grupo_homologador not in (select cod_grupo from GesPet..PeticionGrupo where pet_nrointerno = PET.pet_nrointerno and cod_sector = SEC.cod_sector and cod_grupo = @grupo_homologador) and (select count(1) from PeticionGrupo where RTRIM(cod_sector)=RTRIM(SEC.cod_sector) and pet_nrointerno = PET.pet_nrointerno) = 1) or
				 (@grupo_homologador in (select cod_grupo from GesPet..PeticionGrupo where pet_nrointerno = PET.pet_nrointerno and cod_sector = SEC.cod_sector and cod_grupo = @grupo_homologador) and (select count(1) from PeticionGrupo where RTRIM(cod_sector)=RTRIM(SEC.cod_sector) and pet_nrointerno = PET.pet_nrointerno) > 1))
				)
				 and (@cod_recurso is null OR PET.pet_nrointerno IN (
						select pr.pet_nrointerno
						from PeticionRecurso pr 
						where 
							pr.pet_nrointerno = PET.pet_nrointerno and 
							pr.cod_recurso = @cod_recurso))
			order by 
				prioridadBPE
			return(0)
		end

	/* pertenece a la rama ejecutora CGRU */
	if  @modo_recurso = 'CGRU' or 
		@modo_recurso = 'ANAL'
		begin
			 select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.fe_pedido,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				PET.cod_BPE,
				nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
				nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
				PET.cod_clase,
				pet_imptech = case
					when PET.pet_imptech = 'S' then 'Si'
					when PET.pet_imptech = 'N' then 'No'
				end,
				cod_area_hij=GRU.cod_grupo,
				nom_area_hij = (select NOMH.nom_grupo from Grupo NOMH where RTRIM(NOMH.cod_grupo)=RTRIM(GRU.cod_grupo)),
				cod_estado_hij=GRU.cod_estado,
				nom_estado_hij = (select ESTHIJ.nom_estado from Estados ESTHIJ where RTRIM(ESTHIJ.cod_estado)=RTRIM(GRU.cod_estado)),
				cod_situacion_hij=GRU.cod_situacion,
				nom_situacion_hij=(select SITHIJ.nom_situacion from Situaciones SITHIJ where RTRIM(GRU.cod_situacion)=RTRIM(SITHIJ.cod_situacion)),
				fe_ini_plan_hij=GRU.fe_ini_plan,
				fe_fin_plan_hij=GRU.fe_fin_plan,
				horaspresup_hij=GRU.horaspresup,
				GRU.prio_ejecuc,
				cantnieto=0,
				pet_regulatorio = case 
					when PET.pet_regulatorio = 'S' then 'Si'
					when PET.pet_regulatorio = 'N' then 'No'
				end,
				'',
				'',
				pet_ro = isnull(PET.pet_ro,'-'),
				pet_riesgo = (case
					when PET.pet_ro = 'A' then 'Alta'
					when PET.pet_ro = 'M' then 'Media'
					when PET.pet_ro = 'B' then 'Baja'
					else 'No'
				end),
				PET.puntuacion,
				c.categoria,
				prioridadBPE = (select isnull(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno)
			from
				GesPet..PeticionGrupo GRU,
				GesPet..Peticion PET inner join
				#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where 
				PET.pet_nrointerno = GRU.pet_nrointerno and 
				(@pet_nroasignado=0 or @pet_nroasignado=PET.pet_nroasignado) and
				(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
				(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or 
					(charindex(RTRIM(GRU.cod_estado),RTRIM(@cod_estado)) > 0 or 
						(@cod_estado = 'PENDI0' and GRU.cod_estado IN ('ESTIMA','ESTIOK','PLANIF') and exists (select px.pet_nrointerno from PeticionPlancab px where px.pet_nrointerno = PET.pet_nrointerno and px.prioridad <> 0)) OR
						(@cod_estado = 'PENDI1' and PET.cod_tipo_peticion <> 'PRO' and NOT (PET.cod_clase is null or PET.cod_clase = 'SINC') and GRU.cod_estado IN ('ESTIMA','ESTIOK','PLANIF') and not exists(select px.pet_nrointerno from PeticionPlancab px where px.pet_nrointerno = PET.pet_nrointerno and px.prioridad <> 0)) OR
						(@cod_estado = 'PENDI2' and PET.cod_tipo_peticion = 'PRO' and (PET.cod_clase is null or PET.cod_clase = 'SINC') and GRU.cod_estado IN ('ESTIMA','ESTIOK','PLANIF')) OR
						(@cod_estado = 'PENDI3' and GRU.cod_estado = 'PLANOK')
					)
				) 
				and 
				((@cod_nivel='DIRE' and RTRIM(GRU.cod_direccion)=RTRIM(@cod_area)) or
				(@cod_nivel='GERE' and RTRIM(GRU.cod_gerencia)=RTRIM(@cod_area)) or
				(@cod_nivel='SECT' and RTRIM(GRU.cod_sector)=RTRIM(@cod_area)) or
				(@cod_nivel='GRUP' and RTRIM(GRU.cod_grupo)=RTRIM(@cod_area))) and 
				(@cod_recurso is null OR PET.pet_nrointerno IN (
					select pr.pet_nrointerno
					from PeticionRecurso pr 
					where 
						pr.pet_nrointerno = PET.pet_nrointerno and 
						pr.cod_recurso = @cod_recurso))
			order by 
				PET.prioridadBPE
			return(0)
		end

	if @modo_recurso = 'ADMI' or
	   @modo_recurso = 'SADM'
		begin
			 select PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.fe_pedido,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				PET.cod_BPE,
				nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
				nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion),
				PET.cod_clase,
				pet_imptech = case
					when PET.pet_imptech = 'S' then 'Si'
					when PET.pet_imptech = 'N' then 'No'
				end,
				cod_area_hij='',
				nom_area_hij='',
				cod_estado_hij='',
				nom_estado_hij='',
				cod_situacion_hij='',
				nom_situacion_hij='',
				fe_ini_plan_hij=null,
				fe_fin_plan_hij=null,
				horaspresup_hij=0,
				cantnieto=(select count(1) from PeticionSector where  pet_nrointerno = PET.pet_nrointerno),
				pet_regulatorio = case 
					when PET.pet_regulatorio = 'S' then 'Si'
					when PET.pet_regulatorio = 'N' then 'No'
				end,
				'',
				'',
				pet_ro = isnull(PET.pet_ro,'-'),
				pet_riesgo = (case
					when PET.pet_ro = 'A' then 'Alta'
					when PET.pet_ro = 'M' then 'Media'
					when PET.pet_ro = 'B' then 'Baja'
					else 'No'
				end),
				PET.puntuacion,
				c.categoria,
				prioridadBPE = (select isnull(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno)
			from 
				GesPet..Peticion PET inner join
				#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where 
				(@pet_nroasignado=0 or @pet_nroasignado=PET.pet_nroasignado) and
				(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
				(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0)  and 
				(@cod_recurso is null OR PET.pet_nrointerno IN (
					select pr.pet_nrointerno
					from PeticionRecurso pr 
					where 
						pr.pet_nrointerno = PET.pet_nrointerno and 
						pr.cod_recurso = @cod_recurso))
			order by 
				prioridadBPE
			return(0)
		end

	return(0)
go

grant execute on dbo.sp_GetPeticionArea to GesPetUsr
go

print 'Actualización finalizada.'
go
