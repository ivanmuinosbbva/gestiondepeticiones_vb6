use GesPet
go
print 'sp_DeleteTareaSector'
go
if exists (select * from sysobjects where name = 'sp_DeleteTareaSector' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteTareaSector
go
create procedure dbo.sp_DeleteTareaSector 
               @cod_tarea       char(8)=null, 
               @cod_sector          char(8)=null 
as 
 
if  (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='') and 
    (@cod_tarea is null or RTRIM(@cod_tarea) is null or RTRIM(@cod_tarea)='') 
begin 
    raiserror  30010 'No se puede invocar con dos parametros nulos'  
    select 30010 as ErrCode , 'No se puede invocar con dos parametros nulos'  as ErrDesc 
return (30010) 
end 
 
delete  GesPet..TareaSector 
where   (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) and  
    (@cod_tarea is null or RTRIM(@cod_tarea) is null or RTRIM(@cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
 
return(0) 
go



grant execute on dbo.sp_DeleteTareaSector to GesPetUsr 
go


