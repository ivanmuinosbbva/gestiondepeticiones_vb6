/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 09.06.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertIndicadoresEmpresa'
go

if exists (select * from sysobjects where name = 'sp_InsertIndicadoresEmpresa' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertIndicadoresEmpresa
go

create procedure dbo.sp_InsertIndicadoresEmpresa
	@empid_peticion		int,
	@empid_indicador	int
as
	insert into GesPet.dbo.IndicadoresEmpresa (
		empid_peticion,
		empid_indicador)
	values (
		@empid_peticion,
		@empid_indicador)
go

grant execute on dbo.sp_InsertIndicadoresEmpresa to GesPetUsr
go

print 'Actualización realizada.'
go
