use GesPet
go
print 'sp_GetEscalamiento'
go
if exists (select * from sysobjects where name = 'sp_GetEscalamiento' and sysstat & 7 = 4)
drop procedure dbo.sp_GetEscalamiento
go
create procedure dbo.sp_GetEscalamiento 
    @evt_alcance char(3)=null, 
    @cod_estado char(6)=null 
as 
    select  evt_alcance, 
        cod_estado, 
        pzo_desde, 
        pzo_hasta, 
        cod_accion, 
        fec_activo 
    from    GesPet..Escalamientos 
    where   (RTRIM(@evt_alcance) is null or RTRIM(@evt_alcance)='' or RTRIM(evt_alcance)=RTRIM(@evt_alcance)) and 
        (RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or RTRIM(cod_estado)=RTRIM(@cod_estado)) 
 
return(0) 
go



grant execute on dbo.sp_GetEscalamiento to GesPetUsr 
go


