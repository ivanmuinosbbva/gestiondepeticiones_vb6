/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 21.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateInterfazTeraDet'
go

if exists (select * from sysobjects where name = 'sp_UpdateInterfazTeraDet' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateInterfazTeraDet
go

create procedure dbo.sp_UpdateInterfazTeraDet
	@dbName			varchar(30),
	@objname		varchar(50),
	@objfield		varchar(50),
	@objfieldhab	char(1),
	@objfielddesc	varchar(255)
as
	update 
		GesPet.dbo.InterfazTeraDet
	set
		objfieldhab		= @objfieldhab,
		objfielddesc	= @objfielddesc,
		objultact		= getdate()
	where 
		LTRIM(RTRIM(dbName)) = LTRIM(RTRIM(@dbName)) and
		LTRIM(RTRIM(objname)) = LTRIM(RTRIM(@objname)) and
		LTRIM(RTRIM(objfield)) = LTRIM(RTRIM(@objfield))
go

grant execute on dbo.sp_UpdateInterfazTeraDet to GesPetUsr
go

print 'Actualización realizada.'
go
