use GesPet
go
print 'sp_DeleteProyectoBalanceDet'
go
if exists (select * from sysobjects where name = 'sp_DeleteProyectoBalanceDet' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteProyectoBalanceDet
go
create procedure dbo.sp_DeleteProyectoBalanceDet
	@prj_nrointerno	int=0,
	@cod_BalanceRubro	char(8)=null,
	@cod_BalanceSubRubro	char(8)=null,
	@secuencia		int

as 

begin tran
delete from ProyectoBalanceDet 
where	prj_nrointerno             = @prj_nrointerno and
	RTRIM(cod_BalanceRubro)    = RTRIM(@cod_BalanceRubro) and 
	RTRIM(cod_BalanceSubRubro) = RTRIM(@cod_BalanceSubRubro) and 
	secuencia                  = @secuencia

commit tran
return(0) 
go

grant execute on dbo.sp_DeleteProyectoBalanceDet to GesPetUsr 
go
