/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */
 
/*
-000- a. FJS 02.10.2009 - 
-001- a. FJS 20.11.2009 - Se agrega el c�digo para manejar el hecho de que no pueda modificarse el nombre de un archivo si ya ha sido ingresado con otro c�digo.
-001- b. FJS 20.11.2009 - Siempre que es modificado un archivo, se marca para que sea reenviado a HOST si ya fu� enviado alguna vez.
-002- a. FJS 27.12.2010 - Se agrega la columna dsn_nomprod (nombre del archivo de producci�n).
-003- a. FJS 30.08.2012 - Pet. N� 51089
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT001'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT001' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT001
go

create procedure dbo.sp_UpdateHEDT001
	@dsn_id			char(8)=null,
	@dsn_nom		char(50)=null,
	@dsn_desc		char(255)=null,
	@cod_recurso	char(10)='',
	@dsn_enmas		char(1)=null,
	@dsn_nomrut		char(8)=null,
	@dsn_appid		char(30)=null,
	@dsn_nomprod	char(100)=null,			-- add -002- a.
	@dsn_cpy		char(1),
	@dsn_cpybbl		char(50),
	@dsn_cpynom		char(50)
as
	--{ add -001- a.
	declare @aux_dsn_nom	char(50)
	declare @estado_ant		char(1)

	select @estado_ant = (select x.estado from GesPet..HEDT001 x where RTRIM(x.dsn_id) = RTRIM(@dsn_id))

	if exists (select x.dsn_nom from GesPet..HEDT001 x where RTRIM(x.dsn_id) <> RTRIM(@dsn_id) and LTRIM(RTRIM(x.dsn_nom)) = LTRIM(RTRIM(@dsn_nom)))
	--if exists (select x.dsn_nom from GesPet..HEDT001 x where LTRIM(RTRIM(x.dsn_nom)) = LTRIM(RTRIM(@dsn_nom)))
		begin 
			raiserror 30011 'El nombre del archivo ya existe. Revise.'
			select 30011 as ErrCode , 'El nombre del archivo ya existe. Revise.' as ErrDesc
			return (30011)
		end
	else
		begin
	--}
			-- Actualiza los datos editables del archivo
			update 
				GesPet.dbo.HEDT001
			set
				dsn_nom       = @dsn_nom,
				dsn_desc      = @dsn_desc,
				dsn_enmas     = @dsn_enmas,
				dsn_nomrut	  = @dsn_nomrut,
				app_id		  = @dsn_appid,
				dsn_nomprod	  = @dsn_nomprod,	-- add -002- a.
				dsn_feult	  = getdate(),
				dsn_userid	  = @cod_recurso,	--SUSER_NAME(),
				dsn_cpy		  = @dsn_cpy,
				dsn_cpybbl	  = @dsn_cpybbl,
				dsn_cpynom    = @dsn_cpynom
			where 
				(dsn_id = @dsn_id or @dsn_id is null)
			
			-- Vuelve a marcar el mismo para que sea reenviado a HOST por haber sido modificado
			if @estado_ant = 'C'
				begin
					update GesPet.dbo.HEDT001
					set estado = 'B'
					where (dsn_id = @dsn_id or @dsn_id is null)
				end
		end
go

grant execute on dbo.sp_UpdateHEDT001 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
