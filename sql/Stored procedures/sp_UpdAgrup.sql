/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_UpdAgrup'
go
if exists (select * from sysobjects where name = 'sp_UpdAgrup' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdAgrup
go
create procedure dbo.sp_UpdAgrup 
	@agr_nrointerno		int=0,
	@agr_nropadre		int=0,
	@agr_titulo  		char(50)='',
	@agr_vigente  		char(1)='',
	@cod_direccion 		char(8)='',
	@cod_gerencia  		char(8)='',
	@cod_sector   		char(8)='',
	@cod_grupo   		char(8)='',
	@cod_usualta		char(10)='',
	@agr_visibilidad	char(3)='',
	@agr_actualiza		char(3)='',
	@agr_admpet  		char(1)=''
as 

update Agrup set 
    agr_nropadre	= @agr_nropadre,  
    agr_titulo		= @agr_titulo, 
    agr_vigente		= @agr_vigente,   
    cod_direccion	= @cod_direccion, 
    cod_gerencia	= @cod_gerencia, 
    cod_sector		= @cod_sector, 
    cod_grupo		= @cod_grupo, 
    cod_usualta		= @cod_usualta, 
    agr_visibilidad	= @agr_visibilidad,         
    agr_actualiza	= @agr_actualiza,         
    agr_admpet	= @agr_admpet,         
    audit_user = SuseR_NAME(),   
    audit_date = getdate()   
	where  agr_nrointerno = @agr_nrointerno   
 
return(0)   
go



grant execute on dbo.sp_UpdAgrup to GesPetUsr 
go
