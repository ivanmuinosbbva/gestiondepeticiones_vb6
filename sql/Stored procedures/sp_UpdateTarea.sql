use GesPet
go
print 'sp_UpdateTarea'
go
if exists (select * from sysobjects where name = 'sp_UpdateTarea' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateTarea
go
create procedure dbo.sp_UpdateTarea 
               @cod_tarea       char(8), 
               @nom_tarea       char(50), 
               @flg_habil       char(1) 
as 
 
if exists (select cod_tarea from Tarea where RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
begin 
    update Tarea 
    set nom_tarea = @nom_tarea, 
        flg_habil = @flg_habil 
    where RTRIM(cod_tarea) = RTRIM(@cod_tarea) 
end 
else 
begin 
      insert into Tarea 
             (cod_tarea, nom_tarea, flg_habil) 
      values 
             (@cod_tarea, @nom_tarea, @flg_habil) 
end 
return(0) 
go



grant execute on dbo.sp_UpdateTarea to GesPetUsr 
go


