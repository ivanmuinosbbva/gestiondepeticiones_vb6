use GesPet
go
print 'sp_GetBalanceSubRubroXT'
go
if exists (select * from sysobjects where name = 'sp_GetBalanceSubRubroXT' and sysstat & 7 = 4)
drop procedure dbo.sp_GetBalanceSubRubroXT
go
create procedure dbo.sp_GetBalanceSubRubroXT
	@cod_BalanceRubro	char(8)=null,
	@cod_BalanceSubRubro	char(8)=null,
	@flg_habil		char(1)=null

as
begin
	select
		BSU.cod_BalanceRubro,
		BSU.cod_BalanceSubRubro,
		BSU.nom_BalanceSubRubro,
		BSU.nom_largo,
		BSU.signo,
		BSU.secuencia,
		BSU.flg_habil

	from
		BalanceSubRubro BSU
	where
		(@cod_BalanceRubro is null or RTRIM(@cod_BalanceRubro) is null or RTRIM(@cod_BalanceRubro)='' or RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro)) and
		(@cod_BalanceSubRubro is null or RTRIM(@cod_BalanceSubRubro) is null or RTRIM(@cod_BalanceSubRubro)='' or RTRIM(cod_BalanceSubRubro) = RTRIM(@cod_BalanceSubRubro)) and
		(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil))
	order by BSU.cod_BalanceRubro,BSU.secuencia ASC
end
return(0)
go

grant execute on dbo.sp_GetBalanceSubRubroXT to GesPetUsr
go

