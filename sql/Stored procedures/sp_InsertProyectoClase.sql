/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 04.11.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoClase'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoClase
go

create procedure dbo.sp_InsertProyectoClase
	@ProjCatId		int,
	@ProjClaseId	int,
	@ProjClaseNom	char(100)
as
	insert into GesPet.dbo.ProyectoClase (
		ProjCatId,
		ProjClaseId,
		ProjClaseNom)
	values (
		@ProjCatId,
		@ProjClaseId,
		@ProjClaseNom)
	return(0)
go

grant execute on dbo.sp_InsertProyectoClase to GesPetUsr
go

print 'Actualización realizada.'
go
