/*
-000- a. FJS 12.05.2009 - Nuevo SP para manejo de Aplicativos por Grupo ejecutor
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertGrupoAplicativo'
go

if exists (select * from sysobjects where name = 'sp_InsertGrupoAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertGrupoAplicativo
go

create procedure dbo.sp_InsertGrupoAplicativo
	@cod_grupo	char(8)=null,
	@app_id		char(30)=null
as
	insert into GesPet..GrupoAplicativo (
		cod_grupo,
		app_id)
	values (
		@cod_grupo,
		@app_id)
go

grant execute on dbo.sp_InsertGrupoAplicativo to GesPetUsr
go

print 'Actualización realizada.'
