/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.01.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMHoras'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMHoras' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMHoras
go

create procedure dbo.sp_DeleteProyectoIDMHoras
	@ProjId		int,
	@ProjSubId	int,
	@ProjSubSId	int,
	@cod_nivel	char(3),
	@cod_area	char(8)
as
	declare @cant_horas		int

	select @cant_horas = cant_horas
	from GesPet..ProyectoIDMHoras
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		cod_nivel = @cod_nivel and
		cod_area = @cod_area

	delete from
		GesPet..ProyectoIDMHoras
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		cod_nivel = @cod_nivel and
		cod_area = @cod_area

	if @cod_nivel = 'GER'
		update GesPet..ProyectoIDM
		set totalhs_gerencia = totalhs_gerencia - @cant_horas
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	else
		update GesPet..ProyectoIDM
		set totalhs_sector = totalhs_sector - @cant_horas
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
go

grant execute on dbo.sp_DeleteProyectoIDMHoras to GesPetUsr
go

print 'Actualización realizada.'
go
