/*
	-001- a. FJS 20.04.2015 - Nuevo: se agrega un nuevo atributo para sectores, que indica si el mismo es v�lido para usar como sector solicitante en peticiones especiales.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSectorXt'
go

if exists (select * from sysobjects where name = 'sp_GetSectorXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSectorXt
go

create procedure dbo.sp_GetSectorXt 
	@cod_sector		char(8)=null, 
	@cod_gerencia	char(8)=null, 
	@cod_direccion	char(8)=null, 
	@flg_habil		char(1)=null 
as 
	begin 
		select 
			Gr.cod_sector, 
			Gr.nom_sector, 
			Gr.cod_gerencia, 
			Ge.nom_gerencia, 
			Ge.cod_direccion, 
			nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)), 
			Gr.cod_abrev, 
			Gr.flg_habil, 
			Gr.es_ejecutor, 
			Gr.cod_bpar, 
			nom_bpar = isnull((select R.nom_recurso from Recurso R where RTRIM(R.cod_recurso) = RTRIM(Gr.cod_bpar)),''), 
			Gr.abrev_sector,
			Gr.soli_hab				-- add -001- a.
		from 
			Sector Gr, Gerencia Ge 
		where 
			(@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(Gr.cod_sector) = RTRIM(@cod_sector)) and 
			(@cod_gerencia is null or RTRIM(@cod_gerencia) is null or RTRIM(@cod_gerencia)='' or RTRIM(Gr.cod_gerencia) = RTRIM(@cod_gerencia)) and 
			(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(Gr.flg_habil) = RTRIM(@flg_habil)) and 
			(RTRIM(Gr.cod_gerencia) = RTRIM(Ge.cod_gerencia)) and 
			(@cod_direccion is null or RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)='' or RTRIM(Ge.cod_direccion) = RTRIM(@cod_direccion)) 
		order by  
			Ge.cod_direccion, 
			Gr.cod_gerencia, 
			Gr.cod_sector, 
			Gr.flg_habil
	end 
	return(0)   
go

grant execute on dbo.sp_GetSectorXt to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go
