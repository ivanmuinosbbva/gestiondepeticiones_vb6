/*
-000- a. FJS 17.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetValoracion'
go

if exists (select * from sysobjects where name = 'sp_GetValoracion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetValoracion
go

create procedure dbo.sp_GetValoracion
	@valorDriver		int=null,
	@valorId			int=null
as
	select 
		v.valorId,
		v.valorTexto,
		v.valorNum,
		v.valorOrden,
		v.valorHab,
		v.valorDriver
	from 
		Valoracion v
	where
		(@valorDriver is null or v.valorDriver = @valorDriver) and
		(@valorId is null or v.valorId = @valorId)
	order by 
		v.valorDriver,
		v.valorOrden
	
	return(0)
go

grant execute on dbo.sp_GetValoracion to GesPetUsr
go

print 'Actualización realizada.'
go
