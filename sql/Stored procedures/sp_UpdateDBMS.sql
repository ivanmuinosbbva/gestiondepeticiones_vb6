/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateDBMS'
go

if exists (select * from sysobjects where name = 'sp_UpdateDBMS' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateDBMS
go

create procedure dbo.sp_UpdateDBMS
	@dbmsId		int,
	@dbmsNom	varchar(50)
as
	update 
		GesPet.dbo.DBMS
	set
		dbmsNom = LTRIM(RTRIM(@dbmsNom))
	where 
		dbmsId = @dbmsId
go

grant execute on dbo.sp_UpdateDBMS to GesPetUsr
go

print 'Actualización realizada.'
go
