/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMNovedades2'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMNovedades2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMNovedades2
go

create procedure dbo.sp_GetProyectoIDMNovedades2
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null,
	@mem_fecha		datetime,
	@mem_campo		char(10)
as
	select 
		n.ProjId,
		n.ProjSubId,
		n.ProjSubSId,
		n.mem_fecha,
		n.mem_campo,
		n.mem_texto,
		n.cod_usuario,
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = n.cod_usuario),
		n.mem_accion,		-- Acci�n realizada (UPD: actualizaci�n; DEL: borrado)
		nom_accion = case
			when n.mem_accion = 'UPD' then 'Actualizaci�n'
			when n.mem_accion = 'DEL' then 'Borrado'
			else 'Agregado'
		end,
		n.mem_fecha2,		-- Fecha en que se borr� o actualiz� el dato
		n.cod_usuario2,		-- Usuario que realiz� este cambio
		nom_usuario2 = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = n.cod_usuario2)
	from 
		GesPet.dbo.ProyectoIDMNovedades n
	where
		n.ProjId = @ProjId and
		n.ProjSubId = @ProjSubId and
		n.ProjSubSId = @ProjSubSId and
		(@mem_fecha is null or n.mem_fecha = @mem_fecha) and
		(@mem_campo is null or n.mem_campo = @mem_campo) and 
		mem_secuencia = 0
	group by
		n.ProjId,
		n.ProjSubId,
		n.ProjSubSId,
		n.mem_fecha,
		n.mem_campo,
		n.mem_texto,
		n.cod_usuario,
		n.mem_accion,
		n.mem_fecha2,
		n.cod_usuario2
	order by
		n.ProjId,
		n.ProjSubId,
		n.ProjSubSId,
		n.mem_campo,
		n.mem_fecha	desc
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMNovedades2 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
