/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 24.06.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertAccionesIDM'
go

if exists (select * from sysobjects where name = 'sp_InsertAccionesIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertAccionesIDM
go

create procedure dbo.sp_InsertAccionesIDM
	@cod_accion	char(8),
	@nom_accion	char(100)
as
	insert into GesPet.dbo.AccionesIDM (
		cod_accion,
		nom_accion)
	values (
		@cod_accion,
		@nom_accion)
go

grant execute on dbo.sp_InsertAccionesIDM to GesPetUsr
go

print 'Actualización realizada.'
go
