/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateIndicador'
go

if exists (select * from sysobjects where name = 'sp_UpdateIndicador' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateIndicador
go

create procedure dbo.sp_UpdateIndicador
	@indicadorId	int,
	@indicadorNom	char(60),
	@indicadorHab	char(1)
as
	update 
		GesPet.dbo.Indicador
	set
		indicadorNom = @indicadorNom,
		indicadorHab = @indicadorHab
	where 
		indicadorId = @indicadorId
go

grant execute on dbo.sp_UpdateIndicador to GesPetUsr
go

print 'Actualización realizada.'
go
