/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteHEDT002'
go

if exists (select * from sysobjects where name = 'sp_DeleteHEDT002' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHEDT002
go

create procedure dbo.sp_DeleteHEDT002
	@dsn_id	char(8),
	@cpy_id	char(8)=null
as
	begin
		delete from GesPet..HEDT002
		where
			dsn_id = @dsn_id  and
			(cpy_id = @cpy_id or @cpy_id is null)

		delete from GesPet..HEDT003
		where 
			dsn_id = @dsn_id and
			(cpy_id = @cpy_id or @cpy_id is null)
	end
go

grant execute on dbo.sp_DeleteHEDT002 to GesPetUsr
go

print 'Actualización realizada.'
