/*
-000- a. FJS 10.03.2008 - Nuevo SP para obtener la carga de horas en peticiones por recursos seg�n perfil del mismo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHorasReales'
go

if exists (select * from sysobjects where name = 'sp_GetHorasReales' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHorasReales
go

create procedure dbo.sp_GetHorasReales
    @agrup              char(3)='NULL',        -- SEC: Devuelve solo Sectores / GRU: Devuelve Sectores y Grupos / OUT: Devuelve todo lo que no esta inclu�do expl�citamente en la petici�n
    @pet_nrointerno     int=0,
    @pet_sect           char(8),
    @pet_grup           char(8)
as
	if @agrup = 'SEC'
		begin
			select SUM(CONVERT(REAL, HRS.horas)/CONVERT(REAL,60)) as Horas
			from
				GesPet..HorasTrabajadas HRS inner join
				GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso left join
				GesPet..PeticionSector PET on HRS.pet_nrointerno = PET.pet_nrointerno and REC.cod_sector = PET.cod_sector
			where
				HRS.pet_nrointerno = @pet_nrointerno and
				(RTRIM(REC.cod_sector) = RTRIM(@pet_sect) or @pet_sect is null) and
				(RTRIM(REC.cod_grupo) = RTRIM(@pet_grup) or @pet_grup is null)
		end
	if @agrup = 'GRU'
		begin
			select SUM(CONVERT(REAL, HRS.horas)/CONVERT(REAL,60)) as Horas
			from
				GesPet..HorasTrabajadas HRS inner join
				GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso left join
				GesPet..PeticionGrupo PET on HRS.pet_nrointerno = PET.pet_nrointerno and REC.cod_sector = PET.cod_sector and REC.cod_grupo = PET.cod_grupo
			where
				HRS.pet_nrointerno = @pet_nrointerno and
				(RTRIM(REC.cod_sector) = RTRIM(@pet_sect) or @pet_sect is null) and
				(RTRIM(REC.cod_grupo) = RTRIM(@pet_grup) or @pet_grup is null)
		end
	return(0)
go

grant Execute on dbo.sp_GetHorasReales to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
