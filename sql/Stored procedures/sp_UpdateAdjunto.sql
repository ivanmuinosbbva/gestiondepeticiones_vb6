use GesPet
go
print 'sp_UpdateAdjunto'
go
if exists (select * from sysobjects where name = 'sp_UpdateAdjunto' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateAdjunto
go
create procedure dbo.sp_UpdateAdjunto
	@pet_nrointerno	int,
	@adj_file	char(50),
	@adj_texto	char(250)=''
as

if not exists (select adj_file from Adjunto where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file))
begin
	raiserror 30001 'El archivo no esta declarado como adjunto.'
	select 30001 as ErrCode , 'El archivo no esta declarado como adjunto.' as ErrDesc
	return (30001)
end
else
begin
	update Adjunto
		set adj_texto=@adj_texto
		where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file)
end
return(0)
go

grant execute on dbo.sp_UpdateAdjunto to GesPetUsr
go
