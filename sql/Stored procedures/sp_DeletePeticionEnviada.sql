/*
-000- a. FJS 15.02.2010 - Para eliminar las peticiones de la bandeja diaria. 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionEnviada'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionEnviada' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionEnviada
go

create procedure dbo.sp_DeletePeticionEnviada  
	@pet_nrointerno		int,
	@cod_sector			char(8),
	@cod_grupo			char(8)
as
	delete 
	from GesPet..PeticionChangeMan
	where 
		pet_nrointerno = @pet_nrointerno and
		ltrim(rtrim(pet_sector)) = rtrim(@cod_sector) and
		ltrim(rtrim(pet_grupo)) = rtrim(@cod_grupo)

	delete 
	from GesPet..PeticionEnviadas
	where 
		pet_nrointerno = @pet_nrointerno and
		ltrim(rtrim(pet_sector)) = rtrim(@cod_sector) and
		ltrim(rtrim(pet_grupo)) = rtrim(@cod_grupo)
	return(0)
go

grant Execute  on dbo.sp_DeletePeticionEnviada to GesPetUsr 
go

print 'Actualización realizada.'