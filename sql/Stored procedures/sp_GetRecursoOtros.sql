/*
-000- a. FJS 25.02.2013 - Nuevo SP para devolver los recursos de vinculo "F: FSW/Otros".
-001- a. FJS 15.05.2013 - Se lee de una nueva tabla.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoOtros'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoOtros' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoOtros
go

create procedure dbo.sp_GetRecursoOtros
	@cod_query		char(10) = null, 
	@modo_query		char(2)  = 'R', 
	@flg_estado		char(1)  = null
as 
	select 
		r.cod_fab,
		r.nom_fab,
		r.cod_gerencia, 
		r.cod_direccion, 
		r.cod_sector, 
		r.cod_grupo, 
		r.estado_fab
	from 
		GesPet..Fabrica r
	where
		(@cod_query is null or RTRIM(@cod_query)='') or 
		(@modo_query = 'R' and RTRIM(r.cod_fab) = RTRIM(@cod_query)) or
		(@modo_query = 'DI' and RTRIM(r.cod_direccion) = RTRIM(@cod_query)) or 
		(@modo_query = 'GE' and RTRIM(r.cod_gerencia) = RTRIM(@cod_query)) or 
		(@modo_query = 'GR' and RTRIM(r.cod_sector) = RTRIM(@cod_query)) or 
		(@modo_query = 'SU' and RTRIM(r.cod_grupo) = RTRIM(@cod_query)) and 
		(RTRIM(@flg_estado) is null or RTRIM(@flg_estado)='' or RTRIM(r.estado_fab) = RTRIM(@flg_estado))
	return(0)
go

grant execute on dbo.sp_GetRecursoOtros to GesPetUsr
go

print 'Actualización realizada.'
go
