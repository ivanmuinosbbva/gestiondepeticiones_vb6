/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 23.07.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMHitosField'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMHitosField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMHitosField
go

create procedure dbo.sp_UpdateProyectoIDMHitosField
	@ProjId				int,
	@ProjSubId			int,
	@ProjSubSId			int,
	@hit_nrointerno		int,
	@campo				char(80),
	@valortxt			char(255),
	@valordate			smalldatetime,
	@valornum			int
as
	if UPPER(RTRIM(@campo))='HITO_NOMBRE'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_nombre = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno

	if UPPER(RTRIM(@campo))='HITO_FE_INI'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_fe_ini = @valordate
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno

	if UPPER(RTRIM(@campo))='HITO_FE_FIN'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_fe_fin = @valordate
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='HITO_DESCRIPCION'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_descripcion = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='HITO_ESTADO'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_estado = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='COD_NIVEL'
		update GesPet.dbo.ProyectoIDMHitos
		set cod_nivel = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='COD_AREA'
		update GesPet.dbo.ProyectoIDMHitos
		set cod_area = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='HITO_FECHA'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_fecha = @valordate
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='HITO_USUARIO'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_usuario = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='HST_NROINTERNO'
		update GesPet.dbo.ProyectoIDMHitos
		set hst_nrointerno = @valornum
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno	
	
	if UPPER(RTRIM(@campo))='HIT_ORDEN'
		update GesPet.dbo.ProyectoIDMHitos
		set hit_orden = @valornum
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno	

	if UPPER(RTRIM(@campo))='HITO_EXPOS'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_expos = LTRIM(RTRIM(@valortxt))
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno
	
	if UPPER(RTRIM(@campo))='HITO_ID'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_id = @valornum
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and hit_nrointerno = @hit_nrointerno	
go

grant execute on dbo.sp_UpdateProyectoIDMHitosField to GesPetUsr
go

print 'Actualización realizada.'
go


/*
	if UPPER(RTRIM(@campo))='HITO_FE_FIN'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_fe_fin = @valordate
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
	
	if UPPER(RTRIM(@campo))='HITO_DESCRIPCION'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_descripcion = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
	
	if UPPER(RTRIM(@campo))='HITO_ESTADO'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_estado = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
	
	if UPPER(RTRIM(@campo))='COD_NIVEL'
		update GesPet.dbo.ProyectoIDMHitos
		set cod_nivel = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
	
	if UPPER(RTRIM(@campo))='COD_AREA'
		update GesPet.dbo.ProyectoIDMHitos
		set cod_area = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
	
	if UPPER(RTRIM(@campo))='HITO_FECHA'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_fecha = @valordate
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
	
	if UPPER(RTRIM(@campo))='HITO_USUARIO'
		update GesPet.dbo.ProyectoIDMHitos
		set hito_usuario = @valortxt
		where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
	
	if UPPER(RTRIM(@campo))='HST_NROINTERNO'
		update GesPet.dbo.ProyectoIDMHitos
		set hst_nrointerno = @valornum
		where 
			ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
			LTRIM(RTRIM(hito_nombre)) = LTRIM(RTRIM(@hito_nombre)) and 
			hito_fe_ini = convert(smalldatetime,@hito_fe_ini) and hito_fe_fin = convert(smalldatetime,@hito_fe_fin)
*/