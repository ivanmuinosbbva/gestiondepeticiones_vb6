/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionInfohc'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionInfohc' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionInfohc
go

create procedure dbo.sp_UpdatePeticionInfohc
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_tipo		char(3),
	@info_punto		int,
	@info_recurso	char(10),
	@info_estado	char(6)
as
	update 
		GesPet.dbo.PeticionInfohc
	set
		info_tipo		= @info_tipo,
		info_punto		= @info_punto, 
		info_recurso	= @info_recurso,
		info_estado		= @info_estado
	where 
		pet_nrointerno = @pet_nrointerno and
		info_id = @info_id and
		info_idver = @info_idver
go

grant execute on dbo.sp_UpdatePeticionInfohc to GesPetUsr
go

print 'Actualización realizada.'
go
