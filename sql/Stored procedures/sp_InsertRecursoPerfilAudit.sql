/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertRecursoPerfilAudit'
go

if exists (select * from sysobjects where name = 'sp_InsertRecursoPerfilAudit' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertRecursoPerfilAudit
go

create procedure dbo.sp_InsertRecursoPerfilAudit
	@cod_recurso	char(10),
	@cod_perfil		char(4),
	@cod_ope		int,
	@cod_nivel		char(4),
	@cod_area		char(8),
	@auditfch		smalldatetime,
	@auditusr		char(10)
as
	insert into GesPet.dbo.RecursoPerfilAudit (
		cod_recurso,
		cod_perfil,
		cod_ope,
		cod_nivel,
		cod_area,
		auditfch,
		auditusr)
	values (
		@cod_recurso,
		@cod_perfil,
		@cod_ope,
		@cod_nivel,
		@cod_area,
		@auditfch,
		@auditusr)
go

grant execute on dbo.sp_InsertRecursoPerfilAudit to GesPetUsr
go

print 'Actualización realizada.'
go
