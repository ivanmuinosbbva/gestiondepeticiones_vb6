use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDM2'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDM2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDM2
go

create procedure dbo.sp_GetProyectoIDM2
	@ProjId			int=null, 
	@ProjSubId		int=null, 
	@ProjSubsId		int=null,
	@ProjNom		varchar(100)=null,
	@tipo			char(1)='P',
	@nivel			int
as 
	if @tipo = 'P'
		begin 
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				PRJ.ProjNom,
				isnull(PRJ.ProjFchAlta,'') as ProjFchAlta,
				'nivel' = (case
					when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
					when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
					when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
				end)
				,orden = (PRJ.ProjId * 10000 + PRJ.ProjSubId * 10 + PRJ.ProjSubSId)				-- Para ordenar la grilla
			from
				GesPet..ProyectoIDM PRJ 
			where
				(@ProjId is null or PRJ.ProjId = @ProjId) and
				(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
				(@ProjSubsId is null or PRJ.ProjSubSId = @ProjSubsId) and
				(@ProjNom is null or LTRIM(RTRIM(PRJ.ProjNom)) like '%' + LTRIM(RTRIM(@ProjNom)) + '%') and
				(@nivel is null OR (
					(@nivel = 1 and PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 ) OR 
					(@nivel = 2 and PRJ.ProjSubId <> 0 and PRJ.ProjSubSId = 0) OR 
					(@nivel = 3 and PRJ.ProjSubId <> 0 and PRJ.ProjSubSId <> 0)))
			order by
				7
			return(0)
		end

	if @tipo = 'A'
		begin 
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				PRJ.ProjNom,
				isnull(PRJ.ProjFchAlta,'') as ProjFchAlta,
				'nivel' = (case
					when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
					when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
					when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
				end)
				,orden = (PRJ.ProjId * 10000 + PRJ.ProjSubId * 10 + PRJ.ProjSubSId)				-- Para ordenar la grilla
			from
				GesPet..ProyectoIDM PRJ 
			where
				(@ProjId is null or PRJ.ProjId = @ProjId) and
				(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
				(@ProjSubsId is null or PRJ.ProjSubSId = @ProjSubsId) and
				(@ProjNom is null or LTRIM(RTRIM(PRJ.ProjNom)) like '%' + LTRIM(RTRIM(@ProjNom)) + '%') and
				(@nivel is null OR (
					(@nivel = 1 and PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 ) OR 
					(@nivel = 2 and PRJ.ProjSubId <> 0 and PRJ.ProjSubSId = 0) OR 
					(@nivel = 3 and PRJ.ProjSubId <> 0 and PRJ.ProjSubSId <> 0)))
			order by
				6,
				7
			return(0)
		end
go

grant execute on dbo.sp_GetProyectoIDM2 to GesPetUsr
go

print 'Actualización realizada.'
go
