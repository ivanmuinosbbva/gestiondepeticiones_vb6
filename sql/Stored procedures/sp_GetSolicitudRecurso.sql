/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSolicitudRecurso'
go

if exists (select * from sysobjects where name = 'sp_GetSolicitudRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSolicitudRecurso
go

create procedure dbo.sp_GetSolicitudRecurso
	@cod_recurso	char(10),
	@sol_tipo		char(1),
	@campo			char(15)
as
	if upper(rtrim(@campo)) = upper('sol_file_prod')
		select 
			a.sol_file_prod
		from 
			GesPet..Solicitudes a
		where
			a.sol_recurso = @cod_recurso and
			a.sol_tipo = @sol_tipo
		group by 
			a.sol_file_prod
	
	if upper(rtrim(@campo)) = upper('sol_file_desa')
		select 
			a.sol_file_desa
		from 
			GesPet..Solicitudes a
		where
			a.sol_recurso = @cod_recurso and
			a.sol_tipo = @sol_tipo
		group by
			a.sol_file_desa
	
	if upper(rtrim(@campo)) = upper('sol_file_out')
		select 
			a.sol_file_out
		from 
			GesPet..Solicitudes a
		where
			a.sol_recurso = @cod_recurso and
			a.sol_tipo = @sol_tipo
		group by
			a.sol_file_out
	
	if upper(rtrim(@campo)) = upper('sol_lib_Sysin')
		select 
			a.sol_lib_Sysin
		from 
			GesPet..Solicitudes a
		where
			a.sol_recurso = @cod_recurso and
			a.sol_tipo = @sol_tipo
		group by 
			a.sol_lib_Sysin

	if upper(rtrim(@campo)) = upper('sol_mem_Sysin')
		select 
			a.sol_mem_Sysin
		from 
			GesPet..Solicitudes a
		where
			a.sol_recurso = @cod_recurso and
			a.sol_tipo = @sol_tipo
		group by
			a.sol_mem_Sysin

	if upper(rtrim(@campo)) = upper('sol_nrotabla')
		select 
			a.sol_nrotabla
		from 
			GesPet..Solicitudes a
		where
			a.sol_recurso = @cod_recurso and
			a.sol_tipo = @sol_tipo
		group by
			a.sol_nrotabla
go

grant execute on dbo.sp_GetSolicitudRecurso to GesPetUsr
go

print 'Actualizaci�n realizada.'