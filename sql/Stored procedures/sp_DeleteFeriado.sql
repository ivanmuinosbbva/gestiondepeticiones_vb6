use GesPet
go
print 'sp_DeleteFeriado'
go
if exists (select * from sysobjects where name = 'sp_DeleteFeriado' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteFeriado
go
create procedure dbo.sp_DeleteFeriado 
             @aaaamm        char(6) 
as 
   delete from Feriado 
   where SUBSTRING(convert(char(8),fecha,112),1,6) = @aaaamm 
   return(0) 
go



grant execute on dbo.sp_DeleteFeriado to GesPetUsr 
go


