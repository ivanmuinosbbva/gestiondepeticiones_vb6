/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR  SUSER_SNAME*/

/*
-001- a. FJS 13.05.2016 - Nueva versi�n para usar con n�meros reales.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePetField2'
go

if exists (select * from sysobjects where name = 'sp_UpdatePetField2' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePetField2
go

create procedure dbo.sp_UpdatePetField2
	@pet_nrointerno		int,
	@campo				varchar(30),
	@valortxt			varchar(255)='',
	@valordate			smalldatetime=null,
	@valornum			REAL=null
as
	declare @empresa				int
	declare @empresa_inter			int
	declare @empresa_default		int
	declare @sum_impacto			real
	declare @sum_facilidad			real
	declare @sum_impacto_facilidad	real
	declare @valor_impacto			real
	declare @valor_facilidad		real
	declare @puntuacion				real

	if RTRIM(@campo)='IMPACTO'
		update Peticion
		set valor_impacto = @valornum,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	
	if RTRIM(@campo)='FACILIDA'
		update Peticion
		set valor_facilidad = @valornum,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='PUNTAJE'
		begin 
			-- Obtengo la empresa de la petici�n, y los valores de impacto y facilidad
			select 
				@empresa = p.pet_emp,
				@valor_impacto = p.valor_impacto,
				@valor_facilidad = p.valor_facilidad
			from Peticion p
			where p.pet_nrointerno = @pet_nrointerno

			select @empresa_default = 1
			select @empresa_inter = isnull(ie.empid_indicador,@empresa_default)
			from
				Empresa e LEFT JOIN 
				IndicadoresEmpresa ie ON (e.empid = ie.empid_peticion)
			where e.empid = @empresa

			-- a. Sumatoria del total de los pesos de los drivers de Impacto
			select @sum_impacto = CONVERT(REAL,SUM(d.driverValor))
			from 
				Drivers d left join
				Indicador i on (i.indicadorId = d.indicadorId)
			where 
				i.indicadorEmp = @empresa and 
				i.indicadorCategoria = 'IMPACTO'
			
			-- b. Sumatoria del total de los pesos de los drivers de Facilidad
			select @sum_facilidad = CONVERT(REAL,SUM(d.driverValor))
			from 
				Drivers d left join
				Indicador i on (i.indicadorId = d.indicadorId)
			where 
				i.indicadorEmp = @empresa and 
				i.indicadorCategoria = 'FACILIDA'

			select @sum_impacto_facilidad = @sum_impacto + @sum_facilidad

			select @puntuacion = 
				(@valor_facilidad * (@sum_facilidad) / @sum_impacto_facilidad) +
				(@valor_impacto * (@sum_impacto / @sum_impacto_facilidad))

			update Peticion
			set puntuacion = @puntuacion
			where pet_nrointerno = @pet_nrointerno
		end

	return(0)
go

grant execute on dbo.sp_UpdatePetField2 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
