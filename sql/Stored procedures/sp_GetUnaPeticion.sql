use GesPet
go

print 'Creando/actualizando SP: sp_GetUnaPeticion'
go

if exists (select * from sysobjects where name = 'sp_GetUnaPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetUnaPeticion
go

create procedure dbo.sp_GetUnaPeticion
	@pet_nrointerno    int    
as  
	declare @per_nrointerno			int
	declare @hoy					smalldatetime
	
	-- Obtengo el período actual de planificación
	select @hoy = getdate()
	select @per_nrointerno = p.per_nrointerno
	from Periodo p 
	where
		@hoy >= fe_desde and 
		@hoy <= fe_hasta
	
	--{ add -009- a. Esto es para obtener los agrupamientos de SDA
	declare @secuencia		int  
	declare @ret_status     int  
	declare @agr_nrointerno	int
	
	SELECT @agr_nrointerno = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	--select @agr_nrointerno = 1609			-- SDA Padre (PRODUCCION)
	--select @agr_nrointerno = 691			-- SDA Padre (DESAROLLO)

	create table #AgrupXPetic (
		pet_nrointerno		int,
		agr_nrointerno		int) 

	if @agr_nrointerno<>0  
		begin  
			create table #AgrupArbol(  
				secuencia		int,  
				nivel			int,  
				agr_nrointerno  int)  
			
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin  
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError  
						return (@ret_status)  
					end  
				end  
			insert into #AgrupXPetic (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_vigente='S'
		end
	--}

     select
        PET.pet_nrointerno,
        PET.pet_nroasignado,
        PET.titulo,
        PET.cod_tipo_peticion,
        PET.prioridad,
        PET.pet_nroanexada,
        PET.fe_pedido,
        PET.fe_requerida,
        PET.fe_comite,
        PET.fe_ini_plan,
        PET.fe_fin_plan,
        PET.fe_ini_real,
        PET.fe_fin_real,
        PET.horaspresup,
        PET.cod_direccion,
        PET.cod_gerencia,
        PET.cod_sector,
        PET.cod_usualta,
        usualta_usrname = (select R1.nom_recurso from Recurso R1 where PET.importancia_usr=R1.cod_recurso),
        PET.cod_solicitante,
        PET.cod_referente,
        PET.cod_bpar,
        PET.cod_supervisor,
        PET.cod_director,
        PET.cod_estado,
        nom_estado = (select EST.nom_estado from Estados EST where RTRIM(PET.cod_estado)=RTRIM(EST.cod_estado)),
        PET.fe_estado,
        PET.cod_situacion,
        nom_situacion = (select SIT.nom_situacion from  Situaciones SIT where RTRIM(PET.cod_situacion)=RTRIM(SIT.cod_situacion)),  
        PET.corp_local,
        PET.cod_orientacion,
        PET.importancia_cod,
        PET.importancia_prf,
        PET.importancia_usr,
        importancia_usrname  = (select R2.nom_recurso from Recurso R2 where PET.importancia_usr=R2.cod_recurso),
        anx_nroasignado = (select ANX.pet_nroasignado from Peticion ANX where PET.pet_nroanexada=ANX.pet_nrointerno), 
        nom_orientacion = (select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion), 
        importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod), 
        PET.fe_ini_orig,
        PET.fe_fin_orig,
		PET.pet_regulatorio,
		PET.cant_planif,
        PET.pet_sox001,
        PET.prj_nrointerno, 
        PET.cod_clase, 
        PET.pet_imptech 
	   ,PET.pet_projid
	   ,PET.pet_projsubid
	   ,PET.pet_projsubsid
	   ,PET.pet_emp
		,pet_ro = isnull(PET.pet_ro,'-')
		,pet_riesgo = (case
			when PET.pet_ro = 'A' then 'Alta'
			when PET.pet_ro = 'M' then 'Media'
			when PET.pet_ro = 'B' then 'Baja'
			else 'No'
		end)
		,PET.cod_BPE,
		PET.fecha_BPE,
		PET.pet_driver,
		driverNom = case
			when PET.pet_driver = 'DESA' then 'Sistemas'
			when PET.pet_driver = 'BPE'	then 'RGP'
			else 'Otro'
		end,
		PET.valor_impacto,
		PET.valor_facilidad,
		PET.cargaBeneficios,
		PET.puntuacion,
		categoria = case
			when PET.cod_tipo_peticion in ('AUI','AUX') OR PET.pet_regulatorio = 'S' then 'REG'
			when exists (select x.pet_nrointerno from #AgrupXPetic x where x.pet_nrointerno = PET.pet_nrointerno) then 'SDA'
			when exists (select count(1) from ProyectoIDM px where PET.pet_projid = px.ProjId and PET.pet_projsubid = px.ProjSubId and PET.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then 'PES'
			when exists (select count(1) from ProyectoIDM px where PET.pet_projid = px.ProjId and PET.pet_projsubid = px.ProjSubId and PET.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then 'ENG'
			when PET.cod_clase in ('OPTI','CORR','SPUF','ATEN') then 'CSI'
			when exists (
				 select count(1) 
				 from ProyectoIDM px 
				 where PET.pet_projid = px.ProjId and PET.pet_projsubid = px.ProjSubId and PET.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
				 having count(1) > 0) OR (PET.valor_impacto <> 0 OR PET.valor_facilidad <>0) then 'PRI'
			when PET.cod_clase = 'SINC' then '---'
			else 'SPR'
		end,
		pet_prioridad = ISNULL((select ISNULL(pc.prioridad,0) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno),0),
		prioridad_total = (select count(1) from PeticionPlancab pc where pc.per_nrointerno = @per_nrointerno),
		en_planificacion = (
			select per.per_abrev 
			from 
				PeticionPlancab pc inner join
				Periodo per on (per.per_nrointerno = pc.per_nrointerno)
			where pc.pet_nrointerno = PET.pet_nrointerno),
		PET.sda_agr_nrointerno,
		PET.sgi_incidencia
	from  
        GesPet..Peticion PET 
    where 
        PET.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null
	return(0)
go

grant Execute on dbo.sp_GetUnaPeticion to GesPetUsr 
go

print 'Actualización realizada.'
go
