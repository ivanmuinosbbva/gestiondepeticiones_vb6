/*
-000- a. FJS 13.07.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionPlancab'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionPlancab' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionPlancab
go

create procedure dbo.sp_InsertPeticionPlancab
	@per_nrointerno		int,
	@pet_nrointerno		int,
	@prioridad			int=null,
	@cod_bpe			char(10)=null
as 
	if not exists (select pet_nrointerno from GesPet..PeticionPlancab where per_nrointerno = @per_nrointerno and pet_nrointerno = @pet_nrointerno)
		begin	
			insert into GesPet..PeticionPlancab (
				per_nrointerno,
				pet_nrointerno,
				prioridad,
				plan_actfch,
				plan_actusr,
				cod_bpe,
				plan_origen)
			values (
				@per_nrointerno,
				@pet_nrointerno,
				@prioridad,
				getdate(),
				suser_name(),
				@cod_bpe,
				2)
		end
	else
		begin
			update GesPet..PeticionPlancab
			set 
				prioridad   = @prioridad,
				cod_bpe		= @cod_bpe,
				plan_actfch = getdate(),
				plan_actusr = suser_name()
			where
				per_nrointerno = @per_nrointerno and
				pet_nrointerno = @pet_nrointerno
		end
	
	return(0)
go

grant execute on dbo.sp_InsertPeticionPlancab to GesPetUsr
go

print 'Actualización realizada.'
go
