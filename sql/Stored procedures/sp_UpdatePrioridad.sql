/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePrioridad'
go

if exists (select * from sysobjects where name = 'sp_UpdatePrioridad' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePrioridad
go

create procedure dbo.sp_UpdatePrioridad
	@prior_id	char(1),
	@prior_dsc	char(50),
	@prior_ord	int,
	@prior_hab	char(1)
as
	update 
		GesPet.dbo.Prioridad
	set
		prior_dsc = @prior_dsc,
		prior_ord = @prior_ord,
		prior_hab = @prior_hab
	where 
		(prior_id = @prior_id or @prior_id is null)
	return(0)
go

grant execute on dbo.sp_UpdatePrioridad to GesPetUsr
go

print 'Actualización realizada.'
go
