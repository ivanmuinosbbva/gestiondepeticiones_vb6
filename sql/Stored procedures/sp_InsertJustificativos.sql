/*
-000- a. FJS 04.08.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertJustificativos'
go

if exists (select * from sysobjects where name = 'sp_InsertJustificativos' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertJustificativos
go

create procedure dbo.sp_InsertJustificativos
	@jus_codigo			int=0,
	@jus_descripcion	varchar(255)='',
	@jus_hab			char(1)=''
as
	insert into GesPet.dbo.Justificativos (
		jus_codigo,
		jus_descripcion,
		jus_hab)
	values (
		@jus_codigo,
		@jus_descripcion,
		@jus_hab)
go

grant execute on dbo.sp_InsertJustificativos to GesPetUsr
go

print 'Actualización realizada.'
