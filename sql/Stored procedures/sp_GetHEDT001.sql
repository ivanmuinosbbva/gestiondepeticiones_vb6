/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */
 
/*
-000- a. FJS 02.10.2009 - 
-001- a. FJS 03.11.2009 - Se agrega el campo de actualizaci�n de estado por proceso de transmisi�n.
-002- a. FJS 05.11.2009 - Se agrega la descripci�n del estado para el archivo DSN.
-002- b. FJS 16.11.2009 - Se agrega una descripci�n en vez del resultado del campo bruto.
-003- a. FJS 16.12.2009 - Se agregan campos para indicar la validez del archivo declarado.
-004- a. FJS 17.06.2010 - Se contemplan nuevos estados al respecto del visado de SI: pendiente, rechazado o visado.
-004- b. FJS 17.06.2010 - Se agrega nuevo campo de texto para informar el motivo de rechazo de un DSN por parte de SI.
-005- a. FJS 07.01.2011 - Se agrega el nombre productivo del DSN.
-006- a. FJS 08.07.2011 - Para que un DSN se considere v�lido, adem�s debe tener el nombre productivo.
-007- a. FJS 30.08.2012 - Pet. N� 51089
-008- a. FJS 12.03.2013 - Pet. N� 51031
-009- a. FJS 25.03.2015 - Nuevo: se agregan nuevos par�metros para filtrar.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT001'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT001' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT001
go

create procedure dbo.sp_GetHEDT001
	@dsn_id				char(8)=null,
	@dsn_nom			char(50)=null,
	@dsn_nomprod		char(50)=null,		-- add -009- a.
	@dsn_owner			char(10)=null,
	@fecha_alta			smalldatetime=null,
	@estado				char(1)=null
as
	select 
		a.dsn_id,
		a.dsn_nom,
		dsn_enmas = 
			case
				when a.dsn_enmas = 'S' then 'Si'
				when a.dsn_enmas = 'N' then 'No'
			end,
		a.dsn_nomrut,		
		a.dsn_feult,
		cod_sector  = (select x.cod_sector from GesPet..Recurso x where x.cod_recurso = a.dsn_userid),
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_userid),	-- add -004- a.
		dsn_vb =
			case
				/*
				when a.dsn_vb = 'N' then 'Pendiente'
				when a.dsn_vb = 'R' then 'Rechazado'
				when a.dsn_vb = 'S' then 'Visado'
				*/
				when a.dsn_vb = 'B' then 'Borrador'
				when a.dsn_vb = 'N' then 'Pendiente revisi�n'
				when a.dsn_vb = 'D' then 'A revisar DyD'
				when a.dsn_vb = 'H' then 'Hist�rico'
				when a.dsn_vb = 'P' then 'Pendiente pos-revisi�n'
				when a.dsn_vb = 'R' then 'Rechazado'
				when a.dsn_vb = 'S' then 'Visado'
				else 'Pendiente revisi�n'
			end,
		nom_vb_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_vb_userid),	-- add -004- a.
		a.dsn_vb_fe,
		a.app_id,
		app_name = (select x.app_nombre from GesPet..Aplicativo x where x.app_id = a.app_id),
		a.dsn_desc,
		nom_estado = 
			case
				when a.estado = 'A' then 'A�n sin utilizar'
				else 'Ya utilizado'
			end,
		a.estado,
		valido = 
			case
				when a.dsn_enmas = 'N' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
				when (
					select count(x.dsn_id) 
					from GesPet..HEDT003 x 
					where x.dsn_id = a.dsn_id and (x.cpo_nomrut <> '' and x.cpo_nomrut is not null))>0 and 
				a.dsn_enmas = 'S' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
				/*
				when ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
					  (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and (x.cpo_nomrut <> '' and x.cpo_nomrut is not null))>0) and a.dsn_enmas = 'S' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
				*/
				else 'No'
			end,
		a.dsn_vb as dsn_vb_id,
		a.dsn_txt,
		a.dsn_nomprod,
		a.fe_hst_estado,
		nom_userid = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_userid),
		nom_vb_userid = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_vb_userid),
		a.dsn_userid as 'cod_recurso',
		a.dsn_cpy,
		a.dsn_cpybbl,
		a.dsn_cpynom,
		a.dsn_cantuso
	from 
		GesPet.dbo.HEDT001 a
	where
		(@dsn_id is null or a.dsn_id = @dsn_id) and
		--(@dsn_nom is null or a.dsn_nom = @dsn_nom) and		-- No quitar!!!													-- del -009- a.
		(@dsn_nom is null or LTRIM(RTRIM(UPPER(a.dsn_nom))) like '%' + LTRIM(RTRIM(UPPER(@dsn_nom))) + '%') and					-- add -009- a.
		(@dsn_nomprod is null or LTRIM(RTRIM(UPPER(a.dsn_nomprod))) like '%' + LTRIM(RTRIM(UPPER(@dsn_nomprod))) + '%') and		-- add -009- a.
		(@dsn_owner is null or LTRIM(RTRIM(a.dsn_userid)) = LTRIM(RTRIM(@dsn_owner))) and
		--a.dsn_vb <> 'H' and 																									-- del -009- a.
		(@fecha_alta is null or a.dsn_feult >= @fecha_alta) and 
		((@estado is null and a.dsn_vb <> 'H') or a.dsn_vb = @estado)															-- add -009- a.
	order by 
		a.dsn_id
	return(0)
go

grant execute on dbo.sp_GetHEDT001 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go

/*
(
			@dsn_valido is null or 
			(
				@dsn_valido = 'S' and 
				a.dsn_enmas = 'N' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '')
			) or (
				
				
					select count(x.dsn_id) 
					from GesPet..HEDT003 x 
					where x.dsn_id = a.dsn_id and (x.cpo_nomrut <> '' and x.cpo_nomrut is not null))>0 and 
				a.dsn_enmas = 'S' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
			
			)
		
		)
*/