/*
-000- a. FJS 09.06.2008 - Nuevo SP para agregar mensajes independientes solo para el grupo Homologador
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DoMensajeHomologacion'
go

if exists (select * from sysobjects where name = 'sp_DoMensajeHomologacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DoMensajeHomologacion
go

create procedure dbo.sp_DoMensajeHomologacion
	@pet_nrointerno		int=null, 
	@cod_perfil			varchar(8)=null, 
	@cod_hijo			varchar(8)=null,
	@cod_txtmsg			int=null,
	@cod_estado			varchar(6)=null,
	@txt_txtmsg			varchar(250)=null
as 
	declare @proxnumero	int
	select @proxnumero = 0  

	execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT    

	insert 
		Mensajes
	values (
		@proxnumero,
		@pet_nrointerno,
		getdate(),
		@cod_perfil,
		'GRUP',
		@cod_hijo,
		@cod_txtmsg,
		@cod_estado,  
		'',  
		@txt_txtmsg)
	return(0) 
go

grant execute on dbo.sp_DoMensajeHomologacion to GesPetUsr
go

print 'Actualización realizada.'
go