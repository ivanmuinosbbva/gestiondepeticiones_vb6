use GesPet
go

print 'Actualizando el SP: sp_UpdateHorasTrabajadas...'
go

if exists (select * from sysobjects where name = 'sp_UpdateHorasTrabajadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHorasTrabajadas
go

create procedure dbo.sp_UpdateHorasTrabajadas
	@cod_recurso		char(10),
	@cod_tarea_a		char(8)='',
	@pet_nrointerno_a	int=0,
	@fe_desde_a			smalldatetime,
	@fe_hasta_a			smalldatetime,
	@cod_tarea			char(8)='',
	@pet_nrointerno		int=0,
	@fe_desde			smalldatetime,
	@fe_hasta			smalldatetime,
	@horas				int,
	@trabsinasignar		char(1)=null,
	@observaciones		varchar(255)=null,
	@tipo				int=0
as

/* si esta modificando el mismo registro */
	if @cod_tarea_a=@cod_tarea and
		@pet_nrointerno_a=@pet_nrointerno and
		@fe_desde_a=@fe_desde and
		@fe_hasta_a=@fe_hasta
	begin
		if exists (
			select cod_recurso from GesPet..HorasTrabajadas 
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
			pet_nrointerno = @pet_nrointerno and
			(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) and
			convert(char(8),fe_desde,112) = convert(char(8),@fe_desde,112) and
			convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta,112))
			begin
				update GesPet..HorasTrabajadas
				set 
					trabsinasignar = @trabsinasignar,
					horas = @horas,
					observaciones = @observaciones,
					tipo = @tipo
				where 
					RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
					pet_nrointerno = @pet_nrointerno and
					(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) and
					convert(char(8),fe_desde,112) = convert(char(8),@fe_desde,112) and
					convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta,112)
				return(0)
			end
		else
			begin
				raiserror 30012 'Periodo/tarea/peticion inexistente'
				select 30012 as ErrCode , 'Periodo/tarea/peticion inexistente' as ErrDesc
				return (30012)
			end
	end

	/* que no se pise con alguna informacion existente (y no es justo esta misma)*/
	if exists (select cod_recurso from GesPet..HorasTrabajadas where
		RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
		pet_nrointerno = @pet_nrointerno and
		(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) and
		convert(char(8),@fe_desde,112) <= convert(char(8),fe_desde,112) and
		convert(char(8),@fe_hasta,112) >= convert(char(8),fe_hasta,112) and
			( not	(pet_nrointerno = @pet_nrointerno_a and
				(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea_a)) and
				convert(char(8),fe_desde,112) = convert(char(8),@fe_desde_a,112) and
				convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta_a,112)))
		)
		begin
			raiserror 30011 'Periodo ya informado para esa tarea/peticion -1'
			select 30011 as ErrCode , 'Periodo ya informado para esa tarea/peticion -1' as ErrDesc
			return (30011)
		end
	
	if exists (select cod_recurso from GesPet..HorasTrabajadas where
		RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
		pet_nrointerno = @pet_nrointerno and
		(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) and
		((
		convert(char(8),@fe_desde,112) >= convert(char(8),fe_desde,112) and
		convert(char(8),@fe_desde,112) <= convert(char(8),fe_hasta,112)
		) or (
		convert(char(8),@fe_hasta,112) >= convert(char(8),fe_desde,112) and
		convert(char(8),@fe_hasta,112) <= convert(char(8),fe_hasta,112)
		)) and
			(not	(pet_nrointerno = @pet_nrointerno_a and
				(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea_a)) and
				convert(char(8),fe_desde,112) = convert(char(8),@fe_desde_a,112) and
				convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta_a,112)))
		)
		begin
			raiserror 30011 'Periodo ya informado para esa tarea/peticion -2'
			select 30011 as ErrCode , 'Periodo ya informado para esa tarea/peticion -2' as ErrDesc
			return (30011)
		end


		/* elimina lo anterior */
		delete GesPet..HorasTrabajadas
		where RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
			pet_nrointerno = @pet_nrointerno_a and
			(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea_a)) and
			convert(char(8),fe_desde,112) = convert(char(8),@fe_desde_a,112) and
			convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta_a,112)

		/* inserta lo nuevo */
		insert into GesPet..HorasTrabajadas
			(cod_recurso,
			cod_tarea,
			pet_nrointerno,
			fe_desde,
			fe_hasta,
			horas,
			trabsinasignar,
			observaciones,
			audit_user,
			audit_date,
			tipo)
		values
			(@cod_recurso,
			@cod_tarea,
			@pet_nrointerno,
			@fe_desde,
			@fe_hasta,
			@horas,
			@trabsinasignar,
			@observaciones,
			SUSER_NAME(),
			getdate(),
			@tipo)
		return(0)
go

grant execute on dbo.sp_UpdateHorasTrabajadas to GesPetUsr
go

print 'Actualización finalizada.'
go

print 'Fin de proceso.'
go
