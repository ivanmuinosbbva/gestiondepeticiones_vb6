/*
-000- a. FJS 13.05.2016 - Nuevo SP para obtener el catálogo de tablas para ambiente SSDD.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetCatalogoTabla'
go

if exists (select * from sysobjects where name = 'sp_GetCatalogoTabla' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetCatalogoTabla
go

create procedure dbo.sp_GetCatalogoTabla
	@id				int=null,
	@dbname			varchar(40)=null,
	@app_id			char(30)=null,
	@DBMS			int=null
as 
	select 
		ct.id,
		ct.nombreTabla,
		ct.baseDeDatos,
		ct.esquema,
		ct.DBMS,
		DBMS_nombre = (select d.dbmsNom from DBMS d where d.dbmsId = ct.DBMS),
		ct.servidorNombre,
		ct.servidorPuerto,
		ct.app_id,
		app_nombre = (select x.app_nombre from Aplicativo x where x.app_id = ct.app_id),
		ct.fe_alta,
		ct.usr_alta,
		ct.datosSensibles,
		ct.visado,
		ct.visadoFecha,
		ct.visadoUsuario
	from 
		GesPet..CatalogoTabla ct
	where 
		(@id is null or ct.id = @id) and 
		(@dbname is null or (ct.baseDeDatos = @dbname OR ct.baseDeDatos LIKE '%' + LTRIM(RTRIM(@dbname)) + '%')) and 
		(@app_id is null or ct.app_id = @app_id) and 
		(@DBMS is null or ct.DBMS = @DBMS)
	return(0)
go

grant execute on dbo.sp_GetCatalogoTabla to GesPetUsr
go
grant execute on dbo.sp_GetCatalogoTabla to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_GetCatalogoTabla','anymode'
go

print 'Actualización realizada.'
go
