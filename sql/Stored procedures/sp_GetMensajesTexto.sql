use GesPet
go
print 'sp_GetMensajesTexto'
go
if exists (select * from sysobjects where name = 'sp_GetMensajesTexto' and sysstat & 7 = 4)
drop procedure dbo.sp_GetMensajesTexto
go
create procedure dbo.sp_GetMensajesTexto 
             @cod_txtmsg    int=null 
as 
    select  cod_txtmsg, 
        msg_texto 
    from    GesPet..MensajesTexto 
    where   (@cod_txtmsg is null or @cod_txtmsg=cod_txtmsg) 
return(0) 
go



grant execute on dbo.sp_GetMensajesTexto to GesPetUsr 
go


