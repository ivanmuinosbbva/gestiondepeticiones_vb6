/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
-001- a. FJS 16.03.2015 - Se agregan nuevos datos: marca de visibilidad en hitos.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMHitos'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMHitos' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMHitos
go

create procedure dbo.sp_UpdateProyectoIDMHitos
	@ProjId				int,
	@ProjSubId			int,
	@ProjSubSId			int,
	@hit_nrointerno		int,
	@hito_nombre		char(100),
	@hito_descripcion	char(255),
	@hito_fe_ini		char(20),
	@hito_fe_fin		char(20),
	@hito_estado		char(6),
	@cod_nivel			char(4),
	@cod_area			char(8),
	@orden				int,
	@hito_expos			char(1),
	@hito_id			int				-- upd -002- a.
as
	update 
		GesPet..ProyectoIDMHitos
	set
		hito_nombre		= @hito_nombre,
		hito_descripcion= @hito_descripcion,
		hito_fe_ini		= isnull(convert(smalldatetime, @hito_fe_ini),null),
		hito_fe_fin		= isnull(convert(smalldatetime, @hito_fe_fin),null),
		hito_estado		= @hito_estado,
		cod_nivel		= @cod_nivel,
		cod_area		= @cod_area,
		hito_fecha		= getdate(),
		hito_usuario	= SUSER_NAME(),
		hit_orden		= @orden,
		--{ add -002- a.
		hito_expos		= @hito_expos,
		hito_id			= @hito_id
		--}
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hit_nrointerno = @hit_nrointerno

	return(0)
go

grant execute on dbo.sp_UpdateProyectoIDMHitos to GesPetUsr
go

print 'Actualización realizada.'
go