/*
-000- a. FJS 04.03.2010 - Nuevo SP para validar que una solicitud no genere un mismo archivo destino en la tanda de solicitudes pendientes de procesar.
-001- a. FJS 30.04.2010 - Se corrige la condici�n l�gica para cumplir la condici�n correctamente.
-002- a. FJS 09.10.2010 - Bug: para solicitudes sin enmascarar, no debe requerir la completitud del campo [sol_trnm_fe] ya que este nunca es actualizado
						  por el proceso de pasaje de solicitudes, ya que es enviado por mail.
-003- a. FJS 07.07.2014 - Nuevo: las solicitudes por emergencia no se tienen en cuenta para esta validaci�n.
-004- a. FJS 23.10.2014 - Nuevo: se vuelve a codificar la regla para validar que no se este pidiendo un mismo archivo dos veces antes de que corra el proceso.


Este control se realiza para saber si un usuario esta pidiendo el mismo archivo dos veces seguidas.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSolicitudValida'
go

if exists (select * from sysobjects where name = 'sp_GetSolicitudValida' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSolicitudValida
go

create procedure dbo.sp_GetSolicitudValida
	@archivo_salida		varchar(255)
as
	if ltrim(rtrim(@archivo_salida)) = ' ' or @archivo_salida is null
		return(0)
	else
		begin
			select a.sol_nroasignado
			from GesPet..Solicitudes a
			where
				(a.sol_eme = 'N' OR a.sol_eme is null) and		-- No son EMErgencias
				/*
					No se encuentra en estos estados: 

					D: Aprobado y en proceso de envio
					E: Enviado y finalizado
					F: En espera de Restore
					G: En espera (ejecuci�n diferida)
				*/
				a.sol_estado not in ('D','E','F','G') and
				LTRIM(RTRIM(a.sol_file_desa)) = LTRIM(RTRIM(@archivo_salida))
		end
go

grant execute on dbo.sp_GetSolicitudValida to GesPetUsr
go

print 'Actualizaci�n realizada.'
go

/*
	if ltrim(rtrim(@archivo_salida)) = ' ' or @archivo_salida is null
		begin
			return(0)
		end
	else
		begin
			select 
				a.sol_nroasignado
			from 
				GesPet..Solicitudes a
			where
				(a.sol_trnm_fe is null and a.sol_mask = 'S') and		-- upd -002- a. Se agrega [and a.sol_mask = 'S']
				a.sol_trnm_fe is null and
				a.sol_estado <> 'G' and
				a.sol_eme <> 'S' and									-- add -003- a. 
				((a.sol_tipo in ('1','5') and rtrim(ltrim(a.sol_file_desa)) = ltrim(rtrim(@archivo_salida))) or
				(a.sol_tipo = '2' and rtrim(ltrim(a.sol_file_out)) = ltrim(rtrim(@archivo_salida))) or
				(a.sol_tipo = '3' and rtrim(ltrim(a.sol_file_out)) = ltrim(rtrim(@archivo_salida))) or
				(a.sol_tipo in ('4','6') and rtrim(ltrim(a.sol_file_out)) = ltrim(rtrim(@archivo_salida))))	-- upd -001- a. 
			return(0)
		end
*/