/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdEstadosPlanField'
go

if exists (select * from sysobjects where name = 'sp_UpdEstadosPlanField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdEstadosPlanField
go

create procedure dbo.sp_UpdEstadosPlanField
	@cod_estado_plan	int,
	@campo				char(80),
	@valortxt			char(255),
	@valordate			smalldatetime,
	@valornum			int
as
	if rtrim(@campo)='COD_ESTADO_PLAN'
		update 
			GesPet.dbo.EstadosPlanificacion
		set
			cod_estado_plan = @valornum
		where 
			(cod_estado_plan = @cod_estado_plan or @cod_estado_plan is null)

	if rtrim(@campo)='NOM_ESTADO_PLAN'
		update 
			GesPet.dbo.EstadosPlanificacion
		set
			nom_estado_plan = @valortxt
		where 
			(cod_estado_plan = @cod_estado_plan or @cod_estado_plan is null)

	if rtrim(@campo)='ESTADO_HAB'
		update 
			GesPet.dbo.EstadosPlanificacion
		set
			estado_hab = @valortxt
		where 
			(cod_estado_plan = @cod_estado_plan or @cod_estado_plan is null)
	
	return(0)
go

grant execute on dbo.sp_UpdEstadosPlanField to GesPetUsr
go

print 'Actualización realizada.'
