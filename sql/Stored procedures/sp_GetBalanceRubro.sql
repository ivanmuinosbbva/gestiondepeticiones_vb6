use GesPet
go
print 'sp_GetBalanceRubro'
go
if exists (select * from sysobjects where name = 'sp_GetBalanceRubro' and sysstat & 7 = 4)
drop procedure dbo.sp_GetBalanceRubro
go
create procedure dbo.sp_GetBalanceRubro
		@cod_BalanceRubro	char(8)=null,
		@flg_habil		char(1)=null
as
begin
	select
		cod_BalanceRubro,
		nom_BalanceRubro,
		flg_habil		
	from
		GesPet..BalanceRubro
	where
		(@cod_BalanceRubro is null or RTRIM(@cod_BalanceRubro) is null or RTRIM(@cod_BalanceRubro)='' or RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro)) and
		(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil))
	order by nom_BalanceRubro ASC
end
return(0)
go

grant execute on dbo.sp_GetBalanceRubro to GesPetUsr
go
