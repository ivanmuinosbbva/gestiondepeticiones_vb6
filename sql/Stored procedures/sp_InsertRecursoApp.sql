/*
-000- a. FJS 27.06.2008 - Nuevo SP para determinar los usuarios conectados a un momento al aplicativo
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertRecursoApp'
go

if exists (select * from sysobjects where name = 'sp_InsertRecursoApp' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertRecursoApp
go

create procedure dbo.sp_InsertRecursoApp
	@cod_recurso		char(10)=null,
	@cod_equipo			char(20)=null,
	@fe_ingreso			smalldatetime=null,
	@hr_ingreso			char(8)=null,
	@cod_usuario		char(8)=null
as 
	insert into GesPet..Aucab
		(cod_recurso, fe_ingreso, hr_ingreso, fe_egreso, cod_equipo, cod_usuario) 
	values
		(@cod_recurso, getdate(), @hr_ingreso, null, @cod_equipo, @cod_usuario)
	
	/*
	delete from GesPet..Aucab
	where 		
		cod_recurso = @cod_recurso and 
		cod_equipo = @cod_equipo and
		datediff(day, getdate(), fe_ingreso) <= 7 
	*/

go

grant execute on dbo.sp_InsertRecursoApp to GesPetUsr
go

print 'Actualización realizada.'
