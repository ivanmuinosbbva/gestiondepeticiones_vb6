/*
-000- a. FJS 04.08.2011 - Nuevo SP para determinar la validez de una petici�n (Dimensions CM).
-001- a. FJS 27.04.2015 - Actualizaci�n: por error en el uso de la tabla tmp en la base tempdb, se omite el llamado al sp que trae el detalle.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DIM_GetUnaPeticionValida'
go

if exists (select * from sysobjects where name = 'sp_DIM_GetUnaPeticionValida' and sysstat & 7 = 4)
	drop procedure dbo.sp_DIM_GetUnaPeticionValida
go

create procedure dbo.sp_DIM_GetUnaPeticionValida
	@modo				char(1)='0',	-- 0: modo validaci�n real (para pasajes a prod.) / 1: modo validaci�n light
	@pet_nroasignado	int,
	@tipo_pasaje		char(3),		-- Los par�metros a continuaci�n no tienen utilidad por el momento... 
	@usuario			char(10)=null,
	@fecha				smalldatetime=null
as 
	declare @pet_nrointerno		int

	if charindex(@tipo_pasaje,'EME|EXC|NOR|')=0
		begin
			select 0,null,null,'El tipo de pasaje no es correcto (Opc.: EME,EXC y NOR)',null,null
			return 0
		end
	
	if @modo = '0'
		begin
			if @tipo_pasaje = 'EME' 
				if @pet_nroasignado = 1111111
					select 1,null,null,'EME - V�LIDA PARA PASAJES A PRODUCCI�N',null,null
				else
					/*
						EME - V�LIDA POR EMERGENCIA (1) : V�lida porque es EME, existe la petici�n y es v�lida p/ pasajes a Prod.
						EME - V�LIDA POR EMERGENCIA (2) : V�lida porque es EME, existe la petici�n pero NO ES v�lida p/ pasajes a Prod.
						EME - NO EXISTE LA PETICI�N     : No es v�lida a pesar de ser EME porque no existe la petici�n.
					*/
					if (exists (
						select 1 
						from 
							GesPet..PeticionEnviadas pe inner join
							GesPet..Peticion p on (pe.pet_nrointerno = p.pet_nrointerno)
						where p.pet_nroasignado = @pet_nroasignado))
							select 1,null,null,'EME - V�LIDA POR EMERGENCIA (1)',null,null
					else
						if (exists (select 1 from GesPet..Peticion p where p.pet_nroasignado = @pet_nroasignado))
							select 1,null,null,'EME - V�LIDA POR EMERGENCIA (2)',null,null
						else
							select 0,null,null,'EME - NO EXISTE LA PETICI�N',null,null
			else
				begin
					if (exists (
						select 1 
						from 
							GesPet..PeticionEnviadas pe inner join
							GesPet..Peticion p on (pe.pet_nrointerno = p.pet_nrointerno)
						where p.pet_nroasignado = @pet_nroasignado))
							-- Es v�lida para pasajes a Producci�n
							select 1,null,null, @tipo_pasaje + ' - V�LIDA PARA PASAJES A PRODUCCI�N',null,null
					else
						if not exists (select 1 from GesPet..Peticion p where p.pet_nroasignado = @pet_nroasignado)
							-- No existe el nro. de petici�n
							select 0,null,null,@tipo_pasaje + ' - NO EXISTE LA PETICI�N',null,null
						else
							if exists (select 1 from GesPet..Peticion p where p.pet_nroasignado = @pet_nroasignado and charindex(p.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0)
								-- Petici�n en estado terminal
								select 0,null,null,@tipo_pasaje + ' - PETICI�N EN ESTADO TERMINAL',null,null
							else
								begin
									select 0,null,null,@tipo_pasaje + ' - NO V�LIDA PARA PASAJES A PRODUCCI�N',null,null
								end
				end
		end
	else
		select 0,null,null,'Modo incorrecto (solo soporta cero (0))',null,null
go

grant execute on dbo.sp_DIM_GetUnaPeticionValida to GesPetUsr
go

grant execute on dbo.sp_DIM_GetUnaPeticionValida to dimension
go

print 'Actualizaci�n realizada.'
go
