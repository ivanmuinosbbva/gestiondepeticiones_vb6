/*
-001- a. FJS 03.03.2008 - Se agregaron comentarios al SP.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionRecurso'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionRecurso
go

create procedure dbo.sp_DeletePeticionRecurso
	@pet_nrointerno     int = 0, 
	@cod_recurso		char(10) = null
as 
/* falta validar */  
-- Si el c�digo de recurso es NULL entonces borra todos los recursos declarados para la petici�n
if @cod_recurso is null  
    begin  
	    delete GesPet..PeticionRecurso  
		where @pet_nrointerno = pet_nrointerno 
	    return(0)
    end  
-- Si el n�mero interno de la petici�n es cero (0), borra el recurso pasado como par�metro para todas las peticiones existentes
if @pet_nrointerno=0 
    begin  
	    delete GesPet..PeticionRecurso  
		where RTRIM(cod_recurso)=RTRIM(@cod_recurso)  
	    return(0)  
    end
 
if exists 
    (select cod_recurso 
     from GesPet..PeticionRecurso 
     where @pet_nrointerno = pet_nrointerno and 
           cod_recurso = @cod_recurso)
        begin
            /*  
            ver que pasa con las horas trabajadas 
            if exists (select cod_sector from GesPet..Recurso where RTRIM(cod_sector) = RTRIM(@cod_sector))  
            begin  
                raiserror  30011 'Existen Recursos asociados a este Sector'   
                select 30011 as ErrCode, 'Existen Recursos asociados a este Sector'  as ErrDesc  
            return (30011)  
            end  
            */  
            delete GesPet..PeticionRecurso 
            where @pet_nrointerno = pet_nrointerno and cod_recurso = @cod_recurso
            return(0)  
        end  
else
    begin  
        raiserror 30001 'Peticion/Recurso Inexistente'    
        select 30001 as ErrCode, 'Peticion/Recurso Inexistente' as ErrDesc
        return (30001)    
    end
    return(0)
go

grant execute on dbo.sp_DeletePeticionRecurso to GesPetUsr 
go

print 'Actualizaci�n realizada.'