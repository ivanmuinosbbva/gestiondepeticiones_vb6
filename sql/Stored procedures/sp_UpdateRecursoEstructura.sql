/*
-001- a. FJS 14.11.2016 - Nuevo Sp.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateRecursoEstructura'
go

if exists (select * from sysobjects where name = 'sp_UpdateRecursoEstructura' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRecursoEstructura
go

create procedure dbo.sp_UpdateRecursoEstructura 
    @cod_recurso			char(10), 
    @new_cod_direccion		char(8), 
    @new_cod_gerencia		char(8), 
    @new_cod_sector			char(8), 
    @new_cod_grupo			char(8)
as 
	INSERT INTO Estructura 
	select 
		getDate(), 
		r.cod_recurso, 
		@new_cod_direccion, 
		@new_cod_gerencia, 
		@new_cod_sector, 
		@new_cod_grupo,
		SUSER_NAME(),
		getdate()
	from Recurso r
	where r.cod_recurso = @cod_recurso
	return(0)
go

sp_procxmode 'sp_UpdateRecursoEstructura', anymode
go

grant execute on dbo.sp_UpdateRecursoEstructura to GesPetUsr 
go

print 'Actualización realizada.'
go
