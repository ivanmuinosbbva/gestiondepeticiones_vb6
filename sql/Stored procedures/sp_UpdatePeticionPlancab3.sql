/*
-000- a. FJS 09.08.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlancab3'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlancab3' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlancab3
go

create procedure dbo.sp_UpdatePeticionPlancab3
	@per_nrointerno		int, 
	@pet_nrointerno		int,
	@cod_grupo			char(8)
as 
	update GesPet..PeticionPlandet
	set plan_estado = 'PLANIF'
	where
		per_nrointerno = @per_nrointerno and
		pet_nrointerno = @pet_nrointerno and
		cod_grupo = @cod_grupo
	
	update GesPet..PeticionPlancab
	set plan_estado = 'PLANIF'
	where
		per_nrointerno = @per_nrointerno and
		pet_nrointerno = @pet_nrointerno
go

grant execute on dbo.sp_UpdatePeticionPlancab3 to GesPetUsr
go

print 'Actualización realizada.'
