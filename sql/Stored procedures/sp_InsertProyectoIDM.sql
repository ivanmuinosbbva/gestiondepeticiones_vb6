/*
-000- a. FJS 05.05.2009 - Proyectos IDM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoIDM'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoIDM
go

create procedure dbo.sp_InsertProyectoIDM
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null,
	@ProjNom		char(100)=null,
	@ProjCatId		int=null,
	@ProjClaseId	int=null
as
	insert into GesPet.dbo.ProyectoIDM (
		ProjId,
		ProjSubId,
		ProjSubSId,
		ProjNom,
		ProjFchAlta,
		ProjCatId,
		ProjClaseId,
		cod_estado,
		fch_estado,
		usr_estado,
		marca,
		plan_tyo,
		empresa,
		area,
		clas3,
		semaphore,
		cod_direccion,
		cod_gerencia,
		cod_sector,
		cod_direccion_p,
		cod_gerencia_p,
		cod_sector_p,
		cod_grupo_p,
		avance,
		cod_estado2,
		fe_modif,
		fe_inicio,
		fe_fin,
		fe_finreprog,
		fe_ult_nove,
		semaphore2,
		sema_fe_modif,
		sema_usuario,
		tipo_proyecto,
		totalhs_gerencia,
		totalhs_sector,
		totalreplan,
		fe_aprob,
		cambio_alcance,
		codigo_gps)
	values (
		@ProjId,
		@ProjSubId,
		@ProjSubSId,
		@ProjNom,
		getdate(),
		@ProjCatId,
		@ProjClaseId,
		'EVALUA',			-- 'PLANIF',
		getdate(),
		SUSER_NAME(),
		'A',
		null,
		null,
		null,
		null,
		'3',
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		0,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		'I',
		0,
		0,
		0,
		null,
		'N',
		null)
go

grant execute on dbo.sp_InsertProyectoIDM to GesPetUsr
go

print 'Actualización realizada.'
go