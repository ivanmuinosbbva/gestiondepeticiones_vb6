/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR SuseR_NAME*/
/*
sp_UpdAgrupAlcance 
	493,
	'A82993',
	'DIR',
	'DIR',
	'DIR',
	'MEDIO',
	'PYS',
	'6',
	'6-03'
*/

use GesPet
go

print 'sp_UpdAgrupAlcance'
go

if exists (select * from sysobjects where name = 'sp_UpdAgrupAlcance' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdAgrupAlcance
go

create procedure dbo.sp_UpdAgrupAlcance 
	@agr_nrointerno		int=0,
	@cod_usualta		char(10)='',
	@agr_visibilidad	char(3)='',
	@agr_actualiza		char(3)='',
	@agr_vigente  		char(1)='',
	@cod_direccion 		char(8)='',
	@cod_gerencia  		char(8)='',
	@cod_sector   		char(8)='',
	@cod_grupo   		char(8)=''
as 

	declare @secuencia int
	declare @ret_status     int

	/*    Tabla Temporaria  */
	create table #AgrupArbol(
				secuencia	int,
				nivel 		int,
				agr_nrointerno 	int)

	/* ejecuta el store procedure recursivo que arma la tabla de trabajo */
	select @secuencia=0
	execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT
	if (@ret_status <> 0)
		begin
			if (@ret_status = 30003)
				select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
				return (@ret_status)
		end

	update Agrup set 
		cod_usualta		= @cod_usualta, 
		agr_visibilidad	= @agr_visibilidad,         
		cod_direccion	= @cod_direccion, 
		cod_gerencia	= @cod_gerencia, 
		cod_sector		= @cod_sector, 
		cod_grupo		= @cod_grupo, 
		audit_user		= SuseR_NAME(),   
		audit_date		= getdate()   
	where agr_nrointerno in (select B.agr_nrointerno from #AgrupArbol B)
	 
	if @agr_vigente='N'
		update Agrup set 
			agr_vigente	= @agr_vigente,   
			audit_user	= SuseR_NAME(),   
			audit_date	= getdate()   
		where agr_nrointerno in (select B.agr_nrointerno from #AgrupArbol B)

	if @agr_actualiza<>'NON'
		update Agrup set 
			agr_actualiza	= @agr_actualiza,
			audit_user		= SuseR_NAME(),
			audit_date		= getdate()
		where agr_nrointerno in (select B.agr_nrointerno from #AgrupArbol B)

	return(0)
go

grant execute on dbo.sp_UpdAgrupAlcance to GesPetUsr 
go
