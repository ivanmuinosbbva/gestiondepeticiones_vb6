/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMNovedades'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMNovedades' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMNovedades
go

create procedure dbo.sp_DeleteProyectoIDMNovedades
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@mem_fecha		char(20),
	@mem_campo		char(10),
	@mem_secuencia	smallint,
	@modo			char(3)='DEL'
as
	if @modo = 'DEL'
		begin
			update 
				GesPet.dbo.ProyectoIDMNovedades
			set
				mem_accion	 = 'DEL',
				mem_fecha2	 = getdate(),
				cod_usuario2 = SUSER_NAME()
			where 
				ProjId		= @ProjId and
				ProjSubId	= @ProjSubId and
				ProjSubSId	= @ProjSubSId and
				mem_fecha	= convert(datetime, @mem_fecha) and
				mem_campo	= @mem_campo 
				--mem_secuencia = @mem_secuencia
		end
	else
		delete from
			GesPet.dbo.ProyectoIDMNovedades
		where
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId and
			(@mem_fecha is null or mem_fecha = convert(datetime, @mem_fecha)) and
			(@mem_campo is null or mem_campo = @mem_campo) and
			(@mem_secuencia is null or mem_secuencia = @mem_secuencia)
	return(0)
go

grant execute on dbo.sp_DeleteProyectoIDMNovedades to GesPetUsr
go

print 'Actualización realizada.'
go