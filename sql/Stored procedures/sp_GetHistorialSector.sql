use GesPet
go
print 'sp_GetHistorialSector'
go
if exists (select * from sysobjects where name = 'sp_GetHistorialSector' and sysstat & 7 = 4)
drop procedure dbo.sp_GetHistorialSector
go
create procedure dbo.sp_GetHistorialSector 
    @pet_nrointerno     int=0, 
    @cod_sector         char(8)

as   
declare @hst_nrointerno int
declare @nombre char(50)
declare @estado char(50)

select @nombre=cod_abrev from Sector S where (RTRIM(S.cod_sector)=RTRIM(@cod_sector))

select  @hst_nrointerno=PS.hst_nrointerno_sol,
        @estado = (select EST.nom_estado from Estados EST where RTRIM(PS.cod_estado)=RTRIM(EST.cod_estado))
from    GesPet..PeticionSector PS
where   (@pet_nrointerno=pet_nrointerno) and 
	(RTRIM(PS.cod_sector)=RTRIM(@cod_sector))

if (@hst_nrointerno <> 0) and (exists (select H.hst_nrointerno from HistorialMemo H where H.hst_nrointerno=@hst_nrointerno))
begin
	select @nombre as 'nombre',
		@estado as 'estado',
		H.hst_nrointerno,
		H.hst_secuencia,
		H.mem_texto   
	from HistorialMemo H
	where  (H.hst_nrointerno=@hst_nrointerno) 
	order by H.hst_secuencia   
end
else
begin
	select @nombre as 'nombre',
		@estado as 'estado',
		0 as 'hst_nrointerno',
		0 as 'hst_secuencia',
		'' as 'mem_texto'
	UNION ALL
	select G.nom_grupo as 'nombre',
		estado = (select EST.nom_estado from Estados EST where RTRIM(PG.cod_estado)=RTRIM(EST.cod_estado)),
		H.hst_nrointerno,
		H.hst_secuencia,
		H.mem_texto   
	from HistorialMemo H, PeticionGrupo PG, Grupo G
	where  (@pet_nrointerno=PG.pet_nrointerno) and 
		(RTRIM(@cod_sector)=RTRIM(PG.cod_sector)) and
		(RTRIM(G.cod_grupo)=RTRIM(PG.cod_grupo)) and
		(PG.hst_nrointerno_sol=H.hst_nrointerno) 
	order by hst_nrointerno,hst_secuencia   
end
return(0)   
go



grant execute on dbo.sp_GetHistorialSector to GesPetUsr 
go


