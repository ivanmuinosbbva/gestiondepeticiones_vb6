/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 08.05.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetFabrica'
go

if exists (select * from sysobjects where name = 'sp_GetFabrica' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetFabrica
go

create procedure dbo.sp_GetFabrica
	@cod_fab		char(10)=null
	/*,
	@nom_fab		char(50)=null,
	@estado_fab		char(1)=null
	*/
as
	select 
		p.cod_fab,
		--nom_fabgrp = isnull(LTRIM(RTRIM(tp.TipoFabNom)) + ' - ' + (select LTRIM(RTRIM(g.nom_grupo)) from Grupo g where g.cod_grupo = p.cod_grupo), p.nom_fab),
		nom_fab = case 
			when tp.TipoFabNom is null then LTRIM(RTRIM(p.nom_fab))
			when (select LTRIM(RTRIM(g.nom_grupo)) from Grupo g where g.cod_grupo = p.cod_grupo) is null then LTRIM(RTRIM(p.nom_fab))
			else LTRIM(RTRIM(tp.TipoFabNom)) + ' - ' + (select LTRIM(RTRIM(g.nom_grupo)) from Grupo g where g.cod_grupo = p.cod_grupo)
		end,
		p.nom_fab		as nom_default,
		p.cod_gerencia,
		p.cod_direccion,
		p.cod_sector,
		p.cod_grupo,
		p.estado_fab,
		p.tipo_fab,
		tp.TipoFabNom,
		nom_direccion = (select d.nom_direccion from Direccion d where d.cod_direccion = p.cod_direccion),
		nom_gerencia = (select gr.nom_gerencia from Gerencia gr where gr.cod_gerencia = p.cod_gerencia),
		nom_sector = (select s.nom_sector from Sector s where s.cod_sector = p.cod_sector),
		nom_grupo = (select g.nom_grupo from Grupo g where g.cod_grupo = p.cod_grupo),
		nom_responsable = (select r.nom_recurso from Recurso r where r.cod_direccion = p.cod_direccion and r.cod_gerencia = p.cod_gerencia and r.cod_sector = p.cod_sector and r.cod_grupo = p.cod_grupo and r.flg_cargoarea = 'S')
	from 
		Fabrica p left join
		TipoFabrica tp on (tp.TipoFabId = p.tipo_fab)
	where
		(@cod_fab is null or p.cod_fab = @cod_fab) 
		/*and
		(@nom_fab is null or p.nom_fab = @nom_fab) and
		(@estado_fab is null or p.estado_fab = @estado_fab)
		*/
	order by 1
	return(0)
go

grant execute on dbo.sp_GetFabrica to GesPetUsr
go

print 'Actualización realizada.'
go
