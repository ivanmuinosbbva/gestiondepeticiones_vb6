use GesPet
go

print 'sp_DeletePeticionSector'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionSector' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionSector
go

create procedure dbo.sp_DeletePeticionSector 
    @pet_nrointerno     int, 
    @cod_sector         char(8)=null 
as 
	/* falta validar */ 
	if @cod_sector is null 
		begin 
			delete GesPet..PeticionGrupo 
			  where @pet_nrointerno=pet_nrointerno 

			delete GesPet..PeticionSector 
			  where @pet_nrointerno=pet_nrointerno 
		end 
	else 
		begin 
			if exists (select cod_sector from GesPet..PeticionSector where @pet_nrointerno=pet_nrointerno and RTRIM(cod_sector)=RTRIM(@cod_sector)) 
				begin 
					/* 
					if exists (select cod_sector from GesPet..Recurso where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
					begin 
						raiserror  30011 'Existen Recursos asociados a este Sector'  
						select 30011 as ErrCode , 'Existen Recursos asociados a este Sector'  as ErrDesc 
					return (30011) 
					end 
					if exists (select cod_sector from GesPet..Peticion where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
					begin 
						raiserror  30011 'Existen Peticiones asociadas a este Sector'  
						select 30011 as ErrCode , 'Existen Peticiones asociadas a este Sector'  as ErrDesc 
					return (30011) 
					end 
					*/ 
					delete GesPet..PeticionGrupo 
					  where @pet_nrointerno=pet_nrointerno and 
					  (RTRIM(cod_sector)=RTRIM(@cod_sector)) 

					delete GesPet..PeticionSector 
					  where @pet_nrointerno=pet_nrointerno and 
					  (RTRIM(cod_sector)=RTRIM(@cod_sector)) 
				end 
			else 
				begin 
					raiserror 30001 'Sector Inexistente'   
					select 30001 as ErrCode , 'Sector Inexistente' as ErrDesc   
					return (30001)   
				end 
		end 
	return(0) 
go

grant execute on dbo.sp_DeletePeticionSector to GesPetUsr 
go
