/*
-000- a. FJS 20.01.2010 - Nuevo SP para poner operativas para proceso aquellas que han quedado marcadas como para ejecutar el fin de semana (Ejecución diferida)
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSolicitudesDiferidas'
go

if exists (select * from sysobjects where name = 'sp_UpdateSolicitudesDiferidas' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSolicitudesDiferidas
go

create procedure dbo.sp_UpdateSolicitudesDiferidas
as 
	update 
		GesPet..Solicitudes
	set 
		sol_estado = 'C'
	where 
		sol_estado = 'G'
go
 
grant execute on dbo.sp_UpdateSolicitudesDiferidas to GesPetUsr
go

print 'Actualización realizada.'
