/*
-000- a. FJS 11.10.2011 - Nuevo SP para devolver todos los proyectos de IDM.
*/

use GesPet
go

print 'Creando/actualizando SP: rptProyectosIDM1'
go

if exists (select * from sysobjects where name = 'rptProyectosIDM1' and sysstat & 7 = 4)
	drop procedure dbo.rptProyectosIDM1
go

create procedure dbo.rptProyectosIDM1
as 
	select
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId,
		PRJ.ProjNom,
		PRJ.ProjFchAlta,
		PRJ.ProjCatId,
		ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where PRJ.ProjCatId = x.ProjCatId),
		PRJ.ProjClaseId,
		ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where PRJ.ProjCatId = x.ProjCatId and PRJ.ProjClaseId = x.ProjClaseId),
		'nivel' = 
			case
				when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
				when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
				when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
			end,
		PRJ.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = PRJ.cod_estado),
		PRJ.fch_estado,
		PRJ.usr_estado,
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
		,PRJ.marca
		,PRJ.plan_tyo
		,plan_TYO_nom = case
			when PRJ.plan_tyo = 'S' then 'Si'
			when PRJ.plan_tyo = 'N' then 'No'
			else ''
		end
		,PRJ.empresa
		,nom_empresa = case
			when PRJ.empresa = 'B' then 'Banco'
			when PRJ.empresa = 'C' then 'Consolidar'
			else ''
		end
		,PRJ.area
		,area_nom = case
			when PRJ.area = 'C' then 'Corporativo'
			when PRJ.area = 'R' then 'Regional'
			when PRJ.area = 'L' then 'Local'
			else ''
		end
		,PRJ.clas3
		,clas3_nom = case
			when PRJ.clas3 = 'C' then 'Customer Centric'
			when PRJ.clas3 = 'M' then 'Modelo de distribución'
			when PRJ.clas3 = 'L' then 'LEAN'
			else ''
		end
		,PRJ.semaphore
		,PRJ.avance
		,PRJ.cod_estado2
		,PRJ.fe_modif
		,PRJ.fe_inicio
		,PRJ.fe_fin
		,PRJ.fe_finreprog
		,PRJ.semaphore2																	-- Semáforo (cambio manual)
		,PRJ.sema_fe_modif																-- Fecha de modificación manual del semáforo
		,PRJ.sema_usuario																-- Usuario que modificó manualmente el semáforo
		,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId)			-- Para ordenar la grilla
	from
		GesPet..ProyectoIDM PRJ
	order by
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId
	return(0)
go

grant execute on dbo.rptProyectosIDM1 to GesPetUsr
go

print 'Actualización realizada.'
go
