/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_ChgEstadoGrupo'
go
if exists (select * from sysobjects where name = 'sp_ChgEstadoGrupo' and sysstat & 7 = 4)
drop procedure dbo.sp_ChgEstadoGrupo
go
create procedure dbo.sp_ChgEstadoGrupo 
    @pet_nrointerno     int, 
    @cod_sector         char(8)=null, 
    @cod_grupo      char(8)=null, 
    @cod_estado         char(6)='' 
as 
declare @estadosterminales varchar(255) 
select @estadosterminales = 'CANCEL|RECHAZ|RECHTE|TERMIN' 
    update GesPet..PeticionGrupo 
    set cod_estado  =   @cod_estado     , 
    fe_estado   =   getdate(), 
    cod_situacion =     '', 
    ult_accion  = '', 
    hst_nrointerno_sol= 0, 
    hst_nrointerno_rsp= 0, 
    audit_user = SUSER_NAME(),   
    audit_date = getdate()   
      where @pet_nrointerno=pet_nrointerno and 
        (@cod_sector is null or RTRIM(cod_sector)=RTRIM(@cod_sector)) and 
        (@cod_grupo is null or RTRIM(cod_grupo)=RTRIM(@cod_grupo)) and 
        charindex(cod_estado,@estadosterminales)=0 
return(0) 
go



grant execute on dbo.sp_ChgEstadoGrupo to GesPetUsr 
go


