/*
-001- a. FJS 19.06.2009 - Se modifica porque esta calculando mal cuando se establecen rangos de fechas para la carga de horas.
-002- a. FJS 06.07.2009 - Se modifica porque estaba calculando mal cuando se cargan rangos que incluyen d�as feriados.
*/

/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'Creando/actualizando SP: sp_rptHorasTrabajadasRecurso'
go

if exists (select * from sysobjects where name = 'sp_rptHorasTrabajadasRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHorasTrabajadasRecurso
go

create procedure dbo.sp_rptHorasTrabajadasRecurso 
    @cod_recurso    varchar(10), 
    @fec_desde  smalldatetime, 
    @fec_hasta  smalldatetime 
as 
 
declare @pet_desde  smalldatetime 
declare @pet_hasta  smalldatetime 
declare @pet_horas  numeric(10,2) 
 
declare @aux_fecha  smalldatetime 
declare @aux_horas  numeric(10,2) 
 
declare @dias_totales int 
declare @dias_habiles int 
declare @diasferiados int 
declare @horasxdia numeric(10,2) 
 
 
CREATE TABLE #TmpSalida( 
        fe_infor smalldatetime, 
        horas_trab numeric(10,2), 
        horas_asig numeric(10,2) 
) 
 
/*primero gerenar todos los dias habiles del periodo */ 
select @aux_fecha=@fec_desde 
WHILE (convert(char(8),@aux_fecha,112) <= convert(char(8),@fec_hasta,112)) 
BEGIN 
    /* si no es feriado */ 
    if not exists (select 1 from GesPet..Feriado  where  convert(char(8),fecha,112) = convert(char(8),@aux_fecha,112)) 
    BEGIN 
        insert into #TmpSalida  (fe_infor,horas_trab,horas_asig)     
        values          (@aux_fecha,0,0)  
    END 
    select @aux_fecha = dateadd(day,1,@aux_fecha) 
END 
 
 
DECLARE CursHoras CURSOR FOR 
select  fe_desde, 
    fe_hasta, 
    horas 
from    GesPet..HorasTrabajadas 
where   RTRIM(cod_recurso) = RTRIM(@cod_recurso) and 
    convert(char(8),fe_desde,112) <= convert(char(8),@fec_hasta,112) and 
    convert(char(8),fe_hasta,112) >= convert(char(8),@fec_desde,112) 
for read only 
 
 
OPEN CursHoras 
FETCH CursHoras INTO  
    @pet_desde, 
    @pet_hasta, 
    @pet_horas 
WHILE (@@sqlstatus = 0) 
BEGIN 
    /* dias habiles del periodo para la peticion/tarea  */ 
    select @dias_totales = datediff(day, @pet_desde, @pet_hasta) + 1 
    select @dias_habiles = @dias_totales 
    select @diasferiados = count(1) from GesPet..Feriado 
       where  convert(char(8),fecha,112) >= convert(char(8),@pet_desde,112) and convert(char(8),fecha,112) <= convert(char(8),@pet_hasta,112) 
    if @diasferiados < @dias_totales 
       select @dias_habiles = @dias_habiles - @diasferiados 
    select @horasxdia = convert(numeric(10,2), @pet_horas / @dias_habiles)
    select @aux_fecha=@pet_desde 
     
    /* con cada dia del periodo */ 
    WHILE (convert(char(8),@aux_fecha,112) <= convert(char(8),@pet_hasta,112)) 
    BEGIN 
        /* si esta en el periodo solicitado */ 
        if convert(char(8),@aux_fecha,112) <= convert(char(8),@fec_hasta,112) and convert(char(8),@aux_fecha,112) >= convert(char(8),@fec_desde,112) 
        BEGIN 
            /* si no es feriado */ 
            if not exists (select 1 from GesPet..Feriado  where  convert(char(8),fecha,112) = convert(char(8),@aux_fecha,112)) 
            BEGIN 
                if exists (select 1 from #TmpSalida where convert(char(8),fe_infor,112) = convert(char(8),@aux_fecha,112)) 
                BEGIN 
                    update #TmpSalida 
                    set horas_trab = convert(numeric(10,2), horas_trab + @horasxdia)		-- del -001- a.	-- add -002- a.
					--set horas_trab = horas_trab + convert(numeric(10,2), @pet_horas) / (datediff(day, @pet_desde, @pet_hasta) + 1)	-- add -001- a.	-- del -002- a.
                    where convert(char(8),fe_infor,112) = convert(char(8),@aux_fecha,112)  
                END 
                ELSE 
                BEGIN 
                    insert into #TmpSalida  (fe_infor,horas_trab,horas_asig)     
                    values (@aux_fecha,convert(numeric(10,2), @horasxdia),convert(numeric(10,2), 0))  
                END 
            END 
        END 
        select @aux_fecha = dateadd(day,1,@aux_fecha) 
    END 
    FETCH CursHoras INTO  
        @pet_desde, 
        @pet_hasta, 
        @pet_horas 
END 
CLOSE CursHoras 
DEALLOCATE CURSOR CursHoras 
 
select fe_infor, 
    convert(int,horas_trab) as horas_trab 
from #TmpSalida  
order by fe_infor 
 
return(0)   
go

grant execute on dbo.sp_rptHorasTrabajadasRecurso to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go
