/*
-001- a. FJS 23.12.2008 - Se quita la porci�n de c�digo que elimina la petici�n de la tabla PeticionChangeMan.
-002- a. FJS 17.09.2009 - Se corrige por error (estaba borrando la bandeja diaria).
-003- a. FJS 22.02.2010 - Se elimina de ambos lugares (diario e hist�rico).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionEnviadas'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionEnviadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionEnviadas
go

create procedure dbo.sp_DeletePeticionEnviadas  
	@pet_nrointerno		int
as
	begin tran  
		--{ add -003- a. 
		delete from GesPet..PeticionChangeMan  
		where pet_nrointerno = @pet_nrointerno

		delete from GesPet..PeticionEnviadas  
		where pet_nrointerno = @pet_nrointerno
		--}
		/* del -001- a.
		delete from GesPet..PeticionChangeMan  
		where pet_nrointerno  = @pet_nrointerno
		*/
	commit tran
return(0)  
go

grant Execute  on dbo.sp_DeletePeticionEnviadas to GesPetUsr 
go

print 'Actualizaci�n realizada.'