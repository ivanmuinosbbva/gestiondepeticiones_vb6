/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.05.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMAdjuntos'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMAdjuntos' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMAdjuntos
go

create procedure dbo.sp_UpdateProyectoIDMAdjuntos
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@adj_tipo		char(4),
	@adj_file		char(100),
	@adj_texto		char(250),
	@cod_nivel		char(4),
	@cod_area		char(8)
as
	update 
		GesPet.dbo.ProyectoIDMAdjuntos
	set
		adj_texto   = @adj_texto,
		cod_nivel	= @cod_nivel,
		cod_area	= @cod_area,
		audit_user  = SUSER_NAME(),
		audit_date  = getdate()
	where 
		ProjId		= @ProjId and
		ProjSubId	= @ProjSubId and
		ProjSubSId	= @ProjSubSId and
		adj_tipo	= @adj_tipo and
		adj_file	= @adj_file
go

grant execute on dbo.sp_UpdateProyectoIDMAdjuntos to GesPetUsr
go

print 'Actualización realizada.'
go
