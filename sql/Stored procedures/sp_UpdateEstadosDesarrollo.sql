/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateEstadosDesarrollo'
go

if exists (select * from sysobjects where name = 'sp_UpdateEstadosDesarrollo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEstadosDesarrollo
go

create procedure dbo.sp_UpdateEstadosDesarrollo
	@cod_estado_desa	int,
	@nom_estado_desa	char(50),
	@estado_hab			char(1)
as
	update 
		GesPet.dbo.EstadosDesarrollo
	set
		nom_estado_desa = @nom_estado_desa,
		estado_hab		= @estado_hab
	where 
		(cod_estado_desa = @cod_estado_desa or @cod_estado_desa is null)
	return(0)
go

grant execute on dbo.sp_UpdateEstadosDesarrollo to GesPetUsr
go

print 'Actualización realizada.'
