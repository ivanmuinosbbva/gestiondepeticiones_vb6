/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. - FJS 22.05.2008 - Se agrega la parte de homologación (tres nuevos campos: fe_produccion, fe_suspension y cod_motivo)
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionGrupo'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionGrupo
go

create procedure dbo.sp_InsertPeticionGrupo 
    @pet_nrointerno     int, 
    @cod_grupo          char(8), 
    @fe_ini_plan        smalldatetime=null, 
    @fe_fin_plan        smalldatetime=null, 
    @fe_ini_real        smalldatetime=null, 
    @fe_fin_real        smalldatetime=null, 
	--{ add -001- a.
	@fe_produccion		smalldatetime=null, 
	@fe_suspension		smalldatetime=null, 
	@cod_motivo			int=0,
	--}
    @horaspresup        smallint=0, 
    @cod_estado         char(6)='', 
    @fe_estado			smalldatetime=null, 
    @cod_situacion      char(6)='', 
    @hst_nrointerno_sol int=0, 
    @hst_nrointerno_rsp int=0 
as 
declare @cod_gerencia   char(8), 
		@cod_direccion  char(8), 
		@cod_sector char(8) 
 
select @cod_sector=cod_sector from Grupo where cod_grupo=@cod_grupo 
select @cod_gerencia=cod_gerencia from Sector where cod_sector=@cod_sector 
select @cod_direccion=cod_direccion from Gerencia where cod_gerencia=@cod_gerencia 
 
if exists (select cod_grupo from GesPet..PeticionGrupo where @pet_nrointerno=pet_nrointerno and RTRIM(cod_sector)=RTRIM(@cod_sector) and RTRIM(cod_grupo)=RTRIM(@cod_grupo)) 
	begin 
		raiserror 30001 'Alta de SubSector Existente'   
		select 30001 as ErrCode , 'Alta de SubSector Existente' as ErrDesc   
		return (30001)   
	end 
else 
	begin 
		  insert into GesPet..PeticionGrupo( 
			pet_nrointerno, 
			cod_grupo, 
			cod_sector, 
			cod_gerencia, 
			cod_direccion, 
			fe_ini_plan,     
			fe_fin_plan,     
			fe_ini_real,     
			fe_fin_real,
			--{ add -001- a.
			fe_produccion,
			fe_suspension,
			cod_motivo,
			--}
			horaspresup,     
			cod_estado,  
			fe_estado,   
			cod_situacion, 
			ult_accion, 
			hst_nrointerno_sol, 
			hst_nrointerno_rsp, 
			audit_user,   
			audit_date ) 
		  values(   @pet_nrointerno, 
			@cod_grupo, 
			@cod_sector, 
			@cod_gerencia, 
			@cod_direccion, 
			@fe_ini_plan,    
			@fe_fin_plan,    
			@fe_ini_real,    
			@fe_fin_real,    
			--{ add -001- a.
			@fe_produccion,
			@fe_suspension,
			@cod_motivo,
			--}
			@horaspresup,    
			@cod_estado,     
			@fe_estado,  
			@cod_situacion, 
			'', 
			@hst_nrointerno_sol, 
			@hst_nrointerno_rsp, 
			SUSER_NAME(),   
			getdate())
	end 
return(0) 
go

grant execute on dbo.sp_InsertPeticionGrupo to GesPetUsr 
go

grant execute on dbo.sp_InsertPeticionGrupo to RolGesIncConex
go

sp_procxmode 'sp_InsertPeticionGrupo', anymode
go

print 'Actualización realizada.'