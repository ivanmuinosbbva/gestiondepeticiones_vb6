/*
-000- a. FJS 16.09.2008 - Nuevo SP para devolver todos los proyectos de IDM.
-001- a. FJS 16.06.2009 - Porque no me esta trayendo aquellos que no tienen categor�a ni clase.
-002- a. FJS 17.06.2009 - Se cambia y la funcionalidad utilizada para devolver �ltimos n�meros se pasa a otro SP.
-003- a. FJS 28.06.2010 - Se agregan los nuevos campos de estado, fecha de �ltima modificaci�n y el usuario que la realiz�.
-004- a. FJS 31.08.2010 - Se agrega el atributo Marca.
-005- a. FJS 23.11.2010 - Se agregan los nuevos atributos (pet. 24493).
-006- a. FJS 30.09.2011 - Se quita la marca.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMFicha'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMFicha' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMFicha
go

create procedure dbo.sp_GetProyectoIDMFicha
	@ProjId			int, 
	@ProjSubId		int, 
	@ProjSubsId		int
as 
	select
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId,
		PRJ.ProjNom,
		PRJ.ProjFchAlta,
		PRJ.ProjCatId,
		CAT.ProjCatNom,
		PRJ.ProjClaseId,
		CLS.ProjClaseNom,
		'nivel' = 
			case
				when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
				when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
				when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
			end,
		--{ add -003- a.
		PRJ.cod_estado,
		nom_estado = case
			when PRJ.cod_estado is null then case
				when PRJ.cod_estado2 = 'PLANIF' then 'A Planificar'
				when PRJ.cod_estado2 = 'DESEST' then 'Desestimado'
				when PRJ.cod_estado2 = 'EJECUC' then 'En Ejecuci�n'
				when PRJ.cod_estado2 = 'PRUEBA' then 'En Prueba'
				when PRJ.cod_estado2 = 'TERMIN' then 'Finalizado'
				when PRJ.cod_estado2 = 'SUSPEN' then 'Suspendido'
				end
			else (select x.nom_estado from GesPet..Estados x where x.cod_estado = PRJ.cod_estado)
		end,
		PRJ.fch_estado,
		PRJ.usr_estado,
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
		--}
		,PRJ.marca	-- add -004- a.
		--{ add -005- a.
		,PRJ.plan_tyo
		,plan_TYO_nom = case
			when PRJ.plan_tyo = 'S' then 'Si'
			when PRJ.plan_tyo = 'N' then 'No'
			else ''
		end
		,PRJ.empresa
		,nom_empresa = case
			when PRJ.empresa = 'B' then 'Banco'
			when PRJ.empresa = 'C' then 'Consolidar'
			else ''
		end
		,PRJ.area
		,area_nom = case
			when PRJ.area = 'C' then 'Corporativo'
			when PRJ.area = 'R' then 'Regional'
			when PRJ.area = 'L' then 'Local'
			else ''
		end
		,PRJ.clas3
		,clas3_nom = case
			when PRJ.clas3 = 'C' then 'Customer Centric'
			when PRJ.clas3 = 'M' then 'Modelo de distribuci�n'
			when PRJ.clas3 = 'L' then 'LEAN'
			else ''
		end
		,PRJ.semaphore
		,PRJ.cod_direccion
		,PRJ.cod_gerencia
		,PRJ.cod_sector
		,nom_areasoli = (case
			when PRJ.cod_gerencia is null then 
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion))
			when PRJ.cod_sector is null then
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' � ' + 
				RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia))
			else
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' � ' + 
				RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)) + ' � ' + 
				RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector))
		end)
		,PRJ.cod_direccion_p
		,PRJ.cod_gerencia_p
		,PRJ.cod_sector_p
		,PRJ.cod_grupo_p
		,nom_areaprop = (case
			when PRJ.cod_gerencia_p is null then 
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
			when PRJ.cod_sector_p is null then
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' � ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
			when PRJ.cod_grupo_p is null then
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' � ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
				' � ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
			else
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' � ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
				' � ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
				' � ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
		end)
		,PRJ.avance
		,PRJ.cod_estado2
		,PRJ.fe_modif
		,PRJ.fe_inicio
		,PRJ.fe_fin
		,PRJ.fe_finreprog
		--}
		,PRJ.semaphore2																	-- Sem�foro (cambio manual)
		,PRJ.sema_fe_modif																-- Fecha de modificaci�n manual del sem�foro
		,PRJ.sema_usuario																-- Usuario que modific� manualmente el sem�foro
		,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId)			-- Para ordenar la grilla
	from
		GesPet..ProyectoIDM PRJ left join 
		GesPet..ProyectoCategoria CAT on (PRJ.ProjCatId = CAT.ProjCatId) left join		-- upd -001- a. Se cambia 'inner' por 'left'
		GesPet..ProyectoClase CLS on (PRJ.ProjCatId = CLS.ProjCatId and PRJ.ProjClaseId = CLS.ProjClaseId)
	where
		PRJ.ProjId = @ProjId and
		PRJ.ProjSubId = @ProjSubId and
		PRJ.ProjSubSId = @ProjSubsId
	order by
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId
return(0)
go

grant execute on dbo.sp_GetProyectoIDMFicha to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
