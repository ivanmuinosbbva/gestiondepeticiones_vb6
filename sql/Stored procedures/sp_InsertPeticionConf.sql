/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. - FJS 24.04.2008 - Se cambia el registro del usuario por una variable. Si la variable viene sin datos, se usa la funci�n del sistema para obtenerlo.
-002- a. - FJS 03.07.2008 - Se restablece la restricci�n del nro. de secuencia para permitir m�s de un OK parcial de pruebas del usuario
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionConf'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionConf' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionConf
go

create procedure dbo.sp_InsertPeticionConf
	@pet_nrointerno	int,
	@ok_tipo	char(8),
	@ok_modo	char(3),
	@ok_texto	char(250),
	@ok_secuencia	int
   ,@usuario    char(10)	-- add -001- a.
as

declare @secuencia int 

/* busco la secuencia mayor */ 
if @ok_secuencia=0 
	begin 
		select @secuencia = max(ok_secuencia) from PeticionConf where @pet_nrointerno=pet_nrointerno and RTRIM(ok_tipo)=RTRIM(@ok_tipo) 
		if @secuencia is null 
			select @secuencia = 0 
			select @secuencia = @secuencia + 1 
	end 
else 
	begin 
		select @secuencia = @ok_secuencia 
	end 

if exists (select 
				ok_tipo 
			from 
				PeticionConf 
			where 
				@pet_nrointerno = pet_nrointerno 
				and RTRIM(ok_tipo) = RTRIM(@ok_tipo)
				and ok_secuencia = @secuencia)	-- add -002- a.
	begin
		raiserror 30001 'La confirmacion ya estaba declarada.'
		select 30001 as ErrCode , 'La confirmacion ya estaba declarada.' as ErrDesc
		return (30001)
	end
else
	--{ add -001- a.
	if RTRIM(@usuario) = '' or @usuario is null
		select @usuario = SUSER_NAME()
	--}
	begin
		insert into PeticionConf
		   (pet_nrointerno,
			ok_tipo,
			ok_modo,
			ok_texto,
			audit_user,
			audit_date,
			ok_secuencia)
		values
		   (@pet_nrointerno,
			@ok_tipo,
			@ok_modo,
			@ok_texto,
			@usuario,		-- upd -001- a. Se reemplaza la funci�n SUSER_NAME() por la variable de usuario
			getdate(),
			@secuencia)
	end
return(0)
go

grant execute on dbo.sp_InsertPeticionConf to GesPetUsr
go

grant execute on dbo.sp_InsertPeticionConf to RolGesIncConex
go

sp_procxmode 'sp_InsertPeticionConf', anymode
go

print 'Actualizaci�n realizada.'
go