/* 
-001- a. FJS 22.12.2010 - Se modifica para permitir el cambio de tipo de adjunto.
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_UpdatePeticionAdjunto'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionAdjunto' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionAdjunto
go

create procedure dbo.sp_UpdatePeticionAdjunto
	@pet_nrointerno	int,
	@adj_tipo		char(8),
	@adj_file		char(100),
	@adj_texto		char(250)='',
	--@adj_reltipo		char(8), 
	--@adj_relfile		char(100), 
	@newadj_tipo	char(8)=null,
	@newadj_file	char(100)=null
as
	--{ add
	begin
		update GesPet..PeticionAdjunto
		set 
			adj_tipo  = @newadj_tipo,
			adj_file  = @newadj_file,
			--adj_reltipo	= @adj_reltipo,	 
			--adj_relfile	= @adj_relfile,
			adj_texto = @adj_texto
		where 
			@pet_nrointerno = pet_nrointerno and 
			RTRIM(adj_tipo)=RTRIM(@adj_tipo) and
			RTRIM(adj_file)=RTRIM(@adj_file)
	end		
	/*
	if not exists (select adj_file from PeticionAdjunto where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file) and RTRIM(adj_tipo)=RTRIM(@adj_tipo))
		begin
			raiserror 30001 'El archivo no esta declarado como adjunto.'
			select 30001 as ErrCode , 'El archivo no esta declarado como adjunto.' as ErrDesc
			return (30001)
		end
	else
		begin
			update PeticionAdjunto
				set adj_texto=@adj_texto
				where @pet_nrointerno=pet_nrointerno and RTRIM(adj_file)=RTRIM(@adj_file) and RTRIM(adj_tipo)=RTRIM(@adj_tipo)
		end
	*/
	return(0)
go

grant execute on dbo.sp_UpdatePeticionAdjunto to GesPetUsr
go

print 'Actualización finalizada.'
go