/*
-000- a. FJS 31.10.2008 - Nuevo SP devolver con mejor detalle los eventos del historial de una petición
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHistorialEvento'
go

if exists (select * from sysobjects where name = 'sp_GetHistorialEvento' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHistorialEvento
go

create procedure dbo.sp_GetHistorialEvento
      @pet_nrointerno   int=null,
      @hst_nrointerno   int=null,
	  @hst_cod_evento	varchar(8)=null,
	  @hst_fecha_desde	smalldatetime=null,
	  @hst_fecha_hasta	smalldatetime=null
as
    select
		HIS.pet_nrointerno, 
		HIS.hst_nrointerno, 
		HIS.hst_fecha, 
		HIS.cod_evento, 
		HIS.pet_estado, 
		HIS.cod_sector, 
		HIS.sec_estado, 
		HIS.cod_grupo, 
		HIS.gru_estado, 
		HIS.replc_user, 
		HIS.audit_user
    from 
		GesPet..Historial HIS
    where 
		(@pet_nrointerno is null or HIS.pet_nrointerno=@pet_nrointerno) and   
		(@hst_nrointerno is null or HIS.hst_nrointerno=@hst_nrointerno) and
		(@hst_cod_evento is null or HIS.cod_evento=@hst_cod_evento) and
		(@hst_fecha_desde is null or HIS.hst_fecha>=@hst_fecha_desde) and 
		(@hst_fecha_hasta is null or HIS.hst_fecha<=@hst_fecha_hasta)
    order by 
		HIS.pet_nrointerno,
		HIS.hst_nrointerno 
go

grant execute on dbo.sp_GetHistorialEvento to GesPetUsr
go

print 'Actualización realizada.'
