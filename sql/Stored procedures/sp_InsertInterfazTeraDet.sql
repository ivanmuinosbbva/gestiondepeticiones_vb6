/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 21.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertInterfazTeraDet'
go

if exists (select * from sysobjects where name = 'sp_InsertInterfazTeraDet' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertInterfazTeraDet
go

create procedure dbo.sp_InsertInterfazTeraDet
	@dbName			varchar(30),
	@objname		varchar(50),
	@objfield		varchar(50),
	@objfieldhab	char(1),
	@objfielddesc	varchar(255)
as
	insert into GesPet.dbo.InterfazTeraDet (
		dbName,
		objname,
		objfield,
		objfieldhab,
		objfielddesc,
		objultact)
	values (
		@dbName,
		@objname,
		@objfield,
		@objfieldhab,
		@objfielddesc,
		getdate())
go

grant execute on dbo.sp_InsertInterfazTeraDet to GesPetUsr
go

print 'Actualización realizada.'
go
