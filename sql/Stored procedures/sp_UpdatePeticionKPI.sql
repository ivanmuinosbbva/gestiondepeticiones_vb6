/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 23.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionKPI'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionKPI' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionKPI
go

create procedure dbo.sp_UpdatePeticionKPI
	@pet_nrointerno		int,
	@pet_kpiid			int,
	@pet_kpidesc		varchar(150),
	@kpi_unimedId		char(10),
	@valorEsperado		numeric(10,2),
	@valorPartida		numeric(10,2),
	@tiempoEsperado		numeric(10,2),
	@unimedTiempo		char(10)
as
if not exists (
		select pet_nrointerno 
		from GesPet..PeticionKPI 
		where 
			pet_nrointerno = @pet_nrointerno and
			pet_kpiid = @pet_kpiid)
		
		insert into GesPet.dbo.PeticionKPI (
			pet_nrointerno,
			pet_kpiid,
			pet_kpidesc,
			kpi_unimedId,
			valorEsperado,
			valorPartida,
			tiempoEsperado,
			unimedTiempo,
			audit_fecha,
			audit_usuario)
		values (
			@pet_nrointerno,
			@pet_kpiid,
			@pet_kpidesc,
			@kpi_unimedId,
			@valorEsperado,
			@valorPartida,
			@tiempoEsperado,
			@unimedTiempo,
			getdate(),
			SUSER_NAME())
	else
		update GesPet..PeticionKPI
		set 
			pet_kpidesc		= @pet_kpidesc,
			kpi_unimedId	= @kpi_unimedId,
			valorEsperado	= @valorEsperado,
			valorPartida	= @valorPartida,
			tiempoEsperado	= @tiempoEsperado,
			unimedTiempo	= @unimedTiempo,
			audit_fecha		= getdate(),
			audit_usuario	= SUSER_NAME()
		where 
			pet_nrointerno = @pet_nrointerno and
			pet_kpiid = @pet_kpiid
	
go

grant execute on dbo.sp_UpdatePeticionKPI to GesPetUsr
go

print 'Actualización realizada.'
go
