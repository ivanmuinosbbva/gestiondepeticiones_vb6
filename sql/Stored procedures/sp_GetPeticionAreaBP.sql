/* 
-001- a. FJS 27.06.2007 - Se agrega a la consulta resultado los campos clase de petici�n e implicancia de IT.
-002- a. FJS 11.07.2007 - Se agrega una nueva columna (pet_sox001) al conjunto de resultados.
-003- a. FJS 11.09.2008 - Se agrega la columna de regulatorio.
-004- a. FJS 11.07.2012 - Se agrega el campo para indentificar peticiones vinculadas a la mitigaci�n de factores de Riesgo Operacional (pet_ro).
-005- a. FJS 29.01.2016 - Nuevo: se agrega funcionalidad para la nueva gerencia BPE.
-006- a. FJS 29.06.2016 - Nuevo: nuevo campo calculado para determinar la puntuaci�n de una petici�n (basado en f�rmula de valorizaci�n).
-007- a. FJS 29.06.2016 - Nuevo: nuevo campo calculado para determinar la categor�a de una petici�n.
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetPeticionAreaBP'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionAreaBP' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionAreaBP
go

create procedure dbo.sp_GetPeticionAreaBP
	@pet_nroasignado	int=0,
	@cod_bpar			varchar(10),
	@cod_estado			varchar(250)=null,
	@anx_especial		varchar(1)=null,
	@agr_nrointerno		int=0,
	@prio_ejecuc		int=0,
	@gestion			char(8)
as
	declare @secuencia		int
	declare @ret_status     int
	
	/*
	--{ add -006- a.
	declare @sum_impacto			real
	declare @sum_facilidad			real
	declare @sum_impacto_facilidad	real

	select @sum_impacto = SUM(d.driverValor)
	from Drivers d
	where d.indicadorId = 1		-- Impacto

	select @sum_facilidad = SUM(d.driverValor)
	from Drivers d
	where d.indicadorId = 2		-- Impacto

	select @sum_impacto_facilidad = @sum_impacto + @sum_facilidad
	--}
	*/

	create table #AgrupXPetic(
		pet_nrointerno 	int)

	create table #AgrupArbol(
				secuencia		int,
				nivel 			int,
				agr_nrointerno 	int)
	
	--{ add -007- a.
	create table #tablaCriterio (
		pet_nrointerno		int,
		categoria			char(6))

	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	--}

	--{ add -007- a. Esto es para obtener los agrupamientos de SDA
	declare	@agr_SDA	int
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
			
			INSERT INTO #tablaCriterio
			select 
				p.pet_nrointerno,
				categoria = case
					when p.cod_tipo_peticion in ('AUI','AUX') OR p.pet_regulatorio = 'S' then '1. REG'
					when exists (select x.pet_nrointerno from #AgrupXPeticSDA x where x.pet_nrointerno = p.pet_nrointerno) then '2. SDA'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then '3. PES'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then '4. ENG'
					when p.cod_clase in ('OPTI','CORR','SPUF','ATEN') then '6. CSI'
					when exists (
						 select count(1) 
						 from ProyectoIDM px 
						 where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
						 having count(1) > 0) OR (p.valor_impacto <> 0 OR p.valor_facilidad <>0) then '5. PRI'
					when p.cod_clase = 'SINC' then '999'
					else '7. SPR'
				end
			from Peticion p 
		end
	--}

	/* esto es si se pidio agrupamiento */
	if @agr_nrointerno<>0
		begin
			truncate table #AgrupArbol
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT
			if (@ret_status <> 0)
				begin
					if (@ret_status = 30003)
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
					return (@ret_status)
				end
			insert into #AgrupXPetic (pet_nrointerno)
				select	AP.pet_nrointerno
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where AA.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_nrointerno=AP.agr_nrointerno and
					AG.agr_vigente='S'
		end

	/* son solamente las del BPAR */
	if @anx_especial = 'S'
		begin
			select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.fe_pedido,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
				PET.cod_clase,
				PET.pet_imptech,
				PET.pet_sox001,	
				cod_area_hij='',
				nom_area_hij='',
				cod_estado_hij='',
				nom_estado_hij='',
				cod_situacion_hij='',
				nom_situacion_hij='',
				fe_ini_plan_hij=null,
				fe_fin_plan_hij=null,
				horaspresup_hij=0,
				PET.pet_regulatorio,
				PET.pet_ro, 	
				pet_riesgo = (case
					when PET.pet_ro = 'A' then 'Alta'
					when PET.pet_ro = 'M' then 'Media'
					when PET.pet_ro = 'B' then 'Baja'
					else 'No'
				end)
				--,PET.puntuacion
				,c.categoria	-- add -009- a.
				,prioridadBPE = ISNULL((select ISNULL(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno),999999)
				,analisis = case 
					when horaspresup > 0 then '�'
					else '-'
				end,
				planificado = case
					when (PET.fe_ini_plan is not null and PET.fe_fin_plan is not null) then '�'
					else '-'
				end
			from  GesPet..Peticion PET inner join
					#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where	
				(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
				((@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0) OR (
				 PET.cod_clase in ('NUEV','EVOL') and 
				 exists (select pv.valitemvalor
				  from PeticionValidacion pv
				  where pv.pet_nrointerno = PET.pet_nrointerno and pv.valitemvalor not in ('S','N')))) and
				((RTRIM(PET.cod_bpar)=RTRIM(@cod_bpar) AND NOT PET.pet_nrointerno in (
					select ps.pet_nrointerno 
					from PeticionSector ps 
					where ps.pet_nrointerno = PET.pet_nrointerno)) OR (RTRIM(PET.cod_BPE) = RTRIM(@cod_bpar))) and 
				(@gestion is null OR PET.pet_driver = @gestion)
			UNION ALL
			select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.fe_pedido,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				--nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
				nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
				PET.cod_clase,
				PET.pet_imptech,
				PET.pet_sox001,	
				cod_area_hij='',
				nom_area_hij='',
				cod_estado_hij='',
				nom_estado_hij='',
				cod_situacion_hij='',
				nom_situacion_hij='',
				fe_ini_plan_hij=null,
				fe_fin_plan_hij=null,
				horaspresup_hij=0,
				PET.pet_regulatorio,
				pet_ro = isnull(PET.pet_ro,'-'),
			    pet_riesgo = (case
					when PET.pet_ro = 'A' then 'Alta'
					when PET.pet_ro = 'M' then 'Media'
					when PET.pet_ro = 'B' then 'Baja'
					else 'No'
			   end)
			   --,PET.puntuacion
				,c.categoria	-- add -009- a.
				,prioridadBPE = ISNULL((select ISNULL(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno),999999)
				,analisis = case 
					when horaspresup > 0 then '�'
					else '-'
				end,
				planificado = case
					when (PET.fe_ini_plan is not null and PET.fe_fin_plan is not null) then '�'
					else '-'
				end
			from GesPet..Peticion PET inner join
					#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where   
				(RTRIM(PET.cod_estado)='ANEXAD') and
				(PET.pet_nroanexada in (
					select XP.pet_nrointerno 
					from GesPet..Peticion XP 
					where 
						PET.pet_nroanexada=XP.pet_nrointerno and 
						@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(XP.cod_estado),RTRIM(@cod_estado))>0))
				--and (@prio_ejecuc > 1 AND PET.prioridadBPE > 0)
			order by
				PET.prioridadBPE
			return(0)
		end
	else
		begin
			select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.cod_tipo_peticion,
				PET.titulo,
				PET.fe_pedido,
				PET.prioridad,
				PET.importancia_cod,
				nom_importancia = isnull((select Ip.nom_importancia from Importancia Ip where RTRIM(Ip.cod_importancia)=RTRIM(PET.importancia_cod)),''),
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.horaspresup,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				--nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_bpar),
				nom_bpe = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
				PET.cod_estado,
				nom_estado = (select ESTPET.nom_estado from Estados ESTPET where RTRIM(ESTPET.cod_estado)=RTRIM(PET.cod_estado)),
				PET.cod_situacion,
				nom_situacion=(select SITPET.nom_situacion from Situaciones SITPET where RTRIM(PET.cod_situacion)=RTRIM(SITPET.cod_situacion)),
				PET.cod_clase,
				PET.pet_imptech,
				PET.pet_sox001,
				cod_area_hij='',
				nom_area_hij='',
				cod_estado_hij='',
				nom_estado_hij='',
				cod_situacion_hij='',
				nom_situacion_hij='',
				fe_ini_plan_hij=null,
				fe_fin_plan_hij=null,
				horaspresup_hij=0
				,PET.pet_regulatorio	-- add -003- a.
				,pet_ro = isnull(PET.pet_ro,'-')
			    ,pet_riesgo = (case
					when PET.pet_ro = 'A' then 'Alta'
					when PET.pet_ro = 'M' then 'Media'
					when PET.pet_ro = 'B' then 'Baja'
					else 'No'
				end)
				--,PET.puntuacion
				,c.categoria			-- add -009- a.
				,prioridadBPE = ISNULL((select ISNULL(pc.prioridad,999999) from PeticionPlancab pc where pc.pet_nrointerno = PET.pet_nrointerno),999999)
				,analisis = case 
					when horaspresup > 0 then '�'
					else '-'
				end,
				planificado = case
					when (PET.fe_ini_plan is not null and PET.fe_fin_plan is not null) then '�'
					else '-'
				end
			from  GesPet..Peticion PET inner join
					#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
			where
				(@pet_nroasignado=0 or @pet_nroasignado=PET.pet_nroasignado) and
				(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and 
				--(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0) and
				((@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0) OR (
				 PET.cod_clase in ('NUEV','EVOL') and 
				 exists (select pv.valitemvalor
				  from PeticionValidacion pv
				  where pv.pet_nrointerno = PET.pet_nrointerno and pv.valitemvalor not in ('S','N')))) and
				((RTRIM(PET.cod_bpar)=RTRIM(@cod_bpar) AND NOT PET.pet_nrointerno in (
					select ps.pet_nrointerno 
					from PeticionSector ps 
					where ps.pet_nrointerno = PET.pet_nrointerno)) OR RTRIM(PET.cod_bpar)=RTRIM(@cod_bpar) OR (RTRIM(PET.cod_BPE) = RTRIM(@cod_bpar))) and 
				(@gestion is null OR PET.pet_driver = @gestion)
			order by 
				PET.prioridadBPE	ASC
				--PET.puntuacion	DESC
		return(0)
	end
	return(0)
go

grant execute on dbo.sp_GetPeticionAreaBP to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go

 /*
si @pet_nroasignado<>0 la condicion se SUMA a las condiciones del area
por lo tanto si se quiere ver por numero, solamente se mostrara si pertenece
a la jerarquia de areas
idem @agr_nrointerno,@prio_ejecuc<>0
 */
/*
modo recurso
    SOLI/REFE/SUPE/AUTO/ADMI/CSEC/CGRU/CDIR/CGER etc
cod nivel
    BBVA/DIRE/GERE/SECT/GRUP
cod area
    GRUP/SECT/GERE/DIRE/BBVA
*/