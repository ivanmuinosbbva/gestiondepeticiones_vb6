use GesPet
go

print 'sp_GetPeticionEnviadas'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionEnviadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionEnviadas
go

create procedure dbo.sp_GetPeticionEnviadas 
    (@pet_nrointerno    int,    
     @pet_sector        char(8),    
     @pet_grupo         char(8))   
as     
   
select  
    pet_nrointerno, 
    pet_sector, 
    pet_grupo, 
    pet_record, 
    pet_date, 
    pet_usrid,  
    pet_done 
from  
    GesPet..PeticionEnviadas 
where 
	(pet_nrointerno = @pet_nrointerno OR @pet_nrointerno IS NULL) and  
    (pet_sector = @pet_sector OR @pet_sector IS NULL) and  
    (pet_grupo = @pet_grupo OR @pet_grupo IS NULL) 
 return(0) 
 
go

grant Execute  on dbo.sp_GetPeticionEnviadas to GesPetUsr 
go
