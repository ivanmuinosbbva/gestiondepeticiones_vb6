/*
-001- a. FJS 21.09.2009 - Antes de agregar la petici�n a la bandeja diaria de las peticiones v�lidas para pasajes a Producci�n, la elimino directamente
						  de la tabla de peticiones "En ejecuci�n".
-002- a. FJS 22.02.2010 - Se modifica el proceso de autodepuraci�n de la bandeja diaria: se eliminan de la misma, peticiones en alguno de los estados
						  terminales que han superado el plazo m�ximo parametrizado.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPetChangeMan'
go

if exists (select * from sysobjects where name = 'sp_InsertPetChangeMan' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPetChangeMan
go

create procedure dbo.sp_InsertPetChangeMan
    (@pet_nrointerno    int,   
     @pet_sector        char(8),   
     @pet_grupo         char(8))  
as    
	set dateformat ymd  
	  
	declare @FechaHoy varchar(8)  
	declare @pet_record varchar(80)
	declare @Dias int 

	-- Establece la cantidad de d�as a evaluar seg�n par�metro
	set @Dias = (select var_numero from GesPet..Varios where var_codigo = 'CGMPRM1')

	-- Autodepuraci�n seg�n par�metro
	/* -- del -002- a.
	delete
	from dbo.PeticionChangeMan  
	where datediff(day, pet_date, getdate()) > @Dias
	*/
	--{ add -002- a.
	insert into GesPet..PeticionMemo 
		select 
			a.pet_nrointerno,
			'PETVALIDA',
			0,
			'La petici�n n� ' + rtrim(ltrim(convert(char(10), b.pet_nroasignado))) + 
			' ha dejado de ser v�lida el ' + convert(char(10), getdate(), 103) + ' para pasajes a Prod. porque ya transcurrieron m�s de ' + rtrim(ltrim(convert(char(10), @Dias))) + 
			' d�as desde que alcanz� un estado terminal (' + rtrim(ltrim(b.cod_estado)) + ')'
		from 
			GesPet..PeticionChangeMan a left join 
			GesPet..Peticion b on a.pet_nrointerno = b.pet_nrointerno
		where
			b.cod_estado in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and
			datediff(day, b.fe_estado, getdate()) > @Dias

	delete
	from dbo.PeticionChangeMan
	where pet_nrointerno in (
		select 
			a.pet_nrointerno
		from 
			GesPet..PeticionChangeMan a left join 
			GesPet..Peticion b on a.pet_nrointerno = b.pet_nrointerno
		where
			b.cod_estado in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and
			datediff(day, b.fe_estado, getdate()) > @Dias)
	--}

	--{ add -001- a.
	-- Se borra si existiera de la tabla de peticiones en ejecuci�n (PeticionesEnviadas2)
	delete
	from GesPet..PeticionEnviadas2
	where pet_nrointerno = @pet_nrointerno
	--}
	 
	-- Agregado a la bandeja diaria (si ya existe, entonces se actualizan los datos correspondientes)
	if not exists (select pet_nrointerno from GesPet..PeticionChangeMan 
					where 
						pet_nrointerno = @pet_nrointerno and 
						pet_sector = @pet_sector and 
						pet_grupo = @pet_grupo) 
		begin
			insert into dbo.PeticionChangeMan
				select 
					pet_nrointerno, 
					pet_sector, 
					pet_grupo, 
					pet_record, 
					pet_date  
				from 
					GesPet.dbo.PeticionEnviadas  
				where 
					pet_nrointerno  = @pet_nrointerno and  
					pet_sector		= @pet_sector and  
					pet_grupo		= @pet_grupo 
			
			return(0)
		end
	else 
		begin 
			set @pet_record = (select pet_record from GesPet.dbo.PeticionEnviadas where pet_nrointerno = @pet_nrointerno and pet_sector = @pet_sector and pet_grupo = @pet_grupo) 

			update
				dbo.PeticionChangeMan 
			set 
				 pet_record = @pet_record 
			where  
				pet_nrointerno = @pet_nrointerno and  
				pet_sector = @pet_sector and  
				pet_grupo = @pet_grupo 
			
			return(0)  
		end 
go

grant Execute  on dbo.sp_InsertPetChangeMan to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go