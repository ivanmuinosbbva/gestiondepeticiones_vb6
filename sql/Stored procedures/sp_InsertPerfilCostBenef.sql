use GesPet
go
print 'sp_InsertPerfilCostBenef'
go
if exists (select * from sysobjects where name = 'sp_InsertPerfilCostBenef' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertPerfilCostBenef
go
create procedure dbo.sp_InsertPerfilCostBenef
	@cod_PerfilCostBenef	char(8),
	@nom_PerfilCostBenef	char(30),
	@flg_habil		char(1)=null

as

if exists (select cod_PerfilCostBenef from GesPet..PerfilCostBenef where RTRIM(cod_PerfilCostBenef) = RTRIM(@cod_PerfilCostBenef))
	begin 
		raiserror 30011 'Codigo de Perfil Costo Beneficio Existente'   
		select 30011 as ErrCode , 'Codigo de Perfil Costo Beneficio Existente' as ErrDesc   
		return (30011)   
	end
else
	begin 
	insert into GesPet..PerfilCostBenef
			(cod_PerfilCostBenef, nom_PerfilCostBenef, flg_habil)
		values
			(@cod_PerfilCostBenef, @nom_PerfilCostBenef, @flg_habil)
	end 
return(0)
go


grant execute on dbo.sp_InsertPerfilCostBenef to GesPetUsr
go
