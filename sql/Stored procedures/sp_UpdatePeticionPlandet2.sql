/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlandet2'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlandet2' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlandet2
go

create procedure dbo.sp_UpdatePeticionPlandet2
	@per_nrointerno		int=null,
	@pet_nrointerno		int=null,
	@cod_grupo			char(8)=null,
	@plan_desaestado	int=null,
	@plan_motivsusp		int=null
as
	update 
		GesPet.dbo.PeticionPlandet
	set
		plan_desaestado = @plan_desaestado,
		plan_motivsusp	= @plan_motivsusp
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(cod_grupo = @cod_grupo or @cod_grupo is null)
	return(0)
go

grant execute on dbo.sp_UpdatePeticionPlandet2 to GesPetUsr
go

print 'Actualización realizada.'
