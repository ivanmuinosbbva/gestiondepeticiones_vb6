/* Documentaci�n: 

El prop�sito de este programa es devolver un conjunto de resultados enumerando uno o m�s recursos con posibilidad de tomar acci�n sobre
la petici�n solicitada (en la variable @pet_nrointerno) para un estado posible (@cod_estado).

L�gica program�tica:

a. 
b. Controla que exista la petici�n solicitada (@pet_nrointerno ). Si no existe, dispara un evento de error y aborta el SP.
c. Guarda en variables locales el c�digo de direcci�n, gerencia y sector de la petici�n solicitada.


*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetRecursoActuantePeticion'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoActuantePeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoActuantePeticion
go

create procedure dbo.sp_GetRecursoActuantePeticion
	@pet_nrointerno	int,
	@cod_estado	 char(6)=null
as
	declare @cod_direccion	char(8)
	declare @cod_gerencia	char(8)
	declare @cod_sector	char(8)
	declare @flg_petici	char(1)

	select @flg_petici=RTRIM(flg_petici) from GesPet..Estados where RTRIM(cod_estado) = RTRIM(@cod_estado)

	if not exists (select pet_nrointerno from GesPet..Peticion where pet_nrointerno = @pet_nrointerno)
		begin
			select 30010 as ErrCode , 'Peticion Inexistente' as ErrDesc
			raiserror  30010 'Peticion Inexistente'
			return (30010)
		end

	select  
		@cod_direccion=cod_direccion,
		@cod_gerencia=cod_gerencia,
		@cod_sector=cod_sector
	from GesPet..Peticion where pet_nrointerno = @pet_nrointerno

	select  
		Re.cod_recurso,
		Re.nom_recurso,
		Rp.cod_perfil,
		nom_perfil = (select Pe.nom_perfil from Perfil Pe where RTRIM(Pe.cod_perfil) = RTRIM(Rp.cod_perfil)),
		Rp.cod_nivel,
		Rp.cod_area
	from
		GesPet..RecursoPerfil Rp, GesPet..Recurso Re
	where
		(RTRIM(Rp.cod_recurso) = RTRIM(Re.cod_recurso)) and
		(RTRIM(Re.estado_recurso)='A') and
		((RTRIM(Rp.cod_perfil) <> 'SADM') or (RTRIM(@flg_petici)='1')) and
		Rp.cod_perfil in (select DISTINCT cod_perfil from GesPet..AccionesPerfil where  (RTRIM(cod_accion) = 'PCHGEST' and RTRIM(cod_estado) = RTRIM(@cod_estado))) and
		((Rp.cod_nivel='BBVA') or
		(Rp.cod_nivel='DIRE' and RTRIM(@cod_direccion)=RTRIM(Rp.cod_area)) or
		(Rp.cod_nivel='GERE' and RTRIM(@cod_gerencia)=RTRIM(Rp.cod_area)) or
		(Rp.cod_nivel='SECT' and RTRIM(@cod_sector)=RTRIM(Rp.cod_area)))

	return(0)
go

grant execute on dbo.sp_GetRecursoActuantePeticion to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go