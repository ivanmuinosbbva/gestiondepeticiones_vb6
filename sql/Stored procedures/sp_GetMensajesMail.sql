/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 17.11.2009 - Bug: existe un problema cuando un hay definida una delegaci�n para un recurso que recibe correo. Cuando el rango de fechas delegadas ha expirado,
						  no se estan enviando los correos al recurso que deleg� originalmente. Se estan perdiendo esos avisos.
-002- a. FJS 29.08.2011 - Agregado: se envian mensaje de correo a un pool espec�fico de SI.
-003- a. FJS 29.08.2016 - Nuevo: se adapta para el envio de mensajes a BPE cuando replanifica una petici�n.
*/


use GesPet
go

print 'Creando/actualizando SP: sp_GetMensajesMail'
go

if exists (select * from sysobjects where name = 'sp_GetMensajesMail' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetMensajesMail
go

create procedure dbo.sp_GetMensajesMail 
    @fecdesde varchar(8)='NULL',
    @fechasta varchar(8)='NULL'
as 
	/* solo con recursos ACTIVOS */

	set dateformat ymd 
	SET NOCOUNT ON

	declare @cod_recurso	char(10)
	declare @nom_recurso	char(50) 
	declare @email			varchar(60)
	declare @euser			char(1) 
	declare @dlg_recurso    char(10)
	declare @dlg_desde      smalldatetime
	declare @dlg_hasta      smalldatetime
	declare @dlg_email		varchar(60)
	declare @dlg_euser		char(1) 
	declare @domain			char(50)			-- add -002- a.

	declare @cod_perfil		char(4) 
	declare @cod_nivel		char(4) 
	declare @cod_area		char(8) 

	--select @domain = '@correo1.arg.igrupobbva'	-- add -002- a.
	select @domain = '@bbva.com'					-- add -002- a.

	-- Se crea un cursor de Recursos ordenado por nombre de recurso que se encuentren en estado Activo
	declare CursRecursos Cursor for 
		select 
			R.cod_recurso, 
			R.nom_recurso, 
			R.email,
			R.euser,
			R.dlg_recurso, 
			R.dlg_desde, 
			R.dlg_hasta,
			dlg_email = (select D.email from GesPet..Recurso D where D.cod_recurso=R.dlg_recurso), 
			dlg_euser = (select D.euser from GesPet..Recurso D where D.cod_recurso=R.dlg_recurso)
		from 
			GesPet..Recurso R
		where 	
			(RTRIM(R.estado_recurso) = 'A') 
		order by 
			nom_recurso 
		for Read Only  
	
	CREATE TABLE #TmpSalida( 
		cod_recurso		char(10)	not null,
		email			char(60)	not null,
		msg_nrointerno	int			not null, 
		pet_nrointerno	int			not null, 
		cod_perfil		varchar(10) not null, 
		cod_direccion	varchar(10) not null, 
		cod_gerencia	varchar(10) null, 
		cod_sector		varchar(10) null, 
		cod_grupo		varchar(10) null) 

	open CursRecursos  
	fetch CursRecursos into @cod_recurso, @nom_recurso, @email, @euser, @dlg_recurso, @dlg_desde, @dlg_hasta, @dlg_email, @dlg_euser
	if @@sqlstatus <> 2		-- Comprueba que el cursor tenga datos
		begin 
			while @@sqlstatus = 0
				begin  
					if RTRIM(@euser) = 'N'		-- Marca de 'Recibe correo' desactivada: entonces no debe mandarle mails
						begin
							select @email = ''
						end
					if '|' + RTRIM(@dlg_recurso) <> '|'	-- Entonces existe un delegado designado para este recurso
						begin
							-- Si est� dentro del rango de fechas desde-hasta que lo habilita para ser delegado
							if convert(char(8), @dlg_desde,112) <= convert(char(8),getdate(),112) and convert(char(8), @dlg_hasta,112) >= convert(char(8),getdate(),112)
								begin
									-- Si el recurso real est� delegado en otro, le borro la direccion de email al recurso que deleg�...
									select @email = ''
									if ISNULL(@dlg_euser,'S') <> 'N'	-- Si no esta indicado explicitamente el NO enviar correo
									begin
										-- Y le asigno la del delegando, si asi lo quiso
										If  '|' + RTRIM(ISNULL(@dlg_email,'')) <> '|'
											begin
												select @email = @dlg_email
											End
									end
								end
						end
					-- Si existe una direcci�n de email (ya por el recurso, ya por el delegado)
					If '|' + RTRIM(@email) <> '|'
						begin
							/*		print	'%1! %2! %3! %4! %5!',
										@nom_recurso,
										@euser,
										@email,
										@dlg_euser,
										@dlg_email
							*/
							-- Para cada recurso, busca los perfiles/area
							-- con cada perfil busca los mensajes para ese perfil/area
							
							DECLARE CursPerfil CURSOR FOR 
								select cod_perfil, cod_nivel, cod_area 
								from RecursoPerfil 
								where RTRIM(cod_recurso)=RTRIM(@cod_recurso) 
								for read only 
							
							OPEN CursPerfil FETCH CursPerfil INTO @cod_perfil, @cod_nivel, @cod_area 
								WHILE (@@sqlstatus = 0) 
									BEGIN 
										--{ add
										-- Nivel: BBVA
										insert #TmpSalida 
											select 
												@cod_recurso,
												@email,
												Me.msg_nrointerno, 
												Me.pet_nrointerno, 
												Me.cod_perfil, 
												'',
												'',
												'',
												''
											from GesPet..Mensajes Me
											where 
												Me.cod_perfil = @cod_perfil and 
												(@cod_nivel='BBVA' and @cod_area = 'BBVA') and 
												(@fecdesde='NULL' or convert(char(8),Me.fe_mensaje,112)>= @fecdesde) and
												(@fechasta='NULL' or convert(char(8),Me.fe_mensaje,112)<= @fechasta)

										-- Primero a nivel: Sector
										insert #TmpSalida 
										select  
											@cod_recurso,
											@email,
											Me.msg_nrointerno, 
											Me.pet_nrointerno, 
											Me.cod_perfil, 
											Ge.cod_direccion, 
											Gr.cod_gerencia, 
											Gr.cod_sector, 
											'' 
										from
											GesPet..Mensajes Me, 
											GesPet..Sector  Gr (index idx_Sector), 
											GesPet..Gerencia Ge 
										where   
											(Me.cod_nivel='SECT') and 
											(Me.cod_perfil = @cod_perfil) and 
											(@fecdesde='NULL' or convert(char(8),Me.fe_mensaje,112)>= @fecdesde) and
											(@fechasta='NULL' or convert(char(8),Me.fe_mensaje,112)<= @fechasta) and
											(RTRIM(Gr.cod_sector)=RTRIM(Me.cod_area)) and 
											(RTRIM(Gr.cod_gerencia)=RTRIM(Ge.cod_gerencia)) and 
											((@cod_nivel='DIRE' and RTRIM(Ge.cod_direccion)=RTRIM(@cod_area)) or 
											(@cod_nivel='GERE' and RTRIM(Ge.cod_gerencia)=RTRIM(@cod_area)) or 
											(@cod_nivel='SECT' and RTRIM(Gr.cod_sector)=RTRIM(@cod_area))) 

										-- Segundo a nivel: Grupo
										insert #TmpSalida 
										select  @cod_recurso,
											@email,
											Me.msg_nrointerno, 
											Me.pet_nrointerno, 
											Me.cod_perfil, 
											Ge.cod_direccion, 
											Gr.cod_gerencia, 
											Gr.cod_sector, 
											Su.cod_grupo 
										from     
											GesPet..Mensajes Me, 
											GesPet..Grupo  Su, 
											GesPet..Sector  Gr (index idx_Sector), 
											GesPet..Gerencia Ge 
										where   
											(Me.cod_nivel='GRUP') and 
											(Me.cod_perfil = @cod_perfil) and 
											(@fecdesde='NULL' or convert(char(8),Me.fe_mensaje,112)>= @fecdesde) and
											(@fechasta='NULL' or convert(char(8),Me.fe_mensaje,112)<= @fechasta) and
											(RTRIM(Su.cod_grupo)=RTRIM(Me.cod_area)) and 
											(RTRIM(Gr.cod_sector)=RTRIM(Su.cod_sector)) and 
											(RTRIM(Gr.cod_gerencia)=RTRIM(Ge.cod_gerencia)) and 
											((@cod_nivel='DIRE' and RTRIM(Ge.cod_direccion)=RTRIM(@cod_area)) or 
											(@cod_nivel='GERE' and RTRIM(Ge.cod_gerencia)=RTRIM(@cod_area)) or 
											(@cod_nivel='SECT' and RTRIM(Gr.cod_sector)=RTRIM(@cod_area)) or 
											(@cod_nivel='GRUP' and RTRIM(Su.cod_grupo)=RTRIM(@cod_area))) 
										FETCH CursPerfil INTO @cod_perfil, @cod_nivel, @cod_area 
									end 
							CLOSE CursPerfil 
							DEALLOCATE CURSOR CursPerfil
						end
						fetch CursRecursos into @cod_recurso, @nom_recurso,@email,@euser,@dlg_recurso,@dlg_desde,@dlg_hasta,@dlg_email,@dlg_euser
			end
		end 
	close CursRecursos  
	DEALLOCATE CURSOR CursRecursos
	
	--{ add -002- a. Para SI
	insert #TmpSalida 
		select  
			Me.cod_area,
			(select LTRIM(RTRIM(v.var_texto)) from GesPet..Varios v where v.var_codigo = 'SI_POOL') + @domain,
			Me.msg_nrointerno,
			Me.pet_nrointerno, 
			Me.cod_perfil,
			Ge.cod_direccion,
			Gr.cod_gerencia,
			Gr.cod_sector,
			Su.cod_grupo
		from     
			GesPet..Mensajes Me, 
			GesPet..Grupo  Su, 
			GesPet..Sector  Gr (index idx_Sector), 
			GesPet..Gerencia Ge 
		where 
			(RTRIM(Me.cod_perfil) = 'POOL') and 
			(RTRIM(Me.cod_nivel ) = 'GRUP') and 
			(@fecdesde='NULL' or convert(char(8),Me.fe_mensaje,112)>= @fecdesde) and
			(@fechasta='NULL' or convert(char(8),Me.fe_mensaje,112)<= @fechasta) and
			(RTRIM(Su.cod_grupo)=RTRIM(Me.cod_area)) and 
			(RTRIM(Gr.cod_sector)=RTRIM(Su.cod_sector)) and 
			(RTRIM(Gr.cod_gerencia)=RTRIM(Ge.cod_gerencia)) and 
			(RTRIM(Su.cod_grupo)=RTRIM(Me.cod_area))
	--}
	
	select  
		Tx.cod_recurso,
		nom_recurso = isnull((select R.nom_recurso from Recurso R where R.cod_recurso=Tx.cod_recurso),(select G.nom_grupo from GesPet..Grupo G where G.cod_grupo = Tx.cod_recurso)),	-- upd -002- a.
		Tx.email,
		Tx.msg_nrointerno, 
		Me.pet_nrointerno, 
		Pe.pet_nroasignado, 
		Pe.titulo, 
		Me.fe_mensaje, 
		Me.cod_perfil, 
		nom_perfil  = isnull((select Pf.nom_perfil from Perfil Pf where Me.cod_perfil=Pf.cod_perfil),'N/A (Pool)'),		-- upd -002- a.
		txt_txtmsg = '' + LTRIM(Me.txt_txtmsg),
		msg_texto  = (select '' + LTRIM(Mt.msg_texto) from MensajesTexto Mt where Me.cod_txtmsg=Mt.cod_txtmsg)
	from    
		#TmpSalida Tx, 
		GesPet..Mensajes Me, 
		GesPet..Peticion Pe
	where	
		(Tx.msg_nrointerno=Me.msg_nrointerno) and 
		(Tx.pet_nrointerno=Pe.pet_nrointerno) and
		((Me.cod_perfil<>'SOLI') or (Me.cod_perfil='SOLI' and Tx.cod_recurso=Pe.cod_solicitante)) and
		((Me.cod_perfil<>'GBPE') or (Me.cod_perfil='GBPE' and Tx.cod_recurso=Pe.cod_BPE)) and						-- upd 
		(Tx.msg_nrointerno = (select max(x.msg_nrointerno) from #TmpSalida x 
								where Tx.cod_recurso=x.cod_recurso and Tx.pet_nrointerno=x.pet_nrointerno)) 
	order by 
		Tx.cod_recurso ASC ,
		Tx.msg_nrointerno ASC 

	return(0) 
go

grant execute on dbo.sp_GetMensajesMail to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go