use GesPet
go
print 'sp_UpdateTipoPrj'
go
if exists (select * from sysobjects where name = 'sp_UpdateTipoPrj' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateTipoPrj
go
create procedure dbo.sp_UpdateTipoPrj
	@cod_tipoprj	char(8)=null,
	@nom_tipoprj	char(30)=null,
	@flg_habil	char(1)=null,
	@secuencia	int = 1

as


declare @old_secuencia int  

select  @old_secuencia = BSR.secuencia  
	from  TipoPrj BSR
	where 	RTRIM(cod_tipoprj)<>RTRIM(@cod_tipoprj)

if @old_secuencia = @secuencia 
	begin 
		raiserror 30011 'Ya existe esa secuencia para ese elemento'   
		select 30011 as ErrCode , 'Ya existe esa secuencia para ese elemento' as ErrDesc   
		return (30011)   
	end 

if not exists (select cod_tipoprj from TipoPrj where RTRIM(cod_tipoprj)=RTRIM(@cod_tipoprj))
	begin 
		raiserror 30011 'TipoPrj Inexistente'   
		select 30011 as ErrCode , 'TipoPrj Inexistente' as ErrDesc   
		return (30011)   
	end 

Update TipoPrj
	set	
	nom_tipoprj=@nom_tipoprj,
	flg_habil=	    @flg_habil,
	secuencia=	    @secuencia
	where RTRIM(cod_tipoprj)=RTRIM(@cod_tipoprj)

return(0)
go

grant execute on dbo.sp_UpdateTipoPrj to GesPetUsr
go
