-- Actualización.

use GesPet
go

print 'Creando/actualizando SP: sp_GetSectorAbrev'
go

if exists (select * from sysobjects where name = 'sp_GetSectorAbrev' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSectorAbrev
go

create procedure dbo.sp_GetSectorAbrev
	@cod_abrev	char(3)
as
	begin
	select
		Gr.cod_sector,
		Gr.nom_sector,
		Gr.cod_gerencia,
		Ge.nom_gerencia,
		Ge.cod_direccion,
		nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)),
		Gr.cod_abrev,
		Gr.flg_habil,
		Gr.cod_bpar,
		nom_bpar = isnull((select R.nom_recurso from Recurso R where RTRIM(R.cod_recurso) = RTRIM(Gr.cod_bpar)),'')
	from
		GesPet..Sector Gr,
		GesPet..Gerencia Ge
	where
		(RTRIM(Gr.cod_abrev) = RTRIM(@cod_abrev)) and
		(RTRIM(Gr.cod_gerencia) = RTRIM(Ge.cod_gerencia))
	end
	return(0)
go

grant execute on dbo.sp_GetSectorAbrev to GesPetUsr
go

print 'Actualización realizada.'
go