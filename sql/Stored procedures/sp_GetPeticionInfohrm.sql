/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionInfohrm'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionInfohrm' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionInfohrm
go

create procedure dbo.sp_GetPeticionInfohrm
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@cod_recurso	char(10)=null
as
	select 
		encabezado = (select ig.info_grunom from Infogru ig where ig.info_gruid = d.infoprot_gru),
		pr.pet_nrointerno,
		pr.info_id,
		pr.info_idver,
		pr.cod_recurso,
		nom_recurso = (select r.nom_recurso from Recurso r where r.cod_recurso = pr.cod_recurso),
		pr.info_item,
		d.infoprot_req,
		req_dsc = case d.infoprot_req
			when 'S' then 'Obligatorio'
			when 'R' then 'Recomendado'
			when 'N' then 'Opcional'
			else '-'
		end,
		pd.info_estado,
		nom_estado = case pd.info_estado
			when 'X' then 'N/A'
			when 'S' then 'Si'
			when 'N' then 'No'
			when 'R' then 'REF.SIS.'
			when 'U' then 'USUARIO'
			when 'V' then 'SUPERV.'
			when 'L' then 'LIDER'
			when 'P' then 'P950'
			when 'C' then 'C100'
		end,
		pd.info_valor,
		valor_dsc = case 
			when pd.info_valor in (0,1) then '-'
			when pd.info_valor = 2 then 'OME'
			when pd.info_valor = 3 then 'OMA'
			else ''
		end,
		pr.info_comen,
		i.infoprot_itemdsc,
		pr.fecha_envio,
		pr.fecha_reenvio
	from 
		PeticionInfohr pr inner join 
		PeticionInfohc pc on (pr.pet_nrointerno = pc.pet_nrointerno and pr.info_id = pc.info_id and pr.info_idver = pc.info_idver) inner join 
		PeticionInfohd pd on (pr.pet_nrointerno = pd.pet_nrointerno and pr.info_id = pd.info_id and pr.info_idver = pd.info_idver and pr.info_item = pd.info_item) inner join
		Infoproi i on (pr.info_item = i.infoprot_item) inner join
		Infoprod d on (d.infoprot_id = pc.infoprot_id and d.infoprot_item = i.infoprot_item)
	where 
		pr.pet_nrointerno = @pet_nrointerno and
		pr.info_id = @info_id and 
		(@info_idver is null or pr.info_idver = @info_idver) and
		(@cod_recurso is null or pr.cod_recurso = @cod_recurso)
	order by 
		1,
		pr.pet_nrointerno,
		pr.info_id,
		pr.info_idver,
		pr.cod_recurso,
		pr.info_item
	return(0)
go

grant execute on dbo.sp_GetPeticionInfohrm to GesPetUsr
go

print 'Actualización realizada.'
go
