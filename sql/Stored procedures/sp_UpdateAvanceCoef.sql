use GesPet
go
print 'sp_UpdateAvanceCoef'
go
if exists (select * from sysobjects where name = 'sp_UpdateAvanceCoef' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateAvanceCoef
go
create procedure dbo.sp_UpdateAvanceCoef
	@modo			char(1)='A',
	@cod_AvanceCoef		char(8)=null,
	@nom_AvanceCoef		char(30)=null,
	@cod_AvanceGrupo	char(8)=null,
	@flg_habil		char(1)=null,
	@nom_largo		char(100)=null,
	@porcentaje		int = null



as

if not exists (select cod_AvanceCoef from GesPet..AvanceCoef where RTRIM(cod_AvanceCoef) = RTRIM(@cod_AvanceCoef))
	begin 
		raiserror 30011 'Avance Coeficiente Inexistente'   
		select 30011 as ErrCode , 'Avance Coeficiente Inexistente' as ErrDesc   
		return (30011)   
	end 

Update GesPet..AvanceCoef
	set	
	cod_AvanceCoef=@cod_AvanceCoef,
	nom_AvanceCoef=@nom_AvanceCoef,
	cod_AvanceGrupo=   @cod_AvanceGrupo,
	flg_habil=	    @flg_habil,
	nom_largo=	    @nom_largo,
	porcentaje=	    @porcentaje

	where RTRIM(cod_AvanceCoef) = RTRIM(@cod_AvanceCoef)


return(0)
go



grant execute on dbo.sp_UpdateAvanceCoef to GesPetUsr
go
