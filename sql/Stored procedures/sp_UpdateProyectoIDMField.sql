/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 16.06.2010 - 
-001- a. FJS 26.01.2016 - Se agrega el campo codigoEmpresa.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMField'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMField
go

create procedure dbo.sp_UpdateProyectoIDMField
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as
	if LTRIM(RTRIM(@campo))='PROJNOM'
		update GesPet.dbo.ProyectoIDM
		set ProjNom = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if LTRIM(RTRIM(@campo))='PROJFCHALTA'
		update GesPet.dbo.ProyectoIDM
		set ProjFchAlta = @valordate
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if LTRIM(RTRIM(@campo))='PROJCATID'
		update GesPet.dbo.ProyectoIDM
		set ProjCatId = @valornum
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if LTRIM(RTRIM(@campo))='PROJCLASEID'
		update GesPet.dbo.ProyectoIDM
		set ProjClaseId = @valornum
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if LTRIM(RTRIM(@campo))='COD_ESTADO'
		update 
			GesPet.dbo.ProyectoIDM
		set
			cod_estado	= @valortxt,
			fch_estado	= getdate(),
			usr_estado	= SUSER_NAME()
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if LTRIM(RTRIM(@campo))='FCH_ESTADO'
		update GesPet.dbo.ProyectoIDM
		set fch_estado = @valordate
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if LTRIM(RTRIM(@campo))='USR_ESTADO'
		update GesPet.dbo.ProyectoIDM
		set usr_estado = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if LTRIM(RTRIM(@campo))='SEMAFORO'
		update  
			GesPet.dbo.ProyectoIDM
		set
			semaphore	= @valortxt,		-- Sem�foro (autom�tico)
			fe_modif	= getdate(),		-- Fecha de �ltima modificaci�n
			semaphore2		= null,			-- Sem�foro manual
			sema_fe_modif	= null,			-- Fecha de modificaci�n manual del sem�foro
			sema_usuario	= null			-- Usuario que modific� manualmente el sem�foro
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if LTRIM(RTRIM(@campo))='SEMAFORO_MANUAL'
		update 
			GesPet.dbo.ProyectoIDM
		set
			semaphore2		= @valortxt,
			sema_fe_modif	= getdate(),		-- Fecha de modificaci�n manual del sem�foro
			sema_usuario	= SUSER_NAME()		-- Usuario que modific� manualmente el sem�foro
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if LTRIM(RTRIM(@campo))='FE_MODIF'
		update GesPet.dbo.ProyectoIDM
		set fe_modif = @valordate
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if LTRIM(RTRIM(@campo))='COD_ESTADO2'
		update  
			GesPet.dbo.ProyectoIDM
		set
			cod_estado2		= @valortxt
			/* ,
			cod_estado		= null,
			fe_modif		= getdate()			-- Fecha de modificaci�n manual
			*/
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if LTRIM(RTRIM(@campo))='MARCA'					-- Estado
		update GesPet.dbo.ProyectoIDM
		set marca	= @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='COD_DIRECCION'
		update GesPet.dbo.ProyectoIDM
		set cod_direccion = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='COD_GERENCIA'
		update GesPet.dbo.ProyectoIDM
		set cod_gerencia = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='COD_SECTOR'
		update GesPet.dbo.ProyectoIDM
		set cod_sector	= @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='COD_DIRECCION_P'
		update GesPet.dbo.ProyectoIDM
		set cod_direccion_p	= @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='COD_GERENCIA_P'
		update GesPet.dbo.ProyectoIDM
		set cod_gerencia_p	= @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='COD_SECTOR_P'
		update GesPet.dbo.ProyectoIDM
		set cod_sector_p = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='COD_GRUPO_P'
		update GesPet.dbo.ProyectoIDM
		set cod_grupo_p	= @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if UPPER(LTRIM(RTRIM(@campo)))='TIPO_PROYECTO'
		update GesPet.dbo.ProyectoIDM
		set tipo_proyecto = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if UPPER(LTRIM(RTRIM(@campo)))='TOTALHS_GER'
		update GesPet.dbo.ProyectoIDM
		set totalhs_gerencia = @valornum
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='TOTALHS_SEC'
		update GesPet.dbo.ProyectoIDM
		set totalhs_sector = @valornum
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if UPPER(LTRIM(RTRIM(@campo)))='TOTALREPLAN'
		update GesPet.dbo.ProyectoIDM
		set totalreplan	= @valornum
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if LTRIM(RTRIM(@campo))='FE_APROB'
		update GesPet.dbo.ProyectoIDM
		set fe_aprob = @valordate
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	if LTRIM(RTRIM(@campo))='CAMBIO_ALCANCE'
		update GesPet.dbo.ProyectoIDM
		set cambio_alcance = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	
	if LTRIM(RTRIM(@campo))='CODIGO_GPS'
		update GesPet.dbo.ProyectoIDM
		set codigo_gps = @valortxt
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId

	--{ add -001- a.	
	if LTRIM(RTRIM(@campo))='EMPRESA'
		update GesPet.dbo.ProyectoIDM
		set codigoEmpresa = @valornum
		where 
			ProjId = @ProjId and
			ProjSubId = @ProjSubId and
			ProjSubSId = @ProjSubSId
	--}
go

grant execute on dbo.sp_UpdateProyectoIDMField to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
