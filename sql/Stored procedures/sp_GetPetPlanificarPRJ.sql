/*
-000- a. FJS 12.07.2010 - Nuevo SP para...
-001- a. FJS 12.10.2010 - Corrección: se cambia el criterio de cálculo de la columna [alca] (se cambia TEST por ALCA).
-002- a. FJS 18.11.2010 - Planificación DYD (2° etapa): se identifica las peticiones agregadas por DYD.
-003- a. FJS 06.12.2010 - Mejora: se agrega la descripción del período de planificación.
-004- a. FJS 11.03.2011 - Modificación: se agrega un nuevo campo para reflejar la primera fecha de planificación cuando una petición nunca ha sido planificada.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetPlanificarPRJ'
go

if exists (select * from sysobjects where name = 'sp_GetPetPlanificarPRJ' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetPlanificarPRJ
go

create procedure dbo.sp_GetPetPlanificarPRJ
	@cod_bpar		char(8)=null,
	@cod_grupo		char(8)=null,
	@per_nrointerno	int=null,
	@prioridad		char(1)=null
as 
	select 
		isnull(p.per_nrointerno,0) as per_nrointerno,
		a.pet_nroasignado,
		p.pet_nrointerno,
		a.cod_tipo_peticion,
		a.cod_clase,
		a.pet_regulatorio,
		isnull(a.pet_projid,0) as pet_projid,
		isnull(a.pet_projsubid,0) as pet_projsubid,
		isnull(a.pet_projsubsid,0) as pet_projsubsid,
		a.titulo,
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),
		pc.plan_actusr,
		plan_actusrnom = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = pc.plan_actusr),
		pc.plan_actfch,
		pc.prioridad,
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),
		a.cod_sector,
		nom_sector    = (select RTRIM(x.nom_sector) from GesPet..Sector x where x.cod_sector = a.cod_sector),
		nom_gerencia  = (select RTRIM(x.nom_gerencia) from GesPet..Gerencia x where x.cod_gerencia = a.cod_gerencia),
		nom_direccion = (select RTRIM(x.nom_direccion) from GesPet..Direccion x where x.cod_direccion = a.cod_direccion),
		'alca' = (
		case
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'ALCA') > 0 then 'U'
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'ALCA') = 0 then 'S'
			else 'N'
		end),
		p.cod_grupo,
		resp_grupo = (select x.nom_recurso from GesPet..Recurso x where x.cod_grupo = p.cod_grupo and x.cod_sector = b.cod_sector and x.flg_cargoarea = 'S'),
		g.nom_grupo,
		b.cod_estado as 'grp_cod_estado',
		'grp_nom_estado' = (select x.nom_estado from GesPet..Estados x where x.cod_estado = b.cod_estado),
		b.horaspresup,
		p.plan_orden,
		p.plan_ordenfin,
		estado_orden = (
		case
			when (p.plan_ordenfin is null and pc.prioridad is null) then 'Pendiente priorización'
			when (p.plan_ordenfin is null and pc.prioridad is not null and pc.prioridad = '1') then 'Pendiente de orden'
			else '-'
		end),
		'mas_dyd' = (
		case
			when (select count(*) from GesPet..PeticionGrupo x inner join GesPet..Grupo y on (x.cod_grupo = y.cod_grupo)
					where x.pet_nrointerno = a.pet_nrointerno and y.grupo_homologacion = 'S' and x.cod_gerencia = 'DESA' and charindex(x.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0) > 1 then 'Si'
			when (select count(*) from GesPet..PeticionGrupo x inner join GesPet..Grupo y on (x.cod_grupo = y.cod_grupo)
					where x.pet_nrointerno = a.pet_nrointerno and y.grupo_homologacion = 'S' and x.cod_gerencia = 'DESA' and charindex(x.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0) = 1 then 'No'
			else '-'
		end),
		p.plan_estado,
		'plan_situacion' = '',
		'plan_situacion_dsc' = case
			when (p.plan_estado = 'PLANIF') then 'Pendiente'
			when (p.plan_estado = 'PLANOK') then 'Ordenado'
			when (p.plan_estado = 'APROBA') then 'Confirmado'
			else '-'
		end,
		p.plan_act2fch,
		p.plan_act2usr,
		'plan_act2usrnom' = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.plan_act2usr),
		dyd_default_orden = case 
			when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) = 1 and pc.prioridad is not null and p.plan_orden is null then '0'
			when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) > 1 and pc.prioridad is not null and p.plan_orden is null and (b.cod_estado = 'EJECUC' or (b.cod_estado = 'SUSPEN' and b.cod_motivo in (1,2,7,8))) then '0'
			when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) > 1 and pc.prioridad is not null and p.plan_orden is null and not (b.cod_estado = 'EJECUC' or (b.cod_estado = 'SUSPEN' and b.cod_motivo in (1,2,7,8))) then '*'
			else ''
		end,
		p.plan_ori,		-- add -002- a.
		nom_origen = (
			case
				when p.plan_ori = 1 then 'Planificación inicial'
				when p.plan_ori = 2 then 'Agregada'
				--{ add -002- a.
				when p.plan_ori = 3 then 'Planificación inicial (DYD)'
				when p.plan_ori = 4 then 'Agregada (DYD)'
				--}
				else '***'
			end),				-- Origen de planificación
		p.plan_estadoini,		-- Inicial: Estado de planificación
		nom_estadoini = (select x.nom_estado_plan from GesPet..EstadosPlanificacion x where x.cod_estado_plan = p.plan_estadoini),
		p.plan_desaestado,		-- Seguimiento: Estado actual del desarrollo
		nom_desaestado = (select x.nom_estado_desa from GesPet..EstadosDesarrollo x where x.cod_estado_desa = p.plan_desaestado),
		p.plan_fealcadef,		-- Inicial: Fecha de definición de alcance
		p.plan_fefunctst,		-- Inicial: Fecha de fin de pruebas funcionales
		p.plan_motivrepl,		-- Replanificar: Motivo de replanificación
		p.plan_feplannue,		-- Replanificar: Nueva fecha de planificación
		p.plan_feplanori,		-- Replanificar: Fecha original de planificación
		p.plan_horasper,		-- Inicial: Cantidad de horas planificadas para el presente período
		p.plan_horasreplan,		-- Replanificar: Cantidad de horas totales replanificadas
		p.plan_motivsusp,		-- Seguimiento: Motivo de Susp./Desplan./Canc./Rech.
		p.plan_motnoplan,		-- Inicial: Motivo de no planificación (no se puede planificar)
		nom_motivo_replan = (select x.nom_motivo_replan from GesPet..MotivosReplan x where x.cod_motivo_replan = p.plan_motivrepl),
		nom_motivrepl = (select x.nom_motivo_replan from GesPet..MotivosReplan x where x.cod_motivo_replan = p.plan_motivrepl),
		nom_motivsusp = (select x.nom_motivo_suspen from GesPet..MotivosSuspen x where x.cod_motivo_suspen = p.plan_motivsusp),
		nom_motnoplan = (select x.nom_motivo_noplan from GesPet..MotivosNoPlan x where x.cod_motivo_noplan = p.plan_motnoplan),
		p.plan_cantreplan		-- Replanificar: Cantidad de replanificaciones (contador)
		,per_abrev = isnull((select x.per_abrev from GesPet..Periodo x where x.per_nrointerno = pc.per_nrointerno),'-')			-- add -003- a.
		,plan_fchestadoini		-- Fecha de la primera planificación															-- add -004- a.
	from 
		GesPet..PeticionPlandet p left join 
		GesPet..PeticionPlancab pc on (p.per_nrointerno = pc.per_nrointerno and p.pet_nrointerno = pc.pet_nrointerno) left join
		GesPet..PeticionGrupo b on (p.pet_nrointerno = b.pet_nrointerno and p.cod_grupo = b.cod_grupo) left join
		GesPet..Peticion a on (b.pet_nrointerno = a.pet_nrointerno) left join 
		GesPet..Grupo g on (b.cod_grupo = g.cod_grupo)
	where 
		(@cod_bpar is null or a.cod_bpar = @cod_bpar) and 
		(p.per_nrointerno is null or @per_nrointerno is null or p.per_nrointerno = @per_nrointerno) and
		(@cod_grupo is null or p.cod_grupo = @cod_grupo) and
		(@prioridad is null or pc.prioridad = @prioridad) 
	order by
		g.nom_grupo,
		pc.prioridad,
		p.plan_ordenfin,
		a.pet_nroasignado
	return (0)
go

grant execute on dbo.sp_GetPetPlanificarPRJ to GesPetUsr
go

print 'Actualización realizada.'