/*
-001- FJS 06.07.2007 - Se agrega el campo cod_clase al conjunto de resultados.
*/
use GesPet
go
print 'Actualizando el procedimiento almacenado: sp_GetPeticionSectorMinuta'
go
if exists (select * from sysobjects where name = 'sp_GetPeticionSectorMinuta' and sysstat & 7 = 4)
drop procedure dbo.sp_GetPeticionSectorMinuta
go
create procedure dbo.sp_GetPeticionSectorMinuta
	@pet_nrointerno		int=0,
	@cod_gerencia		char(8)='NULL',
	@cod_sector		char(8)='NULL',
	@cod_estado		varchar(100)='',
	@fe_desde		smalldatetime=null,
	@fe_hasta		smalldatetime=null
as
select
	Pg.pet_nrointerno,
	P.pet_nroasignado,
	P.cod_tipo_peticion,
	P.titulo,
	sol_direccion = (select nom_direccion from Direccion where cod_direccion=P.cod_direccion),
	sol_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=P.cod_gerencia),
	sol_sector = (select nom_sector from Sector where cod_sector=P.cod_sector),
	Pg.cod_sector,
	Pg.cod_gerencia,
	Pg.cod_direccion,
	Pg.horaspresup,
	sec_fe_fin_real=Pg.fe_fin_real,
	Pg.cod_estado,
	Pg.fe_estado,
	sec_nom_sector=(select nom_sector from Sector where RTRIM(cod_sector)=RTRIM(Pg.cod_sector)) ,
	--nom_estado=(select nom_estado from Estados where RTRIM(cod_estado)=RTRIM(Pg.cod_estado))	-- del -001-
	--{ add -001-
	nom_estado=(select nom_estado from Estados where RTRIM(cod_estado)=RTRIM(Pg.cod_estado)),
	P.cod_clase
	--}
from
	PeticionSector Pg , Peticion P
where
	(@pet_nrointerno = 0 or @pet_nrointerno = Pg.pet_nrointerno) and
	(Pg.pet_nrointerno=P.pet_nrointerno) and
	(RTRIM(@cod_gerencia)='NULL' or RTRIM(Pg.cod_gerencia)=RTRIM(@cod_gerencia)) and
	(RTRIM(@cod_sector)='NULL' or RTRIM(Pg.cod_sector)=RTRIM(@cod_sector)) and
	(charindex(RTRIM(Pg.cod_estado),@cod_estado)>0) and
	(convert(char(8),Pg.fe_estado,112)>=convert(char(8),@fe_desde,112) and convert(char(8),Pg.fe_estado,112)<=convert(char(8),@fe_hasta,112)) and
	Pg.fe_estado = (select max(I.fe_estado) from PeticionSector I where Pg.pet_nrointerno=I.pet_nrointerno and (Pg.cod_gerencia=I.cod_gerencia) and (RTRIM(@cod_sector)='NULL' or Pg.cod_sector=I.cod_sector) and (charindex(RTRIM(I.cod_estado),@cod_estado)>0))
order by P.pet_nroasignado

return(0)
go

grant execute on dbo.sp_GetPeticionSectorMinuta to GesPetUsr
go

print 'Actualización finalizada.'
/*
sp_GetPeticionSectorMinuta 0,'DESA','NULL','ANEXAD|ANULAD|TERMIN|CANCEL|RECHAZ','2004-01-09','2004-01-09'
sp_GetPeticionSectorMinuta 0,'DESA','25','TERMIN','2004-04-01','2004-04-30'
*/