/*
-000- a. FJS 20.08.2010 - Nuevo SP para habilitar los estados a contemplar en IGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ABM_Estado'
go

if exists (select * from sysobjects where name = 'sp_IGM_ABM_Estado' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ABM_Estado
go

create procedure dbo.sp_IGM_ABM_Estado
	@IGM_estado		char(6), 
	@CGM_estado		char(6)
as 
	if exists (select cod_IGM_estado from GesPet..IGM_Estados where cod_IGM_estado = @IGM_estado and cod_CGM_estado = @CGM_estado)
		begin
			delete from GesPet..IGM_Estados
			where 
				cod_IGM_estado = @IGM_estado and
				cod_CGM_estado = @CGM_estado
			
			insert into GesPet..IGM_Estados values (@IGM_estado, @CGM_estado)
			return(0)
		end
	else
		begin
			insert into GesPet..IGM_Estados values (@IGM_estado, @CGM_estado)
			return(0)
		end
go

grant execute on dbo.sp_IGM_ABM_Estado to GesPetUsr
go

grant execute on dbo.sp_IGM_ABM_Estado to GrpTrnIGM
go

print 'Actualización realizada.'
go