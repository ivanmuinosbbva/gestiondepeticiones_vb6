use GesPet
go
print 'sp_InsAgrupAgrup'
go
if exists (select * from sysobjects where name = 'sp_InsAgrupAgrup' and sysstat & 7 = 4)
drop procedure dbo.sp_InsAgrupAgrup
go
create procedure dbo.sp_InsAgrupAgrup 
	@agr_nrointerno		int=0,
	@agr_nropadre		int=0
as 
 
	if not exists (select agr_nrointerno from Agrup where agr_nrointerno = @agr_nrointerno)   
	begin 
		select 30010 as ErrCode , 'Error: No existe Agrupamiento' as ErrDesc 
		raiserror  30010 'Error: No existe Agrupamiento'  
		return (30010) 
	end 
	if not exists (select agr_nrointerno from Agrup where agr_nrointerno = @agr_nropadre)   
	begin 
		select 30010 as ErrCode , 'Error: No existe Agrupamiento Padre' as ErrDesc 
		raiserror  30010 'Error: No existe Agrupamiento Padre'  
		return (30010) 
	end 
 
	update Agrup set 
		agr_nropadre	= @agr_nropadre
	where  agr_nrointerno=@agr_nrointerno

return(0)   
go



grant execute on dbo.sp_InsAgrupAgrup to GesPetUsr 
go


