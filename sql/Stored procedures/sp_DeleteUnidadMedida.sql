/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteUnidadMedida'
go

if exists (select * from sysobjects where name = 'sp_DeleteUnidadMedida' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteUnidadMedida
go

create procedure dbo.sp_DeleteUnidadMedida
	@unimedId	char(10)=''
as
	delete from
		GesPet.dbo.UnidadMedida
	where
		unimedId = @unimedId
go

grant execute on dbo.sp_DeleteUnidadMedida to GesPetUsr
go

print 'Actualización realizada.'
go
