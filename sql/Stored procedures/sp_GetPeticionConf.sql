/*
-001- a. FJS 13.01.2009 - Optimización de SP para mejorar la performance del aplicativo.
*/

use GesPet
go

print 'Creando/actualizando el SP: sp_GetPeticionConf'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionConf' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionConf
go

create procedure dbo.sp_GetPeticionConf
	@pet_nrointerno	int = null, 
	@ok_tipo		char(8) = null, 
	@ok_secuencia	int = null 
as 
	select 
		Pc.pet_nrointerno, 
		Pc.ok_tipo, 
		ok_tiponom = case
			when Pc.ok_tipo in ('ALCA','ALCP') then 'OK Documento alcance'
			when Pc.ok_tipo in ('TEST','TESP') then 'OK Pruebas usuario'
			when Pc.ok_tipo in ('DATF','DATP') then 'OK Datos de usuario'
			when Pc.ok_tipo in ('OKPF','OKPP') then 'OK Supervisor'
			when Pc.ok_tipo in ('HOMA.F','HOMA.P') then 'OMA'
			when Pc.ok_tipo in ('HOME.F','HOME.P') then 'OME'
			when Pc.ok_tipo in ('HSOB.F','HSOB.P') then 'SOB'
			when Pc.ok_tipo in ('SIOK.F','SIOK.P') then 'SEG - Conforme'
			when Pc.ok_tipo in ('SIOB.F','SIOB.P') then 'SEG - Conforme c/ observaciones'
			when Pc.ok_tipo in ('SIRE.F','SIRE.P') then 'SEG - No conforme'
			when Pc.ok_tipo in ('AROK.F','AROK.P') then 'ARQ - Conforme'
			when Pc.ok_tipo in ('AROB.F','AROB.P') then 'ARQ - Conforme c/ observaciones'
			when Pc.ok_tipo in ('ARRE.F','ARRE.P') then 'ARQ - No conforme'
		end,
		ok_tipoCombo = case
			when Pc.ok_tipo in ('ALCA','ALCP') then 'ALC'
			when Pc.ok_tipo in ('TEST','TESP') then 'TES'
			when Pc.ok_tipo in ('DATF','DATP') then 'DAT'
			when Pc.ok_tipo in ('OKPF','OKPP') then 'OKP'
			when Pc.ok_tipo in ('HOMA.F','HOMA.P') then 'OMA'
			when Pc.ok_tipo in ('HOME.F','HOME.P') then 'OME'
			when Pc.ok_tipo in ('HSOB.F','HSOB.P') then 'SOB'
			when Pc.ok_tipo in ('SIOK.F','SIOK.P') then 'SIOK'
			when Pc.ok_tipo in ('SIOB.F','SIOB.P') then 'SIOB'
			when Pc.ok_tipo in ('SIRE.F','SIRE.P') then 'SIRE'
			when Pc.ok_tipo in ('AROK.F','AROK.P') then 'AROK'
			when Pc.ok_tipo in ('AROB.F','AROB.P') then 'AROB'
			when Pc.ok_tipo in ('ARRE.F','ARRE.P') then 'ARRE'
		end,
		ok_tipoclase = case
			when Pc.ok_tipo in ('ALCA','TEST','DATF','OKPF','HOMA.F','HOME.F','HSOB.F','SIOK.F','SIOB.F','SIRE.F','AROK.F','AROB.F','ARRE.F') then 'Final'
			when Pc.ok_tipo in ('ALCP','TESP','DATP','OKPP','HOMA.P','HOME.P','HSOB.P','SIOK.P','SIOB.P','SIRE.P','AROK.P','AROB.P','ARRE.P') then 'Parcial'
		end,
		ok_modonom = case
			when Pc.ok_modo = 'REQ' then 'Requerido'
			when Pc.ok_modo = 'EXC' then 'Exención'
			when Pc.ok_modo = 'ALT' then 'Alternativo'
			when Pc.ok_modo = 'N/A' then '-'
			else '-'
		end,
		ok_orden = case 
			when Pc.ok_tipo in ('SIOK','SIOB','SIRE') then 1
			when Pc.ok_tipo in ('ALCA','ALCP') then 2
			when Pc.ok_tipo in ('TEST','TESP') then 3
			when Pc.ok_tipo in ('DATF','DATP') then 4
			when Pc.ok_tipo in ('OKPF','OKPP') then 5
			when Pc.ok_tipo in ('HOMA.F','HOMA.P','HOME.F','HOME.P','HSOB.F','HSOB.P') then 6
			else 9999
		end,
		Pc.ok_modo, 
		Pc.ok_texto, 
		Pc.audit_user, 
		nom_audit = isnull((select Ra.nom_recurso from Recurso Ra where RTRIM(Ra.cod_recurso)=RTRIM(Pc.audit_user)),'--Automático--'), 
		Pc.audit_date, 
		Pc.ok_secuencia 
	from 
		PeticionConf Pc 
	where	
		(@pet_nrointerno is null or Pc.pet_nrointerno=@pet_nrointerno) and  
		(@ok_tipo is null or RTRIM(@ok_tipo)='' or RTRIM(Pc.ok_tipo) = RTRIM(@ok_tipo) or RTRIM(Pc.ok_tipo) LIKE RTRIM(@ok_tipo)) and
		(@ok_secuencia is null or Pc.ok_secuencia=@ok_secuencia) 
	order by 
		Pc.pet_nrointerno, 
		ok_orden,
		Pc.audit_date
	return(0) 
go

grant execute on dbo.sp_GetPeticionConf to GesPetUsr
go

print 'Actualización realizada.'
go
