/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteTipoPet'
go

if exists (select * from sysobjects where name = 'sp_DeleteTipoPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteTipoPet
go

create procedure dbo.sp_DeleteTipoPet
	@cod_tipopet	char(3)=''
as
	delete from GesPet.dbo.TipoPet
	where cod_tipopet = @cod_tipopet
go

grant execute on dbo.sp_DeleteTipoPet to GesPetUsr
go

print 'Actualización realizada.'
go
