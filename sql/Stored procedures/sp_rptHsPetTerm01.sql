/* 
-000- a. - FJS 13.09.2013 - Creado.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptHsPetTerm01'
go

if exists (select * from sysobjects where name = 'sp_rptHsPetTerm01' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHsPetTerm01
go

create procedure dbo.sp_rptHsPetTerm01
	@fe_desde		char(8)='NULL',
	@fe_hasta		char(8)='NULL'
as
	select 
		p.pet_nroasignado,
		p.titulo,
		nom_estado = (select e.nom_estado from GesPet..Estados e where e.cod_estado = p.cod_estado),
		p.fe_estado,
		sum(convert(numeric(10,2),ht.horas)/60) as HORAS_DYD,
		0 as HORAS_BP
	from 
		GesPet..HorasTrabajadas ht inner join
		GesPet..Recurso r on (ht.cod_recurso = r.cod_recurso) inner join
		GesPet..Peticion p on (ht.pet_nrointerno = p.pet_nrointerno)
	where 
		ltrim(rtrim(r.cod_gerencia))='DESA' and
		ht.pet_nrointerno in (
		select p.pet_nrointerno
		from GesPet..Peticion p 
		where 
			charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0 and
			(LTRIM(RTRIM(@fe_desde)) = 'NULL' or convert(char(8),p.fe_estado,112) >= @fe_desde) and
			(LTRIM(RTRIM(@fe_hasta)) = 'NULL' or convert(char(8),p.fe_estado,112) <= @fe_hasta)
	)
	group by
		p.pet_nroasignado,
		p.titulo,
		p.fe_estado
	having
		sum(ht.horas/60) > 0
	union
	select 
		p.pet_nroasignado,
		p.titulo,
		nom_estado = (select e.nom_estado from GesPet..Estados e where e.cod_estado = p.cod_estado),
		p.fe_estado,
		0 as HORAS_DYD,
		sum(convert(numeric(10,2),ht.horas)/60) as HORAS_BP
	from 
		GesPet..HorasTrabajadas ht inner join
		GesPet..Recurso r on (ht.cod_recurso = r.cod_recurso) inner join
		GesPet..Peticion p on (ht.pet_nrointerno = p.pet_nrointerno)
	where 
		ltrim(rtrim(r.cod_gerencia))='ORG.' and
		ht.pet_nrointerno in (
		select p.pet_nrointerno
		from GesPet..Peticion p 
		where 
			charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0 and
			(LTRIM(RTRIM(@fe_desde)) = 'NULL' or convert(char(8),p.fe_estado,112) >= @fe_desde) and
			(LTRIM(RTRIM(@fe_hasta)) = 'NULL' or convert(char(8),p.fe_estado,112) <= @fe_hasta)
	)
	group by
		p.pet_nroasignado,
		p.titulo,
		p.fe_estado
	having
		sum(ht.horas/60) > 0
	return (0)
go

grant execute on dbo.sp_rptHsPetTerm01 to GesPetUsr
go

print 'Actualización realizada.'
go