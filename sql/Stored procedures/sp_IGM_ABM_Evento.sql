/*
-000- a. FJS 20.08.2010 - Nuevo SP para habilitar eventos del historial a contemplar en IGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ABM_Evento'
go

if exists (select * from sysobjects where name = 'sp_IGM_ABM_Evento' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ABM_Evento
go

create procedure dbo.sp_IGM_ABM_Evento
	@cod_evento	char(8), 
	@hab		char(1) 
as 
	if exists (select cod_accion from GesPet..Acciones where cod_accion=@cod_evento)
		begin
			update GesPet..Acciones
			set IGM_hab = @hab
			where cod_accion = @cod_evento
			return(0)
		end
	else
		begin
			select 30010 as ErrCode, 'Error: No existe el c�digo de evento especificado.' as ErrDesc 
			raiserror 30010 'Error: No existe el c�digo de evento especificado.'
			return (30010) 
		end
go

grant execute on dbo.sp_IGM_ABM_Evento to GesPetUsr
go

grant execute on dbo.sp_IGM_ABM_Evento to GrpTrnIGM
go

print 'Actualizaci�n realizada.'
go