/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

Modo 1: genera la tabla tmpHEDTHST de la cual se obtienen las solicitudes a enviar a CM. 
Modo 2: marco los archivos utilizados en el cat�logo como ya enviados.

-000- a. FJS 26.10.2009 - Nuevo SP para cargar el archivo de base para transmitir el cat�logo de archivos enmascarables a HOST.
-001- a. FJS 26.01.2010 - Se actualiza por las modificaciones realizadas respecto a los restores y solicitudes diferidas.
-002- a. FJS 20.08.2010 - Se agrega autodepuraci�n para solicitudes pendientes de aprobaci�n por parte de Supervisores.
-003- a. FJS 07.07.2014 - Nuevo: se contemplan las solicitudes por EMErgencia.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_HEDTTransmitir'
go

if exists (select * from sysobjects where name = 'sp_HEDTTransmitir' and sysstat & 7 = 4)
	drop procedure dbo.sp_HEDTTransmitir
go

create procedure dbo.sp_HEDTTransmitir
	@modo		int=1
as 

declare @tmp_dsn_sol	char(50)
declare @tmp_dsn_id		char(8)
declare	@tmp_dsn_nom	char(50)
declare @FixedString	char(50)
declare @usuario		char(10)
declare @dias			int			-- add -002- a.

	if @modo = 1
		begin
			-- Borro la temporal
			truncate table GesPet..tmpHEDTHST

			-- Temporales
			create table #dsns1 ( 
				tmp_usuario		char(10)	null,
				tmp_dsn_sol		char(50)	null,
				tmp_dsn_nom		char(50)	null
			)

			create table #dsns2 ( 
				t_dsn_nom		char(50)	null
			)
			
			/*
				La temporal #dsns1 contendr� todos los archivos de cat�logo
				involucrados en las solicitudes de la corrida actual; solicitudes 
				en estado = 'C' (aprobadas).
			*/

			insert into #dsns1
				select
					sol_recurso,
					sol_file_desa,
					/*
					archivo = 
					case
						--when sol_tipo = '1' then sol_file_desa		-- del -001- a.
						when sol_tipo in ('1','5') then sol_file_desa	-- add -001- a.
						else sol_file_out
					end,
					*/
					''
				from GesPet..Solicitudes
				where sol_estado = 'C' and sol_eme <> 'S'				-- upd -003- a.

			-- Con un cursor recorro y transformo cada archivo al formato adecuado.
			declare CursDSN cursor for
				select tmp_dsn_sol, tmp_usuario
				from #dsns1
				group by tmp_dsn_sol, tmp_usuario
				for read only 

			open CursDSN 
				fetch CursDSN into @tmp_dsn_sol, @usuario
				while @@sqlstatus = 0
					begin
						select @FixedString = substring(@tmp_dsn_sol, 1, (charindex(RTRIM(@usuario), @tmp_dsn_sol) + char_length(RTRIM(@usuario))))
						select @tmp_dsn_nom = right(@tmp_dsn_sol, char_length(@tmp_dsn_sol) - char_length(rtrim(@FixedString)))

						update #dsns1
						set tmp_dsn_nom = rtrim(@tmp_dsn_nom)
						where RTRIM(tmp_dsn_sol) = RTRIM(@tmp_dsn_sol)

						fetch CursDSN into @tmp_dsn_sol, @usuario
					end
			close CursDSN 
			deallocate cursor CursDSN
			
			insert into #dsns2
				select tmp_dsn_nom from #dsns1 group by tmp_dsn_nom
			
			/*
			-- Muestro el resultado
			select *
			from #dsns2
			*/
			
			-- Marco los registros a transmitir desde el origen
			-- HEDT001
			update GesPet..HEDT001
			set estado = 'B'
			from GesPet..HEDT001 a inner join #dsns2 b on (a.dsn_nom = b.t_dsn_nom)
			where a.estado = 'A'
			
			-- HEDT002
			update GesPet..HEDT002
			set estado = 'B'
			from GesPet..HEDT001 a inner join GesPet..HEDT002 b on (a.dsn_id = b.dsn_id)
			where a.estado = 'B' and b.estado <> 'C'

			-- HEDT003
			update GesPet..HEDT003
			set estado = 'B'
			from GesPet..HEDT002 a inner join GesPet..HEDT003 b on (a.cpy_id = b.cpy_id)
			where a.estado = 'B' and b.estado <> 'C'

			-- Consulto las tablas correspondientes y agrego los registros a transmitir
			-- HEDT001
			insert into GesPet..tmpHEDTHST (
					HEDT0_table,
					HEDT0_dsn_id,
					HEDT0_cpy_id,
					HEDT0_cpo_id,
					HEDT1_dsn_nom,
					HEDT1_dsn_desc,
					HEDT1_dsn_enmas,
					HEDT1_dsn_nomrut,
					HEDT1_dsn_feult,
					HEDT1_dsn_userid,
					HEDT1_dsn_vb,
					HEDT1_dsn_vb_fe,
					HEDT1_dsn_vb_userid,
					HEDT1_app_id)
				select
					'HEDT001',
					a.dsn_id,
					'',
					'',
					a.dsn_nom,
					a.dsn_desc,
					a.dsn_enmas,
					a.dsn_nomrut,
					convert(char(8),a.dsn_feult,112),
					a.dsn_userid,
					a.dsn_vb,
					convert(char(8), a.dsn_vb_fe, 112),
					a.dsn_vb_userid,
					left(a.app_id,2)
				from GesPet..HEDT001 a
				where a.estado = 'B'

			-- HEDT002
			insert into GesPet..tmpHEDTHST (
					HEDT0_table,
					HEDT0_dsn_id,
					HEDT0_cpy_id,
					HEDT0_cpo_id,
					HEDT2_cpy_dsc,
					HEDT2_cpy_nomrut,
					HEDT2_cpy_feult,
					HEDT2_cpy_userid)
				select
					'HEDT002',
					b.dsn_id,
					b.cpy_id,
					'',
					b.cpy_dsc,
					b.cpy_nomrut,
					convert(char(8), b.cpy_feult, 112),
					b.cpy_userid
				from GesPet..HEDT002 b
				where b.estado = 'B'

			-- HEDT003
			insert into GesPet..tmpHEDTHST (
					HEDT0_table,
					HEDT0_dsn_id,
					HEDT0_cpy_id,
					HEDT0_cpo_id,
					HEDT3_cpo_nrobyte,
					HEDT3_cpo_canbyte,
					HEDT3_cpo_type,
					HEDT3_cpo_decimals,
					HEDT3_cpo_dsc,
					HEDT3_cpo_nomrut,
					HEDT3_cpo_feult,
					HEDT3_cpo_userid)
				select
					'HEDT003',
					c.dsn_id,
					c.cpy_id,
					c.cpo_id,
					cpo_nrobyte = 
					case
						when datalength(rtrim(c.cpo_nrobyte)) = 1 then '000' + rtrim(c.cpo_nrobyte)
						when datalength(rtrim(c.cpo_nrobyte)) = 2 then '00' + rtrim(c.cpo_nrobyte) 
						when datalength(rtrim(c.cpo_nrobyte)) = 3 then '0' + rtrim(c.cpo_nrobyte)
						when datalength(rtrim(c.cpo_nrobyte)) = 4 then rtrim(c.cpo_nrobyte)
					end,
					cpo_canbyte = 
					case 
						when datalength(rtrim(c.cpo_canbyte)) = 1 then '000' + rtrim(c.cpo_canbyte)
						when datalength(rtrim(c.cpo_canbyte)) = 2 then '00' + rtrim(c.cpo_canbyte)
						when datalength(rtrim(c.cpo_canbyte)) = 3 then '0' + rtrim(c.cpo_canbyte)
						when datalength(rtrim(c.cpo_canbyte)) = 4 then rtrim(c.cpo_canbyte)
					end,
					c.cpo_type,
					cpo_decimals = 
					case 
						when datalength(rtrim(c.cpo_decimals)) = 1 then '000' + rtrim(c.cpo_decimals)
						when datalength(rtrim(c.cpo_decimals)) = 2 then '00' + rtrim(c.cpo_decimals)
						when datalength(rtrim(c.cpo_decimals)) = 3 then '0' + rtrim(c.cpo_decimals)
						when datalength(rtrim(c.cpo_decimals)) = 4 then rtrim(c.cpo_decimals)
					end,
					c.cpo_dsc,
					c.cpo_nomrut,
					convert(char(8), c.cpo_feult, 112),
					c.cpo_userid
				from GesPet..HEDT003 c
				where c.estado = 'B'
		end
	else
		begin
			-- Marco los registro que transmit� como ya enviados
			-- HEDT001
			update GesPet..HEDT001
			set 
				estado		  = 'C',
				fe_hst_estado = getdate()
			where estado = 'B'

			-- HEDT002
			update GesPet..HEDT002
			set 
				estado		  = 'C',
				fe_hst_estado = getdate()
			where estado = 'B'

			-- HEDT003
			update GesPet..HEDT003
			set 
				estado		  = 'C',
				fe_hst_estado = getdate()
			where estado = 'B'
		end

	--{ add -002- a. Depura aquellas solicitudes pendientes de aprobaci�n (nivel 3) con m�s de N d�as desde su solicitud.
	select @dias = 60
	select @dias = a.var_numero
	from GesPet..Varios a
	where a.var_codigo = 'TSOENM1'
	
	delete from GesPet..Solicitudes
	where 
		sol_estado = 'A' and 
		sol_mask = 'N' and
		datediff(dd, sol_fecha, getdate()) > @dias
	--}
	return(0)
go

grant execute on dbo.sp_HEDTTransmitir to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
