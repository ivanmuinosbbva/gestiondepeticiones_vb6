/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 17.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionBeneficios'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionBeneficios' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionBeneficios
go

create procedure dbo.sp_DeletePeticionBeneficios
	@pet_nrointerno	int=0,
	@indicadorId	int=0,
	@subindicadorId	int=0
as
	delete from
		GesPet.dbo.PeticionBeneficios
	where
		pet_nrointerno = @pet_nrointerno and
		(@indicadorId is null or indicadorId = @indicadorId) and
		(@subindicadorId is null or subindicadorId = @subindicadorId)
go

grant execute on dbo.sp_DeletePeticionBeneficios to GesPetUsr
go

print 'Actualización realizada.'
go
