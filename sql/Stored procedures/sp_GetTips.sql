/*
-000- a. FJS 24.11.2008 - Nuevo SP para devolver los TIPs del sistema.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetTips'
go

if exists (select * from sysobjects where name = 'sp_GetTips' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetTips
go

create procedure dbo.sp_GetTips
	@tips_version	char(10)=null,
	@tips_tipo		char(6)=null,
	@tips_nro		int=null,
	@tips_rng		int=null
as
	select
		TIP.tips_version,
		TIP.tips_tipo,
		TIP.tips_nro,
		TIP.tips_rng,
		TIP.tips_txt,
		char_length(TIP.tips_txt) as largo,
		TIP.tips_date,
		TIP.tips_hab
	from 
		GesPet..Tips TIP
	where
		(TIP.tips_version = @tips_version or @tips_version is null) and 
		(TIP.tips_tipo = @tips_tipo or @tips_tipo is null) and 
		(TIP.tips_nro = @tips_nro or @tips_nro is null) and 
		(TIP.tips_rng = @tips_rng or @tips_rng is null)
go

grant execute on dbo.sp_GetTips to GesPetUsr
go

print 'Actualización realizada.'
