/*
Este SP vuelca en la tabla PeticionChangeMan todas las peticiones enviadas
*/

use GesPet
go

print 'sp_InsertPetChangeMan2'
go

if exists (select * from sysobjects where name = 'sp_InsertPetChangeMan2' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPetChangeMan2
go

create procedure dbo.sp_InsertPetChangeMan2
    (@pet_nrointerno    int)  
as    
  
insert into dbo.PeticionChangeMan  
    select 
		a.pet_nrointerno, 
		a.pet_sector, 
		a.pet_grupo, 
		a.pet_record, 
		a.pet_date  
    from 
		GesPet.dbo.PeticionEnviadas a left join 
		GesPet.dbo.PeticionChangeMan b on a.pet_nrointerno = b.pet_nrointerno
    where 
		(a.pet_nrointerno = @pet_nrointerno Or @pet_nrointerno is null) and
        (b.pet_date is null or b.pet_date = '')

return(0)
go

grant Execute  on dbo.sp_InsertPetChangeMan2 to GesPetUsr 
go
