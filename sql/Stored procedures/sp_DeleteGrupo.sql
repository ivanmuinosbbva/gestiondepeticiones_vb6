use GesPet
go
print 'sp_DeleteGrupo'
go
if exists (select * from sysobjects where name = 'sp_DeleteGrupo' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteGrupo
go
create procedure dbo.sp_DeleteGrupo 
           @cod_grupo   char(8) 
as 
 
if exists (select cod_grupo from GesPet..Grupo where RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
begin 
     
    if exists (select cod_grupo from GesPet..Recurso where RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
    begin 
        raiserror  30011 'Existen Recursos asociados a este Grupo'  
        select 30011 as ErrCode , 'Existen Recursos asociados a este Grupo'  as ErrDesc 
    return (30011) 
    end 
 
    if exists (select cod_recurso from GesPet..RecursoPerfil where RTRIM(cod_nivel) = 'GRUP' and RTRIM(cod_area) = RTRIM(@cod_grupo)) 
    begin 
        raiserror  30011 'Existen Perfiles asociados a este Sector'  
        select 30011 as ErrCode , 'Existen Perfiles asociados a este Sector'  as ErrDesc 
    return (30011) 
    end 
 
     
    if exists (select cod_grupo from GesPet..PeticionGrupo where RTRIM(cod_grupo) = RTRIM(@cod_grupo)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a este Grupo'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a este Grupo'  as ErrDesc 
    return (30011) 
    end 
     
    delete GesPet..Grupo 
      where RTRIM(cod_grupo) = RTRIM(@cod_grupo) 
end 
else 
begin 
    raiserror 30001 'Grupo Inexistente'   
    select 30001 as ErrCode , 'Grupo Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteGrupo to GesPetUsr 
go


