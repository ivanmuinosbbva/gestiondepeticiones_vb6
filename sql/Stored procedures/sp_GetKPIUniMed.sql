/*
-000- a. FJS 22.02.2016 - Nuevo SP para obtener las unidades de medida.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetKPIUniMed'
go

if exists (select * from sysobjects where name = 'sp_GetKPIUniMed' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetKPIUniMed
go

create procedure dbo.sp_GetKPIUniMed
	@kpiid		int
as 
	select 
		i.kpiid,
		um.unimedId,
		um.unimedDesc,
		um.unimedSmb 
	from
		IndicadorKPIUniMed i inner join
		UnidadMedida um on (i.unimedId = um.unimedId)
	where
		i.kpiid = @kpiid
	return(0)
go

grant execute on dbo.sp_GetKPIUniMed to GesPetUsr
go

print 'Actualización realizada.'
go
