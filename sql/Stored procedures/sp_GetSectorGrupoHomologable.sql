/*
-000- a. FJS 28.02.2008 - Nuevo SP para obtener los grupos homologables de un sector específico
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSectorGrupoHomologable'
go

if exists (select * from sysobjects where name = 'sp_GetSectorGrupoHomologable' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSectorGrupoHomologable
go

create procedure dbo.sp_GetSectorGrupoHomologable
	@cod_sector char(8)=NULL
as 
    select
        G.cod_grupo,
        G.nom_grupo,
        G.cod_sector,
        G.flg_habil,
        G.es_ejecutor,
        G.cod_bpar,
        G.grupo_homologacion
    from
        GesPet..Grupo G
    where
        RTRIM(G.cod_sector) = RTRIM(@cod_sector) and
        G.grupo_homologacion = 'S'
return(0) 
go

grant execute on dbo.sp_GetSectorGrupoHomologable to GesPetUsr
go

print 'Actualización realizada.'
