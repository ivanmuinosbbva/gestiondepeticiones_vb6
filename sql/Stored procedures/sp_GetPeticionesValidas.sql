/*
-000- a. FJS 07.08.2008 - Nuevo SP para mostrar a la gente de calidad las peticiones que se encuentran en el archivo
						  para pasar a Changeman.
-001- a. FJS 23.09.2009 - Se corrige el SP porque esta buscando en la tabla equivocada (en vez de mirar el diario, esta mirando el histórico).
-001- b. FJS 24.09.2009 - Se agrega el campo de estado actual de la petición.
-002- a. FJS 23.02.2010 - Se elimina esta sentencia.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesValidas'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesValidas' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesValidas
go

create procedure dbo.sp_GetPeticionesValidas
	@NroDesde		int=null, 
	@NroHasta		int=null, 
	@Tipo			char(3)=null, 
	@Clase			char(4)=null,
	@Titulo			char(50)=null,
	@Prioridad		char(1)=null,
	@FechaDesde		smalldatetime=null,
	@FechaHasta		smalldatetime=null
as 
	select
		CHG.pet_nrointerno,
		PET.pet_nroasignado,
		PET.cod_tipo_peticion,
		PET.cod_clase,
		PET.titulo,
		PET.prioridad,
		nom_sector = (select
							RTRIM(DIR.nom_direccion) + ' >> ' + RTRIM(GER.nom_gerencia) + ' >> ' + RTRIM(SEC.nom_sector)
					  from
							GesPet..Direccion DIR inner join
							GesPet..Gerencia GER on (DIR.cod_direccion = GER.cod_direccion) inner join
							GesPet..Sector SEC on (GER.cod_gerencia = SEC.cod_gerencia)
					  where
							SEC.cod_sector = PET.cod_sector),
		max(CHG.pet_date) as fecha
		--{ add -001- b.
		,PET.cod_estado
		,nom_estado = (select nom_estado from GesPet..Estados where cod_estado = PET.cod_estado)
		--}
	from
		--GesPet..Peticion PET inner join GesPet..PeticionEnviadas CHG on (PET.pet_nrointerno = CHG.pet_nrointerno)	-- del -001- a. 
		GesPet..PeticionChangeMan CHG inner join 
		GesPet..Peticion PET on (PET.pet_nrointerno = CHG.pet_nrointerno)	-- add -001- a.
	where
		--(PET.pet_nrointerno in (select CHG.pet_nrointerno from GesPet..PeticionEnviadas CHG)) and		-- del -001- a.
		--(PET.pet_nrointerno in (select CHG.pet_nrointerno from GesPet..PeticionChangeMan CHG)) and		-- add -001- a.	-- del -002- a.
		(@NroDesde is null or PET.pet_nroasignado >= @NroDesde) and
		(@NroHasta is null or PET.pet_nroasignado <= @NroHasta) and
		(@Tipo is null or PET.cod_tipo_peticion = @Tipo) and
		(@Clase is null or PET.cod_clase = @Clase) and
		(@Titulo is null or upper(PET.titulo) like '%' + upper(rtrim(@Titulo)) + '%') and
		(@Prioridad is null or PET.prioridad = @Prioridad) and
		(@FechaDesde is null or @FechaDesde >= pet_date) and
		(@FechaHasta is null or @FechaHasta <= pet_date)
	group by
		CHG.pet_nrointerno,
		PET.pet_nroasignado,
		PET.cod_tipo_peticion,
		PET.cod_clase,
		PET.cod_sector,
		PET.titulo,
		PET.prioridad
		,PET.cod_estado		-- add -001- a.
	order by
		PET.pet_nroasignado
go

grant execute on dbo.sp_GetPeticionesValidas to GesPetUsr
go

print 'Actualización realizada.'