use GesPet
go
print 'sp_InsertAvanceGrupo'
go
if exists (select * from sysobjects where name = 'sp_InsertAvanceGrupo' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertAvanceGrupo
go
create procedure dbo.sp_InsertAvanceGrupo
	@cod_AvanceGrupo	char(8),
	@nom_AvanceGrupo	char(30),	
	@flg_habil		char(1)=null,
	@txt_AvanceGrupo	char(100)

as

if exists (select cod_AvanceGrupo from GesPet..AvanceGrupo where RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo))
	begin 
		raiserror 30011 'Codigo de AvanceGrupo Existente'   
		select 30011 as ErrCode , 'Codigo de AvanceGrupo Existente' as ErrDesc   
		return (30011)   
	end
else
	begin 
	insert into GesPet..AvanceGrupo
			(cod_AvanceGrupo, nom_AvanceGrupo,txt_AvanceGrupo, flg_habil)
		values
			(@cod_AvanceGrupo, @nom_AvanceGrupo, @txt_AvanceGrupo, @flg_habil)
	end 
return(0)
go


grant execute on dbo.sp_InsertAvanceGrupo to GesPetUsr
go
