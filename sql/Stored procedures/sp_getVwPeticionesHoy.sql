/*
-000- a. FJS 20.06.2008 - Nuevo SP para interacturar desde el sistema SGI.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_getVwPeticionesHoy'
go

if exists (select * from sysobjects where name = 'sp_getVwPeticionesHoy' and sysstat & 7 = 4)
	drop procedure dbo.sp_getVwPeticionesHoy
go

create procedure dbo.sp_getVwPeticionesHoy
as 
    select 
        NroInterno,  
        NroAsignado, 
        PeticionTipo, 
        PeticionClase, 
        PeticionTitulo, 
        PeticionDescripcion, 
        PeticionFechaAlta, 
        FechaUltimoEstado, 
        PeticionEstado 
    from  
        GesPet.dbo.vwPeticionesHoy
go

grant execute on dbo.sp_getVwPeticionesHoy to GesPetUsr
go

grant execute on dbo.sp_getVwPeticionesHoy to cgm_gesinc
go

print 'Actualización realizada.'
