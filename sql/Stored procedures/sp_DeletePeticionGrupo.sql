use GesPet
go
print 'sp_DeletePeticionGrupo'
go
if exists (select * from sysobjects where name = 'sp_DeletePeticionGrupo' and sysstat & 7 = 4)
drop procedure dbo.sp_DeletePeticionGrupo
go
create procedure dbo.sp_DeletePeticionGrupo 
    @pet_nrointerno     int, 
    @cod_sector         char(8)=null, 
    @cod_grupo          char(8)=null 
as 
/* falta validar */ 
if @cod_grupo is null 
begin 
    delete GesPet..PeticionGrupo 
      where @pet_nrointerno=pet_nrointerno and RTRIM(cod_sector)=RTRIM(@cod_sector) 
end 
else 
begin 
    if exists (select cod_sector from GesPet..PeticionGrupo where @pet_nrointerno=pet_nrointerno and RTRIM(cod_sector)=RTRIM(@cod_sector)  and RTRIM(cod_grupo)=RTRIM(@cod_grupo)) 
    begin 
        /* 
        if exists (select cod_sector from GesPet..Recurso where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
        begin 
            raiserror  30011 'Existen Recursos asociados a este Sector'  
            select 30011 as ErrCode, 'Existen Recursos asociados a este Sector'  as ErrDesc 
        return (30011) 
        end 
        if exists (select cod_sector from GesPet..Peticion where RTRIM(cod_sector) = RTRIM(@cod_sector)) 
        begin 
            raiserror  30011 'Existen Peticiones asociadas a este Sector'  
            select 30011 as ErrCode, 'Existen Peticiones asociadas a este Sector'  as ErrDesc 
        return (30011) 
        end 
        */ 
        delete GesPet..PeticionGrupo 
          where @pet_nrointerno=pet_nrointerno and 
          RTRIM(cod_sector)=RTRIM(@cod_sector) and  
          RTRIM(cod_grupo)=RTRIM(@cod_grupo) 
    end 
    else 
    begin 
        raiserror 30001 'Grupo Inexistente'   
        select 30001 as ErrCode, 'Grupo Inexistente' as ErrDesc   
        return (30001)   
    end 
end 
return(0) 
go



grant execute on dbo.sp_DeletePeticionGrupo to GesPetUsr 
go


