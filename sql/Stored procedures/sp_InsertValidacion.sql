/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertValidacion'
go

if exists (select * from sysobjects where name = 'sp_InsertValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertValidacion
go

create procedure dbo.sp_InsertValidacion
	@valid		int,
	@valitem	int,
	@valdesc	varchar(255),
	@valorden	int,
	@valhab		char(1)='S'
as
	insert into GesPet.dbo.Validacion (
		valid,
		valitem,
		valdesc,
		valorden,
		valhab,
		audituser,
		auditfecha)
	values (
		@valid,
		@valitem,
		@valdesc,
		@valorden,
		@valhab,
		suser_name(),
		getdate())
go

grant execute on dbo.sp_InsertValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
