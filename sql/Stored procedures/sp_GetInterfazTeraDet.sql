/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 21.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetInterfazTeraDet'
go

if exists (select * from sysobjects where name = 'sp_GetInterfazTeraDet' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetInterfazTeraDet
go

create procedure dbo.sp_GetInterfazTeraDet
	@dbName			varchar(30)=null,
	@objname		varchar(50)=null,
	@objfield		varchar(50)=null,
	@objfieldhab	char(1)='S'
as
	select 
		ag.dbName,
		ag.objname,
		ag.objfield,
		ag.objfieldhab,
		ag.objfielddesc,
		ag.objultact
	from 
		GesPet.dbo.InterfazTeraDet ag
	where
		(@dbName is null or LTRIM(RTRIM(ag.dbName)) = LTRIM(RTRIM(@dbName))) and
		(@objname is null or LTRIM(RTRIM(ag.objname)) = LTRIM(RTRIM(@objname))) and
		(@objfield is null or LTRIM(RTRIM(ag.objfield)) = LTRIM(RTRIM(@objfield))) and
		(@objfieldhab is null or ag.objfieldhab = @objfieldhab)
go

grant execute on dbo.sp_GetInterfazTeraDet to GesPetUsr
go

print 'Actualización realizada.'
go
