/*
-001- a. FJS 04.07.2007 - Se agregan al conjunto de resultados las columnas cod_class y pet_imptech de la tabla Peticion
-002- a. FJS 06.07.2007 - Se agrega a la consulta los condicionamientos para soportar filtro por clase de petici�n e indicador de impacto tecnol�gico.
-003- a. FJS 16.05.2008 - Se completa en la exportaci�n los indicadores de documentos adjuntos que posee la petici�n
-004- a. FJS 23.05.2008 - Se agregan 4 campos al conjunto de resultado para la parte de homologaci�n.
-005- a. FJS 06.08.2008 - Se cambia lo que antes era el T700 por el nuevo producto SOx T710.
-006- a. FJS 10.09.2008 - Se agrega el campo 'pet_regulatorio' al conjunto de resultados y el filtro correspondiente.
-006- b. FJS 12.09.2008 - Se agrega la identificaci�n de documentos adjuntos (gen�rico) y de los conformes (aqu� si desglosado)
-007- a. FJS 25.09.2008 - Se agregan los datos de Proyectos IDM para la exportaci�n.
-008- a. FJS 31.10.2008 - Se agrega la opci�n de poder listar peticiones con el indicador de impacto tecnol�gico en Indeterminado.
-009- a. FJS 03.02.2011 - Mejora: se quita la parte de proyectos que no tiene utilidad.
-010- a. FJS 12.03.2012 - Fusi�n Banco/Consolidar.
-010- b. FJS 24.04.2012 - Mejora: para filtrar por empresa, se envia una cadena con las empresas a filtrar.
-011- a. FJS 12.07.2012 - Nuevo: Se agrega el campo para indentificar peticiones vinculadas a la mitigaci�n de factores de Riesgo Operacional.
-012- a. FJS 16.02.2016 - Nuevo: se agrega funcionalidad para la nueva gerencia de BPE.
-013- a. FJS 29.06.2016 - Nuevo: nuevo campo calculado para determinar la puntuaci�n de una petici�n (basado en f�rmula de valorizaci�n).
-014- a. FJS 29.06.2016 - Nuevo: nuevo campo calculado para determinar la categor�a de una petici�n.
*/

use GesPet
go

print 'Actualizando el procedimiento almacenado: sp_rptPeticion03'
go

if exists (select * from sysobjects where name = 'sp_rptPeticion03' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptPeticion03
go

create procedure dbo.sp_rptPeticion03
    @pet_nrodesde			int=0,  
    @pet_nrohasta			int=0,  
    @pet_tipo				varchar(4)='NULL',  
    @pet_prioridad			varchar(4)='NULL',  
    @pet_desde_bpar			varchar(8)='NULL',
    @pet_hasta_bpar			varchar(8)='NULL',
    @pet_nivel				varchar(4)='NULL',  
    @pet_area				varchar(8)='NULL',  
    @pet_estado				varchar(250)='NULL',  
    @pet_desde_estado		varchar(8)='NULL',  
    @pet_hasta_estado		varchar(8)='NULL',  
    @pet_situacion			varchar(8)='NULL',  
    @pet_titulo				varchar(120)='NULL',  
    @sec_nivel				varchar(4)='NULL',  
    @sec_dire				varchar(8)='NULL',  
    @sec_gere				varchar(8)='NULL',  
    @sec_sect				varchar(8)='NULL',
    @sec_grup				varchar(8)='NULL',  
    @sec_estado				varchar(250)='NULL',  
    @sec_situacion			varchar(8)='NULL',  
    @sec_desde_iplan		varchar(8)='NULL',  
    @sec_hasta_iplan		varchar(8)='NULL',  
    @sec_desde_fplan		varchar(8)='NULL',  
    @sec_hasta_fplan		varchar(8)='NULL',  
    @sec_desde_ireal		varchar(8)='NULL',  
    @sec_hasta_ireal		varchar(8)='NULL',  
    @sec_desde_freal		varchar(8)='NULL',  
    @sec_hasta_freal		varchar(8)='NULL',   
    @agr_nrointerno			int=0,  
    @cod_importancia		varchar(4)='NULL',  
    @cod_bpar				varchar(10)='NULL',  
    @cod_clase				char(4)=null,		--'NULL',  
    @pet_imptech			char(1)=null,
	@pet_regulatorio		char(1)=null,
	@pet_emp				int=null,
	@pet_ro					char(1)=null,
	@prjid					int=null,
	@prjsubid				int=null,
	@prjsubsid				int=null,
	@cod_GBPE				char(10)=null,
	@fechaDesdeBPE			varchar(8)='NULL',
	@fechaHastaBPE			varchar(8)='NULL',
	@categoria				char(3)=null,
	@pet_desde_alta			varchar(8)='NULL',
	@pet_hasta_alta			varchar(8)='NULL'
as  
	declare @secuencia		int  
	declare @ret_status		int  
	declare @categoria_loc	char(6)
	
	--{ add -018- a.
	if @categoria is not null
		select @categoria_loc = case
			when @categoria = 'REG' then '1. REG'
			when @categoria = 'SDA' then '2. SDA'
			when @categoria = 'PES' then '3. PES'
			when @categoria = 'ENG' then '4. ENG'
			when @categoria = 'PRI' then '5. PRI'
			when @categoria = 'CSI' then '6. CSI'
			when @categoria = '---' then '---'
			when @categoria = 'SPR' then '7. SPR'
		end
	--}

	--{ add -014- a.
	create table #tablaCriterio (
		pet_nrointerno		int,
		categoria			char(6))

	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 

	/* Tabla Temporaria */
	create table #AgrupArbol(
		secuencia		int,  
		nivel			int,  
		agr_nrointerno  int)
	--}
	  
	create table #PeticYHijo(  
		pet_nrointerno  int,  
		pet_nroasignado int,  
		hij_cod_direccion varchar(8),  
		hij_cod_gerencia varchar(8),  
		hij_cod_sector varchar(8),  
		hij_cod_grupo varchar(8))
	  
	create table #AgrupXPetic(
		pet_nrointerno  int)  
	  
	--{ add -014- a. Esto es para obtener los agrupamientos de SDA
	declare	@agr_SDA	int
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
			
			INSERT INTO #tablaCriterio
			select 
				p.pet_nrointerno,
				categoria = case
					when p.cod_tipo_peticion in ('AUI','AUX') OR p.pet_regulatorio = 'S' then '1. REG'
					when exists (select x.pet_nrointerno from #AgrupXPeticSDA x where x.pet_nrointerno = p.pet_nrointerno) then '2. SDA'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then '3. PES'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then '4. ENG'
					when p.cod_clase in ('OPTI','CORR','SPUF','ATEN') then '6. CSI'
					when exists (
						 select count(1) 
						 from ProyectoIDM px 
						 where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
						 having count(1) > 0) OR (p.valor_impacto <> 0 OR p.valor_facilidad <>0) then '5. PRI'
					when p.cod_clase = 'SINC' then '---'
					else '7. SPR'
				end
			from Peticion p 
		end
	--}

	/* esto es si se pidio agrupamiento */  
	if @agr_nrointerno<>0  
		begin
			truncate table #AgrupArbol
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)  
			begin  
				if (@ret_status = 30003)  
				begin  
					select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError  
					return (@ret_status)  
				end  
			end  
			insert into #AgrupXPetic (pet_nrointerno)  
				select  AP.pet_nrointerno  
				from	#AgrupArbol AA, AgrupPetic AP, Agrup AG  
				where	AA.agr_nrointerno=AP.agr_nrointerno and  
						AG.agr_nrointerno=AP.agr_nrointerno and  
						AG.agr_vigente='S'  
		end  

	/* si estoy seguro de que no me ineresan los sector/grupo */
	if	@sec_nivel='NULL' and @sec_dire='NULL' and @sec_gere='NULL' and @sec_sect='NULL' and @sec_grup='NULL' and @sec_estado='NULL' and @sec_situacion='NULL' and
		@sec_desde_iplan='NULL' and @sec_hasta_iplan='NULL' and @sec_desde_fplan='NULL' and @sec_hasta_fplan='NULL' and
		@sec_desde_ireal='NULL' and @sec_hasta_ireal='NULL' and @sec_desde_freal='NULL' and @sec_hasta_freal='NULL'
	begin
		 select 
			PET.pet_nrointerno,
			PET.pet_nroasignado,
			PET.titulo,
			PET.cod_tipo_peticion,
			PET.prioridad,
			PET.corp_local,
			PET.cod_orientacion,
			nom_orientacion = (select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),
			PET.importancia_cod,
			importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod),
			PET.pet_nroanexada,
			PET.fe_pedido,
			PET.fe_requerida,
			PET.fe_comite,
			PET.fe_ini_plan,
			PET.fe_fin_plan,
			PET.fe_ini_real,
			PET.fe_fin_real,
			PET.fe_ini_orig,
			PET.fe_fin_orig,
			PET.cant_planif,
			PET.horaspresup,
			PET.cod_direccion,
			PET.cod_gerencia,
			PET.cod_sector,
			PET.cod_bpar,
			nom_bpar = isnull((select R1.nom_recurso from Recurso R1 where RTRIM(R1.cod_recurso)=RTRIM(PET.cod_bpar)),''),
			PET.cod_estado,
			nom_estado = (select ESTPET.nom_estado from Estados ESTPET where ESTPET.cod_estado=PET.cod_estado),
			PET.fe_estado,
			PET.cod_situacion,
			nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion),
			sec_cod_direccion=null,
			sec_nom_direccion=null,
			sec_cod_gerencia=null,
			sec_nom_gerencia=null,
			sec_cod_sector=null,
			sec_nom_sector=null,
			sec_cod_grupo=null,
			sec_nom_grupo=null,
			sec_cod_estado=null,
			sec_nom_estado=null,
			sec_cod_situacion=null,
			sec_nom_situacion=null,
			sec_horaspresup=0,
			horasCargadas = (
				select SUM(convert(REAL, HRS.horas) / convert(REAL, 60))
				from GesPet..HorasTrabajadas HRS 
				where HRS.pet_nrointerno = PET.pet_nrointerno
			),
			sec_fe_ini_plan=null,
			sec_fe_fin_plan=null,
			sec_fe_ini_real=null,
			sec_fe_fin_real=null,
			sec_fe_ini_orig=null,
			sec_fe_fin_orig=null,
			sol_direccion = (select nom_direccion from Direccion where cod_direccion=PET.cod_direccion),
			sol_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=PET.cod_gerencia),
			sol_sector = (select nom_sector from Sector where cod_sector=PET.cod_sector),
			prio_ejecuc=0,
			info_adicio=null,
			PET.prj_nrointerno,
			prj_titulo = '',
			PET.cod_clase,
			PET.pet_imptech,
			PET.pet_regulatorio,
			PET.pet_projid,
			PET.pet_projsubid,
			PET.pet_projsubsid,
			projnom = (select PRJIDM.ProjNom from GesPet..ProyectoIDM PRJIDM where PRJIDM.ProjId = PET.pet_projid and PRJIDM.ProjSubId = PET.pet_projsubid and PRJIDM.ProjSubSId = PET.pet_projsubsid),
			PET.pet_emp,
			pet_ro = isnull(PET.pet_ro,'-'),
			pet_riesgo = (case
				when PET.pet_ro = 'A' then 'Alta'
				when PET.pet_ro = 'M' then 'Media'
				when PET.pet_ro = 'B' then 'Baja'
				else 'No'
			end),
			PET.cargaBeneficios,
			PET.pet_driver,
			PET.cod_BPE,
			nomBPE = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
			PET.fecha_BPE,
			PET.valor_impacto,
			PET.valor_facilidad,
			ISNULL(PET.puntuacion,0)	as puntuacion,
			c.categoria
		from  
			GesPet..Peticion PET INNER JOIN 
				#tablaCriterio c on (PET.pet_nrointerno = c.pet_nrointerno)
		where   
			(@pet_emp is null or PET.pet_emp = @pet_emp) and
			(@pet_ro is null or @pet_ro = PET.pet_ro) and 
			(@pet_nrodesde = 0 or PET.pet_nroasignado >= @pet_nrodesde) and
			(@pet_nrohasta = 0 or PET.pet_nroasignado <= @pet_nrohasta) and
			(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and
			(@pet_estado is null or RTRIM(@pet_estado)='NULL' or charindex(RTRIM(PET.cod_estado),RTRIM(@pet_estado))>0) and
			(@pet_tipo is null or RTRIM(@pet_tipo)='NULL' or RTRIM(PET.cod_tipo_peticion) = RTRIM(@pet_tipo)) and  
			(@cod_clase is null or @cod_clase = 'NULL' or @cod_clase=PET.cod_clase) and
			(@pet_imptech is null or @pet_imptech=PET.pet_imptech) and 
			(@pet_titulo is null or RTRIM(@pet_titulo)='NULL' or charindex(UPPER(RTRIM(@pet_titulo)),UPPER(RTRIM(PET.titulo)))>0) and  
			(@pet_prioridad is null or RTRIM(@pet_prioridad)='NULL'or RTRIM(PET.prioridad) = RTRIM(@pet_prioridad)) and 
			(@cod_importancia is null or RTRIM(@cod_importancia)='NULL' or RTRIM(PET.importancia_cod) = RTRIM(@cod_importancia)) and  
			(@cod_bpar is null or RTRIM(@cod_bpar)='NULL' or RTRIM(PET.cod_bpar) = RTRIM(@cod_bpar)) and  
			(RTRIM(@pet_desde_estado)='NULL' or convert(char(8),PET.fe_estado,112) >= @pet_desde_estado) and  
			(RTRIM(@pet_hasta_estado)='NULL' or convert(char(8),PET.fe_estado,112) <= @pet_hasta_estado) and  
			(RTRIM(@pet_desde_bpar)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_bpar) and  
			(RTRIM(@pet_hasta_bpar)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_bpar) and  
			(@pet_situacion is null or RTRIM(@pet_situacion)='NULL' or RTRIM(PET.cod_situacion) = RTRIM(@pet_situacion)) and  
			((@pet_nivel is null or RTRIM(@pet_nivel)='NULL') or  
			 (@pet_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@pet_area)) or  
			 (@pet_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@pet_area)) or  
			 (@pet_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@pet_area)))  
			and (PET.pet_regulatorio = @pet_regulatorio or @pet_regulatorio is null)
			and (
				(PET.pet_projid = @prjid or @prjid is null) and
				(PET.pet_projsubid = @prjsubid or @prjsubid is null) and
				(PET.pet_projsubsid = @prjsubsid or @prjsubsid is null)
			)
			and (@cod_GBPE is null or @cod_GBPE = 'NULL' or LTRIM(RTRIM(PET.cod_BPE)) = LTRIM(RTRIM(@cod_GBPE)))
			and (RTRIM(@fechaDesdeBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) >= @fechaDesdeBPE) 
			and (RTRIM(@fechaHastaBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) <= @fechaHastaBPE)
			and (@categoria IS NULL OR c.categoria = @categoria_loc)
			and (RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_pedido,112) >= @pet_desde_alta)
			and (RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_pedido,112) <= @pet_hasta_alta)
		order by 
			PET.pet_nroasignado,
			PET.pet_nrointerno  
		return(0)  
	end  
	  
	/* para grupo busca en grupos, para el resto lo hace en sectores */  
	if @sec_nivel='GRUP'  
		begin  
		 select 
			PET.pet_nrointerno,  
			PET.pet_nroasignado,  
			PET.titulo,  
			PET.cod_tipo_peticion,  
			PET.prioridad,  
			PET.corp_local,  
			PET.cod_orientacion,
			nom_orientacion = (select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),  
			PET.importancia_cod,  
			importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod),  
			PET.pet_nroanexada,  
			PET.fe_pedido,  
			PET.fe_requerida,  
			PET.fe_comite,  
			PET.fe_ini_plan,  
			PET.fe_fin_plan,  
			PET.fe_ini_real,  
			PET.fe_fin_real,  
			PET.fe_ini_orig,  
			PET.fe_fin_orig,  
			PET.cant_planif,  
			PET.horaspresup,  
			horasCargadas = (
				select SUM(convert(REAL, HRS.horas) / convert(REAL, 60))
				from GesPet..HorasTrabajadas HRS 
				where HRS.pet_nrointerno = PET.pet_nrointerno
			),
			PET.cod_direccion,  
			PET.cod_gerencia,  
			PET.cod_sector,  
			PET.cod_bpar,  
			nom_bpar = isnull((select R1.nom_recurso from Recurso R1 where RTRIM(R1.cod_recurso)=RTRIM(PET.cod_bpar)),''),  
			PET.cod_estado,  
			nom_estado = (select ESTPET.nom_estado from Estados ESTPET where ESTPET.cod_estado=PET.cod_estado),  
			PET.fe_estado,  
			PET.cod_situacion,  
			nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion),  
			sec_cod_direccion=HIJ.cod_direccion,  
			sec_nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=HIJ.cod_direccion),  
			sec_cod_gerencia=HIJ.cod_gerencia,  
			sec_nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where Ge.cod_gerencia=HIJ.cod_gerencia),  
			sec_cod_sector=HIJ.cod_sector,  
			sec_nom_sector = (select Gr.nom_sector from Sector Gr where Gr.cod_sector=HIJ.cod_sector),  
			sec_cod_grupo=HIJ.cod_grupo,  
			sec_nom_grupo = (select Sb.nom_grupo from Grupo Sb where Sb.cod_grupo=HIJ.cod_grupo),
			sec_cod_estado=HIJ.cod_estado,  
			sec_nom_estado = (select ESTHIJ.nom_estado from Estados ESTHIJ where ESTHIJ.cod_estado=HIJ.cod_estado),  
			sec_cod_situacion=HIJ.cod_situacion,  
			sec_nom_situacion = (select SITHIJ.nom_situacion from Situaciones SITHIJ where SITHIJ.cod_situacion=HIJ.cod_situacion),  
			sec_horaspresup=HIJ.horaspresup,  
			sec_fe_ini_plan=HIJ.fe_ini_plan,  
			sec_fe_fin_plan=HIJ.fe_fin_plan,  
			sec_fe_ini_real=HIJ.fe_ini_real,  
			sec_fe_fin_real=HIJ.fe_fin_real,  
			sec_fe_ini_orig=HIJ.fe_ini_orig,  
			sec_fe_fin_orig=HIJ.fe_fin_orig,  
			sol_direccion = (select nom_direccion from Direccion where cod_direccion=PET.cod_direccion),  
			sol_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=PET.cod_gerencia),  
			sol_sector = (select nom_sector from Sector where cod_sector=PET.cod_sector),  
			HIJ.prio_ejecuc,  
			HIJ.info_adicio,  
			PET.prj_nrointerno,
			HIJ.fe_produccion,
			HIJ.fe_suspension,
			nom_motivo = (select nom_motivo from GesPet..Motivos where cod_motivo = HIJ.cod_motivo),
			null as ObsEstado,
			prj_titulo = '',
			PET.cod_clase,  
			PET.pet_imptech,  
			PET.pet_regulatorio,
			PET.pet_projid,
			PET.pet_projsubid,
			PET.pet_projsubsid,
			projnom = (select PRJIDM.ProjNom from GesPet..ProyectoIDM PRJIDM where PRJIDM.ProjId = PET.pet_projid and PRJIDM.ProjSubId = PET.pet_projsubid and PRJIDM.ProjSubSId = PET.pet_projsubsid),
			PET.pet_emp,
			pet_ro = isnull(PET.pet_ro,'-'),
			pet_riesgo = (case
				when PET.pet_ro = 'A' then 'Alta'
				when PET.pet_ro = 'M' then 'Media'
				when PET.pet_ro = 'B' then 'Baja'
				else 'No'
			end),
			PET.cargaBeneficios,
			PET.pet_driver,
			PET.cod_BPE,
			nomBPE = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
			PET.fecha_BPE,
			PET.valor_impacto,
			PET.valor_facilidad,
			ISNULL(PET.puntuacion,0)	as puntuacion,
			c.categoria,
			horas_grupo = (
				select SUM(CONVERT(REAL, HRS.horas)/CONVERT(REAL,60)) as Horas
				from
					GesPet..HorasTrabajadas HRS inner join
					GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso INNER join
					GesPet..PeticionGrupo pg on HRS.pet_nrointerno = pg.pet_nrointerno and REC.cod_sector = pg.cod_sector and REC.cod_grupo = pg.cod_grupo
				where
					HRS.pet_nrointerno = PET.pet_nrointerno and
					(RTRIM(REC.cod_sector) = RTRIM(HIJ.cod_sector)) and
					(RTRIM(REC.cod_grupo) = RTRIM(HIJ.cod_grupo)))
		from    
			GesPet..Peticion PET,  
			#tablaCriterio c,
			GesPet..PeticionGrupo HIJ  
		where   
			(@pet_emp is null or PET.pet_emp = @pet_emp) and 
			(@pet_ro is null or @pet_ro = PET.pet_ro) and
			(@pet_nrodesde = 0 or PET.pet_nroasignado >= @pet_nrodesde) and  
			(@pet_nrohasta = 0 or PET.pet_nroasignado <= @pet_nrohasta) and  
			(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and  
			(@pet_estado is null or RTRIM(@pet_estado)='NULL' or charindex(RTRIM(PET.cod_estado),RTRIM(@pet_estado))>0) and  
			(@pet_tipo is null or RTRIM(@pet_tipo)='NULL' or RTRIM(PET.cod_tipo_peticion) = RTRIM(@pet_tipo)) and  
			(@cod_clase is null or @cod_clase = 'NULL' or @cod_clase=PET.cod_clase) and
			(@pet_imptech is null or @pet_imptech=PET.pet_imptech) and
			(@pet_titulo is null or RTRIM(@pet_titulo)='NULL' or charindex(UPPER(RTRIM(@pet_titulo)),UPPER(RTRIM(PET.titulo)))>0) and  
			(@pet_prioridad is null or RTRIM(@pet_prioridad)='NULL'or RTRIM(PET.prioridad) = RTRIM(@pet_prioridad)) and  
			(@cod_importancia is null or RTRIM(@cod_importancia)='NULL' or RTRIM(PET.importancia_cod) = RTRIM(@cod_importancia)) and  
			(@cod_bpar is null or RTRIM(@cod_bpar)='NULL' or RTRIM(PET.cod_bpar) = RTRIM(@cod_bpar)) and  
			(RTRIM(@pet_desde_estado)='NULL' or convert(char(8),PET.fe_estado,112) >= @pet_desde_estado) and  
			(RTRIM(@pet_hasta_estado)='NULL' or convert(char(8),PET.fe_estado,112) <= @pet_hasta_estado) and  
			(RTRIM(@pet_desde_bpar)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_bpar) and  
			(RTRIM(@pet_hasta_bpar)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_bpar) and  
			(@pet_situacion is null or RTRIM(@pet_situacion)='NULL' or RTRIM(PET.cod_situacion) = RTRIM(@pet_situacion)) and  
			((@pet_nivel is null or RTRIM(@pet_nivel)='NULL') or  
			(@pet_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@pet_area)) or  
			(@pet_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@pet_area)) or  
			(@pet_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@pet_area))) and  
			(RTRIM(@sec_desde_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) >= @sec_desde_iplan) and  
			(RTRIM(@sec_hasta_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) <= @sec_hasta_iplan) and  
			(RTRIM(@sec_desde_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) >= @sec_desde_fplan) and  
			(RTRIM(@sec_hasta_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) <= @sec_hasta_fplan) and  
			(RTRIM(@sec_desde_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) >= @sec_desde_ireal) and  
			(RTRIM(@sec_hasta_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) <= @sec_hasta_ireal) and  
			(RTRIM(@sec_desde_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) >= @sec_desde_freal) and  
			(RTRIM(@sec_hasta_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) <= @sec_hasta_freal) and  
			PET.pet_nrointerno = HIJ.pet_nrointerno and  
			PET.pet_nrointerno = c.pet_nrointerno and 
			(RTRIM(@sec_dire)='NULL' or RTRIM(HIJ.cod_direccion)=RTRIM(@sec_dire)) and  
			(RTRIM(@sec_gere)='NULL' or RTRIM(HIJ.cod_gerencia)=RTRIM(@sec_gere)) and  
			(RTRIM(@sec_sect)='NULL' or RTRIM(HIJ.cod_sector)=RTRIM(@sec_sect)) and  
			(RTRIM(@sec_grup)='NULL' or RTRIM(HIJ.cod_grupo)=RTRIM(@sec_grup)) and  
			(@sec_estado is null or RTRIM(@sec_estado)='NULL' or charindex(RTRIM(HIJ.cod_estado),RTRIM(@sec_estado))>0) and  
			(@sec_situacion is null or RTRIM(@sec_situacion)='NULL' or RTRIM(HIJ.cod_situacion) = RTRIM(@sec_situacion))  
			and (PET.pet_regulatorio = @pet_regulatorio or @pet_regulatorio is null)
			and (
				(PET.pet_projid = @prjid or @prjid is null) and
				(PET.pet_projsubid = @prjsubid or @prjsubid is null) and
				(PET.pet_projsubsid = @prjsubsid or @prjsubsid is null)
			)
			and (@cod_GBPE is null or @cod_GBPE = 'NULL' or LTRIM(RTRIM(PET.cod_BPE)) = LTRIM(RTRIM(@cod_GBPE)))
			and (RTRIM(@fechaDesdeBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) >= @fechaDesdeBPE) 
			and (RTRIM(@fechaHastaBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) <= @fechaHastaBPE)
			and (@categoria IS NULL OR c.categoria = @categoria_loc)
			and (RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_pedido,112) >= @pet_desde_alta)
			and (RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_pedido,112) <= @pet_hasta_alta)
		order by 
			PET.pet_nroasignado,
			PET.pet_nrointerno  
		return(0)
	end  
	  
	/* resto lo hace en sectores */  
	select PET.pet_nrointerno,  
		PET.pet_nroasignado,
		PET.titulo,  
		PET.cod_tipo_peticion,  
		PET.prioridad,  
		PET.corp_local,  
		PET.cod_orientacion,  
		nom_orientacion = (select nom_orientacion from Orientacion where cod_orientacion=PET.cod_orientacion),  
		PET.importancia_cod,  
		importancia_nom = (select nom_importancia from Importancia where cod_importancia=PET.importancia_cod),  
		PET.pet_nroanexada,  
		PET.fe_pedido,  
		PET.fe_requerida,  
		PET.fe_comite,  
		PET.fe_ini_plan,  
		PET.fe_fin_plan,  
		PET.fe_ini_real,  
		PET.fe_fin_real,  
		PET.fe_ini_orig,  
		PET.fe_fin_orig,  
		PET.cant_planif,  
		PET.horaspresup,  
		horasCargadas = (
			select SUM(convert(REAL, HRS.horas) / convert(REAL, 60))
			from GesPet..HorasTrabajadas HRS 
			where HRS.pet_nrointerno = PET.pet_nrointerno
		),
		PET.cod_direccion,  
		PET.cod_gerencia,  
		PET.cod_sector,  
		PET.cod_bpar,  
		nom_bpar = isnull((select R1.nom_recurso from Recurso R1 where RTRIM(R1.cod_recurso)=RTRIM(PET.cod_bpar)),''),  
		PET.cod_estado,  
		nom_estado = (select ESTPET.nom_estado from Estados ESTPET where ESTPET.cod_estado=PET.cod_estado),  
		PET.fe_estado,
		PET.cod_situacion,  
		nom_situacion = (select SITPET.nom_situacion from Situaciones SITPET where SITPET.cod_situacion=PET.cod_situacion),  
		sec_cod_direccion=HIJ.cod_direccion,  
		sec_nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=HIJ.cod_direccion),  
		sec_cod_gerencia=HIJ.cod_gerencia,
		sec_nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where Ge.cod_gerencia=HIJ.cod_gerencia),  
		sec_cod_sector=HIJ.cod_sector,  
		sec_nom_sector = (select Gr.nom_sector from Sector Gr where Gr.cod_sector=HIJ.cod_sector),  
		sec_cod_grupo='',  
		sec_nom_grupo='',  
		sec_cod_estado=HIJ.cod_estado,  
		sec_nom_estado = (select ESTHIJ.nom_estado from Estados ESTHIJ where ESTHIJ.cod_estado=HIJ.cod_estado),  
		sec_cod_situacion=HIJ.cod_situacion,  
		sec_nom_situacion = (select SITHIJ.nom_situacion from Situaciones SITHIJ where SITHIJ.cod_situacion=HIJ.cod_situacion),  
		sec_horaspresup=HIJ.horaspresup,  
		sec_fe_ini_plan=HIJ.fe_ini_plan,  
		sec_fe_fin_plan=HIJ.fe_fin_plan,  
		sec_fe_ini_real=HIJ.fe_ini_real,  
		sec_fe_fin_real=HIJ.fe_fin_real,  
		sec_fe_ini_orig=HIJ.fe_ini_orig,  
		sec_fe_fin_orig=HIJ.fe_fin_orig,  
		sol_direccion = (select nom_direccion  from Direccion where cod_direccion=PET.cod_direccion),  
		sol_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=PET.cod_gerencia),  
		sol_sector = (select nom_sector from Sector where cod_sector=PET.cod_sector),  
		prio_ejecuc=0,  
		info_adicio='',  
		PET.prj_nrointerno,  
		prj_titulo = '',
		PET.cod_clase,  
		PET.pet_imptech,
		PET.pet_regulatorio,	
		PET.pet_projid,
		PET.pet_projsubid,
		PET.pet_projsubsid,
		projnom = (select PRJIDM.ProjNom from GesPet..ProyectoIDM PRJIDM where PRJIDM.ProjId = PET.pet_projid and PRJIDM.ProjSubId = PET.pet_projsubid and PRJIDM.ProjSubSId = PET.pet_projsubsid),
		PET.pet_emp,
		pet_ro = isnull(PET.pet_ro,'-'),
		pet_riesgo = (case
			when PET.pet_ro = 'A' then 'Alta'
			when PET.pet_ro = 'M' then 'Media'
			when PET.pet_ro = 'B' then 'Baja'
			else 'No'
		end),
		PET.cargaBeneficios,
		PET.pet_driver,
		PET.cod_BPE,
		nomBPE = (select r.nom_recurso from Recurso r where r.cod_recurso = PET.cod_BPE),
		PET.fecha_BPE,
		PET.valor_impacto,
		PET.valor_facilidad,
		ISNULL(PET.puntuacion,0)	as puntuacion,
		c.categoria,
		horas_sector = (
			select SUM(CONVERT(REAL, HRS.horas)/CONVERT(REAL,60)) as Horas
			from
				GesPet..HorasTrabajadas HRS inner join
				GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso INNER join
				GesPet..PeticionSector ps on HRS.pet_nrointerno = ps.pet_nrointerno and REC.cod_sector = ps.cod_sector
			where
				HRS.pet_nrointerno = PET.pet_nrointerno and
				(RTRIM(REC.cod_sector) = RTRIM(HIJ.cod_sector)))
	from    
		GesPet..Peticion PET,  
		#tablaCriterio c,
		GesPet..PeticionSector HIJ
	where   
		(@pet_emp is null or PET.pet_emp = @pet_emp) and
		(@pet_ro is null or @pet_ro = PET.pet_ro) and 
		(@pet_nrodesde = 0 or PET.pet_nroasignado >= @pet_nrodesde) and  
		(@pet_nrohasta = 0 or PET.pet_nroasignado <= @pet_nrohasta) and  
		(@agr_nrointerno=0 or PET.pet_nrointerno in (select AXP.pet_nrointerno from #AgrupXPetic AXP)) and  
		(@pet_estado is null or RTRIM(@pet_estado)='NULL' or charindex(RTRIM(PET.cod_estado),RTRIM(@pet_estado))>0) and 
		(@pet_tipo is null or RTRIM(@pet_tipo)='NULL' or RTRIM(PET.cod_tipo_peticion) = RTRIM(@pet_tipo)) and
		(@cod_clase is null or @cod_clase = 'NULL' or @cod_clase=PET.cod_clase) and
		(@pet_imptech is null or @pet_imptech=PET.pet_imptech) and
		(@pet_titulo is null or RTRIM(@pet_titulo)='NULL' or charindex(UPPER(RTRIM(@pet_titulo)),UPPER(RTRIM(PET.titulo)))>0) and  
		(@pet_prioridad is null or RTRIM(@pet_prioridad)='NULL'or RTRIM(PET.prioridad) = RTRIM(@pet_prioridad)) and  
		(@cod_importancia is null or RTRIM(@cod_importancia)='NULL' or RTRIM(PET.importancia_cod) = RTRIM(@cod_importancia)) and  
		(@cod_bpar is null or RTRIM(@cod_bpar)='NULL' or RTRIM(PET.cod_bpar) = RTRIM(@cod_bpar)) and  
		(RTRIM(@pet_desde_estado)='NULL' or convert(char(8),PET.fe_estado,112) >= @pet_desde_estado) and  
		(RTRIM(@pet_hasta_estado)='NULL' or convert(char(8),PET.fe_estado,112) <= @pet_hasta_estado) and  
		(RTRIM(@pet_desde_bpar)='NULL' or convert(char(8),PET.fe_comite,112) >= @pet_desde_bpar) and  
		(RTRIM(@pet_hasta_bpar)='NULL' or convert(char(8),PET.fe_comite,112) <= @pet_hasta_bpar) and  
		(@pet_situacion is null or RTRIM(@pet_situacion)='NULL' or RTRIM(PET.cod_situacion) = RTRIM(@pet_situacion)) and  
		((@pet_nivel is null or RTRIM(@pet_nivel)='NULL') or  
		(@pet_nivel='DIRE' and RTRIM(PET.cod_direccion)=RTRIM(@pet_area)) or  
		(@pet_nivel='GERE' and RTRIM(PET.cod_gerencia)=RTRIM(@pet_area)) or  
		(@pet_nivel='SECT' and RTRIM(PET.cod_sector)=RTRIM(@pet_area))) and  
		(RTRIM(@sec_desde_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) >= @sec_desde_iplan) and  
		(RTRIM(@sec_hasta_iplan)='NULL' or convert(char(8),HIJ.fe_ini_plan,112) <= @sec_hasta_iplan) and  
		(RTRIM(@sec_desde_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) >= @sec_desde_fplan) and  
		(RTRIM(@sec_hasta_fplan)='NULL' or convert(char(8),HIJ.fe_fin_plan,112) <= @sec_hasta_fplan) and  
		(RTRIM(@sec_desde_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) >= @sec_desde_ireal) and  
		(RTRIM(@sec_hasta_ireal)='NULL' or convert(char(8),HIJ.fe_ini_real,112) <= @sec_hasta_ireal) and  
		(RTRIM(@sec_desde_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) >= @sec_desde_freal) and  
		(RTRIM(@sec_hasta_freal)='NULL' or convert(char(8),HIJ.fe_fin_real,112) <= @sec_hasta_freal) and
		PET.pet_nrointerno = HIJ.pet_nrointerno and 
		PET.pet_nrointerno = c.pet_nrointerno and
		(RTRIM(@sec_dire)='NULL' or RTRIM(HIJ.cod_direccion)=RTRIM(@sec_dire)) and
		(RTRIM(@sec_gere)='NULL' or RTRIM(HIJ.cod_gerencia)=RTRIM(@sec_gere)) and
		(RTRIM(@sec_sect)='NULL' or RTRIM(HIJ.cod_sector)=RTRIM(@sec_sect)) and
		(@sec_estado is null or RTRIM(@sec_estado)='NULL' or charindex(RTRIM(HIJ.cod_estado),RTRIM(@sec_estado))>0) and
		(@sec_situacion is null or RTRIM(@sec_situacion)='NULL' or RTRIM(HIJ.cod_situacion) = RTRIM(@sec_situacion))
		and (PET.pet_regulatorio = @pet_regulatorio or @pet_regulatorio is null)
		and (
			(PET.pet_projid = @prjid or @prjid is null) and
			(PET.pet_projsubid = @prjsubid or @prjsubid is null) and
			(PET.pet_projsubsid = @prjsubsid or @prjsubsid is null)
		)
		and (@cod_GBPE is null or @cod_GBPE = 'NULL' or LTRIM(RTRIM(PET.cod_BPE)) = LTRIM(RTRIM(@cod_GBPE)))
		and (RTRIM(@fechaDesdeBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) >= @fechaDesdeBPE) 
		and (RTRIM(@fechaHastaBPE)='NULL' or convert(char(8),PET.fecha_BPE,112) <= @fechaHastaBPE)
		and (@categoria IS NULL OR c.categoria = @categoria_loc)
		and (RTRIM(@pet_desde_alta)='NULL' or convert(char(8),PET.fe_pedido,112) >= @pet_desde_alta)
		and (RTRIM(@pet_hasta_alta)='NULL' or convert(char(8),PET.fe_pedido,112) <= @pet_hasta_alta)
	order by 
		PET.pet_nroasignado,
		PET.pet_nrointerno
	return(0)
go

grant execute on dbo.sp_rptPeticion03 to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go

			/*
			Horas_Responsables = (
				select
					sum(cast(HRS.horas as float(1)))/60
				from
					GesPet..HorasTrabajadas HRS inner join
					GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso
				where
					HRS.pet_nrointerno = PET.pet_nrointerno and
					HRS.cod_recurso in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and
					HRS.pet_nrointerno in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo)
				),
			Horas_Recursos = (
				select
					sum(cast(HRS.horas as float(1)))/60
				from
					GesPet..HorasTrabajadas HRS inner join
					GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso
				where
					HRS.pet_nrointerno = PET.pet_nrointerno and
					HRS.cod_recurso not in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and
					HRS.pet_nrointerno in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo)
				),
			Horas_Responsables_NoAsignadas = (
				select
					sum(cast(HRS.horas as float(1)))/60
				from
					GesPet..HorasTrabajadas HRS inner join
					GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso
				where
					HRS.pet_nrointerno = PET.pet_nrointerno and
					HRS.cod_recurso in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and
					HRS.pet_nrointerno not in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo)
				),
			Horas_Recursos_NoAsignadas = (
				select
					sum(cast(HRS.horas as float(1)))/60
				from
					GesPet..HorasTrabajadas HRS inner join
					GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso
				where
					HRS.pet_nrointerno = PET.pet_nrointerno and
					HRS.cod_recurso not in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and
					HRS.pet_nrointerno not in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo)
				),
			*/