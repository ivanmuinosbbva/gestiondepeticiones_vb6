/*
-000- a. FJS 27.06.2008 - Nuevo SP para determinar los usuarios conectados a un momento al aplicativo. 
						  Aqu� se registra el logout al aplicativo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteRecursoApp'
go

if exists (select * from sysobjects where name = 'sp_DeleteRecursoApp' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteRecursoApp
go

create procedure dbo.sp_DeleteRecursoApp
	@cod_recurso		char(10)=null,
	@cod_equipo			char(20)=null,
	@fe_desde			smalldatetime=null,
	@fe_hasta			smalldatetime=null
as 
	delete from
		GesPet..Aucab 
	where
		(cod_recurso = @cod_recurso or @cod_recurso is null) and 
		(cod_equipo = @cod_equipo or @cod_equipo is null) and
		(fe_ingreso >= @fe_desde or @fe_desde is null) and 
		(fe_ingreso <= @fe_hasta or @fe_hasta is null)
go

grant execute on dbo.sp_DeleteRecursoApp to GesPetUsr
go

print 'Actualizaci�n realizada.'
