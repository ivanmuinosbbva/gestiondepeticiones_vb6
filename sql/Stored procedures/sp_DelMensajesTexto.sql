use GesPet
go
print 'sp_DelMensajesTexto'
go
if exists (select * from sysobjects where name = 'sp_DelMensajesTexto' and sysstat & 7 = 4)
drop procedure dbo.sp_DelMensajesTexto
go
create procedure dbo.sp_DelMensajesTexto 
             @cod_txtmsg int 
as 
   if  exists (select cod_txtmsg from GesPet..MensajesEvento 
        where cod_txtmsg = @cod_txtmsg)   
   begin   
     select 30012 as ErrCode ,'Este mensaje posee eventos asociados' as ErrDesc   
     raiserror 30012 'Este mensaje posee eventos asociados'   
     return (30012)   
   end   
 
   if  exists (select cod_txtmsg from GesPet..Mensajes 
        where cod_txtmsg = @cod_txtmsg)   
   begin   
     select 30013 as ErrCode ,'Existen mensajes generados con este c�digo' as ErrDesc   
     raiserror 30013 'Existen mensajes generados con este c�digo'   
     return (30013)   
   end   
     
    delete  GesPet..MensajesTexto 
    where   @cod_txtmsg=cod_txtmsg 
return(0) 
go



grant execute on dbo.sp_DelMensajesTexto to GesPetUsr 
go


