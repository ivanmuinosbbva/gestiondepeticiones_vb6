/*
-000- a. FJS 20.08.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ABM_Gerencia'
go

if exists (select * from sysobjects where name = 'sp_IGM_ABM_Gerencia' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ABM_Gerencia
go

create procedure dbo.sp_IGM_ABM_Gerencia
	@cod_direccion	char(8),
	@cod_gerencia	char(8),
	@hab			char(1)
as 
	if exists (select cod_gerencia from GesPet..Gerencia where cod_direccion=@cod_direccion and cod_gerencia=@cod_gerencia)
		begin
			update GesPet..Gerencia
			set IGM_hab = @hab
			where 
				cod_direccion = @cod_direccion and
				cod_gerencia  = @cod_gerencia
			return(0)
		end
	else
		begin
			select 30010 as ErrCode, 'Error: No existe el c�digo de gerencia especificado.' as ErrDesc 
			raiserror 30010 'Error: No existe el c�digo de gerencia especificado.'
			return (30010) 
		end
go

grant execute on dbo.sp_IGM_ABM_Gerencia to GesPetUsr
go

grant execute on dbo.sp_IGM_ABM_Gerencia to GrpTrnIGM
go

print 'Actualizaci�n realizada.'
go