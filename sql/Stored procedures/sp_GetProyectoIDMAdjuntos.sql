use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMAdjuntos'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMAdjuntos' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMAdjuntos
go

create procedure dbo.sp_GetProyectoIDMAdjuntos
	@ProjId			int=null, 
	@ProjSubId		int=null, 
	@ProjSubsId		int=null,
	@adj_tipo		char(8)=null,
	@adj_file		char(100)=null
as 
	select
		p.ProjId,
		p.ProjSubId,
		p.ProjSubSId,
		p.adj_tipo,
		p.adj_file,
		p.adj_texto,
		p.adj_objeto,
		p.audit_user,
		nom_user = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.audit_user),
		p.audit_date,
		p.cod_nivel,
		p.cod_area,
		nom_area = (
			case
				when p.cod_nivel = 'DIRE' then 
					(select d.nom_direccion 
					from GesPet..Direccion d
					where d.cod_direccion = p.cod_area)
				when p.cod_nivel = 'GERE' then 
					(select d.nom_direccion + ' » ' + g.nom_gerencia 
					from 
						GesPet..Direccion d inner join
						GesPet..Gerencia g on (d.cod_direccion = g.cod_direccion)
					where g.cod_gerencia = p.cod_area)
				when p.cod_nivel = 'SECT' then
					(select d.nom_direccion + ' » ' + g.nom_gerencia + ' » ' + s.nom_sector
					from 
						GesPet..Direccion d inner join
						GesPet..Gerencia g on (d.cod_direccion = g.cod_direccion) inner join
						GesPet..Sector s on (g.cod_gerencia = s.cod_gerencia)
					where s.cod_sector = p.cod_area)
				when p.cod_nivel = 'GRUP' then
					(select d.nom_direccion + ' » ' + g.nom_gerencia + ' » ' + s.nom_sector + ' » ' + gr.nom_grupo
					from 
						GesPet..Direccion d inner join
						GesPet..Gerencia g on (d.cod_direccion = g.cod_direccion) inner join
						GesPet..Sector s on (g.cod_gerencia = s.cod_gerencia) inner join
						GesPet..Grupo gr on (s.cod_sector = gr.cod_sector)
					where gr.cod_grupo = p.cod_area)
				else 'Todo el BBVA'
			end)
	from 
		GesPet..ProyectoIDMAdjuntos p 
	where
		(@ProjId is null or p.ProjId = @ProjId) and 
		(@ProjSubId is null or p.ProjSubId = @ProjSubId) and 
		(@ProjSubsId is null or p.ProjSubSId = @ProjSubsId) and
		(@adj_tipo is null or p.adj_tipo = @adj_tipo) and 
		(@adj_file is null or p.adj_file = @adj_file)
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMAdjuntos to GesPetUsr
go

print 'Actualización realizada.'
go
