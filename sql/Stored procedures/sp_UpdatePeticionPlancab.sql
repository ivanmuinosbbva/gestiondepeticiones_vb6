/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlancab'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlancab' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlancab
go

create procedure dbo.sp_UpdatePeticionPlancab
	@per_nrointerno	int=null,
	@pet_nrointerno	int=null,
	@prioridad		int=null,
	@cod_bpe		char(10)=null
as
	update 
		GesPet.dbo.PeticionPlancab
	set
		per_nrointerno	= @per_nrointerno,
		pet_nrointerno	= @pet_nrointerno,
		prioridad		= @prioridad,
		cod_bpe			= @cod_bpe
	where 
		per_nrointerno = @per_nrointerno and 
		pet_nrointerno = @pet_nrointerno
go

grant execute on dbo.sp_UpdatePeticionPlancab to GesPetUsr
go

print 'Actualización realizada.'
go

