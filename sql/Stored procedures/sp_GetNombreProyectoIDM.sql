/*
-000- a. XXX dd.mm.aaaa - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetNombreProyectoIDM'
go

if exists (select * from sysobjects where name = 'sp_GetNombreProyectoIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetNombreProyectoIDM
go

create procedure dbo.sp_GetNombreProyectoIDM
	@PrjId		int=null, 
	@PrjSubId	int=null, 
	@PrjSubSId	int=null 
as 
	declare @Prj_Nivel1	varchar(100)
	declare @Prj_Nivel2	varchar(100)
	declare @Prj_Nivel3	varchar(100)

	create table #Niveles 
	( 
		nivel		int				null, 
		nom_nivel	varchar(100)	null 
	) 

	select @Prj_Nivel1 = (select ProjNom from GesPet..ProyectoIDM where ProjId = @PrjId and ProjSubId = 0 and ProjSubSId = 0 group by ProjNom)
	select @Prj_Nivel2 = (select ProjNom from GesPet..ProyectoIDM where ProjId = @PrjId and ProjSubId = @PrjSubId and ProjSubSId = 0 group by ProjNom)
	select @Prj_Nivel3 = (select ProjNom from GesPet..ProyectoIDM where ProjId = @PrjId and ProjSubId = @PrjSubId and ProjSubSId = @PrjSubSId group by ProjNom)

	insert into #Niveles (nivel, nom_nivel) values (1, @Prj_Nivel1)
	insert into #Niveles (nivel, nom_nivel) values (2, @Prj_Nivel2)
	insert into #Niveles (nivel, nom_nivel) values (3, @Prj_Nivel3)

	select
		nivel,
		nom_nivel
	from
		#Niveles
go

grant execute on dbo.sp_GetNombreProyectoIDM to GesPetUsr
go

print 'Actualización realizada.'