/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteInfogru'
go

if exists (select * from sysobjects where name = 'sp_DeleteInfogru' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteInfogru
go

create procedure dbo.sp_DeleteInfogru
	@info_gruid	int=0
as
	delete from
		GesPet.dbo.Infogru
	where
		(info_gruid = @info_gruid or @info_gruid is null)
go

grant execute on dbo.sp_DeleteInfogru to GesPetUsr
go

print 'Actualización realizada.'
go
