use GesPet
go
print 'sp_GetProyectoMemo'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoMemo' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoAvanceMemo
go
create procedure dbo.sp_GetProyectoMemo 
	@prj_nrointerno   int,   
	@mem_campo      char(6)   
   
as   
    select mem_texto   
    from GesPet..ProyectoMem   
    where prj_nrointerno = @prj_nrointerno and   
		RTRIM(mem_campo) = RTRIM(@mem_campo) 
    order by mem_secuencia   
return(0)   
go



grant execute on dbo.sp_GetProyectoMemo to GesPetUsr 
go


