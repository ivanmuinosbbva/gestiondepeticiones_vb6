/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMAlertas'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMAlertas' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMAlertas
go

create procedure dbo.sp_DeleteProyectoIDMAlertas
	@ProjId		int,
	@ProjSubId	int,
	@ProjSubSId	int,
	@alert_item	int
	--@alert_tipo	char(1)
as
	delete from
		GesPet.dbo.ProyectoIDMAlertas
	where
		ProjId		= @ProjId and
		ProjSubId   = @ProjSubId and
		ProjSubSId  = @ProjSubSId and
		alert_itm	= @alert_item
		--alert_tipo = @alert_tipo
	return(0)
go

grant execute on dbo.sp_DeleteProyectoIDMAlertas to GesPetUsr
go

print 'Actualización realizada.'
go
