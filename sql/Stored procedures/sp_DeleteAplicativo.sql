/*
-000- a. FJS 12.05.2009 - Nuevo SP para manejo de Aplicativos
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteAplicativo'
go

if exists (select * from sysobjects where name = 'sp_DeleteAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteAplicativo
go

create procedure dbo.sp_DeleteAplicativo
	@app_id				char(30)
as
	delete from
		GesPet..Aplicativo
	where
		app_id = @app_id
go

grant execute on dbo.sp_DeleteAplicativo to GesPetUsr
go

print 'Actualización realizada.'
