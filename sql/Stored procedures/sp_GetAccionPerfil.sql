use GesPet
go
print 'sp_GetAccionPerfil'
go
if exists (select * from sysobjects where name = 'sp_GetAccionPerfil' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAccionPerfil
go
create procedure dbo.sp_GetAccionPerfil
	@cod_accion	char(8)=null,
	@cod_perfil	char(4)=null,
	@cod_estado	char(6)=null
as
	begin
		select
			Ap.cod_accion,
			Ap.cod_perfil,
			nom_perfil = (select x.nom_perfil from GesPet..Perfil x where x.cod_perfil = Ap.cod_perfil),
			Ap.cod_estado,
			cod_estnew=Ew.cod_estado,
			nom_accion = (select Ac.nom_accion from Acciones Ac where RTRIM(Ac.cod_accion) = RTRIM(Ap.cod_accion)),
			Ap.flg_hereda,
			Es.nom_estado,
			nom_estnew=Ew.nom_estado,
			Ew.imagen
		from
			GesPet..AccionesPerfil Ap,
			GesPet..Estados Es,
			GesPet..Estados Ew
		where
			(@cod_accion is null or RTRIM(@cod_accion) is null or RTRIM(@cod_accion)='' or RTRIM(Ap.cod_accion) = RTRIM(@cod_accion) or RTRIM(Ap.cod_accion)='ALL') and
			(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or RTRIM(Ap.cod_estado) = RTRIM(@cod_estado)) and
			(@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(Ap.cod_perfil) = RTRIM(@cod_perfil)) and
			(RTRIM(Ap.cod_estado)*=RTRIM(Es.cod_estado)) and
			(RTRIM(Ap.cod_estnew)*=RTRIM(Ew.cod_estado))
		order by 
			Ap.cod_accion,
			Ap.cod_perfil,
			Ew.terminal
			--Es.nom_estado
	end
	return(0)
go

grant execute on dbo.sp_GetAccionPerfil to GesPetUsr
go


