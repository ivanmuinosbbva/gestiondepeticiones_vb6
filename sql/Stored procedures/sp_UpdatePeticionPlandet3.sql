/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlandet3'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlandet3' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlandet3
go

create procedure dbo.sp_UpdatePeticionPlandet3
	@per_nrointerno		int,
	@pet_nrointerno		int,
	@cod_grupo			char(8),
	@plan_motivrepl		int,
	@plan_feplanori		smalldatetime,
	@plan_feplannue		smalldatetime,
	@plan_horasreplan	int
as
	declare @hoy		smalldatetime
	declare @ultima		smalldatetime
	declare @cant		int

	select @cant = 0
	select @hoy  = getdate()

	select 
		@ultima = x.plan_fchreplan,
		@cant   = isnull(x.plan_cantreplan, 0)
	from 
		GesPet..PeticionPlandet x
	where
		x.per_nrointerno = @per_nrointerno and
		x.pet_nrointerno = @pet_nrointerno and
		x.cod_grupo		 = @cod_grupo

	if @ultima is null
		begin
			select @cant = 1
		end
	else
		if convert(char(8),@ultima,112) <> convert(char(8),@hoy,112)
			begin
				select @cant = @cant + 1
			end

	update 
		GesPet..PeticionPlandet
	set
		plan_motivrepl	 = @plan_motivrepl,
		plan_feplanori	 = @plan_feplanori,
		plan_feplannue	 = @plan_feplannue,
		--plan_cantreplan	 = plan_cantreplan + 1,
		plan_cantreplan  = @cant,
		plan_horasreplan = @plan_horasreplan,
		plan_fchreplan   = getdate()
	where 
		per_nrointerno = @per_nrointerno and
		pet_nrointerno = @pet_nrointerno and
		cod_grupo = @cod_grupo
	return(0)
go

grant execute on dbo.sp_UpdatePeticionPlandet3 to GesPetUsr
go

print 'Actualización realizada.'
