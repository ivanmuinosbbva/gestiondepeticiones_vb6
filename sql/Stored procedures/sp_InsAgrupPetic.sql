/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_InsAgrupPetic'
go
if exists (select * from sysobjects where name = 'sp_InsAgrupPetic' and sysstat & 7 = 4)
drop procedure dbo.sp_InsAgrupPetic
go
create procedure dbo.sp_InsAgrupPetic 
             @agr_nrointerno   int=0,
             @pet_nrointerno   int=0
as 
 
	if not exists (select agr_nrointerno from Agrup where agr_nrointerno = @agr_nrointerno)   
	begin 
		select 30010 as ErrCode , 'Error: No existe Agrupamiento' as ErrDesc 
		raiserror  30010 'Error: No existe Agrupamiento'  
		return (30010) 
	end 
	if not exists (select pet_nrointerno from Peticion where pet_nrointerno = @pet_nrointerno)   
	begin 
		select 30010 as ErrCode , 'Error: No existe Peticion' as ErrDesc 
		raiserror  30010 'Error: No existe Peticion'  
		return (30010) 
	end 
 
	if not exists (select agr_nrointerno from AgrupPetic where  agr_nrointerno=@agr_nrointerno and pet_nrointerno=@pet_nrointerno)
	begin 
		insert into AgrupPetic 
			(agr_nrointerno,
			pet_nrointerno,
			audit_user,
			audit_date)
		values   
			(@agr_nrointerno,
			@pet_nrointerno,  
			SuseR_NAME(),   
			getdate()   )
	end 

return(0)   
go



grant execute on dbo.sp_InsAgrupPetic to GesPetUsr 
go


