print 'Creando/actualizando SP: sp_InsertMotivo'
go

if exists (select * from sysobjects where name = 'sp_InsertMotivo' and sysstat & 7 = 4)
    drop procedure dbo.sp_InsertMotivo
go

create procedure dbo.sp_InsertMotivo
    @cod_motivo     int=null,
    @nom_motivo     char(120)=null,
	@hab_motivo		char(1)=''
as
    insert into GesPet.dbo.Motivos
        (cod_motivo,
         nom_motivo,
		 hab_motivo)
    values
        (@cod_motivo,
         @nom_motivo,
		 @hab_motivo)

return(0)
go

grant execute on dbo.sp_InsertMotivo to GesPetUsr
go

print 'Actualización realizada.'