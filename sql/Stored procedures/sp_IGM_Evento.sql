/*
-000- a. FJS 20.08.2010 - Nuevo SP para devolver los eventos a utilizar en el historial por IGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_Evento'
go

if exists (select * from sysobjects where name = 'sp_IGM_Evento' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_Evento
go

create procedure dbo.sp_IGM_Evento
	@cod_evento		char(8)=null
as 
	select
		a.cod_accion as 'cod_evento',
		a.nom_accion as 'nom_evento',
		a.IGM_hab
	from
		GesPet..Acciones a
	where
		(@cod_evento is null or a.cod_accion = @cod_evento)
	return(0)
go

grant execute on dbo.sp_IGM_Evento to GesPetUsr
go

grant execute on dbo.sp_IGM_Evento to GrpTrnIGM
go

print 'Actualización realizada.'
go 