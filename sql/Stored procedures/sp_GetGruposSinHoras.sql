use GesPet
go

/* sp_GetGruposSinHoras 'DESA','20050101' */

print 'sp_GetGruposSinHoras'
go
if exists (select * from sysobjects where name = 'sp_GetGruposSinHoras' and sysstat & 7 = 4)
drop procedure dbo.sp_GetGruposSinHoras
go
create procedure dbo.sp_GetGruposSinHoras
	@cod_gerencia  varchar(8),
	@fe_fin_real char(8)
as

select 	PET.pet_nroasignado,
	PET.cod_tipo_peticion,
	PET.titulo,
	PET.prioridad,
	GRU.cod_grupo,
	nom_grupo = (select NOMH.nom_grupo from Grupo NOMH where RTRIM(NOMH.cod_grupo)=RTRIM(GRU.cod_grupo)),
	GRU.cod_estado,
	nom_estado = (select ESTHIJ.nom_estado from Estados ESTHIJ where RTRIM(ESTHIJ.cod_estado)=RTRIM(GRU.cod_estado)),
	fe_ini_real = isnull(convert(varchar(10),GRU.fe_ini_real,103),''),
	fe_fin_real = isnull(convert(varchar(10),GRU.fe_fin_real,103),''),
	PET.pet_nrointerno
from
	GesPet..PeticionGrupo GRU,
	GesPet..Peticion PET
where RTRIM(GRU.cod_gerencia)=RTRIM(@cod_gerencia) and
	((RTRIM(GRU.cod_estado)='EJECUC' or (RTRIM(GRU.cod_estado)='TERMIN' and convert(char(8),GRU.fe_fin_real,112)>=@fe_fin_real)) and not exists (select HT.pet_nrointerno from HorasTrabajadas HT where HT.pet_nrointerno=PET.pet_nrointerno)) and
	PET.pet_nrointerno = GRU.pet_nrointerno
	order by PET.pet_nroasignado
return(0)
go

grant execute on dbo.sp_GetGruposSinHoras to GesPetUsr
go

