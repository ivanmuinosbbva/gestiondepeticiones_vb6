/*
-000- a. FJS 18.03.2008 - Nuevo SP para obtener todos los datos a nivel de grupo del grupo Homologador de una petición.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetGrupoHomologacion'
go

if exists (select * from sysobjects where name = 'sp_GetPetGrupoHomologacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetGrupoHomologacion
go

create procedure dbo.sp_GetPetGrupoHomologacion
    @pet_nrointerno		int=null,
	@tipo				char(1)=null
as 
    select 
        pg.pet_nrointerno,
        pg.cod_grupo,
        pg.cod_sector,
        pg.cod_gerencia,
        pg.cod_direccion,
        pg.fe_ini_plan,
        pg.fe_fin_plan,
        pg.fe_ini_real,
        pg.fe_fin_real,
        pg.horaspresup,
        pg.cod_estado,
        pg.fe_estado,
        pg.cod_situacion,
        pg.ult_accion,
        pg.hst_nrointerno_sol,
        pg.hst_nrointerno_rsp,
        pg.audit_user,
        pg.audit_date,
        pg.fe_fec_plan,
        pg.fe_ini_orig,
        pg.fe_fin_orig,
        pg.fe_fec_orig,
        pg.cant_planif,
        pg.prio_ejecuc,
        pg.info_adicio,
		ps.cod_estado	as sector_cod_estado
    from
        PeticionGrupo pg inner join 
		PeticionSector ps on (pg.pet_nrointerno = ps.pet_nrointerno and pg.cod_sector = ps.cod_sector) inner join
		Grupo g on (pg.cod_sector = ps.cod_sector and pg.cod_grupo = g.cod_grupo)
    where
        pg.pet_nrointerno = @pet_nrointerno and 
        (@tipo is null or g.grupo_homologacion = @tipo)

return(0) 
go

grant execute on dbo.sp_GetPetGrupoHomologacion to GesPetUsr
go

print 'Actualización realizada.'
go