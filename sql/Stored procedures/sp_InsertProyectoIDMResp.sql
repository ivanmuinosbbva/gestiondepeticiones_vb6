/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.05.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoIDMResp'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoIDMResp' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoIDMResp
go

create procedure dbo.sp_InsertProyectoIDMResp
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@cod_nivel		char(4),
	@cod_area		char(8),
	@propietaria	char(1)
as
	insert into GesPet.dbo.ProyectoIDMResponsables (
		ProjId,
		ProjSubId,
		ProjSubSId,
		cod_nivel,
		cod_area,
		propietaria,
		resp_user,
		resp_fecha)
	values (
		@ProjId,
		@ProjSubId,
		@ProjSubSId,
		@cod_nivel,
		@cod_area,
		@propietaria,
		SUSER_NAME(),
		getdate())
go

grant execute on dbo.sp_InsertProyectoIDMResp to GesPetUsr
go

print 'Actualización realizada.'
go
