/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 18.08.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeriodoField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeriodoField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeriodoField
go

create procedure dbo.sp_UpdatePeriodoField
	@per_nrointerno	int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as
	if rtrim(@campo)='PER_NROINTERNO'
		update GesPet.dbo.Periodo set per_nrointerno = @valornum where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null)

	if rtrim(@campo)='PER_NOMBRE'
		update GesPet.dbo.Periodo set per_nombre = @valortxt where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null)

	if rtrim(@campo)='PER_TIPO'
		update GesPet.dbo.Periodo set per_tipo = @valortxt where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null)

	if rtrim(@campo)='FE_DESDE'
		update GesPet.dbo.Periodo set fe_desde = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null)

	if rtrim(@campo)='FE_HASTA'
		update GesPet.dbo.Periodo set fe_hasta = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null)

	if rtrim(@campo)='PER_ESTADO'
		update GesPet.dbo.Periodo 
		set per_estado = @valortxt,
			per_ultfchest = getdate(),
			per_ultusrid  = SUSER_NAME()
		where (per_nrointerno = @per_nrointerno or @per_nrointerno is null)

	if rtrim(@campo)='PER_ULTFCHEST'
		update GesPet.dbo.Periodo set per_ultfchest = @valordate where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null)

	if rtrim(@campo)='PER_ULTUSRID'
		update GesPet.dbo.Periodo set per_ultusrid = @valortxt where 
			(per_nrointerno = @per_nrointerno or @per_nrointerno is null)
	
	return(0)
go

grant execute on dbo.sp_UpdatePeriodoField to GesPetUsr
go

print 'Actualización realizada.'
