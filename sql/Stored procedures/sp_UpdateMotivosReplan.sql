/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateMotivosReplan'
go

if exists (select * from sysobjects where name = 'sp_UpdateMotivosReplan' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateMotivosReplan
go

create procedure dbo.sp_UpdateMotivosReplan
	@cod_motivo_replan	int,
	@nom_motivo_replan	char(50),
	@estado_hab			char(1)
as
	update 
		GesPet.dbo.MotivosReplan
	set
		nom_motivo_replan = @nom_motivo_replan,
		estado_hab		  = @estado_hab
	where 
		(cod_motivo_replan = @cod_motivo_replan or @cod_motivo_replan is null)
	return(0)
go

grant execute on dbo.sp_UpdateMotivosReplan to GesPetUsr
go

print 'Actualización realizada.'
