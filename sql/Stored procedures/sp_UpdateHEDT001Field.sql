/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 29.10.2009 - 
-001- a. FJS 11.10.2009 - Cuando se modifica el estado, se propaga el mismo cambio a las dependientes (HEDT002 y HEDT003).
-002- a. FJS 07.01.2011 - Se agrega un nuevo campo
-003- a. FJS 30.08.2012 - Pet. N� 51089
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT001Field'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT001Field' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT001Field
go

create procedure dbo.sp_UpdateHEDT001Field
	@dsn_id			char(8),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as
	if rtrim(@campo)='DSN_ID'
		update GesPet.dbo.HEDT001
		set	dsn_id = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_NOM'
		update GesPet.dbo.HEDT001
		set	dsn_nom = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_DESC'
		update GesPet.dbo.HEDT001
		set	dsn_desc = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_ENMAS'
		update GesPet.dbo.HEDT001
		set	dsn_enmas = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_NOMRUT'
		update GesPet.dbo.HEDT001
		set	dsn_nomrut = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_FEULT'
		update GesPet.dbo.HEDT001
		set	dsn_feult = @valordate
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_USERID'
		update GesPet.dbo.HEDT001
		set	dsn_userid = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_VB'
		update GesPet.dbo.HEDT001
		set	dsn_vb = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_VB_FE'
		update GesPet.dbo.HEDT001
		set	dsn_vb_fe = @valordate
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='DSN_VB_USERID'
		update GesPet.dbo.HEDT001
		set	dsn_vb_userid = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='APP_ID'
		update GesPet.dbo.HEDT001
		set	app_id = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if rtrim(@campo)='ESTADO'
		begin
			update GesPet.dbo.HEDT001
			set	estado = @valortxt
			where (dsn_id = @dsn_id or @dsn_id is null)
			--{ add -001- a.
			update GesPet.dbo.HEDT002
			set	estado = @valortxt
			where (dsn_id = @dsn_id or @dsn_id is null)

			update GesPet.dbo.HEDT003
			set	estado = @valortxt
			where (dsn_id = @dsn_id or @dsn_id is null)
			--}
		end

	if rtrim(@campo)='FE_HST_ESTADO'
		update GesPet.dbo.HEDT001
		set	fe_hst_estado = @valordate
		where (dsn_id = @dsn_id or @dsn_id is null)

	--{ add -002- a.
	if rtrim(@campo)='DSN_NOMPROD'
		update GesPet.dbo.HEDT001
		set	dsn_nomprod = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)
	--}
	--{ add -003- a.
	if lower(rtrim(@campo))='dsn_cpy'					-- Indica si tiene COPY
		update GesPet.dbo.HEDT001
		set	dsn_cpy = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if lower(rtrim(@campo))='dsn_cpybbl'				-- Biblioteca de copy del archivo productivo
		update GesPet.dbo.HEDT001
		set	dsn_cpybbl = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if lower(rtrim(@campo))='dsn_cpynom'				-- Nombre de copy del archivo productivo
		update GesPet.dbo.HEDT001
		set	dsn_cpynom = @valortxt
		where (dsn_id = @dsn_id or @dsn_id is null)

	if lower(rtrim(@campo))='dsn_cantuso'				-- Cantidad de veces utilizado en las solicitudes
		update GesPet.dbo.HEDT001
		set	dsn_cantuso = @valornum
		where (dsn_id = @dsn_id or @dsn_id is null)
	
	if lower(rtrim(@campo))='suma_dsn_cantuso'			-- Incrementa en 1 el valor del campo
		-- Actualiza la cantidad en m�s o en menos
		update GesPet.dbo.HEDT001
		set	dsn_cantuso = dsn_cantuso + @valornum
		where (dsn_id = @dsn_id or @dsn_id is null)
	--}
	return(0)
go

grant execute on dbo.sp_UpdateHEDT001Field to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
