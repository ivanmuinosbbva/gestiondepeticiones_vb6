/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Objetivo:		Actualizar la estructura jerarquica en aquellas relaciones que corresponda cuando un Grupo cambia de Sector.

Descripci�n:	1. Se cargan los datos estructurales del grupo c�mo estaban ANTES de hacer el cambio.
				2. Se cargan los datos estructurales del grupo c�mo quedar�n LUEGO de hacer el cambio.
				3. Cargo en un cursor todos los recursos que estan dependiendo directamente del grupo a modificar.
				4. Actualizo los c�digos correspondientes de cada recurso seg�n los perfiles que tenga asignado en RecursoPerfil.
				5. Actualizo los c�digos correspondientes de cada recurso en la tabla Recurso.
				6. Actualizo los c�digos correspondientes de cada recurso en la tabla Agrupamientos.
				7. Actualizo los c�digos correspondientes de cada recurso involucrado en peticiones.
				8. Actualizo los c�digos correspondientes de los documentos adjuntos.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

use GesPet
go

print 'Creando/actualizando SP: sp_ChangeGrupoSector'
go

if exists (select * from sysobjects where name = 'sp_ChangeGrupoSector' and sysstat & 7 = 4)
	drop procedure dbo.sp_ChangeGrupoSector
go

create procedure dbo.sp_ChangeGrupoSector
	@cod_grupo	char(8),
	@new_sector	char(8)
as
	declare @new_direccion	char(8)
	declare @new_gerencia	char(8)
	declare @cod_recurso	char(10)
	declare @old_sector		char(8)
	declare @old_gerencia	char(8)
	declare @old_direccion	char(8)
	declare @pet_nrointerno int
	
	-- Verifica que el nuevo sector asignado al grupo exista
	if not exists (select Se.cod_sector from GesPet..Sector Se, GesPet..Gerencia Ge
					where RTRIM(Se.cod_sector) = RTRIM(@new_sector) and
					RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia))
		begin
			raiserror  30010 'Sector inexistente'
			select 30010 as ErrCode , 'Sector inexistente'  as ErrDesc
			return (30010)
		end

	-- 1. Cargo los datos estructurales jer�rquicos del grupo a reestructurar (sector, gerencia y direcci�n actual a la que pertenece)
	select  
		@old_direccion = Ge.cod_direccion,
		@old_gerencia  = Se.cod_gerencia,
		@old_sector    = Se.cod_sector
	from 
		Grupo Gr, Sector Se, Gerencia Ge
	where 
		RTRIM(Gr.cod_grupo) = RTRIM(@cod_grupo) and
		RTRIM(Se.cod_sector) = RTRIM(Gr.cod_sector) and
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia)

	-- 2. Cargo los datos estructurales jer�rquicos del sector que reemplaza al anterior (gerencia y direcci�n actual a la que pertenece)
	select  
		@new_direccion = Ge.cod_direccion,
		@new_gerencia  = Se.cod_gerencia
	from 
		Sector Se ,Gerencia Ge
	where 
		RTRIM(Se.cod_sector) = RTRIM(@new_sector) and
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia)

	-- 3. Cargo en un cursor todos los recursos pertenecientes al grupo que quiero reestructurar
	declare CursRecursos Cursor
		for 
			select cod_recurso
			from Recurso
			where RTRIM(cod_grupo) = RTRIM(@cod_grupo)
		for Read Only

		begin transaction
			-- RECURSOPERFIL
			/* Actualizo la nueva estructura jer�rquica para cada uno de los perfiles de cada uno de los usuarios del grupo a reestructurar */
			open CursRecursos
			fetch CursRecursos into @cod_recurso
			if @@sqlstatus = 0
				begin
					While @@sqlstatus = 0
						begin
							-- Reemplazo el sector anterior con el nuevo c�digo de sector para los perfiles que tuviera el recurso
							update 
								RecursoPerfil
							set 
								cod_area = @new_sector
							where 
								RTRIM(cod_recurso)	= @cod_recurso and
								RTRIM(cod_nivel)	= 'SECT' and
								RTRIM(cod_area)		= @old_sector
							-- Reemplazo la gerencia anterior con el nuevo c�digo de gerencia del sector nuevo
							update 
								RecursoPerfil
							set 
								cod_area = @new_gerencia
							where 
								RTRIM(cod_recurso) = @cod_recurso and
								RTRIM(cod_nivel) = 'GERE' and
								RTRIM(cod_area) = @old_gerencia
							-- Reemplazo la direcci�n anterior con el nuevo c�digo de direcci�n del sector nuevo
							update 
								RecursoPerfil
							set 
								cod_area = @new_direccion
							where 
								RTRIM(cod_recurso) = @cod_recurso and
								RTRIM(cod_nivel) = 'DIRE' and
								RTRIM(cod_area) = @old_direccion

							fetch CursRecursos into @cod_recurso
						end
				end
			close CursRecursos
			-- RECURSO
			/* Actualizo la estructura jer�rquica de los recursos del grupo a reestructurar */
			update 
				GesPet..Recurso
			set	
				cod_direccion = @new_direccion,
				cod_gerencia  = @new_gerencia,
				cod_sector    = @new_sector
			where 
				RTRIM(cod_grupo) = RTRIM(@cod_grupo)
			
			-- AGRUPAMIENTOS del recurso
			/* Actualizo la estructura jer�rquica de los recursos del grupo a reestructurar */
			update 
				GesPet..Agrup
			set	
				cod_direccion = @new_direccion,
				cod_gerencia  = @new_gerencia,
				cod_sector    = @new_sector
			where 
				RTRIM(cod_grupo) = RTRIM(@cod_grupo)
			
			-- PETICIONRECURSO
			/* Actualizo la estructura jer�rquica en las peticiones en las que se ve involucrado el grupo a reestructurar */
			update 
				GesPet..PeticionRecurso
			set	
				cod_direccion = @new_direccion,
				cod_gerencia  = @new_gerencia,
				cod_sector    = @new_sector
			where 
				RTRIM(cod_grupo) = RTRIM(@cod_grupo)

			-- PETICIONADJUNTO
			/* Actualizo la estructura jer�rquica en los documentos adjuntos a peticiones en las que se ve involucrado el grupo a reestructurar */
			update 
				GesPet..PeticionAdjunto
			set
				cod_sector = @new_sector,
				cod_grupo  = @cod_grupo
			where
				cod_sector = @old_sector and 
				cod_grupo  = @cod_grupo

		commit transaction
	deallocate cursor CursRecursos
return(0)
go

grant execute on dbo.sp_ChangeGrupoSector to GesPetUsr
go

print 'Actualizaci�n realizada.'
