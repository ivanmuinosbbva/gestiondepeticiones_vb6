/*
-000- a. XXX dd.mm.aaaa - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ParticipantesRecursos'
go

if exists (select * from sysobjects where name = 'sp_IGM_ParticipantesRecursos' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ParticipantesRecursos
go

create procedure dbo.sp_IGM_ParticipantesRecursos
	@pet_nrointerno	int=null
as 
	select
		r.cod_direccion,
		nom_direccion = isnull((select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = r.cod_direccion),''),
		r.cod_gerencia,
		nom_gerencia = isnull((select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = r.cod_gerencia),''),
		r.cod_sector,
		nom_sector = isnull((select x.nom_sector from GesPet..Sector x where x.cod_sector = r.cod_sector),''),
		r.cod_grupo,
		nom_grupo = isnull((select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = r.cod_grupo),''),
		h.cod_recurso,
		r.nom_recurso
	from
		GesPet..HorasTrabajadas h inner join
		GesPet..Recurso r on (h.cod_recurso = r.cod_recurso) inner join
		GesPet..Peticion p on (h.pet_nrointerno = p.pet_nrointerno) inner join
		GesPet..Gerencia g on (r.cod_gerencia = g.cod_gerencia)
	where
		(r.cod_direccion = 'MEDIO') and (g.IGM_hab = 'S') and 
		(@pet_nrointerno is null or p.pet_nrointerno = @pet_nrointerno)
	group by
		r.cod_direccion,
		r.cod_gerencia,
		r.cod_sector,
		r.cod_grupo,
		h.cod_recurso,
		r.nom_recurso
	order by
		r.nom_recurso

	return(0)
go

grant execute on dbo.sp_IGM_ParticipantesRecursos to GesPetUsr
go

grant execute on dbo.sp_IGM_ParticipantesRecursos to GrpTrnIGM
go

print 'Actualización realizada.'
go