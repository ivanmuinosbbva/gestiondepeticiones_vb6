use GesPet
go

print 'sp_GetPeticionChangeMan'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionChangeMan' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionChangeMan
go

create procedure dbo.sp_GetPeticionChangeMan 
    (@pet_nrointerno    int,    
     @pet_sector        char(8),    
     @pet_grupo         char(8))   
as     
   
select  
    pet_nrointerno, 
    pet_sector, 
    pet_grupo, 
    pet_date, 
    pet_record,
    datediff(day, pet_date, getdate()) as Diferencia
from  
    GesPet..PeticionChangeMan 
where 
	(pet_nrointerno = @pet_nrointerno OR @pet_nrointerno IS NULL) and  
    (pet_sector = @pet_sector OR @pet_sector IS NULL) and  
    (pet_grupo = @pet_grupo OR @pet_grupo IS NULL) 
 
return(0) 
 
go

grant Execute  on dbo.sp_GetPeticionChangeMan to GesPetUsr 
go
