/*
-000- a. FJS 05.05.2009 - Proyectos IDM Adjuntos
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMAdjuntos'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMAdjuntos' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMAdjuntos
go

create procedure dbo.sp_DeleteProyectoIDMAdjuntos
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@adj_tipo		char(8),
	@adj_file		char(100)
as
	delete from
		GesPet..ProyectoIDMAdjuntos
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		adj_tipo = @adj_tipo and
		adj_file = @adj_file
	return(0)
go

grant execute on dbo.sp_DeleteProyectoIDMAdjuntos to GesPetUsr
go

print 'Actualización realizada.'
go
