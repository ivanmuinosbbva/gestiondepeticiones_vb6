use GesPet
go
/* sp_UpdateProyectoAvanceDet 3,'200704','ADEFINIR','00010030','S' */
print 'sp_UpdateProyectoAvanceDet'
go
if exists (select * from sysobjects where name = 'sp_UpdateProyectoAvanceDet' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateProyectoAvanceDet
go
create procedure dbo.sp_UpdateProyectoAvanceDet
	@prj_nrointerno		int,
	@pav_aamm		char(6),
	@cod_AvanceGrupo	char(8),
	@cod_AvanceCoef		char(8),
	@pav_completado		char(1)='S'
as

declare	@pav_calcul int

if not exists (select prj_nrointerno from ProyectoAvanceCab where prj_nrointerno=@prj_nrointerno and pav_aamm=@pav_aamm)
	begin 
		raiserror 30011 'Planilla de avance inexistente'   
		select 30011 as ErrCode , 'Planilla de avance inexistente' as ErrDesc   
		return (30011)   
	end 

update ProyectoAvanceDet
set pav_completado=@pav_completado
where	prj_nrointerno=@prj_nrointerno and 
	pav_aamm=@pav_aamm and
	cod_AvanceGrupo=@cod_AvanceGrupo and
	cod_AvanceCoef=@cod_AvanceCoef

select @pav_calcul=SUM(pav_porcentaje)
	from ProyectoAvanceDet
	where	prj_nrointerno=@prj_nrointerno and 
		pav_aamm=@pav_aamm and 
		pav_completado='S'


if @pav_calcul is null
	select @pav_calcul=0


update ProyectoAvanceCab
set pav_calcul=@pav_calcul
where	prj_nrointerno=@prj_nrointerno and 
	pav_aamm=@pav_aamm

return(0)
go

grant execute on dbo.sp_UpdateProyectoAvanceDet to GesPetUsr
go
