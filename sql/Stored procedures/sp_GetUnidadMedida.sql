/*
-000- a. FJS 22.02.2016 - Nuevo SP para obtener las unidades de medida.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetUnidadMedida'
go

if exists (select * from sysobjects where name = 'sp_GetUnidadMedida' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetUnidadMedida
go

create procedure dbo.sp_GetUnidadMedida
	@unimedId		char(10)=null
as 
	select 
		u.unimedId,
		u.unimedDesc,
		u.unimedSmb 
	from
		UnidadMedida u
	where
		(@unimedId is null or LTRIM(RTRIM(u.unimedId)) = LTRIM(RTRIM(@unimedId)))
	return(0)
go

grant execute on dbo.sp_GetUnidadMedida to GesPetUsr
go

print 'Actualización realizada.'
go
