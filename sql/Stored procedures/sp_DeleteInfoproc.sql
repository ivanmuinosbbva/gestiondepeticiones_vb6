/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteInfoproc'
go

if exists (select * from sysobjects where name = 'sp_DeleteInfoproc' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteInfoproc
go

create procedure dbo.sp_DeleteInfoproc
	@infoprot_id	int=0
as
	delete from
		GesPet.dbo.Infoproc
	where
		(infoprot_id = @infoprot_id or @infoprot_id is null)
go

grant execute on dbo.sp_DeleteInfoproc to GesPetUsr
go

print 'Actualización realizada.'
go
