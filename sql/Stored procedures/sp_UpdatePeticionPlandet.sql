/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
-001- a. FJS 11.03.2011 - Modificación: se agrega un nuevo campo para reflejar la primera fecha de planificación cuando una petición nunca ha sido planificada.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlandet'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlandet' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlandet
go

create procedure dbo.sp_UpdatePeticionPlandet
	@per_nrointerno		int,
	@pet_nrointerno		int,
	@cod_grupo			char(8),
	@plan_orden			int,
	@plan_ordenfin		int,
	@plan_estado		char(6),
	@plan_actfch		smalldatetime,
	@plan_actusr		char(10),
	@plan_act2fch		smalldatetime,
	@plan_act2usr		char(10),
	@plan_ori			int,
	@plan_estadoini		int,
	@plan_motnoplan		int,
	@plan_fealcadef		smalldatetime,
	@plan_fefunctst		smalldatetime,
	@plan_horasper		int,
	@plan_desaestado	int,
	@plan_motivsusp		int,
	@plan_motivrepl		int,
	@plan_feplanori		smalldatetime,
	@plan_feplannue		smalldatetime,
	@plan_cantreplan	int,
	@plan_horasreplan	int,
	@plan_fchreplan		smalldatetime,
	@plan_fchestadoini	smalldatetime		-- add -001- a.
as
	update 
		GesPet.dbo.PeticionPlandet
	set
		plan_orden			= @plan_orden,
		plan_ordenfin		= @plan_ordenfin,
		plan_estado			= @plan_estado,
		plan_actfch			= @plan_actfch,
		plan_actusr			= @plan_actusr,
		plan_act2fch		= @plan_act2fch,
		plan_act2usr		= @plan_act2usr,
		plan_ori			= @plan_ori,
		plan_estadoini		= @plan_estadoini,
		plan_motnoplan		= @plan_motnoplan,
		plan_fealcadef		= @plan_fealcadef,
		plan_fefunctst		= @plan_fefunctst,
		plan_horasper		= @plan_horasper,
		plan_desaestado		= @plan_desaestado,
		plan_motivsusp		= @plan_motivsusp,
		plan_motivrepl		= @plan_motivrepl,
		plan_feplanori		= @plan_feplanori,
		plan_feplannue		= @plan_feplannue,
		plan_cantreplan		= @plan_cantreplan,
		plan_horasreplan	= @plan_horasreplan,
		plan_fchreplan		= @plan_fchreplan,
		plan_fchestadoini	= @plan_fchestadoini	-- add -001- a.
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(cod_grupo = @cod_grupo or @cod_grupo is null)
	return(0)
go

grant execute on dbo.sp_UpdatePeticionPlandet to GesPetUsr
go

print 'Actualización realizada.'
go