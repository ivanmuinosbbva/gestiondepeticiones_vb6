/*
-001- a. FJS 23.04.2008 - Se modifica la condición para poder reutilizarlo para eliminar los conformes de Homologación.
-002- a. FJS 25.11.2014 - Idem punto -001- a.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionConf'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionConf' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionConf
go

create procedure dbo.sp_DeletePeticionConf
	@pet_nrointerno	int,
	@ok_tipo		char(8),
	@ok_modo		char(3), 
	@ok_secuencia	int=null 
as
	delete 
		GesPet..PeticionConf
	where 
		@pet_nrointerno = pet_nrointerno and 
		(@ok_tipo is null or RTRIM(ok_tipo) = RTRIM(@ok_tipo)) and			-- upd -002- a.
		(@ok_modo is null or RTRIM(ok_modo) = RTRIM(@ok_modo)) and			-- upd -001- a.
		(@ok_secuencia is null or ok_secuencia = @ok_secuencia)				-- upd -002- a.
return(0)
go

grant execute on dbo.sp_DeletePeticionConf to GesPetUsr
go

print 'Actualización realizada.'
go
