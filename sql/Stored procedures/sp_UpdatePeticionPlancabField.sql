/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 26.07.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlancabField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlancabField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlancabField
go

create procedure dbo.sp_UpdatePeticionPlancabField
	@per_nrointerno	int,
	@pet_nrointerno	int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if rtrim(@campo)='PER_NROINTERNO'
	update 
		GesPet.dbo.PeticionPlancab
	set
		per_nrointerno = @valornum
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

if rtrim(@campo)='PET_NROINTERNO'
	update 
		GesPet.dbo.PeticionPlancab
	set
		pet_nrointerno = @valornum
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

if rtrim(@campo)='PRIORIDAD'
	update 
		GesPet.dbo.PeticionPlancab
	set
		prioridad = @valornum
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

if rtrim(@campo)='ORIGEN'
	update 
		GesPet.dbo.PeticionPlancab
	set
		plan_origen = @valornum
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

if rtrim(@campo)='PLAN_ACTFCH'
	update 
		GesPet.dbo.PeticionPlancab
	set
		plan_actfch = @valordate
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

if rtrim(@campo)='PLAN_ACTUSR'
	update 
		GesPet.dbo.PeticionPlancab
	set
		plan_actusr = @valortxt
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

if rtrim(@campo)='COD_BPE'
	update 
		GesPet.dbo.PeticionPlancab
	set
		cod_bpe = @valortxt
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

if rtrim(@campo)='HISTORIAL'
	update 
		GesPet.dbo.PeticionPlancab
	set
		hst_nrointerno = @valornum
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

go

grant execute on dbo.sp_UpdatePeticionPlancabField to GesPetUsr
go

print 'Actualización realizada.'
