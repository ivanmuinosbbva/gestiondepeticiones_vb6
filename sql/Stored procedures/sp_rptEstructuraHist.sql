/*
-001- a. FJS 14.11.2016 - Nuevo SP.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptEstructuraHist'
go

if exists (select * from sysobjects where name = 'sp_rptEstructuraHist' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptEstructuraHist
go

create procedure dbo.sp_rptEstructuraHist 
	 @cod_direccion     char(8)='NULL',
	 @cod_gerencia      char(8)='NULL',
	 @cod_sector		char(8)='NULL',
	 @cod_grupo			char(8)='NULL',
	 --@detalle           char(1)='0',
	 @fecha				datetime=null
as 
	set nocount on
	
	create table #temp (
		fecha		datetime,
		cod_recurso	char(10))

	-- Solo aquellos con cambio de estructura
	insert into #temp
		select MAX(e.fecha), e.cod_recurso
		from 
			Recurso r left join
			Estructura e on (r.cod_recurso = e.cod_recurso)
		where 
			--r.cod_recurso = 'A125958' and 
			--convert(char(8),e.fecha,112) <= '20160205'  	-- �Donde estaba hasta esta fecha?
			e.fecha <= @fecha
		group by
			e.cod_recurso

	-- Luego, el SP devuelve este resultado:

	select 
		r.cod_recurso,
		r.nom_recurso,
		e.cod_direccion,
		e.cod_gerencia,
		--r.cod_sector,
		--r.cod_grupo,
		e.cod_sector,
		e.cod_grupo
	from 
		--Recurso r inner join 
		Recurso r LEFT join 
		--#temp on (#temp.cod_recurso = r.cod_recurso) inner join  
		#temp on (#temp.cod_recurso = r.cod_recurso) LEFT join
		Estructura e on (e.cod_recurso = #temp.cod_recurso and convert(char(8),#temp.fecha,112) = convert(char(8),e.fecha,112))
	/*
	where
		r.cod_recurso = 'A125958'
	*/
		
	return(0) 
go

grant execute on dbo.sp_rptEstructuraHist to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go