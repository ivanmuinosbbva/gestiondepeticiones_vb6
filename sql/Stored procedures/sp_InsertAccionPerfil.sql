/*
-001- a. FJS 02.10.2008 - Nuevo SP para agregar a la parametrizaci�n de Acciones por Perfil
*/

use GesPet
go

print 'Actualizando el procedimiento almacenado: sp_InsertAccionPerfil'
go
if exists (select * from sysobjects where name = 'sp_InsertAccionPerfil' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertAccionPerfil

go

create procedure dbo.sp_InsertAccionPerfil 
	@cod_accion  char(8),
	@cod_perfil  char(4),
	@cod_estado  char(6),
	@cod_estnew  char(6),
	@flg_hereda  char(1)
as 
 
if exists (select cod_accion 
		   from AccionesPerfil 
		   where cod_accion = @cod_accion and 
				 cod_perfil = @cod_perfil and 
				 cod_estado = @cod_estado and 
				 cod_estnew = @cod_estnew) 
    begin 
        raiserror 30001 'El c�digo de acci�n x perfil ya estaba declarado.' 
        select 30001 as ErrCode , 'El c�digo de acci�n x perfil ya estaba declarado.' as ErrDesc 
        return (30001) 
    end 
else 
    begin 
        insert into GesPet..AccionesPerfil
			(cod_accion,
			 cod_perfil,
			 cod_estado,
			 cod_estnew,
			 flg_hereda) 
            values
			(@cod_accion,
			 @cod_perfil,
			 @cod_estado,
			 @cod_estnew,
			 @flg_hereda)
     end 
return(0) 
go

grant execute on dbo.sp_InsertAccionPerfil to GesPetUsr 
go

print 'Actualizaci�n finalizada.'