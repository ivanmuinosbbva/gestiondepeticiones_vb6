/*
-001- a. FJS 03.11.2008 - Se modifica el valor de la variable par�metro para el campo nom_accion (la longitud pas� de 30 a 100).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertAcciones'
go

if exists (select * from sysobjects where name = 'sp_InsertAcciones' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertAcciones

go
create procedure dbo.sp_InsertAcciones 
    @cod_accion			char(8), 
    @nom_accion			varchar(100),		-- upd -001- a.
	@IGM_hab			char(1)='S',
	@tipo_accion		char(6),
	@agrupador			int,
	@leyenda			varchar(100),
	@orden				int,
	@porEstado			char(1),
	@multiplesEstados	char(1)
as 

if exists (select cod_accion from Acciones where cod_accion = @cod_accion) 
    begin 
        raiserror 30001 'El c�digo de acci�n ya estaba declarado.' 
        select 30001 as ErrCode , 'El c�digo de acci�n ya estaba declarado.' as ErrDesc 
        return (30001)
    end
else 
    begin 
        insert into Acciones(
            cod_accion,
            nom_accion,
			IGM_hab,
			tipo_accion,
			agrupador,
			leyenda,
			orden,
			porEstado,
			multiplesEstados)
		values(
            @cod_accion,
            @nom_accion,
			@IGM_hab,
			@tipo_accion,
			@agrupador,
			@leyenda,
			@orden,
			@porEstado,
			@multiplesEstados)
    end 
	return(0) 
go

grant execute on dbo.sp_InsertAcciones to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go
