use GesPet
go

print 'Creando/actualizando SP: sp_InsertDocumento'
go

if exists (select * from sysobjects where name = 'sp_InsertDocumento' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertDocumento
go

create procedure dbo.sp_InsertDocumento
	@cod_doc	char(6),
	@nom_doc	char(80)=null,
	@hab_doc	char(1)=null,
	@actual		char(1)='S'
as
	insert into GesPet.dbo.Documento (
		cod_doc,
		nom_doc,
		hab_doc,
		actual)
	values (
		@cod_doc,
		@nom_doc,
		@hab_doc,
		@actual)
go

grant execute on dbo.sp_InsertDocumento to GesPetUsr
go

print 'Actualización realizada.'
go