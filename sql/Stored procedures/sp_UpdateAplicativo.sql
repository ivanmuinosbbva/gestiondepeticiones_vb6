/*
-000- a. FJS 19.05.2009 - Nuevo SP para manejo de Aplicativos
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateAplicativo'
go

if exists (select * from sysobjects where name = 'sp_UpdateAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateAplicativo
go

create procedure dbo.sp_UpdateAplicativo
	@app_id				char(30)=null,
	@app_nombre			char(100)=null,
	@app_hab			char(1)=null,
	@cod_direccion		char(8)=null,
	@cod_gerencia		char(8)=null,
	@cod_sector			char(8)=null,
	@cod_grupo			char(8)=null,
	@cod_r_direccion	char(10)=null,
	@cod_r_gerencia		char(10)=null,
	@cod_r_sector		char(10)=null,
	@cod_r_grupo		char(10)=null,
	@app_amb			char(1)=null
as
	update 
		GesPet..Aplicativo
	set
		app_nombre = @app_nombre,
		app_hab = @app_hab,
		cod_direccion = @cod_direccion,
		cod_gerencia = @cod_gerencia,
		cod_sector = @cod_sector,
		cod_grupo = @cod_grupo,
		cod_r_direccion = @cod_r_direccion,
		cod_r_gerencia = @cod_r_gerencia,
		cod_r_sector = @cod_r_sector,
		cod_r_grupo = @cod_r_grupo,
		app_amb = @app_amb,
		app_fe_lupd = getdate()
	where 
		app_id = @app_id
go

grant execute on dbo.sp_UpdateAplicativo to GesPetUsr
go

print 'Actualización realizada.'
