/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionValidacion'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionValidacion
go

create procedure dbo.sp_GetPeticionValidacion
	@pet_nrointerno	int=null,
	@valid			int=null,
	@valitem		int=null
as
	select 
		ag.pet_nrointerno,
		ag.valid,
		ag.valitem,
		v.valdesc,
		v.valorden,
		v.valhab,
		ag.valitemvalor,
		ag.audituser,
		ag.auditfecha
	from 
		PeticionValidacion ag inner join
		Validacion v on (ag.valid = v.valid and ag.valitem = v.valitem)
	where
		(ag.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(ag.valid = @valid or @valid is null) and
		(ag.valitem = @valitem or @valitem is null)
	order by
		ag.pet_nrointerno,
		ag.valid,
		ag.valitem
go

grant execute on dbo.sp_GetPeticionValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
