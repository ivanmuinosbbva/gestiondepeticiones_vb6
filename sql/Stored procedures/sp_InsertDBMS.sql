/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertDBMS'
go

if exists (select * from sysobjects where name = 'sp_InsertDBMS' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertDBMS
go

create procedure dbo.sp_InsertDBMS
	@dbmsId		int,
	@dbmsNom	varchar(50)
as
	insert into GesPet.dbo.DBMS (
		dbmsId,
		dbmsNom)
	values (
		@dbmsId,
		LTRIM(RTRIM(@dbmsNom)))
go

grant execute on dbo.sp_InsertDBMS to GesPetUsr
go

print 'Actualización realizada.'
go
