/*
-000- a. FJS 03.09.2010 - Nuevo SP para devolver los estados de CGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_GetEstadoCGM'
go

if exists (select * from sysobjects where name = 'sp_IGM_GetEstadoCGM' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_GetEstadoCGM
go

create procedure dbo.sp_IGM_GetEstadoCGM
	@cod_estado		char(6)=null
as 
	select
		a.cod_estado,
		a.nom_estado,
		a.IGM_hab
	from 
		GesPet..Estados a
	where
		(@cod_estado is null or a.cod_estado = @cod_estado)
	return(0)
go

grant execute on dbo.sp_IGM_GetEstadoCGM to GesPetUsr
go

grant execute on dbo.sp_IGM_GetEstadoCGM to GrpTrnIGM
go

print 'Actualización realizada.'
go 