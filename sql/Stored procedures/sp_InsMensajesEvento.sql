use GesPet
go

print 'sp_InsMensajesEvento'
go

if exists (select * from sysobjects where name = 'sp_InsMensajesEvento' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsMensajesEvento
go

create procedure dbo.sp_InsMensajesEvento 
    @evt_alcance char(3), 
    @cod_accion char(8), 
    @cod_estado char(6), 
    @cod_perfil char(4), 
    @cod_txtmsg int=0, 
    @est_recept varchar(3), 
    @est_codigo varchar(255)='', 
    @txt_extend varchar(255)='', 
    @txt_pzofin varchar(1) ='N', 
    @flg_activo varchar(1) ='S' 
as 
 
	if exists (select 1 from GesPet..MensajesEvento where 
			RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
			RTRIM(cod_accion)=RTRIM(@cod_accion) and 
			RTRIM(cod_estado)=RTRIM(@cod_estado) and 
			RTRIM(cod_perfil)=RTRIM(@cod_perfil))   
	begin   
		 select 30012 as ErrCode ,'Evento/Mensaje Existente' as ErrDesc   
		 raiserror 30012 'Evento/Mensaje Existente'   
		 return (30012)   
	end 
	else 
	begin 
		insert into GesPet..MensajesEvento  
			(evt_alcance, 
			cod_accion, 
			cod_estado, 
			cod_perfil, 
			cod_txtmsg, 
			est_recept, 
			est_codigo, 
			txt_extend, 
			txt_pzofin, 
			flg_activo) 
		values 
			(@evt_alcance, 
			@cod_accion, 
			@cod_estado, 
			@cod_perfil, 
			@cod_txtmsg, 
			@est_recept, 
			@est_codigo, 
			@txt_extend, 
			@txt_pzofin, 
			@flg_activo) 
	end 
	return(0) 
go

grant execute on dbo.sp_InsMensajesEvento to GesPetUsr 
go
