/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.09.2010 - 
-001- a. FJS 11.03.2011 - Modificación: se agrega un nuevo campo para reflejar la primera fecha de planificación cuando una petición nunca ha sido planificada.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlandet1'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlandet1' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlandet1
go

create procedure dbo.sp_UpdatePeticionPlandet1
	@per_nrointerno		int=null,
	@pet_nrointerno		int=null,
	@cod_grupo			char(8)=null,
	@plan_estadoini		int=null,
	@plan_motnoplan		int=null,
	@plan_fealcadef		smalldatetime=null,
	@plan_fefunctst		smalldatetime=null,
	@plan_horasper		int=null,
	@plan_fchestadoini	smalldatetime=null		-- add -001- a.
as
	update 
		GesPet.dbo.PeticionPlandet
	set
		plan_estadoini		= @plan_estadoini,
		plan_motnoplan		= @plan_motnoplan,
		plan_fealcadef		= @plan_fealcadef,
		plan_fefunctst		= @plan_fefunctst,
		plan_horasper		= @plan_horasper,
		plan_fchestadoini	= @plan_fchestadoini	-- add -001- a.
	where 
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(cod_grupo = @cod_grupo or @cod_grupo is null)
	return(0)
go

grant execute on dbo.sp_UpdatePeticionPlandet1 to GesPetUsr
go

print 'Actualización realizada.'
