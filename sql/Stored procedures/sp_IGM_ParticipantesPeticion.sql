/*
-000- a. XXX dd.mm.aaaa - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ParticipantesPeticion'
go

if exists (select * from sysobjects where name = 'sp_IGM_ParticipantesPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ParticipantesPeticion
go

create procedure dbo.sp_IGM_ParticipantesPeticion
	@pet_nrointerno	int=null
as 
	declare @fecha_actual_desde smalldatetime
	declare @fecha_actual_hasta smalldatetime
	declare @cod_direccion_eje  char(8)

	select @fecha_actual_desde = convert(smalldatetime,convert(char(4),year(getdate())) + '0101 00:00')
	select @fecha_actual_hasta = convert(smalldatetime,convert(char(4),year(getdate())) + '1231 23:59')
	select @cod_direccion_eje  = 'MEDIO'

	select
		 cod_direccion		= (select pp.cod_direccion from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
		 nom_direccion		= (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = (select pp.cod_direccion from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET)),
		 cod_gerencia		= (select pp.cod_gerencia from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
		 nom_gerencia		= (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = (select pp.cod_gerencia  from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET)),
		 T2.N_PET			as pet_nrointerno,
		 pet_nroasignado	= (select isnull(pp.pet_nroasignado,0) from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
		 pet_projid			= (select isnull(pp.pet_projid,0) from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
		 pet_projsubid		= (select isnull(pp.pet_projsubid,0) from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
		 pet_projsubsid		= (select isnull(pp.pet_projsubsid,0) from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
		 horas_tyo_periodo	= convert(numeric(10,4),(H_ACT/ convert(numeric(10,4), 60))),
		 horas_tyo_total	= convert(numeric(10,4),(H_TOT/ convert(numeric(10,4), 60)))
	 from
		(select 
			COD_REC,
			N_PET,
			H_ACT = sum(HOR_ACT),
			H_TOT = sum(HOR_TOT)
		 from
			   (select 
					 COD_REC = h.cod_recurso,
					 N_PET   = h.pet_nrointerno,
					 HOR_ACT = (case when (h.fe_desde >= @fecha_actual_desde) and 
 										  (h.fe_hasta <= @fecha_actual_hasta) then h.horas
								else 0 
								end),
					 HOR_TOT = (case when h.fe_hasta <= @fecha_actual_hasta then h.horas
								else 0 
								end)
				from 
					 GesPet..HorasTrabajadas h 
				where
					 (h.pet_nrointerno > 0 and h.pet_nrointerno is not null )) T1
			   group by 
				T1.COD_REC, 
				T1.N_PET) T2
		 where
			  T2.COD_REC in (select r.cod_recurso from GesPet..Recurso r where r.cod_direccion = @cod_direccion_eje) and
			  T2.N_PET   in (select p.pet_nrointerno
							 from GesPet..Peticion p
							 where 	p.cod_gerencia in (select g.cod_gerencia from GesPet..Gerencia g where g.IGM_hab='S') and
									p.cod_estado   in (select e.cod_estado from GesPet..Estados e where e.IGM_hab = 'S') and
									(@pet_nrointerno is null or p.pet_nrointerno = @pet_nrointerno))
	return(0)
go

grant execute on dbo.sp_IGM_ParticipantesPeticion to GesPetUsr
go

grant execute on dbo.sp_IGM_ParticipantesPeticion to GrpTrnIGM
go

print 'Actualizaci�n realizada.'
go

	/*
	declare @c_cod_direccion	char(8)
	declare @c_cod_gerencia		char(8)
	declare @c_pet_nrointerno	int
	declare @c_ProjId			int
	declare @c_ProjSubId		int
	declare @c_ProjSubSId		int
	declare @p_horas_act		int
	declare @p_horas_tot		int

	-- Horas TYO a�o en curso
	declare @fecha_actual_desde		char(8)
	declare @fecha_actual_hasta		char(8)

	-- Establecer las fechas del a�o en curso
	select @fecha_actual_desde = convert(char(4), datepart(yy,getdate())) + '0101'
	select @fecha_actual_hasta = convert(char(8), getdate(), 112)


	create table #TmpSalida(
		cod_direccion		char(8)			null,
		nom_direccion		char(30)		null,
		cod_gerencia		char(8)			null,
		nom_gerencia		char(30)		null,
		pet_nrointerno		int				null,
		pet_nroasignado		int				null,
		pet_projid			int				null,
		pet_projsubid		int				null,
		pet_projsubsid		int				null,
		horas_tyo_actual	numeric(10,4)	null,
		horas_tyo_total		numeric(10,4)	null)
	
	insert into #TmpSalida
		select
			r.cod_direccion,
			nom_direccion = isnull((select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = r.cod_direccion),''),
			r.cod_gerencia,
			nom_gerencia = isnull((select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = r.cod_gerencia),''),
			p.pet_nrointerno,
			p.pet_nroasignado,
			p.pet_projid,
			p.pet_projsubid,
			p.pet_projsubsid,
			0,
			0
		from
			GesPet..HorasTrabajadas h inner join
			GesPet..Recurso r on (h.cod_recurso = r.cod_recurso) inner join
			GesPet..Peticion p on (h.pet_nrointerno = p.pet_nrointerno) inner join
			GesPet..Gerencia g on (r.cod_gerencia = g.cod_gerencia)
		where
			(r.cod_direccion = 'MEDIO') and (g.IGM_hab = 'S') and 
			(@pet_nrointerno is null or h.pet_nrointerno = @pet_nrointerno)
		group by
			r.cod_direccion,
			r.cod_gerencia,
			p.pet_nrointerno,
			p.pet_nroasignado,
			p.pet_projid,
			p.pet_projsubid,
			p.pet_projsubsid

	declare cursTmp Cursor for 
		select 
			cod_direccion,
			cod_gerencia,
			pet_nrointerno,
			pet_projid,
			pet_projsubid,
			pet_projsubsid
		from #TmpSalida
		where 
			(@pet_nrointerno is null or pet_nrointerno = @pet_nrointerno)
		group by 
			cod_direccion,
			cod_gerencia,
			pet_nrointerno,
			pet_projid,
			pet_projsubid,
			pet_projsubsid
		for Read Only
	
	open cursTmp
	fetch cursTmp into @c_cod_direccion, @c_cod_gerencia, @c_pet_nrointerno, @c_ProjId, @c_ProjSubId, @c_ProjSubSId
	if @@sqlstatus = 0
	begin
		while @@sqlstatus = 0
		begin
			-- Horas TYO a�o en curso
			--exec sp_HorasTrabajadasXArea @fecha_actual_desde, @fecha_actual_hasta, 'DIRE', 'MEDIO', @c_ProjId, @c_ProjSubId, @c_ProjSubSId, @c_pet_nrointerno, @p_horas_act output
			exec sp_HorasTrabajadasXArea @fecha_actual_desde, @fecha_actual_hasta, 'GERE', @c_cod_gerencia, @c_ProjId, @c_ProjSubId, @c_ProjSubSId, @c_pet_nrointerno, @p_horas_act output
			-- Horas TYO totales
			--exec sp_HorasTrabajadasXArea 'NULL', 'NULL', 'DIRE', 'MEDIO', @c_ProjId, @c_ProjSubId, @c_ProjSubSId, @c_pet_nrointerno, @p_horas_tot output
			exec sp_HorasTrabajadasXArea 'NULL', 'NULL', 'GERE', @c_cod_gerencia, @c_ProjId, @c_ProjSubId, @c_ProjSubSId, @c_pet_nrointerno, @p_horas_tot output

			update #TmpSalida
			set 
				horas_tyo_actual = convert(numeric(10,4), (@p_horas_act/ convert(numeric(10,4),60))),
				horas_tyo_total  = convert(numeric(10,4), (@p_horas_tot/ convert(numeric(10,4),60)))
			where 
				RTRIM(cod_direccion) = RTRIM(@c_cod_direccion) and
				RTRIM(cod_gerencia)  = RTRIM(@c_cod_gerencia) and
				pet_nrointerno = @c_pet_nrointerno
			
			fetch cursTmp into @c_cod_direccion, @c_cod_gerencia, @c_pet_nrointerno, @c_ProjId, @c_ProjSubId, @c_ProjSubSId
		end
	end
	close cursTmp
	deallocate cursor cursTmp

	select 
		a.cod_direccion,
		a.nom_direccion,
		a.cod_gerencia,
		a.nom_gerencia,
		a.pet_nrointerno,
		a.pet_nroasignado,
		pet_projid = isnull(a.pet_projid,0),
		pet_projsubid = isnull(a.pet_projsubid,0),
		pet_projsubsid = isnull(a.pet_projsubsid,0),
		horas_tyo_actual = isnull(a.horas_tyo_actual,0),
		horas_tyo_total = isnull(a.horas_tyo_total,0)
	from
		#TmpSalida a
	
	drop table #TmpSalida
	*/