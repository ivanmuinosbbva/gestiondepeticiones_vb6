/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetGerenciaXt'
go

if exists (select * from sysobjects where name = 'sp_GetGerenciaXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetGerenciaXt
go

create procedure dbo.sp_GetGerenciaXt 
	@cod_gerencia	char(8)=null, 
	@cod_direccion	char(8)=null, 
	@flg_habil		char(1)=null 
as 
	begin 
		select 
			Ge.cod_gerencia, 
			Ge.nom_gerencia, 
			Ge.cod_direccion, 
			nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)),
			abrev_direccion = (select Di.abrev_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)),
			Ge.flg_habil, 
			Ge.es_ejecutor, 
			Ge.IGM_hab,
			Ge.abrev_gerencia 
		from 
			GesPet..Gerencia Ge 
		where 
			(@cod_gerencia is null or RTRIM(@cod_gerencia) is null or RTRIM(@cod_gerencia)='' or RTRIM(Ge.cod_gerencia) = RTRIM(@cod_gerencia)) and 
			(@cod_direccion is null or RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)='' or RTRIM(Ge.cod_direccion) = RTRIM(@cod_direccion)) and 
			(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(Ge.flg_habil) = RTRIM(@flg_habil)) 
		order by  
			Ge.cod_direccion, 
			Ge.cod_gerencia, 
			Ge.flg_habil
	end 
	return(0) 
go

grant execute on dbo.sp_GetGerenciaXt to GesPetUsr
go

sp_procxmode 'sp_GetGerenciaXt', 'anymode'
go

print 'Actualización realizada.'
go
