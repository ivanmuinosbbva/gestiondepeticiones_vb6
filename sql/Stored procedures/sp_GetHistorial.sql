/*
-001- a. FJS 14.11.2008 - Se modifica para permitir obtener el historial completo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHistorial'
go

if exists (select * from sysobjects where name = 'sp_GetHistorial' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHistorial
go

create procedure dbo.sp_GetHistorial 
      @pet_nrointerno   int=null,   
      @hst_nrointerno   int=null   
as   
    select  
	    HIS.pet_nrointerno, 
	    HIS.hst_nrointerno, 
	    HIS.hst_fecha, 
	    HIS.cod_evento, 
	    HIS.pet_estado, 
	    HIS.cod_sector, 
	    HIS.sec_estado, 
	    HIS.cod_grupo, 
	    HIS.gru_estado, 
	    HIS.replc_user, 
	    HIS.audit_user 
    from 
		GesPet..Historial HIS
    where 
		HIS.pet_nrointerno=@pet_nrointerno and 
		(@hst_nrointerno is null or HIS.hst_nrointerno=@hst_nrointerno) 
    order by 
		HIS.pet_nrointerno,
		HIS.hst_nrointerno 
	return(0) 
go

grant execute on dbo.sp_GetHistorial to GesPetUsr 
go

print 'Actualización realizada.'
go
