/*
-001- a. FJS 20.05.2010 - Da error 1105.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DelHistorial'
go

if exists (select * from sysobjects where name = 'sp_DelHistorial' and sysstat & 7 = 4)
	drop procedure dbo.sp_DelHistorial
go

create procedure dbo.sp_DelHistorial 
	@pet_nrointerno		int, 
	@flag				char(3)='ALL',
	@cod_evento			varchar(250)=''
as

	--begin tran
	if @flag = 'ALL'
		BEGIN
			delete Historial 
			where  pet_nrointerno = @pet_nrointerno 
		END

	if @flag = 'INC'
		BEGIN
			delete Historial 
			where pet_nrointerno = @pet_nrointerno and
				(charindex(RTRIM(cod_evento),RTRIM(@cod_evento))>0)
		END

	if @flag = 'EXC'
		BEGIN
			delete Historial 
			where pet_nrointerno = @pet_nrointerno and
				(charindex(RTRIM(cod_evento),RTRIM(@cod_evento))=0)
		END

	delete HistorialMemo   
	where hst_nrointerno not in (select hst_nrointerno from Historial) 

	--commit tran
	return(0)   
go

grant execute on dbo.sp_DelHistorial to GesPetUsr 
go

print 'Actualización realizada.'
