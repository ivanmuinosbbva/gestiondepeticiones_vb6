/*
-001- a. FJS 16.07.2007 - Se hace un join a la tabla de peticiones para poder agregar al conjunto de resultados actual, la 
						  fecha de envio al BP, el indicador de impacto tecnol�gico y la clase de la petici�n. Esto es por 
						  el punto 6 del documento CGM - Controles SOX 2007 / Alcance del desarrollo previsto - v8.0
-002- a. FJS 08.01.2009 - Se quita el join para ganar performance (los datos ser�n obtenidos del SP original de peticiones).
-002- b. FJS 08.01.2009 - Se optimiza para aprovechar el �ndice 'idx_PetInterno'.
-003- a. FJS 27.12.2016 - Se agregan dos funciones para determinar si debe considerarse el sector para el c�lculo del estado de la petici�n.
						  La primer funci�n determina si existen grupos ejecutores distintos de homologaci�n, seguridad inform�tica, arquitectura, etc.
						  Si no existen, devuelve 0. Si existen, devuelve la cantidad de grupos descontando los grupos especiales.
						  La segunda funci�n, devuelve 0 si el sector no tiene ninguno de los grupos especiales definidos, y > 0 si lo tiene.
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetPeticionSector'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionSector' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionSector
go

create procedure dbo.sp_GetPeticionSector 
    @pet_nrointerno     int=0, 
    @cod_sector         char(8)=null 
as 
begin 
    select  
        Ps.pet_nrointerno, 
        Ps.cod_sector, 
        Ps.cod_gerencia, 
        Ps.cod_direccion, 
        Ps.fe_ini_plan, 
        Ps.fe_fin_plan, 
        Ps.fe_ini_real, 
        Ps.fe_fin_real, 
        Ps.horaspresup, 
        Ps.cod_estado, 
        Ps.fe_estado, 
        Ps.cod_situacion, 
        Ps.hst_nrointerno_sol, 
        Ps.hst_nrointerno_rsp, 
        nom_sector=(select nom_sector from GesPet..Sector where RTRIM(cod_sector)=RTRIM(Ps.cod_sector)) , 
        nom_estado=(select nom_estado from GesPet..Estados where RTRIM(cod_estado)=RTRIM(Ps.cod_estado)) , 
        nom_situacion=(select nom_situacion from GesPet..Situaciones where RTRIM(cod_situacion)=RTRIM(Ps.cod_situacion)),
		--{ add -003- a.
		considerar = (
			select count(1) 
			from PeticionGrupo Pg inner join Grupo g on (Pg.cod_grupo = g.cod_grupo)
			where 
				Pg.pet_nrointerno = Ps.pet_nrointerno and 
				Pg.cod_sector = Ps.cod_sector and 
				charindex(g.grupo_homologacion,'H|1|2|')=0),
		especial = (
			select count(1)
			from PeticionGrupo Pg inner join Grupo g on (Pg.cod_grupo = g.cod_grupo)
			where 
				Pg.pet_nrointerno = Ps.pet_nrointerno and 
				Pg.cod_sector = Ps.cod_sector and 
				charindex(g.grupo_homologacion,'H|1|2|')>0)
		--}
    from
		GesPet..PeticionSector Ps 
    where   
        (@pet_nrointerno = 0 or @pet_nrointerno = Ps.pet_nrointerno) and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(Ps.cod_sector) = RTRIM(@cod_sector))
    order by 
		Ps.pet_nrointerno, 
		Ps.cod_sector ASC	-- upd -002- b.
end 

return(0) 
go

grant execute on dbo.sp_GetPeticionSector to GesPetUsr 
go

print 'Actualizaci�n finalizada.'
go

