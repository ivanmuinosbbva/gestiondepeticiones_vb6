/*
-000- a. FJS 06.08.2010 - Nuevo SP para confirmar la priorizaci�n y ordenamiento (�ste �ltimo opcional para el BP).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionPlancab2'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionPlancab2' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionPlancab2
go

create procedure dbo.sp_UpdatePeticionPlancab2
	@per_nrointerno		int=null, 
	@pet_nrointerno		int=null,
	@modo				char(10)
as 
	if @modo = 'BP'
		begin
			-- Actualiza el estado de PeticionPlandet
			update GesPet..PeticionPlandet
			set
				plan_actfch = getdate(),
				plan_actusr	= suser_name(),
				plan_estado	= 'PLANOK'
			from 
				GesPet..PeticionPlandet b inner join
				GesPet..PeticionPlancab a on (a.per_nrointerno = b.per_nrointerno and a.pet_nrointerno = b.pet_nrointerno)
			where
				b.per_nrointerno = @per_nrointerno and 
				b.pet_nrointerno = @pet_nrointerno and
				(b.cod_grupo is not null or b.cod_grupo <> '') and
				(a.prioridad is not null or a.prioridad <> '') 
				/*and
				((a.prioridad = '1' and b.plan_orden is not null) or
				 (a.prioridad <> '1'))
				*/
			
			-- Actualiza el estado de PeticionPlancab
			update GesPet..PeticionPlancab
			set 
				plan_actfch = getdate(),
				plan_actusr	= suser_name(),
				plan_estado	= 'PLANOK'
			where
				per_nrointerno = @per_nrointerno and 
				pet_nrointerno = @pet_nrointerno and
				(prioridad is not null or prioridad <> '')
		end
	if @modo = 'PRJ'
		begin
			update GesPet..PeticionPlancab
			set 
				plan_act2fch = getdate(),
				plan_act2usr = suser_name(),
				plan_estado	 = 'PLANOK'
			where
				per_nrointerno = @per_nrointerno and 
				pet_nrointerno = @pet_nrointerno and
				(prioridad is not null or prioridad <> '')
		end
		
	return(0)
go

grant execute on dbo.sp_UpdatePeticionPlancab2 to GesPetUsr
go

print 'Actualizaci�n realizada.'
