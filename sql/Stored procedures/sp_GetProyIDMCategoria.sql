/*
-000- a. FJS 27.10.2008 - Nuevo SP para mostrar las categorías en que puede encuadrarse un proyecto IDM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyIDMCategoria'
go

if exists (select * from sysobjects where name = 'sp_GetProyIDMCategoria' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyIDMCategoria
go

create procedure dbo.sp_GetProyIDMCategoria
	@ProjCatId		int=null, 
	@ProjCatNom		varchar(100)=null
as 
	select
		PRJ.ProjCatId,
		PRJ.ProjCatNom
	from 
		GesPet..ProyectoCategoria PRJ
	where 
		(@ProjCatId is null or PRJ.ProjCatId = @ProjCatId) and 
		(@ProjCatNom is null or upper(PRJ.ProjCatNom) like '%' + upper(ltrim(rtrim(@ProjCatNom))) + '%')
go

grant execute on dbo.sp_GetProyIDMCategoria to GesPetUsr
go

print 'Actualización realizada.'
