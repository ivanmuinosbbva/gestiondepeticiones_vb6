/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateInfoproi'
go

if exists (select * from sysobjects where name = 'sp_UpdateInfoproi' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateInfoproi
go

create procedure dbo.sp_UpdateInfoproi
	@infoprot_item		int,
	@infoprot_itemdsc	varchar(255),
	@infoprot_itemest	char(1),
	@infoprot_itemtpo	char(1),
	@infoprot_itemres	char(4),
	@infoprot_itemresp	varchar(255)
as
	update 
		GesPet.dbo.Infoproi
	set
		infoprot_itemdsc	= @infoprot_itemdsc,
		infoprot_itemest	= @infoprot_itemest,
		infoprot_itemtpo	= @infoprot_itemtpo,
		infoprot_itemres	= @infoprot_itemres,
		infoprot_itemresp	= @infoprot_itemresp
	where 
		infoprot_item = @infoprot_item
go

grant execute on dbo.sp_UpdateInfoproi to GesPetUsr
go

print 'Actualización realizada.'
go
