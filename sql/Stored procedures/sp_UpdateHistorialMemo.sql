/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHistorialMemo'
go

if exists (select * from sysobjects where name = 'sp_UpdateHistorialMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHistorialMemo
go

create procedure dbo.sp_UpdateHistorialMemo
	@hst_nrointerno		int,
	@hst_secuencia		smallint,
	@mem_texto			char(255)
as 
	UPDATE HistorialMemo
	SET mem_texto = @mem_texto
	WHERE  
		hst_nrointerno = @hst_nrointerno AND 
		hst_secuencia = @hst_secuencia
go

grant execute on dbo.sp_UpdateHistorialMemo to GesPetUsr
go

print 'Actualización realizada.'
go
