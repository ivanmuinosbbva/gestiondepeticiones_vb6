/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */
 
/*
-000- a. FJS 02.10.2009 - 
-009- a. FJS 25.03.2015 - Nuevo: se agrega un nuevo parámetro para filtrar (nombre productivo).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT001a'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT001a' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT001a
go

create procedure dbo.sp_GetHEDT001a
	@dsn_nom			char(50)
as
	select 
		a.dsn_id,
		a.dsn_nom,
		a.dsn_enmas,
		a.dsn_vb		as dsn_vb_id,
		valido = (
		case
			when a.dsn_enmas = 'N' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
			when ((select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and (x.cpo_nomrut <> '' and x.cpo_nomrut is not null))>0) and a.dsn_enmas = 'S' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
			else 'No'
		end)
	from 
		GesPet..HEDT001 a
	where
		LTRIM(RTRIM(a.dsn_nom)) = LTRIM(RTRIM(@dsn_nom))
	order by 
		a.dsn_nom
	
	return(0)
go

grant execute on dbo.sp_GetHEDT001a to GesPetUsr
go

print 'Actualización realizada.'
go
