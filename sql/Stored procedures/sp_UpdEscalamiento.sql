use GesPet
go
print 'sp_UpdEscalamiento'
go
if exists (select * from sysobjects where name = 'sp_UpdEscalamiento' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdEscalamiento
go
create procedure dbo.sp_UpdEscalamiento 
    @evt_alcance char(3), 
    @cod_estado char(6), 
    @pzo_desde  int, 
    @pzo_hasta  int, 
    @cod_accion char(8), 
    @fec_activo smalldatetime 
as 
 
if not exists (select 1 from GesPet..Escalamientos where 
        RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
        RTRIM(cod_estado)=RTRIM(@cod_estado) and 
        pzo_desde=@pzo_desde and 
        pzo_hasta=@pzo_hasta and 
        RTRIM(cod_accion)=RTRIM(@cod_accion))   
begin   
     select 30012 as ErrCode ,'Evento/Mensaje Inexistente' as ErrDesc   
     raiserror 30012 'Evento/Mensaje Inexistente'   
     return (30012)   
end 
else 
begin 
    update GesPet..Escalamientos  
    set     fec_activo=@fec_activo 
    where   RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
        RTRIM(cod_estado)=RTRIM(@cod_estado) and 
        pzo_desde=@pzo_desde and 
        pzo_hasta=@pzo_hasta and 
        RTRIM(cod_accion)=RTRIM(@cod_accion) 
end 
return(0) 
go



grant execute on dbo.sp_UpdEscalamiento to GesPetUsr 
go


