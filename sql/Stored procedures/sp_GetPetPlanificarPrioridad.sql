/*
-000- a. FJS 19.05.2017 - SP que devuelve las peticiones ordenadas por priorización para un periodo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetPlanificarPrioridad'
go

if exists (select * from sysobjects where name = 'sp_GetPetPlanificarPrioridad' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetPlanificarPrioridad
go

create procedure dbo.sp_GetPetPlanificarPrioridad
	@per_nrointerno	int
as 
	select
		pc.per_nrointerno,
		pc.prioridad,
		pc.pet_nrointerno
	from 
		GesPet..PeticionPlancab pc
	where
		pc.per_nrointerno = @per_nrointerno and
		(pc.prioridad is not null and pc.prioridad <> 0)
	order by
		pc.per_nrointerno,
		pc.prioridad
	
	return (0)
go

grant execute on dbo.sp_GetPetPlanificarPrioridad to GesPetUsr
go

print 'Actualización realizada.'
go