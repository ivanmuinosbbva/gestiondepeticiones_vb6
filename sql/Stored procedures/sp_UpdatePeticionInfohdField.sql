/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionInfohdField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionInfohdField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionInfohdField
go

create procedure dbo.sp_UpdatePeticionInfohdField
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_item		int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if UPPER(RTRIM(@campo))='PET_NROINTERNO'
	update 
		GesPet.dbo.PeticionInfohd
	set
		pet_nrointerno = @valornum
	where 
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(info_id = @info_id or @info_id is null) and
		(info_item = @info_item or @info_item is null)
if UPPER(RTRIM(@campo))='INFO_ID'
	update 
		GesPet.dbo.PeticionInfohd
	set
		info_id = @valornum
	where 
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(info_id = @info_id or @info_id is null) and
		(info_item = @info_item or @info_item is null)
if UPPER(RTRIM(@campo))='INFO_ITEM'
	update 
		GesPet.dbo.PeticionInfohd
	set
		info_item = @valornum
	where 
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(info_id = @info_id or @info_id is null) and
		(info_item = @info_item or @info_item is null)
if UPPER(RTRIM(@campo))='INFO_VALOR'
	update 
		GesPet.dbo.PeticionInfohd
	set
		info_valor = @valornum
	where 
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(info_id = @info_id or @info_id is null) and
		(info_item = @info_item or @info_item is null)
if UPPER(RTRIM(@campo))='INFO_ORDEN'
	update 
		GesPet.dbo.PeticionInfohd
	set
		info_orden = @valornum
	where 
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(info_id = @info_id or @info_id is null) and
		(info_item = @info_item or @info_item is null)
go

grant execute on dbo.sp_UpdatePeticionInfohdField to GesPetUsr
go

print 'Actualización realizada.'
go
