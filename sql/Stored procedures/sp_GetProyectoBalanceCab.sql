use GesPet
go
print 'sp_GetProyectoBalanceCab'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoBalanceCab' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoBalanceCab
go
create procedure dbo.sp_GetProyectoBalanceCab 
	@prj_nrointerno	int=0
as 

select 
	AVC.prj_nrointerno, 
	AVC.cod_PerfilCosto, 
	nom_PerfilCosto = (select DIRPRF.nom_PerfilCostBenef from PerfilCostBenef DIRPRF where RTRIM(DIRPRF.cod_PerfilCostBenef)=RTRIM(AVC.cod_PerfilCosto)),
	AVC.cod_PerfilBenef, 
	nom_PerfilBeneficio = (select DIRPRB.nom_PerfilCostBenef from PerfilCostBenef DIRPRB where RTRIM(DIRPRB.cod_PerfilCostBenef)=RTRIM(AVC.cod_PerfilBenef)),
	AVC.txt_Beneficio
from  ProyectoBalanceCab AVC
where	@prj_nrointerno=0 or AVC.prj_nrointerno=@prj_nrointerno 


return(0) 
go

grant execute on dbo.sp_GetProyectoBalanceCab to GesPetUsr 
go
