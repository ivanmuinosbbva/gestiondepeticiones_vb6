/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertDrivers'
go

if exists (select * from sysobjects where name = 'sp_InsertDrivers' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertDrivers
go

create procedure dbo.sp_InsertDrivers
	@driverId	int,
	@driverNom	char(255),
	@driverResp	char(4),
	@driverHab	char(1),
	@indicadorId	int,
	@driverValor	int,
	@driverAyuda	char(255),
	@driverReq		char(1)
as
	insert into GesPet.dbo.Drivers (
		driverId,
		driverNom,
		driverResp,
		driverHab,
		indicadorId,
		driverValor,
		driverAyuda,
		driverReq)
	values (
		@driverId,
		@driverNom,
		@driverResp,
		@driverHab,
		@indicadorId,
		@driverValor,
		@driverAyuda,
		@driverReq)
go

grant execute on dbo.sp_InsertDrivers to GesPetUsr
go

print 'Actualización realizada.'
go
