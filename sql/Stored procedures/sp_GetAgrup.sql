use GesPet
go
print 'sp_GetAgrup'
go
if exists (select * from sysobjects where name = 'sp_GetAgrup' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrup
go
create procedure dbo.sp_GetAgrup 
             @agr_nrointerno   int
as 
begin 
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	nom_nropadre = (select P.agr_titulo from Agrup P where P.agr_nrointerno=A.agr_nropadre),
	pdr_vigente = (select Q.agr_vigente from Agrup Q where Q.agr_nrointerno=A.agr_nropadre),
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.agr_admpet,
	A.audit_user,
	A.audit_date
    from 
        Agrup A
    where   
        (@agr_nrointerno = 0 or A.agr_nrointerno = @agr_nrointerno)
    order by A.agr_nrointerno ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetAgrup to GesPetUsr 
go
