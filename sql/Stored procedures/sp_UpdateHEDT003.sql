/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - Se crea el SP para manejo de catalogo de archivos de datos enmascarables.
-001- a. FJS 22.10.2009 - Se cambia la longitud del campo cpo_id de 8 a 18.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT003'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT003' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT003
go

create procedure dbo.sp_UpdateHEDT003
	@dsn_id			char(8)=null,
	@cpy_id			char(8)=null,
	@cpo_id			char(18)=null,		-- upd -002- a.
	@cpo_nrobyte	char(4)=null,
	@cpo_canbyte	char(4)=null,
	@cpo_type		char(1)=null,
	@cpo_decimals	char(4)=null,
	@cpo_dsc		char(20)=null,
	@cpo_nomrut		char(8)=null
as
	declare @fmt_cpo_nrobyte	char(4)
	declare @fmt_cpo_canbyte	char(4)
	declare @fmt_cpo_decimals	char(4)
	declare @fe_hoy				smalldatetime
	declare @fe_hoytxt			char(8)
	declare @user_id			char(10)

	select @user_id = SUSER_NAME()
	select @fe_hoy = getdate()
	select @fe_hoytxt = convert(char(8), @fe_hoy, 112)
	
	-- @cpo_nrobyte
	select @fmt_cpo_nrobyte = 
		case 
			when datalength(rtrim(@cpo_nrobyte)) = 1 then '000' + rtrim(@cpo_nrobyte)
			when datalength(rtrim(@cpo_nrobyte)) = 2 then '00' + rtrim(@cpo_nrobyte) 
			when datalength(rtrim(@cpo_nrobyte)) = 3 then '0' + rtrim(@cpo_nrobyte)
			when datalength(rtrim(@cpo_nrobyte)) = 4 then rtrim(@cpo_nrobyte)
		else
			rtrim(@cpo_nrobyte)
		end
	-- @cpo_canbyte
	select @fmt_cpo_canbyte = 
		case 
			when datalength(rtrim(@cpo_canbyte)) = 1 then '000' + rtrim(@cpo_canbyte)
			when datalength(rtrim(@cpo_canbyte)) = 2 then '00' + rtrim(@cpo_canbyte)
			when datalength(rtrim(@cpo_canbyte)) = 3 then '0' + rtrim(@cpo_canbyte)
			when datalength(rtrim(@cpo_canbyte)) = 4 then rtrim(@cpo_canbyte)
		else
			rtrim(@cpo_canbyte)
		end
	-- @cpo_decimals
	select @fmt_cpo_decimals = 
		case 
			when datalength(rtrim(@cpo_decimals)) = 1 then '000' + rtrim(@cpo_decimals)
			when datalength(rtrim(@cpo_decimals)) = 2 then '00' + rtrim(@cpo_decimals)
			when datalength(rtrim(@cpo_decimals)) = 3 then '0' + rtrim(@cpo_decimals)
			when datalength(rtrim(@cpo_decimals)) = 4 then rtrim(@cpo_decimals)
		else
			rtrim(@cpo_decimals)
		end
	begin
		update 
			GesPet.dbo.HEDT003
		set
			cpo_nrobyte  = @cpo_nrobyte,
			cpo_canbyte  = @cpo_canbyte,
			cpo_type	 = @cpo_type,
			cpo_decimals = @cpo_decimals,
			cpo_dsc		 = @cpo_dsc,
			cpo_nomrut	 = @cpo_nomrut,
			cpo_feult	 = @fe_hoy,
			cpo_userid	 = @user_id
		where 
			(dsn_id = @dsn_id or @dsn_id is null) and
			(cpy_id = @cpy_id or @cpy_id is null) and
			(cpo_id = @cpo_id or @cpo_id is null)
			
			/*
			-- Guardo en la tabla de transmisión
			execute sp_InsertHEDTHST 'HEDT003', @dsn_id, @cpy_id, @cpo_id, null, null, null, null, null, null, null, null, null, null,
												null, null, null, null, 
												@fmt_cpo_nrobyte, @fmt_cpo_canbyte, @cpo_type, @fmt_cpo_decimals, @cpo_dsc, @cpo_nomrut, @fe_hoytxt, @user_id
			*/
	end
	return(0)
go

grant execute on dbo.sp_UpdateHEDT003 to GesPetUsr
go

print 'Actualización realizada.'
