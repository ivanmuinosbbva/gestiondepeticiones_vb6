/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.22 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteHitoClase'
go

if exists (select * from sysobjects where name = 'sp_DeleteHitoClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHitoClase
go

create procedure dbo.sp_DeleteHitoClase
	@hitoid	int=0
as
	delete from
		GesPet.dbo.HitoClase
	where
		(hitoid = @hitoid or @hitoid is null)
go

grant execute on dbo.sp_DeleteHitoClase to GesPetUsr
go

print 'Actualización realizada.'
go
