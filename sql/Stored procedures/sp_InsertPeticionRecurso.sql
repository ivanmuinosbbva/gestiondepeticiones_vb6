/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 13.10.2009 - Se corrige el SP porque esta buscando de manera incorrecta al recurso.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionRecurso'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionRecurso
go

create procedure dbo.sp_InsertPeticionRecurso 
	@pet_nrointerno     int, 
	@cod_recurso	char(10),
	@cod_grupo      char(8)=null, 
	@cod_sector     char(8)=null, 
	@cod_gerencia   char(8)=null,
	@cod_direccion  char(8)=null
as 
 
--if exists (select cod_recurso from GesPet..PeticionRecurso where @pet_nrointerno=pet_nrointerno and RTRIM(cod_recurso)=RTRIM(@cod_recurso))	-- del -001- a.
--{ add -001- a.
if exists (select cod_recurso 
			from GesPet..PeticionRecurso 
			where @pet_nrointerno = pet_nrointerno and 
					RTRIM(cod_direccion) = RTRIM(@cod_direccion) and
					RTRIM(cod_gerencia) = RTRIM(@cod_gerencia) and
					RTRIM(cod_sector) = RTRIM(@cod_sector) and
					RTRIM(cod_grupo) = RTRIM(@cod_grupo) and
					RTRIM(cod_recurso) = RTRIM(@cod_recurso))
--}
	begin 
		raiserror 30001 'Alta de Peticion/Recurso Existente'   
		select 30001 as ErrCode , 'Alta de Peticion/Recurso Existente' as ErrDesc   
		return (30001)   
	end 
else 
	begin 
		insert into GesPet..PeticionRecurso( 
			pet_nrointerno, 
			cod_recurso,
			cod_grupo, 
			cod_sector, 
			cod_gerencia, 
			cod_direccion, 
			audit_user,   
			audit_date ) 
		values(
			@pet_nrointerno, 
			@cod_recurso,
			@cod_grupo, 
			@cod_sector, 
			@cod_gerencia, 
			@cod_direccion, 
			SUSER_NAME(),   
			getdate())
	end 
return(0) 
go

grant execute on dbo.sp_InsertPeticionRecurso to GesPetUsr 
go

print 'Actualización realizada.'
