/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 31.05.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertAccionesPerfilEstados'
go

if exists (select * from sysobjects where name = 'sp_InsertAccionesPerfilEstados' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertAccionesPerfilEstados
go

create procedure dbo.sp_InsertAccionesPerfilEstados
	@cod_version	int,
	@cod_accion		char(8),
	@cod_perfil		char(4),
	@cod_estado		char(6),
	@cod_estnew		char(6),
	@flg_hereda		char(1)
as
	insert into GesPet.dbo.AccionesPerfilEstados (
		cod_version,
		cod_accion,
		cod_perfil,
		cod_estado,
		cod_estnew,
		flg_hereda)
	values (
		@cod_version,
		@cod_accion,
		@cod_perfil,
		@cod_estado,
		@cod_estnew,
		@flg_hereda)
go

grant execute on dbo.sp_InsertAccionesPerfilEstados to GesPetUsr
go

print 'Actualización realizada.'
go
