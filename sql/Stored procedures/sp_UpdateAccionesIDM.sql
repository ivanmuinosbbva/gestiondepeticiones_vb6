/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 24.06.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateAccionesIDM'
go

if exists (select * from sysobjects where name = 'sp_UpdateAccionesIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateAccionesIDM
go

create procedure dbo.sp_UpdateAccionesIDM
	@cod_accion	char(8),
	@nom_accion	char(100)
as
	update GesPet.dbo.AccionesIDM
	set nom_accion = @nom_accion
	where cod_accion = @cod_accion
go

grant execute on dbo.sp_UpdateAccionesIDM to GesPetUsr
go

print 'Actualización realizada.'
go
