/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 09.11.2010 - Manejo de documentos adjuntos del sistema
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateDocumento'
go

if exists (select * from sysobjects where name = 'sp_UpdateDocumento' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateDocumento
go

create procedure dbo.sp_UpdateDocumento
	@cod_doc	char(6),
	@nom_doc	char(80),
	@hab_doc	char(1)
as
	update 
		GesPet.dbo.Documento
	set
		nom_doc = @nom_doc,
		hab_doc = @hab_doc
	where 
		cod_doc = @cod_doc
go

grant execute on dbo.sp_UpdateDocumento to GesPetUsr
go

print 'Actualización realizada.'
go
