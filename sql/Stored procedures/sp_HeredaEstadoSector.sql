
/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
/*
sp_HeredaEstadoSector
	@pet_nrointerno=7681,
	@new_estado='SUSPEN',
	@fe_ini_plan=null,
	@fe_fin_plan=null,
	@fe_ini_real=null,
	@fe_fin_real=null,
	@horaspresup=0,
	@estado_filtro='OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK|PLANIF|PLANOK|EJECUC|REVISA',
	@que_hereda='E',
	@hst_nrointerno=40213
*/
use GesPet
go
print 'sp_HeredaEstadoSector'
go
if exists (select * from sysobjects where name = 'sp_HeredaEstadoSector' and sysstat & 7 = 4)
drop procedure dbo.sp_HeredaEstadoSector
go
create procedure dbo.sp_HeredaEstadoSector
	@pet_nrointerno	int,
	@new_estado	char(6)='',
	@fe_ini_plan	smalldatetime=null,
	@fe_fin_plan	smalldatetime=null,
	@fe_ini_real	smalldatetime=null,
	@fe_fin_real	smalldatetime=null,
	@horaspresup	smallint,
	@estado_filtro	varchar(255)='NULL',
	@que_hereda	char(1)='',
	@hst_nrointerno	int
as

declare @x_fin_orig smalldatetime
declare @x_ini_orig smalldatetime
declare @x_fin_plan smalldatetime
declare @x_ini_plan smalldatetime
declare @x_fin_real smalldatetime
declare @x_ini_real smalldatetime
declare @cod_estado char(6)
declare @cant_planif int
declare @replanif char(1)
declare @cod_grupo char(8)
declare @cod_sector char(8)

DECLARE CursSector CURSOR FOR
select	PS.cod_sector
from	PeticionSector PS
where @pet_nrointerno=PS.pet_nrointerno and
	(RTRIM(@estado_filtro)='NULL' or charindex(PS.cod_estado,@estado_filtro)>0 )
for read only

OPEN CursSector
FETCH CursSector INTO @cod_sector
WHILE (@@sqlstatus = 0)
BEGIN
	select @replanif = 'N'
	if @que_hereda='E'
	begin
		update	GesPet..PeticionSector
		set	cod_estado  = @new_estado,
			fe_estado   =   getdate(),
			cod_situacion =	 '',
			ult_accion  = '',
			hst_nrointerno_sol= @hst_nrointerno,
			hst_nrointerno_rsp= @hst_nrointerno,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where @pet_nrointerno=pet_nrointerno and
			(RTRIM(cod_sector)=RTRIM(@cod_sector))
	end
	if @que_hereda='F' or @que_hereda='A' or @que_hereda='H' or @que_hereda='X'
	/* averigua si es planificacion o replanificacion */
	begin
		select  @x_ini_real=fe_ini_real,@x_fin_real=fe_fin_real,@x_ini_orig=fe_ini_orig,@x_fin_orig=fe_fin_orig, @cod_estado=cod_estado,@x_ini_plan=fe_ini_plan, @x_fin_plan=fe_fin_plan,@cant_planif=cant_planif
		from   GesPet..PeticionSector
		where @pet_nrointerno=pet_nrointerno and
			RTRIM(cod_sector)=RTRIM(@cod_sector)

		if  (@fe_ini_plan is not null or @fe_fin_plan is not null) and charindex(@cod_estado,'PLANOK|EJECUC|TERMIN')>0
		/* solo los estados que me interesan */
		/* y seguro que estoy mandando fechas */
		begin
			if ((@x_ini_plan is null) or ((@x_ini_plan is not null) and ('X'+convert(char(8),@x_ini_plan,112)<>'X'+convert(char(8),@fe_ini_plan,112)))) or
			   ((@x_fin_plan is null) or ((@x_fin_plan is not null) and ('X'+convert(char(8),@x_fin_plan,112)<>'X'+convert(char(8),@fe_fin_plan,112))))
			/* es una fecha nueva o distinta a la que existia */
			/* entonces puedo decir que estoy planificando una vez m�s */
			begin
				if @cant_planif is null
					select @cant_planif = 0
				select @cant_planif = @cant_planif + 1
				if @cant_planif = 1
					select @replanif = 'P'
				else
					select @replanif = 'R'
			end
		end
	end
	if @que_hereda='F'
	begin
		update GesPet..PeticionSector  set
			hst_nrointerno_sol= @hst_nrointerno,
			hst_nrointerno_rsp= @hst_nrointerno,
			fe_ini_plan=@fe_ini_plan,
			fe_fin_plan=@fe_fin_plan,
			fe_ini_real=@fe_ini_real,
			fe_fin_real=@fe_fin_real,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where @pet_nrointerno=pet_nrointerno and
			(RTRIM(cod_sector)=RTRIM(@cod_sector))
	end
	if @que_hereda='A'
	begin
		update GesPet..PeticionSector
		set cod_estado  = @new_estado,
			fe_estado   =   getdate(),
			cod_situacion =  '',
			ult_accion  = '',
			hst_nrointerno_sol= @hst_nrointerno,
			hst_nrointerno_rsp= @hst_nrointerno,
			fe_ini_plan=@fe_ini_plan,
			fe_fin_plan=@fe_fin_plan,
			fe_ini_real=@fe_ini_real,
			fe_fin_real=@fe_fin_real,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where @pet_nrointerno=pet_nrointerno and
			(RTRIM(cod_sector)=RTRIM(@cod_sector))
	end
	if @que_hereda='H'
	begin
		update GesPet..PeticionSector
		set cod_estado  = @new_estado,
			fe_estado   =   getdate(),
			cod_situacion =  '',
			ult_accion  = '',
			hst_nrointerno_sol= @hst_nrointerno,
			hst_nrointerno_rsp= @hst_nrointerno,
			fe_ini_plan=@fe_ini_plan,
			fe_fin_plan=@fe_fin_plan,
			fe_ini_real=@fe_ini_real,
			fe_fin_real=@fe_fin_real,
			horaspresup=@horaspresup,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where @pet_nrointerno=pet_nrointerno and
			(RTRIM(cod_sector)=RTRIM(@cod_sector))
	end
	if @que_hereda='X'
	begin
		if @fe_ini_real is null
			select @fe_ini_real=@x_ini_real
		if @fe_fin_real is null
			select @fe_fin_real=@x_fin_real
		if (@fe_fin_real is not null) and (@fe_ini_real is not null) and ('X'+convert(char(8),@fe_ini_real,112)>'X'+convert(char(8),@fe_fin_real,112))
		begin
			select @fe_fin_real=@fe_ini_real
		end
		update GesPet..PeticionSector set
			hst_nrointerno_sol= @hst_nrointerno,
			hst_nrointerno_rsp= @hst_nrointerno,
			fe_ini_plan=@fe_ini_plan,
			fe_fin_plan=@fe_fin_plan,
			fe_ini_real=@fe_ini_real,
			fe_fin_real=@fe_fin_real,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where @pet_nrointerno=pet_nrointerno and
			(RTRIM(cod_sector)=RTRIM(@cod_sector))
	end

	if @que_hereda='F' or @que_hereda='A' or @que_hereda='H' or @que_hereda='X'
	begin
		if @replanif = 'P'
		begin
			update GesPet..PeticionSector
			set	fe_ini_orig = @fe_ini_plan,
				fe_fin_orig = @fe_fin_plan,
				cant_planif	= @cant_planif,
				fe_fec_orig = getdate(),
				fe_fec_plan = getdate()
			where	@pet_nrointerno=pet_nrointerno and
				RTRIM(cod_sector)=RTRIM(@cod_sector)
		end
		if @replanif = 'R'
		begin
			update GesPet..PeticionSector
			set	cant_planif	= @cant_planif,
				fe_fec_plan = getdate()
			where	@pet_nrointerno=pet_nrointerno and
				RTRIM(cod_sector)=RTRIM(@cod_sector)
		end
	end
	if  (@fe_ini_plan is null or @fe_fin_plan is null)
	begin
		update GesPet..PeticionSector
		set fe_fec_plan = null
		where @pet_nrointerno=pet_nrointerno and
			RTRIM(cod_sector)=RTRIM(@cod_sector)
	end

	/* ahora hace lo mismo con los hijos del sector */
	DECLARE CursGrupo CURSOR FOR
	select	PG.cod_grupo
	from	PeticionGrupo PG
	where @pet_nrointerno=PG.pet_nrointerno and
		(RTRIM(PG.cod_sector)=RTRIM(@cod_sector)) and
		(RTRIM(@estado_filtro)='NULL' or charindex(PG.cod_estado,@estado_filtro)>0 )
	for read only
	OPEN CursGrupo
	FETCH CursGrupo INTO @cod_grupo
	WHILE (@@sqlstatus = 0)
	BEGIN
		select @replanif = 'N'
		
		if @que_hereda='E'
		begin
			update GesPet..PeticionGrupo
			set cod_estado  = @new_estado,
			fe_estado   =   getdate(),
			cod_situacion =	 '',
			ult_accion  = '',
			hst_nrointerno_sol= @hst_nrointerno,
			hst_nrointerno_rsp= @hst_nrointerno,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
			where @pet_nrointerno=pet_nrointerno and
			(RTRIM(cod_sector)=RTRIM(@cod_sector)) and
			(RTRIM(cod_grupo)=RTRIM(@cod_grupo))
		end

		if @que_hereda='F' or @que_hereda='A' or @que_hereda='H' or @que_hereda='X'
		/* averigua si es planificacion o replanificacion */
		begin
			select @x_ini_real=fe_ini_real,@x_fin_real=fe_fin_real,@x_ini_orig=fe_ini_orig,@x_fin_orig=fe_fin_orig, @cod_estado=cod_estado,@x_ini_plan=fe_ini_plan, @x_fin_plan=fe_fin_plan,@cant_planif=cant_planif
			from   GesPet..PeticionGrupo
			where @pet_nrointerno=pet_nrointerno and
				RTRIM(cod_sector)=RTRIM(@cod_sector) and
				RTRIM(cod_grupo)=RTRIM(@cod_grupo)

			if  (@fe_ini_plan is not null or @fe_fin_plan is not null) and charindex(@cod_estado,'PLANOK|EJECUC|TERMIN')>0
			/* solo los estados que me interesan */
			/* y seguro que estoy mandando fechas */
			begin
				if ((@x_ini_plan is null) or ((@x_ini_plan is not null) and ('X'+convert(char(8),@x_ini_plan,112)<>'X'+convert(char(8),@fe_ini_plan,112)))) or
				((@x_fin_plan is null) or ((@x_fin_plan is not null) and ('X'+convert(char(8),@x_fin_plan,112)<>'X'+convert(char(8),@fe_fin_plan,112))))
				/* es una fecha nueva o distinta a la que existia */
				/* entonces puedo decir que estoy planificando una vez m�s */
				begin
					if @cant_planif is null
						select @cant_planif = 0
					select @cant_planif = @cant_planif + 1
					if @cant_planif = 1
						select @replanif = 'P'
					else
						select @replanif = 'R'
				end
			end

		end

		if @que_hereda='F'
		begin
			update GesPet..PeticionGrupo  set
				hst_nrointerno_sol= @hst_nrointerno,
				hst_nrointerno_rsp= @hst_nrointerno,
				fe_ini_plan=@fe_ini_plan,
				fe_fin_plan=@fe_fin_plan,
				fe_ini_real=@fe_ini_real,
				fe_fin_real=@fe_fin_real,
				audit_user = SUSER_NAME(),
				audit_date = getdate()
			where @pet_nrointerno=pet_nrointerno and
				(RTRIM(cod_sector)=RTRIM(@cod_sector)) and
				(RTRIM(cod_grupo)=RTRIM(@cod_grupo))
		end

		if @que_hereda='A'
		begin
			update GesPet..PeticionGrupo
			set cod_estado  = @new_estado,
				fe_estado   =   getdate(),
				cod_situacion =  '',
				ult_accion  = '',
				hst_nrointerno_sol= @hst_nrointerno,
				hst_nrointerno_rsp= @hst_nrointerno,
				fe_ini_plan=@fe_ini_plan,
				fe_fin_plan=@fe_fin_plan,
				fe_ini_real=@fe_ini_real,
				fe_fin_real=@fe_fin_real,
				audit_user = SUSER_NAME(),
				audit_date = getdate()
			where @pet_nrointerno=pet_nrointerno and
				(RTRIM(cod_sector)=RTRIM(@cod_sector)) and
				(RTRIM(cod_grupo)=RTRIM(@cod_grupo))
		end

		if @que_hereda='H'
		begin
			update GesPet..PeticionGrupo
			set cod_estado = @new_estado,
				fe_estado = getdate(),
				cod_situacion = '',
				ult_accion = '',
				hst_nrointerno_sol= @hst_nrointerno,
				hst_nrointerno_rsp= @hst_nrointerno,
				fe_ini_plan=@fe_ini_plan,
				fe_fin_plan=@fe_fin_plan,
				fe_ini_real=@fe_ini_real,
				fe_fin_real=@fe_fin_real,
				horaspresup=@horaspresup,
				audit_user = SUSER_NAME(),
				audit_date = getdate()
			where @pet_nrointerno=pet_nrointerno and
				(RTRIM(cod_sector)=RTRIM(@cod_sector)) and
				(RTRIM(cod_grupo)=RTRIM(@cod_grupo))
		end

		if @que_hereda='X'
		begin
			if @fe_ini_real is null
				select @fe_ini_real=@x_ini_real
			if @fe_fin_real is null
				select @fe_fin_real=@x_fin_real
			if (@fe_fin_real is not null) and (@fe_ini_real is not null) and ('X'+convert(char(8),@fe_ini_real,112)>'X'+convert(char(8),@fe_fin_real,112))
			begin
				select @fe_fin_real=@fe_ini_real
			end
			update GesPet..PeticionGrupo set
				hst_nrointerno_sol= @hst_nrointerno,
				hst_nrointerno_rsp= @hst_nrointerno,
				fe_ini_plan=@fe_ini_plan,
				fe_fin_plan=@fe_fin_plan,
				fe_ini_real=@fe_ini_real,
				fe_fin_real=@fe_fin_real,
				audit_user = SUSER_NAME(),
				audit_date = getdate()
			where @pet_nrointerno=pet_nrointerno and
				(RTRIM(cod_sector)=RTRIM(@cod_sector)) and
				(RTRIM(cod_grupo)=RTRIM(@cod_grupo))
		end

		if @que_hereda='F' or @que_hereda='A' or @que_hereda='H' or @que_hereda='X'
		begin
			if @replanif = 'P'
			begin
				update GesPet..PeticionGrupo
				set	fe_ini_orig = @fe_ini_plan,
					fe_fin_orig = @fe_fin_plan,
					cant_planif	= @cant_planif,
					fe_fec_orig = getdate(),
					fe_fec_plan = getdate()
				where	@pet_nrointerno=pet_nrointerno and
					RTRIM(cod_sector)=RTRIM(@cod_sector) and
					RTRIM(cod_grupo)=RTRIM(@cod_grupo)
			end

			if @replanif = 'R'
			begin
				update GesPet..PeticionGrupo
				set	cant_planif	= @cant_planif,
					fe_fec_plan = getdate()
				where	@pet_nrointerno=pet_nrointerno and
					RTRIM(cod_sector)=RTRIM(@cod_sector) and
					RTRIM(cod_grupo)=RTRIM(@cod_grupo)
			end
		end

		if  (@fe_ini_plan is null or @fe_fin_plan is null)
		begin
			update GesPet..PeticionGrupo
			set fe_fec_plan = null
			where @pet_nrointerno=pet_nrointerno and
				RTRIM(cod_sector)=RTRIM(@cod_sector) and
				RTRIM(cod_grupo)=RTRIM(@cod_grupo)
		end

		
		FETCH CursGrupo INTO @cod_grupo
	END
	CLOSE CursGrupo
	DEALLOCATE CURSOR CursGrupo
	/* fin */
	FETCH CursSector INTO @cod_sector
END
CLOSE CursSector
DEALLOCATE CURSOR CursSector
return(0)
go



grant execute on dbo.sp_HeredaEstadoSector to GesPetUsr
go


