use GesPet
go
print 'sp_GetProyectoSectores'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoSectores' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoSectores
go
create procedure dbo.sp_GetProyectoSectores
	@prj_nrointerno	int,
	@cod_estado	varchar(250)=null
as
select DISTINCT
	SEC.cod_direccion,
	nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(SEC.cod_direccion)),
	SEC.cod_gerencia,
	nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where RTRIM(Ge.cod_gerencia) = RTRIM(SEC.cod_gerencia)),
	SEC.cod_sector,
	nom_sector = (select Se.nom_sector from Sector Se where RTRIM(Se.cod_sector) = RTRIM(SEC.cod_sector))
from
	PeticionSector SEC
where
	SEC.pet_nrointerno in (select PET.pet_nrointerno from Peticion PET where PET.prj_nrointerno = @prj_nrointerno)



return(0)
go



grant execute on dbo.sp_GetProyectoSectores to GesPetUsr
go


