use GesPet
go
print 'sp_DeleteTarea'
go
if exists (select * from sysobjects where name = 'sp_DeleteTarea' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteTarea
go
create procedure dbo.sp_DeleteTarea 
               @cod_tarea       char(8) 
as 
 
if exists (select cod_tarea from GesPet..Tarea where RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
begin 
    if exists (select cod_tarea from GesPet..TareaSector where RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
    begin 
        raiserror  30010 'Existe alg�n Sector asociado a esta Tarea'  
        select 30010 as ErrCode , 'Existe alg�n Sector asociado a esta Tarea'  as ErrDesc 
    return (30010) 
    end 
     
    if exists (select cod_tarea from GesPet..HorasTrabajadas where RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
    begin 
        raiserror  30011 'Existen Horas Trabajadas asociados a este Tarea'  
        select 30011 as ErrCode , 'Existen  Horas Trabajadas asociados a este Tarea'  as ErrDesc 
    return (30011) 
    end 
 
    delete GesPet..Tarea 
      where RTRIM(cod_tarea) = RTRIM(@cod_tarea) 
end 
else 
begin 
    raiserror 30001 'Tarea Inexistente'   
    select 30001 as ErrCode , 'Tarea Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteTarea to GesPetUsr 
go


