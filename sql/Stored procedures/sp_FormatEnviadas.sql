/*
-000- a. FJS 06.05.2009 - Nuevo SP para formatear la cadena de envio a HOST (ChangeMan).
-001- a. FJS 25.09.2009 - Se corrige el uso de la función porque efectua un trim indeseado.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_FormatEnviadas'
go

if exists (select * from sysobjects where name = 'sp_FormatEnviadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_FormatEnviadas
go

create procedure dbo.sp_FormatEnviadas
	@pet_nrointerno		int, 
	@pet_sector			char(8)=null,
	@pet_grupo			char(8)=null,
	@pet_record			char(80)	output
as 
	select @pet_record = ''

	declare @hora		char(6)
	declare @hor		char(2)
	declare @min		char(2)
	declare @seg		char(2)
	declare @usuario	char(8)		-- add -001- a.

	select @hora = ''
	select @hor = @hor + substring(convert(char(10), getdate(), 108), 1, 2)
	select @min = @min + substring(convert(char(10), getdate(), 108), 4, 2)
	select @seg = @seg + substring(convert(char(10), getdate(), 108), 7, 2)

	select @hora = convert(char(2), @hor) + convert(char(2), @min) + convert(char(2), @seg)

	--{ add -001- a.
	select @usuario = substring(SUSER_NAME(),1,8)
	if datalength(@usuario) < 8
		begin
			select @usuario = @usuario + space(8-datalength(@usuario))
		end
	--}

	select @pet_record = 
		space(3) +
		case
			when datalength(ltrim(rtrim(convert(char(10), pet_nroasignado)))) = 1 then '000000' + ltrim(rtrim(convert(char(10), pet_nroasignado)))
			when datalength(ltrim(rtrim(convert(char(10), pet_nroasignado)))) = 2 then '00000' + ltrim(rtrim(convert(char(10), pet_nroasignado)))
			when datalength(ltrim(rtrim(convert(char(10), pet_nroasignado)))) = 3 then '0000' + ltrim(rtrim(convert(char(10), pet_nroasignado)))
			when datalength(ltrim(rtrim(convert(char(10), pet_nroasignado)))) = 4 then '000' + ltrim(rtrim(convert(char(10), pet_nroasignado)))
			when datalength(ltrim(rtrim(convert(char(10), pet_nroasignado)))) = 5 then '00' + ltrim(rtrim(convert(char(10), pet_nroasignado)))
			when datalength(ltrim(rtrim(convert(char(10), pet_nroasignado)))) = 6 then '0' + ltrim(rtrim(convert(char(10), pet_nroasignado)))
			else ltrim(rtrim(convert(char(7), pet_nroasignado)))
		end +
		space(1) +
		cod_clase + space(4) + 
		case
			when (@pet_sector is null or @pet_sector = '') then space(8)
			when (@pet_sector is not null or @pet_sector <> '') then 
				case
					when datalength(ltrim(rtrim(@pet_sector))) = 1 then ltrim(rtrim(@pet_sector)) + space(7)
					when datalength(ltrim(rtrim(@pet_sector))) = 2 then ltrim(rtrim(@pet_sector)) + space(6) 
					when datalength(ltrim(rtrim(@pet_sector))) = 3 then ltrim(rtrim(@pet_sector)) + space(5)
					when datalength(ltrim(rtrim(@pet_sector))) = 4 then ltrim(rtrim(@pet_sector)) + space(4)
					when datalength(ltrim(rtrim(@pet_sector))) = 5 then ltrim(rtrim(@pet_sector)) + space(3)
					when datalength(ltrim(rtrim(@pet_sector))) = 6 then ltrim(rtrim(@pet_sector)) + space(2)
					when datalength(ltrim(rtrim(@pet_sector))) = 7 then ltrim(rtrim(@pet_sector)) + space(1)
					else @pet_sector
				end
		end + 
		case
			when (@pet_grupo is null or @pet_grupo = '') then space(8)
			when (@pet_grupo is not null or @pet_grupo <> '') then 
				case
					when datalength(ltrim(rtrim(@pet_grupo))) = 1 then ltrim(rtrim(@pet_grupo)) + space(7)
					when datalength(ltrim(rtrim(@pet_grupo))) = 2 then ltrim(rtrim(@pet_grupo)) + space(6)
					when datalength(ltrim(rtrim(@pet_grupo))) = 3 then ltrim(rtrim(@pet_grupo)) + space(5)
					when datalength(ltrim(rtrim(@pet_grupo))) = 4 then ltrim(rtrim(@pet_grupo)) + space(4)
					when datalength(ltrim(rtrim(@pet_grupo))) = 5 then ltrim(rtrim(@pet_grupo)) + space(3)
					when datalength(ltrim(rtrim(@pet_grupo))) = 6 then ltrim(rtrim(@pet_grupo)) + space(2)
					when datalength(ltrim(rtrim(@pet_grupo))) = 7 then ltrim(rtrim(@pet_grupo)) + space(1)
					else @pet_grupo
				end
		end +
		--substring(SUSER_NAME(),1,8) +		-- del -001- a.
		@usuario + 		-- add -001- a.
		convert(char(8), getdate(), 112) +
		@hora + 
		space(21) +
		space(2)
	from GesPet..Peticion
	where pet_nrointerno = @pet_nrointerno

go

grant execute on dbo.sp_FormatEnviadas to GesPetUsr
go

print 'Actualización realizada.'
