/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.11.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoIDMHisto'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoIDMHisto' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoIDMHisto
go

create procedure dbo.sp_InsertProyectoIDMHisto
	@hst_nrointerno	int OUTPUT, 
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@hst_fecha		char(20),
	@cod_evento		char(8),
	@proj_estado	char(6),
	@replc_user		char(10),
	@audit_user		char(10)
as

	declare @proxnumero int 
	select @proxnumero = 0 
	execute sp_HistorialIDMProximo @proxnumero OUTPUT 
 
	select @hst_nrointerno = @proxnumero 
 
	insert into GesPet.dbo.ProyectoIDMHisto (
		ProjId,
		ProjSubId,
		ProjSubSId,
		hst_nrointerno,
		hst_fecha,
		cod_evento,
		proj_estado,
		replc_user,
		audit_user)
	values (
		@ProjId,
		@ProjSubId,
		@ProjSubSId,
		@hst_nrointerno,
		convert(smalldatetime, @hst_fecha),
		@cod_evento,
		@proj_estado,
		@replc_user,
		@audit_user)
go

grant execute on dbo.sp_InsertProyectoIDMHisto to GesPetUsr
go

print 'Actualización realizada.'
go
