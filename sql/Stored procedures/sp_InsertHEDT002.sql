/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHEDT002'
go

if exists (select * from sysobjects where name = 'sp_InsertHEDT002' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHEDT002
go

create procedure dbo.sp_InsertHEDT002
	@dsn_id		char(8)=null,
	@cpy_id		char(8)=null,
	@cpy_dsc	char(50)=null,
	@cpy_nomrut	char(8)=null
as
	declare @fe_hoy		smalldatetime
	declare @fe_hoytxt	char(8)
	declare @user_id	char(10)

	select @fe_hoy  = getdate()
	select @user_id = SUSER_NAME()
	select @fe_hoytxt = convert(char(8), @fe_hoy, 112)

	begin
		insert into GesPet.dbo.HEDT002 (
			dsn_id,
			cpy_id,
			cpy_dsc,
			cpy_nomrut,
			cpy_feult,
			cpy_userid,
			estado,
			fe_hst_estado)
		values (
			@dsn_id,
			@cpy_id,
			@cpy_dsc,
			@cpy_nomrut,
			@fe_hoy,
			@user_id,
			'A',
			null)
		/*
		-- Guardo en la tabla de transmisión
		execute sp_InsertHEDTHST 'HEDT002', @dsn_id, @cpy_id, '', null, null, null, null, null, null, null, null, null, null,
											@cpy_dsc, @cpy_nomrut, @fe_hoytxt, @user_id, 
											null, null, null, null, null, null, null, null
		*/
	end
	return(0)
go

grant execute on dbo.sp_InsertHEDT002 to GesPetUsr
go

print 'Actualización realizada.'
