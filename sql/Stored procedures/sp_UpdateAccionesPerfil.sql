use GesPet
go
print 'sp_UpdateAccionesPerfil'
go
if exists (select * from sysobjects where name = 'sp_UpdateAccionesPerfil' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateAccionesPerfil
go
create procedure dbo.sp_UpdateAccionesPerfil 
            @cod_accion  char(8)=null, 
            @cod_perfil  char(4)=null, 
            @cod_estado  char(6)=null, 
            @cod_estnew  char(6)=null 
as 
 
      insert into GesPet..AccionesPerfil 
        (cod_accion,cod_perfil,cod_estado,cod_estnew) 
      values 
             (@cod_accion, @cod_perfil,@cod_estado,@cod_estnew) 
return(0) 
go



grant execute on dbo.sp_UpdateAccionesPerfil to GesPetUsr 
go


