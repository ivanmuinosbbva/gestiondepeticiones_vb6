use GesPet
go
print 'sp_DelEscalamiento'
go
if exists (select * from sysobjects where name = 'sp_DelEscalamiento' and sysstat & 7 = 4)
drop procedure dbo.sp_DelEscalamiento
go
create procedure dbo.sp_DelEscalamiento 
    @evt_alcance char(3), 
    @cod_estado char(6), 
    @pzo_desde  int, 
    @pzo_hasta  int, 
    @cod_accion char(8) 
as 
    delete  GesPet..Escalamientos where 
        RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
        RTRIM(cod_estado)=RTRIM(@cod_estado) and 
        pzo_desde=@pzo_desde and 
        pzo_hasta=@pzo_hasta and 
        RTRIM(cod_accion)=RTRIM(@cod_accion) 
return(0) 
go



grant execute on dbo.sp_DelEscalamiento to GesPetUsr 
go


