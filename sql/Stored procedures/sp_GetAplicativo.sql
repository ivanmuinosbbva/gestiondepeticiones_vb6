/*
-000- a. FJS 12.05.2009 - Nuevo SP para manejo de Aplicativos
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetAplicativo'
go

if exists (select * from sysobjects where name = 'sp_GetAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAplicativo
go

create procedure dbo.sp_GetAplicativo
	@app_id				char(30)=null,
	@app_nombre			char(100)=null,
	@app_hab			char(1)=null
as
	select 
		a.app_id,
		a.app_nombre,
		a.app_hab,
		a.cod_direccion,
		nom_direccion = (select b.nom_direccion from GesPet..Direccion b where b.cod_direccion = a.cod_direccion),
		a.cod_gerencia,
		nom_gerencia = (select b.nom_gerencia from GesPet..Gerencia b where b.cod_gerencia = a.cod_gerencia),
		a.cod_sector,
		nom_sector = (select b.nom_sector from GesPet..Sector b where b.cod_sector = a.cod_sector),
		a.cod_grupo,
		nom_grupo = (select b.nom_grupo from GesPet..Grupo b where b.cod_grupo = a.cod_grupo),
		a.cod_r_direccion,
		a.cod_r_gerencia,
		a.cod_r_sector,
		a.cod_r_grupo,
		a.app_amb,
		a.app_fe_alta,
		a.app_fe_lupd
	from 
		GesPet.dbo.Aplicativo a
	where
		(a.app_id = @app_id or @app_id is null) and
		(a.app_nombre = @app_nombre or @app_nombre is null) and
		(a.app_hab = @app_hab or @app_hab is null)

	/*
	select 
		a.app_id,
		a.app_nombre,
		a.app_hab,
		a.cod_direccion,
		nom_direccion = (select b.nom_direccion from GesPet..Direccion b where b.cod_direccion = a.cod_direccion),
		usr_direccion = 
		case
			when (a.cod_r_direccion is null) then (select b.nom_recurso 
													from GesPet..Recurso b 
													where 
													 b.cod_direccion = a.cod_direccion and 
													 b.flg_cargoarea = 'S' and
													(b.cod_gerencia = '' or 
													 b.cod_gerencia = space(col_length('GesPet..Recurso', 'b.cod_gerencia')) or 
													 b.cod_gerencia is null) 
													and 
													(b.cod_sector = '' or 
													 b.cod_sector = space(col_length('GesPet..Recurso', 'b.cod_sector')) or 
													 b.cod_sector is null) 
													and 
													(b.cod_grupo = '' or 
													 b.cod_grupo = space(col_length('GesPet..Recurso', 'b.cod_grupo')) or 
													 b.cod_grupo is null) 
													)
			when (a.cod_r_direccion is not null) then (select b.nom_recurso from GesPet..Recurso b where b.cod_recurso = a.cod_r_direccion)
		end,
		a.cod_gerencia,
		nom_gerencia = (select b.nom_gerencia from GesPet..Gerencia b where b.cod_gerencia = a.cod_gerencia),
		usr_gerencia =
		case
			when (a.cod_r_gerencia is null) then (select b.nom_recurso 
													from GesPet..Recurso b 
													where 
													 b.cod_direccion = a.cod_direccion and 
													 b.cod_gerencia = a.cod_gerencia and
													 b.flg_cargoarea = 'S' and
													(b.cod_sector = '' or 
													 b.cod_sector = space(col_length('GesPet..Recurso', 'b.cod_sector')) or 
													 b.cod_sector is null) 
													and 
													(b.cod_grupo = '' or 
													 b.cod_grupo = space(col_length('GesPet..Recurso', 'b.cod_grupo')) or 
													 b.cod_grupo is null) 
													)
			when (a.cod_r_gerencia is not null) then (select b.nom_recurso from GesPet..Recurso b where b.cod_recurso = a.cod_r_gerencia)
		end,
		a.cod_sector,
		nom_sector = (select b.nom_sector from GesPet..Sector b where b.cod_sector = a.cod_sector),
		usr_sector = 
		case
			when (a.cod_r_sector is null) then (select b.nom_recurso 
													from GesPet..Recurso b 
													where 
													 b.cod_direccion = a.cod_direccion and 
													 b.cod_gerencia = a.cod_gerencia and
													 b.cod_sector = a.cod_sector and 
													 b.flg_cargoarea = 'S' and
													(b.cod_grupo = '' or 
													 b.cod_grupo = space(col_length('GesPet..Recurso', 'b.cod_grupo')) or 
													 b.cod_grupo is null) 
													)
			when (a.cod_r_sector is not null) then (select b.nom_recurso from GesPet..Recurso b where b.cod_recurso = a.cod_r_sector)
		end,
		a.cod_grupo,
		nom_grupo = (select b.nom_grupo from GesPet..Grupo b where b.cod_grupo = a.cod_grupo),
		usr_grupo =
		case
			when (a.cod_r_grupo is null) then (select b.nom_recurso 
													from GesPet..Recurso b 
													where 
													 b.cod_direccion = a.cod_direccion and 
													 b.cod_gerencia = a.cod_gerencia and
													 b.cod_sector = a.cod_sector and 
													 b.cod_grupo = a.cod_grupo and 
													 b.flg_cargoarea = 'S')
			when (a.cod_r_grupo is not null) then (select b.nom_recurso from GesPet..Recurso b where b.cod_recurso = a.cod_r_grupo)
		end,
		a.cod_r_direccion,
		a.cod_r_gerencia,
		a.cod_r_sector,
		a.cod_r_grupo,
		a.app_amb,
		a.app_fe_alta,
		a.app_fe_lupd
	from 
		GesPet.dbo.Aplicativo a
	where
		(a.app_id = @app_id or @app_id is null) and
		(a.app_nombre = @app_nombre or @app_nombre is null) and
		(a.app_hab = @app_hab or @app_hab is null)
	*/
go

grant execute on dbo.sp_GetAplicativo to GesPetUsr
go

print 'Actualización realizada.'
