/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */
 
/*
Un DSN es v�lido si tiene campos definidos (al menos uno) con rutina de enmascaramiento para dicho campo y se encuentra declarado con datos sensibles

-000- a. FJS 27.10.2009 - Este sp es utilizado para traer solo aquellos elementos pendientes de visar por parte de Seguridad Inform�tica.
-001- a. FJS 20.11.2009 - Se agrega una descripci�n en vez del resultado del campo bruto.
-002- a. FJS 16.12.2009 - Se agregan campos para indicar la validez del archivo declarado.
-003- a. FJS 17.06.2010 - Se contemplan nuevos estados al respecto del visado de SI: pendiente, rechazado o visado.
-004- a. FJS 17.06.2010 - Se agrega nuevo campo de texto para informar el motivo de rechazo de un DSN por parte de SI.
-005- a. FJS 06.07.2010 - Se modifica el filtro para soportar nuevos par�metros.
-006- a. FJS 15.03.2011 - Debe adem�s, considerar los dsn en estado 'B' (ejemplo: aquellos modificados por el usuario).
-007- a. FJS 08.07.2011 - Para que un DSN se considere v�lido, adem�s debe tener el nombre productivo.
-008- a. FJS 30.08.2012 - Pet. N� 51089
*/
 
use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT101'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT101' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT101
go

create procedure dbo.sp_GetHEDT101
	@dsn_id		char(8)=null,
	@dsn_nom	char(50)=null,
	--@fch_desde	char(8)=null,
	--@fch_hasta	char(8)=null,
	@fch_desde	smalldatetime=null,
	@fch_hasta	smalldatetime=null,
	@estados	char(30)=null
	--@dsn_vb		char(20)=null
as
	select 
		a.dsn_id,
		a.dsn_nom,
		dsn_enmas = 
			case
				when a.dsn_enmas = 'S' then 'Si'
				when a.dsn_enmas = 'N' then 'No'
			end,
		a.dsn_nomrut,
		a.dsn_feult,
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_userid),
		dsn_vb =
			case
				/* del -008- a.
				when a.dsn_vb = 'N' or a.dsn_vb is null then 'Pendiente'
				when a.dsn_vb = 'R' then 'Rechazado'
				when a.dsn_vb = 'S' then 'Visado'
				*/
				when a.dsn_vb = 'B' then 'Borrador'
				when a.dsn_vb = 'N' then 'Pendiente revisi�n'
				when a.dsn_vb = 'D' then 'A revisar DyD'
				when a.dsn_vb = 'H' then 'Hist�rico'
				when a.dsn_vb = 'P' then 'Pendiente pos-revisi�n'
				when a.dsn_vb = 'R' then 'Rechazado'
				when a.dsn_vb = 'S' then 'Visado'
				else 'Pendiente revisi�n'
			end,
		nom_vb_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_vb_userid),
		a.dsn_vb_fe,
		a.app_id,
		app_name = (select x.app_nombre from GesPet..Aplicativo x where x.app_id = a.app_id),
		a.dsn_desc,
		nom_estado = 
			case
				when a.estado = 'A' then 'A�n sin utilizar'
				when a.estado = 'B' then 'Ya utilizado'
				when a.estado = 'C' then 'Ya utilizado'
			end,
		a.estado,
		valido = 
			case
				when a.dsn_enmas = 'N' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
				when ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
					  (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0 and a.dsn_id not in
					  (select x.dsn_id from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null)) 
					and a.dsn_enmas = 'S' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '') then 'Si'
				else
					'No'
			end,
		a.dsn_vb as dsn_vb_id,
		a.dsn_txt,
		a.dsn_nomprod,
		a.dsn_userid,
		nom_userid = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_userid),
		a.fe_hst_estado,
		nom_vb_userid = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.dsn_vb_userid),
		dsn_vb1 = (case
			/* del -008- a.
			when a.dsn_vb = 'N' or a.dsn_vb is null then 'N'
			when a.dsn_vb = 'R' then 'R'
			when a.dsn_vb = 'S' then 'S'
			*/
			when a.dsn_vb = 'B' then 'B'
			when a.dsn_vb = 'N' or a.dsn_vb is null then 'N'
			when a.dsn_vb = 'D' then 'D'
			when a.dsn_vb = 'H' then 'H'
			when a.dsn_vb = 'P' then 'P'
			when a.dsn_vb = 'R' then 'R'
			when a.dsn_vb = 'S' then 'S'
		end),
		a.dsn_cpybbl,
		a.dsn_cpynom,
		a.dsn_cantuso
	from 
		GesPet.dbo.HEDT001 a
	where
		(@dsn_id is null or a.dsn_id = @dsn_id) and
		(@dsn_nom is null or upper(a.dsn_nom) like '%' + upper(@dsn_nom) + '%') and
		(@fch_desde is null or a.dsn_feult >= @fch_desde) and
		(@fch_hasta is null or a.dsn_feult <= @fch_hasta) and
		(@estados is null or charindex(a.dsn_vb, RTRIM(@estados)) > 0) and
		a.dsn_vb <> 'B'						-- Se excluyen aquellos en estado "Borrador" (no son v�lidos)	 -- add -008- a.
		--(@dsn_vb is null or charindex(a.dsn_vb, RTRIM(@dsn_vb)) > 0) and
		--charindex(a.estado,'B|C|')>0		-- upd -006- a. En este caso el estado es "Ya utilizado" (B y C) -- del -008- a.
	order by a.dsn_id
	return(0)
go

grant execute on dbo.sp_GetHEDT101 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
