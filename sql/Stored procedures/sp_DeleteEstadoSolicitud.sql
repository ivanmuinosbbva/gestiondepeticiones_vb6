/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 11.08.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteEstadoSolicitud'
go

if exists (select * from sysobjects where name = 'sp_DeleteEstadoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteEstadoSolicitud
go

create procedure dbo.sp_DeleteEstadoSolicitud
	@cod_estado	char(1)=''
as
	delete from
		GesPet.dbo.EstadoSolicitud
	where
		cod_estado = @cod_estado
go

grant execute on dbo.sp_DeleteEstadoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
