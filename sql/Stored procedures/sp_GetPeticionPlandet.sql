/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
-001- a. FJS 15.11.2010 - Planificación DYD - 2da. etapa: agregado de peticiones de OPTimización.
-002- a. FJS 11.03.2011 - Modificación: se agrega un nuevo campo para reflejar la primera fecha de planificación cuando una petición nunca ha sido planificada.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionPlandet'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionPlandet' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionPlandet
go

create procedure dbo.sp_GetPeticionPlandet
	@per_nrointerno	int=null,
	@pet_nrointerno	int=null,
	@cod_grupo	char(8)=null
as
	select 
		a.per_nrointerno,
		p.per_abrev,
		a.pet_nrointerno,
		a.cod_grupo,
		a.plan_orden,
		a.plan_ordenfin,
		a.plan_estado,
		a.plan_actfch,
		a.plan_actusr,
		a.plan_act2fch,
		a.plan_act2usr,
		a.plan_ori,
		nom_stage = (case
			when a.plan_ori = 1 then 'Planificación inicial'
			when a.plan_ori = 2 then 'Agregada'
			--{ add -001- a.
			when a.plan_ori = 3 then 'Planificación inicial (DYD)'
			when a.plan_ori = 4 then 'Agregada (DYD)'
			else '***'
			--}
		end),
		a.plan_estadoini,
		nom_estadoini = (select x.nom_estado_plan from GesPet..EstadosPlanificacion x where x.cod_estado_plan = a.plan_estadoini),
		a.plan_motnoplan,
		nom_motnoplan = (select x.nom_motivo_noplan from GesPet..MotivosNoPlan x where x.cod_motivo_noplan = a.plan_motnoplan),
		a.plan_fealcadef,
		a.plan_fefunctst,
		a.plan_horasper,
		a.plan_desaestado,
		nom_desaestado = (select x.nom_estado_desa from GesPet..EstadosDesarrollo x where x.cod_estado_desa = a.plan_desaestado),
		a.plan_motivsusp,
		nom_motivsusp = (select x.nom_motivo_suspen from GesPet..MotivosSuspen x where x.cod_motivo_suspen = a.plan_motivsusp),
		a.plan_motivrepl,
		nom_motivrepl = (select x.nom_motivo_replan from GesPet..MotivosReplan x where x.cod_motivo_replan = a.plan_motivrepl),
		a.plan_feplanori,
		a.plan_feplannue,
		a.plan_cantreplan,
		a.plan_horasreplan,
		a.plan_fchreplan,
		a.plan_fchestadoini		-- add -002- a.
	from 
		GesPet..PeticionPlandet a inner join
		GesPet..PeticionPlancab z on (a.per_nrointerno = z.per_nrointerno and a.pet_nrointerno = z.pet_nrointerno) inner join
		GesPet..Periodo p on (p.per_nrointerno = a.per_nrointerno)
	where
		(a.per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(a.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(a.cod_grupo = @cod_grupo or @cod_grupo is null)
	return(0)
go

grant execute on dbo.sp_GetPeticionPlandet to GesPetUsr
go

print 'Actualización realizada.'
go