/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteMotivosReplan'
go

if exists (select * from sysobjects where name = 'sp_DeleteMotivosReplan' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteMotivosReplan
go

create procedure dbo.sp_DeleteMotivosReplan
	@cod_motivo_replan	int
as
	delete from
		GesPet.dbo.MotivosReplan
	where
		(cod_motivo_replan = @cod_motivo_replan or @cod_motivo_replan is null)
	return(0)
go

grant execute on dbo.sp_DeleteMotivosReplan to GesPetUsr
go

print 'Actualización realizada.'
