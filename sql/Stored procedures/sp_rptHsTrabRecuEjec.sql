/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/* 
-001- a. - FJS 02.05.2008 - Se cambia la condici�n de "menor o igual" a "menor" porque el informe debe
							devolver aquellos recursos con un porcentaje inferior al solicitado, y no
							inferior o igual, como esta haciendo. Esta incidencia fu� reportada por Carlos Patrignani.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptHsTrabRecuEjec'
go

if exists (select * from sysobjects where name = 'sp_rptHsTrabRecuEjec' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHsTrabRecuEjec
go

create procedure dbo.sp_rptHsTrabRecuEjec
	@fdesde		varchar(8)='NULL',
	@fhasta		varchar(8)='NULL',
	@nivel		varchar(4)='NULL',
	@area		varchar(8)='NULL',
	@recurso	varchar(10)='NULL',
	@porcmin	varchar(3)='0'
as
	if @fdesde is null
		select @fdesde ='NULL'
	if @fhasta is null
		select @fhasta ='NULL'
	if @nivel is null
		select @nivel ='NULL'
	if @area is null
		select @area ='NULL'
	if @recurso is null
		select @recurso ='NULL'
	if @porcmin is null
		select @porcmin ='0'

	declare @cod_direccion		varchar(10)
	declare @cod_gerencia		varchar(10)
	declare @cod_sector			varchar(10)
	declare @cod_grupo			varchar(10)
	declare @cod_recurso		varchar(10)
	declare @cod_tarea			varchar(10)
	declare @nom_tarea			varchar(50)
	declare @pet_nrointerno		int
	declare @pet_nroasignado	int
	declare @titulo				varchar(50)
	declare @fe_desde			smalldatetime
	declare @fe_hasta			smalldatetime
	declare @horas				int
	declare @trabsinasignar		char(1)
	declare @sol_desde			smalldatetime
	declare @sol_hasta			smalldatetime
	declare @sol_dias			int
	declare @ret_dias			int
	declare @ret_horas			int
	declare @horas_nom			numeric(10,2)
	declare @horas_tra			numeric(10,2)
	declare @xhoras_tra			numeric(10,2)
	declare @porc_min			numeric(10,2)
	declare @ret_desde			smalldatetime
	declare @ret_hasta			smalldatetime
	declare @tip_asig			varchar(3)
	declare @cod_asig			varchar(10)
	declare @cod_real			varchar(10)
	declare @nom_asig			varchar(50)

	set dateformat ymd

	if RTRIM(@fdesde)='NULL'
	begin
		select @sol_desde=dateadd(mm,-1,getdate())
		select @fdesde=convert(char(8),@sol_desde,112)
	end
	else
		select @sol_desde=@fdesde
	if RTRIM(@fhasta)='NULL'
	begin
		select @sol_hasta=dateadd(mm,1,getdate())
		select @fhasta=convert(char(8),@sol_hasta,112)
	end
	else
		select @sol_hasta=@fhasta

	select @porc_min = convert(numeric(10,2), @porcmin)

	execute sp_GetHorasPeriodo @sol_desde,
		@sol_hasta,
		@sol_desde,
		@sol_hasta,
		0,
		@ret_desde	OUTPUT,
		@ret_hasta	OUTPUT,
		@sol_dias	OUTPUT,
		@ret_horas	OUTPUT

	CREATE TABLE #TmpHoras(
		cod_direccion	char(10) null,
		cod_gerencia	char(10) null,
		cod_sector		char(10) null,
		cod_grupo		char(10) null,
		cod_recurso		char(10) null,
		nom_direccion	char(40) null,
		nom_gerencia	char(40) null,
		nom_sector		char(40) null,
		nom_grupo		char(40) null,
		nom_recurso		char(40) null,
		horas_nom		numeric(10,2),
		horas_tra		numeric(10,2),
		horas_por		numeric(10,2))

	CREATE TABLE #CursXRecurso(
		cod_direccion	char(10) null,
		cod_gerencia	char(10) null,
		cod_sector		char(10) null,
		cod_grupo		char(10) null,
		cod_recurso		char(10) null,
		horasdiarias	int)
		--horasdiarias	smallint)

	CREATE TABLE #CursXHoras(
		fe_desde		smalldatetime,
		fe_hasta		smalldatetime,
		horas			int)
		--horas			smallint)

	set rowcount 0
	--print 'INSERT CursXRecurso'
	insert into #CursXRecurso
		select	
			Re.cod_direccion,
			Re.cod_gerencia,
			Re.cod_sector,
			Re.cod_grupo,
			Re.cod_recurso,
			Re.horasdiarias
		from
			GesPet..Recurso Re
		where	
			(RTRIM(@recurso)='NULL' OR RTRIM(Re.cod_recurso)=RTRIM(@recurso)) and
			(Re.estado_recurso<>'B') and
			(Re.horasdiarias>0) and
			((@nivel is null or RTRIM(@nivel)='NULL') or
			(@nivel='DIRE' and RTRIM(Re.cod_direccion)=RTRIM(@area)) or
			(@nivel='GERE' and RTRIM(Re.cod_gerencia)=RTRIM(@area)) or
			(@nivel='SECT' and RTRIM(Re.cod_sector)=RTRIM(@area)) or
			(@nivel='GRUP' and RTRIM(Re.cod_grupo)=RTRIM(@area)))
		order by 
			Re.cod_direccion,Re.cod_gerencia,Re.cod_sector,Re.cod_grupo,Re.cod_recurso


	while (exists (select cod_recurso from #CursXRecurso))
	BEGIN
		set rowcount 1

		SELECT	
			@cod_direccion	= cod_direccion,
			@cod_gerencia	= cod_gerencia,
			@cod_sector		= cod_sector,
			@cod_grupo		= cod_grupo,
			@cod_recurso	= cod_recurso,
			@horas_nom		= horasdiarias
		FROM #CursXRecurso

		set rowcount 0

		insert into #CursXHoras
		select	
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas
		from	GesPet..HorasTrabajadas Ht
		where	
			(RTRIM(@fdesde)='NULL' or convert(char(8),Ht.fe_hasta,112) >= @fdesde) and
			(RTRIM(@fhasta)='NULL' or convert(char(8),Ht.fe_desde,112) <= @fhasta) and
			(RTRIM(Ht.cod_recurso)=RTRIM(@cod_recurso))

		select @horas_tra = 0

		while (exists (select fe_desde from #CursXHoras))
		BEGIN
			set rowcount 1

			SELECT	
				@fe_desde=fe_desde,
				@fe_hasta=fe_hasta,
				@horas=horas
			FROM #CursXHoras

			execute sp_GetHorasPeriodo @sol_desde,
				@sol_hasta,
				@fe_desde,
				@fe_hasta,
				@horas,
				@ret_desde OUTPUT,
				@ret_hasta OUTPUT,
				@ret_dias OUTPUT,
				@ret_horas OUTPUT

			select @horas_tra = @horas_tra + @ret_horas

			set rowcount 1
			delete #CursXHoras
			set rowcount 0
		END

		insert #TmpHoras (
			cod_direccion,
			cod_gerencia,
			cod_sector,
			cod_grupo,
			cod_recurso,
			horas_nom,
			horas_tra,
			horas_por)
		values	(
			@cod_direccion,
			@cod_gerencia,
			@cod_sector,
			@cod_grupo,
			@cod_recurso,
			@horas_nom * @sol_dias,
			@horas_tra,
			0)

		set rowcount 1
		delete #CursXRecurso
		set rowcount 0
	END

	update #TmpHoras
		set horas_por = convert(numeric(10,2),(convert(numeric(10,2),(horas_tra / 60)) / horas_nom))
		where horas_nom > 0

	select  
		TS.cod_direccion,
		nom_direccion = isnull((select nom_direccion from Direccion where cod_direccion=TS.cod_direccion),''),
		TS.cod_gerencia,
		nom_gerencia = isnull((select nom_gerencia from Gerencia where cod_gerencia=TS.cod_gerencia),''),
		TS.cod_sector,
		nom_sector = isnull((select nom_sector from Sector where cod_sector=TS.cod_sector),''),
		TS.cod_grupo,
		nom_grupo = isnull((select nom_grupo from Grupo where cod_grupo=TS.cod_grupo),''),
		TS.cod_recurso,
		nom_recurso = (select nom_recurso from Recurso where cod_recurso=TS.cod_recurso),
		convert(numeric(10,2),TS.horas_tra/6000) as asignadas_si,
		convert(numeric(10,2),TS.horas_nom) as asignadas_no,
		TS.horas_por
	from 
		#TmpHoras  TS
	where 
		TS.horas_por < @porc_min	-- upd -001- a. - Se cambia [TS.horas_por <= @porc_min] por [TS.horas_por < @porc_min]
	order by 
		TS.cod_direccion,TS.cod_gerencia,TS.cod_sector,TS.cod_grupo,TS.cod_recurso

	return (0)
go

grant execute on dbo.sp_rptHsTrabRecuEjec to GesPetUsr
go

print 'Actualizaci�n realizada.'
go