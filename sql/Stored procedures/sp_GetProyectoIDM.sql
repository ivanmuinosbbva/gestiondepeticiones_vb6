/*
-000- a. FJS 16.09.2008 - Nuevo SP para devolver todos los proyectos de IDM.
-001- a. FJS 16.06.2009 - Porque no me esta trayendo aquellos que no tienen categor�a ni clase.
-002- a. FJS 17.06.2009 - Se cambia y la funcionalidad utilizada para devolver �ltimos n�meros se pasa a otro SP.
-003- a. FJS 28.06.2010 - Se agregan los nuevos campos de estado, fecha de �ltima modificaci�n y el usuario que la realiz�.
-004- a. FJS 31.08.2010 - Se agrega el atributo Marca.
-005- a. FJS 23.11.2010 - Se agregan los nuevos atributos (pet. 24493).
-006- a. FJS 30.09.2011 - Se quita la marca.
-007- a. FJS 12.01.2016 - Se agrega el campo para empresa (codigoEmpresa).
-008- a. FJS 04.07.2016 - Nuevo: se utiliza el campo abreviatura para armar las descripciones de �rea.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDM'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDM
go

create procedure dbo.sp_GetProyectoIDM
	@ProjId			int=null, 
	@ProjSubId		int=null, 
	@ProjSubsId		int=null,
	@ProjNom		char(100)=null,
	@cod_estado		char(60)=null,
	@flg_area		char(4)=null,
	@cod_nivel		char(4)=null,
	@cod_area		char(8)=null
as 
	select
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId,
		PRJ.ProjNom,
		PRJ.ProjFchAlta,
		PRJ.ProjCatId,
		ProjCatNom = (select z.ProjCatNom from GesPet..ProyectoCategoria z where z.ProjCatId = PRJ.ProjCatId),
		PRJ.ProjClaseId,
		ProjClaseNom = (select z.ProjClaseNom from GesPet..ProyectoClase z where z.ProjCatId = PRJ.ProjCatId and z.ProjClaseId = PRJ.ProjClaseId),
		/*
		PRJ.ProjCatId,
		CAT.ProjCatNom,
		PRJ.ProjClaseId,
		CLS.ProjClaseNom,
		*/
		'nivel' = 
			case
				when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
				when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
				when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
			end,
		--{ add -003- a.
		PRJ.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado),
		PRJ.fch_estado,
		PRJ.usr_estado,
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
		--}
		,PRJ.marca	-- add -004- a.
		--{ add -005- a.
		,PRJ.plan_tyo
		,plan_TYO_nom = case
			when PRJ.plan_tyo = 'S' then 'Si'
			when PRJ.plan_tyo = 'N' then 'No'
			else ''
		end
		/* { del -007- a.
		,PRJ.empresa
		,nom_empresa = case
			when PRJ.empresa = 'B' then 'Banco'
			when PRJ.empresa = 'C' then 'BBVA Seguros'
			when PRJ.empresa = 'D' then 'RCF'
			when PRJ.empresa = 'E' then 'PSA'
			when PRJ.empresa = 'F' then 'RCF/PSA'
			else ''
		end
		} */ 
		--{ add -007- a.
		,PRJ.codigoEmpresa		as empresa
		,nom_empresa = (select emp.empabrev from Empresa emp where emp.empid = PRJ.codigoEmpresa)
		--}
		,PRJ.area
		,area_nom = case
			when PRJ.area = 'C' then 'Corporativo'
			when PRJ.area = 'R' then 'Regional'
			when PRJ.area = 'L' then 'Local'
			else ''
		end
		,PRJ.clas3
		,clas3_nom = case
			when PRJ.clas3 = '1' then '2013'
			when PRJ.clas3 = '2' then '2014'
			when PRJ.clas3 = '3' then '2015'
			when PRJ.clas3 = '4' then '2013 a 2014'
			when PRJ.clas3 = '5' then '2013 a 2015'
			when PRJ.clas3 = '6' then '2014 a 2015'
			else ''
			/*
			when PRJ.clas3 = 'C' then 'Customer Centric'
			when PRJ.clas3 = 'M' then 'Modelo de distribuci�n'
			when PRJ.clas3 = 'L' then 'LEAN'
			else ''
			*/
		end
		,PRJ.semaphore
		,PRJ.cod_direccion
		,PRJ.cod_gerencia
		,PRJ.cod_sector
		,nom_areasoli = (case
			when PRJ.cod_gerencia is null then (select ISNULL(d.abrev_direccion,d.nom_direccion) from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)
			when PRJ.cod_sector is null then (select ISNULL(d.abrev_direccion,d.nom_direccion) from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion) + ' � ' + 
				(select ISNULL(g.abrev_gerencia,g.nom_gerencia) from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)
			else
				(select ISNULL(d.abrev_direccion,d.nom_direccion) from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion) + ' � ' + 
				(select ISNULL(g.abrev_gerencia,g.nom_gerencia) from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia) + ' � ' + 
				(select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector)
		end)
		,PRJ.cod_direccion_p
		,PRJ.cod_gerencia_p
		,PRJ.cod_sector_p
		,PRJ.cod_grupo_p
		,nom_areaprop = (case
			when PRJ.cod_gerencia_p is null then RTRIM((select ISNULL(d.abrev_direccion,d.nom_direccion) from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
			when PRJ.cod_sector_p is null then 
				RTRIM((select ISNULL(d.abrev_direccion,d.nom_direccion) from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' � ' + RTRIM((select ISNULL(g.abrev_gerencia,g.nom_gerencia) from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
			when PRJ.cod_grupo_p is null then
				RTRIM((select ISNULL(d.abrev_direccion,d.nom_direccion) from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' � ' + RTRIM((select ISNULL(g.abrev_gerencia,g.nom_gerencia) from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
				' � ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
			else
				RTRIM((select ISNULL(d.abrev_direccion,d.nom_direccion) from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' � ' + RTRIM((select ISNULL(g.abrev_gerencia,g.nom_gerencia) from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
				' � ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
				' � ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
		end)
		/*
		,nivel_areaprop = (case
			when PRJ.cod_grupo_p is not null or PRJ.cod_grupo_p <> '' then 'GRUP'
			when PRJ.cod_sector_p is not null or PRJ.cod_sector_p <> '' and PRJ.cod_grupo_p is null or PRJ.cod_grupo_p = '' then 'SECT'
			when PRJ.cod_gerencia_p is not null or PRJ.cod_gerencia_p <> '' and PRJ.cod_sector_p is null or PRJ.cod_sector_p = '' then 'GERE'
			when PRJ.cod_direccion_p is not null or PRJ.cod_direccion_p <> '' and PRJ.cod_gerencia_p is null or PRJ.cod_gerencia_p = '' then 'DIRE'
			else 'Todo el BBVA'
		end)
		*/
		,PRJ.avance
		,PRJ.cod_estado2
		, nom_estado2 = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado2)
		,PRJ.fe_modif
		,PRJ.fe_inicio
		,PRJ.fe_fin
		,PRJ.fe_finreprog
		--}
		,PRJ.semaphore2																	-- Sem�foro (cambio manual)
		,PRJ.sema_fe_modif																-- Fecha de modificaci�n manual del sem�foro
		,PRJ.sema_usuario																-- Usuario que modific� manualmente el sem�foro
		--,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId)			-- Para ordenar la grilla
		,orden = (PRJ.ProjId * 10000 + PRJ.ProjSubId * 10 + PRJ.ProjSubSId)				-- Para ordenar la grilla
		,PRJ.tipo_proyecto
		,PRJ.totalhs_gerencia
		,PRJ.totalhs_sector
		,PRJ.totalreplan
		,PRJ.fe_aprob
		--,PRJ.cambio_alcance
		,cambio_alcance = (case
			when (select count(1) from GesPet..ProyectoIDMHitos z where 
			z.ProjId = PRJ.ProjId and z.ProjSubId = PRJ.ProjSubId and z.ProjSubSId = PRJ.ProjSubSId and LTRIM(RTRIM(UPPER(z.hito_nombre))) = 'CAMBIO DE ALCANCE') > 0 then 'Si'
			else 'No'
		end)
		,PRJ.codigo_gps
	from
		GesPet..ProyectoIDM PRJ
		/*
		GesPet..ProyectoIDM PRJ left join 
		GesPet..ProyectoCategoria CAT on (PRJ.ProjCatId = CAT.ProjCatId) left join		-- upd -001- a. Se cambia 'inner' por 'left'
		GesPet..ProyectoClase CLS on (PRJ.ProjCatId = CLS.ProjCatId and PRJ.ProjClaseId = CLS.ProjClaseId)
		*/
	where
		(@ProjId is null or PRJ.ProjId = @ProjId) and
		(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
		(@ProjSubsId is null or PRJ.ProjSubSId = @ProjSubsId) and
		(@ProjNom is null or LTRIM(RTRIM(PRJ.ProjNom)) like '%' + LTRIM(RTRIM(@ProjNom)) + '%') and
		((@cod_estado = '||NULL|' or PRJ.cod_estado is null) or 
		  @cod_estado is null or LTRIM(RTRIM(@cod_estado))='' or (charindex(PRJ.cod_estado,LTRIM(RTRIM(@cod_estado)))>0 and PRJ.marca='A')) and
		(@flg_area is null or @flg_area = 'PROP' and (
				(@cod_nivel = 'DIRE' and PRJ.cod_direccion_p = @cod_area) or
				(@cod_nivel = 'GERE' and PRJ.cod_gerencia_p = @cod_area) or
				(@cod_nivel = 'SECT' and PRJ.cod_sector_p = @cod_area) or
				(@cod_nivel = 'GRUP' and PRJ.cod_grupo_p = @cod_area)
			)
		)
	order by
		--PRJ.marca	desc,	-- add -004- a.	-- del -006- a.
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId
return(0)
go

grant execute on dbo.sp_GetProyectoIDM to GesPetUsr
go

print 'Actualizaci�n realizada.'
go


/*
(@flg_area = 'RESP' and @cod_area in (
			select
				pg.cod_direccion,
				pg.cod_gerencia,
				pg.cod_sector,
				pg.cod_grupo
			from 
				GesPet..PeticionGrupo pg inner join
				GesPet..Peticion p on (pg.pet_nrointerno = p.pet_nrointerno)
			where
				p.pet_projid = @ProjId and p.pet_projsubid = @ProjSubId and p.pet_projsubsid = @ProjSubsId
			group by
				pg.cod_direccion,
				pg.cod_gerencia,
				pg.cod_sector,
				pg.cod_grupo
			having
				(@cod_nivel = 'DIRE' and pg.cod_direccion = @cod_area) and
				(@cod_nivel = 'GERE' and pg.cod_gerencia = @cod_area) and
				(@cod_nivel = 'SECT' and pg.cod_sector = @cod_area) and 
				(@cod_nivel = 'GRUP' and pg.cod_grupo = @cod_area)
			)
		)
*/

/*
,nom_areasoli = (
	RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) +
	' � ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia)) + 
	' � ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector))
)
*/
--,PRJ.cod_responsable
--,nom_responsable = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.cod_responsable)
--,PRJ.cod_bpar
--,nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.cod_bpar)