/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateEstadosIDMField'
go

if exists (select * from sysobjects where name = 'sp_UpdateEstadosIDMField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEstadosIDMField
go

create procedure dbo.sp_UpdateEstadosIDMField
	@cod_estado	char(6),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if UPPER(RTRIM(@campo))='COD_ESTADO'
	update 
		GesPet.dbo.EstadosIDM
	set
		cod_estado = @valortxt
	where 
		cod_estado = @cod_estado
if UPPER(RTRIM(@campo))='NOM_ESTADO'
	update 
		GesPet.dbo.EstadosIDM
	set
		nom_estado = @valortxt
	where 
		cod_estado = @cod_estado
if UPPER(RTRIM(@campo))='FLG_RNKUP'
	update 
		GesPet.dbo.EstadosIDM
	set
		flg_rnkup = @valortxt
	where 
		cod_estado = @cod_estado
if UPPER(RTRIM(@campo))='FLG_HERDLT1'
	update 
		GesPet.dbo.EstadosIDM
	set
		flg_herdlt1 = @valortxt
	where 
		cod_estado = @cod_estado
if UPPER(RTRIM(@campo))='FLG_PETICI'
	update 
		GesPet.dbo.EstadosIDM
	set
		flg_petici = @valortxt
	where 
		cod_estado = @cod_estado
if UPPER(RTRIM(@campo))='FLG_SECGRU'
	update 
		GesPet.dbo.EstadosIDM
	set
		flg_secgru = @valortxt
	where 
		cod_estado = @cod_estado
go

grant execute on dbo.sp_UpdateEstadosIDMField to GesPetUsr
go

print 'Actualización realizada.'
go
