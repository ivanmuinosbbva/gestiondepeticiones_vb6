use GesPet
go
print 'sp_GetPeticionImport'
go
if exists (select * from sysobjects where name = 'sp_GetPeticionImport' and sysstat & 7 = 4)
drop procedure dbo.sp_GetPeticionImport
go
create procedure dbo.sp_GetPeticionImport 
             @fec_import     smalldatetime
as 
 
/* pertenece a la rama solicitante */ 
         select PET.pet_nrointerno, 
            PET.pet_nroasignado, 
            PET.titulo,  
            PET.prioridad,   
            PET.fe_ini_plan, 
            PET.fe_fin_plan,             
            PET.horaspresup,             
            PET.cod_usualta,             
            PET.cod_solicitante,         
            PET.cod_estado,          
            nom_estado=(select EST.nom_estado from GesPet..Estados EST where RTRIM(PET.cod_estado)=RTRIM(EST.cod_estado)),          
            PET.cod_situacion, 
            nom_situacion=(select SIT.nom_situacion from GesPet..Situaciones SIT where RTRIM(PET.cod_situacion)=RTRIM(SIT.cod_situacion)),          
            cod_area_hij='', 
            nom_area_hij='', 
            cod_estado_hij='', 
            nom_estado_hij='', 
            cod_situacion_hij='', 
            nom_situacion_hij='',
            fe_ini_plan_hij=null,
            fe_fin_plan_hij=null,
            horaspresup_hij=0             
        from  GesPet..Historial HIS, GesPet..Peticion PET
	where 	HIS.cod_evento='PIMPORT' and
		convert(char(8),HIS.hst_fecha,112) = convert(char(8),@fec_import,112) and 
		PET.pet_nrointerno=HIS.pet_nrointerno
        order by PET.pet_nroasignado,PET.pet_nrointerno 
return(0) 
go



grant execute on dbo.sp_GetPeticionImport to GesPetUsr 
go


