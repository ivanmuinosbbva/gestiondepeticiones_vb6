/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.11.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMHisto'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMHisto' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMHisto
go

create procedure dbo.sp_UpdateProyectoIDMHisto
	@ProjId	int,
	@ProjSubId	int,
	@ProjSubSId	int,
	@hst_nrointerno	int,
	@hst_fecha	smalldatetime,
	@cod_evento	char(8),
	@proj_estado	char(6),
	@replc_user	char(10),
	@audit_user	char(10)
as
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		hst_fecha = @hst_fecha,
		cod_evento = @cod_evento,
		proj_estado = @proj_estado,
		replc_user = @replc_user,
		audit_user = @audit_user
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
go

grant execute on dbo.sp_UpdateProyectoIDMHisto to GesPetUsr
go

print 'Actualización realizada.'
go
