use GesPet
go
print 'sp_InsertProyectoBalanceDet'
go
if exists (select * from sysobjects where name = 'sp_InsertProyectoBalanceDet' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertProyectoBalanceDet
go
create procedure dbo.sp_InsertProyectoBalanceDet
	@prj_nrointerno		int,
	@cod_BalanceRubro	char(8),
	@cod_BalanceSubRubro	char(8),
	@bde_texto		char(50),
	@bde_moneda		char(3),
	@xbde_estimado		char(15),
	@xbde_real		char(15)

as

declare @secuencia int
select @secuencia  = 0 

declare	@bde_estimado	numeric(12,2)
declare	@bde_real	numeric(12,2)

select @bde_estimado=convert(numeric(12,2),@xbde_estimado)
select @bde_real=convert(numeric(12,2),@xbde_real)

if not exists (select prj_nrointerno from Proyecto where prj_nrointerno=@prj_nrointerno)
	begin 
		raiserror 30011 'Proyecto inexistente'   
		select 30011 as ErrCode , 'Proyecto inexistente' as ErrDesc   
		return (30011)   
	end 

if not exists (select cod_BalanceSubRubro from BalanceSubRubro where RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and RTRIM(cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro))
	begin 
		raiserror 30011 'Subrubro inexistente'   
		select 30011 as ErrCode , 'Subrubro inexistente' as ErrDesc   
		return (30011)   
	end 

if not exists (select prj_nrointerno from ProyectoBalanceCab where prj_nrointerno=@prj_nrointerno)
	begin 
		raiserror 30011 'Cabezal balance no creado'   
		select 30011 as ErrCode , 'Cabezal balance no creado' as ErrDesc   
		return (30011)   
	end 


select @secuencia = MAX(secuencia) from ProyectoBalanceDet where
		RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and
		RTRIM(cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro) and
		prj_nrointerno=@prj_nrointerno

if @secuencia is null
select @secuencia = 0

select @secuencia = @secuencia + 1


insert into ProyectoBalanceDet
	(prj_nrointerno, 
	cod_BalanceRubro, 
	cod_BalanceSubRubro, 
	secuencia,
	bde_texto, 
	bde_moneda, 
	bde_estimado,
	bde_real,
	audit_user, 
	audit_date)
values
	(@prj_nrointerno,
	@cod_BalanceRubro,
	@cod_BalanceSubRubro,
	@secuencia,
	@bde_texto, 
	@bde_moneda,
	@bde_estimado,
	@bde_real,
	SUSER_NAME(), 
	getdate())

return(0)
go

grant execute on dbo.sp_InsertProyectoBalanceDet to GesPetUsr
go
