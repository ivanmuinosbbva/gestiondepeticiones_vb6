/*
-000- a. FJS 15.06.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_HorasTrabajadas'
go

if exists (select * from sysobjects where name = 'sp_IGM_HorasTrabajadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_HorasTrabajadas
go

create procedure dbo.sp_IGM_HorasTrabajadas
	@cod_recurso	char(10)=null,
	@cod_direccion	char(8)=null,
	@cod_gerencia	char(8)=null,
	@cod_sector		char(8)=null,
	@cod_grupo		char(8)=null
as 
	select 
		r.cod_direccion		as 'rec_direccion',
		r.cod_gerencia		as 'rec_gerencia',
		r.cod_sector		as 'rec_sector',
		r.cod_grupo			as 'rec_grupo',
		h.cod_recurso		as 'recurso',
		h.cod_tarea			as 'tarea',
		t.nom_tarea			as 'tarea_descripcion',
		h.pet_nrointerno	as 'pet_interno',
		p.pet_nroasignado	as 'pet_numero',
		p.titulo			as 'pet_titulo',
		p.cod_direccion		as 'pet_direccion',
		p.cod_gerencia		as 'pet_gerencia',
		p.cod_sector		as 'pet_sector',
		h.fe_desde			as 'desde',
		h.fe_hasta			as 'hasta',
		h.horas				as 'minutos'
	from 
		GesPet..HorasTrabajadas h left join
		GesPet..Recurso r on (h.cod_recurso = r.cod_recurso) left join
		GesPet..Peticion p on (h.pet_nrointerno = p.pet_nrointerno) left join
		GesPet..Tarea t on (h.cod_tarea = t.cod_tarea)
	where 
		(@cod_recurso is null or h.cod_recurso = @cod_recurso) and
		(@cod_direccion is null or r.cod_direccion = @cod_direccion) and 
		(@cod_gerencia is null or r.cod_gerencia = @cod_gerencia) and 
		(@cod_sector is null or r.cod_sector = @cod_sector) and 
		(@cod_grupo is null or r.cod_grupo = @cod_grupo)
	order by
		fe_desde,
		fe_hasta

	return(0)
go

grant execute on dbo.sp_IGM_HorasTrabajadas to GesPetUsr
go

grant execute on dbo.sp_IGM_HorasTrabajadas to GrpTrnIGM
go

print 'Actualización realizada.'
go