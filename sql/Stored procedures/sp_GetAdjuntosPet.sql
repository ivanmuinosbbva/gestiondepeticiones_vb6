/*
-001- a. FJS 18.07.2008 - Agregado de una columna que devuelve el tama�o en bytes del documento adjunto
-002- a. FJS 21.07.2008 - Se agrega el sector y el grupo que adjunta el documento.
-003- a. FJS 08.01.2009 - Se optimiza para aprovechar el �ndice 'idx_PeticionAdjunto'
-004- a. FJS 14.04.2010 - Se optimiza la consulta.
-005- a. FJS 14.10.2010 - Indica si la extensi�n del archivo pertenece a alguna de las extensiones conocidas de archivos comprimidos (arc|arj|gz|gzip|sea|tar|tar.gz|tar.Z|tgz|Z|zip|ace|rar|).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetAdjuntosPet'
go

if exists (select * from sysobjects where name = 'sp_GetAdjuntosPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAdjuntosPet
go

create procedure dbo.sp_GetAdjuntosPet
	@pet_nrointerno		int=null,
	@adj_tipo			char(8)=null,
	@cod_sector			char(8)=null,
	@cod_grupo			char(8)=null
as
	select
		a.pet_nrointerno,
		a.adj_file,
		a.adj_tipo,
		a.adj_texto,
		a.audit_user,
		nom_audit = isnull((select Ra.nom_recurso from Recurso Ra where RTRIM(Ra.cod_recurso)=RTRIM(a.audit_user)),''),
		a.audit_date,
		--datalength(adj_objeto)/1024 as adj_size,
		datalength(adj_objeto)	as adj_size,
		a.cod_sector,
		nom_sector = isnull((select s.nom_sector from GesPet..Sector s where s.cod_sector = a.cod_sector),''),
		a.cod_grupo,
		nom_grupo = isnull((select b.nom_grupo from GesPet..Grupo b where b.cod_sector = a.cod_sector and b.cod_grupo = a.cod_grupo),'') /*,
		a.adj_unc,
		a.adj_filename,
		a.adj_filesystem,
		a.adj_CRC32 */
	from 
		GesPet..PeticionAdjunto a
	where	
		a.pet_nrointerno = @pet_nrointerno and
		(@adj_tipo is null or RTRIM(adj_tipo) = RTRIM(@adj_tipo)) and 
		(@cod_sector is null or RTRIM(a.cod_sector) = LTRIM(RTRIM(@cod_sector))) and 
		(@cod_grupo is null or RTRIM(a.cod_grupo) = LTRIM(RTRIM(@cod_grupo)))
	order by 
		a.pet_nrointerno, 
		a.adj_tipo, 
		a.adj_file
	return(0)
go

grant execute on dbo.sp_GetAdjuntosPet to GesPetUsr
go

print 'Actualizaci�n realizada.'
go