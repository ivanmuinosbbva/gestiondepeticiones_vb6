use GesPet
go
print 'sp_GetUnaPeticionAsigSinProy'
go
if exists (select * from sysobjects where name = 'sp_GetUnaPeticionAsigSinProy' and sysstat & 7 = 4)
drop procedure dbo.sp_GetUnaPeticionAsigSinProy
go
create procedure dbo.sp_GetUnaPeticionAsigSinProy
	@pet_nroasignado	int
as
select PET.pet_nrointerno,
	PET.pet_nroasignado,
	PET.titulo,
	PET.cod_tipo_peticion,
	PET.prioridad,
	PET.pet_nroanexada,
	PET.fe_pedido,
	PET.fe_requerida,
	PET.fe_comite,
	PET.fe_ini_plan,
	PET.fe_fin_plan,
	PET.fe_ini_real,
	PET.fe_fin_real,
	PET.horaspresup,
	PET.cod_direccion,
	PET.cod_gerencia,
	PET.cod_sector,
	PET.cod_usualta,
	PET.cod_solicitante,
        PET.cod_bpar,         
	PET.cod_referente,
	PET.cod_supervisor,
	PET.cod_director,
	PET.cod_estado,
	nom_estado = (select EST.nom_estado from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),
	PET.fe_estado,
	PET.cod_situacion,
	nom_situacion = (select SIT.nom_situacion from Situaciones SIT where RTRIM(SIT.cod_situacion) = RTRIM(PET.cod_situacion)),
	cod_area_hij='',
	nom_area_hij='',
	cod_estado_hij='',
	nom_estado_hij='',
	cod_situacion_hij='',
	nom_situacion_hij=''
from	GesPet..Peticion PET
where	PET.pet_nroasignado = @pet_nroasignado and (prj_nrointerno=0 or prj_nrointerno is null)
return(0)
go



grant execute on dbo.sp_GetUnaPeticionAsigSinProy to GesPetUsr
go
