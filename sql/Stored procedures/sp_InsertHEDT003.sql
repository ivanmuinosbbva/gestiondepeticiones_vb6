/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - Se crea el SP para manejo de catalogo de archivos de enmascaramiento.
-001- a. FJS 22.10.2009 - Se modifica la longitud del campo cpo_id de 8 a 18 char.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHEDT003'
go

if exists (select * from sysobjects where name = 'sp_InsertHEDT003' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHEDT003
go

create procedure dbo.sp_InsertHEDT003
	@dsn_id			char(8)=null,
	@cpy_id			char(8)=null,
	@cpo_id			char(18)=null,
	@cpo_nrobyte	char(4)=null,
	@cpo_canbyte	char(4)=null,
	@cpo_type		char(1)=null,
	@cpo_decimals	char(4)=null,
	@cpo_dsc		char(20)=null,
	@cpo_nomrut		char(8)=null
as
	declare @fe_hoy		smalldatetime
	declare @fe_hoytxt	char(8)
	declare @user_id	char(10)

	select @fe_hoy  = getdate()
	select @user_id = SUSER_NAME()
	select @fe_hoytxt = convert(char(8), @fe_hoy, 112)

	-- Para formatear los n�meros
	declare @fmt_cpo_nrobyte	char(4)
	declare @fmt_cpo_canbyte	char(4)
	declare @fmt_cpo_decimals	char(4)
	
	-- @cpo_nrobyte
	select @fmt_cpo_nrobyte = 
		case 
			when datalength(rtrim(@cpo_nrobyte)) = 1 then '000' + rtrim(@cpo_nrobyte)
			when datalength(rtrim(@cpo_nrobyte)) = 2 then '00' + rtrim(@cpo_nrobyte) 
			when datalength(rtrim(@cpo_nrobyte)) = 3 then '0' + rtrim(@cpo_nrobyte)
			when datalength(rtrim(@cpo_nrobyte)) = 4 then rtrim(@cpo_nrobyte)
		else
			rtrim(@cpo_nrobyte)
		end
	-- @cpo_canbyte
	select @fmt_cpo_canbyte = 
		case 
			when datalength(rtrim(@cpo_canbyte)) = 1 then '000' + rtrim(@cpo_canbyte)
			when datalength(rtrim(@cpo_canbyte)) = 2 then '00' + rtrim(@cpo_canbyte)
			when datalength(rtrim(@cpo_canbyte)) = 3 then '0' + rtrim(@cpo_canbyte)
			when datalength(rtrim(@cpo_canbyte)) = 4 then rtrim(@cpo_canbyte)
		else
			rtrim(@cpo_canbyte)
		end
	-- @cpo_decimals
	select @fmt_cpo_decimals = 
		case 
			when datalength(rtrim(@cpo_decimals)) = 1 then '000' + rtrim(@cpo_decimals)
			when datalength(rtrim(@cpo_decimals)) = 2 then '00' + rtrim(@cpo_decimals)
			when datalength(rtrim(@cpo_decimals)) = 3 then '0' + rtrim(@cpo_decimals)
			when datalength(rtrim(@cpo_decimals)) = 4 then rtrim(@cpo_decimals)
		else
			rtrim(@cpo_decimals)
		end

	if exists (select x.cpo_id from GesPet..HEDT003 x where RTRIM(x.dsn_id) = RTRIM(@dsn_id) and RTRIM(x.cpy_id) = RTRIM(@cpy_id) and RTRIM(x.cpo_id) = RTRIM(@cpo_id))
		begin 
			raiserror 30011 'El c�digo del campo para el archivo ya existe. Revise.'
			select 30011 as ErrCode , 'El c�digo del campo para el archivo ya existe. Revise.' as ErrDesc
			return (30011)
		end
	else
		begin
			insert into GesPet.dbo.HEDT003 (
				dsn_id,
				cpy_id,
				cpo_id,
				cpo_nrobyte,
				cpo_canbyte,
				cpo_type,
				cpo_decimals,
				cpo_dsc,
				cpo_nomrut,
				cpo_feult,
				cpo_userid,
				estado,
				fe_hst_estado)
			values (
				@dsn_id,
				@cpy_id,
				@cpo_id,
				@cpo_nrobyte,
				@cpo_canbyte,
				@cpo_type,
				@cpo_decimals,
				@cpo_dsc,
				@cpo_nomrut,
				@fe_hoy,
				@user_id,
				'A',
				null)
			/*
			-- Guardo en la tabla de transmisi�n
			execute sp_InsertHEDTHST 'HEDT003', @dsn_id, @cpy_id, @cpo_id, null, null, null, null, null, null, null, null, null, null,
												null, null, null, null, 
												@fmt_cpo_nrobyte, @fmt_cpo_canbyte, @cpo_type, @fmt_cpo_decimals, @cpo_dsc, @cpo_nomrut, @fe_hoytxt, @user_id
			*/
			return(0)
		end
go

grant execute on dbo.sp_InsertHEDT003 to GesPetUsr
go

print 'Actualizaci�n realizada.'
