use GesPet
go
print 'sp_GetAgrupPeticDup'
go
if exists (select * from sysobjects where name = 'sp_GetAgrupPeticDup' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrupPeticDup
go
create procedure dbo.sp_GetAgrupPeticDup 
             @agr_nrointerno   int=0,
             @pet_nrointerno   int=0
as 

declare @secuencia int
declare @ret_status int

declare @agr_nrointernoX int
declare @agr_nrointernoY int
declare @agr_nropadreX int


select @agr_nrointernoY=@agr_nrointerno
select @agr_nropadreX=agr_nropadre,@agr_nrointernoX=agr_nrointerno from Agrup where @agr_nrointernoY=agr_nrointerno
while ((@agr_nropadreX is not null) and (@agr_nropadreX > 0))
begin
	select @agr_nrointernoY=@agr_nropadreX
	select @agr_nropadreX=agr_nropadre,@agr_nrointernoX=agr_nrointerno from Agrup where @agr_nrointernoY=agr_nrointerno
end

select @secuencia=0

/*    Tabla Temporaria  */
create table #AgrupArbol(
	secuencia	int,
	nivel 		int,
	agr_nrointerno 	int)

execute @ret_status = sp_GetAgrupArbol @agr_nrointernoX, 0, @secuencia OUTPUT
if (@ret_status <> 0)
begin
	if (@ret_status = 30003)
		select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
	return (@ret_status)
end
    select  
	AP.agr_nrointerno,
	AP.pet_nrointerno,
	AG.agr_titulo
    from 
        AgrupPetic AP, Agrup AG, #AgrupArbol AXP
    where   
	(AXP.agr_nrointerno=AP.agr_nrointerno) and
	(@pet_nrointerno=AP.pet_nrointerno) and
	(AG.agr_nrointerno=AP.agr_nrointerno)
return(0) 
go



grant execute on dbo.sp_GetAgrupPeticDup to GesPetUsr 
go
