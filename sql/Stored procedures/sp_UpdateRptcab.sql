/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateRptcab'
go

if exists (select * from sysobjects where name = 'sp_UpdateRptcab' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRptcab
go

create procedure dbo.sp_UpdateRptcab
	@rptid	char(10),
	@rptnom	char(50),
	@rpthab	char(1),
	@rptfch	smalldatetime
as
	update 
		GesPet.dbo.Rptcab
	set
		rptnom = @rptnom,
		rpthab = @rpthab,
		rptfch = @rptfch
	where 
		rptid = @rptid
go

grant execute on dbo.sp_UpdateRptcab to GesPetUsr
go

print 'Actualización realizada.'
go
