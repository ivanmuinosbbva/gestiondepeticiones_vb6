/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertMotivosReplan'
go

if exists (select * from sysobjects where name = 'sp_InsertMotivosReplan' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertMotivosReplan
go

create procedure dbo.sp_InsertMotivosReplan
	@cod_motivo_replan	int,
	@nom_motivo_replan	char(50),
	@estado_hab			char(1)='S'
as
	insert into GesPet.dbo.MotivosReplan (
		cod_motivo_replan,
		nom_motivo_replan,
		estado_hab)
	values (
		@cod_motivo_replan,
		@nom_motivo_replan,
		@estado_hab)
	return(0)
go

grant execute on dbo.sp_InsertMotivosReplan to GesPetUsr
go

print 'Actualización realizada.'
