/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'Actualizando procedimiento almacenado: sp_rptHsTrabAreaSoli'
go

if exists (select * from sysobjects where name = 'sp_rptHsTrabAreaSoli' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHsTrabAreaSoli
go

create procedure dbo.sp_rptHsTrabAreaSoli
	@fdesde		varchar(8)='NULL',
	@fhasta		varchar(8)='NULL',
	@nivSoli    varchar(4)='NULL',
	@areSoli    varchar(8)='NULL',
	@nivEjec    varchar(4)='NULL',
	@areEjec    varchar(8)='NULL',
	@detalle	varchar(1)='0'
as
	if @fdesde is null
		select @fdesde ='NULL'
	if @fhasta is null
		select @fhasta ='NULL'
	if @nivSoli is null
		select @nivSoli = 'NULL'
	if @areSoli is null
		select @areSoli = 'NULL'
	if @nivEjec is null
		select @nivEjec = 'NULL'
	if @areEjec is null
		select @areEjec = 'NULL'
	if @detalle	is null
		select @detalle = '0'

	declare @cod_direccion		varchar (10)
	declare @cod_gerencia		varchar (10)
	declare @cod_sector			varchar (10)
	declare @pet_nrointerno		int
	declare @pet_nroasignado	int
	declare @titulo				varchar(50)
	declare @fe_desde			smalldatetime
	declare @fe_hasta			smalldatetime
	declare @horas				int
	declare @trabsinasignar		char (1)
	declare @sol_desde			smalldatetime
	declare @sol_hasta			smalldatetime
	declare @ret_dias			int
	declare @ret_horas			int
	declare @horas_si			numeric(10,2)
	declare @horas_no			numeric(10,2)
	declare @xhoras_si			numeric(10,2)
	declare @xhoras_no			numeric(10,2)
	declare @ret_desde			smalldatetime
	declare @ret_hasta			smalldatetime
	declare @tip_asig			varchar(3)
	declare @cod_asig			varchar(10)
	declare @cod_real			varchar(10)
	declare @nom_asig			varchar(50)

	set dateformat ymd

	if RTRIM(@fdesde)='NULL'
	begin
		select @sol_desde=dateadd(mm,-1,getdate())
		select @fdesde=convert(char(8),@sol_desde,112)
	end
	else
		select @sol_desde=@fdesde
	if RTRIM(@fhasta)='NULL'
	begin
		select @sol_hasta=dateadd(mm,1,getdate())
		select @fhasta=convert(char(8),@sol_hasta,112)
	end
	else
		select @sol_hasta=@fhasta

		CREATE TABLE #TmpRecurso(
			cod_recurso			varchar(10)		null,
			nom_recurso			varchar(50)		null,
			cod_direccion		varchar(10)		null, 
			cod_gerencia		varchar(10)		null, 
			cod_sector			varchar(10)		null, 
			cod_grupo			varchar(10)		null)

		insert into #TmpRecurso
			select 
				cod_recurso, 
				nom_recurso,
				cod_direccion, 
				cod_gerencia, 
				cod_sector, 
				cod_grupo
			from GesPet..Recurso

		insert into #TmpRecurso
			select 
				cod_fab, 
				nom_fab,
				cod_direccion, 
				cod_gerencia, 
				cod_sector, 
				cod_grupo
			from GesPet..Fabrica


	CREATE TABLE #TmpSalida(
		cod_direccion	varchar(10) null,
		cod_gerencia	varchar(10) null,
		cod_sector		varchar(10) null,
		nom_direccion	varchar(80) null,
		nom_gerencia	varchar(80) null,
		nom_sector		varchar(80) null,
		cod_asig		varchar(10) null,
		cod_real		varchar(10) null,
		nom_asig		varchar(50) null,
		entre_desde		smalldatetime,
		entre_hasta		smalldatetime,
		horas_si		numeric(10,2),
		horas_no		numeric(10,2)
	)

	DECLARE CursHoras CURSOR FOR
		select
		Pt.cod_direccion,
		Pt.cod_gerencia,
		Pt.cod_sector,
		Ht.pet_nrointerno,
		Pt.pet_nroasignado,
		Pt.titulo,
		Ht.trabsinasignar,
		Ht.fe_desde,
		Ht.fe_hasta,
		Ht.horas
		from GesPet..Peticion Pt,
			GesPet..HorasTrabajadas Ht, #TmpRecurso Re 
		where
			((@nivSoli is null or RTRIM(@nivSoli)='NULL') or
			(@nivSoli='DIRE' and RTRIM(Pt.cod_direccion)=RTRIM(@areSoli)) or
			(@nivSoli='GERE' and RTRIM(Pt.cod_gerencia)=RTRIM(@areSoli)) or
			(@nivSoli='SECT' and RTRIM(Pt.cod_sector)=RTRIM(@areSoli))) and
			(Ht.pet_nrointerno = Pt.pet_nrointerno) and
			(RTRIM(Ht.cod_recurso) = RTRIM(Re.cod_recurso)) and
			((@nivEjec is null or RTRIM(@nivEjec)='NULL') or
			(@nivEjec='DIRE' and RTRIM(Re.cod_direccion)=RTRIM(@areEjec)) or
			(@nivEjec='GERE' and RTRIM(Re.cod_gerencia)=RTRIM(@areEjec)) or
			(@nivEjec='SECT' and RTRIM(Re.cod_sector)=RTRIM(@areEjec)) or
			(@nivEjec='GRUP' and RTRIM(Re.cod_grupo)=RTRIM(@areEjec))) and
			(RTRIM(@fdesde)='NULL' or convert(char(8),Ht.fe_hasta,112) >= @fdesde) and
			(RTRIM(@fhasta)='NULL' or convert(char(8),Ht.fe_desde,112) <= @fhasta)
		order by 
			Pt.cod_direccion,Pt.cod_gerencia,Pt.cod_sector,Pt.pet_nrointerno
		for read only

		OPEN CursHoras
		FETCH CursHoras INTO
			@cod_direccion,
			@cod_gerencia,
			@cod_sector,
			@pet_nrointerno,
			@pet_nroasignado,
			@titulo,
			@trabsinasignar,
			@fe_desde,
			@fe_hasta,
			@horas

		WHILE (@@sqlstatus = 0)
		BEGIN
	/*  print @cod_sector */

		execute sp_GetHorasPeriodo @sol_desde,
			@sol_hasta,
			@fe_desde,
			@fe_hasta,
			@horas,
			@ret_desde OUTPUT,
			@ret_hasta OUTPUT,
			@ret_dias OUTPUT,
			@ret_horas OUTPUT

		if RTRIM(@trabsinasignar)='S'
		begin
			select @horas_si = 0
			select @horas_no = @ret_horas
		end
		else
		begin
			select @horas_si = @ret_horas
			select @horas_no = 0
		end

		select @cod_asig=convert(varchar(10),@pet_nroasignado)
		select @cod_real=convert(varchar(10),@pet_nrointerno)
		select @nom_asig=@titulo

	/* debug
		SELECT  @ret_desde,
			@ret_hasta,
			@ret_dias,
			@ret_horas,
			@horas_si,
			@horas_no
	*/
		insert #TmpSalida
			(cod_direccion,
			cod_gerencia,
			cod_sector,
			cod_asig,
			cod_real,
			nom_asig,
			entre_desde,
			entre_hasta,
			horas_si,
			horas_no)
		values
			(@cod_direccion,
			@cod_gerencia,
			@cod_sector,
			@cod_asig,
			@cod_real,
			@nom_asig,
			@ret_desde,
			@ret_hasta,
			@horas_si,
			@horas_no)

		FETCH CursHoras INTO
			@cod_direccion,
			@cod_gerencia,
			@cod_sector,
			@pet_nrointerno,
			@pet_nroasignado,
			@titulo,
			@trabsinasignar,
			@fe_desde,
			@fe_hasta,
			@horas
		END
		CLOSE CursHoras
		DEALLOCATE CURSOR CursHoras


	if @detalle='0'
		select  TS.cod_direccion,
		nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=TS.cod_direccion),
		TS.cod_gerencia,
		nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where Ge.cod_gerencia=TS.cod_gerencia),
		TS.cod_sector,
		nom_sector = (select Se.nom_sector from Sector Se where Se.cod_sector=TS.cod_sector),
		TS.cod_real,
		TS.cod_asig,
		TS.nom_asig,
		TS.entre_desde as desde,
		TS.entre_hasta as hasta,
		convert(numeric(10,2),horas_si/6000) as asignadas_si,
		convert(numeric(10,2),horas_no/6000) as asignadas_no
		from #TmpSalida TS
		order by TS.cod_direccion,TS.cod_gerencia,TS.cod_sector,TS.cod_real,desde
	if @detalle='1'
		select  distinct
		TS.cod_direccion,
		nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=TS.cod_direccion),
		TS.cod_gerencia,
		nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where Ge.cod_gerencia=TS.cod_gerencia),
		TS.cod_sector,
		nom_sector = (select Se.nom_sector from Sector Se where Se.cod_sector=TS.cod_sector),
		TS.cod_real,
		TS.cod_asig,
		TS.nom_asig,
		min(TS.entre_desde) as desde,
		max(TS.entre_hasta) as hasta,
		convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
		convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no
		from #TmpSalida TS
		group by TS.cod_direccion,TS.cod_gerencia,TS.cod_sector,TS.cod_real,TS.cod_asig,TS.nom_asig
		order by TS.cod_direccion,TS.cod_gerencia,TS.cod_sector,TS.cod_real,TS.cod_asig,TS.nom_asig
	if @detalle='2'
		select  distinct
		TS.cod_direccion,
		nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=TS.cod_direccion),
		TS.cod_gerencia,
		nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where Ge.cod_gerencia=TS.cod_gerencia),
		'' as cod_sector,
		'' as nom_sector,
		TS.cod_real,
		TS.cod_asig,
		TS.nom_asig,
		min(TS.entre_desde) as desde,
		max(TS.entre_hasta) as hasta,
		convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
		convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no
		from #TmpSalida TS
		group by TS.cod_direccion,TS.cod_gerencia,TS.cod_real,TS.cod_asig,TS.nom_asig
		order by TS.cod_direccion,TS.cod_gerencia,TS.cod_real,TS.cod_asig,TS.nom_asig
	if @detalle='3'
		select  distinct
		TS.cod_direccion,
		nom_direccion = (select Di.nom_direccion from Direccion Di where Di.cod_direccion=TS.cod_direccion),
		'' as cod_gerencia,
		'' as nom_gerencia,
		'' as cod_sector,
		'' as nom_sector,
		TS.cod_real,
		TS.cod_asig,
		TS.nom_asig,
		min(TS.entre_desde) as desde,
		max(TS.entre_hasta) as hasta,
		convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
		convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no
		from #TmpSalida TS
		group by TS.cod_direccion,TS.cod_real,TS.cod_asig,TS.nom_asig
		order by TS.cod_direccion,TS.cod_real,TS.cod_asig,TS.nom_asig
	return(0)
go

grant execute on dbo.sp_rptHsTrabAreaSoli to GesPetUsr
go

print 'Actualización finalizada.'
go