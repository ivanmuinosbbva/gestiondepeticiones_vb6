/*
-001- a. FJS 20.04.2015 - Nuevo: se agrega un nuevo atributo para sectores, que indica si el mismo es v�lido para usar como sector solicitante en peticiones especiales.
-002- a. FJS 28.01.2016 - Nuevo: se agrega un nuevo atributo para indicar cual es el perfil para la recepci�n de las peticiones de este grupo.

*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSector'
go

if exists (select * from sysobjects where name = 'sp_GetSector' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSector
go

create procedure dbo.sp_GetSector 
	@cod_sector		char(8)=null, 
	@cod_gerencia	char(8)=null, 
	@flg_habil		char(1)=null 
as 
	begin 
		select 
			cod_sector, 
			nom_sector, 
			cod_gerencia, 
			cod_abrev, 
			flg_habil, 
			es_ejecutor, 
			cod_bpar, 
			soli_hab,
			cod_perfil			-- add -002- a.
		from 
			GesPet..Sector 
		where 
			(@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) and 
			(@cod_gerencia is null or RTRIM(@cod_gerencia) is null or RTRIM(@cod_gerencia)='' or RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)) and 
			(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil)) 
		order by 
			nom_sector ASC 
	end
	return(0) 
go

grant execute on dbo.sp_GetSector to GesPetUsr
go

print 'Actualizaci�n realizada.'
go