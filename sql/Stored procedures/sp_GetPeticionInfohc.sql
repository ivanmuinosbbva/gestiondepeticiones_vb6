/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionInfohc'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionInfohc' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionInfohc
go

create procedure dbo.sp_GetPeticionInfohc
	@pet_nrointerno	int=null,
	@info_id		int=null,
	@info_idver		int=null,
	@info_tipo		char(1)=null,
	@info_estado	varchar(80)=null
as
	select 
		p.pet_nrointerno,
		p.info_id,
		p.info_idver,
		p.info_tipo,
		info_tipodsc = case
			when p.info_tipo = 'C' then 'Completo'
			when p.info_tipo = 'R' then 'Reducido'
		end,
		info_punto,
		tipo_dsc = '',
		p.info_fecha,
		p.info_recurso,
		nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.info_recurso),
		p.info_estado,
		p.infoprot_id,
		c.infoprot_nom,
		c.infoprot_punto,
		evaluacion = (
			select MAX(d.info_valor)
			from PeticionInfohd d
			where 
				d.pet_nrointerno = p.pet_nrointerno and 
				d.info_id = p.info_id and
				d.info_idver = p.info_idver and 
				d.info_valor is not null),
		pendientes = (
			select count(1)
			from 
				PeticionInfohc c inner join 
				PeticionInfohd d on (c.pet_nrointerno = d.pet_nrointerno and c.info_id = d.info_id and c.info_idver = d.info_idver) inner join
				Infoprod ip on (c.infoprot_id = ip.infoprot_id and ip.infoprot_item = d.info_item)
			where 
				d.pet_nrointerno = p.pet_nrointerno and 
				d.info_id = p.info_id and
				d.info_idver = p.info_idver and 
				(ip.infoprot_req = 'S' and (ip.infoprot_req = 'N'))
		)
	from 
		GesPet..PeticionInfohc p inner join
		GesPet..Infoproc c on (p.infoprot_id = c.infoprot_id)
	where
		(p.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(p.info_id = @info_id or @info_id is null) and
		(p.info_idver = @info_idver or @info_idver is null) and 
		(p.info_tipo = @info_tipo or @info_tipo is null) and
		(@info_estado is null or charindex(p.info_estado,LTRIM(RTRIM(@info_estado)))>0)
	order by
		p.pet_nrointerno,
		p.info_id		desc,
		p.info_idver
go

grant execute on dbo.sp_GetPeticionInfohc to GesPetUsr
go

print 'Actualización realizada.'
go
