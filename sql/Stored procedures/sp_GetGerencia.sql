/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetGerencia'
go

if exists (select * from sysobjects where name = 'sp_GetGerencia' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetGerencia
go
create procedure dbo.sp_GetGerencia 
	@cod_gerencia	char(8)=null, 
	@cod_direccion	char(8)=null, 
	@flg_habil		char(1)=null 
as 
	begin 
		select 
			cod_gerencia, 
			nom_gerencia, 
			cod_direccion, 
			flg_habil, 
			es_ejecutor, 
			IGM_hab,
			abrev_gerencia 
		from 
			GesPet..Gerencia 
		where 
			(@cod_gerencia is null or RTRIM(@cod_gerencia) is null or RTRIM(@cod_gerencia)='' or RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)) and 
			(@cod_direccion is null or RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)='' or RTRIM (cod_direccion) = RTRIM(@cod_direccion)) and 
			(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil)) 
		order by 
			nom_gerencia ASC 
	end 
	return(0) 
go

grant execute on dbo.sp_GetGerencia to GesPetUsr
go

sp_procxmode 'sp_GetGerencia', 'anymode'
go

print 'Actualización realizada.'
go
