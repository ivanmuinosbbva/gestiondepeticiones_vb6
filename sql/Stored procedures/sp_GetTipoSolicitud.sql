/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.07.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetTipoSolicitud' 
go

if exists (select * from sysobjects where name = 'sp_GetTipoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetTipoSolicitud
go

create procedure dbo.sp_GetTipoSolicitud
	@cod_tipo	char(1)=null
as
	select 
		p.cod_tipo,
		p.nom_tipo,
		p.dsc_tipo
	from 
		GesPet..TipoSolicitud p
	where
		@cod_tipo is null or p.cod_tipo = @cod_tipo
	return(0)
go

grant execute on dbo.sp_GetTipoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
