use GesPet
go
print 'sp_DelMensajePerfil'
go
if exists (select * from sysobjects where name = 'sp_DelMensajePerfil' and sysstat & 7 = 4)
drop procedure dbo.sp_DelMensajePerfil
go
create procedure dbo.sp_DelMensajePerfil 
	@pet_nrointerno     int, 
	@cod_perfil char(4),
	@cod_nivel char(4),
	@cod_area char(8)

as 
set dateformat ymd 

/* 
Borra todos los mensajes de la petici�n para el perfil de un determinado 
nivel y area (Ej.: responsables de grupo de un grupo en particular)
*/

delete Mensajes
where (pet_nrointerno=@pet_nrointerno) and
    (RTRIM(cod_perfil)=RTRIM(@cod_perfil)) and 
    (RTRIM(cod_nivel)=RTRIM(@cod_nivel)) and 
    (RTRIM(cod_area)=RTRIM(@cod_area)) 

/*
Si el c�digo de perfil es 'HSEC' entonces:
    Elimina para la petici�n todos los mensajes para el nivel de Grupo para el sector pasado en @cod_area
    Esto es para quitar todos los mensajes a los responsables de Ejecuci�n de un determinado sector. 
    Se utiliza cuando cambia el estado de un sector y hay que eliminar todos los mensajes de los Responsables
    de Ejecuci�n involucrados con el sector.
*/

if @cod_perfil='HSEC'
begin
	delete Mensajes
        where (pet_nrointerno=@pet_nrointerno) and
            (RTRIM(cod_perfil)='CGRU') and 
            (RTRIM(cod_nivel)='GRUP') and 
            (cod_area in (select G.cod_grupo from Grupo G where RTRIM(G.cod_sector)=RTRIM(@cod_area)))
end

/*
Si el c�digo de perfil es 'PETI' entonces:
    Se eliminan todos los mensajes de una petici�n para TODOS los Responsables de Ejecuci�n y TODOS
    los Responsables de Sector (grupos y sectores).
*/

if @cod_perfil='PETI'
begin
	delete Mensajes
        where (pet_nrointerno=@pet_nrointerno) and
            (RTRIM(cod_perfil)='CGRU')
	delete Mensajes
        where (pet_nrointerno=@pet_nrointerno) and
            (RTRIM(cod_perfil)='CSEC')
end

return(0) 
go

grant execute on dbo.sp_DelMensajePerfil to GesPetUsr 
go


