/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertInfoprod'
go

if exists (select * from sysobjects where name = 'sp_InsertInfoprod' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertInfoprod
go

create procedure dbo.sp_InsertInfoprod
	@infoprot_id		int,
	@infoprot_item		int,
	@infoprot_req		char(1),
	@infoprot_itemest	char(1),
	@infoprot_gru		int,
	@infoprot_orden		int
as
	insert into GesPet.dbo.Infoprod (
		infoprot_id,
		infoprot_item,
		infoprot_req,
		infoprot_itemest,
		infoprot_gru,
		infoprot_orden)
	values (
		@infoprot_id,
		@infoprot_item,
		@infoprot_req,
		@infoprot_itemest,
		@infoprot_gru,
		@infoprot_orden)
go

grant execute on dbo.sp_InsertInfoprod to GesPetUsr
go

print 'Actualización realizada.'
go
