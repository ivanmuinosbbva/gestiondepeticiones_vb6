/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 28.10.2010 - SPs para el mantenimiento de la tabla Estados
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetEstados'
go

if exists (select * from sysobjects where name = 'sp_GetEstados' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEstados
go

create procedure dbo.sp_GetEstados
	@cod_estado	char(6)=null
as
	select 
		a.cod_estado,
		a.nom_estado,
		a.flg_rnkup,
		a.flg_herdlt1,
		a.flg_petici,
		a.flg_secgru,
		a.IGM_hab
	from 
		GesPet.dbo.Estados a
	where
		(@cod_estado is null or a.cod_estado = @cod_estado)
	return(0)
go

grant execute on dbo.sp_GetEstados to GesPetUsr
go

print 'Actualización realizada.'
go