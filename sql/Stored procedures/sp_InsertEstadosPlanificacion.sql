/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertEstadosPlanificacion'
go

if exists (select * from sysobjects where name = 'sp_InsertEstadosPlanificacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertEstadosPlanificacion
go

create procedure dbo.sp_InsertEstadosPlanificacion
	@cod_estado_plan	int,
	@nom_estado_plan	char(50),
	@estado_hab			char(1)
as
	insert into GesPet.dbo.EstadosPlanificacion (
		cod_estado_plan,
		nom_estado_plan,
		estado_hab)
	values (
		@cod_estado_plan,
		@nom_estado_plan,
		@estado_hab)
	return(0)
go

grant execute on dbo.sp_InsertEstadosPlanificacion to GesPetUsr
go

print 'Actualización realizada.'
