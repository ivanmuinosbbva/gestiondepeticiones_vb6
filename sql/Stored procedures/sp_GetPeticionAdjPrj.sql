use GesPet
go
print 'sp_GetPeticionAdjPrj'
go
if exists (select * from sysobjects where name = 'sp_GetPeticionAdjPrj' and sysstat & 7 = 4)
drop procedure dbo.sp_GetPeticionAdjPrj
go
create procedure dbo.sp_GetPeticionAdjPrj
	@prj_nrointerno	int,
	@titulo		varchar(59)=null,
	@cod_estado	varchar(250)=null
as

select PET.pet_nrointerno,
	PET.pet_nroasignado,
	PET.titulo,
	PET.cod_tipo_peticion,
	PET.prioridad,
	PET.pet_nroanexada,
	PET.fe_pedido,
	PET.fe_requerida,
	PET.fe_comite,
	PET.fe_ini_plan,
	PET.fe_fin_plan,
	PET.fe_ini_real,
	PET.fe_fin_real,
	PET.horaspresup,
	PET.cod_direccion,
	PET.cod_gerencia,
	PET.cod_sector,
	PET.cod_usualta,
	PET.cod_solicitante,
	PET.cod_referente,
	PET.cod_supervisor,
	PET.cod_director,
	PET.cod_estado,
	nom_estado = isnull((select EST.nom_estado from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),''),
	flg_rnkup = isnull((select EST.flg_rnkup from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),'0'),
	PET.fe_estado,
	PET.cod_situacion,
	nom_situacion = (select SIT.nom_situacion from Situaciones SIT where RTRIM(SIT.cod_situacion) = RTRIM(PET.cod_situacion)),
	PET.prjnrointerno,
from Peticion PET
where	(PET.prj_nrointerno=@prj_nrointerno) and
	(@titulo is null or RTRIM(@titulo) is null or RTRIM(@titulo)='' or charindex(UPPER(RTRIM(@titulo)),UPPER(RTRIM(PET.titulo)))>0) and
	(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado))>0)
order by PET.pet_nroasignado,PET.pet_nrointerno
return(0)
go



grant execute on dbo.sp_GetPeticionAdjPrj to GesPetUsr
go


