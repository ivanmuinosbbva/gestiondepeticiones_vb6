/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateEstadosIDM'
go

if exists (select * from sysobjects where name = 'sp_UpdateEstadosIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEstadosIDM
go

create procedure dbo.sp_UpdateEstadosIDM
	@cod_estado	char(6),
	@nom_estado	char(30),
	@flg_rnkup	char(2),
	@flg_herdlt1	char(1),
	@flg_petici	char(1),
	@flg_secgru	char(1)
as
	update 
		GesPet.dbo.EstadosIDM
	set
		nom_estado = @nom_estado,
		flg_rnkup = @flg_rnkup,
		flg_herdlt1 = @flg_herdlt1,
		flg_petici = @flg_petici,
		flg_secgru = @flg_secgru
	where 
		cod_estado = @cod_estado
go

grant execute on dbo.sp_UpdateEstadosIDM to GesPetUsr
go

print 'Actualización realizada.'
go
