/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetValidacion'
go

if exists (select * from sysobjects where name = 'sp_GetValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetValidacion
go

create procedure dbo.sp_GetValidacion
	@valid		int=null,
	@valitem	int=null,
	@valhab		char(1)='S'
as
	select 
		v.valid,
		v.valitem,
		v.valdesc,
		v.valorden,
		v.valhab,
		v.audituser,
		v.auditfecha
	from 
		GesPet.dbo.Validacion v
	where
		(v.valid = @valid or @valid is null) and
		(v.valitem = @valitem or @valitem is null) and 
		(v.valhab = @valhab or @valhab is null)
	order by
		v.valid,
		v.valorden
go

grant execute on dbo.sp_GetValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
