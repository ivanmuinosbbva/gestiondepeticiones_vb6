/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 07.01.2010 - Error del DBMS: Arithmetic overflow during implicit conversion of NUMERIC NULL value '166980000.00' to a NUMERIC field.
						  Arithmetic overflow occurred.
						  Se cambia la longitud de cada campo y variable de Numeric 10,2 a 12,2.
-002- a. FJS 02.10.2014 - Optimización de consulta.


Niveles de detalle (@detalle):

0: Sin agrupamiento
1: Recurso
2: Grupo
3: Sector
4: Gerencia
5: Dirección
*/

use GesPet
go

print 'Creando / actualizando: sp_rptHsTrabProyecto'
go

if exists (select * from sysobjects where name = 'sp_rptHsTrabProyecto' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHsTrabProyecto
go

create procedure dbo.sp_rptHsTrabProyecto 
	@fdesde		char(8)='NULL', 
	@fhasta		char(8)='NULL', 
	@projid		int,
	@projsubid	int,
	@projsubsid	int,
	@detalle	char(1)='1'
as 
	if @fdesde is null
		select @fdesde = 'NULL'
	if @fhasta is null
		select @fhasta = 'NULL'
	if @detalle is null
		select @detalle = '1'

	declare @cod_direccion		char (10) 
	declare @cod_gerencia		char (10) 
	declare @cod_sector			char (10) 
	declare @cod_grupo			char (10) 
	declare @nom_direccion		char (40) 
	declare @nom_gerencia		char (40) 
	declare @nom_sector			char (40) 
	declare @nom_grupo			char (40) 
	declare @cod_recurso		char (10) 
	declare @cod_tarea			char (10) 
	declare @nom_tarea			char (50) 
	declare @pet_nrointerno		int 
	declare @pet_nroasignado	int 
	declare @titulo				char(50) 
	declare @fe_desde			smalldatetime 
	declare @fe_hasta			smalldatetime 
	declare @horas				int 
	declare @trabsinasignar		char(1) 
	declare @sol_desde			smalldatetime 
	declare @sol_hasta			smalldatetime 
	declare @ret_dias			numeric(12,2)
	declare @ret_horas			numeric(12,2)
	declare @horas_si			numeric(12,2) 
	declare @horas_no			numeric(12,2) 
	declare @xhoras_si			numeric(12,2)
	declare @xhoras_no			numeric(12,2)
	declare @ret_desde			smalldatetime 
	declare @ret_hasta			smalldatetime 
	declare @tip_asig			char(3) 
	--declare @cod_asig			char(10) 
	--declare @cod_real			char(10) 
	--declare @nom_asig			char(50) 
	declare @Tot_horas_si		numeric(12,2) 
	declare @Tot_horas_no		numeric(12,2) 
	declare @Tot_fecha_desde	smalldatetime 
	declare @Tot_fecha_hasta	smalldatetime 
	declare	@observaciones		char(255)
	 
	set dateformat ymd 
	 
	if RTRIM(@fdesde)='NULL' 
		begin 
			select @sol_desde=dateadd(mm,-1,getdate()) 
			select @fdesde=convert(char(8),@sol_desde,112) 
		end 
	else 
		select @sol_desde=@fdesde 

	if RTRIM(@fhasta)='NULL' 
		begin 
			select @sol_hasta=dateadd(mm,1,getdate()) 
			select @fhasta=convert(char(8),@sol_hasta,112) 
		end 
	else 
		select @sol_hasta=@fhasta 
	
	-- Temporal con las peticiones vinculadas al proyecto
	SELECT pet_nrointerno
	INTO #peti
	FROM Peticion
	WHERE 
		pet_projid     = @projid and 
		pet_projsubid  = @projsubid and
		pet_projsubsid = @projsubsid

	CREATE TABLE #TmpRecurso(
		tcod_recurso		char(10)	null,
		tnom_recurso		char(50)	null,
		tcod_direccion		char(10)	null, 
		tcod_gerencia		char(10)	null, 
		tcod_sector			char(10)	null, 
		tcod_grupo			char(10)	null)

	insert into #TmpRecurso
		select 
			cod_recurso, 
			nom_recurso,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo
		from GesPet..Recurso

	insert into #TmpRecurso
		select 
			cod_fab, 
			nom_fab,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo
		from GesPet..Fabrica

	CREATE TABLE #TmpSalida( 
		pet_nrointerno		int				null,
		pet_nroasignado		int				null,
		nom_peticion		char(50)		null, 
		cod_direccion		char(10)		null, 
		cod_gerencia		char(10)		null, 
		cod_sector			char(10)		null, 
		cod_grupo			char(10)		null, 
		reg_tipo			char(1)			null, 
		nom_direccion		char(80)		null, 
		nom_gerencia		char(80)		null, 
		nom_sector			char(80)		null, 
		nom_grupo			char(80)		null, 
		cod_recurso			char(10)		null, 
		nom_area			char(120)		null, 
		est_area			char(30)		null, 
		nom_recurso			char(50)		null, 
		presu_desde			smalldatetime	null, 
		presu_hasta			smalldatetime	null, 
		presu_horas			numeric(12,2)	null,
		entre_desde			smalldatetime	null, 
		entre_hasta			smalldatetime	null, 
		horas_si			numeric(12,2)	null, 
		horas_no			numeric(12,2)	null,
		observaciones		char(255) 
	) 
	 
	DECLARE CursHoras CURSOR FOR 
		select  
			#TmpRecurso.tcod_direccion, 
			#TmpRecurso.tcod_gerencia, 
			#TmpRecurso.tcod_sector, 
			#TmpRecurso.tcod_grupo, 
			Ht.pet_nrointerno,
			(select x.pet_nroasignado from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno) as pet_nroasignado, 
			(select x.titulo from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno) as nom_peticion, 
			Ht.cod_recurso, 
			Ht.trabsinasignar, 
			Ht.fe_desde, 
			Ht.fe_hasta, 
			Ht.horas,
			Ht.observaciones
		 from 
			GesPet..HorasTrabajadas Ht, #TmpRecurso
		where 
			Ht.pet_nrointerno in (
				select pt.pet_nrointerno
				from #peti pt) and 
			(RTRIM(@fdesde)='NULL' or convert(char(8),Ht.fe_hasta,112) >= @fdesde) and 
			(RTRIM(@fhasta)='NULL' or convert(char(8),Ht.fe_desde,112) <= @fhasta) and 
			(RTRIM(Ht.cod_recurso) = RTRIM(#TmpRecurso.tcod_recurso))
		order by #TmpRecurso.tcod_direccion,#TmpRecurso.tcod_gerencia,#TmpRecurso.tcod_sector,#TmpRecurso.tcod_grupo,#TmpRecurso.tcod_recurso, Ht.pet_nrointerno 
		for read only 
	 
	-- proceso prorrateo de horas propiamente dicho
	OPEN CursHoras 
	FETCH CursHoras INTO  
			@cod_direccion, 
			@cod_gerencia, 
			@cod_sector, 
			@cod_grupo, 
			@pet_nrointerno,
			@pet_nroasignado,
			@titulo,
			@cod_recurso, 
			@trabsinasignar, 
			@fe_desde, 
			@fe_hasta, 
			@horas,
			@observaciones 
	 
	WHILE (@@sqlstatus = 0) 
		BEGIN 
			-- calculo del prorrateo
			execute sp_GetHorasPeriodo @sol_desde,  
				@sol_hasta,  
				@fe_desde, 
				@fe_hasta, 
				@horas, 
				@ret_desde OUTPUT, 
				@ret_hasta OUTPUT, 
				@ret_dias OUTPUT, 
				@ret_horas OUTPUT 
		 
			if RTRIM(@trabsinasignar)='S' 
				begin 
					select @horas_si = 0 
					select @horas_no = @ret_horas 
				end 
			else 
				begin 
					select @horas_si = @ret_horas 
					select @horas_no = 0 
				end 
		 
			-- inserta en temporaria
			insert #TmpSalida (
				pet_nrointerno, 
				pet_nroasignado,
				nom_peticion,
				cod_direccion, 
				cod_gerencia, 
				cod_sector, 
				cod_grupo, 
				cod_recurso, 
				reg_tipo, 
				presu_desde, 
				presu_hasta, 
				presu_horas, 
				entre_desde, 
				entre_hasta, 
				horas_si, 
				horas_no,
				observaciones) 
			values (
				@pet_nrointerno, 
				@pet_nroasignado,
				@titulo,
				@cod_direccion, 
				@cod_gerencia, 
				@cod_sector, 
				@cod_grupo, 
				@cod_recurso, 
				'0', 
				null, 
				null, 
				0, 
				@ret_desde, 
				@ret_hasta, 
				@horas_si, 
				@horas_no,
				@observaciones) 
		 
			FETCH CursHoras INTO  
				@cod_direccion, 
				@cod_gerencia, 
				@cod_sector, 
				@cod_grupo, 
				@pet_nrointerno,
				@pet_nroasignado,
				@titulo,
				@cod_recurso, 
				@trabsinasignar, 
				@fe_desde, 
				@fe_hasta, 
				@horas,
				@observaciones 
		END 
	CLOSE CursHoras 
	DEALLOCATE CURSOR CursHoras 
	
	-- forzado de areas para los que cargaron horas sin dependencia de sectores
	update #TmpSalida 
	set cod_grupo='99999999' 
	where   RTRIM(cod_grupo) is null or RTRIM(cod_grupo)=''  
	update #TmpSalida 
	set cod_sector='99999999' 
	where   RTRIM(cod_sector) is null or RTRIM(cod_sector)=''  
	update #TmpSalida 
	set cod_gerencia='99999999' 
	where   RTRIM(cod_gerencia) is null or RTRIM(cod_gerencia)='' 
	update #TmpSalida 
	set cod_direccion='99999999' 
	where   RTRIM(cod_direccion) is null or RTRIM(cod_direccion)='' 
	
	-- inserta los cabezales
	-- el cabezal de la peticion
	insert #TmpSalida (
		pet_nrointerno,
		pet_nroasignado,
		nom_peticion,
		cod_direccion, 
		cod_gerencia, 
		cod_sector, 
		cod_grupo, 
		cod_recurso, 
		est_area, 
		reg_tipo, 
		presu_desde, 
		presu_hasta, 
		presu_horas, 
		entre_desde, 
		entre_hasta, 
		horas_si, 
		horas_no,
		observaciones) 
	values( 
		NULL, 
		NULL, 
		NULL,
		'XXXXXXXX', 
		'XXXXXXXX', 
		'XXXXXXXX', 
		'XXXXXXXX', 
		'', 
		'', 
		'9', 
		null, 
		null, 
		0, 
		null, 
		null, 
		0, 
		0,
	'') 

	/*
	-- el cabezal de la peticion-sector
	insert #TmpSalida (
		pet_nrointerno,
		pet_nroasignado,
		nom_peticion,
		cod_direccion, 
		cod_gerencia, 
		cod_sector, 
		cod_grupo, 
		est_area, 
		cod_recurso, 
		reg_tipo, 
		presu_desde, 
		presu_hasta, 
		presu_horas, 
		entre_desde, 
		entre_hasta, 
		horas_si, 
		horas_no,
		observaciones) 
	select 
		NULL, 
		NULL, 
		NULL,
		ps.cod_direccion, 
		ps.cod_gerencia, 
		ps.cod_sector, 
		'', 
		--e.nom_estado, 
		NULL,
		'', 
		'3', 
		--ps.fe_ini_plan, 
		--ps.fe_fin_plan, 
		--ps.horaspresup, 
		NULL,
		NULL,
		NULL,
		null, 
		null, 
		0, 
		0,
		''
	from
		--Peticion p, 
		PeticionSector ps, 
		HorasTrabajadas Ht,
		Estados e
	where
		--p.pet_nrointerno in (select pt.pet_nrointerno from #peti pt) and 
		--p.pet_nrointerno = ps.pet_nrointerno and 
		Ht.pet_nrointerno in (select pt.pet_nrointerno from #peti pt) and 
		ps.pet_nrointerno = Ht.pet_nrointerno and 
		ps.cod_estado *= e.cod_estado 
 
 
	-- el cabezal de la peticion-grupo
	insert #TmpSalida (
		pet_nrointerno,
		pet_nroasignado,
		nom_peticion,
		cod_direccion, 
		cod_gerencia, 
		cod_sector, 
		cod_grupo, 
		est_area, 
		cod_recurso, 
		reg_tipo, 
		presu_desde, 
		presu_hasta, 
		presu_horas, 
		entre_desde, 
		entre_hasta, 
		horas_si, 
		horas_no,
		observaciones) 
	select 
		NULL, 
		NULL, 
		NULL,
		pg.cod_direccion, 
		pg.cod_gerencia, 
		pg.cod_sector, 
		pg.cod_grupo, 
		--e.nom_estado, 
		NULL,
		''
		'', 
		'2', 
		--pg.fe_ini_plan, 
		--pg.fe_fin_plan, 
		--pg.horaspresup, 
		NULL,
		NULL,
		NULL,
		null, 
		null, 
		0, 
		0,
		''
	from    
		--Peticion p, 
		HorasTrabajadas Ht,
		PeticionGrupo pg, 
		Estados e
	where   
		--p.pet_nrointerno in (select pt.pet_nrointerno from #peti pt) and
		--p.pet_nrointerno = pg.pet_nrointerno and 
		Ht.pet_nrointerno in (select pt.pet_nrointerno from #peti pt) and
		Ht.pet_nrointerno = pg.pet_nrointerno and 
		pg.cod_estado *= e.cod_estado
			
	-- fin inserta cabezales
	
	*/
	 
	
	-- cabezal de la asignacion-grupo para los que no estuvieron asignados
	insert #TmpSalida (
		pet_nrointerno,
		pet_nroasignado,
		nom_peticion,
		cod_direccion, 
		cod_gerencia, 
		cod_sector, 
		cod_grupo, 
		cod_recurso, 
		reg_tipo, 
		presu_desde, 
		presu_hasta, 
		presu_horas, 
		entre_desde, 
		entre_hasta, 
		horas_si, 
		horas_no,
		observaciones) 
	select  distinct 
		NULL, 
		NULL, 
		NULL,
		Ts.cod_direccion, 
		Ts.cod_gerencia, 
		Ts.cod_sector, 
		Ts.cod_grupo, 
		'', 
		'2', 
		null, 
		null, 
		0, 
		null, 
		null, 
		0, 
		0,
		''
	from    
		#TmpSalida Ts 
	where   
		RTRIM(Ts.cod_grupo) is not null and  
		Ts.reg_tipo='0' and 
		Ts.cod_grupo not in (select cod_grupo from #TmpSalida where reg_tipo='2') 
	
	-- ahora el cabezal de la asignacion-sector para los que no estuvieron asignados
	insert #TmpSalida (
		pet_nrointerno,
		pet_nroasignado,
		nom_peticion,
		cod_direccion, 
		cod_gerencia, 
		cod_sector, 
		cod_grupo, 
		cod_recurso, 
		reg_tipo, 
		presu_desde, 
		presu_hasta, 
		presu_horas, 
		entre_desde, 
		entre_hasta, 
		horas_si, 
		horas_no,
		observaciones) 
	select distinct 
		NULL, 
		NULL, 
		NULL,
		Ts.cod_direccion, 
		Ts.cod_gerencia, 
		Ts.cod_sector, 
		'', 
		'', 
		'3', 
		null, 
		null, 
		0, 
		null, 
		null, 
		0, 
		0,
		''
	from    #TmpSalida Ts 
	where   RTRIM(Ts.cod_sector) is not null and  
		Ts.reg_tipo='0' and 
		Ts.cod_sector not in (select cod_sector from #TmpSalida where reg_tipo='3') 
	 
	-- ahora el cabezal de la asignacion-gerencia para los que no estuvieron asignados
	insert #TmpSalida (
		pet_nrointerno,
		pet_nroasignado,
		nom_peticion,
		cod_direccion, 
		cod_gerencia, 
		cod_sector, 
		cod_grupo, 
		cod_recurso, 
		reg_tipo, 
		presu_desde, 
		presu_hasta, 
		presu_horas, 
		entre_desde, 
		entre_hasta, 
		horas_si, 
		horas_no,
		observaciones) 
	select distinct 
		NULL, 
		NULL, 
		NULL,
		Ts.cod_direccion, 
		Ts.cod_gerencia, 
		'', 
		'', 
		'', 
		'4', 
		null, 
		null, 
		0, 
		null, 
		null, 
		0, 
		0,
		''
	from    #TmpSalida Ts 
	where   RTRIM(Ts.cod_gerencia) is not null and  
		Ts.reg_tipo='0' and 
		Ts.cod_gerencia not in (select cod_gerencia from #TmpSalida where reg_tipo='4') 
	 
	-- ahora el cabezal de la asignacion-direccion para los que no estuvieron asignados
	insert #TmpSalida (
		pet_nrointerno,
		pet_nroasignado,
		nom_peticion,
		cod_direccion, 
		cod_gerencia, 
		cod_sector, 
		cod_grupo, 
		cod_recurso, 
		reg_tipo, 
		presu_desde, 
		presu_hasta, 
		presu_horas, 
		entre_desde, 
		entre_hasta, 
		horas_si, 
		horas_no,
		observaciones) 
	select distinct
		NULL, 
		NULL, 
		NULL,
		Ts.cod_direccion, 
		'', 
		'', 
		'', 
		'', 
		'5', 
		null, 
		null, 
		0, 
		null, 
		null, 
		0, 
		0,
		''
	from    #TmpSalida Ts 
	where   RTRIM(Ts.cod_direccion) is not null and  
		Ts.reg_tipo='0' and 
		Ts.cod_direccion not in (select cod_direccion from #TmpSalida where reg_tipo='5') 
	-- fin cabezales
	
		 
	-- completa con las descripciones de las areas
	update #TmpSalida 
	set nom_direccion=Di.nom_direccion, 
		nom_gerencia=Ge.nom_gerencia, 
		nom_sector=Gr.nom_sector, 
		nom_grupo=Sb.nom_grupo, 
		nom_recurso=Re.tnom_recurso 
	from #TmpSalida Ts, 
		GesPet..Direccion Di, GesPet..Gerencia Ge, GesPet..Sector Gr, GesPet..Grupo Sb,#TmpRecurso Re 
	where 
		RTRIM(Ts.cod_direccion)*=RTRIM(Di.cod_direccion) and 
		RTRIM(Ts.cod_gerencia)*=RTRIM(Ge.cod_gerencia) and 
		RTRIM(Ts.cod_sector)*=RTRIM(Gr.cod_sector) and 
		RTRIM(Ts.cod_grupo)*=RTRIM(Sb.cod_grupo) and 
		RTRIM(Ts.cod_recurso)*=RTRIM(Re.tcod_recurso) 
	 
	update  #TmpSalida 
	set nom_gerencia = ' » ' + RTRIM(nom_gerencia) 
	where (RTRIM(nom_gerencia) is not null and RTRIM(nom_gerencia)<>'') 
	 
	update  #TmpSalida 
	set nom_sector = ' » ' + RTRIM(nom_sector) 
	where (RTRIM(nom_sector) is not null and RTRIM(nom_sector)<>'') 
	 
	update  #TmpSalida 
	set nom_grupo = ' » ' + RTRIM(nom_grupo) 
	where (RTRIM(nom_grupo) is not null and RTRIM(nom_grupo)<>'') 
	 
	update #TmpSalida 
	set nom_gerencia=' » (sin gerencia)' 
	where   RTRIM(cod_gerencia)='99999999' 
	update #TmpSalida 
	set nom_sector=' » (sin sector)' 
	where   RTRIM(cod_sector)='99999999' 
	update #TmpSalida 
	set nom_grupo=' » (sin grupo)' 
	where   RTRIM(cod_grupo)='99999999' 
	update #TmpSalida 
	set nom_area=RTRIM(isnull(nom_direccion,'')) + RTRIM(isnull(nom_gerencia,'')) + RTRIM(isnull(nom_sector,'')) + RTRIM(isnull(nom_grupo,'')) 
	where   reg_tipo='5' or reg_tipo='4' or reg_tipo='3' or reg_tipo='2' 
	 
	update #TmpSalida 
	set nom_area='TOTAL: ' + RTRIM(nom_peticion) 
	where 
		RTRIM(cod_grupo)='XXXXXXXX' and  
		RTRIM(cod_sector)='XXXXXXXX' and  
		RTRIM(cod_gerencia)='XXXXXXXX' and 
		RTRIM(cod_direccion)='XXXXXXXX' 
	 
	 
	-- totalizar en cada copete de grupo/sector/gerencia/direccion
	 
	if @detalle='5' or @detalle='4' or @detalle='3' or @detalle='2' 
	begin 
		DECLARE CursUpdate CURSOR FOR 
			select DISTINCT 
				cod_direccion, 
				cod_gerencia, 
				cod_sector, 
				cod_grupo 
			from #TmpSalida 
			where   reg_tipo=@detalle 
			order by cod_direccion, cod_gerencia,   cod_sector, cod_grupo 
			for read only 
	 
		OPEN CursUpdate 
		FETCH CursUpdate INTO  
				@cod_direccion, 
				@cod_gerencia, 
				@cod_sector, 
				@cod_grupo 
		WHILE (@@sqlstatus = 0) 
		BEGIN 
			select  
				@Tot_fecha_desde=min(entre_desde), 
				@Tot_fecha_hasta=max(entre_hasta), 
				@Tot_horas_si=sum(horas_si), 
				@Tot_horas_no=sum(horas_no) 
			from   #TmpSalida 
			where   
				(RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)='' OR RTRIM(cod_direccion)=RTRIM(@cod_direccion)) and 
				(RTRIM(@cod_gerencia) is null or RTRIM(@cod_gerencia)=''  OR RTRIM(cod_gerencia)=RTRIM(@cod_gerencia)) and 
				(RTRIM(@cod_sector) is null or RTRIM(@cod_sector)=''  OR RTRIM(cod_sector)=RTRIM(@cod_sector)) and 
				(RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)=''  OR RTRIM(cod_grupo)=RTRIM(@cod_grupo)) and 
				reg_tipo='0' 
	 
			Update #TmpSalida 
				Set entre_desde = @Tot_fecha_desde, 
					entre_hasta = @Tot_fecha_hasta, 
					horas_si = @Tot_horas_si, 
					horas_no = @Tot_horas_no 
			where   ('X' + RTRIM(isnull(cod_direccion,''))='X' + RTRIM(isnull(@cod_direccion,''))) and 
				('X' + RTRIM(isnull(cod_gerencia,''))='X' + RTRIM(isnull(@cod_gerencia,''))) and 
				('X' + RTRIM(isnull(cod_sector,''))='X' + RTRIM(isnull(@cod_sector,''))) and 
				('X' + RTRIM(isnull(cod_grupo,''))='X' + RTRIM(isnull(@cod_grupo,''))) and 
				reg_tipo=@detalle 
			 
			FETCH CursUpdate INTO  
				@cod_direccion, 
				@cod_gerencia, 
				@cod_sector, 
				@cod_grupo 
		END 
		CLOSE CursUpdate 
		DEALLOCATE CURSOR CursUpdate 
	end 
	 
	-- el total de la asignacion
	select  
		@Tot_fecha_desde=min(entre_desde), 
		@Tot_fecha_hasta=max(entre_hasta), 
		@Tot_horas_si=sum(horas_si), 
		@Tot_horas_no=sum(horas_no) 
	from   #TmpSalida 
	where   reg_tipo='0' 
	Update #TmpSalida 
	Set entre_desde = @Tot_fecha_desde, 
		entre_hasta = @Tot_fecha_hasta, 
		horas_si = @Tot_horas_si, 
		horas_no = @Tot_horas_no 
	where   
		RTRIM(cod_direccion)='XXXXXXXX' and 
		RTRIM(cod_gerencia)='XXXXXXXX'  and 
		RTRIM(cod_sector)='XXXXXXXX'  and 
		RTRIM(cod_grupo)='XXXXXXXX' and 
		reg_tipo='9' 
	-- fin totalizacion
	
	-- salida segun nivel de detalle
	if @detalle='0' 
		select   
			--pet_nrointerno,
			--pet_nroasignado,
			--nom_peticion,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo, 
			nom_area, 
			est_area, 
			cod_recurso, 
			nom_recurso, 
			reg_tipo, 
			presu_desde, 
			presu_hasta, 
			presu_horas, 
			entre_desde as desde, 
			entre_hasta as hasta, 
			-- { upd -001- a.
			convert(numeric(12,2),horas_si/6000) as asignadas_si, 
			convert(numeric(12,2),horas_no/6000) as asignadas_no,
			--}
			observaciones
		from #TmpSalida  
		where reg_tipo='0' or reg_tipo='1' or reg_tipo='2'  or reg_tipo='9' 
		order by cod_direccion,cod_gerencia,cod_sector,cod_grupo,cod_recurso,reg_tipo 
	
	if @detalle='1' 
		select  distinct 
			--pet_nrointerno,
			--pet_nroasignado,
			--nom_peticion,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo, 
			nom_area, 
			'' as est_area, 
			cod_recurso, 
			nom_recurso, 
			reg_tipo, 
			presu_desde, 
			presu_hasta, 
			presu_horas, 
			min(entre_desde) as desde, 
			max(entre_hasta) as hasta, 
			convert(numeric(12,2),sum(horas_si)/6000) as asignadas_si, 
			convert(numeric(12,2),sum(horas_no)/6000) as asignadas_no,
			'' as observaciones
		from #TmpSalida  
		group by 
			reg_tipo,
			cod_direccion,
			cod_gerencia,
			cod_sector,
			cod_grupo,
			cod_recurso,
			nom_recurso,
			--pet_nrointerno,
			--pet_nroasignado,
			--nom_peticion,
			nom_area,
			presu_desde,
			presu_hasta,
			presu_horas
		having reg_tipo='0' 
		UNION ALL 
		select  distinct 
			--pet_nrointerno, 
			--pet_nroasignado,
			--nom_peticion,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo, 
			nom_area, 
			est_area, 
			cod_recurso, 
			nom_recurso, 
			reg_tipo, 
			presu_desde, 
			presu_hasta, 
			presu_horas, 
			entre_desde as desde, 
			entre_hasta as hasta, 
			convert(numeric(12,2),horas_si/6000) as asignadas_si, 
			convert(numeric(12,2),horas_no/6000) as asignadas_no,
			'' as observaciones
		from #TmpSalida  
		where reg_tipo='2' or reg_tipo='9' 
		order by cod_direccion,cod_gerencia,cod_sector,cod_grupo,cod_recurso 

	if @detalle='2' or @detalle='3' or @detalle='4' or @detalle='5'
		select  distinct 
			--pet_nrointerno,
			--pet_nroasignado,
			--nom_peticion,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo, 
			nom_area, 
			est_area, 
			'' as cod_recurso, 
			'' as nom_recurso, 
			reg_tipo, 
			presu_desde, 
			presu_hasta, 
			presu_horas, 
			entre_desde as desde, 
			entre_hasta as hasta, 
			convert(numeric(12,2),horas_si/6000) as asignadas_si, 
			convert(numeric(12,2),horas_no/6000) as asignadas_no,
			'' as observaciones
		from #TmpSalida  
		where CONVERT(int, reg_tipo)=CONVERT(int,@detalle) or reg_tipo='9' 
		order by cod_direccion,cod_gerencia,cod_sector,cod_grupo 
	return(0) 
go

grant execute on dbo.sp_rptHsTrabProyecto to GesPetUsr 
go

print 'Listo.'
go