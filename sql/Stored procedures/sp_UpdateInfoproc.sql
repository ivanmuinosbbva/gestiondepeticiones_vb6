/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateInfoproc'
go

if exists (select * from sysobjects where name = 'sp_UpdateInfoproc' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateInfoproc
go

create procedure dbo.sp_UpdateInfoproc
	@infoprot_id		int,
	@infoprot_nom		char(50),
	@infoprot_tipo		char(1),
	@infoprot_punto		char(1),
	@infoprot_estado	char(1),
	@infoprot_vermaj	smallint,
	@infoprot_vermin	smallint
as
	update 
		GesPet.dbo.Infoproc
	set
		infoprot_nom	= @infoprot_nom,
		infoprot_tipo	= @infoprot_tipo,
		infoprot_punto	= @infoprot_punto,
		infoprot_estado	= @infoprot_estado,
		infoprot_vermaj	= @infoprot_vermaj,
		infoprot_vermin	= @infoprot_vermin
	where 
		infoprot_id = @infoprot_id
go

grant execute on dbo.sp_UpdateInfoproc to GesPetUsr
go

print 'Actualización realizada.'
go
