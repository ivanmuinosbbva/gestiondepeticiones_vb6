/*
-000- a. FJS 22.06.2009 - Nuevo SP para ver las peticiones relacionadas a proyectos IDM.
-001- a. FJS 30.07.2015 - Actualización por problemas de ejecución.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyIDMPet'
go

if exists (select * from sysobjects where name = 'sp_GetProyIDMPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyIDMPet
go

create procedure dbo.sp_GetProyIDMPet
	@projid		int=null,
	@projsubid	int=null,
	@projsubsid	int=null
as 
	declare @codigo	char(30)

	-- Armo el código
	if @projid is null
		select @projid = 0
	if @projsubid is null
		select @projsubid = 0
	if @projsubsid is null
		select @projsubsid = 0
	
	if @projid = 0
		select @codigo = null
	else
		select @codigo = rtrim(convert(char(8), @projid)) + '.' + rtrim(convert(char(8), @projsubid)) + '.' + rtrim(convert(char(8), @projsubsid))

	
	select
		/*
		b.pet_projid,
		b.pet_projsubid,
		b.pet_projsubsid,
		*/
		b.ProjId, 
		b.ProjSubId,
		b.ProjSubSId, 
		b.ProjNom,
		b.ProjCatId,
		--c.ProjCatNom,
		ProjCatNom = isnull((select x.ProjCatNom from GesPet..ProyectoCategoria x where b.ProjCatId = x.ProjCatId),'-'),
		a.pet_nrointerno,
		a.pet_nroasignado,
		a.cod_tipo_peticion,
		a.cod_clase,
		a.titulo,
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where a.cod_estado = x.cod_estado),
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where a.cod_bpar = x.cod_recurso)
	from
		GesPet..Peticion a right join
		GesPet..ProyectoIDM b on (a.pet_projid = b.ProjId and a.pet_projsubid = b.ProjSubId and a.pet_projsubsid = b.ProjSubSId and a.pet_projid is not null and a.pet_projid <> 0) 
		/* inner join
		GesPet..ProyectoCategoria c on (b.ProjCatId = c.ProjCatId) */
	where
		(rtrim(convert(char(8), a.pet_projid)) + '.' + 
		 rtrim(convert(char(8), a.pet_projsubid)) + '.' + 
		 rtrim(convert(char(8), a.pet_projsubsid)) = @codigo) or (@codigo is null) and
		 a.pet_nroasignado is not null
		/*
		(@projid is null or @projid = 0 or a.pet_projid = @projid) and 
		(@projsubid	is null or @projsubid = 0 or a.pet_projsubid = @projsubid) and 
		(@projsubsid is null or @projsubsid = 0 or a.pet_projsubsid = @projsubsid)
		*/
	order by
		b.ProjId, 
		b.ProjSubId,
		b.ProjSubSId,
		b.ProjCatId,
		a.pet_nroasignado
return(0)
go

grant execute on dbo.sp_GetProyIDMPet to GesPetUsr
go

print 'Actualización realizada.'
go
