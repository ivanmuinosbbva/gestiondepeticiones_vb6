/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionInfohc'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionInfohc' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionInfohc
go

create procedure dbo.sp_InsertPeticionInfohc
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_tipo		char(3),
	@info_fecha		smalldatetime,
	@info_recurso	char(10),
	@info_estado	char(6),
	@infoprot_id	int
as
	insert into GesPet.dbo.PeticionInfohc (
		pet_nrointerno,
		info_id,
		info_idver,
		info_tipo,
		info_fecha,
		info_recurso,
		info_estado,
		infoprot_id)
	values (
		@pet_nrointerno,
		@info_id,
		@info_idver,
		@info_tipo,
		@info_fecha,
		@info_recurso,
		@info_estado,
		@infoprot_id)
go

grant execute on dbo.sp_InsertPeticionInfohc to GesPetUsr
go

print 'Actualización realizada.'
go
