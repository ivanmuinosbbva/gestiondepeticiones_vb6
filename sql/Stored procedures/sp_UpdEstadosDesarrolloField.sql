/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdEstadosDesarrolloField'
go

if exists (select * from sysobjects where name = 'sp_UpdEstadosDesarrolloField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdEstadosDesarrolloField
go

create procedure dbo.sp_UpdEstadosDesarrolloField
	@cod_estado_desa	int,
	@campo				char(80),
	@valortxt			char(255),
	@valordate			smalldatetime,
	@valornum			int
as
	if rtrim(@campo)='COD_ESTADO_DESA'
		update 
			GesPet.dbo.EstadosDesarrollo
		set
			cod_estado_desa = @valornum
		where 
			(cod_estado_desa = @cod_estado_desa or @cod_estado_desa is null)

	if rtrim(@campo)='NOM_ESTADO_DESA'
		update 
			GesPet.dbo.EstadosDesarrollo
		set
			nom_estado_desa = @valortxt
		where 
			(cod_estado_desa = @cod_estado_desa or @cod_estado_desa is null)

	if rtrim(@campo)='ESTADO_HAB'
		update 
			GesPet.dbo.EstadosDesarrollo
		set
			estado_hab = @valortxt
		where 
			(cod_estado_desa = @cod_estado_desa or @cod_estado_desa is null)
	return(0)
go

grant execute on dbo.sp_UpdEstadosDesarrolloField to GesPetUsr
go

print 'Actualización realizada.'
