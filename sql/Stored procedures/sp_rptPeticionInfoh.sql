/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptPeticionInfoh'
go

if exists (select * from sysobjects where name = 'sp_rptPeticionInfoh' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptPeticionInfoh
go

create procedure dbo.sp_rptPeticionInfoh
	@pet_nrointerno	int=null,
	@info_id		int=null,
	@info_idver		int=null,
	@info_req		char(1)=null
as
	select 
		d.pet_nrointerno,
		pet_nroasignado = (select p.pet_nroasignado from Peticion p where p.pet_nrointerno = d.pet_nrointerno),
		d.info_id,
		d.info_idver,
		infoprot_nom = (select ic.infoprot_nom from Infoproc ic where ic.infoprot_id = c.infoprot_id),
		c.info_tipo,
		info_tiponom = case
			when c.info_tipo = 'C' then 'Completo'
			when c.info_tipo = 'R' then 'Reducido'
		end,
		c.info_fecha,
		c.info_recurso,
		evaluacion = (
			select MAX(d.info_valor)
			from PeticionInfohd d
			where 
				d.pet_nrointerno = c.pet_nrointerno and 
				d.info_id = c.info_id and
				d.info_idver = c.info_idver and 
				d.info_valor is not null),
		nom_homologador = (select r.nom_recurso from Recurso r where r.cod_recurso = c.info_recurso),
		pd.infoprot_gru,
		ig.info_grunom,
		ig.info_gruorden,
		d.info_item,
		pi.infoprot_itemdsc,
		pd.infoprot_req,
		req_dsc = case 
			when pd.infoprot_req = 'S' then 'Obligatorio'
			when pd.infoprot_req = 'R' then 'Recomendado'
			when pd.infoprot_req = 'N' then 'Opcional'
			else '-'
		end,
		c.info_estado				as info_estado_cab,
		nom_infoestadocab = case
			when c.info_estado = 'TERMIN' then 'Finalizado'
			when c.info_estado = 'CONFEC' then 'En confección'
			when c.info_estado = 'VERIFI' then 'Verificado'
			when c.info_estado = 'BAJA' then 'Baja'
			else 'N/A'
		end,
		d.info_estado,
		nom_estado = (case d.info_estado
			when 'X' then 'N/A'
			when 'S' then 'Si'
			when 'N' then 'No'
			when 'R' then 'REF.SIS.'
			when 'U' then 'USUARIO'
			when 'V' then 'SUPERV.'
			when 'L' then 'LIDER'
			when 'P' then 'P950'
			when 'C' then 'C100'
		end),
		d.info_valor,
		valor_dsc = case 
			when d.info_valor in (0,1) then '-'
			when d.info_valor = 2 then 'OME'
			when d.info_valor = 3 then 'OMA'
			else ''
		end,
		d.info_orden,
		pi.infoprot_itemtpo
	from 
		GesPet..PeticionInfohd d inner join
		GesPet..PeticionInfohc c on (d.pet_nrointerno = c.pet_nrointerno and d.info_id = c.info_id and d.info_idver = c.info_idver) inner join
		GesPet..Infoprod pd on (pd.infoprot_id = c.infoprot_id and pd.infoprot_item = d.info_item) inner join
		GesPet..Infogru ig on (ig.info_gruid = pd.infoprot_gru) inner join 
		GesPet..Infoproc pc on (pc.infoprot_id = c.infoprot_id) inner join
		GesPet..Infoproi pi on (pd.infoprot_item = pi.infoprot_item)
	where
		(d.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(d.info_id = @info_id or @info_id is null) and
		(d.info_idver = @info_idver or @info_idver is null) and 
		(@info_req = 'S' or pd.infoprot_req in ('S','R'))
	order by
		d.pet_nrointerno,
		d.info_id,
		d.info_idver,
		ig.info_gruorden,
		d.info_orden
go

grant execute on dbo.sp_rptPeticionInfoh to GesPetUsr
go

print 'Actualización realizada.'
go
