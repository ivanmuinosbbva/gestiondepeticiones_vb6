/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.07.2014 - Nuevo
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateTipoSolicitud'
go

if exists (select * from sysobjects where name = 'sp_UpdateTipoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateTipoSolicitud
go

create procedure dbo.sp_UpdateTipoSolicitud
	@cod_tipo	char(1),
	@nom_tipo	char(20),
	@dsc_tipo	char(50)
as
	update 
		GesPet.dbo.TipoSolicitud
	set
		nom_tipo = @nom_tipo,
		dsc_tipo = @dsc_tipo
	where 
		cod_tipo = @cod_tipo
go

grant execute on dbo.sp_UpdateTipoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
