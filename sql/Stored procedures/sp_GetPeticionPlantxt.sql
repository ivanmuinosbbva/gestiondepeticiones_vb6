/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 07.10.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionPlantxt'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionPlantxt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionPlantxt
go

create procedure dbo.sp_GetPeticionPlantxt
	@per_nrointerno	int=null,
	@pet_nrointerno	int=null,
	@cod_grupo		char(8)=null,
	@mem_campo		char(10)=null,
	@mem_secuencia	smallint=null
as
	select 
		a.per_nrointerno,
		a.pet_nrointerno,
		a.cod_grupo,
		a.mem_campo,
		a.mem_secuencia,
		a.mem_texto
	from 
		GesPet.dbo.PeticionPlantxt a
	where
		(a.per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(a.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(a.cod_grupo = @cod_grupo or @cod_grupo is null) and
		(a.mem_campo = @mem_campo or @mem_campo is null) and
		(a.mem_secuencia = @mem_secuencia or @mem_secuencia is null)
	order by
		a.per_nrointerno,
		a.pet_nrointerno,
		a.cod_grupo,
		a.mem_campo,
		a.mem_secuencia
	return(0)
go

grant execute on dbo.sp_GetPeticionPlantxt to GesPetUsr
go

print 'Actualización realizada.'
