/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR  SUSER_SNAME*/

/*
-001- a. FJS 29.02.2008 - Se agrega el poder actualizar el modelo de control de SOx para la petici�n.
-002- a. FJS 31.10.2008 - Se agrega el poder modificar los datos de Business Partner, Impacto tecnol�gico, Tipo, Clase y Regulatorio.
-003- a. FJS 18.12.2008 - Se agrega el poder actualizar la fecha del cambio del ultimo estado de una petici�n.
-004- a. FJS 16.06.2010 - Cada vez que es actualizado el estado de una petici�n, actualizo el estado del proyecto IDM al que pertenece (si corresponde).
-005- a. FJS 27.10.2011 - Se expande a 120 caracteres el t�tulo de la petici�n.
-006- a. FJS 29.03.2012 - Se agrega el campo empresa (pet_emp).
-007- a. FJS 11.07.2012 - Se agrega el campo para indentificar peticiones vinculadas a la mitigaci�n de factores de Riesgo Operacional (pet_ro).
-008- a. FJS 11.02.2016 - Nuevo: se agrega funcionalidad para BPE.
-009- a. FJS 14.03.2016 - Nuevo: se agrega la actualizaci�n de proyectos IDM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePetField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePetField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePetField
go

create procedure dbo.sp_UpdatePetField
	@pet_nrointerno	int,
	@campo		char(15),
	@valortxt	char(120)=' ',
	@valordate	smalldatetime=null,
	@valornum	int=null
as
	declare @cod_estado			char(6)
	declare @ProjId				int
	declare @ProjSubId			int
	declare @ProjSubSId			int
	declare @old_estado			char(6)
	declare @tipo_proyecto		char(1)

	--{ add -005- a.
	if RTRIM(@campo)='TITULO' 
		update GesPet..Peticion 
		set titulo = LEFT(@valortxt,120), 
			audit_user = SUSER_NAME(), 
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno 
	--}
	--{ add -001- a.
	if RTRIM(@campo)='METODO' 
		update GesPet..Peticion 
		set pet_sox001 = @valornum, 
			audit_user = SUSER_NAME(), 
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno 
	--}
	if RTRIM(@campo)='PRIORI'
		update Peticion
		set prioridad = LEFT(@valortxt,1),
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='FECHAPED'
		update Peticion
		set fe_pedido = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	
	if RTRIM(@campo)='FECHAREQ'
		update Peticion
		set fe_requerida = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	
	if RTRIM(@campo)='FECHACOM'
		update Peticion
		set fe_comite = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='FECHAIRL'
		update Peticion
		set fe_ini_real = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='FECHAFRL'
		update Peticion
		set fe_fin_real = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='FECHAIPN'
	begin
		update Peticion
		set fe_ini_plan = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
		if @valordate is null
			update Peticion
			set fe_fec_plan = null
			where pet_nrointerno = @pet_nrointerno
	end
	if RTRIM(@campo)='FECHAFPN'
	begin
		update Peticion
		set fe_fin_plan = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
		if @valordate is null
			update Peticion
			set fe_fec_plan = null
			where pet_nrointerno = @pet_nrointerno
	end
	if RTRIM(@campo)='HORASPN'
		update Peticion
		set horaspresup = @valornum,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='PROYECTO'
		 update Peticion
		 set prj_nrointerno = @valornum,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		 where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='ESTADO'
	begin
		select 
			@cod_estado = cod_estado,
			@ProjId = pet_projid,
			@ProjSubId = pet_projsubid,
			@ProjSubSId = pet_projsubsid
		from	
			Peticion
		where pet_nrointerno = @pet_nrointerno

		if RTRIM(@cod_estado)<>RTRIM(@valortxt)
		begin
			if charindex(RTRIM(@cod_estado),RTRIM('ANEXAD|ANULAD|TERMIN|CANCEL|RECHAZ'))>0
			begin
			execute sp_DeleteMensaje
				@pet_nrointerno=@pet_nrointerno,
				@msg_nrointerno=0
			end
			execute sp_DoMensaje
				@evt_alcance='PET',
				@cod_accion='PCHGEST',
				@pet_nrointerno=@pet_nrointerno,
				@cod_hijo='',
				@dsc_estado=@valortxt,
				@txt_txtmsg='',
				@txt_txtprx=''

			update Peticion
			set cod_estado = @valortxt,
				fe_estado = getdate(),
				audit_user = SUSER_NAME(),
				audit_date = getdate()
			where pet_nrointerno = @pet_nrointerno

			--{ add -004- a.
			if exists(
				select ProjId
				from GesPet..ProyectoIDM where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId)

				select @tipo_proyecto = tipo_proyecto, @old_estado	= cod_estado
				from GesPet..ProyectoIDM where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId

				begin
					if charindex(@old_estado,'DESEST|CANCEL|TERMIN|')=0 and @tipo_proyecto = 'P'
						begin
							exec sp_GetEstadoProyectoIDM @ProjId, @ProjSubId, @ProjSubSId, @cod_estado output
							update 
								GesPet..ProyectoIDM
							set
								cod_estado = @cod_estado,
								fch_estado = getdate(),
								usr_estado = SUSER_NAME()
							where
								ProjId = @ProjId and 
								ProjSubId = @ProjSubId and 
								ProjSubSId = @ProjSubSId
						end
				end
			--}
		end
	end
	if RTRIM(@campo)='SITUAC'
		update Peticion
		set cod_situacion = LEFT(LTRIM(@valortxt),6),
			ult_accion = '',
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='ULTACC'
		update Peticion
		set	ult_accion = LEFT(LTRIM(@valortxt),8),
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='NROANEX'
		update Peticion
		set pet_nroanexada = @valornum,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='NROASIG'
		if exists (select pet_nrointerno from Peticion where pet_nroasignado = @valornum)
		begin
			select 30002 as ErrCode ,'Existen Peticion con este numero asignado' as ErrDesc
			raiserror 30002 'Existen Peticion con este numero asignado'
			return (30002)
		end
		else
		begin
			update Peticion
			set pet_nroasignado = @valornum,
				audit_user = SUSER_NAME(),
				audit_date = getdate()
			where pet_nrointerno = @pet_nrointerno
		end
	if RTRIM(@campo)='CHKSOLI'
		update Peticion
		set cod_solicitante	= LEFT(@valortxt,10),
			audit_user		= SUSER_NAME(),
			audit_date		= getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='CHKREFE'
		update Peticion
		set cod_referente	= LEFT(@valortxt,10),
			audit_user		= SUSER_NAME(),
			audit_date		= getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='CHKSUPE'
		update Peticion
		set cod_supervisor	= LEFT(@valortxt,10),
			audit_user		= SUSER_NAME(),
			audit_date		= getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='CHKAUTO'
		update Peticion
		set cod_director= LEFT(@valortxt,10),
			audit_user	= SUSER_NAME(),
			audit_date	= getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='BPARTNE'
		update Peticion
		set cod_bpar	= LEFT(@valortxt,10),
			audit_user	= SUSER_NAME(),
			audit_date	= getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='IMPOCOD'
		update Peticion
		set importancia_cod = LEFT(LTRIM(@valortxt),2),
			audit_user		= SUSER_NAME(),
			audit_date		= getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='IMPOUSR'
		update Peticion
		set importancia_usr = LEFT(LTRIM(@valortxt),10),
			audit_user		= SUSER_NAME(),
			audit_date		= getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='IMPONIV'
		update Peticion
		set importancia_prf = LEFT(LTRIM(@valortxt),4),
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='BYPCOMI'
		update Peticion
		set byp_comite = LEFT(LTRIM(@valortxt),1),
			ult_accion = '',
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	--{ add -002- a.
	if RTRIM(@campo)='BPARTNER'
		update Peticion
		set cod_bpar   = LEFT(LTRIM(@valortxt),10),
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='IMPACTO'
		update Peticion
		set pet_imptech = LEFT(LTRIM(@valortxt),1),
			audit_user  = SUSER_NAME(),
			audit_date  = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='TIPO'
		update Peticion
		set cod_tipo_peticion = LEFT(LTRIM(@valortxt),3),
			audit_user		  = SUSER_NAME(),
			audit_date		  = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='CLASE'
		update Peticion
		set cod_clase	= LEFT(LTRIM(@valortxt),4),
			audit_user  = SUSER_NAME(),
			audit_date  = getdate()
		where pet_nrointerno = @pet_nrointerno
	if RTRIM(@campo)='REGULATORIO'
		update Peticion
		set pet_regulatorio = LEFT(LTRIM(@valortxt),1),
			audit_user		= SUSER_NAME(),
			audit_date		= getdate()
		where pet_nrointerno = @pet_nrointerno
	--}
	--{ add -003- a.
	if RTRIM(@campo)='FECHAESTADO'
		update Peticion
		set fe_estado = @valordate
		where pet_nrointerno = @pet_nrointerno
	--}
	--{ add -006- a.
	if RTRIM(@campo)='EMPRESA'
		update Peticion
		set pet_emp = @valornum
		where pet_nrointerno = @pet_nrointerno
	--}
	--{ add -007- a.
	if RTRIM(@campo)='RIESGO'
		update Peticion
		set pet_ro = LEFT(LTRIM(@valortxt),1)
		where pet_nrointerno = @pet_nrointerno
	--}
	--{ add -008- a.
	if RTRIM(@campo)='REFBPE'
		update Peticion
		set cod_BPE = LEFT(LTRIM(@valortxt),10),
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='FECHABPE'
		update Peticion
		set fecha_BPE = @valordate,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='GESTION'
		update Peticion
		set pet_driver = LEFT(LTRIM(@valortxt),8),
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	
	if RTRIM(@campo)='VALOR_IMPACTO'
		update Peticion
		set valor_impacto = @valornum,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno
	
	if RTRIM(@campo)='VALOR_FACILIDAD'
		update Peticion
		set valor_facilidad = @valornum,
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='SECTOR'
		update Peticion
		set cod_sector = LTRIM(RTRIM(@valortxt)),
			audit_user = SUSER_NAME(),
			audit_date = getdate()
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='BENEFICIOS'
		update Peticion
		set cargaBeneficios = LTRIM(RTRIM(@valortxt))
		where pet_nrointerno = @pet_nrointerno
	--}
	--{ add -009- a.
	if UPPER(RTRIM(@campo))=UPPER('pet_projid')
		update Peticion
		set pet_projid = @valornum
		where pet_nrointerno = @pet_nrointerno

	if UPPER(RTRIM(@campo))=UPPER('pet_projsubid')
		update Peticion
		set pet_projsubid = @valornum
		where pet_nrointerno = @pet_nrointerno
	
	if UPPER(RTRIM(@campo))=UPPER('pet_projsubsid')
		update Peticion
		set pet_projsubsid = @valornum
		where pet_nrointerno = @pet_nrointerno
	
	if RTRIM(@campo)='ORIENTACION'
		update Peticion
		set cod_orientacion = LTRIM(RTRIM(@valortxt))
		where pet_nrointerno = @pet_nrointerno
	--}	
	/*
	if RTRIM(@campo)='PRIORIDADBPE'
		update Peticion
		set prioridadBPE = @valornum
		where pet_nrointerno = @pet_nrointerno
	*/
	
	if UPPER(RTRIM(@campo))=UPPER('cant_planif')
		update Peticion
		set cant_planif = @valornum
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='USUARIO_ALTA'
		update Peticion
		set cod_usualta	= LEFT(@valortxt,10)
		where pet_nrointerno = @pet_nrointerno

	if UPPER(RTRIM(@campo))=UPPER('sda_agr_nrointerno')
		update Peticion
		set sda_agr_nrointerno = @valornum
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='SGI_INCIDENCIA'
		update Peticion
		set sgi_incidencia = LEFT(@valortxt,20)
		where pet_nrointerno = @pet_nrointerno

	if RTRIM(@campo)='PUNTAJE'
		begin
			-- En este cursor me guardo todos los grupos DYD "t�cnicos" en estado activo
			declare @sum_impacto			real
			declare @sum_facilidad			real
			declare @valor_impacto			real
			declare @valor_facilidad		real
			declare @driverId				int
			declare @itemValor				real
			declare @valor					real
			declare @porcentaje				real

			select @valor_impacto = 0
			select @valor_facilidad = 0

			declare CursDrivers1 cursor for
				select 
					d.driverId,
					valor_referencia = convert(REAL,(select v.valorNum from Valoracion v where v.valorId = pb.valor)) * convert(REAL, convert(REAL,d.driverValor) / (select convert(REAL,SUM(x.driverValor)) from Drivers x where x.indicadorId = d.indicadorId))
				from 
					Drivers d left join
					PeticionBeneficios pb on (pb.indicadorId = d.indicadorId and pb.subindicadorId = d.driverId)
				where 
					pb.pet_nrointerno = @pet_nrointerno and 
					d.indicadorId = 1
			for read only

			open CursDrivers1 fetch CursDrivers1 into @driverId, @itemValor
				while (@@sqlstatus = 0) 
					begin
						select @valor_impacto = @valor_impacto + @itemValor
						fetch CursDrivers1 into @driverId, @itemValor
					end
			close CursDrivers1
			update Peticion set valor_impacto = @valor_impacto * convert(REAL,20) where pet_nrointerno = @pet_nrointerno

			declare CursDrivers2 cursor for
				select 
					d.driverId,
					valor_referencia = (select v.valorNum from Valoracion v where v.valorId = pb.valor) * convert(REAL, convert(REAL,d.driverValor) / (select convert(REAL,SUM(x.driverValor)) from Drivers x where x.indicadorId = d.indicadorId))
				from 
					Drivers d left join
					PeticionBeneficios pb on (pb.indicadorId = d.indicadorId and pb.subindicadorId = d.driverId)
				where 
					pb.pet_nrointerno = @pet_nrointerno and 
					d.indicadorId = 2
			for read only

			open CursDrivers2 fetch CursDrivers2 into @driverId, @itemValor
				while (@@sqlstatus = 0) 
					begin
						select @valor_facilidad = @valor_facilidad + @itemValor
						fetch CursDrivers2 into @driverId, @itemValor
					end
			close CursDrivers2
			update Peticion set valor_facilidad = @valor_facilidad * 20 where pet_nrointerno = @pet_nrointerno

			-- Elimina la referencia al cursor
			deallocate cursor CursDrivers1
			deallocate cursor CursDrivers2
		end
return(0)
go

grant execute on dbo.sp_UpdatePetField to GesPetUsr
go

grant execute on dbo.sp_UpdatePetField to RolGesIncConex
go

sp_procxmode 'sp_UpdatePetField', anymode
go

print 'Actualizaci�n realizada.'
go
