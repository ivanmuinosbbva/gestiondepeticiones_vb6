/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionInfohd'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionInfohd' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionInfohd
go

create procedure dbo.sp_DeletePeticionInfohd
	@pet_nrointerno		int=0,
	@info_id			int=0,
	@info_idver			int=0,
	@info_item			int=0
as
	-- Responsables
	DELETE 
	FROM PeticionInfohr
	WHERE 
		pet_nrointerno = @pet_nrointerno and 
		info_id = @info_id and
		(info_idver = @info_idver or @info_idver is null) and
		(info_item = @info_item or @info_item is null)

	-- Detalle
	delete from
		GesPet.dbo.PeticionInfohd
	where
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(info_id = @info_id or @info_id is null) and
		(info_idver = @info_idver or @info_idver is null) and
		(info_item = @info_item or @info_item is null)
go

grant execute on dbo.sp_DeletePeticionInfohd to GesPetUsr
go

print 'Actualización realizada.'
go
