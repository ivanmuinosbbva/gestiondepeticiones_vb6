use GesPet
go
print 'sp_InsProyIDMHisMemo'
go
if exists (select * from sysobjects where name = 'sp_InsProyIDMHisMemo' and sysstat & 7 = 4)
drop procedure dbo.sp_InsProyIDMHisMemo
go
create procedure dbo.sp_InsProyIDMHisMemo   
    @hst_nrointerno		int,   
	@mem_campo			char(10),
    @mem_secuencia		smallint, 
    @mem_texto			char(255)   
as   
       insert into GesPet..ProyectoIDMHistoMemo   
           (hst_nrointerno,
			mem_campo,
            mem_secuencia,   
            mem_texto)   
       values   
           (@hst_nrointerno,
		    @mem_campo,
            @mem_secuencia,   
            @mem_texto)   
return(0)   
go

grant execute on dbo.sp_InsProyIDMHisMemo to GesPetUsr 
go
