/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetAllDataBases'
go

if exists (select * from sysobjects where name = 'sp_GetAllDataBases' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAllDataBases
go

create procedure dbo.sp_GetAllDataBases
	@DBMS		int
as
	select ct.baseDeDatos
	from GesPet..CatalogoTabla ct
	where
		(@DBMS is null or ct.DBMS = @DBMS)
	group by
		ct.baseDeDatos
go

grant execute on dbo.sp_GetAllDataBases to GesPetUsr
go

print 'Actualización realizada.'
go
