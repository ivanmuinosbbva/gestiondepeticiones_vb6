/*
-001- a. FJS 14.10.2009 - Nuevo SP para ejecutar la transferencia de cartera activa de peticiones a otro grupo ejecutor. Este ejecuta por grupo y petici�n, todas aquellas
						  peticiones donde el grupo que cede se encuentra participando de manera activa.
-002- a. FJS 25.11.2010 - Se actualiza para contemplar la actualizaci�n de los datos de planificaci�n. 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_TraspasarPeticionAGrupo'
go

if exists (select * from sysobjects where name = 'sp_TraspasarPeticionAGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_TraspasarPeticionAGrupo
go

create procedure dbo.sp_TraspasarPeticionAGrupo
	@pet_nrointerno	int,
	@cod_sector		char(8), 
	@cod_grupo		char(8), 
	@new_sector		char(8),
	@new_grupo		char(8),
	@finaliza		char(1)
as 
	begin tran

	declare @hst_nrointerno		int
	declare @per_nrointerno		int			-- add -002- a.
	declare @cod_direccion		char(8)
	declare @cod_gerencia		char(8)
	declare @new_direccion		char(8)
	declare @new_gerencia		char(8)
	
	declare @nom_sector_ant		char(60)
	declare @nom_grupo_ant		char(60)
	declare @nom_sector_new		char(60)
	declare @nom_grupo_new		char(60)

	declare @pet_estado			char(6)
	declare @sec_estado			char(6)
	declare @estado_grupo		char(6)
	declare @estado_new_sector	char(6)
	declare @estado_new_grupo	char(6)

	declare @fIniPlan    smalldatetime
	declare @fFinPlan    smalldatetime
	declare @fIniReal    smalldatetime
	declare @fFinReal    smalldatetime
	--declare @hsPresup    smallint
	declare @hsPresup    int				-- upd
	declare @new_estado	 char(6)

	-- 1. Estructura superior del grupo de origen
	select 
		@cod_direccion = a.cod_direccion,
		@cod_gerencia  = a.cod_gerencia
	from
		GesPet..Gerencia a, GesPet..Sector b
	where
		a.cod_gerencia	= b.cod_gerencia and
		b.cod_sector	= @cod_sector

	select @nom_sector_ant = a.nom_sector
	from GesPet..Sector a
	where a.cod_sector = @cod_sector
	
	select @nom_grupo_ant = a.nom_grupo
	from GesPet..Grupo a
	where a.cod_grupo = @cod_grupo

	-- 2. Estructura superior del grupo destino
	select 
		@new_direccion = a.cod_direccion,
		@new_gerencia  = a.cod_gerencia
	from
		GesPet..Gerencia a, GesPet..Sector b
	where
		a.cod_gerencia	= b.cod_gerencia and
		b.cod_sector	= @new_sector

	select @nom_sector_new = a.nom_sector
	from GesPet..Sector a
	where a.cod_sector = @new_sector
	
	select @nom_grupo_new = a.nom_grupo
	from GesPet..Grupo a
	where a.cod_grupo = @new_grupo

	-- Obtengo el estado actual de la petici�n
	select @pet_estado = a.cod_estado
	from GesPet..Peticion a 
	where a.pet_nrointerno = @pet_nrointerno

	-- Agrego el evento principal en el historial
	execute sp_AddHistorial 
		@hst_nrointerno  OUTPUT,
		@pet_nrointerno,
		'TRASP',
		@pet_estado,
		'',
		'',
		'',
		'',
		''
	insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
		(@hst_nrointerno, 0, 'El grupo ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + ' cede su cartera de peticiones activas al grupo '  + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + '. Este es un proceso autom�tico generado a pedido del Resp. de Ejecuci�n del grupo que cede la cartera.')
	
	-- Compruebo si existe el sector del nuevo grupo
	if exists (select a.cod_sector from GesPet..PeticionSector a where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @new_sector)
		begin
			-- Obtengo el estado actual del sector heredero
			select @estado_new_sector = a.cod_estado
			from GesPet..PeticionSector a 
			where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @new_sector

			-- Obtengo el estado actual del sector cedente
			select @sec_estado = a.cod_estado
			from GesPet..PeticionSector a 
			where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector

			-- Compruebo si existe el grupo heredero
			if exists (select a.cod_grupo from GesPet..PeticionGrupo a where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @new_sector and a.cod_grupo = @new_grupo)
				begin
					-- Obtengo el estado actual de la petici�n
					select @pet_estado = a.cod_estado
					from GesPet..Peticion a 
					where a.pet_nrointerno = @pet_nrointerno

					-- Obtengo el estado actual del grupo cedente
					select @estado_grupo = a.cod_estado
					from GesPet..PeticionGrupo a 
					where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector and a.cod_grupo = @cod_grupo

					-- Obtengo el estado actual del grupo heredero, si ya existe
					select @estado_new_grupo = a.cod_estado
					from GesPet..PeticionGrupo a 
					where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @new_sector and a.cod_grupo = @new_grupo
					
					-- Si se encuentra en alguno de los estados terminales, tengo que reactivarla con los datos del grupo cedente.
					if charindex(@estado_new_grupo, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD') > 0
						begin
							-- Agrego el evento en el historial
							execute sp_AddHistorial 
								@hst_nrointerno  OUTPUT,
								@pet_nrointerno,
								'GCHGEST',
								@pet_estado,
								@new_sector,
								@estado_new_grupo,
								@new_grupo,
								@estado_grupo,
								''
							insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
								(@hst_nrointerno, 0, 'Se reactiva el grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')

							-- Reactiva al grupo heredero
							update 
								GesPet..PeticionGrupo
							set 
								cod_estado			= @estado_grupo,
								fe_estado			= getdate(),
								hst_nrointerno_sol  = @hst_nrointerno,
								hst_nrointerno_rsp  = 0,
								audit_user			= SUSER_NAME(),
								audit_date			= getdate()
							where 
								pet_nrointerno	= @pet_nrointerno and
								cod_sector		= @new_sector and
								cod_grupo		= @new_grupo
							
							-- SECTOR  - Destino -
							-- Integro el estado del sector (seg�n sus grupos componedores)
							execute sp_GetIntegracionGrupos @pet_nrointerno, @new_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

							-- Si cambio el estado del sector destino, debo registrarlo en el historial de la petici�n
							if @new_estado <> @estado_new_sector
								begin
									-- Agrego el evento en el historial
									execute sp_AddHistorial 
										@hst_nrointerno  OUTPUT,
										@pet_nrointerno,
										'SCHGEST',
										@pet_estado,
										@new_sector,
										@sec_estado,
										'',
										'',
										''
									-- AQUI
									if charindex(@estado_new_sector, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD') > 0
										insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
											(@hst_nrointerno, 0, 'Se reactiv� el estado del sector del grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
									else
										insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
											(@hst_nrointerno, 0, 'Se modific� el estado del sector del grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
								end
							
							-- Actualizo los datos del sector
							update GesPet..PeticionSector
							set
								fe_ini_plan = @fIniPlan,
								fe_fin_plan = @fFinPlan,
								fe_ini_real = @fIniReal,
								fe_fin_real = @fFinReal,
								horaspresup = @hsPresup,
								cod_estado	= @new_estado,
								fe_estado	= getdate(),
								audit_user	= SUSER_NAME(),
								audit_date	= getdate(),
								hst_nrointerno_sol = @hst_nrointerno
							where
								pet_nrointerno = @pet_nrointerno and
								cod_sector = @new_sector

							-- SECTOR  - Origen -
							-- Integro el estado del sector (seg�n sus grupos componedores)
							execute sp_GetIntegracionGrupos @pet_nrointerno, @cod_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

							-- Si cambio el estado del sector origen, debo registrarlo en el historial de la petici�n
							if @new_estado <> @sec_estado
								begin
									-- Agrego el evento en el historial
									execute sp_AddHistorial 
										@hst_nrointerno  OUTPUT,
										@pet_nrointerno,
										'SCHGEST',
										@pet_estado,
										@cod_sector,
										@new_estado,
										'',
										'',
										''
									if charindex(@sec_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD') > 0
										insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
											(@hst_nrointerno, 0, 'Se reactiv� el estado del sector del grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
									else
										insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
											(@hst_nrointerno, 0, 'Se modific� el estado del sector del grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
								end

							-- Actualizo los datos del sector
							update GesPet..PeticionSector
							set
								fe_ini_plan = @fIniPlan,
								fe_fin_plan = @fFinPlan,
								fe_ini_real = @fIniReal,
								fe_fin_real = @fFinReal,
								horaspresup = @hsPresup,
								cod_estado	= @new_estado,
								fe_estado	= getdate(),
								audit_user	= SUSER_NAME(),
								audit_date	= getdate(),
								hst_nrointerno_sol = @hst_nrointerno
							where
								pet_nrointerno	= @pet_nrointerno and
								cod_sector		= @cod_sector

							-- Integro el estado de la petici�n (seg�n sus sectores componedores)
							execute sp_GetIntegracionSectores @pet_nrointerno, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT
							
							-- Actualizo los datos de la petici�n
							update GesPet..Peticion
							set
								fe_ini_plan = @fIniPlan,
								fe_fin_plan = @fFinPlan,
								fe_ini_real = @fIniReal,
								fe_fin_real = @fFinReal,
								horaspresup = @hsPresup,
								cod_estado	= @new_estado,
								fe_estado	= getdate(),
								audit_user	= SUSER_NAME(),
								audit_date	= getdate()
							where
								pet_nrointerno = @pet_nrointerno

							-- Si cambio el estado de la petici�n, debo registrarlo en el historial
							if @new_estado <> @pet_estado
								begin
									-- Agrego el evento en el historial
									execute sp_AddHistorial 
										@hst_nrointerno  OUTPUT,
										@pet_nrointerno,
										'PCHGEST',
										@pet_estado,
										'',
										'',
										'',
										'',
										''
									insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
										(@hst_nrointerno, 0, 'Se reactiv� el grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + '.')
								end
						end
				end
			else
				-- No existe el grupo que debe heredar la petici�n
				begin
					-- Obtengo el estado actual de la petici�n
					select @pet_estado = a.cod_estado
					from GesPet..Peticion a 
					where a.pet_nrointerno = @pet_nrointerno

					-- Obtengo el estado actual del grupo cedente
					select @estado_grupo = a.cod_estado
					from GesPet..PeticionGrupo a 
					where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector and a.cod_grupo = @cod_grupo

					-- Agrego el evento en el historial
					execute sp_AddHistorial 
						@hst_nrointerno  OUTPUT,
						@pet_nrointerno,
						'GNEW000',
						@pet_estado,
						@new_sector,
						@estado_new_sector,
						@new_grupo,
						@estado_grupo,
						''
					insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values (@hst_nrointerno, 0, 'Se agrega el grupo '  + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por cesi�n de cartera.')

					-- Agrega el grupo con los datos del anterior
					insert into GesPet..PeticionGrupo 
						select
							@pet_nrointerno,
							@new_grupo,
							@new_sector,
							@new_gerencia,
							@new_direccion,
							a.fe_ini_plan,
							a.fe_fin_plan,
							a.fe_ini_real,
							a.fe_fin_real,
							a.horaspresup,
							a.cod_estado,
							getdate(),
							a.cod_situacion,
							a.ult_accion,
							@hst_nrointerno,
							0,
							SUSER_NAME(),
							getdate(),
							a.fe_fec_plan,
							a.fe_ini_orig,
							a.fe_fin_orig,
							a.fe_fec_orig,
							a.cant_planif,
							a.prio_ejecuc,
							a.info_adicio,
							a.fe_produccion,
							a.fe_suspension,
							a.cod_motivo
						from GesPet..PeticionGrupo a
						where 
							a.pet_nrointerno = @pet_nrointerno and
							a.cod_sector	 = @cod_sector and
							a.cod_grupo		 = @cod_grupo
					
					-- SECTOR - Destino - 
					-- Integro el estado del sector (seg�n sus grupos componedores)
					execute sp_GetIntegracionGrupos @pet_nrointerno, @new_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

					-- Si cambio el estado del sector destino, debo registrarlo en el historial de la petici�n
					if @new_estado <> @estado_new_sector
						begin
							-- Agrego el evento en el historial
							execute sp_AddHistorial 
								@hst_nrointerno  OUTPUT,
								@pet_nrointerno,
								'SCHGEST',
								@pet_estado,
								@new_sector,
								@new_estado,
								'',
								'',
								''

							if charindex(@estado_new_sector, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD') > 0
								begin
									insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
										(@hst_nrointerno, 0, 'Se reactiv� el estado del sector del grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones del grupo ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
								end
							else
								begin
									insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
										(@hst_nrointerno, 0, 'Se modific� el estado del sector del grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones del grupo ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
								end
						end

					-- Actualizo los datos del sector
					update GesPet..PeticionSector
					set
						fe_ini_plan = @fIniPlan,
						fe_fin_plan = @fFinPlan,
						fe_ini_real = @fIniReal,
						fe_fin_real = @fFinReal,
						horaspresup = @hsPresup,
						cod_estado	= @new_estado,
						fe_estado	= getdate(),
						audit_user	= SUSER_NAME(),
						audit_date	= getdate(),
						hst_nrointerno_sol = @hst_nrointerno
					where
						pet_nrointerno  = @pet_nrointerno and
						cod_sector		= @new_sector

					-- SECTOR - Origen - 
					-- Integro el estado del sector (seg�n sus grupos componedores)
					execute sp_GetIntegracionGrupos @pet_nrointerno, @cod_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

					-- Actualizo los datos del sector
					update GesPet..PeticionSector
					set
						fe_ini_plan = @fIniPlan,
						fe_fin_plan = @fFinPlan,
						fe_ini_real = @fIniReal,
						fe_fin_real = @fFinReal,
						horaspresup = @hsPresup,
						cod_estado	= @new_estado,
						fe_estado	= getdate(),
						audit_user	= SUSER_NAME(),
						audit_date	= getdate()
					where
						pet_nrointerno	= @pet_nrointerno and
						cod_sector		= @cod_sector

					-- Si cambio el estado del sector origen, debo registrarlo en el historial de la petici�n
					if @new_estado <> @sec_estado
						begin
							-- Agrego el evento en el historial
							execute sp_AddHistorial 
								@hst_nrointerno  OUTPUT,
								@pet_nrointerno,
								'SCHGEST',
								@pet_estado,
								@cod_sector,
								@sec_estado,
								'',
								'',
								''
							insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
								(@hst_nrointerno, 0, 'Se reactiv� el grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + '.')
						end

					-- Integro el estado de la petici�n (seg�n sus sectores componedores)
					execute sp_GetIntegracionSectores @pet_nrointerno, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT
					
					-- Actualizo los datos de la petici�n
					update GesPet..Peticion
					set
						fe_ini_plan = @fIniPlan,
						fe_fin_plan = @fFinPlan,
						fe_ini_real = @fIniReal,
						fe_fin_real = @fFinReal,
						horaspresup = @hsPresup,
						cod_estado	= @new_estado,
						fe_estado	= getdate(),
						audit_user	= SUSER_NAME(),
						audit_date	= getdate()
					where
						pet_nrointerno = @pet_nrointerno

					-- Si cambio el estado de la petici�n, debo registrarlo en el historial
					if @new_estado <> @pet_estado
						begin
							-- Agrego el evento en el historial
							execute sp_AddHistorial 
								@hst_nrointerno  OUTPUT,
								@pet_nrointerno,
								'PCHGEST',
								@pet_estado,
								'',
								'',
								'',
								'',
								''
							insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
								(@hst_nrointerno, 0, 'Se reactiv� el grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + '.')
						end
				end
		end
	else
		-- No exist�a el sector del grupo heredero: hay que agregarlo con los mismos datos que el sector del grupo cedente.
		begin
			-- Obtengo el estado actual del sector cedente
			select @sec_estado = a.cod_estado
			from GesPet..PeticionSector a 
			where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector

			-- Obtengo el estado actual del grupo cedente
			select @estado_grupo = a.cod_estado
			from GesPet..PeticionGrupo a 
			where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector and a.cod_grupo = @cod_grupo

			-- Agrego el evento en el historial (Nuevo sector)
			execute sp_AddHistorial 
				@hst_nrointerno  OUTPUT,
				@pet_nrointerno,
				'SNEW000',
				@pet_estado,
				@new_sector,
				@sec_estado,
				'',
				'',
				''
			insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values (@hst_nrointerno, 0, 'Se agrega el sector ' + RTRIM(@nom_sector_new) + ' por cesi�n de cartera.')

			-- Agrego el Sector del grupo heredero con los atributos del sector del grupo cedente
			insert into GesPet..PeticionSector 
				select
					pet_nrointerno		= a.pet_nrointerno, 
					cod_sector			= @new_sector,
					cod_gerencia		= @new_gerencia,
					cod_direccion		= @new_direccion,
					fe_ini_plan			= a.fe_ini_plan,
					fe_fin_plan			= a.fe_fin_plan,
					fe_ini_real			= a.fe_ini_real,
					fe_fin_real			= a.fe_fin_real,
					horaspresup			= a.horaspresup,
					cod_estado			= a.cod_estado,
					fe_estado			= getdate(),
					cod_situacion		= a.cod_situacion,
					ult_accion			= a.ult_accion,
					hst_nrointerno_sol  = @hst_nrointerno,
					hst_nrointerno_rsp  = 0,
					audit_user			= SUSER_NAME(),
					audit_date			= getdate(),
					fe_fec_plan			= a.fe_fec_plan,
					fe_ini_orig			= a.fe_ini_orig,
					fe_fin_orig			= a.fe_fin_orig,
					fe_fec_orig			= a.fe_fec_orig,
					cant_planif			= a.cant_planif
				from GesPet..PeticionSector a
				where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector

			-- Agrego el evento en el historial
			execute sp_AddHistorial 
				@hst_nrointerno  OUTPUT,
				@pet_nrointerno,
				'GNEW000',
				@pet_estado,
				@new_sector,
				@sec_estado,
				@new_grupo,
				@estado_grupo,
				''
			insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values (@hst_nrointerno, 0, 'Se agrega el grupo '  + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por cesi�n de cartera.')

			-- No existe el grupo que debe heredar la petici�n: Agrega el grupo con los datos del anterior
			insert into GesPet..PeticionGrupo 
				select
					@pet_nrointerno,
					@new_grupo,
					@new_sector,
					@new_gerencia,
					@new_direccion,
					a.fe_ini_plan,
					a.fe_fin_plan,
					a.fe_ini_real,
					a.fe_fin_real,
					a.horaspresup,
					@estado_grupo,
					getdate(),
					a.cod_situacion,
					a.ult_accion,
					@hst_nrointerno,
					0,
					SUSER_NAME(),
					getdate(),
					a.fe_fec_plan,
					a.fe_ini_orig,
					a.fe_fin_orig,
					a.fe_fec_orig,
					a.cant_planif,
					a.prio_ejecuc,
					a.info_adicio,
					a.fe_produccion,
					a.fe_suspension,
					a.cod_motivo
				from GesPet..PeticionGrupo a
				where 
					a.pet_nrointerno = @pet_nrointerno and
					a.cod_sector	 = @cod_sector and
					a.cod_grupo		 = @cod_grupo
				
			-- SECTOR - Destino - 
			-- Integro el estado del sector (seg�n sus grupos componedores)
			execute sp_GetIntegracionGrupos @pet_nrointerno, @new_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

			-- Actualizo los datos del sector
			update GesPet..PeticionSector
			set
				fe_ini_plan = @fIniPlan,
				fe_fin_plan = @fFinPlan,
				fe_ini_real = @fIniReal,
				fe_fin_real = @fFinReal,
				horaspresup = @hsPresup,
				cod_estado	= @new_estado,
				fe_estado	= getdate(),
				audit_user	= SUSER_NAME(),
				audit_date	= getdate()
			where
				pet_nrointerno	= @pet_nrointerno and
				cod_sector		= @new_sector
			
			-- Si cambio el estado del sector destino, debo registrarlo en el historial de la petici�n
			if @new_estado <> @sec_estado
				begin
					-- Agrego el evento en el historial
					execute sp_AddHistorial 
						@hst_nrointerno  OUTPUT,
						@pet_nrointerno,
						'SCHGEST',
						@pet_estado,
						@new_sector,
						@new_estado,
						'',
						'',
						''
					insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
						(@hst_nrointerno, 0, 'Se agreg� el grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
				end

			-- SECTOR - Origen - 
			-- Integro el estado del sector (seg�n sus grupos componedores)
			execute sp_GetIntegracionGrupos @pet_nrointerno, @cod_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

			-- Actualizo los datos del sector
			update GesPet..PeticionSector
			set
				fe_ini_plan = @fIniPlan,
				fe_fin_plan = @fFinPlan,
				fe_ini_real = @fIniReal,
				fe_fin_real = @fFinReal,
				horaspresup = @hsPresup,
				cod_estado	= @new_estado,
				fe_estado	= getdate(),
				audit_user	= SUSER_NAME(),
				audit_date	= getdate()
			where
				pet_nrointerno	= @pet_nrointerno and
				cod_sector		= @cod_sector

			-- Si cambio el estado del sector origen, debo registrarlo en el historial de la petici�n
			if @new_estado <> @sec_estado
				begin
					-- Agrego el evento en el historial
					execute sp_AddHistorial 
						@hst_nrointerno  OUTPUT,
						@pet_nrointerno,
						'SCHGEST',
						@pet_estado,
						@cod_sector,
						@sec_estado,
						'',
						'',
						''
					insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
						(@hst_nrointerno, 0, 'Se agreg� el grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
				end

			-- Integro el estado de la petici�n (seg�n sus sectores componedores)
			execute sp_GetIntegracionSectores @pet_nrointerno, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT
			
			-- Actualizo los datos de la petici�n
			update GesPet..Peticion
			set
				fe_ini_plan = @fIniPlan,
				fe_fin_plan = @fFinPlan,
				fe_ini_real = @fIniReal,
				fe_fin_real = @fFinReal,
				horaspresup = @hsPresup,
				cod_estado	= @new_estado,
				fe_estado	= getdate(),
				audit_user	= SUSER_NAME(),
				audit_date	= getdate()
			where
				pet_nrointerno = @pet_nrointerno

			-- Si cambio el estado de la petici�n, debo registrarlo en el historial
			if @new_estado <> @pet_estado
				begin
					-- Agrego el evento en el historial
					execute sp_AddHistorial 
						@hst_nrointerno  OUTPUT,
						@pet_nrointerno,
						'PCHGEST',
						@pet_estado,
						'',
						'',
						'',
						'',
						''
					insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
						(@hst_nrointerno, 0, 'Se agreg� el grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' por recibir la cartera activa de peticiones de ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + '.')
				end
		end
	--{ add -002- a. Obtengo el per�odo vigente
	select @per_nrointerno = x.per_nrointerno
	from GesPet..Periodo x
	where (getdate() >= x.fe_desde and getdate() <= x.fe_hasta)
	--}

	-- Si finaliza el grupo cedente, paso el estado del grupo a Cancelado Definitivamente y registro el evento en el historial
	if @finaliza = 'S'
		begin
			-- Obtengo el estado actual de la petici�n
			select @pet_estado = a.cod_estado
			from GesPet..Peticion a 
			where a.pet_nrointerno = @pet_nrointerno

			-- Obtengo el estado actual del sector cedente
			select @sec_estado = a.cod_estado
			from GesPet..PeticionSector a 
			where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector

			-- Obtengo el estado actual del grupo cedente
			select @estado_grupo = a.cod_estado
			from GesPet..PeticionGrupo a 
			where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @cod_sector and a.cod_grupo = @cod_grupo

			-- Obtengo el estado actual del grupo heredero, si ya existe
			select @estado_new_grupo = a.cod_estado
			from GesPet..PeticionGrupo a 
			where a.pet_nrointerno = @pet_nrointerno and a.cod_sector = @new_sector and a.cod_grupo = @new_grupo

			-- Agrego el evento en el historial
			execute sp_AddHistorial 
				@hst_nrointerno  OUTPUT,
				@pet_nrointerno,
				'GCHGEST',
				@pet_estado,
				@cod_sector,
				@sec_estado,
				@cod_grupo,
				'CANCEL',
				''
			insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values (@hst_nrointerno, 0, 'El grupo '  + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + ' es cancelado autom�ticamente por cesi�n de cartera de peticiones al grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + '.')

			update GesPet..PeticionGrupo
			set 
				cod_estado			= 'CANCEL',
				fe_estado			= getdate(),
				hst_nrointerno_sol  = @hst_nrointerno
			where
				pet_nrointerno = @pet_nrointerno and
				cod_sector	   = @cod_sector and
				cod_grupo	   = @cod_grupo

			-- SECTOR - Origen - 
			-- Integro el estado del sector (seg�n sus grupos componedores)
			execute sp_GetIntegracionGrupos @pet_nrointerno, @cod_sector, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT

			-- Actualizo los datos del sector
			update GesPet..PeticionSector
			set
				fe_ini_plan = @fIniPlan,
				fe_fin_plan = @fFinPlan,
				fe_ini_real = @fIniReal,
				fe_fin_real = @fFinReal,
				horaspresup = @hsPresup,
				cod_estado	= @new_estado,
				fe_estado	= getdate(),
				audit_user	= SUSER_NAME(),
				audit_date	= getdate(),
				hst_nrointerno_sol = @hst_nrointerno
			where
				pet_nrointerno	= @pet_nrointerno and
				cod_sector		= @cod_sector

			-- Si cambio el estado del sector origen, debo registrarlo en el historial de la petici�n
			if @new_estado <> @sec_estado
				begin
					-- Agrego el evento en el historial
					execute sp_AddHistorial 
						@hst_nrointerno  OUTPUT,
						@pet_nrointerno,
						'SCHGEST',
						@pet_estado,
						@cod_sector,
						@new_estado,
						'',
						'',
						''
					insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
						(@hst_nrointerno, 0, 'El grupo ' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + ' transfiere su cartera activa de peticiones al grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + '.')
				end

			-- Integro el estado de la petici�n (seg�n sus sectores componedores)
			execute sp_GetIntegracionSectores @pet_nrointerno, @fIniPlan OUTPUT, @fFinPlan OUTPUT, @fIniReal OUTPUT, @fFinReal OUTPUT, @hsPresup OUTPUT, @new_estado OUTPUT
			
			-- Actualizo los datos de la petici�n
			update GesPet..Peticion
			set
				fe_ini_plan = @fIniPlan,
				fe_fin_plan = @fFinPlan,
				fe_ini_real = @fIniReal,
				fe_fin_real = @fFinReal,
				horaspresup = @hsPresup,
				cod_estado	= @new_estado,
				fe_estado	= getdate(),
				audit_user	= SUSER_NAME(),
				audit_date	= getdate()
			where
				pet_nrointerno = @pet_nrointerno

			-- Vuelvo a obtener el estado de la petici�n (luego de los cambios)
			select @pet_estado = a.cod_estado
			from GesPet..Peticion a 
			where a.pet_nrointerno = @pet_nrointerno

			-- Y si volvi� a cambiar, debo registrarlo en el historial
			if @new_estado <> @pet_estado
				begin
					-- Agrego el evento en el historial
					execute sp_AddHistorial 
						@hst_nrointerno  OUTPUT,
						@pet_nrointerno,
						'PCHGEST',
						@new_estado,
						'',
						'',
						'',
						'',
						''
					insert into HistorialMemo(hst_nrointerno, hst_secuencia, mem_texto) values 
						(@hst_nrointerno, 0, 'El grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ' transfiere su cartera activa de peticiones al grupo ' + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + '.')
				end
			
			-- Documentos de metodolog�a del grupo cediente son "transferidos" al grupo heredero
			update GesPet..PeticionAdjunto
			set
				cod_sector = @new_sector,
				cod_grupo  = @new_grupo,
				adj_texto  = 'Actualizado autom�ticamente por traspaso de cartera entre grupos (' + RTRIM(@nom_sector_ant) + ' � ' + RTRIM(@nom_grupo_ant) + ' traspasa cartera de peticiones al grupo '  + RTRIM(@nom_sector_new) + ' � ' + RTRIM(@nom_grupo_new) + ').  Se actualizaron los datos del documento adjunto.',
				audit_user = SUSER_NAME(),
				audit_date = getdate()
			where 
				pet_nrointerno	= @pet_nrointerno and
				cod_sector		= @cod_sector and
				cod_grupo		= @cod_grupo
		end
	commit tran
go

grant execute on dbo.sp_TraspasarPeticionAGrupo to GesPetUsr
go

print 'Actualizaci�n realizada.'
go