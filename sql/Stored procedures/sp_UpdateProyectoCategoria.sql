/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 04.11.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoCategoria'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoCategoria' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoCategoria
go

create procedure dbo.sp_UpdateProyectoCategoria
	@ProjCatId	int,
	@ProjCatNom	char(100)
as
	update 
		GesPet.dbo.ProyectoCategoria
	set
		ProjCatNom = @ProjCatNom
	where 
		ProjCatId = @ProjCatId
	return(0)
go

grant execute on dbo.sp_UpdateProyectoCategoria to GesPetUsr
go

print 'Actualización realizada.'
go