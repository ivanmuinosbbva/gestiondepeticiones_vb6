/*
-001- a. FJS 11.09.2008 - Se agregan los datos a la impresión de: Tipo, Clase, Impacto y Regulatorio. 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetUnaPeticionXt'
go

if exists (select * from sysobjects where name = 'sp_GetUnaPeticionXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetUnaPeticionXt
go

create procedure dbo.sp_GetUnaPeticionXt   
	@pet_nrointerno    int   
as

--{ add -001- a.
	-- Temporal para descripción de Tipo de Petición 
	create table #TpoPet 
	( 
		cod_tipo_peticion	char(3)		null, 
		nom_tipo_peticion	char(30)	null 
	) 
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('NOR', 'Normal') 
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('PRJ', 'Proyecto') 
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('ESP', 'Especial') 
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('PRO', 'Propia') 
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('AUX', 'Auditoría Externa') 
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('AUI', 'Auditoría Interna') 
 
	-- Temporal para descripción de Clase de Petición 
	create table #ClasePet 
	( 
		cod_clase	char(4)		null, 
		nom_clase	char(30)	null 
	) 
	insert into #ClasePet (cod_clase, nom_clase) values ('SINC', 'Sin clase') 
	insert into #ClasePet (cod_clase, nom_clase) values ('CORR', 'Mant. Correctivo') 
	insert into #ClasePet (cod_clase, nom_clase) values ('ATEN', 'Atención a usuario') 
	insert into #ClasePet (cod_clase, nom_clase) values ('OPTI', 'Optimización') 
	insert into #ClasePet (cod_clase, nom_clase) values ('EVOL', 'Mant. evolutivo') 
	insert into #ClasePet (cod_clase, nom_clase) values ('NUEV', 'Nuevo desarrollo') 
	insert into #ClasePet (cod_clase, nom_clase) values ('SPUF', 'SPUFI') 
--}



	select 
		PET.pet_nrointerno, 
        PET.pet_nroasignado, 
        PET.titulo,  
        PET.cod_tipo_peticion, 
        PET.prioridad,   
        PET.pet_nroanexada, 
        PET.fe_pedido, 
        PET.fe_requerida, 
        PET.fe_comite, 
        PET.fe_ini_plan, 
        PET.fe_fin_plan,             
        PET.fe_ini_real,             
        PET.fe_fin_real,             
        PET.horaspresup,             
        PET.cod_direccion,           
        nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(PET.cod_direccion)),        
        PET.cod_gerencia,            
        nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where RTRIM(Ge.cod_gerencia) = RTRIM(PET.cod_gerencia)),         
        PET.cod_sector,          
        nom_sector = (select Gr.nom_sector from Sector Gr where RTRIM(Gr.cod_sector) = RTRIM(PET.cod_sector)),           
        PET.cod_usualta,             
        nom_usualta = (select Ua.nom_recurso from Recurso Ua where RTRIM(Ua.cod_recurso) = RTRIM(PET.cod_usualta)),           
        PET.cod_solicitante,         
        nom_solicitante = (select Sl.nom_recurso from Recurso Sl where RTRIM(Sl.cod_recurso) = RTRIM(PET.cod_solicitante)),      
        PET.cod_bpar,         
        nom_bpar = (select BP.nom_recurso from Recurso BP where RTRIM(BP.cod_recurso) = RTRIM(PET.cod_bpar)),      
        PET.cod_referente,       
        nom_referente = (select Rf.nom_recurso from Recurso Rf where RTRIM(Rf.cod_recurso) = RTRIM(PET.cod_referente)),        
        PET.cod_supervisor,      
        nom_supervisor = (select Sp.nom_recurso from Recurso Sp where RTRIM(Sp.cod_recurso) = RTRIM(PET.cod_supervisor)),       
        PET.cod_director,        
        nom_director = (select Dr.nom_recurso from Recurso Dr where RTRIM(Dr.cod_recurso) = RTRIM(PET.cod_director)),         
        PET.cod_estado,          
        nom_estado = (select EST.nom_estado from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),          
        PET.fe_estado,           
        PET.cod_situacion, 
        nom_situacion = (select SIT.nom_situacion from Situaciones SIT where RTRIM(SIT.cod_situacion) = RTRIM(PET.cod_situacion))
		--{ add -001- a.
		,nom_tipo_peticion = (select nom_tipo_peticion from #TpoPet where cod_tipo_peticion = PET.cod_tipo_peticion)
		,PET.cod_clase
		,nom_clase = (select nom_clase from #ClasePet where cod_clase = PET.cod_clase)
		,PET.pet_imptech
		,PET.pet_sox001
		,PET.pet_regulatorio
		--}
    from  
		GesPet..Peticion PET
    where  
		PET.pet_nrointerno = @pet_nrointerno 
return(0)   
go

grant execute on dbo.sp_GetUnaPeticionXt to GesPetUsr 
go

print 'Actualización realizada.'