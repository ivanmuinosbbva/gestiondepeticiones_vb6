/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

-000- a. FJS 11.08.2009 - Nuevo SP para el envio de solicitudes a CMAQ.
-001- a. FJS 21.01.2010 - Se modifica el c�digo de tipo de solicitud para los que fueron RESTORE.
-002- a. FJS 22.04.2010 - Se modifica porque para los tipos 6 (SORT con Restore) esta informando mal el archivo de intermedio (esta poniendo el de producci�n).
-003- a. FJS 14.07.2014 - Nuevo: se agrega funcionalidad para solicitudes por EMErgencia.
-004- a. FJS 01.08.2014 - Nuevo: se agrega funcionalidad para RESTORE en UNLO.
*/

/*
REFERENCIA:

A: Pendiente de aprobaci�n N4
B: Pendiente de aprobaci�n N3
C: Aprobado
D: Aprobado y en proceso de envio
E: Enviado y finalizado
F: En espera de Restore
G: En espera (ejecuci�n diferida)		Aquellas que nacen como de ejecuci�n diferida, luego pasan a estado "C"
*/
 
use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSoliEnviadas'
go

if exists (select * from sysobjects where name = 'sp_UpdateSoliEnviadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSoliEnviadas
go

create procedure dbo.sp_UpdateSoliEnviadas
	@tipo				int,
	@sol_nroasignado	int=null
as
	-- Declaraci�n de variables locales
	declare	@cur_sol_nroasignado	int
	declare @dias	int

	truncate table GesPet..tmpSolicitudes

	if @tipo = 1
		begin
			-- 1. Guardo en un cursor aquellas solicitudes que corresponda transmitir. Ser�n aquellas en estado 'Aprobado'
			declare CursSoli cursor for
				select a.sol_nroasignado from GesPet..Solicitudes a
				where (a.sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null) and a.sol_estado = 'C'
				group by a.sol_nroasignado
			for read only 

			open CursSoli 
				fetch CursSoli into @cur_sol_nroasignado
			while (@@sqlstatus = 0) 
			begin 
				-- Marco la solicitud como en estado 'Enviando...'
				update GesPet..Solicitudes
				set sol_estado = 'D'
				where sol_nroasignado = @cur_sol_nroasignado
				
				-- La guardo en la auxiliar para luego ser invocada por el bcpout
				insert into GesPet..tmpSolicitudes
					select 
						sol_nroasignado,
						--sol_tipo,		-- del -001- a.
						--{ add -001- a.
						sol_tipo = 
						case 
							when sol_tipo = '5' then '1'
							when sol_tipo = '6' then '4'
							when sol_tipo = '7' then '2'		-- add -004- a.
						else
							a.sol_tipo
						end,
						--}
						sol_mask,
						sol_fecha,
						sol_recurso,
						nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
						pet_nrointerno,
						sol_resp_ejec,
						nom_resp_ejec = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec),
						sol_resp_sect,
						nom_resp_sect = 
						case
							when (select charindex(',', x.nom_recurso) from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect) > 0 then (select upper(substring(x.nom_recurso, 1, charindex(',', x.nom_recurso)-1)) from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect)
							when (select charindex(',', x.nom_recurso) from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect) = 0 then (select upper(x.nom_recurso) from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect)
						end,
						sol_seguridad,
						nom_seguridad = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_seguridad),
						sol_fe_auto1,
						sol_fe_auto2,
						sol_fe_auto3,
						sol_file_prod,
						sol_file_desa,
						/*
						--{ add -002- a.
						sol_file_prod = 
						case
							when a.sol_tipo = '6' then a.sol_file_desa
							else a.sol_file_prod
						end,
						sol_file_desa = 
						case 
							when a.sol_tipo = '6' then a.sol_file_prod
							else a.sol_file_desa
						end,
						--}
						*/
						/*{ del -002- a.
						sol_file_prod,
						sol_file_desa,
						*/
						sol_file_out,
						sol_lib_Sysin,
						sol_mem_Sysin,
						sol_join,
						sol_nrotabla,
						fe_formato
					from GesPet..Solicitudes a
					where a.sol_nroasignado = @cur_sol_nroasignado
				
				fetch CursSoli into @cur_sol_nroasignado
			end
			close CursSoli 
			deallocate cursor CursSoli 
		end
	else
		begin
			declare CursSoli cursor for
				select a.sol_nroasignado from GesPet..Solicitudes a
				where (a.sol_nroasignado = @sol_nroasignado or @sol_nroasignado is null) and a.sol_estado = 'D'
				group by a.sol_nroasignado
			for read only 

			open CursSoli 
				fetch CursSoli into @cur_sol_nroasignado
				while (@@sqlstatus = 0) 
					begin 
						-- Marco la solicitud como en estado 'Enviado y finalizado'
						update 
							GesPet..Solicitudes
						set 
							sol_estado   = 'E',
							sol_trnm_fe  = getdate(),
							sol_trnm_usr = SUSER_NAME()
						where 
							sol_nroasignado = @cur_sol_nroasignado and
							sol_estado = 'D'

						fetch CursSoli into @cur_sol_nroasignado
					end
			close CursSoli 
			deallocate cursor CursSoli 
		end

	-- Evaluo de eliminar aquellas solicitudes con m�s de N d�as (por defecto son 30 d�as)
	select @dias = (select var_numero from GesPet..Varios where var_codigo = 'CLIST')
	if @dias = 0 or @dias is null
		begin
			select @dias = 30			-- Inicializaci�n por defecto: 30 d�as
		end

	delete from GesPet..Solicitudes
	where datediff(day, sol_fecha, getdate()) > @dias and sol_estado = 'E'

	return(0)
go

grant execute on dbo.sp_UpdateSoliEnviadas to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
