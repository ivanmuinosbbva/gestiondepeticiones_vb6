/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 06.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertInterfazTera'
go

if exists (select * from sysobjects where name = 'sp_InsertInterfazTera' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertInterfazTera
go

create procedure dbo.sp_InsertInterfazTera
	@dbName		char(30),
	@objname	char(50),
	@objlabel	char(50),
	@objhab		char(1)
as
	insert into GesPet.dbo.InterfazTera (
		dbName,
		objname,
		objlabel,
		objhab)
	values (
		@dbName,
		@objname,
		@objlabel,
		@objhab)
go

grant execute on dbo.sp_InsertInterfazTera to GesPetUsr
go

print 'Actualización realizada.'
go
