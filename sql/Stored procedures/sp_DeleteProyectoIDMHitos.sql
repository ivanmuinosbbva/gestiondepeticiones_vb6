/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMHitos'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMHitos' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMHitos
go

create procedure dbo.sp_DeleteProyectoIDMHitos
	@ProjId			int=0,
	@ProjSubId		int=0,
	@ProjSubSId		int=0,
	@hit_nrointerno	int=null
as
	delete from
		GesPet.dbo.ProyectoIDMHitos
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		(@hit_nrointerno is null or hit_nrointerno = @hit_nrointerno)
	return(0)
go

grant execute on dbo.sp_DeleteProyectoIDMHitos to GesPetUsr
go

print 'Actualización realizada.'
go