/*
-000- a. FJS 25.01.2016 - Creación.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateGerencia'
go

if exists (select * from sysobjects where name = 'sp_UpdateGerencia' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateGerencia
go

create procedure dbo.sp_UpdateGerencia
	@modo			char(1)='A',
	@cod_gerencia	char(8),
	@nom_gerencia	char(80),
	@cod_direccion	char(8),
	@flg_habil		char(1)='S',
	@es_ejecutor	char(1)='S',
	@IGM			char(1)='S',
	@abreviatura	varchar(30)
as

if @modo='M'
	begin
		if not exists (select cod_gerencia from GesPet..Gerencia where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia))
		begin
			raiserror 30011 'Gerencia Inexistente'
			select 30011 as ErrCode , 'Gerencia Inexistente' as ErrDesc
			return (30011)
		end
	end
else
	begin
		if exists (select cod_gerencia from GesPet..Gerencia where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia))
		begin
			raiserror 30011 'Codigo de Gerencia Existente'
			select 30011 as ErrCode , 'Codigo de Gerencia Existente' as ErrDesc
			return (30011)
		end
	end

	if @modo='M'
	begin
		BEGIN TRANSACTION
			if RTRIM(@flg_habil)='N'
			begin
				update GesPet..Grupo
				set flg_habil = 'N'
				from	GesPet..Grupo Su, GesPet..Sector Gr
				where (RTRIM(Su.cod_sector) = RTRIM(Gr.cod_sector)) and
					(RTRIM(Gr.cod_gerencia) = RTRIM(@cod_gerencia))

				update GesPet..Sector
				set flg_habil = 'N'
				from	GesPet..Sector Gr, GesPet..Gerencia Ge
				where (RTRIM(Gr.cod_gerencia) = RTRIM(@cod_gerencia))
			end

			update GesPet..Gerencia
			set nom_gerencia	= @nom_gerencia,
				cod_direccion	= @cod_direccion,
				flg_habil		= @flg_habil,
				es_ejecutor		= @es_ejecutor,
				IGM_hab			= @IGM,
				abrev_gerencia	= @abreviatura
			where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)
		COMMIT
	end
	else
		begin
			insert into GesPet..Gerencia (cod_gerencia, nom_gerencia, cod_direccion, flg_habil, es_ejecutor, IGM_hab, abrev_gerencia)
			values (@cod_gerencia, @nom_gerencia,@cod_direccion, @flg_habil, @es_ejecutor, @IGM, @abreviatura)
		end
	return(0)
go

grant execute on dbo.sp_UpdateGerencia to GesPetUsr
go

print 'Actualización realizada.'
go
