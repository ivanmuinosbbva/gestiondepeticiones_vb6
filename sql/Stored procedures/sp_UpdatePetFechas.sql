use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePetFechas'
go


if exists (select * from sysobjects where name = 'sp_UpdatePetFechas' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePetFechas
go

create procedure dbo.sp_UpdatePetFechas   
    @xnrointerno		int,   
    @fe_ini_plan		smalldatetime=null, 
    @fe_fin_plan		smalldatetime=null, 
    @fe_ini_real		smalldatetime=null, 
    @fe_fin_real		smalldatetime=null, 
    @horaspresup		int 
as   
	declare @x_fin_orig		smalldatetime 
	declare @x_ini_orig		smalldatetime 
	declare @x_fin_plan		smalldatetime 
	declare @x_ini_plan		smalldatetime 
	declare @cod_estado		char(6)
	declare @cant_planif	int
	declare @replanif		char(1)
	declare @aux_txtmsg		varchar(250) 

	select @replanif = 'N'
 
	-- Asignación inicial de fechas globales, estado actual y cantidad de horas planificadas de la petición
	select  
		@x_ini_orig  = fe_ini_orig,
		@x_fin_orig  = fe_fin_orig, 
		@x_ini_plan  = fe_ini_plan, 
		@x_fin_plan  = fe_fin_plan,
		@cod_estado  = cod_estado,
		@cant_planif = cant_planif
	from 
		GesPet..Peticion  
	where  
		pet_nrointerno = @xnrointerno

	-- Si tiene inicio o fin de planificación y el nuevo estado es 'Planificado' o 'En ejecución' o 'Finalizado'
	if (@fe_ini_plan is not null or @fe_fin_plan is not null) and charindex(@cod_estado,'PLANOK|EJECUC|TERMIN') > 0
		begin 
			if ((@x_ini_plan is null) or ((@x_ini_plan is not null) and ('X'+convert(char(8),@x_ini_plan,112)<>'X'+convert(char(8),@fe_ini_plan,112)))) or 
			   ((@x_fin_plan is null) or ((@x_fin_plan is not null) and ('X'+convert(char(8),@x_fin_plan,112)<>'X'+convert(char(8),@fe_fin_plan,112)))) 
			/* es una fecha nueva o distinta a la que existia */
			/* entonces puedo decir que estoy planificando una vez más */
			begin 
				if @cant_planif is null
					select @cant_planif = 0
				select @cant_planif = @cant_planif + 1
				if @cant_planif = 1
					select @replanif = 'P'
				else
					select @replanif = 'R'
			end
		end

	if (@x_fin_plan is not null) and 
	   (@fe_fin_plan is not null) and 
	   ('X' + convert(char(8),@x_fin_plan,112)<> 'X' + convert(char(8),@fe_fin_plan,112)) 
		begin 
			select @aux_txtmsg='Cambio de fecha planificada de finalización del: ' + convert(varchar(10),@x_fin_plan,103) + '  al: ' + convert(varchar(10),@fe_fin_plan,103) + '.' 
		 
			execute sp_DoMensaje 
				@evt_alcance='PET', 
				@cod_accion='PCHGFEC', 
				@pet_nrointerno=@xnrointerno, 
				@cod_hijo='', 
				@dsc_estado='NULL', 
				@txt_txtmsg=@aux_txtmsg, 
				@txt_txtprx='' 
		end 

		update GesPet..Peticion  
		set fe_ini_plan     =  @fe_ini_plan, 
			fe_fin_plan     =  @fe_fin_plan,         
			fe_ini_real     =  @fe_ini_real,         
			fe_fin_real     =  @fe_fin_real,         
			horaspresup     =  @horaspresup 
		where  pet_nrointerno = @xnrointerno   

		-- Planificación inicial
		if @replanif = 'P'
			update GesPet..Peticion  
			set fe_ini_orig = @fe_ini_plan, 
				fe_fin_orig = @fe_fin_plan,         
				cant_planif	= @cant_planif,
				fe_fec_orig = getdate(),
				fe_fec_plan = getdate()
			where  pet_nrointerno = @xnrointerno   
		
		-- Replanificaciones
		if @replanif = 'R'
			update GesPet..Peticion  
			set cant_planif	= @cant_planif,
				fe_fec_plan = getdate()
			where  pet_nrointerno = @xnrointerno   
		
		-- Si no hay alguna de las fechas de planificación, si inicializa el contador de re-planificaciones
		if  (@fe_ini_plan is null or @fe_fin_plan is null)
			update GesPet..Peticion
			set fe_fec_plan = null
			where  pet_nrointerno = @xnrointerno
	return(0)
go

grant execute on dbo.sp_UpdatePetFechas to GesPetUsr 
go

print 'Actualización realizada.'
go
