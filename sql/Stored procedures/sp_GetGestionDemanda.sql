/*
-001- a. FJS 04.04.2017 - Nuevo: se utiliza para mostrar qui�n atiende las peticiones que cargan las �reas usuarias.
						  Esto es configurado a nivel de sector, donde se especifica si es Sistemas o BPE (y qui�n)
						  el que atiende a determinado sector solicitante.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetGestionDemanda'
go

if exists (select * from sysobjects where name = 'sp_GetGestionDemanda' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetGestionDemanda
go

create procedure dbo.sp_GetGestionDemanda 
as 
 	select 
		d.cod_direccion,
		d.nom_direccion,
		g.cod_gerencia,
		g.nom_gerencia,
		s.cod_sector,
		s.nom_sector,
		s.cod_perfil,
		s.cod_bpar,
		nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = s.cod_bpar)
	from 
		Sector s inner join
		Gerencia g on (s.cod_gerencia = g.cod_gerencia) inner join
		Direccion d on (d.cod_direccion = g.cod_direccion)
	where
		s.flg_habil = 'S'
	order by
		d.cod_direccion,
		s.cod_gerencia
	return(0)
go

grant execute on dbo.sp_GetGestionDemanda to GesPetUsr 
go

exec sp_procxmode 'sp_GetGestionDemanda', 'anymode'

print 'Actualizaci�n realizada.'
go
