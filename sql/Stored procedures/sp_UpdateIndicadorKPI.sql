/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateIndicadorKPI'
go

if exists (select * from sysobjects where name = 'sp_UpdateIndicadorKPI' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateIndicadorKPI
go

create procedure dbo.sp_UpdateIndicadorKPI
	@kpiid		int,
	@kpinom		varchar(60),
	@kpireq		char(1),
	@kpihab		char(1),
	@unimedId	char(10),
	@kpiayuda	varchar(255),
	@kpiemp		int
as
	update 
		GesPet.dbo.IndicadorKPI
	set
		kpinom		= @kpinom,
		kpireq		= @kpireq,
		kpihab		= @kpihab,
		unimedId	= @unimedId,
		kpiayuda	= @kpiayuda,
		kpiemp		= @kpiemp
	where 
		kpiid = @kpiid
go

grant execute on dbo.sp_UpdateIndicadorKPI to GesPetUsr
go

print 'Actualización realizada.'
go
