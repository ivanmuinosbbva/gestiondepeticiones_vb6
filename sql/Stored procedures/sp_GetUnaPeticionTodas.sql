use GesPet
go
print 'sp_GetUnaPeticionTodas'
go
if exists (select * from sysobjects where name = 'sp_GetUnaPeticionTodas' and sysstat & 7 = 4)
drop procedure dbo.sp_GetUnaPeticionTodas
go

create procedure dbo.sp_GetUnaPeticionTodas 
    @pet_nrointerno     int, 
    @pet_nroasignado    int, 
    @titulo             char(50), 
    @cod_tipo_peticion  char(3), 
    @prioridad          char(1), 
    @pet_nroanexada     int, 
    @fe_pedido          smalldatetime, 
    @fe_requerida       smalldatetime, 
    @fe_comite          smalldatetime, 
    @fe_ini_plan        smalldatetime, 
    @fe_fin_plan        smalldatetime, 
    @fe_ini_real        smalldatetime, 
    @fe_fin_real        smalldatetime, 
    @horaspresup        smallint, 
    @cod_direccion      char(8), 
    @cod_gerencia       char(8), 
    @cod_sector         char(8), 
    @cod_usualta        char(10), 
    @cod_solicitante    char(10), 
    @cod_referente      char(10), 
    @cod_supervisor     char(10), 
    @cod_director       char(10), 
    @cod_estado         char(6), 
    @fe_estado          smalldatetime, 
    @cod_situacion      char(6), 
    @ult_accion         char(8), 
    @audit_user         char(10), 
    @audit_date         smalldatetime, 
    @corp_local         char(1), 
    @cod_orientacion    char(2), 
    @importancia_cod    char(2), 
    @importancia_prf    char(4), 
    @importancia_usr    char(10), 
    @fe_fec_plan        smalldatetime, 
    @fe_ini_orig        smalldatetime, 
    @fe_fin_orig        smalldatetime, 
    @fe_fec_orig        smalldatetime, 
    @cant_planif        int, 
    @prj_nrointerno     int, 
    @byp_comite         char(1), 
    @cod_bpar           char(10), 
    @cod_clase          char(4), 
    @pet_imptech        char(1), 
    @pet_sox001         tinyint 
as
    select 
        a.pet_nrointerno, 
        a.pet_nroasignado, 
        a.titulo, 
        a.cod_tipo_peticion, 
        a.prioridad, 
        a.pet_nroanexada, 
        a.fe_pedido, 
         a.fe_requerida, 
        a.fe_comite, 
        a.fe_ini_plan, 
        a.fe_fin_plan, 
        a.fe_ini_real, 
        a.fe_fin_real, 
        a.horaspresup, 
        a.cod_direccion, 
        a.cod_gerencia, 
        a.cod_sector, 
        a.cod_usualta, 
        a.cod_solicitante, 
        a.cod_referente, 
        a.cod_supervisor, 
        a.cod_director, 
        a.cod_estado, 
        a.fe_estado, 
        a.cod_situacion, 
        a.ult_accion, 
        a.audit_user, 
        a.audit_date, 
        a.corp_local, 
        a.cod_orientacion, 
        a.importancia_cod, 
        a.importancia_prf, 
        a.importancia_usr, 
        a.fe_fec_plan, 
        a.fe_ini_orig, 
        a.fe_fin_orig, 
        a.fe_fec_orig, 
        a.cant_planif, 
        a.prj_nrointerno, 
        a.byp_comite, 
        a.cod_bpar, 
        a.cod_clase, 
        a.pet_imptech, 
        a.pet_sox001 
    from 
        GesPet..Peticion a
    where 
        (a.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
        (a.pet_nroasignado = @pet_nroasignado Or @pet_nroasignado is null) and 
        (a.titulo = @titulo Or @titulo is null) and 
        (a.cod_tipo_peticion  = @cod_tipo_peticion Or @cod_tipo_peticion is null) and 
        (a.prioridad  = @prioridad Or @prioridad is null) and 
        (a.pet_nroanexada = @pet_nroanexada Or @pet_nroanexada is null) and 
        (a.fe_pedido  = @fe_pedido Or @fe_pedido is null) and 
        (a.fe_requerida  = @fe_requerida Or @fe_requerida is null) and 
        (a.fe_comite  = @fe_comite Or @fe_comite is null) and 
        (a.fe_ini_plan  = @fe_ini_plan Or @fe_ini_plan is null) and 
        (a.fe_fin_plan  = @fe_fin_plan Or @fe_fin_plan is null) and 
        (a.fe_ini_real  = @fe_ini_real Or @fe_ini_real is null) and 
        (a.fe_fin_real  = @fe_fin_real Or @fe_fin_real is null) and 
        (a.horaspresup  = @horaspresup Or @horaspresup is null) and 
        (a.cod_direccion = @cod_direccion Or @cod_direccion is null) and 
        (a.cod_gerencia  = @cod_gerencia Or @cod_gerencia is null) and 
        (a.cod_sector = @cod_sector Or @cod_sector is null) and 
        (a.cod_usualta = @cod_usualta Or @cod_usualta is null) and 
        (a.cod_solicitante  = @cod_solicitante Or @cod_solicitante is null) and 
        (a.cod_referente  = @cod_referente Or @cod_referente is null) and 
        (a.cod_supervisor  = @cod_supervisor Or @cod_supervisor is null) and 
        (a.cod_director = @cod_director Or @cod_director is null) and 
        (a.cod_estado = @cod_estado Or @cod_estado is null) and 
        (a.fe_estado = @fe_estado Or @fe_estado is null) and 
        (a.cod_situacion  = @cod_situacion Or @cod_situacion is null) and 
        (a.ult_accion  = @ult_accion Or @ult_accion is null) and 
        (a.audit_user  = @audit_user Or @audit_user is null) and 
        (a.audit_date  = @audit_date Or @audit_date is null) and 
        (a.corp_local  = @corp_local Or @corp_local is null) and 
        (a.cod_orientacion = @cod_orientacion Or @cod_orientacion is null) and 
        (a.importancia_cod  = @importancia_cod Or @importancia_cod is null) and 
        (a.importancia_prf  = @importancia_prf Or @importancia_prf is null) and 
        (a.importancia_usr  = @importancia_usr Or @importancia_usr is null) and 
        (a.fe_fec_plan  = @fe_fec_plan Or @fe_fec_plan is null) and 
        (a.fe_ini_orig  = @fe_ini_orig Or @fe_ini_orig is null) and 
        (a.fe_fin_orig  = @fe_fin_orig Or @fe_fin_orig is null) and 
        (a.fe_fec_orig  = @fe_fec_orig Or @fe_fec_orig is null) and 
        (a.cant_planif  = @cant_planif Or @cant_planif is null) and 
        (a.prj_nrointerno = @prj_nrointerno Or @prj_nrointerno is null) and 
        (a.byp_comite = @byp_comite Or @byp_comite is null) and 
        (a.cod_bpar = @cod_bpar Or @cod_bpar is null) and 
        (a.cod_clase = @cod_clase Or @cod_clase is null) and 
        (a.pet_imptech = @pet_imptech Or @pet_imptech is null) and 
        (a.pet_sox001 = @pet_sox001 Or @pet_sox001 is null)
return(0) 
go

grant Execute  on dbo.sp_GetUnaPeticionTodas to GesPetUsr 
go
