/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetMotivosReplan'
go

if exists (select * from sysobjects where name = 'sp_GetMotivosReplan' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetMotivosReplan
go

create procedure dbo.sp_GetMotivosReplan
	@cod_motivo_replan	int=null,
	@estado_hab			char(1)=null
as
	select 
		a.cod_motivo_replan,
		a.nom_motivo_replan,
		a.estado_hab
	from 
		GesPet.dbo.MotivosReplan a
	where
		(a.cod_motivo_replan = @cod_motivo_replan or @cod_motivo_replan is null) and
		(a.estado_hab = @estado_hab or @estado_hab is null)
	return(0)
go

grant execute on dbo.sp_GetMotivosReplan to GesPetUsr
go

print 'Actualización realizada.'
