/*
-000- a. FJS 29.07.2011 - Nuevo SP para agregar los grupos por petición en la planificación en un solo paso.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionPlancab2'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionPlancab2' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionPlancab2
go

create procedure dbo.sp_InsertPeticionPlancab2
	@per_nrointerno	int,
	@pet_nrointerno	int,
	@prioridad		char(1)
as 
	declare @estado_periodo	char(6)

	select @estado_periodo = per.per_estado
	from GesPet..Periodo per
	where per.per_nrointerno = @per_nrointerno

	insert into GesPet..PeticionPlandet
		select 
			@per_nrointerno,
			@pet_nrointerno,
			pg.cod_grupo,
			plan_orden = case			-- Solo el grupo en ejecución debe tener orden cero: el resto de los grupos, deben tener orden indeterminado.
				when (
					select count(*)
					from
						GesPet..Peticion x inner join
						GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
						GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
					where 
						x.pet_nrointerno = p.pet_nrointerno and
						y.cod_gerencia = 'DESA' and
						charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
						(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
						(z.grupo_homologacion = 'S')
				) = 1 and @prioridad = '1' then 0
				when (
					select count(*)
					from
						GesPet..Peticion x inner join
						GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
						GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
					where 
						x.pet_nrointerno = p.pet_nrointerno and
						y.cod_gerencia = 'DESA' and
						charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
						(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
						(z.grupo_homologacion = 'S')
				) > 1 and @prioridad = '1' and (pg.cod_estado = 'EJECUC' or (pg.cod_estado = 'SUSPEN' and pg.cod_motivo in (1,2,7,8))) then 0
				else null
			end,
			plan_ordenfin = null,
			plan_estado = 'PLANIF',
			plan_actfch = getdate(),
			plan_actusr = SUSER_NAME(),
			plan_act2fch = null,
			plan_act2usr = null,
			case
				when charindex(@estado_periodo,'PLAN10|PLAN20|PLAN30|')>0 then 1
				when charindex(@estado_periodo,'PLAN36|')>0 then 2
				else null
			end,
			plan_estadoini = 1,
			plan_motnoplan = null,
			plan_fealcadef = null,
			plan_fefunctst = null,
			plan_horasper = null,
			plan_desaestado = null,
			plan_motivsusp = null,
			plan_motivrepl = null,
			plan_feplanori = null,
			plan_feplannue = null,
			plan_cantreplan = null,
			plan_horasreplan = null,
			plan_fchreplan = null,
			plan_fchestadoini = null
		from
			GesPet..PeticionGrupo pg left join
			GesPet..Peticion p on (pg.pet_nrointerno = p.pet_nrointerno) inner join
			GesPet..Grupo g on (pg.cod_grupo = g.cod_grupo)
		where
			pg.pet_nrointerno = @pet_nrointerno and
			(charindex(p.cod_clase, 'NUEV|EVOL|') > 0 or (charindex(p.cod_clase,'OPTI|')>0 and p.pet_nrointerno is not null)) and
			charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
			(pg.cod_gerencia = 'DESA') and
			(g.grupo_homologacion = 'S' and charindex(pg.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0) and
			(charindex(pg.cod_estado, 'ESTIOK|ESTIRK|ESTIMA|PLANOK|PLANIF|PLANRK|EJECRK|EJECUC|SUSPEN|') > 0) 
			and pg.cod_grupo not in 
				(select x.cod_grupo 
				 from GesPet..PeticionPlandet x 
				 where 
					x.per_nrointerno = @per_nrointerno and
					x.pet_nrointerno = @pet_nrointerno)
	return(0)
go

grant execute on dbo.sp_InsertPeticionPlancab2 to GesPetUsr
go

print 'Actualización realizada.'
go