use GesPet
go
print 'sp_DelAgrupPetic'
go
if exists (select * from sysobjects where name = 'sp_DelAgrupPetic' and sysstat & 7 = 4)
drop procedure dbo.sp_DelAgrupPetic
go
create procedure dbo.sp_DelAgrupPetic 
             @agr_nrointerno   int=0,
             @pet_nrointerno   int=0
as 

    delete AgrupPetic where  agr_nrointerno=@agr_nrointerno and pet_nrointerno=@pet_nrointerno
 
return(0)   
go



grant execute on dbo.sp_DelAgrupPetic to GesPetUsr 
go
