use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoAcargoArea'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoAcargoArea' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoAcargoArea
go

create procedure dbo.sp_GetRecursoAcargoArea 
    @cod_direccion      char(8)='', 
    @cod_gerencia       char(8)='', 
    @cod_sector         char(8)='', 
    @cod_grupo          char(8)='' 
as 
	select 
		r.cod_recurso, 
		r.nom_recurso, 
		r.vinculo, 
		r.cod_gerencia, 
		r.cod_direccion, 
		r.cod_sector, 
		r.cod_grupo, 
		r.flg_cargoarea, 
		r.estado_recurso, 
		r.horasdiarias, 
		r.observaciones, 
		r.dlg_recurso, 
		r.dlg_desde, 
		r.dlg_hasta,
		r.email,
		r.euser
	from 
		GesPet..Recurso r
	where
		r.cod_direccion = @cod_direccion and 
		r.cod_gerencia = @cod_gerencia and 
		r.cod_sector = @cod_sector and 
		r.cod_grupo = @cod_grupo and 
		r.estado_recurso = 'A' and 
		r.flg_cargoarea = 'S'
	order by 
		r.nom_recurso 
	
	return(0) 
go

grant execute on dbo.sp_GetRecursoAcargoArea to GesPetUsr 
go

print 'Actualización realizada.'
