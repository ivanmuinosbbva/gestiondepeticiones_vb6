/*
-000- a. FJS 05.04.2011 - Nuevo SP para listar peticiones en estado terminal con fecha de finalizaci�n mayor a 90 d�as, para zipear sus archivos.
-001- a. FJS 14.07.2011 - No se devuelve el objeto incrustado (BLOB) porque se traer� en otro sp puntualmente para una petici�n. 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesToZip'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesToZip' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesToZip
go

create procedure dbo.sp_GetPeticionesToZip
	@pet_nrointerno	int=null
as 
	set chained off
	declare @dias		int
	declare @new_char	char(1)

	select @dias = 90
	select @new_char = ''

	select @dias = var_numero from GesPet..Varios where var_codigo = 'ZIPDIAS'
	select @new_char = var_texto from GesPet..Varios where var_codigo = 'REEMPCHR'

	select 
		pa.pet_nrointerno,
		pa.adj_tipo,
		pa.adj_file,
		--adj_filefmt = str_replace(pa.adj_file,char(39),@new_char),
		p.pet_nroasignado,
		substring(ltrim(rtrim(pa.adj_file)),1,char_length(ltrim(rtrim(pa.adj_file)))-charindex('.',ltrim(rtrim(reverse(pa.adj_file))))) as adj_filenoext
	from 
		GesPet..PeticionAdjunto pa inner join
		GesPet..Peticion p on (pa.pet_nrointerno = p.pet_nrointerno)
	where
		(@pet_nrointerno is null or pa.pet_nrointerno = @pet_nrointerno) and
		charindex(substring(ltrim(rtrim(upper(pa.adj_file))),char_length(ltrim(rtrim(pa.adj_file)))-charindex('.',ltrim(rtrim(reverse(pa.adj_file))))+1,char_length(ltrim(rtrim(pa.adj_file)))),'.ZIP|.RAR|')=0 and
		charindex(char(39),pa.adj_file)=0 and 
		pa.pet_nrointerno in (
			select p.pet_nrointerno
			from GesPet..Peticion p
			where
				charindex(p.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0 and
				datediff(dd,p.fe_estado,getdate())>@dias)
	order by 1
	return(0)
go

grant execute on dbo.sp_GetPeticionesToZip to GesPetUsr
go

exec sp_procxmode 'sp_GetPeticionesToZip', anymode
go

print 'Actualizaci�n realizada.'
go
