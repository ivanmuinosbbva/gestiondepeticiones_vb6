/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetRptdet'
go

if exists (select * from sysobjects where name = 'sp_GetRptdet' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRptdet
go

create procedure dbo.sp_GetRptdet
	@rptid	char(10)=''
as
	select 
		rpt.rptid,
		rpt.rptitem,
		rpt.rptnom,
		rpt.rpthab
	from 
		GesPet.dbo.Rptdet rpt
	where
		rpt.rptid = @rptid
go

grant execute on dbo.sp_GetRptdet to GesPetUsr
go

print 'Actualización realizada.'
go
