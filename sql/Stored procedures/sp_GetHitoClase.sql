/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.22 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHitoClase'
go

if exists (select * from sysobjects where name = 'sp_GetHitoClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHitoClase
go

create procedure dbo.sp_GetHitoClase
	@hitoid	int=null
as
	select 
		p.hitoid,
		p.hitodesc,
		p.hitoexpos
	from 
		GesPet.dbo.HitoClase p
	where
		(p.hitoid = @hitoid or @hitoid is null)
go

grant execute on dbo.sp_GetHitoClase to GesPetUsr
go

print 'Actualización realizada.'
go
