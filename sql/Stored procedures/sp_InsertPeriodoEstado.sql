/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 15.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeriodoEstado'
go

if exists (select * from sysobjects where name = 'sp_InsertPeriodoEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeriodoEstado
go

create procedure dbo.sp_InsertPeriodoEstado
	@cod_estado_per	char(8),
	@nom_estado_per	char(100)
as
	insert into GesPet.dbo.PeriodoEstado (
		cod_estado_per,
		nom_estado_per)
	values (
		@cod_estado_per,
		@nom_estado_per)
	return(0)
go

grant execute on dbo.sp_InsertPeriodoEstado to GesPetUsr
go

print 'Actualización realizada.'
